//
//  SlideMenuViewController.h
//  genie
//
//  Created by ketan on 20/04/15.
//  Copyright (c) 2015 Mobicule. All rights reserved.
//

#import <FirebaseAnalytics/FirebaseAnalytics.h>

#import "AppDelegate.h"
#import "BaseViewController.h"
#import "KLCPopup.h"
#import "LoginViewPopUp.h"
#import "CustomIOSAlertView.h"
#import "LoadingView.h"




@interface SlideMenuViewController : BaseViewController <UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UIAlertViewDelegate,CustomIOSAlertViewDelegate>
{
    LoadingView *activityLoadingView;
}

@property (strong, nonatomic) IBOutlet UIView *slideMenuView;
@property (strong, nonatomic) IBOutlet UITableViewCell *headerView;
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UITableView *tblViewMenu;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewSectionFooter;
@property (weak, nonatomic) IBOutlet UIView *viewForTableHeader;
@property (weak, nonatomic) IBOutlet UIView *viewContainer;

#pragma mark - Visa PopUp Elements
@property (weak, nonatomic) IBOutlet UILabel *lblPopUpHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblPopUpInfo;

@property (strong, nonatomic) IBOutlet UIView *viewVisaPopUp;
@property (weak, nonatomic) IBOutlet UITextField *txtFldFirstName;
@property (weak, nonatomic) IBOutlet UITextField *txtFldLastName;
@property (weak, nonatomic) IBOutlet UITextField *txtFldPhoneNo;
@property (weak, nonatomic) IBOutlet UITextField *txtFldMailId;

- (IBAction)btnSubmitOnVisaInsuranceClicked:(id)sender;


#pragma mark - Email Us PopUp
@property (strong, nonatomic) IBOutlet UIView *viewEmailUsPopUp;
@property (weak, nonatomic) IBOutlet UITextField *txtFlgQuerryRltdTo;
@property (weak, nonatomic) IBOutlet UITextField *txtFldEmailPopUpName;
@property (weak, nonatomic) IBOutlet UITextField *txtFldEmailPopUpMailId;
@property (weak, nonatomic) IBOutlet UITextField *txtFldEmailPopUpPhoneNo;
@property (weak, nonatomic) IBOutlet UITextField *txtFldEmailPopUpQueryFor;
@property (weak, nonatomic) IBOutlet UITextField *txtFldEmailPopUpQryRltdTo;
@property (weak, nonatomic) IBOutlet UITextField *txtFldEmailPopUpBFN;
@property (weak, nonatomic) IBOutlet UITextView *txtViewEmailPopUpComplaint;
@property (weak, nonatomic) IBOutlet UILabel *lblUserId;
- (IBAction)btnSubmitOnEmilPopUpClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnSignInOut;
@property (strong, nonatomic) IBOutlet UILabel *lblWelcomeText;


#pragma mark - Select Query for Pop Up
@property (strong, nonatomic) IBOutlet UIView *viewPopUpQueryFor;

@property (weak, nonatomic) IBOutlet UIImageView *imageViewQueryFor;

- (IBAction)btnOnQueryForPopUpClicked:(id)sender;

#pragma mark - Select Query Related to PopUp
@property (strong, nonatomic) IBOutlet UIView *viewPopUpQueryRelatedTo;

@property (weak, nonatomic) IBOutlet UIImageView *imageViewQueryRelatedTo;

- (IBAction)btnOnQueryRelatedPopUpClicked:(id)sender;
- (IBAction)onSignInButtonClicked:(id)sender;

-(void)setLoginStatusValue;
@property (weak, nonatomic) IBOutlet UILabel *labelDealsOffers;

@end
