//
//  MapTabViewController.h
//  holidays
//
//  Created by ketan on 29/09/15.
//  Copyright © 2015 Mobicule. All rights reserved.
//

#import "BaseViewController.h"
#import <MapKit/MapKit.h>
#import "iCarousel.h"

typedef void(^isCompleted)(BOOL);

@interface MapTabViewController : BaseViewController<iCarouselDataSource, iCarouselDelegate,MKMapViewDelegate>
{
    LoadingView *activityLoadingView;
}
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (nonatomic, strong) IBOutlet iCarousel *carousel;
@property (strong,nonatomic)NSArray *arrayOfHolidays;
@property (strong, nonatomic) IBOutlet UIView *viewForCarousalCell;
@property (strong,nonatomic) NSString *searchedCity;




@end
