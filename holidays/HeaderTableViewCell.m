//
//  HeaderTableViewCell.m
//  holidays
//
//  Created by Kush_Tech on 03/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "HeaderTableViewCell.h"
#import "UIImageView+WebCache.h"
@implementation HeaderTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
}

-(void)loadImageFromHolidayPackageDetail:(HolidayPackageDetail*)packAgeDetails withHolidayObj:(Holiday*)holidayObj{
    self.packageDetailInCell = packAgeDetails;
    dispatch_async(dispatch_get_main_queue(), ^(void){
       self.like_view.layer.cornerRadius = self.like_view.frame.size.height/2;
        self.share_view.layer.cornerRadius = self.share_view.frame.size.height/2;
        self.compare_view.layer.cornerRadius = self.compare_view.frame.size.height/2;
    });
    
    
    NSArray *imgDetailInfoArray = packAgeDetails.arrayPackageImagePathList;
    if (imgDetailInfoArray.count != 0)
    {
        NSDictionary *imageDict = [[NSDictionary alloc] init];
//        NSDictionary *imageDict = imgDetailInfoArray[0];
        for (NSDictionary *object in imgDetailInfoArray){
            if ([[object objectForKey:@"position"] intValue] == 1){
                imageDict = object;
                break;
            }
        }
//        NSString* imageName = [[imageDict valueForKey:@"path"] stringByReplacingOccurrencesOfString:@" " withString:@"_"];
//        NSString *imageUrlString = [NSString stringWithFormat:@"%@/images/holidays/%@/photos/%@",kUrlForImage,packAgeDetails.strPackageId,imageName];
        
        NSString *imageUrlString = [holidayObj.packageImagePath stringByReplacingOccurrencesOfString:@" " withString:@""];
        if(imageUrlString == nil){
            NSDictionary *imageDict = imgDetailInfoArray[0];
            NSString* imageName = [[imageDict valueForKey:@"path"] stringByReplacingOccurrencesOfString:@" " withString:@"_"];
            imageUrlString = [NSString stringWithFormat:@"%@/images/holidays/%@/photos/%@",kUrlForImage,packAgeDetails.strPackageId,imageName];
            [self.headerDelegate setImageName:imageUrlString];
            self.imgUrlString = imageUrlString;
        }else{
            [self.headerDelegate setImageName:imageUrlString];
            self.imgUrlString = imageUrlString;
        }
        NSURL* urlImage=[NSURL URLWithString:imageUrlString];
        
        if(urlImage)
        {
            UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            
            indicator.center = self.tour_imgView.center;// it will display in center of image view
            [self.tour_imgView addSubview:indicator];
            [indicator startAnimating];
            
            [self.tour_imgView sd_setImageWithURL:urlImage placeholderImage:[UIImage imageNamed:@"defaultBanner.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
             {
                [indicator stopAnimating];
            }];
        }
    }
    [self.btn_Compare setTag:([[CoreDataSingleton sharedInstance] checkIsPackagePresentInEntityWithName:CompareListHoliday withPackageID:packAgeDetails.strPackageId]) ? 1 : 0];
//    @"compareSelect" : @"compareNoSelect"
    self.compare_ImgView.image =  [UIImage imageNamed:([[CoreDataSingleton sharedInstance] checkIsPackagePresentInEntityWithName:CompareListHoliday withPackageID:packAgeDetails.strPackageId]) ? @"compareSelect" : @"compareNoSelectInPDP"];
//    self.share_ImgView.image = [self coloredImage:[UIImage imageNamed:@"shareInPDP"] withColor:[UIColor colorWithRed:0 green:83/255.0 blue:169/255.0 alpha:1.0]];
    self.heart_ImgView.image = [UIImage imageNamed:([[CoreDataSingleton sharedInstance] checkIsPackagePresentInEntityWithName:WishListHoliday withPackageID:packAgeDetails.strPackageId]) ? @"heartBlue" : @"wishListInPDP"];
    self.holidayObjInHeaderTableView = holidayObj;
    self.btn_likes.tag = ([[CoreDataSingleton sharedInstance] checkIsPackagePresentInEntityWithName:WishListHoliday withPackageID:packAgeDetails.strPackageId]) ? 1 : 0;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)btn_sharePress:(id)sender {
    [self.headerDelegate sharePress];
}
- (IBAction)btn_likePress:(id)sender {
    UIButton* myButton = (UIButton*)sender;
    if (myButton.tag == 0){
       NSLog(@"Added In WishList");
        [myButton setTag:1];
        [self likePressed];
        [self.headerDelegate likePress:YES];
    }else{
        NSLog(@"Removed From WishList");
        [myButton setTag:0];
        [self dislikePressed];
        [self.headerDelegate likePress:NO];
    }
    
}
- (IBAction)btn_ViewGallery:(id)sender {
    [self.headerDelegate showViewGallery];
    
}
- (IBAction)btn_ComparePress:(id)sender {
    UIButton* myButton = (UIButton*)sender;
    if (myButton.tag == 0){
        [myButton setTag:1];
        [self addToCompareList];
    }
    NSLog(@"packageDetail %@",self.compPackageDetailDict);
    [self.headerDelegate comparePress:(myButton.tag == 1)];
}
-(void)likePressed{
    NSLog(@"packageDetail %@",self.holidayObjInHeaderTableView.objDetailDict);
    NSString * jsonString = [self convertJsonDictToString:self.holidayObjInHeaderTableView.objDetailDict];
    NSDictionary *dict =  @{@"packageData" : jsonString,@"packageID" : self.holidayObjInHeaderTableView.strPackageId, @"packageName" : self.holidayObjInHeaderTableView.strPackageName, @"packagePrize" : [NSNumber numberWithInt:self.holidayObjInHeaderTableView.packagePrise],@"packageImgUrl" : self.holidayObjInHeaderTableView.packageImagePath};
    NSLog(@"%@",dict);
    WishListHolidayObject *wishObject = [[WishListHolidayObject alloc]initWithWishListObject:dict];
    [[CoreDataSingleton sharedInstance]saveWishListObjectInCoreDataForEntity:WishListHoliday withObject:wishObject];
    
}
-(void)dislikePressed{
    NSArray *array = [[CoreDataSingleton sharedInstance] getArrayOfObjectForEntityName:WishListHoliday];
    NSArray *packIdArray = [[CoreDataSingleton sharedInstance]getdeletedObjectFromPackageListArray:array withPackID:self.holidayObjInHeaderTableView.strPackageId forEntityName:WishListHoliday];
    if ([packIdArray count]>0){
        [[[CoreDataSingleton sharedInstance]getManagedObjectContext] deleteObject:[packIdArray firstObject]];
        NSError * error = nil;
        if (![[[CoreDataSingleton sharedInstance]getManagedObjectContext] save:&error]){
            NSLog(@"Error ! %@", error);
        }
    }
}
-(void)addToCompareList{
    NSString *packageTypeString = (self.packageDetailInCell.startingPriceStandard > 0) ? standardPackage : (self.packageDetailInCell.startingPriceDelux > 0) ? deluxePackage : (self.packageDetailInCell.startingPricePremium > 0) ? premiumPackage : standardPackage;
    NSInteger mainStartPrice;
    if ([packageTypeString isEqualToString:standardPackage]){
        mainStartPrice = self.packageDetailInCell.startingPriceStandard;
    }else if ([packageTypeString isEqualToString:deluxePackage]){
        mainStartPrice = self.packageDetailInCell.startingPriceDelux;
    }else{
        mainStartPrice = self.packageDetailInCell.startingPricePremium;
    }
    NSString *imgString = (self.imgUrlString) ? self.imgUrlString : @"";
    NSString * jsonString = [self convertJsonDictToString:self.compPackageDetailDict];
    NSDictionary *dict =  @{@"packageData" : jsonString,@"packageID" : self.packageDetailInCell.strPackageId, @"packageName" : self.packageDetailInCell.strPackageName, @"packagePrize" : [NSNumber numberWithInteger:mainStartPrice],@"packageImgUrl" : imgString};
    NSLog(@"%@",dict);
    CompareHolidayObject *compareObject = [[CompareHolidayObject alloc]initWithCompareHolidayObject:dict];
    [[CoreDataSingleton sharedInstance]saveCompareListObjectInCoreDataForEntity:CompareListHoliday withObject:compareObject];
    
}

@end
