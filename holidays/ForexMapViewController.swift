//
//  ForexMapViewController.swift
//  holidays
//
//  Created by ketan on 20/12/17.
//  Copyright © 2017 Mobicule. All rights reserved.
//

import UIKit
import MapKit

class ForexMapViewController: UIViewController
{
    var  destinationName : NSString = ""
    var lattitude : Double  =  0.00;
    var longitude : Double  =  0.00;
    @IBOutlet var mapView: MKMapView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.showLocationOnMap()
        
//        if destinationName.isEqual(to: "")
//        {
//            let annotation = MKPointAnnotation()
//            
//            let branchCordinates : CLLocationCoordinate2D = CLLocationCoordinate2D.init(latitude: lattitude, longitude: longitude)
//            
//            annotation.coordinate = branchCordinates;
//            
//            mapView.addAnnotation(annotation)
//            
//            let circle = MKCircle(center: branchCordinates, radius: 20)
//            
//            mapView.setRegion(MKCoordinateRegion(center: branchCordinates, span: MKCoordinateSpan(latitudeDelta: 0.002, longitudeDelta: 0.002)), animated: true)
//            mapView.add(circle)
//
//        }
//        else
//        {
//            
//            let address = destinationName
//            
//             self.forwardGeocoding(address: address as String)
//            
////            let geoCoder = CLGeocoder()
////            geoCoder.geocodeAddressString(address as String) { (placemarks, error) in
////                guard
////                    let placemarks = placemarks,
////                    let location = placemarks.first?.location
////                    else
////                {
////                        // handle no location found
////                        return
////                }
//            
//                // Use your location
//                
//            
//
//            }
//
        }
    
    @IBAction func onCloseButtonClicked(_ sender: Any)
    {
        self .dismiss(animated: true, completion: nil)
    }
    
    func showLocationOnMap()
    {
        mapView.mapType = MKMapType.standard
        
        let location = CLLocationCoordinate2D(latitude: lattitude,longitude: longitude)
        
        let span = MKCoordinateSpan.init(latitudeDelta: 0.05, longitudeDelta: 0.05)
        let region = MKCoordinateRegion(center: location, span: span)
        mapView.setRegion(region, animated: true)
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = location
        //        annotation.title = "iOSDevCenter-Kirit Modi"
        //        annotation.subtitle = "Ahmedabad"
        mapView.addAnnotation(annotation)
    }
    
//    func forwardGeocoding(address: String)
//    {
//        CLGeocoder().geocodeAddressString(address, completionHandler:
//        {
//            (placemarks, error) in
//            
//            if error != nil
//            {
//                print(error!)
//                return
//            }
//            if (placemarks?.count)! > 0
//            {
//                let placemark = placemarks?[0]
//                let location = placemark?.location
//                let coordinate = location?.coordinate
//                print("\nlat: \(coordinate!.latitude), long: \(coordinate!.longitude)")
//               // if (placemark?.areasOfInterest?.count)! > 0 {
//                 //   let areaOfInterest = placemark!.areasOfInterest![0]
//                 //   0print(areaOfInterest)
//                    
//                    
//                    let branchCordinates : CLLocationCoordinate2D = CLLocationCoordinate2D.init(latitude: coordinate!.latitude, longitude: coordinate!.longitude)
//                
//                let annotation = MKPointAnnotation()
//                
//                annotation.coordinate = branchCordinates;
//                
//                    self.mapView.addAnnotation(annotation)
//                    
//                    let circle = MKCircle(center: coordinate!, radius: 20)
//                    
//                    self.mapView.setRegion(MKCoordinateRegion(center: branchCordinates, span: MKCoordinateSpan(latitudeDelta: 0.002, longitudeDelta: 0.002)), animated: true)
//                    self.mapView.add(circle)
//
////                else
////                {
////                    print("No area of interest found.")
////                }
//            }
//        })
//    }
}
