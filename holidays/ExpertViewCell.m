//
//  ExpertViewCell.m
//  holidays
//
//  Created by Kush_Tech on 11/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "ExpertViewCell.h"

@implementation ExpertViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.backgroundColor = [UIColor clearColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
