//
//  DeviationViewController.swift
//  sotc-consumer-application
//
//  Created by Saurav Kumar on 09/08/17.
//  Copyright © 2017 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit
import MessageUI

class DeviationViewController: UIViewController , UIActionSheetDelegate ,UITextFieldDelegate ,PassengerPopUpProtocol,UIPopoverPresentationControllerDelegate,MFMailComposeViewControllerDelegate {
    
    //MARK: - Outlets
    
    @IBOutlet weak var passangerFloatLabelTextField: FloatLabelTextField!
    
    
    
    @IBOutlet weak var preTourRadioBtn: UIButton!
    @IBOutlet weak var deviationFloatLabelTextField: FloatLabelTextField!
    
    @IBOutlet weak var postTouRadioBtn: UIButton!
    @IBOutlet weak var deviationDetailsUIView: UIView!
    
    @IBOutlet weak var oldStartDateFloatLabelTextField: FloatLabelTextField!
    
    @IBOutlet weak var newDateFloatLabelTextField: FloatLabelTextField!
    
    @IBOutlet weak var remarkFloatLabelTextField: FloatLabelTextField!
    
    @IBOutlet weak var sendDeviationRequestUIButton: UIButton!
    
    @IBOutlet var checkMarkImageView: UIImageView!
    
    var datePickerView:UIDatePicker!
    
    
    var selecedPassenger:Passenger?
    
    var selectedPassengerNumber:String? = nil
    
    var selectedDeviation:String = Constant.Deviation.PRE_DEVIATION
    
     var  selectedPassengerDetails : [(isSelected: Bool, index: Int, passenger: Passenger?)] = []
    
    var deviationDropDownPassengerDetails : [(isOpen: Bool,isSelectAll: Bool, section: Int ,row : [(isSelected: Bool, index: Int ,passenger:Passenger?)])] = []
    
    var tourDetails:Tour?
    
    var overlay: UIView?
    
    //MARK: - Controller Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
                 self.oldStartDateFloatLabelTextField.text = AppUtility.convertDateToString(currentDateFormat: "yyyy-MM-dd", date: tourDetails?.departureDate! as! Date , requiredDateFormat: "dd-MM-yyyy")
        
        if self.selecedPassenger != nil
        {
            self.passangerFloatLabelTextField.text = "\(selecedPassenger?.firstName ?? "") \(selecedPassenger?.lastName ?? "")"
            
            self.selectedPassengerNumber = self.selecedPassenger?.passengerNumber
        }
        
       // self.oldStartDateFloatLabelTextField.text = AppUtility.convertDateToString(date: )
        
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.setRedNavigationBar()
        self.navigationController?.setnavigatiobBarTitle(title: Title.DEVIATION, viewController: self)
        //self.navigationController?.setEmergencyNavigationRightButton(showEmergencyButton: true, viewController: self)
        self.navigationController?.setNagationBackButton(showRightButton: true, viewController: self)
        
        
    }
    
    func backButtonClick() -> Void {
        
        
        self.navigationController?.popViewController(animated: true)
        
        self.navigationController?.setNavigationBarTranslucent()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func deviationRadioSelection(_ sender: UIButton) {
        
    self.view.endEditing(true)
        
        if sender.tag == 1
        {
            if self.selectedDeviation != Constant.Deviation.PRE_DEVIATION
            {
                self.newDateFloatLabelTextField.text = ""
            }
            
            self.selectedDeviation = Constant.Deviation.PRE_DEVIATION
            
            self.preTourRadioBtn.setImage(#imageLiteral(resourceName: "seletedRadio_button"), for: .normal)
            
            self.postTouRadioBtn.setImage(#imageLiteral(resourceName: "unSelectedRadio_Button"), for: .normal)
            
            
            
             self.oldStartDateFloatLabelTextField.text = AppUtility.convertDateToString(currentDateFormat: "yyyy-MM-dd", date: tourDetails?.departureDate! as! Date , requiredDateFormat: "dd-MM-yyyy")
           
            
        }else if sender.tag == 2
        {
            
            if self.selectedDeviation != Constant.Deviation.POST_DEVIATION
            {
                self.newDateFloatLabelTextField.text = ""
            }
            
            self.postTouRadioBtn.setImage(#imageLiteral(resourceName: "seletedRadio_button"), for: .normal)
            
             self.preTourRadioBtn.setImage(#imageLiteral(resourceName: "unSelectedRadio_Button"), for: .normal)
            
           
            
             self.selectedDeviation = Constant.Deviation.POST_DEVIATION
            
            self.oldStartDateFloatLabelTextField.text = AppUtility.convertDateToString(currentDateFormat: "yyyy-MM-dd", date: tourDetails?.arrivalDate! as! Date , requiredDateFormat: "dd-MM-yyyy")
        }
        
    }
    
    //MARK: - UITextField Delegate Implementaion
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        switch textField {
            
        case self.passangerFloatLabelTextField:self.displayPassangerActionSheet()
            
              return false
            
        case self.deviationFloatLabelTextField: self.displayDeviationActionSheet()
              return false
            
        case self.newDateFloatLabelTextField: self.displayDatePicker(textField: textField)
            
            return true
            
        default:
            return true
        }
        
 
        return true
    }
    
    public func textFieldDidBeginEditing(_ textField: UITextField)
    {
        
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool // called when 'return' key pressed. return NO to ignore.
    {
        textField.resignFirstResponder()
        return true;
    }
    
   
    
    //MARK: - Create Action Sheet
    
    private func displayDatePicker(textField:UITextField)
    {
         datePickerView  = UIDatePicker()
        
        datePickerView.datePickerMode = UIDatePicker.Mode.date
        
        textField.inputView = datePickerView
        
        //datePickerView.minimumDate = Date()
        
        if selectedDeviation == Constant.Deviation.PRE_DEVIATION
        {
        
       if let departureDate:Date = tourDetails?.departureDate as Date?
        {
             datePickerView.maximumDate = departureDate //.addingTimeInterval(-30*24*60*60)
            
              datePickerView.minimumDate = Date()
        
        }
        
        }else if selectedDeviation == Constant.Deviation.POST_DEVIATION
        {
            if let arrivalDate:Date = tourDetails?.arrivalDate as Date?
            {
                datePickerView.minimumDate = arrivalDate //.addingTimeInterval(-30*24*60*60)
                
            }
            
        }
       
        
       // addTarget(self, action: #selector(datePickerValueChanged(_:)), for: .touchUpInside)
        
        datePickerView.addTarget(self, action: #selector(datePickerValueChanged(_:)), for: .valueChanged)
        
        var toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
       // toolBar.tintColor = UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1)
        let doneButton = UIBarButtonItem(title: AlertMessage.TITLE_DONE, style: UIBarButtonItem.Style.done, target: self, action: #selector(donePressed))
        let cancelButton = UIBarButtonItem(title: AlertMessage.TITLE_CANCEL, style: UIBarButtonItem.Style.plain, target: self, action: #selector(cancelPressed))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        
        textField.delegate = self
        textField.inputAccessoryView = toolBar
        
    
    }
    
    @objc func donePressed()
    {
        
        self.newDateFloatLabelTextField.text = AppUtility.convertDateToString(currentDateFormat: "MMM-dd-yyyy", date: datePickerView.date , requiredDateFormat: "dd-MM-yyyy")
        
         view.endEditing(true)
        
    }

    @objc func cancelPressed(){
        view.endEditing(true) // or do something
    }
    
    private func displayPassangerActionSheet()
    {
        
        self.passangerFloatLabelTextField.resignFirstResponder()
        
       /* let passangerActionSheet:UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        passangerActionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:nil))
        
        if let passengerList:Array<Passenger> = tourDetails?.passengerRelation?.allObjects as? Array<Passenger>
        {
        
        
        for pasenger in passengerList /*DeviationConstants.PASSANGER_NAMES*/
        {
             let passengerName = pasenger.firstName! + " " + pasenger.lastName!
          
            
            passangerActionSheet.addAction(UIAlertAction(title: passengerName, style: .default, handler: { (action: UIAlertAction!)in
            
            self.passangerFloatLabelTextField.text = action.title
                
            }))
        
        }
        
                self.present(passangerActionSheet, animated: true, completion: nil)
        }
        
 */
        
        self.presentPassengerPopover()
    }
    
    private func displayDeviationActionSheet()
    {
        
        self.deviationFloatLabelTextField.resignFirstResponder()
        
        let deviationActionSheet:UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        deviationActionSheet.addAction(UIAlertAction(title: AlertMessage.TITLE_CANCEL, style: .cancel, handler:nil))
        
        for i in DeviationConstants.DEVIATION_TYPES
        {
            deviationActionSheet.addAction(UIAlertAction(title: i, style: .default, handler: { (action: UIAlertAction!)in
                
                self.deviationFloatLabelTextField.text = action.title
                
            }))
        }
        
        self.present(deviationActionSheet, animated: true, completion: nil)
        
    }
    
    //MARK: - Button Actions
    
    @objc func datePickerValueChanged(_ sender: UIDatePicker)
    {
        self.newDateFloatLabelTextField.text = AppUtility.convertDateToString(currentDateFormat: "MMM-dd-yyyy", date: sender.date, requiredDateFormat: "dd-MM-yyyy")
    }
    
    @IBAction func sendDeviationRequestAction(_ sender: UIButton) {
        
       /* let mailComposeViewController = configuredMailComposeViewController()
        
        if MFMailComposeViewController.canSendMail() {
            
            self.present(mailComposeViewController, animated: true, completion: nil)
            
        } else {
            
            self.showSendMailErrorAlert()
        }*/
     
        self.sendDeviationEmailHit()
        
    }
    
    func sendDeviationEmailHit()
    {
        
        let validationMessage:String = self.validateDevaitonDetails()
        
        if(validationMessage.caseInsensitiveCompare("") != ComparisonResult.orderedSame)
        {
            self.showAlertViewWithTitle(title: "", message: validationMessage)
        }else{
        
        let tourName:String =  (tourDetails?.tourName ?? "")!
        
        let branchName:String = (tourDetails?.branch ?? "")!
        
        let bookingId:String =  (tourDetails?.bfNumber ?? "")!
            
           
            
            let tourDateInNSDate = tourDetails?.departureDate
            
            var tourDate:String = ""
            
            if tourDateInNSDate != nil
            {
                 tourDate = AppUtility.convertDateToString(currentDateFormat: "yyyy-MM-dd", date:tourDateInNSDate! as Date, requiredDateFormat: "dd-MM-yyyy")
            }
            
            
            
            let passengerName =  self.passangerFloatLabelTextField.text!
            
            let bfnNumber:String =  tourDetails?.bfNumber ?? ""
        
        let groupDate =  self.oldStartDateFloatLabelTextField.text!
        
        let newData =  self.newDateFloatLabelTextField.text!
        
        let remark =  self.remarkFloatLabelTextField.text!
        
        var passengerArray : Array<Any> = []
        
        if self.selectedPassengerNumber != nil
        {
            let fullName =  self.selecedPassenger?.firstName ?? ""  + " " +  (self.selecedPassenger?.lastName ?? "")
            
            let emailId =  self.selecedPassenger?.emailId ?? ""
            
            let passengerDict:Dictionary<String,String> = ["fullname":fullName,"mailId":emailId]
            
            passengerArray.append(passengerDict)
            
        }else
        {
            for(_,data) in self.selectedPassengerDetails.enumerated()
            {
                let fullName =  data.passenger?.firstName ?? ""  + " " +  (data.passenger?.lastName ?? "")
                
                let emailId =  data.passenger?.emailId ?? ""
                
                let passengerDict:Dictionary<String,String> = ["fullname":fullName,"mailId":emailId]
                
                passengerArray.append(passengerDict)
                
                
            }
        }
            
            
            var contactNumber:String = ""
            
            let passengerContactNumber = self.selecedPassenger?.mobileNumber ?? ""
            
            if !passengerContactNumber.isEmpty && !(passengerContactNumber == "")
            {
                contactNumber = passengerContactNumber
                
            }else{
                
                contactNumber = tourDetails?.mobileNumber ?? ""
            }
        
       /* let sendMailDictionary:Dictionary<String,Any> = ["messageType":"DEVIATION","TourName":tourName,
        "TourDate":tourDate,
        "BookingID":bookingId,
        "BranchName":branchName,
        "ContactNumber":contactNumber,
        "SalesUser":self.tourDetails?.createdBy ?? "",
        "Remarks":remark,
        
        "CustomerName":(self.selecedPassenger?.firstName ?? "")! + " " + (selecedPassenger?.lastName ?? "")!,
        "GroupDate":AppUtility.changeDateFormat(currentDateFormat: "dd-MM-yyyy", date: groupDate, requiredDateFormat: "dd-MM-yyyy"),
        "NewDate":AppUtility.changeDateFormat(currentDateFormat: "dd-MM-yyyy", date: newData, requiredDateFormat: "dd-MM-yyyy"),"PassengerName":passengerArray]*/
            let fullName =  self.selecedPassenger?.firstName ?? ""  + " " +  (self.selecedPassenger?.lastName ?? "")
            
            let emailId =  self.selecedPassenger?.emailId ?? ""
            
            let sendMailDictionary:Dictionary<String,Any> = ["messageType":"DEVIATION",
                                                             "tourName":tourName,
                                                             "tourDate":tourDate,
                                                             "bookingID":bookingId,
                                                             "branchName":branchName,
                                                             "contactNumber":contactNumber,
                                                             "salesUser":self.tourDetails?.createdBy ?? "",
                                                             "remarks":remark,
                                                             "customerName":(self.selecedPassenger?.firstName!)! + " " + (selecedPassenger?.lastName!)!,
                                                             "groupDate":AppUtility.changeDateFormat(currentDateFormat: "dd-MM-yyyy", date: groupDate, requiredDateFormat: "dd-MM-yyyy"),
                                                             "newDate":AppUtility.changeDateFormat(currentDateFormat: "dd-MM-yyyy", date: newData, requiredDateFormat: "dd-MM-yyyy"),
                                                             "passengerName":fullName,
                                                             "mailId":emailId]
        
        LoadingIndicatorView.show("Loading")

        
            UserBookingController.sendEmailRequest(emailRequestData:sendMailDictionary, completion: { (status, message) in
                LoadingIndicatorView.hide()
                self.showAlertViewWithTitle(title: "", message: message)
                
                if status
                {
                    self.resetUI()
                    
                    
                    let notificationDate = Date()//.addingTimeInterval(2)  //.addingTimeInterval(24 * 60 * 60)
                    
                    let noticationDateTimeInMiliSec = (notificationDate.timeIntervalSince1970 * 1000)
                    
                    let notificationData:Dictionary<String,Any> = self.getDeviationNotificationData(bfnNumber: bfnNumber ,notificationDateTimeInMiliSec: noticationDateTimeInMiliSec)
                    
                    NotificationController.fireLocaleNotification(notificationUserInfo: notificationData,fireDateTime: notificationDate)
                }
            })
           
        //}
    }
    
    }
    
    func resetUI()
    {
        //self.oldStartDateFloatLabelTextField.text = ""
        
        self.newDateFloatLabelTextField.text = ""
        
        self.passangerFloatLabelTextField.text = ""
        
        self.remarkFloatLabelTextField.text = ""
        
        if self.selectedPassengerDetails.count > 0
        {
            self.selectedPassengerDetails.removeAll()
        }
        
        if self.deviationDropDownPassengerDetails.count > 0
        {
            self.deviationDropDownPassengerDetails.removeAll()
        }
        
        self.selectedPassengerNumber = nil
        
    }
    
    func validateDevaitonDetails() -> String
    {
        if self.selectedPassengerNumber == nil && self.selectedPassengerDetails.count <= 0
        {
            return AlertMessage.Deviation.SELECT_PASSENGER
            
        }else if self.selectedDeviation == ""
        {
             return AlertMessage.Deviation.SELECT_DEVIATION
            
        }else if self.newDateFloatLabelTextField.text == nil  || (self.newDateFloatLabelTextField.text?.isEmpty)!
        {
             return AlertMessage.Deviation.SELECT_DEVIATION_DATE
            
        }/*else if self.remarkFloatLabelTextField.text == nil  || (self.remarkFloatLabelTextField.text?.isEmpty)!
        {
             return "Please select passenger"
        }*/
        
        return ""
    }
    
    
    
    
    
    //MARK: Method to send Deviation-Request mail
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
       
        
       
        
        let tourName:String =  (tourDetails?.tourName!)!
        var branchName:String = (tourDetails?.branch!)!
        
        let bookingId:String =  (tourDetails?.tourCode!)!
        
        let departureDate:String =  AppUtility.convertDateToString(date: tourDetails?.departureDate! as! Date)
        
        let passengerName = "Passenger Name:- " + self.passangerFloatLabelTextField.text! + "\n"
        
       let groupDate = "Group Date:- " + departureDate + "\n"
        
        let newData = "New date:- " + self.newDateFloatLabelTextField.text! +  "\n"
        
        let remark = "Remark:- " + self.remarkFloatLabelTextField.text!  + "\n"
        
        let bodyMessage = "Dear SOTC Team,\n \t This is in reference to our " + tourName + " departing on " + departureDate + ", \n "+bookingId+"&gt; \n \t ( Booking details :-  "+branchName+" | Sales User ) \n \t We would like to change our air travel dates. We plan o travel in advance / return late. Request you to please check availability and advice the airfare difference applicable if any.\n " + passengerName + groupDate + newData + remark + "\n Awaiting your revert at the soonest. \n Thanking You, \n" + self.passangerFloatLabelTextField.text!
        
        mailComposerVC.setToRecipients(["aditi.modi@mobicule.com"])
        mailComposerVC.setSubject("Deviation request")
        mailComposerVC.setMessageBody(bodyMessage, isHTML: false)
        
        return mailComposerVC
    }
    
    func showSendMailErrorAlert() {
        let sendMailErrorAlert = UIAlertView(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.", delegate: self, cancelButtonTitle: AlertMessage.TITLE_OK)
        sendMailErrorAlert.show()
    }
    
    // MARK: MFMailComposeViewControllerDelegate
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
        
    }
    @IBAction func onTermsConditionButtonTap(_ sender: Any){
        
        checkMarkImageView.image = UIImage(named:"checkbox_tick")
        
        
    }
    
    
    //MARK: UIPopoverPresentationController Delegate
    
    
    dynamic func presentationController(_ presentationController: UIPresentationController, willPresentWithAdaptiveStyle style: UIModalPresentationStyle, transitionCoordinator: UIViewControllerTransitionCoordinator?) {
        // add a semi-transparent view to parent view when presenting the popover
        let parentView = presentationController.presentingViewController.view
        
        let overlay = UIView(frame: (parentView?.bounds)!)
        overlay.backgroundColor = UIColor(white: 0.0, alpha: 0.4)
        parentView?.addSubview(overlay)
        
        let views: [String: UIView] = ["parentView": parentView!, "overlay": overlay]
        
        parentView?.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[overlay]|", options: [], metrics: nil, views: views))
        parentView?.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[overlay]|", options: [], metrics: nil, views: views))
        
        overlay.alpha = 0.0
        
        transitionCoordinator?.animate(alongsideTransition: { _ in
            overlay.alpha = 1.0
        }, completion: nil)
        
        self.overlay = overlay
    }
    
    func prepareForPopoverPresentation(_ popoverPresentationController: UIPopoverPresentationController) {
        
        
            popoverPresentationController.permittedArrowDirections = .up
            popoverPresentationController.sourceView = self.passangerFloatLabelTextField
            popoverPresentationController.sourceRect = CGRect(x:self.passangerFloatLabelTextField.frame.origin.x + 20 , y: self.passangerFloatLabelTextField.frame.size.height , width: 0, height: 0)
        
        
        
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    func popoverPresentationControllerDidDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) {
        
          /* self.passangerFloatLabelTextField.text = self.passangerFloatLabelTextField.text
        
       if let passengerPopUpViewController: PassengerPopUpViewController =  popoverPresentationController.presentedViewController as? PassengerPopUpViewController
        {
            if passengerPopUpViewController.sectionDetails.count > 0
            {
                let selectedPassengerDetails = passengerPopUpViewController.sectionDetails[0].row.filter(){$0.isSelected == true && $0.index > 0  }
                
                if selectedPassengerDetails.count > 0
                {
                    self.selectedPassengerDetails = selectedPassengerDetails
                    
                    self.setSelectedPassengerDetailsToText(selectedPassengerDetails: selectedPassengerDetails)
                    
                }
            }
        } */
        
        guard let overlay = overlay else {
            return
        }
        DispatchQueue.main.async() {
            UIView.animate(withDuration: 0.2, animations: {
                overlay.alpha = 0.0
            }, completion: { _ in
                overlay.removeFromSuperview()
            })
        }
        
    }
    
    
    func presentPassengerPopover() {
        
        let popoverContentController = PassengerPopUpViewController(nibName: "PassengerPopUpViewController", bundle: nil)
        
        
        popoverContentController.passangerDetailsArray = self.sortPassengerByPassengerNumber()
        popoverContentController.deviationDetailsArray = Array((self.tourDetails?.deviationRelation)!) as? [Deviation]
        
        popoverContentController.tourDetails = tourDetails
        popoverContentController.moduleName = Constant.Deviation.DEVIATION_DROP_DOWN
        
        popoverContentController.sectionDetails = self.deviationDropDownPassengerDetails
        
        popoverContentController.presentingView = self
        
        popoverContentController.selectedPassengerNumber = self.selectedPassengerNumber
        
        popoverContentController.passengerProtocol = self
        // Set your popover size.
        popoverContentController.preferredContentSize = CGSize(width: UIScreen.main.bounds.width - 40, height: (UIScreen.main.bounds.height - 300))
        
        // Set the presentation style to modal so that the above methods get called.
        popoverContentController.modalPresentationStyle = .popover
        
        // Set the popover presentation controller delegate so that the above methods get called.
        popoverContentController.popoverPresentationController!.delegate = self
        
        
        
        // Present the popover.
        self.present(popoverContentController, animated: true, completion: nil)
        
    }
    
    
    func onOkButtonClick(vieWcontroller:UIViewController)
    {
        //self.passangerFloatLabelTextField.text = self.passangerFloatLabelTextField.text
        
       
        
        if let passengerPopUpViewController: PassengerPopUpViewController =  vieWcontroller as? PassengerPopUpViewController
        {
              self.selectedPassengerNumber = nil
            
            if self.selectedPassengerDetails.count > 0
            {
                self.selectedPassengerDetails.removeAll()
            }
            
            if passengerPopUpViewController.sectionDetails.count > 0
            {
                self.deviationDropDownPassengerDetails = passengerPopUpViewController.sectionDetails
                
                let selectedPassengerDetails = passengerPopUpViewController.sectionDetails[0].row.filter(){$0.isSelected == true && $0.index > 0  } 
                
                if selectedPassengerDetails.count > 0
                {
                    self.selectedPassengerDetails = selectedPassengerDetails
                    
                    self.setSelectedPassengerDetailsToText(selectedPassengerDetails: selectedPassengerDetails)
                    
                }else
                {
                    self.passangerFloatLabelTextField.text  = ""
                }
            }else
            {
                 self.passangerFloatLabelTextField.text  = ""
            }
        }
        
        guard let overlay = overlay else {
            return
        }
        DispatchQueue.main.async() {
            UIView.animate(withDuration: 0.2, animations: {
                
                self.dismiss(animated: true, completion: nil)
                overlay.alpha = 0.0
                
                
            }, completion: { _ in
                
                overlay.removeFromSuperview()
                
                
            })
        }

    }
    
    func setSelectedPassengerDetailsToText(selectedPassengerDetails:[(isSelected: Bool, index: Int, passenger: Passenger?)]){
        
        var passengerText:String = ""
        
        for passengerDetails in selectedPassengerDetails
        {
                if (passengerDetails.passenger?.firstName != nil) &&  (passengerDetails.passenger?.lastName != nil)
                {
                    passengerText = passengerText + "\(passengerDetails.passenger?.firstName ?? "" ) \(passengerDetails.passenger?.lastName ?? "")"
                    
                    if selectedPassengerDetails.count > 1
                    {
                        passengerText = passengerText + ","
                    }
                    
                }
            
            
        }
        
        self.passangerFloatLabelTextField.text = passengerText
    }
    
    
    func showAlertViewWithTitle(title : String,message:String) -> Void {
        let alertController : UIAlertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: AlertMessage.TITLE_OK, style: UIAlertAction.Style.default) {
            UIAlertAction in
            NSLog("OK Pressed")
        }
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func getDeviationNotificationData(bfnNumber:String , notificationDateTimeInMiliSec:Any) -> Dictionary<String,Any>  {
        
        
     
        
        var notificationData:Dictionary<String,String> = Dictionary<String,String>()
        notificationData[NotificationConstant.BFN] = bfnNumber
        notificationData[NotificationConstant.TITLE] = bfnNumber
        notificationData[NotificationConstant.CREATED_ON] = "\(notificationDateTimeInMiliSec)"
 
        notificationData[NotificationConstant.IMAGE_URL] = ""
        notificationData[NotificationConstant.MESSAGE] = AlertMessage.Deviation.DEVIATION_NOTIFICATION_MSG
        notificationData[NotificationConstant.NOTIFICATION_TYPE] = NotificationType.DeviationLocale.rawValue
        notificationData[NotificationConstant.REDIRECT] = ""
        notificationData[NotificationConstant.SUBTITLE] = AlertMessage.Deviation.DEVIATION_NOTIFICATION_MSG
        
     
        
        return notificationData
        
    }
    
    func sortPassengerByPassengerNumber() -> Array<Passenger> {
        
        if let passengerArray:Array<Passenger> = self.tourDetails?.passengerRelation?.allObjects as? Array<Passenger>
        {
            if passengerArray.count > 0
            {
                
                let sortedPassengerArray = passengerArray.sorted(){ $0.passengerNumber! < $1.passengerNumber!}
                
                return sortedPassengerArray
                
            }
        }
        
        return []
        
    }
    
   
}
    


//MARK:Deviation Constant

struct DeviationConstants {
    //App Constants
    static let PASSANGER_NAMES = ["David", "John"]
    
    static let DEVIATION_TYPES = ["Pre tour deviation", "Post tour deviation"]
}



