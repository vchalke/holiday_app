
#import <Foundation/Foundation.h>


//alertView Messages
NSString *const kMessageNoInternet;
NSString *const kMessageNoInternetTryAfterSometime;
NSString *const kMessageSomeErrorTryAfterSometime;

// Server related
NSString *const serverUrl;
NSString *const kInternationalHoliday;
NSString *const kIndianHoliday;
NSString *const kServerResponseKeyStatus;
NSString *const kServerResponseKeyData;
NSString *const kStatusSuccess;
NSString *const kStatusFailure;


// Serach Pacakage related
NSString *const kBudgetKey;
NSString *const kNightToSpendKey;
NSString *const kDestinationKey;
NSString *const kHolidays;
NSString *const kFilters;
NSString *const kDurationList;


//Filter Packages Keys
NSString *const kFitlerMaxPrice;
NSString *const kFilterMinPrice;

//Notification Keys
NSString *const kDataDidFilteredNotification;

//traveller Info
NSString *const kTravellerTypeChild;
NSString *const kTravellerTypeAdult;
NSString *const kTravellerTypeInfant;

NSString *const kTravellerRoomTypeSR;
NSString *const kTravellerRoomTypeTS;
NSString *const kTravellerRoomTypeDR;
NSString *const kTravellerRoomTypeTR;
NSString *const kTravellerRoomTypeCWB;
NSString *const kTravellerRoomTypeCNB;
NSString *const kTravellerRoomTypeInfant;
NSString *const kCurrency_japanese_yen;
NSString *const kCurrency_thai_baht;

//userID and password
NSString *const kuserDefaultUserId;
NSString *const kuserDefaultPassword;
NSString *const kuserDefaultRequestId;
NSString *const kuserDefaultTokenId;
NSString *const kuserDefaultTC_MobileNumber;
NSString *const kLoginUserDetails;
NSString *const kUserFirstName;
NSString *const kUserImageString;
NSString *const kUserDeviceUniqueID;
NSString *const kCurrentTransactionId;

NSString *const kpackageTypeFITInternational ;
NSString *const kpackageTypeGITInternational ;
NSString *const kpackageTypeFITDomestic ;
NSString *const kpackageTypeGITDomestic ;

NSString *const kProfile;
NSString *const kMyFavourite;
NSString *const kOrderSummary;



//Login Status Constant
NSString *const kLoginStatus;
NSString *const kLoginEmailId;
NSString *const kLoginPasswords;
NSString *const kPhoneNo;
NSString *const kLoginSuccess;
NSString *const kLoginFailed;
NSString *const kVersionUpgradeMessage;

//FireBase logEventWithName
NSString *const kInternational_Holidays;
NSString *const kIndian_Holidays;


//Astra URL
NSString *const kpgURLForPayment;
NSString *const kpgURLForConfirmation;
NSString *const kbaseURLForAstra;
NSString *const kbaseURLForAstraNew;
NSString *const kUrlForFlights ;
NSString *const kUrlForHotels ;
NSString *const kUrlForVisa;
NSString *const kUrlForInsurance;
NSString *const kUrlForForex;

NSString *const kForexBuyURL;

NSString *const kForexSellURL;

NSString *const kForexMoneyTransferURL;

NSString *const kForexKnowYourCardBalanceURL;

NSString *const kForexReloadForexURL;

NSString *const kForexCurrencyConverterURL;

NSString *const kForexLiveRatesyURL;


NSString *const krequestTypeQueryParam ;
NSString *const krequestTypePathParam ;
NSString *const krequestTypeJsonParam ;


NSString *const kAstraUrlNewToken ;
NSString *const kAstraUrlSearchPackage;
NSString *const kAstraUrlPackageFilterSearch;
NSString *const kAstraUrlPackageDetail;
NSString *const kAstraUrlFareCalender;
NSString *const kAstraUrlPreConfirmationBooking;
NSString *const kAstraUrlGstStates;
NSString *const kAstraUrlPricing;
NSString *const kAstraUrlPromocode;
NSString *const kAstraPdpPromocodeURL;
NSString *const kAstraTcHolidayRSConfirmation;

NSString *const kAstraUrlBooking;
NSString *const kAstraUrlLoginCheck;
NSString *const kAstraUrlSendOtp;
NSString *const kAstraUrlSavePass;
NSString *const kAstraUrlForgotPassword;
NSString *const kAstraVerifyCredentials;
NSString *const kAstraSocialLogin;
NSString *const kAstraCreateLogin;
NSString *const kpgURLForPayment;
NSString *const kpgURLForConfirmation;
NSString *const kpgURLForDeals;
NSString *const kUrlForImage;
NSString *const kUrlForCreateLead;
NSString *const KUrlHolidayCreateLead;
NSString *const kVersionUpgradeAppstoreURL;
NSString *const KGetCurrencyList;

NSString *const kSignInWithOtp;
NSString *const kSendOtp;
NSString *const kSignInWithPassword;

NSString *const kGetbannerDataURL;
NSString *const KGetByPackageId;
NSString *const kGetbannerServiceURL;
NSString *const kGetbannerServiceURL;
NSString *const kTravelBlogURL;
NSString *const getByPackageId;
NSString *const kGetCustInfoURL;

NSString *const kLoginRequestID;
NSString *const kLoginTokenID;

NSString *const kEnterEmailAddress;
NSString *const kEnterPassword;
NSString *const kEnterConfirmPassword;
NSString *const kEnterMobileNumber;

//CertificateConstant
NSString *const kSSLCertificates;

//HolidayLandingConstant
NSString *const kserviceTopBanners;
NSString *const kserviceWhatsNew;
NSString *const kserviceLastMinuteDeals;
NSString *const kserviceSummerHolidays;
NSString *const kserviceAdventureHoliday;
NSString *const kserviceCategoriesHoliday;
NSString *const kserviceUpcomingEvents;
NSString *const kserviceCherryBlossomSpecial;
NSString *const kservicePopularDestinations;


//HolidayLandingConstant Level Name
NSString *const levelNumberOne;
NSString *const levelNumberTwo;
NSString *const levelNumberThree;
NSString *const levelNumberFour;
NSString *const levelNumberFive;
NSString *const levelNumberSix;
NSString *const levelNumberSeven;
NSString *const levelNumberEight;
NSString *const levelNumberNine;
NSString *const levelNumberTen;
