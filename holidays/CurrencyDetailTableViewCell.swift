//
//  CurrencyDetailTableViewCell.swift
//  holidays
//
//  Created by Swapnil on 22/12/17.
//  Copyright © 2017 Mobicule. All rights reserved.
//

import UIKit

class CurrencyDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var productLabel: UILabel!
    @IBOutlet weak var buyRateLabel: UILabel!
    @IBOutlet weak var sellRateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
        
//        if selected
//        {
//            print("selected")
//            self.buyRateLabel.textColor = UIColor.red
//        }
//        else
//        {
//            self.buyRateLabel.textColor = UIColor.black
//        }
        
    }
    
//    override func setHighlighted(_ highlighted: Bool, animated: Bool)
//    {
//        super.setHighlighted(highlighted, animated: animated)
//        if highlighted
//        {
//            self.buyRateLabel.textColor = UIColor.red
//        }
//        else
//        {
//            self.buyRateLabel.textColor = UIColor.black
//        }
//    }
}
