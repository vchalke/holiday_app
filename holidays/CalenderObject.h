//
//  CalenderObject.h
//  holidays
//
//  Created by Kush_Tech on 26/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CalenderObject : NSObject
-(instancetype)initWithCalenderObjectDict:(NSDictionary *)dictionary;
@property(nonatomic,strong)NSDictionary *dbResponseBean;
@property(nonatomic,strong)NSArray *bookable;
@property(nonatomic,strong)NSArray *onRequest;
@property(nonatomic,strong)NSString *calendarEndDate;
@property(nonatomic,strong)NSString *calendarStartDate;
@property(nonatomic)BOOL isDateAvialable;
@property(nonatomic,strong)NSString *mode;
@property(nonatomic,strong)NSString *pkgId;
@property(nonatomic,strong)NSString *pkgIdentificationNo;
@property(nonatomic)int pkgSubType;

-(instancetype)initWithBookableAndRequestObjectDict:(NSDictionary *)dictionary withPackgeSubType:(int)subType;
@property(nonatomic,strong)NSString *date;
@property(nonatomic)NSInteger strikeOutPrice;
@property(nonatomic)NSInteger price;


@property(nonatomic,strong)NSString *DATE;
@property(nonatomic)NSInteger AVL_INV;
@property(nonatomic)NSInteger DR_PRICE;
@property(nonatomic)NSInteger DR_STRIKEOUT;
@property(nonatomic,strong)NSString *INVENTORY;
@property(nonatomic,strong)NSString *LT_PROD_CODE;
@property(nonatomic,strong)NSString *PROD_ITIN_CODE;
@property(nonatomic)NSInteger LAST_SELL_DAY;
@property(nonatomic)NSInteger NO_OF_SLAB_SEAT;

@end

NS_ASSUME_NONNULL_END
