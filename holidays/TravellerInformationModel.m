//
//  TravellerInformationModel.m
//  holidays
//
//  Created by ketan on 23/10/15.
//  Copyright © 2015 Mobicule. All rights reserved.
//

#import "TravellerInformationModel.h"
@implementation TravellerInformationModel
@synthesize travellerRoomingType,travellerRoomNumber,travellerType,indivisualPrice,cell,travellerAge;

- (instancetype)initWithRoomingType:(NSString *)roomingType withRoomNo:(int)roomNo withType:(NSString *) type
{
    self = [super init];
    if (self) {
        self.travellerRoomingType = roomingType;
        self.travellerRoomNumber = roomNo;
        self.travellerType = type;
        cell = nil;
        self.travellerAge = 0;
    }
    return self;
}
@end
