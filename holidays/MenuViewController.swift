//
//  MenuViewController.swift
//  sotc-consumer-application
//
//  Created by Saurav Kumar on 18/07/17.
//  Copyright © 2017 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit

 
class MenuViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet var menuTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.menuTableView.register(UINib(nibName:  "MenuViewTableViewCell", bundle: nil), forCellReuseIdentifier: "MenuTableCell")
        
        self.menuTableView.tableFooterView = UIView()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.setRedNavigationBar()
        self.navigationController?.setnavigatiobBarTitle(title: "MORE", viewController: self)
        
        
        NotificationController.updateNotificationBadgeCount()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    // MARK: - TableView Methods
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 0  {
            
            return 100
        }
        else
        {
            return 80
            
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableCell", for: indexPath) as? MenuViewTableViewCell
            else {
                
                fatalError("The dequeued cell is not an instance of MenuViewTableViewCell.")
        }
        
        
        
        
        cell.deviationButton.isHidden  = true
        
        switch indexPath.row {
            
        case 4:
            
          //  cell.menuItemImageView.image = #imageLiteral(resourceName: "grantAccess")
            cell.menuItemTitleLabel.text = "Vikas Kumar Patil"
        case 2:
            
            cell.menuItemImageView.image = #imageLiteral(resourceName: "grantAccess")
            cell.menuItemTitleLabel.text = "Grant Access"
            
        case 5:
            
            cell.menuItemImageView.image = #imageLiteral(resourceName: "grantAccess")
            cell.menuItemTitleLabel.text = "Feedback"
        case 0:
            
            cell.menuItemImageView.image = #imageLiteral(resourceName: "helpMenu")
            cell.menuItemTitleLabel.text = "Help"
        case 1:
            
            cell.menuItemImageView.image = #imageLiteral(resourceName: "logoutMenu")
            cell.menuItemTitleLabel.text = "Logout"
            cell.menuDetailIcon.isHidden = true
        default:
            cell.menuItemTitleLabel.text = ""
            cell.menuItemImageView.image = #imageLiteral(resourceName: "grantAccess")
        }
        
        
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch indexPath.row {
            
        case 2: break
            
            
        case 0:
            
        self.navigationController?.pushViewController(HelpViewController(nibName: "HelpViewController", bundle: nil), animated: true)

            
            
        case 1:
            
            
                let alert = UIAlertController(title: "Logout", message: "Do you want to logout?", preferredStyle: UIAlertController.Style.alert)
                
                alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: { (UIAlertAction) in
                    
                    
//                        let loginViewController : SOTCLoginViewController = SOTCLoginViewController(nibName: "SOTCLoginViewController", bundle: nil)
//                    
//             
//                    UserDefaults.standard.set(false, forKey: UserDefauldConstant.isUserLoggedIn)
//                        
//                        (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController = loginViewController
//                 
//        
                
                
                }))
                alert.addAction(UIAlertAction(title: "No", style: UIAlertAction.Style.destructive, handler: nil))
                
                UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
          
            
           
            
          
        case 3:
            
            break
            
        
        case 4: break
 
        default: break
      
        }
        
        
    }
    
    
    
}
