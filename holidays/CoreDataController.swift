 //
//  CoreDataController.swift
//  sotc-consumer-application
//
//  Created by Saurav Kumar on 31/08/17.
//  Copyright © 2017 Mobicule Technologies Private Limited. All rights reserved.
//

import Foundation
import CoreData

class CoreDataController {
    
 
  class func saveContext ()
  {
        
        if #available(iOS 10.0, *) {
            
            CoreDataController.saveContextPersistentContainer()
            
        } else {
    
            CoreDataController.saveContextForBeforeIOS10()
        }
    }
    
    
    class func getContext() -> NSManagedObjectContext {
        
        
        if #available(iOS 10.0, *) {
            
           /* let privateMOC = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
            privateMOC.parent = CoreDataController.persistentContainer.viewContext
            return privateMOC*/
            
            return CoreDataController.persistentContainer.viewContext
            
        } else {
            
          /*  let privateMOC = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
            privateMOC.parent = CoreDataController.managedObjectContext
            return privateMOC*/
            
            return CoreDataController.managedObjectContext
        }
    }
    
   class func setMergePolicy(mergePolicy:NSMergePolicy)  {
    
    
    if #available(iOS 10.0, *) {
        
        CoreDataController.getContext().mergePolicy = mergePolicy
        
    } else {
        
      CoreDataController.managedObjectContext.mergePolicy = mergePolicy
        
    }
    
    
        
        
    }
    
    // MARK: - Core Data stack for iOS 8
    
    static var applicationDocumentsDirectory: NSURL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "com.mobicule.tfthfghghtthgf" in the application's documents Application Support directory.
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        
        printLog(urls[urls.count-1] as NSURL)
        return urls[urls.count-1] as NSURL
    }()
    
    static var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = Bundle.main.url(forResource: "sotc_consumer_application", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    
    static var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: CoreDataController.managedObjectModel)
        let url = CoreDataController.applicationDocumentsDirectory.appendingPathComponent("sotc_consumer_application.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            
            let options = [ NSInferMappingModelAutomaticallyOption : true,
                            NSMigratePersistentStoresAutomaticallyOption : true]
            
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: options)
            
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject
            
            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
           // abort()
        }
        
        return coordinator
    }()
    
      static var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = CoreDataController.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()
    
    class func saveContextForBeforeIOS10()  {
        
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
               // abort()
            }
        }
        
    }
    
    
    
    // MARK: - Core Data stack for ios 10
    
    @available(iOS 10.0, *)
    static var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "sotc_consumer_application")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
               // fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
   
    class func saveContextPersistentContainer () {
        
        if #available(iOS 10.0, *) {
            let context = CoreDataController.persistentContainer.viewContext
            if context.hasChanges {
                do {
                    try context.save()
                } catch {
                    // Replace this implementation with code to handle the error appropriately.
                    // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    // let nserror = error as NSError
                    printLog(error.localizedDescription)
                    //fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
                }            }
    
        } else {
            // Fallback on earlier versions
        }
           }
    
    
    
    
    
}
