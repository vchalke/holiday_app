//
//  RequestedDeviationViewCell.swift
//  sotc-consumer-application
//
//  Created by Payal on 12/09/17.
//  Copyright © 2017 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit

class RequestedDeviationViewCell: UITableViewCell {


    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }

    @IBOutlet var newDateLabel: UILabel!
    @IBOutlet var remarkLabel: UILabel!
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
