//
//  HeaderTwoLineTableViewCell.h
//  holidays
//
//  Created by Kush_Tech on 16/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HeaderTwoLineTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbl_titleOne;
@property (weak, nonatomic) IBOutlet UILabel *lbl_titleTwo;
-(void)showEditing:(NSInteger)index;
@end

NS_ASSUME_NONNULL_END
