//
//  PopUpViewController.swift
//  holidays
//
//  Created by Komal Katkade on 12/30/17.
//  Copyright © 2017 Mobicule. All rights reserved.
//

import UIKit

class PopUpViewController: UIViewController
{
    
    @IBOutlet weak var heightConstraintOfPopUpView: NSLayoutConstraint!
    @IBOutlet weak var textviewMessage: UITextView!
    var msg : String = ""
  
    override func viewDidLoad()
    {
        super.viewDidLoad()
        textviewMessage.text = msg
        if ((textviewMessage.contentSize.height + 60)  < 550)
        {
        heightConstraintOfPopUpView.constant = textviewMessage.contentSize.height + 60
        }
        else
        {
           heightConstraintOfPopUpView.constant = 550
        }
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(PopUpViewController.dismissView))
        self.view.addGestureRecognizer(tap)
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onClickOfCloseButton(_ sender: Any)
    {
        self.dismiss(animated: false, completion: nil)
    }
    
    @objc func dismissView()
    {
       self.dismiss(animated: false, completion: nil)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
