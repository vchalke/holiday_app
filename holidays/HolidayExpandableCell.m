//
//  ExpandableCell.m
//  holidays
//
//  Created by Pushpendra Singh on 21/08/15.
//  Copyright (c) 2015 Mobicule. All rights reserved.
//

#import "HolidayExpandableCell.h"

@implementation HolidayExpandableCell

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView
{
//    CGSize contentSize = webView.scrollView.contentSize;
//    CGSize viewSize = webView.bounds.size;
//    
//    float rw = viewSize.width / contentSize.width;
//    
//    webView.scrollView.minimumZoomScale = rw;
//    webView.scrollView.maximumZoomScale = rw;
//    webView.scrollView.zoomScale = rw;
}

@end
