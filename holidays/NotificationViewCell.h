//
//  NotificationViewCell.h
//  holidays
//
//  Created by Kush_Team on 23/06/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NotificationViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *img_views;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_heights;
@property (weak, nonatomic) IBOutlet UILabel *lbl_dates;
@property (weak, nonatomic) IBOutlet UILabel *lbl_titles;
@property (weak, nonatomic) IBOutlet UILabel *lbl_description;
@end

NS_ASSUME_NONNULL_END
