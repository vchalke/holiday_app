//
//  ChildHotelTableViewCell.h
//  holidays
//
//  Created by Kush_Tech on 03/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HolidayAccomodationObject.h"
#import "SightSeenObject.h"
#import "HCSStarRatingView.h"
NS_ASSUME_NONNULL_BEGIN

@interface ChildHotelTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbl_Title;
@property (weak, nonatomic) IBOutlet UILabel *lbl_numOfDays;
@property (weak, nonatomic) IBOutlet UILabel *lbl_subTitle;
@property (weak, nonatomic) IBOutlet UILabel *lbl_address;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cnst_htofAddress;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *star_ratingView;
@property (weak, nonatomic) IBOutlet UIView *view_addres;

-(void)loadDataInChildFromDetailModel:(HolidayAccomodationObject*)detailModel withHideAddess:(BOOL)flag;
-(void)loadDSightSeeingFromDetailModel:(SightSeenObject*)sightModel;
@end

NS_ASSUME_NONNULL_END
