//
//  BuyForexPaymentBO.swift
//  holidays
//
//  Created by Komal Katkade on 12/19/17.
//  Copyright © 2017 Mobicule. All rights reserved.
//

import Foundation
class BuyForexPayementBO
{
    var totalTaxAmount : NSInteger = 0
    var SGST : NSInteger = 0
    var CGST : NSInteger = 0
    var IGST : NSInteger = 0
    var isAtBranchChecked : Bool = false
    var isHomeDeliveryChecked : Bool = false
    var isAdvancePaymentChecked : Bool = false
    var isFullPaymentChecked : Bool = false
    var totalPayableAmount : NSInteger = 0
    var tcilForexQuote : NSMutableDictionary
    var paymentType : NSString

    init(totalTaxAmount:NSInteger,SGST:NSInteger,CGST:NSInteger,isAtBranchChecked:Bool,isHomeDeliveryChecked: Bool,isAdvancePaymentChecked: Bool,isFullPaymentChecked: Bool,IGST:NSInteger,totalPayableAmount:NSInteger,tcilForexQuote : NSDictionary,paymentType : NSString)
    {
        self.totalTaxAmount = totalTaxAmount
        self.SGST = SGST
        self.CGST = CGST
        self.IGST = IGST
        self.isAtBranchChecked = isAtBranchChecked
        self.isHomeDeliveryChecked = isHomeDeliveryChecked
        self.isAdvancePaymentChecked = isAdvancePaymentChecked
        self.isFullPaymentChecked = isFullPaymentChecked
        self.totalPayableAmount = totalPayableAmount
        self.tcilForexQuote = tcilForexQuote.mutableCopy() as! NSMutableDictionary
        self.paymentType = paymentType
    }
}
