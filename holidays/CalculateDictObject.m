//
//  CalculateDictObject.m
//  holidays
//
//  Created by Kush_Tech on 09/04/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "CalculateDictObject.h"

@implementation CalculateDictObject
-(instancetype)initWithCalculateObjectDict:(NSDictionary *)dictionary{
    if ([super init])
    {
        self.adultDiscount = [[dictionary valueForKey:@"adultDiscount"] integerValue];
        self.adultPrice = [[dictionary valueForKey:@"adultPrice"] integerValue];
        self.advancePayableAmount = [[dictionary valueForKey:@"advancePayableAmount"] integerValue];
        self.currencyBreakup = [dictionary valueForKey:@"currencyBreakup"];
        
        self.amountOne = [[[self.currencyBreakup objectAtIndex:0] valueForKey:@"amount"] integerValue];
        self.currencyRateOne = [[[self.currencyBreakup objectAtIndex:0] valueForKey:@"currencyRate"] floatValue];
        self.currencycodeOne = [[self.currencyBreakup objectAtIndex:0] valueForKey:@"currencycode"];
        
        self.amountTwo = [[[self.currencyBreakup objectAtIndex:1] valueForKey:@"amount"] integerValue];
        self.currencyRateTwo = [[[self.currencyBreakup objectAtIndex:1] valueForKey:@"currencyRate"] floatValue];
        self.currencycodeTwo = [[self.currencyBreakup objectAtIndex:1] valueForKey:@"currencycode"];
        
        self.discountResponseBean = [dictionary valueForKey:@"discountResponseBean"];
        self.discAmont = [[self.discountResponseBean valueForKey:@"discAmont"] integerValue];
        self.discCode = [self.discountResponseBean valueForKey:@"discCode"];
        
        
        self.advanceDiscountGSTDescription = [dictionary valueForKey:@"advanceDiscountGSTDescription"];
        self.fullDiscountGSTDescription = [dictionary valueForKey:@"fullDiscountGSTDescription"];
        self.gSTDescription = [dictionary valueForKey:@"gSTDescription"];
        
        
        self.totalPrice = [[dictionary valueForKey:@"totalPrice"] integerValue];
        self.totalServicetax = [[dictionary valueForKey:@"totalServicetax"] integerValue];
        self.totalServicetaxAdvanceDiscount = [[dictionary valueForKey:@"totalServicetaxAdvanceDiscount"] integerValue];
        self.totalServicetaxFullDiscount = [[dictionary valueForKey:@"totalServicetaxFullDiscount"] integerValue];
        self.totaltourCost = [[dictionary valueForKey:@"tourCost"] integerValue];
        
        
    }
    return self;
}

@end
