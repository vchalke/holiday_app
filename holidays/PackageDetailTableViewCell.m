//
//  PackageDetailTableViewCell.m
//  holidays
//
//  Created by Kush_Tech on 03/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "PackageDetailTableViewCell.h"

@implementation PackageDetailTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
//    UITapGestureRecognizer *singleFingerTap =
//    [[UITapGestureRecognizer alloc] initWithTarget:self
//                                            action:@selector(handleSingleTap:)];
//    [self.flightView addGestureRecognizer:singleFingerTap];
//    [self.hotelView addGestureRecognizer:singleFingerTap];
//    [self.sightView addGestureRecognizer:singleFingerTap];
//    [self.visaView addGestureRecognizer:singleFingerTap];
//    [self.transferView addGestureRecognizer:singleFingerTap];
//    [self.mealView addGestureRecognizer:singleFingerTap];
    
    
    [self.btn_Flights addTarget:self action:@selector(functionName:) forControlEvents:UIControlEventTouchUpInside];
    [self.btn_Hotels addTarget:self action:@selector(functionName:) forControlEvents:UIControlEventTouchUpInside];
    [self.btn_Sightseeing addTarget:self action:@selector(functionName:) forControlEvents:UIControlEventTouchUpInside];
    [self.btn_Visa addTarget:self action:@selector(functionName:) forControlEvents:UIControlEventTouchUpInside];
    [self.btn_Transfer addTarget:self action:@selector(functionName:) forControlEvents:UIControlEventTouchUpInside];
    [self.btn_Meals addTarget:self action:@selector(functionName:) forControlEvents:UIControlEventTouchUpInside];

    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) functionName:(UIButton *) sender {

//    NSArray *titleArray = [NSArray arrayWithObjects:@"Flights",@"Hotels",@"Itinenary",@"Inclusion",@"Exclusion",@"Sightseeing",@"Meals", nil];
    NSArray *titleArray = [NSArray arrayWithObjects:@"Flights",@"Hotels",@"Sightseeing",@"Visa,Passport & Insurance",@"Transfer",@"Meals", nil];
    NSLog(@"%ld",(long)sender.tag);
    [self.packageDetailDelegate clickViewIndex:sender.tag inclusionStr:[titleArray objectAtIndex:sender.tag-1]];

//    [self.packageDetailDelegate clickViewIndex:sender.tag inclusionStr:[self->titlesAray objectAtIndex:sender.tag-1]];
    
}

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
    
}

-(void)showAllPackageDetail:(HolidayPackageDetail*)detailModel{
    self->titlesAray = [[NSMutableArray alloc]init];
    self.lbl_Title.text = detailModel.strPackageName;
//    self.lbl_duration.text = [NSString stringWithFormat:@"%i Nights & %i Days",detailModel.durationNoDays,detailModel.durationNoDays+1];
    self.lbl_duration.text = [NSString stringWithFormat:@"%i Nights & %i Days",detailModel.durationNoDays-1,detailModel.durationNoDays];
    self.siteArray = detailModel.timeLineList;
    NSSortDescriptor *brandDescriptor = [[NSSortDescriptor alloc] initWithKey:@"position" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:brandDescriptor];
    NSArray *sortedArray = [detailModel.timeLineList sortedArrayUsingDescriptors:sortDescriptors];
    NSMutableString *mutString = [[NSMutableString alloc]init];
//    [mutString appendString:@"✈ "];
    for (int i=0; i<[sortedArray count]; i++) {
        NSDictionary *siteNameDict = sortedArray[i];
        NSLog(@"%@",siteNameDict);
        NSDictionary *cityDict = [siteNameDict valueForKey:@"cityCode"];
        NSLog(@"%@",cityDict);
        NSDictionary *iconDict = [siteNameDict valueForKey:@"iconId"];
        NSLog(@"%@",iconDict);
        NSString *iconName = [iconDict valueForKey:@"iconName"];
        NSLog(@"%@",iconName);
        NSString *cityName = [cityDict valueForKey:@"cityName"];
        NSLog(@"%@",cityName);
        NSString *timeStr =[NSString stringWithFormat:@"%@",[siteNameDict valueForKey:@"noOfNights"]];
        NSLog(@"%@",timeStr);
        NSString *dashString = @" ➝ ";
//        NSString *dashString = @" ➜ ";
        [mutString appendString:[NSString stringWithFormat:@"%@ (%@N)%@",cityName,timeStr,(i==sortedArray.count-1) ? @"" : dashString]];
    }
//     self.lbl_Destination.text = mutString;
    [self setImageIcon:[UIImage imageNamed:@"flightInSRP"] WithText:mutString];
    CGFloat widthConstant = self.contentsViews.frame.size.width/6;//self.topInfoeViews.frame.size.width/6.2;
    CGFloat constant = 0;
    
    self.flightView.hidden = !detailModel.airFlag;
    self.hotelView.hidden = !detailModel.accomFlag;
    self.sightView.hidden = !detailModel.sightSeeingFlag;
    self.visaView.hidden = !detailModel.tourMngerFlag;
    self.transferView.hidden = !detailModel.transferFlag;
    self.mealView.hidden = !detailModel.mealsFlag;
    
    constant += (!detailModel.airFlag)? 0 : widthConstant ;
    constant += (!detailModel.accomFlag)? 0 : widthConstant ;
    constant += (!detailModel.sightSeeingFlag)? 0 : widthConstant ;
    constant += (!detailModel.tourMngerFlag)? 0 : widthConstant ;
    constant += (!detailModel.transferFlag)? 0 : widthConstant ;
    constant += (!detailModel.mealsFlag)? 0 : widthConstant ;
    
    self.const_StackViewWidth.constant = constant;
    [self.btn_upDownToggle addTarget:self action:@selector(toggleButton:) forControlEvents:UIControlEventTouchUpInside];
    CGSize sizeOfText = [self.lbl_Destination.text sizeWithFont:self.lbl_Destination.font
                            constrainedToSize:self.lbl_Destination.frame.size
                                lineBreakMode:NSLineBreakByCharWrapping];
    labelNumOFLines = sizeOfText.height / self.lbl_Destination.font.pointSize;
    [self setHeightOfLabel:NO];
    
//    [NSArray arrayWithObjects:@"Flights",@"Hotels",@"Itinenary",@"Inclusion",@"Exclusion",@"Sightseeing",@"Meals", nil];
//    (detailModel.airFlag) ? [self->titlesAray addObject:[NSString stringWithFormat:@"Flights"]] : [self notUsed];
//    (detailModel.accomFlag) ? [self->titlesAray addObject:[NSString stringWithFormat:@"Hotels"]] : [self notUsed];
//    (detailModel.sightSeeingFlag) ? [self->titlesAray addObject:[NSString stringWithFormat:@"Itinenary"]] : [self notUsed];
//    (detailModel.transferFlag) ? [self->titlesAray addObject:[NSString stringWithFormat:@"Visa"]] : [self notUsed];
//    (detailModel.mealsFlag) ? [self->titlesAray addObject:[NSString stringWithFormat:@"Meals"]] : [self notUsed];
    
}
-(void)setImageIcon:(UIImage*)image WithText:(NSString*)strText{

    NSTextAttachment *imageAttachment = [[NSTextAttachment alloc] init];
    imageAttachment.image = image;
//    imageAttachment.bounds = CGRectMake(-5.0, 0, imageAttachment.image.size.width, imageAttachment.image.size.height);
    imageAttachment.bounds = CGRectMake(0, 0, 13, 13);
    NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:imageAttachment];
    NSMutableAttributedString *completeText = [[NSMutableAttributedString alloc] initWithString:@" "];
    [completeText appendAttributedString:attachmentString];
    NSAttributedString *textAfterIcon = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@" ➝ %@ ➝ ",strText]];
    [completeText appendAttributedString:textAfterIcon];
    [completeText appendAttributedString:attachmentString];
    self.lbl_Destination.textAlignment = NSTextAlignmentLeft;
    self.lbl_Destination.attributedText = completeText;
}
- (void) toggleButton:(UIButton *)sender {

    self.btn_upDownToggle.tag = (sender.tag == 0) ? 1 : 0 ;
    [self setHeightOfLabel:(self.btn_upDownToggle.tag == 1)];
}
- (void)setHeightOfLabel:(BOOL)isFlag{
    self.cnst_LabelHeight.constant = isFlag ? 20 * (labelNumOFLines + 1) : 20;
    [self.btn_upDownToggle setImage:isFlag ? [UIImage imageNamed:@"dropUpInCircle"] : [UIImage imageNamed:@"dropDownInCircle"] forState:UIControlStateNormal];
}
- (void)notUsed{
    
}
@end
