//
//  BankOffersTableViewCell.h
//  holidays
//
//  Created by Kush_Team on 29/07/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BankOffersTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UICollectionView *bankOfferColecttion;
@property (weak, nonatomic)NSArray *bankOfferArray;
-(void)setBankOffer;
@end

NS_ASSUME_NONNULL_END
