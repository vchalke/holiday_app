//
//  PopularDestinationCell.m
//  holidays
//
//  Created by Kush_Tech on 07/05/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "PopularDestinationCell.h"
#import "upcomingEventCell.h"

@implementation PopularDestinationCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setupCollectionViewTitle:(NSString*)title subTitle:(NSString*)subTitle
{
    [self.collection_Views registerNib:[UINib nibWithNibName:@"upcomingEventCell" bundle:nil] forCellWithReuseIdentifier:@"upcomingEventCell"];
    self.collection_Views.delegate = self;
    self.collection_Views.dataSource = self;
    self.lbl_titles.text = title;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [self.popularDestArray count]; //5
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    upcomingEventCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"upcomingEventCell" forIndexPath:indexPath];
//    NSArray *imgArray = [NSArray arrayWithObjects:@"natureThree",@"natureOne",@"natureTwo",@"natureThree",@"natureOne",@"natureFour", nil];
//    cell.img_views.image = [UIImage imageNamed:[imgArray objectAtIndex:indexPath.row]];
        [cell showPopularDestination:[self.popularDestArray objectAtIndex:indexPath.row]];
    [cell hideOtherLabels:NO];
    return cell;
}
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(2.0, 2.0, 2.0, 2.0);
}
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(self.collection_Views.frame.size.width*0.42, self.collection_Views.frame.size.height*0.9);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    WhatsNewObject *obj = [self.popularDestArray objectAtIndex:indexPath.row];
    [self.delegate jumpToTheWebViewFromPopularDestination:obj.redirectUrl withTitle:obj.bannerName withObject:obj];
}
@end
