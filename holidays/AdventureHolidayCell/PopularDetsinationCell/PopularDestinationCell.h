//
//  PopularDestinationCell.h
//  holidays
//
//  Created by Kush_Tech on 07/05/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WhatsNewObject.h"
NS_ASSUME_NONNULL_BEGIN
@protocol PopularDestinationCellDelegaete <NSObject>
@optional
-(void)jumpToTheWebViewFromPopularDestination:(NSString *)webUrlString withTitle:(NSString *)webViewTitle withObject:(WhatsNewObject *)newObject;
@end
@interface PopularDestinationCell : UITableViewCell<UICollectionViewDelegate,UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UICollectionView *collection_Views;
@property (weak, nonatomic)NSArray *popularDestArray;
@property (nonatomic, weak) id <PopularDestinationCellDelegaete> delegate;
@property (weak, nonatomic) IBOutlet UILabel *lbl_titles;
-(void)setupCollectionViewTitle:(NSString*)title subTitle:(NSString*)subTitle;
@end

NS_ASSUME_NONNULL_END
