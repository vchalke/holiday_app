//
//  IntroAdventureTableViewCell.h
//  holidays
//
//  Created by Kush_Tech on 17/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WhatsNewObject.h"
NS_ASSUME_NONNULL_BEGIN
@protocol IntroAdventureTableViewCellDelegaete <NSObject>
@optional
-(void)jumpToTheWebViewFromAdventure:(NSString *)webUrlString withTitle:(NSString *)webViewTitle withObject:(WhatsNewObject*)newObject;
@end
@interface IntroAdventureTableViewCell : UITableViewCell<UICollectionViewDelegate,UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UILabel *lbl_Header;
@property (weak, nonatomic) IBOutlet UICollectionView *collection_adventureView;
@property (weak, nonatomic) IBOutlet UIPageControl *page_control;
@property (weak, nonatomic) NSArray *dataArray;
-(void)setupCollectionViewTitle:(NSString*)title subTitle:(NSString*)subTitle;
@property (nonatomic, weak) id <IntroAdventureTableViewCellDelegaete> delegate;
@end

NS_ASSUME_NONNULL_END
