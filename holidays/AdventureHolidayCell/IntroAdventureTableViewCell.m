//
//  IntroAdventureTableViewCell.m
//  holidays
//
//  Created by Kush_Tech on 17/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "IntroAdventureTableViewCell.h"
#import "BannerCollectionViewCell.h"

@implementation IntroAdventureTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setupCollectionViewTitle:(NSString*)title subTitle:(NSString*)subTitle
{
    [self.collection_adventureView registerNib:[UINib nibWithNibName:@"BannerCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"BannerCollectionViewCell"];
    self.collection_adventureView.delegate = self;
    self.collection_adventureView.dataSource = self;
    self.page_control.numberOfPages = [self.dataArray count];
    self.lbl_Header.text = title;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [self.dataArray count];
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    BannerCollectionViewCell *cell= (BannerCollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"BannerCollectionViewCell" forIndexPath:indexPath];
//    cell.lbl_Title.text = @"Adventure HOLIDAYS";
//    cell.lbl_SubTitle.text = @"Manali | Kullu";
    if ([self.dataArray count]>0){
        WhatsNewObject *bannerObj = [[WhatsNewObject alloc] initWithImgArray:[self.dataArray objectAtIndex:indexPath.row]];
        [cell showWhatsNewDetails:bannerObj withData:YES];
    }
    return cell;
}
//-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
//    return UIEdgeInsetsMake(5.0, 5.0, 5.0, 5.0);
//}
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(self.collection_adventureView.frame.size.width, self.collection_adventureView.frame.size.height);
    
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    WhatsNewObject *obj = [self.dataArray objectAtIndex:indexPath.row];
    [self.delegate jumpToTheWebViewFromAdventure:obj.redirectUrl withTitle:obj.bannerName withObject:obj];
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
for (UICollectionViewCell *cell in [self.collection_adventureView visibleCells]) {
    NSIndexPath *indexPath = [self.collection_adventureView indexPathForCell:cell];
    self.page_control.currentPage = indexPath.item;
}
}
@end
