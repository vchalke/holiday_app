//
//  ChangeFlightsVC.swift
//  holidays
//
//  Created by Ios_Team on 09/09/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

import UIKit

class ChangeFlightsVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tbl_views: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tbl_views.register(UINib(nibName: "FlightHeaderTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "FlightHeaderTableViewCell")
        self.tbl_views.register(UINib(nibName: "FilterOptionsTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "FilterOptionsTableViewCell")
        self.tbl_views.register(UINib(nibName: "FlightChangeTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "FlightChangeTableViewCell")
    }
    @IBAction func btn_backPress(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 5
    }
    
    func numberOfSections(in tableView: UITableView) -> Int{
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        if (indexPath.row==0){
            let cell :FilterOptionsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "FilterOptionsTableViewCell", for: indexPath) as! FilterOptionsTableViewCell
            return cell
        }
        let cell :FlightChangeTableViewCell = tableView.dequeueReusableCell(withIdentifier: "FlightChangeTableViewCell", for: indexPath) as! FlightChangeTableViewCell
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableCell(withIdentifier: "FlightHeaderTableViewCell" ) as! FlightHeaderTableViewCell
        return headerView
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (indexPath.row==0){
            return 70
        }
        return 310
    }
}
