//
//  OtherDetailTableViewCell.h
//  holidays
//
//  Created by Kush_Tech on 03/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HolidayPackageDetail.h"
#import "NewMasterCellTableViewCell.h"
#import <WebKit/WebKit.h>
#import "PackageDetailModel.h"
NS_ASSUME_NONNULL_BEGIN

@protocol OtherDetailTableViewCellDelegaete <NSObject>
@optional
- (void)sendInfoOfSection:(NSString*)sectionTitle withDiscription:(NSString*)detailString inclusion:(NSString*)inclusionStr exclusion:(NSString*)exclusionString withPackageModel:(PackageDetailModel*)packageModel;
- (void)sendInfoArrayOfSection:(NSString*)sectionTitle withDiscriptionArray:(NSMutableArray*)detailArray inclusionArray:(NSMutableArray*)inclusionStrArray exclusionArray:(NSMutableArray*)exclusionStringArray withPackageModel:(PackageDetailModel*)packageModel;
- (void)sendInfoArrayInDict:(NSDictionary*)dictionary withsectionTitle:(NSString*)sectionTitle withPackageModel:(PackageDetailModel*)packageModel;
@end

@interface OtherDetailTableViewCell : NewMasterCellTableViewCell{
    HolidayPackageDetail *packageObj;
    PackageDetailModel *packageDetailModel;
    NSMutableString *inclusionString;
    NSMutableString *exclusionString;
    NSMutableString *sightSeeingString;
    NSMutableString *visaString;
     NSMutableString *mealsString;
    NSMutableString *allOtherString;
    NSMutableArray *allOtherStringArray;
    NSMutableArray *inclusionStringArray;
    NSMutableArray *exclusionStringArray;
}

-(void)loadDataFromPackageDetail:(HolidayPackageDetail*)packageData forSection:(NSString*)sectionString;

-(void)loadDataFromPackageModel:(PackageDetailModel*)packageModel forHolidayPackageDetail:(HolidayPackageDetail*)holiPackageDetailsModel forSection:(NSString*)sectionString;

@property (nonatomic, weak) id <OtherDetailTableViewCellDelegaete> otherInfoDelegate;
@property (weak, nonatomic) IBOutlet UILabel *lbl_description;
@property (nonatomic) NSInteger *sectionNum;
@property (nonatomic, strong) NSArray *itinenaryDetailArray;
@property (nonatomic, strong) NSString *sectionTitle;
@property (weak, nonatomic) IBOutlet UIWebView *webViewss;

@end

NS_ASSUME_NONNULL_END
