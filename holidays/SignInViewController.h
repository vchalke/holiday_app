//
//  SignInViewController.h
//
//  Copyright 2012 Google Inc.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

#import <UIKit/UIKit.h>
#import "NewMasterVC.h"
@class GIDSignInButton;

// A view controller for the Google+ sign-in button which initiates a standard
// OAuth 2.0 flow and provides an access token and a refresh token. A "Sign out"
// button is provided to allow users to sign out of this application.

@protocol GoogleSignInViewDelegate <NSObject>

-(void)didFinishWithGoogleLogin:(NSString * )userEmailID;
-(void)didFinishWithGoogleLoginWithuserData:(NSDictionary * )userEmailID;

@end
//VJ_Commented
//@interface SignInViewController : UIViewController<UIAlertViewDelegate>
//VJ_Added
@interface SignInViewController : NewMasterVC<UIAlertViewDelegate>
@property (strong, nonatomic) IBOutlet GIDSignInButton *googleSignInButton;

@property(strong)id<GoogleSignInViewDelegate> googleViewDelegate;

@end
