//
//  BuyForexLocationViewController.swift
//  holidays
//
//  Created by ketan on 27/11/17.
//  Copyright © 2017 Mobicule. All rights reserved.
//

import UIKit

class BuyForexLocationViewController: ForexBaseViewController,UITextFieldDelegate,UITextViewDelegate,GetAddressViaSmsPopUpViewControllerDelegate
{
    @IBOutlet weak var saveAsPreferredBranchRadioButton: UIButton!
    @IBOutlet weak var getAddrViaSMSRadioButton: UIButton!
    @IBOutlet weak var textfieldCityNameForDelivery: UITextField!
    @IBOutlet weak var textviewAdress: UITextView!
    @IBOutlet weak var textfieldPincode: UITextField!
    @IBOutlet weak var labelTotalPayableAmountfromDeliveryView: UILabel!
    @IBOutlet var deliveryView: UIView!
    @IBOutlet weak var buyForexLocationScrollView: TPKeyboardAvoidingScrollView!//UIScrollView!
    @IBOutlet var buyForexLocationViewContainer: UIView!
    @IBOutlet weak var textFieldBranchAddress: UITextView!
    @IBOutlet var buyForexLocationView: UIView!
    @IBOutlet weak var textFieldBranch: UITextField!
    @IBOutlet weak var textFieldCustomerState: UITextField!
    var buyForexBo : BuyForexBO = BuyForexBO.init()
    
    @IBOutlet weak var totalPayableAmount: UILabel!
    var arrayForCity : [NSDictionary] = [];
    var arrayForBranch : [NSDictionary] = [];
    var branchName : NSString = ""
    var cityCode = ""
    var tcBranchId : Int?
     var branchAddress : NSString = ""
    
    @IBOutlet weak var activeTextField: UITextField?
    
    var lattitude : Double  =  0.00;
    var longitude : Double  =  0.00;
    
    @IBAction func onMapOrAddressButtonClicked(_ sender: Any)
    {
        if !(textFieldBranchAddress.text?.isEmpty)!
        {
            let mapVC : ForexMapViewController = ForexMapViewController.init(nibName: "ForexMapViewController", bundle: nil)
            
            mapVC.destinationName = self.branchName;
            mapVC.lattitude = self.lattitude
            mapVC.longitude = self.longitude
            mapVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            self .present(mapVC, animated: true, completion: nil)
        }
        else
        {
            showAlert(message: "Please select branch")
        }
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        Bundle.main.loadNibNamed("BuyForexLocationViewController", owner: self, options: nil)
        
        super.addViewInBaseView(childView: self.buyForexLocationViewContainer)
        super.setUpHeaderLabel(labelHeaderNameText: "Buy Forex")
        
        if (buyForexBo.buyForexPayment?.isAtBranchChecked)!
        {
            buyForexLocationScrollView.addSubview(self.buyForexLocationView)
        }
        else
        {
            buyForexLocationScrollView.addSubview(self.deliveryView)
        }
        
        totalPayableAmount.text = "\(buyForexBo.buyForexPayment!.totalPayableAmount) INR"
        labelTotalPayableAmountfromDeliveryView.text = "\(buyForexBo.buyForexPayment!.totalPayableAmount) INR"
        LoadingIndicatorView.show("Loading")
        
        self.fetchCustomerBranch()
        
        textfieldPincode.delegate = self
        textFieldBranch.delegate = self
        textviewAdress.delegate = self
        textfieldCityNameForDelivery.delegate = self
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ForexBuyPassangerViewController.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        
       // KeyboardAvoiding.setAvoidingView(self.buyForexLocationView, withTriggerView: self.textFieldCustomerState)
       // KeyboardAvoiding.setAvoidingView(self.buyForexLocationView, withTriggerView: self.textFieldBranch)
        
        saveAsPreferredBranchRadioButton.setImage(UIImage.init(named: "ForexCheckBoxOn"), for: UIControl.State.selected)
        saveAsPreferredBranchRadioButton.setImage(UIImage.init(named: "ForexCheckBoxOff"), for: UIControl.State.normal)
        saveAsPreferredBranchRadioButton.isSelected = false
        
        getAddrViaSMSRadioButton.setImage(UIImage.init(named: "ForexCheckBoxOn"), for: UIControl.State.selected)
        getAddrViaSMSRadioButton.setImage(UIImage.init(named: "ForexCheckBoxOff"), for: UIControl.State.normal)
        getAddrViaSMSRadioButton.isSelected = false
        
        self.bindPreferdBranch()
        
        self.addDoneButtonOnKeyboard()
    }
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle       = UIBarStyle.default
        let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(BuyForexLocationViewController.doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.textFieldCustomerState.inputAccessoryView = doneToolbar
        self.textFieldBranch.inputAccessoryView = doneToolbar
        self.textfieldPincode.inputAccessoryView = doneToolbar
        self.textviewAdress.inputAccessoryView = doneToolbar
        self.textfieldCityNameForDelivery.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction()
    {
        self.textFieldCustomerState.resignFirstResponder()
        self.textFieldBranch.resignFirstResponder()
        self.textfieldPincode.resignFirstResponder()
        self.textviewAdress.resignFirstResponder()
        self.textfieldCityNameForDelivery.resignFirstResponder()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.setNotificationKeyboard()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillLayoutSubviews()
    {
        super.setSubViewFrame(childView: self.buyForexLocationViewContainer)
        if (buyForexBo.buyForexPayment?.isAtBranchChecked)!
        {
            self.buyForexLocationView.frame = CGRect.init(x: 0, y: 0, width: buyForexLocationScrollView.frame.size.width, height: 1000)
            buyForexLocationScrollView.contentSize = CGSize.init(width: 0, height: 650);
            
        }
        else
        {
            self.deliveryView.frame = CGRect.init(x: 0, y: 0, width: buyForexLocationScrollView.frame.size.width, height: 1000)
            buyForexLocationScrollView.contentSize = CGSize.init(width: 0, height: 550);
        }
        
    }
    
    override func viewDidLayoutSubviews()
    {
        super.setSubViewFrame(childView: self.buyForexLocationViewContainer)
        if (buyForexBo.buyForexPayment?.isAtBranchChecked)!
        {
            self.buyForexLocationView.frame = CGRect.init(x: 0, y: 0, width: buyForexLocationScrollView.frame.size.width, height: 1300)
            buyForexLocationScrollView.contentSize = CGSize.init(width: 0, height: 650);
        }
        else
        {
            self.deliveryView.frame = CGRect.init(x: 0, y: 0, width: buyForexLocationScrollView.frame.size.width, height: 1300)
            buyForexLocationScrollView.contentSize = CGSize.init(width: 0, height: 550);
        }
    }
    
    // MARK: - TextField Methods
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == textFieldCustomerState
        {
            self.textFieldBranchAddress.text = ""
            self.textFieldBranch.text = ""
            self.branchName = ""
            self.tcBranchId = nil
            self.branchAddress = ""
            
            self.arrayForBranch = []
            self.lattitude = 0.00
            self.longitude = 0.00
            
        }else if textField == textFieldBranch
        {
            self.textFieldBranchAddress.text = ""
            self.textFieldBranch.text = ""
            self.branchAddress = ""
            self.tcBranchId = nil
            self.lattitude = 0.00
            self.longitude = 0.00
            
        }else if textField == activeTextField
        {
            activeTextField=textField;
        }
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        activeTextField=nil;
    }
    
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool
    {
        if textviewAdress.text == "Enter Address"
        {
            textviewAdress.text = ""
            textviewAdress.textColor = UIColor.black
            
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField == textFieldCustomerState
        {
            let totalString  = "\(textField.text ?? "")\(string)"
            
            if totalString.count > 2
            {
                if !string.isEmpty
                {
                    let filteredArray = arrayForCity.filter
                    {
                        guard let dictionary = $0 as? [String: Any],
                            let name  = dictionary["cityName"] as? String else
                        {
                            return false
                        }
                        return name.localizedCaseInsensitiveContains(totalString)
                    }
                    
                    if filteredArray.isEmpty
                    {
                        showAlert(message: "Please Enter Valid City")
                    }
                    else
                    {
                        let alertController : UIAlertController  = UIAlertController.init(title: "Customer City", message: "Select Customer City", preferredStyle:.actionSheet)
                        
                        for dict in filteredArray
                        {
                            let productname = dict.object(forKey: "cityName")
                            let cityCode = dict.object(forKey: "cityCode")
                            
                            self.cityCode = (cityCode as! NSString) as String
                            
                            let  alertAction: UIAlertAction = UIAlertAction.init(title: productname as? String, style: .default, handler:
                            {
                                
                                (alert: UIAlertAction!) -> Void in
                                textField.text = productname as? String
                                self.fetchBranchDetails(withCityCode: cityCode as! NSString)
                                
                            })
                            
                            alertController.addAction(alertAction)
                        }
                        
                        let  alertCancel: UIAlertAction = UIAlertAction.init(title: "Cancel" , style: .cancel, handler:
                        {
                            
                            (alert: UIAlertAction!) -> Void in
                            
                            
                            //textField.text = productname as? String
                        })
                        alertController.addAction(alertCancel)
                        self.present(alertController, animated:true, completion: nil)
                    }
                }
            }
        }
        else if textField == textFieldBranch
        {
            let totalString  = "\(textField.text ?? "")\(string)"
            
            if totalString.count > 2
            {
                if !string.isEmpty
                {
                    
                    let filteredArray = arrayForBranch.filter
                    {
                        guard let dictionary = $0 as? [String: Any],
                            let name  = dictionary["branchName"] as? String else
                        {
                            return false
                        }
                        return name.localizedCaseInsensitiveContains(totalString)
                    }
                    
                    if filteredArray.isEmpty
                    {
                        showAlert(message: "Please Enter Valid Branch")
                    }
                    else
                    {
                        let alertController : UIAlertController  = UIAlertController.init(title: "Customer Branch", message: "Select Customer Branch", preferredStyle:.actionSheet)
                        
                        for dict in filteredArray
                        {
                            
                            //                        "address1":"hgfhgf",
                            //                        "address2":"hgfhgfh",
                            //                        "alternateNo":"9879789789789",
                            //                        "branchCode":"14",
                            //                        "branchId":11,
                            //                        "branchName":"Borivali Manek Nagar",
                            //                        "city":"Mumbai",
                            //                        "landline":7567567567567,
                            //                        "latitude":19.217907,
                            //                        "longitude":72.847084,
                            //                        "mobile":9978987978,
                            //                        "state":"Maharashtra"
                            
                            
                            let productname = dict.object(forKey: "branchName")
                            let branchAddress = "\(dict.object(forKey: "address1") ?? "") \(dict.object(forKey: "address2") ?? "") \(dict.object(forKey: "branchName") ?? "") \(dict.object(forKey: "city") ?? "")"
                            
                            let branchNameLocal = dict.object(forKey: "branchName")
                            let branchLattitude = dict.object(forKey: "latitude")
                            let branchLongitude = dict.object(forKey: "longitude")
                             let branchId = dict.object(forKey: "branchId")
                            
                            let  alertAction: UIAlertAction = UIAlertAction.init(title: productname as? String, style: .default, handler:
                            {
                                (alert: UIAlertAction!) -> Void in
                                textField.text = productname as? String
                                self.textFieldBranchAddress.text = branchAddress
                                self.branchAddress = branchAddress as NSString
                                self.branchName = branchNameLocal as! NSString
                                self.tcBranchId = branchId as? Int
                                self.lattitude = branchLattitude as! Double
                                self.longitude = branchLongitude as! Double
                            })
                            
                            alertController.addAction(alertAction)
                        }
                        
                        let  alertCancel: UIAlertAction = UIAlertAction.init(title: "Cancel" , style: .cancel, handler:
                        {
                            (alert: UIAlertAction!) -> Void in
                            //textField.text = productname as? String
                        })
                        alertController.addAction(alertCancel)
                        self.present(alertController, animated:true, completion: nil)
                    }
                }
            }
        }else if textField == textfieldPincode
    {
    
    let pincodeCharacterCount = self.textfieldPincode.text?.count ?? 0
    if (range.length + range.location > pincodeCharacterCount){
    return false
    }
    
    let newLength = pincodeCharacterCount + string.count - range.length
    return newLength <= 6
    
    }
        return true;
    }

    func dismissKeyboard()
    {
        view.endEditing(true)
    }
    
    // MARK: Final request
    func createJSONRequest(isReconfirm:Int)
    {
        LoadingIndicatorView.show()
        
        let reqJSON : NSMutableDictionary = NSMutableDictionary()
        let tcilForexBooking : NSMutableDictionary = NSMutableDictionary()
        tcilForexBooking.setValue(0, forKey: "addressId")
        tcilForexBooking.setValue(buyForexBo.buyForexPayment?.totalPayableAmount, forKey: "bookingAmount")
        tcilForexBooking.setValue("ONLINE", forKey: "bookingType")
       // tcilForexBooking.setValue("F", forKey: "paymentType")
        tcilForexBooking.setValue(buyForexBo.buyForexPayment?.paymentType, forKey: "paymentType")
        tcilForexBooking.setValue("\(buyForexBo.buyForexPayment?.tcilForexQuote.object(forKey: "quoteId") ?? "")", forKey: "quoteId")
        
        if (buyForexBo.buyForexPayment?.isAtBranchChecked)!
        {
            tcilForexBooking.setValue("PICKUP", forKey: "deliveryMode")
            tcilForexBooking.setValue(buyForexBo.buyForexPassengerInfo?.travellerArray[0].travellerTitle, forKey: "deliveryTitle")
            tcilForexBooking.setValue(buyForexBo.buyForexPassengerInfo?.travellerArray[0].travellerFirstName, forKey: "deliveryFirstName")
            tcilForexBooking.setValue(buyForexBo.buyForexPassengerInfo?.travellerArray[0].travellerLastName, forKey: "deliveryLastName")
            tcilForexBooking.setValue(buyForexBo.emailID, forKey: "deliveryEmail")
            tcilForexBooking.setValue(buyForexBo.buyForexPassengerDetails?.mobileNo, forKey: "deliveryMobile")
            tcilForexBooking.setValue("", forKey: "deliveryAddress")
            tcilForexBooking.setValue(textFieldCustomerState.text, forKey: "deliveryCity")
            tcilForexBooking.setValue(buyForexBo.buyForexPassengerInfo?.stateOfCustomer, forKey: "deliveryState")
            tcilForexBooking.setValue("", forKey: "deliveryPincode")
        }
        else
        {
            tcilForexBooking.setValue("DELIVERY", forKey: "deliveryMode")
            tcilForexBooking.setValue(buyForexBo.buyForexPassengerInfo?.travellerArray[0].travellerTitle, forKey: "deliveryTitle")
            tcilForexBooking.setValue(buyForexBo.buyForexPassengerInfo?.travellerArray[0].travellerFirstName, forKey: "deliveryFirstName")
            tcilForexBooking.setValue(buyForexBo.buyForexPassengerInfo?.travellerArray[0].travellerLastName, forKey: "deliveryLastName")
            tcilForexBooking.setValue(buyForexBo.emailID, forKey: "deliveryEmail")
            tcilForexBooking.setValue( buyForexBo.buyForexPassengerDetails?.mobileNo, forKey: "deliveryMobile")
            tcilForexBooking.setValue(textviewAdress.text, forKey: "deliveryAddress")
            tcilForexBooking.setValue(textfieldCityNameForDelivery.text, forKey: "deliveryCity")
            tcilForexBooking.setValue(buyForexBo.buyForexPassengerInfo?.stateOfCustomer, forKey: "deliveryState")
            tcilForexBooking.setValue(textfieldPincode.text, forKey: "deliveryPincode")
        }
        
        tcilForexBooking.setValue("App", forKey: "bookingThrough")
        tcilForexBooking.setValue("", forKey: "preconfirmPageUrl")
        tcilForexBooking.setValue(buyForexBo.buyForexPassengerDetails?.emailID, forKey: "bookedFor")
        tcilForexBooking.setValue(buyForexBo.buyForexPassengerDetails?.emailID, forKey: "bookedForEmail")
        tcilForexBooking.setValue("F", forKey: "isOnBehalf")
        tcilForexBooking.setValue("N", forKey: "isRoeEdit")
        tcilForexBooking.setValue("Guest", forKey: "userId") // komal 5 jul 2018
        reqJSON.setValue(tcilForexBooking, forKey: "tcilForexBooking")
        
        //quote
        
        let tcilForexQuoteTravellerCollection : NSMutableArray =   (buyForexBo.buyForexPayment!.tcilForexQuote.object(forKey: "tcilForexQuoteTravellerCollection") as! NSArray).mutableCopy() as! NSMutableArray
        for i in 0..<tcilForexQuoteTravellerCollection.count
        {
            let dict : NSMutableDictionary = (tcilForexQuoteTravellerCollection[i] as! NSDictionary).mutableCopy() as! NSMutableDictionary
            dict.setValue(buyForexBo.buyForexPayment?.tcilForexQuote.object(forKey: "quoteId")!, forKey: "quoteId")
            dict.setValue(buyForexBo.buyForexPassengerInfo?.travellerArray[i].travellerTitle, forKey: "travellerTitle")
            dict.setValue(buyForexBo.buyForexPassengerInfo?.travellerArray[i].travellerFirstName, forKey: "travellerFirstName")
            //
            
            let dot : NSDate = buyForexBo.buyForexPassengerInfo!.travellerArray[i].travellerDateOfTravel!
            
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MM-yyyy"
            
            let resultString = formatter.string(from: dot as Date)
            printLog("resultString---> \(resultString)")
            
//            let calendar = Calendar.current
//            let year = calendar.component(.year, from: dot as Date)
//            let month = calendar.component(.month, from: dot as Date)
//            let day = calendar.component(.day, from: dot as Date)
//            dict.setValue("\(day)-\(month)-\(year)", forKey: "travellerDot")
            dict.setValue(resultString, forKey: "travellerDot")
            dict.setValue(buyForexBo.buyForexPassengerInfo?.travellerArray[i].travellerLastName, forKey: "travellerLastName")
            dict.setValue(buyForexBo.buyForexOptionViewDetails?.purposeOfTravel, forKey: "travelType")
            if buyForexBo.buyForexPassengerInfo?.travellerArray[i].travellerTitle == "Mr."
            {
                dict.setValue("M", forKey: "travellerGender")
            }
            else
            {
                dict.setValue("F", forKey: "travellerGender")
            }
            
            tcilForexQuoteTravellerCollection.replaceObject(at: i, with: dict)
        }
        
        buyForexBo.buyForexPayment?.tcilForexQuote.setValue(tcilForexQuoteTravellerCollection, forKey: "tcilForexQuoteTravellerCollection")
        reqJSON.setValue( ForexAppUtility.convertQuoteResponse(quoteDict: (buyForexBo.buyForexPayment?.tcilForexQuote)!), forKey: "tcilForexQuote")
        reqJSON.setValue(isReconfirm, forKey: "isReconfirm")
        
        let jsonData: NSData = try!JSONSerialization.data(withJSONObject: reqJSON, options: JSONSerialization.WritingOptions.prettyPrinted )as NSData
        printLog(NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue)! as String)
        let pathParameter  : NSString = "/tcForexRS/booking/buy"
        ForexCommunicationManager.sharedInstance.execTaskWithAlamofire(pathParam: pathParameter, queryParam: "", requestType: "post", jsonDict: reqJSON) { (status, response) in
            
            DispatchQueue.main.async {
                LoadingIndicatorView.hide()
            
            
            if(response != nil)
            {
                 //  let _ : NSArray = response as! NSArray
                let responseDict : NSDictionary = response as! NSDictionary
                printLog("responseDict---> \(responseDict)")
                
                let transactionId = responseDict["isError"] as! Bool
                let message = responseDict["message"] as! String
                
                if transactionId == false
                {
                    let url = URL(string: "\(kpgURLForPayment)".appending(message))
                    
                    if #available(iOS 10.0, *)
                    {
                        UIApplication.shared.open(url!, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: { (result) in
                            
                            LoadingIndicatorView.hide()
                        })
                    }
                    else
                    {
                        UIApplication.shared.openURL(url!)
                    }
                }
                else if message.lowercased().contains(BookingFareIncrease) || message.lowercased().contains(BookingFareDecrease)
                {
                    
                    self.showAlertWithCancel(message: message as NSString)
                    
                }else{
                    
                    self.showAlert(message: message as NSString)
                }
            }
            else
            {
                self.showAlert(message: "Some error has occurred")
            }
            }
        }
        
        /*{
         "tcilForexBooking": {
         "addressId": 6843,
         "bookingAmount": 60108,
         "bookingType": "ONLINE",
         "paymentType": "F",
         "quoteId": "1445",
         "deliveryMode": "PICKUP",
         "deliveryTitle": "Ms",
         "deliveryFirstName": "Shruti",
         "deliveryLastName": "Tele",
         "deliveryEmail": "shruti.tele@in.thomascook.com",
         "deliveryMobile": "9879789789",
         "deliveryAddress": "yry yrtyr\nhjkhj\njkhjkh\nhjkhhk",
         "deliveryCity": "Mumbai",
         "deliveryState": "Maharashtra",
         "deliveryPincode": "454664",
         "preconfirmPageUrl": "https://uatastra.thomascook.in/Forex/buy/forex-booking-review?quoteId=1445&type=3",
         "bookedFor": "shruti.tele@in.thomascook.com",
         "bookedForEmail": "shruti.tele@in.thomascook.com",
         "isOnBehalf": "T",
         "bookingThrough": "Desktop",
         "isRoeEdit": "N"
         },
         "tcilForexQuote": {
         "advanceAmount": 6011,
         "bookerEmailId": "bh@dfg.hfgh",
         "bookerMobileNo": "9789979797",
         "convenienceFee": 0,
         "createDate": "2017-11-24T17:25:09+05:30",
         "custId": 53,
         "dateOfTravel": "29-11-2017",
         "deliveryCharges": 0,
         "gstAfterConvenienceFee": 0,
         "gstAfterDelivery": 0,
         "gstBranch": "23",
         "gstBranchCode": "BHO",
         "gstState": "27",
         "isBranchUt": "N",
         "isRoeEdit": "N",
         "isStateUt": "N",
         "moduleId": 1,
         "moduleName": "Buy",
         "pageType": "PCP",
         "quoteId": 1445,
         "quoteStatus": "Y",
         "splitDiffAmount": 0,
         "tcilForexQuoteRmInfoCollection": [],
         "tcilForexQuoteTravellerCollection": [
         {
         "createDate": "2017-11-24T17:25:09+05:30",
         "tcilForexQuoteTravellerProductCollection": [
         {
         "amount": 1000,
         "createDate": "2017-11-24T17:25:09+05:30",
         "currencyCode": "USD",
         "currencyId": 1,
         "currencyName": "US Dollar",
         "equivalentInr": 60000,
         "moduleName": "Buy",
         "nostroAvailable": "Y",
         "productId": 1,
         "productName": "Cash",
         "productOrder": 0,
         "roe": 60,
         "travellerProductId": 2086
         }
         ],
         "travelType": "Personal",
         "travellerAmount": 60000,
         "travellerDot": "29-11-2017",
         "travellerFirstName": "Shruti",
         "travellerGender": "F",
         "travellerId": 1552,
         "travellerLastName": "Tele",
         "travellerNo": 1,
         "travellerOrder": 0,
         "travellerServiceTax": 108,
         "travellerServiceTaxDesc": "IGST@18%=108",
         "travellerTitle": "Ms",
         "travellerTotalProducts": 1,
         "quoteId": 1445
         }
         ],
         "totalNoOfProducts": 1,
         "totalNoOfTraveller": 1,
         "totalPrice": 60108,
         "totalPriceAfterConvenienceFee": 0,
         "totalPriceAfterDeliveryCharge": 0,
         "totalQuoteAmount": 60000,
         "totalServiceTax": 108
         },
         "isReconfirm": 0
         }
         */
    }
    
    
    
    // MARK: Fetch Customer Branch
    func fetchCustomerBranch()
    {
        let pathParameter  : NSString = "/tcForexRS/generic/gstCity"
        ForexCommunicationManager.sharedInstance.execTask(pathParam:pathParameter , queryParam: "", requestType: "get", jsonDict:NSDictionary())
        {
            (status, response) in
            
            DispatchQueue.main.async { () -> Void in
                LoadingIndicatorView.hide()
           
            
            if(response != nil)
            {
                let jsonArray : NSArray = response as! NSArray
                self.arrayForCity = NSMutableArray.init(array: jsonArray) as! [NSDictionary];
            }
            else
            {
                self.showAlert(message: "Some error has occurred")
            }
             }
        }
        
    }
    
    func fetchBranchDetails(withCityCode cityCode:NSString)
    {
        LoadingIndicatorView.show();
        //http://172.16.177.159:6602/tcForexRS/generic/branchdetails/BOM
        
        let pathParameter  : NSString = "/tcForexRS/generic/branchdetails/\(cityCode)" as NSString
        ForexCommunicationManager.sharedInstance.execTask(pathParam:pathParameter , queryParam: "", requestType: "get", jsonDict:NSDictionary())
        {
            (status, response) in
            DispatchQueue.main.async { () -> Void in
                LoadingIndicatorView.hide()
           
            if(response != nil)
            {
                let jsonArray : NSArray = response as! NSArray
                self.arrayForBranch = NSMutableArray.init(array: jsonArray) as! [NSDictionary];
            }
            else
            {
                self.showAlert(message: "Some error has occurred")
            }
             }
        }
        
    }
    func validateFieldsForDeliveryView()->Bool
    {
        if ((textfieldPincode.text?.trimmingCharacters(in: CharacterSet(charactersIn: " ")))?.isEmpty)!
        {
            showAlert(message: "Please enter Pincode")
            return false
        }
        if !((textfieldPincode.text?.trimmingCharacters(in: CharacterSet(charactersIn: " ")))?.isEmpty)!
        {
            if (textfieldPincode.text?.count)! < 6
            {
                showAlert(message: "Please enter validPincode")
                return false
            }
        }
        if ((textviewAdress.text?.trimmingCharacters(in: CharacterSet(charactersIn: " ")))?.isEmpty)!
        {
            showAlert(message: "Please enter address")
            return false
        }
        if ((textfieldCityNameForDelivery.text?.trimmingCharacters(in: CharacterSet(charactersIn: " ")))?.isEmpty)!
        {
            showAlert(message: "Please enter city name")
            return false
        }
        
        return true
    }
    
    
    func validateFields()->Bool
    {
        if ((textFieldCustomerState.text?.trimmingCharacters(in: CharacterSet(charactersIn: " ")))?.isEmpty)! || (textFieldCustomerState.text?.count)! < 3
        {
            showAlert(message: "Please Select City")
            return false
        }
        if ((textFieldBranch.text?.trimmingCharacters(in: CharacterSet(charactersIn: " ")))?.isEmpty)! || (textFieldBranch.text?.count)! < 3
        {
            showAlert(message: "Select Select Branch")
            return false
        }
        return true
    }
    
    @IBAction func continueButtonClickedFromDeliveryView(_ sender: Any)
    {
        if validateFieldsForDeliveryView()
        {
            createJSONRequest(isReconfirm: 0)
        }
        
    }
    
    @IBAction func continueButtonClicked(_ sender: Any)
    {
        if validateFields()
        {
            createJSONRequest(isReconfirm: 0)
        }
    }
    
    @IBAction func saveAsPreferredRadioButtonClicked(_ sender: Any)
    {
        if ((textFieldCustomerState.text?.trimmingCharacters(in: CharacterSet(charactersIn: " ")))?.isEmpty)!
        {
            showAlert(message: "Select city")
            
        }
        else if ((textFieldBranch.text?.trimmingCharacters(in: CharacterSet(charactersIn: " ")))?.isEmpty)!
        {
            showAlert(message: "Select branch")
            
        }
        else
        {
            saveAsPreferredBranchRadioButton.isSelected =  !saveAsPreferredBranchRadioButton.isSelected
            
             ForexAppUtility.savePrefredBranch(cityCode: self.cityCode, cityName: self.textFieldCustomerState.text ?? "", branchCode: self.tcBranchId, branchName: self.textFieldBranch.text ?? "", address: self.branchAddress as String, latitude: self.lattitude, longitude: self.longitude, isSave: saveAsPreferredBranchRadioButton.isSelected)
        }
    }
    
    @IBAction func getAddressViaSmsBtnClicked(_ sender: Any)
    {
        if !(textFieldBranchAddress.text?.isEmpty)!
        {
//            getAddrViaSMSRadioButton.isSelected = !getAddrViaSMSRadioButton.isSelected
//            
//            if getAddrViaSMSRadioButton.isSelected == true
//            {
//                let branchAddress = self.textFieldBranchAddress.text
//                let mobileNumber = buyForexBo.buyForexOptionViewDetails!.contactDetail
//                
//                self.getBranchAddressViaSms(mobileNumber: mobileNumber,branchAddress: branchAddress!)
//                {
//                    (String,status) in
//                    
//                    print("String---> \(String)")
//                    print("status---> \(status)")
//                    
//                    if status
//                    {
//                        
//                    }
//                    else
//                    {
//                        
//                    }
//                }
//            }
            
            getAddrViaSMSRadioButton.isSelected = !getAddrViaSMSRadioButton.isSelected
            
            if getAddrViaSMSRadioButton.isSelected == true
            {
                let popupVC: GetAddressViaSmsPopUpViewController = GetAddressViaSmsPopUpViewController(nibName: "GetAddressViaSmsPopUpViewController", bundle: nil)
                popupVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                popupVC.delegate = self
                popupVC.branchAddress = self.textFieldBranchAddress.text
                self.present(popupVC, animated: false, completion: nil)
            }
            
        }
        else
        {
            getAddrViaSMSRadioButton.isSelected = false
            showAlert(message: "Please select branch.")
        }
    }
    
    func unCheckGetAddressViaSmsButton()
    {
        getAddrViaSMSRadioButton.isSelected = false
    }
    
    func getBranchAddressViaSms(mobileNumber:String ,branchAddress: String, completion: @escaping (_ responseDataString:String , _ status:Bool) -> Void)
    {
        let urlString = String(format: AuthenticationConstant.SendBranchAddressTOSERVERURL,mobileNumber,branchAddress)
        let serviceUrl = URL(string: urlString.replacingOccurrences(of: " ", with: "%20"))
        
        var request = URLRequest(url: serviceUrl!)
        request.httpMethod = HTTPMethod.get.rawValue
        request.setValue(ContentType.ApplicationText.rawValue, forHTTPHeaderField: APIConstants.ContentType)
        
        let session = URLSession.shared
        session.dataTask(with: request)
        {
            (data, response, error) in
            
            if let error = error
            {
                printLog(error)
                completion(error.localizedDescription,false)
            }
            else
            {
                if let response = response
                {
                    printLog(response)
                }
                
                if let data1 = data
                {
                    let responseString:String = String(data: data1, encoding: String.Encoding.utf8)!
                    completion(responseString,true)
                }
            }
        }.resume()
    }
    
    // MARK: - Keyboard Handling Methods
    @objc func keyboardWasShown(notification: NSNotification)
    {
        var info = notification.userInfo!
        let keyboardSize = (info[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsets.init(top: 0.0, left: 0.0, bottom: keyboardSize!.height+10, right: 0.0)
        self.buyForexLocationScrollView.contentInset = contentInsets
        self.buyForexLocationScrollView.scrollIndicatorInsets = contentInsets
        var aRect : CGRect = self.deliveryView.frame
        aRect.size.height -= keyboardSize!.height
        if let activeField = self.activeTextField
        {
            if (!aRect.contains(activeField.frame.origin))
            {
                self.buyForexLocationScrollView.scrollRectToVisible(activeField.frame, animated: true)
            }
        }
    }
    
    @objc func keyboardWillBeHidden(notification: NSNotification)
    {
        let contentInsets : UIEdgeInsets = UIEdgeInsets.init(top: 0.0, left: 0.0,bottom: 0.0, right: 0.0)
        self.buyForexLocationScrollView.contentInset = contentInsets
        self.buyForexLocationScrollView.scrollIndicatorInsets = contentInsets
        self.view.endEditing(true)
    }
    
    func setNotificationKeyboard ()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    
    func bindPreferdBranch() {
        
        if let savedBranchDetasils:Dictionary<String,Any> = ForexAppUtility.getPreferdBranchDetails()
        {
            saveAsPreferredBranchRadioButton.isSelected = true
            
            if  let cityCode:String = savedBranchDetasils[cityCodeSavePreferedBranch] as? String
            {
                self.cityCode = cityCode
            }
            
            if let cityName:String = savedBranchDetasils[cityNameSavePreferedBranch] as? String
            {
                self.textFieldCustomerState.text = cityName
            }
            if let branchName:String = savedBranchDetasils[branchNameSavePreferedBranch] as? String
            {
                self.textFieldBranch.text = branchName
                self.branchName = branchName as NSString
            }
            if let branchCode:Int = savedBranchDetasils[branchCodeSavePreferedBranch] as? Int
            {
                self.tcBranchId = branchCode
            }
            if let address:String = savedBranchDetasils[addressSavePreferedBranch] as? String
            {
                textFieldBranchAddress.text =  address
                self.branchAddress  = address as NSString
            }
            if let latitude:Double = savedBranchDetasils[latitudeSavePreferedBranch] as? Double
            {
                self.lattitude = latitude
            }
            if let longitude:Double = savedBranchDetasils[longitudeSavePreferedBranch] as? Double
            {
                self.longitude = longitude
            }
            
            self.fetchBranchDetails(withCityCode: cityCode as NSString)
            
        }
    }
    
    
    func showAlertWithCancel(message:NSString) -> Void
    {
        let alert = UIAlertController(title: "Alert", message: message as String, preferredStyle: UIAlertController.Style.alert)
       
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: { UIAlertAction in
            alert.dismiss(animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { UIAlertAction in
            
            self.createJSONRequest(isReconfirm: 1)
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
}




// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
