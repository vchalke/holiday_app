//
//  TravelDocTableViewCell.swift
//  holidays
//
//  Created by Kush_Team on 22/06/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

import UIKit
protocol TravelDocTableViewCellDelegate {
    func showPassportVisaDetails()
    func showImageInMainViewWithString(dictionary:Dictionary<String,Any>, withTag:Int)
}
class TravelDocTableViewCell: UITableViewCell,UITableViewDelegate,UITableViewDataSource,PassPortInfoTableViewCellDelegate {
    var profileDetails:[String:Any]?
    @IBOutlet weak var tbl_viewsOne: UITableView!
    @IBOutlet weak var tbl_viewsTwo: UITableView!
    @IBOutlet weak var btn_firstPlus: UIButton!
    @IBOutlet weak var btn_secondPlus: UIButton!
    var delegate : TravelDocTableViewCellDelegate? = nil
    var travelerInfoModel : CustomerProfileModel = CustomerProfileModel()
    @IBOutlet weak var cnst_heightOfFirst: NSLayoutConstraint!
    @IBOutlet weak var cnst_heightOfSecond: NSLayoutConstraint!
    var isShowPassport = false
    var isShowVisa = false
    var firstDatasCount = 1
    var secondDatasCount = 2
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setTableViewProfileModelsDetails(profileModel:CustomerProfileModel){
        travelerInfoModel = profileModel
        
        self.tbl_viewsOne.register(UINib(nibName: "PassPortInfoTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "PassPortInfoTableViewCell")
        self.tbl_viewsOne.delegate = self
        self.tbl_viewsOne.dataSource = self;
        
        self.tbl_viewsTwo.register(UINib(nibName: "VisaInfoTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "VisaInfoTableViewCell")
        self.tbl_viewsTwo.delegate = self
        self.tbl_viewsTwo.dataSource = self;
        
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if (tableView == self.tbl_viewsOne){
            return 1
        }
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        if (tableView == self.tbl_viewsOne){
            let cell :PassPortInfoTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PassPortInfoTableViewCell", for: indexPath) as! PassPortInfoTableViewCell
            cell.setProfileModelsDetailsInPassports(profileModel: self.travelerInfoModel)
            cell.btn_passportOne.addTarget(self, action: #selector(self.passportClick(_:)), for: .touchUpInside)
            cell.btn_passportTwo.addTarget(self, action: #selector(self.passportClick(_:)), for: .touchUpInside)
            cell.delegates = self
            return cell
        }
        let cell :VisaInfoTableViewCell = tableView.dequeueReusableCell(withIdentifier: "VisaInfoTableViewCell", for: indexPath) as! VisaInfoTableViewCell
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (tableView == self.tbl_viewsOne){
            return 450
        }
        return 230
    }
    @objc func passportClick(_ sender:UIButton)
    {
        printLog(travelerInfoModel.passportInfo)
        self.delegate?.showImageInMainViewWithString(dictionary:travelerInfoModel.passportInfo, withTag:sender.tag)
    }
    //MARK:-  Passports Delegates
    func showImageWithString(string: String) {
        
    }
    
    @IBAction func btn_firstPlus(_ sender: Any) {
        
        self.hideFirstView(flag: isShowPassport)
        isShowPassport = !isShowPassport
        
    }
    @IBAction func btn_SecondPlus(_ sender: Any) {
        
        self.hideSecondView(flag: isShowVisa)
        isShowVisa = !isShowVisa
    }
    func hideFirstView(flag:Bool){
        self.cnst_heightOfFirst.constant = flag ? 0 : CGFloat(firstDatasCount * 450)
        self.tbl_viewsOne.isHidden = flag
        self.btn_firstPlus.setImage(flag ? UIImage.init(named: "plusGray") : UIImage.init(named: "minusGray"), for: .normal)
        self.delegate?.showPassportVisaDetails()
    }
    func hideSecondView(flag:Bool){
        self.cnst_heightOfSecond.constant = flag ? 0 : CGFloat(secondDatasCount * 230)
        self.tbl_viewsTwo.isHidden = flag
        self.btn_secondPlus.setImage(flag ? UIImage.init(named: "plusGray") : UIImage.init(named: "minusGray"), for: .normal)
        self.delegate?.showPassportVisaDetails()
    }
}
