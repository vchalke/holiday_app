//
//  BuyForexOptionsViewController.swift
//  holidays
//
//  Created by ketan on 23/11/17.
//  Copyright © 2017 Mobicule. All rights reserved.
//

import UIKit

class BuyForexOptionsViewController: ForexBaseViewController,UITextFieldDelegate
{
    @IBOutlet var buyForexOptionView: UIView!
    @IBOutlet weak var textFieldSelectTravelPurpose: UITextField!
    @IBOutlet weak var textFieldNumberOfTravellers: UITextField!
    @IBOutlet weak var textFieldPhoneNumber: UITextField!
    @IBOutlet weak var textFieldDate: UITextField!
    @IBOutlet weak var textFieldMonth: UITextField!
    @IBOutlet weak var textFieldYear: UITextField!
    var selectedDate : Date = Date.init()
    var previousController : String = ""
    
    override func viewDidLoad()
    {
        Bundle.main.loadNibNamed("BuyForexOptionsViewController", owner: self, options: nil)
        super.addViewInBaseView(childView: self.buyForexOptionView)
        super.setUpHeaderLabel(labelHeaderNameText: "Buy Forex")
        super.viewDidLoad()
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ForexBuyPassangerViewController.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        textFieldPhoneNumber.delegate = self
        KeyboardAvoiding.setAvoidingView(self.buyForexOptionView, withTriggerView: self.textFieldPhoneNumber)
        
         if let tc_MobileNumber:String = UserDefaults.standard.value(forKey: "userDefaultTC-LoginMobileNumber") as? String
        {
            self.textFieldPhoneNumber.text = tc_MobileNumber
        }
        
        self.addDoneButtonOnKeyboard()
    }
    
    override func backButtonClicked(buttonView:UIButton )
    {
        if self.previousController == "SlideMenuViewController"
        {
            SlideNavigationController.sharedInstance().popToRootViewController(animated: true)
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle       = UIBarStyle.default
        let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(BuyForexOptionsViewController.doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.textFieldPhoneNumber.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction()
    {
        self.textFieldPhoneNumber.resignFirstResponder()
    }
    
    func dismissKeyboard()
    {
        view.endEditing(true)
    }
    
 
    func validateFields() -> Bool
    {
        if (textFieldSelectTravelPurpose.text?.isEmpty)!
        {
            showAlert(message: "Please select purpose of travel.")
            return false
        }
        if (textFieldNumberOfTravellers.text?.isEmpty)!
        {
            showAlert(message: "Please select number of travellers.")
            return false
        }
        if (textFieldDate.text?.isEmpty)!
        {
            showAlert(message: "Please select date of travel.")
            return false
        }
        if (textFieldPhoneNumber.text?.isEmpty)!
        {
            showAlert(message: "Please enter mobile number.")
            return false
        }
       
        if !isValidMobileNo(testStr: textFieldPhoneNumber.text!)
        {
            showAlert(message: "Please enter valid mobile number.")
            return false
        }

        return true
        
    }
    @IBAction func onSelectDateButtonClicked(_ sender: Any)
    {
        self.datePickerTapped()
    }
    
    override func viewWillLayoutSubviews()
    {
        super.setSubViewFrame(childView: self.buyForexOptionView)
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField == textFieldPhoneNumber
        {
            
            let textFieldText: NSString = (textField.text ?? "") as NSString
            
            let txtAfterUpdate = textFieldText.replacingCharacters(in: range, with: string)
            
            
            if txtAfterUpdate.count > 10
            {
                return false
            }
            
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        if textField == textFieldNumberOfTravellers
        {
            let arrayFornumberTraveller  =  ["1","2","3","4"]
            
            let alertController : UIAlertController  = UIAlertController.init(title: "Number of travellers", message: "Select number of travellers", preferredStyle:.actionSheet)
            
            for travellerNumber in arrayFornumberTraveller
            {
                let  alertAction: UIAlertAction = UIAlertAction.init(title: travellerNumber, style: .default, handler:
                {
                    
                    (alert: UIAlertAction!) -> Void in
                    self.textFieldNumberOfTravellers.text = travellerNumber
                })
                
                alertController.addAction(alertAction)
            }
            
            
            self.present(alertController, animated:true, completion: nil)
            
            return false
        }
        else if textField == textFieldSelectTravelPurpose
        {
            let arrayForTravelPurpose  =  ["Personal","Studies","Business"]
            
            let alertController : UIAlertController  = UIAlertController.init(title: "Purpose of Travel", message: "Select Purpose of Travel", preferredStyle:.actionSheet)
            
            for travellPurpose in arrayForTravelPurpose
            {
                let  alertAction: UIAlertAction = UIAlertAction.init(title: travellPurpose, style: .default, handler:
                {
                    
                    (alert: UIAlertAction!) -> Void in
                    self.textFieldSelectTravelPurpose.text = travellPurpose
                })
                
                alertController.addAction(alertAction)
            }
            
            
            self.present(alertController, animated:true, completion: nil)
            
            return false
            
        }
        
        return true
    }
    
    func datePickerTapped()
    {
        let currentDate = Date()
        var dateComponents = DateComponents()
        dateComponents.month = -3
        _ = Calendar.current.date(byAdding: dateComponents, to: currentDate)
        
        let datePicker = DatePickerDialog(textColor: .black,
                                          buttonColor: .black,
                                          font: UIFont.boldSystemFont(ofSize: 17),
                                          showCancelButton: true)
        datePicker.show("Select Travel Date",
                        doneButtonTitle: "Done",
                        cancelButtonTitle: "Cancel",
                        //minimumDate: threeMonthAgo,
            //maximumDate: currentDate,
        datePickerMode: .date) { (date) in
            if let dt = date
            {
                let currentDate : Date = Date.init()
         
                
                let calendar = NSCalendar.current
                let date1 = calendar.startOfDay(for: dt)
                let date2 = calendar.startOfDay(for:currentDate)

                let components =  calendar.dateComponents([.day], from: date1, to: date2)
                
                if components.day! > -2 || components.day! < (-61)
                {
                    self.showAlert(message: "Dear Customer, since your travel date is immediate, we cannot proceed ahead with this transaction. Our forex expert will contact you for further assistance on the contact details shared with us. You can also call us on toll free no 1800 2099 100.")
                    self.textFieldDate.text = ""
                    self.textFieldMonth.text = ""
                    self.textFieldYear.text = ""
                }
                else
                {
                    let formatter = DateFormatter()
                    formatter.dateFormat = "MM/dd/yyyy"
                    self.selectedDate = dt
                    let year = calendar.component(.year, from: dt)
                    let month = calendar.component(.month, from: dt)
                    let day = calendar.component(.day, from: dt)
                    self.textFieldDate.text = "\(day)"
                    self.textFieldMonth.text = "\(month)"
                    self.textFieldYear.text = "\(year)"
                }
            }
        }
    }
    /*
    //
    //    func textField(_ textField: UITextField,
    //                   shouldChangeCharactersIn range: NSRange,
    //                   replacementString string: String) -> Bool
    //    {
    //
    //        var startString = ""
    //
    //        if (textField.text != nil)
    //        {
    //            startString += textField.text!
    //        }
    //
    //        startString += string
    //
    //        let limitNumber =  Int(startString)
    //
    //        if limitNumber! > 60
    //        {
    //            return false
    //        }
    //        else
    //        {
    //            return true;
    //        }
    //    }
    */
    
    func createLead(mobileNumber : String)
    {
//        https://thomascookindia--tst1.custhelp.com/cgi-bin/thomascookindia.cfg/php/custom/mobile_iosapp_forex_lead.php?mobile=9811976670&requirement_type=Buy&sub_type=Send Money
        
        let url = URL(string: "\(kUrlForCreateLead)mobile=\(String(describing: mobileNumber))&requirement_type=Buy")
        printLog("url---> \(String(describing: url))")
        
        URLSession.shared.dataTask(with: url!, completionHandler:
        {
            (data, response, error) in
            
            if(error != nil)
            {
                printLog("error")
            }
            else
            {
                if let data = data, let stringResponse = String(data: data, encoding: .utf8)
                {
                    printLog("Response---< \(stringResponse)")
                }
            }
        }).resume()
    }
    
    @IBAction func onContinueButtonClicked(_ sender: Any)
    {
        if validateFields()
        {
            self.createLead(mobileNumber: self.textFieldPhoneNumber.text!)
            
            let forexBuyPassangerVc: ForexBuyPassangerViewController = ForexBuyPassangerViewController(nibName: "ForexBaseViewController", bundle: nil)
            let contactDetail : String = textFieldPhoneNumber.text!
            let buyForexBo : BuyForexBO = BuyForexBO.init()
            let buyForexOptionViewDetails : BuyForexOptionsViewBO = BuyForexOptionsViewBO.init()
            buyForexOptionViewDetails.purposeOfTravel = textFieldSelectTravelPurpose.text! as NSString
            buyForexOptionViewDetails.noOfTraveller = NSInteger.init(textFieldNumberOfTravellers.text!)!
            buyForexOptionViewDetails.travelDate = selectedDate as NSDate
            buyForexOptionViewDetails.contactDetail = contactDetail
            
            buyForexBo.buyForexOptionViewDetails = buyForexOptionViewDetails
            forexBuyPassangerVc.previousScreen = "option view"
            forexBuyPassangerVc.buyForexBo = buyForexBo
            self.navigationController?.pushViewController(forexBuyPassangerVc, animated: true)
            
        }
    }
    
    @IBAction func purposeOptionalButtonPress(_ sender: UIButton) {
       
//        self.blurView.alpha = 1
//        self.purposeView.alpha = 1
        let popupVC: PopUpViewController = PopUpViewController(nibName: "PopUpViewController", bundle: nil)
        popupVC.msg = "For any purpose of travel the total amount of Foreign Exchange that you can carry is restricted to USD 250,000 or equivalent in a year."
        popupVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(popupVC, animated: false, completion: nil)
      
    }
    
//    @IBAction func numberOfTravellerOptionalButtonPress(_ sender: UIButton) {
//        self.blurView.alpha = 1
//         self.numberOfTravellerView.alpha = 1
//    }
    
    @IBAction func dateOptionalButtonPress(_ sender: UIButton) {
//        self.blurView.alpha = 1
//        self.dateView.alpha = 1
        let popupVC: PopUpViewController = PopUpViewController(nibName: "PopUpViewController", bundle: nil)
        popupVC.msg = "As per RBI rules, your date of travel should be within the next 60 days for buying Foreign Exchanges today. If the date of travel is different for passenger, you can edit the same for each traveller before making the payment."
        popupVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(popupVC, animated: false, completion: nil)

    }
    
//    @IBAction func datehelpCancelButton(_ sender: UIButton) {
//        self.blurView.alpha = 0
//        self.dateView.alpha = 0
//    }
//    
    @IBAction func numberOfTravellerhelpCancelButton(_ sender: UIButton) {
//        self.blurView.alpha = 0
//        self.numberOfTravellerView.alpha = 0
        let popupVC: PopUpViewController = PopUpViewController(nibName: "PopUpViewController", bundle: nil)
        popupVC.msg = "One can buy currencies only for their blood relative; also the person making the payment needs to be travelling."
        popupVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(popupVC, animated: false, completion: nil)

    }

 
    
//    @IBAction func purposehelpCancelButton(_ sender: UIButton) {
//        
//        self.blurView.alpha = 0
//        self.purposeView.alpha = 0
//    }
//    
    
    
    
    
}


