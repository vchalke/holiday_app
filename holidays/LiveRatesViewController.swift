//
//  LiveRatesViewController.swift
//  holidays
//
//  Created by Parshwanath on 09/12/17.
//  Copyright © 2017 Mobicule. All rights reserved.
//

import UIKit

class LiveRatesViewController: ForexBaseViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate
{
    //MARK: - Outlets
    @IBOutlet var liveRatesView: UIView!
    @IBOutlet weak var txtSearchBar: UITextField!
    @IBOutlet weak var liveRatesTableView: UITableView!
    
    var currencyRateDict : Dictionary<String,Any>?
    var searchedArray : [Any] = Array()
    var searchText : String = ""
    
    var arrayForRateCardsBuy : [NSDictionary] = [];
    var arrayForRateCardsSell : [NSDictionary] = [];
    
    var buyRatesArray:[Any]!
    var sellRatesArray:[Any]!
    var currInfoArray:[Any]!
    
    var previousController : String = ""
    
    //MARK: - Controller Methods
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.buyRatesArray = []
        self.sellRatesArray = []
        self.currInfoArray = []
        
        Bundle.main.loadNibNamed(LIVE_RATES_VC, owner: self, options: nil)
        super.addViewInBaseView(childView: self.liveRatesView)
        super.setUpHeaderLabel(labelHeaderNameText: "Live Rates")
        
        txtSearchBar.attributedPlaceholder = NSAttributedString(string: "Search your currency", attributes: convertToOptionalNSAttributedStringKeyDictionary([convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor): UIColor.white]))
        
        self.liveRatesTableView.register(UINib(nibName:"LiveRatesTableViewCell", bundle: nil), forCellReuseIdentifier: "LiveRatesTableViewCell")
        
        self.getCurrencyRateCardsForBuy()
        self.addDoneButtonOnKeyboard()
    }
    
    override func backButtonClicked(buttonView:UIButton)
    {
        if self.previousController == "SlideMenuViewController"
        {
            SlideNavigationController.sharedInstance().popToRootViewController(animated: true)
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillLayoutSubviews()
    {
        super.setSubViewFrame(childView: self.liveRatesView)
    }
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle       = UIBarStyle.default
        let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(LiveRatesViewController.doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.txtSearchBar.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction()
    {
        self.txtSearchBar.resignFirstResponder()
    }
    
    //MARK: - UITextFieldMethods
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if string.isEmpty
        {
            searchText = String(searchText.dropLast())
        }
        else
        {
            searchText=textField.text!+string
        }
        
        let predicate = NSPredicate(format: "SELF.currencyName CONTAINS[cd] %@", searchText)
        let arr = (self.currInfoArray as NSArray).filtered(using: predicate)
        
        if arr.count > 0
        {
            searchedArray.removeAll(keepingCapacity: true)
            searchedArray=arr
        }
        else
        {
            searchedArray=self.currInfoArray
        }
        liveRatesTableView.reloadData()
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    // MARK: TableView delegates
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return searchedArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "LiveRatesTableViewCell", for: indexPath) as? LiveRatesTableViewCell else
        {
            fatalError("The dequeued cell is not an instance of LiveRatesTableViewCell.")
        }
        
        DispatchQueue.main.async
        {
            let currInfo = self.searchedArray[indexPath.row] as! [String:Any]
            
            let buyRate = currInfo["buyRate"] as? Double
            let buyRateValue = (Double((buyRate?.truncate(places: 2))!))
            
            let sellRate = currInfo["sellRate"] as? Double
            let sellRateValue = (Double((sellRate?.truncate(places: 2))!))
            
            cell.labelCurrName?.text = (currInfo as AnyObject).value(forKeyPath: "currencyName") as? String
            cell.labelCurrCode?.text = (currInfo as AnyObject).value(forKeyPath: "currencyCode") as? String
            cell.labelBuyRate?.text = String(describing: buyRateValue)
            cell.labelSellRate?.text = String(describing: sellRateValue)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let headerView = (Bundle.main.loadNibNamed("HeaderViewLiveRates", owner: self, options: nil)?[0] as? UIView)
        return headerView;
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 45
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        let currDetailVC : CurrencyDetailsViewController = CurrencyDetailsViewController(nibName: "ForexBaseViewController", bundle: nil)
        
        let currInfoBuy = self.searchedArray[indexPath.row] as! [String:Any]
        currDetailVC.currencyCode = ((currInfoBuy as AnyObject).value(forKeyPath: "currencyCode") as? String)!
        currDetailVC.currencyName = ((currInfoBuy as AnyObject).value(forKeyPath: "currencyName") as? String)!
        self.navigationController?.pushViewController(currDetailVC, animated: true)
    }
    
    // MARK: FetchDataFromServer
    func getCurrencyRateCardsForBuy()
    {
        LoadingIndicatorView.show();
        
       
        
        let pathParameter  : NSString = "/tcForexRS/generic/roe/1/3"
        
        ForexCommunicationManager.sharedInstance.execTask(pathParam: pathParameter, queryParam: "", requestType: "get", jsonDict: NSDictionary.init())
        {
            (status, response) in
            
            DispatchQueue.main.async { () -> Void in
                LoadingIndicatorView.hide()
           
            
            if(response != nil)
            {
                let jsonArray : NSArray = response as! NSArray
                self.arrayForRateCardsBuy = NSMutableArray.init(array: jsonArray) as! [NSDictionary];
                
                self.getCurrencyRateCardsForSell()
                
                
              
                    self.liveRatesTableView.reloadData()
               
            }
            else
            {
                self.showAlert(message: "Some error has occurred")
            }
                 }
        }
    }
    
    func getCurrencyRateCardsForSell()
    {
        let pathParameter  : NSString = "/tcForexRS/generic/roe/2/3"
        
        ForexCommunicationManager.sharedInstance.execTask(pathParam: pathParameter, queryParam: "", requestType: "get", jsonDict: NSDictionary.init())
        {
            (status, response) in
            
            DispatchQueue.main.async { () -> Void in
                LoadingIndicatorView.hide()
           
            
            if(response != nil)
            {
                let jsonArray : NSArray = response as! NSArray
                self.arrayForRateCardsSell = NSMutableArray.init(array: jsonArray) as! [NSDictionary];
//                print("arrayForRateCardsSell---> \(self.arrayForRateCardsSell.count)")
//                print("arrayForRateCardsBuy---> \(self.arrayForRateCardsBuy.count)")
                
                for element in self.arrayForRateCardsBuy
                {
                    var dict = element as! [String:Any]
                    let currencyName = dict["currencyName"] as! String
                    let currencyCode = dict["currencyCode"] as! String
                    let buyRateValue = dict["roe"] as! Double
                    
                    var productDict:[String:Any] = [:]
                    productDict["currencyName"] = currencyName
                    productDict["currencyCode"] = currencyCode
                    productDict["buyRate"] = buyRateValue
                    
                    self.buyRatesArray.append(productDict)
                }
                
                for element in self.arrayForRateCardsSell
                {
                    var dict = element as! [String:Any]
                    let currencyName = dict["currencyName"] as! String
                    let sellRateValue = dict["roe"] as! Double
                    
                    var productDict:[String:Any] = [:]
                    productDict["currencyName"] = currencyName
                    productDict["sellRate"] = sellRateValue
                    
                    self.sellRatesArray.append(productDict)
                }
                
                for element in self.buyRatesArray
                {
                    var dict = element as! [String:Any]
                    let currencyName = dict["currencyName"] as! String
                    
                    for element in self.sellRatesArray
                    {
                        var duplicatedict = element as! [String:Any]
                        let duplicateCurrName = duplicatedict["currencyName"] as! String
                        if currencyName == duplicateCurrName
                        {
                            dict["sellRate"] = duplicatedict["sellRate"]
                            self.currInfoArray.append(dict)
                        }
                    }
                }
                
                self.searchedArray = self.currInfoArray
              
                DispatchQueue.main.async
                {
                    self.liveRatesTableView.reloadData()
                }
            }
            else
            {
                self.showAlert(message: "Some error has occurred")
            }
                 }
        }
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromNSAttributedStringKey(_ input: NSAttributedString.Key) -> String {
	return input.rawValue
}
