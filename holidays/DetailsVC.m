//
//  DetailsVC.m
//  holidays
//
//  Created by Pushpendra Singh on 19/08/15.
//  Copyright (c) 2015 Mobicule. All rights reserved.
//

#import "DetailsVC.h"
#import "HolidayExpandableCell.h"
#import "BookNowViewController.h"

#define NUM_TOP_ITEMS 20
#define NUM_SUBITEMS 5

@interface DetailsVC ()
{
    NSArray * accommodationTypeArray;
    
    NSArray * inclusionList;
    NSArray * exclusionList;
    NSString * currentAccommodationType;
    BOOL *isCHECK;
    int isChildFlag;
    int isCancelFlag;
    NSInteger selectedCell;
    NSIndexPath *cancelIndexPath;
}

@end

@implementation DetailsVC{
    
    int rowHeight;
    BOOL isChildClicked;
    int cellIndex,preCellIndex;
    NSArray *imageName;
    NSMutableArray * sightSeenArray ;
     NSArray * sightSeeingCityArray;
}


#pragma mark - Data generators

- (NSArray *)topLevelItems {
    
    NSMutableArray *items = [NSMutableArray array];
    
    [items addObject:@"Inclusions"];
    [items addObject:@"Exclusions"];
    //[items addObject:@"Things to Note"];
    return items;
}

- (NSArray *)subItemsFor:(NSString * ) topLevelTitle{
    
    NSMutableArray *items = [NSMutableArray array];
    
    
    
    if ([topLevelTitle isEqualToString:@"Inclusions"]) {
        
        [items addObjectsFromArray:inclusionList];
        
    }
    else  if ([topLevelTitle isEqualToString:@"Exclusions"]){
        
         [items addObjectsFromArray:exclusionList];
    }
    else  if ([topLevelTitle isEqualToString:@"Things to Note"]) {
        
        
    }
    
    
    return items;
}

- (NSArray *)subSubItems {
    NSMutableArray *items = [NSMutableArray array];
    //    int numItems = arc4random() % NUM_SUBITEMS + 2;
    //
    //    for (int i = 0; i < numItems; i++) {
    //        [items addObject:[NSString stringWithFormat:@"SubSubItem %d", i + 1]];
    //    }
    
    [items addObject:@"Flight Details"];
    [items addObject:@"Accommodation"];
    [items addObject:@"Meals"];
    [items addObject:@"Visa, Passport and Insurance"];
    [items addObject:@"Sightseeing"];
    
    
    return items;
}

#pragma mark - View management

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    currentAccommodationType = [[NSString alloc] init];
    
    [self InitAccommodationType];
    
    
    isCHECK=0;
    cellIndex = -1;
    preCellIndex = -1;
    rowHeight = 100;
    isChildFlag=0;
    selectedCell=-1;
    isCancelFlag=0;
    
    imageName=@[@"inclusion.png",@"exclusion.png",@"note.png"];
    
    topItems = [[NSArray alloc] initWithArray:[self topLevelItems]];
    subItems = [NSMutableArray new];
    
    currentExpandedIndex = -1;
    
    for (int i = 0; i < [topItems count]; i++) {
        
        [subItems addObject:[self subItemsFor:[topItems objectAtIndex:i]]];
    }
    
}

-(void)setAccomType:(NSString *)accomType
{
    if ([[accomType lowercaseString] isEqualToString:@"standard"])
    {
        self.holidayPackageDetail.stringSelectedAccomType = @"0";
    }
    else if ([[accomType lowercaseString] isEqualToString:@"delux"]||[[accomType lowercaseString] isEqualToString:@"deluxe"])
    {
        self.holidayPackageDetail.stringSelectedAccomType = @"1";
    }
    else
    {
        self.holidayPackageDetail.stringSelectedAccomType = @"2";
    }
}


-(void)InitAccommodationType
{
    sightSeenArray = [[NSMutableArray alloc] init];
    
    NSDictionary * dict = [self.completePackageDetail objectAtIndex:0];
    
    NSArray* accomArray = self.holidayPackageDetail.arrayAccomTypeList;
    
    if ([accomArray count] > 0) {
        
        accommodationTypeArray = [[NSArray alloc] initWithArray:accomArray];
        
        [self.AccommodationTypeBtn setTitle:[accomArray objectAtIndex:0] forState:UIControlStateNormal];
        
        self.holidayPackageDetail.stringSelectedAccomType = @"0";
        
        [self setAccomType:[accomArray objectAtIndex:0]];
        
    }
    
    NSDictionary *packageDetailDict = [dict valueForKey:@"packageDetail"];
    
   //isFlightDefaultMsg
    //flightDefaultMsg
    
    //hotelDefaultMsg
    //isHotelDefaultMsg
    
    //isMealsDefaultMsg
    //mealsDefaultMsg
    
    //isSightseeingDefaultMsg
    //sightseeingDefaultMsg
    
    //isTransferDefaultMsg
    //transferDefaultMsg
    
    //isVisaDefaultMsg
    //visaDefaultMsg
    
    
  //  Sightseeings
   // Accommodation
    //Meals
    //Flight Details
    //Visa, Passport & Insurance
    //Notes
    //Other
    
  //  NSArray* inclusionListArray = [dict objectForKey:@"inclusionList"];
   
    
//    if ([inclusionListArray count] > 0)
//    {
//        
//        
//        inclusionList = [[NSArray alloc] initWithArray:inclusionListArray];
//    }
    
    //included as per itinarary
    
    
    
      NSMutableArray *inclusionlistMutable = [[NSMutableArray alloc] init];
    
    if (self.holidayPackageDetail.mealsFlag == YES)
    {
        [inclusionlistMutable addObject:@"Meals"];
    }
    
    if (self.holidayPackageDetail.airFlag == YES)
    {
          [inclusionlistMutable addObject:@"Flight Details"];
    }
    
    if (self.holidayPackageDetail.tourMngerFlag == YES)
    {
        [inclusionlistMutable addObject:@"Visa, Passport & Insurance"];
    }
    
    if (self.holidayPackageDetail.accomFlag == YES)
    {
        [inclusionlistMutable addObject:@"Accommodation"];
    }
    
    if (self.holidayPackageDetail.sightSeeingFlag == YES)
    {
         [inclusionlistMutable addObject:@"Sightseeings"];
    }

    [inclusionlistMutable addObject:@"Tour Price includes"];
    
   
    inclusionList = [[NSArray alloc] initWithArray:inclusionlistMutable];
    
    
   NSArray * excludeList = [dict objectForKey:@"excludeList"];
    NSLog(@"%@", excludeList);
    
   NSString *string = [dict objectForKey:@"thingsNote"];
    NSLog(@"%@", string);
    
}



- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger rownumber=[topItems count] + ((currentExpandedIndex > -1) ? [[subItems objectAtIndex:currentExpandedIndex] count] : 0);
    
    return rownumber;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *ParentCellIdentifier = @"ParentCell";
    static NSString *ChildCellIdentifier = @"ChildCell";
    
    BOOL isChild =
    currentExpandedIndex > -1
    && indexPath.row > currentExpandedIndex
    && indexPath.row <= currentExpandedIndex + [[subItems objectAtIndex:currentExpandedIndex] count];
    
    HolidayExpandableCell *cell;
    
    if (isChild) {
        
        cell = (HolidayExpandableCell*)[tableView dequeueReusableCellWithIdentifier:ChildCellIdentifier];
        
        if (cell == nil)
        {
            NSArray *nib=[[NSBundle mainBundle]loadNibNamed:@"ChildCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
    }
    else
    {
        
        cell = (HolidayExpandableCell*)[tableView dequeueReusableCellWithIdentifier:ParentCellIdentifier];
        
        if (cell == nil) {
            
            NSArray *nib=[[NSBundle mainBundle]loadNibNamed:@"HolidayExpandableCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
            
        }
        
    }
    
    if (isChild) {
        
        NSString * string =  [[subItems objectAtIndex:currentExpandedIndex] objectAtIndex:indexPath.row - currentExpandedIndex - 1];
        cell.lblChildTitle.text=[NSString stringWithFormat:@"          %@", [[subItems objectAtIndex:currentExpandedIndex] objectAtIndex:indexPath.row - currentExpandedIndex - 1]];
        //   cell.lblChildTitle.text = [[subItems objectAtIndex:currentExpandedIndex] objectAtIndex:indexPath.row - currentExpandedIndex - 1];
        //
        
        cell.webView.frame=CGRectMake(0, 0, cell.webView.frame.size.width, cell.webView.frame.size.height);
        cell.imgChildIndicator.image=[UIImage imageNamed:@"plus.png"];
        
        
        cell.webView.delegate = self;
        cell.webView.scalesPageToFit  = NO;
        cell.webView.clipsToBounds=YES;
        cell.webView.scrollView.scrollEnabled = YES;
        cell.webView.scrollView.delegate = self;
        cell.webView.scrollView.bounces = NO;
        [cell.webView setClipsToBounds:YES];
        
        
        cell.lblChildTitle.frame=CGRectMake(50, cell.lblChildTitle.frame.origin.y, cell.lblChildTitle.frame.size.width, cell.lblChildTitle.frame.size.height);
        if ([string caseInsensitiveCompare:@"Visa, Passport & Insurance"] ==  NSOrderedSame) {
            
            cell.VizaView.hidden = YES;
            [cell.vizaButton  addTarget:self action:@selector(loadVizaDataIntoHTML:) forControlEvents:UIControlEventTouchUpInside];
            [cell.passportButton addTarget:self action:@selector(loadPassportDataIntoHTML:) forControlEvents:UIControlEventTouchUpInside];
            [cell.insuranceButton addTarget:self action:@selector(loadInsuranceDataIntoHTML:) forControlEvents:UIControlEventTouchUpInside];
//            UIImage *imgUnChecked=[UIImage imageNamed:@"unselectedCheckbox.png"];
//            [cell.passportButton setImage:imgUnChecked forState:UIControlStateNormal];
//            [cell.vizaButton setImage:imgUnChecked forState:UIControlStateNormal];
//            [cell.insuranceButton setImage:imgUnChecked forState:UIControlStateNormal];
            
        }
        else
        {
            cell.VizaView.hidden = YES;
        }
        cell.lblChildTitle.font=[UIFont fontWithName:TITILLUM_REGULAR size:13];
        //  cell.contentView.backgroundColor=[UIColor lightGrayColor];
        cell.contentView.layer.borderWidth = 0.6;
        cell.contentView.layer.borderColor=[UIColor lightGrayColor].CGColor;
    }
    else
    {
        
        int topIndex = (currentExpandedIndex > -1 && indexPath.row > currentExpandedIndex)
        ? (int)indexPath.row - (int)[[subItems objectAtIndex:currentExpandedIndex] count]
        : (int)indexPath.row;
        
        cell.topWebView.delegate = self;
        
        cell.topWebView.scrollView.delegate = self;
        cell.topWebView.scrollView.scrollEnabled = NO;
        cell.topWebView.scrollView.bounces = NO;
        
        cell.topWebView.scalesPageToFit  = NO;
        
        cell.lblTitle.text = [topItems objectAtIndex:topIndex];
        
        //cell.detailTextLabel.text = @"";
        //cell.backgroundColor=[UIColor darkGrayColor];
        
        cell.imgIcon.image=[UIImage imageNamed:[imageName objectAtIndex:topIndex]];
        cell.contentView.layer.borderWidth=0.6;
        cell.contentView.layer.borderColor=[UIColor whiteColor].CGColor;
    }
    
    
    
    return cell;
}


-(void)loadVizaDataIntoHTML:(UIButton * )sender
{
    
    CGPoint center= sender.center;
    
    CGPoint rootViewPoint = [sender.superview convertPoint:center toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:rootViewPoint];
    NSLog(@"%ld",(long)indexPath.row);
    
    HolidayExpandableCell *cell= (HolidayExpandableCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    
  //  UIImage *imgChecked=[UIImage imageNamed:@"selectedCheckbox.png"];
  //  UIImage *imgUnChecked=[UIImage imageNamed:@"unselectedCheckbox.png"];
    
   // [cell.vizaButton setImage:imgChecked forState:UIControlStateNormal];
   // [cell.passportButton setImage:imgUnChecked forState:UIControlStateNormal];
   // [cell.insuranceButton setImage:imgUnChecked forState:UIControlStateNormal];
    
    NSDictionary * dict = [self.completePackageDetail objectAtIndex:0];
    
    
    NSDictionary *packageDetailDict = [dict valueForKey:@"packageDetail"];
    
    NSArray * accomdationList = [packageDetailDict objectForKey:@"tcilHolidayVisaCollection"];
    
    NSPredicate * predicate1 =  [NSPredicate predicateWithFormat:@"(packageClassId == %@) ", self.holidayPackageDetail.stringSelectedAccomType ];
    
    NSArray *filteredArray = [accomdationList filteredArrayUsingPredicate:predicate1];
    
    
    NSDictionary *insuranceDict = [filteredArray firstObject];
    
    
    [cell.webView setHidden:NO];
    
    NSString * string = [insuranceDict objectForKey:@"visa"];
    
    if (string ==  nil) {
        
        [cell.webView loadHTMLString:[NSString stringWithFormat:@"<html><body><font face=\"Titillium-regular\" color=\"black\" size=\"2\"><h5 style= \"margin:0px\"> No Data Found </h5></body></html>"]  baseURL:nil];
        
    }
    else
    {
        [cell.webView loadHTMLString:string  baseURL:nil];
    }
    
    
   // [cell.webView loadHTMLString:[dict objectForKey:@"visa"]  baseURL:nil];
    
    
}
-(void)loadPassportDataIntoHTML:(UIButton *)sender
{
    
    
    CGPoint center= sender.center;
    
    CGPoint rootViewPoint = [sender.superview convertPoint:center toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:rootViewPoint];
    NSLog(@"%ld",(long)indexPath.row);
    
    HolidayExpandableCell *cell= (HolidayExpandableCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    
//    UIImage *imgChecked=[UIImage imageNamed:@"selectedCheckbox.png"];
//    UIImage *imgUnChecked=[UIImage imageNamed:@"unselectedCheckbox.png"];
//    
//    [cell.vizaButton setImage:imgUnChecked forState:UIControlStateNormal];
//    [cell.passportButton setImage:imgChecked forState:UIControlStateNormal];
//    [cell.insuranceButton setImage:imgUnChecked forState:UIControlStateNormal];
    
    
    
    NSDictionary * dict = [self.completePackageDetail objectAtIndex:0];
    
    [cell.webView setHidden:NO];
    
    NSString * string = [dict objectForKey:@"passport"];
    
    if (string ==  nil) {
        
        [cell.webView loadHTMLString:[NSString stringWithFormat:@"<html><body><font face=\"Titillium-regular\" color=\"black\" size=\"2\"><h5 style= \"margin:0px\"> No Data Found </h5></body></html>"]  baseURL:nil];
        
    }
    else
    {
        [cell.webView loadHTMLString:[dict objectForKey:@"passport"]  baseURL:nil];
    }
    
    
    
}

-(void)loadInsuranceDataIntoHTML:(UIButton *)sender
{
    
    CGPoint center= sender.center;
    
    CGPoint rootViewPoint = [sender.superview convertPoint:center toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:rootViewPoint];
    NSLog(@"%ld",(long)indexPath.row);
    
    HolidayExpandableCell *cell= (HolidayExpandableCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    
    //UIImage *imgUnChecked=[UIImage imageNamed:@"unselectedCheckbox.png"];
    
//    UIImage *imgChecked=[UIImage imageNamed:@"selectedCheckbox.png"];
//    UIImage *imgUnChecked=[UIImage imageNamed:@"unselectedCheckbox.png"];
//    
//    [cell.vizaButton setImage:imgUnChecked forState:UIControlStateNormal];
//    [cell.passportButton setImage:imgUnChecked forState:UIControlStateNormal];
//    [cell.insuranceButton setImage:imgChecked forState:UIControlStateNormal];
    
    
    NSDictionary * dict = [self.completePackageDetail objectAtIndex:0];
    
    
    NSDictionary *packageDetailDict = [dict valueForKey:@"packageDetail"];
    
    NSArray * accomdationList = [packageDetailDict objectForKey:@"tcilHolidayVisaCollection"];
    
    NSPredicate * predicate1 =  [NSPredicate predicateWithFormat:@"(packageClassId == %@) ", self.holidayPackageDetail.stringSelectedAccomType ];
    
    NSArray *filteredArray = [accomdationList filteredArrayUsingPredicate:predicate1];
    
    
    NSDictionary *insuranceDict = [filteredArray firstObject];
    

    
    [cell.webView setHidden:NO];
    
    NSString * string = [insuranceDict objectForKey:@"insurance"];
    
    if (string ==  nil) {
        
        [cell.webView loadHTMLString:[NSString stringWithFormat:@"<html><body><font face=\"Titillium-regular\" color=\"black\" size=\"2\"><h5 style= \"margin:0px\"> No Data Found </h5></body></html>"]  baseURL:nil];
        
    }
    else
    {
        [cell.webView loadHTMLString:string  baseURL:nil];
    }
    
    
    
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    scrollView.scrollEnabled=YES;
    
    //    if(scrollView == self.tableView)
    //    {
    //
    //    }
    //    else
    //    {
    //
    //
    //            CGPoint currentContentOffset = CGPointMake(0, scrollView.contentOffset.y);
    //
    //            [self.tableView setContentOffset:currentContentOffset animated:NO];
    //
    //
    //    }
    
    //
    //    if (scrollView.contentOffset.y > 0  ||  scrollView.contentOffset.y < 0 )
    //        scrollView.contentOffset = CGPointMake(scrollView.contentOffset.x, 0);
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([indexPath row] == cellIndex) {
        if (isCancelFlag==1) {
            isCancelFlag=0;
            return 30;
        }
        return  (rowHeight);
    }
    if (isChildFlag) {
        
        return 30;
    }
    
    return 30;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    
    BOOL isChild =
    currentExpandedIndex > -1
    && indexPath.row > currentExpandedIndex
    && indexPath.row <= currentExpandedIndex + [[subItems objectAtIndex:currentExpandedIndex] count];
    
    cellIndex=-1;
    NSLog(@"index path %d",indexPath.row);
    
    if (isChild && isChildFlag==1) {
        NSMutableArray *indexPaths = nil;
        
        [self.tableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
        // ;
        [self updateChildCellIndicator:(int)indexPath.row imageName:@"plus.png" WithChildType:nil];
        isChildFlag=0;
        HolidayExpandableCell *cell= (HolidayExpandableCell *)[self.tableView cellForRowAtIndexPath:indexPath];
        cell.lblChildTitle.textColor=[UIColor darkGrayColor];
        if (indexPath.row==selectedCell) {
            return;
        }
        
    }
    
    if (isChild && isChildFlag==0)
    {
        
        NSLog(@"A child was tapped, do what you will with it");
        isChildFlag=1;
        isChildClicked=YES;
        selectedCell=indexPath.row;
        
        cancelIndexPath=indexPath;
        rowHeight = 100;
        NSString * string =  [[subItems objectAtIndex:currentExpandedIndex] objectAtIndex:indexPath.row - currentExpandedIndex - 1];
        
        
        HolidayExpandableCell *cell;
        
        [self updateChildCellIndicator:preCellIndex imageName:@"plus.png" WithChildType:string];
        
        cellIndex = (int)indexPath.row;
        //        NSIndexPath *path = [NSIndexPath indexPathWithIndex:preCellIndex];
        //        cell= (ExpandableCell *)[self.tableView cellForRowAtIndexPath:path];
        //        cell.lblChildTitle.textColor=[UIColor darkGrayColor];
        
        [self updateChildCellIndicator:cellIndex imageName:@"minus.png" WithChildType:string];
        
        preCellIndex=cellIndex;
        cell= (HolidayExpandableCell *)[self.tableView cellForRowAtIndexPath:indexPath];
        cell.lblChildTitle.textColor=[UIColor blueColor];
        [self.tableView beginUpdates];
        //    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        [self.tableView endUpdates];
        return;
    }
    isChildFlag=0;
    isChildClicked=NO;
    
    [self.tableView beginUpdates];
    
    if (currentExpandedIndex == indexPath.row) {
        
        [self collapseSubItemsAtIndex:currentExpandedIndex];
        [self updateCellIndicator:currentExpandedIndex imageName:@"down_arrow.png"];
        currentExpandedIndex = -1;
        
    }
    else {
        
        BOOL shouldCollapse = currentExpandedIndex > -1;
        
        if (shouldCollapse) {
            
            [self collapseSubItemsAtIndex:currentExpandedIndex];
            
            [self updateCellIndicator:currentExpandedIndex imageName:@"down_arrow.png"];
        }
        
        [self updateCellIndicator:currentExpandedIndex imageName:@"down_arrow.png"];
        
        
        
        currentExpandedIndex = (shouldCollapse && indexPath.row > currentExpandedIndex) ? (int)indexPath.row - (int)[[subItems objectAtIndex:currentExpandedIndex] count] : (int)indexPath.row;
        
        
        
        [self expandItemAtIndex:currentExpandedIndex WithType:[topItems objectAtIndex:currentExpandedIndex]];
        
        [self updateCellIndicator:currentExpandedIndex imageName:@"up_arrow.png"];
    }
    
    [self.tableView endUpdates];
    
}

- (void)expandItemAtIndex:(int)index WithType:(NSString *)type {
    
    // Inclusions
    // Exclusions
    // Things to Note
    
    NSMutableArray *indexPaths = [NSMutableArray new];
    NSArray *currentSubItems = [subItems objectAtIndex:index];
    
    NSDictionary * dict = [self.completePackageDetail objectAtIndex:0];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
    
    HolidayExpandableCell *cell= (HolidayExpandableCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    
    //ParentCell
    //ChildCell
    //ExpandableCell *cell= (ExpandableCell *)[self.tableView dequeueReusableCellWithIdentifier:@"ChildCell" forIndexPath:indexPath];
    
    if ([type caseInsensitiveCompare:@"Exclusions"] ==  NSOrderedSame)
    {
        if([_holidayPackageDetail.strPackageType caseInsensitiveCompare:@"International"]==NSOrderedSame)
        {
          [FIRAnalytics logEventWithName:@"International_Holidays_Details_Exclusion_PDP" parameters:@{kFIRParameterItemName:[NSString stringWithFormat:@"PackageName :%@, PackageId : %@, PackageType :%@",_holidayPackageDetail.strPackageName,_holidayPackageDetail.strPackageId,_holidayPackageDetail.strPackageType]}];
        }
        else
        {
            [FIRAnalytics logEventWithName:@"Indian_Holidays_Details_Exclusion_PDP" parameters:@{kFIRParameterItemName:[NSString stringWithFormat:@"PackageName :%@, PackageId : %@, PackageType :%@",_holidayPackageDetail.strPackageName,_holidayPackageDetail.strPackageId,_holidayPackageDetail.strPackageType]}];

        }
        cellIndex = index;
        preCellIndex = cellIndex;
        NSMutableString * htmlString=[NSMutableString new] ;
        
        NSDictionary *packageDetail = [dict valueForKey:@"packageDetail"];
        
        NSArray * excludeList = [packageDetail objectForKey:@"tcilHolidayIncludeExcludeCollection"];

        
//        NSPredicate * predicate1 ;
//        if ([_AccommodationTypeBtn.titleLabel.text isEqualToString:@"Deluxe"])
//        {
//           predicate1 =  [NSPredicate predicateWithFormat:@"(accomType == %@) or (accomType == %@) ", self.AccommodationTypeBtn.titleLabel.text,@"Delux"];
//        }
//        else
//        {
//             predicate1 =  [NSPredicate predicateWithFormat:@"(accomType == %@) ", self.AccommodationTypeBtn.titleLabel.text ];
//        }
        
       
        
      //  NSArray *filteredArray = [excludeList filteredArrayUsingPredicate:predicate1];
        
        NSArray *filteredArray = excludeList;
        
        //self.holidayPackageDetail.stringSelectedAccomType
        
        for (NSDictionary * mealDict  in filteredArray)
        {
            NSInteger accomTypeID = [[mealDict valueForKey:@"packageClassId"] integerValue];
            
            if([self.holidayPackageDetail.stringSelectedAccomType integerValue] == accomTypeID)
            {
                 [htmlString appendString:[mealDict objectForKey:@"excludes"]];
            }
        }
        
        if ([htmlString isEqualToString:@""])
        {
            
            [cell.topWebView setHidden:NO];
            
            [cell.topWebView loadHTMLString:[NSString stringWithFormat:@"<html><body><font face=\"Titillium-regular\" color=\"black\" size=\"2\"><h5 style= \"margin:0px\"> No Data Found </h5></body></html>"]  baseURL:nil];
        }
        else
        {
            UIScrollView *superScrollView = (UIScrollView *)self.view.superview;
            superScrollView.scrollEnabled = NO;
            
            [cell.topWebView setHidden:NO];
            
          
           
          NSString *htmlText =    [NSString stringWithFormat:@"<font face='Titillium-regular' size='3'>%@", htmlString];
            
         [cell.topWebView loadHTMLString:htmlText baseURL:nil];

            
            
            //[cell.topWebView loadHTMLString:htmlString  baseURL:nil];
           // [cell.topWebView sizeToFit];
            //[cell.topWebView setScalesPageToFit:YES];
            
        }
        
    }
    else if ([type caseInsensitiveCompare:@"Things to Note"] ==  NSOrderedSame) {
        
        if ([_holidayPackageDetail.strPackageType caseInsensitiveCompare:@"international"]==NSOrderedSame)
        {
            [FIRAnalytics logEventWithName:@"International_Holidays_Details_Things_to_Note_PDP" parameters:@{kFIRParameterItemName:[NSString stringWithFormat:@"PackageName :%@,PackageId :%@",_holidayPackageDetail.strPackageName,_holidayPackageDetail.strPackageId]}];
        }
        else
        {
            [FIRAnalytics logEventWithName:@"Indian_Holidays_Details_Things_to_Note_PDP" parameters:@{kFIRParameterItemName:[NSString stringWithFormat:@"PackageName :%@,PackageId :%@",_holidayPackageDetail.strPackageName,_holidayPackageDetail.strPackageId]}];
        }
       
        cellIndex = index;
        
        
        preCellIndex = cellIndex;
        
        NSString * string = [dict objectForKey:@"thingsNote"];
        
        if ([string isEqualToString:@""]) {
            
            [cell.topWebView setHidden:NO];
            
            [cell.topWebView loadHTMLString:[NSString stringWithFormat:@"<html><body><font face=\"Titillium-regular\" color=\"black\" size=\"2\"><h5 style= \"margin:0px\"> No Data Found </h5></body></html>"]  baseURL:nil];
        }
        else
        {
            
            [cell.topWebView setHidden:NO];
            
            [cell.topWebView loadHTMLString:string  baseURL:nil];
            
        }
        
        
    }
    else
    {
        if([_holidayPackageDetail.strPackageType caseInsensitiveCompare:@"international"]==NSOrderedSame)
        {
            [FIRAnalytics logEventWithName:@"International_Holidays_Details_Exclusion_PDP" parameters:@{kFIRParameterItemName:[NSString stringWithFormat:@"PackageName :%@, PackageType :%@, PackageID :%@",_holidayPackageDetail.strPackageName,_holidayPackageDetail.strPackageType,_holidayPackageDetail.strPackageId]}];
        }
        else
        {
           [FIRAnalytics logEventWithName:@"Indian_Holidays_Details_Exclusion_PDP" parameters:@{kFIRParameterItemName:[NSString stringWithFormat:@"PackageName :%@, PackageType :%@, PackageID :%@",_holidayPackageDetail.strPackageName,_holidayPackageDetail.strPackageType,_holidayPackageDetail.strPackageId]}];        }
        
        int insertPos = index + 1;
        
        for (int i = 0; i < [currentSubItems count]; i++)
        {
            [indexPaths addObject:[NSIndexPath indexPathForRow:insertPos++ inSection:0]];
        }
        [self.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
        // [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
        
    }
    
    
}

- (void)collapseSubItemsAtIndex:(int)index {
    
    //0- inclusions
    //1- exclusions
    //2- Things to note
    
    NSMutableArray *indexPaths = [NSMutableArray new];
    
    for (int i = index + 1; i <= index + [[subItems objectAtIndex:index] count]; i++) {
        
        [indexPaths addObject:[NSIndexPath indexPathForRow:i inSection:0]];
    }
    
    [self.tableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
    //rowHeight=0;
}
//---

- (void)childcollapseSubItemsAtIndex:(int)index {
    
    NSMutableArray *indexPaths = [NSMutableArray new];
    
    for (int i = index + 1; i <= index + 0; i++) {
        
        [indexPaths addObject:[NSIndexPath indexPathForRow:i inSection:0]];
    }
    
    [self.tableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
    
    
}


#pragma mark- update indicator image
-(void)updateCellIndicator:(int)row imageName:(NSString*)imageString{
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
    
    HolidayExpandableCell *cell= (HolidayExpandableCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    cell.imgIndicator.image=[UIImage imageNamed:imageString];
}

-(void)updateChildCellIndicator:(int)row imageName:(NSString*)imageString WithChildType:(NSString *)childType{
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
    
    HolidayExpandableCell *cell= (HolidayExpandableCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    cell.imgChildIndicator.image=[UIImage imageNamed:imageString];
    cell.imgChildIndicator.contentMode = UIViewContentModeScaleAspectFit;
    //   cell.lblChildTitle.textColor=[UIColor blueColor];
    if ([imageString isEqualToString:@"plus.png"])
    {
        
        cell.VizaView.hidden = YES;
       
        
        //  cell.contentView.backgroundColor=[UIColor lightGrayColor];
    }
    else
    {
        // cell.contentView.backgroundColor=[UIColor whiteColor];
        
        NSDictionary * dict = [self.completePackageDetail objectAtIndex:0];
        
        NSDictionary *packageDetailDict = [dict valueForKey:@"packageDetail"];
        
        if ([childType isEqualToString:@"Sightseeings"])
        {
            [cell.webView setHidden:YES];
            cell.VizaView.hidden = YES;
            cell.constraintVisaView.constant = 0;
            
            NSMutableString * htmlString = [NSMutableString new] ;
            //NSArray * mealArray = [dict objectForKey:@"sightSeenList"];
            
            NSString *sightSeeingHTML ;
            
            if (_holidayPackageDetail.sightSeeingFlag == YES && _holidayPackageDetail.sightseenCollection.count > 0) {
                
                NSDictionary *datadict = [_holidayPackageDetail.sightseenCollection firstObject];
                if ([[[datadict valueForKey:@"isTypeDefaultMsg"] lowercaseString] isEqualToString:@"n"]) {
                    NSDictionary * dict = [self.completePackageDetail objectAtIndex:0];
                                                      
                                                      NSDictionary *packageDetailDict = [dict valueForKey:@"packageDetail"];
                                                      
                                                      NSArray * accomdationList = [packageDetailDict objectForKey:@"tcilHolidaySightseeingCollection"];//tcilHolidayVisaCollection
                                                      
                                                      NSPredicate * predicate1 =  [NSPredicate predicateWithFormat:@"(packageClassId == %d) ", [self.holidayPackageDetail.stringSelectedAccomType intValue] ];
                                                      
                                                      NSArray *filteredArray = [accomdationList filteredArrayUsingPredicate:predicate1];
                                                      
                                                      if(filteredArray.count>0)
                                                      {
                                                          for (int i = 0 ; i<filteredArray.count; i++)
                                                          {
                                                              NSDictionary *insuranceDict = [filteredArray objectAtIndex:i];
                                                              
                                                              NSDictionary *mealDict = [insuranceDict valueForKey:@"sightseeingId"];
                                                              
                                                              NSDictionary *cityCodeDict = [mealDict valueForKey:@"cityCode"];
                                                              
                                                              NSString *cityName = [cityCodeDict valueForKey:@"cityName"];
                                                              
                                                              NSString *name  = [mealDict valueForKey:@"name"];
                                                              
                                                              NSString *desscription  = [mealDict valueForKey:@"description"];
                                                              
                                                              NSString *sightSeeingHTMLLocal = [NSString stringWithFormat:@"<p>%@  :%@</p><p>%@</p>",cityName,name,desscription];
                                                              
                                                              [htmlString appendString:sightSeeingHTMLLocal];
                                                          }
                                                      }
                }else{
                    sightSeeingHTML = [datadict valueForKey:@"typeDefaultMsg"];
                    [htmlString appendString:sightSeeingHTML];
                }
                
               
            }else{
                if ([[[packageDetailDict valueForKey:@"isSightseeingDefaultMsg"] lowercaseString] isEqualToString:@"y"])
                {
                    sightSeeingHTML = [packageDetailDict valueForKey:@"sightseeingDefaultMsg"];
                    [htmlString appendString:sightSeeingHTML];
                }
                else
                {
                    NSDictionary * dict = [self.completePackageDetail objectAtIndex:0];
                    
                    NSDictionary *packageDetailDict = [dict valueForKey:@"packageDetail"];
                    
                    NSArray * accomdationList = [packageDetailDict objectForKey:@"tcilHolidaySightseeingCollection"];//tcilHolidayVisaCollection
                    
                    NSPredicate * predicate1 =  [NSPredicate predicateWithFormat:@"(packageClassId == %d) ", [self.holidayPackageDetail.stringSelectedAccomType intValue] ];
                    
                    NSArray *filteredArray = [accomdationList filteredArrayUsingPredicate:predicate1];
                    
                    if(filteredArray.count>0)
                    {
                        for (int i = 0 ; i<filteredArray.count; i++)
                        {
                            NSDictionary *insuranceDict = [filteredArray objectAtIndex:i];
                            
                            NSDictionary *mealDict = [insuranceDict valueForKey:@"sightseeingId"];
                            
                            NSDictionary *cityCodeDict = [mealDict valueForKey:@"cityCode"];
                            
                            NSString *cityName = [cityCodeDict valueForKey:@"cityName"];
                            
                            NSString *name  = [mealDict valueForKey:@"name"];
                            
                            NSString *desscription  = [mealDict valueForKey:@"description"];
                            
                            NSString *sightSeeingHTMLLocal = [NSString stringWithFormat:@"<p>%@  :%@</p><p>%@</p>",cityName,name,desscription];
                            
                            [htmlString appendString:sightSeeingHTMLLocal];
                        }
                    }

                    //sightSeeingHTML = @"";
                }
            }
            
            

           

            
            
          /*  NSPredicate * predicate1 =  [NSPredicate predicateWithFormat:@"(accomType == %@) ", self.AccommodationTypeBtn.titleLabel.text ];
            
            NSArray *filteredArray = [mealArray filteredArrayUsingPredicate:predicate1];
            
            for (NSDictionary * dict  in filteredArray)
            {
                NSString *cityName = [NSString stringWithFormat:@"%@",[dict objectForKey:@"city"]];
                
                NSString *SightSeen = [NSString stringWithFormat:@"%@" ,[dict objectForKey:@"sightSeeingName"]];
                
                SightSeen = [NSString stringWithFormat:@"<html><body><font face=\"Titillium-Semibold\" color=\"gray\" size=\"2\">%@</font><br></body></html>",[dict objectForKey:@"sightSeeingName"]];
                
                cityName = [NSString stringWithFormat:@"<html><body><font face=\"Titillium-Semibold\" color=\"black\" size=\"2.5\">%@</font><br></body></html>",[dict objectForKey:@"city"]];
               
                [htmlString appendString:cityName];
                [htmlString appendString:SightSeen];
                
                [htmlString appendString:[dict objectForKey:@"description"]];
                
            }*/
            
            
            //[htmlString appendString:@"<html><body><br></body></html>"];

            
            if ([htmlString isEqualToString:@""])
            {
                
                [cell.webView setHidden:NO];
                
                [cell.webView loadHTMLString:[NSString stringWithFormat:@"<html><body><font face=\"Titillium-regular\" color=\"black\" size=\"2\"><h5 style= \"margin:0px\"> As per itinerary </h5></body></html>"]  baseURL:nil];
            }
            else
            {
                
                NSString *htmlText =    [NSString stringWithFormat:@"<font face='Titillium-regular' size='3'>%@", htmlString];

                [cell.webView setHidden:NO];
                
                [cell.webView loadHTMLString:htmlText  baseURL:nil];
                
            }
            
           
     
            if (sightSeenArray.count <=  0 )
            {
           
                if ([self.AccommodationTypeBtn.titleLabel.text caseInsensitiveCompare:@"Standard"] ==  NSOrderedSame)
                {
                    
                    sightSeeingCityArray = [dict objectForKey:@"standardSightSeeingCityList"];
                    
                }
                else if ([self.AccommodationTypeBtn.titleLabel.text caseInsensitiveCompare:@"Deluxe"] ==  NSOrderedSame)
                {
                    sightSeeingCityArray = [dict objectForKey:@"deluxSightSeeingCityList" ];
                    
                    
                }
                else if ([self.AccommodationTypeBtn.titleLabel.text caseInsensitiveCompare:@"Premium"] ==  NSOrderedSame)
                {
                
                    sightSeeingCityArray = [dict objectForKey:@"premiumSightSeeingCityList" ];
                    
                }
           
            }

                
/*                if (sightSeeingCityArray > 0 )
               {
                    
                    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"Select SightSeeing City :" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:nil,
                                        nil];
                    
                    
                   for (int index = 0; index < sightSeeingCityArray.count; index++)
                   {
                        
                    [popup addButtonWithTitle:[sightSeeingCityArray objectAtIndex:index]];
           
                   }
                    popup.tag = 2;
                   [popup showInView:[UIApplication sharedApplication].keyWindow];
                    
               }
                
              
                
            }
        
            else
            {
                
                NSMutableString * htmlString=[NSMutableString new] ;
                NSDictionary * mealDict;
                NSString *cityName=[[sightSeenArray objectAtIndex:0]objectForKey:@"city"];
                
                NSString *city=[NSString stringWithFormat:@"<html><body><font face=\"Titillium-Semibold\" color=\"gray\" size=\"3\">%@</font><br></body></html>",cityName];
                [htmlString appendString:city];
                
                for (mealDict  in sightSeenArray) {
                    NSString *SightSeen=[mealDict objectForKey:@"sightSeeingName"];
                    
                    SightSeen=[NSString stringWithFormat:@"<html><body><font face=\"Titillium-Semibold\" color=\"gray\" size=\"2\">%@</font><br></body></html>",[mealDict objectForKey:@"sightSeeingName"]];
                    // htmlString=[NSString stringWithFormat:@"%@%@",SightSeen,[mealDict objectForKey:@"description"]];
                    [htmlString appendString:SightSeen];
                    [htmlString appendString:[mealDict objectForKey:@"description"]];
                    
                }
                [htmlString appendString:@"<html><body><br></body></html>"];
                
                
                if ([htmlString isEqualToString:@""]) {
                    
                    [cell.webView setHidden:NO];
                    
                    [cell.webView loadHTMLString:[NSString stringWithFormat:@"<html><body> <h5 style= \"margin:0px\"> No Data Found </h5></body></html>"]  baseURL:nil];
            }
            else
            {
                    
                [cell.webView setHidden:NO];
                    
                [cell.webView loadHTMLString:htmlString  baseURL:nil];
                    
            }
                

           }
          }*/
        }
        
        else if ([childType isEqualToString:@"Accommodation"] )
        {
            [cell.webView setHidden:YES];
            cell.constraintVisaView.constant = 0;
            cell.VizaView.hidden = YES;
            
            NSMutableString * htmlString=[NSMutableString new] ;
            
            NSString *sightSeeingHTML = [packageDetailDict valueForKey:@"hotelDefaultMsg"];

            if (_holidayPackageDetail.accomFlag == YES && _holidayPackageDetail.accombdationCollection.count > 0) {
                
                //   sightSeeingHTML = @"";
                          //  [htmlString appendString:sightSeeingHTML];
                NSDictionary *datadict = [_holidayPackageDetail.accombdationCollection firstObject];
                
                if ([[[datadict valueForKey:@"isTypeDefaultMsg"] lowercaseString] isEqualToString:@"n"]) {
                    NSDictionary * dict = [self.completePackageDetail objectAtIndex:0];
                                    
                            NSDictionary *packageDetailDict = [dict valueForKey:@"packageDetail"];

                                
                               NSArray * accomdationList = [packageDetailDict objectForKey:@"tcilHolidayAccomodationCollection"];
                                
                                NSPredicate * predicate1 =  [NSPredicate predicateWithFormat:@"(packageClassId == %@) ", self.holidayPackageDetail.stringSelectedAccomType ];
                                
                                NSArray *filteredArray = [accomdationList filteredArrayUsingPredicate:predicate1];
                                
                                for (NSDictionary * accomDict  in filteredArray)
                                {
                                    NSMutableString *stringOfImages = [NSMutableString new] ;
                                    
                                    NSDictionary *hotelIDDict =  [accomDict valueForKey:@"accomodationHotelId"];
                                    
                                    
                                    //NSString *starRatingString = [[hotelIDDict valueForKey:@"starRating"] intValue];
                                    
                                    NSString *totalStar = @"5";
                                    NSString *selectedStar =[NSString stringWithFormat:@"%d", [[hotelIDDict valueForKey:@"starRating"] intValue]];
                                    //totalStar = [[hotelIDDict valueForKey:@"starRating"] string];
                                    
                                    
                    //                NSString *totalStar;
                    //                NSString *selectedStar;
                    //
                    //                if (![starRatingString isEqualToString:@""])
                    //                {
                    //                    NSArray *ratingArray = [starRatingString componentsSeparatedByString:@"/"];
                    //                    if (ratingArray.count == 2 )
                    //                    {
                    //                        selectedStar = ratingArray[0];
                    //                        totalStar = ratingArray[1];
                    //                    }
                    //                }
                    //                else
                    //                {
                    //                    totalStar = @"";
                    //                }
                                    
                                    for (int i = 0; i<[totalStar intValue]; i++)
                                    {
                                        NSString *imageNameURL;
                                        if (i < [selectedStar intValue])
                                        {
                                             imageNameURL = [[NSBundle mainBundle] pathForResource:@"rating_selected" ofType:@"png"];
                                        }else
                                        {
                                             imageNameURL = [[NSBundle mainBundle] pathForResource:@"rating_unselected" ofType:@"png"];
                                        }
                                        
                                        NSURL *urlImage = [NSURL fileURLWithPath:imageNameURL];
                                        [stringOfImages appendString:[NSString stringWithFormat:@"<img src=\"%@\" style=\"padding-left:2%%\" width=\"5%%\">",urlImage]];
                                    }
                                    [stringOfImages appendString:@"</br>"];
                                    [htmlString appendString:[NSString stringWithFormat:@"<html><head></head><body><font face=\"Titillium-Regular\" color=\"gray\" size=\"4\">&nbsp&nbsp&nbsp%@</font><br><font face=\"Titillium-Regular\" color=\"gray\" size=\"2\">&nbsp&nbsp&nbsp&nbsp%@ Nights <br>&nbsp&nbsp&nbsp&nbsp%@</br>%@</body></html></br>" ,[hotelIDDict objectForKey:@"hotelName"] ,[NSString stringWithFormat:@"%d",[[accomDict objectForKey:@"noOfNights"] intValue]],[hotelIDDict objectForKey:@"city"],stringOfImages]];
                                }
                }else{
                    sightSeeingHTML = [datadict valueForKey:@"typeDefaultMsg"];
                                                   
                    [htmlString appendString:sightSeeingHTML];
                }

                        
                                
                            
            }else{
                if ([[[packageDetailDict valueForKey:@"isHotelDefaultMsg"] lowercaseString] isEqualToString:@"y"])
                            {
                                sightSeeingHTML = [packageDetailDict valueForKey:@"hotelDefaultMsg"];
                                
                                [htmlString appendString:sightSeeingHTML];
                            }
                            else
                            {
                             //   sightSeeingHTML = @"";
                          //  [htmlString appendString:sightSeeingHTML];

                        NSDictionary * dict = [self.completePackageDetail objectAtIndex:0];
                                
                        NSDictionary *packageDetailDict = [dict valueForKey:@"packageDetail"];

                            
                           NSArray * accomdationList = [packageDetailDict objectForKey:@"tcilHolidayAccomodationCollection"];
                            
                            NSPredicate * predicate1 =  [NSPredicate predicateWithFormat:@"(packageClassId == %@) ", self.holidayPackageDetail.stringSelectedAccomType ];
                            
                            NSArray *filteredArray = [accomdationList filteredArrayUsingPredicate:predicate1];
                            
                            for (NSDictionary * accomDict  in filteredArray)
                            {
                                NSMutableString *stringOfImages = [NSMutableString new] ;
                                
                                NSDictionary *hotelIDDict =  [accomDict valueForKey:@"accomodationHotelId"];
                                
                                
                                //NSString *starRatingString = [[hotelIDDict valueForKey:@"starRating"] intValue];
                                
                                NSString *totalStar = @"5";
                                NSString *selectedStar =[NSString stringWithFormat:@"%d", [[hotelIDDict valueForKey:@"starRating"] intValue]];
                                //totalStar = [[hotelIDDict valueForKey:@"starRating"] string];
                                
                                
                //                NSString *totalStar;
                //                NSString *selectedStar;
                //
                //                if (![starRatingString isEqualToString:@""])
                //                {
                //                    NSArray *ratingArray = [starRatingString componentsSeparatedByString:@"/"];
                //                    if (ratingArray.count == 2 )
                //                    {
                //                        selectedStar = ratingArray[0];
                //                        totalStar = ratingArray[1];
                //                    }
                //                }
                //                else
                //                {
                //                    totalStar = @"";
                //                }
                                
                                for (int i = 0; i<[totalStar intValue]; i++)
                                {
                                    NSString *imageNameURL;
                                    if (i < [selectedStar intValue])
                                    {
                                         imageNameURL = [[NSBundle mainBundle] pathForResource:@"rating_selected" ofType:@"png"];
                                    }else
                                    {
                                         imageNameURL = [[NSBundle mainBundle] pathForResource:@"rating_unselected" ofType:@"png"];
                                    }
                                    
                                    NSURL *urlImage = [NSURL fileURLWithPath:imageNameURL];
                                    [stringOfImages appendString:[NSString stringWithFormat:@"<img src=\"%@\" style=\"padding-left:2%%\" width=\"5%%\">",urlImage]];
                                }
                                [stringOfImages appendString:@"</br>"];
                                [htmlString appendString:[NSString stringWithFormat:@"<html><head></head><body><font face=\"Titillium-Regular\" color=\"gray\" size=\"4\">&nbsp&nbsp&nbsp%@</font><br><font face=\"Titillium-Regular\" color=\"gray\" size=\"2\">&nbsp&nbsp&nbsp&nbsp%@ Nights <br>&nbsp&nbsp&nbsp&nbsp%@</br>%@</body></html></br>" ,[hotelIDDict objectForKey:@"hotelName"] ,[NSString stringWithFormat:@"%d",[[accomDict objectForKey:@"noOfNights"] intValue]],[hotelIDDict objectForKey:@"city"],stringOfImages]];
                            }
                                
                            }
            }
            
            

            
            [cell.webView setHidden:NO];
            
            [cell.webView loadHTMLString:htmlString  baseURL:nil];
            
            //accomdationList
            
//            if ([childType isEqualToString:])
//            {
//                [self expandItemAtIndex:indexPath.section WithType:@"Exclusion"];
//            }
            
            if ([htmlString isEqualToString:@""]) {
                
                [cell.webView setHidden:NO];
                
                [cell.webView loadHTMLString:[NSString stringWithFormat:@"<html><body><font face=\"Titillium-regular\" color=\"black\" size=\"2\"><h5 style=\"margin:0px\" > As per itinerary </h5></body></html>"]  baseURL:nil];
            }
            else
            {
                
                [cell.webView setHidden:NO];
                
                NSString *htmlText =    [NSString stringWithFormat:@"<font face='Titillium-regular' size='3'>%@", htmlString];

                
                [cell.webView loadHTMLString:htmlText  baseURL:nil];
                
            }

            
            
        }
        else if ([childType isEqualToString:@"Meals"])
        {
            
            cell.VizaView.hidden = YES;
            cell.constraintVisaView.constant = 0;
            [cell.contentView layoutIfNeeded];
            
            NSMutableString * htmlString=[NSMutableString new] ;
    /*        NSArray * mealArray = [dict objectForKey:@"mealList"];
            
            NSPredicate * predicate1 =  [NSPredicate predicateWithFormat:@"(accomType == %@) ", self.AccommodationTypeBtn.titleLabel.text ];
            
            NSArray *filteredArray = [mealArray filteredArrayUsingPredicate:predicate1];
            
            for (NSDictionary * mealDict  in filteredArray) {
                
                [htmlString appendString:[mealDict objectForKey:@"description"]];
                
            }*/
            
            NSString *sightSeeingHTML;
            
            if (_holidayPackageDetail.mealsFlag == YES && _holidayPackageDetail.mealCollection.count > 0) {
                
                NSDictionary *datadict = [_holidayPackageDetail.mealCollection firstObject];
                
                if ([[[datadict valueForKey:@"isTypeDefaultMsg"] lowercaseString] isEqualToString:@"n"]) {
                    NSDictionary * dict = [self.completePackageDetail objectAtIndex:0];
                            
                            NSDictionary *packageDetailDict = [dict valueForKey:@"packageDetail"];
                            
                            NSArray * accomdationList = [packageDetailDict objectForKey:@"tcilHolidayMealCollection"];//tcilHolidayVisaCollection
                            
                            NSPredicate * predicate1 =  [NSPredicate predicateWithFormat:@"(packageClassId == %@) ", self.holidayPackageDetail.stringSelectedAccomType ];
                            
                            NSArray *filteredArray = [accomdationList filteredArrayUsingPredicate:predicate1];
                            
                            if(filteredArray.count>0)
                            {
                                for (NSDictionary * insuranceDict  in filteredArray)
                                {
                                    NSDictionary *mealDict = [insuranceDict valueForKey:@"tcilMstMeal"];
                                    
                                    NSString *mealDesc  = [mealDict valueForKey:@"mealDescription"];
                                    
                                    if (mealDesc != NULL && ![mealDesc  isEqual: @""])
                                    {
                                         [htmlString appendString:[NSString stringWithFormat:@"<p>%@</p>",mealDesc]];
                                    }
                                   
                                }
                    
                                
                               

                            }
                }else{
                    sightSeeingHTML = [datadict valueForKey:@"typeDefaultMsg"];
                                           [htmlString appendString:sightSeeingHTML];
                }
                
                        
                        
                        
                    
            }else{
                if ([[[packageDetailDict valueForKey:@"isMealsDefaultMsg"] lowercaseString] isEqualToString:@"y"])
                    {
                        sightSeeingHTML = [packageDetailDict valueForKey:@"mealsDefaultMsg"];
                        [htmlString appendString:sightSeeingHTML];
                    }
                    else
                    {
                        NSDictionary * dict = [self.completePackageDetail objectAtIndex:0];
                        
                        NSDictionary *packageDetailDict = [dict valueForKey:@"packageDetail"];
                        
                        NSArray * accomdationList = [packageDetailDict objectForKey:@"tcilHolidayMealCollection"];//tcilHolidayVisaCollection
                        
                        NSPredicate * predicate1 =  [NSPredicate predicateWithFormat:@"(packageClassId == %@) ", self.holidayPackageDetail.stringSelectedAccomType ];
                        
                        NSArray *filteredArray = [accomdationList filteredArrayUsingPredicate:predicate1];
                        
                        if(filteredArray.count>0)
                        {
                            for (NSDictionary * insuranceDict  in filteredArray)
                            {
                                NSDictionary *mealDict = [insuranceDict valueForKey:@"tcilMstMeal"];
                                
                                NSString *mealDesc  = [mealDict valueForKey:@"mealDescription"];
                                
                                if (mealDesc != NULL && ![mealDesc  isEqual: @""])
                                {
                                     [htmlString appendString:[NSString stringWithFormat:@"<p>%@</p>",mealDesc]];
                                }
                               
                            }
                
                            
                           

                        }
                        
                        
                    }
            }
           
            
            
            
           

            
            if ([htmlString isEqualToString:@""]) {
                
                [cell.webView setHidden:NO];
                
                [cell.webView loadHTMLString:[NSString stringWithFormat:@"<html><body><font face=\"Titillium-regular\" color=\"black\" size=\"2\"><h5 style=\"margin:0px\" > As per itinerary </h5></body></html>"]  baseURL:nil];
            }
            else
            {
                
                NSString *htmlText =    [NSString stringWithFormat:@"<font face='Titillium-regular' size='3'>%@", htmlString];

                [cell.webView setHidden:NO];
                
                [cell.webView loadHTMLString:htmlText  baseURL:nil];
                
            }
            
        }
        else if ([childType isEqualToString:@"Flight Details"] ) {
            
            [cell.webView setHidden:NO];
            cell.VizaView.hidden = YES;
            cell.constraintVisaView.constant = 0;
            
           // NSString * htmlString = [dict objectForKey:@"airfare"];
            
            NSMutableString * htmlString=[NSMutableString new];
            NSString *sightSeeingHTML;
            
            if ([[[packageDetailDict valueForKey:@"isFlightDefaultMsg"] lowercaseString] isEqualToString:@"y"])
            {
               sightSeeingHTML = [packageDetailDict valueForKey:@"flightDefaultMsg"];
            }
            else
            {
                sightSeeingHTML = @"";
            }

            
            
            [htmlString appendString:sightSeeingHTML];

            
            if ([htmlString isEqualToString:@""])
            {
                
                [cell.webView setHidden:NO];
                
                [cell.webView loadHTMLString:[NSString stringWithFormat:@"<html><body><font face=\"Titillium-regular\" color=\"black\" size=\"2.5\"><h5 style= \"margin:0px\"> As per itinerary </h5></body></html>"]  baseURL:nil];            }
            else
            {
                
                NSString *htmlText =    [NSString stringWithFormat:@"<font face='Titillium-regular' size='3'>%@", htmlString];

                [cell.webView setHidden:NO];
                
                [cell.webView loadHTMLString:htmlText  baseURL:nil];
                
            }
        }
        else if ([childType isEqualToString:@"Visa, Passport & Insurance"])
        {
            
            [cell.webView setHidden:YES];
            
         //   NSString * string = [dict objectForKey:@"insurance"];
            
            
              NSString * string  = @"";
            if (_holidayPackageDetail.tourMngerFlag == YES && _holidayPackageDetail.visaCollection.count > 0) {
                
                NSDictionary *datadict = [_holidayPackageDetail.visaCollection firstObject];
                
                if ([[[datadict valueForKey:@"isTypeDefaultMsg"] lowercaseString] isEqualToString:@"n"]) {
                    cell.VizaView.hidden = NO;

                                    [cell.insuranceButton sendActionsForControlEvents:UIControlEventTouchUpInside];
                                    
                                    NSDictionary * dict = [self.completePackageDetail objectAtIndex:0];
                                    
                                    NSDictionary *packageDetailDict = [dict valueForKey:@"packageDetail"];
                                    
                                    NSArray * accomdationList = [packageDetailDict objectForKey:@"tcilHolidayVisaCollection"];//tcilHolidayVisaCollection
                                    
                                    NSPredicate * predicate1 =  [NSPredicate predicateWithFormat:@"(packageClassId == %@) ", self.holidayPackageDetail.stringSelectedAccomType ];
                                    
                                    NSArray *filteredArray = [accomdationList filteredArrayUsingPredicate:predicate1];
                                    
                                    if (filteredArray.count > 0)
                                    {
                                        for (NSDictionary *insuranceDict in filteredArray)
                                        {
                    //                        if ([insuranceDict valueForKey:@"insurance"] != NULL && [[insuranceDict valueForKey:@"insurance"]  isEqual: @""] )
                                            string = [NSString stringWithFormat:@"%@<p>%@</p>",string,[insuranceDict valueForKey:@"insurance"]];
                                        }
                                    }
                                   
                                    if ([string isEqualToString:@""])
                                    {
                                        [cell.webView loadHTMLString:[NSString stringWithFormat:@"<html><body><font face=\"Titillium-regular\" color=\"black\" size=\"2.5\"><h5 style= \"margin:0px\"> As per itinerary </h5></body></html>"]  baseURL:nil];
                                    }
                                    else
                                    {
                                        NSString *htmlText =    [NSString stringWithFormat:@"<font face='Titillium-regular' size='3'>%@", string];

                                        [cell.webView loadHTMLString:htmlText baseURL:nil];
                                    }
                }else{
                    string = [datadict valueForKey:@"typeDefaultMsg"];
                    
                    cell.VizaView.hidden = YES;
                    [cell.webView setHidden:NO];
                    cell.constraintVisaView.constant = 0;

                    
                    if ([string isEqualToString:@""])
                    {
                        
                        [cell.webView loadHTMLString:[NSString stringWithFormat:@"<html><body><font face=\"Titillium-regular\" color=\"black\" size=\"2.5\"><h5 style= \"margin:0px\"> As per itinerary </h5></body></html>"]  baseURL:nil];
                        
                    }
                    else
                    {
                        NSString *htmlText =    [NSString stringWithFormat:@"<font face='Titillium-regular' size='3'>%@", string];

                        
                        [cell.webView loadHTMLString:htmlText baseURL:nil];
                    }
                }
                                                
                    
            }else{
                  if ([[[packageDetailDict valueForKey:@"isVisaDefaultMsg"] lowercaseString] isEqualToString:@"y"])
                            {
                                string = [packageDetailDict valueForKey:@"visaDefaultMsg"];
                                
                                cell.VizaView.hidden = YES;
                                [cell.webView setHidden:NO];
                                cell.constraintVisaView.constant = 0;

                                
                                if ([string isEqualToString:@""])
                                {
                                    
                                    [cell.webView loadHTMLString:[NSString stringWithFormat:@"<html><body><font face=\"Titillium-regular\" color=\"black\" size=\"2.5\"><h5 style= \"margin:0px\"> As per itinerary </h5></body></html>"]  baseURL:nil];
                                    
                                }
                                else
                                {
                                    NSString *htmlText =    [NSString stringWithFormat:@"<font face='Titillium-regular' size='3'>%@", string];

                                    
                                    [cell.webView loadHTMLString:htmlText baseURL:nil];
                                }

                            }
                            else
                            {
                                cell.VizaView.hidden = NO;

                                [cell.insuranceButton sendActionsForControlEvents:UIControlEventTouchUpInside];
                                
                                NSDictionary * dict = [self.completePackageDetail objectAtIndex:0];
                                
                                NSDictionary *packageDetailDict = [dict valueForKey:@"packageDetail"];
                                
                                NSArray * accomdationList = [packageDetailDict objectForKey:@"tcilHolidayVisaCollection"];//tcilHolidayVisaCollection
                                
                                NSPredicate * predicate1 =  [NSPredicate predicateWithFormat:@"(packageClassId == %@) ", self.holidayPackageDetail.stringSelectedAccomType ];
                                
                                NSArray *filteredArray = [accomdationList filteredArrayUsingPredicate:predicate1];
                                
                                if (filteredArray.count > 0)
                                {
                                    for (NSDictionary *insuranceDict in filteredArray)
                                    {
                //                        if ([insuranceDict valueForKey:@"insurance"] != NULL && [[insuranceDict valueForKey:@"insurance"]  isEqual: @""] )
                                        string = [NSString stringWithFormat:@"%@<p>%@</p>",string,[insuranceDict valueForKey:@"insurance"]];
                                    }
                                }
                               
                                if ([string isEqualToString:@""])
                                {
                                    [cell.webView loadHTMLString:[NSString stringWithFormat:@"<html><body><font face=\"Titillium-regular\" color=\"black\" size=\"2.5\"><h5 style= \"margin:0px\"> As per itinerary </h5></body></html>"]  baseURL:nil];
                                }
                                else
                                {
                                    NSString *htmlText =    [NSString stringWithFormat:@"<font face='Titillium-regular' size='3'>%@", string];

                                    [cell.webView loadHTMLString:htmlText baseURL:nil];
                                }

                                
                            }
            }
            
          
            
            
          //  NSDictionary *packageDetailDict = [dict valueForKey:@"packageDetail"];
            
            
            
            
        }
        else if ([childType isEqualToString:@"Notes"]) {
            
            [cell.webView setHidden:NO];
            cell.VizaView.hidden = YES;
            cell.constraintVisaView.constant = 0;
            
            NSString * htmlString = [dict objectForKey:@"notes"];
            
            if ([htmlString isEqualToString:@""]) {
                
                [cell.webView setHidden:NO];
                
                [cell.webView loadHTMLString:[NSString stringWithFormat:@"<html><body><font face=\"Titillium-regular\" color=\"black\" size=\"2.5\"><h5 style= \"margin:0px\"> No Data Found </h5></body></html>"]  baseURL:nil];
            }
            else
            {
                
                [cell.webView setHidden:NO];
                
                [cell.webView loadHTMLString:htmlString  baseURL:nil];
                
            }
            
        }
        else if ([childType isEqualToString:@"Other"]) {
            
            UIScrollView *superScrollView = (UIScrollView *)self.view.superview;
            superScrollView.scrollEnabled = NO;

            [cell.webView setHidden:NO];
            cell.VizaView.hidden = YES;
            cell.constraintVisaView.constant = 0;
            
            NSString * htmlString = [dict objectForKey:@"other"];
            
            if ([htmlString isEqualToString:@""]) {
                
                [cell.webView setHidden:NO];
                
                [cell.webView loadHTMLString:[NSString stringWithFormat:@"<html><body><font face=\"Titillium-regular\" color=\"black\" size=\"2.5\"><h5 style= \"margin:0px\"> No Data Found </h5></body></html>"]  baseURL:nil];
            }
            else
            {
                
                [cell.webView setHidden:NO];
                
                [cell.webView loadHTMLString:htmlString  baseURL:nil];
                [cell.webView sizeToFit];
                [cell.webView setScalesPageToFit:YES];
                
            }
            
        }
        else if ([childType isEqualToString:@"Tour Price includes"])
        {
            
            [cell.webView setHidden:NO];
            cell.VizaView.hidden = YES;
            cell.constraintVisaView.constant = 0;
            
            NSMutableString * htmlString=[NSMutableString new];
            
            
            NSDictionary *packageDetail = [dict valueForKey:@"packageDetail"];
            
            NSArray * excludeList = [packageDetail objectForKey:@"tcilHolidayIncludeExcludeCollection"];
            
            NSArray *filteredArray = excludeList;
            
            if (filteredArray.count > 0)
            {
            
            for (NSDictionary * mealDict  in filteredArray)
            {
                NSInteger accomTypeID = [[mealDict valueForKey:@"packageClassId"] integerValue];
                
                if([self.holidayPackageDetail.stringSelectedAccomType integerValue] == accomTypeID)
                {
                    [htmlString appendString:[mealDict objectForKey:@"includes"]];
                }
            }
            }
            
           // [htmlString appendString:sightSeeingHTML];
            
            if ([htmlString isEqualToString:@""]) {
                
                [cell.webView setHidden:NO];
                
                [cell.webView loadHTMLString:[NSString stringWithFormat:@"<html><body><font face=\"Titillium-regular\" color=\"black\" size=\"2.5\"><h5 style= \"margin:0px\"> As per itinerary </h5></body></html>"]  baseURL:nil];            }
            else
            {
                
                [cell.webView setHidden:NO];
                
                NSString *htmlText =    [NSString stringWithFormat:@"<font face='Titillium-regular' size='3'>%@", htmlString];

                [cell.webView loadHTMLString:htmlText  baseURL:nil];
                
            }
        }
        else
        {
            
            cell.VizaView.hidden = YES;
            
        }
    }
}


- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    
    NSLog(@" web view height  ; _ %f",webView.scrollView.contentSize.height);
    
    //webView.frame=CGRectMake(0, 0, webView.frame.size.width, webView.frame.size.height);
    //[webView setBackgroundColor:[UIColor blueColor]];
    
    //----
    
    //    CGRect frame = webView.frame;
    //    frame.size.height = 1;
    //    webView.frame = frame;
    //    CGSize fittingSize = [UIWebView sizeThatFits:CGSizeZero];
    //    frame.size = fittingSize;
    //    webView.frame = frame;
    
    
    CGRect frame = webView.frame;
    frame.size.height = 1;
    webView.frame = frame;
    CGSize fittingSize = [webView sizeThatFits:CGSizeZero];
    frame.size = fittingSize;
    webView.frame = frame;
    rowHeight= fittingSize.height+60;
    //  [self.tableView reloadData];

    //----
    
    
    
    
    //-- // rowHeight = webView.scrollView.contentSize.height+60 ;
    
    //webView.scalesPageToFit=YES;
    //  [self.tableView reloadData];
    
    [self.tableView beginUpdates];
    //    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    [self.tableView endUpdates];
    
    
}

#pragma mark- set multi color to text

-(NSMutableAttributedString*)setColorToText:(NSString*)text withColor:(UIColor*)color{
    
    int length = (int)[text length];
    
    NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:text];
    
    [string addAttribute:NSForegroundColorAttributeName value:color range:NSMakeRange(0,length-1)];
    
    
    return string;
}

- (IBAction)onAccommodationTypeBtnClick:(id)sender {
    
    
    //  UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"Select accommodation type:" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:nil,
    //                        nil];
    
    UIAlertController *durationActionSheet = [UIAlertController alertControllerWithTitle:@"Accommodation Type" message:@"Select accommodation type:" preferredStyle:UIAlertControllerStyleActionSheet];
    
    for (int index = 0; index < accommodationTypeArray.count; index++) {
        
        NSString *titleString = accommodationTypeArray[index];
        UIAlertAction *action = [UIAlertAction actionWithTitle:titleString style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
            self.AccommodationTypeBtn.titleLabel.text = titleString;
            [self setAccomType:titleString];
            
            
           // currentAccommodationType = [[NSString alloc] init];
            
            //[self InitAccommodationType];
            
            
            isCHECK = 0;
            cellIndex = -1;
            preCellIndex = -1;
            rowHeight = 100;
            isChildFlag = 0;
            selectedCell=-1;
            isCancelFlag = 0;
            
            imageName=@[@"inclusion.png",@"exclusion.png",@"note.png"];
            
            topItems = [[NSArray alloc] initWithArray:[self topLevelItems]];
            subItems = [NSMutableArray new];
            
            currentExpandedIndex = -1;
            
            for (int i = 0; i < [topItems count]; i++) {
                
                [subItems addObject:[self subItemsFor:[topItems objectAtIndex:i]]];
            }

            
            [self.tableView reloadData];
            
        }];
        
        [durationActionSheet addAction:action];
        
        // [popup addButtonWithTitle:[accommodationTypeArray objectAtIndex:index]];
    }
    // popup.tag = 1;
    // [popup showInView:[UIApplication sharedApplication].keyWindow];
    
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
        
        [self dismissViewControllerAnimated:durationActionSheet completion:nil];
    }];
    
    [durationActionSheet addAction:cancelAction];
    
    [self.view.window.rootViewController presentViewController:durationActionSheet animated:YES completion:nil];
    
    
    
}


//- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
//    
//    if (popup.tag == 1) {
//        
//        [self.AccommodationTypeBtn setTitle:[popup buttonTitleAtIndex:buttonIndex] forState:UIControlStateNormal];
//        
//    }
//    
//    else if (popup.tag == 2)
//    {
//        
//        
//        NSString  * string  =    [popup buttonTitleAtIndex:buttonIndex];
//        
//        if (![string isEqualToString:@"Cancel"]) {
//            
//         //   [sightSeenArray removeAllObjects];
//            
//            
//            NSDictionary * dict = [self.completePackageDetail objectAtIndex:0];
//            
//            if (dict != nil) {
//                
//                
//                NSArray * sightSeenList = [dict objectForKey:@"sightSeenList"];
//                
//                if (sightSeenList.count > 0 ) {
//                    
//                    
//                    NSPredicate * predicate1 =  [NSPredicate predicateWithFormat:@"(city == %@) ", string];
//                    
//                    NSArray *filteredArray = [sightSeenList filteredArrayUsingPredicate:predicate1];
//                    
//                    if ([filteredArray count] > 0) {
//                        
//                        [sightSeenArray addObjectsFromArray:filteredArray];
//                        
//                        [self updateChildCellIndicator:cellIndex imageName:@"minus.png" WithChildType:@"Sightseeings"];
//                        
//                    }
//                }
//            }
//        }
//        else
//        {
//            // [self updateChildCellIndicator:cellIndex imageName:@"minus.png" WithChildType:@"Sightseeings"];
//            //[self childcollapseSubItemsAtIndex:cellIndex];
//            //  ExpandableCell *cell;//= (ExpandableCell *)[self.tableView cellForRowAtIndexPath:cellIndex];
//            //[cell.webView removeFromSuperview];
//            
//            //   cell.webView.hidden=YES;
//            // [self.tableView reloadData];
//            //NSMutableArray *indexPaths = [NSMutableArray new];
//            //  [self.tableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
//            
//            isCancelFlag=1;
//            NSMutableArray *indexPaths = nil;
//            
//            [self.tableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
//            // ;
//            [self updateChildCellIndicator:(int)cancelIndexPath imageName:@"plus.png" WithChildType:nil];
//            isChildFlag=0;
//            ExpandableCell *cell= (ExpandableCell *)[self.tableView cellForRowAtIndexPath:cancelIndexPath];
//            cell.lblChildTitle.textColor=[UIColor darkGrayColor];
//            cell.imgChildIndicator.image=[UIImage imageNamed:@"plus.png"];
//            if (cancelIndexPath.row==selectedCell) {
//                return;
//            }
//
//        }
//    }
//    
//}




@end
