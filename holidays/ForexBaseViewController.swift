//
//  ForexBaseViewController.swift
//  holidays
//
//  Created by ketan on 22/11/17.
//  Copyright © 2017 Mobicule. All rights reserved.
//

import UIKit

class ForexBaseViewController: UIViewController
{
    
    @IBOutlet weak var backButton: UIButton!
    
    var menuOpen : Bool = false
    
    @IBOutlet weak var labelHeaderName: UILabel!
    @IBOutlet weak var munuButton: UIButton!
    
    @IBOutlet weak var parentView: UIView!
    
    @IBOutlet weak var menuWidthConstraint: NSLayoutConstraint!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        backButton.addTarget(self, action: #selector(backButtonClicked(buttonView:)), for: UIControl.Event.touchUpInside)
        
       // KeyboardAvoiding.avoidingView = self.view
        
//        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    
//    func keyboardWillShow(notification: NSNotification) {
//        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
//            if self.view.frame.origin.y == 0
//            {
//                self.view.frame.origin.y -= keyboardSize.height - 25
//            }
//        }
//    }
//    
//    func keyboardWillHide(notification: NSNotification) {
//        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
//            if self.view.frame.origin.y != 0{
//                self.view.frame.origin.y += keyboardSize.height - 25
//            }
//        }
//    }
    
    
    
    func addViewInBaseView(childView :UIView)
    {
        self.parentView.addSubview(childView);
        self.setSubViewFrame(childView: childView)
    }
    
    
    func setSubViewFrame(childView :UIView)
    {
          let screenSize: CGRect = UIScreen.main.bounds
          let screenWidth = screenSize.width
          childView.frame = CGRect(x: 0, y: 0, width: screenWidth, height: parentView.frame.size.height)
        
    }
    
    @objc func backButtonClicked(buttonView:UIButton)
    {
     
        if self.isKind(of: ForexLandingPageViewController.self)
        {
           //[[SlideNavigationController sharedInstance] popToRootViewControllerAnimated:YES];
            
           // SlideNavigationController.sharedInstance().popToRootViewController(animated: true)
            self.dismiss(animated: true, completion: nil)
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
       // let anyObj : Any = ""
        
        menuOpen = true
        
        //self.onMenuButtonClicked(anyObj);
    }
    
    @IBAction func onMenuButtonClicked(_ sender: Any)
    {

        
        SlideNavigationController.sharedInstance().toggleLeftMenu()
        
//        if self.revealViewController() != nil
//        {
//            self.revealViewController().rightRevealToggle(nil)
//            
//            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
//        }

        
     /*   if(self.menuOpen)
        {
            self.menuWidthConstraint.constant = 0;
        }
        else
        {
            self.menuWidthConstraint.constant = 240;
        }
        
        
        
        menuOpen = !menuOpen*/
    }
    
    func isValidEmail(testStr:String) -> Bool
    {
        
        print("validate emilId: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr)
        return result
        
    }
    
    func isValidMobileNo(testStr:String) -> Bool
    {
        print("validate emilId: \(testStr)")
        let mobRegEx = "^[789]\\d{9}$"
        let mobTest = NSPredicate(format:"SELF MATCHES %@", mobRegEx)
        let result = mobTest.evaluate(with: testStr)
       
        return result
    }
    
    func isValidDateOfTraveller(date : Date) -> Bool
    {
        let currentDate : Date = Date.init()
        let calendar = NSCalendar.current
        let date1 = calendar.startOfDay(for: date )
        let date2 = calendar.startOfDay(for:currentDate)
        let components =  calendar.dateComponents([.day], from: date1, to: date2)
        if components.day! > -2 || components.day! < (-61)
        {
            return false
        }
        return true
    }
    
    func isValidDateOfBirth(date : Date) -> Bool
    {
        let gregorian = Calendar(identifier: .gregorian)
        let ageComponents = gregorian.dateComponents([.year], from: date, to: Date())
        let age = ageComponents.year!
        if age < 18
        {
            return false
        }
        return true
    }

    func showAlert(message:NSString) -> Void
    {
        let alert = UIAlertController(title: "Alert", message: message as String, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { UIAlertAction in
            alert.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    func hideMenuButton() -> Void
    {
        munuButton.isHidden = false;
        backButton.isHidden = true;
    }
    
    func setUpHeaderLabel(labelHeaderNameText:String)
    {
        labelHeaderName.text = labelHeaderNameText
    }
    
    @IBAction func callButtonClicked(_ sender: UIButton)
    {
        let phoneNo = 18002099100 as UInt64
        if let url = URL(string: "tel://\(phoneNo)"), UIApplication.shared.canOpenURL(url)
        {
            if #available(iOS 10, *)
            {
                UIApplication.shared.open(url)
            }
            else
            {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    @IBAction func requestCallButtonClicked(_ sender: UIButton)
    {
        self.displayForexRequestCallPopUp()
    }
    
    
    func displayForexRequestCallPopUp()
    {
        let forexRequestCallPopUpVC: ForexRequestCallPopUpViewController = ForexRequestCallPopUpViewController(nibName: FOREX_REQUEST_CALL_POPUP_VC, bundle: nil)
        
        forexRequestCallPopUpVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(forexRequestCallPopUpVC, animated: false, completion: nil)
    }
    
}


