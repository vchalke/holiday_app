//
//  HeaderConfiguration.h
//  mobicule-device-network-layer
//
//  Created by Kishan Gupta on 04/12/15.
//  Copyright (c) 2015 Mobicule. All rights reserved.
//

#import <Foundation/Foundation.h>

#define MULTIPART_FORM_DATA   @"multipart/form-data"
#define APPLICATION_JSON      @"application/json"

#define CONTENT_TYPE          @"Content-Type"
#define ACCEPT                @"Accept"
#define AUTHORIZATION         @"Authorization"

@interface RequestHeader : NSObject {

    NSMutableDictionary* headerFields;
    NSString* _requestBody;

}

/**
 *  This is use to add header field for particular request.
 *
 *  @param value Pass value for particular header field.
 *
 *  @param key Pass key of particular header field.
 *
 *  @return nothing.
 */
- (void)addHeaderFields:(NSString*)value withKey:(NSString*)key ;

/**
 *  This is use to set Accept header field value for particular request.
 *
 *  @param value Pass value for Accept header field.
 *
 *  @return nothing.
 */
- (void)setHeaderValueForFieldAccept:(NSString*)value ;

/**
 *  This is use to set ContentType header field value for particular request.
 *
 *  @param value Pass value for ContentType header field.
 *
 *  @return nothing.
 */
- (void)setHeaderValueForFieldContentType:(NSString*)value ;

/**
*  This is use to set Authorization header field value for particular request.
*
*  @param value Pass value for Authorization header field.
*
*  @return nothing.
*/
- (void)setHeaderValueForFieldAuthorization:(NSString*)value ;

/**
 *  This is use to set RequestBody for particular request.
 *
 *  @param requestBody This is the particular request that you want to sent.
 *
 *  @return nothing.
 */
- (void)setRequestBody:(NSString*)requestBody;

/**
 *  This is use to get header field dictionary.
 *
 *  @return header fields in type dictionary.
 */
- (NSDictionary*)getHeaderFields;

/**
 *  This is use to get request body of particular request.
 *
 *  @return RequestBody in type string.
 */
- (NSString*)getRequestBody;

@end
