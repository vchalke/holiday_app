//
//  OrderSummaryVC.h
//  holidays
//
//  Created by Designer on 22/12/15.
//  Copyright © 2015 Mobicule. All rights reserved.
//

#import "BaseViewController.h"

@interface OrderSummaryVC : BaseViewController<UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UIView *viewOrderSummary;
@property (strong, nonatomic) IBOutlet UIView *viewFlight;
@property (strong, nonatomic) IBOutlet UIView *viewHotel;
- (IBAction)actionOnLoginBtn:(id)sender;
@property (weak,nonatomic) NSString *headerName;
- (IBAction)actionOnFlightHeaderBtn:(id)sender;
- (IBAction)actionOnHotelHeaderBtn:(id)sender;
- (IBAction)actionOnHolidayHeaderBtn:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnHoliday;
@property (weak, nonatomic) IBOutlet UIButton *btnFlight;
@property (weak, nonatomic) IBOutlet UIButton *btnHotel;
@property (weak, nonatomic) IBOutlet UITableView *tableViewOrderDetail;
- (IBAction)onHotelDetailsClicked:(id)sender;
- (IBAction)onFlightDetailsClicked:(id)sender;

@end
