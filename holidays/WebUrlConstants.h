//
//  WebUrlConstants.h
//  holidays
//
//  Created by Kush_Tech on 03/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <Foundation/Foundation.h>

NSString *const kForexWebUrl;
NSString *const kVisaWebUrl;
NSString *const kFlightsWebUrl;
NSString *const kInsuranceWebUrl;
NSString *const kCruiseWebUrl;
NSString *const kHotelsWebUrl;
NSString *const kGiftCardsWebUrl;



// PDP Screen Titles

NSString *const sTourName;
NSString *const sFlights;
NSString *const sTransfer;
NSString *const sHotels;
NSString *const sMeals;
NSString *const sSightSeeing;
NSString *const sItininery;
NSString *const sInclusion;
NSString *const sExclusion;
NSString *const sVisaTransferInsurance;


// Coredata Classes Names

NSString *const standardPackage;
NSString *const deluxePackage;
NSString *const premiumPackage;

// Coredata Classes Names

NSString *const RecentViewHoliday;
NSString *const WishListHoliday;
NSString *const CompareListHoliday;


// Userdefaults Value

NSString *const filterOptionsDict;
NSString *const filterDepartureCity;
NSString *const filterTheme;
NSString *const filterPackageType;
NSString *const filterMinBudget;
NSString *const filterMaxBudget;
NSString *const filterDuration;
NSString *const filterTravelMonth;
NSString *const filterByGroup;
NSString *const filterByStay;
NSString *const filterByCustomized;
NSString *const filterByAirInclusive;


// Apple Sign In Userdefaults Value

NSString *const kAppleSignInDetailsOfDevice;
