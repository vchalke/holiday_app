//
//  CityCovered+CoreDataProperties.swift
//  holidays
//
//  Created by Saurav on 28/11/17.
//  Copyright © 2017 Mobicule. All rights reserved.
//

import Foundation
import CoreData


extension CityCovered {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CityCovered> {
        return NSFetchRequest<CityCovered>(entityName: "CityCovered")
    }

    @NSManaged public var cityCode: String?
    @NSManaged public var cityName: String?
    @NSManaged public var holidayTimelineId: String?
    @NSManaged public var iconId: String?
    @NSManaged public var isActive: String?
    @NSManaged public var ltCityCode: String?
    @NSManaged public var noOfNights: String?
    @NSManaged public var packageId: String?
    @NSManaged public var position: String?
    @NSManaged public var prodITINCode: String?
    @NSManaged public var prodITINCode_packageId_cityCode: String?
    @NSManaged public var tourRelation: Tour?

}
