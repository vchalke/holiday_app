    //
    //  TicketsViewController.swift
    //  sotc-consumer-application
    //
    //  Created by Saurav Kumar on 09/08/17.
    //  Copyright © 2017 Mobicule Technologies Private Limited. All rights reserved.
    //

    import UIKit

    class TicketsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UIDocumentInteractionControllerDelegate,UIPopoverPresentationControllerDelegate,DocumentListViewDelegate {

    @IBOutlet var ticketTableView: UITableView!
        
    var overlay: UIView?
    var sourceView: UIView?
        var moduleName:String?
        
        var isfromNotificationHomeVc:Bool = false


   

    var passengerDetailsArray:Array<Passenger>?
    var passengerTicketDoucumets: Array<TourPassengerDouments>?

    override func viewDidLoad() {
    super.viewDidLoad()


    self.ticketTableView.register(UITableViewCell.self, forCellReuseIdentifier: "ticketTableView")

    self.ticketTableView.tableFooterView = UIView(frame: .zero)

    // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {

    self.navigationController?.setRedNavigationBar()
    self.navigationController?.setnavigatiobBarTitle(title: self.moduleName ?? "", viewController: self)
    self.navigationController?.setEmergencyNavigationRightButton(showEmergencyButton: true, viewController: self)
    //self.navigationController?.setNagationBackButton(showRightButton: true, viewController: self)



    }

    func backButtonClick() -> Void {


    self.navigationController?.popViewController(animated: true)

    self.navigationController?.setNavigationBarTranslucent()

    }

    override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
    }
        
        override func viewWillDisappear(_ animated: Bool) {
            
            if self.navigationController?.viewControllers.firstIndex(of: self) == nil && self.isfromNotificationHomeVc
            {
                SlideNavigationController.sharedInstance().popViewController(animated: true)
            }
            
        }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

    return passengerTicketDoucumets?.count ?? 0 ///passengerDetailsArray!.count
    }


    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

    return 80
    }


    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

    guard let cell = tableView.dequeueReusableCell(withIdentifier: "ticketTableView", for: indexPath) as? UITableViewCell
    else {

    fatalError("The dequeued cell is not an instance of MealTableViewCell.")
    }

    cell.textLabel? .font = UIFont(name:"Roboto-Light", size:15)

   /* if let passenger = passengerDetailsArray?[indexPath.row] {

    cell.textLabel?.text  = "\((passenger.firstName ?? "").capitalized) \((passenger.middleName ?? "").capitalized) \((passenger.lastName ?? "").capitalized)"

    cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
    }*/
        
        if let singlePassengerTicketDoucumet = self.passengerTicketDoucumets?[indexPath.row] {
            
            cell.textLabel?.text = "Document \(indexPath.row + 1)"
            
            //cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
        }

    // cell.textLabel?.text = "Vikas Patil"

    return cell

    }


    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

   // let passenger:Passenger = (passengerDetailsArray?[indexPath.row])!

 //  self.checkAndDownloadDocumnet(documentArray: getTicketDocuments(by: passenger.passengerNumber!))
        
        let cell = tableView.cellForRow(at: indexPath)
       self.sourceView = cell
        
        
        if let singlePassengerTicketDoucument = self.passengerTicketDoucumets?[indexPath.row] {
          
            
            var fileName:String = "Document \(indexPath.row + 1).pdf"
            
            let passengerNumber:String = singlePassengerTicketDoucument.passengerNumber ?? ""
            
            let ticketNumber:String = "Document \(indexPath.row + 1)"
            
            let downloadUrl:String = singlePassengerTicketDoucument.docummentURL ?? ""
            
            if downloadUrl != nil && !downloadUrl.isEmpty && downloadUrl != ""
            {
                fileName = TourDocumentController.getDecodedFileName(url:downloadUrl)
            }
            
            let saveFileUrl:String =  passengerNumber + "/" + ticketNumber + "/" + fileName
            
            let documnetUrl:String = TourDocumentController.getDocumentFilePath(url: saveFileUrl, bfnNumber: singlePassengerTicketDoucument.bfNumber!, moduleFolder: Title.TICKETS)
            
            self.downloadDocument(filePath: documnetUrl, url: downloadUrl)
        }
        
      /*  if let  filteredDocument:Array<TourPassengerDouments> = self.getTicketDocuments(by: passenger.passengerNumber!)
       {
        
        if let documentListArray:Array<DocumentDetails> = self.getDocumentDisplayList(passengerDocumnets: filteredDocument, passenger: passenger)
        {
        
            if(documentListArray.count > 1)
            {
                 self.presentPopover(passengerDocumnets: documentListArray)
                
            }else if(documentListArray.count == 1)
            {
                let documnet:DocumentDetails = documentListArray.first!
                
                //self.viewDownloadedFile(fileurl: URL(fileURLWithPath:documnet.documnetUrl!))
                
                self.downloadDocument(filePath: documnet.documnetUrl!, url: documnet.downloadUrl!)
                
            }else
            {
                self.displayAlert()
            }
        }else{
        
            self.displayAlert()
        }
        
        }else{
            
            self.displayAlert()
        
        }*/

        tableView.deselectRow(at: indexPath, animated: true)
        
       

    }


    func getTicketDocuments(by passengerNumber:String) -> Array<TourPassengerDouments> {

    let filteredDocumnetsArray = passengerTicketDoucumets?.filter() { $0.passengerNumber == passengerNumber }

    return filteredDocumnetsArray!

    }
        
        func getDocumentDisplayList(passengerDocumnets:Array<TourPassengerDouments>,passenger:Passenger) -> Array<DocumentDetails> {
            
            var documnetListArray:Array<DocumentDetails> = []
            
            var index:Int = 0
            
            for document in passengerDocumnets
            {
                index = index + 1
                
                let passengerNumber:String = passenger.passengerNumber ?? ""
                
                let ticketNumber:String = "Document \(index)"
                
                var fileName:String = "Document \(index).pdf"
                
                let documentUrl:String = document.docummentURL ?? ""
                
                if documentUrl != nil && !documentUrl.isEmpty && documentUrl != ""
                {
                    fileName = TourDocumentController.getDecodedFileName(url:documentUrl)
                }
                
                let saveFileUrl:String =  passengerNumber + "/" + ticketNumber + "/" + fileName
                
                var documnetdetails : DocumentDetails = DocumentDetails()
                
                documnetdetails.displayTitle = "Ticket \(index)"
                documnetdetails.downloadUrl = document.docummentURL
                documnetdetails.documnetUrl = TourDocumentController.getDocumentFilePath(url: saveFileUrl, bfnNumber: document.bfNumber!, moduleFolder: moduleName ?? "") //TourDocumentController.getDocumentFilePathByDecoding(url: document.docummentURL! , bfnNumber: document.bfNumber!,moduleFolder:Title.TICKETS)
                
                documnetListArray.append(documnetdetails)
            }
            
            return documnetListArray
            
        }

        
        
   
        

    //MARK: UIDocumentInteractionController Delegate

    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController
    {
        return self
    }

    func viewDownloadedFile(fileurl : URL) -> Void
    {

    let concurrentQueue = DispatchQueue(label: "queuename", attributes: .concurrent)
    concurrentQueue.sync {

    //DispatchQueue.main.sync {

        let documentController : UIDocumentInteractionController = UIDocumentInteractionController.init(url: fileurl)

    //self.documentController.uti =
        
        
        documentController.name  = self.moduleName

    documentController.delegate = self

    documentController.presentPreview(animated: true)
        
        LoadingIndicatorView.hide()

    // }

    }

    }
        
        
    func presentPopover(passengerDocumnets:Array<DocumentDetails>) {
            
            
            let popoverContentController = DocumentListViewController(nibName: "DocumentListViewController", bundle: nil)
        
        popoverContentController.delegate = self
        
            popoverContentController.presentingView = self
            // Set your popover size.
            popoverContentController.preferredContentSize = CGSize(width: (UIScreen.main.bounds.width/2) + 100, height: (UIScreen.main.bounds.height/3) + 100)
            
            // Set the presentation style to modal so that the above methods get called.
            popoverContentController.modalPresentationStyle = .popover
            
            // Set the popover presentation controller delegate so that the above methods get called.
            popoverContentController.popoverPresentationController!.delegate = self
            
            popoverContentController.documnetListArray = passengerDocumnets
            
            // Present the popover.
            self.present(popoverContentController, animated: true, completion: nil)
            
        }
        
        
    //MARK: UIPopoverPresentationController Delegate
        
        
    dynamic func presentationController(_ presentationController: UIPresentationController, willPresentWithAdaptiveStyle style: UIModalPresentationStyle, transitionCoordinator: UIViewControllerTransitionCoordinator?) {
            // add a semi-transparent view to parent view when presenting the popover
            let parentView = presentationController.presentingViewController.view
            
            let overlay = UIView(frame: (parentView?.bounds)!)
            overlay.backgroundColor = UIColor(white: 0.0, alpha: 0.4)
            parentView?.addSubview(overlay)
            
            let views: [String: UIView] = ["parentView": parentView!, "overlay": overlay]
            
            parentView?.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[overlay]|", options: [], metrics: nil, views: views))
            parentView?.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[overlay]|", options: [], metrics: nil, views: views))
            
            overlay.alpha = 0.0
            
            transitionCoordinator?.animate(alongsideTransition: { _ in
                overlay.alpha = 1.0
            }, completion: nil)
            
            self.overlay = overlay
        }
        
    func prepareForPopoverPresentation(_ popoverPresentationController: UIPopoverPresentationController) {
            
            popoverPresentationController.permittedArrowDirections = .any
            popoverPresentationController.sourceView =  self.sourceView//self.view
            popoverPresentationController.sourceRect = CGRect(x: 20, y: 70, width: 0, height: 0)
        }
        
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
            return .none
        }
        
    func popoverPresentationControllerDidDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) {
            
            
            guard let overlay = overlay else {
                return
            }
            DispatchQueue.main.async() {
                UIView.animate(withDuration: 0.2, animations: {
                    overlay.alpha = 0.0
                }, completion: { _ in
                    overlay.removeFromSuperview()
                })
            }
            
        }
        
    func removeOverLay() {
            
            
            guard let overlay = self.overlay else {
                return
            }
            DispatchQueue.main.async() {
                UIView.animate(withDuration: 0.2, animations: {
                    overlay.alpha = 0.0
                }, completion: { _ in
                    overlay.removeFromSuperview()
                })
            }
            
        }
        
        //DocumentListViewDelegate delegate methods
        
    func selectedDocument(documnetUrl:String, downloadUrl:String)
         {
            self.removeOverLay();            
            
            self.downloadDocument(filePath: documnetUrl, url: downloadUrl)
            
            
        }
        
    func downloadDocument(filePath:String ,url:String) {
            
            //  let passengerDocumnet:TourPassengerDouments =  documentArray.first!
            
            LoadingIndicatorView.show("Loading")
            
            NetworkCommunication.downloadDocuments(urlString: url, destinationFilePath: filePath, completion: { (downlodedfilePath, status) in
                
                printLog(status,filePath);
                
                if(status)
                {
                    self.viewDownloadedFile(fileurl: URL(fileURLWithPath:filePath))
                    
                }
                else{
                    
                    let isFilePresent = TourDocumentController.isFileExistInDocumentDirectory(fileNamePath: filePath)
                    
                    if(isFilePresent.chkFile)
                    {
                        self.viewDownloadedFile(fileurl: URL(fileURLWithPath:filePath))
                        
                    }else{
                    
                   // DispatchQueue.main.sync {
                        
                        LoadingIndicatorView.hide()
                        
                       self.displayAlert()
                   // }
                    
                    }
                    
                }
                
            })
        }
        

    func displayAlert()
        {
            let alert = UIAlertController(title: "", message: AlertMessage.Tickets.TICKET_YET_TO_ISSUED, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: AlertMessage.TITLE_OK, style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)        }
        
    }
