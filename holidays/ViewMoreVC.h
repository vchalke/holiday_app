//
//  ViewMoreVCViewController.h
//  holidays
//
//  Created by Kush_Tech on 04/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PackageDetailModel.h"
#import "PackageDetailModel.h"
#import "NewMasterVC.h"
#import "HolidayPackageDetail.h"
NS_ASSUME_NONNULL_BEGIN

@interface ViewMoreVC : NewMasterVC
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_FirstView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_SecondView;
@property (strong ,nonatomic) PackageDetailModel *packageModel;
@property (strong ,nonatomic) NSString *sectionTitLe;

@property (strong ,nonatomic) NSString *firstDescription;
@property (strong ,nonatomic) NSString *inclusionDescription;
@property (strong ,nonatomic) NSString *exclusionDescription;

@property (strong ,nonatomic) NSArray *allDescriptionArray;
@property (strong ,nonatomic) NSArray *inclusionDescriptionArray;
@property (strong ,nonatomic) NSArray *exclusionDescriptionArray;

@property (weak, nonatomic) IBOutlet UIView *first_view;
@property (weak, nonatomic) IBOutlet UIView *second_view;
@property (weak, nonatomic) IBOutlet UILabel *lbl_first;
@property (weak, nonatomic) IBOutlet UIView *slidingViewOne;
@property (weak, nonatomic) IBOutlet UIView *slidingViewTwo;
@property (weak, nonatomic) IBOutlet UIButton *btn_inclusion;
@property (weak, nonatomic) IBOutlet UIButton *btn_exclusion;
@property (weak, nonatomic) IBOutlet UITextView *lbl_descriptionText;
@property (weak, nonatomic) IBOutlet UIWebView *web_viewa;
@property (weak, nonatomic) IBOutlet UILabel *lbl_prizes;
@property (weak, nonatomic) IBOutlet UIView *view_Offer;
@property (nonatomic) BOOL isOfferApplicable;
@property (strong ,nonatomic) NSString *lblaPrize;
@property (strong ,nonatomic) HolidayPackageDetail *holidayPkgModelInViewMore;
@end

NS_ASSUME_NONNULL_END
