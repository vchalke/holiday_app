//
//  HorizontalScrollCell.h
//
//  Created by ketan on 08/10/15.
//  Copyright © 2015 Mobicule. All rights reserved.
//


#import <UIKit/UIKit.h>

@class HorizontalScrollCellAction;
@protocol HorizontalScrollCellDelegate <NSObject>
-(void)cellSelectedWithGesture:(UITapGestureRecognizer *)tapGesture withScrollView:(UIScrollView *)scrollView;
@end

@interface HorizontalScrollCell : UICollectionViewCell <UIScrollViewDelegate>
{
    CGFloat supW;
    CGFloat off;
    CGFloat diff;
}


@property (strong, nonatomic) IBOutlet UIScrollView *scroll;

@property (strong, nonatomic) IBOutlet UILabel *title;
-(void)setUpCellWithArray:(NSArray *)array withSelectedtab:(int)clickedTab withISFIT:(BOOL)isFIT withISMRP:(BOOL)isMRP withNoOfPacks:(NSString *)noofPacks;

@property (nonatomic,strong) id<HorizontalScrollCellDelegate> cellDelegate;

@end
// Copyright belongs to original author
// http://code4app.net (en) http://code4app.com (cn)
// From the most professional code share website: Code4App.net