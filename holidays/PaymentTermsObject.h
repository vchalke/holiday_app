//
//  PaymentTermsObject.h
//  holidays
//
//  Created by Kush_Tech on 23/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PaymentTermsObject : NSObject
-(instancetype)initWithPaymentTermsObjectDict:(NSDictionary *)dictionary;
@property(nonatomic,strong)NSArray *tcilHolidayTermsConditionsCollection;
@property(nonatomic,strong)NSMutableArray *descriptionArray;
@property(nonatomic,strong)NSString *paymentTerms;
@property(nonatomic,strong)NSString *cancellationPolicy;
@end

NS_ASSUME_NONNULL_END
