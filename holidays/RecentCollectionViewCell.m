//
//  RecentCollectionViewCell.m
//  holidays
//
//  Created by Kush_Tech on 24/02/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "RecentCollectionViewCell.h"
#import "UIImageView+WebCache.h"
@implementation RecentCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(void)loadObjectInCell:(RecentHolidayObject*)recentObj{
//    NSNumberFormatter* numberFormatter = [[NSNumberFormatter alloc] init];
//    [numberFormatter setNumberStyle: NSNumberFormatterDecimalStyle];
//    NSString *numberString = [numberFormatter stringFromNumber: [NSNumber numberWithInteger: recentObj.packagePrize]];
//    self.lbl_prize.text = numberString;
    self.lbl_prize.text = [self getNumberInString:recentObj.packagePrize];
//    self.lbl_prize.text = [NSString stringWithFormat:@"%ld",recentObj.packagePrize];
    self.lbl_title.text = recentObj.packageName;
    NSLog(@"%@",recentObj.packageImgUrl);
    NSURL* urlImage=[NSURL URLWithString:recentObj.packageImgUrl];
    
        if(urlImage)
        {
            UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];

            indicator.center = self.packageImgView.center;// it will display in center of image view
            [self.packageImgView addSubview:indicator];
            [indicator startAnimating];
            
            [self.packageImgView sd_setImageWithURL:urlImage placeholderImage:[UIImage imageNamed:@"defaultBanner.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
             {
                 [indicator stopAnimating];
             }];
        }
    
}
-(NSString*)getNumberInString:(NSInteger)number{
    
    NSNumberFormatter* numberFormatter = [[NSNumberFormatter alloc] init];
     [numberFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_IN"]]; // For India Only
    [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [numberFormatter setGeneratesDecimalNumbers:FALSE];
    [numberFormatter setMaximumFractionDigits:0];
    NSString *numberString = [numberFormatter stringFromNumber: [NSNumber numberWithInteger: number]];
    return numberString;
}
@end
