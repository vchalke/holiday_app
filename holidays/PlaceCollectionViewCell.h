//
//  PlaceCollectionViewCell.h
//  Holiday
//
//  Created by Kush Thakkar on 14/02/20.
//  Copyright © 2020 Kush Thakkar. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN


@interface PlaceCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *placeImageView;

@property (weak, nonatomic) IBOutlet UITextView *infoTextView;

@property (weak, nonatomic) IBOutlet UILabel *placeNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *placeNumLabel;
@property (weak, nonatomic) IBOutlet UIButton *addButton;

- (IBAction)addButtonClicked:(UIButton *)sender;

@end

NS_ASSUME_NONNULL_END
