//
//  MyProfileVc.h
//  holidays
//
//  Created by Designer on 22/12/15.
//  Copyright © 2015 Mobicule. All rights reserved.
//

#import "BaseViewController.h"
//#import "LoginViewPopUp.h"
#import "NewLoginPopUpViewController.h"
@protocol DismissNewLoginPopUp;
@interface MyProfileVc : BaseViewController<DismissNewLoginPopUp/*dismissLoginPopUp*/>{

}
    
@property (weak, nonatomic) IBOutlet UILabel *emailId;

@property (strong, nonatomic) IBOutlet UIView *loginView;
@property (weak, nonatomic) IBOutlet UIView *profileViewAfterLogin;
- (IBAction)actionOnBtnMyFavourites:(id)sender;
@property(nonatomic ,assign)BOOL IsMyProfileLoginSuccess;
@property (strong, nonatomic) IBOutlet UIView *MyFavouriteView;
- (IBAction)actionOnFavouriteSearchBtn:(id)sender;
- (IBAction)actionOnMyBookingBtn:(id)sender;

- (IBAction)actionForLogin:(id)sender;
@property (weak,nonatomic) NSString *headerName;
@property (weak, nonatomic) IBOutlet UILabel *labelPhoneNumber;

@end
