//
//  TopWebVC.m
//  holidays
//
//  Created by Kush_Tech on 03/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "TopWebVC.h"

@interface TopWebVC ()<UIWebViewDelegate,WKUIDelegate,WKNavigationDelegate>
{
 LoadingView *activityLoadingView;
}
@end

@implementation TopWebVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.lbl_Title.text = self.web_title;
    if (self.isFromPdpCalculate){
        [self showArrayOfStringInPdp:self.stringArray];
    }else{
    activityLoadingView = [LoadingView loadingViewInView:self.web_views
           withString:@""
    andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
    NSString *urlAddress = self.web_url_string;
        [self.web_views setBackgroundColor:[UIColor clearColor]];
        [self.web_views setOpaque:NO];
    self.web_views.UIDelegate = self;
    self.web_views.navigationDelegate = self;
    NSURL *url = [NSURL URLWithString:urlAddress];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [self.web_views loadRequest:requestObj];
    
    }
    
}

-(void)showArrayOfStringInPdp:(NSArray*)strArray{
    for (NSString *objectStr in strArray) {
        NSString *htmlString = [NSString stringWithFormat:@"<font face='HelveticaNeue' size='5'>%@", objectStr];
        [self.web_views loadHTMLString:htmlString baseURL:nil];
    }
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation{
    NSLog(@"Webview navigation loding finished");
    [activityLoadingView removeFromSuperview];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
   if (self.web_views.isLoading)
      return;

   NSLog(@"Webview loding finished");
}
- (IBAction)btn_Back:(id)sender {
    [self jumpToPreviousViewController];
}
@end
