//
//  UserBookingDBManager.swift
//  sotc-consumer-application
//
//  Created by Parshwanath on 27/09/17.
//  Copyright © 2017 Mobicule Technologies Private Limited. All rights reserved.
//

//import UIKit
import CoreData

class UserBookingDBManager: NSObject {
    
    
    //MARK: Save Methods
    
    class func saveBFNList(bfnListResponse:AEXMLDocument)
    {
        
        do{
            
            
            UserBookingDBManager.DeleteAllTourAndRelatedData()
            
            
            
            let userMobileNumber:String = UserDefaults.standard.value(forKey:UserDefauldConstant.userMobileNumber) as! String
            
            let predicate = NSPredicate(format: "mobileNumber == \(userMobileNumber)")
            
            
            let tableFetchRequest: NSFetchRequest<UserProfile> = UserProfile.fetchRequest()
            tableFetchRequest.predicate = predicate
            
            let tableList = try CoreDataController.getContext().fetch(tableFetchRequest)
            
            
            
            
            if let bookingList = bfnListResponse["BOOKING_FILE_LIST_RESPONSE"]["BOOKING_LIST"].all {
                
                var index:Int = 0
                
                for tourDetail in bookingList {
                    
                    let tourDetail1:Tour = NSEntityDescription.insertNewObject(forEntityName: "Tour", into: CoreDataController.getContext()) as! Tour
                    
                    index += 1
                    
                    if let bookingFileNumber = tourDetail["BOOKING_FILE_NO"].value {
                        
                        tourDetail1.bfNumber = "\(bookingFileNumber)"
                        
                        
                    }
                    if let bookingFileName = tourDetail["BOOKING_FILE_NAME"].value {
                        
                        printLog(bookingFileName)
                        
                        
                    }
                    if let bookingDepartureDate = tourDetail["DEPARTURE_DATE"].value {
                        
                        
                        
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "dd-MM-yyyy" //Your date format
                        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
                        
                        
                        let date = dateFormatter.date(from: bookingDepartureDate) //according to date format your date string
                        
                        
                        tourDetail1.departureDate = (date! as NSDate) as Date
                    }
                    if let bookingArrivalDate = tourDetail["ARRIVAL_DATE"].value {
                        
                        
                        
                        
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "dd-MM-yyyy" //Your date format
                        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
                        
                        
                        let date = dateFormatter.date(from: bookingArrivalDate) //according to date format your date string
                        
                        
                        tourDetail1.arrivalDate = (date! as NSDate) as Date
                        
                        
                    }
                    if let tourDays = tourDetail["NO_OF_DAYS"].value {
                        
                        printLog(tourDays)
                    }
                    if let productITINCode = tourDetail["PROD_ITIN_CODE"].value
                    {
                        
                        tourDetail1.prodITINCode = "\(productITINCode)"
                    }
                    if let productBookingStatus = tourDetail["PRODUCT_BOOKING_STATUS"].value
                    {
                        
                        tourDetail1.productBookingStatus = "\(productBookingStatus)"
                    }
                    
                    if let bookingFileName = tourDetail["BOOKING_FILE_NAME"].value {
                        
                        printLog(bookingFileName)
                        tourDetail1.bookingFileName = bookingFileName
                        
                    }
                    
                    
                    if let opportunityId = tourDetail["OPPORTUNITY_ID"].value {
                        
                        printLog(opportunityId)
                        
                        tourDetail1.opportunityId = "\(opportunityId)"
                        
                    }
                    
                    if let tourName = tourDetail["TOUR_NAME"].value {
                        
                        printLog(tourName)
                        
                        tourDetail1.tourName = "\(tourName)"
                        
                    }
                    if let region = tourDetail["REGION"].value {
                        
                        
                        
                        tourDetail1.region = "\(region)"
                        
                    }
                    
                    if let tourZone = tourDetail["ZONE"].value {
                        
                        tourDetail1.tourZone = "\(tourZone)"
                        
                    }
                    
                    
                    if let tourCode = tourDetail["TOUR_CODE"].value {
                        
                        printLog(tourCode)
                        
                        tourDetail1.tourCode = "\(tourCode)"
                        
                    }
                    
                    if let paymentFlag = tourDetail["PAYMENT_FLAG"].value {
                        
                        
                        
                        tourDetail1.paymentFlag = "\(paymentFlag)"
                        
                    }
                    
                    tableList.first?.addToTourRelation(tourDetail1)
                }
                
                
                CoreDataController.getContext().mergePolicy = CoreDataConstant.dataMergePolicy
                
                CoreDataController.saveContext()
            }
        }catch
        {
            printLog(error)
        }
    }
    
    class func saveBFNListDetails(bfnListDetailsResponse:AEXMLDocument,bfNumber:String) -> String
    {
        do
        {
            
            let predicate = NSPredicate(format: "bfNumber == '\(bfNumber)'")
            
            let fetchRequest: NSFetchRequest<Tour> = Tour.fetchRequest()
            
            fetchRequest.predicate = predicate
            
            
            let tourListFiltered = try CoreDataController.getContext().fetch(fetchRequest)
            
            //        let tourListFiltered = tourList?.filter({ (Tour) -> Bool in
            //
            //            Tour.bfNumber == bfNumber
            //        })
            
            
            if tourListFiltered.count == 1 {
                
                UserBookingDBManager.DeleteTourDetailRelatedData(bfnNumber: bfNumber)
                
                if let passengerList = bfnListDetailsResponse["BOOKING_DETAILS_RESPONSE"]["PASSENGER_DETAILS"]["PASSENGER"].all {
                    
                    
                    for passengerDetail in passengerList {
                        
                        let passengerNumber = passengerDetail["PASSENGER_NO"].value
                        
                        
                        if passengerNumber != nil && !(passengerNumber?.isEmpty)! {
                            
                            
                            let passengerObject:Passenger = NSEntityDescription.insertNewObject(forEntityName: "Passenger", into: CoreDataController.getContext()) as! Passenger
                            
                            passengerObject.passengerNumber = "\(passengerNumber ?? "")"
                            
                            
                            if let passengerTitle = passengerDetail["TITLE"].value {
                                
                                passengerObject.title = "\(passengerTitle)"
                                
                                
                            }
                            if let passengerFirstName = passengerDetail["FIRST_NAME"].value {
                                
                                passengerObject.firstName = "\(passengerFirstName)"
                                
                            }
                            if let passengerMiddleName = passengerDetail["MIDDLE_NAME"].value {
                                
                                passengerObject.middleName = "\(passengerMiddleName)"
                                
                            }
                            if let passengerLastName = passengerDetail["LAST_NAME"].value {
                                
                                passengerObject.lastName = "\(passengerLastName)"
                            }
                            if let passengerEmail = passengerDetail["EMAIL"].value {
                                
                                passengerObject.emailId = "\(passengerEmail)"
                            }
                            if let passengerMobileNo = passengerDetail["MOBILE"].value {
                                passengerObject.mobileNumber = "\(passengerMobileNo)"
                                
                                
                            }
                            if let passengerType = passengerDetail["PAX_TYPE"].value {
                                
                                
                                passengerObject.paxType = "\(passengerType)"
                                
                            }
                            
                            if let passengerRoomType = passengerDetail["ROOM_TYPE"].value {
                                
                                
                                passengerObject.roomType = passengerRoomType
                                
                            }
                            
                            if let passengerMeal = passengerDetail["MEAL"].value {
                                
                                
                            }
                            if let passengerAddress = passengerDetail["PASSENGER_ADDRESS"].value {
                                
                                passengerObject.address = "\(passengerAddress)"
                            }
                            if let passengerPassportNumber = passengerDetail["PASSPORT_NUMBER"].value {
                                
                                passengerObject.passportNumber = "\(passengerPassportNumber)"
                                
                            }
                            if let passengerAadhaarNumber = passengerDetail["AADHAAR_NUMBER"].value {
                                
                                passengerObject.aadhaarNumber = "\(passengerAadhaarNumber)"
                                
                                
                            }
                            if let passengerRoomNumber = passengerDetail["ROOM_NO"].value {
                                
                                passengerObject.roomNumber = "\(passengerRoomNumber)"
                                
                            }
                            if let passengerDocumentStatus = passengerDetail["DOCUMENT_STATUS"].value {
                                
                                
                                passengerObject.documentStatus = "\(passengerDocumentStatus)"
                                
                            }
                            
                            passengerObject.bfNumber = tourListFiltered.first?.bfNumber
                            
                            passengerObject.passengerNumber_bfNumber = "\(passengerNumber ?? "")_\(tourListFiltered.first?.bfNumber ?? "")"
                            
                            tourListFiltered.first?.addToPassengerRelation(passengerObject)
                            
                        }
                        
                    }
                    
                }
                
                if let visaList = bfnListDetailsResponse["BOOKING_DETAILS_RESPONSE"]["VISA_DETAILS"]["VISA"].all {
                    
                    for visaDetail in visaList {
                        
                        
                        
                        let passengerNumber = visaDetail["PASSENGER_NO"].value
                        
                        let visaCountry = visaDetail["VISA_COUNTRY"].value
                        
                        if passengerNumber != nil && visaCountry != nil && !(passengerNumber?.isEmpty)! && !(passengerNumber?.isEmpty)! {
                            
                            
                            let visaObject:Visa = NSEntityDescription.insertNewObject(forEntityName: "Visa", into: CoreDataController.getContext()) as! Visa
                            
                            
                            
                            visaObject.visaCountry = "\(visaCountry ?? "")"
                            
                            visaObject.passengerNumber = "\(passengerNumber ?? "")"
                            
                            
                            
                            
                            if let visaStatus = visaDetail["VISA_STATUS"].value {
                                
                                visaObject.visaStatus = "\(visaStatus)"
                                
                            }
                            if let visaType = visaDetail["VISA_TYPE"].value {
                                
                                visaObject.visaType = "\(visaType)"
                                
                                
                                
                            }
                            if let visaRequestStatus = visaDetail["VISA_REQUEST_STATUS"].value {
                                
                                visaObject.visaRequestStatus = "\(visaRequestStatus)"
                            }
                            
                            
                            visaObject.bfNumber_passengerNumber_visaCountry = "\(tourListFiltered.first?.bfNumber ?? "")_\(passengerNumber ?? "")_\(visaCountry ?? "")"
                            
                            visaObject.bfNumber = tourListFiltered.first?.bfNumber
                            
                            tourListFiltered.first?.addToVisaRelation(visaObject)
                            
                        }
                        
                        
                    }
                    
                }
                
                if let insuranceList = bfnListDetailsResponse["BOOKING_DETAILS_RESPONSE"]["INSURANCE_DETAILS"]["INSURANCE"].all {
                    
                    for insuranceDetail in insuranceList {
                        
                        
                        let policyNumber = insuranceDetail["POLICY_NO"].value
                        
                        
                        if policyNumber != nil && !(policyNumber?.isEmpty)!
                            
                        {
                            
                            let insuranceObject:Insurance = NSEntityDescription.insertNewObject(forEntityName: "Insurance", into: CoreDataController.getContext()) as! Insurance
                            
                            
                            insuranceObject.policyNumber = "\(policyNumber ?? "")"
                            
                            
                            
                            if let passengerNumber = insuranceDetail["PASSENGER_NO"].value {
                                
                                insuranceObject.passengerNumber = "\(passengerNumber)"
                                
                            }
                            
                            if let policyURL = insuranceDetail["POLICY_URL"].value {
                                
                                insuranceObject.policyURL = "\(policyURL)"
                                
                            }
                            
                            insuranceObject.bfNumber = tourListFiltered.first?.bfNumber
                            
                            tourListFiltered.first?.addToInsuranceRelation(insuranceObject)
                        }
                        
                    }
                }
                
                
                
                
                if let insuranceList = bfnListDetailsResponse["BOOKING_DETAILS_RESPONSE"]["PAYMENT_DETAILS"]["PAYMENT"].all {
                    
                    for insuranceDetail in insuranceList {
                        
                        
                        
                        
                        
                        let currency = insuranceDetail["CURRENCY"].value
                        
                        
                        if currency != nil && !(currency?.isEmpty)!
                        {
                            
                            let paymentObject:Payment = NSEntityDescription.insertNewObject(forEntityName: "Payment", into: CoreDataController.getContext()) as! Payment
                            
                            
                            paymentObject.currency = "\(currency!)"
                            
                            
                            if let totalReceivable = insuranceDetail["TOTAL_RECEIVABLE"].value {
                                
                                paymentObject.totalReceivable = "\(totalReceivable)"
                                
                                
                            }
                            if let totalReceived = insuranceDetail["TOTAL_RECEIVED"].value {
                                
                                paymentObject.totalReceived = "\(totalReceived)"
                                
                            }
                            if let balancePending = insuranceDetail["BALANCE"].value {
                                
                                paymentObject.balancePending = "\(balancePending)"
                                
                            }
                            
                            paymentObject.bfNumber = tourListFiltered.first?.bfNumber
                            
                            paymentObject.currency_bfNumber = "\(currency ?? "")_\(tourListFiltered.first?.bfNumber ?? "")"
                            
                            tourListFiltered.first?.addToPaymentRelation(paymentObject)
                            
                            
                        }
                    }
                }
                
                
                
                
                
                if let receiptList = bfnListDetailsResponse["BOOKING_DETAILS_RESPONSE"]["RECEIPT_DETAILS"]["RECEIPT"].all {
                    
                    for receiptDetail in receiptList {
                        
                        
                        
                        
                        
                        let receiptNumber = receiptDetail["RECEIPT_NO"].value
                        
                        
                        if  receiptNumber != nil && !(receiptNumber?.isEmpty)! {
                            
                            
                            
                            let receiptObject:PaymentReceipt = NSEntityDescription.insertNewObject(forEntityName: "PaymentReceipt", into: CoreDataController.getContext()) as! PaymentReceipt
                            
                            receiptObject.receiptNumber = "\(receiptNumber ?? "")"
                            
                            
                            
                            
                            if let receiptDate = receiptDetail["RECEIPT_DATE"].value {
                                
                                
                                let dateFormatter = DateFormatter()
                                dateFormatter.dateFormat = "dd-MM-yyyy" //Your date format
                                dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                                dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
                                let date = dateFormatter.date(from: receiptDate) //according to date format your date string
                                
                                receiptObject.receiptDate = date! as NSDate
                                
                            }
                            if let receiptCurrency = receiptDetail["RECEIPT_CURRENCY"].value {
                                
                                receiptObject.receiptCurrency = "\(receiptCurrency)"
                                
                            }
                            if let receiptAmount = receiptDetail["RECEIPT_AMOUNT"].value {
                                
                                receiptObject.receiptAmount = "\(receiptAmount)"
                                
                            }
                            if let receiptType = receiptDetail["RECEIPT_TYPE"].value {
                                
                                receiptObject.receiptType = "\(receiptType)"
                                
                            }
                            if let receiptURL = receiptDetail["RECEIPT_URL"].value {
                                
                                receiptObject.receiptURL = "\(receiptURL)"
                                
                            }
                            
                            receiptObject.bfNumber = tourListFiltered.first?.bfNumber
                            
                            tourListFiltered.first?.addToReceiptRelation(receiptObject)
                            
                        }
                    }
                }
                
                if let passengerDocumentList = bfnListDetailsResponse["BOOKING_DETAILS_RESPONSE"]["PASSENGER_DOCUMENT_DETAILS"]/*["PASSENGER_DOCUMENT"]*/.all {
                    
                    
                    
                    for passengerDocument in passengerDocumentList {
                        
                        if let documents = passengerDocument["DOCUMENT"].all{
                            
                            for document in documents{
                                
                                
                                
                                let passengerNumber = passengerDocument["PASSENGER_NO"].value
                                
                                
                                
                                let documentType = document["DOCUMENT_TYPE"].value
                                
                                
                                if passengerNumber != nil && documentType != nil && !(passengerNumber?.isEmpty)! && !(documentType?.isEmpty)! {
                                    
                                    let tourPassengerDouments:TourPassengerDouments = NSEntityDescription.insertNewObject(forEntityName: "TourPassengerDouments", into: CoreDataController.getContext()) as! TourPassengerDouments
                                    
                                    tourPassengerDouments.passengerNumber = "\(passengerNumber ?? "")"
                                    tourPassengerDouments.documentType = "\(documentType ?? "")"
                                    
                                    if let documentURL = document["DOCUMENT_URL"].value {
                                        
                                        tourPassengerDouments.docummentURL = "\(documentURL)"
                                        
                                    }
                                    
                                    tourPassengerDouments.bfNumber = tourListFiltered.first?.bfNumber
                                    
                                    tourPassengerDouments.bfNumber_documentType_passengerNumber = "\(tourListFiltered.first?.bfNumber ?? "")_\(documentType ?? "")_\(passengerNumber ?? "")"
                                    
                                    tourListFiltered.first?.addToPassengerDocumentRelation(tourPassengerDouments)
                                    
                                }
                            }
                        }
                        
                    }
                }
                
                
                
                
                if let deviationList = bfnListDetailsResponse["BOOKING_DETAILS_RESPONSE"]["DEVIATION_DETAILS"]["DEVIATION"].all {
                    
                    
                    
                    
                    for deviationDetail in deviationList {
                        
                        let deviationObject:Deviation = NSEntityDescription.insertNewObject(forEntityName: "Deviation", into: CoreDataController.getContext()) as! Deviation
                        
                        
                        
                        if let passengerNumber = deviationDetail["PASSENGER_NO"].value {
                            
                            deviationObject.passengerNumber = "\(passengerNumber)"
                            
                        }
                        if let sector = deviationDetail["SECTOR"].value {
                            
                            deviationObject.sector = "\(sector)"
                            
                            
                        }
                        if let startDate = deviationDetail["START_DATE"].value {
                            
                            let dateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "dd-MM-yyyy" //Your date format
                            dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                            dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
                            let date = dateFormatter.date(from: startDate) //according to date format your date string
                            
                            
                            deviationObject.startDate = (date! as NSDate)
                            
                            
                            
                        }
                        if let newDate = deviationDetail["NEW_DATE"].value {
                            
                            
                            
                            
                            let dateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "dd-MM-yyyy" //Your date format
                            dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                            dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
                            let date = dateFormatter.date(from: newDate) //according to date format your date string
                            
                            
                            deviationObject.newDate = (date! as NSDate)
                            
                            
                            
                            
                            
                        }
                        if let status = deviationDetail["STATUS"].value {
                            
                            deviationObject.status = "\(status)"
                        }
                        
                        deviationObject.bfNumber = tourListFiltered.first?.bfNumber
                        
                        
                        
                        
                        
                        tourListFiltered.first?.addToDeviationRelation(deviationObject)
                        
                        
                    }
                    
                }
                
                
                
                if let optionalList = bfnListDetailsResponse["BOOKING_DETAILS_RESPONSE"]["OPTIONAL_DETAILS"]["OPTIONAL"].all {
                    
                    for optionalPackageDetail in optionalList {
                        
                        let optionalCode = optionalPackageDetail["OPTIONAL_CODE"].value
                        
                        if optionalCode != nil && !(optionalCode?.isEmpty)! {
                            
                            let optionalPackageObject:OptionalPackageBooked = NSEntityDescription.insertNewObject(forEntityName: "OptionalPackageBooked", into: CoreDataController.getContext()) as! OptionalPackageBooked
                            
                            optionalPackageObject.optionalCode = "\(optionalCode ?? "")"
                            
                            if let status = optionalPackageDetail["STATUS"].value {
                                
                                optionalPackageObject.bookStatus = "\(status)"
                            }
                            
                            if let passengerNumber = optionalPackageDetail["PASSENGER_NO"].value {
                                
                                optionalPackageObject.passengerNumber = "\(passengerNumber)"
                                
                                
                                
                                
                                
                                optionalPackageObject.optionalCode_bfNumber = "\(optionalCode ?? "")_\(tourListFiltered.first?.bfNumber ?? "")_\(passengerNumber )"
                                
                                optionalPackageObject.bfNumber = tourListFiltered.first?.bfNumber
                                
                                tourListFiltered.first?.addToBookedOptionalPackageRelation(optionalPackageObject)
                                
                            }
                        }
                        
                    }
                    
                }
                
                if let visaCountryMasterArray = bfnListDetailsResponse["BOOKING_DETAILS_RESPONSE"]["VISA_COUNTRY"]["COUNTRY"].all {
                    
                    for visaCountryDetail in visaCountryMasterArray {
                        
                        let countryName = visaCountryDetail["NAME"].value
                        
                        if countryName != nil && !(countryName?.isEmpty)! {
                            
                            let visaCountryObject:VisaCountryMaster = NSEntityDescription.insertNewObject(forEntityName: "VisaCountryMaster", into: CoreDataController.getContext()) as! VisaCountryMaster
                            
                            visaCountryObject.countryName = "\(countryName ?? "")"
                            
                            visaCountryObject.countryName_bfNumber = "\(countryName ?? "")_\(tourListFiltered.first?.bfNumber ?? "")"
                            
                            tourListFiltered.first?.addToVisaCountryRelation(visaCountryObject)
                            
                        }
                    }
                    
                }
                
                if let businessUnit = bfnListDetailsResponse["BOOKING_DETAILS_RESPONSE"]["BUSINESS_UNIT"].value {
                    
                    tourListFiltered.first?.businessUnit = "\(businessUnit)"
                    
                }
                if let createdBy = bfnListDetailsResponse["BOOKING_DETAILS_RESPONSE"]["CREATED_BY"].value {
                    
                    tourListFiltered.first?.createdBy = "\(createdBy)"
                }
                
                 if let piUrl = bfnListDetailsResponse["BOOKING_DETAILS_RESPONSE"]["PI_URL"].value {
                 
                    tourListFiltered.first?.performInoiceDocURL = "\(piUrl)"
                 
                 }
                
                if let branch = bfnListDetailsResponse["BOOKING_DETAILS_RESPONSE"]["BRANCH"].value {
                    
                    tourListFiltered.first?.branch = "\(branch)"
                    
                }
                if let visaDocUrl = bfnListDetailsResponse["BOOKING_DETAILS_RESPONSE"]["VISA_DOC_URL"].value {
                    
                    tourListFiltered.first?.visaDocUrl = "\(visaDocUrl)"
                    
                }
                
                if let heKitFlag = bfnListDetailsResponse["BOOKING_DETAILS_RESPONSE"]["HE_KIT_FLAG"].value {
                    
                    tourListFiltered.first?.heKitFlag = "\(heKitFlag)"
                    
                }
                if let paymentFlag = bfnListDetailsResponse["BOOKING_DETAILS_RESPONSE"]["PAYMENT_FLAG"].value {
                    
                    tourListFiltered.first?.paymentFlag = "\(paymentFlag)"
                    
                }
                if let productBookingStatus = bfnListDetailsResponse["BOOKING_DETAILS_RESPONSE"]["PRODUCT_BOOKING_STATUS"].value {
                    
                    tourListFiltered.first?.productBookingStatus = "\(productBookingStatus)"
                    
                }
                
                if let kycFlag = bfnListDetailsResponse["BOOKING_DETAILS_RESPONSE"]["KYC"].value {
                    
                    tourListFiltered.first?.kyc = "\(kycFlag)"
                    
                }
                if let tourName = bfnListDetailsResponse["BOOKING_DETAILS_RESPONSE"]["TOUR_NAME"].value {
                    
                    tourListFiltered.first?.tourName = "\(tourName)"
                    
                }
                
                if let noOfNights = bfnListDetailsResponse["BOOKING_DETAILS_RESPONSE"]["NO_OF_DAYS"].value {
                    
                    tourListFiltered.first?.noOfNights = "\(noOfNights)"
                    
                }
                
                if let masterUserMobileNumber = bfnListDetailsResponse["BOOKING_DETAILS_RESPONSE"]["MOBILE"].value {
                    
                    tourListFiltered.first?.mobileNumber = "\(masterUserMobileNumber)"
                    
                }
                
                if let masterUserEmailID = bfnListDetailsResponse["BOOKING_DETAILS_RESPONSE"]["EMAIL"].value {
                    
                    tourListFiltered.first?.emailID = "\(masterUserEmailID)"
                }
                
                if let arrivalDate = bfnListDetailsResponse["BOOKING_DETAILS_RESPONSE"]["ARRIVAL_DATE"].value {
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "dd-MM-yyyy" //Your date format
                    dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                    dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
                    let date = dateFormatter.date(from: arrivalDate) //according to date format your date string
                    
                    
                    tourListFiltered.first?.arrivalDate = (date! as NSDate) as Date
                    
                }
                if let departureDate = bfnListDetailsResponse["BOOKING_DETAILS_RESPONSE"]["DEPARTURE_DATE"].value {
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "dd-MM-yyyy" //Your date format
                    dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                    dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
                    let date = dateFormatter.date(from: departureDate) //according to date format your date string
                    
                    
                    tourListFiltered.first?.departureDate = (date! as NSDate) as Date
                    
                }
                // VJ_Added_Start on 14/08/20
                if let bookingDate = bfnListDetailsResponse["BOOKING_DETAILS_RESPONSE"]["BOOKING_DATE"].value {
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "dd-MM-yyyy" //Your date format
                    dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                    dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
                    let date = dateFormatter.date(from: bookingDate) //according to date format your date string
                    
                    
                    tourListFiltered.first?.bookingDate = (date! as NSDate) as Date
                    
                }
                CoreDataController.getContext().mergePolicy = CoreDataConstant.dataMergePolicy
                
                
                CoreDataController.saveContext()
                
            }
            
            
            return "Data Insert Successfully"
        }
        catch
        {
            printLog(error)
            
            return "Failed To Insert Data"
        }
        
    }
    
    
    class func saveBFNOptionalDetails(bfnOptionalDetailsResponse:AEXMLDocument,bfNumber:String)
    {
        
        do
        {
            
              UserBookingDBManager.DeleteAllOptionalMasterData(bfnNumber: bfNumber)
            
            
            let fetchRequest: NSFetchRequest<Tour> = Tour.fetchRequest()
            
            let tourList = try CoreDataController.getContext().fetch(fetchRequest)
            
            let tourListFiltered = tourList.filter({ (Tour) -> Bool in
                
                Tour.bfNumber == bfNumber
            })
            
            if let optionalPackageList = bfnOptionalDetailsResponse["BOOKING_OPTIONAL_RESPONSE"]["OPTIONAL_DETAILS"].all {
                
                
                for optionalPackageDetail in optionalPackageList {
                    
                    
                    let optionalCode = optionalPackageDetail["OPTIONAL_CODE"].value
                    
                    if optionalCode != nil && !(optionalCode?.isEmpty)! {
                        
                        
                        let optionalPackageObject:OptionalMaster = NSEntityDescription.insertNewObject(forEntityName: "OptionalMaster", into: CoreDataController.getContext()) as! OptionalMaster
                        optionalPackageObject.optionalCode = "\(optionalCode ?? "")"
                        
                        
                        
                        if let optionalDescription = optionalPackageDetail["OPTIONAL_DESCRIPTION"].value {
                            
                            optionalPackageObject.optionalDescription = "\(optionalDescription)"
                            
                            
                        }
                        if let optionalCurrency = optionalPackageDetail["OPTIONAL_CURRENCY"].value {
                            
                            optionalPackageObject.optionalCurrency = "\(optionalCurrency)"
                            
                        }
                        if let adultSP = optionalPackageDetail["ADULT_SP"].value {
                            
                            optionalPackageObject.adultSP = "\(adultSP)"
                            
                        }
                        if let childSP = optionalPackageDetail["CHILD_SP"].value {
                            
                            optionalPackageObject.childSP = "\(childSP)"
                        }
                        if let infantSP = optionalPackageDetail["INFANT_SP"].value {
                            
                            optionalPackageObject.infantSP = "\(infantSP)"
                        }
                        
                        
                        optionalPackageObject.bfNumber = tourListFiltered.first?.bfNumber
                        
                        optionalPackageObject.optionalCode_bfNumber = "\(optionalCode ?? "")_\(tourListFiltered.first?.bfNumber ?? "")"
                        
                        tourListFiltered.first?.addToOptionalMasterRelation(optionalPackageObject)
                        
                    }
                }
                
            }
            
            
            
            
            CoreDataController.getContext().mergePolicy = CoreDataConstant.dataMergePolicy
            
            
            CoreDataController.saveContext()
        }
        catch
        {
            printLog(error)
        }
        
        
        
    }
    
    class func saveUserBookedImageAndCityCovered(bookingTourList:Array<Tour>,packageDetailsResponseData:Dictionary<String,Any>) -> Bool {
        
        
        if let packageDetailsResponseArray:Array<Dictionary<String,Any>> = packageDetailsResponseData["data"] as? Array<Dictionary<String,Any>>
        {
            
            for tourObject in bookingTourList
            {
                
                let packageDetailsResponseArrayTemp:Array<Any> = packageDetailsResponseArray.filter({ (packageDetailsDict :Dictionary<String,Any>) -> Bool in
                    
                    
                    "\(packageDetailsDict["ltItineraryCode"] ?? "")" == tourObject.prodITINCode
                    
                })
                printLog("After Filter packageDetailsResponseArray \(packageDetailsResponseArrayTemp)")
                if packageDetailsResponseArrayTemp != nil && packageDetailsResponseArrayTemp.count > 0   {
                    
                    
                    if let packageDetailsDict :Dictionary<String,Any> = packageDetailsResponseArrayTemp.first as? Dictionary<String,Any>
                    {
                        printLog("packageDetailsDict \(packageDetailsDict)")
                        printLog("packageDetailsDictimageSet \(packageDetailsDict["imageSet"])")
                        if let prodITINCode:String =  packageDetailsDict["ltItineraryCode"] as? String
                        {                            
                            let packageId:String? =  (packageDetailsDict["packageId"] as? String)!
                            
                            if packageId != nil &&  !(packageId?.isEmpty)! {
                                
                                
                                tourObject.packageId = packageId
                                
                                if let pkgName:String =  packageDetailsDict["pkgName"] as? String
                                {
                                    tourObject.packageName = pkgName
                                }
                                
                                
                                if let timeLineSetArray:Array<Dictionary<String,Any>> = packageDetailsDict["timelineSet"] as? Array<Dictionary<String,Any>>
                                {
                                    if let citySetArray:Array<Dictionary<String,Any>> = packageDetailsDict["citySet"] as? Array<Dictionary<String,Any>>
                                    {
                                        
                                        
                                        for timeLine in timeLineSetArray
                                        {
                                            
                                            
                                            if let timeLineDict:Dictionary<String,Any> = timeLine as? Dictionary<String,Any>
                                            {
                                                
                                                
                                                
                                                
                                                let filterCityArray =  citySetArray.filter(){ $0["cityCode"] as? String
                                                    == timeLine["cityCode"] as? String
                                                    
                                                }
                                                
                                                if filterCityArray != nil && filterCityArray.count > 0
                                                {
                                                    var city = filterCityArray.first
                                                    
                                                    let cityCode:String? =  (city?["cityCode"] as? String)!
                                                    
                                                    if cityCode != nil && !(cityCode?.isEmpty)! {
                                                        
                                                        let cityCovered:CityCovered
                                                            = NSEntityDescription.insertNewObject(forEntityName: "CityCovered", into: CoreDataController.getContext()) as! CityCovered
                                                        
                                                        cityCovered.cityCode = cityCode
                                                        
                                                        
                                                        if let cityName:String =  city?["cityName"] as? String
                                                        {
                                                            cityCovered.cityName = cityName
                                                            
                                                        }
                                                        
                                                        if let holidayTimelineId:Int =  timeLine["holidayTimelineId"] as? Int
                                                        {
                                                            cityCovered.holidayTimelineId = String(holidayTimelineId)
                                                            
                                                        }
                                                        
                                                        if let iconId:Int =  timeLine["iconId"] as? Int
                                                        {
                                                            cityCovered.iconId = String(iconId)
                                                            
                                                        }
                                                        
                                                        if let isActive:String =  timeLine["isActive"] as? String
                                                        {
                                                            cityCovered.isActive = isActive
                                                            
                                                        }
                                                        
                                                        if let noOfNights:Int =  timeLine["noOfNights"] as? Int
                                                        {
                                                            cityCovered.noOfNights = String(noOfNights)
                                                            
                                                        }
                                                        
                                                        if let packageId:String =  timeLine["packageId"] as? String
                                                        {
                                                            
                                                            cityCovered.packageId = packageId
                                                            
                                                        }
                                                        
                                                        if let position =  timeLine["position"]
                                                        {
                                                            cityCovered.position = "\(position)"
                                                            
                                                        }
                                                        
                                                        
                                                        
                                                        cityCovered.prodITINCode = prodITINCode
                                                        
                                                        cityCovered.prodITINCode_packageId_cityCode = "\(tourObject.bfNumber)_\(packageId ?? "")_\(cityCode ?? "")"
                                                        
                                                        tourObject.addToCityCoveredRelation(cityCovered)  //cityCoveredRelation?.adding(cityCovered)
                                                        
                                                    }
                                                    
                                                    
                                                }
                                                
                                                printLog(filterCityArray)
                                            }
                                            
                                        }
                                        
                                        
                                        
                                    }
                                    
                                }
                                
                                if let imageSetSetArray:Array<Dictionary<String,Any>> = packageDetailsDict["imageSet"] as? Array<Dictionary<String,Any>>
                                {
                                    
                                    for imageSet in imageSetSetArray
                                    {
                                        //if imageSetSetArray.count > 0
                                        
                                        // let imageSet = imageSetSetArray.first
                                        
                                        if let imageDict:Dictionary<String,Any> = imageSet as? Dictionary<String,Any>
                                        {
                                            
                                            
                                            
                                            if let imageId:Int =  imageDict["imageId"] as? Int
                                            {
                                                
                                                let tourPackageImages:TourPackageImages
                                                    = NSEntityDescription.insertNewObject(forEntityName: "TourPackageImages", into: CoreDataController.getContext()) as! TourPackageImages
                                                
                                                tourPackageImages.imageId = String(imageId)
                                                
                                                
                                                if let entityType:String =  imageDict["entity"] as? String ?? "" //as? String ?? ""
                                                {
                                                    tourPackageImages.entityType = entityType
                                                }
                                                
                                                
                                                
                                                if let imageOrder:Int =  imageDict["imageOrder"] as? Int
                                                {
                                                    tourPackageImages.imageOrder = String(imageOrder)
                                                }
                                                
                                                if let imageTitle:String =  imageDict["imageTitle"] as? String
                                                {
                                                    tourPackageImages.itineraryImageTitle = imageTitle
                                                }
                                                
                                                if let packageId:String =  imageDict["packageId"] as? String
                                                {
                                                    tourPackageImages.packageId = packageId
                                                    
                                                }
                                                
                                                if let path:String =  imageDict["path"] as? String
                                                {
                                                    tourPackageImages.path = path
                                                }
                                                
                                                
                                                
                                                tourPackageImages.imageId_packageId_prodITINCode = "\(tourObject.bfNumber ?? "")_\(packageId ?? "")_\(imageId)"
                                                tourPackageImages.prodITINCode = prodITINCode
                                                
                                                tourObject.addToPackageImagesRelation(tourPackageImages)   //adding(TourPackageImages)
                                                
                                            }
                                            
                                            
                                            
                                            
                                        }
                                    }
                                    
                                }
                                
                                
                            }
                            
                            
                            
                        }
                        
                    }
                    
                }
                
                
            }
            
        
            
            CoreDataController.getContext().mergePolicy = CoreDataConstant.dataMergePolicy
            
            
            CoreDataController.saveContext()
            
            return true
            
        }else{
            
            return false        }
        
    }
    
    class func saveUserItienearyImageAndItienearyDetails(bfNumber:String,itienearyDetailsResponseData:Dictionary<String,Any>) -> Bool {
        
        do{
            UserBookingDBManager.DeleteAllItineraryImageData(bfnNumber: bfNumber)
            UserBookingDBManager.DeleteAllItineraryData(bfnNumber: bfNumber)
            
            
            if let itienearyDetailsDataArray:Array<Any> = itienearyDetailsResponseData["data"] as? Array<Any>
            {
                
               
                
                let fetchRequest: NSFetchRequest<Tour> = Tour.fetchRequest()
                
                let tourList = try CoreDataController.getContext().fetch(fetchRequest)
                
                
                for itienearyDetails in itienearyDetailsDataArray
                {
                    if let itienearyDetailsDict :Dictionary<String,Any> = itienearyDetails as? Dictionary<String,Any>
                    {
                        
                        if let packageId:String =  itienearyDetailsDict["packageId"] as? String
                        {
                            if !(packageId.isEmpty) {
                                
                                let tourListFiltered = tourList.filter({ (Tour) -> Bool in
                                    
                                    (Tour.packageId == packageId ) && (Tour.bfNumber == bfNumber )
                                })
                                

                                
                                if tourListFiltered != nil && (tourListFiltered.count) > 0
                                {
                                    let tour:Tour =  (tourListFiltered.first)!
                                    
                                    let tourPassengerDouments:TourPassengerDouments = NSEntityDescription.insertNewObject(forEntityName: "TourPassengerDouments", into: CoreDataController.getContext()) as! TourPassengerDouments
                                    
                                    let defaultPassengerNumber = "-1"
                                    let itinearayDocumentType = Constant.ItiearyDetails.ITIENRARY_DOC
                                    tourPassengerDouments.passengerNumber = defaultPassengerNumber
                                    tourPassengerDouments.documentType = itinearayDocumentType
                                    
                                    if let itinearyDownloadUrl:String =  itienearyDetailsDict["downloadUrl"] as? String
                                    {
                                        
                                        tourPassengerDouments.docummentURL = "\(itinearyDownloadUrl)"
                                        
                                    }
                                    
                                    tourPassengerDouments.bfNumber = tourListFiltered.first?.bfNumber
                                    
                                    tourPassengerDouments.bfNumber_documentType_passengerNumber = "\(tourListFiltered.first?.bfNumber ?? "")_\(itinearayDocumentType)_\(defaultPassengerNumber)"
                                    
                                    tourListFiltered.first?.addToPassengerDocumentRelation(tourPassengerDouments)
                                    
                                    if let itinerarySetArray:Array<Dictionary<String,Any>> = itienearyDetailsDict["itinerarySet"] as? Array<Dictionary<String,Any>>
                                    {
                                        if let citySetArray:Array<Dictionary<String,Any>> = itienearyDetailsDict["citySet"] as? Array<Dictionary<String,Any>>
                                        {
                                            
                                            
                                            for itinerarySet in itinerarySetArray
                                            {
                                                
                                                
                                                
                                                if let itineraryDict:Dictionary<String,Any> = itinerarySet as? Dictionary<String,Any>
                                                {
                                                    
                                                    let filterCityArray =  citySetArray.filter(){ $0["cityCode"] as? String
                                                        == itineraryDict["cityCode"] as? String
                                                        
                                                    }
                                                    
                                                    if filterCityArray != nil && filterCityArray.count > 0
                                                    {
                                                        var city = filterCityArray.first
                                                        
                                                        let holidayTimelineId:Int? =  itineraryDict["holidayItineraryId"] as! Int
                                                        
                                                        
                                                        if holidayTimelineId != nil
                                                        {
                                                            let itineraryDetail:ItineraryDetail
                                                                = NSEntityDescription.insertNewObject(forEntityName: "ItineraryDetail", into: CoreDataController.getContext()) as! ItineraryDetail
                                                            
                                                            itineraryDetail.holidayItineraryId =  "\(holidayTimelineId ?? 0)"
                                                            
                                                            
                                                            
                                                            if let cityCode:String =  city?["cityCode"] as? String ?? ""
                                                            {
                                                                itineraryDetail.cityCode = cityCode
                                                                
                                                            }
                                                            
                                                            if let cityName:String =  city?["cityName"] as? String
                                                            {
                                                                itineraryDetail.cityName = cityName
                                                                
                                                            }
                                                            
                                                            if let ltCityCode:String =  city?["ltCityCode"] as? String ?? ""
                                                            {
                                                                itineraryDetail.ltCityCode = ltCityCode
                                                                
                                                            }
                                                            
                                                            
                                                            if let image:String =  itineraryDict["image"] as? String
                                                            {
                                                                itineraryDetail.image = image
                                                                
                                                            }
                                                            
                                                            if let highlights:String =  itineraryDict["highlights"] as? String ?? ""
                                                            {
                                                                itineraryDetail.highlights = highlights
                                                                
                                                            }
                                                            
                                                            if let itineraryDay:Int =  itineraryDict["itineraryDay"] as? Int
                                                            {
                                                                itineraryDetail.itineraryDay = String(itineraryDay)
                                                                
                                                            }
                                                            
                                                            if let itineraryDescription:String =  itineraryDict["itineraryDescription"] as? String ?? ""
                                                            {
                                                                
                                                                let itinearyDesciptionData = itineraryDescription.data(using: .utf8)
                                                                itineraryDetail.itineraryDescription = itinearyDesciptionData as? NSData
                                                                
                                                            }
                                                            
                                                            if let overnightIconId:String =  itineraryDict["overnightIconId"] as? String ?? ""
                                                            {
                                                                itineraryDetail.overnightIconId = overnightIconId
                                                                
                                                            }
                                                            
                                                            if let overnightStay:String =  itineraryDict["overnightStay"] as? String ?? ""
                                                            {
                                                                itineraryDetail.overnightStay = overnightStay
                                                                
                                                            }
                                                            
                                                            if let overnightStay:String =  itineraryDict["overnightStay"] as? String ?? ""
                                                            {
                                                                itineraryDetail.overnightStay = overnightStay
                                                                
                                                            }
                                                            
                                                            
                                                            itineraryDetail.prodITINCode_packageId_holidayItineraryId = "\(packageId)_\(holidayTimelineId ?? 0)_\(bfNumber)"
                                                            
                                                            itineraryDetail.packageId = packageId
                                                            
                                                            tour.addToItineraryRelation(itineraryDetail)   //iti?.adding(cityCovered)
                                                            
                                                            
                                                        }
                                                        
                                                        
                                                        
                                                    }
                                                    
                                                    printLog(filterCityArray)
                                                }
                                                
                                            }
                                            
                                            
                                            
                                        }
                                        
                                    }
                                    
                                    if let imageSetSetArray:Array<Dictionary<String,Any>> = itienearyDetailsDict["imageSet"] as? Array<Dictionary<String,Any>>
                                    {
                                        
                                        for imageSet in imageSetSetArray
                                        {
                                            
                                            
                                            if let imageDict:Dictionary<String,Any> = imageSet as? Dictionary<String,Any>
                                            {
                                                
                                                
                                                
                                                if let imageId:Int =  imageDict["imageId"] as? Int
                                                {
                                                    let itineraryImages:ItineraryImages
                                                        = (NSEntityDescription.insertNewObject(forEntityName: "ItineraryImages", into: CoreDataController.getContext()) as! ItineraryImages)
                                                    
                                                    itineraryImages.imageId = String(imageId)
                                                    
                                                    if let entityType:String =  imageDict["entity"] as? String ?? "" //as? String ?? ""
                                                    {
                                                        itineraryImages.entityType = entityType
                                                    }
                                                    
                                                    
                                                    
                                                    if let imageOrder:String =  imageDict["imageOrder"] as? String ?? ""
                                                    {
                                                        itineraryImages.imageOrder = imageOrder
                                                    }
                                                    
                                                    if let imageTitle:String =  imageDict["imageTitle"] as? String ?? ""
                                                    {
                                                        itineraryImages.itineraryImageTitle = imageTitle
                                                    }
                                                    
                                                    if let packageId:String =  imageDict["packageId"] as? String ?? ""
                                                    {
                                                        itineraryImages.packageId = packageId
                                                    }
                                                    
                                                    if let path:String =  imageDict["path"] as? String ?? ""
                                                    {
                                                        itineraryImages.path = path
                                                    }
                                                    
                                                    
                                                    itineraryImages.imageId_packageId_prodITINCode = "\(imageId)_\(packageId)_\(bfNumber)"
                                                    
                                                    tour.addToTourImagesRelation(itineraryImages)
                                                }
                                                
                                                
                                                
                                                
                                            }
                                        }
                                        
                                    }
                                    
                                }
                                
                            }
                            
                        }
                        
                    }
                    
                }
                
                
                CoreDataController.getContext().mergePolicy = CoreDataConstant.dataMergePolicy
                
                
                CoreDataController.saveContext()
                
                return true
                
            }
            
            return false
            
        }
        catch{
            
            printLog(error)
            
            return false
        }
        
    }
    
    class func parseCurrentFxRatesList(currencyDetailsResponseData:Dictionary<String,Any>) -> Array<CurrencyData>
    {
        var rateOfExchangeArray:Array<CurrencyData> = []
        
        if let currencyDetailsArray:Array<Any> = currencyDetailsResponseData["data"] as? Array<Any>
        {
            
            
            for currencyDetails in currencyDetailsArray {
                
                var courrencyData:CurrencyData = CurrencyData.init()
                
                if let currencyDict:Dictionary<String,Any> = currencyDetails as? Dictionary<String,Any>
                {
                    
                    if let exchangeRateId:Int =  currencyDict["exchangeRateId"] as? Int
                    {
                        courrencyData.exchangeRateId = String(exchangeRateId)
                        
                    }
                    
                    if let currencyRate:Double =  currencyDict["currencyRate"] as? Double
                    {
                        courrencyData.currencyRate = String(currencyRate)
                        
                    }
                    if let currencyName:String =  currencyDict["currencyName"] as? String
                    {
                        courrencyData.currencyName = currencyName
                        
                    }
                    
                    let todayDate = Date()
                    
                    let dateString:String = AppUtility.convertDateToString(date: todayDate)
                    
                    courrencyData.currencyDate = dateString
                    
                    // this is used in unique constraints
                    if let currencyCode:String =  currencyDict["currencyCode"] as? String
                    {
                        courrencyData.currencyCode = currencyCode
                    }
                    rateOfExchangeArray.append(courrencyData)
                    
                }
                
                
            }
            
            
        }
        
        return rateOfExchangeArray
        
    }
    
    /*class func saveCurrentFxRatesList(currencyDetailsResponseData:Dictionary<String,Any>)
    {
        
            if let currencyDetailsArray:Array<Any> = currencyDetailsResponseData["data"] as? Array<Any>
            {
                
                for currencyDetails in currencyDetailsArray {
                    
                    let courrency:Currency = NSEntityDescription.insertNewObject(forEntityName: "Currency", into: CoreDataController.getContext()) as! Currency
                    
                    if let currencyDict:Dictionary<String,Any> = currencyDetails as? Dictionary<String,Any>
                    {
                        
                        if let exchangeRateId:Int =  currencyDict["exchangeRateId"] as? Int
                        {
                            courrency.exchangeRateId = Int64(exchangeRateId)
                            
                        }
                        
                        if let currencyRate:Double =  currencyDict["currencyRate"] as? Double
                        {
                            courrency.currencyRate = String(currencyRate)
                            
                        }
                        if let currencyName:String =  currencyDict["currencyName"] as? String
                        {
                            courrency.currencyName = currencyName
                            
                        }
                        
                        let todayDate = Date()
                        
                        let dateString:String = AppUtility.convertDateToString(date: todayDate)
                        
                        courrency.currencyDate = dateString
                        
                        // this is used in unique constraints
                        if let currencyCode:String =  currencyDict["currencyCode"] as? String
                        {
                            courrency.currencyCode = currencyCode
                            
                            //                            if !currencyCode.isEmpty && currencyCode != ""
                            //                            {
                            //                                let currencyCode_currencyDate = currencyCode + "_" + dateString
                            //
                            //                            }
                            
                            if let currencyRate:Double =  currencyDict["currencyRate"] as? Double
                            {
                                courrency.currencyRate = String(currencyRate)
                                
                                CoreDataController.getContext().mergePolicy = CoreDataConstant.dataMergePolicy
                                
                                CoreDataController.saveContext()
                                
                            }
                        }
                        
                    }
                    
                    
                }
                
                
            }
            
        
    }*/
    
    
    class func saveNotificationDetailsInDB(notificationData:Dictionary<String,Any>,mobileNumber:String)
    {
        if let dataString:String = notificationData["data"] as? String
        {
            if let data = dataString.data(using: .utf8) {
                do {
                    
                    let dataDict:[String: Any] =  try JSONSerialization.jsonObject(with: data, options: []) as! [String: Any]
                    
                    
                    
                    let notifications:Notifications = NSEntityDescription.insertNewObject(forEntityName: "Notifications", into: CoreDataController.getContext()) as! Notifications
                    
                    
                    notifications.userMobileNumber = mobileNumber
                    
                    
                    if let message = dataDict["message"] as? String
                    {
                        notifications.message = message
                    }
                    
                    if let title = dataDict["title"] as? String
                    {
                        notifications.title = title
                    }
                    if let imageUrl = dataDict["imageUrl"] as? String
                    {
                        notifications.imageURL = imageUrl
                    }
                    
                    if let notificationType = dataDict["notificationType"] as? String
                    {
                        notifications.type = notificationType
                    }
                    
                    if let redirect = dataDict["redirect"] as? String
                    {
                        notifications.redirect = redirect
                    }
                    
                    if let subTitle = dataDict["subTitle"] as? String
                    {
                        notifications.subTitle = subTitle
                    }
                    
                    if let bfn = dataDict["bfn"] as? String
                    {
                        notifications.bfNumber =
                        bfn
                    }
                    
                    if let createdOn = dataDict["createdOn"] as? String
                    {
                        let date:NSDate? = Date(timeIntervalSince1970: (TimeInterval(createdOn)! / 1000.0)) as NSDate
                        
                        if date != nil
                      {
                        
                        notifications.createdOn = date
                        
                      }
                        
                    }
                    CoreDataController.getContext().mergePolicy = CoreDataConstant.dataMergePolicy
                    
                    CoreDataController.saveContext()
                    
                } catch {
                    printLog(error.localizedDescription)
                }
            }
            
            
        }
    }

    class func parseCurrencyByCode(curencyByCodeResponseData:Dictionary<String,Any>)->CurrencyData?
    {
        
        
        if let currencyDetailsDict: Dictionary<String,Any> = curencyByCodeResponseData["currencyObj"] as? Dictionary<String,Any>
        {
            var courrencyData:CurrencyData = CurrencyData.init()
            
            if let exchangeRateId:Int =  currencyDetailsDict["exchangeRateId"] as? Int
            {
                courrencyData.exchangeRateId = String(exchangeRateId)
                
            }
            if let currencyRate:Double =  currencyDetailsDict["currencyRate"] as? Double
            {
                courrencyData.currencyRate = String(currencyRate)
                
            }
            if let currencyName:String =  currencyDetailsDict["currencyName"] as? String
            {
                courrencyData.currencyName = currencyName
                
            }
            
            let todayDate = Date()
            
            let dateString:String = AppUtility.convertDateToString(date: todayDate)
            
            courrencyData.currencyDate = dateString
            
            return courrencyData
            
        }
        
        return nil
    }
    
    class func saveLocaleNotificationDetailsInDB(notificationData:Dictionary<String,String>,mobileNumber:String , fireDateTime:Date)
    {
        
        let notifications:Notifications = NSEntityDescription.insertNewObject(forEntityName: "Notifications", into: CoreDataController.getContext()) as! Notifications
        
        
        notifications.userMobileNumber = mobileNumber
        
        
        if let message = notificationData["message"] as? String
        {
            notifications.message = message
        }
        
        if let title = notificationData["title"]
        {
            notifications.title = title
        }
        if let imageUrl = notificationData["imageUrl"]
        {
            notifications.imageURL = imageUrl
        }
        
        if let notificationType = notificationData["notificationType"]                     {
            notifications.type = notificationType
        }
        
        if let redirect = notificationData["redirect"]                    {
            notifications.redirect = redirect
        }
        
        if let subTitle = notificationData["subTitle"]
        {
            notifications.subTitle = subTitle
        }
        
        if let bfn = notificationData["bfn"]
        {
            notifications.bfNumber =
            bfn
        }
        
        if let createdOn = notificationData["createdOn"]
        {
            notifications.createdOn = fireDateTime as NSDate
            
        }
        CoreDataController.getContext().mergePolicy = CoreDataConstant.dataMergePolicy
        
        CoreDataController.saveContext()
        
    }
    
    class func isPaymentDone(by currency:String,tourDetails:Tour) -> Bool
    {
        if let paymentArray:Array<Payment> = tourDetails.paymentRelation?.allObjects as? Array<Payment>
        {
            let filterPaymentArray:Array<Payment> = paymentArray.filter(){$0.currency?.uppercased() == currency.uppercased()}
            
            if filterPaymentArray.count > 0
            {
                if  let totalReceivable:String =  filterPaymentArray[0].totalReceivable
                {
                    if  let totalReceived:String =  filterPaymentArray[0].totalReceived
                    {
                        let totalReceivableDouble:Double = Double(totalReceivable)!
                        
                        let totalReceivedDouble:Double = Double(totalReceived)!
                        
                        if totalReceivedDouble >= totalReceivableDouble
                        {
                            return true
                        }
                    }
                    
                }
            }
            
        }
        
        
        return false
    }
    
    class func isPaymentDonePaymentFlag(by tourDetails:Tour) -> Bool
    {
        if tourDetails.paymentFlag?.lowercased() == "yes"
        {
            return true
        }
        
        
        return false
    }
    
    /*  class func getTourDetails(by bfnNumber:String)
     {
     do{
     
     let predicate = NSPredicate(format: "bfNumber == '\(bfNumber)'")
     
     let fetchRequest: NSFetchRequest<Tour> = Tour.fetchRequest()
     
     fetchRequest.predicate = predicate
     
     
     let tourListFiltered = try CoreDataController.getContext().fetch(fetchRequest)
     
     }
     catch{
     
     }
     
     }*/
    
    //MARK: Fetch Methods
    
    class func fetchTourDetail(bfNumber:String) -> Tour? {
        
        
        do{
            
            let predicate = NSPredicate(format: "bfNumber == '\(bfNumber ?? "")'")
            
            let fetchRequest: NSFetchRequest<Tour> = Tour.fetchRequest()
            
            fetchRequest.predicate = predicate
            
            
            let tourListFiltered:Array<Tour>? = try CoreDataController.getContext().fetch(fetchRequest)
            
            if tourListFiltered !=  nil && (tourListFiltered?.count)! > 0
            {
                return tourListFiltered?.first
                
            }
          
            
        }
        catch
        {
            return nil
            
        }
        
        return nil
        
    }
    
    
    //MARK: Delete Methods
    
    class func DeleteAllTourAndRelatedData()  {
        
        do {
            
            let userMobileNumber:String = UserDefaults.standard.value(forKey:UserDefauldConstant.userMobileNumber) as! String
            
            let predicate = NSPredicate(format: "mobileNumber == \(userMobileNumber)")
            
            
            let tableFetchRequest: NSFetchRequest<UserProfile> = UserProfile.fetchRequest()
            tableFetchRequest.predicate = predicate
            
            let tableList:Array<UserProfile>? = try CoreDataController.getContext().fetch(tableFetchRequest)
            
            if tableList != nil && (tableList?.count)! > 0 {
                
                
                for item in (tableList?.first?.tourRelation)! {
                    
                    CoreDataController.getContext().delete(item as! NSManagedObject)
                    
                }
                
                try CoreDataController.getContext().save()
                
                
            }
            
            
            
        } catch  {
            printLog(error)
        }
    }
    
    
    
    class  func DeleteTourDetailRelatedData(bfnNumber:String)  {
        
        UserBookingDBManager.DeleteAllVisaData(bfnNumber: bfnNumber)
        UserBookingDBManager.DeleteAllTourDocumentData(bfnNumber: bfnNumber)
        UserBookingDBManager.DeleteAllTourDeviationData(bfnNumber: bfnNumber)
        UserBookingDBManager.DeleteAllPaymentData(bfnNumber: bfnNumber)
        UserBookingDBManager.DeleteAllInsuranceData(bfnNumber: bfnNumber)
        UserBookingDBManager.DeleteAllOptionalPackageData(bfnNumber: bfnNumber)
        UserBookingDBManager.DeleteAllPassengerData(bfnNumber: bfnNumber)
        UserBookingDBManager.DeleteAllVisaCountry(bfnNumber: bfnNumber)
        UserBookingDBManager.DeleteAllPaymentReceiptData(bfnNumber: bfnNumber)
        
        
        
    }
    
    
    
    class func DeleteAllTourDocumentData(bfnNumber:String)  {
        
        do {
            
            
            
            let predicate = NSPredicate(format: "bfNumber == '\(bfnNumber)'")
            
            
            let tableFetchRequest: NSFetchRequest<Tour> = Tour.fetchRequest()
            tableFetchRequest.predicate = predicate
            
            let tableList:Array<Tour>? = try CoreDataController.getContext().fetch(tableFetchRequest)
            
            if tableList != nil && (tableList?.count)! > 0 {
                
                
                for item in (tableList?.first?.passengerDocumentRelation)! {
                    
                    CoreDataController.getContext().delete(item as! NSManagedObject)
                    
                }
                
                try CoreDataController.getContext().save()
                
                
            }
            
            
            
        } catch  {
            printLog(error)
        }
    }
    
    class func DeleteAllTourDeviationData(bfnNumber:String)  {
        
        do {
            
            
            
            let predicate = NSPredicate(format: "bfNumber == '\(bfnNumber)'")
            
            
            let tableFetchRequest: NSFetchRequest<Tour> = Tour.fetchRequest()
            tableFetchRequest.predicate = predicate
            
            let tableList:Array<Tour>? = try CoreDataController.getContext().fetch(tableFetchRequest)
            
            if tableList != nil && (tableList?.count)! > 0 {
                
                
                for item in (tableList?.first?.deviationRelation)! {
                    
                    CoreDataController.getContext().delete(item as! NSManagedObject)
                    
                }
                
                try CoreDataController.getContext().save()
                
                
            }
            
            
            
        } catch  {
            printLog(error)
        }
    }
    class func DeleteAllPaymentData(bfnNumber:String)  {
        
        do {
            
            
            
            let predicate = NSPredicate(format: "bfNumber == '\(bfnNumber)'")
            
            
            let tableFetchRequest: NSFetchRequest<Tour> = Tour.fetchRequest()
            tableFetchRequest.predicate = predicate
            
            let tableList:Array<Tour>? = try CoreDataController.getContext().fetch(tableFetchRequest)
            
            if tableList != nil && (tableList?.count)! > 0 {
                
                
                for item in (tableList?.first?.paymentRelation)! {
                    
                    CoreDataController.getContext().delete(item as! NSManagedObject)
                    
                }
                
                try CoreDataController.getContext().save()
                
                
            }
            
            
            
        } catch  {
            printLog(error)
        }
    }
    class func DeleteAllInsuranceData(bfnNumber:String)  {
        
        do {
            
            
            
            let predicate = NSPredicate(format: "bfNumber == '\(bfnNumber)'")
            
            
            let tableFetchRequest: NSFetchRequest<Tour> = Tour.fetchRequest()
            tableFetchRequest.predicate = predicate
            
            let tableList:Array<Tour>? = try CoreDataController.getContext().fetch(tableFetchRequest)
            
            if tableList != nil && (tableList?.count)! > 0 {
                
                
                for item in (tableList?.first?.insuranceRelation)! {
                    
                    CoreDataController.getContext().delete(item as! NSManagedObject)
                    
                }
                
                try CoreDataController.getContext().save()
                
                
            }
            
            
            
        } catch  {
            printLog(error)
        }
    }
    class func DeleteAllVisaData(bfnNumber:String)  {
        
        do {
            
            
            
            let predicate = NSPredicate(format: "bfNumber == '\(bfnNumber)'")
            
            
            let tableFetchRequest: NSFetchRequest<Tour> = Tour.fetchRequest()
            tableFetchRequest.predicate = predicate
            
            let tableList:Array<Tour>? = try CoreDataController.getContext().fetch(tableFetchRequest)
            
            if tableList != nil && (tableList?.count)! > 0 {
                
                
                for item in (tableList?.first?.visaRelation)! {
                    
                    CoreDataController.getContext().delete(item as! NSManagedObject)
                    
                }
                
                try CoreDataController.getContext().save()
                
                
            }
            
            
            
        } catch  {
            printLog(error)
        }
    }
    
    class func DeleteAllItineraryData(bfnNumber:String)  {
        
        do {
            
            
            
            let predicate = NSPredicate(format: "bfNumber == '\(bfnNumber)'")
            
            
            let tableFetchRequest: NSFetchRequest<Tour> = Tour.fetchRequest()
            tableFetchRequest.predicate = predicate
            
            let tableList:Array<Tour>? = try CoreDataController.getContext().fetch(tableFetchRequest)
            
            if tableList != nil && (tableList?.count)! > 0 {
                
                
                for item in (tableList?.first?.itineraryRelation)! {
                    
                    CoreDataController.getContext().delete(item as! NSManagedObject)
                    
                }
                
                try CoreDataController.getContext().save()
                
                
            }
            
            
            
        } catch  {
            printLog(error)
        }
    }
    class func DeleteAllItineraryImageData(bfnNumber:String)  {
        
        do {
            
            
            
            let predicate = NSPredicate(format: "bfNumber == '\(bfnNumber)'")
            
            
            let tableFetchRequest: NSFetchRequest<Tour> = Tour.fetchRequest()
            tableFetchRequest.predicate = predicate
            
            let tableList:Array<Tour>? = try CoreDataController.getContext().fetch(tableFetchRequest)
            
            if tableList != nil && (tableList?.count)! > 0 {
                
                
                for item in (tableList?.first?.tourImagesRelation)! {
                    
                    CoreDataController.getContext().delete(item as! NSManagedObject)
                    
                }
                
                try CoreDataController.getContext().save()
                
                
            }
            
            
            
        } catch  {
            printLog(error)
        }
    }
    class func DeleteAllOptionalMasterData(bfnNumber:String)  {
        
        do {
            
            
            
            let predicate = NSPredicate(format: "bfNumber == '\(bfnNumber)'")
            
            
            let tableFetchRequest: NSFetchRequest<Tour> = Tour.fetchRequest()
            tableFetchRequest.predicate = predicate
            
            let tableList:Array<Tour>? = try CoreDataController.getContext().fetch(tableFetchRequest)
            
            if tableList != nil && (tableList?.count)! > 0 {
                
                
                for item in (tableList?.first?.optionalMasterRelation)! {
                    
                    CoreDataController.getContext().delete(item as! NSManagedObject)
                    
                }
                
                try CoreDataController.getContext().save()
                
                
            }
            
            
            
        } catch  {
            printLog(error)
        }
    }
    class func DeleteAllOptionalPackageData(bfnNumber:String)  {
        
        do {
            
            
            
            let predicate = NSPredicate(format: "bfNumber == '\(bfnNumber)'")
            
            
            let tableFetchRequest: NSFetchRequest<Tour> = Tour.fetchRequest()
            tableFetchRequest.predicate = predicate
            
            let tableList:Array<Tour>? = try CoreDataController.getContext().fetch(tableFetchRequest)
            
            if tableList != nil && (tableList?.count)! > 0 {
                
                
                for item in (tableList?.first?.bookedOptionalPackageRelation)! {
                    
                    CoreDataController.getContext().delete(item as! NSManagedObject)
                    
                }
                
                try CoreDataController.getContext().save()
                
                
            }
            
            
            
        } catch  {
            printLog(error)
        }
    }
    
    class func DeleteAllVisaCountry(bfnNumber:String)  {
        
        do {
            
            
            
            let predicate = NSPredicate(format: "bfNumber == '\(bfnNumber)'")
            
            
            let tableFetchRequest: NSFetchRequest<Tour> = Tour.fetchRequest()
            tableFetchRequest.predicate = predicate
            
            let tableList:Array<Tour>? = try CoreDataController.getContext().fetch(tableFetchRequest)
            
            if tableList != nil && (tableList?.count)! > 0 {
                
                
                for item in (tableList?.first?.visaCountryRelation)! {
                    
                    CoreDataController.getContext().delete(item as! NSManagedObject)
                    
                }
                
                try CoreDataController.getContext().save()
                
                
            }
            
            
            
        } catch  {
            printLog(error)
        }
    }
    
    class func DeleteAllPaymentReceiptData(bfnNumber:String)  {
        
        do {
            
            
            
            let predicate = NSPredicate(format: "bfNumber == '\(bfnNumber)'")
            
            
            let tableFetchRequest: NSFetchRequest<Tour> = Tour.fetchRequest()
            tableFetchRequest.predicate = predicate
            
            let tableList:Array<Tour>? = try CoreDataController.getContext().fetch(tableFetchRequest)
            
            if tableList != nil && (tableList?.count)! > 0 {
                
                
                for item in (tableList?.first?.receiptRelation)! {
                    
                    CoreDataController.getContext().delete(item as! NSManagedObject)
                    
                }
                
                try CoreDataController.getContext().save()
                
                
            }
            
            
            
        } catch  {
            printLog(error)
        }
    }
    
    class func DeleteAllPassengerData(bfnNumber:String)  {
        
        do {
            
            
            
            let predicate = NSPredicate(format: "bfNumber == '\(bfnNumber)'")
            
            
            let tableFetchRequest: NSFetchRequest<Tour> = Tour.fetchRequest()
            tableFetchRequest.predicate = predicate
            
            let tableList:Array<Tour>? = try CoreDataController.getContext().fetch(tableFetchRequest)
            
            if tableList != nil && (tableList?.count)! > 0 {
                
                
                for item in (tableList?.first?.passengerRelation)! {
                    
                    CoreDataController.getContext().delete(item as! NSManagedObject)
                    
                }
                
                try CoreDataController.getContext().save()
                
                
            }
            
            
            
        } catch  {
            printLog(error)
        }
    }
    class func deleteCurencyData() -> Bool{
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Currency")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        
        do {
            
            let result = try CoreDataController.getContext().execute(deleteRequest)
            return true
            
        } catch let error as NSError {
            
            printLog(error.description)
            return false
        }
    }
    
}

struct CurrencyData {
    
    public var currencyCode: String?
    public var currencyDate: String?
    public var currencyName: String?
    public var currencyRate: String?
    public var exchangeRateId: String?
    
    init() {
        
        self.currencyCode = ""
        self.currencyDate = ""
        self.currencyName = ""
        currencyRate = ""
        self.exchangeRateId = ""
        
        
}
}
