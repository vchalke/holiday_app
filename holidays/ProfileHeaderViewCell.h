//
//  ProfileHeaderViewCell.h
//  holidays
//
//  Created by Kush_Team on 26/05/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@protocol ProfileHeaderViewCellDelegaete <NSObject>
@optional
- (void)logOutButtonClicked;
@end
@interface ProfileHeaderViewCell : UITableViewCell
@property (nonatomic, weak) id <ProfileHeaderViewCellDelegaete> headerDelegaet;
@property (weak, nonatomic) IBOutlet UIView *baseViews;
@property (weak, nonatomic) IBOutlet UIImageView *img_user;
@property (weak, nonatomic) IBOutlet UILabel *lbl_userName;
@property (weak, nonatomic) IBOutlet UILabel *lbl_emailId;
@property (weak, nonatomic) IBOutlet UIButton *btn_logout;
-(void)setProfileImage:(NSString*)imgStr;
-(void)setUserProfileInfo;
@end

NS_ASSUME_NONNULL_END
