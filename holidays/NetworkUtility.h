//
//  NetworkUtility.h
//  mobicule-device-network-layer
//
//  Created by Neetesh Dubey on 10/06/16.
//  Copyright © 2016 Mobicule. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetworkUtility : NSObject

/**
 *  This is use to Compress the data .
 *
 *  @param data         Pass a particular  data of type NSData .
 *
 *  @return                 Compressed data.
 */
+ (NSData *)gzipDeflate : (NSData*)data;

/**
 *  This is use to Decompress the data .
 *
 *  @param data         Pass a particular Compressed data .
 *
 *  @return                 Decompressed data.
 */
+ (NSData *)gzipInflate : (NSData*)data;

/**
 *  This is use to Generate Secure Secret key .
 *
 *  @param packageName         Pass a particular  packageName of type NSString .
 *
 *  @return                              Secure Secret key .
 */
- (NSString *)generateSecretKey : (NSString *)packageName;

/**
 *  This is use to Generate Digest .
 *
 *  @param requestBody         Pass a particular  requestBody of type NSString .
 *  @param key                      Pass a particular  Secret key  of type NSString .
 *
 *  @return                            Digest of type NSString .
 */
- (NSString *)generateDigest : (NSString *)requestBody withKey :(NSString *)key;

/**
 *  This is use to Generate Digest Value.
 *
 *  @param requestBody         Pass a particular  requestBody of type NSString .
 *
 *  @param requestBody         Pass a particular  package name of type NSString .
 *
 *  @return                            Digest Value of type NSString .
 */
- (NSString *)generateDigestValue : (NSString *)requestBody withPackageName:(NSString *)packageName;

-(NSString *)getOTP:(NSString *)otpString;

-(NSString *)setOTP:(NSString *)sendOTPString;

@end


