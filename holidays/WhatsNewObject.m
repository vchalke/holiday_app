//
//  WhatsNewObject.m
//  holidays
//
//  Created by Kush_Team on 27/05/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "WhatsNewObject.h"

@implementation WhatsNewObject

-(instancetype)initWithImgArray:(NSDictionary *)arrayObjDict{
   if ([super init])
    {
        NSLog(@"%@",[arrayObjDict valueForKey:@"bannerLargeImage"]);
        NSLog(@"%@",[arrayObjDict valueForKey:@"bannerMediumImage"]);
        NSLog(@"%@",[arrayObjDict valueForKey:@"bannerSmallImage"]);
        self.packageID = [arrayObjDict valueForKey:@"packageID"];
        self.bannerType = [arrayObjDict valueForKey:@"bannerType"];
        self.bannerName = [arrayObjDict valueForKey:@"bannerName"];
        self.redirectUrl = [arrayObjDict valueForKey:@"redirectUrl"];
        self.bannerPageName = [arrayObjDict valueForKey:@"bannerPageName"];
        self.bannerLevelName = [arrayObjDict valueForKey:@"bannerLevelName"];
        self.bannerDuration = [arrayObjDict valueForKey:@"bannerDuration"];
        self.bannerLargeImage = [arrayObjDict valueForKey:@"bannerLargeImage"];
        self.bannerLargeImage = [self.bannerLargeImage stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        self.bannerMediumImage = [arrayObjDict valueForKey:@"bannerMediumImage"];
        self.bannerMediumImage = [self.bannerMediumImage stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        self.bannerSmallImage = [arrayObjDict valueForKey:@"bannerSmallImage"];
        self.bannerSmallImage = [self.bannerSmallImage stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        
//         NSString *strikeOutPrize = [[arrayObjDict valueForKey:@"bannerStrikeoutPrice"] absoluteString];
//        NSString *strikeOutPrize = [arrayObjDict valueForKey:@"bannerStrikeoutPrice"];
//        strikeOutPrize = [strikeOutPrize stringByReplacingOccurrencesOfString:@"," withString:@""];
//        NSNumber *strikeOutPrize = [arrayObjDict valueForKey:@"bannerStrikeoutPrice"];
//        NSString *strikeOutPrize = [arrayObjDict valueForKey:@"bannerStrikeoutPrice"];
//        self.bannerStrikeoutPrice = [strikeOutPrize integerValue];
        
//        NSString *strikeOutPrize = [arrayObjDict valueForKey:@"bannerStrikeoutPrice"];
//        NSLocale *locale = [NSLocale currentLocale];
//        NSString *thousandSeparator = [locale objectForKey:NSLocaleGroupingSeparator];
//        NSString *result = [strikeOutPrize stringByReplacingOccurrencesOfString:thousandSeparator withString:@""];
//        self.bannerStrikeoutPrice = [result integerValue];
        
//        NSString *startPrize = [[arrayObjDict valueForKey:@"bannerStartingPrice"] absoluteString];
//        NSString *startPrize = [arrayObjDict valueForKey:@"bannerStartingPrice"];
//        startPrize = [startPrize stringByReplacingOccurrencesOfString:@"," withString:@""];
//        NSString *startPrize = [arrayObjDict valueForKey:@"bannerStartingPrice"];
//        self.bannerStartingPrice = [startPrize integerValue];
        
        self.bannerStrikeoutPrice = [arrayObjDict valueForKey:@"bannerStrikeoutPrice"];
        self.bannerStartingPrice = [arrayObjDict valueForKey:@"bannerStartingPrice"];
        self.bannerOrder = [[arrayObjDict valueForKey:@"bannerOrder"] integerValue];
        self.orderOnThePage = [[arrayObjDict valueForKey:@"orderOnThePage"] integerValue];
        self.destinationName = [arrayObjDict valueForKey:@"destinationName"];
        self.bannerLevelType = [arrayObjDict valueForKey:@"bannerLevelType"];
        self.mainBannerImgUrl = [arrayObjDict valueForKey:@"bannerSmallImage"];
         NSLog(@"mainBannerImgUrl %@",self.mainBannerImgUrl);
//        self.mainBannerImgUrl = [self.mainBannerImgUrl stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        if ([self.mainBannerImgUrl isEqualToString:@"NA"])
        {
            self.mainBannerImgUrl = [arrayObjDict valueForKey:@"bannerMediumImage"];
        }
        if ([self.mainBannerImgUrl isEqualToString:@"NA"])
        {
            self.mainBannerImgUrl = [arrayObjDict valueForKey:@"bannerLargeImage"];
        }
        
    }
    return self;
}
@end
