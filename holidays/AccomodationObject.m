//
//  AccomodationObject.m
//  holidays
//
//  Created by Kush_Tech on 09/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "AccomodationObject.h"

@implementation AccomodationObject
-(instancetype)initWithAccomodationObjectDict:(NSDictionary *)dictionary{
    if ([super init])
    {
        self.hotelName = [dictionary valueForKey:@"hotelName"];
        self.country = [dictionary valueForKey:@"country"];
        self.state = [dictionary valueForKey:@"state"];
        self.starRating = [[dictionary valueForKey:@"starRating"] integerValue];

    }
    
    return self;
}
-(instancetype)initWithAccomodationObjectOtherDict:(NSDictionary *)dictionary{
    if ([super init])
    {
        NSDictionary *cityDict = [dictionary valueForKey:@"cityCode"];
        self.hotelName = [cityDict valueForKey:@"hotelName"];
        self.country = [dictionary valueForKey:@"country"];
        self.state = [dictionary valueForKey:@"state"];
        self.starRating = [[dictionary valueForKey:@"starRating"] integerValue];

    }
    
    return self;
}
@end
