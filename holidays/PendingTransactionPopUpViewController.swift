//
//  PendingTransactionPopUpViewController.swift
//  holidays
//
//  Created by ketan on 18/01/18.
//  Copyright © 2018 Mobicule. All rights reserved.
//

import UIKit
protocol pendingPopUpDelegate
{
    func onClosePopUp(With email:String, phoneNumber:NSString);
}

class PendingTransactionPopUpViewController: UIViewController
{
    var delegate : pendingPopUpDelegate!
    var pendingTransactionViewController : PendingTransactionViewController?
    
    @IBOutlet weak var textFieldEmailID: UITextField!
    
    @IBOutlet weak var textFieldMobileNumber: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(PendingTransactionPopUpViewController.dismissKeyboard))
        
        //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
        //tap.cancelsTouchesInView = false
        
        self.view.addGestureRecognizer(tap)

    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func onSubmitButtonClicked(_ sender: Any)
    {
        self.delegate.onClosePopUp(With:textFieldEmailID.text! , phoneNumber:textFieldMobileNumber.text! as NSString);
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        self.view.endEditing(true)
        
    }
    
    @IBAction func closePopUp(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
        self.pendingTransactionViewController?.navigationController?.popViewController(animated: true)
    }
    
}
