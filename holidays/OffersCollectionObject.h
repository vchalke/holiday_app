//
//  OffersCollectionObject.h
//  holidays
//
//  Created by Kush_Team on 11/06/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface OffersCollectionObject : NSObject
@property(nonatomic,strong)NSString *isOfferVisible;
@property(nonatomic,strong)NSString *packageid;
@property(nonatomic,strong)NSString *holidayOfferName;
@property(nonatomic)NSInteger holidayOfferId;
@property(nonatomic)NSInteger offerId;
-(instancetype)initWithCollectionObject:(NSDictionary *)arrayObjDict;
@end

NS_ASSUME_NONNULL_END
