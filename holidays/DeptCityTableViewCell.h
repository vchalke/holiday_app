//
//  DeptCityTableViewCell.h
//  holidays
//
//  Created by Kush_Team on 24/06/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@protocol DeptCityTableViewCellDelegate <NSObject>
@optional
- (void)clickCity:(NSString*)cityName;
@end
@interface DeptCityTableViewCell : UITableViewCell<UITextFieldDelegate>
{
    NSString *selectDeptCity;
}
@property (weak, nonatomic) IBOutlet UITextField *txt_deptCity;
@property (weak, nonatomic) NSMutableArray *arrayDepartureList;
@property (weak, nonatomic) NSMutableArray *arrayOfHolidayData;
@property (nonatomic, weak) id <DeptCityTableViewCellDelegate> deptCityDelgate;
-(void)setupCityTableView;
@property (strong,nonatomic) NSMutableDictionary *deptCityFilterDict;
@end

NS_ASSUME_NONNULL_END
