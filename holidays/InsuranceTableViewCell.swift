//
//  InsuranceTableViewCell.swift
//  sotc-consumer-application
//
//  Created by Saurav Kumar on 12/08/17.
//  Copyright © 2017 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit

class InsuranceTableViewCell: UITableViewCell {

    
    @IBOutlet weak var uiLabelPassengerName: UILabel!
    
    @IBOutlet weak var uiLabelNumber: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
