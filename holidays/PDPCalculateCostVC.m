//
//  PDPCalculateCostVC.m
//  holidays
//
//  Created by Kush_Team on 25/05/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "PDPCalculateCostVC.h"
#import "MKDropdownMenu.h"
#import "TopWebVC.h"
#import "PaymentTermsObject.h"
#import "CalenderScreenVC.h"
#import "RoomsDataModel.h"
#import "PackageDomesticFit.h"
#import "PackageInternationFIT.h"
#import "PackageInternationalGIT.h"
#import "Constants.h"
#import "CalendarViewController.h"
#import "DateStringSingleton.h"
#import "PDPCalculatePromoVC.h"
#import "TravellerInformationModel.h"
#import "NetCoreAnalyticsVC.h"
#import "NewLoginPopUpViewController.h"
#import "CalculateDictObject.h"
//#import "Thomas_Cook_Holidays-Swift.h"
#define kPaymentTerms @"Payment Terms"
#define kCancellatipnPolicy @"Cancellation Policy"
#define kTermsAndCond @"Terms & Conditions"

@interface PDPCalculateCostVC ()<UITextFieldDelegate,NewMasterVCDelegate,CalenderScreenVCDelegate>
{
    NSArray *stateArray;
    NSArray *adultArray;
    BOOL isCheck;
    NSArray *packageTypeArray;
    NSMutableArray *roomRecordArray;
    NSMutableArray *ItineraryCodeArray;
    NSArray *travellerArrayForCalculation;
    LoadingView *activityLoadingView;
    NSString *packageType;
    int accomType;
    NSString *departFrom;
    NSDictionary *selectedHubDict;
    NSString *selectedLtItineraryCode;
    NSString *bookingAmountString;
    NSString *bookingAmountForBookingSubmit;
    NSDictionary *dictForDate;
     NSDictionary *calculationDict;
}
@end

@implementation PDPCalculateCostVC

- (void)viewDidLoad {
//    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self checkButtonClicked:nil];
    [self.btn_check addTarget:self action:@selector(checkButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.btn_policyterm addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.btn_privacypolicy addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.btn_termscondi addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    stateArray = [NSArray arrayWithObjects:@"Delhi",@"Punjab",@"Maharashtra",@"Gujarat",@"TamilNadu",@"Kerlaa", nil];
    adultArray = [NSArray arrayWithObjects:@"2 Adults",@"4 Adults",@"6 Adults",@"8 Adults",@"10 Adults",@"12 Adults", nil];
    
    self.txt_mobileNum.inputAccessoryView = [self getUIToolBarForKeyBoard];
    self.masterDelgate = self;
    
    roomRecordArray = [[NSMutableArray alloc]init];
    RoomsDataModel *roomModel = [[RoomsDataModel alloc]init];
    [roomRecordArray addObject:roomModel];
    [self setRoomDataModelWithIndex:1];
    
    packageTypeArray = self.holidayPkgDetailInPdpCost.arrayAccomTypeList;
    NSLog(@"newPackageDetailDict %@",self.holidayPkgDetailInPdpCost.packageDetailDictObj);
    NSLog(@"arrayItinerary %@",self.holidayPkgDetailInPdpCost.arrayItinerary);
    NSLog(@"arrayHubList %@",self.holidayPkgDetailInPdpCost.arrayHubList);
    NSLog(@"arrayOfHubList %@",self.holidayPkgDetailInPdpCost.arrayOfHubList);
    NSLog(@"ltItineraryCode %@",self.holidayPkgDetailInPdpCost.ltItineraryCode);
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [self.txt_deptDate resignFirstResponder];
    [CoreUtility reloadRequestID];
    if([[self.holidayPkgDetailInPdpCost.strPackageSubType lowercaseString] isEqualToString:@"fit fixed"]|| [[self.holidayPkgDetailInPdpCost.strPackageSubType lowercaseString] isEqualToString:@"git"]){
        if([[self.holidayPkgDetailInPdpCost.strPackageType lowercaseString] isEqualToString:@"international"]){
            packageType = kpackageTypeGITInternational;
        }else{
            packageType = kpackageTypeGITDomestic;
        }
    }else{
        if([[self.holidayPkgDetailInPdpCost.strPackageType lowercaseString] isEqualToString:@"international"]){
            packageType = kpackageTypeFITInternational;
        } else{
            packageType = kpackageTypeFITDomestic;
        }
    }
    if(self.holidayPkgDetailInPdpCost.stringSelectedAccomType == nil || [self.holidayPkgDetailInPdpCost.stringSelectedAccomType isEqualToString:@""]){
        if (packageTypeArray.count != 0){
            NSString *packageTypeLocal = (NSString *)packageTypeArray[0];
            [self setAccomType:@"standard"];
        }
    }else{
        if ([self.holidayPkgDetailInPdpCost.stringSelectedAccomType isEqualToString:@"0"]){
            accomType = 0;
        }else if ([self.holidayPkgDetailInPdpCost.stringSelectedAccomType isEqualToString:@"1"]){
            accomType = 1;
        }else{
            accomType = 2;
        }
    }
}
-(void)setAccomType:(NSString *)accomTypeString
{
    if ([[accomTypeString lowercaseString] isEqualToString:@"standard"]){
        self.holidayPkgDetailInPdpCost.stringSelectedAccomType = @"0";
    }else if ([[accomTypeString lowercaseString] isEqualToString:@"delux"]||[[accomTypeString lowercaseString] isEqualToString:@"deluxe"]){
        self.holidayPkgDetailInPdpCost.stringSelectedAccomType = @"1";
    }else{
        self.holidayPkgDetailInPdpCost.stringSelectedAccomType = @"2";
    }
}
-(void) buttonClicked:(UIButton*)sender
{
    switch (sender.tag) {
        case 0:
            [self jumpToWebViewFromPDP:@"" withTitle:kPaymentTerms];
            break;
        case 1:
            [self jumpToWebViewFromPDP:@"" withTitle:kCancellatipnPolicy];
            break;
        case 2:
            [self jumpToWebViewFromPDP:@"" withTitle:kTermsAndCond];
            break;
        default:
            break;
    }
}
-(void) checkButtonClicked:(UIButton*)sender{
    isCheck = !isCheck;
    [self.btn_check setImage:[UIImage imageNamed:(isCheck) ? @"rightCheckSelect" : @"rightCheckNoSelect"] forState:UIControlStateNormal];
}
-(void)setRoomDataModelWithIndex:(NSInteger)index{
//    RoomsDataModel *roomModel = roomRecordArray[index-1];
    RoomsDataModel *roomModel = [roomRecordArray firstObject];
    roomModel.adultCount = [[self.txt_numoftravel.text stringByReplacingOccurrencesOfString:@" Adults" withString:@""]intValue];
    roomModel.infantCount = 0;
    roomModel.arrayChildrensData = 0;
    [roomRecordArray replaceObjectAtIndex:0 withObject:roomModel];
}

#pragma mark - Calender Screen Delegaet
-(void)setSelectedDate:(NSDate *)date withVCIndex:(int)index withDataDict:(nonnull NSDictionary *)dataDict{
    self.txt_deptDate.text = ([[NSDate date] compare:date] == NSOrderedAscending) ? [[DateStringSingleton shared]getDateTimeStringFormat:@"dd-MM-yyyy" DateValue:date]:@"";
    NSLog(@"dataDict %@",dataDict);
    dictForDate = dataDict;
}
#pragma mark - Checking Validations
-(NSString*)getEmptyWarning{
    if ([self.txt_deptCity.text length]==0 || [self.txt_numoftravel.text length]==0 || [self.txt_deptDate.text length]==0 || [self.txt_mobileNum.text length]==0){
        return @"All Fields Are Mandatory";
    }
    if ([self validatePhone:self.txt_mobileNum.text]){
        return @"Enter Valid Mobile Number";
    }
    if (!isCheck){
        return @"Click On Accept Policy";
    }
    return @"";
}
#pragma mark - Checking Validations


-(void)textFieldDepartureCityPress{
    
    NSArray *hubList = [NSArray new];
    if([packageType isEqualToString:kpackageTypeFITDomestic]||[packageType isEqualToString:kpackageTypeFITInternational]){
        hubList = self.holidayPkgDetailInPdpCost.arraytcilHolidayPriceCollection;
        
    }
    else{
        hubList = self.holidayPkgDetailInPdpCost.arrayLtPricingCollection;
    }
    UIAlertController *durationActionSheet = [UIAlertController alertControllerWithTitle:@"Please Select" message:@"Select your choice" preferredStyle:UIAlertControllerStyleActionSheet];
    NSArray *filteredarray;
    filteredarray = hubList;
    filteredarray = [hubList filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(packageClassId == %@)", [NSString stringWithFormat:@"%d",accomType]]];
    
    NSArray* uniqueValues;
    if([packageType isEqualToString:kpackageTypeFITDomestic]||[packageType isEqualToString:kpackageTypeFITInternational]){
        uniqueValues = [filteredarray valueForKeyPath:[NSString stringWithFormat:@"@distinctUnionOfObjects.hubCityCode.cityName"]];
    }
    else{
        uniqueValues = [filteredarray valueForKeyPath:[NSString stringWithFormat:@"@distinctUnionOfObjects.hubCode.cityName"]];
    }
    NSMutableArray * uniqueDataArray = [[NSMutableArray alloc] init];
    for (NSString * city in uniqueValues)
    {
        NSExpression *lhs;
        if([packageType isEqualToString:kpackageTypeFITDomestic]||[packageType isEqualToString:kpackageTypeFITInternational]){
            
            lhs = [NSExpression expressionForKeyPath:@"hubCityCode.cityName"];
        }
        else{
            lhs = [NSExpression expressionForKeyPath:@"hubCode.cityName"];
            
        }
        NSExpression *rhs = [NSExpression expressionForConstantValue:city];
        NSPredicate * finalPredicate = [NSComparisonPredicate
                                        predicateWithLeftExpression:lhs
                                        rightExpression:rhs
                                        modifier:NSDirectPredicateModifier
                                        type:NSContainsPredicateOperatorType
                                        options:NSCaseInsensitivePredicateOption];
        NSArray * filteredChannelArray   = [[filteredarray filteredArrayUsingPredicate:finalPredicate] mutableCopy];
        if (filteredChannelArray.count > 0) {
            [uniqueDataArray addObject:[filteredChannelArray objectAtIndex:0]];
        }
    }
    NSSortDescriptor *nameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"hubCode.cityName"  ascending:YES];
    NSArray *sortDescriptors = @[nameDescriptor];
    NSArray * ordered = [uniqueDataArray sortedArrayUsingDescriptors:sortDescriptors];
    
    for (int j =0 ; j < ordered.count; j++)
    {
        NSDictionary *cityDict = ordered[j];
        NSDictionary *hubCodeDict;
        if([packageType isEqualToString:kpackageTypeFITDomestic]||[packageType isEqualToString:kpackageTypeFITInternational]) {
            hubCodeDict = [cityDict valueForKey:@"hubCityCode"];
        }
        else{
            hubCodeDict = [cityDict valueForKey:@"hubCode"];
        }
        NSString *titleString = [hubCodeDict valueForKey:@"cityName"];
        NSString *departFromLocal = [hubCodeDict valueForKey:@"cityCode"];
        NSString *ltCodeString = [cityDict valueForKey:@"ltItineraryCode"];
        UIAlertAction *action = [UIAlertAction actionWithTitle:titleString style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                 {
            departFrom = departFromLocal;
            self.txt_deptCity.text = titleString;
            selectedHubDict = hubCodeDict;
            selectedLtItineraryCode = ltCodeString;
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"hubCode.cityName contains[c] %@ ",titleString];
            NSArray *filteredData = [filteredarray filteredArrayUsingPredicate:predicate];
            ItineraryCodeArray = [[NSMutableArray alloc]init];
            NSLog(@"%@",filteredData);
            for(int i =0; i<filteredData.count;i++){
                NSDictionary *cityDict = filteredData[i];
                selectedLtItineraryCode = [cityDict valueForKey:@"ltItineraryCode"];
                [ItineraryCodeArray addObject:selectedLtItineraryCode];
            }
        }];
        [durationActionSheet addAction:action];
    }
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    [durationActionSheet addAction:cancelAction];
    [self.view.window.rootViewController presentViewController:durationActionSheet animated:YES completion:nil];
    
}
#pragma mark - TextField Delegaet
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField != self.txt_mobileNum){
        [textField resignFirstResponder];
        if (textField == self.txt_deptCity){
            [self textFieldDepartureCityPress];
        }else if (textField == self.txt_numoftravel){
            [self showMultipleOptionsInAlertWithTitle:@"Please Select" msg:@"Number Of Adults" style:UIAlertControllerStyleActionSheet buttArray:[NSArray arrayWithObjects:@"1 Adult",@"2 Adults",@"3 Adults",@"4 Adults",@"5 Adults",nil] completion:^(NSString * string) {
                if (![string isEqualToString:@"Cancel"]){
                self.txt_numoftravel.text = string;
                }
            }];
        }else if (textField == self.txt_deptDate){
            if ([self.txt_deptCity.text length]==0){
                [self showAlertViewWithTitle:@"Alert !" withMessage:@"Select Departure City"];
            }else{
                [self checkValidityOfCombinations];
                [self showCalenderViewAspsdInPdp];
            }
        }
    }
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if(textField == self.txt_mobileNum)
    {
        if ([textField.text length]>9){
            return YES;
        }
    }
    return YES;
}
#pragma mark - Button Actions

- (IBAction)btn_CalculateTourCost:(id)sender {
    NSString *warningString = [self getEmptyWarning];
    if ([warningString length]==0){
//        [self jumpYourQuoteVC];
        [self calculatePriceAstraAPI];
    }else{
        [self showButtonsInAlertWithTitle:@"Alert" msg:warningString style:UIAlertControllerStyleAlert buttArray:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString * string) {
            
        }];
    }
}
- (IBAction)btn_BackPress:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)btn_AddRoomPress:(id)sender{
    [self showMultipleOptionsInAlertWithTitle:@"Please Select" msg:@" Number Of Rooms" style:UIAlertControllerStyleActionSheet buttArray:[NSArray arrayWithObjects:@"Room 1",@"Room 2",@"Room 3",@"Room 4",@"Room 5",nil] completion:^(NSString * string) {
        if (![string isEqualToString:@"Cancel"]){
        self.lbl_roomNum.text = string;
        }
    }];
}
#pragma mark - Jump To Other Views

-(void)setToolBarActionTextField:(UIBarButtonItem *)barButtonItem{
    [self.txt_mobileNum resignFirstResponder];
}

-(void)jumpToWebViewFromPDP:(NSString *)webUrlString withTitle:(NSString *)webViewTitle{
    TopWebVC *controler = [[TopWebVC alloc] initWithNibName:@"TopWebVC" bundle:nil];
    controler.web_url_string = webUrlString;
    controler.web_title = webViewTitle;
    controler.isFromPdpCalculate = true;
    NSMutableArray *passARray = [[NSMutableArray alloc]init];
    for (id object in self.holidayPkgDetailInPdpCost.arrayTermsAndConditions) {
        PaymentTermsObject *payobject = [[PaymentTermsObject alloc]initWithPaymentTermsObjectDict:object];
        if ([webViewTitle isEqualToString:kPaymentTerms])
            [passARray addObject:payobject.paymentTerms];
        else if ([webViewTitle isEqualToString:kCancellatipnPolicy])
            [passARray addObject:payobject.cancellationPolicy];
        else if ([webViewTitle isEqualToString:kTermsAndCond])
            [passARray addObject:payobject.descriptionArray];
    }
    controler.stringArray = passARray;
    [self.navigationController pushViewController:controler animated:YES];
}

-(void)jumpCalenderVC:(NSDictionary*)responseDict{
    CalenderScreenVC *controler = [[CalenderScreenVC alloc] initWithNibName:@"CalenderScreenVC" bundle:nil];
    controler.calenderDelgate = self;
    controler.dataDict = responseDict;
    if ([[self.holidayPkgDetailInPdpCost.packageMRP lowercaseString] isEqualToString:@"true"])
    {
        controler.isMRP = YES;
    }
    else
    {
        controler.isMRP = NO;
    }
    controler.holidayPackageDetailInCalender = self.holidayPkgDetailInPdpCost;
    controler.arrayTravellerCalculation = travellerArrayForCalculation;
    controler.roomRecordArray = roomRecordArray;
    controler.hubName = departFrom;
    controler.packageId = self.holidayPkgDetailInPdpCost.strPackageId;
    controler.accomType = accomType;
    controler.previosVCIndex = 100;
    [self.navigationController pushViewController:controler animated:YES];
}
-(void)jumpYourQuoteVC{
    PDPCalculatePromoVC *controller = [[PDPCalculatePromoVC alloc] initWithNibName:@"PDPCalculatePromoVC" bundle:nil];
    controller.holidayPkgDetailInPromoVC = self.holidayPkgDetailInPdpCost;
    controller.dictForDate = dictForDate;
    controller.departureDate = self.txt_deptDate.text;
    controller.departureCity = self.txt_deptCity.text;
    controller.numOfTravellers = self.txt_numoftravel.text;
    controller.selectHubDict = selectedHubDict;
    controller.numOfRooms = self.lbl_roomNum.text;
    [self.navigationController pushViewController:controller animated:YES];
}
#pragma mark - Reuse of Existing Methods in holiday

#pragma mark - Showing Calender

-(void)showCalenderViewAspsdInPdp{

    if([CoreUtility connected])
    {

    @try
    {
        int totalChildCount = 0;
        int totalAdultCount = 0;

        for (int i = 0; i<roomRecordArray.count; i++)
        {
            RoomsDataModel *dataModel = roomRecordArray[i];
            totalAdultCount = totalAdultCount + dataModel.adultCount;
            totalChildCount = totalChildCount + (int)dataModel.arrayChildrensData.count;
        }

        NSMutableDictionary *dictOfData = [[NSMutableDictionary alloc]init];
        [dictOfData setValue:self.holidayPkgDetailInPdpCost.strPackageId forKey:@"packageId"];
        [dictOfData setValue:packageType forKey:@"accomType"];
        [dictOfData setValue:[NSString stringWithFormat:@"%d",totalAdultCount+totalChildCount] forKey:@"noOfPax"];
        [dictOfData setValue:departFrom forKey:@"departFrom"];

            activityLoadingView = [LoadingView loadingViewInView:self.view.superview.superview
                                                      withString:@""
                                               andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];


            NetworkHelper *helper = [NetworkHelper sharedHelper];



            NSMutableArray *arrayForItinarayCode = [[NSMutableArray alloc] init];



        if (![packageType isEqualToString:kpackageTypeFITInternational] && ![packageType isEqualToString:kpackageTypeFITDomestic])
        {
          //  [arrayForItinarayCode addObject:selectedLtItineraryCode];
            [arrayForItinarayCode addObjectsFromArray:ItineraryCodeArray];
        }

            NSMutableDictionary *jsonDict = [[NSMutableDictionary alloc] init];
            [jsonDict setObject:arrayForItinarayCode forKey:@"ltItineraryCode"];
            //[jsonDict setObject:packageDetail.ltMarket forKey:@"market"];
            [jsonDict setObject:@"-1" forKey:@"market"];

            [jsonDict setObject:departFrom forKey:@"hubCode"];
            [jsonDict setObject:[NSNumber numberWithInteger:[self.holidayPkgDetailInPdpCost.strPackageSubTypeID integerValue]] forKey:@"pkgSubTypeId"];
            [jsonDict setObject:self.self.holidayPkgDetailInPdpCost.stringSelectedAccomType forKey:@"pkgClassId"];
            [jsonDict setObject:self.holidayPkgDetailInPdpCost.strPackageId forKey:@"pkgId"];
            [jsonDict setObject:@"TCIL" forKey:@"mode"];
            [jsonDict setObject:@"N" forKey:@"isHsa"];


            NSDictionary *headerDict = [CoreUtility getHeaderDict];

            [helper getResponseWithRequestType:@"POST" withQueryParam:@"" withPathParam:@"" withJsonParam:jsonDict withHeaders:headerDict withUrl:kAstraUrlFareCalender success:^(NSDictionary *responseDict)
             {
                 NSLog(@"Response Dict : %@",responseDict);


                 dispatch_async(dispatch_get_main_queue(), ^(void)
                                {
                                    [activityLoadingView removeFromSuperview];
                                    if (responseDict)
                                    {
                                        if (responseDict.count>0){
                                            [self jumpCalenderVC:responseDict];

                                        }
                                        else{
                                            [super showAlertViewWithTitle:@"Alert" withMessage:@"Dates not available for this package."];
                                        }
                                    }else{
                                        [super showAlertViewWithTitle:@"Alert" withMessage:@"Dates not available for this package."];

                                    }
                                });

             }
                                       failure:^(NSError *error)
             {dispatch_async(dispatch_get_main_queue(), ^(void)
                             {
                                 [activityLoadingView removeFromSuperview];
                                 [super showAlertViewWithTitle:@"Alert" withMessage:@"Dates not available for this package."];
                                 NSLog(@"Response Dict : %@",error);
                             });

             }];
    }
    @catch (NSException *exception)
    {
        NSLog(@"%@", exception.reason);
        [activityLoadingView removeFromSuperview];
    }
    @finally
    {
        NSLog(@"exception finally called");

    }
    }
    else{
        [super showAlertViewWithTitle:@"Alert" withMessage:kMessageNoInternet];
    }

}

-(BOOL)checkValidityOfCombinations
{
    NSLog(@"%@",roomRecordArray)  ;
    id travellerInfo;

    if([[self.holidayPkgDetailInPdpCost.strPackageSubType lowercaseString] isEqualToString:@"fit fixed"]|| [[self.holidayPkgDetailInPdpCost.strPackageSubType lowercaseString] isEqualToString:@"git"]){
        if([[self.holidayPkgDetailInPdpCost.strPackageType lowercaseString] isEqualToString:@"international"]){
            PackageInternationalGIT *packageInternational = [[PackageInternationalGIT alloc]initWithTravellerArray:roomRecordArray];
            packageInternational.travellerRegion = self.holidayPkgDetailInPdpCost.stringRegion;
            travellerInfo = [packageInternational calaculatePackageInternationalGIT];
        }else{
            PackageDomesticFit *packageInternational = [[PackageDomesticFit alloc]initWithTravellerArray:roomRecordArray];
            travellerInfo = [packageInternational calaculatePackageInternationalFIT];
        }
    }else{
        if([[self.holidayPkgDetailInPdpCost.strPackageType lowercaseString] isEqualToString:@"international"]){
            PackageInternationFIT *packageInternational = [[PackageInternationFIT alloc]initWithTravellerArray:roomRecordArray];
            packageInternational.travellerRegion = self.holidayPkgDetailInPdpCost.stringRegion;
            travellerInfo = [packageInternational calaculatePackageInternationalFIT];
        } else{
            PackageDomesticFit *packageInternational = [[PackageDomesticFit alloc]initWithTravellerArray:roomRecordArray];
            travellerInfo = [packageInternational calaculatePackageInternationalFIT];
        }
    }

    NSLog(@"%@",roomRecordArray)  ;
    if ([travellerInfo isKindOfClass:[NSString class]]||travellerInfo == nil ){
        return NO;
    }
    else if([travellerInfo isKindOfClass:[NSArray class]]){
        travellerArrayForCalculation = (NSArray*)travellerInfo;
        if (travellerArrayForCalculation.count == 0){
            return NO;
        }
        return YES;
    }
    return NO;
}

// Jump to Promo Code
-(void)calculatePriceAstraAPI
{
    @try
    {
        activityLoadingView = [LoadingView loadingViewInView:self.view.superview.superview
                                                  withString:@""
                                           andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
        
        
        NSMutableArray *arrayForRooming = [[NSMutableArray alloc] init];
        for (int i = 0; i<roomRecordArray.count; i++)
        {
            
            int numberOfAdult = 0;
            int numberOfCwb = 0;
            int numberOfCnbJ = 0;
            int numberOfCnbS = 0;
            int numberOfInfants = 0;
            int totalPax = 0;
            
            
            RoomsDataModel *roomsModelInsatance = roomRecordArray[i];
            numberOfAdult = roomsModelInsatance.adultCount;
            numberOfInfants = roomsModelInsatance.infantCount;
            NSArray *childArray = roomsModelInsatance.arrayChildrensData;
            //      int childWithCNBCount = 0;
            //    NSMutableArray *childDictArray = [[NSMutableArray alloc]init];
            //  int childWithCWBCount = 0;
            
            for (int j = 0; j<childArray.count; j++)
            {
                NSDictionary *childDict = childArray[j];
                UIButton *button = [childDict valueForKey:@"button"];
                UILabel *labelAge = [childDict valueForKey:@"textField"];
                
                //  NSDictionary *dictForChild ;
                if ([button isSelected]) {
                
                    numberOfCwb ++;
                    
                }else{
                    if ([packageType isEqualToString:kpackageTypeGITInternational]||[packageType isEqualToString:kpackageTypeFITInternational]){
                        numberOfCnbS ++;
                    }
                    else{
                        if ([labelAge.text intValue] < 5){
                            numberOfCnbJ ++;
                        }else {
                            numberOfCnbS ++;
                        }
                        
                    }
                }
            }
            
            totalPax = numberOfCnbS + numberOfCnbJ + numberOfAdult + numberOfInfants + numberOfCwb;
          
            NSMutableDictionary *roomDict = [[NSMutableDictionary alloc] init];
            [roomDict setObject:[NSNumber numberWithInt:i+1] forKey:@"roomNo"];
            [roomDict setObject:[NSNumber numberWithInt:numberOfAdult] forKey:@"noAdult"];
            [roomDict setObject:[NSNumber numberWithInt:numberOfCwb] forKey:@"noCwb"];
            [roomDict setObject:[NSNumber numberWithInt:numberOfCnbJ] forKey:@"noCnbJ"];
            [roomDict setObject:[NSNumber numberWithInt:numberOfCnbS] forKey:@"noCnbS"];
            [roomDict setObject:[NSNumber numberWithInt:numberOfInfants] forKey:@"inf"];
            [roomDict setObject:[NSNumber numberWithInt:totalPax] forKey:@"pax"];
            [arrayForRooming addObject:roomDict];
        }
        
        
        CoreUtility *coreobj=[CoreUtility sharedclassname];
        NSString *phoneNumber = @"";
        
        if (coreobj.strPhoneNumber.count != 0)
        {
            phoneNumber = coreobj.strPhoneNumber[0];
        }
        
        NSString *emailIDString = [[NSUserDefaults standardUserDefaults] objectForKey:kLoginEmailId];
        
        
        NSMutableDictionary *jsonDict = [[NSMutableDictionary alloc] init];
        
        [jsonDict setObject:self.holidayPkgDetailInPdpCost.packageMode forKey:@"mode"];
        
        // [jsonDict setObject:packageDetail.ltMarket forKey:@"ltMarket"];
        [jsonDict setObject:@"" forKey:@"ltMarket"];
        
        [jsonDict setObject:self.holidayPkgDetailInPdpCost.packageContinent forKey:@"continent"];
        [jsonDict setObject:self.holidayPkgDetailInPdpCost.isHSA forKey:@"isHsa"];
        [jsonDict setObject:[NSArray new] forKey:@"optionalsActivities"];
        [jsonDict setObject:arrayForRooming forKey:@"room"];
        [jsonDict setObject:self.holidayPkgDetailInPdpCost.strPackageId forKey:@"pkgId"];
        [jsonDict setObject:[NSNumber numberWithInteger:[self.holidayPkgDetailInPdpCost.strPackageSubTypeID integerValue]] forKey:@"pkgSubType"];
        [jsonDict setObject:[NSString stringWithFormat:@"%d",accomType] forKey:@"pkgSubClass"]; //fit git
        
        [jsonDict setObject:[selectedHubDict valueForKey:@"cityCode"] forKey:@"hub"];
        [jsonDict setObject:[selectedHubDict valueForKey:@"cityName"] forKey:@"hubCity"];
        [jsonDict setObject:[self->dictForDate valueForKey:@"DATE"] forKey:@"departureDate"];
        [jsonDict setObject:[self->dictForDate valueForKey:@"LT_PROD_CODE"] forKey:@"ltProdCode"];
        [jsonDict setObject:phoneNumber forKey:@"userMobileNo"];
        [jsonDict setObject:@"" forKey:@"userEmailId"];
        
        if (emailIDString)
        {
            [jsonDict setObject:emailIDString forKey:@"userEmailId"];
        }
        
        [jsonDict setObject:@"Direct" forKey:@"enquirySource"];//
        [jsonDict setObject:@"Direct" forKey:@"enquiryMedium"];//
        [jsonDict setObject:@"" forKey:@"enquiryCampaign"];//
        [jsonDict setObject:@"" forKey:@"LP"];//
        [jsonDict setObject:@"" forKey:@"bookURL"];//
        //[jsonDict setObject:packageDetail.ltItineraryCode forKey:@"ltItineraryCode"];
        [jsonDict setObject:[self->dictForDate valueForKey:@"PROD_ITIN_CODE"] forKey:@"ltItineraryCode"];
        
        NSArray *farecaLtItineraryCodeArray = [[NSArray alloc] initWithObjects:[self->dictForDate valueForKey:@"PROD_ITIN_CODE"], nil];
        
        [jsonDict setObject:farecaLtItineraryCodeArray forKey:@"farecaLtItineraryCode"];
        
        
        [jsonDict setObject:@"" forKey:@"regionId"]; //
        
        [jsonDict setObject:self.holidayPkgDetailInPdpCost.productID forKey:@"productId"];
        
        
        if ([self.holidayPkgDetailInPdpCost.strPackageType isEqualToString:@"domestic"])
        {
            [jsonDict setObject:@"2" forKey:@"regionId"];
        }
        else
        {
            if ([[self.holidayPkgDetailInPdpCost.packageContinent uppercaseString] isEqualToString:@"AMERICA"]||[[self.holidayPkgDetailInPdpCost.packageContinent uppercaseString] isEqualToString:@"SOUTH AMERICA"]||[[self.holidayPkgDetailInPdpCost.packageContinent uppercaseString] isEqualToString:@"NORTH AMERICA"] ||[[self.holidayPkgDetailInPdpCost.packageContinent uppercaseString] isEqualToString:@"AFRICA"])
            {
                [jsonDict setObject:@"1" forKey:@"regionId"];
            }
            else
            {
                [jsonDict setObject:@"0" forKey:@"regionId"];
            }
        }
        
        
        [jsonDict setObject:@"" forKey:@"crmEnquiryId"]; //
        [jsonDict setObject:@"" forKey:@"crmStatus"];//?? - empty
        
        //        VJ_Commented
        //        [jsonDict setObject:[selectedStateDict valueForKey:@"gstStateCode"] forKey:@"custState"];
        //        [jsonDict setObject:[selectedStateDict valueForKey:@"gstState"] forKey:@"custStateName"];
        //        [jsonDict setObject:[selectedStateDict valueForKey:@"isUnionTerritory"] forKey:@"isUnionTerritory"];
        
        //        VJ_Added
        [jsonDict setObject:@"" forKey:@"custState"];
        [jsonDict setObject:@"" forKey:@"custStateName"];
        [jsonDict setObject:@"" forKey:@"isUnionTerritory"];
        
        
        [jsonDict setObject:@"true" forKey:@"isGstApplicable"]; //? ?
        
        NSDictionary *headerDict = [CoreUtility getHeaderDict];
        
        NetworkHelper *helper = [NetworkHelper sharedHelper];
        
        [helper getResponseWithRequestType:@"POST" withQueryParam:@"" withPathParam:@"" withJsonParam:jsonDict withHeaders:headerDict withUrl:kAstraUrlPricing success:^(NSDictionary *responseDict)
         {
            NSLog(@"Response Dict : %@",responseDict);
            
            dispatch_async(dispatch_get_main_queue(), ^(void)
                           {
                [activityLoadingView removeFromSuperview];
                
                if (responseDict)
                {
                    if (responseDict.count>0)
                    {
                        NSDictionary *pricingDict = responseDict;
                        
                        calculationDict = pricingDict;
                        
                        if (calculationDict!=nil && calculationDict.count!=0)
                        {
                            
                            NSString *messege = [calculationDict valueForKey:@"message"];
                            
                            if (messege == nil )
                            {
                                
                                double tourCost = [[calculationDict valueForKey:@"tourCost"] doubleValue];
                                
                                if (tourCost != 0)
                                {
//                                     PreBookingQuotationViewController* quoteView =  [[PreBookingQuotationViewController alloc] initWithNibName:@"PreBookingQuotationViewController" bundle:nil];
//                                    quoteView.pricingDict = responseDict;
//                                    NSString *quotationId = responseDict[@"quotationId"];
//                                       [self.navigationController pushViewController:quoteView animated:YES];
                                    
                                    if([[self.holidayPkgDetailInPdpCost.strPackageSubType lowercaseString] isEqualToString:@"fit"])
                                    {
                                        
                                        dispatch_async(dispatch_get_main_queue(), ^(void) {
                                            [self calculationForPackageForFIT];
                                            
                                        });
                                    }
                                    else
                                    {
                                        dispatch_async(dispatch_get_main_queue(), ^(void) {
                                            [self calculationForPackageForNONFIT];
                                            
                                        });
                                        
                                    }
                                    
                                    
                                    if (self.holidayPkgDetailInPdpCost.pkgStatusId != 1 || [[self.holidayPkgDetailInPdpCost.stringIsOnReq lowercaseString] isEqualToString:@"y"])
                                    {
                                        
                                        
                                    }
                                    else
                                    {
                                        
                                        
                                        dispatch_async(dispatch_get_main_queue(), ^(void) {
                                            [self.view layoutIfNeeded];
                                            
                                        });
                                        
                                    }
                                    
                                    [UIView animateWithDuration:0.4 animations:^{
                                        
                                        [self.view layoutIfNeeded];
                                    }];
                                }
                                else
                                {
                                    [super showAlertViewWithTitle:@"Alert" withMessage:@"Price not available for this package"];
                                    
                                }
                                
                            }
                            else
                            {
                                [super showAlertViewWithTitle:@"Alert" withMessage:messege];
                            }
                            
                            
                            
                        }
                        
                    }
                    else
                    {
                        [super showAlertViewWithTitle:@"Alert" withMessage:@"some error occurred"];
                        
                    }
                }
                else
                {
                    [super showAlertViewWithTitle:@"Alert" withMessage:@"some error occurred"];
                    
                }
            });
            
        }
                                   failure:^(NSError *error)
         {dispatch_async(dispatch_get_main_queue(), ^(void)
                         {
             [activityLoadingView removeFromSuperview];
             [super showAlertViewWithTitle:@"Alert" withMessage:@"some error occurred"];
             NSLog(@"Response Dict : %@",error);
         });
            
        }];
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"%@", exception.reason);
        [activityLoadingView removeFromSuperview];
    }
    @finally
    {
        NSLog(@"exception finally called");
    }
    
}

-(void)calculationForPackageForFIT{
//    CalculateDictObject *calObject = [[CalculateDictObject alloc]initWithCalculateObjectDict:calculationDict];
//    self.lbl_titletotalCalculateCostINR.text = [NSString stringWithFormat:@"TotalTour Cost %@ calculated @ %@ %0.2f",calObject.currencycodeOne,calObject.currencycodeTwo,calObject.currencyRateOne];
//    self.lbl_tourCostINR.text = [NSString stringWithFormat:@"%@ %ld +  %ld",calObject.currencycodeOne,(long)calObject.amountOne,(long)calObject.amountTwo];
}
-(void)calculationForPackageForNONFIT{
    NSLog(@"%@",calculationDict);
//    CalculateDictObject *calObject = [[CalculateDictObject alloc]initWithCalculateObjectDict:calculationDict];
//    self.lbl_titletotalCalculateCostINR.text = [NSString stringWithFormat:@"TotalTour Cost %@ calculated @ %@ %0.2f",calObject.currencycodeOne,calObject.currencycodeTwo,calObject.currencyRateOne];
//    self.lbl_tourCostINR.text = [NSString stringWithFormat:@"%@ %ld +  %@",calObject.currencycodeOne,(long)calObject.amountOne,[self getNumberFormatterString:(long)calObject.amountTwo]];
//    self.lbl_totalTourCost.text = [NSString stringWithFormat:@"%@",[self getNumberFormatterString:(long)calObject.totaltourCost]];
//    self.lbl_payFullPayment.text = [NSString stringWithFormat:@"%@",[self getNumberFormatterString:(long)calObject.totaltourCost]];
}

@end
