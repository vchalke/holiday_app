//
//  Holiday.m
//  holidays
//
//  Created by vaibhav on 19/09/15.
//  Copyright (c) 2015 Mobicule. All rights reserved.
//

#import "Holiday.h"

@implementation Holiday


-(instancetype)initWithDataDict:(NSDictionary *)dataDict
{
    NSDictionary *packageDatailDict =[dataDict valueForKey:@"packageDetail"]; self.objDetailDict = dataDict;
    
    if ([super init])
    {
        _arrayOfDestination  = [dataDict valueForKey:@"destination"]; // tcilHolidayDestinationCollection
        self.themeCollectionArray = [packageDatailDict valueForKey:@"tcilHolidayThemeCollection"];
        NSArray *cityCollection = [packageDatailDict valueForKey:@"tcilHolidayLtPricingCollection"];
        NSArray *categoryCollection = [packageDatailDict valueForKey:@"tcilHolidayCategoryCollection"]; //changes for tcilholiday collection
        self.tcilHolidayOffersCollection = [packageDatailDict valueForKey:@"tcilHolidayOffersCollection"];
        self.flightDefaultMsg = [packageDatailDict valueForKey:@"flightDefaultMsg"];
        NSMutableArray *hubListArray = [[NSMutableArray alloc] init];
        self.arrayOfCityList = [[NSMutableArray alloc] init];
        for (int index = 0; index < cityCollection.count; index++)
        {
            NSDictionary * cityDict = [cityCollection objectAtIndex:index];
            
            NSDictionary *cityCodeDict = [cityDict valueForKey:@"hubCode"];
            
            NSString *cityNameString = [cityCodeDict valueForKey:@"cityName"];
            
            [hubListArray addObject:cityNameString];
            [self.arrayOfCityList addObject:[NSString stringWithFormat:@"%@",[cityCodeDict valueForKey:@"cityName"]]];
        }
        
//       VJ ADDED
        
        NSArray *themeCollection = [packageDatailDict valueForKey:@"tcilHolidayThemeCollection"]; //changes for tcilholiday collection
        
        NSMutableArray *themeListArray = [[NSMutableArray alloc] init];
        
        for (int index = 0; index < themeCollection.count; index++)
        {
            NSDictionary * themeDict = [themeCollection objectAtIndex:index];
            
            NSDictionary *themeCodeDict = [themeDict valueForKey:@"tcilMstHolidayThemes"];
            
            NSString *themeNameString = [themeCodeDict valueForKey:@"name"];
            
            [themeListArray addObject:themeNameString];
        }
        
        
        
        
        _arrayOfHubList      =  hubListArray; //tcilHolidayCityCollection
        
        _arrayOfThemeList     =  themeListArray; //tcilHolidayThemeCollection
        
        _strCurrenyCode      = [dataDict valueForKey:@"currencyCode"]; // not used
        _strPackageDesc      = [dataDict valueForKey:@"packageDesc"];  // not used
        _strPackageImgPath   = [dataDict valueForKey:@"packageImagePath"]; // not used
        
        
        _strPackageName      = [packageDatailDict valueForKey:@"pkgName"]; //strPackageName
        _strpackageOfferDesc = [dataDict valueForKey:@"packageOfferDesc"]; // not used
        _strOfferPrisePP     = [dataDict valueForKey:@"staringOfferPricePP"]; // not used
        
        _durationNoDays      = [[packageDatailDict valueForKey:@"duration"]intValue]; //duration
        
        
        NSDictionary *packageSubtype = [packageDatailDict valueForKey:@"pkgSubtypeId"];
        _strPackageSubType   = [packageSubtype valueForKey:@"pkgSubtypeName"];
    
        
        _timeLineList        = [packageDatailDict valueForKey:@"tcilHolidayTimelineCollection"];//tcilHolidayTimelineCollection
        _packageImagePath    = [NSString stringWithFormat:@"%@/images/holidays/%@/%@",kUrlForImage,[packageDatailDict valueForKey:@"packageId"],[packageDatailDict valueForKey:@"packageThumbnailImage"]];//packageThumbnailImage
        _strPackageId        = [packageDatailDict valueForKey:@"packageId"]; //packageId
        
        _strPackageValidFrom = [packageDatailDict valueForKey:@"validFrom"]; //validFrom
        _strPackageValidTo   = [packageDatailDict valueForKey:@"validTo"];//validTo
        
        
        
        NSDictionary *seasonIDDict = [packageDatailDict valueForKey:@"seasonId"];
        
        _strSeasonType      =  [seasonIDDict valueForKey:@"name"];
        
        
        /*
         "seasonId":{
         "altTag":"Summer",
         "imagePath":"Test",
         "isActive":"Y",
         "name":"Summer",
         "seasonId":1
         },
         */
        
        _packagePrise        = [[dataDict valueForKey:@"minimumStartingPrice"] intValue];
        
        _minimumPackagePrise = [[dataDict valueForKey:@"startingPricePremium"] intValue];
        
        
        _packagePriceArray = [packageDatailDict valueForKey:@"tcilHolidayStartingPriceCollection"];
        
        _strPackageType      = [dataDict valueForKey:@"pkgType"];
        
      _mealCollection = [[NSMutableArray alloc] init];
               _visaCollection = [[NSMutableArray alloc] init];
               _hotelCollection = [[NSMutableArray alloc] init];
              _sightseenCollection = [[NSMutableArray alloc] init];
             _accombdationCollection = [[NSMutableArray alloc] init];
        
        
        if(categoryCollection.count>0){
            
            if (([packageDatailDict valueForKey:@"isFlightIncluded"]!=nil && [[[packageDatailDict valueForKey:@"isFlightIncluded"]lowercaseString] isEqualToString:@"y"]))
            {
                _airFlag = YES;
            }else
            {
                _airFlag = NO;
            }
            
            for (NSDictionary * typedict in categoryCollection){
                // NSLog(@"%@",typedict);
                NSString *key = @" ";
                NSString *isIncluded= @" ";
                key = [typedict valueForKey:@"type"];
                
                isIncluded = [typedict valueForKey:@"isTypeIncluded"];
                
                ((void (^)())@{
                    @"Meal" : ^{
                    if ([[isIncluded uppercaseString] isEqualToString:@"Y"]) {
                        _mealsFlag = YES;
                        [_mealCollection addObject:typedict];
                    }
                    
                    
                },
                    //                               @"Flight" : ^{
                    //                    if ([[isIncluded uppercaseString] isEqualToString:@"Y"]) {
                    //                        _airFlag = YES;
                    //                    }
                    //
                    //
                    //                    [flightCollection addObject:typedict];
                    //                },
                    @"Visa" : ^{
                    if ([[isIncluded uppercaseString] isEqualToString:@"Y"]) {
                        _tourMngerFlag = YES;
                        [_visaCollection addObject:typedict];
                    }
                    
                    
                    
                },
                    @"Hotel" : ^{
                    if ([[isIncluded uppercaseString] isEqualToString:@"Y"]) {
                        _accomFlag = YES;
                        [_hotelCollection addObject:typedict];
                    }
                    
                    
                    
                },
                    @"Sightseeing" : ^{
                    if ([[isIncluded uppercaseString] isEqualToString:@"Y"]) {
                        _sightSeeingFlag = YES;
                        [_sightseenCollection addObject:typedict];
                    }
                    
                    
                },
                    @"Accomodation" : ^{
                    if ([[isIncluded uppercaseString] isEqualToString:@"Y"]) {
                        _accomFlag = YES;
                        [_accombdationCollection addObject:typedict];
                    }
                    
                    
                    
                },
                    @"Transfer" : ^{
                    if ([[isIncluded uppercaseString] isEqualToString:@"Y"]) {
                        _transferFlag = YES;
                        [_transferCollection addObject:typedict];
                    }
                    
                    
                    
                },
                             }[key] ?: ^{
                    NSLog(@"default");
                })();
                
            }
        }else{
            
            if (([packageDatailDict valueForKey:@"isMealsIncluded"]!=nil &&[[[packageDatailDict valueForKey:@"isMealsIncluded"]lowercaseString] isEqualToString:@"y"]))
            {
                _mealsFlag = YES;
            }else
            {
                _mealsFlag = NO;
            }
            if (([packageDatailDict valueForKey:@"isFlightIncluded"]!=nil && [[[packageDatailDict valueForKey:@"isFlightIncluded"]lowercaseString] isEqualToString:@"y"]))
            {
                _airFlag = YES;
            }else
            {
                _airFlag = NO;
            }
            
            if (([packageDatailDict valueForKey:@"isVisaIncluded"]!=nil && [[[packageDatailDict valueForKey:@"isVisaIncluded"]lowercaseString] isEqualToString:@"y"]))
            {
                _tourMngerFlag = YES;
            }
            else
            {
                _tourMngerFlag = NO;
            }
            
            if (([packageDatailDict valueForKey:@"isHotelIncluded"]!=nil && [[[packageDatailDict valueForKey:@"isHotelIncluded"]lowercaseString] isEqualToString:@"y"]))
            {
                _accomFlag = YES;
            }else
            {
                _accomFlag = NO;
            }
            
            if (([packageDatailDict valueForKey:@"isSightseeingIncluded"]!=nil && [[[packageDatailDict valueForKey:@"isSightseeingIncluded"]lowercaseString] isEqualToString:@"y"]))
            {
                _sightSeeingFlag = YES;
            }
            else
            {
                _sightSeeingFlag = NO;
            }
            
            // Vijay Added
            if (([packageDatailDict valueForKey:@"isTransferIncluded"]!=nil && [[[packageDatailDict valueForKey:@"isTransferIncluded"]lowercaseString] isEqualToString:@"y"]))
            {
                _transferFlag = YES;
            }
            else
            {
                _transferFlag = NO;
            }
        }
     
       /* //change with holiday collection.
        if (([packageDatailDict valueForKey:@"isMealsIncluded"]!=nil &&[[[packageDatailDict valueForKey:@"isMealsIncluded"]lowercaseString] isEqualToString:@"y"]) ||([packageDatailDict valueForKey:@"isMealsDefaultMsg"]!=nil && [[[packageDatailDict valueForKey:@"isMealsDefaultMsg"]lowercaseString] isEqualToString:@"y"]) )
        {
            _mealsFlag = YES;
        }else
        {
            _mealsFlag = NO;
        }
        if (([packageDatailDict valueForKey:@"isFlightIncluded"]!=nil && [[[packageDatailDict valueForKey:@"isFlightIncluded"]lowercaseString] isEqualToString:@"y"]) ||([packageDatailDict valueForKey:@"isFlightDefaultMsg"]!=nil && [[[packageDatailDict valueForKey:@"isFlightDefaultMsg"]lowercaseString] isEqualToString:@"y"]))
        {
            _airFlag = YES;
        }else
        {
            _airFlag = NO;
        }
        
        if (([packageDatailDict valueForKey:@"isVisaIncluded"]!=nil && [[[packageDatailDict valueForKey:@"isVisaIncluded"]lowercaseString] isEqualToString:@"y"]) ||([packageDatailDict valueForKey:@"isVisaDefaultMsg"]!=nil && [[[packageDatailDict valueForKey:@"isVisaDefaultMsg"]lowercaseString] isEqualToString:@"y"]))
        {
            _tourMngerFlag = YES;
        }
        else
        {
            _tourMngerFlag = NO;
        }
        
       if (([packageDatailDict valueForKey:@"isHotelIncluded"]!=nil && [[[packageDatailDict valueForKey:@"isHotelIncluded"]lowercaseString] isEqualToString:@"y"]) || ([packageDatailDict valueForKey:@"isHotelDefaultMsg"]!=nil && [[[packageDatailDict valueForKey:@"isHotelDefaultMsg"]lowercaseString] isEqualToString:@"y"]))
        {
             _accomFlag = YES;
        }else
        {
             _accomFlag = NO;
        }
        
        if (([packageDatailDict valueForKey:@"isSightseeingIncluded"]!=nil && [[[packageDatailDict valueForKey:@"isSightseeingIncluded"]lowercaseString] isEqualToString:@"y"])||([packageDatailDict valueForKey:@"isSightseeingDefaultMsg"]!=nil && [[[packageDatailDict valueForKey:@"isSightseeingDefaultMsg"]lowercaseString] isEqualToString:@"y"]))
        {
            _sightSeeingFlag = YES;
        }
        else
        {
            _sightSeeingFlag = NO;
        }
        */
        self.stringBookingOnline = [dataDict valueForKey:@"onlineBooking"];
       self.stringPopularFlag = [packageDatailDict valueForKey:@"isTcilRecomended"];
        
        self.pkgStatusId = [[packageDatailDict valueForKey:@"pkgStatusId"] intValue];
        self.Offers = [packageDatailDict valueForKey:@"offers"];
        _tcilHolidayTravelmonthCollection = [packageDatailDict valueForKey:@"tcilHolidayTravelmonthCollection"];
        
        self.startingPriceStandard = [[dataDict valueForKey:@"startingPriceStandard"] intValue];
        self.startingPriceDelux = [[dataDict valueForKey:@"startingPriceDelux"] intValue];
        self.startingPricePremium = [[dataDict valueForKey:@"startingPricePremium"] intValue];
        self.strikeoutPriceStandard = [[dataDict valueForKey:@"strikeoutPriceStandard"] intValue];
        self.strikeoutPriceDelux = [[dataDict valueForKey:@"strikeoutPriceDelux"] intValue];
        self.strikeoutPricePremium = [[dataDict valueForKey:@"strikeoutPricePremium"] intValue];
        
    }
    
    return self;
}
@end
