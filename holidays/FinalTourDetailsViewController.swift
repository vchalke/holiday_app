//
//  FinalTourDetailsViewController.swift
//  sotc-consumer-application
//
//  Created by Saurav Kumar on 09/08/17.
//  Copyright © 2017 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit
import QuickLook

class FinalTourDetailsViewController: UIViewController,UIGestureRecognizerDelegate,UIDocumentInteractionControllerDelegate{
    
    @IBOutlet var briefingSheetView: UIView!
    @IBOutlet var finalVoucherView: UIView!
    
    
    
    var briefingSheetList:[TourPassengerDouments]?
    var finalVoucherList:[TourPassengerDouments]?
    
    var isfromNotificationHomeVc:Bool = false
    
    
    var tourDetail:Tour?
    
    
    
    
    var documentController : UIDocumentInteractionController!
    
    let destination = DownloadRequest.suggestedDownloadDestination(for: .documentDirectory)
    
    let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
    
    var fileurlPath : NSURL!
   
    
    
    //    let destinationPath;
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        
       
      
        
        briefingSheetList = self.getBriefingSheetList()
        
        finalVoucherList = self.getFinalVoucherList()
        
       // self.navigationController?.setNagationBackButton(showRightButton: true, viewController: self)
        
        
        self.addTapGestureRecognizer();
        
 
        
        
    }
    func backButtonClick() -> Void {
        
        
        self.navigationController?.popViewController(animated: true)
        
        self.navigationController?.setNavigationBarTranslucent()
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        
        self.tabBarController?.tabBar.isHidden = true
        
        self.navigationController?.setRedNavigationBar()
        self.navigationController?.setnavigatiobBarTitle(title: Title.FINAL_TOUR_DETAILS, viewController: self)
        self.navigationController?.setEmergencyNavigationRightButton(showEmergencyButton: true, viewController: self)

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        if self.navigationController?.viewControllers.firstIndex(of: self) == nil && self.isfromNotificationHomeVc
        {
            SlideNavigationController.sharedInstance().popViewController(animated: true)
        }
        
    }
    
    
    //MARK: UINavigationController Delegate
    
    func leftButtonClick() -> Void
    {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    //MARK: Tap Gesture - BriefingSheetView & FinalVoucherView
    
    func addTapGestureRecognizer()
    {
        
        let briefingSheetTap = UITapGestureRecognizer(target: self, action: #selector(FinalTourDetailsViewController.onBriefingSheetViewClick))
        briefingSheetTap.delegate = self
        briefingSheetView.addGestureRecognizer(briefingSheetTap)
        
        
        let finalVoucherTap = UITapGestureRecognizer(target: self, action: #selector(FinalTourDetailsViewController.onFinalVoucherViewClick))
        finalVoucherTap.delegate = self
        finalVoucherView.addGestureRecognizer(finalVoucherTap)
    }
    
    
    @objc func onBriefingSheetViewClick()
    {
      /*  let filePath = TourDocumentController.getTourFileDirectoryPath(bfnNumber: "GS17009944")
        
       var filePathDoc = filePath.FinalTourPath + "/NOC FORMATS.JPG";
        
        
        self.viewDownloadedFile(fileurl: URL(fileURLWithPath:filePathDoc))*/
        
        if (self.briefingSheetList != nil && (self.briefingSheetList?.count)! > 0 )
        {
        
            
           
            
                if((self.briefingSheetList?.count)! == 1)
                {
                    let documnet:TourPassengerDouments = self.briefingSheetList!.first!
                    
                    //self.viewDownloadedFile(fileurl: URL(fileURLWithPath:documnet.documnetUrl!))
                    
                    
                    
                    var fileName:String = Title.BRIEFING_SHEET_DETAILS + ".pdf"
                    
                    let passengerNumber = documnet.passengerNumber ?? ""
                    
                    let documentUrl:String = documnet.docummentURL ?? ""
                    
                    if !documentUrl.isEmpty && documentUrl != ""
                    {
                        fileName = TourDocumentController.getDecodedFileName(url:documentUrl)
                    }
                    
                    let saveFileUrl:String =  Title.BRIEFING_SHEET_DETAILS + "/" + passengerNumber + "/" + fileName
                    
                    let filePath = TourDocumentController.getDocumentFilePath(url: saveFileUrl, bfnNumber: documnet.bfNumber!, moduleFolder: Title.FINAL_TOUR_DETAILS)
                    
                    self.downloadDocument(filePath: filePath, url: documnet.docummentURL!)
                    
                }else if((self.briefingSheetList?.count)! > 1 )
                {
                    let passengerDetails:Array<PassengerDetails> = self.getPassengerListDetails(tourPassengerDouments:self.briefingSheetList!, voucherType: Title.BRIEFING_SHEET_DETAILS)
                    
                    let viewController:PassengerListViewController = PassengerListViewController(nibName: "PassengerListViewController", bundle: nil)
                    
                    viewController.passengerDetailsListArray = passengerDetails
                    
                    self.navigationController?.pushViewController(viewController, animated: true)
                    
                }else
                {
            
                }
        }else
        {
            self.displayAlert(message: AlertMessage.FinalTourDetails.NO_DOCUMNET_DATA)
        }
        
        /* let refreshAlert = UIAlertController(title: "Alert", message: "Do you want to download document?.", preferredStyle: UIAlertControllerStyle.alert)
        
       refreshAlert.addAction(UIAlertAction(title: AlertMessage.TITLE_OK, style: .default, handler: { (action: UIAlertAction!) in
            
            let fileExist = self.isFileExistInDocumentDirectory(fileName:"/air format updated tc (1).xlsx")
            
            self.fileurlPath = NSURL(fileURLWithPath: fileExist.destinatioFilePath)
            
            
            if !fileExist.chkFile
            {
                
                self.downloadFileInDocumentDirectory()
            }
                
            else
            {
                
                self.viewDownloadedFile(fileurl: self.fileurlPath as URL)
                
            }
            
            
            
        }))
        
        
        
        
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
            printLog("Handle Cancel Logic here")
        }))
        
        present(refreshAlert, animated: true, completion: nil)*/
        
        
    }
    
    @objc func onFinalVoucherViewClick()
    {
        if (self.finalVoucherList != nil && (self.finalVoucherList?.count)! > 0 )
        {      
             if((self.finalVoucherList?.count)! == 1)
             {
             
                let documnet:TourPassengerDouments = self.finalVoucherList!.first!
                
                //self.viewDownloadedFile(fileurl: URL(fileURLWithPath:documnet.documnetUrl!))
                
                
                var fileName:String = Title.FINAL_VOUCHER_DETAILS + ".pdf"
                
                let passengerNumber = documnet.passengerNumber ?? ""
                
                let documentUrl:String = documnet.docummentURL ?? ""
                
                if !documentUrl.isEmpty && documentUrl != ""
                {
                    fileName = TourDocumentController.getDecodedFileName(url:documentUrl)
                }
                
                let saveFileUrl:String =  Title.FINAL_VOUCHER_DETAILS + "/" + passengerNumber + "/" + fileName
                
                let filePath = TourDocumentController.getDocumentFilePath(url: saveFileUrl, bfnNumber: documnet.bfNumber!, moduleFolder: Title.FINAL_TOUR_DETAILS)
                
                //TourDocumentController.getDocumentFilePathByDecoding(url: documnet.docummentURL! , bfnNumber: documnet.bfNumber!,moduleFolder: Title.FINAL_TOUR_DETAILS)
                
                self.downloadDocument(filePath: filePath, url: documnet.docummentURL!)

                
             }else if((self.finalVoucherList?.count)! > 1 )
             {
                let passengerDetails:Array<PassengerDetails> = self.getPassengerListDetails(tourPassengerDouments:self.finalVoucherList!, voucherType: Title.FINAL_VOUCHER_DETAILS)
                
                let viewController:PassengerListViewController = PassengerListViewController(nibName: "PassengerListViewController", bundle: nil)
                
                viewController.passengerDetailsListArray = passengerDetails
                
                 self.navigationController?.pushViewController(viewController, animated: true)
             
             }else
             {
             
             }
        }else
        {
            self.displayAlert(message: AlertMessage.FinalTourDetails.NO_DOCUMNET_DATA)
        }
        
        
        
        /*let refreshAlert = UIAlertController(title: "Alert", message: "Do you want to download document?.", preferredStyle: UIAlertControllerStyle.alert)
        
        
        
        refreshAlert.addAction(UIAlertAction(title: AlertMessage.TITLE_OK, style: .default, handler: { (action: UIAlertAction!) in
            
            let fileExist = self.isFileExistInDocumentDirectory(fileName:"/manual.pdf")
            
            self.fileurlPath = NSURL(fileURLWithPath: fileExist.destinatioFilePath)
            
            
            if !fileExist.chkFile
            {
                
                self.downloadFileInDocumentDirectory()
            }
                
            else
            {
                
                self.viewDownloadedFile(fileurl: self.fileurlPath as URL)
                
            }
            
            
            
        }))
        
        
        
        
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
            printLog("Handle Cancel Logic here")
        }))
        
        present(refreshAlert, animated: true, completion: nil)*/
        
    }
    
    
    //MARK: UIDocumentInteractionController Delegate
    
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController
    {
        return self
    }
    
    func downloadDocument(filePath:String ,url:String) {
        
        //  let passengerDocumnet:TourPassengerDouments =  documentArray.first!
        
        LoadingIndicatorView.show("Loading")
        
        NetworkCommunication.downloadDocuments(urlString: url, destinationFilePath: filePath, completion: { (downlodedfilePath, status) in
            
            printLog(status,filePath);
            
            if(status)
            {
                self.viewDownloadedFile(fileurl: URL(fileURLWithPath:filePath))
                
            }
            else{
                
                let isFilePresent = TourDocumentController.isFileExistInDocumentDirectory(fileNamePath: filePath)
                
                if(isFilePresent.chkFile)
                {
                    self.viewDownloadedFile(fileurl: URL(fileURLWithPath:filePath))
                    
                }else{
                    
                  //  DispatchQueue.main.sync {
                        
                        LoadingIndicatorView.hide()
                        
                        self.displayAlert(message: AlertMessage.FinalTourDetails.NO_DOCUMNET_DATA)
                   // }
                    
                }
                
            }
            
            
            
            
        })
    }
   
    
    
    func viewDownloadedFile(fileurl : URL) -> Void
    {
        
        let concurrentQueue = DispatchQueue(label: "queuename", attributes: .concurrent)
        concurrentQueue.sync {
            
            
//            UIDocumentInteractionController documentController = new UIDocumentInteractionController();
//            documentController.Url = new NSUrl(filepath, false);
//            string fileExtension = Path.GetExtension(filepath).Substring(1);
//            string uti = UTType.CreatePreferredIdentifier(UTType.TagClassFilenameExtension.ToString(), fileExtension, null);
//            documentController.Uti = uti;
//            
//            UIView presentingView = UIApplication.SharedApplication.KeyWindow.RootViewController.View;
//            documentController.PresentOpenInMenu(CGRect.Empty, presentingView, true);
            
            
          
            
            LoadingIndicatorView.hide()
            
            self.documentController = UIDocumentInteractionController.init(url: fileurl as URL)
            
            //self.documentController.uti =
            self.documentController.name  = Title.VIEW_PDF
            
            self.documentController.delegate = self
            
            self.documentController.presentPreview(animated: true)
            
        }
        
    }
    
    func getBriefingSheetList() ->  Array<TourPassengerDouments> {
        
        if let passengerDocumnets:Array<TourPassengerDouments> =  self.tourDetail?.passengerDocumentRelation?.allObjects as? Array<TourPassengerDouments>
        {
            let briefingSheets = passengerDocumnets.filter(){$0.documentType?.uppercased() == "B SHEET"}
            
            return briefingSheets;
            
        }
        
        return []
        
        
    }
    
    func getFinalVoucherList() ->  Array<TourPassengerDouments> {
        
        if let passengerDocumnets:Array<TourPassengerDouments> =  self.tourDetail?.passengerDocumentRelation?.allObjects as? Array<TourPassengerDouments>
        {
            let briefingSheets = passengerDocumnets.filter(){$0.documentType?.uppercased() == "TCV"}
            
            return briefingSheets;
            
        }
        
        return []
        
    }
    
    func displayAlert(message:String)
    {
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: AlertMessage.TITLE_OK, style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)        }
    
    func getPassengerListDetails(tourPassengerDouments:[TourPassengerDouments],voucherType:String) -> Array<PassengerDetails>
    {
        var passengerDetails:Array<PassengerDetails> = []
        
        
       if let passengerDetailsArray:Array<Passenger> = self.tourDetail?.passengerRelation?.allObjects as? Array<Passenger>
       {
            for tourPassengerDoument in tourPassengerDouments
            {
                
                let passengerDetailArray:Array<Passenger> =  passengerDetailsArray.filter(){$0.passengerNumber! == tourPassengerDoument.passengerNumber}
                
                if(passengerDetailArray != nil && passengerDetailArray.count > 0)
                {
                    for singlePassengerDetail in passengerDetailArray
                    {
                
                        var passengerDetail : PassengerDetails = PassengerDetails()
                    
                        let documnetDictPath = TourDocumentController.getTourFileDirectoryPath(bfnNumber: (tourDetail?.bfNumber)!)
                    
                        var fileName = TourDocumentController.getDecodedFileName(url: tourPassengerDoument.docummentURL!)
                        
                        if fileName.isEmpty || fileName == ""
                        {
                            fileName = voucherType +  ".pdf"
                        }
                    
                        let  folderDocumentPath:String = documnetDictPath.FinalTourPath + "/" + voucherType + "/" + singlePassengerDetail.passengerNumber! + "/" + fileName
                    
                        passengerDetail.displayTitle = singlePassengerDetail.firstName! + " " + singlePassengerDetail.lastName!
                    
                        passengerDetail.subtitle = fileName
                    
                        passengerDetail.filePathUrl = folderDocumentPath
                    
                        passengerDetail.downloadUrl = tourPassengerDoument.docummentURL!
                        
                        passengerDetails.append(passengerDetail)
                        
                    }
                    
                    //passengerDetails.
                }
                
            }
        }
        
        return passengerDetails
    }
    
    
}
