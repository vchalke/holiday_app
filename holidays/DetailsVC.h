//
//  DetailsVC.h
//  holidays
//
//  Created by Pushpendra Singh on 19/08/15.
//  Copyright (c) 2015 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HolidayPackageDetail.h"

@interface DetailsVC : UIViewController<UITableViewDataSource,UITableViewDelegate,UIActionSheetDelegate,UIWebViewDelegate,UIScrollViewDelegate>{
    NSArray *topItems;
    NSMutableArray *subItems; // array of arrays
    
    int currentExpandedIndex;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) IBOutlet UIButton *AccommodationTypeBtn;

@property (strong,nonatomic) NSArray * completePackageDetail;
@property (strong,nonatomic) HolidayPackageDetail *holidayPackageDetail;

- (IBAction)onAccommodationTypeBtnClick:(id)sender;



@end
