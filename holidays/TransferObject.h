//
//  TransferObject.h
//  holidays
//
//  Created by Kush_Tech on 04/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface TransferObject : NSObject
-(instancetype)initWithTransferDict:(NSDictionary *)dictionary;
@property(nonatomic,strong)NSString *transferdescription;
@property (nonatomic) BOOL isActive;
@property (nonatomic) NSInteger packageClassId;
@property (nonatomic) NSInteger transfersId;
@end

NS_ASSUME_NONNULL_END
