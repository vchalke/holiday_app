//
//  LoginViewController.h
//  holidays
//
//  Created by Sarita on 02/03/17.
//  Copyright © 2017 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleSignIn/GoogleSignIn.h>

@interface LoginViewController : UIViewController<GIDSignInDelegate,GIDSignInUIDelegate>
@property(weak, nonatomic) IBOutlet GIDSignInButton *signInButton;
@property (strong, nonatomic) IBOutlet UITextField *userEmailAddress;
@property(strong,nonatomic) id object;
+ (LoginViewController *)sharedInstance;

@property (strong, nonatomic) IBOutlet UIView *loginVCView;
@end
