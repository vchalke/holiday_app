//
//  NSData+AES.h
//  AESEncryptionDemo
//

//

#import <Foundation/Foundation.h>

@interface NSData (AESAlgo)

- (NSData *)AES128EncryptWithKey:(NSString *)key;
- (NSData *)AES128DecryptWithKey:(NSString *)key;

@end
