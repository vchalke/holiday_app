//
//  UITextField+categoryTextField.h
//  holidays
//
//  Created by ketan on 26/10/15.
//  Copyright © 2015 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (categoryTextField)
- (CGRect)textRectForBounds:(CGRect)bounds;
- (CGRect)editingRectForBounds:(CGRect)bounds;
//- (CGRect)leftViewRectForBounds:(CGRect)bounds;
@end
