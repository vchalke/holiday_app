//
//  CalendarViewController.m
//  holidays
//
//  Created by ketan on 19/10/15.
//  Copyright © 2015 Mobicule. All rights reserved.
//

#import "CalendarViewController.h"
#import "RoomsDataModel.h"
#import "TravellerInformationModel.h"

@interface CalendarViewController ()<HorizontalScrollCellDelegate>
{
    NSArray *arrayForRows;
    NSArray *jsonArray;
    int tabSelected;
    NSDictionary *selectedDateDict;
    UIView *previousSelectedView;
    NSString *noOfPacks;
}

@end

@implementation CalendarViewController
@synthesize isMRP,dataDict,roomRecordArray,arrayTravellerCalculation,packageId,hubName;
- (void)viewDidLoad {
   // isMRP = YES;
    [super viewDidLoad];
    [super setHeaderTitle:@"Calendar"];
    tabSelected = 3;
    [[NSBundle mainBundle]loadNibNamed:@"CalendarViewController" owner:self options:nil];
    [super addViewInBaseView:self.calendarView];
    [self groupArray];
    [self setUpCollection];
   }

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
  
}

-(void)groupArray
{
    if (jsonArray.count == 0)
    {
       // NSString *filePath = [[NSBundle mainBundle] pathForResource:@"CalendarResponse" ofType:@"json"];
        
       // NSString *filePath = [[NSBundle mainBundle] pathForResource:@"FareCalender" ofType:@"json"];
       // NSData *data = [NSData dataWithContentsOfFile:filePath];
        
       // NSDictionary *calenderDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        
        
        BOOL isLT = YES;
        
        NSDictionary *calenderDict = dataDict;
        
        NSDictionary *ltResponseBeanDict = [calenderDict valueForKey:@"ltResponseBean"];
        
        if (ltResponseBeanDict == nil || ltResponseBeanDict.count == 0)
        {
            ltResponseBeanDict = [calenderDict valueForKey:@"dbResponseBean"];
            
            isLT = NO;
        }
        
        NSArray *bookableArray = [ltResponseBeanDict valueForKey:@"bookable"];
        
        NSArray *onRequestArray = [ltResponseBeanDict valueForKey:@"onRequest"];
        
        /*
         "AVL_INV":"0",
         "DATE":"01-03-2018",
         "DR_PRICE":477500,
         "DR_STRIKEOUT":487500,
         "INVENTORY":"INVA",
         "LAST_SELL_DAY":"35",
         "LT_PROD_CODE":"TCASTRA010318",
         "NO_OF_SLAB_SEAT":"95",
         "PROD_ITIN_CODE":"2017TCASTRAS1"
         */
        
     
        
        
        if (!isLT)
        {
            if (bookableArray != nil && bookableArray.count != 0)
            {
                NSMutableArray *arrayFordb = [[NSMutableArray alloc] init];
                
                for (int i = 0; i<bookableArray.count; i++)
                {
                    NSDictionary *bookableDict = [bookableArray objectAtIndex:i];
                    
                    NSMutableDictionary *dictBookable = [[NSMutableDictionary alloc] init];
                    [dictBookable setObject:@"" forKey:@"AVL_INV"];
                    [dictBookable setObject:[bookableDict valueForKey:@"date"] forKey:@"DATE"];
                    [dictBookable setObject:[bookableDict valueForKey:@"price"] forKey:@"DR_PRICE"];
                    [dictBookable setObject:@"date" forKey:@"DR_STRIKEOUT"];
                    [dictBookable setObject:@"INVA" forKey:@"INVENTORY"];
                    [dictBookable setObject:@"" forKey:@"LAST_SELL_DAY"];
                    [dictBookable setObject:@"" forKey:@"LT_PROD_CODE"];
                    [dictBookable setObject:@"" forKey:@"NO_OF_SLAB_SEAT"];
                    [dictBookable setObject:@"" forKey:@"PROD_ITIN_CODE"];
                    [arrayFordb addObject:dictBookable];
                }
                
                bookableArray = arrayFordb;
            }
        }
        
        if (!isLT)
        {
            if (onRequestArray != nil && onRequestArray.count != 0)
            {
                NSMutableArray *arrayFordb = [[NSMutableArray alloc] init];
                
                for (int i = 0; i<onRequestArray.count; i++)
                {
                    NSDictionary *onReqDict = [onRequestArray objectAtIndex:i];
                    NSMutableDictionary *dictBookable = [[NSMutableDictionary alloc] init];
                    [dictBookable setObject:@"" forKey:@"AVL_INV"];
                    [dictBookable setObject:[onReqDict valueForKey:@"date"] forKey:@"DATE"];
                    [dictBookable setObject:[onReqDict valueForKey:@"price"] forKey:@"DR_PRICE"];
                    [dictBookable setObject:@"" forKey:@"DR_STRIKEOUT"];
                    [dictBookable setObject:@"ONREQA" forKey:@"INVENTORY"];
                    [dictBookable setObject:@"" forKey:@"LAST_SELL_DAY"];
                    [dictBookable setObject:@"" forKey:@"LT_PROD_CODE"];
                    [dictBookable setObject:@"" forKey:@"NO_OF_SLAB_SEAT"];
                    [dictBookable setObject:@"" forKey:@"PROD_ITIN_CODE"];
                    [arrayFordb addObject:dictBookable];
                }
                
                onRequestArray = arrayFordb;
            }
        }

        
        
        

        
        
        
        NSMutableArray *arrayForDayList = [[NSMutableArray alloc] initWithArray:bookableArray];
        [arrayForDayList addObjectsFromArray:onRequestArray];
        
        
        jsonArray = [[NSArray alloc] initWithArray:arrayForDayList];
        
        NSLog(@"%@",calenderDict);
        
        
        
       // jsonArray = [self.dataDict valueForKey:@"dayList"];
    }
    
    int noOfAdults = 0;
    int noOfChilds = 0;
    for (int i = 0; i<self.roomRecordArray.count; i++)
    {
        RoomsDataModel *dataModel = roomRecordArray[i];
        noOfAdults = noOfAdults + dataModel.adultCount;
        noOfChilds = noOfChilds + (int)dataModel.arrayChildrensData.count;
    }
    noOfPacks = [NSString stringWithFormat:@"%d",noOfAdults+noOfChilds];

    BOOL isFIT = NO;
    if([[self.packageDetail.strPackageSubType lowercaseString] isEqualToString:@"fit"])
    {
        isFIT = YES;
    }
    
    NSArray *arrayTotalDataWithoutDrFilter =[[NSArray alloc]initWithArray:jsonArray];
    
    
    NSPredicate *predicateString = [NSPredicate predicateWithFormat:@"%K > %d", @"DR_PRICE", 0];
    //predicateWithFormat:@"userID == %d", [stdUserNumber intValue]];

    
     NSArray *arrayTotalData = [NSMutableArray arrayWithArray:[arrayTotalDataWithoutDrFilter filteredArrayUsingPredicate:predicateString]];
    
       jsonArray = [[NSArray alloc] initWithArray:arrayTotalData];
    
    if (arrayTotalData.count != 0)
    {
        
        if (tabSelected == 3)
        {
            arrayTotalData = [[NSArray alloc]initWithArray:jsonArray];
        }
        else if (tabSelected == 1)
        {
            NSPredicate *predicateString;
            NSMutableArray *filteredArray =[[NSMutableArray alloc]init] ;
            
//            if (isFIT)
//            {
                predicateString = [NSPredicate predicateWithFormat:@"%K == %@", @"INVENTORY", @"INVA"];
                
                filteredArray = [NSMutableArray arrayWithArray:[arrayTotalData filteredArrayUsingPredicate:predicateString]];
//            }
//            else
//            {
//                    for (int  i = 0 ; i< arrayTotalData.count;i++)
//                    {
//                        NSDictionary *dict  = arrayTotalData[i];
//                        if ([[dict valueForKey:@"AVL_INV"] intValue] >= [noOfPacks intValue])
//                        {
//                            [filteredArray addObject:dict];
//                        }
//                    }
//                    
//            }
            
           
            arrayTotalData = [[NSArray alloc]initWithArray:filteredArray];
        }
        else if (tabSelected == 2)
        {
            NSPredicate *predicateString;
            NSMutableArray *filteredArray =[[NSMutableArray alloc]init] ;
            
//            if (isFIT)
//            {
               predicateString = [NSPredicate predicateWithFormat:@"(%K == %@) or (%K == %@)", @"INVENTORY", @"ONREQA",@"INVENTORY", @"INVNA"];
                filteredArray = [NSMutableArray arrayWithArray:[arrayTotalData filteredArrayUsingPredicate:predicateString]];
//            }
//            else
//            {
//                    for (int  i = 0 ; i< arrayTotalData.count;i++)
//                    {
//                        NSDictionary *dict  = arrayTotalData[i];
//                        if ([[dict valueForKey:@"AVL_INV"] intValue] < [noOfPacks intValue])
//                        {
//                            [filteredArray addObject:dict];
//                        }
//                    }
//            }
            
            
            arrayTotalData = [[NSArray alloc]initWithArray:filteredArray];
        }
        
        NSMutableArray *arrayOfMonths = [[NSMutableArray alloc]init];
        
        for(int i = 1; i<13 ; i++)
        {
            NSPredicate *predicateString;
            if(i <= 9)
            {
            predicateString = [NSPredicate predicateWithFormat:@"%K contains[cd] %@", @"DATE", [NSString stringWithFormat:@"-0%d-",i]];
            }else
            {
            predicateString = [NSPredicate predicateWithFormat:@"%K contains[cd] %@", @"DATE", [NSString stringWithFormat:@"-%d-",i]];
            }
            NSLog(@"predicate %@",predicateString);
            NSArray *filteredArray = [NSMutableArray arrayWithArray:[arrayTotalData filteredArrayUsingPredicate:predicateString]];
            if (filteredArray.count != 0)
            {
                [arrayOfMonths addObject:filteredArray];
            }
        }
        
        NSLog(@"%@",arrayOfMonths);
        [self prepareMonthArray:arrayOfMonths];
        
    }else
    {
        UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Alert"
                                                                           message:@"No Dates Found"
                                                                    preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                                                             
             [alertView dismissViewControllerAnimated:YES completion:nil];
            [[SlideNavigationController sharedInstance] popViewControllerAnimated:NO];
            
                                                         }];
        
        [alertView addAction:okAction];
        
        [self presentViewController:alertView animated:YES completion:nil];
        
        
    }
}


- (IBAction)onTabButtonsClicked:(id)sender
{
    tabSelected = (int)[sender tag];
    [self groupArray];
}

-(void)prepareMonthArray:(NSArray *)array
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd-MM-yyyy"];
    
    NSMutableArray *arrTemp = [NSMutableArray array];
    
    NSMutableArray *sortedArray = [[NSMutableArray alloc] initWithArray:array];
    
    // fast enumeration of the array
    for (int indexI = 1; indexI < sortedArray.count; indexI++)
    {
        for(int indexJ=0; indexJ < (sortedArray.count - indexI); indexJ++)
        {
            
            NSMutableArray *arrOfDataI = [sortedArray objectAtIndex:indexJ];
        
            NSMutableArray *arrOfDataJ = [sortedArray objectAtIndex:(indexJ + 1)];

            NSDate *dateI = [formatter dateFromString:[[[NSDictionary alloc] initWithDictionary:[arrOfDataI objectAtIndex:0]] valueForKey:@"DATE"]];
            
            NSDate *dateJ = [formatter dateFromString:[[[NSDictionary alloc] initWithDictionary:[arrOfDataJ objectAtIndex:0]] valueForKey:@"DATE"]];
            
            if ([dateI compare:dateJ] == NSOrderedDescending)
            {
                arrTemp = [sortedArray objectAtIndex:indexJ];
                
                [sortedArray replaceObjectAtIndex:indexJ withObject:arrOfDataJ];
                
                [sortedArray replaceObjectAtIndex:(indexJ+1) withObject:arrTemp];
            }
        }
        
    }
    
    arrayForRows = [[NSArray alloc]initWithArray:sortedArray];
    
    [self.collection reloadData];
}

-(void)setUpCollection
{
    self.collection.delegate = self;
    self.collection.dataSource = self;
    
    UINib *hsCellNib = [UINib nibWithNibName:@"HorizontalScrollCell" bundle:nil];
    
    [self.collection registerNib:hsCellNib forCellWithReuseIdentifier:@"cvcHsc"];
    
    [self.collection reloadData];
}

#pragma mark -collection View Datasource and Delegate

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return arrayForRows.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    HorizontalScrollCell *hsc =[collectionView dequeueReusableCellWithReuseIdentifier:@"cvcHsc"
                                                                         forIndexPath:indexPath];
    
    [hsc setBackgroundColor:[UIColor clearColor]];
     hsc.title.text = [NSString stringWithFormat:@"IndexPath : %ld",(long)indexPath.row];
     NSArray *arrayForColumn = arrayForRows[indexPath.row];
    BOOL isFIT = NO;
    if([[self.packageDetail.strPackageSubType lowercaseString] isEqualToString:@"fit"])
    {
        isFIT = YES;
    }
    
    [hsc setUpCellWithArray:arrayForColumn withSelectedtab:tabSelected withISFIT:isFIT withISMRP:isMRP withNoOfPacks:noOfPacks];
    [hsc.scroll setFrame:CGRectMake(hsc.scroll.frame.origin.x, hsc.scroll.frame.origin.y, hsc.frame.size.width, 88)];
    
    hsc.cellDelegate = self;
    
    return hsc;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize retval = CGSizeMake(self.view.frame.size.width - 18, 120);
    
    return retval;
}

//-(NSIndexPath*)GetIndexPathFromSender:(id)sender
//{
//    if(!sender)
//    {
//        return nil;
//    }
//    
//    if([sender isKindOfClass:[UITableViewCell class]])
//    {
//        UITableViewCell *cell = sender;
//        return [_tableViewForPassangerDetail indexPathForCell:cell];
//    }
//    
//    return [self GetIndexPathFromSender:((UIView*)[sender superview])];
//}

-(void)cellSelectedWithGesture:(UITapGestureRecognizer *)tapGesture withScrollView:(UIScrollView *)scrollView
{
    UIView *selectedView = (UIView *)tapGesture.view;
   // NSLog(@"%ld",(long)[selectedView tag]);
    int indexForDate = (int)[selectedView tag];
    NSIndexPath *indexPath;
    indexPath = [self.collection indexPathForItemAtPoint:[self.collection convertPoint:scrollView.center fromView:scrollView.superview]];
    NSLog(@"Index path %ld ",(long)indexPath.row);
    
    NSArray *arrayForDays = arrayForRows[indexPath.row];
    NSDictionary *dictForDate = arrayForDays[indexForDate];
    NSLog(@"%@",dictForDate);
    selectedDateDict = dictForDate;
    
//    if([[self.packageDetail.strPackageSubType lowercaseString] isEqualToString:@"fit"])
//    {
        if ([[[dictForDate valueForKey:@"INVENTORY"]lowercaseString] isEqualToString:@"onreqa"] || [[[dictForDate valueForKey:@"INVENTORY"]lowercaseString] isEqualToString:@"invna"] )
        {
            self.packageDetail.stringIsOnReq = @"y";
        }
        else
        {
            self.packageDetail.stringIsOnReq = @"n";
        }
//    }
//    else
//    {
//        
//        if ([[dictForDate valueForKey:@"AVL_INV"] intValue] < [noOfPacks intValue])
//        {
//             self.packageDetail.stringIsOnReq = @"y";
//        }else
//        {
//            self.packageDetail.stringIsOnReq = @"n";
//        }
//    }
    
    [selectedView setBackgroundColor:[UIColor colorFromHexString:@"#F37022"]];
    if (previousSelectedView)
    {
        [previousSelectedView setBackgroundColor:[UIColor clearColor]];
    }
    previousSelectedView = selectedView;
}



- (IBAction)onProceedButtonClicked:(id)sender
{
   /* if (selectedDateDict.count !=0)
    {
        if([[self.packageDetail.strPackageSubType lowercaseString] isEqualToString:@"fit"])
        {
            activityLoadingView = [LoadingView loadingViewInView:self.view.superview.superview
                                                      withString:@""
                                               andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
            [self FetchDataForCalculationsForNonMRP];
        }
        else
        {
            
            activityLoadingView = [LoadingView loadingViewInView:self.view.superview.superview
                                                      withString:@""
                                               andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
            [self FetchDataForCalculationsForMRP];
        }
    }else
    {
         [super showAlertViewWithTitle:@"Alert" withMessage:@"Select Date"];
    }*/
    
    [self.delegate SetDataForCalendarDate:selectedDateDict withCalculationData:nil];
    
    [[SlideNavigationController sharedInstance]popViewControllerAnimated:NO];

    
}

-(void)FetchDataForCalculationsForNonMRP
{
    /*
  {
  "type": "webservice",
  "entity": "packagePrice",
  "action": "search",
  "data": {
  "departureDate": "29-11-2014",
  "packageId": "PKG140372",
  "accomType": "0",
  "totalPsngrs": "2",
  "totalAdults": "2",
  "totalCwb": "0",
  "totalCnb": "0",
  "hub": "DEL"
  }
  }

 */
    
    //fit
    
   /* int noOfAdults = 0;
    int noOfCwb = 0;
    int noOfCnb = 0;
    int infantCount = 0;
    int noOfChilds = 0;
    for (int i = 0; i< self.arrayTravellerCalculation.count; i++)
    {
        TravellerInformationModel *travellerInfoModelInstance = self.arrayTravellerCalculation[i];
        NSString *travellerType = travellerInfoModelInstance.travellerType;
        
        if ([travellerType isEqualToString:kTravellerTypeChild])
        {
            noOfChilds = noOfChilds+1;
        }
        else if ([travellerType isEqualToString:kTravellerTypeAdult])
        {
            noOfAdults = noOfAdults + 1;
        }
        else if ([travellerType isEqualToString:kTravellerTypeInfant])
        {
            infantCount = infantCount + 1;
        }
        
        
    }
    
    
    for (int i = 0; i<self.roomRecordArray.count; i++)
    {
        RoomsDataModel *dataModel = roomRecordArray[i];
        
        NSMutableArray *childArray = dataModel.arrayChildrensData;
        
        for (int j = 0; j<childArray.count; j++)
        {
            NSDictionary *childDict = childArray[j];
            UIButton *button = [childDict valueForKey:@"button"];
            if ([button isSelected]) {
                noOfCwb = noOfCwb+1;
                
            }
            else
            {
                noOfCnb = noOfCnb + 1;
            }
        }
      
    }
    
    
    
    NSMutableDictionary *dictOfData = [[NSMutableDictionary alloc]init];
    [dictOfData setValue:self.packageId  forKey:@"packageId"];
    [dictOfData setValue:[selectedDateDict valueForKey:@"date"]  forKey:@"departureDate"];
    [dictOfData setValue:self.hubName  forKey:@"hub"];
    [dictOfData setValue:[NSString stringWithFormat:@"%d",noOfAdults+noOfChilds+infantCount] forKey:@"totalPsngrs"];
    [dictOfData setValue:[NSString stringWithFormat:@"%d",noOfAdults]  forKey:@"totalAdults"];
    [dictOfData setValue:[NSString stringWithFormat:@"%d",noOfCwb]  forKey:@"totalCwb"];
    [dictOfData setValue:[NSString stringWithFormat:@"%d",noOfCnb]  forKey:@"totalCnb"];
    [dictOfData setValue:[NSString stringWithFormat:@"%d",_accomType]  forKey:@"accomType"];
    
    if ([super connected])
    {

        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NetworkHelper *helper = [NetworkHelper sharedHelper];
            NSString *strResponse = [helper getDataFromServerForType:@"webservice" entity:@"packagePrice" action:@"search" andUserJson:dictOfData];
            NSString *strStatus = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"status"];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if ([strStatus isEqualToString:kStatusSuccess])
                {
                    NSArray *responseArray = [[JsonSerealizer arrayOfJSonFromJsonData:strResponse] valueForKey:@"data"];
                    NSDictionary *responseDict;
                    if (responseArray.count != 0)
                    {
                        responseDict = [[NSDictionary alloc]initWithObjectsAndKeys:responseArray,@"priceList",nil];
                        //responseDict = responseArray[0];
                    }
                    [self.delegate SetDataForCalendarDate:selectedDateDict withCalculationData:responseDict];
                    
                    [[SlideNavigationController sharedInstance]popViewControllerAnimated:NO];
                }
                else
                {
                    NSString *messsage = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"message"];
                    if (messsage)
                    {
                        if ([messsage isEqualToString:kVersionUpgradeMessage])
                        {
                            NSDictionary *downloadDict = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"data"];
                            NSString *downloadURL = [downloadDict valueForKey:@"download_url"];
                            [super showAlertViewForVersionUpdateWithUrl:downloadURL];
                            
                        }
                        else
                        {
                            [super showAlertViewWithTitle:@"Alert" withMessage:@"Oops! Due to limited availability, we are not able to process your request through mobile app.Please Call 18002000464 and share your details"];
                        }
                    }else
                    {
                        [super showAlertViewWithTitle:@"Alert" withMessage:kMessageSomeErrorTryAfterSometime];
                    }
                    
                }
                
                if ([activityLoadingView isDescendantOfView:self.view.superview.superview])
                {
                    [activityLoadingView removeView];
                    activityLoadingView = nil;
                }
            });
        });
    }
    else
    {
        [super showAlertViewWithTitle:@"Alert" withMessage:kMessageNoInternet];
    }*/
    
    
    
  /* 
    "{
    ""mode"":""TCIL"",
    ""ltMarket"":""-1"", ""continent"":""Asia"",""isHsa"":""N"",
    ""room"":[{""roomNo"":1,""noAdult"":2,""noCwb"":0,""noCnbJ"":0,""noCnbS"":0,""inf"":0,""pax"":2}],
    ""optionalsActivities"":[], : Non mandatory
    ""pkgId"":""PKG001424"",
    ""pkgSubType"":2,
    ""pkgSubClass"":""2"",
    ""hub"":""IDR"",
    ""hubCity"":""Indore"",
    ""departureDate"":""23-10-2017"",
    ""ltProdCode"":"""",
    ""userMobileNo"":""9964434636"",
    ""userEmailId"":""aa@hh.ll"",
    ""enquirySource"":""Direct"", : GA - Non mandatory
    ""enquiryMedium"":""Direct"", : GA - Non mandatory
    ""enquiryCampaign"":""not set"", : GA - Non mandatory
    ""LP"":""/"", : GA - Non mandatory
    ""bookURL"":""/holidays/jal-mahotsav?pkgId=PKG001424&packageClas"", : GA - Non mandatory
    ""ltItineraryCode"":""-1"",
    ""regionId"":2,
    ""crmEnquiryId"":"""", Non mandatory
    ""crmStatus"":""N"",
    ""custState"":""27"",
    ""custStateName"":""Maharashtra"",
    ""isUnionTerritory"":""N"",
    ""isGstApplicable"":true
    }"
   */

    @try
    {
        
    NSMutableDictionary *jsonDict = [[NSMutableDictionary alloc] init];
    [jsonDict setObject:@"TCIL" forKey:@"mode"];
    //[jsonDict setObject:_packageDetail.ltMarket forKey:@"ltMarket"];
    [jsonDict setObject:@"-1" forKey:@"ltMarket"];
    [jsonDict setObject:_packageDetail.stringRegion forKey:@"continent"];
    [jsonDict setObject:_packageDetail.isHSA forKey:@"isHsa"];
    [jsonDict setObject:[NSArray new] forKey:@"optionalsActivities"];
    [jsonDict setObject:_packageDetail.strPackageId forKey:@"pkgId"];
    [jsonDict setObject:[NSNumber numberWithInteger:[_packageDetail.strPackageSubTypeID integerValue]] forKey:@"pkgSubType"];
    [jsonDict setObject:@"" forKey:@"pkgSubClass"]; //fit git
    [jsonDict setObject:hubName forKey:@"hub"];
    [jsonDict setObject:@"" forKey:@"hubCity"];
    [jsonDict setObject:[selectedDateDict valueForKey:@"DATE"] forKey:@"departureDate"];
    [jsonDict setObject:[selectedDateDict valueForKey:@"LT_PROD_CODE"] forKey:@"ltProdCode"];
    [jsonDict setObject:@"" forKey:@"userMobileNo"];
    [jsonDict setObject:@"" forKey:@"userEmailId"];
    [jsonDict setObject:@"" forKey:@"enquirySource"];//
    [jsonDict setObject:@"" forKey:@"enquiryMedium"];//
    [jsonDict setObject:@"" forKey:@"enquiryCampaign"];//
    [jsonDict setObject:@"" forKey:@"LP"];//
    [jsonDict setObject:@"" forKey:@"bookURL"];//
    [jsonDict setObject:_packageDetail.ltItineraryCode forKey:@"ltItineraryCode"];
   //  [jsonDict setObject:@"-1" forKey:@"ltItineraryCode"];
    [jsonDict setObject:@"" forKey:@"regionId"]; //
    [jsonDict setObject:@"" forKey:@"crmEnquiryId"]; //
    [jsonDict setObject:@"" forKey:@"crmStatus"];//?? - empty
    
    [jsonDict setObject:@"" forKey:@"custState"];
    [jsonDict setObject:@"" forKey:@"custStateName"];
    
    [jsonDict setObject:@"" forKey:@"isUnionTerritory"];//??
    [jsonDict setObject:@"" forKey:@"isGstApplicable"]; //??

    
    NSDictionary *headerDict = [CoreUtility getHeaderDict];
    
    NetworkHelper *helper = [NetworkHelper sharedHelper];

    [helper getResponseWithRequestType:@"POST" withQueryParam:@"" withPathParam:@"" withJsonParam:jsonDict withHeaders:headerDict withUrl:kAstraUrlFareCalender success:^(NSDictionary *responseDict)
     {
         NSLog(@"Response Dict : %@",responseDict);
         
         
         dispatch_async(dispatch_get_main_queue(), ^(void)
                        {
                            [activityLoadingView removeFromSuperview];
                            if (responseDict)
                            {
                                if (responseDict.count>0)
                                {
                                    
                                }
                            }
                        });
         
     }
                               failure:^(NSError *error)
     {
         dispatch_async(dispatch_get_main_queue(), ^(void)
                     {
                         [activityLoadingView removeFromSuperview];
                         [super showAlertViewWithTitle:@"Alert" withMessage:@"some error occurred"];
                         NSLog(@"Response Dict : %@",error);
                     });
         
     }];

    }
    @catch (NSException *exception)
    {
        NSLog(@"%@", exception.reason);
        [activityLoadingView removeFromSuperview];
    }
    @finally
    {
        NSLog(@"exception finally called");
    }
    
}

-(void)FetchDataForCalculationsForMRP
{
    //nonFit
    
    int noOfSR = 0;
    int noOfDr = 0;
    int noOfTr = 0;
    int noOfCwb = 0;
    int noOfCnb = 0;
    int noOfInf = 0;
    int noOfAdults = 0;
    int noOfChilds = 0;
    int noOfJc = 0;
    
//    noOfInf = noOfInf + infantCount;
//    noOfAdults = noOfAdults + dataModel.adultCount;
//    noOfChilds = noOfChilds + (int)dataModel.arrayChildrensData.count;
    
    for (int i = 0; i< self.arrayTravellerCalculation.count; i++)
    {
        TravellerInformationModel *travellerInfoModelInstance = self.arrayTravellerCalculation[i];
    NSString *travellerRoomingType = travellerInfoModelInstance.travellerRoomingType;
        NSString *travellerType = travellerInfoModelInstance.travellerType;
        
        if ([travellerRoomingType isEqualToString:kTravellerRoomTypeCNB])
        {
               if([[self.packageDetail.strPackageType lowercaseString] isEqualToString:@"international"])
               {
                    noOfCnb = noOfCnb + 1;
               }
               else
               {
                if (travellerInfoModelInstance.travellerAge >=2 && travellerInfoModelInstance.travellerAge <=4 )
                {
                    noOfJc = noOfJc+1;
                }
                else
                {
                    noOfCnb = noOfCnb + 1;
                }
                   
               }
            
           
        }
        else if ([travellerRoomingType isEqualToString:kTravellerRoomTypeCWB])
        {
            noOfCwb = noOfCwb +1;
        }
        
         if ([travellerType isEqualToString:kTravellerTypeChild])
        {
           noOfChilds = noOfChilds + 1;
        }
        else if ([travellerType isEqualToString:kTravellerTypeAdult])
        {
            noOfAdults = noOfAdults + 1;
        }
        else if ([travellerType isEqualToString:kTravellerTypeInfant])
        {
            noOfInf = noOfInf + 1;
        }
    
    //else if ([travellerRoomingType isEqualToString:kTravellerRoomTypeSR])
//        {
//            noOfSR = noOfSR + 1;
//        }else if ([travellerRoomingType isEqualToString:kTravellerRoomTypeDR])
//        {
//            noOfDr = noOfDr + 1 ;
//        }else if ([travellerRoomingType isEqualToString:kTravellerRoomTypeTR])
//        {
//            noOfTr = noOfTr + 1 ;
//        }
    }
    
    
    for (int i = 0; i<self.roomRecordArray.count; i++)
    {
        RoomsDataModel *dataModel = roomRecordArray[i];
         noOfSR = noOfSR + dataModel.SRCount;
        noOfDr = noOfDr + dataModel.DRCount;
        noOfTr = noOfTr + dataModel.TRCount;
        
    }
    
    
    
    /*
     {
     "type": "webservice",
     "entity": "holiday",
     "action": "packagePriceMRP",
     "data": {
     "ltProdCode": "",
     "packageId": "",
     "departureDate": "dd-MM-yyyy",
     "hub": "",
     "noOfPax": "10",
     "noOfSr": "",
     "noOfDr": "",
     "noOfTr": "",
     "noOfQr": "",
     "noOfCwb": "",
     "noOfCnb": "",
     "noOfJc": "",
     "noOfInf": ""
     }
     }
     */

    NSString *ltProductCode = [selectedDateDict valueForKey:@"ltProductCode"];
    NSMutableDictionary *dictOfData = [[NSMutableDictionary alloc]init];
    [dictOfData setValue:ltProductCode  forKey:@"ltProdCode"];
    [dictOfData setValue:self.packageId  forKey:@"packageId"];
    [dictOfData setValue:[selectedDateDict valueForKey:@"date"]  forKey:@"departureDate"];
    [dictOfData setValue:self.hubName  forKey:@"hub"];
    [dictOfData setValue:[NSString stringWithFormat:@"%d",noOfAdults+noOfChilds+noOfInf]  forKey:@"noOfPax"];
    [dictOfData setValue:[NSString stringWithFormat:@"%d",noOfSR] forKey:@"noOfSr"];
    [dictOfData setValue:[NSString stringWithFormat:@"%d",noOfDr]  forKey:@"noOfDr"];
    [dictOfData setValue:[NSString stringWithFormat:@"%d",noOfTr]  forKey:@"noOfTr"];
    [dictOfData setValue:@"0" forKey:@"noOfQr"];
    [dictOfData setValue:[NSString stringWithFormat:@"%d",noOfCwb]  forKey:@"noOfCwb"];
    [dictOfData setValue:[NSString stringWithFormat:@"%d",noOfCnb]  forKey:@"noOfCnb"];
    [dictOfData setValue:[NSString stringWithFormat:@"%d",noOfJc]  forKey:@"noOfJc"];
    [dictOfData setValue:[NSString stringWithFormat:@"%d",noOfInf]  forKey:@"noOfInf"];
    
    if ([super connected])
    {
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NetworkHelper *helper = [NetworkHelper sharedHelper];
            NSString *strResponse = [helper getDataFromServerForType:@"webservice" entity:@"holiday" action:@"packagePriceMRP" andUserJson:dictOfData];
            NSString *strStatus = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"status"];
            //NSString *message = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"message"];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if ([strStatus isEqualToString:kStatusSuccess])
                {
                    NSDictionary *responseDataDict = [[JsonSerealizer arrayOfJSonFromJsonData:strResponse] valueForKey:@"data"];
                    
                    
                    [self.delegate SetDataForCalendarDate:selectedDateDict withCalculationData:responseDataDict];
                    
                    [[SlideNavigationController sharedInstance]popViewControllerAnimated:NO];
                }
                else
                {
                    NSString *messsage = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"message"];
                    if (messsage)
                    {
                        if ([messsage isEqualToString:kVersionUpgradeMessage])
                        {
                            NSDictionary *downloadDict = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"data"];
                            NSString *downloadURL = [downloadDict valueForKey:@"download_url"];
                            [super showAlertViewForVersionUpdateWithUrl:downloadURL];
                            
                        }
                        else
                        {//18002000464,7039003560
                            [super showAlertViewWithTitle:@"Alert" withMessage:@"Oops! Due to limited availability, we are not able to process your request through mobile app.Please Call 8652908370 and share your details"];
                            [[SlideNavigationController sharedInstance] popViewControllerAnimated:NO];
                        }
                    }else
                    {
                        [super showAlertViewWithTitle:@"Alert" withMessage:kMessageSomeErrorTryAfterSometime];
                    }
                    
                }
                
                if ([activityLoadingView isDescendantOfView:self.view.superview.superview])
                {
                    [activityLoadingView removeView];
                    activityLoadingView = nil;
                }
            });
        });
    }
    else
    {
        [super showAlertViewWithTitle:@"Alert" withMessage:kMessageNoInternet];
        
    }
    
    
}

-(void)backButtonAction
{


}


- (IBAction)onDownArrowButtonClick:(id)sender {
    if (arrayForRows.count != 0)
    {
        CGFloat calculatedPosY = _collection.contentOffset.y + 10;
        if (calculatedPosY <= (120 * arrayForRows.count-1)-(_collection.frame.size.height))
        {
            [UIView animateWithDuration:0.2
                             animations:^{_collection.contentOffset = CGPointMake(0.0, calculatedPosY);}
                             completion:^(BOOL finished){ }];
        }
      //  CGFloat calculatedPosY = (120 * arrayForRows.count-1)-(_collection.frame.size.height) ;
       

    }
    
}

- (IBAction)onUpArrowButtonClick:(id)sender {
    
    CGFloat calculatedPosY = (_collection.contentOffset.y - 10) ;
    if (calculatedPosY >= 0)
    {
    [UIView animateWithDuration:0.2
                     animations:^{_collection.contentOffset = CGPointMake(0.0, calculatedPosY);}
                     completion:^(BOOL finished){ }];
    }

}
@end
