//
//  NotificationViewTableViewCell.h
//  holidays
//
//  Created by darshan on 29/11/18.
//  Copyright © 2018 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationViewTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *notificationTitle;
@property (weak, nonatomic) IBOutlet UILabel *notificationMsg;
@property (weak, nonatomic) IBOutlet UILabel *notificationDate;
@property (weak, nonatomic) IBOutlet UIImageView *notificationImage;
@property (weak, nonatomic) IBOutlet UIButton *PlayButton;
@property (weak, nonatomic) IBOutlet UIView *cellView;

@end
