//
//  LandingCollectionViewCell.swift
//  holidays
//
//  Created by Komal Katkade on 11/22/17.
//  Copyright © 2017 Mobicule. All rights reserved.
//

import UIKit

class LandingCollectionViewCell: UICollectionViewCell {

    
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var rightConstraint: NSLayoutConstraint!
    @IBOutlet weak var menuImageView: UIImageView!
    @IBOutlet weak var leftConstraint: NSLayoutConstraint!
    override func awakeFromNib() {
      
        super.awakeFromNib()
        // Initialization code
    }

}
