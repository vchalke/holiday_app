//
//  TravelBlogObject.h
//  holidays
//
//  Created by Kush_Tech on 16/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface TravelBlogObject : NSObject
-(instancetype)initWithTravelBlogsArray:(NSDictionary *)object;
@property(nonatomic,strong)NSString *link;
@property(nonatomic,strong)NSDictionary *title;
@property(nonatomic,strong)NSString *titleString;
@property(nonatomic,strong)NSDictionary *content;

@end

NS_ASSUME_NONNULL_END
