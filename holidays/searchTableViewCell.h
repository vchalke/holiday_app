//
//  searchTableViewCell.h
//  holidays
//
//  Created by Designer on 14/12/15.
//  Copyright © 2015 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface searchTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@end
