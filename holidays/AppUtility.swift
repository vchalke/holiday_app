//
//  AppUtility.swift
//  sotc-consumer-application
//
//  Created by Saurav Kumar on 20/07/17.
//  Copyright © 2017 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit

class AppUtility: NSObject {
    
  class func hexStringToUIColor (hex:String) -> UIColor {
    
    
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
  class  func displayAlert(message:String)
    {
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: AlertMessage.TITLE_OK, style: UIAlertAction.Style.default, handler: nil))
      UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
    
    class func displayAlertController(title:String , message:String) {
        
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: AlertMessage.TITLE_OK, style: UIAlertAction.Style.default, handler: nil))
        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
        
    }

    class func displayAlert(title:String , message:String) {
        
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: AlertMessage.TITLE_OK, style: UIAlertAction.Style.default, handler: nil))
        if let tc = UIApplication.shared.keyWindow?.rootViewController as? UITabBarController {
            if let navigationVc =  tc.selectedViewController as? UINavigationController
            {
                navigationVc.topViewController?.present(alert, animated: true, completion: nil)
            }
        }
    }
    
  class func CallToEmergencyNumber(phoneNumber:String)  {
    
        if let url = URL(string: "telprompt:\(phoneNumber)") {
            
            if UIApplication.shared.canOpenURL(url) {
                
                UIApplication.shared.openURL(url)
            }
            else
            {
                
                AppUtility.displayAlert(message: "Failed to call \(phoneNumber)")
                
            }
        }
        
        
    }

    
    class func getAllImagePresentInDirectory(directoryPath:String) -> Array<String>{
        
        var imagesFilePath:Array<String> = []
        
        let files = FileManager.default.enumerator(atPath: directoryPath)
        
        for file in (files?.allObjects as! [String]) {
            
            
            if URL(fileURLWithPath: file).pathExtension.lowercased() == "jpg" || URL(fileURLWithPath: file).pathExtension.lowercased() == "png" || URL(fileURLWithPath: file).pathExtension.lowercased() == "jpeg" {
                
                imagesFilePath.append("\(directoryPath)" + "/" + "\(file)")
                
            }
            
        }
        
        return imagesFilePath
        
        
        
    }

    
   class func convertDateToString(date:Date?) -> String
    {
        if  let dateValue:Date = date
        {
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let myString = formatter.string(from: dateValue)
        // convert your string to date
        let yourDate = formatter.date(from: myString)
        //then again set the date format whhich type of output you need
        formatter.dateFormat = "dd-MMM-yyyy"
        // again convert your date to string
        let myStringafd = formatter.string(from: yourDate!)
        
        printLog(myStringafd)
        
        return myStringafd
        }
        return ""
    }
    
    class func convertDateToString(currentDateFormat:String,date:Date,requiredDateFormat:String) -> String
    {
        if date != nil{
            
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date
        formatter.dateFormat = currentDateFormat
        
        let myString = formatter.string(from: date)
        // convert your string to date
        let yourDate = formatter.date(from: myString)
        //then again set the date format whhich type of output you need
        formatter.dateFormat = requiredDateFormat
        // again convert your date to string
        let myStringafd = formatter.string(from: yourDate!)
        
        printLog(myStringafd)
        
        return myStringafd
        }
        
        return ""
    }
    
    class func convertStringToDate(string:String ,format:String) -> Date
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format// Your date format
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")//TimeZone.current
        let serverDate: Date = dateFormatter.date(from: string)! // according to date format your date string
        return serverDate
    }
    class func convertStartDateToString(string:String ,format:String) -> Date
    {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")//TimeZone.current
        dateFormatter.dateFormat = format
        let date = dateFormatter.date(from:string)!
        return date
    }
    class func changeDateFormat(currentDateFormat:String, date:String , requiredDateFormat:String) -> String
    {
        
        
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date
        formatter.dateFormat = currentDateFormat
       
        let yourDate = formatter.date(from: date)
        //then again set the date format whhich type of output you need
        formatter.dateFormat = requiredDateFormat
        // again convert your date to string
        let myStringafd = formatter.string(from: yourDate!)
        
        printLog(myStringafd)
        
        return myStringafd
    }
    
    
    class func convertDateToString(date:Date , dateFormat:String) -> String
    {
        let formatter = DateFormatter()
        formatter.dateFormat = dateFormat
        
        let myStringafd = formatter.string(from: date)
        
  
        
        return myStringafd
    }
    
    
    class func checkStirngIsEmpty(value:String)->Bool
    {
        let trimString:String = value.trimmingCharacters(in: .whitespaces)
        
        if !trimString.isEmpty
        {
            return false
        }
        
        return true
    }
    
    class  func stringRepresenation(fromJSonDataObject dataObject: Any) -> String
    {
        let jsonData: Data? = try? JSONSerialization.data(withJSONObject: dataObject, options: .prettyPrinted)
        let jsonString = String(data: jsonData!, encoding: String.Encoding.utf8)
        let strFormattedString: String? = jsonString?.replacingOccurrences(of: "\\/", with: "/")
        
        return strFormattedString!
    }
    
    
  class   func setTabbarController() -> Void {
        
        
        UITabBarItem.appearance().setTitleTextAttributes(convertToOptionalNSAttributedStringKeyDictionary([convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor): UIColor.gray]), for:.normal)
        
        UITabBarItem.appearance().setTitleTextAttributes(convertToOptionalNSAttributedStringKeyDictionary([convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor): UIColor.black]), for:.selected)
        
        
        
        let userBookingVC : UserBookingHomeViewController = UserBookingHomeViewController (nibName: "BaseViewController", bundle: nil)
        userBookingVC.title = Title.MY_BOOKING
        userBookingVC.tabBarItem.selectedImage = #imageLiteral(resourceName: "userBookingTabSelected").withRenderingMode(.alwaysOriginal)
        userBookingVC.tabBarItem.image = #imageLiteral(resourceName: "userBookingTabUnSelected").withRenderingMode(.alwaysOriginal)
        
        
        let menuVC : MenuViewController = MenuViewController (nibName: "MenuViewController", bundle: nil)
        menuVC.title = "MORE"
        menuVC.tabBarItem.selectedImage  = #imageLiteral(resourceName: "moreTabSelected").withRenderingMode(.alwaysOriginal)
        menuVC.tabBarItem.image = #imageLiteral(resourceName: "moreTabUnSelected").withRenderingMode(.alwaysOriginal)
        
        
        let notificationVC : SOTCNotificationViewController = SOTCNotificationViewController (nibName: "SOTCNotificationViewController", bundle: nil)
        notificationVC.title = "NOTIFICATION"
        notificationVC.tabBarItem.selectedImage  = #imageLiteral(resourceName: "notificationTabSelected").withRenderingMode(.alwaysOriginal)
        notificationVC.tabBarItem.image = #imageLiteral(resourceName: "notificationTabUnSelected").withRenderingMode(.alwaysOriginal)
        
        
        let userBookingNavVC : UINavigationController = UINavigationController(rootViewController: userBookingVC)
        let notificationNavVC : UINavigationController = UINavigationController(rootViewController: notificationVC)
        let menuNavVC : UINavigationController = UINavigationController(rootViewController: menuVC)
        
        
        let applicationTabbar : UITabBarController = UITabBarController()
        
        applicationTabbar.setViewControllers([userBookingNavVC], animated: true)
        
        SlideNavigationController.sharedInstance().pushViewController(applicationTabbar, animated: true)
        
    }
    
    @objc class func setLoginController() -> Void {
        
        let loginViewController : SOTCLoginViewController = SOTCLoginViewController(nibName: "BaseViewController", bundle: nil)
        
        
           SlideNavigationController.sharedInstance().pushViewController(loginViewController, animated: true)
        
        
    }
    
    class func setPaymentViewController() -> Void {
        
        let paymentViewController : PaymentViewController = PaymentViewController(nibName: "BaseViewController", bundle: nil)
        
        
        SlideNavigationController.sharedInstance().pushViewController(paymentViewController, animated: true)
        
        
    }
    
    class func setTicketViewController() -> Void {
        
        let ticketViewController : TicketsViewController = TicketsViewController(nibName: "BaseViewController", bundle: nil)
        
        
        SlideNavigationController.sharedInstance().pushViewController(ticketViewController, animated: true)
        
        
    }
    
    class func setOtherTourDocuments() -> Void {
        
        let tourViewController : TourDetailViewController = TourDetailViewController(nibName: "BaseViewController", bundle: nil)
        
        
        SlideNavigationController.sharedInstance().pushViewController(tourViewController, animated: true)
        
        
    }
    class func setProfileViewController() -> Void {
        
        let userViewController : UserProfileViewController = UserProfileViewController(nibName: "BaseViewController", bundle: nil)
        
        
        SlideNavigationController.sharedInstance().pushViewController(userViewController, animated: true)
        
        
    }
    
    
    class func setDeviationViewController() -> Void {
        
        let deviationViewController : DeviationViewController = DeviationViewController(nibName: "BaseViewController", bundle: nil)
        
        
        SlideNavigationController.sharedInstance().pushViewController(deviationViewController, animated: true)
        
        
    }
    
    
    class func setVisaViewController() -> Void {
        
        let visaViewController : VisaViewController = VisaViewController(nibName: "BaseViewController", bundle: nil)
        
        
        SlideNavigationController.sharedInstance().pushViewController(visaViewController, animated: true)
        
        
    }
    
    class func setFeedbackViewController() -> Void {
        
        let feedBackController : FeedbackWebViewController = FeedbackWebViewController(nibName: "BaseViewController", bundle: nil)
        
        
        SlideNavigationController.sharedInstance().pushViewController(feedBackController, animated: true)
        
        
    }
    
    
    class func getHomeController(redirectionController:UIViewController) -> UITabBarController  {
        
        UITabBarItem.appearance().setTitleTextAttributes(convertToOptionalNSAttributedStringKeyDictionary([convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor): UIColor.gray]), for:.normal)
        
        UITabBarItem.appearance().setTitleTextAttributes(convertToOptionalNSAttributedStringKeyDictionary([convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor): UIColor.black]), for:.selected)
        
        
        
        let userBookingVC : UserBookingHomeViewController = UserBookingHomeViewController (nibName: "BaseViewController", bundle: nil)
        userBookingVC.title = Title.MY_BOOKING
        userBookingVC.tabBarItem.selectedImage = #imageLiteral(resourceName: "userBookingTabSelected").withRenderingMode(.alwaysOriginal)
        userBookingVC.tabBarItem.image = #imageLiteral(resourceName: "userBookingTabUnSelected").withRenderingMode(.alwaysOriginal)
        
        redirectionController.navigationController?.navigationBar.isHidden = false
        userBookingVC.isFromNotification = true
        userBookingVC.redirectController = redirectionController
        
        
        let menuVC : MenuViewController = MenuViewController (nibName: "MenuViewController", bundle: nil)
        menuVC.title = "MORE"
        menuVC.tabBarItem.selectedImage  = #imageLiteral(resourceName: "moreTabSelected").withRenderingMode(.alwaysOriginal)
        menuVC.tabBarItem.image = #imageLiteral(resourceName: "moreTabUnSelected").withRenderingMode(.alwaysOriginal)
        
        
        let notificationVC : SOTCNotificationViewController = SOTCNotificationViewController (nibName: "SOTCNotificationViewController", bundle: nil)
        notificationVC.title = "NOTIFICATION"
        notificationVC.tabBarItem.selectedImage  = #imageLiteral(resourceName: "notificationTabSelected").withRenderingMode(.alwaysOriginal)
        notificationVC.tabBarItem.image = #imageLiteral(resourceName: "notificationTabUnSelected").withRenderingMode(.alwaysOriginal)
        
        
        let userBookingNavVC : UINavigationController = UINavigationController(rootViewController: userBookingVC)
        let notificationNavVC : UINavigationController = UINavigationController(rootViewController: notificationVC)
        let menuNavVC : UINavigationController = UINavigationController(rootViewController: menuVC)
        
        
        let applicationTabbar : UITabBarController = UITabBarController()
        
        applicationTabbar.setViewControllers([userBookingNavVC], animated: true)
        
        return applicationTabbar
    }
    
    class func getLoginController() -> SOTCLoginViewController  {
        
        let loginViewController : SOTCLoginViewController = SOTCLoginViewController(nibName: "BaseViewController", bundle: nil)
        
        return loginViewController
    }
    
    
   
   class func isValidEmail(testStr:String) -> Bool {
        
        printLog("validate emilId: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr)
        return result
        
    }
    
   @objc class func checkAndSubscibeNotificationOnServer()
    {
        if (UserDefaults.standard.bool(forKey: UserDefauldConstant.isUserLoggedIn))
        {
            
            if (UserDefaults.standard.bool(forKey: UserDefauldConstant.isRegisterForPushNotification)) {
                
                if !(UserDefaults.standard.bool(forKey: UserDefauldConstant.isSubscribeForPushNotification)) {
                    
                    NotificationController.sendNotificationSubscriberRequest { (Bool, String) in
                        
                        if Bool
                        {
                            UserDefaults.standard.set(true, forKey: UserDefauldConstant.isSubscribeForPushNotification)
                            
                            printLog("Notification subscription registered on server")
                        }
                        else
                        {
                            UserDefaults.standard.set(false, forKey: UserDefauldConstant.isSubscribeForPushNotification)
                            printLog("Notification subscription Faield")
                            
                        }
                        
                        
                    }
                }else
                {
                    
                    printLog("AllReady subscription done -- server")
                    
                    
                }
                
            }
            else
            {
                printLog("isRegisterForPushNotification  false -- device tocken sub")
            }
        }else
        {
            printLog("user is not logged in -- device tocken submission")
        }
        
    }
    
    @objc class func checkNetConnection() -> Bool
    {
        
        if (NetworkReachabilityManager.init()?.isReachable)! {
            
            return true
            ///printLog("Reachable")
        }
        else
        {
            AppUtility.displayAlert(message: AlertMessage.CHECK_NET_CONNECTION)
            return false
            
        }
    }
    
   @objc class func checkIsVersionUpgrade()
    {
        UserBookingController.getAppVersion { (status, version) in
            
            let deviceversion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
            
            
            if status && version != nil
            {
                var serverersionReplacesString = version!
                
                if version!.contains("v")
                {
                     serverersionReplacesString = version?.replacingOccurrences(of: "v", with: "") ?? ""
                }
                
                if version!.contains("V")
                {
                     serverersionReplacesString = version?.replacingOccurrences(of: "V", with: "") ?? ""
                }
               
                
                 let serverersionArray = serverersionReplacesString.split(separator: ".")
               
                if serverersionArray.count > 0
                {
                    let deviceVersionArray = deviceversion?.split(separator: ".")
                    
                    if deviceVersionArray?.count ?? 0 > 0
                    {
                        let serverersion = serverersionArray[0]
                        let serverVersionFirstDigit:NSInteger = NSInteger(serverersion)!
                        
                        let deviceverersionStr = deviceVersionArray![0]
                        let deviceVersionFirstDigit:NSInteger = NSInteger(deviceverersionStr)!
                        
                        if serverVersionFirstDigit > deviceVersionFirstDigit
                        {
                            let alert = UIAlertController(title: "Alert", message: AlertMessage.VERSION_UPGRADE_MSG, preferredStyle: UIAlertController.Style.alert)
                            
                            alert.addAction(UIAlertAction(title: AlertMessage.TITLE_OK, style: UIAlertAction.Style.default, handler: { (alert) in
                                
                                let url = URL(string: kVersionUpgradeAppstoreURL)
                                
                                if #available(iOS 10.0, *)
                                {
                                    UIApplication.shared.open(url!, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: { (result) in
                                        
                                        LoadingIndicatorView.hide()
                                    })
                                }
                                else
                                {
                                    UIApplication.shared.openURL(url!)
                                }
                            }))
                            
                            UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
                            
                        }
                    }
                    
                    
                    
                }
                
                
                
                
                
                /// serverersion = serverersion.toLengthOf(length:0)
                
             /*   if serverersion.compare(deviceversion!) == .orderedDescending
                {
                    let firstDigitOfServerVersion:Character = serverersion.first!
                    
                    let firstDigitOfDeviceVersion:Character = (deviceversion?.first)!
                    
                    if String(describing: firstDigitOfServerVersion).compare(String(firstDigitOfDeviceVersion)) == .orderedDescending
                    {
                        let alert = UIAlertController(title: "Alert", message: AlertMessage.VERSION_UPGRADE_MSG, preferredStyle: UIAlertControllerStyle.alert)
                        
                        alert.addAction(UIAlertAction(title: AlertMessage.TITLE_OK, style: UIAlertActionStyle.default, handler: { (alert) in
                            
                            let url = URL(string: kVersionUpgradeAppstoreURL)
                            
                            if #available(iOS 10.0, *)
                            {
                                UIApplication.shared.open(url!, options: [:], completionHandler: { (result) in
                                    
                                    LoadingIndicatorView.hide()
                                })
                            }
                            else
                            {
                                UIApplication.shared.openURL(url!)
                            }
                        }))
                        
                        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)

                    }
                    else
                    {
                       /* let alert = UIAlertController(title: "Alert", message: AlertMessage.VERSION_UPGRADE_MSG, preferredStyle: UIAlertControllerStyle.alert)
                        
                        alert.addAction(UIAlertAction(title: AlertMessage.TITLE_OK, style: UIAlertActionStyle.default, handler: { (alert) in
                            
                            let url = URL(string: kVersionUpgradeAppstoreURL)
                            
                            if #available(iOS 10.0, *)
                            {
                                UIApplication.shared.open(url!, options: [:], completionHandler: { (result) in
                                    
                                    LoadingIndicatorView.hide()
                                })
                            }
                            else
                            {
                                UIApplication.shared.openURL(url!)
                            }
                        }))
                        
                        alert.addAction(UIAlertAction(title: AlertMessage.TITLE_CANCEL, style: UIAlertActionStyle.cancel, handler: { (alert) in
                        
                        }))

                        
                        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)*/

                    }
                    
                }*/
            }
        }
        
    }
    //get uuid method.
    class func setUUID() ->String  {
        
        let uuID = KeychainService.loadUUID()
        
        if uuID != nil || uuID != ""{
            
            KeychainService.saveUUID(token:  "\(UIDevice.current.identifierForVendor?.uuidString ?? "")" as NSString)
            
            printLog(KeychainService.loadUUID() ?? "")
            
            return KeychainService.loadUUID()! as String
            
        }
        
        return uuID! as String
        
    }
//MARK:- Call A Helpeline Number
    @objc class func callToTCFromPhaseTwo()
    {
        guard let number = URL(string: "telprompt://18002099100") else { return }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(number)
        } else {
            UIApplication.shared.openURL(number)
        }
    }
    //MARK:- START Check is Device JailBroken
    
    class func jailbroken(application: UIApplication) -> Bool {
//        return true
        guard let cydiaUrlScheme = NSURL(string: "cydia://package/com.example.package") else { return isJailbroken() }
        return application.canOpenURL(cydiaUrlScheme as URL) || isJailbroken()
    }
    
    class func isJailbroken() -> Bool {
        
        if TARGET_IPHONE_SIMULATOR == 1 {
            return false
        }
        
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: "/Applications/Cydia.app") ||
            fileManager.fileExists(atPath: "/Library/MobileSubstrate/MobileSubstrate.dylib") ||
            fileManager.fileExists(atPath: "/bin/bash") ||
            fileManager.fileExists(atPath: "/usr/sbin/sshd") ||
            fileManager.fileExists(atPath: "/etc/apt") ||
            fileManager.fileExists(atPath: "/usr/bin/ssh") ||
            fileManager.fileExists(atPath: "/private/var/lib/apt/"){
            return true
        }
        
        if canOpen(path: "/Applications/Cydia.app") ||
            canOpen(path: "/Library/MobileSubstrate/MobileSubstrate.dylib") ||
            canOpen(path: "/bin/bash") ||
            canOpen(path: "/usr/sbin/sshd") ||
            canOpen(path: "/etc/apt") ||
            canOpen(path: "/usr/bin/ssh") {
            return true
        }
        
        let path = "/private/" + NSUUID().uuidString
        do {
            try "anyString".write(toFile: path, atomically: true, encoding: String.Encoding.utf8)
            try fileManager.removeItem(atPath: path)
            return true
        } catch {
            return false
        }
    }
    
    class func canOpen(path: String) -> Bool {
        let file = fopen(path, "r")
        guard file != nil else { return false }
        fclose(file)
        return true
    }
   
    
   @objc class func checkIsDeviceRooted()
    {
       if AppUtility.isJailbroken() || jailbroken(application: UIApplication.shared)
       {
        
        let topWindow:UIWindow  = UIWindow(frame: UIScreen.main.bounds)
        topWindow.rootViewController = UIViewController()
        topWindow.windowLevel = UIWindow.Level.alert + 1
        
        let alert = UIAlertController(title: "", message: AlertMessage.ROOTED_DEVICE_MSG, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: AlertMessage.TITLE_OK, style:  UIAlertAction.Style.default, handler: { (alert) in
            exit(0)
        }))
     
        topWindow.makeKeyAndVisible()
        topWindow.rootViewController?.present(alert, animated: true, completion: nil)
        
        }
    }
    
    class func fillPricingModelWithResponseDict(dict:Dictionary<String,Any>,pricingModel: PricingModel,quotationView: QuotationView)
       {
           var responseDict : Dictionary<String,Any> = dict
           let holidayResponseBean : Dictionary<String,Any> = responseDict["holidayResponseBean"] as? Dictionary<String,Any> ?? Dictionary.init()
           if holidayResponseBean.count > 0
           {
               responseDict = responseDict["holidayResponseBean"] as! Dictionary<String,Any>
           }
          
           if let promoCodeAmount : Dictionary<String,Any> = dict["promoCodeAmount"] as? Dictionary<String,Any>
           {
               pricingModel.discountAdvancePrice = promoCodeAmount["discountAmount"] as? Double ?? 0
               pricingModel.totalTourCost = String(describing: promoCodeAmount["revisedBookingAmount"] ?? 0.0)
               let priceResponseBean : Dictionary<String,Any> = responseDict["priceResponseBean"] as? Dictionary<String,Any> ?? Dictionary.init()
               pricingModel.advancePayableAmount = priceResponseBean["advancePayableAmount"] as? Double ?? 0
              // pricingModel.totalTourCost = priceResponseBean["totalPrice"] as? String ?? ""
               pricingModel.totalServicetaxAdvanceDiscount = priceResponseBean["totalServicetaxAdvanceDiscount"] as? Double ?? 0
               pricingModel.totalServicetax = priceResponseBean["totalServicetax"] as? Double ?? 0
               pricingModel.totalOptionalCost =  priceResponseBean["totalOptionalCost"] as? Double ?? 0
               pricingModel.gSTDescription = priceResponseBean["gSTDescription"] as? Array<Dictionary<String,Any>> ?? Array.init()
               pricingModel.currencyBreakup = priceResponseBean["currencyBreakup"] as? Array<Dictionary<String,Any>> ?? Array.init()
               quotationView.quoteID =  priceResponseBean["quotationId"] as? String ?? ""
            pricingModel.tcsAmount = priceResponseBean["tcsAmount"] as? Double ?? 0
            pricingModel.tcsPercentage = priceResponseBean["tcsPercentage"] as? Double ?? 0
            pricingModel.isInternational = pricingModel.tcsAmount > 0
           }
           else
           {
               pricingModel.discountAdvancePrice = responseDict["discountAmount"] as? Double ?? 0
              // pricingModel.totalTourCost = responseDict["revisedBookingAmount"] as? String ?? ""
               pricingModel.advancePayableAmount = responseDict["advancePayableAmount"] as? Double ?? 0
               pricingModel.totalTourCost = String(describing: responseDict["totalPrice"] ?? 0.0) as? String ?? ""
               pricingModel.totalServicetaxAdvanceDiscount = responseDict["totalServicetaxAdvanceDiscount"] as? Double ?? 0
               pricingModel.discountAdvancePrice = responseDict["discountAdvancePrice"] as? Double ?? 0
               pricingModel.totalServicetax = responseDict["totalServicetax"] as? Double ?? 0
               pricingModel.totalOptionalCost =  responseDict["totalOptionalCost"] as? Double ?? 0
               pricingModel.gSTDescription = responseDict["gSTDescription"] as? Array<Dictionary<String,Any>> ?? Array.init()
               pricingModel.currencyBreakup = responseDict["currencyBreakup"] as? Array<Dictionary<String,Any>> ?? Array.init()
               quotationView.quoteID =  responseDict["quotationId"] as? String ?? ""
            pricingModel.tcsAmount = responseDict["tcsAmount"] as? Double ?? 0
            pricingModel.tcsPercentage = responseDict["tcsPercentage"] as? Double ?? 0
            pricingModel.isInternational = pricingModel.tcsAmount > 0
           }
        pricingModel.discountFullPrice = responseDict["discountFullPrice"] as? Double ?? 0
          pricingModel.promocodeAmount = responseDict["promocodeAmount"] as? Double ?? 0
           
       }

    class func displayToastMessage(_ message : String){
        let toastView = UILabel()
        toastView.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        toastView.textColor = UIColor.white
        toastView.textAlignment = .center
        toastView.font = UIFont(name:"Roboto-Regular", size:15)
        toastView.layer.cornerRadius = 10
        toastView.layer.masksToBounds = true
        toastView.text = message
        toastView.numberOfLines = 0
        toastView.alpha = 0
        toastView.translatesAutoresizingMaskIntoConstraints = false
        
        let window = UIApplication.shared.delegate?.window!
        window?.addSubview(toastView)
        
        let horizontalCenterContraint: NSLayoutConstraint = NSLayoutConstraint(item: toastView, attribute: .centerX, relatedBy: .equal, toItem: window, attribute: .centerX, multiplier: 1, constant: 0)
        
        let widthContraint: NSLayoutConstraint = NSLayoutConstraint(item: toastView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1, constant: 300)
        
        let verticalContraint: [NSLayoutConstraint] = NSLayoutConstraint.constraints(withVisualFormat: "V:|-(>=200)-[toastView(==50)]-68-|", options: [.alignAllCenterX, .alignAllCenterY], metrics: nil, views: ["toastView": toastView])
        
        NSLayoutConstraint.activate([horizontalCenterContraint, widthContraint])
        NSLayoutConstraint.activate(verticalContraint)
        
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseIn, animations: {
            toastView.alpha = 1
        }, completion: nil)
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double((Int64)(2 * NSEC_PER_SEC)) / Double(NSEC_PER_SEC), execute: {
            UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseIn, animations: {
                toastView.alpha = 0
            }, completion: { finished in
                toastView.removeFromSuperview()
            })
        })
    }


       class func getPricingRequestJson(packageDetailModel : PackageDetailsModel,pricingModel : PricingModel,optionalArray : Array<OptionalModel>?) -> Dictionary<String,Any> {
           /*
            {"mode":"TCIL","ltMarket":"EFIT","room":[{"roomNo":1,"noAdult":2,"noCwb":0,"noCnbJ":0,"noCnbS":0,"inf":0,"pax":2}],"optionalsActivities":[],"pkgId":"PKG002815","pkgSubType":6,"pkgSubClass":"0","hub":"BLR","hubCity":"Bengaluru","departureDate":"26-07-2018","ltProdCode":"FAR904260718","userMobileNo":"7777777777","userEmailId":"yogeshwar.patil@in.thomascook.com","enquirySource":"Direct","enquiryMedium":"Direct","enquiryCampaign":"not set","LP":"/","bookURL":"/holidays/easy-thailand-3-star?pkgId=PKG002815&packageClassId=0&destination=PKG002815_asia_CONTINENT_1&destTag=Thailand","ltItineraryCode":"2018FAR904S1","regionId":0,"crmEnquiryId":"","crmStatus":"N","custStateName":"Maharashtra","isGstApplicable":true,"websiteTerm":"","agentId":null,"isOnBehalf":"N"} */
           
        var dataDict : Dictionary<String,Any> = Dictionary.init()//
        dataDict[PricingConstants.Mode] = "TCIL" // HARDCODED ***
        dataDict[PricingConstants.ltMarket] = packageDetailModel.ltMarket
        let user:String = UserDefaults.standard.value(forKey: kLoginEmailId) as? String ?? ""
//        let user:String = UserDefaults.standard.value(forKey: APIConstants.User) as? String ?? ""
           var roomArray : Array<Dictionary<String,Any>> = Array.init()
           for i in 0..<pricingModel.pricingArrayRooms.count
           {
               let roomModel : RoomModel = pricingModel.pricingArrayRooms[i]
               var roomDict : Dictionary<String,Any> = Dictionary.init()
               roomDict["roomNo"] = i + 1
               roomDict["noAdult"] = roomModel.adultCount
               roomDict["noCwb"] = roomModel.childWithBedCount
               roomDict["noCnbJ"] = 0
               roomDict["noCnbS"] = roomModel.childWithoutBedCount
               roomDict["inf"] = roomModel.infantCount
//               roomDict["pax"] = roomModel.adultCount + roomModel.childWithBedCount + roomModel.childWithoutBedCount
            roomDict["pax"] = roomModel.adultCount + roomModel.childWithBedCount + roomModel.childWithoutBedCount + roomModel.infantCount // Copied From SOTC and then added infant count
               roomArray.append(roomDict)
           }
           dataDict[PricingConstants.room] = roomArray
           
           if optionalArray == nil
           {
               dataDict[PricingConstants.optionalsActivities] = []
           }
           else
           {
               var selectedOptionalEvents : Array<Dictionary<String,Any>> = Array.init()
               for optionalModel in optionalArray!
               {
                   if optionalModel.isSelected
                   {
                       /*
    "adultNo": "1",
    "childNo": "0",
    "currencyCode": "EUR",
    "infantNo": "0",
    "name": "Moulin Rouge 1st Show and 1-2 champagne (without transfer) (Kids below 6 years not allowed)",
    "optionalsId": 39
    */
                       var dict : Dictionary<String,Any> = Dictionary.init()
                       dict["adultNo"] = "\(optionalModel.adultCount)"
                       dict["childNo"] = "\(optionalModel.childCount)"
                       dict["currencyCode"] = "\(optionalModel.currencyCode)"
                       dict["infantNo"] = "\(optionalModel.infantCount)"
                       dict["name"] = "\(optionalModel.eventName)"
                       dict["optionalsId"] = optionalModel.optionalID
                       selectedOptionalEvents.append(dict)
                   }
                  
               }
                dataDict[PricingConstants.optionalsActivities] = selectedOptionalEvents
               
           }
           dataDict[PricingConstants.pkgId] = packageDetailModel.packageId
           if let pkgSubtypeId : Int = packageDetailModel.pkgSubtypeId[PricingConstants.pkgSubtypeId] as? Int
           {
               dataDict["pkgSubType"] = pkgSubtypeId
           }
           dataDict[PricingConstants.pkgSubClass] = "\(pricingModel.packageClassId)"
           dataDict[PricingConstants.hub] = pricingModel.pricinghubCode
           dataDict[PricingConstants.hubCity] = pricingModel.pricingDepartureCity
           dataDict[PricingConstants.departureDate] = pricingModel.pricingTourDate
           dataDict[PricingConstants.ltProdCode] = pricingModel.ltProdCode
           dataDict[PricingConstants.userMobileNo] = pricingModel.pricingMobileNumber
//           dataDict[PricingConstants.userEmailId] = "jchalke@gmail.com"//"app@mobicule.com" // logged in user
        dataDict[PricingConstants.userEmailId] = user
           dataDict[PricingConstants.enquirySource] = "Direct" // HARDCODED ***
           dataDict[PricingConstants.enquiryMedium] = "Direct" // HARDCODED ***
           dataDict[PricingConstants.enquiryCampaign] = ""
           dataDict[PricingConstants.LP] = "" // HARDCODED ***
           dataDict[PricingConstants.bookURL] = "" // HARDCODED ***
           dataDict[PricingConstants.LtItineraryCode] = pricingModel.ltItineraryCode // new logic
           dataDict[PricingConstants.farecaLtItineraryCode] = [pricingModel.ltItineraryCode]
           
           if packageDetailModel.packageTypeId != 1
           {
               dataDict[PricingConstants.regionId] = 2
           }
           else  //international
           {
               let continentName : String = packageDetailModel.continentName.uppercased()
               if continentName == "AMERICA" ||  continentName == "SOUTH AMERICA" || continentName == "NORTH AMERICA" || continentName == "AFRICA"
               {
                   dataDict[PricingConstants.regionId] = 1
               }
               else
               {
                   dataDict[PricingConstants.regionId] = 0
               }
           }
           
           dataDict[PricingConstants.crmEnquiryId] = "" // HARDCODED ***
           dataDict[PricingConstants.crmStatus] = "" // HARDCODED ***
           dataDict[PricingConstants.custStateName] = "Maharashtra" // HARDCODED ***
           dataDict[PricingConstants.isGstApplicable] = true // HARDCODED ***
           dataDict[PricingConstants.websiteTerm] = ""
//           dataDict[PricingConstants.agentId] = ""
           dataDict[PricingConstants.isOnBehalf] = "N"//Added On 21July20 //Commented On 28July20
        dataDict[PricingConstants.isCrmQuoteLead] = "N"//Changed To N On 29July20 //Commented On 28July20
        dataDict["productId"] = packageDetailModel.productId// packageSubTypeId //Not previous Added On 21July20
        dataDict["isUnionTerritory"] = "N"//isUnionTerritory
        dataDict["isHsa"] = "N"//isUnionTerritory
        dataDict["custState"] = 27//isUnionTerritory // HARDCODED ***As Per maharashtra (No slection of State so Hardcode)
        dataDict["continent"] = packageDetailModel.continentName//continent
        dataDict["device"] = "App" //Added On 08Sept20
        //Added On 16Sept20 For International Holiday
        if (packageDetailModel.packageTypeId == 1){
            dataDict[PricingConstants.isTcsApplicable] = true // HARDCODED ***
        }
           printLog("Pricing request : ", dataDict.jsonRepresentation())
           return dataDict
       }
    
    class func checkVersioning(completionBlock :  @escaping (_ isUpgrade:Bool)->Void)
    {
        LoadingIndicatorView.show("Loading")
        
        var headerDict : Dictionary<String,Any> = Dictionary()
        headerDict["user"] = "mobicule"
        headerDict["uniqueId"] = "1234567899658748"
        
        PreBookingCommunicationManager.getPrebookingData(requestType:"GET", queryParam: "", pathParam: nil, jsonParam: nil, headers: headerDict, requestURL: "tcCommonRS/extnrt/getVersion", returnInCaseOffailure: false)
        { (status,response) in
            
            DispatchQueue.main.async {
                
            LoadingIndicatorView.hide()
                
            
            printLog("status is \(status)")
            
            printLog("response is \(String(describing: response))")
            if status == true
            {
                if let responseData : Dictionary<String,Any> = response as? Dictionary<String, Any>
                {
                    let serverVersion:String = responseData["version"] as? String ?? ""
                    let appVersion:String = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String ?? ""
                    var intappVersion:Double
                    let intServerVersion = Double(serverVersion) ?? 0.0
                    if (appVersion.count >= 3){
                        intappVersion = Double(appVersion.substring(to:appVersion.index(appVersion.startIndex, offsetBy: 3))) ?? 0.0
                    }else{
                        intappVersion = Double(appVersion.substring(to:appVersion.index(appVersion.startIndex, offsetBy: appVersion.count))) ?? 0.0
                    }
                    
                        if(intServerVersion > intappVersion)
                        {
                            completionBlock(true)

                            let alert = UIAlertController(title: "Alert", message: "New version available", preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: AlertMessage.TITLE_OK, style: UIAlertAction.Style.default, handler: { (alert) in
                                
//                                let url = URL(string: "itms-apps://itunes.apple.com/kw/app/sotc-engage/id1330170083?mt=8")
                                let url = URL(string:kVersionUpgradeAppstoreURL)
                                if #available(iOS 10.0, *)
                                {
                                    UIApplication.shared.open(url!, options: [:], completionHandler: { (result) in
                                        
                                      //  LoadingIndicatorView.hide()
                                    })
                                }
                                else
                                {
                                    UIApplication.shared.openURL(url!)
                                }
                            }))
                            UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)

                        }
                    else
                        {
                            completionBlock(false)
                       }
                }

            }
                else
            {
                completionBlock(false)
                }
                
        }
        }
    }
    
    
    //mobile number validation
    class func isValidMobileNumber(MobileNumber:String) -> Bool {
        let MoNoRegex = "^[7-9]\\d{0}+[0-9]\\d{8}"
        let MoNoTest = NSPredicate(format:"SELF MATCHES %@", MoNoRegex)
        return MoNoTest.evaluate(with: MobileNumber)
    }
    
    
    class func filterArrayNestedDirectory(ArrayToBeFilter array: Array<Dictionary<String,Any>>, withOuterKey outerKey:String, withInnerKey innerKey:String, AndStringValue value:String) -> Array<Dictionary<String , Any>>  {
        
        let filteredArray : Array<Dictionary<String,Any>> = array.filter({ (dict : Dictionary<String,Any>) -> Bool in
            let innerDict : Dictionary<String,Any> = dict[outerKey] as? Dictionary<String,Any> ?? Dictionary.init()
            let valueToBeCompare : String = innerDict[innerKey] as? String ?? ""
            if valueToBeCompare == value
            {
                return true
            }
            return false
        })
        return filteredArray
    }
    
    
    class func filterArray(ArrayToBeFilter array: Array<Dictionary<String,Any>>, withKey key:String, AndIntegerValue value:Int) -> Array<Dictionary<String , Any>>  {
        
        let filteredArray : Array<Dictionary<String,Any>> = array.filter({ (dict : Dictionary<String,Any>) -> Bool in
            let packageClassId : Int = dict[key] as? Int ?? -1
            if packageClassId == value
            {
                return true
            }
            return false
        })
        return filteredArray
    }
    
    class func filterArray(ArrayToBeFilter array: Array<Dictionary<String,Any>>, withKey key:String, AndStringValue value:String) -> Array<Dictionary<String , Any>>  {
        
        let filteredArray : Array<Dictionary<String,Any>> = array.filter({ (dict : Dictionary<String,Any>) -> Bool in
            let packageClassId : String = dict[key] as? String ?? ""
            if packageClassId == value
            {
                return true
            }
            return false
        })
        return filteredArray
    }
    class func sortHubCity(hubArray : Array<Dictionary<String,Any>>) -> Array<Dictionary<String,Any>>
    {
        return hubArray.sorted(by: { (dict1 : Dictionary<String,Any>, dict2 : Dictionary<String,Any>) -> Bool in
            let city1 : String = (dict1["hubCode"] as? Dictionary<String,Any> ?? Dictionary.init())["cityName"] as? String ?? ""
            let city2 : String = (dict2["hubCode"] as? Dictionary<String,Any> ?? Dictionary.init())["cityName"] as? String ?? ""
            if city1 < city2
            {
                return true
            }
            return false
        })
    }
    
    class func getDifferenceInMonths(fromDate: Date , toDate : Date) -> Int
        {
//            let monthArr = AppUtility.dates(from: fromDate, to: toDate)
            let monthArr = AppUtility.newDates(fromDate: fromDate, toDate: toDate)
    //        let components = Calendar.current.dateComponents([.month, .day], from: fromDate, to: toDate)
    //        let monthDifference = components.month ?? 0
    //        let daysDifference = components.day ?? 0
    //        if monthDifference == 0
    //        {
    //            return 1
    //        }
    //        if (daysDifference > 15){
    //            return (monthDifference + 2)
    //        }
    //        if  (daysDifference >= 0){
    //            return (monthDifference + 1)
    //        }
    //
            return monthArr.count
        }
    
    static func dates(from fromDate: Date, to toDate: Date) -> [Date] {
        var dates: [Date] = []
        var date = fromDate
        
        while date <= toDate {
            dates.append(date)
            guard let newDate = Calendar.current.date(byAdding: .month, value: 1, to: date) else { break }
            date = newDate
        }
        return dates
    }
    
    static func newDates(fromDate: Date , toDate : Date) -> [Date] {
        
        var dates: [Date] = []
//        var date = fromDate
        
//        let comp: DateComponents = Calendar.current.dateComponents([.year, .month], from: date)
//        let startOfMonth = Calendar.current.date(from: comp)!
        
        let calendar: Calendar = Calendar.current
        let starting = calendar.startOfMonth(fromDate)
        let ending = calendar.startOfMonth(toDate)
        var date = starting
        while date <= ending {
            dates.append(date)
            guard let newDate = Calendar.current.date(byAdding: .month, value: 1, to: date) else { break }
            date = newDate
        }
        
        return dates
        
//        let calendar = Calendar(identifier: .gregorian)
//        let components = calendar.dateComponents(Set([.month]), from: fromDate, to: toDate)
//        return components.month ?? 0
        
    }
    
    class func numberOfDays(month:Int,year:Int) -> Int {
        let dateComponents = DateComponents(year: year, month: month)
        let calendar = Calendar.current
        let date = calendar.date(from: dateComponents)!
        
        let range = calendar.range(of: .day, in: .month, for: date)!
        let numDays = range.count
        printLog(numDays)
        return numDays
    }
    
    class func getDates(fromDate: Date ,toDate:Date) -> Array<Date>
    {
        var date = fromDate // first date
        
        let fmt = DateFormatter()
        fmt.dateFormat = "dd/MM/yyyy"
        var datesArray: Array<Date> = Array.init()
        let calendar = Calendar.current
        datesArray.append(date)
        while date < toDate {
            printLog(fmt.string(from: date))
            date = calendar.date(byAdding: .day, value: 1, to: date)!
            datesArray.append(date)
        }
        return datesArray
    }
    class func getDatesInFormat(inDate: Date ,formatter:String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formatter
        dateFormatter.timeZone = TimeZone.current
        let todaysDate = dateFormatter.string(from: inDate)
        return todaysDate
    }
    class func getImageNameFromAttributr(str:NSAttributedString, completion: (String) -> ()){
        str.enumerateAttribute(NSAttributedString.Key.attachment,
                               in: NSMakeRange(0, str.length),
                               options: [],
                               using: {attachment, range, _ in

            if let attachment = attachment as? NSTextAttachment,
                let fileWrapper = attachment.fileWrapper,
                let data = fileWrapper.regularFileContents
            {
                // This prints <NSTextAttachment: 0x7fe68d5304c0> "Untitled 2.jpg"
                // But there's no API to get the name?
                print(attachment.description)
                completion(attachment.description)
            }
        })
    }
    //MARK:- END  Check is Device JailBroken
    
    
    @objc class func getViewAccIPhoneSize(pixel : CGFloat , comparePixel : CGFloat) -> CGFloat {
        let phoneHeight = UIScreen.main.bounds.height
        let phoneHeightInpixel = phoneHeight * pixel
        let finalPixel : CGFloat = phoneHeightInpixel/comparePixel
        return finalPixel
    }
    class func containsSpecialCharacters(string: String) -> Bool {
        do {
            let regex = try NSRegularExpression(pattern: "[^a-z0-9 ]", options: .caseInsensitive)
            if let _ = regex.firstMatch(in: string, options: [], range: NSMakeRange(0, string.count)) {
                return true
            } else {
                return false
            }
        } catch {
            debugPrint(error.localizedDescription)
            return true
        }
    }
    class func validatePANCardNumber(_ strPANNumber : String) -> Bool{
        if (strPANNumber.count != 10){
            return false
        }
        do {
            let regex = try NSRegularExpression(pattern: "[^a-z0-9 ]", options: .caseInsensitive)
            if let _ = regex.firstMatch(in: strPANNumber, options: [], range: NSMakeRange(0, strPANNumber.count)) {
                return false
            } else {
                let regularExpression = "[A-Z]{5}[0-9]{4}[A-Z]{1}"
                let panCardValidation = NSPredicate(format : "SELF MATCHES %@", regularExpression)
                return panCardValidation.evaluate(with: strPANNumber)
            }
        } catch {
            debugPrint(error.localizedDescription)
            return false
        }
    }
}


public func matches(for regex: String, in text: String) -> [String] {
    
    do {
        
        let regex = try NSRegularExpression(pattern: regex)
        let nsString = text as NSString
        let results = regex.matches(in: text, range: NSRange(location: 0, length: nsString.length))
        
        
        return results.map { nsString.substring(with: $0.range)}
        
    } catch let error {
        
        printLog("invalid regex: \(error.localizedDescription)")
        
        return []
    }
}
public func checkRegularExpression(stringToBeCheck:String,_ items:String...) -> Bool  {
    
 
    for item in items {
       
        let matched = matches(for: item, in: stringToBeCheck)
     
        if matched.count !=  stringToBeCheck.count {
            
           return false
        }
    
    }
    
    return true
    
}
/*
extension UITextField {
    
    
    
    override open func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        
        if action == #selector(copy(_:)) || action == #selector(paste(_:)) || action == #selector(delete(_:)) {
            
            if self.tag == 7555 || self.tag == 7556
            {
                return true
            }
            
            return false
        }
        
        return true
    }
    
    override open func delete(_ sender: Any?) {
        
        self.replace(self.selectedTextRange!, withText: "")
    }
}

extension UIView
{
    func shadowAtBottom()
    {
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOpacity = 1
        self.layer.masksToBounds = false
        self.layer.shadowOffset =  CGSize.init(width: 0, height: 1.5)
        self.layer.shadowRadius = 5
    }
    
    func fadedShadow()
    {
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width: -2, height: 3)
        self.layer.shadowRadius = 3
        self.layer.shadowColor = AppUtility.hexStringToUIColor(hex: "cdcdcd").cgColor
        self.layer.borderWidth = 0.3
        self.layer.borderColor = AppUtility.hexStringToUIColor(hex: "cdcdcd").cgColor
        self.layer.masksToBounds = false
    }
    
    func roundedCornersWithBorder()  {
        self.layer.cornerRadius = 2.5
        self.layer.borderColor = AppUtility.hexStringToUIColor(hex: "cdcdcd").cgColor
        self.layer.borderWidth = 1
    }
    
    func roundedCorners()  {
        self.layer.cornerRadius = 2.5
    }
    func roundedCorners(radius :  CGFloat, borderWidth : CGFloat)  {
        self.layer.cornerRadius = radius
        self.layer.borderWidth = borderWidth
        
    }
}

extension String {
    
    subscript (i: Int) -> Character {
        return self[index(startIndex, offsetBy: i)]
    }
    
    subscript (i: Int) -> String {
        return String(self[i] as Character)
    }
    
    subscript (r: Range<Int>) -> String {
        let start = index(startIndex, offsetBy: r.lowerBound)
        let end = index(startIndex, offsetBy: r.upperBound)
        //return self[Range(start ..< end)]
        let range = Range.init(uncheckedBounds: (lower: start, upper: end))
        return String(self[range])
    }
    func numberFormatterDecimal() -> String {
       
        
        let numberFormatter = NumberFormatter()
        numberFormatter.groupingSeparator = ","
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        
        if self.contains(".")
        {
            let index = self.firstIndex(of: ".")
            
            let range = self.index(after: index!)..<self.endIndex
            let stringAfterDecimal = self[range]
            
            printLog(self[range] )
            
            if stringAfterDecimal.count > 0
            {
                if let firstCharactrer = stringAfterDecimal.first , firstCharactrer == "5"
                {
                    printLog(firstCharactrer)
                    
                    let formattedNumber = numberFormatter.string(from: NSNumber(value:Int64(Double(self)!.rounded(.down)) ))
                    
                     return (formattedNumber ?? self)
                    
                }else{
                    
                    printLog("not 5")
                }
            }else{
                
                printLog("unable to find after decimal")
            }
            
            
            
        }else{
            printLog("unable to find . ")
        }
        
        let formattedNumber = numberFormatter.string(from: NSNumber(value:Int64(Double(self)!.rounded()) ))
        
        return (formattedNumber ?? self)
        
        
       
    }
    
    func roundedNumberFormatterDouble() -> Double {
        
        
        if self.contains(".")
        {
            let index = self.firstIndex(of: ".")
            
            let range = self.index(after: index!)..<self.endIndex
            let stringAfterDecimal = self[range]
            
            printLog(self[range] )
            
            if stringAfterDecimal.count > 0
            {
                if let firstCharactrer = stringAfterDecimal.first , firstCharactrer == "5"
                {
                    printLog(firstCharactrer)
                    
                    if let formattedNumber:Double = Double(self)!.rounded(.down)
                    {
                        return formattedNumber
                    }
                   
                    
                }else{
                    
                    printLog("not 5")
                }
            }else{
                
                printLog("unable to find after decimal")
            }
            
            
            
        }else{
            printLog("unable to find . ")
        }
        
        
        if let formattedNumber:Double = Double(self)!.rounded()
        {
            return formattedNumber
        }
       
        return 0.0
       
    }
    
 
    
}

extension Date {
    var millisecondsSince1970:Int64 {
        return Int64((self.timeIntervalSince1970 * 1000.0).rounded())
    }
    
    init(milliseconds:Int64) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds / 1000))
    }
}

*/

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromNSAttributedStringKey(_ input: NSAttributedString.Key) -> String {
	return input.rawValue
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}


extension UIView {
     func drawTri(_ rect: CGRect) {
        let mask = CAShapeLayer()
        mask.frame = self.layer.bounds
        
        let width = self.layer.frame.size.width
        let height = self.layer.frame.size.height
        
        let path = CGMutablePath()
    
        path.addLine(to: CGPoint.init(x: width, y: 0))
        path.addLine(to: CGPoint.init(x: width/2, y: height))
        path.addLine(to: CGPoint.init(x: 0, y: 0))
//        CGPathAddLineToPoint(path, nil, width, 0)
//        CGPathAddLineToPoint(path, nil, width/2, height)
//        CGPathAddLineToPoint(path, nil, 0, 0)
        
        mask.path = path
        self.layer.mask = mask
    }
 }
extension UIButton
{
//    func treatAsCheckBox()  {
//        
//        self.setImage(UIImage.init(named: "checkBoxSelected"), for: UIControl.State.selected)
//        self.setImage(UIImage.init(named: "unselectedCheckbox"), for: UIControl.State.normal)
//    }
    func treatAsCheckBox()  {
        
        self.setImage(UIImage.init(named: "rightCheckSelect"), for: UIControl.State.selected)
        self.setImage(UIImage.init(named: "rightCheckNoSelect"), for: UIControl.State.normal)
    }
}
extension UITextField {
    
    override open func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        if action == #selector(copy(_:)) || action == #selector(paste(_:)) {
            return false
        }
        
        return true
    }
    
}
extension UITextView{
    func adjustUITextViewHeight()
    {
        self.frame.size.height = self.contentSize.height
        self.isScrollEnabled = false
    }
}
extension String {
    
    subscript (i: Int) -> Character {
        return self[index(startIndex, offsetBy: i)]
    }
    
    subscript (i: Int) -> String {
        return String(self[i] as Character)
    }
    
    subscript (r: Range<Int>) -> String {
        let start = index(startIndex, offsetBy: r.lowerBound)
        let end = index(startIndex, offsetBy: r.upperBound)
        return String(self[start..<end])
    }
    
    func trimWhiteSpace() -> String {
        let string = self.trimmingCharacters(in: .whitespacesAndNewlines)
        return string
    }
    

    func numberFormatterDecimal() -> String {
        
        
        let numberFormatter = NumberFormatter()
        numberFormatter.groupingSeparator = ","
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        numberFormatter.locale = Locale(identifier: "en_IN")
        let formattedNumber = numberFormatter.string(from: NSNumber(value:Int64(Double(self)!.rounded()) ))
        
        return (formattedNumber ?? self)
    }
    
    func roundedNumberFormatterDouble() -> Double {
        
        if let formattedNumber:Double = Double(self)!.rounded()
        {
            return formattedNumber
        }
        
        return 0.0
    }
    
    mutating func setFontToHtmlString(fontName font:String, FontSize size:String ,fontColor colorCode:String)  {
        self = "<html><body><font face=\"" + font +  "\" color=\"" + colorCode +  "\" size=\"" + size + "\">" + self + "</body></html>"
        
    }
    
    //#Mark -- SHA 256 encryption
    
    /*
    func sha256() -> String{
        if let stringData = self.data(using: String.Encoding.utf8) {
            return hexStringFromData(input: digest(input: stringData as NSData))
        }
        return ""
    }
    
    private func digest(input : NSData) -> NSData {
        let digestLength = Int(CC_SHA256_DIGEST_LENGTH)
        var hash = [UInt8](repeating: 0, count: digestLength)
        CC_SHA256(input.bytes, UInt32(input.length), &hash)
        return NSData(bytes: hash, length: digestLength)
    }
    */
    private  func hexStringFromData(input: NSData) -> String {
        var bytes = [UInt8](repeating: 0, count: input.length)
        input.getBytes(&bytes, length: input.length)
        
        var hexString = ""
        for byte in bytes {
            hexString += String(format:"%02x", UInt8(byte))
        }
        
        return hexString
    }
    var isAlphanumeric: Bool {
        return !isEmpty && range(of: "[^a-zA-Z0-9]", options: .regularExpression) == nil
    }
}

//@available(iOS 10.0, *)
//@available(iOS 10.0, *)

extension Date {
    
    var millisecondsSince1970:Int64 {
        return Int64((self.timeIntervalSince1970 * 1000.0).rounded())
    }
    
    init(milliseconds:Int64) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds / 1000))
    }
    
    func getMonth() -> Int {
        let date = self
        let calendar = Calendar.current
        let month = calendar.component(.month, from: date)
        return month
    }
    func getDay() -> Int {
        let date = self
        let calendar = Calendar.current
        let day = calendar.component(.day, from: date)
        return day
    }
    func getYear() -> Int {
        let date = self
        let calendar = Calendar.current
        let year = calendar.component(.year, from: date)
        return year
    }
    
    func addMonths(months : Int) -> Date? {
        let calendar = Calendar.current
        var dateComponent = DateComponents.init()
        dateComponent.month = months
        
        let date = calendar.date(byAdding: dateComponent, to: self)
        return date
    }
    func addDays(days : Int) -> Date? {
        let calendar = Calendar.current
        var dateComponent = DateComponents.init()
        dateComponent.day = days
        
        let date = calendar.date(byAdding: dateComponent, to: self)
        return date
    }
    func getWeekday() -> Int {
        let weekday : Int = Calendar.current.component(.weekday, from: self)
        return weekday
    }
    
    func getNextMonthYear() -> [[String:String]] {
        
        let currentYear = self.getYear()
        let currentMonth = self.getMonth()
        let nextYear = currentYear + 1
        var monthToCalculate = self.getMonth()
        
        var nextMonthYear:[[String:String]] = []
        
        for _ in 0..<12
        {
            var nextMonth:Int?
            var year:Int?
            
            if monthToCalculate == 12
            {
                nextMonth = 12;
            }
            else
            {
                nextMonth = (monthToCalculate % 12);
            }
            
            if nextMonth! < currentMonth
            {
                year = nextYear;
                
            } else {
                
                year = currentYear;
            }
            
            
            if nextMonth! >= 0
            {
                let monthName = DateFormatter().monthSymbols[nextMonth! - 1]
                 //printLog("\(monthName) \(year)")
                
                let monthYearDict:[String:String] = ["displayValue":"\(monthName) \(year!)","monthNumber":"\(nextMonth!)"]
                
                nextMonthYear.append(monthYearDict)
                
            }
            
           monthToCalculate = monthToCalculate + 1
            
            
        }
        
        return nextMonthYear
        
    }
    func isGreaterThan_Date(dateToCompare: Date) -> Bool {
        //Declare Variables
        var isGreater = false
        
        //Compare Values
        if self.compare(dateToCompare as Date) == ComparisonResult.orderedDescending {
            isGreater = true
        }
        
        //Return Result
        return isGreater
    }
    
    func isLessThan_Date(dateToCompare: Date) -> Bool {
        //Declare Variables
        var isLess = false
        
        //Compare Values
        if self.compare(dateToCompare as Date) == ComparisonResult.orderedAscending {
            isLess = true
        }
        
        //Return Result
        return isLess
    }
    
    func equalTo_Date(dateToCompare: Date) -> Bool {
        //Declare Variables
        var isEqualTo = false
        
        //Compare Values
        if self.compare(dateToCompare as Date) == ComparisonResult.orderedSame {
            isEqualTo = true
        }
        
        //Return Result
        return isEqualTo
    }
//    func startOfMonth() -> Date {
//        if #available(iOS 10.0, *) {
//            let interval = Calendar.current.dateInterval(of: .month, for: self)
//            return (interval?.start.toLocalTime())!
//        } else {
//            // Fallback on earlier versions
//        }
//         return Date() // Without toLocalTime it give last months last date
//    }
//
//    func endOfMonth() -> Date {
//        if #available(iOS 10.0, *) {
//            let interval = Calendar.current.dateInterval(of: .month, for: self)
//            return interval!.end
//        } else {
//            // Fallback on earlier versions
//        }
//         return Date()
//    }
    
    
}
extension Calendar {
    func startOfMonth(_ date: Date) -> Date {
        return self.date(from: self.dateComponents([.year, .month], from: date))!
        
    }
    func endOfMonth(_ date: Date) -> Date {
        return self.date(byAdding: DateComponents(month: 1, day: -1), to: self.startOfMonth(date))!
        
    }
}
extension UIView
{
    func shadowAtBottom()
    {
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOpacity = 1
        self.layer.masksToBounds = false
        self.layer.shadowOffset =  CGSize.init(width: 0, height: 1.5)
        self.layer.shadowRadius = 5
    }
    
    func fadedShadow()
    {
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width: -2, height: 3)
        self.layer.shadowRadius = 3
        self.layer.shadowColor = AppUtility.hexStringToUIColor(hex: "cdcdcd").cgColor
        self.layer.borderWidth = 0.3
        self.layer.borderColor = AppUtility.hexStringToUIColor(hex: "cdcdcd").cgColor
        self.layer.masksToBounds = false
    }
    
    func roundedCornersWithBorder()  {
        self.layer.cornerRadius = 2.5
        self.layer.borderColor = AppUtility.hexStringToUIColor(hex: "cdcdcd").cgColor
        self.layer.borderWidth = 1
    }
    
    func roundedCorners()  {
        self.layer.cornerRadius = 2.5
    }
    func roundedCorners(radius :  CGFloat, borderWidth : CGFloat)  {
        self.layer.cornerRadius = radius
        self.layer.borderWidth = borderWidth
        
    }
}

extension String {
    
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.width)
    }
}

extension UIApplication {
    class func topViewController(base: UIViewController? = (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        return base
    }
}
extension CALayer {
    
    func addBorder(edge: UIRectEdge, color: UIColor, thickness: CGFloat) {
        
        let border = CALayer()
        
        switch edge {
        case .top:
            border.frame = CGRect(x: 0, y: 0, width: frame.width, height: thickness)
        case .bottom:
            border.frame = CGRect(x: 0, y: frame.height - thickness, width: frame.width, height: thickness)
        case .left:
            border.frame = CGRect(x: 0, y: 0, width: thickness, height: frame.height)
        case .right:
            border.frame = CGRect(x: frame.width - thickness, y: 0, width: thickness, height: frame.height)
        default:
            break
        }
        
        border.backgroundColor = color.cgColor;
        
        addSublayer(border)
    }
    
}

extension String{
    /*
    func convertHtml() -> NSAttributedString{
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do{
//            return try NSAttributedString(data: data, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: String.Encoding.utf8.rawValue], documentAttributes: nil)
            return try NSAttributedString(data: data, options: [NSAttributedString.DocumentAttributeKey: NSAttributedString.DocumentType.html, NSCharacterEncodingDocumentAttribute: String.Encoding.utf8.rawValue], documentAttributes: nil)
        }catch{
            return NSAttributedString()
        }
    }
    */
    func convertHtml() -> String? {
        guard let data = data(using: .utf8) else { return nil }
        do {
            return try NSAttributedString(
                data: data,
                options: [
                    .documentType: NSAttributedString.DocumentType.html
                ],
                documentAttributes: nil
            ).string
        } catch let error as NSError {
            print(error.localizedDescription)
            return  nil
        }
    }
}
 
extension NSDate {
    
    func isGreaterThanDate(dateToCompare: NSDate) -> Bool {
        //Declare Variables
        var isGreater = false
        
        //Compare Values
        if self.compare(dateToCompare as Date) == ComparisonResult.orderedDescending {
            isGreater = true
        }
        
        //Return Result
        return isGreater
    }
    
    func isLessThanDate(dateToCompare: NSDate) -> Bool {
        //Declare Variables
        var isLess = false
        
        //Compare Values
        if self.compare(dateToCompare as Date) == ComparisonResult.orderedAscending {
            isLess = true
        }
        
        //Return Result
        return isLess
    }
    
    func equalToDate(dateToCompare: NSDate) -> Bool {
        //Declare Variables
        var isEqualTo = false
        
        //Compare Values
        if self.compare(dateToCompare as Date) == ComparisonResult.orderedSame {
            isEqualTo = true
        }
        
        //Return Result
        return isEqualTo
    }
    
    func addDays(daysToAdd: Int) -> NSDate {
        let secondsInDays: TimeInterval = Double(daysToAdd) * 60 * 60 * 24
        let dateWithDaysAdded: NSDate = self.addingTimeInterval(secondsInDays)
        
        //Return Result
        return dateWithDaysAdded
    }
    
    func addHours(hoursToAdd: Int) -> NSDate {
        let secondsInHours: TimeInterval = Double(hoursToAdd) * 60 * 60
        let dateWithHoursAdded: NSDate = self.addingTimeInterval(secondsInHours)
        
        //Return Result
        return dateWithHoursAdded
    }
}
 
 public extension UIDevice {
    
    static let modelName: String = {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        func mapToDevice(identifier: String) -> String { // swiftlint:disable:this cyclomatic_complexity
            #if os(iOS)
                switch identifier {
                case "iPod5,1":                                 return "iPod Touch 5"
                case "iPod7,1":                                 return "iPod Touch 6"
                case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
                case "iPhone4,1":                               return "iPhone 4s"
                case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
                case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
                case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
                case "iPhone7,2":                               return "iPhone 6"
                case "iPhone7,1":                               return "iPhone 6 Plus"
                case "iPhone8,1":                               return "iPhone 6s"
                case "iPhone8,2":                               return "iPhone 6s Plus"
                case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
                case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
                case "iPhone8,4":                               return "iPhone SE"
                case "iPhone10,1", "iPhone10,4":                return "iPhone 8"
                case "iPhone10,2", "iPhone10,5":                return "iPhone 8 Plus"
                case "iPhone10,3", "iPhone10,6":                return "iPhone X"
                case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
                case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
                case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
                case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
                case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
                case "iPad6,11", "iPad6,12":                    return "iPad 5"
                case "iPad7,5", "iPad7,6":                      return "iPad 6"
                case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
                case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
                case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
                case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
                case "iPad6,3", "iPad6,4":                      return "iPad Pro 9.7 Inch"
                case "iPad6,7", "iPad6,8":                      return "iPad Pro 12.9 Inch"
                case "iPad7,1", "iPad7,2":                      return "iPad Pro 12.9 Inch 2. Generation"
                case "iPad7,3", "iPad7,4":                      return "iPad Pro 10.5 Inch"
                case "AppleTV5,3":                              return "Apple TV"
                case "AppleTV6,2":                              return "Apple TV 4K"
                case "AudioAccessory1,1":                       return "HomePod"
                case "i386", "x86_64":                          return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "iOS"))"
                default:                                        return identifier
                }
            #elseif os(tvOS)
                switch identifier {
                case "AppleTV5,3": return "Apple TV 4"
                case "AppleTV6,2": return "Apple TV 4K"
                case "i386", "x86_64": return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "tvOS"))"
                default: return identifier
                }
            #endif
        }
        
        return mapToDevice(identifier: identifier)
    }()
    
 }


extension UITextField{
    @IBInspectable var doneAccessory: Bool{
        get{
            return self.doneAccessory
        }
        set (hasDone) {
            if hasDone{
                addDoneButtonOnKeyboard()
            }
        }
    }
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))
        
        //let clear: UIBarButtonItem = UIBarButtonItem(title: "Clear", style: .done, target: self, action: #selector(self.clearButtonAction))
        
        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.inputAccessoryView = doneToolbar
    }
    
    func doneButtonAction()
    {
        self.resignFirstResponder()
    }
    func clearButtonAction()
    {
        
    }
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
    
    func setLeftImgInTextField(leftImage:UIImage?) {
        if let image = leftImage {
            rightViewMode = .always
            var width = 40
            let view = UIView(frame: CGRect(x: 0, y: 0, width: width, height: 20))
            let imageView = UIImageView(frame: CGRect(x: view.frame.origin.x, y: view.frame.origin.y, width: 20, height: 20))
            imageView.image = image
            if borderStyle == UITextField.BorderStyle.none || borderStyle == UITextField.BorderStyle.line {
                width += 5
            }
            
            view.addSubview(imageView)
            rightView = view
        } else {
            rightViewMode = .never
        }
    }
}
