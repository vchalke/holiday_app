//
//  HolidayLandingSearchVC.h
//  holidays
//
//  Created by Kush_Tech on 19/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewMasterVC.h"
#import "HolidayLandingScreenVC.h"
#import <FirebaseAnalytics/FirebaseAnalytics.h>
NS_ASSUME_NONNULL_BEGIN

@interface HolidayLandingSearchVC : HolidayLandingScreenVC<UITextFieldDelegate>
@property (strong,nonatomic) NSString *header_Name;
@property(nonatomic,assign) BOOL boolForSearch_PackgScreen;

@property (weak, nonatomic) IBOutlet UITextField *serach_Text;
@property (weak, nonatomic) IBOutlet UITableView *table_vieww;

@end

NS_ASSUME_NONNULL_END
