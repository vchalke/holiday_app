//
//  PackageListVC.h
//  holidays
//
//  Created by Pushpendra Singh on 12/08/15.
//  Copyright (c) 2015 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Holiday.h"
#import <FirebaseAnalytics/FirebaseAnalytics.h>


@interface PackageListVC : UIViewController<UITableViewDataSource,UITableViewDelegate,SlideNavigationControllerDelegate>
@property (strong, nonatomic) IBOutlet UIView *PackageListView;
@property (weak, nonatomic) IBOutlet UITableView *packageList;
@property (strong, nonatomic) IBOutlet UIView *viewPopup;

@property (strong,nonatomic)NSArray *arrayOfHolidays;

- (IBAction)btnOnSortingPopUpClicked:(id)sender;
-(void)fetchPackageDetails:(Holiday *)objHoliday;
@property (strong,nonatomic) NSArray * completePackageDetail;

@end
