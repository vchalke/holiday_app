//
//  TravellerInformationModel.h
//  holidays
//
//  Created by ketan on 23/10/15.
//  Copyright © 2015 Mobicule. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PassangerDetailCell.h"


@interface TravellerInformationModel : NSObject
@property (nonatomic,strong) NSString *travellerType;
@property int travellerRoomNumber;
@property (nonatomic ,strong) NSString *travellerRoomingType;
@property (nonatomic) float indivisualPrice;
@property (nonatomic,strong) PassangerDetailCell *cell;
@property (nonatomic) int travellerAge;

- (instancetype)initWithRoomingType:(NSString *)roomingType withRoomNo:(int)roomNo withType:(NSString *) type;
@end
