//
//  TermsAndCondViewController.m
//  holidays
//
//  Created by ketan on 15/12/15.
//  Copyright © 2015 Mobicule. All rights reserved.
//

#import "TermsAndCondViewController.h"

@interface TermsAndCondViewController ()

@end

@implementation TermsAndCondViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [super setHeaderTitle:@"Terms And Conditions"];
    [[NSBundle mainBundle]loadNibNamed:@"TermsAndCondViewController" owner:self options:nil];
    [super addViewInBaseView:self.termsConditionView];
    [self.termsWebView loadHTMLString:self.requestUrlString baseURL:nil];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
