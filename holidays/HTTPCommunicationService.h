//
//  SyncCommunicationService.h
//  Mobicule-Sync-Core
//
//  Created by Kishan on 29/06/15.
//  Copyright (c) 2015 Mobicule. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MobiculeLogger.h"
#import "NetworkManager.h"

@interface HTTPCommunicationService : NSObject
{
    BOOL _isLogEnabled;
}

/**
 *  This is use for sent GET/POST request.
 *
 *  @param syncRequest when use POST request pass syncRequest data that set in request body and  when use GET request pass empty syncRequest data string because no such data for set in request body.
 *   
 *  @param URL set URL configuration in network Component.
 *
 *  @param authorization set authorization configuration in network Component.
 *
 *  @param isLogEnable set log configuration for network Component.
 *
 *  @return Response is string type associated with all configuration.
 */
- (NSString *)sendRequest:(NSString*)syncRequest withUrl:(NSString*)url withAuthorization:(NSString*)authorization withShowLogs:(BOOL)isLogEnable ;
- (NSString *)sendRequest1:(NSString*)syncRequest withUrl:(NSString*)url withAuthorization:(NSString*)authorization withShowLogs:(BOOL)isLogEnable withCompletionHandler:(void (^)(NSString* resultString))handler;
@end
