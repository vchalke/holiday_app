//
//  WishListScreenVC.m
//  holidays
//
//  Created by Kush_Tech on 17/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "WishListScreenVC.h"
#import <FirebaseAnalytics/FirebaseAnalytics.h>
#import "CoreUtility.h"
#import "NetCoreAnalyticsVC.h"
#import "Thomas_Cook_Holidays-Swift.h"

@interface WishListScreenVC ()<UICollectionViewDelegate,UICollectionViewDataSource,SRPCollectionCellDelegate>
{
    NSMutableDictionary *payloadList;
    NSMutableDictionary *payLoadForViewDetailsDict;
    LoadingView *activityLoadingView;
    NSMutableArray *dataDictArray;
    NSInteger selectedIndexPath;
    NSInteger selectedSortOption;
}
@end

@implementation WishListScreenVC

- (void)viewDidLoad {
//    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    selectedIndexPath = 1000;
    selectedSortOption = 1000;
    payloadList =  [NSMutableDictionary dictionary];
    payLoadForViewDetailsDict = [[NSMutableDictionary alloc]init];
    [self.collectionViews registerNib:[UINib nibWithNibName:@"SRPCollectionCell" bundle:nil] forCellWithReuseIdentifier:@"SRPCollectionCell"];
    
}
- (void)viewWillAppear:(BOOL)animated{
    [self setAllSavedDataInArray];
}
-(void)setAllSavedDataInArray{
    NSArray *array = [[CoreDataSingleton sharedInstance] getArrayOfObjectForEntityName:WishListHoliday];
    dataDictArray = [[NSMutableArray alloc]init];
    
        for (NSManagedObject *managedObject in array) {
          
            NSDictionary *jsonDict = [self convertStringToJsonDict:[managedObject valueForKey:@"packageData"]];
              NSLog(@"%@",jsonDict);
            [dataDictArray addObject:jsonDict];
            NSLog(@"%@ %@ %@ ",[managedObject valueForKey:@"packageID"], [managedObject valueForKey:@"packageName"], [managedObject valueForKey:@"packagePrize"]);
        }
    
    if ([dataDictArray count]==0){
    [self showAlertViewWithTitle:@"Empty !" withMessage:@"Packages Are Not Added In Wishlist"];
    }
    [self.collectionViews reloadData];
}

#pragma mark - Collection View Delegates And DataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [dataDictArray count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    SRPCollectionCell *cell= (SRPCollectionCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"SRPCollectionCell" forIndexPath:indexPath];
    cell.hert_View.layer.cornerRadius = cell.hert_View.frame.size.height*0.5;
    Holiday *objHoliday = [[Holiday alloc] initWithDataDict:[dataDictArray objectAtIndex:indexPath.row]];
    cell.srpDelgate = self;
    cell.selectedIndexNumber = selectedIndexPath;
    cell.indexNumber = indexPath.row;
//    cell.btn_like.userInteractionEnabled = NO;
    [cell showHolidayPackage:objHoliday withCurrentIndex:indexPath.row withTag:selectedIndexPath];
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if  (selectedIndexPath == indexPath.row){
        return CGSizeMake(self.view.frame.size.width, 500);
    }
    return CGSizeMake(self.view.frame.size.width, 300);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
     Holiday *objHoliday = [[Holiday alloc] initWithDataDict:[dataDictArray objectAtIndex:indexPath.row]];
    [self fetchPackageDetailsInWishList:objHoliday];
}
#pragma mark - SRPCollection Delegate

-(void)setSelectedIndex:(UICollectionViewCell *)cell{
    NSIndexPath *indexPath = [self.collectionViews indexPathForCell:cell];
    NSLog(@"%ld",(long)indexPath.row);
    NSLog(@"%ld",(long)index);
    selectedIndexPath =indexPath.row;
    [self.collectionViews reloadData];
}
-(void)likeDislikePressInCell:(UICollectionViewCell *)cell withFlag:(BOOL)flag{
    [self showToastsWithTitle:(flag) ? @" Added In Wishlist " : @" Removed From Wishlist "];
    [self setAllSavedDataInArray];
}
#pragma mark - Other API Caslling Methods

-(void)fetchPackageDetailsInWishList:(Holiday *)objHoliday{
    LoadingView *activityLoadingView = [LoadingView loadingViewInView:self.view.superview.superview
           withString:@""
    andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
    [self fetchPackageDetailsWithHoliday:objHoliday completionHandler:^(NSDictionary * _Nullable resPonseDict, NSMutableDictionary * _Nullable payLoadDict, NSError * _Nullable error) {
        dispatch_async(dispatch_get_main_queue(), ^(void){
            [activityLoadingView removeFromSuperview];
            if (error != nil){
                [self showAlertViewWithTitle:@"Alert" withMessage:[error localizedDescription]];
            }else if ([resPonseDict count] ==0){
                [self showAlertViewWithTitle:@"Alert" withMessage:ErrorType_toString[emptyData]];
            }else if ([resPonseDict count] > 0){
                NSArray *packageArray = (NSArray *)resPonseDict;
                NSDictionary *dictForCompletePackage = [packageArray objectAtIndex:0];
                HolidayPackageDetail *packageDetailHoliday = [[HolidayPackageDetail alloc] initWithDataDict:dictForCompletePackage];
                PDPScreenVC *controller = [[PDPScreenVC alloc] initWithNibName:@"PDPScreenVC" bundle:nil];
                controller.headerName = objHoliday.strPackageName;
                controller.holidayPackageDetailInPdp = packageDetailHoliday;
                controller.holidayObjInPdp = objHoliday;
                controller.completePackageDetail = [NSArray arrayWithObjects:dictForCompletePackage, nil];
                controller.payloadDict = payLoadDict;
                [self.navigationController pushViewController:controller animated:YES];
            }else{
                [self showAlertViewWithTitle:@"Alert" withMessage:ErrorType_toString[emptyData]];
            }
        });
    }];
    if ([objHoliday.strPackageType caseInsensitiveCompare:@"International"]==NSOrderedSame){
        [FIRAnalytics logEventWithName:@"International_Holidays_View_Details" parameters:@{kFIRParameterItemName:[NSString stringWithFormat:@"PackageName :%@, PackageType :%@",objHoliday.strPackageName,objHoliday.strPackageType]}];
    }else{
       [FIRAnalytics logEventWithName:@"Indian_Holidays_View_Details" parameters:@{kFIRParameterItemName:[NSString stringWithFormat:@"PackageName :%@, PackageType :%@",objHoliday.strPackageName,objHoliday.strPackageType]}];
    }
    [payloadList setObject:@"yes" forKey:@"s^VIEW_DETAILS"];
}

/*
-(void)fetchPackageDetails:(Holiday *)objHoliday
{
   
    @try
    {
        
    NetworkHelper *helper = [NetworkHelper sharedHelper];
    
    NSString *pathParam = objHoliday.strPackageId;
    
    NSDictionary *headerDict = [CoreUtility getHeaderDict];
    
    [helper getResponseWithRequestType:@"" withQueryParam:@"" withPathParam:pathParam withJsonParam:nil withHeaders:headerDict withUrl:kAstraUrlPackageDetail success:^(NSDictionary *responseDict)
     {
         NSLog(@"Response Dict : %@",responseDict);
         
         
         dispatch_async(dispatch_get_main_queue(), ^(void) {
        [activityLoadingView removeFromSuperview];
         if (responseDict)
         {
             if (responseDict.count>0)
             {
                 NSArray *packageArray = (NSArray *)responseDict;
                 NSDictionary *dictForCompletePackage = [packageArray objectAtIndex:0];
                 
                 HolidayPackageDetail *packageDetailHoliday = [[HolidayPackageDetail alloc] initWithDataDict:dictForCompletePackage];
                 [self fireAppIntHoViewDetailsEvent:packageDetailHoliday];
                 
                 PDPScreenVC *controller = [[PDPScreenVC alloc] initWithNibName:@"PDPScreenVC" bundle:nil];
                 controller.headerName = objHoliday.strPackageName;
                 controller.holidayPackageDetailInPdp = packageDetailHoliday;
                 controller.holidayObjInPdp = objHoliday;
                 controller.completePackageDetail = [NSArray arrayWithObjects:dictForCompletePackage, nil];
                 controller.payloadDict = payloadList;
                 [self.navigationController pushViewController:controller animated:YES];
                 
             }
         }
          });
         
     }
                               failure:^(NSError *error)
     {
         dispatch_async(dispatch_get_main_queue(), ^(void)
                        {
                            [activityLoadingView removeFromSuperview];
                            NSLog(@"Response Dict : %@",[error description]);
                        });
     }];
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"%@", exception.reason);
        [activityLoadingView removeFromSuperview];
    }
    @finally
    {
        NSLog(@"exception finally called");
    }
}
-(void)fireAppIntHoViewDetailsEvent:(HolidayPackageDetail *) PackageDetail
{
    
    if((PackageDetail.strPackageName == nil) || (PackageDetail.strPackageName == NULL)){
        [payLoadForViewDetailsDict setObject:@"" forKey:@"s^DESTINATION"];
    }else{
        
        [payLoadForViewDetailsDict setObject:PackageDetail.strPackageName forKey:@"s^DESTINATION"];
    }
    [payLoadForViewDetailsDict setObject:PackageDetail.strPackageName forKey:@"s^DESTINATION"];
    [payLoadForViewDetailsDict setObject:[NSString stringWithFormat:@"%d",PackageDetail.durationNoDays] forKey:@"s^DURATION"];
    [payLoadForViewDetailsDict setObject:[NSString stringWithFormat:@"%d",PackageDetail.packagePrise] forKey:@"s^BUDGET"];
    [payLoadForViewDetailsDict setObject:PackageDetail.strPackageId forKey:@"s^PACKAGE_ID"];
    [payLoadForViewDetailsDict setObject:@"App" forKey:@"s^SOURCE"];
  
  //[payLoadForViewDetails setObject:[NSString stringWithFormat:@"%d",packageDetail.packagePrise] forKey:@"i^PACKAGE_PRICE"];//28-02-2018
    
    [payLoadForViewDetailsDict setObject:[NSNumber numberWithInt:PackageDetail.packagePrise] forKey:@"i^PACKAGE_PRICE"];//28-02-2018
    
    if ([[PackageDetail.strPackageSubType lowercaseString]isEqualToString:@"git"])
    {
        [payLoadForViewDetailsDict setObject:@"no" forKey:@"s^PERSONALIZED_TOUR"];
    }
    else
    {
      [payLoadForViewDetailsDict setObject:@"yes" forKey:@"s^PERSONALIZED_TOUR"];
    }
    
    if(PackageDetail.strPackageType != nil && ![PackageDetail.strPackageType isEqualToString:@""])
    {
        [payLoadForViewDetailsDict setObject:PackageDetail.strPackageType forKey:@"s^TYPE"];
    }
    
    [NetCoreAnalyticsVC getNetCorePushNotificationDetailDictionary:payLoadForViewDetailsDict withPayloadCount:125];
    NSLog(@"Data submitted to the netcore for 105");
    //[NetCoreAnalyticsVC getNetCorePushNotificationDetailDictionary:payLoadForViewDetails withPayloadCount:105];
}
 */
#pragma mark - Button Actions
- (IBAction)btn_backFromView:(id)sender {
    [self jumpToPreviousViewController];
}

@end
