//
//  PrizePerPersonTableViewCell.h
//  holidays
//
//  Created by Kush_Team on 24/06/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TTRangeSlider.h"
#import "Holiday.h"
#import "NewMasterCellTableViewCell.h"
NS_ASSUME_NONNULL_BEGIN

@interface PrizePerPersonTableViewCell : NewMasterCellTableViewCell<TTRangeSliderDelegate>
{
    NSString *minPrize;
    NSString *maxPrize;
}
@property (weak, nonatomic) IBOutlet TTRangeSlider *range_Sliders;
@property (weak, nonatomic) IBOutlet UILabel *lbl_range;
@property(strong,nonatomic)NSMutableArray *holidayArrayFilter;
-(void)setupTableViewCell;

@property (strong,nonatomic) NSMutableDictionary *filterPrizeDict;
@end

NS_ASSUME_NONNULL_END
