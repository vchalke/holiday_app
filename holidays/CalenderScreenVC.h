//
//  CalenderScreenVC.h
//  holidays
//
//  Created by Kush_Tech on 23/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewMasterVC.h"
#import "HolidayPackageDetail.h"
NS_ASSUME_NONNULL_BEGIN

@protocol CalenderScreenVCDelegate <NSObject>
@optional
- (void)setSelectedDate:(NSDate*)date withVCIndex:(int)index withDataDict:(NSDictionary*)dataDict;
@end

@interface CalenderScreenVC : NewMasterVC
@property (nonatomic, weak) id <CalenderScreenVCDelegate> calenderDelgate;
@property (weak, nonatomic) IBOutlet UICollectionView *date_collectionView;
@property (strong ,nonatomic) HolidayPackageDetail *holidayPackageDetailInCalender;
@property (strong,nonatomic) NSArray *roomRecordArray;
@property BOOL isMRP;
@property (strong,nonatomic) NSDictionary *dataDict;
@property (strong ,nonatomic) NSArray *arrayTravellerCalculation;
@property (strong,nonatomic)  NSString *packageId;
@property (strong,nonatomic) NSString *hubName;
@property (nonatomic) int accomType;
@property (strong,nonatomic) NSDictionary *selectedHubDict;
@property (strong,nonatomic) NSDictionary *selectedStateDict;
@property (nonatomic) int previosVCIndex;
@end

NS_ASSUME_NONNULL_END
