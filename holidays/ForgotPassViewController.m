//
//  ForgotPassViewController.m
//  holidays
//
//  Created by Swapnil on 13/01/18.
//  Copyright © 2018 Mobicule. All rights reserved.
//

#import "ForgotPassViewController.h"
#import "UIViewController+MJPopupViewController.h"

@interface ForgotPassViewController ()
{
    LoadingView *activityLoadingView;
}

@end

@implementation ForgotPassViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

-(void)showAlertViewWithTitle:(NSString *)title withMessage:(NSString *)message
{
    UIAlertController *alertView = [UIAlertController alertControllerWithTitle:title
                                                                       message:message
                                                                preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [alertView dismissViewControllerAnimated:YES completion:nil];
        
    }];
    
    [alertView addAction:okAction];
    
    [self presentViewController:alertView animated:YES completion:nil];
}

-(BOOL) checkEmailIdValidation
{
    if ([self.textFieldEmail.text isEqualToString:@""])
    {
        [self showAlertViewWithTitle:@"Alert" withMessage:@"please enter a emailId"];
        return NO;
    }
    else if(![self validateEmailWithString])
    {
        [self showAlertViewWithTitle:@"Alert" withMessage:@"please enter a valid emailId"];
        return NO;
    }
    return YES;
}

- (BOOL)validateEmailWithString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:self.textFieldEmail.text];
    
}

#pragma mark - Button Actions
- (IBAction)submitButtonClicked:(UIButton *)sender
{
    if ([self checkEmailIdValidation])
    {
        [self loginCheckWithType:@"ForgotPassword"];
    }
    else
    {
        [self showAlertViewWithTitle:@"Alert" withMessage:@"Enter username"];
    }
}

- (IBAction)closeButtonClicked:(UIButton *)sender
{
    [self dismissViewControllerAnimated:NO completion:nil];
}

#pragma mark - text field delegate -
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(void)loginCheckWithType:(NSString *)signInType
{
    //abc@test.com/
    activityLoadingView = [LoadingView loadingViewInView:self.view.superview.superview
                                              withString:@""
                                       andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
    
    @try
    {
        
        NetworkHelper *helper = [NetworkHelper sharedHelper];
        
        NSString *pathParam = self.textFieldEmail.text;
        
        NSDictionary *headerDict = [CoreUtility getHeaderDict];
        
        [helper getResponseWithRequestType:@"" withQueryParam:@"" withPathParam:pathParam withJsonParam:nil withHeaders:headerDict withUrl:kAstraUrlLoginCheck success:^(NSDictionary *responseDict)
         {
             NSLog(@"Response Dict : %@",responseDict);
             
             
             dispatch_async(dispatch_get_main_queue(), ^(void)
                            {
                                [activityLoadingView removeFromSuperview];
                                if (responseDict)
                                {
                                    if (responseDict.count>0)
                                    {
                                        if([signInType isEqualToString:@"ForgotPassword"])
                                        {
                                            NSString *messege = [responseDict valueForKey:@"message"];
                                            
                                            if ([[messege lowercaseString] isEqualToString:@"false"])
                                            {
                                                
                                                UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Alert" message:@"Email Id is not registered with us."                                                                                             preferredStyle: UIAlertControllerStyleAlert];
                                                
                                                
                                                
                                                UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                                                           {
                                                                               
                                                                               [self setUserDetailsAndClosePopUp];
                                                                           }];
                                                
                                                [alertView addAction:actionOk];
                                                
                                                [self presentViewController:alertView animated:YES completion:nil];
                                                
                                                
                                            }
                                            else
                                            {
                                                [self resetPasswordAstraAPI];
                                            }
                                            
                                            
                                        }
                                    }
                                    
                                }
                            });
             
         }
                                   failure:^(NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^(void)
                            {
                                [activityLoadingView removeFromSuperview];
                                NSLog(@"Response Dict : %@",[error description]);
                            });
             
             
             
         }];
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"%@", exception.reason);
        [activityLoadingView removeFromSuperview];
    }
    @finally
    {
        NSLog(@"exception finally called");
    }
}

-(void)setUserDetailsAndClosePopUp
{
    [[NSUserDefaults standardUserDefaults]setValue:self.textFieldEmail.text forKey:kuserDefaultUserId]
    ;
//    [[NSUserDefaults standardUserDefaults]setValue:self.textFieldPassword.text forKey:kuserDefaultPassword]
//    ;
    
    NSUserDefaults *LoginStatus=[NSUserDefaults standardUserDefaults];
    [LoginStatus setObject:kLoginSuccess  forKey:kLoginStatus];
    
    NSUserDefaults *userIdForGooglePlusSignIn=[NSUserDefaults standardUserDefaults];
    
    [userIdForGooglePlusSignIn setObject:self.textFieldEmail.text forKey:kLoginEmailId];
    
    //  [[NetCoreSharedManager sharedInstance] setUpIdentity:self.textFieldUserId.text];
    
    //[self fetchMobileNumber];
    
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    
}

-(void)resetPasswordAstraAPI
{
    //2. https://services-uatastra.thomascook.in/TcilMyAccount/login/forgetPassword/test@test.com
    
    
    activityLoadingView = [LoadingView loadingViewInView:self.view.superview.superview
                                              withString:@""
                                       andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
    
    @try
    {
        
        
        NSString *pathParameter = self.textFieldEmail.text;
        
        NetworkHelper *helper = [NetworkHelper sharedHelper];
        
        NSDictionary *headerDict = [CoreUtility getHeaderDict];
        
        [helper getResponseWithRequestType:@"" withQueryParam:@"" withPathParam:pathParameter withJsonParam:nil withHeaders:headerDict withUrl:kAstraUrlForgotPassword success:^(NSDictionary *responseDict)
         {
             NSLog(@"Response Dict resetPasswordAstraAPI :- %@",responseDict);
             
             dispatch_async(dispatch_get_main_queue(), ^(void) {
                 [activityLoadingView removeFromSuperview];
                 if (responseDict)
                 {
                     if (responseDict.count>0)
                     {
                         // NSString *keyString = [responseDict valueForKey:@"text"];
                         // Success Response : {"accountType":"Password Set","custId":0,"message":"true"}
                         
                         NSString *messege = [responseDict valueForKey:@"message"];
                         
                         if ([[messege lowercaseString] isEqualToString:@"true"])
                         {
                             UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Alert" message:@"Your request for new password has been accepted.The new password would be emailed to the registered email address only.Please do write us at support@thomascook.in in case you any require any assistance."
                                                                                         preferredStyle: UIAlertControllerStyleAlert];
                             
                             UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                                        {
                                                            
                                                            [self dismissViewControllerAnimated:NO completion:nil];
                                                        }];
                             
                             [alertView addAction:actionOk];
                             
                             [self presentViewController:alertView animated:YES completion:nil];
                             
                         }
                         else
                         {
                             UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Failure"
                                                                                                message:@"Some error occured"
                                                                                         preferredStyle: UIAlertControllerStyleAlert];
                             
                             
                             
                             UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                                        {
                                                            
                                                            //[self setUserDetailsAndClosePopUp];
                                                        }];
                             
                             [alertView addAction:actionOk];
                             
                             [self presentViewController:alertView animated:YES completion:nil];
                             
                         }
                     }
                 }
             });
             
         }
                                   failure:^(NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^(void)
                            {
                                [activityLoadingView removeFromSuperview];
                                NSLog(@"Response Dict : %@",[error description]);
                            });
             
             
             
         }];
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"%@", exception.reason);
        [activityLoadingView removeFromSuperview];
    }
    @finally
    {
        NSLog(@"exception finally called");
    }
    
    
}

@end
