//
//  OverviewVC.h
//  holidays
//
//  Created by Pushpendra Singh on 18/08/15.
//  Copyright (c) 2015 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Holiday.h"
#import "HolidayPackageDetail.h"
#import "TabMenuVC.h"
#import "BaseViewController.h"

@interface OverviewVC : UIViewController<UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate>
- (IBAction)actionForSideArrowRight:(id)sender;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollSite;
- (IBAction)actionForSideArrowleft:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *viewBottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightConstraint;

@property (weak, nonatomic) IBOutlet UILabel *lblPackageCost;

@property (weak, nonatomic) IBOutlet UILabel *lblNightAndDays;
@property (weak, nonatomic) IBOutlet UILabel *lblGroupTour;
@property (strong, nonatomic) IBOutlet UIView *popUpViewForEmail;
@property (weak, nonatomic) IBOutlet UIImageView *bannerImageView;
@property (strong ,nonatomic) HolidayPackageDetail *packageDetail;
@property (weak, nonatomic) IBOutlet UIButton *btnEmail;
@property(nonatomic,assign) BOOL backBtnFromOverview;
@property (strong, nonatomic) IBOutlet UIView *imageViewInTable;
@property (weak, nonatomic) IBOutlet UITableView *imageTableView;
@property id tabMenuVC;

@property (strong,nonatomic) NSArray * completePackageDetail;
- (IBAction)actionOnViewGalleryBtn:(id)sender;

#pragma mark - Email PopUp

@property(strong,nonatomic)UITextField *txtFldName;
@property(strong,nonatomic)UITextField *txtFldEmail;
@property(strong,nonatomic)UITextField *txtFldPhoneNo;
@property(strong,nonatomic)UITextField *txtFldQueryFor;
@property(strong,nonatomic)UITextField *txtFldQueryRltdTo;
@property(strong,nonatomic)UITextField *txtFldBFNNo;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewOverView;
@property (strong, nonatomic) IBOutlet UIView *viewOverView;
@property (weak, nonatomic) IBOutlet UITextView *conditionsTextView;
@property (strong, nonatomic) BaseViewController *baseViewObj;

@property (weak, nonatomic) IBOutlet UIImageView *imageFlight;
@property (weak, nonatomic) IBOutlet UIImageView *imageMeal;
@property (weak, nonatomic) IBOutlet UIImageView *imageSightSeeing;
@property (weak, nonatomic) IBOutlet UIImageView *imageVisa;
@property (weak, nonatomic) IBOutlet UIImageView *imageAccomodation;

@end



