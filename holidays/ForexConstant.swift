//
//  ForexConstant.swift
//  holidays
//
//  Created by Parshwanath on 09/12/17.
//  Copyright © 2017 Mobicule. All rights reserved.
//

import Foundation

public let OK_ALERT_TITLE = "Ok"

//MARK: - Controller Name Constant
public let MONEY_TRANSFER_VC = "MoneyTransferViewController"
public let FOREX_BASE_VC = "ForexBaseViewController"

//MARK: - Tool Tips

public let PURPOSE_OF_TRAVEL_TOOL_TIP:String = "For any purpose of travel the total amount of Foreign Exchange that you can carry is restricted to USD 250,000 or equivalent in a year."

public let TRAVEL_DATE_TOOL_TIP:String = "As per RBI rules, your date of travel should be within the next 60 days for buying Foreign Exchanges today. If the date of travel is different for passenger, you can edit the same for each traveller before making the payment."

//MARK: VALIDATION_MESSAGE

//MARK: - Reload Forex Constant

public let RELOAD_MINI_FX_AMUNT_MSG:String = "You need to buy forex for minimum 50 "

public let RELOAD_MAX_FX_AMUNT_MSG:String = "As per RBI norms, Total Forex amount per traveller cannot be more than 2,50,000 USD equivalent in a single financial year. Please revise the amount to proceed further."

public let RELOAD_FX_AMOUNT_MAX_LIMIT:Int = 250000

public let RELOAD_MODULE_ID:Int = 4
public let BUY_MODULE_ID:Int = 1
public let SELL_MODULE_ID:Int = 2
public let RELOAD_MODULE_NAME:String = "Reload"

public let RELOAD_FOREX_CARD_VC = "ReloadForexCardViewController"

public let SELECT_CURRENCY_VALIDATION_MSG = "Please select currency"
public let ENTER_FX_AMOUNT_VALIDATION_MSG = "Please enter amount"

public let CURRENCY_CONVERTER_VC = "CurrencyConverterViewController"

public let userDefaultSavePreferedBranch = "savePrefredBranch"
public let cityCodeSavePreferedBranch = "cityCodeSavePrefredBranch"
public let cityNameSavePreferedBranch = "cityNameSavePrefredBranch"
public let branchCodeSavePreferedBranch = "branchCodeSavePrefredBranch"
public let branchNameSavePreferedBranch = "branchNameSavePrefredBranch"
public let addressSavePreferedBranch = "addressNameSavePrefredBranch"
public let latitudeSavePreferedBranch = "latitudeSavePrefredBranch"
public let longitudeSavePreferedBranch = "longitudeSavePrefredBranch"

public let BookingFareIncrease = "fare_increased"
public let BookingFareDecrease = "fare_decreased"

public enum AddCurrencyBtnAction: String {
    
    case Add = "add"
    case Edit  = "edit"
    case Update = "update"
    case defualt = "noAction"
    
}

//MARK: - Live Rates Module
public let LIVE_RATES_VC = "LiveRatesViewController"
public let CURR_DETAIL_VC = "CurrencyDetailsViewController"

//MARK: - Forex Request Call PopUp
public let FOREX_REQUEST_CALL_POPUP_VC:String = "ForexRequestCallPopUpViewController"

public enum TaxType: String {
    
    case CSGST_SGST = "CGST-SGST"
    case IGST  = "IGST"
   
    
}
