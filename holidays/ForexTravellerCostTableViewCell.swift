//
//  ForexTravellerCostTableViewCell.swift
//  holidays
//
//  Created by ketan on 24/11/17.
//  Copyright © 2017 Mobicule. All rights reserved.
//

import UIKit

class ForexTravellerCostTableViewCell: UITableViewCell {

    @IBOutlet weak var labelProductName: UILabel!
    @IBOutlet weak var labelFxAmount: UILabel!
    @IBOutlet weak var labelINRAmount: UILabel!
    @IBOutlet weak var constraintHeight: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
