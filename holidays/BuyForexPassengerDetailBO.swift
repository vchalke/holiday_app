//
//  BuyForexPassengerDetailBO.swift
//  holidays
//
//  Created by Komal Katkade on 12/21/17.
//  Copyright © 2017 Mobicule. All rights reserved.
//

import Foundation
class BuyForexPassengerDetailBO
{
    var mobileNo : NSString = ""
     var emailID : String = ""
    var areAllTravellersAreIndian : Bool = false
    
    init(mobileNo:NSString,areAllTravellersAreIndian:Bool,emailID: String)
    {
        self.mobileNo = mobileNo
         self.emailID = emailID
        self.areAllTravellersAreIndian = areAllTravellersAreIndian
    }
}

