//
//  PaymentTermsObject.m
//  holidays
//
//  Created by Kush_Tech on 23/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "PaymentTermsObject.h"

@implementation PaymentTermsObject
-(instancetype)initWithPaymentTermsObjectDict:(NSDictionary *)dictionary{
    if ([super init])
    {
        self.descriptionArray = [[NSMutableArray alloc]init];
        self.tcilHolidayTermsConditionsCollection = [dictionary valueForKey:@"tcilHolidayTermsConditionsCollection"];
       
        for (NSDictionary *object in self.tcilHolidayTermsConditionsCollection){
            NSString *string = [object valueForKey:@"description"];
            
//           NSAttributedString *attbString = [[NSAttributedString alloc] initWithData:[string dataUsingEncoding:NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: [NSNumber numberWithInt:NSUTF8StringEncoding]} documentAttributes:nil error:nil];
//            [self.descriptionArray addObject:attbString.string];

            [self.descriptionArray addObject:string];
            
        }
         NSLog(@"descriptionArray %@",self.descriptionArray);
        self.paymentTerms = [dictionary valueForKey:@"paymentTerms"];;
        self.cancellationPolicy = [dictionary valueForKey:@"cancellationPolicy"];;
        
    }
    return self;
}
@end
