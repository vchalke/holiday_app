//
//  PassengerListViewController.swift
//  sotc-consumer-application
//
//  Created by Payal on 12/09/17.
//  Copyright © 2017 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit

class DeviationPassengerListViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet var passengerListTableView: UITableView!
    
   // let passengerListArray : [String]? = ["Vikas Amit Patil","Amit Sadashiv Patil"]
    var passangerDetailsArray:Array<Passenger>?
    var deviationceDetailsArray:Array<Deviation>?
    var isfromNotificationHomeVc:Bool = false
    
    var tourDetails:Tour?
    
    var tourStatus:TourStatus?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.passengerListTableView.register(UITableViewCell.self, forCellReuseIdentifier: "passengerTableView")
        
        
       self.passangerDetailsArray = self.sortPassengerByPassengerNumber()
        
        self.deviationceDetailsArray = self.tourDetails?.deviationRelation?.allObjects as? Array<Deviation>
        
        
        
             self.passengerListTableView.tableFooterView = UIView(frame: .zero)
    }

    override func viewWillAppear(_ animated: Bool) {
        
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.setRedNavigationBar()
        self.navigationController?.setnavigatiobBarTitle(title: Title.DEVIATION, viewController: self)
        self.navigationController?.setEmergencyNavigationRightButton(showEmergencyButton: true, viewController: self)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        if self.navigationController?.viewControllers.firstIndex(of: self) == nil && self.isfromNotificationHomeVc
        {
            SlideNavigationController.sharedInstance().popViewController(animated: true)
        }
        
    }
    
    //MARK: UINavigationController Delegate
    
    func leftButtonClick() -> Void
    {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return passangerDetailsArray!.count
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 80
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {

        guard let cell = tableView.dequeueReusableCell(withIdentifier: "passengerTableView", for: indexPath) as? UITableViewCell
            else {
                
                fatalError("The dequeued cell is not an instance of PassengerTableViewCell.")
            }
        
        cell.textLabel? .font = UIFont(name:"Roboto-Light", size:15)
        
        if let passenger = passangerDetailsArray?[indexPath.row] as? Passenger
        {
            
            cell.textLabel?.text  = "\(passenger.firstName ?? "") ".capitalized + "\(passenger.middleName ??  "") ".capitalized + "\(passenger.lastName ??  "")".capitalized
            
            cell.accessoryType = UITableViewCell.AccessoryType.disclosureIndicator
        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        let viewController : RequestedDeviationViewController = RequestedDeviationViewController(nibName: "RequestedDeviationViewController", bundle: nil)
        
        
        if let passenger = passangerDetailsArray?[indexPath.row]
        {
            viewController.passengerName = "\(passenger.firstName ?? "") ".capitalized + "\(passenger.middleName ??  "") ".capitalized + "\(passenger.lastName ??  "")".capitalized
            
            var canCreateDeviationMessage = self.canCreateDeviation(passenger: passenger, departureDate:tourDetails?.departureDate as! Date )
            
            
           let deviationceDetailsArray = self.getDeviationDetails(by : passenger)
            
            if(deviationceDetailsArray != nil && deviationceDetailsArray.count > 0)
            {
                viewController.deviationceDetailsArray = deviationceDetailsArray
                
                viewController.touDetails = self.tourDetails
                
                viewController.selectedPassenger = passenger
                
                viewController.canCreateDeviation = canCreateDeviationMessage == "" ? true : false
                
                 self.navigationController?.pushViewController(viewController, animated: true)
                
            }else{
                
                if canCreateDeviationMessage == ""
                {
                    self.displayDaviationCreationPopUp(passenger:passenger)
                }else
                {
                    displatAlert(message: canCreateDeviationMessage)
                }
            }
        }
    }
    
    func displatAlert(message:String)
    {
        let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: AlertMessage.TITLE_OK, style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func getDeviationDetails(by passenger:Passenger) -> Array<Deviation> {
        
        if passenger.passengerNumber != nil
        {
            let filteredPassengerArray = deviationceDetailsArray?.filter() { $0.passengerNumber == passenger.passengerNumber }
            
            return filteredPassengerArray!;
            
        }
        
        return [];
    }
    
    func isTicketPresent(passenger:Passenger) -> Bool
    {
        if let passengerDocuments:Array<TourPassengerDouments> = tourDetails?.passengerDocumentRelation?.allObjects as? Array<TourPassengerDouments>
        {
           if  passengerDocuments.count > 0
           {
            
           let filteredTicket = passengerDocuments.filter(){/*$0.passengerNumber == passenger.passengerNumber && */($0.documentType?.uppercased() ?? "").contains("VOUCHER")}
            
                if filteredTicket.count > 0
                {
                    return true
                }
            
            }
        }
        
        return false
    }
    
    func canCreateDeviation(passenger:Passenger,departureDate:Date)->String
    {
        if self.tourStatus == TourStatus.Past
        {
            return AlertMessage.Deviation.PAST_TOUR_DEVIATION
            
        }else if self.tourStatus == TourStatus.Cancelled
        {
             return AlertMessage.Deviation.CANCELLED_TOUR_DEVIATION
        }
        else if let days:Int = self.getDaysBetweenTwoDates(firstDate: Date(), secondDate: departureDate)
        {
            if days < 30
            {
              return  AlertMessage.Deviation.BEFORE_30_DAY
                
            }else if isTicketPresent(passenger: passenger){
                
                return AlertMessage.Deviation.ALREADY_DEVIATION_CREATED
            }
            
        }
        
        
        return ""
    }
    
    func getDaysBetweenTwoDates(firstDate:Date,secondDate:Date) -> Int?
    {
        let calendar = NSCalendar.current
        
        // Replace the hour (time) of both dates with 00:00
        let date1 = calendar.startOfDay(for: firstDate)
        let date2 = calendar.startOfDay(for: secondDate)
        
        let components = calendar.dateComponents([.day], from: date1, to: date2)
        
        return components.day
    }
    
    func displayDaviationCreationPopUp(passenger:Passenger)  {
        
        
        
        let alertController = UIAlertController(title: "", message: AlertMessage.Deviation.CREATE_DEVIATION, preferredStyle: UIAlertController.Style.alert)
        
        let saveAction = UIAlertAction(title: AlertMessage.TITLE_CANCEL, style: UIAlertAction.Style.default, handler: {
            alert -> Void in
            
            
        })
        
        let cancelAction = UIAlertAction(title: AlertMessage.TITLE_CREATE_DEVIATION, style: UIAlertAction.Style.default, handler: {
            (action : UIAlertAction!) -> Void in
            
            let viewController : DeviationViewController = DeviationViewController(nibName: "DeviationViewController", bundle: nil)
            
            viewController.tourDetails = self.tourDetails
            viewController.selecedPassenger = passenger
            
            self.navigationController?.pushViewController(viewController, animated: true)
        })
        
        alertController.view.backgroundColor = UIColor.white
        alertController.view.layer.cornerRadius = 10.0
        
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: false, completion: nil)
        
    }
    
    func sortPassengerByPassengerNumber() -> Array<Passenger> {
        
       if let passengerArray:Array<Passenger> = self.tourDetails?.passengerRelation?.allObjects as? Array<Passenger>
       {
            if passengerArray.count > 0
            {
                
            let sortedPassengerArray = passengerArray.sorted(){ $0.passengerNumber! < $1.passengerNumber!}
                
                return sortedPassengerArray
                
            }
        }
        
        return []
        
    }
    
   /* func displayDaviationCreationPopUp()
    {
    
    let refreshAlert = UIAlertController(title: "Deviation", message: "No details found, Do you want to create new deviation?.", preferredStyle: UIAlertControllerStyle.alert)
     
     
     
     refreshAlert.addAction(UIAlertAction(title: "Create Deviation", style: .default, handler: { (action: UIAlertAction!) in
     
        
        
     }))
        
     
     refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
     printLog("Handle Cancel Logic here")
     }))
     
     present(refreshAlert, animated: true, completion: nil)
        
    }*/
    
}
