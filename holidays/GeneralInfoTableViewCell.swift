//
//  GeneralInfoTableViewCell.swift
//  holidays
//
//  Created by Kush_Team on 22/06/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

import UIKit

class GeneralInfoTableViewCell: UITableViewCell {
    
    @IBOutlet weak var txt_birthField: UITextField!
    @IBOutlet weak var txt_matrialField: UITextField!
    @IBOutlet weak var txt_anniverssaryField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setProfileDetails(profileDict:[String:Any]?){
        if let isDict = profileDict{
            let generalInfo = isDict[APIResponseConstants.UserProfile.GENERAL_INFO] as? [String:Any]
            self.txt_birthField.text = generalInfo?["dob"] as? String
            self.txt_anniverssaryField.text = generalInfo?["anniversaryDate"] as? String
            self.txt_matrialField.text = generalInfo?["maritalStatus"] as? String
        }
    }
    
    func setProfileModelsDetails(profileModel:CustomerProfileModel){
        self.txt_birthField.text = profileModel.dob
        self.txt_anniverssaryField.text = profileModel.anniversaryDate
        self.txt_matrialField.text = profileModel.maritalStatus
    }
}
