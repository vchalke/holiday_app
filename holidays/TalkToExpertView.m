//
//  TalkToExpertView.m
//  holidays
//
//  Created by Kush_Tech on 11/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "TalkToExpertView.h"

@implementation TalkToExpertView

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
     self = [super initWithCoder:aDecoder];
     if(self) {
        [self loadNib];
    }
     return self;

}

//- (instancetype)initWithFrame:(CGRect)frame {
//     self = [super initWithFrame:frame];
//     if(self) {
//        [self loadNib];
//    }
//     return self;
//}



-(void)loadNib{
    
    UIView *view = [[[NSBundle bundleForClass:[self class]]loadNibNamed:@"TalkToExpertView" owner:self options:nil] firstObject];
    view.frame = self.bounds;
    [self addSubview:view];
    [self.table_views registerNib:[UINib nibWithNibName:@"ExpertViewCell" bundle:nil] forCellReuseIdentifier:@"ExpertViewCell"];
//   imgArray = [NSArray arrayWithObjects:@"chatInYellow",@"emailInYellow",@"callInYellow", nil];
//    titleArray = [NSArray arrayWithObjects:@" Live Chat ",@" Send Query ",@" Call Us ", nil];
//    imgArray = [NSArray arrayWithObjects:@"emailInYellow",@"callInYellow", nil];
    imgArray = [NSArray arrayWithObjects:@"srpEmail",@"srpPhone", nil];
    titleArray = [NSArray arrayWithObjects:@" Send Query ",@" Call Us ", nil];
    self.table_views.delegate = self;
    self.table_views.dataSource = self;
    
    UITapGestureRecognizer *tapGestOne = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapGestOne:)];
    [self.upper_views addGestureRecognizer:tapGestOne];
    UITapGestureRecognizer *tapGestTwo = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapGestTwo:)];
    [self.bottom_views addGestureRecognizer:tapGestTwo];
}
-(void)setDistanceFromBottom:(CGFloat)distance withLeftSide:(BOOL)isLeft{
    self.cnst_distfromBotom.constant = distance;
    isLeftSide = isLeft;
    [self.table_views reloadData];
}
- (void)tapGestOne:(UITapGestureRecognizer*)sender{
    [self.expertDelgate optionClickwithIndex:100];
}
- (void)tapGestTwo:(UITapGestureRecognizer*)sender{
    [self.expertDelgate optionClickwithIndex:100];
}
#pragma mark - collectionview datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [titleArray count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ExpertViewCell* expertCell = (ExpertViewCell*)[tableView dequeueReusableCellWithIdentifier:@"ExpertViewCell"];
    expertCell.lbl_titleRight.text = [titleArray objectAtIndex:indexPath.row];
    expertCell.imgeViewRight.image = [UIImage imageNamed:[imgArray objectAtIndex:indexPath.row]];
    expertCell.lbl_titleLeft.text = [titleArray objectAtIndex:indexPath.row];
    expertCell.imgeViewLeft.image = [UIImage imageNamed:[imgArray objectAtIndex:indexPath.row]];
    expertCell.lbl_titleLeft.hidden = isLeftSide ? NO : YES;
    expertCell.imgeViewLeft.hidden = isLeftSide ? NO : YES;
    expertCell.lbl_titleRight.hidden = isLeftSide ? YES : NO;
    expertCell.imgeViewRight.hidden = isLeftSide ? YES : NO;
    
    return expertCell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"djdgsgdgaqd");
    [self.expertDelgate optionClickwithIndex:indexPath.row];
}
//-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell*)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
//    
//    [UIView animateWithDuration:2.0 delay:1.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
//        cell.contentView.alpha = 0;
//    }completion:^(BOOL finished) {
//       cell.contentView.alpha = 1.0;
//    }];
//}
@end
