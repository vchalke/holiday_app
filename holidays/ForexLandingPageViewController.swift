//
//  ForexLandingPageViewController.swift
//  holidays
//
//  Created by Komal Katkade on 11/22/17.
//  Copyright © 2017 Mobicule. All rights reserved.
//

import UIKit

class ForexLandingPageViewController:ForexBaseViewController,UICollectionViewDelegate,UICollectionViewDataSource,SlideNavigationControllerDelegate
{
    @IBOutlet weak var menuCollectionView: UICollectionView!
    @IBOutlet var landingPageView: UIView!
    let imageArray : NSArray = ["buy_forex","sell_forex","money_transfer","forex_card","currency_converter","live_rates"]
    var payloadList:NSMutableDictionary = NSMutableDictionary.init()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
      //  super.hideMenuButton()
        
        Bundle.main.loadNibNamed("ForexLandingPageViewController", owner: self, options: nil)
        
        super.addViewInBaseView(childView: landingPageView);
        
        self.menuCollectionView!.register(LandingCollectionViewCell.self, forCellWithReuseIdentifier: "ForexLandingcollectionCell")
        
        self.menuCollectionView.register(UINib(nibName: "LandingCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ForexLandingcollectionCell")
        
        self.setUpCollectionViewLayout()
    }
    
    func setUpCollectionViewLayout()
    {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        let width = UIScreen.main.bounds.width
        layout.sectionInset = UIEdgeInsets(top: 35, left: 20, bottom: 35, right: 20)
        layout.itemSize = CGSize(width: (width / 2)-30, height: (width / 2)-20)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 20
        menuCollectionView!.collectionViewLayout = layout
    }
    
    override func viewWillLayoutSubviews()
    {
        super.setSubViewFrame(childView: landingPageView)
    }
    
    // MARK: CollectionView Methods
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1;
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return 6;
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell : LandingCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ForexLandingcollectionCell", for: indexPath) as! LandingCollectionViewCell
       cell.menuImageView.image = UIImage (named: imageArray.object(at: indexPath.row) as! String)
        return cell;
    }
    
//    func collectionView(_ collectionView: UICollectionView,
//                                 layout collectionViewLayout: UICollectionViewLayout,
//                                 sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize{
//        let width =  UIScreen.main.bounds.width
//
//        return CGSize.init(width: (width/2)-35, height:  (width/2)-35)
//    }
//
//    @objc(collectionView:layout:insetForSectionAtIndex:)  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets{
//        print("collectionViewLayout")
//        //        return UIEdgeInsetsMake(<#T##top: CGFloat##CGFloat#>, <#T##left: CGFloat##CGFloat#>, <#T##bottom: CGFloat##CGFloat#>, <#T##right: CGFloat##CGFloat#>)
//        return UIEdgeInsetsMake(50, 30, 50, 30)
//    }
    
    
    //Web View
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let selectedMenu : NSString = imageArray[indexPath.row] as! NSString
        
         let webViewVC:WebViewController = WebViewController(nibName: "BaseViewController", bundle: nil)
        
        if(selectedMenu.isEqual(to: "buy_forex")    )
        {
            
            payloadList = NSMutableDictionary.init()
            payloadList.setValue("yes", forKey: "s^BUY_FOREX")
            self.netCoreForexHomePageLogEvent()
            
            
            webViewVC.headerString = "Buy Forex"
                       let url = URL(string: kForexBuyURL)
            webViewVC.url = url;
            SlideNavigationController.sharedInstance().pushViewController(webViewVC, animated: true)
        
        }
        else if(selectedMenu.isEqual(to: "sell_forex"))
        {
         
            payloadList = NSMutableDictionary.init()
            payloadList.setValue("yes", forKey: "s^SELL_FOREX")
            self.netCoreForexHomePageLogEvent()
            
             webViewVC.headerString = "Sell Forex";
            
            let url = URL(string: kForexSellURL)
            webViewVC.url = url;
            SlideNavigationController.sharedInstance().pushViewController(webViewVC, animated: true)
            
            
        }
        else if(selectedMenu.isEqual(to: "money_transfer"))
        {
          
            payloadList = NSMutableDictionary.init()
            payloadList.setValue("yes", forKey: "s^MONEY_TRANSFER")
            self.netCoreForexHomePageLogEvent()
            
            webViewVC.headerString = "Money Transfer";
            
            let url = URL(string: kForexMoneyTransferURL)
            webViewVC.url = url;
            SlideNavigationController.sharedInstance().pushViewController(webViewVC, animated: true)
        }
        else if(selectedMenu.isEqual(to: "forex_card"))
        {
           
            payloadList = NSMutableDictionary.init()
            payloadList.setValue("yes", forKey: "s^RELOAD_FOREX_CARD")
            self.netCoreForexHomePageLogEvent()
            
            webViewVC.headerString = "Reload Forex";
            
            let url = URL(string: kForexReloadForexURL)
            webViewVC.url = url;
            SlideNavigationController.sharedInstance().pushViewController(webViewVC, animated: true)
            
        }
        else if(selectedMenu.isEqual(to: "currency_converter"))
        {
         
            payloadList = NSMutableDictionary.init()
            payloadList.setValue("yes", forKey: "s^CURRENCY_CONVERTER")
            self.netCoreForexHomePageLogEvent()
            
            webViewVC.headerString = "Currency Converter";
            
            let url = URL(string: kForexCurrencyConverterURL)
            webViewVC.url = url;
            SlideNavigationController.sharedInstance().pushViewController(webViewVC, animated: true)
        }
        else if(selectedMenu.isEqual(to: "live_rates"))
        {
         
            payloadList = NSMutableDictionary.init()
            payloadList.setValue("yes", forKey: "s^LIVE_RATES")
            self.netCoreForexHomePageLogEvent()
            
            webViewVC.headerString = "Live Rates";
            
            let url = URL(string: kForexLiveRatesyURL)
            webViewVC.url = url;
            SlideNavigationController.sharedInstance().pushViewController(webViewVC, animated: true)
        }
    }
    
    
    //Native
  /*func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let selectedMenu : NSString = imageArray[indexPath.row] as! NSString
        
        let webViewVC:WebViewController = WebViewController(nibName: "BaseViewController", bundle: nil)
        
        if(selectedMenu.isEqual(to: "buy_forex")    )
        {
            
            payloadList = NSMutableDictionary.init()
            payloadList.setValue("yes", forKey: "s^BUY_FOREX")
            self.netCoreForexHomePageLogEvent()
            
                        let buyForexOptionsVC: BuyForexOptionsViewController = BuyForexOptionsViewController(nibName: "ForexBaseViewController", bundle: nil)
                        self.navigationController?.pushViewController(buyForexOptionsVC, animated: true)
            
           
            
        }
        else if(selectedMenu.isEqual(to: "sell_forex"))
        {
            payloadList = NSMutableDictionary.init()
            payloadList.setValue("yes", forKey: "s^SELL_FOREX")
            self.netCoreForexHomePageLogEvent()
            
             let sellForexPayment : SellForexProductViewController = SellForexProductViewController(nibName: "ForexBaseViewController", bundle: nil)
             
             self.navigationController?.pushViewController(sellForexPayment, animated: true)
             
             // self.present(sellForexPayment, animated: true, completion: nil)
            
            
           
            
        }
        else if(selectedMenu.isEqual(to: "money_transfer"))
        {
            payloadList = NSMutableDictionary.init()
            payloadList.setValue("yes", forKey: "s^MONEY_TRANSFER")
            self.netCoreForexHomePageLogEvent()
            
            let moneyTransferViewController : MoneyTransferViewController = MoneyTransferViewController(nibName: FOREX_BASE_VC, bundle: nil)
            moneyTransferViewController.moduleName = "Money Transfer"
            moneyTransferViewController.urlString = kForexMoneyTransferURL
             self.navigationController?.pushViewController(moneyTransferViewController, animated: true)
            
           
        }
        else if(selectedMenu.isEqual(to: "forex_card"))
        {
            
            payloadList = NSMutableDictionary.init()
            payloadList.setValue("yes", forKey: "s^RELOAD_FOREX_CARD")
            self.netCoreForexHomePageLogEvent()
             let reloadForexCardViewController : ReloadForexCardViewController = ReloadForexCardViewController(nibName: FOREX_BASE_VC, bundle: nil)
             self.navigationController?.pushViewController(reloadForexCardViewController, animated: true)
            
            
            
            
        }
        else if(selectedMenu.isEqual(to: "currency_converter"))
        {
            
            payloadList = NSMutableDictionary.init()
            payloadList.setValue("yes", forKey: "s^CURRENCY_CONVERTER")
            self.netCoreForexHomePageLogEvent()
            
             let currencyConverterViewController : CurrencyConverterViewController = CurrencyConverterViewController(nibName: FOREX_BASE_VC, bundle: nil)
             self.navigationController?.pushViewController(currencyConverterViewController, animated: true)
            
            
           
        }
        else if(selectedMenu.isEqual(to: "live_rates"))
        {
            
            payloadList = NSMutableDictionary.init()
            payloadList.setValue("yes", forKey: "s^LIVE_RATES")
            self.netCoreForexHomePageLogEvent()
            
            let liveRatesViewController : LiveRatesViewController = LiveRatesViewController(nibName: FOREX_BASE_VC, bundle: nil)
             self.navigationController?.pushViewController(liveRatesViewController, animated: true)
           
        }
    }*/

    
    // MARK: SlideMenu Methods
    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool
    {
        return true
    }
    
    func slideNavigationControllerShouldDisplayRightMenu() -> Bool
    {
        return false
    }
    
    //MARK:- NetCore log Events
    
    //FOREX_App_Home
    
    func netCoreForexHomePageLogEvent()
    {
        
       if payloadList.object(forKey: "s^BUY_FOREX") == nil
       {
            payloadList.setValue("no", forKey: "s^BUY_FOREX")
        }
        if payloadList.object(forKey: "s^SELL_FOREX") == nil
        {
             payloadList.setValue("no", forKey: "s^SELL_FOREX")
        }
        if payloadList.object(forKey: "s^MONEY_TRANSFER") == nil
        {
              payloadList.setValue("no", forKey: "s^MONEY_TRANSFER")
        }
        if payloadList.object(forKey: "s^RELOAD_FOREX_CARD") == nil
        {
              payloadList.setValue("no", forKey: "s^RELOAD_FOREX_CARD")
        }
        if payloadList.object(forKey: "s^CURRENCY_CONVERTER") == nil
        {
             payloadList.setValue("no", forKey: "s^CURRENCY_CONVERTER")
        }
        if payloadList.object(forKey: "s^LIVE_RATES") == nil
        {
             payloadList.setValue("no", forKey: "s^LIVE_RATES")
        }
        if payloadList.object(forKey: "s^SOURCE") == nil
        {
              payloadList.setValue("App", forKey: "s^SOURCE")
        }
        
        NetCoreAnalyticsVC.getNetCorePushNotificationDetailDictionary(payloadList as! [AnyHashable : Any], withPayloadCount: 138)
        
    }
    
 
    
}
