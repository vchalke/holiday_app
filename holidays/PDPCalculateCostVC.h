//
//  PDPCalculateCostVC.h
//  holidays
//
//  Created by Kush_Team on 25/05/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HolidayPackageDetail.h"
#import "NewMasterVC.h"
NS_ASSUME_NONNULL_BEGIN

@interface PDPCalculateCostVC : NewMasterVC
@property (strong ,nonatomic) NSDictionary *notificationDict;
@property (strong ,nonatomic) HolidayPackageDetail *holidayPkgDetailInPdpCost;
@property (weak, nonatomic) IBOutlet UIButton *btn_policyterm;
@property (weak, nonatomic) IBOutlet UIButton *btn_privacypolicy;
@property (weak, nonatomic) IBOutlet UILabel *lbl_roomNum;
@property (weak, nonatomic) IBOutlet UIButton *btn_termscondi;
@property (weak, nonatomic) IBOutlet UITextField *txt_deptCity;
@property (weak, nonatomic) IBOutlet UITextField *txt_numoftravel;
@property (weak, nonatomic) IBOutlet UITextField *txt_deptDate;
@property (weak, nonatomic) IBOutlet UITextField *txt_mobileNum;
@property (weak, nonatomic) IBOutlet UIButton *btn_check;
@end

NS_ASSUME_NONNULL_END
