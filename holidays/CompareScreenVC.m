//
//  CompareScreenVC.m
//  holidays
//
//  Created by Kush_Tech on 23/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "CompareScreenVC.h"
#import "RoomsDataModel.h"
#import "CalenderScreenVC.h"
#import "DateStringSingleton.h"
#import "PackageDetailModel.h"
#import "ItinenaryObject.h"
#import "TransferObject.h"
#import "IncludeExcludeObject.h"
#import "WebUrlConstants.h"
#import "SightSeenObject.h"
#import "MealsObject.h"
#import "AccomodationObject.h"
#import "SRPScreenVC.h"
#import "Thomas_Cook_Holidays-Swift.h"

@interface CompareScreenVC ()<CalenderScreenVCDelegate,FareCalendarCompareDelegate>
{
    HolidayPackageDetail *pkgDetailOne;
    HolidayPackageDetail *pkgDetailTwo;
    
    NSMutableArray *tourTypeArrayOne;
    NSMutableArray *tourTypeArrayTwo;
    
    NSMutableArray *cityTypeArrayOne;
    NSMutableArray *cityTypeArrayTwo;
    
    NSString *packageTypeOne;
    NSString *packageTypeTwo;
    
    int accomTypeOne;
    int accomTypeTwo;
    
    int selectPackaheClassIdOne;
    int selectPackaheClassIdTwo;
    
    NSArray *packageTypeArrayOne;
    NSArray *packageTypeArrayTwo;
    
    NSMutableArray *roomRecordArray;
    NSMutableArray *ItineraryCodeArray;
    NSArray *travellerArrayForCalculation;
    LoadingView *activityLoadingView;
    
    NSString *departFromOne;
    NSString *selectedLtItineraryCodeOne;
    NSString *departFromTwo;
    NSString *selectedLtItineraryCodeTwo;
    
    NSArray *filteredarrayOne;
    NSArray *filteredarrayTwo;
    
    NSDictionary *firstPkgDetailDict;
    NSDictionary *secondPkgDetailDict;
    
    NSDictionary *firstHubCodeDict;
    NSDictionary *secondHubCodeDict;
    
    int jumpingIndex;
}
@end

@implementation CompareScreenVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    tourTypeArrayOne = [[NSMutableArray alloc]init];
    tourTypeArrayTwo = [[NSMutableArray alloc]init];
    
    cityTypeArrayOne = [[NSMutableArray alloc]init];
    cityTypeArrayTwo = [[NSMutableArray alloc]init];
    
    filteredarrayOne = [[NSMutableArray alloc]init];
    filteredarrayTwo = [[NSMutableArray alloc]init];
    
    firstPkgDetailDict = [[NSDictionary alloc]init];
    secondPkgDetailDict = [[NSDictionary alloc]init];
    
    firstHubCodeDict = [[NSDictionary alloc]init];
    secondHubCodeDict = [[NSDictionary alloc]init];
    
    roomRecordArray = [[NSMutableArray alloc]init];
    RoomsDataModel *roomModel = [[RoomsDataModel alloc]init];
    [roomRecordArray addObject:roomModel];
    [self setRoomDataModelWithIndex:1];
    
    [self loadAllDataOfCompareList];
    
}
-(void)loadAllDataOfCompareList{
    
    [self.btn_cancelPackageOne addTarget:self action:@selector(deleteClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.btn_cancelPackageTwo addTarget:self action:@selector(deleteClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.btn_addAnotherPkgOne addTarget:self action:@selector(addNewPackClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.btn_addAnotherPkgTwo addTarget:self action:@selector(addNewPackClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    self.btn_addAnotherPkgOne.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.btn_addAnotherPkgTwo.titleLabel.textAlignment = NSTextAlignmentCenter;
    
    [self showCompareList];
    
    
}
-(void) deleteClicked:(UIButton*)sender
{
    NSLog(@"%@",[NSString stringWithFormat:@"PKG%06ld",(long)sender.tag]);
    NSArray *array = [[CoreDataSingleton sharedInstance] getArrayOfObjectForEntityName:CompareListHoliday];
    NSArray *packIdArray = [[CoreDataSingleton sharedInstance]getdeletedObjectFromPackageListArray:array withPackID:[NSString stringWithFormat:@"PKG%06ld",(long)sender.tag] forEntityName:CompareListHoliday];
    if ([packIdArray count]>0){
        [[[CoreDataSingleton sharedInstance]getManagedObjectContext] deleteObject:[packIdArray firstObject]];
        NSError * error = nil;
        if (![[[CoreDataSingleton sharedInstance]getManagedObjectContext] save:&error]){
            NSLog(@"Error ! %@", error);
        }
    }
    [self showCompareList];
}
-(void) addNewPackClicked:(UIButton*)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void) showTourTypeClicked:(UIButton*)sender
{
    [self showMultipleOptionsInAlertWithTitle:@"Please Select" msg:@"Tour Type" style:UIAlertControllerStyleActionSheet buttArray:(sender.tag==0) ? tourTypeArrayOne: tourTypeArrayTwo completion:^(NSString * string) {
        if (![string isEqualToString:@"Cancel"]){
            if (sender.tag==0){
                int classiD = [[string uppercaseString] isEqualToString:@"STANDARD"] ? 0 : [[string uppercaseString] isEqualToString:@"DELUXE"] ? 1 : 2;
                selectPackaheClassIdOne = classiD;
                accomTypeOne = classiD;
                self.lbl_toursTypesOne.text = string;
            }else if (sender.tag==1){
                self.lbl_toursTypesTwo.text = string;
                int classiD = [[string uppercaseString] isEqualToString:@"STANDARD"] ? 0 : [[string uppercaseString] isEqualToString:@"DELUXE"] ? 1 : 2;
                selectPackaheClassIdTwo = classiD;
                accomTypeTwo = classiD;
            }
        }
    }];
}
-(void) showDeptCityClicked:(UIButton*)sender
{
    /*
    NSArray *titleArray = [self getArrayOfCitiesFromPackage:(sender.tag==0) ? pkgDetailOne : pkgDetailTwo packageTypes:(sender.tag) ? packageTypeOne : packageTypeTwo withAccomType:(sender.tag) ? accomTypeOne : accomTypeTwo withTag:sender.tag];
    [self showMultipleOptionsInAlertWithTitle:@"Please Select" msg:@"Departure City" style:UIAlertControllerStyleActionSheet buttArray:titleArray completion:^(NSString * string) {
        if (![string isEqualToString:@"Cancel"]){
            if (sender.tag==0){
                self.lbl_departureCityOne.text = string;
                departFromOne = string;
                
            }else if (sender.tag==1){
                self.lbl_departureCityTwo.text = string;
                departFromTwo = string;
            }
        }
    }];
    */
    [self showCitiesArray:(sender.tag==0) ? pkgDetailOne : pkgDetailTwo packageTypes:(sender.tag==0) ? packageTypeOne : packageTypeTwo withAccomType:(sender.tag==0) ? accomTypeOne : accomTypeTwo withTag:sender.tag];
}

-(void) calenderClicked:(UIButton*)sender
{
    if (sender.tag == 0){
        if (![self.lbl_toursTypesOne.text isEqualToString:@"- -"] && ![self.lbl_departureCityOne.text isEqualToString:@"- -"]){
//            [self setItinenaryObjectArray:self.lbl_departureCityOne.text forFilterDataArray:filteredarrayOne withTag:sender.tag];
            NSLog(@"ItineraryCodeArray One %@",ItineraryCodeArray);
            [self showCalenderViewInCompareScreenForPackage:pkgDetailOne withIndex:sender.tag];
        }else{
           
            [self showAlertViewWithTitle:@"Alert" withMessage:@"Select Tour type and Departure City both"];
        }
    }else{
       if (![self.lbl_toursTypesTwo.text isEqualToString:@"- -"] && ![self.lbl_departureCityTwo.text isEqualToString:@"- -"]){
//            [self setItinenaryObjectArray:self.lbl_departureCityTwo.text forFilterDataArray:filteredarrayTwo withTag:sender.tag];
           NSLog(@"ItineraryCodeArray Two %@",ItineraryCodeArray);
            [self showCalenderViewInCompareScreenForPackage:pkgDetailTwo withIndex:sender.tag];
        }else{
            [self showAlertViewWithTitle:@"Alert" withMessage:@"Select Tour type and Departure City both"];
        }
    }
}

#pragma mark - Showing Compare List

-(void)showCompareList{
    NSArray *allCompareListArray = [[CoreDataSingleton sharedInstance]getArrayOfObjectForEntityName:CompareListHoliday];
    [self showDataInFirstView:[allCompareListArray count]>0];
    [self showDataInSecondView:[allCompareListArray count]>1];
}

#pragma mark - Showing Data in UI

-(void)showDataInFirstView:(BOOL)flag{
    self.tour_ImgeViewOne.image = [UIImage imageNamed:@"defaultBanner.png"];
    if (flag){
        NSArray *allCompareListArray = [[CoreDataSingleton sharedInstance]getArrayOfObjectForEntityName:CompareListHoliday];
        CompareHolidayObject *object = [[CompareHolidayObject alloc]initWithCompareHolidayObject:[allCompareListArray objectAtIndex:0]];
        NSLog(@"btn_CompareCancelTwo ! %@", object.packageID);
        NSInteger tagID = [[object.packageID stringByReplacingOccurrencesOfString:@"PKG" withString:@""] integerValue];
        [self.btn_cancelPackageOne setTag:tagID];
        self.lbl_packageNameOne.text = object.packageName;
        self.lbl_tourPrizeOne.text = [self getNumberFormatterString:object.packagePrize];
        [self showImageUsingURL:object.packageImgUrl inImageView:self.tour_ImgeViewOne];
        
        /*
        HolidayPackageDetail *firstPackageDetail = [[HolidayPackageDetail alloc] initWithDataDict:[self convertStringToJsonDict:object.packageData]];
        NSLog(@"packageData  %@", firstPackageDetail.arrayAccomTypeList);
        tourTypeArrayOne = [firstPackageDetail.arrayAccomTypeList mutableCopy];
        if ([tourTypeArrayOne count]==0){
            [tourTypeArrayOne addObject:@"Standard"];
        }
        pkgDetailOne = firstPackageDetail;
        packageTypeArrayOne = pkgDetailOne.arrayAccomTypeList;
        NSLog(@"%@",pkgDetailOne.arrayAccomTypeList);
        [self setPackageTypeForHoliday:firstPackageDetail atIndex:0];
        PackageDetailModel *packageModel = [[PackageDetailModel alloc]initWithPackageDetailDict:firstPackageDetail.packageDetailDictObj];
        [self showInWebViewsWith:firstPackageDetail WithPackageDetail:packageModel withIndex:0];
        self.lbl_holidayTypeOne.text = ([[firstPackageDetail.strPackageSubType uppercaseString]isEqualToString:@"GIT"]) ? @"Group Tour" : @"- -";
        NSArray *arr = [self getCountriesCovered:firstPackageDetail];
        */
        firstPkgDetailDict = [self convertStringToJsonDict:object.packageData];
        pkgDetailOne = [[HolidayPackageDetail alloc] initWithDataDict:[self convertStringToJsonDict:object.packageData]];
        NSLog(@"packageMode  %@", pkgDetailOne.packageMode);
        NSLog(@"arrayOfHubList  %@", pkgDetailOne.arrayOfHubList);
        NSLog(@"packageData  %@", pkgDetailOne.arrayAccomTypeList);
        tourTypeArrayOne = [pkgDetailOne.arrayAccomTypeList mutableCopy];
        if ([tourTypeArrayOne count]==0){
            [tourTypeArrayOne addObject:@"Standard"];
        }
        packageTypeArrayOne = pkgDetailOne.arrayAccomTypeList;
        NSLog(@"%@",pkgDetailOne.arrayAccomTypeList);
        NSLog(@"arrayLtPricingCollection %@",pkgDetailOne.arrayLtPricingCollection);
        NSLog(@"arraytcilHolidayPriceCollection %@",pkgDetailOne.arraytcilHolidayPriceCollection);
        [self setPackageTypeForHoliday:pkgDetailOne atIndex:0];
        PackageDetailModel *packageModel = [[PackageDetailModel alloc]initWithPackageDetailDict:pkgDetailOne.packageDetailDictObj];
        [self showInWebViewsWith:pkgDetailOne WithPackageDetail:packageModel withIndex:0];
        self.lbl_holidayTypeOne.text = ([[pkgDetailOne.strPackageSubType uppercaseString]isEqualToString:@"GIT"]) ? @"Group Tour" : @"- -";
        NSArray *arr = [self getCountriesCovered:pkgDetailOne];
        
        
        self.txtview_CountriesCoverOne.text = [arr componentsJoinedByString: @"\n"];
        [self.btn_tourTypeOne addTarget:self action:@selector(showTourTypeClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.btn_departureCityOne addTarget:self action:@selector(showDeptCityClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.btn_calenderOne addTarget:self action:@selector(calenderClicked:) forControlEvents:UIControlEventTouchUpInside];
        
    }else{
        [self showAllBlankDataWithTag:0];
    }
    
    self.lbl_toursTypesOne.text = @"- -";
    self.lbl_departureCityOne.text = @"- -";
    self.lbl_departureDateOne.text = @"Departure Date";
    self.btn_addAnotherPkgOne.hidden = flag;
    self.btn_cancelPackageOne.hidden = !flag;
    
    NSString *packageTypeString = (pkgDetailOne.startingPriceStandard > 0) ? standardPackage : (pkgDetailOne.startingPriceDelux > 0) ? deluxePackage : (pkgDetailOne.startingPricePremium > 0) ? premiumPackage : standardPackage;
    int startPrice;
    if ([packageTypeString isEqualToString:standardPackage]){
        startPrice = pkgDetailOne.startingPriceStandard;
    }else if ([packageTypeString isEqualToString:deluxePackage]){
        startPrice = pkgDetailOne.startingPriceDelux;
    }else{
        startPrice = pkgDetailOne.startingPricePremium;
    }
    self.lbl_tourPrizeOne.text = [self getNumberFormatterString:startPrice];
}


-(void)showDataInSecondView:(BOOL)flag{
    self.tour_ImgeViewTwo.image = [UIImage imageNamed:@"defaultBanner.png"];
    if (flag){
        NSArray *allCompareListArray = [[CoreDataSingleton sharedInstance]getArrayOfObjectForEntityName:CompareListHoliday];
        CompareHolidayObject *object = [[CompareHolidayObject alloc]initWithCompareHolidayObject:[allCompareListArray objectAtIndex:1]];
        NSLog(@"btn_CompareCancelTwo ! %@", object.packageID);
        NSInteger tagID = [[object.packageID stringByReplacingOccurrencesOfString:@"PKG" withString:@""] integerValue];
        [self.btn_cancelPackageTwo setTag:tagID];
        self.lbl_packageNameTwo.text = object.packageName;
        self.lbl_tourPrizeTwo.text = [self getNumberFormatterString:object.packagePrize];
        [self showImageUsingURL:object.packageImgUrl inImageView:self.tour_ImgeViewTwo];
        
        /*
        HolidayPackageDetail *secondPackageDetail = [[HolidayPackageDetail alloc] initWithDataDict:[self convertStringToJsonDict:object.packageData]];
        NSLog(@"packageData  %@", secondPackageDetail.arrayAccomTypeList);
        tourTypeArrayTwo = [secondPackageDetail.arrayAccomTypeList mutableCopy];
        if ([tourTypeArrayTwo count]==0){
            [tourTypeArrayTwo addObject:@"Standard"];
        }
        pkgDetailTwo = secondPackageDetail;
        packageTypeArrayTwo = pkgDetailTwo.arrayAccomTypeList;
        [self setPackageTypeForHoliday:secondPackageDetail atIndex:1];
        PackageDetailModel *packageModel = [[PackageDetailModel alloc]initWithPackageDetailDict:secondPackageDetail.packageDetailDictObj];
        [self showInWebViewsWith:secondPackageDetail WithPackageDetail:packageModel withIndex:1];
        self.lbl_holidayTypeTwo.text = ([[secondPackageDetail.strPackageSubType uppercaseString]isEqualToString:@"GIT"]) ? @"Group Tour" : @"- -";
        NSArray *arr = [self getCountriesCovered:secondPackageDetail];
        */
        secondPkgDetailDict = [self convertStringToJsonDict:object.packageData];
        pkgDetailTwo = [[HolidayPackageDetail alloc] initWithDataDict:[self convertStringToJsonDict:object.packageData]];
        NSLog(@"packageData  %@", pkgDetailTwo.arrayAccomTypeList);
        tourTypeArrayTwo = [pkgDetailTwo.arrayAccomTypeList mutableCopy];
        if ([tourTypeArrayTwo count]==0){
            [tourTypeArrayTwo addObject:@"Standard"];
        }
        packageTypeArrayTwo = pkgDetailTwo.arrayAccomTypeList;
        NSLog(@"%@",pkgDetailTwo.arrayAccomTypeList);
        NSLog(@"arrayLtPricingCollection %@",pkgDetailTwo.arrayLtPricingCollection);
        NSLog(@"arraytcilHolidayPriceCollection %@",pkgDetailTwo.arraytcilHolidayPriceCollection);
        [self setPackageTypeForHoliday:pkgDetailTwo atIndex:1];
        PackageDetailModel *packageModel = [[PackageDetailModel alloc]initWithPackageDetailDict:pkgDetailTwo.packageDetailDictObj];
        [self showInWebViewsWith:pkgDetailTwo WithPackageDetail:packageModel withIndex:1];
        self.lbl_holidayTypeTwo.text = ([[pkgDetailTwo.strPackageSubType uppercaseString]isEqualToString:@"GIT"]) ? @"Group Tour" : @"- -";
        NSArray *arr = [self getCountriesCovered:pkgDetailTwo];
        
        
        self.txtview_CountriesCoverTwo.text = [arr componentsJoinedByString: @"\n"];
        [self.btn_tourTypeTwo addTarget:self action:@selector(showTourTypeClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self.btn_tdepartureCityTwo addTarget:self action:@selector(showDeptCityClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self.btn_calenderTwo addTarget:self action:@selector(calenderClicked:) forControlEvents:UIControlEventTouchUpInside];
    }else{
        [self showAllBlankDataWithTag:1];
    }
    self.lbl_toursTypesTwo.text = @"- -";
    self.lbl_departureCityTwo.text = @"- -";;
    self.lbl_departureDateTwo.text = @"Departure Date";
    self.btn_addAnotherPkgTwo.hidden = flag;
    self.btn_cancelPackageTwo.hidden = !flag;
    
    NSString *packageTypeString = (pkgDetailTwo.startingPriceStandard > 0) ? standardPackage : (pkgDetailTwo.startingPriceDelux > 0) ? deluxePackage : (pkgDetailTwo.startingPricePremium > 0) ? premiumPackage : standardPackage;
    int startPrice;
    if ([packageTypeString isEqualToString:standardPackage]){
        startPrice = pkgDetailTwo.startingPriceStandard;
    }else if ([packageTypeString isEqualToString:deluxePackage]){
        startPrice = pkgDetailTwo.startingPriceDelux;
    }else{
        startPrice = pkgDetailTwo.startingPricePremium;
    }
    self.lbl_tourPrizeTwo.text = [self getNumberFormatterString:startPrice];
    
}
-(void)showAllBlankDataWithTag:(NSInteger)index{
    if (index==0){
        [self.btn_cancelPackageOne setTag:0];
        self.lbl_packageNameOne.text = @"- -";
        self.txtview_CountriesCoverOne.text =  @"- -";
        self.lbl_holidayTypeOne.text =  @"- -";
        self.lbl_tourPrizeOne.text = @"- -";
    }else{
        [self.btn_cancelPackageTwo setTag:0];
        self.lbl_packageNameTwo.text = @"- -";
        self.txtview_CountriesCoverTwo.text =  @"- -";
        self.lbl_holidayTypeTwo.text =  @"- -";
        self.lbl_tourPrizeTwo.text = @"- -";
    }
    [self showHTMLStringArrayInTextView:[NSArray new] intxtViews:(index==0) ? self.txtview_AccomOne : self.txtview_AccomTwo];
    [self showStringArrayInTextView:[NSArray new] intxtViews:(index==0) ? self.txtview_MealOne : self.txtview_MealTwo];
    [self showStringArrayInTextView:[NSArray new] intxtViews:(index==0) ? self.txtview_SightOne : self.txtview_SightTwo];
    [self showHTMLStringArrayInTextView:[NSArray new] intxtViews:(index==0) ? self.txtview_InclusionOne : self.txtview_InclusionTwo];
    
}
-(void)showInWebViewsWith:(HolidayPackageDetail*)pkgDetail WithPackageDetail:(PackageDetailModel*)pkgDetailModel withIndex:(NSInteger)tagIndex{
    NSMutableArray *accomArray = [[NSMutableArray alloc]init];
    for (id object in pkgDetailModel.itineraryCollectionArray) {
        ItinenaryObject *itinery = [[ItinenaryObject alloc]initWithItinenaryDict:object];
        [accomArray addObject:itinery.itineraryDescription];
    }
    [self showHTMLStringArrayInTextView:[accomArray copy] intxtViews:(tagIndex==0) ? self.txtview_AccomOne : self.txtview_AccomTwo];
   
    NSMutableArray *mealArray = [[NSMutableArray alloc]init];
    for (id object in pkgDetailModel.mealCollectionArray) {
         MealsObject *mealObj = [[MealsObject alloc]initWithMealsObjectDict:object];
        [mealArray addObject:mealObj.mealName];
    }
    [self showStringArrayInTextView:[mealArray copy] intxtViews:(tagIndex==0) ? self.txtview_MealOne : self.txtview_MealTwo];
    
    NSMutableArray *sightArray = [[NSMutableArray alloc]init];
    for (id object in pkgDetailModel.sightseenCollectionArray) {
         SightSeenObject *sightObj = [[SightSeenObject alloc]initWithSightSeenObjectDict:object];
        [sightArray addObject:sightObj.name];
    }
    [self showStringArrayInTextView:[sightArray copy] intxtViews:(tagIndex==0) ? self.txtview_SightOne : self.txtview_SightTwo];
    
    
    NSMutableArray *inclusionArray = [[NSMutableArray alloc]init];
    for (id object in pkgDetailModel.includeexcludeCollectionArray) {
        IncludeExcludeObject *itinery = [[IncludeExcludeObject alloc]initWithIncludeExcludeDict:object];
        [inclusionArray addObject:itinery.includes];
    }
    [self showHTMLStringArrayInTextView:[inclusionArray copy] intxtViews:(tagIndex==0) ? self.txtview_InclusionOne : self.txtview_InclusionTwo];
    
    
}
-(void)showStringArrayInTextView:(NSArray*)stringArray intxtViews:(UITextView*)textView{
    if ([stringArray count]>0){
     textView.text = [stringArray componentsJoinedByString: @"\n"];
    }else{
        textView.text = @"- -";
    }
}
-(void)showHTMLStringArrayInTextView:(NSArray*)stringArray intxtViews:(UITextView*)textView{
    if ([stringArray count]>0){
    for (NSString *objectStr in stringArray) {
      [textView setValue:objectStr forKey:@"contentToHTMLString"];
    }}else{
       textView.text = @"- -";
    }
}
-(NSArray*)getCountriesCovered:(HolidayPackageDetail*)pkgDetail{
    NSMutableArray *cityArray = [[NSMutableArray alloc]init];
    NSArray *siteArray = [[NSArray alloc]init];
    siteArray = pkgDetail.timeLineList;
    NSSortDescriptor *brandDescriptor = [[NSSortDescriptor alloc] initWithKey:@"position" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:brandDescriptor];
    NSArray *sortedArray = [siteArray sortedArrayUsingDescriptors:sortDescriptors];
    for (int i=0; i<[sortedArray count]; i++) {
        NSMutableString *mutString = [[NSMutableString alloc]init];
        NSDictionary *siteNameDict = sortedArray[i];
        NSLog(@"%@",siteNameDict);
        NSDictionary *cityDict = [siteNameDict valueForKey:@"cityCode"];
        NSLog(@"%@",cityDict);
        NSDictionary *iconDict = [siteNameDict valueForKey:@"iconId"];
        NSLog(@"%@",iconDict);
        NSString *iconName = [iconDict valueForKey:@"iconName"];
        NSLog(@"%@",iconName);
        NSString *cityName = [cityDict valueForKey:@"cityName"];
        NSLog(@"%@",cityName);
        NSString *timeStr =[NSString stringWithFormat:@"%@",[siteNameDict valueForKey:@"noOfNights"]];
        NSLog(@"%@",timeStr);
        [mutString appendString:[NSString stringWithFormat:@"%@ (%@N)",cityName,timeStr]];
        [cityArray addObject:mutString];
    }
    return [cityArray copy];
}
-(void)setRoomDataModelWithIndex:(NSInteger)index{
    RoomsDataModel *roomModel = [roomRecordArray firstObject];
    roomModel.adultCount = 1;
    roomModel.infantCount = 0;
    roomModel.arrayChildrensData = 0;
    [roomRecordArray replaceObjectAtIndex:0 withObject:roomModel];
}

-(void)setPackageTypeForHoliday:(HolidayPackageDetail*)packageDetail atIndex:(NSInteger)index{
    [CoreUtility reloadRequestID];
    if([[packageDetail.strPackageSubType lowercaseString] isEqualToString:@"fit fixed"]|| [[packageDetail.strPackageSubType lowercaseString] isEqualToString:@"git"])
    {
        if([[packageDetail.strPackageType lowercaseString] isEqualToString:@"international"]){
            if (index == 0)
                packageTypeOne = kpackageTypeGITInternational;
            if (index == 1)
                packageTypeTwo = kpackageTypeGITInternational;
        }else{
            if (index == 0)
                packageTypeOne = kpackageTypeGITDomestic;
            if (index == 1)
                packageTypeTwo = kpackageTypeGITDomestic;
        }
    }else{
        if([[packageDetail.strPackageType lowercaseString] isEqualToString:@"international"]){
            if (index == 0)
                packageTypeOne = kpackageTypeFITInternational;
            if (index == 1)
                packageTypeTwo = kpackageTypeFITInternational;
        }
        else{
            if (index == 0)
                packageTypeOne = kpackageTypeFITDomestic;
            if (index == 1)
                packageTypeTwo = kpackageTypeFITDomestic;
        }
    }
    if(packageDetail.stringSelectedAccomType == nil || [packageDetail.stringSelectedAccomType isEqualToString:@""]){
        if (packageTypeArrayOne.count != 0){
            [self setAccomType:@"standard" forPackageDetail:packageDetail];
        }
        if (packageTypeArrayTwo.count != 0){
            [self setAccomType:@"standard" forPackageDetail:packageDetail];
        }
        
    }else{
        if ([packageDetail.stringSelectedAccomType isEqualToString:@"0"]){
            if (index == 0)
                accomTypeOne = 0;
            if (index == 1)
                accomTypeTwo = 0;
        }
        else if ([packageDetail.stringSelectedAccomType isEqualToString:@"1"]){
            if (index == 0)
                accomTypeOne = 1;
            if (index == 1)
                accomTypeTwo = 1;
        }
        else{
            if (index == 0)
                accomTypeOne = 2;
            if (index == 1)
                accomTypeTwo = 2;
        }
    }
}
-(void)setAccomType:(NSString *)accomTypeString forPackageDetail:(HolidayPackageDetail*)pkgDetail
{
    if ([[accomTypeString lowercaseString] isEqualToString:@"standard"]){
        pkgDetail.stringSelectedAccomType = @"0";
    }
    else if ([[accomTypeString lowercaseString] isEqualToString:@"delux"]||[[accomTypeString lowercaseString] isEqualToString:@"deluxe"]){
        pkgDetail.stringSelectedAccomType = @"1";
    }
    else{
        pkgDetail.stringSelectedAccomType = @"2";
    }
}
#pragma mark - Showing Images in UI

-(void)showImageUsingURL:(NSString*)imgString inImageView:(UIImageView*)imgView{
    NSURL* urlImage=[NSURL URLWithString:imgString];
    if(urlImage)
    {
        UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        indicator.center = imgView.center;// it will display in center of image view
        [imgView addSubview:indicator];
        [indicator startAnimating];
        [imgView sd_setImageWithURL:urlImage placeholderImage:[UIImage imageNamed:@"defaultBanner.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
            [indicator stopAnimating];
        }];
    }
}


-(NSArray*)getArrayOfCitiesFromPackage:(HolidayPackageDetail*)packageDetail packageTypes:(NSString*)packageType withAccomType:(int)accomType withTag:(NSInteger)tagID{
    
    NSArray *hubList = [NSArray new];
    if([packageType isEqualToString:kpackageTypeFITDomestic]||[packageType isEqualToString:kpackageTypeFITInternational]){
        hubList = packageDetail.arraytcilHolidayPriceCollection;
        
    }else{
        hubList = packageDetail.arrayLtPricingCollection;
    }
    NSArray *filteredarray = [[NSArray alloc] init];
    filteredarray = hubList;
    filteredarray = [hubList filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(packageClassId == %@)", [NSString stringWithFormat:@"%d",accomType]]];
    if (tagID==0){
        filteredarrayOne = filteredarray;}
    if (tagID==1){
        filteredarrayTwo = filteredarray;}
    NSArray* uniqueValues;
    if([packageType isEqualToString:kpackageTypeFITDomestic]||[packageType isEqualToString:kpackageTypeFITInternational]){
        uniqueValues = [filteredarray valueForKeyPath:[NSString stringWithFormat:@"@distinctUnionOfObjects.hubCityCode.cityName"]];
    }else{
        uniqueValues = [filteredarray valueForKeyPath:[NSString stringWithFormat:@"@distinctUnionOfObjects.hubCode.cityName"]];
    }
    NSMutableArray * uniqueDataArray = [[NSMutableArray alloc] init];
    for (NSString * city in uniqueValues)
    {
        NSExpression *lhs;
        
        if([packageType isEqualToString:kpackageTypeFITDomestic]||[packageType isEqualToString:kpackageTypeFITInternational]){
            lhs = [NSExpression expressionForKeyPath:@"hubCityCode.cityName"];
        }else{
            lhs = [NSExpression expressionForKeyPath:@"hubCode.cityName"];
        }
        NSExpression *rhs = [NSExpression expressionForConstantValue:city];
        NSPredicate * finalPredicate = [NSComparisonPredicate
                                        predicateWithLeftExpression:lhs
                                        rightExpression:rhs
                                        modifier:NSDirectPredicateModifier
                                        type:NSContainsPredicateOperatorType
                                        options:NSCaseInsensitivePredicateOption];
        
        NSArray * filteredChannelArray   = [[filteredarray filteredArrayUsingPredicate:finalPredicate] mutableCopy];
        
        if (filteredChannelArray.count > 0) {
            [uniqueDataArray addObject:[filteredChannelArray objectAtIndex:0]];
        }
    }
    NSSortDescriptor *nameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"hubCode.cityName"  ascending:YES];
    NSArray *sortDescriptors = @[nameDescriptor];
    
    NSArray * ordered = [uniqueDataArray sortedArrayUsingDescriptors:sortDescriptors];
    
    NSMutableArray *citiesAray = [[NSMutableArray alloc]init];
    for (int j =0 ; j < ordered.count; j++)
    {
        NSDictionary *cityDict = ordered[j];
        NSDictionary *hubCodeDict;
        if([packageType isEqualToString:kpackageTypeFITDomestic]||[packageType isEqualToString:kpackageTypeFITInternational]){
            hubCodeDict = [cityDict valueForKey:@"hubCityCode"];
        }else{
            hubCodeDict = [cityDict valueForKey:@"hubCode"];
        }
        NSString *titleString = [hubCodeDict valueForKey:@"cityName"];
        NSString *departFromLocal = [hubCodeDict valueForKey:@"cityCode"];
        NSString *ltCodeString = [cityDict valueForKey:@"ltItineraryCode"];
        
        if (tagID==0){
            departFromOne = departFromLocal;
            selectedLtItineraryCodeOne = ltCodeString;
//            firstHubCodeDict = hubCodeDict;
            firstHubCodeDict = cityDict;
        }
        if (tagID==1){
            departFromTwo = departFromLocal;
            selectedLtItineraryCodeTwo = ltCodeString;
//            secondHubCodeDict = hubCodeDict;
            secondHubCodeDict = cityDict;
        }
        [citiesAray addObject:titleString];
    }
    if ([citiesAray count]==0){
        [citiesAray addObject:@"Mumbai"];
    }
    return [citiesAray copy];
}

-(void)showCitiesArray:(HolidayPackageDetail*)packageDetail packageTypes:(NSString*)packageType withAccomType:(int)accomType withTag:(NSInteger)tagID{
    
    NSArray *hubList = [NSArray new];
    if([packageType isEqualToString:kpackageTypeFITDomestic]||[packageType isEqualToString:kpackageTypeFITInternational])
    {
        hubList = packageDetail.arraytcilHolidayPriceCollection;
        
    }
    else
    {
        hubList = packageDetail.arrayLtPricingCollection;
    }
    
    
    UIAlertController *durationActionSheet = [UIAlertController alertControllerWithTitle:@"Cities" message:@"Select your choice" preferredStyle:UIAlertControllerStyleActionSheet];
    NSArray *filteredarray;
    
    filteredarray = hubList;
    
    
    filteredarray = [hubList filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(packageClassId == %@)", [NSString stringWithFormat:@"%d",accomType]]];
//    NSString *classiD = [[self.lbl_toursTypesOne.text uppercaseString] isEqualToString:@"STANDARD"] ? @"0" : [[self.lbl_toursTypesOne.text uppercaseString] isEqualToString:@"DELUX"] ? @"1" : @"2";
//    filteredarray = [hubList filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(packageClassId == %@)", classiD]];
    
    NSArray* uniqueValues;
    
    if([packageType isEqualToString:kpackageTypeFITDomestic]||[packageType isEqualToString:kpackageTypeFITInternational])
    {
        
        uniqueValues = [filteredarray valueForKeyPath:[NSString stringWithFormat:@"@distinctUnionOfObjects.hubCityCode.cityName"]];
        
    }
    else
    {
        
        uniqueValues = [filteredarray valueForKeyPath:[NSString stringWithFormat:@"@distinctUnionOfObjects.hubCode.cityName"]];
        
    }
    
    
    NSMutableArray * uniqueDataArray = [[NSMutableArray alloc] init];
    
    for (NSString * city in uniqueValues)
    {
        
        NSExpression *lhs;
        
        if([packageType isEqualToString:kpackageTypeFITDomestic]||[packageType isEqualToString:kpackageTypeFITInternational])
        {
            
            lhs = [NSExpression expressionForKeyPath:@"hubCityCode.cityName"];
            
        }
        else
        {
            lhs = [NSExpression expressionForKeyPath:@"hubCode.cityName"];
            
        }
        
        NSExpression *rhs = [NSExpression expressionForConstantValue:city];
        NSPredicate * finalPredicate = [NSComparisonPredicate
                                        predicateWithLeftExpression:lhs
                                        rightExpression:rhs
                                        modifier:NSDirectPredicateModifier
                                        type:NSContainsPredicateOperatorType
                                        options:NSCaseInsensitivePredicateOption];
        
        NSArray * filteredChannelArray   = [[filteredarray filteredArrayUsingPredicate:finalPredicate] mutableCopy];
        
        
        if (filteredChannelArray.count > 0) {
            
            [uniqueDataArray addObject:[filteredChannelArray objectAtIndex:0]];
        }
        
        
    }
    
    NSSortDescriptor *nameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"hubCode.cityName"  ascending:YES];
    
    NSArray *sortDescriptors = @[nameDescriptor];
    
    NSArray * ordered = [uniqueDataArray sortedArrayUsingDescriptors:sortDescriptors];
    
    
    //        }
    
    for (int j =0 ; j < ordered.count; j++)
    {
        NSDictionary *cityDict = ordered[j];
        
        NSDictionary *hubCodeDict;
        if([packageType isEqualToString:kpackageTypeFITDomestic]||[packageType isEqualToString:kpackageTypeFITInternational])
        {
            hubCodeDict = [cityDict valueForKey:@"hubCityCode"];
            
        }
        else
        {
            hubCodeDict = [cityDict valueForKey:@"hubCode"];
            
        }
        
        
        NSString *titleString = [hubCodeDict valueForKey:@"cityName"];
        
        NSString *departFromLocal = [hubCodeDict valueForKey:@"cityCode"];
        
        NSString *ltCodeString = [cityDict valueForKey:@"ltItineraryCode"];
        
        
        UIAlertAction *action = [UIAlertAction actionWithTitle:titleString style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                 {
            if (tagID == 0){
                departFromOne = departFromLocal;
                self.lbl_departureCityOne.text = titleString;
                firstHubCodeDict = hubCodeDict;
                selectedLtItineraryCodeOne = ltCodeString;
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"hubCode.cityName contains[c] %@ ",titleString];
                NSArray *filteredData = [filteredarray filteredArrayUsingPredicate:predicate];
                ItineraryCodeArray = [[NSMutableArray alloc]init];
                NSLog(@"%@",filteredData);
                for(int i =0; i<filteredData.count;i++){
                    NSDictionary *cityDict = filteredData[i];
                    selectedLtItineraryCodeOne = [cityDict valueForKey:@"ltItineraryCode"];
                    [ItineraryCodeArray addObject:selectedLtItineraryCodeOne];
                }
            }else{
                departFromTwo = departFromLocal;
                self.lbl_departureCityTwo.text = titleString;
                secondHubCodeDict = hubCodeDict;
                selectedLtItineraryCodeTwo = ltCodeString;
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"hubCode.cityName contains[c] %@ ",titleString];
                NSArray *filteredData = [filteredarray filteredArrayUsingPredicate:predicate];
                ItineraryCodeArray = [[NSMutableArray alloc]init];
                NSLog(@"%@",filteredData);
                for(int i =0; i<filteredData.count;i++){
                    NSDictionary *cityDict = filteredData[i];
                    selectedLtItineraryCodeTwo = [cityDict valueForKey:@"ltItineraryCode"];
                    [ItineraryCodeArray addObject:selectedLtItineraryCodeTwo];
                }
            }
            
        }];
        [durationActionSheet addAction:action];
    }
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
        [self dismissViewControllerAnimated:durationActionSheet completion:nil];
    }];
    [durationActionSheet addAction:cancelAction];
    [self presentViewController:durationActionSheet animated:YES completion:nil];
    
}
-(void)setItinenaryObjectArray:(NSString*)titlstring forFilterDataArray:(NSArray*)filteredarray withTag:(NSInteger)index{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"hubCode.cityName contains[c] %@ ",titlstring];
    NSArray *filteredData = [filteredarray filteredArrayUsingPredicate:predicate];
    ItineraryCodeArray = [[NSMutableArray alloc]init];
    NSLog(@"%@",filteredData);
    for(int i =0; i<filteredData.count;i++){
        NSDictionary *cityDict = filteredData[i];
//        selectedLtItineraryCode = [cityDict valueForKey:@"ltItineraryCode"];
//        [ItineraryCodeArray addObject:selectedLtItineraryCode];
        NSString *itinenaryCodes = [cityDict valueForKey:@"ltItineraryCode"];
        [ItineraryCodeArray addObject:itinenaryCodes];
    }
}

//if (![packageType isEqualToString:kpackageTypeFITInternational] && ![packageType isEqualToString:kpackageTypeFITDomestic])
//{
//    [arrayForItinarayCode addObjectsFromArray:ItineraryCodeArray];
//}
//
//    NSMutableDictionary *jsonDict = [[NSMutableDictionary alloc] init];
//    [jsonDict setObject:arrayForItinarayCode forKey:@"ltItineraryCode"];
//    //[jsonDict setObject:packageDetail.ltMarket forKey:@"market"];
//    [jsonDict setObject:@"-1" forKey:@"market"];
//    [jsonDict setObject:departFrom forKey:@"hubCode"];
//    [jsonDict setObject:[NSNumber numberWithInteger:[self.holidayPkgDetailInPdpCalculate.strPackageSubTypeID integerValue]] forKey:@"pkgSubTypeId"];
//    [jsonDict setObject:self.self.holidayPkgDetailInPdpCalculate.stringSelectedAccomType forKey:@"pkgClassId"];
//    [jsonDict setObject:self.holidayPkgDetailInPdpCalculate.strPackageId forKey:@"pkgId"];
//    [jsonDict setObject:@"TCIL" forKey:@"mode"];
//    [jsonDict setObject:@"N" forKey:@"isHsa"];

-(void)showCalenderViewInCompareScreenForPackage:(HolidayPackageDetail*)packgeDetail withIndex:(NSInteger)pckageIndex{
    NSString *packageTypes = (pckageIndex==0) ? packageTypeOne : packageTypeTwo;
    int accomTypes = (pckageIndex==0) ? accomTypeOne : accomTypeTwo;
    NSString *departFrom= (pckageIndex==0) ? departFromOne : departFromTwo;
    if([CoreUtility connected]){

    @try
    {
        int totalChildCount = 0;
        int totalAdultCount = 0;
        
        for (int i = 0; i<roomRecordArray.count; i++) {
            RoomsDataModel *dataModel = roomRecordArray[i];
            totalAdultCount = totalAdultCount + dataModel.adultCount;
            totalChildCount = totalChildCount + (int)dataModel.arrayChildrensData.count;
        }
        NSMutableDictionary *dictOfData = [[NSMutableDictionary alloc]init];
        [dictOfData setValue:packgeDetail.strPackageId forKey:@"packageId"];
        [dictOfData setValue:packageTypes forKey:@"accomType"];
        [dictOfData setValue:[NSString stringWithFormat:@"%d",totalAdultCount+totalChildCount] forKey:@"noOfPax"];
        [dictOfData setValue:departFrom forKey:@"departFrom"];
        
            activityLoadingView = [LoadingView loadingViewInView:self.view.superview.superview
                                                      withString:@""
                                               andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
            
            
            NetworkHelper *helper = [NetworkHelper sharedHelper];
            NSMutableArray *arrayForItinarayCode = [[NSMutableArray alloc] init];
        
        if (![packageTypes isEqualToString:kpackageTypeFITInternational] && ![packageTypes isEqualToString:kpackageTypeFITDomestic]){
          //  [arrayForItinarayCode addObject:selectedLtItineraryCode];
            [arrayForItinarayCode addObjectsFromArray:ItineraryCodeArray];
        }
            NSMutableDictionary *jsonDict = [[NSMutableDictionary alloc] init];
            [jsonDict setObject:arrayForItinarayCode forKey:@"ltItineraryCode"];
            //[jsonDict setObject:packageDetail.ltMarket forKey:@"market"];
            [jsonDict setObject:@"-1" forKey:@"market"];
        
            [jsonDict setObject:departFrom forKey:@"hubCode"];
            [jsonDict setObject:[NSNumber numberWithInteger:[packgeDetail.strPackageSubTypeID integerValue]] forKey:@"pkgSubTypeId"];
        NSLog(@"stringSelectedAccomType %@",packgeDetail.stringSelectedAccomType);
        NSLog(@"strPackageId %@",packgeDetail.strPackageId);
//            [jsonDict setObject:packgeDetail.stringSelectedAccomType forKey:@"pkgClassId"];
        [jsonDict setObject:[NSString stringWithFormat:@"%i",accomTypes] forKey:@"pkgClassId"];
            [jsonDict setObject:packgeDetail.strPackageId forKey:@"pkgId"];
            [jsonDict setObject:@"TCIL" forKey:@"mode"];
            [jsonDict setObject:@"N" forKey:@"isHsa"];
        NSLog(@"CalenderjsonDict %@",jsonDict);
            NSDictionary *headerDict = [CoreUtility getHeaderDict];
            [helper getResponseWithRequestType:@"POST" withQueryParam:@"" withPathParam:@"" withJsonParam:jsonDict withHeaders:headerDict withUrl:kAstraUrlFareCalender success:^(NSDictionary *responseDict)
             {
                 NSLog(@"Response Dict : %@",responseDict);
                 dispatch_async(dispatch_get_main_queue(), ^(void)
                                {
                                    [activityLoadingView removeFromSuperview];
                                    if (responseDict){
                                        if (responseDict.count>0 && [[responseDict valueForKey:@"isDateAvialable"]boolValue]){
                                            [self jumpCalenderVCFromCompare:responseDict withHolidayPkg:packgeDetail andAccomType:accomTypes andDepartFrom:departFrom withIndex:(int)pckageIndex];
                                        }else{
                                            [super showAlertViewWithTitle:@"Alert" withMessage:@"Dates not available for this package."];
                                        }
                                    }else{
                                        [super showAlertViewWithTitle:@"Alert" withMessage:@"Dates not available for this package."];
                                    }
                                });
                 
             }
                                       failure:^(NSError *error)
             {dispatch_async(dispatch_get_main_queue(), ^(void)
                             {
                                 [activityLoadingView removeFromSuperview];
                                 [super showAlertViewWithTitle:@"Alert" withMessage:@"Dates not available for this package."];
                                 NSLog(@"Response Dict : %@",error);
                             });
                 
             }];
    }
    @catch (NSException *exception){
        NSLog(@"%@", exception.reason);
        [activityLoadingView removeFromSuperview];
    }
    @finally{
        NSLog(@"exception finally called");
        
    }
    }else{
        [super showAlertViewWithTitle:@"Alert" withMessage:kMessageNoInternet];
    }
    
}

-(void)jumpCalenderVCFromCompare:(NSDictionary*)responseDict withHolidayPkg:(HolidayPackageDetail*)pkgDetail andAccomType:(int)accomType andDepartFrom:(NSString*)departFromType withIndex:(int)index{
    /*
    CalenderScreenVC *controler = [[CalenderScreenVC alloc] initWithNibName:@"CalenderScreenVC" bundle:nil];
    controler.calenderDelgate = self;
    controler.dataDict = responseDict;
    controler.isMRP = [[pkgDetail.packageMRP lowercaseString] isEqualToString:@"true"] ? YES : NO;
    controler.holidayPackageDetailInCalender = pkgDetail;
    controler.arrayTravellerCalculation = travellerArrayForCalculation;
    controler.roomRecordArray = roomRecordArray;
    controler.hubName = departFromType;
    controler.packageId = pkgDetail.strPackageId;
    controler.accomType = accomType;
    controler.previosVCIndex = index;
    [self.navigationController pushViewController:controler animated:YES];
    */
    NSLog(@"firstPkgDetailDict %@",firstPkgDetailDict);
    NSLog(@"secondPkgDetailDict %@",secondPkgDetailDict);
    NSLog(@"firstHubCodeDict %@",firstHubCodeDict);
    NSLog(@"secondHubCodeDict %@",secondHubCodeDict);
    CalendarViewController *controler = [[CalendarViewController alloc] initWithNibName:@"CalendarViewController" bundle:nil];
    controler.pkgDict = (index == 0) ? firstPkgDetailDict:secondPkgDetailDict;
    controler.cityInfodict = (index == 0) ? firstHubCodeDict:secondHubCodeDict;
    controler.currentPackageClassId = (index == 0) ? selectPackaheClassIdOne:selectPackaheClassIdTwo;
    controler.delegateCompare = self;
    controler.isFromCompare = YES;
    jumpingIndex = index;
    [self.navigationController pushViewController:controler animated:YES];
    
}
-(void)selectedDateInCompareWithDateModel:(NSString *)dateModel{
    if (jumpingIndex==0){
        self.lbl_departureDateOne.text = dateModel;
    }else{
        self.lbl_departureDateTwo.text = dateModel;
    }
}
//-(void)selectedDate(dateModel: DayModel) {
//
//}
//func selectedDate(dateModel: DayModel) {
//    if dateModel.date != nil
//    {
//        self.pricingModel.pricingTourDate = AppUtility.convertDateToString(date: dateModel.date ?? Date.init(), dateFormat: "dd-MM-yyyy")
//        self.pricingModel.ltProdCode = dateModel.ltProdCode
//        self.pricingModel.ltItineraryCode = dateModel.ltItineraryCode
//        self.pricingModel.onReqeustDate = dateModel.onRequest
//        self.tableViewForPricing.reloadData()
//    }
//
//}
-(void)setSelectedDate:(NSDate *)date withVCIndex:(int)index{
    if (index==0){

        self.lbl_departureDateOne.text = ([[NSDate date] compare:date] == NSOrderedAscending) ? [[DateStringSingleton shared]getDateTimeStringFormat:@"dd-MM-yyyy" DateValue:date]:@"";
    }else if (index==1){

        self.lbl_departureDateTwo.text = ([[NSDate date] compare:date] == NSOrderedAscending) ? [[DateStringSingleton shared]getDateTimeStringFormat:@"dd-MM-yyyy" DateValue:date]:@"";
    }
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
- (IBAction)btn_BackPress:(id)sender {
    [self jumpToPreviousViewController];
}

@end
