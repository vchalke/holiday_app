//
//  ForexBuyPassangerViewController.swift
//  holidays
//
//  Created by ketan on 23/11/17.
//  Copyright © 2017 Mobicule. All rights reserved.
//

import UIKit

class ForexBuyPassangerViewController: ForexBaseViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,BuyForexTableViewCellDelegate,BuyForexTableViewHeaderDelegate
    
{
    @IBOutlet weak var buyForexTableView: UITableView!
    @IBOutlet var forexBuyPassengerView: UIView!
    @IBOutlet weak var buyForexButton: UIButton!
    @IBOutlet weak var labelTotalAmount: UILabel!
   // @IBOutlet weak var buyForexPassengerScrollView: UIScrollView!
     @IBOutlet weak var buyForexPassengerScrollView: TPKeyboardAvoidingScrollView!
    @IBOutlet weak var textfieldCustomerState: UITextField!
    @IBOutlet var passengerView: UIView!
    @IBOutlet weak var continueButton: UIButton!
    
    @IBOutlet weak var labelTopMsg: UILabel!
    @IBOutlet weak var textfieldBranchState: UITextField!
    @IBOutlet weak var heightConstraintOfStateBranchView: NSLayoutConstraint!
    @IBOutlet weak var heightConstraintOfBuyForexButtonView: NSLayoutConstraint!
    
    //height
    
    let largeProductViewHeight : NSInteger = 319
    let minimizedProductViewHeght : NSInteger = 40
    let tableviewHeaderHeight : NSInteger = 30
    let tableviewFooterHeight : NSInteger = 60
    let deleteButtonViewHeight : NSInteger = 40
    let minimizedTravellerViewHeight : NSInteger = 40
    let stateBranchViewHeight : NSInteger = 185
    
    var  count : NSInteger  = 1 ;
    var selectedSection : NSInteger = 0
    @IBOutlet weak var heightConstraintOfTableView: NSLayoutConstraint!
    var newProduct = -1
    var previousScreen : String = ""
    
    var singleTravellerLimit : NSInteger = 0
    var isCashproduct : Bool = false
    var overallTravellerLimit : NSInteger = 250000
    var travellerArray : [TravellerBO] = []
    var productArray : [NSDictionary] = []
    var currencyArray : [NSDictionary] = []
    var isShowingBuyForexView1 = true
    var buyForexBo : BuyForexBO = BuyForexBO.init()
    var arrayForCustomerState : [NSDictionary] = [];
    var arrayForBranch : [NSDictionary] = [];
    
    var isUnionTerritoryState : String = ""
    var gstStateCode : String = ""
    var gstBranchCode : String = ""
    var branchCode : String = ""
    var isUnionTerritoryBranch : String = ""
    var forFreeDelivery : NSInteger = 0
    var dollarRate : Float = 0.0
    
    @IBOutlet weak var activeTextField: UITextField?
    
    // MARK: Methods
    override func viewDidLoad()
    {
        Bundle.main.loadNibNamed("ForexBuyPassangerViewController", owner: self, options: nil)
        super.addViewInBaseView(childView: self.forexBuyPassengerView)
        super.setUpHeaderLabel(labelHeaderNameText: "Buy Forex")
        
        super.viewDidLoad()
        self.buyForexTableView.register(UINib(nibName: "BuyForexTableViewCell", bundle: nil), forCellReuseIdentifier:  "BuyForexTableCell")
        self.buyForexTableView.sectionHeaderHeight = 60
        self.buyForexTableView.sectionFooterHeight = 60
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ForexBuyPassangerViewController.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        self.buyForexTableView.isScrollEnabled = false
        self.buyForexPassengerScrollView.addSubview(self.passengerView)
        heightConstraintOfStateBranchView.constant = 0
        textfieldBranchState.delegate = self
        textfieldCustomerState.delegate = self
        continueButton.isHidden = true
        
        //fetch from server
        fetchProductList()
        fetchCustomerState()
        fetchCustomerBranch()
        
       
        
        if previousScreen == "Pending Transaction"
        {
            populateTravellersData()
        }
        else
        {
            createEmptyTravellers()
        }
        
        
        //KeyboardAvoiding.setAvoidingView(self.forexBuyPassengerView, withTriggerView: self.textfieldBranchState)
        //KeyboardAvoiding.setAvoidingView(self.forexBuyPassengerView, withTriggerView: self.textfieldCustomerState)
        
        self.addDoneButtonOnKeyboard()
    }
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle       = UIBarStyle.default
        let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(ForexBuyPassangerViewController.doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.textfieldCustomerState.inputAccessoryView = doneToolbar
        self.textfieldBranchState.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction()
    {
        self.textfieldCustomerState.resignFirstResponder()
        self.textfieldBranchState.resignFirstResponder()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.buyForexButton.layer.cornerRadius = 10
      //  self.setNotificationKeyboard()
    }
    override func viewDidLayoutSubviews()
    {
        super.setSubViewFrame(childView: self.forexBuyPassengerView)
        self.passengerView.frame = CGRect.init(x: 0, y: 0, width: self.buyForexPassengerScrollView.frame.size.width, height: self.buyForexPassengerScrollView.frame.size.height + 1000 + 120)
    }
    override func viewWillLayoutSubviews()
    {
        super.setSubViewFrame(childView: self.forexBuyPassengerView)
        
        self.passengerView.frame = CGRect.init(x: 0, y: 0, width: self.buyForexPassengerScrollView.frame.size.width, height: self.buyForexPassengerScrollView.frame.size.height + 1000 + 120)
    }
    
    override func backButtonClicked(buttonView:UIButton )
    {
        if !isShowingBuyForexView1
        {
            heightConstraintOfStateBranchView.constant = 0
            heightConstraintOfBuyForexButtonView.constant = 80
            isShowingBuyForexView1 = true
            buyForexTableView.reloadData()
            continueButton.isHidden = true
            labelTopMsg.text = "Please provide the currency requirement of each traveller"
        }
        else
        {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func populateTravellersData()
    {
        let productID : NSInteger = travellerArray[0].getProduct(atIndex: 0).productID!
        fetchCurrencyList(forProductID: productID,textfield: nil);
        //labelInfo.text = "Please confirm your order and continue(Click on traveller to edit)"
        heightConstraintOfStateBranchView.constant = CGFloat(stateBranchViewHeight)
        heightConstraintOfBuyForexButtonView.constant = 0
        heightConstraintOfTableView.constant = CGFloat(travellerArray.count * minimizedTravellerViewHeight)
        isShowingBuyForexView1 = false
        continueButton.isHidden = false
        selectedSection = -1
        labelTotalAmount.text = "\(getTotalTravellerAmount()) INR"
        setTableHeight()
        buyForexTableView.reloadData()
        self.view.layoutIfNeeded()
    }
    
    func createEmptyTravellers()
    {
        let noOfTraveller : NSInteger = (buyForexBo.buyForexOptionViewDetails!.noOfTraveller)
        //let i : NSInteger  = 10
        for _ in 0 ..< noOfTraveller
        {
            addTraveller()
        }
        selectedSection = 0
        heightConstraintOfTableView.constant = CGFloat((minimizedTravellerViewHeight * travellerArray.count) + 30 + (minimizedProductViewHeght * travellerArray[selectedSection].getProductCount()-1) + largeProductViewHeight + tableviewHeaderHeight)
        setTableHeight()
    }
    
    @objc func sectionTapped (sender: UITapGestureRecognizer)
    {
        let headerView : UIView = sender.view!
        if selectedSection == -1
        {             // if any traveller is deleted or minimized
            selectedSection = headerView.tag
            if(!isValidProduct())
            {
                heightConstraintOfTableView.constant = CGFloat((minimizedTravellerViewHeight * travellerArray.count) + 30 + tableviewHeaderHeight + (minimizedProductViewHeght * travellerArray[selectedSection].getProductCount()-1)+largeProductViewHeight)
            }
            else
            {
                heightConstraintOfTableView.constant = CGFloat((minimizedTravellerViewHeight * travellerArray.count) + (minimizedProductViewHeght * travellerArray[selectedSection].getProductCount() + tableviewHeaderHeight + tableviewFooterHeight) );
            }
            
            buyForexTableView.reloadData()
        }
        else if (selectedSection == headerView.tag && isValidProduct())   // close traveller view on tap of opened traveller view
        {
            heightConstraintOfTableView.constant = CGFloat(travellerArray.count * minimizedTravellerViewHeight)
            for i in 0..<travellerArray[selectedSection].getProductCount()
            {
                travellerArray[selectedSection].getProduct(atIndex: i).isUpdateButtonVisible = false
            }
            selectedSection = -1
            buyForexTableView.reloadData()
        }
        else
        {
            if(!isValidProduct())
            {
                //                if(travellerArray[selectedSection].getProductCount() == 1)
                //                {
               // showAlert(message: "Please add details to Traveller \(selectedSection + 1)" as NSString)
                //                }
                //                else
                //                {
                //                    travellerArray[selectedSection].deleteProduct(atIndex: travellerArray[selectedSection].getProductCount()-1)
                //                    selectedSection = headerView.tag
                //                    heightConstraintOfTableView.constant = CGFloat((minimizedTravellerViewHeight * travellerArray.count) + (minimizedProductViewHeght * travellerArray[selectedSection].getProductCount()-1) )
                //                    self.buyForexTableView.reloadData()
                //                }
            }
            else
            {
                travellerArray[selectedSection].changeProductStatus()
                selectedSection = headerView.tag
                if(travellerArray[selectedSection].getProductCount() == 1 && !isValidProductForEdit())  // handling for populated travellers - show large product view
                {
                    heightConstraintOfTableView.constant = CGFloat((minimizedTravellerViewHeight * travellerArray.count) + 30 + tableviewHeaderHeight + (minimizedProductViewHeght * travellerArray[selectedSection].getProductCount()-1)+largeProductViewHeight)
                    self.buyForexTableView.reloadData()
                }
                else
                {
                    if(isShowingBuyForexView1)
                    {
                        if(travellerArray[selectedSection].getProductCount() == 3)
                        {
                            heightConstraintOfTableView.constant = CGFloat((minimizedTravellerViewHeight * travellerArray.count) + (minimizedProductViewHeght * travellerArray[selectedSection].getProductCount()) + tableviewHeaderHeight )      // footer removed
                        }
                        else
                        {
                            heightConstraintOfTableView.constant = CGFloat((minimizedTravellerViewHeight * travellerArray.count) + (minimizedProductViewHeght * travellerArray[selectedSection].getProductCount() + tableviewHeaderHeight + tableviewFooterHeight) )     //with footer
                        }
                        
                    }
                    else
                    {
                        heightConstraintOfTableView.constant = CGFloat((minimizedTravellerViewHeight * travellerArray.count) + (minimizedProductViewHeght * travellerArray[selectedSection].getProductCount() + tableviewHeaderHeight + tableviewFooterHeight) );
                    }
                    
                    self.buyForexTableView.reloadData()
                }
            }
        }
        
    }
    
    func validateTraveller(index : NSInteger) -> Bool {
        for i in 0..<travellerArray[index].getProductCount()
        {
            
                let product : ProductBO = travellerArray[index].getProduct(atIndex: i)
                if product.productName.isEqual(to: "")
                {
                    showAlert(message: "Please add details to Traveller \(index+1)" as NSString)
                    return false
                }
                if product.currency.isEqual(to: "")
                {
                    showAlert(message: "Please add details to Traveller \(index+1)" as NSString)
                    return false
                }
                if product.fxAmount == 0
                {
                    showAlert(message:  "Please add details to Traveller \(index+1)" as NSString)
                    return false
                }
                if !(product.productName.contains("Multi")) &&
                    !(product.productName.contains("One"))
                {
                    if product.currency.contains("Japanese") || product.currency.contains("Thai")
                    {
                        if  product.fxAmount % 100 != 0
                        {
                            showAlert(message: "Please enter Fx Amount in multiples of 100 of Traveller \(index+1)" as NSString)
                            return false
                        }
                        
                    }
                    else
                    {
                        if  product.fxAmount % 50 != 0
                        {
                            showAlert(message: "Please enter Fx Amount in multiples of 50 of Traveller \(index+1)" as NSString)
                            return false
                        }
                    }
                    
                if travellerArray[index].getTravellerAmountInDollars() > overallTravellerLimit
                {
                    showAlert(message: "As per RBI norms, Total Forex amount per traveller cannot be more than 2,50,000 USD equivalent in a single financial year. Please revise the amount to proceed further")
                    return false
                }
                
                
            if  getCASHAmountOfTraveller(index: index) > singleTravellerLimit
            {
                showAlert(message: "As per RBI rules, only 3,000 USD or equivalent is allowed to be carries in CASH per traveller. Please change the amount.")
                return false

            }
                
            }
            
            if travellerArray[index].getTravellerAmountInDollars() > overallTravellerLimit
            {
                showAlert(message: "As per RBI norms, Total Forex amount per traveller cannot be more than 2,50,000 USD equivalent in a single financial year. Please revise the amount to proceed further")
                return false
            }
            
        }
        return true
    }
    
    func validateFieldsOfBuyForexPassengerView() -> Bool
    {
        for j in 0..<travellerArray.count
        {
           if  !validateTraveller(index: j)
           {
            return false
           }
        }
        
        
        return true
        
    }
    
    func validateFieldsOfBuyForexPassengerStateBranchView() -> Bool
    {
        if validateFieldsOfBuyForexPassengerView() == false
        {
            return false
        }
//        if(textfieldCustomerState.text?.isEmpty)!
//        {
//            showAlert(message: "Please Select Customer State")
//            return false
//        }
//        if(textfieldBranchState.text?.isEmpty)!
//        {
//            showAlert(message: "Please Select Branch City")
//            return false
//        }
        if (textfieldCustomerState.text?.count)! < 3
        {
            showAlert(message: "Please Select Valid Customer State")
            return false
        }
        if (textfieldBranchState.text?.count)! < 3
        {
            showAlert(message: "Please Select Valid Branch City")
            return false
        }
        return true
        
    }
    // MARK: fetch Branch
    
    func fetchCustomerBranch()
    {
        let pathParameter  : NSString = "/tcForexRS/generic/gstCity"  // Komal 4 Jul 2018
        ForexCommunicationManager.sharedInstance.execTask(pathParam:pathParameter , queryParam: "", requestType: "get", jsonDict:NSDictionary())
        {
            (status, response) in
                DispatchQueue.main.async {
            if(response != nil)
            {
                let jsonArray : NSArray = response as! NSArray
                //                for i in (0..<jsonArray.count)
                //                {
                //                    let dict : NSDictionary = jsonArray[i] as! NSDictionary
                //                    self.arrayForBranch.append(dict)
                //                }
                self.arrayForBranch = NSMutableArray.init(array: jsonArray) as! [NSDictionary];
            }
            else
            {
                self.showAlert(message: "Some error has occurred")
            }
            }
            
        }
        
    }
    // MARK: fetch state
    func fetchCustomerState()
    {
        let pathParameter  : NSString = "/tcForexRS/generic/gstState"
        ForexCommunicationManager.sharedInstance.execTask(pathParam:pathParameter , queryParam: "", requestType: "get", jsonDict:NSDictionary())
        {
            (status, response) in
            if(response != nil)
            {
                let jsonArray : NSArray = response as! NSArray
                //                for i in (0..<jsonArray.count)
                //                {
                //                    let dict : NSDictionary = jsonArray[i] as! NSDictionary
                //                    self.arrayForCustomerState.append(dict)
                //                }
                
                self.arrayForCustomerState = NSMutableArray.init(array: jsonArray)  as! [NSDictionary]
            }
            else
            {
                self.showAlert(message: "Some error has occurred")
            }
        }
        
    }
    func getGstBranchCode()
    {
        
        let pathParameter  : NSString = "/tcForexRS/generic/gstCity/\(self.getGstBranchCode)" as NSString
        
        ForexCommunicationManager.sharedInstance.execTask(pathParam:pathParameter , queryParam: "", requestType: "get", jsonDict:NSDictionary.init())
        {
            (status, response) in
            if(response != nil)
            {
                
            }
        }
        
    }
    
    
    func fetchProductList () -> Void
    {
        LoadingIndicatorView.show();
        ForexCommunicationManager.sharedInstance.execTask(pathParam: "/tcForexRS/generic/product/1", queryParam: "", requestType: "get", jsonDict:NSDictionary())
        {
            (status, response) in
            DispatchQueue.main.async { () -> Void in
                LoadingIndicatorView.hide()
           
            if( !status || response == nil)
            {
                self.showAlert(message: "Some error has occurred")
            }
            else
            {
                let jsonArray : NSArray = response as! NSArray
                for i in (0..<jsonArray.count)
                {
                    let dict : NSDictionary = jsonArray[i] as! NSDictionary
                    self.productArray.append(dict)
                }
            }
                 }
            
        }
    }
    
    func fetchCurrencyList (forProductID:NSInteger, textfield:UITextField?) -> Void
    {
        LoadingIndicatorView.show();
       
        let pathParameter  : NSString = "/tcForexRS/generic/roe/1/".appending(NSString.init(format: "%i", forProductID) as String ) as (String) as NSString
        ForexCommunicationManager.sharedInstance.execTask(pathParam:pathParameter , queryParam: "", requestType: "get", jsonDict:NSDictionary())
        {
            (status, response) in
            DispatchQueue.main.async { () -> Void in
                LoadingIndicatorView.hide()
           
            if(response != nil)
            {
                let jsonArray : NSArray = response as! NSArray
                for i in (0..<jsonArray.count)
                {
                    let dict : NSDictionary = jsonArray[i] as! NSDictionary
                    self.currencyArray.append(dict)
                }
                for dict in self.currencyArray
                {
                    let currencyname = dict.object(forKey: "currencyName")
                    
                    
                    let currencyCode : String = (dict.object(forKey: "currencyCode") as? String)!
                    if currencyCode.contains("USD")
                    {
                        let roe = dict.object(forKey: "roe") as! Float
                        self.dollarRate = roe;
                            if self.isCashproduct && self.singleTravellerLimit == 0
                            {
                        let inr : NSInteger = NSInteger(round( roe * 3000))
                        self.singleTravellerLimit = inr
                            }
                        self.forFreeDelivery = NSInteger(round( roe * 500))
                    }
                }
              
                    
                    if(textfield != nil)
                    {
                        let alertController : UIAlertController  = UIAlertController.init(title: "Currency", message: "Select Currency", preferredStyle:.actionSheet)
                        let product : ProductBO = self.travellerArray[self.selectedSection].getProduct(atIndex: textfield!.tag)
                        let indexPath : IndexPath = IndexPath.init(row: (textfield?.tag)!, section: self.selectedSection)
                        let cell : BuyForexTableViewCell = self.buyForexTableView.cellForRow(at: indexPath) as! BuyForexTableViewCell
                       
                            for dict in self.currencyArray
                            {
                                let currencyname = dict.object(forKey: "currencyName")
                                let  alertAction: UIAlertAction = UIAlertAction.init(title: currencyname as? String, style: .default, handler:
                                {
                                    (alert: UIAlertAction!) -> Void in
                                    textfield?.text = currencyname as? String
                                    cell.textFieldFXAmt.placeholder = dict.object(forKey: "currencyCode") as? String
                                    cell.textFieldFXAmt.text = ""
                                    cell.textFieldFXINRAmt.text = ""
                                    let isNotroAcc = dict.object(forKey: "isNostroAcc")
                                    
                                    product.setCurrency(currency: textfield!.text! as NSString)
                                    product.setroe(roe: dict.object(forKey: "roe") as! Float)
                                    product.setCurrencyId(currencyId: dict.object(forKey: "currencyId") as! NSInteger)
                                    product.setCurrencyCode(currencyCode : dict.object(forKey: "currencyCode")  as! NSString)
                                    product.isNostroAcc = isNotroAcc as? NSString
                                    self.travellerArray[self.selectedSection].replaceProduct(atIndex: (textfield?.tag)!, newProduct: product)
                                    cell.conversionLabel.text = "1 \(dict.object(forKey: "currencyCode") as! NSString) = \(product.getROE()) INR"
                                    cell.conversionLabel.isHidden = false
                                    // self.heightConstraintOfTableView.constant = CGFloat((self.minimizedTravellerViewHeight * self.travellerArray.count) + 45  + (self.minimizedProductViewHeght * self.travellerArray[self.selectedSection].getProductCount())+self.largeProductViewHeight)
                                    // self.setTableHeight()
                                    textfield!.endEditing(true)
                                    
                                })
                                
                                alertController.addAction(alertAction)
                            }
                            let  alertCancel: UIAlertAction = UIAlertAction.init(title: "Cancel" , style: .cancel, handler:
                            {
                                
                                (alert: UIAlertAction!) -> Void in
                                //textField.text = productname as? String
                            })
                            
                            alertController.addAction(alertCancel)
                        
                        self.present(alertController, animated:true, completion: nil)
                    }
                
            }
                
            else
            {
               
                    self.showAlert(message: "Some error has occured.")
                
            }
            }
            
        }
    }
    
    //MARK: textfield delegates
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        activeTextField=textField;
    }
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        activeTextField=nil;
    }

    // MARK: textfield methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        self.view.endEditing(true)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField == textfieldCustomerState || textField == textfieldBranchState
        {
            if textField == textfieldCustomerState
            {
                let totalString  = "\(textField.text ?? "")\(string)"
                
                if totalString.count > 2
                {
                    if !string.isEmpty
                    {
                        // let resultPredicate = NSPredicate(format: "stateName contains[c] %@", totalString)
                        //let filteredArray = arrayForCustomerState.filter { $0["name"] as! String == "Mark" }
                        textField.resignFirstResponder()
                        let filteredArray = arrayForCustomerState.filter
                        {
                            guard let dictionary = $0 as? [String: Any],
                                let name  = dictionary["stateName"] as? String else
                            {
                                return false
                            }
                            return name.localizedCaseInsensitiveContains(totalString)
                        }
                        
                        if filteredArray.isEmpty
                        {
                            showAlert(message: "Please Enter Valid Customer State")
                        }
                        else
                        {
                            let alertController : UIAlertController  = UIAlertController.init(title: "Customer State", message: "Select Customer State", preferredStyle:.actionSheet)
                            
                            for dict in filteredArray
                            {
                                let productname = dict.object(forKey: "stateName")
                                let isUnion = dict.object(forKey: "isUnionTerritory") as! String
                                let stateCode = dict.object(forKey: "gstStateCode")
                                let  alertAction: UIAlertAction = UIAlertAction.init(title: productname as? String, style: .default, handler:
                                {
                                    (alert: UIAlertAction!) -> Void in
                                    textField.text = productname as? String
                                    
                                    if(stateCode != nil)
                                    {
                                        self.gstStateCode = stateCode as! String
                                    }
                                    self.isUnionTerritoryState = isUnion ;
                                })
                                alertController.addAction(alertAction)
                            }
                            let alertCancel: UIAlertAction = UIAlertAction.init(title: "Cancel" , style: .cancel, handler:
                            {
                                (alert: UIAlertAction!) -> Void in
                                //textField.text = productname as? String
                            })
                            
                            alertController.addAction(alertCancel)
                            
                            self.present(alertController, animated:true, completion: nil)
                        }
                        
                    }
                }
                
            }
            else if textField == textfieldBranchState
            {
                let totalString  = "\(textField.text ?? "")\(string)"
                
                if totalString.count > 2
                {
                    if !string.isEmpty
                    {
                        textField.resignFirstResponder()
                        let filteredArray = arrayForBranch.filter
                        {
                            guard let dictionary = $0 as? [String: Any],
                                let name  = dictionary["cityName"] as? String else //branchName
                            {
                                return false
                            }
                            return name.localizedCaseInsensitiveContains(totalString)
                        }
                        
                        if filteredArray.isEmpty
                        {
                            showAlert(message: "Please Enter Valid Branch City")
                        }
                        else
                        {
                            let alertController : UIAlertController  = UIAlertController.init(title: "Customer Branch", message: "Select Customer Branch", preferredStyle:.actionSheet)
                            
                            for dict in filteredArray
                            {
                                let productname = dict.object(forKey: "cityName")
                                let branchCode = dict.object(forKey: "gstStateCode")
                                let gstbranchCode = dict.object(forKey: "cityCode")
                                let isUnion : NSString = dict.object(forKey: "isUnionTerritory") as! NSString
                                
                                let  alertAction: UIAlertAction = UIAlertAction.init(title: productname as? String, style: .default, handler:
                                {
                                    
                                    (alert: UIAlertAction!) -> Void in
                                    textField.text = productname as? String
                                    self.gstBranchCode = gstbranchCode as! String
                                    self.branchCode = branchCode as! String
                                    self.isUnionTerritoryBranch = isUnion as String
                                    self.view.endEditing(true)
                                    self.getGstBranchCode()
                                })
                                
                                alertController.addAction(alertAction)
                            }
                            
                            let  alertCancel: UIAlertAction = UIAlertAction.init(title: "Cancel" , style: .cancel, handler:
                            {
                                
                                (alert: UIAlertAction!) -> Void in
                                //textField.text = productname as? String
                            })
                            
                            alertController.addAction(alertCancel)
                            
                            self.present(alertController, animated:true, completion: nil)
                        }
                    }
                }
                
            }
        }
        else
        {
            let indexPath : IndexPath = IndexPath.init(row: textField.tag, section: self.selectedSection)
            let cell : BuyForexTableViewCell = self.buyForexTableView.cellForRow(at: indexPath) as! BuyForexTableViewCell
            
            
            if textField == cell.textFieldFXAmt
            {
                
                let textFieldText: NSString = (textField.text ?? "") as NSString
                
                let txtAfterUpdate = textFieldText.replacingCharacters(in: range, with: string)
                
                
                if txtAfterUpdate.count > 6
                {
                    return false
                }
                
            }
            if textField == cell.textFieldFXINRAmt {
                let textFieldText: NSString = (textField.text ?? "") as NSString
                
                let txtAfterUpdate = textFieldText.replacingCharacters(in: range, with: string)
                
                
                if txtAfterUpdate.count > 7
                {
                    return false
                }
            }
            
        }
        
        
        return true;
    }
    
    
    func textFieldProductNameBeginEditing(textField: UITextField)
    {
        let alertController : UIAlertController  = UIAlertController.init(title: "Product", message: "Select Product", preferredStyle:.actionSheet)
        textField.resignFirstResponder()
        self.view.endEditing(true)
        for dict in productArray
        {
            let productname = dict.object(forKey: "productName")
            let  alertAction: UIAlertAction = UIAlertAction.init(title: productname as? String, style: .default, handler:
            {
                (alert: UIAlertAction!) -> Void in
                textField.text = productname as? String
                let product : ProductBO = self.travellerArray[self.selectedSection].getProduct(atIndex: textField.tag)
                let indexPath : IndexPath = IndexPath.init(row: textField.tag, section: self.selectedSection)
                let cell : BuyForexTableViewCell = self.buyForexTableView.cellForRow(at: indexPath) as! BuyForexTableViewCell
                cell.textFieldCurrency.text = ""
                cell.textFieldFXAmt.placeholder = "FX Amount"
                cell.textFieldFXINRAmt.placeholder = "INR Amount"
                cell.textFieldFXAmt.text = ""
                cell.textFieldFXINRAmt.text = ""
                //cell.textFieldFXAmt.delegate = self
                // clear currency on change of product
                product.setProductId(productID: dict.object(forKey: "productId") as! NSInteger)
                product.setCurrency(currency: "")
                product.setFXAmount(fxAmount: 0)
                product.setINRAmount(inrAmount: 0)
                product.setProductName(productName: textField.text! as NSString)
                self.travellerArray[self.selectedSection].replaceProduct(atIndex: textField.tag, newProduct: product)
                
                
            })
            alertController.addAction(alertAction)
        }
        let  alertCancel: UIAlertAction = UIAlertAction.init(title: "Cancel" , style: .cancel, handler:
        {
            (alert: UIAlertAction!) -> Void in
            //textField.text = productname as? String
        })
        
        alertController.addAction(alertCancel)
        
        self.present(alertController, animated:true, completion: nil)
    }
    
    func textFieldCurrencyBeginEditing(textField: UITextField)
    {
        textField.resignFirstResponder()
        self.view.endEditing(true)
         self.currencyArray.removeAll()
        let indexPath : IndexPath = IndexPath.init(row:textField.tag, section: self.selectedSection)
        let cell : BuyForexTableViewCell = self.buyForexTableView.cellForRow(at: indexPath) as! BuyForexTableViewCell
        if cell.textFieldProduct.text == ""
        {
            self.showAlert(message: "Please select product")
        }
        else
        {
        let product : ProductBO = self.travellerArray[self.selectedSection].getProduct(atIndex: textField.tag)
            if product.productName == "Cash"
            {
                isCashproduct = true
            }
        fetchCurrencyList(forProductID: product.productID!, textfield: textField)
        }
    }
    
    @objc func textFieldFXAmountBeginEditing(textField: UITextField)
    {
        let indexPath : IndexPath = IndexPath.init(row: textField.tag, section: self.selectedSection)
        let cell : BuyForexTableViewCell = self.buyForexTableView.cellForRow(at: indexPath) as! BuyForexTableViewCell
        
        if(cell.textFieldCurrency.text == "")
        {
            showAlert(message: "Please select currency")
        }
        if textField.text != ""
        {
            let product : ProductBO = travellerArray[selectedSection].getProduct(atIndex: textField.tag)
            let fxamount : NSInteger = NSInteger.init( textField.text!)!
            let inrAmt =  Float.init(fxamount) * product.getROE()
           
            
            cell.textFieldFXINRAmt.text = "\( NSInteger.init(round(inrAmt)))"
            product.setFXAmount(fxAmount:fxamount)
            //cell.textFieldFXINRAmt.isEditing = false
            product.setINRAmount(inrAmount: NSInteger(inrAmt))
            travellerArray[selectedSection].productArray[textField.tag] = product
            // product.markAsOldProduct()
            // heightConstraintOfTableView.constant = CGFloat((minimizedTravellerViewHeight * travellerArray.count) + (minimizedProductViewHeght * travellerArray[selectedSection].getProductCount())+largeProductViewHeight + tableviewFooterHeight)
            self.labelTotalAmount.text = "\(getTotalTravellerAmount()) INR"
            
            setTableHeight()
            self.passengerView.frame = CGRect.init(x: 0, y: 0, width: Int(self.buyForexPassengerScrollView.frame.size.width), height: Int(buyForexPassengerScrollView.contentSize.height + 1000 ))
        }

        
    }

    @objc func textFieldFXAmountEditingChange(textField: UITextField)
    {
        let indexPath : IndexPath = IndexPath.init(row: textField.tag, section: self.selectedSection)
        let cell : BuyForexTableViewCell = self.buyForexTableView.cellForRow(at: indexPath) as! BuyForexTableViewCell
        
        if(cell.textFieldCurrency.text == "")
        {
            showAlert(message: "Please select currency")
        }
        else
        {
            let product : ProductBO = travellerArray[selectedSection].getProduct(atIndex: textField.tag)
            
            //            let textFieldText: NSString = (textField.text ?? "") as NSString
            //            let txtAfterUpdate = textFieldText.replacingCharacters(in: range, with: string)
            //
            if((textField.text) != "")
            {
                let fxamount : NSInteger = NSInteger.init( textField.text!)!
                let inrAmt =  Float.init(fxamount) * product.getROE()
                
                cell.textFieldFXINRAmt.text = "\( NSInteger.init(round(inrAmt)))"
                product.setFXAmount(fxAmount:fxamount)
                //cell.textFieldFXINRAmt.isEditing = false
                product.setINRAmount(inrAmount: NSInteger(inrAmt))
                product.amountInDollars = inrAmt/dollarRate
                printLog(inrAmt/dollarRate)
                travellerArray[selectedSection].productArray[textField.tag] = product
                // product.markAsOldProduct()
                // heightConstraintOfTableView.constant = CGFloat((minimizedTravellerViewHeight * travellerArray.count) + (minimizedProductViewHeght * travellerArray[selectedSection].getProductCount())+largeProductViewHeight + tableviewFooterHeight)
                self.labelTotalAmount.text = "\(getTotalTravellerAmount()) INR"
                
                setTableHeight()
                self.passengerView.frame = CGRect.init(x: 0, y: 0, width: Int(self.buyForexPassengerScrollView.frame.size.width), height: Int(buyForexPassengerScrollView.contentSize.height + 1000 ))
            }
            else
            {
                product.setFXAmount(fxAmount:0)
                product.setINRAmount(inrAmount: 0)
                cell.textFieldFXINRAmt.text = ""
            }
            travellerArray[selectedSection].replaceProduct(atIndex: textField.tag, newProduct: product)
        }
    }
    
    func textFieldINRAmountBeginEditing(textField: UITextField)
    {
        let indexPath : IndexPath = IndexPath.init(row: self.travellerArray[self.selectedSection].getProductCount()-1, section: self.selectedSection)
        let cell : BuyForexTableViewCell = self.buyForexTableView.cellForRow(at: indexPath) as! BuyForexTableViewCell
        
        if(cell.textFieldCurrency.text == "")
        {
            showAlert(message: "Please select currency")
        }
    }
    
    func textFieldINRAmountEditingChange(textField: UITextField)
    {
        let indexPath : IndexPath = IndexPath.init(row: self.travellerArray[self.selectedSection].getProductCount()-1, section: self.selectedSection)
        let cell : BuyForexTableViewCell = self.buyForexTableView.cellForRow(at: indexPath) as! BuyForexTableViewCell
        
        if(cell.textFieldCurrency.text == "")
        {
            showAlert(message: "Please select currency")
        }
        else
        {
            let product : ProductBO = travellerArray[selectedSection].getProduct(atIndex: textField.tag)
            //            let textFieldText: NSString = (textField.text ?? "") as NSString
            //            let txtAfterUpdate = textFieldText.replacingCharacters(in: range, with: string)
            
            if((textField.text) != "")
            {
                let fxamount : NSInteger = NSInteger.init( textField.text!)!
                let inrAmt =  Float.init(fxamount) / product.getROE()
                //cell.textFieldFXAmt.isEditing = false
                cell.textFieldFXAmt.text = "\(NSInteger(inrAmt))"
                product.setFXAmount(fxAmount:fxamount)
                product.setINRAmount(inrAmount: NSInteger(inrAmt))
                travellerArray[selectedSection].productArray[textField.tag] = product
                // product.markAsOldProduct()
                // heightConstraintOfTableView.constant = CGFloat((minimizedTravellerViewHeight * travellerArray.count) + (minimizedProductViewHeght * travellerArray[selectedSection].getProductCount())+largeProductViewHeight + tableviewFooterHeight)
                self.labelTotalAmount.text = "\(getTotalTravellerAmount()) INR"
                
                setTableHeight()
                self.passengerView.frame = CGRect.init(x: 0, y: 0, width: Int(self.buyForexPassengerScrollView.frame.size.width), height: Int(buyForexPassengerScrollView.contentSize.height + 1000 ))
            }
            else
            {
                product.setFXAmount(fxAmount:0)
                product.setINRAmount(inrAmount: 0)
                cell.textFieldFXAmt.text = ""
            }
            travellerArray[selectedSection].replaceProduct(atIndex: textField.tag, newProduct: product)
        }
    }
    
    func setTableHeight()
    {
        if isShowingBuyForexView1
        {
            self.passengerView.clipsToBounds = false
            self.buyForexPassengerScrollView.contentSize = CGSize.init(width: 0, height: heightConstraintOfTableView.constant + 125);
            self.passengerView.frame = CGRect.init(x: 0, y: 0, width: Int(self.buyForexPassengerScrollView.frame.size.width), height: Int(buyForexPassengerScrollView.contentSize.height + 1000 ))
            self.passengerView.setNeedsLayout()
        }
        else
        {
            self.passengerView.clipsToBounds = true
            self.buyForexPassengerScrollView.contentSize = CGSize.init(width: 0, height: heightConstraintOfTableView.constant + 310);
            self.passengerView.frame = CGRect.init(x: 0, y: 0, width: Int(self.buyForexPassengerScrollView.frame.size.width), height: Int(buyForexPassengerScrollView.contentSize.height + 1000))
        }
    }
    
    @objc func dismissKeyboard()
    {
        view.endEditing(true)
    }
       func getTotalTravellerAmount() -> NSInteger {
        var total : NSInteger = 0
        for i in 0..<travellerArray.count
        {
            total = total + travellerArray[i].getTravellerAmount()
        }
        return total
    }
    
    func getCASHAmountOfTraveller(index:NSInteger) -> NSInteger
    {
        var total : NSInteger = 0
        for i in 0..<travellerArray[index].getProductCount()
        {
            let product : ProductBO = travellerArray[index].getProduct(atIndex: i)
            if product.productName == "Cash"
            {
                total = total + product.inrAmount
            }
        }
        return total
    }
    
    func isValidProduct()->Bool
    {
        if selectedSection != -1
        {
        for i in 0..<travellerArray[selectedSection].getProductCount()
        {
            
            let product : ProductBO = travellerArray[selectedSection].getProduct(atIndex: i)
            if product.productName.isEqual(to: "")
            {
                showAlert(message: "Please select product")
                return false
            }
            if product.currency.isEqual(to: "")
            {
                showAlert(message: "Please select currency" as NSString)
                return false
            }
            if product.fxAmount == 0
            {
                showAlert(message: "Please enter amount ")
                return false
            }

            if !(product.productName.contains("Multi")) &&
                !(product.productName.contains("One"))
            {
                if product.currency.contains("Japanese") || product.currency.contains("Thai")
                {
                    if  product.fxAmount % 100 != 0
                    {
                        showAlert(message: "Please enter Fx Amount in multiples of 100")
                        return false
                    }
                    
                }
                else
                {
                    if  product.fxAmount % 50 != 0
                    {
                        showAlert(message: "Please enter Fx Amount in multiples of 50 " )
                        return false
                    }
                }
              

                if travellerArray[selectedSection].getTravellerAmountInDollars() > overallTravellerLimit
                {
                        showAlert(message: "As per RBI norms, Total Forex amount per traveller cannot be more than 2,50,000 USD equivalent in a single financial year. Please revise the amount to proceed further")
                    return false
                }
                
            if getCASHAmountOfTraveller(index: selectedSection) > singleTravellerLimit
            {
                showAlert(message: "As per RBI rules, only 3,000 USD or equivalent is allowed to be carries in CASH per traveller. Please change the amount.")
                return false
            }
            }
                

            if travellerArray[selectedSection].getTravellerAmountInDollars() > overallTravellerLimit
            {
                showAlert(message: "As per RBI norms, Total Forex amount per traveller cannot be more than 2,50,000 USD equivalent in a single financial year. Please revise the amount to proceed further")
                return false
            }
           
             product.markAsOldProduct()
        }
        }
      
        return true
    }
   
    func isValidProductForEdit()->Bool
    {
        if selectedSection != -1
        {
        let product : ProductBO = travellerArray[selectedSection].getProduct(atIndex: travellerArray[selectedSection].getProductCount()-1)
        if product.productName.isEqual(to: "")
        {
            return false
        }
        
        if product.currency.isEqual(to: "")
        {
            return false
        }
        if product.fxAmount == 0
        {
            return false
        }

        if !(product.productName.contains("Multi")) && !(product.productName.contains("One"))
        {
            if product.currency.contains("Japanese") || product.currency.contains("Thai")
            {
                if  product.fxAmount % 100 != 0 || product.fxAmount == 0
                {
                    
                    return false
                }
                
            }
            else
            {
                if  product.fxAmount % 50 != 0 || product.fxAmount == 0
                {
                    
                    return false
                }
            }
                if travellerArray[selectedSection].getTravellerAmountInDollars() > overallTravellerLimit
            {
                return false
            }
                
        if  getCASHAmountOfTraveller(index: selectedSection) > singleTravellerLimit
        {
            return false
        }
            }
            
        if travellerArray[selectedSection].getTravellerAmountInDollars() > overallTravellerLimit
        {
            return false
        }
       
        for i in 0..<travellerArray[selectedSection].getProductCount()
        {
            travellerArray[selectedSection].getProduct(atIndex: i).isUpdateButtonVisible = false
        }
        product.markAsOldProduct()
        }
        return true
    }
    
    func addTraveller()
    {
        let  firstEmptyProduct: ProductBO = ProductBO.init()
        let firsttraveller : TravellerBO = TravellerBO.init()
        firsttraveller.addProduct(product: firstEmptyProduct)
        travellerArray.append(firsttraveller)
    }
    
    @objc func addProduct(buttonView:UIButton )
    {
        if(isValidProduct())
        {
            selectedSection = buttonView.tag
            
            if(travellerArray[selectedSection].getProductCount() < 3)
            {
                travellerArray[selectedSection].changeProductStatus()
                travellerArray[selectedSection].addProduct(product: ProductBO.init(productName: "", currency: "", fxAmount: 0, inrAmount: 0))
                if travellerArray[selectedSection].getProductCount() == 3
                {
                    heightConstraintOfTableView.constant = CGFloat((minimizedTravellerViewHeight * travellerArray.count)  + tableviewHeaderHeight + (minimizedProductViewHeght * travellerArray[selectedSection].getProductCount()-1) + largeProductViewHeight   )        // handling for height of add product button
                }
                else
                {
                    heightConstraintOfTableView.constant = CGFloat((minimizedTravellerViewHeight * travellerArray.count) + tableviewFooterHeight + tableviewHeaderHeight + (minimizedProductViewHeght * travellerArray[selectedSection].getProductCount()-1) + largeProductViewHeight )
                }
                
                self.buyForexTableView.reloadData()
            }
            else
            {
                showAlert(message: "Cannot add more than 3 products")
            }
        }
        else
        {
            showAlert(message: "Fill product First")
        }
    }
    
    // MARK: tableview  delegate
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return travellerArray.count;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return travellerArray[section].getProductCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell :  BuyForexTableViewCell = tableView.dequeueReusableCell(withIdentifier: "BuyForexTableCell", for: indexPath) as! BuyForexTableViewCell
        
        //set data
        cell.selectionStyle = .none
        let product : ProductBO = travellerArray[indexPath.section].getProduct(atIndex: indexPath.row)
        cell.minimizedProductName.text = product.productName as String
        cell.textFieldProduct.text = product.productName as String
        cell.textFieldCurrency.text = product.currency as String
       // KeyboardAvoiding.setAvoidingView(cell, withTriggerView:cell.textFieldFXAmt)
        
        if(product.fxAmount != 0)
        {
            cell.textFieldFXAmt.text = NSString.init(format: "%i", product.fxAmount) as String
            cell.conversionLabel.text = "1 \(product.getCurrencyCode()) = \(product.getROE()) INR"
            cell.conversionLabel.isHidden = false
            cell.minimizedProductName.text = product.productName as String
            cell.labelFxAmount.text = "\(product.getFXAmount() as String) \(product.currencyCode as String)"
            cell.labelINRAmt.text = "\(product.getINRAmount()) INR"
        }
        else
        {
            cell.textFieldFXAmt.text = ""
            //            cell.textFieldFXAmt.delegate = self
        }
        
        if(product.inrAmount != 0)
        {
            cell.textFieldFXINRAmt.text = NSString.init(format: "%i", product.inrAmount) as String
        }
        else
        {
            cell.textFieldFXINRAmt.text = ""
        }
        
        // set textfield Delegate
        
        cell.cellDelegate = self as BuyForexTableViewCellDelegate
        cell.editButton.tag = indexPath.row
        cell.updateButton.tag = indexPath.row
        cell.labelProductNoInMinimiezedView.text = NSString.init(format:"%i" , indexPath.row + 1) as String
        cell.labelProductNoInLargeView.text = NSString.init(format: "%i", indexPath.row + 1) as String
        cell.labelProductNoInEditView.text = NSString.init(format: "Product %i", indexPath.row + 1) as String
        
        cell.textFieldFXAmt.tag = indexPath.row
        cell.textFieldFXINRAmt.tag = indexPath.row
        cell.textFieldCurrency.tag = indexPath.row
        cell.textFieldProduct.tag = indexPath.row
        
        
        
        //  cell.textFieldProduct.addTarget(self, action:  #selector(textFieldProductNameBeginEditing(textField:)), for: UIControlEvents.touchDown)
        // cell.textFieldCurrency.addTarget(self, action:  #selector(textFieldCurrencyBeginEditing(textField:)), for: UIControlEvents.touchDown)
        cell.textFieldFXAmt.delegate = self
        cell.textFieldFXINRAmt.delegate = self
        cell.textFieldFXAmt.addTarget(self, action:  #selector(textFieldFXAmountEditingChange(textField:)), for: UIControl.Event.editingChanged)
        cell.textFieldFXAmt.addTarget(self, action:  #selector(textFieldFXAmountBeginEditing(textField:)), for: UIControl.Event.editingDidBegin)
        
//        cell.textFieldFXINRAmt.addTarget(self, action: #selector(textFieldINRAmountEditingChange(textField:)), for: UIControlEvents.editingChanged)
//        cell.textFieldFXINRAmt.addTarget(self, action:  #selector(textFieldINRAmountBeginEditing(textField:)), for: UIControlEvents.editingDidBegin)
        
        //cell height
        
        if(selectedSection == indexPath.section)
        {
            if(product.isUpdateButtonVisible)
            {
                cell.heightConstraintOfTravellerView.constant = CGFloat(largeProductViewHeight)
                cell.travellerView.isHidden = false
                cell.minimizeView.isHidden = true
                cell.heightConstraintOfMinimizeView.constant = 0
                cell.updateButtonView.isHidden = false
                cell.heightConstraintOfDeleteButtonView.constant = CGFloat(deleteButtonViewHeight)
                cell.heightConstraintOfconversionView.constant = 30
                cell.conversionLabel.isHidden = false
                
            }
            else if(product.isNewProduct)
            {
                cell.heightConstraintOfTravellerView.constant = CGFloat(largeProductViewHeight)
                cell.travellerView.isHidden = false
                cell.minimizeView.isHidden = true
                cell.heightConstraintOfMinimizeView.constant = 0
                cell.updateButtonView.isHidden = true
                cell.heightConstraintOfDeleteButtonView.constant = 0
                cell.textFieldCurrency.text = ""
                cell.heightConstraintOfconversionView.constant = 30
                cell.conversionLabel.isHidden = true
            }
            else
            {
                cell.heightConstraintOfTravellerView.constant = 0
                cell.travellerView.isHidden = true
                cell.minimizeView.isHidden = false
                cell.heightConstraintOfMinimizeView.constant = CGFloat(minimizedProductViewHeght)
                cell.updateButtonView.isHidden = true
                cell.heightConstraintOfDeleteButtonView.constant = 0
                cell.conversionLabel.isHidden = true
                cell.heightConstraintOfconversionView.constant = 0
            }
        }
        else
        {
            cell.heightConstraintOfTravellerView.constant = 0
            cell.heightConstraintOfMinimizeView.constant = 0
            cell.travellerView.isHidden = true
            cell.minimizeView.isHidden = true
            cell.updateButtonView.isHidden = true
            cell.heightConstraintOfDeleteButtonView.constant = 0
            cell.heightConstraintOfconversionView.constant = 0
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        let product : ProductBO = travellerArray[indexPath.section].getProduct(atIndex: indexPath.row)
        if(selectedSection == indexPath.section)
        {
            if(product.isUpdateButtonVisible)
            {
                return CGFloat(largeProductViewHeight + 100 )
            }
                
            else if(!product.isNewProduct)
            {
                return CGFloat(minimizedProductViewHeght )
            }
            else if product.isNewProduct
            {
                return  CGFloat(largeProductViewHeight + 31)
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if isShowingBuyForexView1 == false
        {
            if(selectedSection == section && travellerArray[section].getProductCount() == 1 && !travellerArray[section].getProduct(atIndex: 0).isEmpty())
            {
                return 70
            }
        }
        if(selectedSection == section && travellerArray[section].getProductCount() == 1 && travellerArray[section].getProduct(atIndex: 0).isNewProduct)
        {
            return 40
        }
        if(selectedSection == section)
        {
            return 70
        }
        return CGFloat(minimizedTravellerViewHeight)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        if(selectedSection == section && travellerArray[section].getProductCount() < 3)
        {
            return CGFloat(tableviewFooterHeight)
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let cell =  Bundle.main.loadNibNamed("SectionHeaderViewCell", owner: self, options: nil)?.first as! SectionHeaderViewCell
        let tapR : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ForexBuyPassangerViewController.sectionTapped))
        tapR.delegate = self as? UIGestureRecognizerDelegate
        tapR.numberOfTapsRequired = 1
        tapR.numberOfTouchesRequired = 1
        cell.addGestureRecognizer(tapR)
        cell.labelTraveller.text = NSString.init(format: "Traveller %i", section+1) as String
        cell.tag = section;
        cell.headerDelegate = self
        if travellerArray.count == 1
        {
            cell.widthConstraintOfDeleteButton.constant = 0
        }
        else
        {
            cell.widthConstraintOfDeleteButton.constant = 25
            
        }
        if isShowingBuyForexView1
        {
            cell.labelTravellerAmount.isHidden = true
        }
        else
        {
            cell.labelTravellerAmount.isHidden = false
            cell.labelTravellerAmount.text = ("\(travellerArray[section].getTravellerAmount()) INR")
            
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
    {
        let view : UIView = UIView.init(frame: CGRect(x: 0, y: 0, width:UIScreen.main.bounds.width, height: 60))
        let button : UIButton = UIButton.init(frame:  CGRect(x:40, y: 10, width:UIScreen.main.bounds.width - 60, height: 30))
        button.setTitle("Add Another Currency / Product", for: UIControl.State.normal)
        button.addTarget(self, action:  #selector(addProduct(buttonView:)), for: .touchUpInside)
        view.backgroundColor = UIColor.white;
        button.backgroundColor = UIColor.darkGray
        button.setTitleColor(UIColor.white, for: UIControl.State.normal)
        button.titleLabel?.font =  UIFont(name: "System", size: 12)
        button.layer.cornerRadius = 15
        button.setTitleColor(UIColor.white, for: UIControl.State.normal)
        button.tag = section
        view.addSubview(button)
        return view
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        self.setTableHeight()
    }
    
    // MARK: tableview cell delegate
    func didPressINRHelpButton(_ tag: Int)
    {
        let popupVC: PopUpViewController = PopUpViewController(nibName: "PopUpViewController", bundle: nil)
        popupVC.msg = "Please enter FX amount in multiples of 50 only. In case of Baht and Japanese Yen enter multiples of of 100 only. You can buy a maximum of USD 3000 or equivalent in case of Currency Cash. You can buy the remaining amount as Forex Card (BPC)  or Travellers Cheque."
        popupVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(popupVC, animated: false, completion: nil)
    }
    func didPressProductNameHelpButton(_ tag: Int)
    {
        let popupVC: PopUpViewController = PopUpViewController(nibName: "PopUpViewController", bundle: nil)
        popupVC.msg = "Borderless Prepaid Multicurrency Travel cards from Thomas Cook India are a secure, convenient and easy way to carry money and make payments whenever you travel abroad. Borderless Prepaid Card allows you to load 8 currencies simultaneously and th balance of amount in any currency can be converted into another currency as and when required. Currency Cash is available in 26 leading currencies of the world at Thomas Cookk India. Foreign Currency Travellers Cheques are convenient and easy to use while traveling abroad and allows you the convenience of cash as well as purchases."
        popupVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(popupVC, animated: false, completion: nil)
    }
    func didPressEditButton(_ tag: Int)
    {
        if(!isValidProductForEdit() && travellerArray[selectedSection].getProductCount() == 1)
        {
            showAlert(message: "Fill product First")
        }
        else if (!isValidProductForEdit() && travellerArray[selectedSection].getProductCount() > 1)
        {
            travellerArray[selectedSection].deleteProduct(atIndex: travellerArray[selectedSection].getProductCount()-1)
        }
        travellerArray[selectedSection].changeStatusOfUpdateButton(atIndex: tag)
        travellerArray[selectedSection].changeProductStatus()
        heightConstraintOfTableView.constant = CGFloat((minimizedTravellerViewHeight * travellerArray.count) + 60 + tableviewFooterHeight + largeProductViewHeight  + tableviewHeaderHeight + (minimizedProductViewHeght * travellerArray[selectedSection].getProductCount())  )
        setTableHeight()
        self.buyForexTableView.reloadData()
    }
    
    func didPressUpdateButton(tag: Int)
    {
        if isValidProduct()
        {
            travellerArray[selectedSection].changeStatusOfUpdateButtonForAll()
            heightConstraintOfTableView.constant = CGFloat((minimizedTravellerViewHeight * travellerArray.count) + (minimizedProductViewHeght * travellerArray[selectedSection].getProductCount()) + tableviewHeaderHeight + tableviewFooterHeight)
            self.buyForexTableView.reloadData()
        }
    }
    
    func didPressDeleteProductButton(_ tag: Int)
    {
        if self.travellerArray[self.selectedSection].getProductCount() == 1 && self.travellerArray.count == 1
        {
                self.showAlert(message: "You can not delete last traveller.")
        }
        else
        {
            let alert = UIAlertController(title: "Alert", message: "Do you wish remove product?", preferredStyle: UIAlertController.Style.alert)
            let yesAction = UIAlertAction(title: "Yes", style: .default) { UIAlertAction in
                if self.travellerArray[self.selectedSection].getProductCount() == 1
                {
                    self.travellerArray.remove(at: tag)
                    self.heightConstraintOfTableView.constant = CGFloat((self.minimizedTravellerViewHeight * self.travellerArray.count))
                    self.selectedSection = -1
                }
                else
                {
                    self.travellerArray[self.selectedSection].deleteProduct(atIndex: tag)
                    self.heightConstraintOfTableView.constant = CGFloat((self.minimizedTravellerViewHeight * self.travellerArray.count) + (self.minimizedProductViewHeght * self.travellerArray[self.selectedSection].getProductCount()) + self.tableviewFooterHeight + 40)
                }
                
                self.labelTotalAmount.text = "\(self.getTotalTravellerAmount()) INR"
                self.buyForexTableView.reloadData()
                alert.dismiss(animated: true, completion: nil)
                
            }
            let noAction = UIAlertAction(title: "No", style: .cancel) { UIAlertAction in
                alert.dismiss(animated: true, completion: nil)
            }
            alert.addAction(noAction)
            alert.addAction(yesAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    // MARK: table header delegate
    
    func didPressDeleteTravellerButton(_ tag: Int)
    {
        if self.travellerArray.count == 1
        {
            self.showAlert(message: "You can not delete last traveller.")
        }
        else if selectedSection == tag || isValidProduct()
        {
            if selectedSection != -1
            {
            for i in 0..<travellerArray[selectedSection].getProductCount()
            {
                travellerArray[selectedSection].getProduct(atIndex: i).isUpdateButtonVisible = false
            }
            }
            let alert = UIAlertController(title: "Alert", message: "Do you want to remove the details of Traveller \(tag + 1) ?", preferredStyle: UIAlertController.Style.alert)
            let yesAction = UIAlertAction(title: "Yes", style: .default) { UIAlertAction in
                
                self.travellerArray.remove(at: tag)
                self.heightConstraintOfTableView.constant = CGFloat((self.minimizedTravellerViewHeight * self.travellerArray.count))
                self.selectedSection = -1
                self.buyForexTableView.reloadData()
                
                self.labelTotalAmount.text = "\(self.getTotalTravellerAmount()) INR"
                
                alert.dismiss(animated: true, completion: nil)
                
            }
            let noAction = UIAlertAction(title: "No", style: .cancel) { UIAlertAction in
                alert.dismiss(animated: true, completion: nil)
            }
            alert.addAction(noAction)
            alert.addAction(yesAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    // MARK: button action
    
    @IBAction func buyForexButtonClicked(_ sender: Any)
    {
        if validateFieldsOfBuyForexPassengerView()
        {
            labelTopMsg.text = "Please confirm your order and continue (Click on traveller to edit)"
            heightConstraintOfStateBranchView.constant = CGFloat(stateBranchViewHeight)
            heightConstraintOfBuyForexButtonView.constant = 0
            heightConstraintOfTableView.constant = CGFloat(travellerArray.count * minimizedTravellerViewHeight)
            isShowingBuyForexView1 = false
            continueButton.isHidden = false
            selectedSection = -1
            labelTotalAmount.text = "\(getTotalTravellerAmount()) INR"
            setTableHeight()
            buyForexTableView.reloadData()
            self.view.layoutIfNeeded()
        }
        
    }
    
    @IBAction func continueButtonClicked(_ sender: Any)
    {
        if validateFieldsOfBuyForexPassengerStateBranchView()
        {
            let buyForexPassengerBo : BuyForexPassengerInfoBO = BuyForexPassengerInfoBO.init(travellerArray: travellerArray as NSArray, stateOfCustomer: textfieldCustomerState.text! as NSString, branchOfCustomer: textfieldBranchState.text! as NSString, totalAmount:NSInteger(( labelTotalAmount.text! as NSString).intValue), stateCode: gstStateCode as NSString, branchCode: branchCode as NSString, isUnionTerritoryState: isUnionTerritoryState as NSString, isUnionTerritoryBranch : isUnionTerritoryBranch as NSString, gstBranchCode: gstBranchCode as NSString)
            buyForexPassengerBo.freeDeliveryCheckAMount = forFreeDelivery
            buyForexBo.addBuyForexPassengerInfo(buyForexPassengerInfo: buyForexPassengerBo)
            let forexPaymentVC : BuyForexPayemtViewController = BuyForexPayemtViewController.init(nibName: "ForexBaseViewController", bundle: nil)
            forexPaymentVC.buyForexBo = buyForexBo
            self.navigationController?.pushViewController(forexPaymentVC, animated: true)
        }
    }
    
    @IBAction func addTravellerButtonClicked(_ sender: Any)
    {
        if(travellerArray.count < 4)
        {
            if(selectedSection == -1)
            {
                selectedSection = (sender as AnyObject).tag
            }
            if(!validateTraveller(index: selectedSection))
            {
               // showAlert(message: "Please add details to Traveller \(selectedSection + 1)" as NSString)
            }
            else if (!isValidProductForEdit() && travellerArray[selectedSection].getProductCount() > 1)
            {
                travellerArray[selectedSection].deleteProduct(atIndex: travellerArray[selectedSection].getProductCount()-1)
            }
            else
            {
                selectedSection = travellerArray.count
                addTraveller()
                heightConstraintOfTableView.constant = CGFloat((minimizedTravellerViewHeight * travellerArray.count) + 50 + (minimizedProductViewHeght * travellerArray[selectedSection].getProductCount()-1)+largeProductViewHeight)
                self.buyForexTableView.reloadData()
            }
        }
        else
        {
            showAlert(message: "Cannot add more than 4 travellers")
        }
    }
    
    //MARK: Keyboard Handling Methods
    @objc func keyboardWasShown(notification: NSNotification)
    {
        var info = notification.userInfo!
        let keyboardSize = (info[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsets.init(top: 0.0, left: 0.0, bottom: keyboardSize!.height+10, right: 0.0)
        self.buyForexPassengerScrollView.contentInset = contentInsets
        self.buyForexPassengerScrollView.scrollIndicatorInsets = contentInsets
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        if let activeField = self.activeTextField
        {
            if (!aRect.contains(activeField.frame.origin))
            {
                self.buyForexPassengerScrollView.scrollRectToVisible(activeField.frame, animated: true)
            }
        }
    }
    // when keyboard hide reduce height of scroll view
    @objc func keyboardWillBeHidden(notification: NSNotification)
    {
        let contentInsets : UIEdgeInsets = UIEdgeInsets.init(top: 0.0, left: 0.0,bottom: 0.0, right: 0.0)
        self.buyForexPassengerScrollView.contentInset = contentInsets
        self.buyForexPassengerScrollView.scrollIndicatorInsets = contentInsets
        self.view.endEditing(true)
    }
    
    // Notification when keyboard show
    func setNotificationKeyboard ()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
}
