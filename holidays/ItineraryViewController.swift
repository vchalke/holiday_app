//
//  ItineraryViewController.swift
//  sotc-consumer-application
//
//  Created by Saurav Kumar on 09/08/17.
//  Copyright © 2017 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit

class ItineraryViewController: UIViewController,CAPSPageMenuDelegate {
    
    
    
    @IBOutlet var scrollView: UIScrollView!
    
    var tourDetails:Tour?
    var cityCovered:CityCovered?
    var pageMenu : CAPSPageMenu?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setPagerControl()
 
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.setRedNavigationBar()
        self.navigationController?.setnavigatiobBarTitle(title: Title.ITINERARY, viewController: self)
        self.navigationController?.setNagationBackButton(showRightButton: true, viewController: self)
         self.navigationController?.setItineraryRightButton(showRightButton: true, viewController: self)
        
        
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        
    
    }
    
    
    func backButtonClick() -> Void {
        
        
        self.navigationController?.popViewController(animated: true)
        
        self.navigationController?.setNavigationBarTranslucent()
        
       
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func setPagerControl() {
        
  
        let sortedItienearyDetails =  self.sortItinearyDetailsByDay()
        
        if sortedItienearyDetails.count > 0
        {
            
            let itineraryDetailsViewController:Array<ItineraryDetailsViewController> =  self.getItienaryDetailsVC(sortedItienearyDetails: sortedItienearyDetails)
            
            pageMenu = CAPSPageMenu(viewControllers: itineraryDetailsViewController, frame:CGRect(x: 0, y: 5, width: UIScreen.main.bounds.width, height:  self.view.frame.maxY), pageMenuOptions: [CAPSPageMenuOption.scrollMenuBackgroundColor(UIColor.white),CAPSPageMenuOption.addBottomMenuHairline(false),CAPSPageMenuOption.selectedMenuItemLabelColor(UIColor.white),CAPSPageMenuOption.unselectedMenuItemLabelColor(UIColor.black),CAPSPageMenuOption.menuItemWidth(40.0),CAPSPageMenuOption.menuHeight(40.0),CAPSPageMenuOption.menuItemCornerRadius(5.0),CAPSPageMenuOption.menuItemBackgroundColor(Constant.AppThemeColor),CAPSPageMenuOption.showMenuItemBackgroundColor(true),CAPSPageMenuOption.viewBackgroundColor(.white),CAPSPageMenuOption.addBottomMenuHairline(true),CAPSPageMenuOption.bottomMenuHairlineColor(UIColor.groupTableViewBackground)])
            
            pageMenu!.delegate = self
            
            
            let pagerViewContainner = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height:  self.view.frame.maxY))
            
              pagerViewContainner.addSubview((pageMenu?.view)!)
            
              self.view.addSubview(pagerViewContainner)
          
          
            var moveToPageIndex:Int = 0
            
            if self.cityCovered != nil
            {
                moveToPageIndex = self.getPagerIndex(sortedItienearyDetails: sortedItienearyDetails, cityCovered: self.cityCovered!)
                
                 pageMenu?.moveToPage(moveToPageIndex)
            }
       
   
        }
    }
    
 
    
    func sortItinearyDetailsByDay() ->Array<ItineraryDetail>  {
        
        if let itineraryDetail:Array<ItineraryDetail>  = tourDetails?.itineraryRelation?.allObjects as? Array<ItineraryDetail>
        {
            if itineraryDetail.count > 0
            {
                
                let sortedItienearyDetails = itineraryDetail.sorted(by: { (ItineraryDetail1, ItineraryDetail2) -> Bool in
                    
                    Int(ItineraryDetail1.itineraryDay!)! < Int(ItineraryDetail2.itineraryDay!)!
                    
                })
                
                return sortedItienearyDetails
            }
        }
        return []
    }
    
    func getItienaryDetailsVC(sortedItienearyDetails:Array<ItineraryDetail>) -> Array<ItineraryDetailsViewController> {
        
        var itienaryVcArray:Array<ItineraryDetailsViewController> = []
        
                for itienearyDetail in sortedItienearyDetails
                {
                    let itineraryDetailVC  = ItineraryDetailsViewController (nibName: "ItineraryDetailsViewController", bundle: nil)
                    
                    itineraryDetailVC.title = itienearyDetail.itineraryDay
                    
                    itineraryDetailVC.itienaryDetail = itienearyDetail
                    
                    itineraryDetailVC.tourDetails = self.tourDetails
                    
                    
                    let itienaryImages:Array<ItineraryImages>? = self.getItienaryImages(itineraryDetail: itienearyDetail)
                    
                    if((itienaryImages?.count)! > 0)
                    {
                        itineraryDetailVC.itinearyImages  = itienaryImages
                        
                      
                    }
                    
                    itienaryVcArray.append(itineraryDetailVC)
                    
                    
                }
                
                return itienaryVcArray
                
    }
    
    
    
    
    func getItienaryImages(itineraryDetail:ItineraryDetail) -> Array<ItineraryImages> {
        
        
        if let itienearyImages:Array<ItineraryImages> = tourDetails?.tourImagesRelation?.allObjects as?  Array<ItineraryImages>
        {
            if itienearyImages.count > 0
                
            {
                
                let itienaryImage = itienearyImages.filter(){$0.imageOrder == itineraryDetail.itineraryDay}
                
                return itienaryImage
            }
        }
        return []
        
    }
    
    
    func getPagerIndex(sortedItienearyDetails:Array<ItineraryDetail>,cityCovered:CityCovered) -> Int {
        
        let pagerIndex = sortedItienearyDetails.firstIndex { (itinearyDetails:ItineraryDetail) -> Bool in
            itinearyDetails.cityCode == cityCovered.cityCode
        }
        
        return pagerIndex ?? 0
    }
 
    
    func shareItineraryDocumentButtonClick()
    {
        
        
        let documentDictPath =  TourDocumentController.getTourFileDirectoryPath(bfnNumber: (tourDetails?.bfNumber)! )
        
        let  filePath = documentDictPath.itienearyTourPath + "/" + Constant.ItiearyDetails.ITIENRARY_DOC + ".pdf"
        let isFilePresent = TourDocumentController.isFileExistInDocumentDirectory(fileNamePath: filePath)
        
        if(isFilePresent.chkFile)
        {
            if let fileURL:NSURL = NSURL(fileURLWithPath: filePath)
            {
                
                /*if let fileData = NSData(contentsOfFile: filePath)
                 {*/
                self.presentShareView(activityItems: [fileURL])
                
            }
            
        }else{
        
            let itienearyDocUrl:Array<TourPassengerDouments> = self.getItienearyDocUrl()
            
            if itienearyDocUrl.count > 0
            {
                if let tourPassengerDouments:TourPassengerDouments = itienearyDocUrl.first
                {
                    let documentUrl:String = tourPassengerDouments.docummentURL ?? ""
            
                    self.downloadVisaRequirmentDocument(filePath: filePath, url: documentUrl)
                }
            }
        }
        
        
        
        // image to share
       
        
    }
    
    
    func presentShareView(activityItems: [Any])
    {
       
        let activityViewController = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        //activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    func downloadItineraryButtonClick()
    {
        let itienearyDocUrl:Array<TourPassengerDouments> = self.getItienearyDocUrl()
        
        if itienearyDocUrl.count > 0
        {
           if let tourPassengerDouments:TourPassengerDouments = itienearyDocUrl.first
            {
                let documentUrl:String = tourPassengerDouments.docummentURL ?? ""
                
              let documentDictPath =  TourDocumentController.getTourFileDirectoryPath(bfnNumber: tourPassengerDouments.bfNumber!)
               
             let  filePath = documentDictPath.itienearyTourPath + "/" + Constant.ItiearyDetails.ITIENRARY_DOC + ".pdf"
            
             //self.downloadDocument(filePath: filePath, url: documentUrl)
                
                self.downloadFreshDocument(filePath: filePath, url: documentUrl)
               
            }
        }
    }
    
    func getItienearyDocUrl() -> Array<TourPassengerDouments>
    {
        if let passengerDocuments:Array<TourPassengerDouments> =  self.tourDetails?.passengerDocumentRelation?.allObjects as? Array<TourPassengerDouments>
        {
            let filteredPassengerDoc:Array<TourPassengerDouments> = passengerDocuments.filter(){ $0.documentType == Constant.ItiearyDetails.ITIENRARY_DOC}
            
            return filteredPassengerDoc
            
            
        }
        return []
    }
    
    
    func downloadVisaRequirmentDocument(filePath:String ,url:String) {
        
            LoadingIndicatorView.show("Loading")
            
            NetworkCommunication.downloadDocuments(urlString: url, destinationFilePath: filePath, completion: { (downlodedfilePath, status) in
                
                printLog(status,filePath);
                
                // DispatchQueue.main.sync {
                
                LoadingIndicatorView.hide()
                
                // self.displayAlert()
                //}
                
                if(status)
                {
                    if let fileURL:NSURL = NSURL(fileURLWithPath: filePath)
                    {
                    
                    /*if let fileData = NSData(contentsOfFile: filePath)
                    {*/
                        self.presentShareView(activityItems: [fileURL])
                        
                    }
                    
                }
                else{
                    
                    AppUtility.displayAlert(message: AlertMessage.DOCUMENT_DOWNLOAD_FAIL)
                }
                
                
                
            })
      
    }
    
    
    func downloadDocument(filePath:String ,url:String) {
        
        let isFilePresent = TourDocumentController.isFileExistInDocumentDirectory(fileNamePath: filePath)
        
        if(isFilePresent.chkFile)
        {
             AppUtility.displayAlertController(title: "", message: AlertMessage.Itineary.FILE_ALREDAY_DOWNLOADED)
            
        }else{
            
            LoadingIndicatorView.show("Loading")
            
            NetworkCommunication.downloadDocuments(urlString: url, destinationFilePath: filePath, completion: { (downlodedfilePath, status) in
                
                printLog(status,filePath);
                
               // DispatchQueue.main.sync {
                    
                    LoadingIndicatorView.hide()
                    
                    // self.displayAlert()
                //}
                
                if(status)
                {
                    AppUtility.displayAlertController(title: "", message:  AlertMessage.Itineary.FILE_DOWNLOADED)
                    
                }
                else{
                    
                    AppUtility.displayAlert(message: AlertMessage.DOCUMENT_DOWNLOAD_FAIL)
                }
                
                
                
            })
        }
    }
    
    func downloadFreshDocument(filePath:String ,url:String) {
        
        LoadingIndicatorView.show("Loading")
        
        NetworkCommunication.downloadDocuments(urlString: url, destinationFilePath: filePath, completion: { (downlodedfilePath, status) in
            
            printLog(status,filePath);
            
            // DispatchQueue.main.sync {
            
            LoadingIndicatorView.hide()
            
            // self.displayAlert()
            //}
            
            if(status)
            {
                AppUtility.displayAlertController(title: "", message:  AlertMessage.Itineary.FILE_DOWNLOADED)
                
            }
            else{
                
                AppUtility.displayAlert(message: AlertMessage.DOCUMENT_DOWNLOAD_FAIL)
            }
            
            
            
        })
        
    }
    
    
    
}
