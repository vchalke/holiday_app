//
//  HolidayLandingSearchVC.m
//  holidays
//
//  Created by Kush_Tech on 19/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "HolidayLandingSearchVC.h"
#import "Reachability.h"
#import "AppDelegate.h"
#import "Holiday.h"
#import "HolidayPackageDetail.h"
#import "NetCoreAnalyticsVC.h"
#import "SRPScreenVC.h"
#import "searchTableViewCell.h"
#import "PDPScreenVC.h"


#define kEntity @"holidays"
#define kType   @"webservice"
#define kAction  @"search"

@interface HolidayLandingSearchVC ()
{
    KLCPopup *customePopUp;
    UITextField * textFieldSearch;
    LoadingView *activityIndicator;
    NSInteger totalNumberOfSections;
    NSMutableArray *destinationPlaceNameArray;
    NSMutableArray *packageCountArrayForCell;
    NSMutableArray *packageNameStringArray;
    NSMutableArray *packageNameStringIdArray;
    NSString *strResponseData;
    NSString *destinationStr;
    int packagearrayIndex;
    UILabel * noDataLbl ;
    
    int cellIndex;
    int flag;
    int ViewFlag;
    int ViewFlagForSearchIcon;
    NSString *strResponse;
    LoadingView *activityLoadingView;
    NSString *stringNoOfNights;
    NSString *stringBudgetPerson;
    int isFlagForInternetMsg;
    CGRect frameForBtnFind;
    NSMutableData *_responseData;
    NSMutableDictionary *payLoadForViewDetails;
    HolidayPackageDetail *packageDetail;
    NSArray *arrayForSearchResult;
    NSString *selecteDestinationDict;
    NSString *monthIndex;
    
}
@end

@implementation HolidayLandingSearchVC

- (void)viewDidLoad {
//    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.table_vieww registerNib:[UINib nibWithNibName:@"searchTableViewCell" bundle:nil] forCellReuseIdentifier:@"searchTableViewCell"];
    _boolForSearch_PackgScreen = true;
    CoreUtility *coreObj=[CoreUtility sharedclassname];
    coreObj.statusForNetPopUp=1;
    // Do any additional setup after loading the view from its nib.
    destinationPlaceNameArray=[[NSMutableArray alloc]init];
    packageCountArrayForCell=[[NSMutableArray alloc]init];
    packageNameStringArray=[[NSMutableArray alloc]init];
    packageNameStringIdArray=[[NSMutableArray alloc]init];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(applicationBecameActive:)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:nil];
    
    payLoadForViewDetails = [[NSMutableDictionary alloc] init];
}
-(void)viewWillAppear:(BOOL)animated{
    [self.serach_Text becomeFirstResponder];
}
- (void)viewDidAppear:(BOOL)animated{
    
}
- (void)applicationBecameActive:(NSNotification *)notification
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (appDelegate.currentDeepLink != nil)
    {
        // self.deepLinkLabel.text = appDelegate.currentDeepLink.lastPathComponent;
        if ([[notification name] isEqualToString:UIApplicationDidBecomeActiveNotification])
        {
            NSDictionary *appIndexingPathDict = [[NSDictionary alloc] init];
            appIndexingPathDict = notification.object;
        }
    }
    else
    {
        //  self.deepLinkLabel.text = @"No incoming link";
    }
    
    
    
}
- (BOOL)connected{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}

- (IBAction)btn_BackPress:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)callToSearchPackageScreen{
    packagearrayIndex=0;
    
    
    cellIndex=0;
    totalNumberOfSections=0;
    
    [destinationPlaceNameArray removeAllObjects];
    [packageCountArrayForCell removeAllObjects];
    [packageNameStringArray removeAllObjects];
    [packageNameStringIdArray removeAllObjects];
    [self.table_vieww reloadData];
    [self.serach_Text resignFirstResponder];
    
    
    CGRect frame;
    
    if (@available(iOS 11.0, *)) {
        UIWindow *window = UIApplication.sharedApplication.keyWindow;
        CGFloat topPadding = window.safeAreaInsets.top;
        //CGFloat bottomPadding = window.safeAreaInsets.bottom;
        frame = CGRectMake(80, topPadding + 4, 225, 40);
    }
    else{
        frame = CGRectMake(80, 4, 225, 40);
    }
    textFieldSearch = [[UITextField alloc] initWithFrame:frame];
    [textFieldSearch setTextColor:[UIColor whiteColor]];
    textFieldSearch.autocorrectionType = UITextAutocorrectionTypeNo;
    textFieldSearch.tag=5;
    
    UIColor *color = [UIColor whiteColor];
    textFieldSearch.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Enter destination name" attributes:@{NSForegroundColorAttributeName: color}];
    
    textFieldSearch.delegate=self;
    
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 2;
    border.borderColor = [UIColor whiteColor].CGColor;
    border.frame = CGRectMake(0, textFieldSearch.frame.size.height - borderWidth,textFieldSearch.frame.size.width, textFieldSearch.frame.size.height);
    border.borderWidth = borderWidth;
    [textFieldSearch.layer addSublayer:border];
    textFieldSearch.layer.masksToBounds = YES;
    [textFieldSearch becomeFirstResponder];
    
//    [super addViewInBaseView:self.searchTableView];
//    [textFieldSearch becomeFirstResponder];
//    super.HeaderLabel.hidden=YES;
//    [super.self.view addSubview:textFieldSearch];
//    _searchTable.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
}
#pragma mark- Text Field Delagets

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    return YES;
}

- (BOOL) textField: (UITextField *)theTextField shouldChangeCharactersInRange: (NSRange)range replacementString: (NSString *)string
{
    if ([self connected]) {
//    if (self.isNetworkPresent) {
        if (theTextField.tag==5)
        {
            NSString *dest = [theTextField.text stringByReplacingCharactersInRange:range withString:string];
            if([self connected]){
//            if (self.isNetworkPresent) {
                if (dest.length>=3)
                {
                    if ([self connected]){
//                    if (self.isNetworkPresent) {
                    
                        @try
                        {
                            
                            NetworkHelper *helper = [NetworkHelper sharedHelper];
                            
                            NSString *queryParam = [NSString stringWithFormat:@"searchAutoSuggest=%@",dest];
                            
                            NSString *stringWithDecode  = [queryParam stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];

                            
                            NSDictionary *headerDict = [CoreUtility getHeaderDict];
                            
                            [helper getResponseWithRequestType:@"" withQueryParam:stringWithDecode withPathParam:@"" withJsonParam:nil withHeaders:headerDict withUrl:kAstraUrlSearchPackage success:^(NSDictionary *responseDict)
                             {
                                 NSLog(@"Response Dict : %@",responseDict);
                                 
                                 if (responseDict)
                                 {
                                     if (responseDict.count>0)
                                     {
                                         NSArray *packageArray = (NSArray *)responseDict;
                                         
                                        NSPredicate *predicateString = [NSPredicate predicateWithFormat:@"%K != %@", @"searchType",@"Z_NAME"];
                                         
                                         NSArray *arrayTotalData = [NSMutableArray arrayWithArray:[packageArray filteredArrayUsingPredicate:predicateString]];
                                         
                                         [self doMyOperationsWithResponse:arrayTotalData];
                                         
                                     }
                                 }
                                 
                             }
                                                       failure:^(NSError *error)
                             {
                                 dispatch_async(dispatch_get_main_queue(), ^(void)
                                                {
                                                    [activityLoadingView removeFromSuperview];
                                                    [self showAlertViewWithTitle:@"Alert" withMessage:@"Some Error Occured"];
                                                    NSLog(@"Response Dict : %@",[error description]);
                                                });
                                 
                                 
                             }];
                            
                        }
                        @catch (NSException *exception)
                        {
                            NSLog(@"%@", exception.reason);
                            [activityLoadingView removeFromSuperview];
                        }
                        @finally
                        {
                            NSLog(@"exception finally called");
                        }
                        
                    }
                    
                }
                
            }
            
        }
        
    }else
    {
        { 
            CoreUtility *coreObj=[CoreUtility sharedclassname];
            [self.view endEditing:YES];
            if (coreObj.statusForNetPopUp==1)
            {
                coreObj.statusForNetPopUp=0;
                UIView *viewForNetDisconnected= [self popUpViewForInternetDiscoonected];
                
                customePopUp = [KLCPopup popupWithContentView:viewForNetDisconnected
                                                     showType:KLCPopupShowTypeGrowIn
                                                  dismissType:KLCPopupDismissTypeFadeOut
                                                     maskType:KLCPopupMaskTypeDimmed
                                     dismissOnBackgroundTouch:YES
                                        dismissOnContentTouch:NO];
                [customePopUp show];
                
                
                
            }
            
        }
        
        
    }
    
    NSString *currentString = [theTextField.text stringByReplacingCharactersInRange:range withString:string];
    int length = (int)[currentString length];
    
    
    
    return YES;
}

-(void)searchDestinationwithbanner:(UITextField *)theTextField{
    if ([self connected]) {
//    if (self.isNetworkPresent) {
        if (theTextField.tag==5)
        {
            NSString *dest = theTextField.text;
            if([self connected]){
//            if (self.isNetworkPresent) {
                if (dest.length>=3)
                {
                    if ([self connected]){
//                    if (self.isNetworkPresent) {
                        @try
                        {
                            
                            //                        NetworkHelper *helper = [NetworkHelper sharedHelper];
                            //                        NSDictionary *dictOfData = [NSDictionary dictionaryWithObjectsAndKeys:dest,kDestinationKey,nil];
                            //
                            //                        [helper getDataFromServerForType1:kType entity:@"packageDestination" action:kAction andUserJson:dictOfData withCompletionHandler:^(NSString *resultString) {
                            //
                            //                            if (resultString != nil) {
                            //
                            //                                [self doMyOperationsWithResponse:resultString];
                            //                            }
                            //                        }];
                            
                            NetworkHelper *helper = [NetworkHelper sharedHelper];
                            
                            NSString *queryParam = [NSString stringWithFormat:@"searchAutoSuggest=%@",dest];
                            
                            NSString *stringWithDecode  = [queryParam stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
                            
                            
                            NSDictionary *headerDict = [CoreUtility getHeaderDict];
                            
                            [helper getResponseWithRequestType:@"" withQueryParam:stringWithDecode withPathParam:@"" withJsonParam:nil withHeaders:headerDict withUrl:kAstraUrlSearchPackage success:^(NSDictionary *responseDict)
                             {
                                 NSLog(@"Response Dict : %@",responseDict);
                                 
                                 if (responseDict)
                                 {
                                     if (responseDict.count>0)
                                     {
                                         NSArray *packageArray = (NSArray *)responseDict;
                                         
                                         NSPredicate *predicateString = [NSPredicate predicateWithFormat:@"%K != %@", @"searchType",@"Z_NAME"];
                                         
                                         NSArray *arrayTotalData = [NSMutableArray arrayWithArray:[packageArray filteredArrayUsingPredicate:predicateString]];
                                         
                                         [self doMyOperationsWithResponse:arrayTotalData];
                                         
                                     }
                                 }
                                 
                             }
                                                       failure:^(NSError *error)
                             {
                                 dispatch_async(dispatch_get_main_queue(), ^(void)
                                                {
                                                    [activityLoadingView removeFromSuperview];
                                                    [super showAlertViewWithTitle:@"Alert" withMessage:@"some error occurred"];
                                                    NSLog(@"Response Dict : %@",[error description]);
                                                });
                                 
                                 
                             }];
                            
                        }
                        @catch (NSException *exception)
                        {
                            NSLog(@"%@", exception.reason);
                            [activityLoadingView removeFromSuperview];
                        }
                        @finally
                        {
                            NSLog(@"exception finally called");
                        }
                        
                    }
                    
                }
                
            }
            
        }
        
    }else
    {
        {
            
            CoreUtility *coreObj=[CoreUtility sharedclassname];
            [self.view endEditing:YES];
            if (coreObj.statusForNetPopUp==1)
            {
                coreObj.statusForNetPopUp=0;
                UIView *viewForNetDisconnected= [self popUpViewForInternetDiscoonected];
                
                customePopUp = [KLCPopup popupWithContentView:viewForNetDisconnected
                                                     showType:KLCPopupShowTypeGrowIn
                                                  dismissType:KLCPopupDismissTypeFadeOut
                                                     maskType:KLCPopupMaskTypeDimmed
                                     dismissOnBackgroundTouch:YES
                                        dismissOnContentTouch:NO];
                [customePopUp show];
                
                
                
            }
            
        }
        
        
    }
}


-(void) doMyOperationsWithResponse:(NSArray *)strResponseDat
{
    arrayForSearchResult = [[NSMutableArray alloc]init];
    arrayForSearchResult = strResponseDat;
    
    // NSArray *components = [strResponse componentsSeparatedByString:@"searchString"];
    
    totalNumberOfSections= arrayForSearchResult.count;
    
    [destinationPlaceNameArray removeAllObjects];
    [packageCountArrayForCell removeAllObjects];
    [packageNameStringArray removeAllObjects];
    [packageNameStringIdArray removeAllObjects];
    
    
    
    noDataLbl.frame  = CGRectMake(-150, -150, 300, 100) ;
    noDataLbl.text =  @"";
    
    
    for (int indexArray=0; indexArray<totalNumberOfSections; indexArray++)
    {
        
        NSDictionary *searchDict = [strResponseDat objectAtIndex:indexArray];
        
        [destinationPlaceNameArray insertObject:[searchDict valueForKey:@"searchString"] atIndex:indexArray];
    }
    
    //packageCountArrayForCell is for cell index
    for (int indexArray=0; indexArray<totalNumberOfSections; indexArray++)
    {
        
        NSDictionary *searchDict = [strResponseDat objectAtIndex:indexArray];
        
        NSInteger count=[[searchDict valueForKey:@"pkgnameIdMappingList"] count];
        
        [packageCountArrayForCell insertObject:[NSNumber numberWithInteger:count]
                                       atIndex:indexArray];
    }
    
    
    //        NSDictionary * data=[[NSDictionary alloc]init];
    //        data=[[[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"data"]valueForKey:@"destinationPlace"];
    packagearrayIndex=0;
    
    for (int i=0; i<[strResponseDat count]; i++)
    {
        
        NSDictionary *searchDict = [strResponseDat objectAtIndex:i];
        
        NSInteger countTotalPackage= [[searchDict valueForKey:@"pkgnameIdMappingList"]  count];
        
        NSString *destinationPlaceName = [searchDict valueForKey:@"searchString"];
        
        NSArray *packageDetailArray = [searchDict valueForKey:@"pkgnameIdMappingList"];
        
        for (NSInteger packageIndex=0; packageIndex<countTotalPackage; packageIndex++)
        {
            
            NSDictionary * packageDetailDict = [packageDetailArray objectAtIndex:packageIndex];
            
            NSString *str= [packageDetailDict valueForKey:@"packageName"];
            
            [packageNameStringArray  insertObject:str atIndex:packagearrayIndex];
            
            NSString *strPackageId= [packageDetailDict valueForKey:@"packageId"];
            
            [packageNameStringIdArray  insertObject:strPackageId atIndex:packagearrayIndex];
            
            packagearrayIndex++;
        }
        
        [FIRAnalytics logEventWithName:@"Search" parameters:@{kFIRParameterItemID:destinationPlaceName}];
        [FIRAnalytics logEventWithName:@"HomePage_Find_holiday_Search" parameters:@{kFIRParameterItemName:[NSString stringWithFormat:@"DestinationPlaceName :%@",destinationPlaceName]}];
        
        NSString *destinationType;
        
        if([_header_Name caseInsensitiveCompare:@"International Holidays"]==NSOrderedSame)
        {
            destinationType = @"International";
            
            [FIRAnalytics logEventWithName:@"HomePage_Find_holiday_Search" parameters:@{kFIRParameterItemName:[NSString stringWithFormat:@"DestinationType :%@",destinationType]}];
        }
        else if ([_header_Name caseInsensitiveCompare:@"Indian Holidays"]==NSOrderedSame)
        {
            destinationType = @"Domestic";
            
            [FIRAnalytics logEventWithName:@"HomePage_Find_holiday_Search" parameters:@{kFIRParameterItemName:[NSString stringWithFormat:@"DestinationType :%@",destinationType]}];
        }
    }
    
    dispatch_async(dispatch_get_main_queue(), ^(void)
                   {
                       [self.table_vieww reloadData];
                   });
    
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    // A response has been received, this is where we initialize the instance var you created
    // so that we can append data to it in the didReceiveData method
    // Furthermore, this method is called each time there is a redirect so reinitializing it
    // also serves to clear it
    _responseData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    // Append the new data to the instance variable you declared
    [_responseData appendData:data];
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    // Return nil to indicate not necessary to store a cached response for this connection
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    // The request is complete and data has been received
    // You can parse the stuff in your instance variable now
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // The request has failed for some reason!
    // Check the error var
}


#pragma mark - Table View Delgate

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    searchTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"searchTableIdentifier"];
    if (cell == nil)
    {
        
        
        NSArray *inb= [[NSBundle mainBundle] loadNibNamed:@"searchTableCell" owner:self options:nil];
        
        cell=[inb objectAtIndex:0];
        
        
        
    }
    
    
    
    
    //  cell.nameLabel.text = [[[[[[[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"data"] valueForKey:@"destinationPlace"] objectAtIndex:(indexPath.section)-totalNumberOfSections] valueForKey:@"packageDetails"] objectAtIndex:indexPath.row] valueForKey:@"packageName"];
    
    // cell.nameLabel.text=[NSString stringWithFormat:@"%@ %@",@"  ", strCell];
    // cell.nameLabel.text=[packageNameStringArray objectAtIndex:0];
    
    NSDictionary *packageDetailDict =  [arrayForSearchResult objectAtIndex:(indexPath.section)-totalNumberOfSections];
    
    cell.nameLabel.text =  [[[packageDetailDict valueForKey:@"pkgnameIdMappingList"] objectAtIndex:indexPath.row] valueForKey:@"packageName"];
    
    cell.tag = indexPath.row;
    cellIndex++;
    if (cellIndex==[packageNameStringArray count])
    {
        cellIndex=(int)indexPath.row;
    }
    return  cell;
    
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath

{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        
        [cell setSeparatorInset:UIEdgeInsetsZero];
        
    }
    
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        
        [cell setPreservesSuperviewLayoutMargins:NO];
        
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        
        [cell setLayoutMargins:UIEdgeInsetsZero];
        
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    cellIndex=0;
    
    
    return (totalNumberOfSections*2);
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.0f;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    if (section==0) {
        return 88.0f;
    }
    if(section==totalNumberOfSections)
        return 88.0f;
    return 44.0f;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if(section<totalNumberOfSections)
        return 0;
    
    NSInteger returnRowValue= [[packageCountArrayForCell objectAtIndex:section-totalNumberOfSections]integerValue];
    
    if (returnRowValue > 4)
    {
        return 4;
    }
    else
    {
        return returnRowValue;
    }
    
    return returnRowValue;
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSLog(@"section %ld",(long)section);
    
    UIView* customView=[[UIView alloc]init];
    UILabel * headerLabel;
    UIView * separator=[[UIView alloc]init];
    UIButton *headerButton = [[UIButton alloc]init];
    // 2 view set for section 0.. 1st for set Destinations text and other for set destinationName.
    if (section==0 || section==totalNumberOfSections ) {
        
        [customView setFrame:CGRectMake(0.0, 0.0, tableView.bounds.size.width, 88.0)];
        customView.backgroundColor = [UIColor whiteColor];
        UIView* customViewHeaderName=[[UIView alloc]initWithFrame:CGRectMake(0.0, 0.0, tableView.bounds.size.width, 44.0)];
        
        customViewHeaderName.backgroundColor = [UIColor colorWithRed:230.0/256.0 green:245.0/256.0 blue:248.0/256.0 alpha:1.0];
        [separator setFrame:CGRectMake(0.0, 87.0, tableView.bounds.size.width, 1)];
        UIView * separatorForHeader = [[UIView alloc] initWithFrame:CGRectMake(0.0, 44.0, tableView.bounds.size.width, 1)];
        
        separator.backgroundColor = [UIColor colorWithRed:170/256.0 green:170/256.0 blue:170/256.0 alpha:1.0];
        
        separatorForHeader.backgroundColor=[UIColor colorWithRed:170/256.0 green:170/256.0 blue:170/256.0 alpha:1.0];
        
        [customView addSubview:separator];
        [customViewHeaderName addSubview:separatorForHeader];
        [customView addSubview:customViewHeaderName];
        
        headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(10,40, tableView.bounds.size.width, 44.0)];
        
        //UILabel *headerLabelName = [[UILabel alloc] initWithFrame:CGRectMake(tableView.bounds.size.width-300, 7.0, tableView.bounds.size.width, 44.0)];
        
        UILabel *headerLabelName = [[UILabel alloc] initWithFrame:CGRectMake(10.0f, 7.0f, tableView.bounds.size.width, 44.0f)];
        
        [headerLabelName setFont:[UIFont fontWithName:TITILLUM_REGULAR size:14]];
        
        if (section==0) {
            headerLabelName.text=@"Destinations:";
        }
        else{
            headerLabelName.text=@"Top Packages:";
            
        }
        [customViewHeaderName addSubview:headerLabelName];
        //set button to section 0
        if (section==0) {
            headerButton.frame=CGRectMake(10, 44, tableView.bounds.size.width, 44.0);
        }
        
    }
    else{
        [customView setFrame:CGRectMake(0.0, 0.0, tableView.bounds.size.width, 44.0)];
        
        customView.backgroundColor = [UIColor whiteColor];
        if (section>totalNumberOfSections) {
            [separator setFrame:CGRectMake(0.0, 0.0, tableView.bounds.size.width, 1)];
            
            separator.backgroundColor =[UIColor colorWithRed:170/256.0 green:170/256.0 blue:170/256.0 alpha:1.0];
            
            [customView addSubview:separator];
        }
        
        
        UIView * separator1 = [[UIView alloc] initWithFrame:CGRectMake(0.0, 43.0, tableView.bounds.size.width, 1)];
        
        separator1.backgroundColor = [UIColor colorWithRed:170/256.0 green:170/256.0 blue:170/256.0 alpha:1.0];
        
        
        [customView addSubview:separator1];
        
        headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, tableView.bounds.size.width, 44.0)];
        
        if (section<totalNumberOfSections) {
            headerButton.frame=CGRectMake(10, 0, tableView.bounds.size.width, 44.0);
        }
        
        
    }
    
    headerLabel.textColor = [UIColor darkGrayColor];
    headerLabel.font = [UIFont boldSystemFontOfSize:16];
    [headerLabel setFont:[UIFont fontWithName:TITILLUM_REGULAR size:16]];
    
    if (section<totalNumberOfSections) {
        
        headerLabel.textColor = [UIColor colorFromHexString:DEFAULT_COLOUR_CODE];
        NSString *headerName = [NSString stringWithFormat:@"%@ %@ %@",@"All", [destinationPlaceNameArray objectAtIndex:section], @"Holidays"];
        if (section==0 || section==totalNumberOfSections ){
            
            UIImageView *imageView =[[UIImageView alloc]initWithFrame:CGRectMake(tableView.bounds.size.width-35, 55, 10, 20
                                                                                 )];
            imageView.image=[UIImage imageNamed:@"right_arrow"];
            [customView addSubview:imageView];
            
        }
        else{
            UIImageView *imageView =[[UIImageView alloc]initWithFrame:CGRectMake(tableView.bounds.size.width-35, 15, 10, 20)];
            imageView.image=[UIImage imageNamed:@"right_arrow"];
            [customView addSubview:imageView];
        }
        headerLabel.text = headerName;//[destinationPlaceNameArray objectAtIndex:section];
        
    }
    else{
        
        headerLabel.textColor = [UIColor colorFromHexString:DEFAULT_COLOUR_CODE];
        NSString *headerName = [NSString stringWithFormat:@"%@ %@",@"Top Packages for", [destinationPlaceNameArray objectAtIndex:section-totalNumberOfSections]];
        
        headerLabel.text = headerName;
        
    }
    
    headerButton.tag = section;
    
    [headerButton   addTarget:self action:@selector(enableCellReordering:) forControlEvents:UIControlEventTouchUpInside];
    [customView addSubview:headerLabel];
    [customView addSubview:headerButton];
    return customView;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"strResponse %@",strResponse);
    
    NSDictionary *packageDetailDict =  [arrayForSearchResult objectAtIndex:(indexPath.section)-totalNumberOfSections];
    
    NSString *pckgName =  [[[packageDetailDict valueForKey:@"pkgnameIdMappingList"] objectAtIndex:indexPath.row] valueForKey:@"packageName"];
    
    NSString *packgId =  [[[packageDetailDict valueForKey:@"pkgnameIdMappingList"] objectAtIndex:indexPath.row] valueForKey:@"packageId"];
    
    
    // NSString *pckgName=  [[[[[[[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"data"] valueForKey:@"destinationPlace"] objectAtIndex:(indexPath.section)-totalNumberOfSections] valueForKey:@"packageDetails"] objectAtIndex:indexPath.row] valueForKey:@"packageName"];
    
    //  NSString *packgId =[[[[[[[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"data"] valueForKey:@"destinationPlace"] objectAtIndex:(indexPath.section)-totalNumberOfSections] valueForKey:@"packageDetails"] objectAtIndex:indexPath.row] valueForKey:@"packageId"];
    
    
    Holiday *objHoliday=[[Holiday alloc]init];
    objHoliday.strPackageId=packgId;
    objHoliday.strPackageName=pckgName;
    
    [self netCoreDataDisplaySearch];
    [self fetchPackageDetails:objHoliday];
    
    
    
}


-(void)enableCellReordering:(UIButton *)sender{
    ViewFlag=0;
    
    // arrayForSearchResult
    
    NSInteger   buttonTag=sender.tag;
    destinationStr=[destinationPlaceNameArray objectAtIndex:buttonTag];
    
    selecteDestinationDict = [arrayForSearchResult objectAtIndex:buttonTag];
    
    if(buttonTag<totalNumberOfSections)
    {
        
        if (_boolForSearch_PackgScreen==true)
        {
             NSString *str=[NSString stringWithFormat:@"%@ %@ %@",@"All",destinationStr,@"Holidays"];
             self.serach_Text.text=str;
            [self searchDestination];
//            [self searchDestinationSelectDict:selecteDestinationDict withText:self.serach_Text.text];
        }
        else{
            [self.serach_Text nextResponder];
            NSString *str=[NSString stringWithFormat:@"%@ %@ %@",@"All",destinationStr,@"Holidays"];
            [textFieldSearch removeFromSuperview];

//            [_searchTableView removeFromSuperview];
            self.serach_Text.text=str;
            [self.serach_Text resignFirstResponder];
            //   self.txtDestination.textColor=[UIColor redColor];

//            super.self.HeaderLabel.hidden=NO;
        }
        
    }
}



#pragma mark- Fetch Package Detail Action

-(void)fetchPackageDetails:(Holiday *)objHoliday
{
    
    activityLoadingView = [LoadingView loadingViewInView:self.view.superview.superview
                                              withString:@""
                                       andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
    
    
    @try
    {
        
        
        NetworkHelper *helper = [NetworkHelper sharedHelper];
        
        
        NSString *pathParam = objHoliday.strPackageId;
        
        
        NSDictionary *headerDict = [CoreUtility getHeaderDict];
        
        [helper getResponseWithRequestType:@"" withQueryParam:@"" withPathParam:pathParam withJsonParam:nil withHeaders:headerDict withUrl:kAstraUrlPackageDetail success:^(NSDictionary *responseDict)
         {
             NSLog(@"Response Dict : %@",responseDict);
             
             
             dispatch_async(dispatch_get_main_queue(), ^(void) {
                 [activityLoadingView removeFromSuperview];
                 if (responseDict)
                 {
                     if (responseDict.count>0)
                     {
                         NSArray *packageArray = (NSArray *)responseDict;
                         NSDictionary *dictForCompletePackage = [packageArray objectAtIndex:0];
                         NSDictionary *packageDetail = [dictForCompletePackage objectForKey:@"packageDetail"];
                         long productId = [[packageDetail valueForKey:@"productId"] longValue];
                         // Unbloacked on 01/09/20
//                         if (productId != 11) {
                             HolidayPackageDetail *packageDetailHoliday = [[HolidayPackageDetail alloc] initWithDataDict:dictForCompletePackage];
                             PDPScreenVC *controller = [[PDPScreenVC alloc] initWithNibName:@"PDPScreenVC" bundle:nil];
                             controller.isFromHomePage = true;
                             controller.headerName = objHoliday.strPackageName;
                             controller.holidayPackageDetailInPdp = packageDetailHoliday;
                             controller.holidayObjInPdp = objHoliday;
                             controller.completePackageDetail = [NSArray arrayWithObjects:dictForCompletePackage, nil];
                             [self.navigationController pushViewController:controller animated:YES];
                             /*
                             
                             TabMenuVC *tabMenuVC = [[TabMenuVC alloc]initWithNibName:@"BaseViewController" bundle:nil];
                             
                             tabMenuVC.headerName = objHoliday.strPackageName;
                             
                             tabMenuVC.flag = @"Overview";
                             tabMenuVC.packageDetail = packageDetailHoliday;
                             [self fireAppIntHoViewDetailsEvent:packageDetailHoliday];
                             tabMenuVC.completePackageDetail = [NSArray arrayWithObjects:dictForCompletePackage, nil];
                             [[SlideNavigationController sharedInstance] pushViewController:tabMenuVC animated:YES];
                             
                             */
//                         }else{
//                             [super showAlertViewWithTitle:@"Alert" withMessage:@"Selected package not availble"];
//                         }
                         
                         
                         
                     }
                 }
             });
             
         }
                                   failure:^(NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^(void)
                            {
                                [activityLoadingView removeFromSuperview];
                                [super showAlertViewWithTitle:@"Alert" withMessage:@"some error occurred"];
                                NSLog(@"Response Dict : %@",[error description]);
                            });
             
             
         }];
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"%@", exception.reason);
        [activityLoadingView removeFromSuperview];
    }
    @finally
    {
        NSLog(@"exception finally called");
    }
    
}

-(void)showAlertViewWithTitle:(NSString *)title withMessage:(NSString *)message
{
    UIAlertController *alertView = [UIAlertController alertControllerWithTitle:title
                                                                       message:message
                                                                preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [alertView dismissViewControllerAnimated:YES completion:nil];
        [self searchWithOnlyDestination];
    }];
    
    [alertView addAction:okAction];
    
    [self presentViewController:alertView animated:YES completion:nil];
}


#pragma mark - Button Action methods
/*
- (IBAction)clickFindHolidays:(id)sender
{
    
    NSString *destinationType;
    if ([_header_Name caseInsensitiveCompare:@"International Holidays"]==NSOrderedSame)
    {
        destinationType = @"International";
        [FIRAnalytics logEventWithName:@"International_Holiday_Find_Holiday" parameters:@{kFIRParameterItemID :[NSString stringWithFormat:@"DestinationType :%@,DestinationName %@ ",destinationType,self.txtDestination.text]}];
        
    }
    if ([_header_Name caseInsensitiveCompare:@"Indian Holidays"]==NSOrderedSame)
    {
        destinationType = @"Indian";
        [FIRAnalytics logEventWithName:@"Indian_Holiday_Find_Holiday" parameters:@{kFIRParameterItemID :[NSString stringWithFormat:@"DestinationType :%@,DestinationName %@ ",destinationType,self.txtDestination.text]}];
    }
    
    
    CoreUtility * coreObj=[CoreUtility sharedclassname];
    if (coreObj.strPhoneNumber.count>0)
    {
//        if (self.txtDestination.text.length==0) {
//            [super showAlertViewWithTitle:@"Alert" withMessage:@"Please enter valid destination"]; // commented by komal 10 Aug 2018
//        }
//        else
//        {
            if( [super connected])
            {
                [self searchDestination];
            }
            else
            {
                
                [self showNetNotConnectedPopUp];
                
            }
//        }
    }
    else
    {
//        if (self.txtDestination.text.length==0) {
//            [super showAlertViewWithTitle:@"Alert" withMessage:@"Please enter valid destination"]; // commented by komal 10 Aug 2018
//        }
//        else
//        {
//
            if (![self.txtMobileNo.text isEqualToString:@""])
            {
                if([self validatePhoneNumber]==YES)
                {
                    NSString * phoneNo=self.txtMobileNo.text;
                    [coreObj.strPhoneNumber addObject:phoneNo];
                    frameForBtnFind=_btnFindHoliday.frame;
                    
                    
                    
                   [[NetCoreSharedManager sharedInstance] setUpIdentity:phoneNo];
                    
                    [[NSUserDefaults standardUserDefaults] setValue:phoneNo forKey:@"Holidays_TextField_MobicleNumber"];
                    
                    [[NSUserDefaults standardUserDefaults] synchronize];// 21/04/2018
                    
                    //                NSMutableDictionary *dictOfData = [[NSMutableDictionary alloc]init];
                    //                [dictOfData setValue:self.packageDetail.strPackageId forKey:@"package_id"];
                    //                [dictOfData setValue:@"" forKey:@"email"];
                    //                [dictOfData setValue:self.txtViewMobileNumber.text forKey:@"mobile"];
                    //                [dictOfData setValue:self.packageDetail.strPackageSubType forKey:@"sub_product_type"];
                    //                [dictOfData setValue:self.packageDetail.strPackageType forKey:@"request_type"];
                    //
                    //
                    //                activityLoadingView = [LoadingView loadingViewInView:self.view.superview.superview
                    //                                                          withString:@""
                    //                                                   andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
                    //
                    //                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    //                    NetworkHelper *helper = [NetworkHelper sharedHelper];
                    //                    NSString *strResponse = [helper getDataFromServerForType:@"lead" entity:@"holidays" action:@"submit" andUserJson:dictOfData];
                    //                    NSString *strStatus = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"status"];
                    //                    dispatch_async(dispatch_get_main_queue(), ^{
                    //                        [activityLoadingView removeFromSuperview];
                    //                        if ([strStatus isEqualToString:kStatusSuccess])
                    //                        {
                    //
                    //                        }
                    //                    });
                    //                });
                    
                    
                    if( [super connected])
                    {
                        [self searchDestination];
                    }else
                    {
                        
                        [self showNetNotConnectedPopUp];
                        
                    }
                }
                else
                {
                    [super showAlertViewWithTitle:@"Alert" withMessage:@"Enter Valid mobile number"];
                }
            }else
            {
                if( [super connected]){
                    [self searchDestination];
                }else
                {
                    
                    [self showNetNotConnectedPopUp];
                    
                }
            }
            
//        }
        
        [self netCoreDataDisplaySearch];
        
        //   NSString *strDestination = self.txtDestination.text;
    }
}


- (IBAction)btnNumberOfNightsClicked:(id)sender {
    
    
    NSArray *nextMonthArray = [self nextYearMonths];
    
    UIAlertController *monthsActionSheet = [UIAlertController alertControllerWithTitle:@"Months"
                                                                               message:@"Select your choice"
                                                                        preferredStyle:UIAlertControllerStyleActionSheet];
    
    for (int localMonthIndex = 0 ; localMonthIndex < nextMonthArray.count; localMonthIndex++)
    {
        NSString *titleString = nextMonthArray[localMonthIndex];
        
        UIAlertAction *action = [UIAlertAction actionWithTitle:titleString style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                 {
                                     
                                     self.txtNights.text = nextMonthArray[localMonthIndex];
                                     
                                     NSString *strMonth = nextMonthArray[localMonthIndex];
                                     
                                     NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
                                     [formatter setDateFormat:@"MMMM yyyy"];
                                     
                                     NSDate *dateCurrent = [formatter dateFromString:strMonth];
                                     [formatter setDateFormat:@"MM-yyyy"];
                                     
                                     monthIndex = [formatter stringFromDate:dateCurrent];
                                     
                                 }];
        
        [monthsActionSheet addAction:action];
    }
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
    {
        self.txtNights.text = @"";
        monthIndex = 0;
        
        [self dismissViewControllerAnimated:monthsActionSheet completion:nil];
    }];
    
    [monthsActionSheet addAction:cancelAction];
    [self.view.window.rootViewController presentViewController:monthsActionSheet animated:YES completion:nil];
    
    
    //    UIView *viewPopUpNightCount =   [self popUpViewForNumberOfNights];
    //
    //    customePopUp = [KLCPopup popupWithContentView:viewPopUpNightCount
    //                                         showType:KLCPopupShowTypeGrowIn
    //                                      dismissType:KLCPopupDismissTypeFadeOut
    //                                         maskType:KLCPopupMaskTypeDimmed
    //                         dismissOnBackgroundTouch:YES
    //                            dismissOnContentTouch:NO];
    //
    //
    //    [customePopUp show];
}

- (IBAction)btnBudgetPerPersonClicked:(id)sender {
    
    UIView *viewPopUpBudget = [self popUpViewForBudget];
    
    customePopUp = [KLCPopup popupWithContentView:viewPopUpBudget
                                         showType:KLCPopupShowTypeGrowIn
                                      dismissType:KLCPopupDismissTypeFadeOut
                                         maskType:KLCPopupMaskTypeDimmed
                         dismissOnBackgroundTouch:YES
                            dismissOnContentTouch:NO];
    
    
    [customePopUp show];
    
}

- (IBAction)btnOnNightsPopUpClicked:(id)sender {
    
    
    UIButton *button = (UIButton *)sender;
    
    self.txtNights.text = button.currentTitle;
    
    if ([[self.txtNights.text lowercaseString] isEqualToString:@"less than 3"])
    {
        stringNoOfNights = @"0-3";
    }
    else if ([[self.txtNights.text lowercaseString] isEqualToString:@"3 to 7"])
    {
        stringNoOfNights = @"3-7";
    }
    else if ([[self.txtNights.text lowercaseString] isEqualToString:@"more than 7"])
    {
        stringNoOfNights = @"7";
    }
    
    [customePopUp dismiss:YES];
}

- (IBAction)btnBudgetOptionClicked:(id)sender
{
    UIButton *button = (UIButton *)sender;
    
    self.txtBudget.text = button.currentTitle;
    
    if ([[self.txtBudget.text lowercaseString] isEqualToString:@"less than inr 40000"])
    {
        stringBudgetPerson = @"1-40000";
    }
    else if ([[self.txtBudget.text lowercaseString] isEqualToString:@"inr 40000 to inr 60000"])
    {
        stringBudgetPerson = @"40000-60000";
    }
    else if ([[self.txtBudget.text lowercaseString] isEqualToString:@"inr 60000 to 1lac"])
    {
        stringBudgetPerson = @"60000-100000";
    }
    else if ([[self.txtBudget.text lowercaseString] isEqualToString:@"inr 1lac to 1.5lac"])
    {
        stringBudgetPerson = @"100000-150000";
    }
    else if ([[self.txtBudget.text lowercaseString] isEqualToString:@"inr 1.5lac to 2lac"])
    {
        stringBudgetPerson = @"150000-200000";
    } else if ([[self.txtBudget.text lowercaseString] isEqualToString:@"above 2.5lac"])
    {
        stringBudgetPerson = @"250000-0";
    }
    
    [customePopUp dismiss:YES];
    
}

*/
-(void)showNetNotConnectedPopUp
{
    CoreUtility *coreObj=[CoreUtility sharedclassname];
    coreObj.statusForNetPopUp=1;
    if (coreObj.statusForNetPopUp==1) {
        
        [self.view endEditing:YES];
        UIView *viewPopUpInternet =   [self popUpViewForInternetDiscoonected];
        
        customePopUp = [KLCPopup popupWithContentView:viewPopUpInternet
                                             showType:KLCPopupShowTypeGrowIn
                                          dismissType:KLCPopupDismissTypeFadeOut
                                             maskType:KLCPopupMaskTypeDimmed
                             dismissOnBackgroundTouch:YES
                                dismissOnContentTouch:NO];
        [customePopUp show];
    }
}

-(void)searchDestination
{
    activityLoadingView = [LoadingView loadingViewInView:self.view.superview.superview
                                              withString:@""
                                       andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
  
    NSMutableString *queryParam = [[NSMutableString alloc] init] ;

    NSString *searchType  = [[selecteDestinationDict valueForKey:@"searchType"] uppercaseString];
    
    if (self.serach_Text.text.length !=0 )
    {
    [queryParam appendFormat:@"searchType=%@",searchType];
    
    
    if ([[searchType uppercaseString] isEqualToString:@"COUNTRY"])
    {
        /*
         searchType=COUNTRY
         destination=IN
         */
       NSString *countryCode = [[selecteDestinationDict valueForKey:@"countryCode"] uppercaseString];
        [queryParam appendFormat:@"&destination=%@",countryCode];
    }
    else if ([[searchType uppercaseString] isEqualToString:@"STATE"])
    {
        /*
         searchType=STATE
         destination=MH
         countryCode=IN
         */
        
        NSString *stateCode = [[selecteDestinationDict valueForKey:@"stateCode"] uppercaseString];
        NSString *countryCode = [[selecteDestinationDict valueForKey:@"countryCode"] uppercaseString];
        
        [queryParam appendFormat:@"&destination=%@",stateCode];
        [queryParam appendFormat:@"&countryCode=%@",countryCode];
        
    }
    else if ([[searchType uppercaseString] isEqualToString:@"CITY"])
    {
        /*
         searchType=CITY
         destination=BOM
         countryCode=IN
         stateCode=MH
         */
        
        NSString *cityCode = [[selecteDestinationDict valueForKey:@"locationCode"] uppercaseString];
        NSString *stateCode = [[selecteDestinationDict valueForKey:@"stateCode"] uppercaseString];
        NSString *countryCode = [[selecteDestinationDict valueForKey:@"countryCode"] uppercaseString];
        
        
        [queryParam appendFormat:@"&destination=%@",cityCode];
        [queryParam appendFormat:@"&countryCode=%@",countryCode];
        [queryParam appendFormat:@"&stateCode=%@",stateCode];

    }
    else if ([[searchType uppercaseString] isEqualToString:@"CONTINENT"])
    {
        /*
         searchType=CONTINENT
         destination=asia
         */
         [queryParam appendFormat:@"&destination=%@",destinationStr];
    }
    else if ([[searchType uppercaseString] isEqualToString:@"THEME"])  // Done by komal 10 Aug 2018
    {
        /*
         "searchString":"Beach",
         "searchType":"THEME",
         */
        NSString *searchString  = [[selecteDestinationDict valueForKey:@"searchString"] uppercaseString];
        [queryParam appendFormat:@"&theme=%@",searchString];
        // To show All Holiday Acc to New PSD We comment This Code On 31_July_20
        /*
         if ([_header_Name isEqualToString:kIndianHoliday])
         {
             [queryParam appendFormat:@"&pkgType=0"];
         }
         else
         {
             [queryParam appendFormat:@"&pkgType=1"];
         }
         */
    }
    else
    {
         [queryParam appendFormat:@"&destination=%@",destinationStr];
    }
    }
    NetworkHelper *helper = [NetworkHelper sharedHelper];
    
    
    //stringBudgetPerson
    
    NSString *budget1 = @"";
    NSString *budget2 = @"";
    
    if ([stringBudgetPerson isEqualToString:@"250000"])
    {
        budget1 = @"250000";
    }
    else
    {
        NSArray *budgetArray = [stringBudgetPerson componentsSeparatedByString:@"-"];
        
        if (budgetArray)
        {
            if(budgetArray.count == 2)
            {
                budget1 = [budgetArray firstObject];
                budget2 = budgetArray[1];
            }
        }
    }
    
    
    if ([budget1 isEqualToString:@""] && [budget2 isEqualToString:@""])
    {
       // queryParam = [NSMutableString stringWithFormat:@"searchType=%@&destination=%@",searchType,destinationStr];
    }
    else if ([budget1 isEqualToString:@""])
    {
        [queryParam appendFormat:@"&budget2=%@",budget2];
    }
    else if ([budget2 isEqualToString:@""])
    {
         [queryParam appendFormat:@"&budget1=%@",budget1];
    }
    else
    {
         [queryParam appendFormat:@"&budget1=%@&budget2=%@",budget1,budget2];
    }

    
    
    if (monthIndex != 0)
    {
        [queryParam appendString:[NSString stringWithFormat:@"&monthOfTravel=%@",monthIndex]];
    }
    
    @try
    {
        if ([queryParam characterAtIndex:0] == '&')
        {
            queryParam = [[queryParam substringFromIndex:1] mutableCopy];
        }
        NSString *stringWithDecode  = [queryParam stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];

        
        NSDictionary *headerDict = [CoreUtility getHeaderDict];
        
        [helper getResponseWithRequestType:@"" withQueryParam:stringWithDecode withPathParam:@"" withJsonParam:nil withHeaders:headerDict withUrl:kAstraUrlPackageFilterSearch success:^(NSDictionary *responseDict)
         {
             dispatch_async(dispatch_get_main_queue(), ^(void)
                            {
                                [activityLoadingView removeFromSuperview];
                                NSLog(@"Response Dict : %@",responseDict);
                                
                                if (responseDict)
                                {
                                    if (responseDict.count>0)
                                    {
                                        NSMutableArray *arrayOfHolidays = [[NSMutableArray alloc] init];
                                        NSArray *packageArray = (NSArray *)responseDict;
                                        
                                        for (NSDictionary *dictOfData in packageArray)
                                        {
                                            NSDictionary *packageDatailDict =[dictOfData valueForKey:@"packageDetail"];
                                            
                                            long productId = [[packageDatailDict valueForKey:@"productId"] longValue];
                                            // Unbloacked on 01/09/20
//                                            if (productId != 11) {
                                                
                                                Holiday *nwHoliday  = [[Holiday alloc]initWithDataDict:dictOfData];
                                                if (![nwHoliday isEqual:nil]) {
                                                    [arrayOfHolidays addObject:nwHoliday];
                                                }
                                                
//                                            }else{
//                                                NSLog(@"Product blocked");
//                                            }
                                            
                                           
                                            
                                        }
                                        
//                                        if ([activityIndicator isDescendantOfView:self.viewSearchHolidays.superview])
//                                        {
//                                            [activityIndicator removeView];
//                                        }
//                                        if ([activityIndicator isDescendantOfView:self.searchTableView.superview])
//                                        {
//                                            [activityIndicator removeView];
//                                        }
                                        
                                        NSDictionary *dictForCompletePackage = [packageArray objectAtIndex:0];
//                                        NSDictionary *packageDetail = [dictForCompletePackage objectForKey:@"packageDetail"];
//                                        long productId = [[packageDetail valueForKey:@"productId"] longValue];
//                                        if (productId != 11) {
//                                        }
                                        HolidayPackageDetail *packageDetailHoliday = [[HolidayPackageDetail alloc] initWithDataDict:dictForCompletePackage];
                                        NSString *header = @"";
                                        if (destinationStr == nil) {
                                            header = @"Holidays";
                                        }
                                        else
                                        {
                                            header = [NSString stringWithFormat:@"%@ %@",destinationStr, @"Holidays"];
                                        }
                                        SRPScreenVC *srpScreenVC=[[SRPScreenVC alloc]initWithNibName:@"SRPScreenVC" bundle:nil];
                                        srpScreenVC.headerName=header;
                                        srpScreenVC.searchedCity = destinationStr;
                                        srpScreenVC.holidayArrayInSrp = arrayOfHolidays;
                                        srpScreenVC.srpHolidayPackage = packageDetailHoliday;
                                        srpScreenVC.totalPackages=[NSString stringWithFormat:@"%lu",(unsigned long)[arrayOfHolidays count]];
                                        srpScreenVC.completePackageDetail = packageArray;
                                        [self.navigationController pushViewController:srpScreenVC animated:YES];
                                       
                                    }
                                    else
                                    {
                                        [super showAlertViewWithTitle:@"error" withMessage:@"Sorry, we could not find any package for the given search. Please try with a different search criteria."];
                                        NSLog(@" response Date Search packageDetailsMessage "  );
                                        
                                    }
                                    
                                }
                                
                            });
         }
                                   failure:^(NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^(void)
                            {
                                [activityLoadingView removeFromSuperview];
                                [super showAlertViewWithTitle:@"Alert" withMessage:[error description]];
                                NSLog(@"Response Dict : %@",[error description]);
                            });
             
         }];
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"%@", exception.reason);
        [activityLoadingView removeFromSuperview];
    }
    @finally
    {
        NSLog(@"exception finally called");
    }
    
    
    //    NSString *strDuration     = self.txtNights.text;
    //    NSString *strBudget      = self.txtBudget.text;
    
    
    
    if (stringBudgetPerson == nil)
    {
        stringBudgetPerson = @"";
    }
    
    if (stringNoOfNights == nil)
    {
        stringNoOfNights = @"";
    }
    
    
}

-(void)searchWithOnlyDestination{
    
    
    NSString *strDestination=destinationStr;
    
    NSDictionary *dictOfData = [NSDictionary dictionaryWithObjectsAndKeys:@"",kBudgetKey,@"",kNightToSpendKey,strDestination,kDestinationKey, nil];
    
//    if (_boolForSearchPackgScreen==true) {
//        activityIndicator = [LoadingView loadingViewInView:self.searchTableView.superview withString:@"" andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
//    }
//    else{
//        activityIndicator = [LoadingView loadingViewInView:self.viewSearchHolidays.superview withString:@"" andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
//    }
    
    if ([self connected])
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            NetworkHelper *helper = [NetworkHelper sharedHelper];
            
            NSString *strResponseLocal = [helper getDataFromServerForType:kType entity:kEntity action:kAction andUserJson:dictOfData];
            
            NSString *strStatus = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponseLocal]valueForKey:@"status"];
            dispatch_async(dispatch_get_main_queue(), ^{
                [activityIndicator removeView];
                if ([strStatus isEqualToString:kStatusSuccess])
                {
                    
                    NSString * packageDetailsMessage = [[[JsonSerealizer dictonaryOfJsonFromJsonData:strResponseLocal]valueForKey:@"data"]valueForKey:@"packageDetailsMessage"];
                    
                    if([packageDetailsMessage isKindOfClass:[NSNull class]])
                    {
                        [super showAlertViewWithTitle:@"error" withMessage:@"Sorry, we could not find any package for the given search. Please try with a different search criteria."];
                        NSLog(@" response Date Search packageDetailsMessage "  );
                    }
                    else if ([@"Data not found." caseInsensitiveCompare:packageDetailsMessage] == NSOrderedSame)
                    {
                        [super showAlertViewWithTitle:@"error" withMessage:@"Sorry, we could not find any package for the given search. Please try with a different search criteria."];
                        NSLog(@" response Date Search packageDetailsMessage "  );
                        
                    }
                    else
                    {
                        NSArray *arrayOfData = [[[JsonSerealizer dictonaryOfJsonFromJsonData:strResponseLocal]valueForKey:kServerResponseKeyData]valueForKey:kHolidays];
                        NSDictionary *filterDict = [[[JsonSerealizer dictonaryOfJsonFromJsonData:strResponseLocal]valueForKey:kServerResponseKeyData]valueForKey:kFilters];
                        NSMutableArray *arrayOfHolidays = [[NSMutableArray alloc]init];
                        
                        for (NSDictionary *dictOfData in arrayOfData)
                        {
                            Holiday *nwHoliday  = [[Holiday alloc]initWithDataDict:dictOfData];
                            [arrayOfHolidays addObject:nwHoliday];
                        }
                        
                        /*
                        if ([activityIndicator isDescendantOfView:self.viewSearchHolidays.superview])
                        {
                            [activityIndicator removeView];
                        }
                        if ([activityIndicator isDescendantOfView:self.searchTableView.superview])
                        {
                            [activityIndicator removeView];
                        }
                        
                        
                        TabMenuVC *tabMenuVC=[[TabMenuVC alloc]initWithNibName:@"BaseViewController" bundle:nil];
                        
                        if (_boolForSearchPackgScreen==true) {
                            
                            tabMenuVC.headerName=[NSString stringWithFormat:@"%@ %@",strDestination, @"Holidays"];
                            
                        }
                        else{
                            tabMenuVC.headerName=[NSString stringWithFormat:@"%@ %@",strDestination, @"Holidays"];
                        }
                        tabMenuVC.searchedCity = destinationStr;
                        tabMenuVC.holidayArray = arrayOfHolidays;
                        tabMenuVC.filterDict = filterDict;
                        tabMenuVC.totalPackages=[NSString stringWithFormat:@"%lu",(unsigned long)[arrayOfData count]];
                        
                        [[SlideNavigationController sharedInstance] pushViewController:tabMenuVC animated:YES];
                        */
                    }
                    
                }
                else
                {
                    NSString *messsage = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponseLocal]valueForKey:@"message"];
                    if (messsage)
                    {
                        if ([messsage isEqualToString:kVersionUpgradeMessage])
                        {
                            NSDictionary *downloadDict = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponseLocal] valueForKey:@"data"];
                            NSString *downloadURL = [downloadDict valueForKey:@"download_url"];
//                            [super showAlertViewForVersionUpdateWithUrl:downloadURL];
                            
                        }else
                        {
                            [activityIndicator removeView];
                            
                            //[super showAlertViewWithTitle:@"Alert" withMessage:@"Sorry, we could not find any package for the given search. Please try with a different search criteria."];
                            
                            [self showFormatedSearchResult];
                        }
                    }
                    else
                    {
                        [super showAlertViewWithTitle:@"Alert" withMessage:messsage];
                        [activityIndicator removeView];
                        
                        
                    }
                    
                }
                
            });
        });
        
    }
    else
    {
//        if([activityIndicator isDescendantOfView:self.viewSearchHolidays])
//        {
//            [activityIndicator removeView];
//        }
        
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
        {
            UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Info"
                                                                               message:kMessageNoInternet
                                                                        preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                
                [alertView dismissViewControllerAnimated:YES completion:nil];
                
            }];
            
            [alertView addAction:actionOk];
            
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Info"
                                                           message:kMessageNoInternet
                                                          delegate:nil
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles:nil, nil];
            
            [alert show];
        }
    }
    
    
}


-(void) showFormatedSearchResult
{
    NSString *strAlertMessage = @"";
    
    NSString *strAlertDuration = @"";
    
    NSString *strAlertBudget = @"";
    
    if ([stringNoOfNights isEqualToString:@"0-3"])
    {
        strAlertDuration = @"less than 3 days";
    }
    else if ([stringNoOfNights isEqualToString:@"3-7"])
    {
        strAlertDuration = @"3 to 7 days";
    }
    else if ([stringNoOfNights isEqualToString:@"7"])
    {
        strAlertDuration = @"more than 7 days";
    }
    
    if ([stringBudgetPerson isEqualToString:@"0-40000"])
    {
        strAlertBudget = @"less than Rs.40000";
    }
    else if ([stringBudgetPerson isEqualToString:@"40000-60000" ])
    {
        strAlertBudget = @"Rs.40000 to Rs.60000";
    }
    else if ([stringBudgetPerson isEqualToString:@"60000-100000"])
    {
        strAlertBudget = @"Rs.60000 to Rs.100000";
    }
    else if ([stringBudgetPerson isEqualToString:@"100000-150000"])
    {
        strAlertBudget = @"Rs.100000 to Rs.150000";
    }
    else if ([stringBudgetPerson isEqualToString:@"150000-200000"])
    {
        strAlertBudget = @"Rs.150000 to Rs.200000";
    }
    else if ([stringBudgetPerson isEqualToString:@"250000"])
    {
        strAlertBudget = @"Rs.250000";
    }
    
    /*
    if ([self.txtDestination.text length] != 0 && [self.txtBudget.text length] != 0 && [self.txtNights.text length] !=0)
    {
        strAlertMessage = [NSString stringWithFormat:@"Sorry! %@ Holiday packages for \"%@\" and \"%@\" are not available. Take a look at our top selling packages that match your destination.", destinationStr, strAlertBudget, strAlertDuration];
    }
    else if ([self.txtDestination.text length] != 0 && [self.txtBudget.text length] != 0)
    {
        strAlertMessage = [NSString stringWithFormat:@"Sorry! %@ Holiday packages for \"%@\" are not available. Take a look at our top selling packages that match your destination.", destinationStr, strAlertBudget];
        
    }
    else if ([self.txtDestination.text length] != 0 && [self.txtNights.text length] !=0)
    {
        strAlertMessage = [NSString stringWithFormat:@"Sorry! %@ Holiday packages for \"%@\" are not available. Take a look at our top selling packages that match your destination.", destinationStr, strAlertDuration];
        
    }
    else
    {
        strAlertMessage = [NSString stringWithFormat:@"Sorry! %@ Holiday packages are not available. Take a look at our top selling packages that match your destination.", destinationStr];
    }
    */
    [self showAlertViewWithTitle:@"Alert" withMessage:strAlertMessage];
}



#pragma mark - Pop Up View Creation
- (UIView *) popUpViewForNumberOfNights {
    
    UIView *popUpView = [UIView new];
    popUpView.frame = CGRectMake(0, 0, 240, 180);
    popUpView.backgroundColor = [UIColor whiteColor];
    popUpView.layer.cornerRadius = 1.0;
    
    UIView *viewHeader = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 240, 45)];
    viewHeader.backgroundColor = [UIColor colorFromHexString:@"#0066FF"];
    
    UILabel *lblHeader = [[UILabel alloc]initWithFrame:CGRectMake(0, 14, 130, 21)];
    lblHeader.font = [UIFont fontWithName:TITILLUM_SEMIBOLD size:17];
    lblHeader.text = @"Number of Nights";
    lblHeader.textColor = [UIColor whiteColor];
    lblHeader.textAlignment = NSTextAlignmentCenter;
    
    [viewHeader addSubview:lblHeader];
    lblHeader.center = viewHeader.center;
    
    UIButton *btnLessThanThree = [[UIButton alloc]initWithFrame:CGRectMake(0, 55, 240, 35)];
    [btnLessThanThree setTitle:@"Less than 3" forState:UIControlStateNormal];
    [btnLessThanThree setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btnLessThanThree.titleLabel.font = [UIFont fontWithName:TITILLUM_SEMIBOLD size:15];
    [btnLessThanThree setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    btnLessThanThree.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0);
    btnLessThanThree.tag = 1;
    [btnLessThanThree addTarget:self
                         action:@selector(btnOnNightsPopUpClicked:)
               forControlEvents:UIControlEventTouchUpInside];
    
    
    UIButton *btn3To7 = [[UIButton alloc]initWithFrame:CGRectMake(0, 90, 240, 35)];
    [btn3To7 setTitle:@"3 To 7" forState:UIControlStateNormal];
    [btn3To7 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btn3To7.titleLabel.font = [UIFont fontWithName:TITILLUM_SEMIBOLD size:15];
    [btn3To7 setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    btn3To7.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0);
    btn3To7.tag = 2;
    [btn3To7 addTarget:self
                action:@selector(btnOnNightsPopUpClicked:)
      forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *btnMoreThan7 = [[UIButton alloc]initWithFrame:CGRectMake(0, 130, 240, 35)];
    [btnMoreThan7 setTitle:@"More than 7" forState:UIControlStateNormal];
    [btnMoreThan7 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btnMoreThan7.titleLabel.font = [UIFont fontWithName:TITILLUM_SEMIBOLD size:15];
    [btnMoreThan7 setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    btnMoreThan7.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0);
    btnMoreThan7.tag = 3;
    [btnMoreThan7 addTarget:self
                     action:@selector(btnOnNightsPopUpClicked:)
           forControlEvents:UIControlEventTouchUpInside];
    
    [popUpView addSubview:viewHeader];
    [popUpView addSubview:btnLessThanThree];
    [popUpView addSubview:btn3To7];
    [popUpView addSubview:btnMoreThan7];
    
    return popUpView;
    
}

-(void)onBudgetResetClicked:(id)sender
{
     stringBudgetPerson = @"";
    
//    self.txtBudget.text = @"";

     [customePopUp dismiss:YES];

}


-(UIView *) popUpViewForBudget
{
    UIView *popUpView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 240, 270)];
    popUpView.backgroundColor = [UIColor whiteColor];
    popUpView.layer.cornerRadius = 1.0;
    
    
    UIView *viewHeader = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 240, 50)];
    viewHeader.backgroundColor = [UIColor colorFromHexString:@"#0066FF"];
    
    UILabel *lblHeader = [[UILabel alloc]initWithFrame:CGRectMake(0, 14, 135, 21)];
    lblHeader.font = [UIFont fontWithName:TITILLUM_SEMIBOLD size:17];
    lblHeader.text = @"Budget per Person";
    lblHeader.textColor = [UIColor whiteColor];
    lblHeader.textAlignment = NSTextAlignmentCenter;
    lblHeader.userInteractionEnabled = YES;
    lblHeader.enabled = YES;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onBudgetResetClicked:)];
    tapGesture.numberOfTapsRequired = 1;
    [lblHeader addGestureRecognizer:tapGesture];
    [viewHeader addSubview:lblHeader];
    lblHeader.center = viewHeader.center;
    
    UIButton *btnLessThan40k = [[UIButton alloc]initWithFrame:CGRectMake(0, 55, 240, 35)];
    [btnLessThan40k setTitle:@"Less than INR 40000" forState:UIControlStateNormal];
    [btnLessThan40k setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btnLessThan40k.titleLabel.font = [UIFont fontWithName:TITILLUM_SEMIBOLD size:15];
    [btnLessThan40k setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    btnLessThan40k.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0);
    btnLessThan40k.tag = 1;
    [btnLessThan40k addTarget:self
                       action:@selector(btnBudgetOptionClicked:)
             forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *btn40kTo60k = [[UIButton alloc]initWithFrame:CGRectMake(0, 90, 240, 35)];
    [btn40kTo60k setTitle:@"INR 40000 To INR 60000" forState:UIControlStateNormal];
    [btn40kTo60k setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btn40kTo60k.titleLabel.font = [UIFont fontWithName:TITILLUM_SEMIBOLD size:15];
    [btn40kTo60k setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    btn40kTo60k.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0);
    btn40kTo60k.tag = 2;
    [btn40kTo60k addTarget:self
                    action:@selector(btnBudgetOptionClicked:)
          forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *btn60kTo1Lacs = [[UIButton alloc]initWithFrame:CGRectMake(0, 125, 240, 35)];
    [btn60kTo1Lacs setTitle:@"INR 60000 To 1Lac" forState:UIControlStateNormal];
    [btn60kTo1Lacs setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btn60kTo1Lacs.titleLabel.font = [UIFont fontWithName:TITILLUM_SEMIBOLD size:15];
    [btn60kTo1Lacs setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    btn60kTo1Lacs.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0);
    btn60kTo1Lacs.tag = 3;
    [btn60kTo1Lacs addTarget:self
                      action:@selector(btnBudgetOptionClicked:)
            forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *btnLess1To1pt5Lac = [[UIButton alloc]initWithFrame:CGRectMake(0, 160, 240, 35)];
    [btnLess1To1pt5Lac setTitle:@"INR 1Lac To 1.5Lac" forState:UIControlStateNormal];
    [btnLess1To1pt5Lac setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btnLess1To1pt5Lac.titleLabel.font = [UIFont fontWithName:TITILLUM_SEMIBOLD size:15];
    [btnLess1To1pt5Lac setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    btnLess1To1pt5Lac.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0);
    btnLess1To1pt5Lac.tag = 4;
    [btnLess1To1pt5Lac addTarget:self
                          action:@selector(btnBudgetOptionClicked:)
                forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *btn1pt5To2Lac = [[UIButton alloc]initWithFrame:CGRectMake(0, 195, 240, 35)];
    [btn1pt5To2Lac setTitle:@"INR 1.5Lac To 2Lac" forState:UIControlStateNormal];
    [btn1pt5To2Lac setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btn1pt5To2Lac.titleLabel.font = [UIFont fontWithName:TITILLUM_SEMIBOLD size:15];
    [btn1pt5To2Lac setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    btn1pt5To2Lac.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0);
    btn1pt5To2Lac.tag = 5;
    [btn1pt5To2Lac addTarget:self
                      action:@selector(btnBudgetOptionClicked:)
            forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *btnAbove2Lac = [[UIButton alloc]initWithFrame:CGRectMake(0, 228, 240, 35)];
    [btnAbove2Lac setTitle:@"Above 2.5Lac" forState:UIControlStateNormal];
    [btnAbove2Lac setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btnAbove2Lac.titleLabel.font = [UIFont fontWithName:TITILLUM_SEMIBOLD size:15];
    [btnAbove2Lac setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    btnAbove2Lac.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0);
    btnAbove2Lac.tag = 6;
    [btnAbove2Lac addTarget:self
                     action:@selector(btnBudgetOptionClicked:)
           forControlEvents:UIControlEventTouchUpInside];
    
    
    [popUpView addSubview:viewHeader];
    [popUpView addSubview:btnLessThan40k];
    [popUpView addSubview:btn40kTo60k];
    [popUpView addSubview:btn60kTo1Lacs];
    [popUpView addSubview:btnLess1To1pt5Lac];
    [popUpView addSubview:btn1pt5To2Lac];
    [popUpView addSubview:btnAbove2Lac];
    
    return popUpView;
}


#pragma mark - Validations

-(BOOL)textFieldValidation
{
    ENABLE_LOGGER();
    /*
    if ([self.txtDestination.text length] == 0||[self.txtDestination.text isEqualToString:@""]) {
        
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Info"
                                                           message:@"Please enter destination details."
                                                          delegate:nil
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles:nil, nil];
        
        [alertView show];
        
        return FALSE;
    }
    else if ([self.txtNights.text length] == 0 || [self.txtNights.text isEqualToString:@""]){
        
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Info"
                                                           message:@"Please enter number of nights."
                                                          delegate:nil
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles:nil, nil];
        
        [alertView show];
        
        return FALSE;
        
    }
    else if ([self.txtBudget.text length] == 0 || [self.txtBudget.text isEqualToString:@""]){
        
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Info"
                                                           message:@"Please enter budget details."
                                                          delegate:nil
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles:nil, nil];
        
        [alertView show];
        
        return FALSE;
    }
    */
    return TRUE;
}




#pragma mark - Slide Navigation Delegate

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
}

- (BOOL)slideNavigationControllerShouldDisplayRightMenu
{
    return NO;
}


#pragma mark -Helper methods
// Vjiay Comment may be not used

/*
-(void)loadHolidayDetailsScreen
{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"holidayData" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:filePath];
    NSArray *json = [[[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil]valueForKey:kServerResponseKeyData]valueForKey:kHolidays];
    
    NSDictionary *filterDict = [[[JsonSerealizer dictonaryOfJsonFromJsonData:data]valueForKey:kServerResponseKeyData]valueForKey:kFilters];
    NSMutableArray *arrayOfHolidays = [[NSMutableArray alloc]init];
    
    for (NSDictionary *dictOfData in json)
    {
        Holiday *nwHoliday  = [[Holiday alloc]initWithDataDict:dictOfData];
        [arrayOfHolidays addObject:nwHoliday];
    }
    
    TabMenuVC *tabMenuVC=[[TabMenuVC alloc]initWithNibName:@"BaseViewController" bundle:nil];
    tabMenuVC.headerName=[NSString stringWithFormat:@"%@ Holidays",destinationStr];
    tabMenuVC.searchedCity = destinationStr;
    tabMenuVC.holidayArray = arrayOfHolidays;
    tabMenuVC.filterDict = filterDict;
    [[SlideNavigationController sharedInstance] pushViewController:tabMenuVC animated:YES];
}
-(void)backButtonAction{
    noDataLbl.text = @"";
    noDataLbl.frame  = CGRectMake(-150, -150, 300, 100) ;
    if (ViewFlagForSearchIcon==1) {
        [[SlideNavigationController sharedInstance] popViewControllerAnimated:YES];
        ViewFlagForSearchIcon=-1;
        return;
    }
    if (ViewFlag==1) {
        
        
        [_searchTableView removeFromSuperview];
        
        super.HeaderLabel.hidden=NO;
        [textFieldSearch removeFromSuperview];
        //[self.view addSubview:noDataLbl];
        
        ViewFlag=-1;
        
    }
    else
        
        [[SlideNavigationController sharedInstance] popViewControllerAnimated:YES];
    
}
 

#pragma mark - Mobile Number Validation Method
-(BOOL)validatePhoneNumber
{
    NSString *string = self.txtMobileNo.text;
    NSString *expression = @"^([+][9][1]|[9][1]|[0]){0,1}([7-9]{1})([0-9]{9})$";
    NSError *error = NULL;
    
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
    
    NSTextCheckingResult *match = [regex firstMatchInString:string options:0 range:NSMakeRange(0, [string length])];
    
    if (match){
        NSLog(@"yes");
        return YES;
    }else{
        NSLog(@"no");
        return NO;
    }
    
}
 */


-(UIView *)popUpViewForInternetDiscoonected{
    UIView *popUpView = [UIView new];
    popUpView.frame = CGRectMake(0, 0, 200,200 );
    popUpView.backgroundColor = [UIColor whiteColor];
    
    UIImageView *imagelogo=[[UIImageView alloc] initWithFrame:CGRectMake(55,50,80,60)];
    imagelogo.image=[UIImage imageNamed:@"login_logo"];
    UILabel *lblInternetHeader=[[UILabel alloc]initWithFrame:CGRectMake(15, 15, 180, 40)];
    lblInternetHeader.font=[UIFont fontWithName:@"Titillium-bold" size:17];
    lblInternetHeader.text=@"Internet not available";
    
    UILabel *lblInternetMsg=[[UILabel alloc]initWithFrame:CGRectMake(10, 105, 190, 50)];
    lblInternetMsg.text=@"Please turn on your internet";
    lblInternetMsg.font=[UIFont fontWithName:@"Titillium-semibold" size:14];
    lblInternetMsg.textColor=[UIColor grayColor];
    
    UIButton *btnInterNetOk=[[UIButton alloc]initWithFrame:CGRectMake(70, 150, 50, 40)];
    [btnInterNetOk addTarget:self action: @selector(actionForNetok) forControlEvents:UIControlEventTouchUpInside];
    [btnInterNetOk setBackgroundColor:[UIColor orangeColor]];
    [btnInterNetOk setTitle:@"OK" forState:UIControlStateNormal];
    
    
    popUpView.layer.cornerRadius = 1.0;
    [popUpView addSubview:lblInternetHeader];
    [popUpView addSubview:imagelogo];
    [popUpView addSubview:lblInternetMsg];
    [popUpView addSubview:btnInterNetOk];
    
    return popUpView;
}
-(void)actionForNetok{
    [customePopUp dismiss:YES];
    
}

#pragma mark - Autopopulate Phone no

-(NSArray *)nextYearMonths
{
    NSDateFormatter  *dateFormatter   = [[NSDateFormatter alloc] init];
    NSDate           *today           = [NSDate date];
    NSCalendar       *currentCalendar = [NSCalendar currentCalendar];
    
    NSDateComponents *monthComponents = [currentCalendar components:NSCalendarUnitMonth fromDate:today];
    int currentMonth = (int)[monthComponents month];
    
    NSDateComponents *yearComponents  = [currentCalendar components:NSCalendarUnitYear  fromDate:today];
    int currentYear  = (int)[yearComponents year];
    int nextYear     = currentYear + 1;
    
    int months  = 1;
    int year;
    
    NSMutableArray *monthsArray = [[NSMutableArray alloc]init];
    
    //[monthsArray addObject:[NSString stringWithFormat:@"%@ %i",[[dateFormatter monthSymbols] objectAtIndex: currentMonth],currentYear]];
    
    for(int m = currentMonth; months <= 12; m++) {
        
        int nextMonth;
        
        if (m == 12)
        {
            nextMonth = 12;
        }
        else
        {
            nextMonth = (m % 12);
        }
        
        if(nextMonth < currentMonth){
            year = nextYear;
        } else {
            year = currentYear;
        }
        
        if (nextMonth >= 0 )
        {
            NSLog(@"Filter pacjkage = %@ %i",[[dateFormatter monthSymbols] objectAtIndex: nextMonth-1],year);
            [monthsArray addObject:[NSString stringWithFormat:@"%@ %i",[[dateFormatter monthSymbols] objectAtIndex: nextMonth-1],year]];
        }
        
        months++;
    }
    
    
    return monthsArray;
}


#pragma mark NetCore log Event

//APP_FIND_HOLIDAYS

-(void)netCoreDataDisplaySearch
{
    
    
    NSMutableDictionary *payloadList =  [NSMutableDictionary dictionary];
    [payloadList setObject:[NSString stringWithFormat:@"%@",self.serach_Text.text] forKey:@"s^DESTINATION"];
//    [payloadList setObject:[NSString stringWithFormat:@"%@",_txtNights.text] forKey:@"s^DURATION"];
//    [payloadList setObject:[NSString stringWithFormat:@"%@", _txtBudget.text] forKey:@"s^BUDGET"];
    [payloadList setObject:@"App" forKey:@"s^SOURCE"];
    if ([_header_Name isEqualToString:@"International Holidays"])//28-02-2018
    {
        [payLoadForViewDetails setObject:@"international" forKey:@"s^TYPE"];
    }
    else
    {
        [payLoadForViewDetails setObject:@"international" forKey:@"s^TYPE"];
    }
    
    [NetCoreAnalyticsVC getNetCorePushNotificationDetailDictionary:payloadList withPayloadCount:122];
    
   // [NetCoreAnalyticsVC getNetCorePushNotificationDetailDictionary:payloadList withPayloadCount:122]; //28-02-2018
}

//APP_INT_HO_VIEW_DETAILS

-(void)fireAppIntHoViewDetailsEvent:(HolidayPackageDetail *)PackagaeDetail
{
    [payLoadForViewDetails setObject:PackagaeDetail.strPackageName forKey:@"s^DESTINATION"];
    [payLoadForViewDetails setObject:[NSString stringWithFormat:@"%d",PackagaeDetail.durationNoDays] forKey:@"s^DURATION"];
    [payLoadForViewDetails setObject:[NSString stringWithFormat:@"%d",PackagaeDetail.packagePrise] forKey:@"s^BUDGET"];
    [payLoadForViewDetails setObject:PackagaeDetail.strPackageId forKey:@"s^PACKAGE_ID"];
    [payLoadForViewDetails setObject:@"App" forKey:@"s^SOURCE"];
    [payLoadForViewDetails setObject:[NSNumber numberWithInt:PackagaeDetail.packagePrise] forKey:@"i^PACKAGE_PRICE"];//28-02-2018
//    [payLoadForViewDetails setObject:[NSString stringWithFormat:@"%d",packageDetail.packagePrise] forKey:@"i^PACKAGE_PRICE"];
    
    if ([[PackagaeDetail.strPackageSubType lowercaseString]isEqualToString:@"git"])
    {
        [payLoadForViewDetails setObject:@"no" forKey:@"s^PERSONALIZED_TOUR"];
    }
    else
    {
        [payLoadForViewDetails setObject:@"yes" forKey:@"s^PERSONALIZED_TOUR"];
    }
    
    
    //28-02-2018
    if ([_header_Name isEqualToString:@"International Holidays"])
    {
        [payLoadForViewDetails setObject:@"international" forKey:@"s^TYPE"];
    }
    else
    {
        [payLoadForViewDetails setObject:@"international" forKey:@"s^TYPE"];
    }
    
    
//    [NetCoreAnalyticsVC getNetCorePushNotificationDetailDictionary:payLoadForViewDetails withPayloadCount:105];
    
    [NetCoreAnalyticsVC getNetCorePushNotificationDetailDictionary:payLoadForViewDetails withPayloadCount:125]; //28-02-2018
    NSLog(@"Data submitted to the netcore for 125");
}


@end
