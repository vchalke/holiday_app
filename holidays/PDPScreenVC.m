//
//  PDPScreenVC.m
//  holidays
//
//  Created by Kush_Tech on 03/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "PDPScreenVC.h"
#import "FlightsTableViewCell.h"
#import "HeaderTableViewCell.h"
#import "PackageDetailTableViewCell.h"
#import "OtherDetailTableViewCell.h"
#import "MainHotelTableViewCell.h"
#import "MainFlightTableViewCell.h"
#import "ItinenaryObject.h"
#import "PackageDetailModel.h"
#import "ViewMoreVC.h"
#import "WebUrlConstants.h"
#import "itinerayScreenVC.h"
#import "ItinenaryDetailTableViewCell.h"
#import "TalkToExpertView.h"
#import "PDPCalculatePrizeVC.h"
#import "PDPCalculateCostVC.h"
#import "CompareScreenVC.h"
#import "MobileNumberPopUpViewController.h"
#import "UIViewController+MJPopupViewController.h"
#import "NetCoreAnalyticsVC.h"
#import "OffersCollectionObject.h"
#import "Thomas_Cook_Holidays-Swift.h"
#import "PhotoCollectionVC.h"
#import "ViewMoreOtherVC.h"
#import "OffersDetailsVC.h"
#import "TalkToExpertVC.h"
#import "RequestViewVC.h"

@interface PDPScreenVC ()<OtherDetailTableViewCellDelegaete,MainHotelTableViewCellDelegaete,MainFlightTableViewCellDelegaete,ItinenaryDetailTableViewCellDelegaete,HeaderTableViewCellDelegaete,PackageDetailTableViewCellDelegaete,MobileNumberVCDelegate,PackagePricingViewControllerDelegate,TalkToExpertVCDelegate>
{
    NSMutableArray *titleArray;
    HolidayPackageDetail *packageDetailObj;
    NSMutableArray *itinenaryStrArray;
    NSDictionary *completePackageDetailDict;
    LoadingView *activityLoadingView;
    MobileNumberPopUpViewController *mobileNumberViewController;
    BOOL isOfferAvailabel;
    NSMutableDictionary *payloadListSortBy, *payloadListWishList, *payLoadSearchResult, *payLoadViewPageDetail ;
    NSInteger mainStartPrice;
}
@end

@implementation PDPScreenVC

- (void)viewDidLoad {
//    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationController.navigationBar.hidden = YES;
    
    [self.base_table_view registerNib:[UINib nibWithNibName:@"MainFlightTableViewCell" bundle:nil] forCellReuseIdentifier:@"MainFlightTableViewCell"];
    [self.base_table_view registerNib:[UINib nibWithNibName:@"HeaderTableViewCell" bundle:nil] forCellReuseIdentifier:@"HeaderTableViewCell"];
    [self.base_table_view registerNib:[UINib nibWithNibName:@"PackageDetailTableViewCell" bundle:nil] forCellReuseIdentifier:@"PackageDetailTableViewCell"];
     [self.base_table_view registerNib:[UINib nibWithNibName:@"OtherDetailTableViewCell" bundle:nil] forCellReuseIdentifier:@"OtherDetailTableViewCell"];
    [self.base_table_view registerNib:[UINib nibWithNibName:@"MainHotelTableViewCell" bundle:nil] forCellReuseIdentifier:@"MainHotelTableViewCell"];
    [self.base_table_view registerNib:[UINib nibWithNibName:@"ItinenaryDetailTableViewCell" bundle:nil] forCellReuseIdentifier:@"ItinenaryDetailTableViewCell"];
    self.lbl_MainTitle.text = self.headerName;
    completePackageDetailDict = [self.completePackageDetail objectAtIndex:0];
    
    [self loadTitlesInArray];
    self.offersView.hidden = YES;
    NSLog(@"completePackageDetail  %@",self.completePackageDetail);
    NSDictionary * dict = [self.completePackageDetail objectAtIndex:0];
    NSLog(@"only dict  %@",dict);
    NSDictionary *packageDetailDict = [dict valueForKey:@"packageDetail"];
    NSLog(@"packageDetail dict  %@",packageDetailDict);
    NSLog(@"all keys of packageDetail dict  %@",[packageDetailDict allKeys]);
    
   
     
    NSArray * photoVideoList = [packageDetailDict objectForKey:@"tcilHolidayPhotoVideoCollection"];
    NSLog(@"photoVideoList array  %@",photoVideoList);
    
    NSArray * flightsList = [packageDetailDict objectForKey:@"tcilHolidayFlightsCollection"];
    NSLog(@"flightsList array  %@",flightsList);
    
    NSArray * accomdationList = [packageDetailDict objectForKey:@"tcilHolidayAccomodationCollection"];
    NSLog(@"accomdationList array  %@",accomdationList);
    
    NSArray * visaList = [packageDetailDict objectForKey:@"tcilHolidayVisaCollection"];
    NSLog(@"visaList array  %@",visaList);
    
    NSArray * mealsList = [packageDetailDict objectForKey:@"tcilHolidayMealCollection"];
    NSLog(@"mealsList array  %@",mealsList);
    
    NSArray * transferList = [packageDetailDict objectForKey:@"tcilHolidayTransfersCollection"];
    NSLog(@"transferList array  %@",transferList);
    
    NSArray * excludeList = [packageDetailDict objectForKey:@"tcilHolidayIncludeExcludeCollection"];
    NSLog(@"excludeList array  %@",excludeList);
    
    NSArray * sightseeList = [packageDetailDict objectForKey:@"tcilHolidaySightseeingCollection"];
    NSLog(@"sightseeList array  %@",sightseeList);
    
    NSArray * tcilHolidayPaymentTermsCollection = [packageDetailDict objectForKey:@"tcilHolidayPaymentTermsCollection"];
    NSLog(@"tcilHolidayPaymentTermsCollection array  %@",tcilHolidayPaymentTermsCollection);

    NSArray * mstPolicy = [packageDetailDict objectForKey:@"mstPolicy"];
    NSLog(@"mstPolicy array  %@",mstPolicy);
    
   
    packageDetailObj = [[HolidayPackageDetail alloc] initWithDataDict:self.dictForCompletePackage];
    
    NSLog(@"%@",[[titleArray objectAtIndex:[titleArray count]-1] uppercaseString]);
    
    
    payloadListSortBy =  [NSMutableDictionary dictionary];
    payloadListWishList = [NSMutableDictionary dictionary];
    payLoadSearchResult = [NSMutableDictionary dictionary];
    payLoadViewPageDetail = [NSMutableDictionary dictionary];
    self.bottomOfferView.hidden = !isOfferAvailabel;
    [self performSelector:@selector(getTokenIDInMaster) withObject:nil afterDelay:1.0];
//    [self performSelector:@selector(callGetOfferAPI) withObject:nil afterDelay:1.0];
    [self checkOfferIsAvailable];
    NSString *packageTypeString = (self.holidayPackageDetailInPdp.startingPriceStandard > 0) ? standardPackage : (self.holidayPackageDetailInPdp.startingPriceDelux > 0) ? deluxePackage : (self.holidayPackageDetailInPdp.startingPricePremium > 0) ? premiumPackage : standardPackage;
    
    if ([packageTypeString isEqualToString:standardPackage]){
        mainStartPrice = self.holidayPackageDetailInPdp.startingPriceStandard;
    }else if ([packageTypeString isEqualToString:deluxePackage]){
        mainStartPrice = self.holidayPackageDetailInPdp.startingPriceDelux;
    }else{
        mainStartPrice = self.holidayPackageDetailInPdp.startingPricePremium;
    }
    self.lbl_packagePrise.text = [self getNumberFormatterString:mainStartPrice];
}
-(void)viewWillAppear:(BOOL)animated{
    self.baseCompareView.hidden = YES;
    [self setCompareViewWithAlert:NO];
    
}

-(void)callGetOfferAPI{
    NetworkHelper *helper = [NetworkHelper sharedHelper];
    if ([self checkInternet])
    {
        NSString *pathParam = self.holidayPackageDetailInPdp.strPackageId;
        NSLog(@"Response Dict pathParam : %@",pathParam);
        activityLoadingView = [LoadingView loadingViewInView:self.view.superview.superview withString:@"" andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE_WHITE]];
        NSDictionary *headerDict = [CoreUtility getHeaderDict];
        [helper getResponseWithRequestType:@"" withQueryParam:@"" withPathParam:pathParam withJsonParam:nil withHeaders:headerDict withUrl:getByPackageId success:^(NSDictionary *responseDict)
         {
            NSLog(@"Response Dict get Banner Data API : %@",responseDict);
            if (responseDict.count != 0) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [activityLoadingView removeFromSuperview];
                    if ([responseDict objectForKey:@"result"]){
                        NSArray *resultArr = [responseDict objectForKey:@"result"];
                        if ([resultArr count]>0){
                            NSMutableArray *mutArray = [[NSMutableArray alloc]init];
                            for (NSDictionary *object in resultArr){
                               [mutArray addObject:[object objectForKey:@"holidayoffername"]] ;
                            }
                            [self setDataInOfferView:[mutArray copy]];
                        }
                    }
                   });
            }else{
                dispatch_async(dispatch_get_main_queue(), ^(void) {
                    [activityLoadingView removeFromSuperview];
                });
            }
        }
                                   failure:^(NSError *error)
         {
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                [activityLoadingView removeFromSuperview];
                [self showButtonsInAlertWithTitle:@"Alert" msg:@"some error occurred" style:UIAlertControllerStyleAlert buttArray:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString * str) {
                    if ([str isEqualToString:@"Ok"]){
                        
                    }
                }];
                
            });
        }];
        
    }
    else
    {
        [self showButtonsInAlertWithTitle:@"Alert" msg:@"some error occurred" style:UIAlertControllerStyleAlert buttArray:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString * str) {
            if ([str isEqualToString:@"Ok"]){
                
            }
        }];
    }
}
-(void)checkOfferIsAvailable{
    if (self.holidayPackageDetailInPdp.Offers.length >0){
        [self.bottomOfferView setHidden:NO];
    }else{
        [self.bottomOfferView setHidden:YES];
    }
}
-(void)setDataInOfferView:(NSArray*)arrayInfo{
    NSMutableAttributedString * string = [[NSMutableAttributedString alloc] init];
//    for (id object in arrayInfo){
//        OffersCollectionObject *objec = [[OffersCollectionObject alloc]initWithCollectionObject:object];
//        [string appendAttributedString:[[NSAttributedString alloc]initWithString:[NSString stringWithFormat:@"  •    %@\n\n",objec.holidayOfferName]]];
//    }
    
    for (NSString *object in arrayInfo){
        [string appendAttributedString:[[NSAttributedString alloc]initWithString:[NSString stringWithFormat:@"  •    %@\n\n",object]]];
    }
    
    //        NSMutableParagraphStyle *paragraphStyle;
    //        paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    //        paragraphStyle.defaultTabInterval = 50.0;
    //        paragraphStyle.firstLineHeadIndent = 45.0;
    //        paragraphStyle.headIndent = 20.0;
    //        paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    //        [string addAttributes:@{NSParagraphStyleAttributeName: paragraphStyle} range:NSMakeRange(0,[string length])];
    self.lbl_offers.text = string.string;
    
    isOfferAvailabel = [arrayInfo count]>0;
    self.bottomOfferView.hidden = !isOfferAvailabel;
}
#pragma mark - Load Titles In Array
-(void)loadTitlesInArray{
    
    titleArray = [[NSMutableArray alloc] init];
    [titleArray addObject:@"Tour Name"];
    /*
    NSDictionary * dict = [self.completePackageDetail objectAtIndex:0];
    NSDictionary *packageDetail = [dict valueForKey:@"packageDetail"];
    if ([[packageDetail objectForKey:@"tcilHolidayFlightsCollection"] count]>0){
        [titleArray addObject:@"Flights"];
    }
    if ([[packageDetail objectForKey:@"tcilHolidayAccomodationCollection"] count]>0){
        [titleArray addObject:@"Hotels"];
    }
    if ([[packageDetail objectForKey:@"tcilHolidayItineraryCollection"] count]>0){
        [titleArray addObject:@"Itinenary"];
    }
    if ([[packageDetail objectForKey:@"tcilHolidayIncludeExcludeCollection"] count]>0){
        [titleArray addObject:@"Inclusion"];
        [titleArray addObject:@"Exclusion"];
    }
    if ([[packageDetail objectForKey:@"tcilHolidaySightseeingCollection"] count]>0){
        [titleArray addObject:@"Sightseeing"];
    }
    if ([[packageDetail objectForKey:@"tcilHolidayVisaCollection"] count]>0){
        [titleArray addObject:@"Visa"];
    }
    if ([[packageDetail objectForKey:@"tcilHolidayMealCollection"] count]>0){
        [titleArray addObject:@"Meals"];
    }
    */
    
    self.holidayPackageDetailInPdp.airFlag ? [titleArray addObject:@"Flights"] : [self notUsedFunction];
    self.holidayPackageDetailInPdp.accomFlag ? [titleArray addObject:@"Hotels"] : [self notUsedFunction];
    [titleArray addObject:@"Itinenary"];
    [titleArray addObject:@"Inclusion"];
    [titleArray addObject:@"Exclusion"];
    self.holidayPackageDetailInPdp.sightSeeingFlag ? [titleArray addObject:@"Sightseeing"] : [self notUsedFunction];
    self.holidayPackageDetailInPdp.tourMngerFlag ? [titleArray addObject:@"Visa,Passport & Insurance"] : [self notUsedFunction];
    self.holidayPackageDetailInPdp.transferFlag ? [titleArray addObject:@"Transfer"] : [self notUsedFunction];
    self.holidayPackageDetailInPdp.mealsFlag ? [titleArray addObject:@"Meals"] : [self notUsedFunction];
}
-(void)notUsedFunction{
    
}
-(void)setCompareViewWithAlert:(BOOL)flag{
    
    [self.btn_CompareCancelOne addTarget:self action:@selector(crossButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.btn_CompareCancelTwo addTarget:self action:@selector(crossButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    self.btn_CompareHide.titleLabel.transform = CGAffineTransformMakeRotation(-M_PI / 2);
    self.btn_CompareHide.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.btn_addNewPackageOne.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.btn_addNewPackageTwo.titleLabel.textAlignment = NSTextAlignmentCenter;
    
    [self.btn_addNewPackageOne addTarget:self action:@selector(addNewPackButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.btn_addNewPackageTwo addTarget:self action:@selector(addNewPackButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    NSArray *allCompareListArray = [[CoreDataSingleton sharedInstance]getArrayOfObjectForEntityName:CompareListHoliday];
    if ([allCompareListArray count]>0){
        CompareHolidayObject *object = [[CompareHolidayObject alloc]initWithCompareHolidayObject:[allCompareListArray objectAtIndex:0]];
        NSLog(@"btn_CompareCancelOne ! %@", object.packageID);
        NSInteger tagID = [[object.packageID stringByReplacingOccurrencesOfString:@"PKG" withString:@""] integerValue];
        [self.btn_CompareCancelOne setTag:tagID];
        self.lbl_titleOne.text = object.packageName;
        self.lbl_subtitleOne.text = [self getNumberFormatterString:object.packagePrize];
        [self showImageFromURLString:object.packageImgUrl inImageView:self.compareImgViewOne];
    }else{
      self.compareImgViewOne.image = [UIImage imageNamed:@"defaultBanner.png"];
    }
    self.lbl_titleOne.hidden = ([allCompareListArray count]>0) ? NO : YES;
    self.lbl_subtitleOne.hidden = ([allCompareListArray count]>0) ? NO : YES;
    self.btn_CompareCancelOne.hidden = ([allCompareListArray count]>0) ? NO : YES;
    self.upperCompareViewOne.hidden = ([allCompareListArray count]>0) ? YES : NO;
    
    if ([allCompareListArray count]>1){
        CompareHolidayObject *object = [[CompareHolidayObject alloc]initWithCompareHolidayObject:[allCompareListArray objectAtIndex:1]];
        NSLog(@"btn_CompareCancelTwo ! %@", object.packageID);
        NSInteger tagID = [[object.packageID stringByReplacingOccurrencesOfString:@"PKG" withString:@""] integerValue];
         [self.btn_CompareCancelTwo setTag:tagID];
        self.lbl_titleTwo.text = object.packageName;
        self.lbl_subtitleTwo.text = [self getNumberFormatterString:object.packagePrize];
        [self showImageFromURLString:object.packageImgUrl inImageView:self.compareImgViewTwo];
    }else{
      self.compareImgViewTwo.image = [UIImage imageNamed:@"defaultBanner.png"];
    }
    self.lbl_titleTwo.hidden = ([allCompareListArray count]>1) ? NO : YES;
    self.lbl_subtitleTwo.hidden = ([allCompareListArray count]>1) ? NO : YES;
    self.btn_CompareCancelTwo.hidden = ([allCompareListArray count]>0) ? NO : YES;
    self.upperCompareViewTwo.hidden = ([allCompareListArray count]>1) ? YES : NO;
    
    if ([allCompareListArray count]>2 && flag){
        [self deslectPackageAlert:@"Alert" messaga:@"Please deselect any one of selected packages"];
        
    }
    [self.base_table_view reloadData];
    
}
-(void)deslectPackageAlert:(NSString*)title messaga:(NSString*)message{
    UIAlertController *alertView = [UIAlertController alertControllerWithTitle:title
                                                                       message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [alertView dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertView addAction:okAction];
    
    [self presentViewController:alertView animated:YES completion:nil];
}
-(void)showImageFromURLString:(NSString*)imgString inImageView:(UIImageView*)imgView{
    NSURL* urlImage=[NSURL URLWithString:imgString];
    if(urlImage)
    {
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    indicator.center = imgView.center;// it will display in center of image view
    [imgView addSubview:indicator];
    [indicator startAnimating];
    [imgView sd_setImageWithURL:urlImage placeholderImage:[UIImage imageNamed:@"defaultBanner.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
     {
        [indicator stopAnimating];
    }];
    }
}
-(void) crossButtonClicked:(UIButton*)sender
{
    NSLog(@"%@",[NSString stringWithFormat:@"PKG%06ld",(long)sender.tag]);
    NSArray *array = [[CoreDataSingleton sharedInstance] getArrayOfObjectForEntityName:CompareListHoliday];
    NSArray *packIdArray = [[CoreDataSingleton sharedInstance]getdeletedObjectFromPackageListArray:array withPackID:[NSString stringWithFormat:@"PKG%06ld",(long)sender.tag] forEntityName:CompareListHoliday];
    if ([packIdArray count]>0){
        [[[CoreDataSingleton sharedInstance]getManagedObjectContext] deleteObject:[packIdArray firstObject]];
        NSError * error = nil;
        if (![[[CoreDataSingleton sharedInstance]getManagedObjectContext] save:&error]){
            NSLog(@"Error ! %@", error);
        }
    }
    [self setCompareViewWithAlert:NO];
}
-(void) addNewPackButtonClicked:(UIButton*)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
//-(NSString*)getNumberFormatterString:(NSInteger)number{
//    
//    NSNumberFormatter* numberFormatter = [[NSNumberFormatter alloc] init];
//    [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
//    [numberFormatter setGeneratesDecimalNumbers:FALSE];
//    [numberFormatter setMaximumFractionDigits:0];
//    NSString *numberString = [numberFormatter stringFromNumber: [NSNumber numberWithInteger: number]];
//    return numberString;
//}
#pragma mark - Table view data source

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [titleArray count];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dictionary = [self.completePackageDetail objectAtIndex:0];

    NSDictionary *packageDetailDict = [dictionary valueForKey:@"packageDetail"];

    PackageDetailModel *packageModel = [[PackageDetailModel alloc]initWithPackageDetailDict:packageDetailDict];
    
    if ([[[titleArray objectAtIndex:indexPath.section] uppercaseString] isEqualToString:sTourName]){
        PackageDetailTableViewCell* packageCell = (PackageDetailTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"PackageDetailTableViewCell"];
        packageCell.packageDetailDelegate = self;
        [packageCell showAllPackageDetail:self.holidayPackageDetailInPdp];
        return packageCell;
    }else if ([[[titleArray objectAtIndex:indexPath.section] uppercaseString] isEqualToString:sFlights]){
        MainFlightTableViewCell* flightCell = (MainFlightTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"MainFlightTableViewCell"];
//        flightCell.flight_defaultMsg = [packageDetailDict valueForKey:@"isFlightDefaultMsg"];
        flightCell.mainFlightDelaget = self;
        [flightCell loadFlightDataFromPackageModel:packageModel holidayPkgModel:self.holidayPackageDetailInPdp withAray:self.holidayPackageDetailInPdp.tcilHolidayFlightsCollection];
        return flightCell;
    }else if ([[[titleArray objectAtIndex:indexPath.section] uppercaseString] isEqualToString:sHotels]){
        MainHotelTableViewCell* hotelCell = (MainHotelTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"MainHotelTableViewCell"];
        hotelCell.mainHotelDelegate = self;
        hotelCell.packageNum = 1;
        hotelCell.isShowViewMore = NO;
        [hotelCell loadHotelDataFromPackageModel:packageModel forHolidayPackageDetail:self.holidayPackageDetailInPdp];
        return hotelCell;
    }else if ([[[titleArray objectAtIndex:indexPath.section] uppercaseString] isEqualToString:sItininery]){
        ItinenaryDetailTableViewCell* itinenaryCell = (ItinenaryDetailTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"ItinenaryDetailTableViewCell"];
        [itinenaryCell loadDataFromPackageModel:packageModel];
        itinenaryCell.itinenaryDelegate = self;
        return itinenaryCell;
    }else if ([[[titleArray objectAtIndex:indexPath.section] uppercaseString] isEqualToString:sSightSeeing]){
        MainHotelTableViewCell* hotelCell = (MainHotelTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"MainHotelTableViewCell"];
        hotelCell.mainHotelDelegate = self;
        hotelCell.packageNum = 2;
        hotelCell.sectionTitle = sSightSeeing;
        hotelCell.isShowViewMore = NO;
        [hotelCell loadSightSeeingDataFromPackageModel:packageModel forHolidayPackageDetail:self.holidayPackageDetailInPdp];
        return hotelCell;
    }
    OtherDetailTableViewCell* infoCell = (OtherDetailTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"OtherDetailTableViewCell"];
    infoCell.otherInfoDelegate = self;
    infoCell.sectionTitle = [titleArray objectAtIndex:indexPath.section];
    [infoCell loadDataFromPackageModel:packageModel forHolidayPackageDetail:self.holidayPackageDetailInPdp forSection:[titleArray objectAtIndex:indexPath.section]];
    return infoCell;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    NSDictionary *dictionary = [self.completePackageDetail objectAtIndex:0];

    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.base_table_view.frame.size.width*0.8, 60)];
    headerView.backgroundColor = [UIColor whiteColor];
    UILabel *lbl_title = [[UILabel alloc]initWithFrame:CGRectMake(20, 0, headerView.frame.size.width, 50)];
    [lbl_title setTextColor:[UIColor blackColor]];
    NSString *headerTitlr = [[titleArray objectAtIndex:section] isEqualToString:@"Visa,Passport & Insurance"] ? @"Visa,Passport & Insurance" : [titleArray objectAtIndex:section];
    [lbl_title setText:headerTitlr];
    lbl_title.font = [UIFont fontWithName:@"Lato-Heavy" size:20];
    [headerView addSubview:lbl_title];
    HeaderTableViewCell *newHeaderView = (HeaderTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"HeaderTableViewCell"];
    newHeaderView.headerDelegate = self;
    newHeaderView.compPackageDetailDict = dictionary;
    [newHeaderView loadImageFromHolidayPackageDetail:self.holidayPackageDetailInPdp withHolidayObj:self.holidayObjInPdp];
    if (section == 0){
        return  newHeaderView;
    }
    return headerView;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([[[titleArray objectAtIndex:indexPath.section] uppercaseString] isEqualToString:sItininery]){
        NSLog(@"Its in sHotels sItininery");
        return  350;
        
    }
//    if ([[[titleArray objectAtIndex:indexPath.section] uppercaseString] isEqualToString:sFlights]){
//        NSLog(@"Its in sHotels sItininery");
//        return  150;
//    }
    if ([[[titleArray objectAtIndex:indexPath.section] uppercaseString] isEqualToString:sFlights]){
        NSLog(@"Its in sFlights");
        return  200;

    }
    if ( [[[titleArray objectAtIndex:indexPath.section] uppercaseString] isEqualToString:sHotels]){
        NSLog(@"Its in sHotels");
        return  260;

    }
    if (indexPath.section == 0){
        NSLog(@"Its in first ROW");
        return  200;
    }
    return  220;
//    return  (indexPath.row == 0) ? 220 : 300;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return (section == 0) ? 250 : 50;
}

#pragma mark - PackageDetailTableViewCell Delagtes
-(void)clickViewIndex:(NSInteger)viewIndex inclusionStr:(NSString *)string{
    if ([titleArray containsObject:string]){
    [self.base_table_view scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:[titleArray indexOfObject:string]] atScrollPosition:UITableViewScrollPositionBottom animated:NO];
    }

}
#pragma mark - OtherDetailTableViewCell Delagtes

-(void)sendInfoArrayOfSection:(NSString *)sectionTitle withDiscriptionArray:(NSMutableArray *)detailArray inclusionArray:(NSMutableArray *)inclusionStrArray exclusionArray:(NSMutableArray *)exclusionStringArray withPackageModel:(PackageDetailModel *)packageModel{
    ViewMoreVC *controller = [[ViewMoreVC alloc] initWithNibName:@"ViewMoreVC" bundle:nil];
    controller.sectionTitLe = sectionTitle;
    controller.allDescriptionArray = detailArray;
    controller.inclusionDescriptionArray = inclusionStrArray;
    controller.exclusionDescriptionArray = exclusionStringArray;
    controller.packageModel = packageModel;
    controller.holidayPkgModelInViewMore = self.holidayPackageDetailInPdp;
    controller.isOfferApplicable = isOfferAvailabel;
    controller.lblaPrize = self.lbl_packagePrise.text;
    [self.navigationController pushViewController:controller animated:YES];
}
- (void)sendInfoOfSection:(NSString*)sectionTitle withDiscription:(NSString*)detailString inclusion:(NSString*)inclusionStr exclusion:(NSString*)exclusionString withPackageModel:(PackageDetailModel*)packageModel{
    ViewMoreVC *controller = [[ViewMoreVC alloc] initWithNibName:@"ViewMoreVC" bundle:nil];
    controller.sectionTitLe = sectionTitle;
    controller.inclusionDescription = inclusionStr;
    controller.exclusionDescription = exclusionString;
    controller.firstDescription = detailString;
    controller.packageModel = packageModel;
    controller.holidayPkgModelInViewMore = self.holidayPackageDetailInPdp;
    controller.isOfferApplicable = isOfferAvailabel;
    controller.lblaPrize = self.lbl_packagePrise.text;
    [self.navigationController pushViewController:controller animated:YES];
}
- (void)sendInfoArrayInDict:(NSDictionary*)dictionary withsectionTitle:(NSString*)sectionTitle withPackageModel:(PackageDetailModel*)packageModel{
    ViewMoreVC *controller = [[ViewMoreVC alloc] initWithNibName:@"ViewMoreVC" bundle:nil];
    controller.sectionTitLe = sectionTitle;
    controller.lblaPrize = self.lbl_packagePrise.text;
    controller.isOfferApplicable = isOfferAvailabel;
    controller.holidayPkgModelInViewMore = self.holidayPackageDetailInPdp;
    controller.packageModel = packageModel;
    [self.navigationController pushViewController:controller animated:YES];
}
- (void)showHotelWithSectionTitle:(NSString *)sectionTitle withPackageModel:(PackageDetailModel *)packageModel{
//    ViewMoreVC *controller = [[ViewMoreVC alloc] initWithNibName:@"ViewMoreVC" bundle:nil];
//    controller.sectionTitLe = sectionTitle;
//    controller.lblaPrize = self.lbl_packagePrise.text;
//    controller.isOfferApplicable = isOfferAvailabel;
//    controller.holidayPkgModelInViewMore = self.holidayPackageDetailInPdp;
//    controller.packageModel = packageModel;
//    [self.navigationController pushViewController:controller animated:YES];
    ViewMoreOtherVC *controller = [[ViewMoreOtherVC alloc] initWithNibName:@"ViewMoreOtherVC" bundle:nil];
    controller.sectionTitLe = sectionTitle;
    controller.holidayPkgModelInViewMore = self.holidayPackageDetailInPdp;
    controller.packageModel = packageModel;
    [self.navigationController pushViewController:controller animated:YES];
}
- (void)showFlightWithSectionTitle:(NSString*)sectionTitle withPackageModel:(PackageDetailModel*)packageModel{
    ViewMoreVC *controller = [[ViewMoreVC alloc] initWithNibName:@"ViewMoreVC" bundle:nil];
    controller.sectionTitLe = sectionTitle;
    controller.lblaPrize = self.lbl_packagePrise.text;
    controller.isOfferApplicable = isOfferAvailabel;
    controller.holidayPkgModelInViewMore = self.holidayPackageDetailInPdp;
    controller.packageModel = packageModel;
    [self.navigationController pushViewController:controller animated:YES];
}
#pragma mark - itinerayScreenVC Delagtes

-(void)jumpToItinenary{
    itinerayScreenVC *itinerayVC = [[itinerayScreenVC alloc] initWithNibName:@"itinerayScreenVC" bundle:nil];
    itinerayVC.holidayPackageDetail = self.holidayPackageDetailInPdp;
     [self.navigationController pushViewController:itinerayVC animated:YES];
}

#pragma mark -Mobile Number Pop Up Delegate

- (void)goButtonClickedWithMobileNumberInAppDelegate:(NSString *)mobileNumber
{
    [self netCoreBookNowEventWihtDetails:mobileNumber];
    [mobileNumberViewController dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate registerWithCMSServer];
}


#pragma mark - Jump To Other View Controllers

-(void)jumpToPDPCalculateAcren{
    
    if([self.holidayPackageDetailInPdp.strPackageType caseInsensitiveCompare:@"International"]==NSOrderedSame)
    {
        [FIRAnalytics logEventWithName:@"International_Holiday_Book_Now_PDP" parameters:@{kFIRParameterItemName:[NSString stringWithFormat:@"PackageName :%@, PackageType :%@",self.holidayPackageDetailInPdp.strPackageName,self.holidayPackageDetailInPdp.strPackageType]}];
    }
    else
    {
        [FIRAnalytics logEventWithName:@"Indian_Holiday_Book_Now_PDP" parameters:@{kFIRParameterItemName:[NSString stringWithFormat:@"PackageName :%@, PackageType :%@",self.holidayPackageDetailInPdp.strPackageName,self.holidayPackageDetailInPdp.strPackageType]}];
    }
    
//    PDPCalculatePrizeVC *controller = [[PDPCalculatePrizeVC alloc] initWithNibName:@"PDPCalculatePrizeVC" bundle:nil];
//    controller.holidayPkgDetailInPdpCalculate = self.holidayPackageDetailInPdp;
    
//    PDPCalculateCostVC *controller = [[PDPCalculateCostVC alloc] initWithNibName:@"PDPCalculateCostVC" bundle:nil];
//    controller.holidayPkgDetailInPdpCost = self.holidayPackageDetailInPdp;
    
    PackagePricingViewController *controller = [[PackagePricingViewController alloc] initWithNibName:@"PackagePricingViewController" bundle:nil];
    controller.delegates = self;
    controller.completePackageDetailDict = [self.completePackageDetail objectAtIndex:0];
    
    [self.navigationController pushViewController:controller animated:YES];
    
}
#pragma mark - Package Pricing Delegate


- (void)addMobileForLeadCreationWithMobNumber:(NSString *)mobNumber{
    [self leadCreationInSRPWithMobNumber:mobNumber];
}


#pragma mark - Lead Creation in PDP

-(void)leadCreationInSRPWithMobNumber:(NSString *)mobNumber{
//    NSString *mobNumber = [[NSUserDefaults standardUserDefaults]objectForKey:kuserDefaultTC_MobileNumber];
    NSString *userEmailID = [[NSUserDefaults standardUserDefaults]objectForKey:kuserDefaultUserId];
    CoreUtility *coreobj=[CoreUtility sharedclassname];
    if (mobNumber.length > 0){
        [coreobj.strPhoneNumber addObject:mobNumber];
        NSMutableDictionary *dictOfData = [[NSMutableDictionary alloc]init];
        [dictOfData setValue:self.holidayPackageDetailInPdp.strPackageId forKey:@"package_id"];
        [dictOfData setValue:(userEmailID == nil) ? @"" : userEmailID forKey:@"email"];
        [dictOfData setValue:(mobNumber == nil) ? @"" : mobNumber forKey:@"mobile"];
        [dictOfData setValue:self.holidayPackageDetailInPdp.strPackageSubType forKey:@"sub_product_type"];
        [dictOfData setValue:self.holidayPackageDetailInPdp.strPackageType forKey:@"request_type"];
        activityLoadingView = [LoadingView loadingViewInView:self.view.superview.superview withString:@"" andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE_WHITE]];
        ///https://thomascookindia.custhelp.com/cgi-bin/thomascookindia.cfg/php/custom/mobile_app_lead_holiday_ios_app_leads.php?email=narendra_k2@yahoo.com&mobile=9811976670&request_type=Domestic&sub_product_type=GIT&package_id=PKG130755
        
        NSString  *urlString  =KUrlHolidayCreateLead;
        //@"https://thomascookindia.custhelp.com/cgi-bin/thomascookindia.cfg/php/custom/mobile_app_lead_holiday_ios_app_leads.php";
        NSString *packageSubType = self.holidayPackageDetailInPdp.strPackageSubType;
        if ([[self.holidayPackageDetailInPdp.strPackageSubType lowercaseString] isEqualToString:@"fit fixed"]){
            packageSubType = @"fit";
        }
        NSString *queryParam = [NSString stringWithFormat:@"?mobile=%@&request_type=%@&sub_product_type=%@&package_id=%@",mobNumber,self.holidayPackageDetailInPdp.strPackageType,packageSubType,self.holidayPackageDetailInPdp.strPackageId];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        NSString *urlStringTotal = [NSString stringWithFormat:@"%@%@",urlString,queryParam];
        NSString * encodedString = [urlStringTotal stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        NSURL *url = [NSURL URLWithString:encodedString];
        [request setURL:url];
        [request setHTTPMethod:@"GET"];
        NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
            dispatch_async(dispatch_get_main_queue(), ^{
                [activityLoadingView removeFromSuperview];
                NSString *requestReply = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
                NSLog(@"Response :- %@",requestReply);
                coreobj.leadSubmitted = YES;
                [self goButtonClickedWithMobileNumberInAppDelegate:mobNumber];
            });
            
        }] resume];
    }
    [payLoadViewPageDetail setObject:@"yes" forKey:@"s^BOOK_NOW"];
    [self netCoreViewDetailPage];
}

#pragma mark - HeaderTableViewCell Delagtes

-(void)comparePress:(BOOL)flags{
    [self setCompareViewWithAlert:flags];
    [self showAndHidCompareView:NO];
}
- (void)likePress:(BOOL)flags{
    [self showToastsWithTitle:(flags) ? @" Added In Wishlist " : @" Removed From Wishlist "];
    [self.base_table_view reloadData];
}
-(void)showViewGallery{
    NSLog(@"VIEW GALLERY");
    if([self.holidayPackageDetailInPdp.strPackageType caseInsensitiveCompare:@"International"]==NSOrderedSame)
    {
        [FIRAnalytics logEventWithName:@"International_Holiday_View_Gallery_PDP" parameters:@{kFIRParameterItemName:[NSString stringWithFormat:@"PackageName :%@, PackageType :%@, PackageID :%@",self.holidayPackageDetailInPdp.strPackageName,self.holidayPackageDetailInPdp.strPackageType,self.holidayPackageDetailInPdp.strPackageId]}];
    }
    else
    {
        [FIRAnalytics logEventWithName:@"Indian_Holiday_View_Gallery_PDP" parameters:@{kFIRParameterItemName:[NSString stringWithFormat:@"PackageName :%@, PackageType :%@, PackageID :%@",self.holidayPackageDetailInPdp.strPackageName,self.holidayPackageDetailInPdp.strPackageType,self.holidayPackageDetailInPdp.strPackageId]}];
        
    }
    PhotoCollectionVC *controller = [[PhotoCollectionVC alloc] initWithNibName:@"PhotoCollectionVC" bundle:nil];
    controller.packageDetailInPhotoGallery = self.holidayPackageDetailInPdp;
    [self.navigationController pushViewController:controller animated:YES];
}
-(void)sharePress{
    NSString *title = [NSString stringWithFormat:@"%@",self.headerName];
    NSString *url = [NSString stringWithFormat:@"URL : https://www.thomascook.in/holidays/international-tour-packages/europe-tour-packages"];
    UIImage *tourImage = [self getImageFromPackageURL:self.holidayPackageDetailInPdp];
    NSArray *itemsToShare ;
    if (tourImage != nil){
    itemsToShare = @[title,url,tourImage];
    }else{
       itemsToShare = @[title,url];
    }
    UIActivityViewController *activityIndication = [[UIActivityViewController alloc]initWithActivityItems:itemsToShare applicationActivities:nil];
     [self presentViewController:activityIndication animated:YES completion:nil];
}
-(void)setImageName:(NSString *)imgString{
    self.packAgeImgString = imgString;
}
-(UIImage*)getImageFromPackageURL:(HolidayPackageDetail*)pkgDetails{
    NSArray *imgDetailInfoArray = pkgDetails.arrayPackageImagePathList;
    if (imgDetailInfoArray.count != 0)
    {
        NSDictionary *imageDict = imgDetailInfoArray[0];
        NSString* imageName = [[imageDict valueForKey:@"path"] stringByReplacingOccurrencesOfString:@" " withString:@"_"];
        NSString *imageUrlString = [NSString stringWithFormat:@"%@/images/holidays/%@/photos/%@",kUrlForImage,pkgDetails.strPackageId,imageName];
        NSURL* urlImage=[NSURL URLWithString:imageUrlString];
        if(urlImage)
        {
            NSData * imageData = [[NSData alloc] initWithContentsOfURL: urlImage];
            return [UIImage imageWithData: imageData];
        }
        return nil;
    }
    return nil;
}
#pragma mark - Saving In Core Data
-(void)saveObjectInCoreData{
    NSDictionary *dict =  @{@"packageID" : self.holidayPackageDetailInPdp.strPackageId, @"packageName" : self.holidayPackageDetailInPdp.strPackageName, @"packagePrize" : [NSNumber numberWithInteger:mainStartPrice] ,@"packageImgUrl" : self.packAgeImgString};
    NSLog(@"%@",dict);
    RecentHolidayObject *object = [[RecentHolidayObject alloc]initWithRecentObject:dict];
    [[CoreDataSingleton sharedInstance] saveRecentObjectInCoreDataForEntity:RecentViewHoliday withObject:object];
}

#pragma mark - Other Methods

-(void)showAndHidCompareView:(BOOL)isShow{
   [UIView transitionWithView:self.baseCompareView duration:0.8 options:UIViewAnimationOptionCurveEaseIn animations:^(void){
       self.baseCompareView.hidden = isShow;
    } completion:nil];
}

-(void)checkBeforeCalculate{
//    NSString *mobNumber = [[NSUserDefaults standardUserDefaults]objectForKey:kuserDefaultTC_MobileNumber];
//    [self leadCreationInSRPWithMobNumber:mobNumber];
    [self jumpToPDPCalculateAcren];
    
    /*
    CoreUtility *coreobj=[CoreUtility sharedclassname];
    
    if (coreobj.strPhoneNumber.count>0)
    {
          NSString *phoneNumber = coreobj.strPhoneNumber[0];
        [self netCoreBookNowEventWihtDetails:phoneNumber];
        
        if (coreobj.leadSubmitted)
        {
            [self jumpToPDPCalculateAcren];
        }
        else
        {
          
            NSMutableDictionary *dictOfData = [[NSMutableDictionary alloc]init];
            [dictOfData setValue:self.holidayPackageDetailInPdp.strPackageId forKey:@"package_id"];
            [dictOfData setValue:@"" forKey:@"email"];
            [dictOfData setValue:phoneNumber forKey:@"mobile"];
            [dictOfData setValue:self.holidayPackageDetailInPdp.strPackageSubType forKey:@"sub_product_type"];
            [dictOfData setValue:self.holidayPackageDetailInPdp.strPackageType forKey:@"request_type"];
            
            
            activityLoadingView = [LoadingView loadingViewInView:self.view.superview.superview withString:@"" andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
            
            ///https://thomascookindia.custhelp.com/cgi-bin/thomascookindia.cfg/php/custom/mobile_app_lead_holiday_ios_app_leads.php?email=narendra_k2@yahoo.com&mobile=9811976670&request_type=Domestic&sub_product_type=GIT&package_id=PKG130755
            
            NSString  *urlString  =KUrlHolidayCreateLead;
            //@"https://thomascookindia.custhelp.com/cgi-bin/thomascookindia.cfg/php/custom/mobile_app_lead_holiday_ios_app_leads.php";
            
            NSString *packageSubType = self.holidayPackageDetailInPdp.strPackageSubType;
            
            if ([[self.holidayPackageDetailInPdp.strPackageSubType lowercaseString] isEqualToString:@"fit fixed"])
            {
                packageSubType = @"fit";
            }

            
        NSString *queryParam = [NSString stringWithFormat:@"?mobile=%@&request_type=%@&sub_product_type=%@&package_id=%@",phoneNumber,self.holidayPackageDetailInPdp.strPackageType,packageSubType,self.holidayPackageDetailInPdp.strPackageId];
            
            
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
            
            NSString *urlStringTotal = [NSString stringWithFormat:@"%@%@",urlString,queryParam];
            
            NSString * encodedString = [urlStringTotal stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
            
            
            NSURL *url = [NSURL URLWithString:encodedString];
            
            [request setURL:url];
            [request setHTTPMethod:@"GET"];
            
            NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
            [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
              {
                  dispatch_async(dispatch_get_main_queue(), ^{
                      
                      [activityLoadingView removeFromSuperview];
                      
                      NSString *requestReply = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
                      
                      coreobj.leadSubmitted = YES;
                      [self jumpToPDPCalculateAcren];
                  });
                  
              }] resume];

        }
       
    }
    else
    {
        NSUserDefaults *userData = [NSUserDefaults standardUserDefaults];
        NSString *statusForLogin= [userData valueForKey:kLoginStatus];
        
        mobileNumberViewController = [[MobileNumberPopUpViewController alloc] initWithNibName:@"MobileNumberPopUpViewController" bundle:nil];
        mobileNumberViewController.delegate = self;
        mobileNumberViewController.packageDetail = self.holidayPackageDetailInPdp;
        mobileNumberViewController.view.frame = CGRectMake(0, 0, 309, 173);
        
        if ([statusForLogin isEqualToString:kLoginSuccess])
        {
            NSString *phoneNo = [userData valueForKey:kPhoneNo];
            mobileNumberViewController.txtViewMobileNumber.text = phoneNo;
        }
        [self presentPopupViewController:mobileNumberViewController animationType:MJPopupViewAnimationFade];
    }
    
     [payLoadViewPageDetail setObject:@"yes" forKey:@"s^BOOK_NOW"];
     [self netCoreViewDetailPage];
     */
    
}
-(void)netCoreViewDetailPage
{
    if (![payLoadViewPageDetail objectForKey:@"s^OVERVIEW"])
    {
        [payLoadViewPageDetail setObject:@"no" forKey:@"s^OVERVIEW"];
    }
    if (![payLoadViewPageDetail objectForKey:@"s^DETAILS"])
    {
        [payLoadViewPageDetail setObject:@"no" forKey:@"s^DETAILS"];
    }
    
    if (![payLoadViewPageDetail objectForKey:@"s^ITINERARY"])
    {
         [payLoadViewPageDetail setObject:@"no" forKey:@"s^ITINERARY"];
    }
    
    //28-02-2018
    /*if(![payLoadViewPageDetail objectForKey:@"s^BOOK_NOW"])
    {
        [payLoadViewPageDetail setObject:@"no" forKey:@"s^BOOK_NOW"];
    }*/
    
    if(![payLoadViewPageDetail objectForKey:@"s^EMAIL_FORM"])
    {
        [payLoadViewPageDetail setObject:@"no" forKey:@"s^EMAIL_FORM"];
    }
    
    if(![payLoadViewPageDetail objectForKey:@"s^CALL"])
    {
        [payLoadViewPageDetail setObject:@"no" forKey:@"s^CALL"];
    }
    
    [payLoadViewPageDetail setObject:self.holidayPackageDetailInPdp.strPackageId forKey:@"s^PACKAGE_ID"];
    [payLoadViewPageDetail setObject:@"App" forKey:@"s^SOURCE"];
    [payLoadViewPageDetail setObject:[NSNumber numberWithInt:self.holidayPackageDetailInPdp.packagePrise] forKey:@"i^PACKAGE_PRICE"];
    
    if ([self.completePackageDetail count]>0)
    {
        NSDictionary *packageDict = [[NSDictionary alloc] init];
        packageDict = [self.completePackageDetail objectAtIndex:0];
        
        NSString *packageType = [packageDict objectForKey:@"pkgType"];
        NSArray *inclusionArray = [packageDict objectForKey:@"inclusionList"];
        NSArray *excludeArray = [packageDict objectForKey:@"excludeList"];
        NSString *thingsNote = [packageDict objectForKey:@"thingsNote"];
        
        
         [payLoadViewPageDetail setObject:@"no" forKey:@"s^INCLUSION"];
        
        if (inclusionArray)
        {
            if (inclusionArray.count !=0)
            {
                [payLoadViewPageDetail setObject:@"yes" forKey:@"s^INCLUSION"];
            }
        }
        
        [payLoadViewPageDetail setObject:@"no" forKey:@"s^EXCLUSION"];
        
        if (excludeArray)
        {
            if (excludeArray.count !=0)
            {
                [payLoadViewPageDetail setObject:@"yes" forKey:@"s^EXCLUSION"];
            }
        }

        if (thingsNote != nil && ![thingsNote isEqualToString:@""])
        {
            [payLoadViewPageDetail setObject:thingsNote forKey:@"s^THINGS_TO_NOTE"];//28-02-2018
        }
        
        [payLoadViewPageDetail setObject:@"" forKey:@"s^THINGS"];//28-02-2018
        
        
        if (packageType != nil && ![packageType isEqualToString:@""])
        {
            [payLoadViewPageDetail setObject:packageType forKey:@"s^TYPE"];//28-02-2018
        }
        
    }
    
//     [NetCoreAnalyticsVC getNetCorePushNotificationDetailDictionary:payLoadViewPageDetail withPayloadCount:106];
    
    [NetCoreAnalyticsVC getNetCorePushNotificationDetailDictionary:payLoadViewPageDetail withPayloadCount:126]; //28-02-2018
}
-(void)netCoreBookNowEventWihtDetails:(NSString*)mobileNumber
{
    NSMutableDictionary *payloadList =  [NSMutableDictionary dictionary];
    [payloadList setObject:mobileNumber forKey:@"s^MobileNo"];
    [payloadList setObject:self.holidayPackageDetailInPdp.strPackageType forKey:@"s^PackageType"];
    [payloadList setObject:self.holidayPackageDetailInPdp.strPackageId forKey:@"s^PackageID"];
    [payloadList setObject:self.holidayPackageDetailInPdp.stringRegion forKey:@"s^Destination"];
    [payloadList setObject:self.holidayPackageDetailInPdp.strPackageName forKey:@"s^PackageName"];
    [payloadList setObject:[NSNumber numberWithInt:self.holidayPackageDetailInPdp.packagePrise] forKey:@"i^PackagePrice"];
    
    
    [NetCoreAnalyticsVC getNetCorePushNotificationDetailDictionary:payloadList withPayloadCount:141]; //28-02-2018
    
  
    
}
#pragma mark - Button Actions

- (IBAction)btn_BackPress:(id)sender {
    
    NSArray *array = [[CoreDataSingleton sharedInstance] getArrayOfObjectForEntityName:RecentViewHoliday];
    if ([array count] > 15){
        [[[CoreDataSingleton sharedInstance]getManagedObjectContext] deleteObject:[array firstObject]];
        NSError * error = nil;
        if (![[[CoreDataSingleton sharedInstance]getManagedObjectContext] save:&error]){
            NSLog(@"Error ! %@", error);
        }
    }
    if (!_isFromHomePage && ![[CoreDataSingleton sharedInstance] checkIsPackagePresentInEntityWithName:RecentViewHoliday withPackageID:self.holidayPackageDetailInPdp.strPackageId]){
        [self saveObjectInCoreData];
    }
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)btn_ShowOffer:(id)sender {
    /*
    if (isOfferAvailabel){
    [self showAndHideOfferView:NO];
    }else{
        [self showToastsWithTitle:@"No Offers Available"];
    }
    */
    OffersDetailsVC *viewVC = [[OffersDetailsVC alloc] init];
    viewVC.holidayInOffers = self.holidayObjInPdp;
    viewVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:viewVC animated:YES completion:nil];
    
}
- (IBAction)btn_CancelOffer:(id)sender {
    [self showAndHideOfferView:YES];
}
-(void)showAndHideOfferView:(BOOL)flag{
    CATransition *animation = [CATransition animation];
    animation.type = kCATransitionFade;
    animation.duration = 0.5;
    [self.base_viewOffer.layer addAnimation:animation forKey:nil];
    self.base_viewOffer.hidden = flag;
}
- (IBAction)btn_PressCalculate:(id)sender {
    [self checkBeforeCalculate];
}
- (IBAction)btn_CallPress:(id)sender {
//    [self callForHolidayEnquiry];
    
    TalkToExpertVC *viewVC = [[TalkToExpertVC alloc] init];
    viewVC.talkToExprHPackage = self.holidayPackageDetailInPdp;
    viewVC.delegate = self;
    viewVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:viewVC animated:YES completion:nil];
    
    [payLoadViewPageDetail setObject:@"yes" forKey:@"s^CALL"];
    [self netCoreViewDetailPage];
//     [AppUtility callToTCFromPhaseTwo];
}
- (IBAction)btn_showCompareAction:(id)sender {
    [self showAndHidCompareView:YES];
}
- (IBAction)btn_jumpCompareViewAction:(id)sender {
    CompareScreenVC *controller = [[CompareScreenVC alloc] initWithNibName:@"CompareScreenVC" bundle:nil];;
    [self.navigationController pushViewController:controller animated:YES];
}
#pragma mark - Delegate of TalkToExpertView

-(void)clickedOptions:(NSInteger)indexNum{
    if (indexNum==0){
        [self requestACall];
    }else if (indexNum==1){
        [self callForHolidayEnquiry];
    }else{
        NSLog(@"No any option clcik");
    }
}

-(void)requestACall{
   
    if ([self.holidayPackageDetailInPdp.strPackageType caseInsensitiveCompare:@"international"]==NSOrderedSame)
    {
        [FIRAnalytics logEventWithName:@"International_Holidays_Email_PDP" parameters:@{kFIRParameterItemID:self.holidayPackageDetailInPdp.strPackageName}];
        [FIRAnalytics logEventWithName:@"International_Holidays_Email_PDP" parameters:@{kFIRParameterItemID:self.holidayPackageDetailInPdp.strPackageType}];
    }
    else{
        [FIRAnalytics logEventWithName:@"Indian_Holidays_Email_PDP" parameters:@{kFIRParameterItemID:self.holidayPackageDetailInPdp.strPackageName}];
        [FIRAnalytics logEventWithName:@"Indian_Holidays_Email_PDP" parameters:@{kFIRParameterItemID:self.holidayPackageDetailInPdp.strPackageType}];
    }
    
    RequestViewVC *requestVC = [[RequestViewVC alloc] initWithNibName:@"RequestViewVC" bundle:nil];
     requestVC.requestHolidayPackage = self.holidayPackageDetailInPdp;
    [self.navigationController pushViewController:requestVC animated:YES];
    
}


#pragma mark - Adding Methoda used in next screen

-(NSArray*)getCommonCitiesFromPackage:(HolidayPackageDetail*)packageDetail packageTypes:(NSString*)packageType withAccomType:(int)accomType{
    
    NSArray *hubList = [NSArray new];
    if([packageType isEqualToString:kpackageTypeFITDomestic]||[packageType isEqualToString:kpackageTypeFITInternational]){
        hubList = packageDetail.arraytcilHolidayPriceCollection;
        
    }else{
        hubList = packageDetail.arrayLtPricingCollection;
    }
    NSArray *filteredarray;
    filteredarray = hubList;
    filteredarray = [hubList filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(packageClassId == %@)", [NSString stringWithFormat:@"%d",accomType]]];
    NSArray* uniqueValues;
    if([packageType isEqualToString:kpackageTypeFITDomestic]||[packageType isEqualToString:kpackageTypeFITInternational]){
        uniqueValues = [filteredarray valueForKeyPath:[NSString stringWithFormat:@"@distinctUnionOfObjects.hubCityCode.cityName"]];
    }else{
        uniqueValues = [filteredarray valueForKeyPath:[NSString stringWithFormat:@"@distinctUnionOfObjects.hubCode.cityName"]];
    }
    NSMutableArray * uniqueDataArray = [[NSMutableArray alloc] init];
    for (NSString * city in uniqueValues)
    {
        NSExpression *lhs;
        
        if([packageType isEqualToString:kpackageTypeFITDomestic]||[packageType isEqualToString:kpackageTypeFITInternational]){
            lhs = [NSExpression expressionForKeyPath:@"hubCityCode.cityName"];
        }else{
            lhs = [NSExpression expressionForKeyPath:@"hubCode.cityName"];
        }
        NSExpression *rhs = [NSExpression expressionForConstantValue:city];
        NSPredicate * finalPredicate = [NSComparisonPredicate
                                        predicateWithLeftExpression:lhs
                                        rightExpression:rhs
                                        modifier:NSDirectPredicateModifier
                                        type:NSContainsPredicateOperatorType
                                        options:NSCaseInsensitivePredicateOption];
        
        NSArray * filteredChannelArray   = [[filteredarray filteredArrayUsingPredicate:finalPredicate] mutableCopy];

        if (filteredChannelArray.count > 0) {
            [uniqueDataArray addObject:[filteredChannelArray objectAtIndex:0]];
        }
    }
    NSSortDescriptor *nameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"hubCode.cityName"  ascending:YES];
    NSArray *sortDescriptors = @[nameDescriptor];
    
    NSArray * ordered = [uniqueDataArray sortedArrayUsingDescriptors:sortDescriptors];
    
    NSMutableArray *citiesAray = [[NSMutableArray alloc]init];
    for (int j =0 ; j < ordered.count; j++)
    {
        NSDictionary *cityDict = ordered[j];
        
        NSDictionary *hubCodeDict;
        if([packageType isEqualToString:kpackageTypeFITDomestic]||[packageType isEqualToString:kpackageTypeFITInternational])
        {
            hubCodeDict = [cityDict valueForKey:@"hubCityCode"];
            
        }else{
            hubCodeDict = [cityDict valueForKey:@"hubCode"];
            
        }
        
        NSString *titleString = [hubCodeDict valueForKey:@"cityName"];
        
//        NSString *departFromLocal = [hubCodeDict valueForKey:@"cityCode"];
//
//        NSString *ltCodeString = [cityDict valueForKey:@"ltItineraryCode"];
        
        [citiesAray addObject:titleString];
        
    }
    if ([citiesAray count]==0){
        [citiesAray addObject:@"Mumbai"];
    }
    return [citiesAray copy];
}
@end
