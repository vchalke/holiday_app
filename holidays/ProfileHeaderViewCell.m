//
//  ProfileHeaderViewCell.m
//  holidays
//
//  Created by Kush_Team on 26/05/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "ProfileHeaderViewCell.h"
#import "UIImageView+WebCache.h"
@implementation ProfileHeaderViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
//    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
//    [userIdForGooglePlusSignIn setObject:imageURL.absoluteString forKey:kUserImageString]
    
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)btn_logoutClick:(id)sender {
    [self.headerDelegaet logOutButtonClicked];
    
}
-(void)setProfileImage:(NSString*)imgStr{
    self.img_user.layer.cornerRadius = (self.baseViews.frame.size.height*0.5)/2;
    NSURL* urlImage=[NSURL URLWithString:imgStr];
        if(urlImage)
        {
            UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            indicator.center = self.img_user.center;// it will display in center of image view
            [self.img_user addSubview:indicator];
            [indicator startAnimating];
            dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0ul);
            dispatch_async(queue, ^{
    //        dispatch_async(dispatch_get_main_queue(), ^{
               [self.img_user sd_setImageWithURL:urlImage placeholderImage:[UIImage imageNamed:@"defaultBanner.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
                {
                   [indicator stopAnimating];
               }];
            });
            
        }
}
-(void)setUserProfileInfo{
    NSDictionary *userdetail = [[NSUserDefaults standardUserDefaults] objectForKey:kLoginUserDetails];
    NSLog(@"UserDetailsDict %@", userdetail);
    self.lbl_userName.text = [NSString stringWithFormat:@"%@ %@",[userdetail objectForKey:@"fname"],[userdetail objectForKey:@"lName"]];
    self.lbl_emailId.text = [NSString stringWithFormat:@"%@",[userdetail objectForKey:@"email"]];
    self.img_user.layer.cornerRadius = (self.baseViews.frame.size.height*0.5)/2;
    NSURL* urlImage=[NSURL URLWithString:[NSString stringWithFormat:@"%@",[userdetail objectForKey:@"profilePicUrl"]]];
        if(urlImage)
        {
            UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            indicator.center = self.img_user.center;// it will display in center of image view
            [self.img_user addSubview:indicator];
            [indicator startAnimating];
            dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0ul);
            dispatch_async(queue, ^{
    //        dispatch_async(dispatch_get_main_queue(), ^{
               [self.img_user sd_setImageWithURL:urlImage placeholderImage:[UIImage imageNamed:@"defaultBanner.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
                {
                   [indicator stopAnimating];
               }];
            });
            
        }
    
}
@end
