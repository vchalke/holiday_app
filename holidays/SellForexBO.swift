//
//  SellForexBO.swift
//  holidays
//
//  Created by Komal Katkade on 12/23/17.
//  Copyright © 2017 Mobicule. All rights reserved.
//

import Foundation
class SellForexBO
{
    //product VC
    var productArray : [ProductBO] = []
    var mobileNumber : String = ""
    var customerState : String = ""
    var customerbranch : String = ""
    var gstStateCode : String = ""
    var branchCode : String = ""
    var gstBranchCode : String = ""
    var isUnionTerritoryState : String = ""
    var isUnionTerritoryBranch : String = ""
    
    //confirm
     var totalAmountWithTax : String = ""
     var quoteResponseDict: NSDictionary = [:] ;
    
    //login
      var emailID : String = ""
    
    //sellerdetailvc
    var title : String = ""
    var firstName : String = ""
    var lastName : String = ""
    var aadhaarNo : String = ""
    var GSTINNo : String = ""
    var dateOfBirth : NSDate = NSDate.init()
    
      //encashmentvc
    var isMakeAdvancePaymentSelected: Bool = false
    var isSellAtBranchSelected: Bool = false
    
    init() {
        productArray = []
        mobileNumber = ""
        customerState = ""
        customerbranch = ""
    }
    func saveSellForexProductViewDetails(productArray:[ProductBO] ,mobileNumber:String,customerState:String,customerbranch:String){
        self.productArray = productArray
        self.mobileNumber = mobileNumber
        self.customerState = customerState
        self.customerbranch = customerbranch
        
        
    }
    
}

