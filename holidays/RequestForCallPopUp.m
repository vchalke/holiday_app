//
//  RequestForCallPopUp.m
//  holidays
//
//  Created by ketan on 31/12/15.
//  Copyright © 2015 Mobicule. All rights reserved.
//

#import "RequestForCallPopUp.h"
#import "UIViewController+MJPopupViewController.h"
#import "NetCoreAnalyticsVC.h"
#import <NetCorePush/NetCorePush.h>

@interface RequestForCallPopUp ()
{
    NSString *packageType;
    
    NSString *requestType;
    
    NSString *subProductType;
}

@end

@implementation RequestForCallPopUp

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.scrollViewRequestCall setContentSize:CGSizeMake(301, 546)];
    [self.viewForRequestCall setFrame:CGRectMake(0, 0, 301, 546)];
    [self.scrollViewRequestCall addSubview:_viewForRequestCall];
}

-(void)viewWillAppear:(BOOL)animated
{
    if([[self.packageDetail.strPackageSubType lowercaseString] isEqualToString:@"fit fixed"]|| [[self.packageDetail.strPackageSubType lowercaseString] isEqualToString:@"git"])
    {
        if([[self.packageDetail.strPackageType lowercaseString] isEqualToString:@"international"])
        {
            packageType = kpackageTypeGITInternational;
            
        }
        else
        {
            packageType = kpackageTypeGITDomestic;
        }
    }else
    {
        if([[self.packageDetail.strPackageType lowercaseString] isEqualToString:@"international"])
        {
            packageType = kpackageTypeFITInternational;
        }
        else
        {
            packageType = kpackageTypeFITDomestic;
        }
    }
    
    CoreUtility *coreobj=[CoreUtility sharedclassname];
    
    if (coreobj.strPhoneNumber.count != 0)
    {
        NSString *phoneNumber = coreobj.strPhoneNumber[0];
        if (phoneNumber != nil && ![phoneNumber isEqualToString:@""])
        {
            self.txtPhoneNumber.text = phoneNumber;
        }
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onSubmitButtonClicked:(id)sender {
    
  
    if([[self.packageDetail.strPackageSubType lowercaseString] isEqualToString:@"fit fixed"]|| [[self.packageDetail.strPackageSubType lowercaseString] isEqualToString:@"git"])
    {
        if([[self.packageDetail.strPackageType lowercaseString] isEqualToString:@"international"])
        {
            packageType = @"INT Holiday GIT";
            
            requestType = @"International";
            
            subProductType = @"GIT";
            
        }
        else
        {
            packageType = @"DOM Holiday GIT";
            
            requestType = @"Domestic";
            
            subProductType = @"GIT";
            
        }
    }else
    {
        if([[self.packageDetail.strPackageType lowercaseString] isEqualToString:@"international"])
        {
            packageType = @"INT Holiday FIT";
            
            requestType = @"International";
            
            subProductType = @"FIT";
        }
        else
        {
            packageType = @"DOM Holiday FIT";
            
            requestType = @"Domestic";
            
            subProductType = @"FIT";
        }
    }
    
    if ([self validEmptyField])
    {
        if ([self validatePhoneNumber])
        {
            if ([self isValidEmail])
            {
                //NSString *requestString =  [NSString stringWithFormat:@"https://thomascookindia.custhelp.com/cgi-bin/thomascookindia.cfg/php/custom/web_lead.php?first_name=%@&last_name=%@&email=%@&mobile=%@&packageId=%@&product=%@&opp_summary=%@&lead_source=B2C app&city=%@&from_city=%@",self.txtFirstName.text,self.txtLastName.text,self.txtEmailIId.text,self.txtPhoneNumber.text,self.packageDetail.strPackageId,packageType,self.txtSummery.text,self.txtCity.text,self.txtDepartureCity.text];
                
                
                NSString  *urlString  = KUrlHolidayCreateLead;
                
                NSString *requestString =  [NSString stringWithFormat:@"%@?first_name=%@&last_name=%@&email=%@&mobile=%@&package_id=%@&request_type=%@&sub_product_type=%@&opp_summary=%@&city=%@&from_city=%@",urlString,self.txtFirstName.text,self.txtLastName.text,self.txtEmailIId.text,self.txtPhoneNumber.text,self.packageDetail.strPackageId,requestType,subProductType,self.txtSummery.text,self.txtCity.text,self.txtDepartureCity.text];
                
                [self netRequestCallBackForm]; // 09-04-2018
                
                [self submitRequestWithURL:requestString];
            }else
            {
                [self showAlertViewWithTitle:@"Alert" withMessage:@"Enter Valid Email"];
            }
        }else
        {
            [self showAlertViewWithTitle:@"Alert" withMessage:@"Enter Valid Mobile Number"];
        }
    }else
    {
        [self showAlertViewWithTitle:@"Alert" withMessage:@"Enter All fields"];
    }

}

-(void)submitRequestWithURL:(NSString *)urlString
{
    activityLoadingView = [LoadingView loadingViewInView:self.view.superview
                                             withString:@""
                                      andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSString *newString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url=[NSURL URLWithString:newString];
        NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:url];
        //  [request setHTTPMethod:@"HEAD"];
        
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            NSHTTPURLResponse *response;
            NSData *responseData =  [NSURLConnection sendSynchronousRequest:request returningResponse:&response error: NULL];
            NSString *responseString =   [[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding];
            NSLog(@"request for call%@",responseString);
            [activityLoadingView removeFromSuperview];
            [self netCoreInternationalHolidayEmailFrom];
            if([response statusCode]==200)
            {
                [self showAlertViewWithTitle:@"Alert" withMessage:@"Request Submitted successfully"];
            }
            else
            {
                [self showAlertViewWithTitle:@"Alert" withMessage:kMessageSomeErrorTryAfterSometime];
            }
            [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
        });
        
    });
}
-(void)showAlertViewWithTitle:(NSString *)title withMessage:(NSString *)message
{
    UIAlertController *alertView = [UIAlertController alertControllerWithTitle:title
                                                                       message:message
                                                                preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [alertView dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertView addAction:okAction];
    
    [self presentViewController:alertView animated:YES completion:nil];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    int flag;
    if (_txtFirstName==textField) {
        [_txtLastName becomeFirstResponder];
    }else if (_txtLastName==textField){
        [_txtPhoneNumber becomeFirstResponder];
    }else if (_txtPhoneNumber==textField){
        [_txtEmailIId becomeFirstResponder];
    }else if (_txtEmailIId==textField){
        [_txtCity becomeFirstResponder];
    }else if (_txtCity ==textField){
        [_txtSummery becomeFirstResponder];
        if (flag==1) {
            [textField resignFirstResponder];
        }
    }else if (_txtSummery==textField){
        [_txtDepartureCity becomeFirstResponder];
    }else if(_txtDepartureCity==textField){
        [textField resignFirstResponder];
        flag=1;
    }
    
    return YES;
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField == self.txtDepartureCity)
    {
//        if(self.packageDetail.stringSelectedAccomType == nil || [self.packageDetail.stringSelectedAccomType isEqualToString:@""])
//        {
//            if (self.packageDetail.arrayAccomTypeList.count != 0)
//            {
//                NSString *packageTypeLocal = (NSString *)self.packageDetail.arrayAccomTypeList[0];
//                [self setAccomType:packageTypeLocal];
//            }
//            
//        }
        
        NSArray *hubList = self.packageDetail.arrayLtPricingCollection;
        UIAlertController *durationActionSheet = [UIAlertController alertControllerWithTitle:@"Cities" message:@"Select your choice" preferredStyle:UIAlertControllerStyleActionSheet];
        NSArray *filteredarray = hubList;
   
        
//        if([packageType isEqualToString:kpackageTypeGITInternational]||[packageType isEqualToString:kpackageTypeGITDomestic])
//        {
//            filteredarray = hubList;
//        }
//        else
//        {
//            
//            filteredarray = [hubList filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(accomType == %@)", self.packageDetail.stringSelectedAccomType]];
//        }
        
        for (int j =0 ; j < filteredarray.count; j++)
        {
            NSDictionary *cityDict = filteredarray[j];
            NSDictionary *hubCodeDict = [cityDict valueForKey:@"hubCode"];

            NSString *titleString = [hubCodeDict valueForKey:@"cityName"];
            UIAlertAction *action = [UIAlertAction actionWithTitle:titleString style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                self.txtDepartureCity.text = titleString;
            }];
            [durationActionSheet addAction:action];
        }
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
        
            [self dismissViewControllerAnimated:durationActionSheet completion:nil];
        }];
        
        [durationActionSheet addAction:cancelAction];
        
        [self.view.window.rootViewController presentViewController:durationActionSheet animated:YES completion:nil];
        return NO;
        
    }
    return YES;
}

-(void)setAccomType:(NSString *)accomTypeString
{
    if ([[accomTypeString lowercaseString] isEqualToString:@"standard"])
    {
        self.packageDetail.stringSelectedAccomType = @"0";
    }
    else if ([[accomTypeString lowercaseString] isEqualToString:@"delux"]||[[accomTypeString lowercaseString] isEqualToString:@"deluxe"])
    {
        self.packageDetail.stringSelectedAccomType = @"1";
    }
    else
    {
        self.packageDetail.stringSelectedAccomType = @"2";
    }
}

-(BOOL)validatePhoneNumber
{
    NSString *string = self.txtPhoneNumber.text;
    NSString *expression = @"^([+][9][1]|[9][1]|[0]){0,1}([7-9]{1})([0-9]{9})$";
    NSError *error = NULL;
    
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
    
    NSTextCheckingResult *match = [regex firstMatchInString:string options:0 range:NSMakeRange(0, [string length])];
    
    if (match){
        NSLog(@"yes");
        return YES;
    }else{
        NSLog(@"no");
        return NO;
    }
    
}

-(BOOL)isValidEmail
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:self.txtEmailIId.text];
}

-(BOOL)validEmptyField
{
    if ([self.txtCity.text isEqualToString:@""]||[self.txtDepartureCity.text isEqualToString:@""]||[self.txtEmailIId.text isEqualToString:@""]||[self.txtFirstName.text isEqualToString:@""]||[self.txtLastName.text isEqualToString:@""]||[self.txtPhoneNumber.text isEqualToString:@""]||[self.txtSummery.text isEqualToString:@""]) {
        return NO;
    }
    return YES;
}

//APP_EMAIL_FORM

-(void)netCoreInternationalHolidayEmailFrom
{
    NSMutableDictionary *payloadList =  [NSMutableDictionary dictionary];
    
    [payloadList setObject:[NSString stringWithFormat:@"%@",self.packageDetail.strPackageId] forKey:@"s^PACKAGE_ID"];
    [payloadList setObject:self.txtSummery.text forKey:@"s^SUMMARY"];
    [payloadList setObject:[NSString stringWithFormat:@"%@",self.packageDetail.strPackageName] forKey:@"s^DESTINATION"];
    [payloadList setObject:@"App" forKey:@"s^SOURCE"];
    [payloadList setObject:self.packageDetail.strPackageType forKey:@"s^TYPE"]; //28-02-2018
    
//  [NetCoreAnalyticsVC getNetCorePushNotificationDetailDictionary:payloadList withPayloadCount:107]; //28-02-2018
    
    [NetCoreAnalyticsVC getNetCorePushNotificationDetailDictionary:payloadList withPayloadCount:127]; //28-02-2018
    
    
    
}



-(void)netRequestCallBackForm  // 09-04-2018
{
    NSMutableDictionary *profileDetail = [[NSMutableDictionary alloc] init];
    [profileDetail setObject:self.txtFirstName.text forKey:@"FIRST_NAME"];
    [profileDetail setObject:self.txtLastName.text forKey:@"LAST_NAME"];
    [profileDetail setObject:self.txtEmailIId.text forKey:@"EMAIL"];
    [profileDetail setObject:self.txtCity.text forKey:@"CITY"];
    [profileDetail setObject:@"view details" forKey:@"FORM_TYPE"];
    
    [NetCoreAnalyticsVC getNetCorePushNotificationDetailDictionary:profileDetail withPayloadCount:102];
    
    CoreUtility *coreobj=[CoreUtility sharedclassname];
    
    if (coreobj.strPhoneNumber.count != 0)
    {
        NSString *phoneNumber = coreobj.strPhoneNumber[0];
        if (phoneNumber != nil && ![phoneNumber isEqualToString:@""])
        {
            [[NetCoreInstallation sharedInstance] netCoreProfilePush:phoneNumber Payload:profileDetail Block:nil];
        }
        else
        {
            [[NetCoreInstallation sharedInstance] netCoreProfilePush:@"" Payload:profileDetail Block:nil];
        }
    }
    else
    {
        [[NetCoreInstallation sharedInstance] netCoreProfilePush:@"" Payload:profileDetail Block:nil];
    }
    
    
    
    
}

@end
