//
//  FilteredOptionsVC.h
//  holidays
//
//  Created by Kush_Tech on 18/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol FilteredOptionsVCDelegate <NSObject>
-(void)showSelectedOptionsFor:(NSString*)string withArray:(NSArray*)array;
@end

@interface FilteredOptionsVC : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, weak) id <FilteredOptionsVCDelegate> filterOptionsDelgate;
@property (weak, nonatomic) IBOutlet UILabel *lbl_headingTitle;
@property (weak, nonatomic) IBOutlet UITableView *table_Views;
@property (strong, nonatomic) NSArray *cityArray;
@property (strong, nonatomic) NSArray *allListArray;
@property (strong, nonatomic) NSString *titles;
@property (strong, nonatomic) NSMutableArray *selectedOptionsArray;
@property (strong, nonatomic) NSArray *passSelectedOptionsArray;
@end

NS_ASSUME_NONNULL_END
