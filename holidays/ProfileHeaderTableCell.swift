//
//  ProfileHeaderTableCell.swift
//  holidays
//
//  Created by Kush_Team on 22/06/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

import UIKit

class ProfileHeaderTableCell: UITableViewCell {

    @IBOutlet weak var mainBaseView: UIView!
    @IBOutlet weak var upperGrayView: UIView!
    @IBOutlet weak var imgBaseView: UIView!
    @IBOutlet weak var img_Profile: UIImageView!
    @IBOutlet weak var lbl_username: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setProfileDetails(profileDict:[String:Any]?){
        if let isDict = profileDict{
            if let firstName = isDict[APIResponseConstants.UserProfile.FIRST_NAME], let lastName = isDict[APIResponseConstants.UserProfile.LAST_NAME]{
                self.lbl_username.text = "\(firstName) \(lastName)"
            }
        }
        if #available(iOS 11.0, *) {
            setViewRadius()
        }
    }
    
    @available(iOS 11.0, *)
    func setViewRadius(){
        self.upperGrayView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        self.imgBaseView.layer.cornerRadius = self.imgBaseView.frame.size.width*0.5
        self.img_Profile.layer.cornerRadius = self.img_Profile.frame.size.width*0.5
//        self.img_Profile.layer.masksToBounds = true
        
//        setCornerRadius
        self.upperGrayView.layer.cornerRadius = self.upperGrayView.frame.size.width*0.5
    }
    
    func setProfileModelsDetails(profileModel:CustomerProfileModel){
        if #available(iOS 11.0, *) {
            setViewRadius()
        }
        self.lbl_username.text = "\(profileModel.firstName) \(profileModel.lastName)"
        //        var path : String = profileModel.profilePicUrl as? String ?? ""
        var path = profileModel.profilePicUrl
        path = path.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        if let url = URL.init(string: path){
            self.img_Profile.sd_setImage(with: url, placeholderImage: UIImage(named: "ProfileActive"))
        }
    }
}
