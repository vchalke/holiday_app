//
//  HolidayLandingScreenVC.h
//  holidays
//
//  Created by Kush_Tech on 12/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewMasterVC.h"
#import "LandingScreenVC.h"
NS_ASSUME_NONNULL_BEGIN

@interface HolidayLandingScreenVC : LandingScreenVC
@property (weak, nonatomic) IBOutlet UITextField *search_textField;
@property (weak, nonatomic) IBOutlet UITableView *table_views;
@property (strong ,nonatomic) NSDictionary *notificationDict;
@property (strong,nonatomic) NSString *headerName;
@property (strong,nonatomic) NSArray *whatsNewArray;
@property (strong,nonatomic) NSArray *bankOffersArray;
@property (strong,nonatomic) NSArray *topBannerArray;
@property(nonatomic,assign) BOOL boolForSearchPackgScreen;

@property (strong,nonatomic) NSArray *summerArray;
@property (strong,nonatomic) NSArray *adventureArray;
@property (strong,nonatomic) NSArray *categoryArray;
@property (strong,nonatomic) NSArray *upcomingArray;
@property (strong,nonatomic) NSArray *cheeryBlossomArray;
@property (strong,nonatomic) NSArray *popularDestinationArray;
@property (strong,nonatomic) NSArray *holidayOneArray;

-(void)searchDestinationSelectDict:(NSDictionary*)selecteDestinationDict withText:(NSString*)searchText;
@end

NS_ASSUME_NONNULL_END
