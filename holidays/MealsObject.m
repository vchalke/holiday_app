//
//  MealsObject.m
//  holidays
//
//  Created by Kush_Tech on 17/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "MealsObject.h"

@implementation MealsObject
-(instancetype)initWithMealsObjectDict:(NSDictionary *)dictionary{
    if ([super init])
    {
        self.packageClassId = [[dictionary valueForKey:@"packageClassId"] integerValue];
        self.tcilMstMeal = [dictionary valueForKey:@"tcilMstMeal"];
        self.mealName = [self.tcilMstMeal valueForKey:@"mealName"];
        self.mealDescription = [self.tcilMstMeal valueForKey:@"mealDescription"];
    }
    
    return self;
}
@end
