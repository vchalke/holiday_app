//
//  LoginViewPopUp.m
//  holidays
//
//  Created by ketan on 10/11/15.
//  Copyright © 2015 Mobicule. All rights reserved.
//

#import "LoginViewPopUp.h"
#import "UIViewController+MJPopupViewController.h"
#import "KLCPopup.h"
#import "MyProfileVc.h"
#import "Reachability.h"
#import "SignInViewController.h"
#import "SlideNavigationController.h"
#import "NetCoreAnalyticsVC.h"
#import "RegisterUserViewController.h"

//static NSString * const kClientID = @"327645202876-3pmqts5pkaaqqvhh1gjh52k7sbuv468a.apps.googleusercontent.com";

static NSString * const kClientID =
@"327645202876-3pmqts5pkaaqqvhh1gjh52k7sbuv468a.apps.googleusercontent.com";

static LoginViewPopUp *singletonInstance;

@import FirebaseAuth;

@interface LoginViewPopUp ()<registerUserDelegate>
{
    //GIDSignIn *signIn ;
    LoginViewPopUp *loginOBJ;
    LoadingView *activityLoadingView;
}

@property (nonatomic, copy) void (^confirmActionBlock)(void);
@property (nonatomic, copy) void (^cancelActionBlock)(void);

@end

@implementation LoginViewPopUp

@synthesize delegate;

-(void)setUp
{
    [GIDSignInButton class];
    
    GIDSignIn *signIn = [GIDSignIn sharedInstance];
    signIn.shouldFetchBasicProfile = YES;
    signIn.delegate = self;
    signIn.uiDelegate = self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil {
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
         [self setUp];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        [self setUp];
        
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
 
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    
    
    CGFloat topPadding;
    CGFloat bottomPadding;
    
    if (@available(iOS 11.0, *)) {
        UIWindow *window = UIApplication.sharedApplication.keyWindow;
        topPadding = window.safeAreaInsets.top;
        bottomPadding = window.safeAreaInsets.bottom;
        
    }
    else{
        topPadding = 0;
        bottomPadding = 0;
    }
    
    self.loginView.frame = CGRectMake(topPadding, 0, 300, 568);
   // self.loginView.frame = CGRectMake(0, 0, screenWidth, self.loginView.frame.size.height);

    
    //[self reportAuthStatus];
   // [self refreshUserInfo];
    //loginOBJ = [[LoginViewPopUp alloc] initWithNibName:@"LoginViewPopUp" bundle:nil];
    
    //For Guest User Login
    [CoreUtility reloadRequestID];

    
    if ([self isForGuestUser])
    {
        
        self.passwordImageView.hidden=true;
        self.textFieldPassword.hidden=true;
        [self.buttonAlreadyMember setSelected:NO];
        self.constraintPassword.constant = 0;
        self.constraintReenterPass.constant = 0;
        [self.buttonSignIn setTitle:@"SUBMIT" forState:UIControlStateNormal];
    }
    
   // [self checkedForGoogleLogin];
}

-(void)viewWillLayoutSubviews
{
    CGFloat topPadding;
    CGFloat bottomPadding;
    
    if (@available(iOS 11.0, *)) {
        UIWindow *window = UIApplication.sharedApplication.keyWindow;
        topPadding = window.safeAreaInsets.top;
        bottomPadding = window.safeAreaInsets.bottom;
        
    }
    else{
        topPadding = 0;
        bottomPadding = 0;
    }
    
    self.loginView.frame = CGRectMake(topPadding, 0, 300, 568);
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;

   //  self.loginView.frame = CGRectMake(0, 0, screenWidth, self.loginView.frame.size.height);
    NSLog(@"loginFrame : - %@",NSStringFromCGRect(self.loginView.frame));
}
-(void)viewDidLayoutSubviews
{
    
}

/*
- (OSStatus) deleteAllKeysForSecClass:(CFTypeRef) secClass
{
    NSMutableDictionary* dict = [NSMutableDictionary dictionary];
    [dict setObject:(__bridge id) secClass forKey:(__bridge id) kSecClass];
    OSStatus result = SecItemDelete((__bridge CFDictionaryRef) dict);
    return result;
}

- (void) clearKeychain
{
    [self deleteAllKeysForSecClass:kSecClassIdentity];
    [self deleteAllKeysForSecClass:kSecClassGenericPassword];
    [self deleteAllKeysForSecClass:kSecClassInternetPassword];
    [self deleteAllKeysForSecClass:kSecClassCertificate];
    [self deleteAllKeysForSecClass:kSecClassKey];
}
*/



- (void)viewDidLoad
{
    [super viewDidLoad];

//    [GIDSignIn sharedInstance].delegate = self;
//    [GIDSignIn sharedInstance].uiDelegate = self;
        
    self.userEmailID = [NSString stringWithFormat:@""];
    
    self.loginView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"login_bg.jpg"]];
    
    UIImageView *imageViewUserID = [[UIImageView alloc]initWithFrame:CGRectMake(10, 0, 25, 25)];
    imageViewUserID.image = [UIImage imageNamed:@"login_mail"];
    imageViewUserID.contentMode = UIViewContentModeCenter;
    
    UIImageView *imageViewPassword = [[UIImageView alloc]initWithFrame:CGRectMake(10, 0, 25, 25)];
    imageViewPassword.image =[UIImage imageNamed:@"login_lock"];
    imageViewPassword.contentMode = UIViewContentModeCenter;
    
    UIImageView *imageViewRePassword = [[UIImageView alloc]initWithFrame:CGRectMake(10, 0, 25, 25)];
    imageViewRePassword.image =[UIImage imageNamed:@"login_lock"];
    imageViewRePassword.contentMode = UIViewContentModeCenter;
    
    UIView *viewPaddingForUserID  = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 40, 25)];
    [viewPaddingForUserID addSubview:imageViewUserID];
    self.textFieldUserId.leftView =  viewPaddingForUserID;
    self.textFieldUserId.leftViewMode = UITextFieldViewModeAlways;
    
    
    UIView *viewPaddingForPassword  = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 40, 25)];
    [viewPaddingForPassword addSubview:imageViewPassword];
    self.textFieldPassword.leftView = viewPaddingForPassword;
    self.textFieldPassword.leftViewMode = UITextFieldViewModeAlways;
    
    
    UIView *viewPaddingForRePassword  = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 40, 25)];
    [viewPaddingForRePassword addSubview:imageViewRePassword];
    self.textFieldReEnterPassword.leftView = viewPaddingForRePassword;
    self.textFieldReEnterPassword.leftViewMode = UITextFieldViewModeAlways;
    
//    [GIDSignIn sharedInstance].shouldFetchBasicProfile = YES;
//    [GIDSignIn sharedInstance].delegate = self;
//    [GIDSignIn sharedInstance].uiDelegate = self;
    
    [FBSDKProfile enableUpdatesOnAccessTokenChange:YES];
    
    [self.loginScrollView addSubview:self.loginView];
 //    [self.loginScrollView addSubview:self.newLoginView];
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;

    [self.loginScrollView setContentSize:CGSizeMake(300, 568)];
  //  [self.loginScrollView setContentSize:CGSizeMake(screenWidth, self.loginView.frame.size.height)];

    
    loginOBJ = [[LoginViewPopUp alloc] initWithNibName:@"LoginViewPopUp" bundle:nil];
    
 //   [GIDSignIn sharedInstance].uiDelegate = self;
 //  [[GIDSignIn sharedInstance] signIn];

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

+ (LoginViewPopUp *)sharedInstance
{
    if (!singletonInstance)
        NSLog(@"SlideNavigationController has not been initialized. Either place one in your storyboard or initialize one in code");
    
    return singletonInstance;
}


- (IBAction)onSignInButtonClicked:(id)sender
{
    
    if ([[self.buttonSignIn titleLabel].text isEqualToString:@"SUBMIT"])
    {
        if ([self checkEmailIdValidation])
        {
            if ([self.buttonAlreadyMember isSelected])
            {
                //[self resetPassword];
                 [self loginCheckWithType:@"ForgotPassword"];
            }
            else
            {
                //[self registerUserForGuest];
                [self loginCheckWithType:@"guestLogin"];
            }
        }
        else
        {
            [self showAlertViewWithTitle:@"Alert" withMessage:@"Enter username"];
        }
        
        
    }
    else if ([[self.buttonSignIn titleLabel].text isEqualToString:@"SIGN IN"])
    {
        [FIRAnalytics logEventWithName:@"Sign_In" parameters:@{kFIRParameterItemID:@"Sign In"}];

        if ([self checkEmailIdValidation] && ![self.textFieldPassword.text isEqualToString:@""])
        {
            //[self signInUser];
            [self loginCheckWithType:@"signIn"];
             //[self loginCheckWithType:kSignInWithOtp];
        }
        else
        {
            NSString *message;
            if (![self checkEmailIdValidation]) {
                
                message = @"Enter username";
            }else
            {
                message = @"Enter password";
            }
            
            [self showAlertViewWithTitle:@"Alert" withMessage:message];
        }
        
    }
    else if ([[self.buttonSignIn titleLabel].text isEqualToString:@"SIGN UP"])
    {
        if ([self checkEmailIdValidation] && ![self.textFieldPassword.text isEqualToString:@""] && ![self.textFieldReEnterPassword.text isEqualToString:@""])
        {
            
            if ([self.textFieldPassword.text isEqualToString:self.textFieldReEnterPassword.text]) {
                
                //[self registerUser];
                [self loginCheckWithType:@"signUp"];
                
            }
            else
                [self showAlertViewWithTitle:@"Alert" withMessage:@"Entered password and re-entered password not matched"];
        }
        else
        {
            if ([self.textFieldUserId.text isEqualToString:@""])
            {
                [self showAlertViewWithTitle:@"Alert" withMessage:@"Enter username"];
            }
            else if ([self.textFieldPassword.text isEqualToString:@""] ||[self.textFieldReEnterPassword.text isEqualToString:@""] )
            {
                [self showAlertViewWithTitle:@"Alert" withMessage:@"Enter password"];
            }
        }
        
        
    }
    
}

-(void)showAlertViewWithTitle:(NSString *)title withMessage:(NSString *)message
{
    UIAlertController *alertView = [UIAlertController alertControllerWithTitle:title
                                                                       message:message
                                                                preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [alertView dismissViewControllerAnimated:YES completion:nil];
        
    }];
    
    [alertView addAction:okAction];
    
    [self presentViewController:alertView animated:YES completion:nil];
}

-(BOOL) checkEmailIdValidation
{
    if ([self.textFieldUserId.text isEqualToString:@""])
    {
        [self showAlertViewWithTitle:@"Alert" withMessage:@"please enter a emailId"];
        return NO;
    }
    else if(![self validateEmailWithString])
    {
        [self showAlertViewWithTitle:@"Alert" withMessage:@"please enter a valid emailId"];
        return NO;
    }
    return YES;
}

- (BOOL)validateEmailWithString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:self.textFieldUserId.text];
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([string isEqualToString:@""])
    {
        return YES;
    }
    if (textField == self.textFieldUserId)
    {
        if([self.textFieldUserId.text length]<=100)
        {
            //        CGRect rect = self.textFieldUserId.rightView.frame;
            //        // only when adding on the end of textfield && it's a space
            //        if (range.location == self.textFieldUserId.text.length && [string isEqualToString:@" "]) {
            //            rect.size.width += 25; // depends on font!
            //        }
            //        else if (string.length == 0) { // backspace
            //            rect.size.width -= 25;
            //            rect.size.width = MAX(0, rect.size.width);
            //        }
            //        else
            //        {
            //            rect.size.width = 0;
            //        }
            //        self.textFieldUserId.rightView.frame = rect;
            
            return YES;
        }
        else
        {
            return NO;
        }
    }
    
    if (textField == self.textFieldPassword)
    {
        if([self.textFieldPassword.text length]<=25)
        {
            CGRect rect = self.textFieldPassword.rightView.frame;
            if (range.location == self.textFieldPassword.text.length && [string isEqualToString:@" "])
            {
                rect.size.width += 25;
            }
            else if (string.length == 0) {
                rect.size.width -= 25;
                rect.size.width = MAX(0, rect.size.width);
            }
            else
            {
                rect.size.width = 0;
            }
            self.textFieldPassword.rightView.frame = rect;
            
            return YES;
        }
        else
        {
            return NO;
        }
        
    }
    if (textField == self.textFieldReEnterPassword)
    {
        if([self.textFieldReEnterPassword.text length]<=25)
        {
            CGRect rect = self.textFieldReEnterPassword.rightView.frame;
            
            if (range.location == self.textFieldReEnterPassword.text.length && [string isEqualToString:@" "]) {
                rect.size.width += 25;
            }
            else if (string.length == 0)
            {
                rect.size.width -= 25;
                rect.size.width = MAX(0, rect.size.width);
            }
            else
            {
                rect.size.width = 0;
            }
            self.textFieldReEnterPassword.rightView.frame = rect;
            
            return YES;
        }
        else
        {
            return NO;
        }
    }
    
    return YES;
    
}


-(void)clearAllTextFields
{
    self.textFieldPassword.text = @"";
    self.textFieldReEnterPassword.text = @"";
    self.textFieldUserId.text  = @"";
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
-(void)resetPassword
{
    activityIndicator = [LoadingView loadingViewInView:self.view withString:@"" andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
    
    NSDictionary *dict = [[NSDictionary alloc]initWithObjectsAndKeys:self.textFieldUserId.text, @"userId",nil];
    
    NetworkHelper *helper = [NetworkHelper sharedHelper];
    if ([CoreUtility connected])
    {
 
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSString *strResponse = [helper getDataFromServerForType:@"webservice" entity:@"holiday" action:@"resetPassword" andUserJson:dict];
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            [activityIndicator removeFromSuperview];
            if (![strResponse isEqualToString:@"No Internet"])
            {
                NSString *strStatus = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"status"];
                NSString *strMessage = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"message"];
                
                if ([[strStatus lowercaseString] isEqualToString:kStatusSuccess])
                {
                    [activityIndicator removeFromSuperview];
                    
                    UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Success"
                                                                                       message:@"Password is sent to your email id"
                                                                                preferredStyle: UIAlertControllerStyleAlert];
                    
                    UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                        
                        [alertView dismissViewControllerAnimated:YES completion:nil];
                        [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
                        if (self.delegate && [self.delegate respondsToSelector:@selector(dismissLoginPopUp)])
                        {
                            [self.delegate dismissLoginPopUp];
                        }
                        
                    }];
                    
                    [alertView addAction:actionOk];
                    
                    [self presentViewController:alertView animated:YES completion:nil];
                }
                else
                {
                    UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Alert"
                                                                                       message:strMessage
                                                                                preferredStyle: UIAlertControllerStyleAlert];
                    
                    UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                        
                        [alertView dismissViewControllerAnimated:YES completion:nil];
                        
                    }];
                    
                    [alertView addAction:actionOk];
                    
                    [self presentViewController:alertView animated:YES completion:nil];
                }
            }
        });
    });
        
    }
    else
    {
        UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Alert"
                                                                           message:kMessageNoInternet
                                                                    preferredStyle: UIAlertControllerStyleAlert];
        
        UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
            [alertView dismissViewControllerAnimated:YES completion:nil];
            
        }];
        
        [alertView addAction:actionOk];
        
        [self presentViewController:alertView animated:YES completion:nil];
    }
}

-(void)registerUserForGuest
{
    activityIndicator = [LoadingView loadingViewInView:self.view withString:@"" andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
    
    [self netCoreDataForLoginWithEmailID:self.textFieldUserId.text];
    
    NSDictionary *dict = [[NSDictionary alloc]initWithObjectsAndKeys:self.textFieldUserId.text, @"userId",nil];
    
    NetworkHelper *helper = [NetworkHelper sharedHelper];
    if ([CoreUtility connected])
    {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSString *strResponse = [helper getDataFromServerForType:@"webservice" entity:@"holiday" action:@"guestLogin" andUserJson:dict];
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            [activityIndicator removeFromSuperview];
            if (![strResponse isEqualToString:@"No Internet"])
            {
                NSString *strStatus = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"status"];
                NSString *strMessage = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"message"];
                
                if ([[strStatus lowercaseString] isEqualToString:kStatusSuccess])
                {
                    [activityIndicator removeFromSuperview];
                    [[NSUserDefaults standardUserDefaults]setValue:self.textFieldUserId.text forKey:kuserDefaultUserId]
                    ;
                    [[NSUserDefaults standardUserDefaults]setValue:self.textFieldPassword.text forKey:kuserDefaultPassword]
                    ;
                    
                    NSUserDefaults *LoginStatus=[NSUserDefaults standardUserDefaults];
                    [LoginStatus setObject:kLoginSuccess  forKey:kLoginStatus];
                    
                    NSUserDefaults *userIdForGooglePlusSignIn=[NSUserDefaults standardUserDefaults];
                    
                    [userIdForGooglePlusSignIn setObject:self.textFieldUserId.text forKey:kLoginEmailId];
                    
                   // [[NetCoreSharedManager sharedInstance] setUpIdentity:self.textFieldUserId.text];

                    [self fetchMobileNumber];
                    
                    UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Success"
                                                                                       message:@"User Signed in  Successfully"
                                                                                preferredStyle: UIAlertControllerStyleAlert];
                    
                    UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                        
                        [alertView dismissViewControllerAnimated:YES completion:nil];
                        [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
                        if (self.delegate && [self.delegate respondsToSelector:@selector(dismissLoginPopUp)])
                        {
                            [self.delegate dismissLoginPopUp];
                        }
                        
                    }];
                    
                    [alertView addAction:actionOk];
                    
                    [self presentViewController:alertView animated:YES completion:nil];
                }
                else
                {
                    UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Alert"
                                                                                       message:strMessage
                                                                                preferredStyle: UIAlertControllerStyleAlert];
                    
                    UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                        
                        [alertView dismissViewControllerAnimated:YES completion:nil];
                        
                    }];
                    
                    [alertView addAction:actionOk];
                    
                    [self presentViewController:alertView animated:YES completion:nil];
                }
            }
        });
    });
    }
    else
    {
        UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Alert"
                                                                           message:kMessageNoInternet
                                                                    preferredStyle: UIAlertControllerStyleAlert];
        
        UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
            [alertView dismissViewControllerAnimated:YES completion:nil];
            
        }];
        
        [alertView addAction:actionOk];
        
        [self presentViewController:alertView animated:YES completion:nil];
    }
}
-(void)signInUser
{
    activityIndicator = [LoadingView loadingViewInView:self.view withString:@"" andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
    
    [self netCoreDataForLoginWithEmailID:self.textFieldUserId.text];

    
    NSDictionary *dict = [[NSDictionary alloc]initWithObjectsAndKeys:self.textFieldUserId.text, @"userId",self.textFieldPassword.text,@"password",nil];
    
    NetworkHelper *helper = [NetworkHelper sharedHelper];
    if([CoreUtility connected])
    {

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSString *strResponse = [helper getDataFromServerForType:@"webservice" entity:@"holiday" action:@"login" andUserJson:dict];
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            [activityIndicator removeFromSuperview];
            if (![strResponse isEqualToString:@"No Internet"])
            {
                NSString *strStatus = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"status"];
                NSString *strMessage = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"message"];
                
                if ([[strStatus lowercaseString] isEqualToString:kStatusSuccess])
                {
                    [[NSUserDefaults standardUserDefaults]setValue:self.textFieldUserId.text forKey:kuserDefaultUserId]
                    ;
                    [[NSUserDefaults standardUserDefaults]setValue:self.textFieldPassword.text forKey:kuserDefaultPassword]
                    ;
                    
                    NSUserDefaults *LoginStatus=[NSUserDefaults standardUserDefaults];
                    [LoginStatus setObject:kLoginSuccess  forKey:kLoginStatus];
                    
                    NSUserDefaults *userIdForGooglePlusSignIn=[NSUserDefaults standardUserDefaults];
                    [userIdForGooglePlusSignIn setObject:self.textFieldUserId.text forKey:kLoginEmailId];
                    
                    [self fetchMobileNumber];
                    
                    UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Success"
                                                                                       message:@"User Signed in Successfully"
                                                                                preferredStyle: UIAlertControllerStyleAlert];
                    
                    
                    
                    UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                        
                        [alertView dismissViewControllerAnimated:YES completion:nil];
                        [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
                        if (self.delegate && [self.delegate respondsToSelector:@selector(dismissLoginPopUp)])
                        {
                            [self.delegate dismissLoginPopUp];
                        }
                        
                    }];
                    
                    [alertView addAction:actionOk];
                    
                    [self presentViewController:alertView animated:YES completion:nil];
                }
                else
                {
                    UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Alert"
                                                                                       message:strMessage
                                                                                preferredStyle: UIAlertControllerStyleAlert];
                    
                    UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                        
                        [alertView dismissViewControllerAnimated:YES completion:nil];
                        
                    }];
                    [alertView addAction:actionOk];
                    [self presentViewController:alertView animated:YES completion:nil];
                }
            }
        });
    });
        
    }
    else
    {
        UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Alert"
                                                                           message:kMessageNoInternet
                                                                    preferredStyle: UIAlertControllerStyleAlert];
        
        UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
            [alertView dismissViewControllerAnimated:YES completion:nil];
            
        }];
        
        [alertView addAction:actionOk];
        [self presentViewController:alertView animated:YES completion:nil];
    }
}

-(void)registerUser
{
    
    activityIndicator = [LoadingView loadingViewInView:self.view withString:@"" andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
    
    NSDictionary *dict = [[NSDictionary alloc]initWithObjectsAndKeys:self.textFieldUserId.text, @"userId",self.textFieldPassword.text,@"password",nil];
    
    NetworkHelper *helper = [NetworkHelper sharedHelper];
    if([CoreUtility connected])
    {

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSString *strResponse = [helper getDataFromServerForType:@"webservice" entity:@"holiday" action:@"registration" andUserJson:dict];
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            [activityIndicator removeFromSuperview];
            if (![strResponse isEqualToString:@"No Internet"])
            {
                NSString *strStatus = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"status"];
                NSString *strMessage = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"message"];
                
                if ([[strStatus lowercaseString] isEqualToString:kStatusSuccess])
                {
                    [[NSUserDefaults standardUserDefaults]setValue:self.textFieldUserId.text forKey:kuserDefaultUserId]
                    ;
                    [[NSUserDefaults standardUserDefaults]setValue:self.textFieldPassword.text forKey:kuserDefaultPassword]
                    ;
                    
                    
                    NSUserDefaults *LoginStatus=[NSUserDefaults standardUserDefaults];
                    [LoginStatus setObject:kLoginSuccess  forKey:kLoginStatus];
                    
                    NSUserDefaults *userIdForGooglePlusSignIn=[NSUserDefaults standardUserDefaults];
                    [userIdForGooglePlusSignIn setObject:self.textFieldUserId.text forKey:kLoginEmailId];
                    
                    [self fetchMobileNumber];
                    
                    
                    UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Success"
                                                                                       message:@"User Registered Successfully"
                                                                                preferredStyle: UIAlertControllerStyleAlert];
                    
                    UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                        
                        [alertView dismissViewControllerAnimated:YES completion:nil];
                        [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
                        if (self.delegate && [self.delegate respondsToSelector:@selector(dismissLoginPopUp)])
                        {
                            [self.delegate dismissLoginPopUp];
                            
                        }
                        
                    }];
                    
                    [alertView addAction:actionOk];
                    
                    [self presentViewController:alertView animated:YES completion:nil];
                }
                else
                {
                    UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Alert"
                                                                                       message:strMessage
                                                                                preferredStyle: UIAlertControllerStyleAlert];
                    
                    UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                        
                        [alertView dismissViewControllerAnimated:YES completion:nil];
                        
                    }];
                    
                    [alertView addAction:actionOk];
                    
                    [self presentViewController:alertView animated:YES completion:nil];
                }
            }
        });
    });
    }
    else
    {
        UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Alert"
                                                                           message:kMessageNoInternet
                                                                    preferredStyle: UIAlertControllerStyleAlert];
        
        UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
            [alertView dismissViewControllerAnimated:YES completion:nil];
            
        }];
        [alertView addAction:actionOk];
        [self presentViewController:alertView animated:YES completion:nil];
    }
}

#pragma mark -buttonActions

- (IBAction)onGuestUserButtonClicked:(id)sender
{
    [self clearAllTextFields];
    [FIRAnalytics logEventWithName:@"Guest_User" parameters:@{kFIRParameterItemID:@"Guest User"}];
    
    self.passwordImageView.hidden=true;
    self.textFieldPassword.hidden=true;
    [self.buttonAlreadyMember setSelected:NO];
    self.constraintPassword.constant = 0;
    self.constraintReenterPass.constant = 0;
    [self.buttonSignIn setTitle:@"SUBMIT" forState:UIControlStateNormal];
    [UIView animateWithDuration:0.4 animations:^{
        
        [self.view layoutIfNeeded];
    }];
}
- (IBAction)onRegisterUserButtonClicked:(id)sender
{
    [self clearAllTextFields];
    
    [FIRAnalytics logEventWithName:@"Sign_Up" parameters:@{kFIRParameterItemID: @"Sign Up"}];
    
    self.textFieldPassword.hidden=false;
    [self.buttonAlreadyMember setSelected:NO];
    self.constraintPassword.constant = 35;
    self.constraintReenterPass.constant = 35;
    [self.buttonSignIn setTitle:@"SIGN UP" forState:UIControlStateNormal];
    [UIView animateWithDuration:0.4 animations:^{
        
        [self.view layoutIfNeeded];
    }];
}
- (IBAction)onForgotPasswordClicked:(id)sender
{
    [FIRAnalytics logEventWithName:@"Forgot_Password" parameters:@{kFIRParameterItemID:@"Forgot Password"}];
    [self clearAllTextFields];
    self.passwordImageView.hidden=true;
    self.textFieldPassword.hidden=true;
    [self.buttonAlreadyMember setSelected:YES];
    self.constraintPassword.constant = 0;
    self.constraintReenterPass.constant = 0;
    [self.buttonSignIn setTitle:@"SUBMIT" forState:UIControlStateNormal];
    [UIView animateWithDuration:0.4 animations:^{
        
        [self.view layoutIfNeeded];
    }];
}
- (IBAction)onAlreadyMemberClicked:(id)sender {
    [self clearAllTextFields];
    if ([self.buttonAlreadyMember isSelected])
    {
        self.textFieldPassword.hidden = false;
        [self.buttonAlreadyMember setSelected:NO];
        self.constraintPassword.constant = 35;
        self.constraintReenterPass.constant = 35;
        [self.buttonSignIn setTitle:@"SIGN UP" forState:UIControlStateNormal];
        
    }
    else
    {
        self.textFieldPassword.hidden=false;
        [self.buttonAlreadyMember setSelected:YES];
        [self.buttonSignIn setTitle:@"SIGN IN" forState:UIControlStateNormal];
        self.constraintPassword.constant = 35;
        self.constraintReenterPass.constant = 0;
    }
    
    [UIView animateWithDuration:0.4 animations:^{
        
        [self.view layoutIfNeeded];
    }];
}


- (IBAction)onLoginWithFacebookButtonClicked:(id)sender {
  
    
    
    //New Facebook Login Code
    
   /*
    FBSDKLoginManager *facebookLogin = [[FBSDKLoginManager alloc] init];
    
    [facebookLogin logInWithReadPermissions:@[@"email"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *facebookResult, NSError *facebookError)
    {
       
        if (facebookError) {
            //NSLog(@"Facebook login failed. Error: %@", facebookError);
            
            UIAlertView * calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Unable to retreive emailid from facebook.please enter your email id here..!" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
            [calert show];
            NSLog(@"Process error");
            [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
            loginOBJ.view.frame = CGRectMake(0, 0, 300, 455);
            [self presentPopupViewController:loginOBJ animationType:MJPopupViewAnimationFade];
            
        }
        else if (facebookResult.isCancelled) {
            //NSLog(@"Facebook login got cancelled.");
            
            UIAlertView * calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Unable to retreive emailid from facebook.please enter your email id" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
            [calert show];
            [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
            loginOBJ.view.frame = CGRectMake(0, 0, 300, 455);
            [self presentPopupViewController:loginOBJ animationType:MJPopupViewAnimationFade];
            
        }
        else
        {
            NSString *accessToken = [[FBSDKAccessToken currentAccessToken] tokenString];
            
            [[FIRAuth auth] signInWithCredential:[FIRFacebookAuthProvider credentialWithAccessToken:accessToken] completion:^(FIRUser *user, NSError *error) {
                if (error) {
                    //NSLog(@"Login failed. %@", error);
                    
                    UIAlertView * calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Unable to retreive emailid from facebook.please enter your email id" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
                    [calert show];
                    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
                    loginOBJ.view.frame = CGRectMake(0, 0, 300, 455);
                    [self presentPopupViewController:loginOBJ animationType:MJPopupViewAnimationFade];
                    
                } else {
                    //NSLog(@"Logged in! %@", user.email);
                    
                    if (user.email)
                    {
                        NSUserDefaults *userIdForGooglePlusSignIn=[NSUserDefaults standardUserDefaults];
                        [userIdForGooglePlusSignIn setObject:user.email forKey:kLoginEmailId];
                        
                        [self fetchMobileNumber];
                        
                        UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Success"
                                                                                           message:@"User Registerd Successfully"
                                                                                    preferredStyle: UIAlertControllerStyleAlert];
                        
                        UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                            
                            [alertView dismissViewControllerAnimated:YES completion:nil];
                            [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
                            if (self.delegate && [self.delegate respondsToSelector:@selector(dismissLoginPopUp)])
                            {
                                [self.delegate dismissLoginPopUp];
                                
                            }
                            
                        }];
                        
                        [alertView addAction:actionOk];
                        
                        [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
                        loginOBJ.view.frame = CGRectMake(0, 0, 300, 455);
                        [self presentPopupViewController:loginOBJ animationType:MJPopupViewAnimationFade];
                        [self presentViewController:alertView animated:YES completion:nil];
                        self.textFieldUserId.text = user.email;
                        [self registerUserForGuest];
                    }
                    else
                    {
                        UIAlertView * calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Unable to retreive emailid from facebook.please enter your email id" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
                        [calert show];
                        [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
                        loginOBJ.view.frame = CGRectMake(0, 0, 300, 455);
                        [self presentPopupViewController:loginOBJ animationType:MJPopupViewAnimationFade];
                    }
                    
                }
            }];
        }
        
    }];
    */
    
     //OLD Facebook Login Code
//    
//    FBSDKLoginManager *loginMgr = [[FBSDKLoginManager alloc] init];
//   
//    [loginMgr logInWithReadPermission
    
     FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
      login.loginBehavior = FBSDKLoginBehaviorWeb;
    [login logOut];
    [login
     logInWithReadPermissions: @[@"public_profile",@"email"]
     fromViewController:self
     handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         if (error)
         {
             
             UIAlertView * calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Unable to retreive emailid from facebook.please enter your email id here..!" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
             [calert show];
             NSLog(@"Process error");
             [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
             loginOBJ.view.frame = CGRectMake(0, 0, 300, 455);
             [self presentPopupViewController:loginOBJ animationType:MJPopupViewAnimationFade];
             
             
         }
         else if (result.isCancelled)
         {
             NSLog(@"Cancelled");
             UIAlertView * calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Unable to retreive emailid from facebook.please enter your email id" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
             [calert show];
             [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
             loginOBJ.view.frame = CGRectMake(0, 0, 300, 455);
             [self presentPopupViewController:loginOBJ animationType:MJPopupViewAnimationFade];
             
             
         }
         else {
             
             [self getBasicInfoWithResult:result];
             //             NSLog(@"Logged in");
             //             UIAlertView * calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Unable to retreive emailid from facebook.please enter your email id" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
             //             [calert show];
             //             [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
             //             loginOBJ.view.frame = CGRectMake(0, 0, 300, 455);
             //             [self presentPopupViewController:loginOBJ animationType:MJPopupViewAnimationFade];
         }
         
     }];
    
    
    
}

-(void)getBasicInfoWithResult:(FBSDKLoginManagerLoginResult *)result
{
    // loginOBJ.view.frame = CGRectMake(0, 0, 300, 455);
    
    if ([result.grantedPermissions containsObject:@"email"])
    {
        if ([FBSDKAccessToken currentAccessToken])
        {
            NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
            // [parameters setValue:@"name,picture.type(large)" forKey:@"fields"];
            [parameters setValue:@"email" forKey:@"fields"];//public_profile
            //[parameters setValue:@"public_profile" forKey:@"fields"];
     
            [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:parameters]
             startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error)
            {
                 if (!error)
                 {
                     NSLog(@"fetched user:%@", result);
                     //  NSString *name = [[result valueForKey:@"name"] lowercaseString];
                     // NSString *username = [name stringByReplacingOccurrencesOfString:@" " withString:@""];
                     // NSArray *picture_arr = [result objectForKey:@"picture"];
                     //    NSArray *data_arr = [picture_arr valueForKey:@"data"];
                     NSString *email = [result valueForKey:@"email"];
                    //  NSString *fbID = [result valueForKey:@"id"];
                     
                     if (email)
                     {
                         
                         NSUserDefaults *userIdForGooglePlusSignIn=[NSUserDefaults standardUserDefaults];
                         [userIdForGooglePlusSignIn setObject:email forKey:kLoginEmailId];
     
                         
                         //[self fetchMobileNumber];
                        
//                         UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Success"
//                                                                                            message:@"User Signed in Successfully"
//                                                                                     preferredStyle: UIAlertControllerStyleAlert];
//                         
//                         UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
//                             
//                             [alertView dismissViewControllerAnimated:YES completion:nil];
//                             [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
//                             
//                             if (self.delegate && [self.delegate respondsToSelector:@selector(dismissLoginPopUp)])
//                             {
//                                 [self.delegate dismissLoginPopUp];
//                                 
//                             }
//                             
//                         }];
//                         
//                         [alertView addAction:actionOk];
//                         
//                         [self presentViewController:alertView animated:YES completion:nil];
                         
                        self.textFieldUserId.text = [NSString stringWithFormat:@"%@", email];
                        
                       [self socialLoginWithFirstName:@"" withLastName:@"" withImageURl:@"" withType:@"FB" withUserID:email withID:@"fb1021596341229809"];
                     }
                     else
                     {
                         UIAlertView * calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Unable to retreive emailid from facebook please enter your email id" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
                         [calert show];
                         [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
                         loginOBJ.view.frame = CGRectMake(0, 0, 300, 455);
                         [self presentPopupViewController:loginOBJ animationType:MJPopupViewAnimationFade];
                     }
                 }
                else
                {
                    NSLog(@"Unable to Login");
                }
                 
             }];
        }
    }
}

- (IBAction)onLoginWithGooglePlusButtonClicked:(id)sender
{

    SignInViewController * signVC =   [[SignInViewController alloc] initWithNibName:@"SignInViewController" bundle:nil];
    signVC.googleViewDelegate =  self;
    [[SlideNavigationController sharedInstance] pushViewController:signVC animated:YES];
}

#pragma mark -Google Sign In




- (void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user withError:(NSError *)error {
    // Perform any operations on signed in user here.
    
    
    NSLog(@"Signed in user");
    
    if (error)
    {
        UIAlertView * calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Unable to retreive emailid from Google plus.please enter your email id here..!" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [calert show];
        NSLog(@"Process error");
        [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
        loginOBJ.view.frame = CGRectMake(0, 0, 300, 455);
        [self presentPopupViewController:loginOBJ animationType:MJPopupViewAnimationFade];
        NSLog(@"%@",error);
    }
    else
    {
        
        GIDAuthentication *authentication = user.authentication;
        FIRAuthCredential *credential =
        [FIRGoogleAuthProvider credentialWithIDToken:authentication.idToken
                                         accessToken:authentication.accessToken];
        
        [[FIRAuth auth] signInWithCredential:credential
                                  completion:^(FIRUser *user, NSError *error) {
                                      NSLog(@"Authentication Done");
                                      
                                      
                                      
                                  }];
        
        NSLog(@"email : %@",[GIDSignIn sharedInstance].currentUser.profile.email);
        
        if(DEBUG_ENABLED)
        {
           
            debug(@"email : %@",[GIDSignIn sharedInstance].currentUser.profile.email);
        }
        
        
        
        
        if ([GIDSignIn sharedInstance].currentUser.profile.email)
        {
            NSUserDefaults *userIdForGooglePlusSignIn=[NSUserDefaults standardUserDefaults];
            [userIdForGooglePlusSignIn setObject:[GIDSignIn sharedInstance].currentUser.profile.email forKey:kLoginEmailId];
            
            [self fetchMobileNumber];
            
            UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Success"
                                                                               message:@"User Signed in Successfully"
                                                                        preferredStyle: UIAlertControllerStyleAlert];
            
            UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                
                [alertView dismissViewControllerAnimated:YES completion:nil];
                [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
                if (self.delegate && [self.delegate respondsToSelector:@selector(dismissLoginPopUp)])
                {
                    [self.delegate dismissLoginPopUp];
                    
                }
                
            }];
            
            [alertView addAction:actionOk];
            
            [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
            loginOBJ.view.frame = CGRectMake(0, 0, 300, 455);
            [self presentPopupViewController:loginOBJ animationType:MJPopupViewAnimationFade];
            [self presentViewController:alertView animated:YES completion:nil];
            self.textFieldUserId.text=[GIDSignIn sharedInstance].currentUser.profile.email;
           // [self registerUserForGuest];
        }

        
        NSString *userId = user.userID;                  // For client-side use only!
        NSString *idToken = user.authentication.idToken; // Safe to send to the server
        NSString *name = user.profile.name;
        NSString *email = user.profile.email;
        NSLog(@"Customer details: %@ %@ %@ %@", userId, idToken, name, email);
    }
    
    

}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}



- (void)dealloc
{
   
    
}


- (void)loginButtonDidLogOut:(FBSDKLoginButton *)loginButton{
    
}

-(UIView *)popUpViewForSendSMS{
    UIView *popUpView = [UIView new];
    popUpView.frame = CGRectMake(0, 0, 300, 480);
    popUpView.backgroundColor = [UIColor whiteColor];
    popUpView.layer.cornerRadius = 1.0;
    [popUpView addSubview:self.loginView];
    return popUpView;
}

#pragma mark - Autopopulate Phone no

-(void)fetchMobileNumber
{
    NSUserDefaults *userIdForGooglePlusSignIn=[NSUserDefaults standardUserDefaults];
    
    NSString *userId=[userIdForGooglePlusSignIn valueForKey:kLoginEmailId];
    
    
    NSDictionary *dictOfData = [NSDictionary dictionaryWithObjectsAndKeys:userId,@"userName",nil];
    
    activityIndicator = [LoadingView loadingViewInView:self.view withString:@"" andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
    
    if ([self connected])
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            NetworkHelper *helper = [NetworkHelper sharedHelper];
            
            NSString *strResponse = [helper getDataFromServerForType:@"webservice" entity:@"myAccountInfo" action:@"search" andUserJson:dictOfData];
            
            NSString *strStatus = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"status"];
            NSDictionary *dictOfData=[[NSDictionary alloc]init];
            
            if ([strStatus isEqualToString:kStatusSuccess])
            {
                dictOfData= [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:kServerResponseKeyData];
                
                NSString *phoneNo = [dictOfData valueForKeyPath:@"profileDetails.phoneNo"];
                
                [userIdForGooglePlusSignIn setObject:phoneNo forKey:kPhoneNo];
                
                if (phoneNo)
                {
                    CoreUtility *coreobj=[CoreUtility sharedclassname];
                    
                    [coreobj.strPhoneNumber addObject:phoneNo];
                    
                    if( nil != phoneNo && ![phoneNo isEqual:[NSNull new]] && ![phoneNo isEqualToString:@""])
                    {
                        
                        [[NSUserDefaults standardUserDefaults]setValue:phoneNo  forKey:kuserDefaultTC_MobileNumber]; //09-04-2018 --
                        [[NSUserDefaults standardUserDefaults]synchronize];
                        
                        [[NetCoreSharedManager sharedInstance] setUpIdentity:phoneNo];
                    }
                    
                }

                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [activityIndicator removeView];
                });
                
            }
            else
            {
                
            }
        });
    }
    else
    {
        [activityIndicator removeView];
    }
}

- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}

-(void)didFinishWithGoogleLogin:(NSString * )userEmailID
{
    NSLog(@"Signed in user");
    
    
    self.userEmailID = userEmailID;
    
    [self checkedForGoogleLogin];
    
    NSLog(@"");
    
}
-(void)checkedForGoogleLogin
{
    
    if (self.userEmailID == nil || [self.userEmailID isEqualToString:@""])
    {
        
        
    }
    else if ([self.userEmailID isEqualToString:@"error"])
    {
        UIAlertView * calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Unable to retreive emailid from Google plus.please enter your email id here..!" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [calert show];
        NSLog(@"Process error");
        [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
        loginOBJ.view.frame = CGRectMake(0, 0, 300, 455);
        [self presentPopupViewController:loginOBJ animationType:MJPopupViewAnimationFade];
    }
    else
    {
        NSUserDefaults *userIdForGooglePlusSignIn=[NSUserDefaults standardUserDefaults];
        
        [userIdForGooglePlusSignIn setObject:self.userEmailID forKey:kLoginEmailId];
        
       // [self fetchMobileNumber];
        
//        UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Success"
//                                                                           message:[NSString stringWithFormat:@"User Signed in Successfully with %@",self.userEmailID]
//                                                                    preferredStyle: UIAlertControllerStyleAlert];
//        
//        UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
//        {
//            
//            [alertView dismissViewControllerAnimated:YES completion:nil];
//            [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
//            if (self.delegate && [self.delegate respondsToSelector:@selector(dismissLoginPopUp)])
//            {
//                [self.delegate dismissLoginPopUp];
//                
//            }
//        }];
//        
//        [alertView addAction:actionOk];
        
      //  [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
        
       // loginOBJ.view.frame = CGRectMake(0, 0, 300, 455);
        
      //  [self presentPopupViewController:loginOBJ animationType:MJPopupViewAnimationFade];
        
      //  [self presentViewController:alertView animated:YES completion:nil];
        
        self.textFieldUserId.text = self.userEmailID;
        
        GIDGoogleUser * googleUser = [[GIDSignIn sharedInstance] currentUser];
        
        NSString *emailId = googleUser.profile.email;
        
        NSString *name = googleUser.profile.name;

//        NSString *givenName = googleUser.profile.givenName;
//        
//        NSString *familyName = googleUser.profile.familyName;
        
        // NSString *imageURl = googleUser.profile.familyName;


        /*
         @property(nonatomic, readonly) NSString *email;
         
         // The Google user's full name.
         @property(nonatomic, readonly) NSString *name;
         
         // The Google user's given name.
         @property(nonatomic, readonly) NSString *givenName;
         
         // The Google user's family name.
         @property(nonatomic, readonly) NSString *familyName;
         
         // Whether or not the user has profile image.
         @property(nonatomic, readonly) BOOL hasImage;

         */
        
        
        
        [self socialLoginWithFirstName:name withLastName:name withImageURl:@"" withType:@"GP" withUserID:emailId withID:@"327645202876-3pmqts5pkaaqqvhh1gjh52k7sbuv468a.apps.googleusercontent.com"];
        
        //[self registerUserForGuest];
        
        
    }
    
  [[SlideNavigationController sharedInstance] popViewControllerAnimated:YES];
}

/*
 NSMutableDictionary *profileDetail = @{ @"EMAIL" : "<email>"};
 NetCoreAppTracking.sharedInstance().sendEvent(withCustomPayload: 102, payload: profileDetail, block: nil);

 */

-(void)netCoreDataForLoginWithEmailID:(NSString *)emailID
{
    
    NSMutableDictionary *payloadList =  [NSMutableDictionary dictionary];
    [payloadList setObject:emailID forKey:@"EMAIL"];
    
    [NetCoreAnalyticsVC getNetCorePushNotificationDetailDictionary:payloadList withPayloadCount:102];
}

-(void)dismissWithSuccessFullRegistration
{
   
//    UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Success"
//                                                                       message:@"User Registered Successfully"
//                                                                preferredStyle: UIAlertControllerStyleAlert];
//    
//    
//    
//    UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
//    {
    
     [self netCoreDataForLoginWithEmailID:self.textFieldUserId.text]; //09-04-2018 --
            [self setUserDetailsAndClosePopUp];
    
    //}];
    
//    [alertView addAction:actionOk];
//    
//    [self presentViewController:alertView animated:YES completion:nil];

    
  //  [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
}

#pragma mark - Astra login flow -

-(void)loginCheckWithType:(NSString *)signInType
{
    //abc@test.com/
    activityLoadingView = [LoadingView loadingViewInView:self.view.superview.superview
                                              withString:@""
                                       andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
    
    @try
    {
        
        NetworkHelper *helper = [NetworkHelper sharedHelper];
        
        NSString *pathParam = _textFieldUserId.text;
        
        NSDictionary *headerDict = [CoreUtility getHeaderDict];
        
        [helper getResponseWithRequestType:@"" withQueryParam:@"" withPathParam:pathParam withJsonParam:nil withHeaders:headerDict withUrl:kAstraUrlLoginCheck success:^(NSDictionary *responseDict)
         {
             NSLog(@"Response Dict : %@",responseDict);
             
             
             dispatch_async(dispatch_get_main_queue(), ^(void)
             {
                 [activityLoadingView removeFromSuperview];
                 if (responseDict)
                 {
                     if (responseDict.count>0)
                     {
                         if ([signInType isEqualToString:@"signIn"] || [signInType isEqualToString:@"signUp"])
                         {
                             [self savePasswordWithType:signInType];
                             
                         }else if ([signInType isEqualToString:kSignInWithOtp])
                         {
                             [self sendOtprequestWithUserName:_textFieldUserId.text andWithLoginType:signInType];
                         }
                         else if ([signInType isEqualToString:@"guestLogin"])
                         {
                             
                              [self netCoreDataForLoginWithEmailID:self.textFieldUserId.text]; //09-04-2018
                             
                             UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Success"
                                                                                                message:@"User Signed in Successfully"
                                                                                         preferredStyle: UIAlertControllerStyleAlert];
                             
                             
                             
                             UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                                        {
                                                            
                                                            [self setUserDetailsAndClosePopUp];
                                                            
                                                        }];
                             
                             [alertView addAction:actionOk];
                             
                             [self presentViewController:alertView animated:YES completion:nil];

                            
                         }
                         else if([signInType isEqualToString:@"ForgotPassword"])
                         {
                              NSString *messege = [responseDict valueForKey:@"message"];
                             
                             if ([[messege lowercaseString] isEqualToString:@"false"])
                             {
                                 
                                 UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Alert" message:@"Email Id is not registered with us."                                                                                             preferredStyle: UIAlertControllerStyleAlert];
                                 
                                 
                                 
                                 UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                                            {
                                                                [self setUserDetailsAndClosePopUp];
                                                            }];
                                 
                                 [alertView addAction:actionOk];
                                 
                                 [self presentViewController:alertView animated:YES completion:nil];
                                 

                             }
                             else
                             {
                                 [self resetPasswordAstraAPI];
                             }
                                 
                             
                         }
                     }
                     
                 }
             });
             
         }
                                   failure:^(NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^(void)
                            {
                                [activityLoadingView removeFromSuperview];
                                NSLog(@"Response Dict : %@",[error description]);
                            });
             
             
             
         }];
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"%@", exception.reason);
        [activityLoadingView removeFromSuperview];
    }
    @finally
    {
        NSLog(@"exception finally called");
         [activityLoadingView removeFromSuperview];
    }
}

-(void)savePasswordWithType:(NSString *)signInType

{
    activityLoadingView = [LoadingView loadingViewInView:self.view.superview.superview
                                              withString:@""
                                       andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
    
    @try
    {
        
        NetworkHelper *helper = [NetworkHelper sharedHelper];
        
     
        
        NSDictionary *headerDict = [CoreUtility getHeaderDict];
        
        [helper getResponseWithRequestType:@"" withQueryParam:@"" withPathParam:@"" withJsonParam:nil withHeaders:headerDict withUrl:kAstraUrlSavePass success:^(NSDictionary *responseDict)
         {
             NSLog(@"Response Dict : %@",responseDict);
             
             
             dispatch_async(dispatch_get_main_queue(), ^(void) {
                 [activityLoadingView removeFromSuperview];
                 
                 if (responseDict)
                 {
                     if (responseDict.count>0)
                     {
                         NSString *keyString = [responseDict valueForKey:@"text"];

                        [self fetchEncryptionPassWithKey:keyString withloginType:signInType];
                         
                     }
                 }
                 
                 
             });
             
         }
                                   failure:^(NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^(void)
                            {
                                [activityLoadingView removeFromSuperview];
                                NSLog(@"Response Dict : %@",[error description]);
                            });
             
             
             
         }];
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"%@", exception.reason);
        [activityLoadingView removeFromSuperview];
    }
    @finally
    {
        NSLog(@"exception finally called");
         [activityLoadingView removeFromSuperview];
    }

}

-(void)fetchEncryptionPassWithKey:(NSString *)key withloginType:(NSString *)loginType
{
    activityIndicator = [LoadingView loadingViewInView:self.view withString:@"" andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
    
    
    //            "key": "495kcG744o"

    NSDictionary *dict = [[NSDictionary alloc]initWithObjectsAndKeys:key, @"key",_textFieldPassword.text,@"password",nil];
    
    NetworkHelper *helper = [NetworkHelper sharedHelper];
    if([CoreUtility connected])
    {
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            NSString *strResponse = [helper getDataFromServerForType:@"sync" entity:@"encrypt" action:@"generate" andUserJson:dict];
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                [activityIndicator removeFromSuperview];
                if (![strResponse isEqualToString:@"No Internet"])
                {
                    NSString *strStatus = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"Status"];
                    NSString *strMessage = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"message"];
                    
                    if ([[strStatus lowercaseString] isEqualToString:kStatusSuccess])
                    {
                        
                        NSDictionary *dataDict = [[[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"data"] firstObject];
                        
                        if ([loginType isEqualToString:@"signIn"])
                        {
                            NSString *text1 = [dataDict valueForKey:@"text1"];
                            NSString *text2 = [dataDict valueForKey:@"text2"];
                            NSString *text3 = [dataDict valueForKey:@"text3"];
                            
                            [self verifyCredentialsWithText1:text1 withText2:text2 withText3:text3 withloginType:loginType withOtp:@""];
                            
                        }
                        else
                        {
                            RegisterUserViewController *registerUserVC = [[RegisterUserViewController alloc] initWithNibName:@"RegisterUserViewController" bundle:nil];
                            registerUserVC.dictforEncryption = dataDict;
                            registerUserVC.userId = self.textFieldUserId.text;
                            registerUserVC.delegate = self;
                           // [self presentViewController:registerUserVC animated:YES completion:nil];
                            
                             [[SlideNavigationController sharedInstance] pushViewController:registerUserVC animated:YES];
                        }
                       
                    }
                    else
                    {
                        UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Alert"
                                                                                           message:strMessage
                                                                                    preferredStyle: UIAlertControllerStyleAlert];
                        
                        UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                            
                            [alertView dismissViewControllerAnimated:YES completion:nil];
                            
                        }];
                        [alertView addAction:actionOk];
                        [self presentViewController:alertView animated:YES completion:nil];
                    }
                }
            });
        });
        
    }
    else
    {
        UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Alert"
                                                                           message:kMessageNoInternet
                                                                    preferredStyle: UIAlertControllerStyleAlert];
        
        UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
            [alertView dismissViewControllerAnimated:YES completion:nil];
            
        }];
        
        [alertView addAction:actionOk];
        [self presentViewController:alertView animated:YES completion:nil];
    }

}

-(void)verifyCredentialsWithText1:(NSString *)text1 withText2:(NSString *)text2 withText3:(NSString *)text3 withloginType:(NSString *)loginType withOtp:(NSString*)otp
{
    
    activityLoadingView = [LoadingView loadingViewInView:self.view.superview.superview
                                              withString:@""
                                       andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
    
    @try
    {
        
        NetworkHelper *helper = [NetworkHelper sharedHelper];
        
        

        NSMutableDictionary *dictForRequest = [[NSMutableDictionary alloc] init];
        [dictForRequest setObject:_textFieldUserId.text forKey:@"userId"];
        [dictForRequest setObject:text1 forKey:@"text1"];
        [dictForRequest setObject:text2 forKey:@"text2"];
        [dictForRequest setObject:text3 forKey:@"text3"];
        
        if ([loginType caseInsensitiveCompare:kSignInWithOtp] == NSOrderedSame)
        {
            [dictForRequest setObject:otp forKey:@"otp"];
            
        }

        NSDictionary *headerDict = [CoreUtility getHeaderDict];
        
        [helper getResponseWithRequestType:@"POST" withQueryParam:@"" withPathParam:@"" withJsonParam:dictForRequest withHeaders:headerDict withUrl:kAstraVerifyCredentials success:^(NSDictionary *responseDict)
         {
             NSLog(@"Response Dict : %@",responseDict);
             
             
             dispatch_async(dispatch_get_main_queue(), ^(void) {
                 [activityLoadingView removeFromSuperview];
                 if (responseDict)
                 {
                     if (responseDict.count>0)
                     {
                         if ([[[responseDict valueForKey:@"message"] lowercaseString] isEqualToString:@"false"])
                         {
                             NSString *reasonOfMessage = [responseDict valueForKey:@"reasonOfMessage"];
                             
                             UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Alert"
                                                                                                message:reasonOfMessage
                                                                                         preferredStyle: UIAlertControllerStyleAlert];
                             
                             UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                                 
                                 [alertView dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
                             [alertView addAction:actionOk];
                             [self presentViewController:alertView animated:YES completion:nil];
                             
                         }
                         else
                         {
                             
                              [self netCoreDataForLoginWithEmailID:self.textFieldUserId.text]; //09-04-2018
                             
                             NSDictionary * userDetailsDict = [responseDict valueForKey:@"userDetail"]; //09-04-2018 --
                             
                            if( userDetailsDict && ![userDetailsDict isEqual:[NSNull new]]) //09-04-2018 --
                               {
                                   NSString * mobileNoString =[userDetailsDict valueForKey:@"mobileNo"];
                                   
                                   if (nil != mobileNoString && ![mobileNoString isEqual:[NSNull new]] && ![mobileNoString isEqualToString:@""])
                                   {
                                       
                                       [[NSUserDefaults standardUserDefaults]setValue:mobileNoString forKey:kuserDefaultTC_MobileNumber]; //09-04-2018 --
                                       
                                       [[NSUserDefaults standardUserDefaults] synchronize];
                                       
                                       [[NetCoreSharedManager sharedInstance] setUpIdentity:mobileNoString]; //24-04-2018 --
                                   }
                                  
                               }
                             
                             
                             
                             
                             UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Alert" message:@"Login successful" preferredStyle: UIAlertControllerStyleAlert];
                             
                             UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                             {
                                 [self setUserDetailsAndClosePopUp];
                                 
                                 

                                 [alertView dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
                             [alertView addAction:actionOk];
                             [self presentViewController:alertView animated:YES completion:nil];

                         }
                     }
                 }
             });
             
         }
                                   failure:^(NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^(void)
                            {
                                [activityLoadingView removeFromSuperview];
                                NSLog(@"Response Dict : %@",[error description]);
                            });
             
             
             
         }];
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"%@", exception.reason);
        [activityLoadingView removeFromSuperview];
    }
    @finally
    {
        NSLog(@"exception finally called");
         [activityLoadingView removeFromSuperview];
    }
}


-(void)socialLoginWithFirstName:(NSString *)firstName withLastName:(NSString *)lastName withImageURl:(NSString *)imageURL withType:(NSString *)type withUserID:(NSString *)userID withID:(NSString *)Id
{
    activityLoadingView = [LoadingView loadingViewInView:self.view.superview.superview
                                              withString:@""
                                       andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
    
    @try
    {
        
        NetworkHelper *helper = [NetworkHelper sharedHelper];
        NSMutableDictionary *dictForRequest = [[NSMutableDictionary alloc] init];
        [dictForRequest setObject:firstName forKey:@"firstName"];
        [dictForRequest setObject:Id forKey:@"id"];
        [dictForRequest setObject:imageURL forKey:@"imgUrl"];
        [dictForRequest setObject:lastName forKey:@"lastName"];
        [dictForRequest setObject:type forKey:@"type"];
        [dictForRequest setObject:userID forKey:@"userId"];

        NSDictionary *headerDict = [CoreUtility getHeaderDict];
        
        [helper getResponseWithRequestType:@"POST" withQueryParam:@"" withPathParam:@"" withJsonParam:dictForRequest withHeaders:headerDict withUrl:kAstraSocialLogin success:^(NSDictionary *responseDict)
         {
             NSLog(@"Response Dict : %@",responseDict);
             
             
             dispatch_async(dispatch_get_main_queue(), ^(void) {
                 [activityLoadingView removeFromSuperview];
                 if (responseDict)
                 {
                     if (responseDict.count>0)
                     {
                         
                         NSDictionary * userDetailsDict = [responseDict valueForKey:@"userDetail"]; //09-04-2018 --
                         
                         if( userDetailsDict && ![userDetailsDict isEqual:[NSNull new]]) //09-04-2018 --
                         {
                             NSString * mobileNoString =[userDetailsDict valueForKey:@"mobileNo"];
                             
                             if (nil != mobileNoString && ![mobileNoString isEqual:[NSNull new]] && ![mobileNoString  isEqualToString:@""])
                             {
                                 
                                 [[NSUserDefaults standardUserDefaults]setValue:mobileNoString forKey:kuserDefaultTC_MobileNumber]; //09-04-2018 --
                                 
                                 [[NSUserDefaults standardUserDefaults] synchronize];
                                 
                                 [[NetCoreSharedManager sharedInstance] setUpIdentity:mobileNoString];//24-04-2018 --
                             }
                         }
                         
                         
                         
                         UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Success"
                                                                                            message:@"User Signed in Successfully"
                                                                                     preferredStyle: UIAlertControllerStyleAlert];
                         
                          [self netCoreDataForLoginWithEmailID:self.textFieldUserId.text]; //09-04-2018
                         
                         UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                                    {                                                       
                                                        [self setUserDetailsAndClosePopUp];
                                                    }];
                         
                         [alertView addAction:actionOk];
                         
                         [self presentViewController:alertView animated:YES completion:nil];

                     }
                 }
                 else
                 {
                     UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Failure"
                                                                                        message:@"Some error occured"
                                                                                 preferredStyle: UIAlertControllerStyleAlert];
                     
                     
                     
                     UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                                {
                                                    
                                                    //[self setUserDetailsAndClosePopUp];
                                                }];
                     
                     [alertView addAction:actionOk];
                     
                     [self presentViewController:alertView animated:YES completion:nil];
                 }
             });
             
         }
                                   failure:^(NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^(void)
                            {
                                [activityLoadingView removeFromSuperview];
                                
                                UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Failure"
                                                                                                   message:[error description]
                                                                                            preferredStyle: UIAlertControllerStyleAlert];
                                
                                
                                
                                UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                                           {
                                                               
                                                               //[self setUserDetailsAndClosePopUp];
                                                           }];
                                
                                [alertView addAction:actionOk];
                                
                                [self presentViewController:alertView animated:YES completion:nil];

                                NSLog(@"Response Dict : %@",[error description]);
                            });
             
             
             
         }];
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"%@", exception.reason);
        [activityLoadingView removeFromSuperview];
    }
    @finally
    {
        NSLog(@"exception finally called");
         [activityLoadingView removeFromSuperview];
    }
}

-(void)setUserDetailsAndClosePopUp
{
    [[NSUserDefaults standardUserDefaults]setValue:self.textFieldUserId.text forKey:kuserDefaultUserId]
    ;
    [[NSUserDefaults standardUserDefaults]setValue:self.textFieldPassword.text forKey:kuserDefaultPassword]
    ;
    
    NSUserDefaults *LoginStatus=[NSUserDefaults standardUserDefaults];
    [LoginStatus setObject:kLoginSuccess  forKey:kLoginStatus];
    
    NSUserDefaults *userIdForGooglePlusSignIn=[NSUserDefaults standardUserDefaults];
    
    [userIdForGooglePlusSignIn setObject:self.textFieldUserId.text forKey:kLoginEmailId];
    
  //  [[NetCoreSharedManager sharedInstance] setUpIdentity:self.textFieldUserId.text];
    
    //[self fetchMobileNumber];
    
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];

}


-(void)resetPasswordAstraAPI
{
    //2. https://services-uatastra.thomascook.in/TcilMyAccount/login/forgetPassword/test@test.com
    
    
    activityLoadingView = [LoadingView loadingViewInView:self.view.superview.superview
                                              withString:@""
                                       andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
    
    @try
    {
        
        
        NSString *pathParameter = self.textFieldUserId.text;
        
        NetworkHelper *helper = [NetworkHelper sharedHelper];
        
        NSDictionary *headerDict = [CoreUtility getHeaderDict];
        
        [helper getResponseWithRequestType:@"" withQueryParam:@"" withPathParam:pathParameter withJsonParam:nil withHeaders:headerDict withUrl:kAstraUrlForgotPassword success:^(NSDictionary *responseDict)
         {
             NSLog(@"Response Dict : %@",responseDict);
             
             dispatch_async(dispatch_get_main_queue(), ^(void) {
                 [activityLoadingView removeFromSuperview];
                 if (responseDict)
                 {
                     if (responseDict.count>0)
                     {
                        // NSString *keyString = [responseDict valueForKey:@"text"];
                        // Success Response : {"accountType":"Password Set","custId":0,"message":"true"}
                         
                         NSString *messege = [responseDict valueForKey:@"message"];
                         
                         if ([[messege lowercaseString] isEqualToString:@"true"])
                         {
                             UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Alert" message:@"Your request for new password has been accepted.The new password would be emailed to the registered email address only.Please do write us at support@thomascook.in in case you any require any assistance."
                                                                                         preferredStyle: UIAlertControllerStyleAlert];
                             
                             UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                                        {
                                                            
                                        [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
                                                        }];
                             
                             [alertView addAction:actionOk];
                             
                             [self presentViewController:alertView animated:YES completion:nil];

                         }
                         else
                         {
                             UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Failure"
                                                                                                message:@"Some error occured"
                                                                                         preferredStyle: UIAlertControllerStyleAlert];
                             
                             
                             
                             UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                                        {
                                                            
                                                            //[self setUserDetailsAndClosePopUp];
                                                        }];
                             
                             [alertView addAction:actionOk];
                             
                             [self presentViewController:alertView animated:YES completion:nil];

                         }
                     }
                 }
             });
             
         }
                                   failure:^(NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^(void)
                            {
                                [activityLoadingView removeFromSuperview];
                                NSLog(@"Response Dict : %@",[error description]);
                            });
             
             
             
         }];
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"%@", exception.reason);
        [activityLoadingView removeFromSuperview];
    }
    @finally
    {
        NSLog(@"exception finally called");
         [activityLoadingView removeFromSuperview];
    }

    
}

-(void)sendOtprequestWithUserName:(NSString *)userName andWithLoginType:(NSString *)loginType
{
    activityLoadingView = [LoadingView loadingViewInView:self.view.superview.superview
                                              withString:@""
                                       andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
    
    @try
    {
        
        NetworkHelper *helper = [NetworkHelper sharedHelper];
        
        NSString *pathParam = _textFieldUserId.text;
        
        NSDictionary *headerDict = [CoreUtility getHeaderDict];
        
        
        [helper getResponseWithRequestType:@"" withQueryParam:@"" withPathParam:pathParam withJsonParam:nil withHeaders:headerDict withUrl:kAstraUrlSendOtp success:^(NSDictionary *responseDict)
         {
             NSLog(@"Response Dict : %@",responseDict);
             
             
             dispatch_async(dispatch_get_main_queue(), ^(void)
                            {
                                [activityLoadingView removeFromSuperview];
                                if (responseDict != nil && ![responseDict isEqual:[NSNull new]])
                                {
                                  NSString * isValidUser = [responseDict objectForKey:@"text"];
                                    
                                    if   (isValidUser != nil && ![isValidUser isEqual:[NSNull new]] && ![isValidUser isEqualToString:@""] && [isValidUser caseInsensitiveCompare:@"true"] == NSOrderedSame)
                                    {
                                        UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Alert" message:@"otp is sent to your registered email"
                                                                                                    preferredStyle: UIAlertControllerStyleAlert];
                                        
                                        UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                                                   {
                                                                       
                                                                       [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
                                                                   }];
                                        
                                        [alertView addAction:actionOk];
                                        
                                        [self presentViewController:alertView animated:YES completion:nil];
                                        
                                        return;
                                        
                                    }
                                    
                                }
                                
                                UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Failure"
                                                                                                   message:@"Some error occured"
                                                                                            preferredStyle: UIAlertControllerStyleAlert];
                                
                                
                                
                                UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                                           {
                                                               
                                                               //[self setUserDetailsAndClosePopUp];
                                                           }];
                                
                                [alertView addAction:actionOk];
                                
                                [self presentViewController:alertView animated:YES completion:nil];
                                
                                return;
                                
                               
                                
                            });
             
             
             
         }
            failure:^(NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^(void)
                            {
                                [activityLoadingView removeFromSuperview];
                                NSLog(@"Response Dict : %@",[error description]);
                            });
             
             
             
         }];
    }@catch (NSException *exception)
    {
        NSLog(@"%@", exception.reason);
        [activityLoadingView removeFromSuperview];
    }
    @finally
    {
        NSLog(@"exception finally called");
        [activityLoadingView removeFromSuperview];
    }
    
}

@end
