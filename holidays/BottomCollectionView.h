//
//  BottomCollectionView.h
//  holidays
//
//  Created by Kush_Tech on 24/02/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BottomCollectionView : UIView<UICollectionViewDelegate,UICollectionViewDataSource>{
    NSArray *imageArray;
    NSArray *titleArray;
}
@property (weak, nonatomic) IBOutlet UICollectionView *bottom_collectionView;

@end

NS_ASSUME_NONNULL_END
