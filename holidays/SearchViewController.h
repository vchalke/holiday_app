//
//  SearchViewController.h
//  holidays
//
//  Created by ketan on 23/12/15.
//  Copyright © 2015 Mobicule. All rights reserved.
//

#import "BaseViewController.h"

@interface SearchViewController : BaseViewController
@property (strong, nonatomic) IBOutlet UIView *searchView;

@end
