//
//  DefaultHeaderRequestBuilder.m
//  genie
//
//  Created by Saurav Kumar on 07/05/15.
//  Copyright (c) 2015 Mobicule. All rights reserved.
//


#define CIRCLE_CODE @"circleCode"

#define PASSWORD @"password"

#define LOGIN @"login";

#define ENTITY @"entity";

#define ACTION @"action";

#define TYPE @"type";

#define DATA @"data";

#define USER @"user";

#import "DefaultHeaderRequestBuilder.h"

@implementation DefaultHeaderRequestBuilder



-(id)initWithType:(NSString *)type withEntity:(NSString *)entity withAction:(NSString *)action withUserJson:(NSDictionary *)userJsonDict
{
    
    self = [super init];
    if(self)
    {
        Type = type;
        Entity = entity;
        Action = action;
        dictUserJson = userJsonDict;
    
        jsonDictionary = [[NSMutableDictionary alloc]init];

    }
    
    return self;
    
}
-(void)addRequestHeader
{

    [jsonDictionary setValue:Type forKey:@"type"];    
    [jsonDictionary setValue:Entity forKey:@"entity"];
    [jsonDictionary setValue:Action forKey:@"action"];
    [jsonDictionary setValue:@"ios" forKey:@"platform"];
    NSString * version = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
    [jsonDictionary setValue:version forKey:@"version"];
}

-(void)addAuthorizationDetails
{
    
}


-(void)addRequestData
{
    [jsonDictionary setValue:dictUserJson forKey:@"data"];
}

-(NSString *)build
{
    [self addRequestHeader];
    [self addRequestData];
    
    NSLog(@" Build Request %@",[JsonSerealizer stringRepresenationFromJSonDataObject:jsonDictionary]);
    
    return [JsonSerealizer stringRepresenationFromJSonDataObject:jsonDictionary];
}

-(void)setRequestData:(NSString *)jsonValue
{
  
}

-(void)putWithKey:(NSString *)key withValue:(NSObject *)object
{
            
}

@end
