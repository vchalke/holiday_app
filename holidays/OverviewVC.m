//
//  OverviewVC.m
//  holidays
//
//  Created by Pushpendra Singh on 18/08/15.
//  Copyright (c) 2015 Mobicule. All rights reserved.
//

#import "OverviewVC.h"
#import "BookNowViewController.h"
#import "SlideNavigationController.h"
#import "ImageViewCell.h"
#import "UIImageView+WebCache.h"
//#import <NetCorePush/NetCorePush.h>
#import "NetCoreAnalyticsVC.h"

@interface OverviewVC ()
@property (weak, nonatomic) IBOutlet UILabel *lblAccomodation;
@property (weak, nonatomic) IBOutlet UILabel *lblVisa;
@property (weak, nonatomic) IBOutlet UILabel *lblSightSeeing;
@property (weak, nonatomic) IBOutlet UILabel *lblDinner;
@property (weak, nonatomic) IBOutlet UILabel *lblFlights;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightForFlightView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightForDinnerView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightForSightSeeingView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightForVisaView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightForAccomodationView;

@end

@implementation OverviewVC
{
    NSArray *siteArray;
    NSTimer *tmpTime;
    BOOL isAppeared;
    LoadingView *activityLoadingView;
    KLCPopup *customePopUp;
    UIScrollView *popUpEmailContainer;
    NSMutableArray *arrayForImageData;
}


@synthesize packageDetail;

-(void)viewDidAppear:(BOOL)animated
{
    NSArray* accomArray = self.packageDetail.arrayAccomTypeList;
    
    
    
    NSDictionary * packageDict = [self.completePackageDetail objectAtIndex:0];
    
    
    
    if (self.packageDetail.stringSelectedAccomType == nil)
    {
        
        if ([accomArray count] > 0)
        {
            [self setAccomType:[accomArray objectAtIndex:0]];
        }
    }
    else
    {
        if ([self.packageDetail.stringSelectedAccomType isEqualToString:@"0"])
        {
            self.lblPackageCost.text = [NSString stringWithFormat:@"Rs. %d",[[packageDict valueForKey:@"startingPriceStandard"] intValue]];
        }
        else if([self.packageDetail.stringSelectedAccomType isEqualToString:@"1"])
        {
            self.lblPackageCost.text = [NSString stringWithFormat:@"Rs. %d",[[packageDict valueForKey:@"startingPriceDelux"] intValue]];
        }
        else if ([self.packageDetail.stringSelectedAccomType isEqualToString:@"2"])
        {
            self.lblPackageCost.text = [NSString stringWithFormat:@"Rs. %d",[[packageDict valueForKey:@"startingPricePremium"] intValue]];
        }
    }

}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    arrayForImageData=[[NSMutableArray alloc]init];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardShown)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardHidden)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    [self setSite];
    
    
    self.lblNightAndDays.text = [NSString stringWithFormat:@"%dN %dD",packageDetail.durationNoDays-1,packageDetail.durationNoDays];
    
    if ([[packageDetail.strPackageSubType lowercaseString]isEqualToString:@"git"])
    {
        self.lblGroupTour.text = @"Group Tour";
    }
    else
    {
        self.lblGroupTour.text = @"Personalised Tour";
    }
    
    
    //    if(![[packageDetail.strPackageSubType lowercaseString] isEqualToString:@"git"])
    //    {
    //        self.lblGroupTour.alpha = 0;
    //    }
    
//    NSString *strCostPP = [NSString stringWithFormat:@"Rs. %d",packageDetail.packagePrise];
//    
//    self.lblPackageCost.text = strCostPP;
    
    
     [self setOverViewImage];
   // [self performSelectorInBackground:@selector(setOverViewImage) withObject:nil];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

-(void)viewDidLayoutSubviews
{
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    
    if ([self.packageDetail.stringNotesDesc isEqualToString:@""]||self.packageDetail.stringNotesDesc == nil)
    {
        self.viewOverView.frame = CGRectMake(0, 0, screenBounds.size.width, 625);
    }
    else
    {
        self.conditionsTextView.text = self.packageDetail.stringNotesDesc;
        self.viewOverView.frame = CGRectMake(0, 0, screenBounds.size.width, 625);
    }
    
    NSLog(@"%@",NSStringFromCGRect(self.viewOverView.frame));
    [self.scrollViewOverView addSubview:self.viewOverView];
    [self.scrollViewOverView setContentSize:self.viewOverView.frame.size];
    
    [self configureInclusions];
}

-(void)setAccomType:(NSString *)accomType
{
    NSDictionary * packageDict = [self.completePackageDetail objectAtIndex:0];
    

    if ([[accomType lowercaseString] isEqualToString:@"standard"])
    {
        self.packageDetail.stringSelectedAccomType = @"0";
        self.lblPackageCost.text = [NSString stringWithFormat:@"Rs. %@",[packageDict valueForKey:@"startingPriceStandard"]];

    }
    else if ([[accomType lowercaseString] isEqualToString:@"delux"]||[[accomType lowercaseString] isEqualToString:@"deluxe"])
    {
        self.packageDetail.stringSelectedAccomType = @"1";
        self.lblPackageCost.text = [NSString stringWithFormat:@"Rs. %d",[[packageDict valueForKey:@"startingPriceDelux"]  intValue]];
    }
    else
    {
        self.packageDetail.stringSelectedAccomType = @"2";
        self.lblPackageCost.text = [NSString stringWithFormat:@"Rs. %d",[[packageDict valueForKey:@"startingPricePremium"]  intValue]];
    }
}



-(void)configureInclusions
{
    
   
    NSDictionary * packageDict = [self.completePackageDetail objectAtIndex:0];
    
    NSDictionary *dict = [packageDict valueForKey:@"packageDetail"];
   
    if (packageDetail.categoryCollection.count > 0) {
        
        if (packageDetail.mealsFlag == YES)
            {
                
                NSMutableString * htmlString=[NSMutableString new] ;
                
                if ([[packageDetail mealCollection] count] > 0) {
                    
                    NSDictionary *mealDict = [[packageDetail mealCollection] firstObject];
                    
                    if (![[mealDict objectForKey:@"typeDefaultMsg"]  isEqualToString:@" "])
                                          {
                                      NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[[mealDict objectForKey:@"typeDefaultMsg"] dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
                                              
                                          NSString *code = [attrStr.string substringFromIndex:[attrStr.string length] - 1];
                                              
                                              if ([code isEqualToString:@"\n"])
                                              {
                                                  NSLog(@"%@",attrStr.string);
                                                  
                                                  self.lblDinner.text = [attrStr.string substringToIndex:[attrStr.string length] - 1];

                                              }
                                              else
                                              {
                                                  self.lblDinner.attributedText = attrStr;

                                              }
                                                  
                                     // self.lblDinner.font = [UIFont systemFontOfSize:13.0];

                                  }
                                  else
                                  {
                                      self.lblDinner.text = @"included as per itinerary";//mealsDefaultMsg

                                  }
                }else{
                     if (![[dict objectForKey:@"mealsDefaultMsg"]  isEqualToString:@""])
                                   {
                               NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[[dict objectForKey:@"mealsDefaultMsg"] dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
                                       
                                   NSString *code = [attrStr.string substringFromIndex:[attrStr.string length] - 1];
                                       
                                       if ([code isEqualToString:@"\n"])
                                       {
                                           NSLog(@"%@",attrStr.string);
                                           
                                           self.lblDinner.text = [attrStr.string substringToIndex:[attrStr.string length] - 1];

                                       }
                                       else
                                       {
                                           self.lblDinner.attributedText = attrStr;

                                       }
                                           
                              // self.lblDinner.font = [UIFont systemFontOfSize:13.0];

                           }
                           else
                           {
                               self.lblDinner.text = @"included as per itinerary";//mealsDefaultMsg

                           }
                }
                
              
                self.lblDinner.font = [UIFont fontWithName:@"Titillium-regular" size:13];


            }
            else
            {
                _imageMeal.hidden = YES;
                self.lblDinner.text = @"";
                
            }
                
            
            if (packageDetail.airFlag == YES)
            {
                
        //        if ([[[dict objectForKey:@"isFlightDefaultMsg"] lowercaseString] isEqualToString:@"y"])
        //        {
                    if (![[dict objectForKey:@"flightDefaultMsg"]  isEqualToString:@""])
                    {
                    
                    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[[dict objectForKey:@"flightDefaultMsg"] dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
                        NSString *code = [attrStr.string substringFromIndex:[attrStr.string length] - 1];
                        
                        if ([code isEqualToString:@"\n"])
                        {
                            NSLog(@"%@",attrStr.string);
                            
                            self.lblFlights.text = [attrStr.string substringToIndex:[attrStr.string length] - 1];
                            
                        }
                        else
                        {

                         self.lblFlights.attributedText = attrStr;
                        }
                }
                else
                {
                    self.lblFlights.text = @"included as per itinerary";//mealsDefaultMsg
                   // self.lblFlights.font = [UIFont systemFontOfSize:13.0];

                }

                self.lblFlights.font = [UIFont fontWithName:@"Titillium-regular" size:13];

            }
            else
            {      _imageFlight.hidden = YES;
                  self.lblFlights.text = @"";

            }
            
            if (packageDetail.tourMngerFlag == YES)
            {
                if ([[packageDetail visaCollection] count]>0) {
                    NSDictionary *visaDict = [[packageDetail visaCollection] firstObject];
                    
                           //        if ([[[dict objectForKey:@"isVisaDefaultMsg"] lowercaseString] isEqualToString:@"y"])
                           //        {
                                       if (![[visaDict objectForKey:@"typeDefaultMsg"]  isEqualToString:@" "])
                                       {

                                       NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[[visaDict objectForKey:@"typeDefaultMsg"] dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
                                           NSString *code = [attrStr.string substringFromIndex:[attrStr.string length] - 1];
                                           
                                           if ([code isEqualToString:@"\n"])
                                           {
                                               NSLog(@"%@",attrStr.string);
                                               
                                               self.lblVisa.text = [attrStr.string substringToIndex:[attrStr.string length] - 1];
                                               
                                           }
                                           else
                                           {

                                          self.lblVisa.attributedText = attrStr;
                                           }
                                      // self.lblVisa.font = [UIFont systemFontOfSize:13.0];

                                   }
                                   else
                                   {
                                       self.lblVisa.text = @"included as per itinerary";//mealsDefaultMsg
                                       
                                   }
                                   
                                   self.lblVisa.font = [UIFont fontWithName:@"Titillium-regular" size:13];

                               }
                               else
                               {
                                       _imageVisa.hidden = YES;
                                       self.lblVisa.text = @"";
                               }
                               
               if (packageDetail.accomFlag == YES)
               {
                   NSMutableString * htmlString=[NSMutableString new] ;
                          NSDictionary *accomDict = [[packageDetail accombdationCollection] firstObject];
                   if (accomDict.count > 0) {
                       if (![[accomDict objectForKey:@"typeDefaultMsg"]  isEqualToString:@" "])
                                                                                                  {
                                                                          NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[[accomDict objectForKey:@"typeDefaultMsg"] dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
                                                                              NSString *code = [attrStr.string substringFromIndex:[attrStr.string length] - 1];
                                                                              
                                                                              if ([code isEqualToString:@"\n"])
                                                                              {
                                                                                  NSLog(@"%@",attrStr.string);
                                                                                  
                                                                                  self.lblAccomodation.text = [attrStr.string substringToIndex:[attrStr.string length] - 1];
                                                                                  
                                                                              }
                                                                              else
                                                                              {
                                                                                  self.lblAccomodation.attributedText = attrStr;
                                                                              }
                                                                              
                                                                          //self.lblAccomodation.font = [UIFont systemFontOfSize:13.0];
                                                                      }
                                                                      else
                                                                      {
                                                                          self.lblAccomodation.text = @"included as per itinerary";//mealsDefaultMsg
                                                                      }
                   }else{
                     
                                  
                   
                   //
                       
                       if (![[dict objectForKey:@"hotelDefaultMsg"]  isEqualToString:@""])
                           {

                           NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[[dict objectForKey:@"hotelDefaultMsg"] dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
                               NSString *code = [attrStr.string substringFromIndex:[attrStr.string length] - 1];
                               
                               if ([code isEqualToString:@"\n"])
                               {
                                   NSLog(@"%@",attrStr.string);
                                   
                                   self.lblAccomodation.text = [attrStr.string substringToIndex:[attrStr.string length] - 1];
                                   
                               }
                               else
                               {
                                   self.lblAccomodation.attributedText = attrStr;
                               }
                               
                           //self.lblAccomodation.font = [UIFont systemFontOfSize:13.0];
                       }
                       else
                       {
                           self.lblAccomodation.text = @"included as per itinerary";//mealsDefaultMsg
                       }
                   
                   
                   }
                                  
                   
                   
                   
                   
                   
                }else{
                     self.lblAccomodation.text = @"included as per itinerary";//mealsDefaultMsg
                }
               
                
                
                
               /* }else{
                    NSArray * accomdationList = [dict objectForKey:@"tcilHolidayAccomodationCollection"];
                    
                    NSPredicate * predicate1 =  [NSPredicate predicateWithFormat:@"(packageClassId == %@) ", @"0" ];
                    
                    NSArray *filteredArray = [accomdationList filteredArrayUsingPredicate:predicate1];
                    
                    for (NSDictionary * accomDict  in filteredArray)
                    {
                        NSMutableString *stringOfImages = [NSMutableString new] ;
                        
                        NSDictionary *hotelIDDict =  [accomDict valueForKey:@"accomodationHotelId"];
                        
                        
                        //NSString *starRatingString = [[hotelIDDict valueForKey:@"starRating"] intValue];
                        
                        NSString *totalStar = @"5";
                        NSString *selectedStar =[NSString stringWithFormat:@"%d", [[hotelIDDict valueForKey:@"starRating"] intValue]];
                       
                        
                        for (int i = 0; i<[totalStar intValue]; i++)
                        {
                            NSString *imageNameURL;
                            if (i < [selectedStar intValue])
                            {
                                imageNameURL = [[NSBundle mainBundle] pathForResource:@"rating_selected" ofType:@"png"];
                            }else
                            {
                                imageNameURL = [[NSBundle mainBundle] pathForResource:@"rating_unselected" ofType:@"png"];
                            }
                            
                            NSURL *urlImage = [NSURL fileURLWithPath:imageNameURL];
                            [stringOfImages appendString:[NSString stringWithFormat:@"<img src=\"%@\" style=\"padding-left:2%%\" width=\"5%%\">",urlImage]];
                        }
                        [stringOfImages appendString:@"</br>"];
                        [htmlString appendString:[NSString stringWithFormat:@"<html><head></head><body><font face=\"Titillium-Regular\" color=\"gray\" size=\"4\">&nbsp&nbsp&nbsp%@</font><br><font face=\"Titillium-Regular\" color=\"gray\" size=\"2\">&nbsp&nbsp&nbsp&nbsp%@ Nights <br>&nbsp&nbsp&nbsp&nbsp%@</br>%@</body></html></br>" ,[hotelIDDict objectForKey:@"hotelName"] ,[NSString stringWithFormat:@"%d",[[accomDict objectForKey:@"noOfNights"] intValue]],[hotelIDDict objectForKey:@"city"],stringOfImages]];
                    }
                    
                    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
                    
                    NSString *code = [attrStr.string substringFromIndex:[attrStr.string length] - 1];
                    
                    if ([code isEqualToString:@"\n"])
                    {
                        NSLog(@"%@",attrStr.string);
                        
                        self.lblAccomodation.attributedText = [attrStr.string substringToIndex:[attrStr.string length] - 1];
                        
                    }
                    else
                    {
                        self.lblAccomodation.attributedText = attrStr;
                        
                    }
                   // self.lblAccomodation.attributedText = htmlString;
                }*/
                self.lblAccomodation.font = [UIFont fontWithName:@"Titillium-regular" size:13];


            }
            else
            {
                _imageAccomodation.hidden = YES;
                self.lblAccomodation.text = @"";

            }
            
            if (packageDetail.sightSeeingFlag == YES)
            {
                
                if ([[packageDetail sightseenCollection] count] > 0) {
                    NSDictionary *sightDict = [[packageDetail sightseenCollection] firstObject];
                           //        if ([[[dict objectForKey:@"isSightseeingDefaultMsg"] lowercaseString] isEqualToString:@"y"])
                           //        {
                                   if (![[sightDict objectForKey:@"typeDefaultMsg"]  isEqualToString:@" "])
                                   {
                                       NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[[sightDict objectForKey:@"typeDefaultMsg"] dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
                                       
                                       NSString *code = [attrStr.string substringFromIndex:[attrStr.string length] - 1];
                                       
                                       if ([code isEqualToString:@"\n"])
                                       {
                                           NSLog(@"%@",attrStr.string);
                                           
                                           self.lblSightSeeing.text = [attrStr.string substringToIndex:[attrStr.string length] - 1];
                                           
                                       }
                                       else
                                       {

                                            self.lblSightSeeing.attributedText = attrStr;
                                       }
                                   }
                                   else
                                   {
                                       self.lblSightSeeing.text = @"included as per itinerary";//mealsDefaultMsg
                                   }
                                   
                }else{
                    self.lblSightSeeing.text = @"included as per itinerary";//mealsDefaultMsg
                }
                 
                self.lblSightSeeing.font = [UIFont fontWithName:@"Titillium-regular" size:13];

            }
            else
            {
                _imageSightSeeing.hidden = YES;
                self.lblSightSeeing.text = @"";

            }
        
    }else{
        if (packageDetail.mealsFlag == YES)
            {
               
                 NSMutableString * htmlString=[NSMutableString new] ;
               // if ([[[dict objectForKey:@"isMealsDefaultMsg"] lowercaseString] isEqualToString:@"y"]){
                        if (![[dict objectForKey:@"mealsDefaultMsg"]  isEqualToString:@""])
                        {
                    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[[dict objectForKey:@"mealsDefaultMsg"] dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
                            
                        NSString *code = [attrStr.string substringFromIndex:[attrStr.string length] - 1];
                            
                            if ([code isEqualToString:@"\n"])
                            {
                                NSLog(@"%@",attrStr.string);
                                
                                self.lblDinner.text = [attrStr.string substringToIndex:[attrStr.string length] - 1];

                            }
                            else
                            {
                                self.lblDinner.attributedText = attrStr;

                            }
                                
                   // self.lblDinner.font = [UIFont systemFontOfSize:13.0];

                }
                else
                {
                    self.lblDinner.text = @"included as per itinerary";//mealsDefaultMsg

                }
             
                self.lblDinner.font = [UIFont fontWithName:@"Titillium-regular" size:13];


            }
            else
            {
                _imageMeal.hidden = YES;
                self.lblDinner.text = @"";
                
            }
                
            
            if (packageDetail.airFlag == YES)
            {
                
        //        if ([[[dict objectForKey:@"isFlightDefaultMsg"] lowercaseString] isEqualToString:@"y"])
        //        {
                    if (![[dict objectForKey:@"flightDefaultMsg"]  isEqualToString:@""])
                    {
                    
                    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[[dict objectForKey:@"flightDefaultMsg"] dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
                        NSString *code = [attrStr.string substringFromIndex:[attrStr.string length] - 1];
                        
                        if ([code isEqualToString:@"\n"])
                        {
                            NSLog(@"%@",attrStr.string);
                            
                            self.lblFlights.text = [attrStr.string substringToIndex:[attrStr.string length] - 1];
                            
                        }
                        else
                        {

                         self.lblFlights.attributedText = attrStr;
                        }
                }
                else
                {
                    self.lblFlights.text = @"included as per itinerary";//mealsDefaultMsg
                   // self.lblFlights.font = [UIFont systemFontOfSize:13.0];

                }

                self.lblFlights.font = [UIFont fontWithName:@"Titillium-regular" size:13];

            }
            else
            {      _imageFlight.hidden = YES;
                  self.lblFlights.text = @"";

            }
            
            if (packageDetail.tourMngerFlag == YES)
            {
                
        //        if ([[[dict objectForKey:@"isVisaDefaultMsg"] lowercaseString] isEqualToString:@"y"])
        //        {
                    if (![[dict objectForKey:@"visaDefaultMsg"]  isEqualToString:@""])
                    {

                    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[[dict objectForKey:@"visaDefaultMsg"] dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
                        NSString *code = [attrStr.string substringFromIndex:[attrStr.string length] - 1];
                        
                        if ([code isEqualToString:@"\n"])
                        {
                            NSLog(@"%@",attrStr.string);
                            
                            self.lblVisa.text = [attrStr.string substringToIndex:[attrStr.string length] - 1];
                            
                        }
                        else
                        {

                       self.lblVisa.attributedText = attrStr;
                        }
                   // self.lblVisa.font = [UIFont systemFontOfSize:13.0];

                }
                else
                {
                    self.lblVisa.text = @"included as per itinerary";//mealsDefaultMsg
                    
                }
                
                self.lblVisa.font = [UIFont fontWithName:@"Titillium-regular" size:13];

            }
            else
            {
                    _imageVisa.hidden = YES;
                    self.lblVisa.text = @"";
            }
            
            if (packageDetail.accomFlag == YES)
            {
                NSMutableString * htmlString=[NSMutableString new] ;
               // if ([[[dict objectForKey:@"isHotelDefaultMsg"] lowercaseString] isEqualToString:@"y"]){
                    if (![[dict objectForKey:@"hotelDefaultMsg"]  isEqualToString:@""])
                    {

                    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[[dict objectForKey:@"hotelDefaultMsg"] dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
                        NSString *code = [attrStr.string substringFromIndex:[attrStr.string length] - 1];
                        
                        if ([code isEqualToString:@"\n"])
                        {
                            NSLog(@"%@",attrStr.string);
                            
                            self.lblAccomodation.text = [attrStr.string substringToIndex:[attrStr.string length] - 1];
                            
                        }
                        else
                        {
                            self.lblAccomodation.attributedText = attrStr;
                        }
                        
                    //self.lblAccomodation.font = [UIFont systemFontOfSize:13.0];
                }
                else
                {
                    self.lblAccomodation.text = @"included as per itinerary";//mealsDefaultMsg
                }
               /* }else{
                    NSArray * accomdationList = [dict objectForKey:@"tcilHolidayAccomodationCollection"];
                    
                    NSPredicate * predicate1 =  [NSPredicate predicateWithFormat:@"(packageClassId == %@) ", @"0" ];
                    
                    NSArray *filteredArray = [accomdationList filteredArrayUsingPredicate:predicate1];
                    
                    for (NSDictionary * accomDict  in filteredArray)
                    {
                        NSMutableString *stringOfImages = [NSMutableString new] ;
                        
                        NSDictionary *hotelIDDict =  [accomDict valueForKey:@"accomodationHotelId"];
                        
                        
                        //NSString *starRatingString = [[hotelIDDict valueForKey:@"starRating"] intValue];
                        
                        NSString *totalStar = @"5";
                        NSString *selectedStar =[NSString stringWithFormat:@"%d", [[hotelIDDict valueForKey:@"starRating"] intValue]];
                       
                        
                        for (int i = 0; i<[totalStar intValue]; i++)
                        {
                            NSString *imageNameURL;
                            if (i < [selectedStar intValue])
                            {
                                imageNameURL = [[NSBundle mainBundle] pathForResource:@"rating_selected" ofType:@"png"];
                            }else
                            {
                                imageNameURL = [[NSBundle mainBundle] pathForResource:@"rating_unselected" ofType:@"png"];
                            }
                            
                            NSURL *urlImage = [NSURL fileURLWithPath:imageNameURL];
                            [stringOfImages appendString:[NSString stringWithFormat:@"<img src=\"%@\" style=\"padding-left:2%%\" width=\"5%%\">",urlImage]];
                        }
                        [stringOfImages appendString:@"</br>"];
                        [htmlString appendString:[NSString stringWithFormat:@"<html><head></head><body><font face=\"Titillium-Regular\" color=\"gray\" size=\"4\">&nbsp&nbsp&nbsp%@</font><br><font face=\"Titillium-Regular\" color=\"gray\" size=\"2\">&nbsp&nbsp&nbsp&nbsp%@ Nights <br>&nbsp&nbsp&nbsp&nbsp%@</br>%@</body></html></br>" ,[hotelIDDict objectForKey:@"hotelName"] ,[NSString stringWithFormat:@"%d",[[accomDict objectForKey:@"noOfNights"] intValue]],[hotelIDDict objectForKey:@"city"],stringOfImages]];
                    }
                    
                    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
                    
                    NSString *code = [attrStr.string substringFromIndex:[attrStr.string length] - 1];
                    
                    if ([code isEqualToString:@"\n"])
                    {
                        NSLog(@"%@",attrStr.string);
                        
                        self.lblAccomodation.attributedText = [attrStr.string substringToIndex:[attrStr.string length] - 1];
                        
                    }
                    else
                    {
                        self.lblAccomodation.attributedText = attrStr;
                        
                    }
                   // self.lblAccomodation.attributedText = htmlString;
                }*/
                self.lblAccomodation.font = [UIFont fontWithName:@"Titillium-regular" size:13];


            }
            else
            {
                _imageAccomodation.hidden = YES;
                self.lblAccomodation.text = @"";

            }
            
            if (packageDetail.sightSeeingFlag == YES)
            {
                
        //        if ([[[dict objectForKey:@"isSightseeingDefaultMsg"] lowercaseString] isEqualToString:@"y"])
        //        {
                if (![[dict objectForKey:@"sightseeingDefaultMsg"]  isEqualToString:@""])
                {
                    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[[dict objectForKey:@"sightseeingDefaultMsg"] dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
                    
                    NSString *code = [attrStr.string substringFromIndex:[attrStr.string length] - 1];
                    
                    if ([code isEqualToString:@"\n"])
                    {
                        NSLog(@"%@",attrStr.string);
                        
                        self.lblSightSeeing.text = [attrStr.string substringToIndex:[attrStr.string length] - 1];
                        
                    }
                    else
                    {

                         self.lblSightSeeing.attributedText = attrStr;
                    }
                }
                else
                {
                    self.lblSightSeeing.text = @"included as per itinerary";//mealsDefaultMsg
                }
                
                self.lblSightSeeing.font = [UIFont fontWithName:@"Titillium-regular" size:13];

            }
            else
            {
                _imageSightSeeing.hidden = YES;
                self.lblSightSeeing.text = @"";

            }
    }
    
    
    [self.viewOverView layoutIfNeeded];
    
}

-(void)setOverViewImage
{
    NSArray *arrayImages = packageDetail.arrayPackageImagePathList;
    
    if (arrayImages.count != 0)
    {
        
        NSDictionary *imageDict = arrayImages[0];
        
        
        NSString* imageName = [[imageDict valueForKey:@"path"] stringByReplacingOccurrencesOfString:@" " withString:@"_"];
        
        NSString *imageUrlString = [NSString stringWithFormat:@"%@/images/holidays/%@/photos/%@",kUrlForImage,packageDetail.strPackageId,imageName];
        
        
        NSURL* urlImage=[NSURL URLWithString:imageUrlString];
        
       // UIImage *overviewImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:urlImage]];
        //dispatch_async(dispatch_get_main_queue(), ^{
            
            if(urlImage)
            {
                //self.bannerImageView.image = overviewImage;
                UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];

                indicator.center = self.bannerImageView.center;// it will display in center of image view
                [self.bannerImageView addSubview:indicator];
                [indicator startAnimating];
                
                [self.bannerImageView sd_setImageWithURL:urlImage placeholderImage:[UIImage imageNamed:@"defaultBanner.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
                 {
                     
                     [indicator stopAnimating];
                     
                 }];
            }
       // });
    }
}
#pragma mark -Table view Delegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [packageDetail.arrayPackageImagePathList count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 149;
    
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
    NSLog(@"indea path cell for row  %ld",(long)indexPath.row);
    
    
    
    /*   activityLoadingView = [LoadingView loadingViewInView:self.view.superview.superview
     withString:@""
     andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];*/
    ImageViewCell *cell = (ImageViewCell *) [tableView dequeueReusableCellWithIdentifier:@"imageTableViewCellIdentifier"];
    if (cell == nil)
    {
        NSArray *inb= [[NSBundle mainBundle] loadNibNamed:@"ImageViewCell" owner:self options:nil];
        
        cell=[inb objectAtIndex:0];
    }
    
    
    
    //  [cell.imageView sd_setImageWithURL:[NSURL URLWithString:@"http://www.domain.com/path/to/image.jpg"]placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    
    cell.imageView.frame=CGRectMake(cell.imageView.frame.origin.x, cell.imageView.frame.origin.y, [[UIScreen mainScreen]bounds].size.width, cell.imageView.frame.size.height);
    cell.selectorView.frame=CGRectMake(cell.selectorView.frame.origin.x, cell.selectorView.frame.origin.y, [[UIScreen mainScreen]bounds].size.width, cell.selectorView.frame.size.height);
    
    
    NSLog(@"ht %f",[[UIScreen mainScreen]bounds].size.height);
    //cell.imageView.frame=CGRectMake(0, 0, 520, 44);
    NSArray *imageArray = packageDetail.arrayPackageImagePathList;
    
    if (imageArray.count != 0)
    {
        
        
        
        NSDictionary *imageDict = packageDetail.arrayPackageImagePathList[indexPath.row];
        
        
        //NSString* imageName = [[imageDict valueForKey:@"path"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        NSString* imageName = [[imageDict valueForKey:@"path"] stringByReplacingOccurrencesOfString:@" " withString:@"_"];

        NSString *imageUrlString = [NSString stringWithFormat:@"%@/images/holidays/%@/photos/%@",kUrlForImage,packageDetail.strPackageId,imageName];
        
        
        NSURL *urlImage = [NSURL URLWithString:imageUrlString];
        
        if(urlImage)
        {
            
            UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            indicator.center = cell.imageView.center;// it will display in center of image view
            [cell.imageView addSubview:indicator];
            [indicator startAnimating];
            
            
            
            [cell.imageView sd_setImageWithURL:urlImage placeholderImage:[UIImage imageNamed:@"defaultBanner.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
             {
                 
                 [indicator stopAnimating];
                 
             }];

//            [cell.imageView sd_setImageWithURL:urlImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//                
//                [indicator stopAnimating];
//                
//            }];
            
            
            
            //[cell.imageView sd_setImageWithURL:urlImage];
            // [cell.imageView setImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:urlImage]]];
            
        }
        
    }
    
    //
    //    if ([activityLoadingView isDescendantOfView:self.view.superview.superview])
    //    {
    //        [activityLoadingView removeView];
    //        activityLoadingView = nil;
    //    }
    return  cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    NSLog(@"indea path did select row  %ld",(long)indexPath.row);
    //  UIView *popUpView = [UIView new];
    UIImageView *popUpImageView=[[UIImageView alloc]init];
    
    //UIView *popUpView = [UIView new];
    popUpImageView.frame = CGRectMake(0, 0, 300 , 300);
    popUpImageView.backgroundColor = [UIColor whiteColor];
    popUpImageView.layer.cornerRadius = 1.0;
    popUpImageView.layer.borderColor=[[UIColor whiteColor]CGColor];
    popUpImageView.layer.borderWidth= 1.0f;
    
    UIButton *closeBtn=[[UIButton alloc]init];
    closeBtn.frame=CGRectMake(popUpImageView.frame.size.width-30, 0, 30, 30);
    [closeBtn addTarget:self action:@selector(closeBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    popUpImageView.userInteractionEnabled = YES;
    
    [closeBtn setBackgroundImage:[UIImage imageNamed:@"plus_light_gray@2x.png"] forState:UIControlStateNormal];
    
    [popUpImageView addSubview: closeBtn];
    
   // UITableViewCell *cell = [self.imageTableView cellForRowAtIndexPath:indexPath];
    
    //UIImage *cellImage = cell.imageView.image;
    
//    NSArray *imageArray = packageDetail.arrayImages;
//    
//    if (imageArray.count != 0)
//    {
//        NSString *imageUrlString = [imageArray objectAtIndex:indexPath.row];
//        NSURL *urlImage = [NSURL URLWithString:imageUrlString];
//        
//        if(urlImage)
//        {
//            [popUpImageView setImage:cellImage];
//        }
//        
//    }
    
    NSDictionary *imageDict = packageDetail.arrayPackageImagePathList[indexPath.row];
    
    
    //NSString* imageName = [[imageDict valueForKey:@"path"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSString* imageName = [[imageDict valueForKey:@"path"] stringByReplacingOccurrencesOfString:@" " withString:@"_"];
    
    NSString *imageUrlString = [NSString stringWithFormat:@"%@/images/holidays/%@/photos/%@",kUrlForImage,packageDetail.strPackageId,imageName];
    
    
    NSURL *urlImage = [NSURL URLWithString:imageUrlString];
    UIImage *cellImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:urlImage]];
    [popUpImageView setImage:cellImage];

    customePopUp = [KLCPopup popupWithContentView:popUpImageView
                                         showType:KLCPopupShowTypeGrowIn
                                      dismissType:KLCPopupDismissTypeFadeOut
                                         maskType:KLCPopupMaskTypeDimmed
                         dismissOnBackgroundTouch:YES
                            dismissOnContentTouch:NO];
    
    [customePopUp show];
    
    
}

-(void)closeBtnAction:(id)sender{
    
    [customePopUp removeFromSuperview];
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath

{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        
        [cell setSeparatorInset:UIEdgeInsetsZero];
        
    }
    
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        
        [cell setPreservesSuperviewLayoutMargins:NO];
        
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        
        [cell setLayoutMargins:UIEdgeInsetsZero];
        
    }
    
}




#pragma mark - Text Field Delegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return NO;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField == self.txtFldQueryFor || textField == self.txtFldQueryRltdTo) {
        
        return NO;
    }
    return YES;
}

#pragma mark - Keyboard Notifiction methods

-(void)keyboardShown
{
    popUpEmailContainer.scrollEnabled = YES;
}

-(void)keyboardHidden
{
    popUpEmailContainer.scrollEnabled = NO;
}
#pragma mark- set site

-(void)setSite
{
    
    NSLog(@"start time");
    
    CGFloat height=_scrollSite.frame.size.height;
    CGFloat width=60;
    //CGFloat yPos=(height/2);
     siteArray = packageDetail.timeLineList;
    NSSortDescriptor *brandDescriptor = [[NSSortDescriptor alloc] initWithKey:@"position" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:brandDescriptor];
    NSArray *sortedArray = [siteArray sortedArrayUsingDescriptors:sortDescriptors];

   
    for (int i=0; i<[siteArray count]; i++) {
        
        UIView *siteView=[[UIView alloc]initWithFrame:CGRectMake(i*width, 0, width, height)];
        UILabel *lblSiteName=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, width, 20)];
        UIImageView *imgSiteDot=[[UIImageView alloc]initWithFrame:CGRectMake(28, 25, 10,10)];
        
        // imgSiteDot.center = siteView.center;
        
        UILabel *lblTime=[[UILabel alloc]initWithFrame:CGRectMake(0, 40, width, 20)];
        
        lblSiteName.font=[UIFont fontWithName:TITILLUM_REGULAR size:11.0];
        lblTime.font=[UIFont fontWithName:TITILLUM_THIN size:10.0];
        
        [lblSiteName setTextAlignment:NSTextAlignmentCenter];
        [lblTime setTextAlignment:NSTextAlignmentCenter];
        
        NSDictionary *siteNameDict = sortedArray[i];
        
        NSDictionary *cityCodeDict = [siteNameDict valueForKey:@"cityCode"];
        
        lblSiteName.text=[cityCodeDict valueForKey:@"cityName"];
        
        lblTime.text = [NSString stringWithFormat:@"%@ Nights",[siteNameDict valueForKey:@"noOfNights"]];
        
        NSDictionary *iconIdDict = [siteNameDict valueForKey:@"iconId"];
        
        NSString *iconName = [iconIdDict valueForKey:@"iconName"];
        
        
        if ([iconName isEqualToString:@"Flight"])
        {
            imgSiteDot=[[UIImageView alloc]initWithFrame:CGRectMake(28, 25, 20 ,20)];
            imgSiteDot.image=[UIImage imageNamed:@"flight_package"];
            lblTime.text = @"";
        }
        else
        {
            imgSiteDot.image=[UIImage imageNamed:@"siteDot"];
        }
        
        
        [siteView addSubview:lblSiteName];
        [siteView addSubview:imgSiteDot];
        [siteView addSubview:lblTime];
        
        siteView.alpha=1.0;
        
        [_scrollSite addSubview:siteView];
        
        //        [UIView beginAnimations:nil context:NULL];
        //        [UIView setAnimationDuration:0.5];
        //        [siteView setAlpha:1.0];
        //        [UIView commitAnimations];
        
    }
    
    [_scrollSite setContentSize:CGSizeMake([sortedArray count]*width, 40)];
    
    
}

#pragma mark - UIButton action methods

- (IBAction)onBookNowButtonClicked:(id)sender
{
    if([packageDetail.strPackageType caseInsensitiveCompare:@"International"]==NSOrderedSame)
    {
        [FIRAnalytics logEventWithName:@"International_Holiday_Book_Now_PDP" parameters:@{kFIRParameterItemName:[NSString stringWithFormat:@"PackageName :%@, PackageType :%@",packageDetail.strPackageName,packageDetail.strPackageType]}];
    }
    else
    {
        [FIRAnalytics logEventWithName:@"Indian_Holiday_Book_Now_PDP" parameters:@{kFIRParameterItemName:[NSString stringWithFormat:@"PackageName :%@, PackageType :%@",packageDetail.strPackageName,packageDetail.strPackageType]}];
    }
    
    BookNowViewController *bookVC = [[BookNowViewController alloc]initWithNibName:@"BaseViewController" bundle:nil];
    bookVC.packageDetail = self.packageDetail;
    
    [[SlideNavigationController sharedInstance] pushViewController:bookVC animated:NO];
}

- (IBAction)btnSendEmailClicked:(id)sender {
    
    NSLog(@"Button clicked");
    
    popUpEmailContainer = [self getViewForEmailPopUp];
    popUpEmailContainer.scrollEnabled = NO;
    
    customePopUp = [KLCPopup popupWithContentView:popUpEmailContainer
                                         showType:KLCPopupShowTypeGrowIn
                                      dismissType:KLCPopupDismissTypeFadeOut
                                         maskType:KLCPopupMaskTypeDimmed
                         dismissOnBackgroundTouch:YES
                            dismissOnContentTouch:NO];
    
    [customePopUp show];
}




#pragma mark - PopUp views
#pragma mark Email PopUp
-(UIScrollView *)getViewForEmailPopUp
{
    UIScrollView *scrollContainerView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, 250, 465)];
    [scrollContainerView setContentSize:CGSizeMake(250, 580)];
    scrollContainerView.layer.cornerRadius = 3.0;
    
    UIView *popUpView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 250, 465)];
    [popUpView setBackgroundColor:[UIColor whiteColor]];
    popUpView.layer.cornerRadius = 3.0;
    
    UILabel *lblHeader = [[UILabel alloc]initWithFrame:CGRectMake(99, 10, 42, 21)];
    lblHeader.text = @"Email";
    lblHeader.textAlignment = NSTextAlignmentCenter;
    lblHeader.textColor = [UIColor colorFromHexString:@"#0194DA"];
    [lblHeader setFont:[UIFont fontWithName:TITILLUM_SEMIBOLD size:16]];
    
    UILabel *lblStarName = [[UILabel alloc]initWithFrame:CGRectMake(8,37,10,10)];
    lblStarName.text = @"★";
    lblStarName.textAlignment = NSTextAlignmentLeft;
    lblStarName.textColor = [UIColor redColor];
    [lblStarName setFont:[UIFont systemFontOfSize:10]];
    
    UILabel *lblName = [[UILabel alloc]initWithFrame:CGRectMake(19,37,50,15)];
    lblName.text = @"Name :";
    lblName.textAlignment = NSTextAlignmentLeft;
    lblName.textColor = [UIColor blackColor];
    [lblName setFont:[UIFont fontWithName:TITILLUM_REGULAR size:14]];
    
    self.txtFldName = [[UITextField alloc]initWithFrame:CGRectMake(8, 58, 234, 30)];
    self.txtFldName.borderStyle = UITextBorderStyleBezel;
    
    
    UILabel *lblStarEmail = [[UILabel alloc]initWithFrame:CGRectMake(8,98,10,10)];
    lblStarEmail.text = @"★";
    lblStarEmail.textAlignment = NSTextAlignmentLeft;
    lblStarEmail.textColor = [UIColor redColor];
    [lblStarEmail setFont:[UIFont systemFontOfSize:10]];
    
    UILabel *lblEmail = [[UILabel alloc]initWithFrame:CGRectMake(19,98,50,15)];
    lblEmail.text = @"Email :";
    lblEmail.textAlignment = NSTextAlignmentLeft;
    lblEmail.textColor = [UIColor blackColor];
    [lblEmail setFont:[UIFont fontWithName:TITILLUM_REGULAR size:14]];
    
    self.txtFldEmail = [[UITextField alloc]initWithFrame:CGRectMake(8, 119, 234, 30)];
    self.txtFldEmail.borderStyle = UITextBorderStyleBezel;
    
    UILabel *lblStarPhoneNo = [[UILabel alloc]initWithFrame:CGRectMake(8,159,10,10)];
    lblStarPhoneNo.text = @"★";
    lblStarPhoneNo.textAlignment = NSTextAlignmentLeft;
    lblStarPhoneNo.textColor = [UIColor redColor];
    [lblStarPhoneNo setFont:[UIFont systemFontOfSize:10]];
    
    UILabel *lblPhoneNo = [[UILabel alloc]initWithFrame:CGRectMake(19,159,79,15)];
    lblPhoneNo.text = @"Phone No :";
    lblPhoneNo.textAlignment = NSTextAlignmentCenter;
    lblPhoneNo.textColor = [UIColor blackColor];
    [lblPhoneNo setFont:[UIFont fontWithName:TITILLUM_REGULAR size:14]];
    
    self.txtFldPhoneNo = [[UITextField alloc]initWithFrame:CGRectMake(8, 180, 234, 30)];
    self.txtFldPhoneNo.borderStyle = UITextBorderStyleBezel;
    self.txtFldPhoneNo.keyboardType = UIKeyboardTypeNumberPad;
    
    UILabel *lblQueryFor = [[UILabel alloc]initWithFrame:CGRectMake(8,220,79,15)];
    lblQueryFor.text = @"Query For :";
    lblQueryFor.textAlignment = NSTextAlignmentLeft;
    lblQueryFor.textColor = [UIColor blackColor];
    [lblQueryFor setFont:[UIFont fontWithName:TITILLUM_REGULAR size:14]];
    
    UIImageView *imgViewQryFor = [[UIImageView alloc]initWithFrame:CGRectMake(220, 251, 15, 10)];
    
    [imgViewQryFor setImage:[UIImage imageNamed:@"dropdown_arrow"]];
    
    self.txtFldQueryFor = [[UITextField alloc]initWithFrame:CGRectMake(8, 241, 234, 30)];
    self.txtFldQueryFor.borderStyle = UITextBorderStyleBezel;
    
    
    UILabel *lblQueryRltdTo = [[UILabel alloc]initWithFrame:CGRectMake(8,281,121,15)];
    lblQueryRltdTo.text = @"Query Related To :";
    lblQueryRltdTo.textAlignment = NSTextAlignmentLeft;
    lblQueryRltdTo.textColor = [UIColor blackColor];
    [lblQueryRltdTo setFont:[UIFont fontWithName:TITILLUM_REGULAR size:14]];
    
    UIImageView *imgViewQryRltdTo = [[UIImageView alloc]initWithFrame:CGRectMake(220, 312, 15, 10)];
    
    [imgViewQryRltdTo setImage:[UIImage imageNamed:@"dropdown_arrow"]];
    
    self.txtFldQueryRltdTo = [[UITextField alloc]initWithFrame:CGRectMake(8, 302, 234, 30)];
    self.txtFldQueryRltdTo.borderStyle = UITextBorderStyleBezel;
    
    
    UILabel *lblStarBFN = [[UILabel alloc]initWithFrame:CGRectMake(8,342,10,10)];
    lblStarBFN.text = @"★";
    lblStarBFN.textAlignment = NSTextAlignmentLeft;
    lblStarBFN.textColor = [UIColor redColor];
    [lblStarBFN setFont:[UIFont systemFontOfSize:10]];
    
    UILabel *lblBFNNo = [[UILabel alloc]initWithFrame:CGRectMake(19,342,220,15)];
    lblBFNNo.text = @"Your BFN (Booking File No.) :";
    lblBFNNo.textAlignment = NSTextAlignmentLeft;
    lblBFNNo.textColor = [UIColor blackColor];
    [lblBFNNo setFont:[UIFont fontWithName:TITILLUM_REGULAR size:14]];
    
    self.txtFldBFNNo = [[UITextField alloc]initWithFrame:CGRectMake(8, 363, 234, 30)];
    self.txtFldBFNNo.borderStyle = UITextBorderStyleBezel;
    self.txtFldBFNNo.keyboardType = UIKeyboardTypeNumberPad;
    
    UIButton *btnSubmit = [[UIButton alloc]initWithFrame:CGRectMake(84, 420, 91, 30)];
    
    [btnSubmit setTitle:@"Submit" forState:UIControlStateNormal];
    [btnSubmit setBackgroundColor:[UIColor colorFromHexString:@"#F37022"]];
    [btnSubmit setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnSubmit.titleLabel setFont:[UIFont fontWithName:TITILLUM_SEMIBOLD size:16]];
    [btnSubmit addTarget:self action:@selector(btnSubmitOnEmailPopUpClicked) forControlEvents:UIControlEventTouchUpInside];
    
    self.txtFldQueryFor.delegate     = self;
    self.txtFldQueryRltdTo.delegate  = self;
    self.txtFldPhoneNo.delegate      = self;
    self.txtFldName.delegate         = self;
    self.txtFldEmail.delegate        = self;
    self.txtFldBFNNo.delegate        = self;
    
    
    [popUpView addSubview:lblHeader];
    [popUpView addSubview:lblStarName];
    [popUpView addSubview:lblName];
    [popUpView addSubview:self.txtFldName];
    [popUpView addSubview:lblStarEmail];
    [popUpView addSubview:lblEmail];
    [popUpView addSubview:self.txtFldEmail];
    [popUpView addSubview:lblStarPhoneNo];
    [popUpView addSubview:lblPhoneNo];
    [popUpView addSubview:self.txtFldPhoneNo];
    [popUpView addSubview:lblQueryFor];
    [popUpView addSubview:imgViewQryFor];
    [popUpView addSubview:self.txtFldQueryFor];
    [popUpView addSubview:lblQueryRltdTo];
    [popUpView addSubview:imgViewQryRltdTo];
    [popUpView addSubview:self.txtFldQueryRltdTo];
    [popUpView addSubview:lblStarBFN];
    [popUpView addSubview:lblBFNNo];
    [popUpView addSubview:self.txtFldBFNNo];
    [popUpView addSubview:btnSubmit];
    
    [scrollContainerView addSubview:popUpView];
    return scrollContainerView;
}


- (IBAction)actionOnViewGalleryBtn:(id)sender {
    //
    //   _baseViewObj=[[BaseViewController alloc]initWithNibName:@"BaseViewController" bundle:nil];
    //     _baseViewObj.delegate=self;
    //  baseViewObj.tabMenuVCView=self;
    
    if([packageDetail.strPackageType caseInsensitiveCompare:@"International"]==NSOrderedSame)
    {
        [FIRAnalytics logEventWithName:@"International_Holiday_View_Gallery_PDP" parameters:@{kFIRParameterItemName:[NSString stringWithFormat:@"PackageName :%@, PackageType :%@, PackageID :%@",packageDetail.strPackageName,packageDetail.strPackageType,packageDetail.strPackageId]}];
    }
    else
    {
        [FIRAnalytics logEventWithName:@"Indian_Holiday_View_Gallery_PDP" parameters:@{kFIRParameterItemName:[NSString stringWithFormat:@"PackageName :%@, PackageType :%@, PackageID :%@",packageDetail.strPackageName,packageDetail.strPackageType,packageDetail.strPackageId]}];
        
    }
    TabMenuVC *tabMenuViewCont=(TabMenuVC *)_tabMenuVC;
    [tabMenuViewCont.TabMenuView addSubview:_imageViewInTable];
    _imageViewInTable.frame=CGRectMake(0, 0, self.imageViewInTable.frame.size.width , [[UIScreen mainScreen]bounds].size.height);
    _backBtnFromOverview=YES;
    [_imageTableView reloadData];
}
- (IBAction)actionForSideArrowleft:(id)sender {
    NSLog(@"left");
    if (siteArray.count>=5) {
        
        
        if (_scrollSite.contentOffset.x >(27 *[siteArray count]-1) -_scrollSite.frame.size.width ) {
            
            [UIView animateWithDuration:0.8 animations:^{
                _scrollSite.contentOffset = CGPointMake(_scrollSite.contentOffset.x-50, 0);
            }];
            
        }
    }
}
- (IBAction)actionForSideArrowRight:(id)sender {
    NSLog(@"Right");
    if (_scrollSite.contentOffset.x < (70 *[siteArray count]-1) -_scrollSite.frame.size.width ) {
        
        [UIView animateWithDuration:0.8 animations:^{
            _scrollSite.contentOffset = CGPointMake(_scrollSite.contentOffset.x+50, 0);
        }];
        
    }
    
}


@end
