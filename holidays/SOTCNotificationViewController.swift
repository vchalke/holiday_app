//
//  NotificationViewController.swift
//  sotc-consumer-application
//
//  Created by Saurav Kumar on 25/07/17.
//  Copyright © 2017 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit

/*struct NotificationImages {
    
    static let Tickets:UIImage = #imageLiteral(resourceName: "ticketsTourDetail")
    static let Profile = ("CAD", "fdf8c1")
    static let Deviation = ("CHF", "f26c60")
    static let FinalTourDetail = (Constant.EUR_CONTRY_CODE, "b8b46a")
    static let TourConfirmationVoucher = ("GBP", "7c8781")
    static let Feedback = ("INR", "d8c6e0")
    static let VisaDocuments = ( "JPY", "b5e3f0")
    static let AppointmentLetter = ("NZD", "f4b389")
    static let Visas = ("SGD", "ccbebe")
    static let Payments = ("THB", "db749f")
    
}*/


class SOTCNotificationViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UITabBarControllerDelegate
{
    
    
    
    
    @IBOutlet var uiNotificationTableView: UITableView!
    
    var notificationDetails:Array<Notifications> = []
    
    
    //MARK : - Controller Methods
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
    
        uiNotificationTableView.register(UINib(nibName:"SOTCNotificationTableViewCell", bundle : nil), forCellReuseIdentifier: "notificationCustomCell")
        
          self.tabBarController?.delegate = self
    
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
 
        self.navigationController?.setRedNavigationBar()
        self.navigationController?.setnavigatiobBarTitle(title: "Notification", viewController: self)
        
        
        NotificationController.updateNotificationBadgeCount()
        
        
        uiNotificationTableView.register(UINib(nibName:"NotificationTableViewCell", bundle : nil), forCellReuseIdentifier: "notificationCustomCell")
        
        self.notificationDetails = self.getNotificationdetails()
        
        
        if self.notificationDetails.count > 0  {
       
             uiNotificationTableView.reloadData()
        }
        else
        {
            AppUtility.displayAlertController(title: "Notification", message: "There are no Notifications to display")
        }
       
        
    }
    
    
    //Mark -- UITableViewDataSource
    
    public func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.notificationDetails.count
    }
    
   
    
   public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
   {
    
        let cell = tableView.dequeueReusableCell(withIdentifier: "notificationCustomCell", for: indexPath as IndexPath) as! SOTCNotificationTableViewCell
    
    
    if let notification : Notifications = notificationDetails[indexPath.row] as? Notifications
    {
        cell.uiNotificationTitleLabel.text = notification.title ?? ""
        
        
        
        let notificationDate:Date? = (notification.createdOn! as Date)
        
       if notificationDate != nil
       {
        
        let dateString = AppUtility.convertDateToString(date: notificationDate!, dateFormat: "dd-MMM-yyyy")
         cell.uiNotificationDateLabel.text = dateString
        
        }
        
        
        
        
        //"25 May 2017"
        cell.uiNotificationDescLabel.text = notification.message ?? ""
        
        let cellImageAndColor = self.getImage(by: notification.type ?? "")
        
        cell.uiImageView.image = cellImageAndColor.image
        
        cell.bfNumberLabel.text = "Booking ID : \(notification.bfNumber ?? "")"
        
        
        cell.uiNotificationTitleLabel.textColor = cellImageAndColor.color
        
        if notification.notificationStatus?.lowercased() == "read" {
            
            
                cell.uibaseView.backgroundColor = UIColor.groupTableViewBackground
        }
        else
        {
            cell.uibaseView.backgroundColor = UIColor.white
        }
   
    }
    
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
         let notification = self.notificationDetails[indexPath.row]
        
        NotificationDBManager.markNotificationsAsReadForNotification(by: notification)
        
        NotificationController.moveToRespectiveViewController(notificationType: notification.type ?? "", bfNumber: notification.bfNumber ?? "", webViewRedirectionURL: notification.redirect ?? "" ,message: notification.message ?? "New notification.Go to notification screen to see.")
        
        tableView.reloadRows(at: [indexPath], with: UITableView.RowAnimation.fade)
        
        
    }
    
    func getNotificationdetails() -> Array<Notifications>{
        
         if let mobileNumber:String = UserDefaults.standard.object(forKey: UserDefauldConstant.userMobileNumber) as? String
         {
            if let notificationDetails:Array<Notifications> = NotificationController.getNotificationsDetailsFromDB(by: mobileNumber)
            {
                return notificationDetails
            }
        }
        
        return []
    }
    
    func getImage(by notificationType:String) -> (image:UIImage,color:UIColor) {
        
    
    switch notificationType.lowercased() {
        
    case NotificationType.Visas.rawValue.lowercased(),NotificationType.VisaDocuments.rawValue.lowercased() :
        
        return (#imageLiteral(resourceName: "appointmentLetterNotification"),Constant.AppThemeColor)
        
    case NotificationType.AppointmentLetter.rawValue.lowercased():
        
        return (#imageLiteral(resourceName: "visaDetailNotification"),AppUtility.hexStringToUIColor(hex: "37a493"))
        
        
    case NotificationType.Tickets.rawValue.lowercased() :
        
        return (#imageLiteral(resourceName: "ticketNotification"),AppUtility.hexStringToUIColor(hex: "6b5ca0"))
        
    case NotificationType.FinalTourDetail.rawValue.lowercased(),NotificationType.TourConfirmationVoucher.rawValue.lowercased() :
        return (#imageLiteral(resourceName: "finalTourDetailNotification"),Constant.AppThemeColor)
        
       case NotificationType.Profile.rawValue.lowercased(),NotificationType.ProfileLocale.rawValue.lowercased():
        
        return (#imageLiteral(resourceName: "profileNotification"),Constant.AppThemeColor)
    case NotificationType.Deviation.rawValue.lowercased(),NotificationType.DeviationLocale.rawValue.lowercased():
        
        return (#imageLiteral(resourceName: "deviationAppointmentNotification"),Constant.AppThemeColor)
        
    case NotificationType.Feedback.rawValue.lowercased():
        
       return (#imageLiteral(resourceName: "feedbackTourDetail"),Constant.AppThemeColor)
        
        
    case NotificationType.Payments.rawValue.lowercased():
        
       return (#imageLiteral(resourceName: "paymentNotification"),Constant.AppThemeColor)
    default:
        
        printLog("Invalid NotificationType")
        return (UIImage(),Constant.AppThemeColor)
    }
    
    }
    
    func getDateform(timeStamp:String?)-> Date?
    {
        
      //  let timeStampInt:Int = Int(timeStamp) ?? 0
        if let timeStamp = timeStamp {
        
        let date = Date(timeIntervalSince1970: (TimeInterval(timeStamp)! / 1000.0))
        
        //var date = NSDate(timeIntervalSince1970: TimeInterval(timeStampInt))
        
        return date
            
        }
        
        return nil
    }
    
        func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController)
     {
        self.uiNotificationTableView.reloadData()
        
        
        if   notificationDetails.count > 0  {
            
            
              self.uiNotificationTableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: UITableView.ScrollPosition.top, animated: true)
        }
      
  
        
    }
 
    
}
