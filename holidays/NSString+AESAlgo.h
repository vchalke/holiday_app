//
//  NSString+AES.h
//  AESEncryptionDemo

//

#import <Foundation/Foundation.h>
#import "NSData+AESAlgo.h"


@interface NSString (AESAlgo)
- (NSString *)AES128EncryptWithKey:(NSString *)key;
- (NSString *)AES128DecryptWithKey:(NSString *)key;
@end
