//
//  PassengerListViewController.swift
//  sotc-consumer-application
//
//  Created by Parshwanath on 04/10/17.
//  Copyright © 2017 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit


struct PassengerDetails {
    
    var displayTitle:String?
    var subtitle:String?
    var filePathUrl:String?
    var downloadUrl:String?
    
    init() {
        
    }
    
    
    
    
}



class PassengerListViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UIDocumentInteractionControllerDelegate {

    var passengerDetailsListArray:Array<PassengerDetails>?
    
    @IBOutlet weak var passengerTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
               self.passengerTableView.register(UITableViewCell.self, forCellReuseIdentifier: "ticketTableView")
        
        self.passengerTableView.tableFooterView = UIView(frame: .zero)


        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.setRedNavigationBar()
        self.navigationController?.setnavigatiobBarTitle(title: Title.FINAL_TOUR_DETAILS, viewController: self)
        self.navigationController?.setNagationBackButton(showRightButton: true, viewController: self)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return passengerDetailsListArray!.count
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 80
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ticketTableView", for: indexPath) as? UITableViewCell
            else {
                
                fatalError("The dequeued cell is not an instance of MealTableViewCell.")
        }
        
        cell.textLabel? .font = UIFont(name:"Roboto-Light", size:15)
        
        if let passengerDetails:PassengerDetails = passengerDetailsListArray?[indexPath.row] {
            
            cell.textLabel?.text = passengerDetails.displayTitle
            
        }
        
        // cell.textLabel?.text = "Vikas Patil"
        
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
          tableView.deselectRow(at: indexPath, animated: true)
        
        let passengerDetails:PassengerDetails = passengerDetailsListArray![indexPath.row]
       
        
        self.downloadDocument(filePath: passengerDetails.filePathUrl!,url: passengerDetails.downloadUrl!)
        
    }
    
    func downloadDocument(filePath:String ,url:String) {
        
        LoadingIndicatorView.show("Loading")
        
        NetworkCommunication.downloadDocuments(urlString: url, destinationFilePath: filePath, completion: { (downlodedfilePath, status) in
            
            printLog(status,filePath);
            
            if(status)
            {
                self.viewDownloadedFile(fileurl: URL(fileURLWithPath:filePath))
                
            }
            else{
                
                let isFilePresent = TourDocumentController.isFileExistInDocumentDirectory(fileNamePath: filePath)
                
                if(isFilePresent.chkFile)
                {
                    self.viewDownloadedFile(fileurl: URL(fileURLWithPath:filePath))
                    
                }else{
                    
                   // DispatchQueue.main.sync {
                        
                        LoadingIndicatorView.hide()
                        
                        //self.displayAlert()
                  //  }
                    
                }
                
            }
            
            
            
            
        })
    }
   

    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController
    {
        return self
    }
    
    
    func viewDownloadedFile(fileurl : URL) -> Void
    {
        
        let concurrentQueue = DispatchQueue(label: "queuename", attributes: .concurrent)
        concurrentQueue.sync {
            
            //DispatchQueue.main.sync {
            
            let documentController : UIDocumentInteractionController = UIDocumentInteractionController.init(url: fileurl)
            
            //self.documentController.uti =
            documentController.name  = Title.VIEW_PDF
            
            documentController.delegate = self
            
            documentController.presentPreview(animated: true)
            
            LoadingIndicatorView.hide()
            
            // }
            
        }
        
    }
    
    
}
