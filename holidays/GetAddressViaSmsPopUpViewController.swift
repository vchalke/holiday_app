//
//  GetAddressViaSmsPopUpViewController.swift
//  holidays
//
//  Created by Swapnil on 07/02/18.
//  Copyright © 2018 Mobicule. All rights reserved.
//

import UIKit

protocol GetAddressViaSmsPopUpViewControllerDelegate: class
{
    func unCheckGetAddressViaSmsButton()
}

class GetAddressViaSmsPopUpViewController: UIViewController,UITextFieldDelegate
{
    
    @IBOutlet weak var textFieldMobileNumber: UITextField!
    var branchAddress : String = ""
    weak var delegate: GetAddressViaSmsPopUpViewControllerDelegate?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.addDoneButtonOnKeyboard()
    }
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle       = UIBarStyle.default
        let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(GetAddressViaSmsPopUpViewController.doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.textFieldMobileNumber.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction()
    {
        self.textFieldMobileNumber.resignFirstResponder()
    }
    
    func isValidMobileNo(testStr:String) -> Bool
    {
        printLog("validate emilId: \(testStr)")
        let mobRegEx = "^[789]\\d{9}$"
        let mobTest = NSPredicate(format:"SELF MATCHES %@", mobRegEx)
        let result = mobTest.evaluate(with: testStr)
        
        return result
    }
    
    func showAlert(message:NSString) -> Void
    {
        let alert = UIAlertController(title: "Alert", message: message as String, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { UIAlertAction in
            alert.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func validateFields() -> Bool
    {
        if !isValidMobileNo(testStr: textFieldMobileNumber.text!)
        {
            showAlert(message: "Please enter valid Mobile number")
            return false
        }
        return true
    }
    
    // MARK:  TextField Methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        self.view.endEditing(true)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField == textFieldMobileNumber
        {
            let textFieldText: NSString = (textField.text ?? "") as NSString
            let txtAfterUpdate = textFieldText.replacingCharacters(in: range, with: string)
            
            if txtAfterUpdate.count > 10
            {
                return false
            }
        }
        return true
    }
    
    // MARK: - Button Actions
    @IBAction func cancelButtonClicked(_ sender: UIButton)
    {
        delegate?.unCheckGetAddressViaSmsButton()
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func sendAddressOnMobileNumber(_ sender: UIButton)
    {
        if validateFields()
        {
            self.getBranchAddressViaSms(mobileNumber: self.textFieldMobileNumber.text!, branchAddress: self.branchAddress)
            {
                (String,status) in
                
                printLog("String---> \(String)")
                printLog("status---> \(status)")
                
//                success | 919011157474 | 3510408628717405518-69037520495717492
                
                if status == true
                {
                    let responseArray = String.components(separatedBy: " | ")
                    printLog("responseArray---> \(responseArray)")
                    
                    if responseArray[0] == "success"
                    {
                        DispatchQueue.main.async
                        {
                            let alert = UIAlertController(title: "Alert", message: "Branch address send successfully on your mobile number" as String, preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { UIAlertAction in
                                    alert.dismiss(animated: true, completion: nil)
                                    self.dismiss(animated: true, completion: nil)
                                }))
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                    else
                    {
                        
                    }
                }
                else
                {
                    self.showAlert(message: "Unable to send address")
                }
            }

        }
    }
    
    
    // MARK: - Sending Address To Server
    func getBranchAddressViaSms(mobileNumber:String ,branchAddress: String, completion: @escaping (_ responseDataString:String , _ status:Bool) -> Void)
    {
        let urlString = String(format: AuthenticationConstant.SendBranchAddressTOSERVERURL,mobileNumber,branchAddress)
        let serviceUrl = URL(string: urlString.replacingOccurrences(of: " ", with: "%20"))
        
        var request = URLRequest(url: serviceUrl!)
        request.httpMethod = HTTPMethod.get.rawValue
        request.setValue(ContentType.ApplicationText.rawValue, forHTTPHeaderField: APIConstants.ContentType)
        
        let session = URLSession.shared
        session.dataTask(with: request)
        {
            (data, response, error) in
            
            if let error = error
            {
                printLog(error)
                completion(error.localizedDescription,false)
            }
            else
            {
                if let response = response
                {
                    printLog(response)
                }
                
                if let data1 = data
                {
                    let responseString:String = String(data: data1, encoding: String.Encoding.utf8)!
                    completion(responseString,true)
                }
            }
        }.resume()
    }
}
