//
//  UserBookingHomeViewController.swift
//  sotc-consumer-application
//
//  Created by Saurav Kumar on 22/08/17.
//  Copyright © 2017 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit
import CoreData

protocol UserBookingTourHomeProtocol{
    
    func fetctAllTourDataFromServer()
    
}

class UserBookingHomeViewController: BaseViewController,UserBookingTourHomeProtocol,CAPSPageMenuDelegate {
    
    @IBOutlet var userBookingHomeView: UIView!
    
    
    var delegateUserBookingList:UserBookingListProtocol? = nil
    var userList: [UserProfile]?
    
     var bookObjectList:[Tour]?
    
    let userBookingUpcoming : UserBookingViewController = UserBookingViewController (nibName: "UserBookingViewController", bundle: nil)
    let userBookingPast : UserBookingViewController = UserBookingViewController (nibName: "UserBookingViewController", bundle: nil)
    
    var pageMenu : CAPSPageMenu?
    var isFromNotification = false
    
    var redirectController:UIViewController?
    
    
    func pushViewController(viewController: UIViewController) {
        
        self.navigationController?.navigationBar.isHidden = false
        
        self.navigationController?.pushViewController(viewController, animated: true)
         
        
    }
    
  
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        
        Bundle.main.loadNibNamed("UserBookingHomeViewController", owner:self, options: nil)
        super.addView(inBaseView:self.userBookingHomeView)
        super.hideBackButton(true)
        
        //super.setHeaderTitle("Self Service ")
     
        super.setHeaderTitle("Manage Holidays ")
        
        
        userBookingPast.delegateUserBookingHome = self
        userBookingUpcoming.delegateUserBookingHome = self
        
        //self.fetchUserBookedTour()
        //self.addTourListViews()
//        self.sliderMenuButton.tag = 2
        if self.isFromNotification && self.redirectController != nil
        {
            self.pushViewController(viewController: self.redirectController!)
            
           // self.addTourListViews()
            
            self.isFromNotification = false
            
            
            
        }else{
        
            self.getBFNListDetails()
            
        }
    }
    
 
    
    override func viewWillAppear(_ animated: Bool) {
        
     self.navigationController?.navigationBar.isHidden = true
     self.tabBarController?.tabBar.isHidden = true
        
        
        UIView.animate(withDuration: 1.0, animations: { 
            
 
            self.navigationController?.setRedNavigationBar()
            self.navigationController?.setnavigatiobBarTitle(title: Title.MY_BOOKING, viewController: self)
            
        }) { (Bool) in
            
            
            
        }
  
        
        NotificationController.updateNotificationBadgeCount()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Tour Data Methods
    
    func getBFNListDetails()
    {
        
        LoadingIndicatorView.show("Loading")
        
        let mobileNumber:String = UserDefaults.standard.object(forKey: UserDefauldConstant.userMobileNumber) as! String
        
        UserBookingController.sendGetBFNListRequest(mobileNumber:  mobileNumber/*"8510894234"*/) { (operationValidationMessage) in
             
            //DispatchQueue.main.sync {
                
                self.fetchUserBookedTour()
                
                let  prodITINCodeArray:Array<String>  = self.getprodITINCodes()
                
                if(prodITINCodeArray.count > 0)
                {
                    self.apiHitPackageDetails(ITIternaryCode: prodITINCodeArray)
                }
                else
                {
                    self.addTourListViews()
                    
                     LoadingIndicatorView.hide()
                    
                }
                
          //  }
            
            
         /*   DispatchQueue.main.sync {
                
                LoadingIndicatorView.hide()
                
            }*/
            
            
            
            
            
        }
        
    }
    
    func fetchUserBookedTour()  {
        
        do {
            
            
            let fetchRequest: NSFetchRequest<UserProfile> = UserProfile.fetchRequest()
            
            let userMobileNumber:String = UserDefaults.standard.value(forKey:UserDefauldConstant.userMobileNumber) as! String
            
            let predicate = NSPredicate(format: "mobileNumber == \(userMobileNumber)")

            fetchRequest.predicate = predicate
             userList = try CoreDataController.getContext().fetch(fetchRequest)
            
            if (userList?.count)! > 0 {
          
                
           bookObjectList = Array((userList?.first?.tourRelation)!) as? [Tour]
                
            bookObjectList?.sort(by: { $0.departureDate?.compare($1.departureDate! as Date) == ComparisonResult.orderedAscending })
             
            
            }
            
        } catch   {
            printLog(error)
        }
        
    }
    
    func getprodITINCodes() -> Array<String>  {
        
        if bookObjectList != nil &&  bookObjectList?.count ?? 0 > 0 {
            
            if bookObjectList != nil {
                
                var prodITINCodeArray:Array<String> = []
                
                for tour in bookObjectList!
                {
                    
                    
                        printLog(tour.bfNumber ?? "")
                      //  printLog(tour.departureDate ?? Date())
                    
                        prodITINCodeArray.append(tour.prodITINCode ?? "")
                        
                    
                    
                }
                
                return prodITINCodeArray
                
            }
            
        }
      
        
        return []
    }
    
    func addTourListViews()  {
        
        
  
        
        if bookObjectList != nil && (bookObjectList?.count)! > 0 {
           
         var upcomingBookedArray:[Tour] = []
        
        let pastBookedArray = bookObjectList?.filter( { (tourDetail: Tour) -> Bool in
            
            return (tourDetail.departureDate?.addingTimeInterval(23.9 * 60 * 60).timeIntervalSince1970)! < Date().timeIntervalSince1970
        })
       if let futureBookedArray = bookObjectList?.filter( { (tourDetail: Tour) -> Bool in
        
            return (tourDetail.departureDate?.timeIntervalSince1970)! > Date().timeIntervalSince1970  })
       {
        
             
       
        //  var currentBookedArray:[Tour] = []
        
        
        if (futureBookedArray.count) > 0 {
            
           // currentBookedArray.append((futureBookedArray.first!))
            
            
            upcomingBookedArray = futureBookedArray
            
            // upcomingBookedArray.remove(at: 0)
            
        }
        
        }
        
        
        //
        //        let userBookingCurrent : UserBookingViewController = UserBookingViewController (nibName: "UserBookingViewController", bundle: nil)
        //        userBookingCurrent.delegate = self
        //        userBookingCurrent.tourBookedList = currentBookedArray
        //        userBookingCurrent.title = "Current"
        
  
        userBookingUpcoming.title = "Upcoming"
        userBookingUpcoming.delegate = self
        userBookingUpcoming.tourBookedList = upcomingBookedArray
        
   
        userBookingPast.title = "Past"
        userBookingPast.delegate = self
        userBookingPast.tourBookedList = pastBookedArray
        
        
        pageMenu = CAPSPageMenu(viewControllers: [/*userBookingCurrent,*/userBookingUpcoming,userBookingPast], frame: CGRect(x: 0.0, y: 48, width: self.userBookingHomeView.frame.width, height: self.userBookingHomeView.frame.height - (self.navigationController?.navigationBar.frame.height)! ), pageMenuOptions: [CAPSPageMenuOption.scrollMenuBackgroundColor(Constant.AppThemeColor) ,CAPSPageMenuOption.titleTextSizeBasedOnMenuItemWidth(true),CAPSPageMenuOption.menuItemWidth(UIScreen.main.bounds.maxX / 2),CAPSPageMenuOption.menuItemFont(UIFont(name:"Roboto", size:15)!),CAPSPageMenuOption.unselectedMenuItemLabelColor(UIColor.groupTableViewBackground),CAPSPageMenuOption.menuHeight(50),CAPSPageMenuOption.menuItemMargin(10)])
        
         pageMenu?.delegate = self
        
        if  self.delegateUserBookingList != nil {
            
             self.delegateUserBookingList?.updateTourData()
            
        }
        
        self.userBookingHomeView.addSubview(pageMenu!.view)
            
        }
        else
        {
 
            userBookingUpcoming.title = "Upcoming"
            userBookingUpcoming.delegate = self
            userBookingUpcoming.tourBookedList = []
            
            
            userBookingPast.title = "Past"
            userBookingPast.delegate = self
            userBookingPast.tourBookedList = []
            
            
            pageMenu = CAPSPageMenu(viewControllers: [/*userBookingCurrent,*/userBookingUpcoming,userBookingPast], frame: CGRect(x: 0.0, y: 48, width: self.userBookingHomeView.frame.width, height: self.userBookingHomeView.frame.height - (self.navigationController?.navigationBar.frame.height)! ), pageMenuOptions: [CAPSPageMenuOption.scrollMenuBackgroundColor(Constant.AppThemeColor) ,CAPSPageMenuOption.titleTextSizeBasedOnMenuItemWidth(true),CAPSPageMenuOption.menuItemWidth(UIScreen.main.bounds.maxX / 2),CAPSPageMenuOption.menuItemFont(UIFont(name:"Roboto", size:15)!),CAPSPageMenuOption.unselectedMenuItemLabelColor(UIColor.groupTableViewBackground),CAPSPageMenuOption.menuHeight(50),CAPSPageMenuOption.menuItemMargin(10)])
            
            pageMenu?.delegate = self
            
            if  self.delegateUserBookingList != nil {
                
                self.delegateUserBookingList?.updateTourData()
                
            }
            
            self.userBookingHomeView.addSubview(pageMenu!.view)
          
            if !self.isFromNotification || self.redirectController == nil
            {
              self.showSyncAlertView()
            }
            
        }
        
        
        
    }
    
    
    
    func apiHitPackageDetails(ITIternaryCode code:Array<String>)
    {
        
        
        UserBookingController.getUserBookedImagesAndCityCovered(bookList:bookObjectList!) { (status) in
            
           // DispatchQueue.main.sync {
                
            self.fetchUserBookedTour()
            self.addTourListViews()
            
            LoadingIndicatorView.hide()
           // }
            
           
        }
        
    }
    
    //MARK: Data Updation Methods On Pull To Refresh
   
    
    func updateBFNListDetails()
    {
        
        LoadingIndicatorView.show("Loading")
        
        let mobileNumber:String = UserDefaults.standard.object(forKey: UserDefauldConstant.userMobileNumber) as! String
        
        UserBookingController.sendGetBFNListRequest(mobileNumber:  mobileNumber/*"8510894234"*/) { (operationValidationMessage) in
            
           // DispatchQueue.main.sync {
                
                self.fetchUserBookedTour()
                
                let  prodITINCodeArray:Array<String>  = self.getprodITINCodes()
                
                if(prodITINCodeArray.count > 0)
                {
                    self.updatePackageDetails(ITIternaryCode: prodITINCodeArray)
                }
                else
                {
                    self.reloadTourListViews()
                    
                    
                }
                
            //}
            
            
         //   DispatchQueue.main.sync {
                
                LoadingIndicatorView.hide()
           
           // }
            
        }
        
    }
    
    func reloadTourListViews()  {
        
       
        
        if bookObjectList != nil && (bookObjectList?.count)! > 0 {
            
        
        var upcomingBookedArray:[Tour] = []
        
        var pastBookingArray:[Tour]  = []
        
       if let pastBookedArray = bookObjectList?.filter( { (tourDetail: Tour) -> Bool in
            return (tourDetail.departureDate?.timeIntervalSince1970)! < Date().timeIntervalSince1970
        })
       {
        if pastBookedArray.count > 0
        {
            pastBookingArray = pastBookedArray
        }
        
        
        }
        if let futureBookedArray = bookObjectList?.filter( { (tourDetail: Tour) -> Bool in
            
            return (tourDetail.departureDate?.timeIntervalSince1970)! > Date().timeIntervalSince1970  })
        {
            if (futureBookedArray.count) > 0 {
                
                upcomingBookedArray = futureBookedArray
                
            }
            
        }
        
        userBookingUpcoming.tourBookedList = upcomingBookedArray
        
        userBookingPast.tourBookedList = pastBookingArray
        
        if userBookingUpcoming.bookingTableView != nil
        {
        
        userBookingUpcoming.bookingTableView.reloadData()
        }
        
        if userBookingPast.bookingTableView != nil
        {
            userBookingPast.bookingTableView.reloadData()
        }
        }
        else
        {
            self.showSyncAlertView()
        }
        
 
        
    }
    func updatePackageDetails(ITIternaryCode code:Array<String>)
    {
        
        
        UserBookingController.getUserBookedImagesAndCityCovered(bookList:bookObjectList!) { (status) in
            
            self.fetchUserBookedTour()
            
            self.reloadTourListViews()
            
            
        }
        
    }
    
    func showSyncAlertView()  {
        
        
        let alertViewController:UIAlertController = UIAlertController(title: "Sync", message: "No tour data available, Please sync again ! ", preferredStyle: UIAlertController.Style.alert)
        
        alertViewController.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: { (UIAlertAction) in
            
            
        }))
        alertViewController.addAction(UIAlertAction(title: "Sync", style: UIAlertAction.Style.default, handler: { (UIAlertAction) in
            
            
               self.updateBFNListDetails()
            
        }))
        
        
        self.present(alertViewController, animated: true) { 
            
            
        }
        
    }
    
    
    
    //MARK: UserBookingTourHomeProtocol
    
    func fetctAllTourDataFromServer() {
        
        self.updateBFNListDetails()
        
    }
    
    //MARK:: CAPSPageMenu Delegate Methods
    
    func willMoveToPage(_ controller: UIViewController, index: Int)
    {
        
//        let viewControoler:UserBookingViewController? = (controller as! UserBookingViewController)
//        
//        
//        if viewControoler != nil {
//            
//             self.delegateUserBookingList = viewControoler
//            
//             self.delegateUserBookingList?.updateTourData()
//            
//            
//        }
        
        
        
    }
  func didMoveToPage(_ controller: UIViewController, index: Int)
  {
    
            let viewControoler:UserBookingViewController? = (controller as! UserBookingViewController)
    
    
            if viewControoler != nil {
    
            if viewControoler?.tourBookedList == nil || viewControoler?.tourBookedList?.count ?? 0 == 0
            {
                AppUtility.displayAlert(message: AlertMessage.NO_BOOKING_AVAILABEL)
                
            }
    
                
            }
    
    }
    
}



