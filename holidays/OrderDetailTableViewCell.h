//
//  OrderDetailTableViewCell.h
//  holidays
//
//  Created by ketan on 06/05/16.
//  Copyright © 2016 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderDetailTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelDateOfTravel;
@property (weak, nonatomic) IBOutlet UILabel *labelReferenceID;
@property (weak, nonatomic) IBOutlet UILabel *labelPackageName;
@property (weak, nonatomic) IBOutlet UILabel *labelTotalCost;
@property (weak, nonatomic) IBOutlet UILabel *labelAmountPaid;
@property (weak, nonatomic) IBOutlet UILabel *labelBalanceAmount;


@end
