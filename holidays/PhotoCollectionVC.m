//
//  PhotoCollectionVC.m
//  holidays
//
//  Created by Kush_Team on 14/07/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "PhotoCollectionVC.h"
#import "ImageViewCell.h"
#import "UIImageView+WebCache.h"
#import "NetCoreAnalyticsVC.h"
@interface PhotoCollectionVC ()
{
    KLCPopup *customePopUp;
    LoadingView *activityLoadingView;
    NSMutableArray *photoCollectionArray;
}
@end

@implementation PhotoCollectionVC

- (void)viewDidLoad {
//    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    photoCollectionArray = [[NSMutableArray alloc]init];
    [self.tableViews registerNib:[UINib nibWithNibName:@"ImageViewCell" bundle:nil] forCellReuseIdentifier:@"imageTableViewCellIdentifier"];
}

- (void)viewWillAppear:(BOOL)animated{
    if ([self.packageDetailInPhotoGallery.arrayPackageImagePathList count]>0){
        for (NSDictionary *object in self.packageDetailInPhotoGallery.arrayPackageImagePathList){
            if ([[object objectForKey:@"typeId"] intValue] == 0){
                [photoCollectionArray addObject:object];
            }
        }
        [self.tableViews reloadData];
    }else{
        [self showAlertViewWithTitle:@"Empty" withMessage:@"No Images Available"];
    }
}
- (void)viewDidAppear:(BOOL)animated{
    
}

#pragma mark -Table view Delegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
//    return [self.packageDetailInPhotoGallery.arrayPackageImagePathList count];
    return [photoCollectionArray count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 150;
    
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
    NSLog(@"indea path cell for row  %ld",(long)indexPath.row);
    
    
    
    /*   activityLoadingView = [LoadingView loadingViewInView:self.view.superview.superview
     withString:@""
     andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];*/
    ImageViewCell *cell = (ImageViewCell *) [tableView dequeueReusableCellWithIdentifier:@"imageTableViewCellIdentifier"];
    
//    if (cell == nil)
//    {
//        NSArray *inb= [[NSBundle mainBundle] loadNibNamed:@"ImageViewCell" owner:self options:nil];
//
//        cell=[inb objectAtIndex:0];
//    }
    
    
    
    //  [cell.imageView sd_setImageWithURL:[NSURL URLWithString:@"http://www.domain.com/path/to/image.jpg"]placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    
    cell.imageView.frame=CGRectMake(cell.imageView.frame.origin.x, cell.imageView.frame.origin.y, [[UIScreen mainScreen]bounds].size.width, cell.imageView.frame.size.height);
    cell.selectorView.frame=CGRectMake(cell.selectorView.frame.origin.x, cell.selectorView.frame.origin.y, [[UIScreen mainScreen]bounds].size.width, cell.selectorView.frame.size.height);
    
    
    NSLog(@"ht %f",[[UIScreen mainScreen]bounds].size.height);
    //cell.imageView.frame=CGRectMake(0, 0, 520, 44);
//    NSArray *imageArray = self.packageDetailInPhotoGallery.arrayPackageImagePathList;
    NSArray *imageArray = photoCollectionArray;
    if (imageArray.count != 0)
    {
        
        
        
//        NSDictionary *imageDict = self.packageDetailInPhotoGallery.arrayPackageImagePathList[indexPath.row];
        NSDictionary *imageDict = photoCollectionArray[indexPath.row];
        
        //NSString* imageName = [[imageDict valueForKey:@"path"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        NSString* imageName = [[imageDict valueForKey:@"path"] stringByReplacingOccurrencesOfString:@" " withString:@"_"];

        NSString *imageUrlString = [NSString stringWithFormat:@"%@/images/holidays/%@/photos/%@",kUrlForImage,self.packageDetailInPhotoGallery.strPackageId,imageName];
        
        
        NSURL *urlImage = [NSURL URLWithString:imageUrlString];
        
        if(urlImage)
        {
            
            UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            indicator.center = cell.imageView.center;// it will display in center of image view
            [cell.imageView addSubview:indicator];
            [indicator startAnimating];
            
            
            
            [cell.imageView sd_setImageWithURL:urlImage placeholderImage:[UIImage imageNamed:@"defaultBanner.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
             {
                 
                 [indicator stopAnimating];
                 
             }];

//            [cell.imageView sd_setImageWithURL:urlImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//
//                [indicator stopAnimating];
//
//            }];
            
            
            
            //[cell.imageView sd_setImageWithURL:urlImage];
            // [cell.imageView setImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:urlImage]]];
            
        }
        
    }
    
    //
    //    if ([activityLoadingView isDescendantOfView:self.view.superview.superview])
    //    {
    //        [activityLoadingView removeView];
    //        activityLoadingView = nil;
    //    }
    return  cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    NSLog(@"indea path did select row  %ld",(long)indexPath.row);
    //  UIView *popUpView = [UIView new];
    UIImageView *popUpImageView=[[UIImageView alloc]init];
    
    //UIView *popUpView = [UIView new];
    popUpImageView.frame = CGRectMake(0, 0, 300 , 300);
    popUpImageView.backgroundColor = [UIColor whiteColor];
    popUpImageView.layer.cornerRadius = 1.0;
    popUpImageView.layer.borderColor=[[UIColor whiteColor]CGColor];
    popUpImageView.layer.borderWidth= 1.0f;
    
    UIButton *closeBtn=[[UIButton alloc]init];
    closeBtn.frame=CGRectMake(popUpImageView.frame.size.width-30, 0, 30, 30);
    [closeBtn addTarget:self action:@selector(closeBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    popUpImageView.userInteractionEnabled = YES;
    
    [closeBtn setBackgroundImage:[UIImage imageNamed:@"plus_light_gray@2x.png"] forState:UIControlStateNormal];
    
    [popUpImageView addSubview: closeBtn];
    
   // UITableViewCell *cell = [self.imageTableView cellForRowAtIndexPath:indexPath];
    
    //UIImage *cellImage = cell.imageView.image;
    
//    NSArray *imageArray = packageDetail.arrayImages;
//
//    if (imageArray.count != 0)
//    {
//        NSString *imageUrlString = [imageArray objectAtIndex:indexPath.row];
//        NSURL *urlImage = [NSURL URLWithString:imageUrlString];
//
//        if(urlImage)
//        {
//            [popUpImageView setImage:cellImage];
//        }
//
//    }
    
//    NSDictionary *imageDict = self.packageDetailInPhotoGallery.arrayPackageImagePathList[indexPath.row];
    NSDictionary *imageDict = photoCollectionArray[indexPath.row];
    
    //NSString* imageName = [[imageDict valueForKey:@"path"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSString* imageName = [[imageDict valueForKey:@"path"] stringByReplacingOccurrencesOfString:@" " withString:@"_"];
    
    NSString *imageUrlString = [NSString stringWithFormat:@"%@/images/holidays/%@/photos/%@",kUrlForImage,self.packageDetailInPhotoGallery.strPackageId,imageName];
    
    
    NSURL *urlImage = [NSURL URLWithString:imageUrlString];
    UIImage *cellImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:urlImage]];
    [popUpImageView setImage:cellImage];

    customePopUp = [KLCPopup popupWithContentView:popUpImageView
                                         showType:KLCPopupShowTypeGrowIn
                                      dismissType:KLCPopupDismissTypeFadeOut
                                         maskType:KLCPopupMaskTypeDimmed
                         dismissOnBackgroundTouch:YES
                            dismissOnContentTouch:NO];
    
    [customePopUp show];
    
    
}

-(void)closeBtnAction:(id)sender{
    
    [customePopUp removeFromSuperview];
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath

{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        
        [cell setSeparatorInset:UIEdgeInsetsZero];
        
    }
    
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        
        [cell setPreservesSuperviewLayoutMargins:NO];
        
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        
        [cell setLayoutMargins:UIEdgeInsetsZero];
        
    }
    
}

- (IBAction)btn_backPress:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


@end
