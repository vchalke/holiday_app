//
//  PopUpPleaseNoteViewController.swift
//  holidays
//
//  Created by Swapnil on 1/9/18.
//  Copyright © 2018 Mobicule. All rights reserved.
//

import UIKit

class PopUpPleaseNoteViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func cancelButtonPress(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
     
     You will have to make the balance payment within the next
     48hours to complete the transaction
     
     Balance payment can only be made at the branches
     
     If the tranaction is not completed within the next 48hours,
     the advance payment collected will be forfeited
    }
    */

}
