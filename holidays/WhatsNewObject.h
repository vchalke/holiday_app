//
//  WhatsNewObject.h
//  holidays
//
//  Created by Kush_Team on 27/05/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface WhatsNewObject : NSObject
@property(nonatomic,strong)NSString *bannerLevelType;
@property(nonatomic,strong)NSString *bannerDuration;
@property(nonatomic,strong)NSString *bannerLevelName;
@property(nonatomic,strong)NSString *bannerLargeImage;
@property(nonatomic,strong)NSString *bannerMediumImage;
@property(nonatomic,strong)NSString *bannerSmallImage;
@property(nonatomic,strong)NSString *mainBannerImgUrl;
@property(nonatomic,strong)NSString *bannerName;
@property(nonatomic,strong)NSString *bannerType;
@property(nonatomic,strong)NSString *packageID;
@property(nonatomic,strong)NSString *bannerPageName;
@property(nonatomic,strong)NSString *redirectUrl;
@property(nonatomic,strong)NSString *bannerStrikeoutPrice;
@property(nonatomic,strong)NSString *bannerStartingPrice;
@property(nonatomic)NSInteger bannerOrder;
@property(nonatomic)NSInteger orderOnThePage;
@property(nonatomic,strong)NSString *destinationName;
-(instancetype)initWithImgArray:(NSDictionary *)arrayObj;
@end

NS_ASSUME_NONNULL_END
