
//  UserProfileDetailsViewController.swift
//  sotc-consumer-application
//
//  Created by Parshwanath on 28/08/17.
//  Copyright © 2017 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit
import MessageUI

class UserProfileDetailsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,MFMailComposeViewControllerDelegate {

    
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userProfileTableView: UITableView!
    var passaenger:Passenger?
    
    
    var tourStatus:TourStatus?
    
    @IBOutlet weak var editProfileButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.isHideEditProfileButton()
        
        
        userNameLabel.text =  "\(passaenger?.firstName ?? "") \(passaenger?.middleName ?? "") \(passaenger?.lastName ?? "")".capitalized // (passaenger?.firstName)! + " " + (passaenger?.lastName)!
        // Do any additional setup after loading the view.
        
        
        self.userProfileTableView.register(UINib(nibName:  "ProfileTableViewCell", bundle: nil), forCellReuseIdentifier: "ProfileTableViewCell")
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   
    @IBAction func onEditProfileClick(_ sender: Any) {
        
        self.showEditProfiletAlertController()
    }
    
    func showEditProfiletAlertController()  {
        
        
        let alertController = UIAlertController(title: AlertMessage.TITLE_REMARKS, message: "", preferredStyle: UIAlertController.Style.alert)
        
        let saveAction = UIAlertAction(title: AlertMessage.TITLE_DONE, style: UIAlertAction.Style.default, handler: {
            alert -> Void in
            
            let firstTextField = alertController.textFields![0] as UITextField
            
            
         /*   let mailComposeViewController = self.configuredMailComposeViewController(remark:firstTextField.text!)
            
            if MFMailComposeViewController.canSendMail() {
                
                self.present(mailComposeViewController, animated: true, completion: nil)
                
            } else {
                
                self.showSendMailErrorAlert()
            }*/
            
            if AppUtility.checkStirngIsEmpty(value: firstTextField.text!)
            {
                self.showAlertViewWithTitle(title: "", message: AlertMessage.Profile.REMARK_VALIDATION)
            }else
            {
                self.sendEditProfileEmailHit(remark:firstTextField.text!)
            }
            
            
           
            
        })
        
        
        alertController.textFields?.first?.keyboardType = UIKeyboardType.decimalPad
        
        let cancelAction = UIAlertAction(title: AlertMessage.TITLE_CANCEL, style: UIAlertAction.Style.default, handler: {
            (action : UIAlertAction!) -> Void in
            
        })
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = ""
        }
        
        alertController.view.backgroundColor = UIColor.white
        alertController.view.layer.cornerRadius = 10.0
        
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)

        
    }

    //MARK: - Profile Table View Data Sourse Methods
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        printLog(UserProfileEnum.count)
        return UserProfileEnum.count
    }
    
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileTableViewCell", for: indexPath) as? ProfileTableViewCell
            else {
                
                fatalError("The dequeued cell is not an instance of MealTableViewCell.")
        }
        
            let cellDispalyDetails = self.getCellDisplayDetails(cellIndex: indexPath.row)
            
            cell.titleLabelDetail?.text = cellDispalyDetails.headerText
            
        
            cell.imageViewDetail?.image = cellDispalyDetails.displayImage
            
            cell.subTitleLabelDetail?.text = cellDispalyDetails.detailsText
            cell.selectionStyle = .none
        

            return cell
        
        
    }
    
    
    //MARK: - Profile Table View Delegate Methods
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        
        return self.getCellRowHeight(cellIndex: indexPath.row)
        
    }
    
    
    func getCellDisplayDetails(cellIndex:Int) -> (headerText: String, detailsText: String?, displayImage:UIImage)
    {
        
        if let userProfileEnum = UserProfileEnum(rawValue: cellIndex)  {
            
            switch userProfileEnum {
                
            case .address:
                return(Constant.Profile.ADRESS_TEXT,passaenger?.address,#imageLiteral(resourceName: "address"))
                
            case .emailId:
                return(Constant.Profile.EMAIL_ID_TEXT,passaenger?.emailId,#imageLiteral(resourceName: "emailId"))
                
            case .contactNumber:
                return(Constant.Profile.CONTACT_NUMBER_TEXT, passaenger?.mobileNumber,#imageLiteral(resourceName: "contactNumber"))
                
           /* case .panCard:
                return("Pan Card", passaenger!.panCard!,#imageLiteral(resourceName: "panCard"))*/
                
            case .aadhar:
                return(Constant.Profile.AADHAR_TEXT, passaenger?.aadhaarNumber,#imageLiteral(resourceName: "aadhar"))
                
            case .passport:
                return(Constant.Profile.PASSPORT_TEXT, passaenger?.passportNumber,#imageLiteral(resourceName: "passport"))
                
                
            default:
                return("" , "" , UIImage())
            }
            
            
        }
        
        return("" , "" , UIImage())
        
    }
    
    func getCellRowHeight(cellIndex:Int) -> CGFloat
    {
        
        if let userProfileEnum = UserProfileEnum(rawValue: cellIndex)  {
            
            
            switch userProfileEnum {
                
                //            case .profile:
                //                return 90.0
                
            case .address:
                return 100.0
                
            case .emailId,.contactNumber,.aadhar,.passport: //.panCard,
                return 70.0
                
                
            default:
                return 0.0
            }
            
            
        }
        
        return 0.0
        
    }

    //MARK: - Send Edit Profile Mail Hit
    func sendEditProfileEmailHit(remark:String)
    {
       if let tourDetails:Tour = (self.passaenger?.tourRelation)
       {
        
            let tourName:String =  (tourDetails.tourName!)
            
            let branchName:String = (tourDetails.branch!)
            
            let bookingId:String =  (tourDetails.bfNumber!)
        
            let bfnNumber:String = (tourDetails.bfNumber!)
        
            let tourDateInNSDate = tourDetails.departureDate
        
            var tourDate:String = ""
        
            if tourDateInNSDate != nil
            {
                tourDate = AppUtility.convertDateToString(currentDateFormat: "yyyy-MM-dd", date:tourDateInNSDate! as Date, requiredDateFormat: "dd-MM-yyyy")
            }
        
            _ =  (passaenger?.firstName!)! + " " + (passaenger?.lastName!)!
            
           // let groupDate =  self.oldStartDateFloatLabelTextField.text!
            
           // let newData =  self.newDateFloatLabelTextField.text!
            
            let remark =  remark
            
            var passengerArray : Array<Any> = []
        
        let fullName =  passaenger?.firstName ?? ""  + " " +  (passaenger?.lastName!)!
        
        let emailId =  passaenger?.emailId ?? ""
        
        let passengerDict:Dictionary<String,String> = ["fullname":fullName,"mailId":emailId]
        
        passengerArray.append(passengerDict)
        
        var contactNumber:String = ""
        
         let passengerContactNumber = self.passaenger?.mobileNumber ?? ""
        
        if !passengerContactNumber.isEmpty && !(passengerContactNumber == "")
        {
            contactNumber = passengerContactNumber
            
        }else{
                contactNumber = tourDetails.mobileNumber ?? ""
        }
            
        /*let sendMailDictionary:Dictionary<String,Any> = ["messageType":"PROFILE","TourName":tourName,
                                                             "TourDate":tourDate,
                                                             "BookingID":bookingId,
                                                             "BranchName":branchName,
                                                             "ContactNumber":contactNumber,
                                                             "SalesUser": tourDetails.createdBy ?? "",
                                                             "Remarks":remark,
                                                             "CustomerName":fullName,
                                                             "PassengerName":passengerArray]*/
        
        let sendMailDictionary:Dictionary<String,Any> = ["messageType":"PROFILE","tourName":tourName,
                                                         "tourDate":tourDate,
                                                         "bookingID":bookingId,
                                                         "branchName":branchName,
                                                         "contactNumber":contactNumber,
                                                         "salesUser":tourDetails.createdBy ?? "",
                                                         "remarks":remark,
                                                         "customerName":fullName,
                                                         "passengerName":fullName,
                                                         "newDate":"",
                                                         "groupDate":"",
                                                         "mailId":emailId]
            
            LoadingIndicatorView.show("Loading")
        
        UserBookingController.sendEmailRequest(emailRequestData:sendMailDictionary, completion: { (status, message) in
            LoadingIndicatorView.hide()
            self.showAlertViewWithTitle(title: "", message: message)
            
            if status
            {
                
                let notificationDate = Date()//.addingTimeInterval(1)  //addingTimeInterval(60) //(24 * 60 * 60)
                
                let noticationDateTimeInMiliSec = (notificationDate.timeIntervalSince1970 * 1000)
                
                
                let notificationData:Dictionary<String,Any> = self.getProfileNotificationData(bfnNumber: bfnNumber,notificationDateTimeInMiliSec: noticationDateTimeInMiliSec)
            
                NotificationController.fireLocaleNotification(notificationUserInfo: notificationData,fireDateTime: notificationDate)
           }
            
        })
        
                
            //}
    }
       
        
    }
    
    
    //MARK: Method to send Deviation-Request mail
    func configuredMailComposeViewController(remark:String) -> MFMailComposeViewController {
        
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
        var bodyMessage = ""
        
       if let tour:Tour = passaenger?.tourRelation
       {
        
        
        let tourName:String =  tour.tourName!
        
         let branchName:String =  tour.branch!
        
        let bookingId:String =  tour.tourCode!
        
        let departureDate:String =  AppUtility.convertDateToString(date: tour.departureDate! as Date)
        
        let passengerName =  (passaenger?.firstName!)! + " " + (passaenger?.lastName!)!
        
        
        let remark = "Remark:- " + remark + " \n"
        
         bodyMessage = " Dear SOTC Team,\n \t This is in reference to our  " + tourName + " departing on " + departureDate + " ," +  bookingId + " \n \t ( Booking details :-  "+branchName+" | Sales User ) \n \t We would request you to make the changes in our booking as mentioned below  \n "+remark+"       \n Thanking You, \n " + passengerName

        }
        
        mailComposerVC.setToRecipients(["aditi.modi@mobicule.com"])
        mailComposerVC.setSubject("Request for Update passenger details")
        mailComposerVC.setMessageBody(bodyMessage, isHTML: false)
        
        return mailComposerVC
    }
    
    func showSendMailErrorAlert() {
        let sendMailErrorAlert = UIAlertView(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.", delegate: self, cancelButtonTitle: AlertMessage.TITLE_OK)
        sendMailErrorAlert.show()
    }
    
    // MARK: MFMailComposeViewControllerDelegate
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
        
    }
    
    func showAlertViewWithTitle(title : String,message:String) -> Void {
        let alertController : UIAlertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: AlertMessage.TITLE_OK, style: UIAlertAction.Style.default) {
            UIAlertAction in
            NSLog("OK Pressed")
        }
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func isHideEditProfileButton()  {
        
        if self.tourStatus == TourStatus.Past ||  self.tourStatus == TourStatus.Cancelled
        {
            self.editProfileButton.isHidden = true
            self.editProfileButton.isUserInteractionEnabled = false
        }
        
    }
    
    func getProfileNotificationData(bfnNumber:String , notificationDateTimeInMiliSec:Any) -> Dictionary<String,Any>  {
        
        var notificationData:Dictionary<String,String> = Dictionary<String,String>()
        notificationData[NotificationConstant.BFN] = bfnNumber
        notificationData[NotificationConstant.TITLE] = bfnNumber
        notificationData[NotificationConstant.CREATED_ON] = "\(notificationDateTimeInMiliSec)"
      //  printLog("next day time interval : \(nextDayDate.millisecondsSince1970)")
        notificationData[NotificationConstant.IMAGE_URL] = ""
        notificationData[NotificationConstant.MESSAGE] = "We are in receipt of your Profile Change Request. Our customer service executive will confirm the status of your request shortly"
        notificationData[NotificationConstant.NOTIFICATION_TYPE] = NotificationType.ProfileLocale.rawValue
        notificationData[NotificationConstant.REDIRECT] = ""
        notificationData[NotificationConstant.SUBTITLE] = "We are in receipt of your Profile Change Request. Our customer service executive will confirm the status of your request shortly"

        return notificationData
        
    }
    
   
    
}

//MARK: - Bussiness Module

enum UserProfileEnum:Int {
    
    case //profile = 0
     address = 0
    , emailId
    ,contactNumber
    , panCard
    , aadhar
    , passport
    
    static var count: Int { return UserProfileEnum.passport.rawValue + 1}
    
}

class UserProfileModule: NSObject {
    
    lazy var userName:String = String()
    
    lazy var address:String = String()
    
    lazy var emailId:String = String()
    
    lazy var contactNumber:String = String()
    
    lazy var panCard:String = String()
    
    lazy var aadhar:String = String()
    
    lazy var passport:String = String()
    
    lazy var profileImage:UIImage = UIImage()
    
    override init() {
        
        super.init()
        
        self.userName = "Profile"
        
        self.address = "Kherani Rd, Next to Post Office, SakiNaka, Andheri East, Asalf, Mumbai, Maharashtra 400072"
        
        self.emailId = "demo@gmail.com"
        
        self.contactNumber = "7052658923"
        
        self.panCard = "56215"
        
        self.aadhar = "54654465646"
        
        self.passport = "78555"
        
        //self.profileImage = #imageLiteral(resourceName: "profilePic")
        
        
        
    }
    
    
    init(userProfileDataDict:Dictionary<String,Any>) {
        
        super.init()
        
        self.userName = userProfileDataDict["userName"] as! String
        
        self.address = userProfileDataDict["address"] as! String
        
        self.emailId = userProfileDataDict["emailId"] as! String
        
        self.contactNumber = userProfileDataDict["contactNumber"] as! String
        
        self.panCard = userProfileDataDict["panCard"] as! String
        
        self.aadhar = userProfileDataDict["aadhar"] as! String
        
        self.passport = userProfileDataDict["passport"] as! String
        
        
    }
    
    
    
    
    
}
