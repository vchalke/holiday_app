//
//  CoreUtility.m
//  mobicule-cms-ios-core
//
//  Created by Ujwala Patil on 4/29/14.
//  Copyright (c) 2014 Girish Patil. All rights reserved.
//

#import "CoreUtility.h"

#import "MobiculeUtilityManager.h"
#import "LoadingView.h"

#define CONTENTS_FOLDER @"content"
#define MULTIMEDIA_FOLDER @"multiMediaFile"
#define CATEGORY_FOLDER @"category"
#define STREAMING_FOLDER @"streamingIcons"
#define MULTIMEDIA_FOLDER_FOR_IMAGE @"images"
#define MULTIMEDIA_FOLDER_FOR_VIDEO @"videos"
#define MULTIMEDIA_FOLDER_FOR_AUDIO @"audios"
#import "Reachability.h"
#import "SSKeychain.h"
#define KEYCHAIN_SERVICE @"Holiday_App_Keychain"
#define KEYCHAIN_ACCOUNT @"com.thomasCookHoliday.app"

#define DATE_FORMAT_24_HR @"dd-MMM-yyyy HH:mm:ss"
#define DATE_FORMAT_12_HR @"dd-MMM-yyyy hh:mm:ss a"

@implementation CoreUtility

+ (void) createDirectoryIfNotExists:(NSString *) directoryPath
{
    NSError *error;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if (![fileManager fileExistsAtPath: directoryPath])
    {
        if(DEBUG_ENABLED)
        {
            debug(@"Directory %@ Not Present. Creating it...", directoryPath);
        }
        
        if(![fileManager createDirectoryAtPath:directoryPath withIntermediateDirectories:YES attributes:nil error:&error])
        {
            debug(@"Error: Create Directory failed. Returning...");
            return;
        }
        
        if(DEBUG_ENABLED)
        {
            debug(@"Directory %@ Created Successfully...", directoryPath);
        }
    }
    else
    {
        debug(@"Directory %@ Already Present.", directoryPath);
    }
}

+ (BOOL) fileExistsAtPath:(NSString *) contentItemPath
{
   
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if (![fileManager fileExistsAtPath:contentItemPath])
    {
        if(DEBUG_ENABLED)
        {
            debug(@"Content %@ Not Present.", contentItemPath);
        }
        
        return NO;
    }
    else
    {
        debug(@"Content %@ Already Present.", contentItemPath);
        return YES;
    }
}

+ (NSString *) getContentsRootPath
{
    return [NSString stringWithFormat:@"%@/%@", [CoreUtility getDocumentsDirectoryPath], CONTENTS_FOLDER];
}



+ (NSString *) getEmergencyMediaRootPath
{
    return [NSString stringWithFormat:@"%@/%@", [CoreUtility getDocumentsDirectoryPath], MULTIMEDIA_FOLDER];
}

+ (NSString *) getEmergencyMediaRootPathForImage
{
    return [NSString stringWithFormat:@"%@/%@", [CoreUtility getEmergencyMediaRootPath], MULTIMEDIA_FOLDER_FOR_IMAGE];
}
+ (NSString *) getEmergencyMediaRootPathForVideo
{
    return [NSString stringWithFormat:@"%@/%@", [CoreUtility getEmergencyMediaRootPath], MULTIMEDIA_FOLDER_FOR_VIDEO];
}
+ (NSString *) getEmergencyMediaRootPathForAudio
{
    return [NSString stringWithFormat:@"%@/%@", [CoreUtility getEmergencyMediaRootPath], MULTIMEDIA_FOLDER_FOR_AUDIO];
}


+ (NSString *) getMediaDirectoryPath:(NSString *)messageId WithMediaType:(NSString * )mediaType

{
    
    if ([mediaType isEqualToString:@"i"]) {
        
        return [NSString stringWithFormat:@"%@/%@", [CoreUtility getEmergencyMediaRootPathForImage], messageId];

        
    }
   else if ([mediaType isEqualToString:@"v"]) {
        
        return [NSString stringWithFormat:@"%@/%@", [CoreUtility getEmergencyMediaRootPathForVideo], messageId];

        
    }
   else if ([mediaType isEqualToString:@"a"]) {
       
       
        return [NSString stringWithFormat:@"%@/%@", [CoreUtility getEmergencyMediaRootPathForAudio], messageId];

        
    }
    
    
        return @"";
}




+ (NSString *) getStreamingRootPath
{
    return [NSString stringWithFormat:@"%@/%@", [CoreUtility getDocumentsDirectoryPath], STREAMING_FOLDER];
}

+ (NSString *) getCategoryRootPath
{
    return [NSString stringWithFormat:@"%@/%@", [CoreUtility getDocumentsDirectoryPath], CATEGORY_FOLDER];
}
+ (NSString *) getContentDirectoryPath:(NSString *)contentId
{
    return [NSString stringWithFormat:@"%@/%@", [CoreUtility getContentsRootPath], contentId];
}

+ (NSString *) getStreamingIconDirectoryPath:(NSString *)contentID
{
    return [NSString stringWithFormat:@"%@/%@", [CoreUtility getStreamingRootPath], contentID];
}


+ (NSString *) getCategoryDirectoryPath:(NSString *)categoryId
{
    return [NSString stringWithFormat:@"%@/%@", [CoreUtility getCategoryRootPath], categoryId];
}

+ (NSString *) getDocumentsDirectoryPath
{
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
}
+ (id) sharedclassname {
    static dispatch_once_t pred = 0;
    __strong static id _sharedObject = nil;
    dispatch_once(&pred, ^{
        _sharedObject = [[self alloc] init];
    });
    return _sharedObject;
}

#pragma mark -
#pragma mark TIMESTAMP

+ (NSString *) convertSystemMillisToDateString:(NSString *) systemMillis
{
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:([systemMillis longLongValue] / 1000)];
    
    NSDateFormatter *dateFormatter = [CoreUtility createDateFormatter12hr];
    
    NSString *dateString = [dateFormatter stringFromDate:date];
    
    
    
    return dateString;
}







+ (NSString *) convertSystemMillisToDateString:(NSString *) systemMillis WithTimeFormat:(NSString *)timeFormat
{
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:([systemMillis longLongValue] / 1000)];
    
    NSDateFormatter *dateFormatter = [CoreUtility createDateFormatter12hr:timeFormat];
    
    NSString *dateString = [dateFormatter stringFromDate:date];
    
 
    
    return dateString;
}


+ (NSString *) convertDateToString:(NSDate *) atDate
{
    //NSDate *date = [NSDate dateWithTimeIntervalSince1970:([systemMillis longLongValue] / 1000)];
    
    NSDateFormatter *dateFormatter = [CoreUtility createDateFormatter12hr];
    
    NSString *dateString = [dateFormatter stringFromDate:atDate];
    
    
    
    return dateString;
}


+ (NSDate *) createDateFromString:(NSString *) dateString
{
    if (!dateString) {
        return nil;
    }
    
    NSDateFormatter *dateFormatter = [CoreUtility createDateFormatter12hr];
    
    NSDate *date = [dateFormatter dateFromString:dateString];
    
   
    
    return date;
}

+ (NSDate *) createDateFromTimeStampString:(NSString *) dateString
{
    if (!dateString) {
        return nil;
    }
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:([dateString longLongValue] / 1000)];
    
    NSDateFormatter *dateFormatter = [CoreUtility createDateFormatter12hr];
    [dateFormatter setDefaultDate:date];
    
    return [dateFormatter defaultDate];
}

+ (NSDateFormatter*) createDateFormatter12hr
{
    NSLocale *posix = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc]init];
    
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormatter setLocale:posix];
    [dateFormatter setDateFormat:DATE_FORMAT_12_HR];
    
    return dateFormatter;
}



+ (NSDateFormatter*) createDateFormatter12hr:(NSString * )timeFormat
{
    NSLocale *posix = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc]init];
    
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormatter setLocale:posix];
    [dateFormatter setDateFormat:timeFormat];
    
    return dateFormatter;
}

+ (BOOL) isFutureDate:(NSString *)date
{
   // NSDate *givenDate = [CoreUtility createDateFromTimeStampString:date];
    NSDate *givenDate = [CoreUtility createDateFromString:[CoreUtility convertSystemMillisToDateString:date]];
    NSDate *currentDate = [NSDate date];
    
    switch ([givenDate compare:currentDate]) {
        case NSOrderedAscending:
            // givenDate is earlier in time than currentDate
            return NO;
        case NSOrderedSame:
            // The dates are the same
            return NO;
        case NSOrderedDescending:
            // givenDate is later in time than currentDate
            return YES;
    }
}

+ (BOOL) isExpiredDate:(NSString *)date
{
    // NSDate *givenDate = [CoreUtility createDateFromTimeStampString:date];
    NSDate *givenDate = [CoreUtility createDateFromString:[CoreUtility convertSystemMillisToDateString:date]];
    NSDate *currentDate = [NSDate date];
    
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSInteger comps = (NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear);
    
    NSDateComponents *givendateComponents = [calendar components:comps
                                                    fromDate:givenDate];
    NSDateComponents *currentComponents = [calendar components:comps
                                                    fromDate:currentDate];
    
    givenDate = [calendar dateFromComponents:givendateComponents];
    currentDate = [calendar dateFromComponents:currentComponents];
    
    switch ([givenDate compare:currentDate]) {
        case NSOrderedAscending:
            // givenDate is earlier in time than currentDate
            return YES;
        case NSOrderedSame:
            // The dates are the same
            return NO;
        case NSOrderedDescending:
            // givenDate is later in time than currentDate
            return NO;
    }
}


+ (NSDateFormatter*) createDateFormatter24hr
{
    NSLocale *posix = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc]init];
    
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormatter setLocale:posix];
    [dateFormatter setDateFormat:DATE_FORMAT_24_HR];
    
    return dateFormatter;
}






+(NSString *)getTimeDiffInStringFromDate:(NSDate*)fromDate WithToDate:(NSDate*)toDate
{
    
    NSInteger seconds = 0;
    NSInteger minutes = 0;
    NSInteger hours  = 0;
    NSInteger day = 0;
    NSInteger month = 0;
    
    NSTimeInterval diffBetweenDates = [toDate timeIntervalSinceDate:fromDate];
    
    if (diffBetweenDates > 60) {
        
        
        minutes = diffBetweenDates /60;
        
        if (minutes > 60) {
            
            hours = minutes / 60;
        }
        
        if (hours > 24) {
            
            day =  hours / 24;
        }
        
        if (day > 24) {
            
            month =  day / 30;
        }
        
        
    }
    
    
    if (month > 0) {
        
       return  [NSString stringWithFormat:@"%02ld  month ago", (long)month];
        
    }
    
    if (day > 0) {
        
     return   [NSString stringWithFormat:@"%02ld  day ago", (long)day];
        
    }
    
    if (hours > 0) {
        
     return   [NSString stringWithFormat:@"%02ld  hours ago", (long)hours];
        
    }
    if (minutes > 0) {
        
     return   [NSString stringWithFormat:@"%02ld  minutes ago", (long)minutes];
        
    }
    if (seconds > 0) {
        
    return    [NSString stringWithFormat:@"%02ld  seconds ago", (long)seconds];
        
    }
    
    else
        return  [NSString stringWithFormat:@"%02ld  seconds ago", (long)0];;
}

+ (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}
/*
+(NSDictionary *)getHeaderDict
{
    NSString *tokenId = [[NSUserDefaults standardUserDefaults] valueForKey:kuserDefaultTokenId];
    NSString *requestId = [[NSUserDefaults standardUserDefaults] valueForKey:kuserDefaultRequestId];

    NSDictionary *dictForHeader = [[NSDictionary alloc] initWithObjectsAndKeys:requestId,@"requestId",tokenId,@"sessionId" ,nil];
    
    return dictForHeader;
}
*/
+(NSDictionary *)getHeaderDict
{
    NSString *tokenId = [[NSUserDefaults standardUserDefaults] valueForKey:kuserDefaultTokenId];
    NSString *requestId = [[NSUserDefaults standardUserDefaults] valueForKey:kuserDefaultRequestId];
    NSString *uniquID = [[NSUserDefaults standardUserDefaults] valueForKey:kUserDeviceUniqueID];
    /*
     requestId: aCvmKn26vC
     sessionId: svI4Ejc1jN147242

     */
    
    NSDictionary *dictForHeader = [[NSDictionary alloc] initWithObjectsAndKeys:requestId,@"requestId",tokenId,@"sessionId",uniquID,@"uniqueId" ,nil];
    
    return dictForHeader;
}
-(NSString *)getDeviceUDID{
    NSString *deviceUdid = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    return deviceUdid;
}
+(void)reloadRequestID
{
    
    LoadingView  *activityLoadingView = [LoadingView loadingViewInView:[UIApplication sharedApplication].keyWindow withString:@"" andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];

    @try
    {
        
        if ([SSKeychain passwordForService:KEYCHAIN_SERVICE account:KEYCHAIN_ACCOUNT] == nil)
        {
            NSString *deviceId = [MobiculeUtilityManager getApplicationUUID];
            [SSKeychain setPassword:deviceId forService:KEYCHAIN_SERVICE account:KEYCHAIN_ACCOUNT];
        }
        
        NetworkHelper *networkHelper = [[NetworkHelper alloc] init];
        
        NSDictionary *headerDict = [[NSDictionary alloc] initWithObjectsAndKeys:[SSKeychain passwordForService:KEYCHAIN_SERVICE account:KEYCHAIN_ACCOUNT],@"uniqueId",@"mobicule",@"user", nil];
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            [networkHelper  getResponseWithRequestType:@"" withQueryParam:@"" withPathParam:@"" withJsonParam:nil withHeaders:headerDict withUrl:kAstraUrlNewToken success:^(NSDictionary *responseDict)
             {
                 dispatch_async(dispatch_get_main_queue(), ^(void) {
                     
                     [activityLoadingView removeFromSuperview];
                     
                     NSLog(@"%@",responseDict);
                     
                     if(responseDict)
                     {
                         if (responseDict.count > 0)
                         {
                             NSDictionary *requestId  = [responseDict valueForKey:@"requestId"];
                             NSDictionary *tokenId  = [responseDict valueForKey:@"tokenId"];
                             
                             if (requestId)
                             {
                                 [[NSUserDefaults standardUserDefaults] setObject:requestId forKey:kuserDefaultRequestId];
                                 
                             }
                             
                             if (tokenId)
                             {
                                 [[NSUserDefaults standardUserDefaults] setObject:tokenId forKey:kuserDefaultTokenId];
                                 
                             }
                         }
                         else
                         {
                             //[self showAlertViewWithTitle:@"Alert" withMessage:@"some error occurred"];
                         }
                     }
                     else
                     {
                         //[super showAlertViewWithTitle:@"Alert" withMessage:@"some error occurred"];
                     }
                 });
             }
                                               failure:^(NSError *error)
             {
                 dispatch_async(dispatch_get_main_queue(), ^(void)
                                {
                                    [activityLoadingView removeFromSuperview];
                                   // [super showAlertViewWithTitle:@"Alert" withMessage:@"some error occurred"];
                                    NSLog(@"Response Dict : %@",[error description]);
                                });
             }];
            
        });
    }
    @catch (NSException *exception)
    {
        NSLog(@"%@", exception.reason);
        [activityLoadingView removeFromSuperview];
    }
    @finally
    {
        NSLog(@"exception finally called");
    }

}

@end
