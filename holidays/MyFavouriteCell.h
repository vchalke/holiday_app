//
//  MyFavouriteCell.h
//  holidays
//
//  Created by ROHIT on 28/12/15.
//  Copyright © 2015 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyFavouriteCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblPackageName;
@property (weak, nonatomic) IBOutlet UILabel *lblpriceList;
@property (weak, nonatomic) IBOutlet UIButton *btnViewDetails;
@property (weak, nonatomic) IBOutlet UIButton *btnFlight;
@property (weak, nonatomic) IBOutlet UIButton *btnVisa;
@property (weak, nonatomic) IBOutlet UIButton *btnSight;
@property (weak, nonatomic) IBOutlet UIButton *btnLunch;
@property (weak, nonatomic) IBOutlet UIButton *btnHotel;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintWidthFlight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintWidthhotel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintWidthFood;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintWidthVisa;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintWidthSight;

- (IBAction)actionForViewDetails:(id)sender;

@end
