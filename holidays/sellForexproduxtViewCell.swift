//
//  sellForexproduxtViewCell.swift
//  holidays
//
//  Created by Komal Katkade on 11/29/17.
//  Copyright © 2017 Mobicule. All rights reserved.
//

import UIKit
protocol  SellForexTableViewCellDelegate: class
{
    func didPressProductNameHelpButton(_ tag: Int)
    func didPressCurrencyHelpButton(_ tag: Int)
    func didPressINRHelpButton(_ tag: Int)
    func textFieldProductNameDidChange(textField: UITextField)
    func textFieldCurrencyDidChange(textField: UITextField)
    func didPressEditButton(_ tag: Int)
    func didPressDeleteProductButton(_ tag: Int)
    func didPressUpdateButton(tag: Int)
}
class sellForexproduxtViewCell: UITableViewCell,UITextFieldDelegate {

    
    @IBOutlet weak var labelProductNoInLargeView: UILabel!
   
    @IBOutlet weak var labelProductNoInEditView: UILabel!
    @IBOutlet weak var labelProductNoInMinimiezedView: UILabel!
    @IBOutlet weak var heightConstraintOfMinimizedView: NSLayoutConstraint!
   
    @IBOutlet weak var updateBtn: UIButton!
    @IBOutlet weak var labelINRAmt: UILabel!
    @IBOutlet weak var heightConstraintOfDeleteButtonView: NSLayoutConstraint!
    @IBOutlet weak var minimizedView: UIView!
    @IBOutlet weak var heightConstraintOfExpandedView: NSLayoutConstraint!
    @IBOutlet weak var textFieldFXINRAmt: UITextField!
    @IBOutlet weak var conversionLabel: UILabel!
    @IBOutlet weak var updateButtonView: UIView!
    @IBOutlet weak var textFieldFXAmt: UITextField!
    @IBOutlet weak var textFieldProduct: UITextField!
    @IBOutlet weak var textFieldCurrency: UITextField!
    @IBOutlet weak var labelFxAmount: UILabel!
    @IBOutlet weak var expandedViewProductView: UIView!
    @IBOutlet weak var minimizedProductName: UILabel!
    @IBOutlet weak var heightConstraintOfconversionView: NSLayoutConstraint!
     weak var cellDelegate: SellForexTableViewCellDelegate?
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
        textFieldCurrency.delegate = self
        textFieldProduct.delegate = self
        
        self.addDoneButtonOnKeyboard()
    }
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle       = UIBarStyle.default
        let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(sellForexproduxtViewCell.doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.textFieldFXAmt.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction()
    {
        self.textFieldFXAmt.resignFirstResponder()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func deleteButtonClicked(_ sender: Any)
    {
          cellDelegate?.didPressDeleteProductButton(textFieldFXAmt.tag)
    }
    
    @IBAction func editButtonClicked(_ sender: Any)
    {
        cellDelegate?.didPressEditButton(textFieldFXAmt.tag)

    }
    @IBAction func updateButtonClicked(_ sender: Any)
    {
        cellDelegate?.didPressUpdateButton(tag: textFieldFXAmt.tag)
        //didPressUpdateButton(tag: editButton.tag, productName: textFieldProduct.text! as NSString, currency:  NSInteger.init(textFieldCurrency.text!), fxAmount: textFieldFXAmt.text, inrAmount: NSInteger.init(textFieldFXINRAmt.text!) )
    }
    @IBAction func productNameHelpBtnClicked(_ sender: Any)
    {
        cellDelegate?.didPressProductNameHelpButton(textFieldFXAmt.tag)
    }
    
    @IBAction func currencyHelpButtonClicked(_ sender: Any)
    {
         cellDelegate?.didPressCurrencyHelpButton(textFieldFXAmt.tag)
    }
    @IBAction func inrAmountHelpBtnClicked(_ sender: Any)
    {
        cellDelegate?.didPressINRHelpButton(textFieldFXAmt.tag)
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        if textField == textFieldProduct
        {
            
            
            cellDelegate?.textFieldProductNameDidChange(textField: textField)
            
            return false
        }
        else if textField == textFieldCurrency
        {
            
            cellDelegate?.textFieldCurrencyDidChange(textField: textField)
            return false
            
        }
        return true
    }

    
}
