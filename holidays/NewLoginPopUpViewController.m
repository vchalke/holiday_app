//
//  NewLoginPopUpViewController.m
//  holidays
//
//  Created by Parshwanath on 24/09/18.
//  Copyright © 2018 Mobicule. All rights reserved.
//

#import "NewLoginPopUpViewController.h"
#import "Thomas_Cook_Holidays-Swift.h"
#import "RegisterUserViewController.h"
#import "Reachability.h"

@interface NewLoginPopUpViewController ()
{
    CGFloat defaultHeight;
    LoadingView *activityLoadingView;
    
}

@end

@implementation NewLoginPopUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    if (self.currentSelectedeScreen != nil && ![self.currentSelectedeScreen isEqualToString:@""])
    {
        [self setUpLoginStackView:self.currentSelectedeScreen];
        
    }else{
        
        [self setUpLoginStackView:@"Login"];
    }
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"login_bg.jpg"]];
    
    
    defaultHeight = self.mainStackView.frame.size.height;
    
    NSLog(@" defualt 1 %f ",defaultHeight);
    
    
    
    [self setUpBorder];
    [self setUp];
    
    /*UIImageView *emailImageView = [[UIImageView alloc]initWithFrame:CGRectMake(10, 0, 25, 25)];
    emailImageView.image =[UIImage imageNamed:@"email"];
    emailImageView.contentMode = UIViewContentModeCenter;
    
    UIView *viewPaddingForEmailID  = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 40, self.emailAddressTxt.frame.size.height)];
    [viewPaddingForEmailID addSubview:emailImageView];
    self.emailAddressTxt.leftView =  emailImageView;
    self.emailAddressTxt.leftViewMode = UITextFieldViewModeAlways;
    [  self.emailAddressTxt layoutIfNeeded];
    
    
    UIImageView *passwordImageView = [[UIImageView alloc]initWithFrame:CGRectMake(10, 0, 25, 25)];
    passwordImageView.image =[UIImage imageNamed:@"password"];
    passwordImageView.contentMode = UIViewContentModeCenter;
    
    UIView *viewPaddingFoPasswordID  = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 40, self.passwordTxt.frame.size.height)];
    [viewPaddingFoPasswordID addSubview:passwordImageView];
    self.passwordTxt.leftView =  viewPaddingFoPasswordID;
    self.passwordTxt.leftViewMode = UITextFieldViewModeAlways;
    
     [  self.passwordTxt layoutIfNeeded];
    
    UIImageView *otpImageView = [[UIImageView alloc]initWithFrame:CGRectMake(10, 0, 25, 25)];
    otpImageView.image =[UIImage imageNamed:@"OTP"];
    otpImageView.contentMode = UIViewContentModeCenter;
    
    UIView *viewPaddingForOTP  = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 40, self.otpTxt.frame.size.height)];
    [viewPaddingForOTP addSubview:otpImageView];
    self.otpTxt.leftView =  viewPaddingForOTP;
    self.otpTxt.leftViewMode = UITextFieldViewModeAlways;
    
     [self.otpTxt layoutIfNeeded];*/
    
 
    
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:true];
    [self.navigationController setNavigationBarHidden:true];
}

-(void)viewWillLayoutSubviews
{
  
    /*CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
      self.view.frame = CGRectMake(0, 0, screenWidth, screenRect.size.height);*/
   
    
    //  self.loginView.frame = CGRectMake(0, 0, screenWidth, self.loginView.frame.size.height);
    //NSLog(@"loginFrame : - %@",NSStringFromCGRect(self.loginView.frame));
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
}

#pragma mark ------- UI  ------
-(void)setUpBorder
{
    self.emailAddressView.layer.borderColor = [[AppUtility hexStringToUIColorWithHex:@"#dcdedf"] CGColor];
    self.emailAddressView.layer.borderWidth = 1.0;
    
   self.otpView.layer.borderColor = [[AppUtility hexStringToUIColorWithHex:@"#dcdedf"] CGColor];
    self.otpView.layer.borderWidth = 1.0;
    
    self.passwordView.layer.borderColor = [[AppUtility hexStringToUIColorWithHex:@"#dcdedf"] CGColor];
    self.passwordView.layer.borderWidth = 1.0;
    
     self.orView.layer.cornerRadius =   self.orView.frame.size.width / 2;
    self.orView.layer.borderColor = [[AppUtility hexStringToUIColorWithHex:@"#dcdedf"] CGColor];
    self.orView.layer.borderWidth = 1.0;
 
}

-(void)setUpDefaultUI
{
    [self.otpImageView setImage:[UIImage imageNamed:@"password"]];
    [self.passwordMainView setHidden:false];
    [self.loginWithView setHidden:false];
    [self.passwordView setHidden:true];
    [self.otpBtn setUserInteractionEnabled:true];
    [self.passwordBtn setUserInteractionEnabled:true];
    [self.otpMainView setHidden:false];
    [self.resendOTPBtn setHidden:true];
    
    [self.login_register_Btn setTitle:@"Login" forState:UIControlStateNormal];
    [self.register_Login_Toggel_Btn setTitle:@"Register" forState:UIControlStateNormal];
    [self.guestUserBtn setTitle:@"Guest User" forState:UIControlStateNormal];
    
    [self.otpBtn setImage:[UIImage imageNamed:@"checkbox"]   forState:UIControlStateNormal];
    [self.passwordBtn setImage:[UIImage imageNamed:@"checkbox_selected"]  forState:UIControlStateNormal];
    
    [self.otpTxt setPlaceholder:@"Enter Password"];
    
 
    
    self.loginStackViewConstantHeight.active = false;
    self.loginStackViewHeight.active = true;
  
     //self.loginStackViewHeight.constant = defaultHeight;
    
  
    
      [self.view layoutSubviews];
   
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark ------- Button Actions ------

- (IBAction)closeBtnClick:(UIButton *)sender {
    
    [self dismissPopupViewControllerWithanimationType];
    
}
- (IBAction)loginBtnClick:(UIButton *)sender {
    
    if ([sender.titleLabel.text caseInsensitiveCompare:@"Send OTP"] == NSOrderedSame || [sender.titleLabel.text caseInsensitiveCompare:@"Resend OTP"] == NSOrderedSame )
    {
        if ([self checkEmailIdValidation])
        {
            [self loginCheckWithType:kSendOtp];
        }
        else
        {
            [self showAlertViewWithTitle:@"Alert" withMessage:@"Enter username"];
            
        }
    }
    
    if ([sender.titleLabel.text caseInsensitiveCompare:@"Login"] == NSOrderedSame )
    {
        if ([self.currentSelectedeScreen caseInsensitiveCompare:@"Login"] == NSOrderedSame || [self.currentSelectedeScreen caseInsensitiveCompare:@"password"] == NSOrderedSame || [self.currentSelectedeScreen caseInsensitiveCompare:@"LoginWithOTP"] == NSOrderedSame)
        {
             if (self.resendOTPBtn.isHidden)
             {
                 // password login
                 
                 if ([self checkEmailIdValidation]  && ![self.otpTxt.text isEqualToString:@""])
                 {
                     [self loginCheckWithType:@"signIn"];
                 }else
                 {
                     NSString *message;
                     if (![self checkEmailIdValidation]) {
                         
                         message = @"Enter username";
                     }else
                     {
                         message = @"Enter password";
                     }
                     
                     [self showAlertViewWithTitle:@"Alert" withMessage:message];
                 }
                 
             }else
             {
                 // otp login
                 if ([self checkEmailIdValidation])
                 {
                     [self loginCheckWithType:kSignInWithOtp];
                 }
                 else
                 {
                     NSString *message;
                     if (![self checkEmailIdValidation]) {
                         
                         message = @"Enter username";
                     }else
                     {
                         message = @"Enter password";
                     }
                     
                     [self showAlertViewWithTitle:@"Alert" withMessage:message];
                     
                 }
             }
            
        }else if ([self.currentSelectedeScreen caseInsensitiveCompare:@"Guest User"] == NSOrderedSame)
        {
             // guest user login
            if ([self checkEmailIdValidation])
            {
                [self loginCheckWithType:@"guestLogin"];
                
            }else
            {
                [self showAlertViewWithTitle:@"Alert" withMessage:@"Enter username"];
                
            }
           
        }
        
    }else if ([sender.titleLabel.text caseInsensitiveCompare:@"Register"] == NSOrderedSame )
    {
        // register new user
       
        if ([self checkEmailIdValidation] && ![self.passwordTxt.text isEqualToString:@""] && ![self.otpTxt.text isEqualToString:@""])
        {
            
            if ([self.passwordTxt.text isEqualToString:self.otpTxt.text]) {
                
                //[self registerUser];
                [self loginCheckWithType:@"signUp"];
                
            }
            else
                [self showAlertViewWithTitle:@"Alert" withMessage:@"Entered password and re-entered password not matched"];
        }
        else
        {
            if ([self.emailAddressTxt.text isEqualToString:@""])
            {
                [self showAlertViewWithTitle:@"Alert" withMessage:@"Enter username"];
            }
            else if ([self.passwordTxt.text isEqualToString:@""] ||[self.otpTxt.text isEqualToString:@""] )
            {
                [self showAlertViewWithTitle:@"Alert" withMessage:@"Enter password"];
            }
        }
        
    }
    
}
- (IBAction)passwordBtnClick:(UIButton *)sender {
    
    //[self setUpLoginStackView:@"password"];
    
       [self setUpDefaultUI];
    
}
- (IBAction)otpBtnClick:(UIButton *)sender {
    
  //  [self setUpDefaultUI];
    
     [self setUpLoginStackView:@"LoginWithOTP"];
    
    if (self.emailAddressTxt.text !=nil && ![self.emailAddressTxt.text  isEqual: @""] )
    {
        [self resnedOTPBtnClick:self.resendOTPBtn];
        
    }
    
 /*   [self.otpBtn setImage:[UIImage imageNamed:@"checkbox_selected"] forState:UIControlStateNormal];
    [self.passwordBtn setImage:[UIImage imageNamed:@"checkbox"] forState:UIControlStateNormal];
    [self.login_register_Btn setTitle:@"Send OTP" forState:UIControlStateNormal];
    
    [self.otpTxt setPlaceholder:@" Enter OTP"];
    
    [self.resendOTPBtn setHidden:false];*/
}

- (IBAction)loginWithFacebook:(UIButton *)sender {
    
    
    
    //New Facebook Login Code
    
    /*
     FBSDKLoginManager *facebookLogin = [[FBSDKLoginManager alloc] init];
     
     [facebookLogin logInWithReadPermissions:@[@"email"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *facebookResult, NSError *facebookError)
     {
     
     if (facebookError) {
     //NSLog(@"Facebook login failed. Error: %@", facebookError);
     
     UIAlertView * calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Unable to retreive emailid from facebook.please enter your email id here..!" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
     [calert show];
     NSLog(@"Process error");
     [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
     loginOBJ.view.frame = CGRectMake(0, 0, 300, 455);
     [self presentPopupViewController:loginOBJ animationType:MJPopupViewAnimationFade];
     
     }
     else if (facebookResult.isCancelled) {
     //NSLog(@"Facebook login got cancelled.");
     
     UIAlertView * calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Unable to retreive emailid from facebook.please enter your email id" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
     [calert show];
     [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
     loginOBJ.view.frame = CGRectMake(0, 0, 300, 455);
     [self presentPopupViewController:loginOBJ animationType:MJPopupViewAnimationFade];
     
     }
     else
     {
     NSString *accessToken = [[FBSDKAccessToken currentAccessToken] tokenString];
     
     [[FIRAuth auth] signInWithCredential:[FIRFacebookAuthProvider credentialWithAccessToken:accessToken] completion:^(FIRUser *user, NSError *error) {
     if (error) {
     //NSLog(@"Login failed. %@", error);
     
     UIAlertView * calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Unable to retreive emailid from facebook.please enter your email id" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
     [calert show];
     [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
     loginOBJ.view.frame = CGRectMake(0, 0, 300, 455);
     [self presentPopupViewController:loginOBJ animationType:MJPopupViewAnimationFade];
     
     } else {
     //NSLog(@"Logged in! %@", user.email);
     
     if (user.email)
     {
     NSUserDefaults *userIdForGooglePlusSignIn=[NSUserDefaults standardUserDefaults];
     [userIdForGooglePlusSignIn setObject:user.email forKey:kLoginEmailId];
     
     [self fetchMobileNumber];
     
     UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Success"
     message:@"User Registerd Successfully"
     preferredStyle: UIAlertControllerStyleAlert];
     
     UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
     
     [alertView dismissViewControllerAnimated:YES completion:nil];
     [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
     if (self.delegate && [self.delegate respondsToSelector:@selector(dismissLoginPopUp)])
     {
     [self.delegate dismissLoginPopUp];
     
     }
     
     }];
     
     [alertView addAction:actionOk];
     
     [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
     loginOBJ.view.frame = CGRectMake(0, 0, 300, 455);
     [self presentPopupViewController:loginOBJ animationType:MJPopupViewAnimationFade];
     [self presentViewController:alertView animated:YES completion:nil];
     self.textFieldUserId.text = user.email;
     [self registerUserForGuest];
     }
     else
     {
     UIAlertView * calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Unable to retreive emailid from facebook.please enter your email id" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
     [calert show];
     [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
     loginOBJ.view.frame = CGRectMake(0, 0, 300, 455);
     [self presentPopupViewController:loginOBJ animationType:MJPopupViewAnimationFade];
     }
     
     }
     }];
     }
     
     }];
     */
    
    //OLD Facebook Login Code
    //
    //    FBSDKLoginManager *loginMgr = [[FBSDKLoginManager alloc] init];
    //
    //    [loginMgr logInWithReadPermission
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    login.loginBehavior = FBSDKLoginBehaviorWeb;
    [login logOut];
    [login
     logInWithReadPermissions: @[@"public_profile",@"email"]
     fromViewController:self
     handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         if (error)
         {
             
             UIAlertView * calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Unable to retreive emailid from facebook.please enter your email id here..!" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
             [calert show];
             NSLog(@"Process error");
//             [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
//             loginOBJ.view.frame = CGRectMake(0, 0, 300, 455);
//             [self presentPopupViewController:loginOBJ animationType:MJPopupViewAnimationFade];
//
             // [self dismissPopupViewControllerWithanimationType];
             
         }
         else if (result.isCancelled)
         {
             NSLog(@"Cancelled");
             UIAlertView * calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Unable to retreive emailid from facebook.please enter your email id" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
             [calert show];
//             [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
//             loginOBJ.view.frame = CGRectMake(0, 0, 300, 455);
//             [self presentPopupViewController:loginOBJ animationType:MJPopupViewAnimationFade];
            //   [self dismissPopupViewControllerWithanimationType];
             
         }
         else {
             
             [self getBasicInfoWithResult:result];
             //             NSLog(@"Logged in");
             //             UIAlertView * calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Unable to retreive emailid from facebook.please enter your email id" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
             //             [calert show];
             //             [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
             //             loginOBJ.view.frame = CGRectMake(0, 0, 300, 455);
             //             [self presentPopupViewController:loginOBJ animationType:MJPopupViewAnimationFade];
         }
         
     }];
    
    
    
}
- (IBAction)loginWithGooglePlus:(UIButton *)sender {
    
    SignInViewController * signVC =   [[SignInViewController alloc] initWithNibName:@"SignInViewController" bundle:nil];
    signVC.googleViewDelegate =  self;
    
    [self.navigationController pushViewController:signVC animated:true];
//    [[SlideNavigationController sharedInstance] presentViewController:signVC animated:true completion:^{
//
//    }];
}
- (IBAction)guestUserBtnClick:(UIButton *)sender {
    
    [self setUpLoginStackView:sender.titleLabel.text];
    
}


- (IBAction)registerBtnClick:(UIButton *)sender {
    
    [self setUpLoginStackView:sender.titleLabel.text];
}


- (IBAction)resnedOTPBtnClick:(UIButton *)sender {
    
    [self loginBtnClick:sender];
}


#pragma mark -------------- Helper methods --------------

-(void)setUpLoginStackView:(NSString *)buttonTitleText
{
    
    if ([buttonTitleText caseInsensitiveCompare:@"Login"] == NSOrderedSame)
    {
       /* [self.passwordMainView setHidden:false];
        [self.otpMainView setHidden:false];
        [self.passwordView setHidden:true];
        [self.loginWithView setHidden:false];
        [self.otpTxt setPlaceholder:@" Enter OTP"];
        
        [self.otpBtn setImage:[UIImage imageNamed:@"checkbox_selected"] forState:UIControlStateNormal];
        [self.passwordBtn setImage:[UIImage imageNamed:@"checkbox"] forState:UIControlStateNormal];
        
        [self.register_Login_Toggel_Btn setTitle:@"Register" forState:UIControlStateNormal];
        [self.login_register_Btn setTitle:@"Login" forState:UIControlStateNormal];
         [self.guestUserBtn setTitle:@"Guest User" forState:UIControlStateNormal];
        [self.resendOTPBtn setHidden:false];*/
        
          [self setUpDefaultUI];
        
        //[self setUpLoginStackView:@"password"];
        [self clearView];
        
    }else if ([buttonTitleText caseInsensitiveCompare:@"Register"] == NSOrderedSame)
    {
        [self.otpImageView setImage:[UIImage imageNamed:@"password"]];
        [self.passwordMainView setHidden:false];
        [self.otpMainView setHidden:false];
        [self.passwordView setHidden:false];
        [self.loginWithView setHidden:true];
        [self.passwordTxt setPlaceholder:@"Enter Password"];
        [self.otpTxt setPlaceholder:@"Confirm Password"];
        [self.otpTxt layoutSubviews];
        [self.login_register_Btn setTitle:@"Register" forState:UIControlStateNormal];
        [self.register_Login_Toggel_Btn setTitle:@"Login" forState:UIControlStateNormal];
        [self.guestUserBtn setTitle:@"Guest User" forState:UIControlStateNormal];
         [self.resendOTPBtn setHidden:true];
        
        
        self.loginStackViewHeight.active = false;
        self.loginStackViewConstantHeight.active = true;
        
        
         [self clearView];
        
    }else if ([buttonTitleText caseInsensitiveCompare:@"Guest User"] == NSOrderedSame)
    {
        [self.passwordMainView setHidden:true];
        [self.otpMainView setHidden:true];
        
      
        self.loginStackViewConstantHeight.constant = (defaultHeight / 3);
        
        [self.login_register_Btn setTitle:@"Login" forState:UIControlStateNormal];
        [self.register_Login_Toggel_Btn setTitle:@"Register" forState:UIControlStateNormal];
        [self.guestUserBtn setTitle:@"Login" forState:UIControlStateNormal];
        [self.resendOTPBtn setHidden:true];
        
        self.loginStackViewHeight.active = false;
        self.loginStackViewConstantHeight.active = true;
        
         [self clearView];

        
    }else if ([buttonTitleText caseInsensitiveCompare:@"LoginWithOTP"] == NSOrderedSame)
    {
        [self.otpImageView setImage:[UIImage imageNamed:@"OTP"]];
         [self.passwordMainView setHidden:false];
         [self.otpMainView setHidden:false];
         [self.passwordView setHidden:true];
         [self.loginWithView setHidden:false];
         [self.otpTxt setPlaceholder:@"Enter OTP"];
         
         [self.otpBtn setImage:[UIImage imageNamed:@"checkbox_selected"] forState:UIControlStateNormal];
         [self.passwordBtn setImage:[UIImage imageNamed:@"checkbox"] forState:UIControlStateNormal];
         
         [self.register_Login_Toggel_Btn setTitle:@"Register" forState:UIControlStateNormal];
         [self.login_register_Btn setTitle:@"Login" forState:UIControlStateNormal];
         [self.guestUserBtn setTitle:@"Guest User" forState:UIControlStateNormal];
         [self.resendOTPBtn setHidden:false];
        
       
        self.loginStackViewConstantHeight.active = false;
         self.loginStackViewHeight.active = true;
        
        
    }else if ([buttonTitleText caseInsensitiveCompare:@"Send OTP"] == NSOrderedSame)
    {
        
        [self.passwordMainView setHidden:false];
        [self.loginWithView setHidden:false];
        [self.passwordView setHidden:true];
        [self.otpBtn setUserInteractionEnabled:true];
        [self.passwordBtn setUserInteractionEnabled:true];
        [self.otpMainView setHidden:true];
        
        [self.login_register_Btn setTitle:@"Send OTP" forState:UIControlStateNormal];
        [self.register_Login_Toggel_Btn setTitle:@"Register" forState:UIControlStateNormal];
        [self.guestUserBtn setTitle:@"Guest User" forState:UIControlStateNormal];
        
        [self.otpBtn setImage:[UIImage imageNamed:@"checkbox_selected"]   forState:UIControlStateNormal];
        [self.passwordBtn setImage:[UIImage imageNamed:@"checkbox"]  forState:UIControlStateNormal];
        
        [self.otpTxt setPlaceholder:@"Enter OTP"];
        
        [self.resendOTPBtn setHidden:true];
        
        self.loginStackViewConstantHeight.active = false;
        self.loginStackViewHeight.active = true;
        
        self.loginStackViewHeight.constant = defaultHeight;
        
        [self.view layoutSubviews];
        
       /* [self.otpBtn setImage:[UIImage imageNamed:@"checkbox"]  forState:UIControlStateNormal];
        [self.passwordBtn setImage:[UIImage imageNamed:@"checkbox_selected"] forState:UIControlStateNormal];
        
        [self.otpTxt setPlaceholder:@" Enter Password"];
        [self.resendOTPBtn setHidden:true];
        [self.login_register_Btn setTitle:@"Login" forState:UIControlStateNormal];
        [self.otpMainView setHidden:false];
        
        self.loginStackViewConstantHeight.active = false;
        self.loginStackViewHeight.active = true;       
       
        
        
        self.loginStackViewHeight.constant = defaultHeight;
    
         [self clearView];*/
    }
    
    
    [self.view layoutSubviews];
    
    self.currentSelectedeScreen = buttonTitleText;
    
    NSLog(@"Height : %f ---  buttonTitleText : %@;",defaultHeight,buttonTitleText);
}

-(void)clearView
{
    self.emailAddressTxt.text = @"";
    self.passwordTxt.text = @"";
    self.otpTxt.text = @"";
}

-(void)setUp
{
    [GIDSignInButton class];
    
    GIDSignIn *signIn = [GIDSignIn sharedInstance];
    signIn.shouldFetchBasicProfile = YES;
    signIn.delegate = self;
    signIn.uiDelegate = self;
}

-(void)setUserDetailsAndClosePopUp
{
    [[NSUserDefaults standardUserDefaults]setValue:self.emailAddressTxt.text forKey:kuserDefaultUserId]
    ;
    [[NSUserDefaults standardUserDefaults]setValue:self.passwordTxt.text forKey:kuserDefaultPassword]
    ;
    
    NSUserDefaults *LoginStatus=[NSUserDefaults standardUserDefaults];
    [LoginStatus setObject:kLoginSuccess  forKey:kLoginStatus];
    
    NSUserDefaults *userIdForGooglePlusSignIn=[NSUserDefaults standardUserDefaults];
    
    [userIdForGooglePlusSignIn setObject:self.emailAddressTxt.text forKey:kLoginEmailId];
    
    //  [[NetCoreSharedManager sharedInstance] setUpIdentity:self.textFieldUserId.text];
    
    //[self fetchMobileNumber];

    [self dismissPopupViewControllerWithanimationType];
    
    //[self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade]; //dissmiss parashwant pop up heare
    
}

- (void)dismissPopupViewControllerWithanimationType
{
    [[SlideNavigationController sharedInstance]dismissViewControllerAnimated:true completion:^{
        
    }];
    
    //[[SlideNavigationController sharedInstance]popViewControllerAnimated:true];
  /*  UIView *sourceView = [self topView];
    UIView *popupView = [sourceView viewWithTag:kMJPopupViewTag];
    UIView *overlayView = [sourceView viewWithTag:kMJOverlayViewTag];
   
    switch (animationType) {
        case MJPopupViewAnimationSlideBottomTop:
        case MJPopupViewAnimationSlideBottomBottom:
        case MJPopupViewAnimationSlideTopTop:
        case MJPopupViewAnimationSlideTopBottom:
        case MJPopupViewAnimationSlideLeftLeft:
        case MJPopupViewAnimationSlideLeftRight:
        case MJPopupViewAnimationSlideRightLeft:
        case MJPopupViewAnimationSlideRightRight:
            [self slideViewOut:popupView sourceView:sourceView overlayView:overlayView withAnimationType:animationType];
            break;
   
        default:
            [self fadeViewOut:popupView sourceView:sourceView overlayView:overlayView];
            break;
    }*/
}

#pragma mark -----  text filed delegated  -------
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([string isEqualToString:@""])
    {
        return YES;
    }
    
    if (textField == self.emailAddressTxt)
    {
        if([self.emailAddressTxt.text length]<=100)
        {
            //        CGRect rect = self.textFieldUserId.rightView.frame;
            //        // only when adding on the end of textfield && it's a space
            //        if (range.location == self.textFieldUserId.text.length && [string isEqualToString:@" "]) {
            //            rect.size.width += 25; // depends on font!
            //        }
            //        else if (string.length == 0) { // backspace
            //            rect.size.width -= 25;
            //            rect.size.width = MAX(0, rect.size.width);
            //        }
            //        else
            //        {
            //            rect.size.width = 0;
            //        }
            //        self.textFieldUserId.rightView.frame = rect;
            
            return YES;
        }
        else
        {
            return NO;
        }
    }
    
    
    if (textField == self.otpTxt)
    {
        if([self.otpTxt.text length]<=25)
        {
            return YES;
        }
        else
        {
            return NO;
        }
        
    }
    
    if (textField == self.passwordTxt)
    {
        if([self.passwordTxt.text length]<=25)
        {
            
            return YES;
        }
        else
        {
            return NO;
        }
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    if (textField == self.emailAddressTxt && !self.resendOTPBtn.isHidden )
    {
        
        [self resnedOTPBtnClick:self.resendOTPBtn];
       
    }
        
   
    return YES;
}



#pragma mark -------------- Login Helper Methods --------------
-(void)loginCheckWithType:(NSString *)signInType
{
    //abc@test.com/
    activityLoadingView = [LoadingView loadingViewInView:[UIApplication sharedApplication].keyWindow
                                              withString:@""
                                       andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
    
   dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
       
       @try
       {
           
           NetworkHelper *helper = [NetworkHelper sharedHelper];
           
           NSString *pathParam = self.emailAddressTxt.text;
           
           NSDictionary *headerDict = [CoreUtility getHeaderDict];
           
           [helper getResponseWithRequestType:@"" withQueryParam:@"" withPathParam:pathParam withJsonParam:nil withHeaders:headerDict withUrl:kAstraUrlLoginCheck success:^(NSDictionary *responseDict)
            {
                NSLog(@"Response Dict : %@",responseDict);
                
                
                dispatch_async(dispatch_get_main_queue(), ^(void)
                               {
                                   [activityLoadingView removeFromSuperview];
                                   if (responseDict)
                                   {
                                       if (responseDict.count>0)
                                       {
                                           if ([signInType isEqualToString:@"signIn"] || [signInType isEqualToString:@"signUp"])
                                           {
                                               [self savePasswordWithType:signInType];
                                               
                                           }else if ([signInType isEqualToString:kSendOtp])
                                           {
                                               [self sendOtprequestWithUserName:self.emailAddressTxt.text andWithLoginType:signInType];
                                               
                                           }else if ([signInType isEqualToString:kSignInWithOtp])
                                           {
                                               [self savePasswordWithType:signInType];
                                              // [self verifyCredentialsWithText1:@"" withText2:@"" withText3:@"" withloginType:signInType withOtp:self.otpTxt.text];
                                           }
                                           else if ([signInType isEqualToString:@"guestLogin"])
                                           {
                                               
                                               [self netCoreDataForLoginWithEmailID:self.emailAddressTxt.text]; //09-04-2018
                                               
                                               UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Success"
                                                                                                                  message:@"User Signed in Successfully"
                                                                                                           preferredStyle: UIAlertControllerStyleAlert];
                                               
                                               
                                               
                                               UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                                                          {
                                                                              
                                                                              [self setUserDetailsAndClosePopUp];
                                                                              
                                                                          }];
                                               
                                               [alertView addAction:actionOk];
                                               
                                               [self presentViewController:alertView animated:YES completion:nil];
                                               
                                               
                                           }
                                           else if([signInType isEqualToString:@"ForgotPassword"])
                                           {
                                               NSString *messege = [responseDict valueForKey:@"message"];
                                               
                                               if ([[messege lowercaseString] isEqualToString:@"false"])
                                               {
                                                   
                                                   UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Alert" message:@"Email Id is not registered with us."                                                                                             preferredStyle: UIAlertControllerStyleAlert];
                                                   
                                                   
                                                   
                                                   UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                                                              {
                                                                                  [self setUserDetailsAndClosePopUp];
                                                                              }];
                                                   
                                                   [alertView addAction:actionOk];
                                                   
                                                   [self presentViewController:alertView animated:YES completion:nil];
                                                   
                                                   
                                               }
                                               else
                                               {
                                                   // [self resetPasswordAstraAPI];
                                               }
                                               
                                               
                                           }
                                       }
                                       
                                   }
                               });
                
            }
                                      failure:^(NSError *error)
            {
                dispatch_async(dispatch_get_main_queue(), ^(void)
                               {
                                   [activityLoadingView removeFromSuperview];
                                   NSLog(@"Response Dict : %@",[error description]);
                               });
                
                
                
            }];
           
       }
       @catch (NSException *exception)
       {
           NSLog(@"%@", exception.reason);
           [activityLoadingView removeFromSuperview];
       }
       @finally
       {
           NSLog(@"exception finally called");
           [activityLoadingView removeFromSuperview];
       }
       
       
   });
    
    
}


-(void)savePasswordWithType:(NSString *)signInType

{
    activityLoadingView = [LoadingView loadingViewInView:self.view.superview.superview
                                              withString:@""
                                       andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
    
    @try
    {
        
        NetworkHelper *helper = [NetworkHelper sharedHelper];
        
        
        
        NSDictionary *headerDict = [CoreUtility getHeaderDict];
        
        [helper getResponseWithRequestType:@"" withQueryParam:@"" withPathParam:@"" withJsonParam:nil withHeaders:headerDict withUrl:kAstraUrlSavePass success:^(NSDictionary *responseDict)
         {
             NSLog(@"Response Dict : %@",responseDict);
             
             
             dispatch_async(dispatch_get_main_queue(), ^(void) {
                 [activityLoadingView removeFromSuperview];
                 if (responseDict)
                 {
                     if (responseDict.count>0)
                     {
                         NSString *keyString = [responseDict valueForKey:@"text"];
                         
                         [self fetchEncryptionPassWithKey:keyString withloginType:signInType];
                         
                     }
                 }
             });
             
         }
                                   failure:^(NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^(void)
                            {
                                [activityLoadingView removeFromSuperview];
                                NSLog(@"Response Dict : %@",[error description]);
                            });
             
             
             
         }];
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"%@", exception.reason);
        [activityLoadingView removeFromSuperview];
    }
    @finally
    {
        NSLog(@"exception finally called");
        [activityLoadingView removeFromSuperview];
    }
    
}

-(void)sendOtprequestWithUserName:(NSString *)userName andWithLoginType:(NSString *)loginType
{
    activityLoadingView = [LoadingView loadingViewInView:self.view.superview.superview
                                              withString:@""
                                       andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
    
    @try
    {
        
        NetworkHelper *helper = [NetworkHelper sharedHelper];
        
        NSString *pathParam = self.emailAddressTxt.text;
        
        NSDictionary *headerDict = [CoreUtility getHeaderDict];
        
        
        [helper getResponseWithRequestType:@"" withQueryParam:@"" withPathParam:pathParam withJsonParam:nil withHeaders:headerDict withUrl:kAstraUrlSendOtp success:^(NSDictionary *responseDict)
         {
             NSLog(@"Response Dict : %@",responseDict);
             
             
             dispatch_async(dispatch_get_main_queue(), ^(void)
                            {
                                [activityLoadingView removeFromSuperview];
                                if (responseDict != nil && ![responseDict isEqual:[NSNull new]])
                                {
                                    NSString * isValidUser = [responseDict objectForKey:@"text"];
                                    
                                    if   (isValidUser != nil && ![isValidUser isEqual:[NSNull new]] && ![isValidUser isEqualToString:@""] && [isValidUser caseInsensitiveCompare:@"true"] == NSOrderedSame)
                                    {
                                        UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Alert" message:@"otp is sent to your registered email"
                                                                                                    preferredStyle: UIAlertControllerStyleAlert];
                                        
                                        UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                                                   {
                                                                       
                                                                       //[self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade]; // 25 sept 2018 //dissmiss parashwant pop up heare
                                                                       //[self dismissPopupViewControllerWithanimationType];
                                                                       
                                                                        [self setUpLoginStackView:@"LoginWithOTP"];
                                                                   }];
                                        
                                        [alertView addAction:actionOk];
                                        
                                        [self presentViewController:alertView animated:YES completion:nil];
                                        
                                        return;
                                        
                                    }else if([[[responseDict valueForKey:@"text"] lowercaseString] isEqualToString:@"blocked"]){
                                        
                                        UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Failure"
                                                                                                                                          message:@"User have been blocked, please contact customer care"
                                                                                                                                   preferredStyle: UIAlertControllerStyleAlert];
                                                                       
                                                                       
                                                                       
                                                                       UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                                                                                  {
                                                                                                      
                                                                                                      //[self setUserDetailsAndClosePopUp];
                                                                                                  }];
                                                                       
                                                                       [alertView addAction:actionOk];
                                                                       
                                                                       [self presentViewController:alertView animated:YES completion:nil];
                                                                       
                                                                       return;
                                    
                                    }
                                    
                                }
                                
                                UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Failure"
                                                                                                   message:@"Some error occured"
                                                                                            preferredStyle: UIAlertControllerStyleAlert];
                                
                                
                                
                                UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                                           {
                                                               
                                                               //[self setUserDetailsAndClosePopUp];
                                                           }];
                                
                                [alertView addAction:actionOk];
                                
                                [self presentViewController:alertView animated:YES completion:nil];
                                
                                return;
                                
                                
                                
                            });
             
             
             
         }
                                   failure:^(NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^(void)
                            {
                                [activityLoadingView removeFromSuperview];
                                NSLog(@"Response Dict : %@",[error description]);
                            });
             
             
             
         }];
    }@catch (NSException *exception)
    {
        NSLog(@"%@", exception.reason);
        [activityLoadingView removeFromSuperview];
    }
    @finally
    {
        NSLog(@"exception finally called");
        [activityLoadingView removeFromSuperview];
    }
    
}


-(void)fetchEncryptionPassWithKey:(NSString *)key withloginType:(NSString *)loginType
{
    activityIndicator = [LoadingView loadingViewInView:self.view withString:@"" andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
    
    
    //            "key": "495kcG744o"
    
   // NSDictionary *dict = [[NSDictionary alloc]initWithObjectsAndKeys:key, @"key",_textFieldPassword.text,@"password",nil]; // 25 sept 2018
    
    NSDictionary *dict = [[NSDictionary alloc]initWithObjectsAndKeys:key, @"key",_otpTxt.text,@"password",nil];
    
    NetworkHelper *helper = [NetworkHelper sharedHelper];
    if([CoreUtility connected])
    {
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            NSString *strResponse = [helper getDataFromServerForType:@"sync" entity:@"encrypt" action:@"generate" andUserJson:dict];
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                [activityIndicator removeFromSuperview];
                if (![strResponse isEqualToString:@"No Internet"])
                {
                    NSString *strStatus = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"Status"];
                    NSString *strMessage = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"message"];
                    
                    if ([[strStatus lowercaseString] isEqualToString:kStatusSuccess])
                    {
                        
                        NSDictionary *dataDict = [[[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"data"] firstObject];
                        
                        if ([loginType isEqualToString:@"signIn"] || [loginType isEqualToString:kSignInWithOtp])
                        {
                            NSString *text1 = [dataDict valueForKey:@"text1"];
                            NSString *text2 = [dataDict valueForKey:@"text2"];
                            NSString *text3 = [dataDict valueForKey:@"text3"];
                            
                            [self verifyCredentialsWithText1:text1 withText2:text2 withText3:text3 withloginType:loginType withOtp:_otpTxt.text];
                            
                        }
                        else
                        {
                            RegisterUserViewController *registerUserVC = [[RegisterUserViewController alloc] initWithNibName:@"RegisterUserViewController" bundle:nil];
                            registerUserVC.dictforEncryption = dataDict;
                            registerUserVC.userId = self.emailAddressTxt.text;
                            registerUserVC.delegate = self;
                            // [self presentViewController:registerUserVC animated:YES completion:nil];
                            
                            [self.navigationController pushViewController:registerUserVC animated:YES];
                            //[[SlideNavigationController sharedInstance] pushViewController:registerUserVC animated:YES];
                        }
                        
                    }
                    else
                    {
                        UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Alert"
                                                                                           message:strMessage
                                                                                    preferredStyle: UIAlertControllerStyleAlert];
                        
                        UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                            
                            [alertView dismissViewControllerAnimated:YES completion:nil];
                            
                        }];
                        [alertView addAction:actionOk];
                        [self presentViewController:alertView animated:YES completion:nil];
                    }
                }
            });
        });
        
    }
    else
    {
        UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Alert"
                                                                           message:kMessageNoInternet
                                                                    preferredStyle: UIAlertControllerStyleAlert];
        
        UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
            [alertView dismissViewControllerAnimated:YES completion:nil];
            
        }];
        
        [alertView addAction:actionOk];
        [self presentViewController:alertView animated:YES completion:nil];
    }
    
}

-(void)verifyCredentialsWithText1:(NSString *)text1 withText2:(NSString *)text2 withText3:(NSString *)text3 withloginType:(NSString *)loginType withOtp:(NSString*)otp
{
    
    activityLoadingView = [LoadingView loadingViewInView:self.view.superview.superview
                                              withString:@""
                                       andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
    
    @try
    {
        
        NetworkHelper *helper = [NetworkHelper sharedHelper];
        
        
        
        NSMutableDictionary *dictForRequest = [[NSMutableDictionary alloc] init];
        [dictForRequest setObject:_emailAddressTxt.text forKey:@"userId"];
        [dictForRequest setObject:text1 forKey:@"text1"];
        [dictForRequest setObject:text2 forKey:@"text2"];
        [dictForRequest setObject:text3 forKey:@"text3"];
        
        if ([loginType caseInsensitiveCompare:kSignInWithOtp] == NSOrderedSame)
        {
            [dictForRequest setObject:otp forKey:@"otp"];
            
        }
        
        NSDictionary *headerDict = [CoreUtility getHeaderDict];
        
        [helper getResponseWithRequestType:@"POST" withQueryParam:@"" withPathParam:@"" withJsonParam:dictForRequest withHeaders:headerDict withUrl:kAstraVerifyCredentials success:^(NSDictionary *responseDict)
         {
             NSLog(@"Response Dict : %@",responseDict);
             
             
             dispatch_async(dispatch_get_main_queue(), ^(void) {
                 [activityLoadingView removeFromSuperview];
                 if (responseDict)
                 {
                     if (responseDict.count>0)
                     {
                         if ([[[responseDict valueForKey:@"message"] lowercaseString] isEqualToString:@"false"])
                         {
                             NSString *reasonOfMessage = [responseDict valueForKey:@"reasonOfMessage"];
                             
                             UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Alert"
                                                                                                message:reasonOfMessage
                                                                                         preferredStyle: UIAlertControllerStyleAlert];
                             
                             UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                                 
                                 [alertView dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
                             [alertView addAction:actionOk];
                             [self presentViewController:alertView animated:YES completion:nil];
                             
                         }
                         else
                         {
                             
                             [self netCoreDataForLoginWithEmailID:self.emailAddressTxt.text]; //09-04-2018
                             
                             NSDictionary * userDetailsDict = [responseDict valueForKey:@"userDetail"]; //09-04-2018 --
                             
                             if( userDetailsDict && ![userDetailsDict isEqual:[NSNull new]]) //09-04-2018 --
                             {
                                 NSString * mobileNoString =[userDetailsDict valueForKey:@"mobileNo"];
                                 
                                 if (nil != mobileNoString && ![mobileNoString isEqual:[NSNull new]] && ![mobileNoString isEqualToString:@""])
                                 {
                                     
                                     [[NSUserDefaults standardUserDefaults]setValue:mobileNoString forKey:kuserDefaultTC_MobileNumber]; //09-04-2018 --
                                     
                                     [[NSUserDefaults standardUserDefaults] synchronize];
                                     
                                     [[NetCoreSharedManager sharedInstance] setUpIdentity:mobileNoString]; //24-04-2018 --
                                 }
                                 
                             }
                             
                             
                             
                             
                             UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Alert" message:@"Login successful" preferredStyle: UIAlertControllerStyleAlert];
                             
                             UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                                        {
                                                            [self setUserDetailsAndClosePopUp];
                                                            
                                                            
                                                            
                                                            [alertView dismissViewControllerAnimated:YES completion:nil];
                                                            
                                                        }];
                             [alertView addAction:actionOk];
                             [self presentViewController:alertView animated:YES completion:nil];
                             
                         }
                     }
                 }
             });
             
         }
                                   failure:^(NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^(void)
                            {
                                [activityLoadingView removeFromSuperview];
                                NSLog(@"Response Dict : %@",[error description]);
                            });
             
             
             
         }];
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"%@", exception.reason);
        [activityLoadingView removeFromSuperview];
    }
    @finally
    {
        NSLog(@"exception finally called");
        [activityLoadingView removeFromSuperview];
    }
}

#pragma mark --------- Validations ------------
-(BOOL) checkEmailIdValidation
{
    if ([self.emailAddressTxt.text isEqualToString:@""])
    {
        [self showAlertViewWithTitle:@"Alert" withMessage:@"please enter a emailId"];
        return NO;
    }
    else if(![self validateEmailWithString])
    {
        [self showAlertViewWithTitle:@"Alert" withMessage:@"please enter a valid emailId"];
        return NO;
    }
    return YES;
}

- (BOOL)validateEmailWithString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:self.emailAddressTxt.text];
    
}

#pragma mark ------------ Alert ----------

-(void)showAlertViewWithTitle:(NSString *)title withMessage:(NSString *)message
{
    UIAlertController *alertView = [UIAlertController alertControllerWithTitle:title
                                                                       message:message
                                                                preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [alertView dismissViewControllerAnimated:YES completion:nil];
        
    }];
    
    [alertView addAction:okAction];
    
    [self presentViewController:alertView animated:YES completion:nil];
}

#pragma mark ------------ NET Core ----------------
-(void)netCoreDataForLoginWithEmailID:(NSString *)emailID
{
    
    NSMutableDictionary *payloadList =  [NSMutableDictionary dictionary];
    [payloadList setObject:emailID forKey:@"EMAIL"];
    
    [NetCoreAnalyticsVC getNetCorePushNotificationDetailDictionary:payloadList withPayloadCount:102];
}

#pragma mark ------- Sign UP -----------
-(void)dismissWithSuccessFullRegistration
{
    
    //    UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Success"
    //                                                                       message:@"User Registered Successfully"
    //                                                                preferredStyle: UIAlertControllerStyleAlert];
    //
    //
    //
    //    UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
    //    {
    
    [self netCoreDataForLoginWithEmailID:self.emailAddressTxt.text]; //09-04-2018 --
    [self setUserDetailsAndClosePopUp];
    
    //}];
    
    //    [alertView addAction:actionOk];
    //
    //    [self presentViewController:alertView animated:YES completion:nil];
    
    
    //  [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
}


#pragma mark ------------- Google login delegates --------
-(void)didFinishWithGoogleLogin:(NSString * )userEmailID
{
    NSLog(@"Signed in user");
    
    
    self.userEmailID = userEmailID;
    
    [self checkedForGoogleLogin];
    
    NSLog(@"");
    
}


-(void)checkedForGoogleLogin
{
    
    if (self.userEmailID == nil || [self.userEmailID isEqualToString:@""])
    {
        
        
    }
    else if ([self.userEmailID isEqualToString:@"error"])
    {
        UIAlertView * calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Unable to retreive emailid from Google plus.please enter your email id here..!" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [calert show];
        NSLog(@"Process error");
       /* [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
        loginOBJ.view.frame = CGRectMake(0, 0, 300, 455);
        [self presentPopupViewController:loginOBJ animationType:MJPopupViewAnimationFade];*/  // 25 sept 2018
    }
    else
    {
        NSUserDefaults *userIdForGooglePlusSignIn=[NSUserDefaults standardUserDefaults];
        
        [userIdForGooglePlusSignIn setObject:self.userEmailID forKey:kLoginEmailId];
        
        // [self fetchMobileNumber];
        
        //        UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Success"
        //                                                                           message:[NSString stringWithFormat:@"User Signed in Successfully with %@",self.userEmailID]
        //                                                                    preferredStyle: UIAlertControllerStyleAlert];
        //
        //        UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
        //        {
        //
        //            [alertView dismissViewControllerAnimated:YES completion:nil];
        //            [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
        //            if (self.delegate && [self.delegate respondsToSelector:@selector(dismissLoginPopUp)])
        //            {
        //                [self.delegate dismissLoginPopUp];
        //
        //            }
        //        }];
        //
        //        [alertView addAction:actionOk];
        
        //  [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
        
        // loginOBJ.view.frame = CGRectMake(0, 0, 300, 455);
        
        //  [self presentPopupViewController:loginOBJ animationType:MJPopupViewAnimationFade];
        
        //  [self presentViewController:alertView animated:YES completion:nil];
        
        self.emailAddressTxt.text = self.userEmailID;
        
        GIDGoogleUser * googleUser = [[GIDSignIn sharedInstance] currentUser];
        
        NSString *emailId = googleUser.profile.email;
        
        NSString *name = googleUser.profile.name;
        
        //        NSString *givenName = googleUser.profile.givenName;
        //
        //        NSString *familyName = googleUser.profile.familyName;
        
        // NSString *imageURl = googleUser.profile.familyName;
        
        
        /*
         @property(nonatomic, readonly) NSString *email;
         
         // The Google user's full name.
         @property(nonatomic, readonly) NSString *name;
         
         // The Google user's given name.
         @property(nonatomic, readonly) NSString *givenName;
         
         // The Google user's family name.
         @property(nonatomic, readonly) NSString *familyName;
         
         // Whether or not the user has profile image.
         @property(nonatomic, readonly) BOOL hasImage;
         
         */
        
        
        
        [self socialLoginWithFirstName:name withLastName:name withImageURl:@"" withType:@"GP" withUserID:emailId withID:@"327645202876-3pmqts5pkaaqqvhh1gjh52k7sbuv468a.apps.googleusercontent.com"];
        
        //[self registerUserForGuest];socialLoginWithFirstName
        
        
    }
    
    [[SlideNavigationController sharedInstance] popViewControllerAnimated:YES];
}



- (void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user withError:(NSError *)error {
    // Perform any operations on signed in user here.
    
    
    NSLog(@"Signed in user");
    
    if (error)
    {
        UIAlertView * calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Unable to retreive emailid from Google plus.please enter your email id here..!" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [calert show];
        NSLog(@"Process error");
      /*  [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
        loginOBJ.view.frame = CGRectMake(0, 0, 300, 455);
        [self presentPopupViewController:loginOBJ animationType:MJPopupViewAnimationFade];
        NSLog(@"%@",error);*/ //-- 28 sept 2018
    }
    else
    {
        
        GIDAuthentication *authentication = user.authentication;
        FIRAuthCredential *credential =
        [FIRGoogleAuthProvider credentialWithIDToken:authentication.idToken
                                         accessToken:authentication.accessToken];
        
        [[FIRAuth auth] signInWithCredential:credential
                                  completion:^(FIRUser *user, NSError *error) {
                                      NSLog(@"Authentication Done");
                                      
                                      
                                      
                                  }];
        
        NSLog(@"email : %@",[GIDSignIn sharedInstance].currentUser.profile.email);
        
        if(DEBUG_ENABLED)
        {
            
            debug(@"email : %@",[GIDSignIn sharedInstance].currentUser.profile.email);
        }
        
        
        
        
        if ([GIDSignIn sharedInstance].currentUser.profile.email)
        {
            NSUserDefaults *userIdForGooglePlusSignIn=[NSUserDefaults standardUserDefaults];
            [userIdForGooglePlusSignIn setObject:[GIDSignIn sharedInstance].currentUser.profile.email forKey:kLoginEmailId];
            
            [self fetchMobileNumber];
            
            UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Success"
                                                                               message:@"User Signed in Successfully"
                                                                        preferredStyle: UIAlertControllerStyleAlert];
            
            UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                
                [alertView dismissViewControllerAnimated:YES completion:nil];
                //[self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade]; 25 sept 2018
                [self dismissPopupViewControllerWithanimationType];
               /* if (self.delegate && [self.delegate respondsToSelector:@selector(dismissLoginPopUp)])
                {
                    [self.delegate dismissLoginPopUp];
                    
                }*/ //25 sept 2018
                
            }];
            
            [alertView addAction:actionOk];
            
            //[self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade]; 25 sept 2018
            [self dismissPopupViewControllerWithanimationType];
          //  loginOBJ.view.frame = CGRectMake(0, 0, 300, 455); 25 sept 2018
            
             //[self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade]; 25 sept 2018
            [self presentViewController:alertView animated:YES completion:nil];
              [self dismissPopupViewControllerWithanimationType];
            self.emailAddressTxt.text=[GIDSignIn sharedInstance].currentUser.profile.email;
            // [self registerUserForGuest];
        }
        
        
        NSString *userId = user.userID;                  // For client-side use only!
        NSString *idToken = user.authentication.idToken; // Safe to send to the server
        NSString *name = user.profile.name;
        NSString *email = user.profile.email;
        NSLog(@"Customer details: %@ %@ %@ %@", userId, idToken, name, email);
    }
    
    
    
}

#pragma mark ------------ Social Login ------------
-(void)socialLoginWithFirstName:(NSString *)firstName withLastName:(NSString *)lastName withImageURl:(NSString *)imageURL withType:(NSString *)type withUserID:(NSString *)userID withID:(NSString *)Id
{
    activityLoadingView = [LoadingView loadingViewInView:self.view.superview.superview
                                              withString:@""
                                       andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
    
    @try
    {
        
        NetworkHelper *helper = [NetworkHelper sharedHelper];
        NSMutableDictionary *dictForRequest = [[NSMutableDictionary alloc] init];
        [dictForRequest setObject:firstName forKey:@"firstName"];
        [dictForRequest setObject:Id forKey:@"id"];
        [dictForRequest setObject:imageURL forKey:@"imgUrl"];
        [dictForRequest setObject:lastName forKey:@"lastName"];
        [dictForRequest setObject:type forKey:@"type"];
        [dictForRequest setObject:userID forKey:@"userId"];
        
        NSDictionary *headerDict = [CoreUtility getHeaderDict];
        
        [helper getResponseWithRequestType:@"POST" withQueryParam:@"" withPathParam:@"" withJsonParam:dictForRequest withHeaders:headerDict withUrl:kAstraSocialLogin success:^(NSDictionary *responseDict)
         {
             NSLog(@"Response Dict : %@",responseDict);
             
             
             dispatch_async(dispatch_get_main_queue(), ^(void) {
                 [activityLoadingView removeFromSuperview];
                 if (responseDict)
                 {
                     if (responseDict.count>0)
                     {
                         
                         NSDictionary * userDetailsDict = [responseDict valueForKey:@"userDetail"]; //09-04-2018 --
                         
                         if( userDetailsDict && ![userDetailsDict isEqual:[NSNull new]]) //09-04-2018 --
                         {
                             NSString * mobileNoString =[userDetailsDict valueForKey:@"mobileNo"];
                             
                             if (nil != mobileNoString && ![mobileNoString isEqual:[NSNull new]] && ![mobileNoString  isEqualToString:@""])
                             {
                                 
                                 [[NSUserDefaults standardUserDefaults]setValue:mobileNoString forKey:kuserDefaultTC_MobileNumber]; //09-04-2018 --
                                 
                                 [[NSUserDefaults standardUserDefaults] synchronize];
                                 
                                 [[NetCoreSharedManager sharedInstance] setUpIdentity:mobileNoString];//24-04-2018 --
                             }
                         }
                         
                         
                         
                         UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Success"
                                                                                            message:@"User Signed in Successfully"
                                                                                     preferredStyle: UIAlertControllerStyleAlert];
                         
                         [self netCoreDataForLoginWithEmailID:self.emailAddressTxt.text]; //09-04-2018
                         
                         UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                                    {
                                                        [self setUserDetailsAndClosePopUp];
                                                    }];
                         
                         [alertView addAction:actionOk];
                         
                         [self presentViewController:alertView animated:YES completion:nil];
                         
                     }
                 }
                 else
                 {
                     UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Failure"
                                                                                        message:@"Some error occured"
                                                                                 preferredStyle: UIAlertControllerStyleAlert];
                     
                     
                     
                     UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                                {
                                                    
                                                    //[self setUserDetailsAndClosePopUp];
                                                }];
                     
                     [alertView addAction:actionOk];
                     
                     [self presentViewController:alertView animated:YES completion:nil];
                 }
             });
             
         }
                                   failure:^(NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^(void)
                            {
                                [activityLoadingView removeFromSuperview];
                                
                                UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Failure"
                                                                                                   message:[error description]
                                                                                            preferredStyle: UIAlertControllerStyleAlert];
                                
                                
                                
                                UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                                           {
                                                               
                                                               //[self setUserDetailsAndClosePopUp];
                                                           }];
                                
                                [alertView addAction:actionOk];
                                
                                [self presentViewController:alertView animated:YES completion:nil];
                                
                                NSLog(@"Response Dict : %@",[error description]);
                            });
             
             
             
         }];
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"%@", exception.reason);
        [activityLoadingView removeFromSuperview];
    }
    @finally
    {
        NSLog(@"exception finally called");
        [activityLoadingView removeFromSuperview];
    }
}

#pragma mark - Autopopulate Phone no

-(void)fetchMobileNumber
{
    NSUserDefaults *userIdForGooglePlusSignIn=[NSUserDefaults standardUserDefaults];
    
    NSString *userId=[userIdForGooglePlusSignIn valueForKey:kLoginEmailId];
    
    
    NSDictionary *dictOfData = [NSDictionary dictionaryWithObjectsAndKeys:userId,@"userName",nil];
    
    activityIndicator = [LoadingView loadingViewInView:self.view withString:@"" andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
    
    if ([self connected])
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            NetworkHelper *helper = [NetworkHelper sharedHelper];
            
            NSString *strResponse = [helper getDataFromServerForType:@"webservice" entity:@"myAccountInfo" action:@"search" andUserJson:dictOfData];
            
            NSString *strStatus = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"status"];
            NSDictionary *dictOfData=[[NSDictionary alloc]init];
            
            if ([strStatus isEqualToString:kStatusSuccess])
            {
                dictOfData= [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:kServerResponseKeyData];
                
                NSString *phoneNo = [dictOfData valueForKeyPath:@"profileDetails.phoneNo"];
                
                [userIdForGooglePlusSignIn setObject:phoneNo forKey:kPhoneNo];
                
                if (phoneNo)
                {
                    CoreUtility *coreobj=[CoreUtility sharedclassname];
                    
                    [coreobj.strPhoneNumber addObject:phoneNo];
                    
                    if( nil != phoneNo && ![phoneNo isEqual:[NSNull new]] && ![phoneNo isEqualToString:@""])
                    {
                        
                        [[NSUserDefaults standardUserDefaults]setValue:phoneNo  forKey:kuserDefaultTC_MobileNumber]; //09-04-2018 --
                        [[NSUserDefaults standardUserDefaults]synchronize];
                        
                        [[NetCoreSharedManager sharedInstance] setUpIdentity:phoneNo];
                    }
                    
                }
                
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [activityIndicator removeView];
                });
                
            }
            else
            {
                
            }
        });
    }
    else
    {
        [activityIndicator removeView];
    }
}

- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}

#pragma mark --------------- Facebook -----------------------
-(void)getBasicInfoWithResult:(FBSDKLoginManagerLoginResult *)result
{
    // loginOBJ.view.frame = CGRectMake(0, 0, 300, 455);
    
    if ([result.grantedPermissions containsObject:@"email"])
    {
        if ([FBSDKAccessToken currentAccessToken])
        {
            NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
            // [parameters setValue:@"name,picture.type(large)" forKey:@"fields"];
            [parameters setValue:@"email" forKey:@"fields"];//public_profile
            //[parameters setValue:@"public_profile" forKey:@"fields"];
            
            [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:parameters]
             startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error)
             {
                 if (!error)
                 {
                     NSLog(@"fetched user:%@", result);
                     //  NSString *name = [[result valueForKey:@"name"] lowercaseString];
                     // NSString *username = [name stringByReplacingOccurrencesOfString:@" " withString:@""];
                     // NSArray *picture_arr = [result objectForKey:@"picture"];
                     //    NSArray *data_arr = [picture_arr valueForKey:@"data"];
                     NSString *email = [result valueForKey:@"email"];
                     //  NSString *fbID = [result valueForKey:@"id"];
                     
                     if (email)
                     {
                         
                         NSUserDefaults *userIdForGooglePlusSignIn=[NSUserDefaults standardUserDefaults];
                         [userIdForGooglePlusSignIn setObject:email forKey:kLoginEmailId];
                         
                         
                         //[self fetchMobileNumber];
                         
                         //                         UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Success"
                         //                                                                                            message:@"User Signed in Successfully"
                         //                                                                                     preferredStyle: UIAlertControllerStyleAlert];
                         //
                         //                         UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                         //
                         //                             [alertView dismissViewControllerAnimated:YES completion:nil];
                         //                             [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
                         //
                         //                             if (self.delegate && [self.delegate respondsToSelector:@selector(dismissLoginPopUp)])
                         //                             {
                         //                                 [self.delegate dismissLoginPopUp];
                         //
                         //                             }
                         //
                         //                         }];
                         //
                         //                         [alertView addAction:actionOk];
                         //
                         //                         [self presentViewController:alertView animated:YES completion:nil];
                         
                         self.emailAddressTxt.text = [NSString stringWithFormat:@"%@", email];
                         
                         [self socialLoginWithFirstName:@"" withLastName:@"" withImageURl:@"" withType:@"FB" withUserID:email withID:@"fb1021596341229809"];
                     }
                     else
                     {
                         UIAlertView * calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Unable to retreive emailid from facebook please enter your email id" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
                         [calert show];
//                         [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
//                         loginOBJ.view.frame = CGRectMake(0, 0, 300, 455);
//                         [self presentPopupViewController:loginOBJ animationType:MJPopupViewAnimationFade];
                         
                        // [self dismissPopupViewControllerWithanimationType];
                     }
                 }
                 else
                 {
                     NSLog(@"Unable to Login");
                 }
                 
             }];
        }
    }
}

@end
