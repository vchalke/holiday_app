//
//  CalenderScreenVC.m
//  holidays
//
//  Created by Kush_Tech on 23/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "CalenderScreenVC.h"
#import "CalenderCollectionViewCell.h"
#import "CollectionReusableView.h"
#import "DateStringSingleton.h"
#import "CalenderObject.h"
#import "RoomsDataModel.h"


@interface CalenderScreenVC ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
{
    NSArray *weekArray;
    NSMutableArray *getMainDateArray;
    NSMutableDictionary *getMainDateDict;
    NSInteger currentmonthIndex;
    CalenderObject *mainCalenderObject;
    NSDate *selectedDateInView;
    NSArray *jsonArray;
    NSString *noOfPacks;
    int tabSelected;
    NSArray *arrayForRows;
    NSDictionary *selectedDict;
}
@end

@implementation CalenderScreenVC

- (void)viewDidLoad {
//    [super viewDidLoad];
    
    
    
    selectedDict = [[NSDictionary alloc]init];
    selectedDateInView = [NSDate date];
    weekArray = [NSArray arrayWithObjects:@"MON",@"TUE",@"WED",@"THUS",@"FRI",@"SAT", @"SUN",nil];
    getMainDateArray = [[NSMutableArray alloc] init];
    getMainDateDict = [[NSMutableDictionary alloc] init];
    [self.date_collectionView registerNib:[UINib nibWithNibName:@"CalenderCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"CalenderCollectionViewCell"];
    [self.date_collectionView registerNib:[UINib nibWithNibName:@"CollectionReusableView" bundle:nil]
    forSupplementaryViewOfKind:UICollectionElementKindSectionHeader
           withReuseIdentifier:@"CollectionReusableView"];
    UICollectionViewFlowLayout *collectionViewFlowLayout = [[UICollectionViewFlowLayout alloc] init];
    collectionViewFlowLayout.headerReferenceSize = CGSizeMake(self.view.frame.size.width, 100);
    self.date_collectionView.collectionViewLayout = collectionViewFlowLayout;
    
    mainCalenderObject = [[CalenderObject alloc]initWithCalenderObjectDict:self.dataDict];
    //    currentmonthIndex = [[DateStringSingleton shared]getCurrentMonth]+1; // Next Month Index
        currentmonthIndex = [[DateStringSingleton shared]getMonthNumberFromDate:[[DateStringSingleton shared]getDateFromStringFormat:@"dd-MM-yyyy" strDateValue:mainCalenderObject.calendarStartDate]];
        [self setMonthArrayFor:currentmonthIndex];
    
    NSLog(@"%@",getMainDateArray);
    NSLog(@"dataDict %@",self.dataDict);
    NSLog(@"arrayTravellerCalculation %@",self.arrayTravellerCalculation);
    NSLog(@"selectedHubDict %@",self.selectedHubDict);
    NSLog(@"selectedStateDict %@",self.selectedStateDict);
    
    NSLog(@"holidayPackageDetailInCalender %@",self.holidayPackageDetailInCalender);
    [self makeGroupArray];
}

-(void)setMonthArrayFor:(NSInteger)monthIndex{
    NSInteger startDateMonth = [[DateStringSingleton shared]getMonthNumberFromDate:[[DateStringSingleton shared]getDateFromStringFormat:@"dd-MM-yyyy" strDateValue:mainCalenderObject.calendarStartDate]];
    NSInteger endDateMonth = [[DateStringSingleton shared]getMonthNumberFromDate:[[DateStringSingleton shared]getDateFromStringFormat:@"dd-MM-yyyy" strDateValue:mainCalenderObject.calendarEndDate]];
//    for (int j=(int)monthIndex; j < (int)monthIndex+4 ; j++){
    for (int j=(int)startDateMonth; j <= (int)endDateMonth ; j++){
        NSMutableArray *array = [[NSMutableArray alloc]init];
    for (int i=1; i <= [[DateStringSingleton shared]getTotalDaysInMonthAtIndex:j]; i++){
        NSString *weekAtDate = [[DateStringSingleton shared]getDateTimeStringFormat:@"EE" DateValue:[[DateStringSingleton shared]getDayNumber:1 OfMonth:j]];
        if (i==1){
            for (NSString *string in weekArray){
                if ([string isEqualToString:[weekAtDate uppercaseString]]){
                    [array addObject:[[DateStringSingleton shared]getDayNumber:i OfMonth:j]];
                    break;
                }else{
                    [array addObject:[NSDate date]];
                }
            }
        }else{
            [array addObject:[[DateStringSingleton shared]getDayNumber:i OfMonth:j]];
        }
    }
    [getMainDateArray addObject:array];
    }
}
-(void)makeGroupArray
{
    if (jsonArray.count == 0)
    {
       // NSString *filePath = [[NSBundle mainBundle] pathForResource:@"CalendarResponse" ofType:@"json"];
        
       // NSString *filePath = [[NSBundle mainBundle] pathForResource:@"FareCalender" ofType:@"json"];
       // NSData *data = [NSData dataWithContentsOfFile:filePath];
        
       // NSDictionary *calenderDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        
        
        BOOL isLT = YES;
        
        NSDictionary *calenderDict = self.dataDict;
        
        NSDictionary *ltResponseBeanDict = [calenderDict valueForKey:@"ltResponseBean"];
        
        if (ltResponseBeanDict == nil || ltResponseBeanDict.count == 0)
        {
            ltResponseBeanDict = [calenderDict valueForKey:@"dbResponseBean"];
            
            isLT = NO;
        }
        
        NSArray *bookableArray = [ltResponseBeanDict valueForKey:@"bookable"];
        
        NSArray *onRequestArray = [ltResponseBeanDict valueForKey:@"onRequest"];
        
        /*
         "AVL_INV":"0",
         "DATE":"01-03-2018",
         "DR_PRICE":477500,
         "DR_STRIKEOUT":487500,
         "INVENTORY":"INVA",
         "LAST_SELL_DAY":"35",
         "LT_PROD_CODE":"TCASTRA010318",
         "NO_OF_SLAB_SEAT":"95",
         "PROD_ITIN_CODE":"2017TCASTRAS1"
         */
        
     
        
        
        if (!isLT)
        {
            if (bookableArray != nil && bookableArray.count != 0)
            {
                NSMutableArray *arrayFordb = [[NSMutableArray alloc] init];
                
                for (int i = 0; i<bookableArray.count; i++)
                {
                    NSDictionary *bookableDict = [bookableArray objectAtIndex:i];
                    
                    NSMutableDictionary *dictBookable = [[NSMutableDictionary alloc] init];
                    [dictBookable setObject:@"" forKey:@"AVL_INV"];
                    [dictBookable setObject:[bookableDict valueForKey:@"date"] forKey:@"DATE"];
                    [dictBookable setObject:[bookableDict valueForKey:@"price"] forKey:@"DR_PRICE"];
                    [dictBookable setObject:@"date" forKey:@"DR_STRIKEOUT"];
                    [dictBookable setObject:@"INVA" forKey:@"INVENTORY"];
                    [dictBookable setObject:@"" forKey:@"LAST_SELL_DAY"];
                    [dictBookable setObject:@"" forKey:@"LT_PROD_CODE"];
                    [dictBookable setObject:@"" forKey:@"NO_OF_SLAB_SEAT"];
                    [dictBookable setObject:@"" forKey:@"PROD_ITIN_CODE"];
                    [arrayFordb addObject:dictBookable];
                }
                
                bookableArray = arrayFordb;
            }
        }
        
        if (!isLT)
        {
            if (onRequestArray != nil && onRequestArray.count != 0)
            {
                NSMutableArray *arrayFordb = [[NSMutableArray alloc] init];
                
                for (int i = 0; i<onRequestArray.count; i++)
                {
                    NSDictionary *onReqDict = [onRequestArray objectAtIndex:i];
                    NSMutableDictionary *dictBookable = [[NSMutableDictionary alloc] init];
                    [dictBookable setObject:@"" forKey:@"AVL_INV"];
                    [dictBookable setObject:[onReqDict valueForKey:@"date"] forKey:@"DATE"];
                    [dictBookable setObject:[onReqDict valueForKey:@"price"] forKey:@"DR_PRICE"];
                    [dictBookable setObject:@"" forKey:@"DR_STRIKEOUT"];
                    [dictBookable setObject:@"ONREQA" forKey:@"INVENTORY"];
                    [dictBookable setObject:@"" forKey:@"LAST_SELL_DAY"];
                    [dictBookable setObject:@"" forKey:@"LT_PROD_CODE"];
                    [dictBookable setObject:@"" forKey:@"NO_OF_SLAB_SEAT"];
                    [dictBookable setObject:@"" forKey:@"PROD_ITIN_CODE"];
                    [arrayFordb addObject:dictBookable];
                }
                
                onRequestArray = arrayFordb;
            }
        }

        NSMutableArray *arrayForDayList = [[NSMutableArray alloc] initWithArray:bookableArray];
        [arrayForDayList addObjectsFromArray:onRequestArray];
        
        
        jsonArray = [[NSArray alloc] initWithArray:arrayForDayList];
        
        NSLog(@"%@",calenderDict);
       // jsonArray = [self.dataDict valueForKey:@"dayList"];
    }
    
    int noOfAdults = 0;
    int noOfChilds = 0;
    for (int i = 0; i<self.roomRecordArray.count; i++)
    {
        RoomsDataModel *dataModel = self.roomRecordArray[i];
        noOfAdults = noOfAdults + dataModel.adultCount;
        noOfChilds = noOfChilds + (int)dataModel.arrayChildrensData.count;
    }
    noOfPacks = [NSString stringWithFormat:@"%d",noOfAdults+noOfChilds];

    BOOL isFIT = NO;
    if([[self.holidayPackageDetailInCalender.strPackageSubType lowercaseString] isEqualToString:@"fit"])
    {
        isFIT = YES;
    }
    
    NSArray *arrayTotalDataWithoutDrFilter =[[NSArray alloc]initWithArray:jsonArray];
    
    
    NSPredicate *predicateString = [NSPredicate predicateWithFormat:@"%K > %d", @"DR_PRICE", 0];
    //predicateWithFormat:@"userID == %d", [stdUserNumber intValue]];

    
     NSArray *arrayTotalData = [NSMutableArray arrayWithArray:[arrayTotalDataWithoutDrFilter filteredArrayUsingPredicate:predicateString]];
    
       jsonArray = [[NSArray alloc] initWithArray:arrayTotalData];
    
    if (arrayTotalData.count != 0)
    {
        
        if (tabSelected == 3)
        {
            arrayTotalData = [[NSArray alloc]initWithArray:jsonArray];
        }
        else if (tabSelected == 1)
        {
            NSPredicate *predicateString;
            NSMutableArray *filteredArray =[[NSMutableArray alloc]init] ;
            
//            if (isFIT)
//            {
                predicateString = [NSPredicate predicateWithFormat:@"%K == %@", @"INVENTORY", @"INVA"];
                
                filteredArray = [NSMutableArray arrayWithArray:[arrayTotalData filteredArrayUsingPredicate:predicateString]];
//            }
//            else
//            {
//                    for (int  i = 0 ; i< arrayTotalData.count;i++)
//                    {
//                        NSDictionary *dict  = arrayTotalData[i];
//                        if ([[dict valueForKey:@"AVL_INV"] intValue] >= [noOfPacks intValue])
//                        {
//                            [filteredArray addObject:dict];
//                        }
//                    }
//
//            }
            
           
            arrayTotalData = [[NSArray alloc]initWithArray:filteredArray];
        }
        else if (tabSelected == 2)
        {
            NSPredicate *predicateString;
            NSMutableArray *filteredArray =[[NSMutableArray alloc]init] ;
            
//            if (isFIT)
//            {
               predicateString = [NSPredicate predicateWithFormat:@"(%K == %@) or (%K == %@)", @"INVENTORY", @"ONREQA",@"INVENTORY", @"INVNA"];
                filteredArray = [NSMutableArray arrayWithArray:[arrayTotalData filteredArrayUsingPredicate:predicateString]];
//            }
//            else
//            {
//                    for (int  i = 0 ; i< arrayTotalData.count;i++)
//                    {
//                        NSDictionary *dict  = arrayTotalData[i];
//                        if ([[dict valueForKey:@"AVL_INV"] intValue] < [noOfPacks intValue])
//                        {
//                            [filteredArray addObject:dict];
//                        }
//                    }
//            }
            
            
            arrayTotalData = [[NSArray alloc]initWithArray:filteredArray];
        }
        
        NSMutableArray *arrayOfMonths = [[NSMutableArray alloc]init];
        
        for(int i = 1; i<13 ; i++)
        {
            NSPredicate *predicateString;
            if(i <= 9)
            {
            predicateString = [NSPredicate predicateWithFormat:@"%K contains[cd] %@", @"DATE", [NSString stringWithFormat:@"-0%d-",i]];
            }else
            {
            predicateString = [NSPredicate predicateWithFormat:@"%K contains[cd] %@", @"DATE", [NSString stringWithFormat:@"-%d-",i]];
            }
            NSLog(@"predicate %@",predicateString);
            NSArray *filteredArray = [NSMutableArray arrayWithArray:[arrayTotalData filteredArrayUsingPredicate:predicateString]];
            if (filteredArray.count != 0)
            {
                [arrayOfMonths addObject:filteredArray];
            }
        }
        
        NSLog(@"%@",arrayOfMonths);
        [self prepareMonthArray:arrayOfMonths];
        
    }else
    {
        UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Alert"
                                                                           message:@"No Dates Found"
                                                                    preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                                                             
             [alertView dismissViewControllerAnimated:YES completion:nil];
            [[SlideNavigationController sharedInstance] popViewControllerAnimated:NO];
            
                                                         }];
        
        [alertView addAction:okAction];
        
        [self presentViewController:alertView animated:YES completion:nil];
        
        
    }
}
-(void)prepareMonthArray:(NSArray *)array
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd-MM-yyyy"];
    
    NSMutableArray *arrTemp = [NSMutableArray array];
    
    NSMutableArray *sortedArray = [[NSMutableArray alloc] initWithArray:array];
    
    // fast enumeration of the array
    for (int indexI = 1; indexI < sortedArray.count; indexI++)
    {
        for(int indexJ=0; indexJ < (sortedArray.count - indexI); indexJ++)
        {
            
            NSMutableArray *arrOfDataI = [sortedArray objectAtIndex:indexJ];
        
            NSMutableArray *arrOfDataJ = [sortedArray objectAtIndex:(indexJ + 1)];

            NSDate *dateI = [formatter dateFromString:[[[NSDictionary alloc] initWithDictionary:[arrOfDataI objectAtIndex:0]] valueForKey:@"DATE"]];
            
            NSDate *dateJ = [formatter dateFromString:[[[NSDictionary alloc] initWithDictionary:[arrOfDataJ objectAtIndex:0]] valueForKey:@"DATE"]];
            
            if ([dateI compare:dateJ] == NSOrderedDescending)
            {
                arrTemp = [sortedArray objectAtIndex:indexJ];
                
                [sortedArray replaceObjectAtIndex:indexJ withObject:arrOfDataJ];
                
                [sortedArray replaceObjectAtIndex:(indexJ+1) withObject:arrTemp];
            }
        }
        
    }
    
    arrayForRows = [[NSArray alloc]initWithArray:sortedArray];
    
    [self.date_collectionView reloadData];
}
#pragma mark - Collection View Delegates And DataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return [getMainDateArray count];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [[getMainDateArray objectAtIndex:section] count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CalenderCollectionViewCell *cell= (CalenderCollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"CalenderCollectionViewCell" forIndexPath:indexPath];
    NSDate *date = [[getMainDateArray objectAtIndex:indexPath.section]objectAtIndex:indexPath.row];
    cell.selectedDate = selectedDateInView;
    [cell loadDateDataInCell:date withIndex:indexPath.row withCalederDateDict:self.dataDict];
    return cell;
}
-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    if (kind == UICollectionElementKindSectionHeader) {
        CollectionReusableView *reusableview = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"CollectionReusableView" forIndexPath:indexPath];
        NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear                                           fromDate:[NSDate date]];
        components.month = currentmonthIndex+indexPath.section;
        NSDate *firstDayOfMonthDate = [[NSCalendar currentCalendar] dateFromComponents: components];
        reusableview.lbl_hederTitle.text = [[DateStringSingleton shared]getDateTimeStringFormat:@"MMM yyyy" DateValue:firstDayOfMonthDate];
        return reusableview;
    }
    return nil;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(self.view.frame.size.width*0.12, self.view.frame.size.width*0.12);
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
  return CGSizeMake(self.view.frame.size.width, 100);;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    NSDate *pressDate = [[getMainDateArray objectAtIndex:indexPath.section]objectAtIndex:indexPath.row];
    NSString *dateString = [[DateStringSingleton shared]getDateTimeStringFormat:@"dd-MM-yyyy" DateValue:pressDate];
    BOOL isPresent = [self isDatePresentDateObject:mainCalenderObject withDateString:dateString withPkgId:mainCalenderObject.pkgSubType];
    selectedDateInView = (isPresent) ? pressDate : [NSDate date];
    
//    NSArray *filterArray = [arrayForRows filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id  _Nullable evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
//        NSArray *array = evaluatedObject;
//
//        return [array filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(NSString *evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
//            return [[evaluatedObject valueForKey:@"DATE"] isEqualToString:dateString];
//        }]];
//    }]];
    
    for(id newObject in arrayForRows){
        for(id singleObject in newObject){
            if ([[singleObject valueForKey:@"DATE"] isEqualToString:dateString]){
                selectedDict = singleObject;
                break;
            }
        }
    }
    
//    if ([filterArray count]>0)
//        selectedDict = [filterArray firstObject];
    [self.date_collectionView reloadData];
}

#pragma mark - Check Is Date Present
-(BOOL)isDatePresentDateObject:(CalenderObject*)calObject withDateString:(NSString*)dateStr withPkgId:(int)type{
    for (id bookableObj in calObject.bookable){
        CalenderObject *requestObj = [[CalenderObject alloc] initWithBookableAndRequestObjectDict:bookableObj withPackgeSubType:type];
        if ([requestObj.date isEqualToString:dateStr]){
            return YES;
        }
    }
    return NO;
}


#pragma mark - IBButton Action
- (IBAction)btn_backInCalenderVC:(id)sender {
    [self jumpToPreviousViewController];
}
- (IBAction)btn_proceedPress:(id)sender {
    
    [self.calenderDelgate setSelectedDate:selectedDateInView withVCIndex:self.previosVCIndex withDataDict:selectedDict];
    [self jumpToPreviousViewController];
}

@end
