//
//  AccDetailsTableViewCell.swift
//  holidays
//
//  Created by Kush_Team on 22/06/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

import UIKit

class AccDetailsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var txt_nameField: UITextField!
    @IBOutlet weak var txt_emailField: UITextField!
    @IBOutlet weak var txt_phoneField: UITextField!
    @IBOutlet weak var txt_addressField: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setProfileDetails(profileDict:[String:Any]?){
        if let isDict = profileDict{
            if let firstName = isDict[APIResponseConstants.UserProfile.FIRST_NAME], let lastName = isDict[APIResponseConstants.UserProfile.LAST_NAME]{
                self.txt_nameField.text = "\(firstName) \(lastName)"
            }
            self.txt_emailField.text = isDict[APIResponseConstants.UserProfile.EMAIL_ID] as? String
            self.txt_phoneField.text = isDict[APIResponseConstants.UserProfile.CONTACT_NUMBER] as? String
            if let isAddreess =  isDict[APIResponseConstants.UserProfile.ADDRESS] as? [String] {
                if (isAddreess.count > 0){
                    self.txt_addressField.text = isAddreess[0]
                }
            }
        }
    }
    func setProfileModelsDetails(profileModel:CustomerProfileModel){
        self.txt_nameField.text = "\(profileModel.firstName) \(profileModel.lastName)"
        self.txt_emailField.text = profileModel.emailId
        self.txt_phoneField.text = profileModel.mobileNo
        if (profileModel.addresses.count > 0){
            let isAddreds = profileModel.addresses[0]
            let streetArea = isAddreds["streetArea"] ?? ""
            let state = isAddreds["state"] ?? ""
            let pincode = isAddreds["pincode"] ?? ""
            self.txt_addressField.text = "\(streetArea) \n\(state) \(pincode)"
        }
    }
}
