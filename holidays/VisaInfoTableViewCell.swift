//
//  VisaInfoTableViewCell.swift
//  holidays
//
//  Created by Kush_Team on 09/07/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

import UIKit

class VisaInfoTableViewCell: UITableViewCell {

    @IBOutlet weak var txt_issuedCountry: UITextField!
    @IBOutlet weak var txt_visaType: UITextField!
    @IBOutlet weak var txt_expDate: UITextField!
    @IBOutlet weak var txt_visaNumber: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setProfileModelsDetailsInVisa(profileModel:CustomerProfileModel){
        
    }
}
