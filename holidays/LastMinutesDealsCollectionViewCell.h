//
//  LastMinutesDealsCollectionViewCell.h
//  holidays
//
//  Created by Kush_Tech on 24/02/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WhatsNewObject.h"
#import "NewMasterCollectionViewCell.h"
NS_ASSUME_NONNULL_BEGIN

@interface LastMinutesDealsCollectionViewCell : NewMasterCollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *banner_image;
@property (weak, nonatomic) IBOutlet UILabel *lbl_destination;
@property (weak, nonatomic) IBOutlet UILabel *lbl_numOfDays;
@property (weak, nonatomic) IBOutlet UILabel *lbl_prize;
@property (weak, nonatomic) IBOutlet UILabel *lbl_prize_low;
-(void)showLastMinutesDeals:(WhatsNewObject*)bannerObj;


@end

NS_ASSUME_NONNULL_END
