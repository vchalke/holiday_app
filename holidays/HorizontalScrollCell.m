//
//  HorizontalScrollCell.m
//
//  Created by ketan on 08/10/15.
//  Copyright © 2015 Mobicule. All rights reserved.
//

#import "HorizontalScrollCell.h"


@implementation HorizontalScrollCell

- (void)awakeFromNib {
    // Initialization codes
 
}

-(void)setUpCellWithArray:(NSArray *)array withSelectedtab:(int)clickedTab withISFIT:(BOOL)isFIT withISMRP:(BOOL)isMRP withNoOfPacks:(NSString *)noofPacks
{
    CGFloat xbase = 10;
    CGFloat width = 70;
    
    [[self.scroll subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [self.scroll setScrollEnabled:YES];
    [self.scroll setShowsHorizontalScrollIndicator:NO];
    
    for(int i = 0; i < [array count]; i++)
    {
        NSDictionary *dictForDate = array[i];
        if (i==0)
        {
            NSString *date = [dictForDate valueForKey:@"DATE"];
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"dd-MM-yyyy"];
            NSDate *dateFromString = [dateFormatter dateFromString:date];
            [dateFormatter setDateFormat:@"MMMM yyyy"] ;
            NSString *InDate = [dateFormatter stringFromDate:dateFromString];
            self.title.text = InDate;
        }
        UIView *custom = [self createCustomViewWithImage:dictForDate withIndex:i withSelectedtab:clickedTab withISFIT:isFIT withISMRP:isMRP withNoOfPacks:noofPacks];
        [self.scroll addSubview:custom];
        [custom setFrame:CGRectMake(xbase, 0, width, self.scroll.frame.size.height)];
        xbase += 0.6 + width;
        
    }
    
    [self.scroll setContentSize:CGSizeMake(xbase, self.scroll.frame.size.height)];
    
    self.scroll.delegate = self;
}

-(UIView *)createCustomViewWithImage:(NSDictionary *)dictForDate withIndex:(int)index withSelectedtab:(int)clickedTab withISFIT:(BOOL)isFIT withISMRP:(BOOL)isMRP withNoOfPacks:(NSString *)noofPacks
{
    
    NSString *price = [NSString stringWithFormat:@"%@",[dictForDate valueForKey:@"DR_PRICE"]];
    NSString *date = [dictForDate valueForKey:@"DATE"];
    int dateInt  = [self getDateWithString:date];
    NSString *day = [self getDayStringFromDateString:date];
    
    UIView *custom = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 70, self.scroll.frame.size.height)];
    [custom setTag:index];
    custom.layer.borderWidth = 0.6;
    custom.layer.borderColor = [UIColor grayColor].CGColor;
    UIView *loweView = [[UIView alloc]initWithFrame:CGRectMake(0, 50, 70, self.scroll.frame.size.height)];
    loweView.backgroundColor = [UIColor colorFromHexString:@"#FFCC83"];
    
    UILabel *priceLabel = [[UILabel alloc]initWithFrame:CGRectMake(5, 10, 60, 20)];
    [priceLabel setFont:[UIFont fontWithName:TITILLUM_REGULAR size:12]];
    priceLabel.text = [NSString stringWithFormat:@"%d",15];
    priceLabel.textAlignment = NSTextAlignmentCenter;
    
    /*
     if (isFIT)
     {
     predicateString = [NSPredicate predicateWithFormat:@"(%K == %@) or (%K == %@)", @"inventory", @"ONREQA",@"inventory", @"INVNA"];
     }
     else
     {
     if (isMRP)
     {
     predicateString = [NSPredicate predicateWithFormat:@"(%K > %@)", @"avlInv",noOfPacks];
     }
     else
     {
     predicateString = [NSPredicate predicateWithFormat:@"(%K == %@) or (%K == %@)", @"inventory", @"ONREQA",@"inventory", @"INVNA"];
     }
     }
     */
    
     priceLabel.text = [NSString stringWithFormat:@"₹ %@",price];
    
//    if (isFIT)
//    {
        if ([[[dictForDate valueForKey:@"INVENTORY"]lowercaseString] isEqualToString:@"onreqa"] || [[[dictForDate valueForKey:@"INVENTORY"]lowercaseString] isEqualToString:@"invna"] )
        {
            loweView.backgroundColor = [UIColor colorFromHexString:@"#4169E1"];
            if ([price isEqualToString:@""] || price == nil)
            {
                priceLabel.text = @"On Request";
            }
            
        }
//    }
//    else
//    {
//            if ([[dictForDate valueForKey:@"AVL_INV"] intValue] < [noofPacks intValue])
//            {
//                loweView.backgroundColor = [UIColor colorFromHexString:@"#4169E1"];
//                if ([price isEqualToString:@""] || price == nil)
//                {
//                    priceLabel.text = @"On Request";
//                }
//            }
//    }
    
    
    
    [loweView addSubview:priceLabel];
    
    
    UILabel *dateLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 20, 25, 20)];
    [dateLabel setFont:[UIFont fontWithName:TITILLUM_REGULAR size:15]];
    dateLabel.text = [NSString stringWithFormat:@"%d",dateInt];
    dateLabel.textAlignment = NSTextAlignmentCenter;
    
    UILabel *dayLabel = [[UILabel alloc]initWithFrame:CGRectMake(35, 21, 31, 20)];
     [dayLabel setFont:[UIFont fontWithName:TITILLUM_THIN size:12]];
    dayLabel.text = [day uppercaseString];
    dayLabel.textAlignment = NSTextAlignmentCenter;

    [custom addSubview:dayLabel];
    [custom addSubview:dateLabel];
    [custom addSubview:loweView];
    [custom setBackgroundColor:[UIColor clearColor]];
    
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [custom addGestureRecognizer:singleFingerTap];
    
    return custom;
}


-(int)getDateWithString:(NSString *)stringDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    NSDate *dateFromString = [dateFormatter dateFromString:stringDate];
    NSCalendar *calendar =[NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:(NSCalendarUnitDay | NSCalendarUnitMonth |NSCalendarUnitYear) fromDate:dateFromString];
    int day = (int)[components day]; //day from the date
    return day;
}

-(NSString *)getDayStringFromDateString:(NSString *)dateString
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    NSDate *dateFromString = [dateFormatter dateFromString:dateString];
    [dateFormatter setDateFormat:@"EEE"] ;
    NSString *InDate = [dateFormatter stringFromDate:dateFromString];//req format 10 jan 2012
    return InDate;

}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate

{
   // [self containingScrollViewDidEndDragging:scrollView];
    
}

- (void)containingScrollViewDidEndDragging:(UIScrollView *)containingScrollView
{
   // CGFloat minOffsetToTriggerRefresh = 25.0f;
    
    NSLog(@"%.2f",containingScrollView.contentOffset.x);
    
    NSLog(@"%.2f",self.scroll.contentSize.width);
    
    if (containingScrollView.contentOffset.x <= -50)
    {
        
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(-50 , 7, 100, 150)];
        
        UIActivityIndicatorView *acc = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        acc.hidesWhenStopped = YES;
        [view addSubview:acc];
        
        [acc setFrame:CGRectMake(view.center.x - 25, view.center.y - 25, 50, 50)];
        
        [view setBackgroundColor:[UIColor clearColor]];
        
        [self.scroll addSubview:view];
        
        [acc startAnimating];
        
        [UIView animateWithDuration: 0.3
         
                              delay: 0.0
         
                            options: UIViewAnimationOptionCurveEaseOut
         
                         animations:^{
                             
                             [containingScrollView setContentInset:UIEdgeInsetsMake(0, 100, 0, 0)];
                             
                         }
                         completion:nil];
        //[containingScrollView setContentInset:UIEdgeInsetsMake(0, 100, 0, 0)];
        
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NSLog(@"Started");
            dispatch_async(dispatch_get_main_queue(), ^{
                
                
                //Do whatever you want.
                
                NSLog(@"Refreshing");
                
               [NSThread sleepForTimeInterval:3.0];
                
                NSLog(@"refresh end");
                
                [UIView animateWithDuration: 0.3
                
                                      delay: 0.0
                
                                    options: UIViewAnimationOptionCurveEaseIn
                
                                 animations:^{
                
                                     [containingScrollView setContentInset:UIEdgeInsetsMake(0, 0, 0, 0)];
                
                                 }
                                                completion:nil];
            });
            
        });
        
    }
}

//The event handling method
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
    NSLog(@"clicked");
    
   // UIView *selectedView = (UIView *)recognizer.view;
    
    if([_cellDelegate respondsToSelector:@selector(cellSelectedWithGesture:withScrollView:)])
        [_cellDelegate cellSelectedWithGesture:recognizer withScrollView:_scroll];
    
    //Do stuff here...
}



@end
// Copyright belongs to original author
// http://code4app.net (en) http://code4app.com (cn)
// From the most professional code share website: Code4App.net
