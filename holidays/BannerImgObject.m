//
//  BannerImgObject.m
//  holidays
//
//  Created by Kush_Tech on 24/02/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "BannerImgObject.h"

@implementation BannerImgObject

-(instancetype)initWithImgArray:(NSDictionary *)arrayObjDict{
   if ([super init])
    {
        NSLog(@"%@",[arrayObjDict valueForKey:@"bannerLargeImage"]);
        NSLog(@"%@",[arrayObjDict valueForKey:@"bannerMediumImage"]);
        NSLog(@"%@",[arrayObjDict valueForKey:@"bannerSmallImage"]);
        
        self.bannerName = [arrayObjDict valueForKey:@"bannerName"];
        self.redirectUrl = [arrayObjDict valueForKey:@"redirectUrl"];
        
        self.bannerLargeImage = [arrayObjDict valueForKey:@"bannerLargeImage"];
        self.bannerMediumImage = [arrayObjDict valueForKey:@"bannerMediumImage"];
        self.bannerSmallImage = [arrayObjDict valueForKey:@"bannerSmallImage"];
        
        self.mainBannerImgUrl = [arrayObjDict valueForKey:@"bannerSmallImage"];
        if ([self.mainBannerImgUrl  isEqual: @"NA"])
        {
            self.mainBannerImgUrl = [arrayObjDict valueForKey:@"bannerMediumImage"];
        }
        if ([self.mainBannerImgUrl  isEqual: @"NA"])
        {
            self.mainBannerImgUrl = [arrayObjDict valueForKey:@"bannerLargeImage"];
        }
        
        
//        NSLog(@"%@",arrayObj.bannerLargeImage);
//           NSLog(@"%@",arrayObj.bannerMediumImage);
//           NSLog(@"%@",arrayObj.bannerSmallImage);
        
        
    }
    return self;
}
@end
