//
//  ItinenaryDetailTableViewCell.m
//  holidays
//
//  Created by Kush_Tech on 05/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "ItinenaryDetailTableViewCell.h"
#import "NumberTableViewCell.h"
#import "ItinenaryObject.h"

@implementation ItinenaryDetailTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self.tableViews registerNib:[UINib nibWithNibName:@"NumberTableViewCell" bundle:nil] forCellReuseIdentifier:@"NumberTableViewCell"];
    self.tableViews.delegate = self;
    self.tableViews.dataSource = self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)loadDataFromPackageModel:(PackageDetailModel*)packageModel{
    selectedOption = 0;
    dataArray = [[NSMutableArray alloc]init];
//    NSMutableString *mutableString = [[NSMutableString alloc]init];
    NSSortDescriptor *brandDescriptor = [[NSSortDescriptor alloc] initWithKey:@"itineraryDay" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:brandDescriptor];
    NSArray *sortedArray = [packageModel.itineraryCollectionArray sortedArrayUsingDescriptors:sortDescriptors];
    for (id object in sortedArray) {
        ItinenaryObject *itinery = [[ItinenaryObject alloc]initWithItinenaryDict:object];
        NSString *discription = itinery.itineraryDescription;
//        NSAttributedString *attrStr = [[NSAttributedString alloc] initWithData:[discription dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:nil error:nil];
//        [mutableString appendString:[NSString stringWithFormat:@"%@\n",attrStr.string]];
        [dataArray addObject:discription];
    }
//    self.lbl_descrption.text = mutableString;
//    [self showArrayOfStringInWebView:[dataArray copy]];
//    [self.web_ItinenaryView loadHTMLString:[dataArray objectAtIndex:0] baseURL:nil];
//    [self.tableViews reloadData];
    itinenaryCount = [packageModel.itineraryCollectionArray count];
    [self buttonPress:nil];
}

#pragma mark - Array Shows In Web Views
//-(void)showArrayOfStringInWebView:(NSArray*)strArray{
//    for (NSString *objectStr in strArray) {
//        NSString *htmlString = [NSString stringWithFormat:@"<font face='HelveticaNeue' size='3'>%@", objectStr];
//        [self.web_ItinenaryView loadHTMLString:htmlString baseURL:nil];
//    }
//}

#pragma mark - UITableView Delegate and DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [dataArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    NumberTableViewCell* cell = (NumberTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"NumberTableViewCell"];
    cell.backgroundColor = [UIColor clearColor];
    [cell.numberButton setTitle:[NSString stringWithFormat:@"%ld",indexPath.row+1] forState:UIControlStateNormal];
    cell.numberButton.tag = indexPath.row;
    [cell.numberButton setTitleColor:(indexPath.row == selectedOption) ? [UIColor whiteColor] : [UIColor colorWithRed:0 green:83/255.0 blue:169/255.0 alpha:1.0] forState:UIControlStateNormal];
    cell.numberButton.backgroundColor = (indexPath.row == selectedOption) ? [UIColor colorWithRed:0 green:83/255.0 blue:169/255.0 alpha:1.0] : [UIColor whiteColor];
    [cell.numberButton addTarget:self action:@selector(buttonPress:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}
- (void) buttonPress:(UIButton *)sender {
    selectedOption = sender.tag;
    //    Lato-Regular
    NSString *htmlStringObj = [NSString stringWithFormat:@"<span style=\"font-family: Lato-Regular; font-size: 15\">%@</span>", [dataArray objectAtIndex:sender.tag]];
    [self.web_ItinenaryView loadHTMLString:htmlStringObj baseURL:nil];
    [self.tableViews reloadData];
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(NumberTableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
//    int arrayCount = (int)_holidayPackageDetail.arrayItinerary.count - 1;
    if(indexPath.row == 0){
        cell.upperView.hidden = YES;
    }
    else if (indexPath.row ==  itinenaryCount-1){
        cell.bottomView.hidden = YES;
    }else{
        cell.upperView.hidden = NO;
        cell.bottomView.hidden = NO;
    }

}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 100;
}
- (IBAction)btn_viewMore:(id)sender {
    [self.itinenaryDelegate jumpToItinenary];
    
}

@end
