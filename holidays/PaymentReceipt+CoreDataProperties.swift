//
//  PaymentReceipt+CoreDataProperties.swift
//  holidays
//
//  Created by Saurav on 28/11/17.
//  Copyright © 2017 Mobicule. All rights reserved.
//

import Foundation
import CoreData


extension PaymentReceipt {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<PaymentReceipt> {
        return NSFetchRequest<PaymentReceipt>(entityName: "PaymentReceipt")
    }

    @NSManaged public var bfNumber: String?
    @NSManaged public var receiptAmount: String?
    @NSManaged public var receiptCurrency: String?
    @NSManaged public var receiptDate: NSDate?
    @NSManaged public var receiptNumber: String?
    @NSManaged public var receiptType: String?
    @NSManaged public var receiptURL: String?
    @NSManaged public var tourRelation: Tour?

}
