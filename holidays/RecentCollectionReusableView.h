//
//  RecentCollectionReusableView.h
//  holidays
//
//  Created by Kush_Team on 16/06/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RecentCollectionReusableView : UICollectionReusableView

@end

NS_ASSUME_NONNULL_END
