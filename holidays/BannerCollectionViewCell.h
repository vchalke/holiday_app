//
//  BannerCollectionViewCell.h
//  holidays
//
//  Created by Kush_Tech on 24/02/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BannerImgObject.h"
#import "WhatsNewObject.h"
NS_ASSUME_NONNULL_BEGIN

@interface BannerCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbl_Title;
@property (weak, nonatomic) IBOutlet UILabel *lbl_SubTitle;
@property (weak, nonatomic) IBOutlet UIImageView *banner_imgView;
@property (weak, nonatomic) IBOutlet UIView *banner_baseView;

-(void)showBannerDetails:(BannerImgObject*)bannerObj;
-(void)showWhatsNewDetails:(WhatsNewObject*)bannerObj withData:(BOOL)flag;

@end

NS_ASSUME_NONNULL_END
