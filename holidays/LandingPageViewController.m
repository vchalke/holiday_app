//
//  LandingPageViewController.m
//  holidayApp
//
//  Created by vaibhav on 06/08/15.
//  Copyright (c) 2015 Mobicule. All rights reserved.
//

#import "LandingPageViewController.h"
#import "PSSlideShow.h"
#import "SearchHolidaysVC.h"
//#import "FlipViewController.h"
#import "FilterPackageVC.h"
#import "NotificationViewController.h"
#import "AppDelegate.h"
#import <CoreData/CoreData.h>
#import "WebViewController.h"
#import "NetCoreAnalyticsVC.h"
#import "SSKeychain.h"
#import "MobiculeUtilityManager.h"
#import "Thomas_Cook_Holidays-Swift.h"
#import "SWRevealViewController.h"
#import "ForexRightSlideViewController.h"
#import "SDWebImageDownloader.h"


#define KEYCHAIN_SERVICE @"Holiday_App_Keychain"
#define KEYCHAIN_ACCOUNT @"com.thomasCookHoliday.app"

#define kEntity @"holidays"
#define kType   @"webservice"
#define kAction  @"search"


//#import <Branch/Branch.h>


@interface LandingPageViewController ()<ManageHolidayPopUpVCDelegate>
{
    NSArray *imagesDataArray;
    NSMutableDictionary *payloadList;
    NSMutableDictionary *payLoadForViewDetails;
    HolidayPackageDetail *packageDetail;
}

@property (strong,nonatomic) IBOutlet PSSlideShow * slideshow;

@end

@implementation LandingPageViewController{
    
    NSTimer *tmpScheduler;
    NSArray *arrayOfButtons;
    NSMutableArray *arrayOfImages;
    BOOL flag;
    UIImageView* imageViewDefault;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [[NSBundle mainBundle]loadNibNamed:@"LandingPageViewController" owner:self options:nil];
    [super addViewInBaseView:self.viewForLandingPage];
    [super hideMenuBar];
    
    self.slideshow.hidden = YES;
    [self getTokenID];
    [self SlideShowStart];
    
    
    
    arrayOfImages = [NSMutableArray new];
    
    if (_notificationDict != nil)
    {
        //        if ([[_notificationDict objectForKey:@"notificationtype"]  isEqualToString:@"package"])
        //        {
        //            NSString *packageID =  [_notificationDict objectForKey:@"redirect"];
        //            [self fetchPackageDetailsWithPackageID:packageID];
        //        }
        //        else if ([[_notificationDict objectForKey:@"notificationtype"] isEqualToString:@"web"])
        //        {
        //            NSString *urlString =  [_notificationDict objectForKey:@"redirect"];
        //            NSURL *url = [NSURL URLWithString:urlString];
        //            WebViewController *webViewVC = [[WebViewController alloc]initWithNibName:@"BaseViewController" bundle:nil];
        //            webViewVC.url = url;
        //            [[SlideNavigationController sharedInstance] pushViewController:webViewVC animated:YES];
        //        }
        //        else if ([[_notificationDict objectForKey:@"notificationtype"] isEqualToString:@"destination"])
        //        {
        //            NSString *destinationString  =  [_notificationDict objectForKey:@"redirect"];
        //            [self searchDestinationWithDestinationName:destinationString];
        //
        //        }
        //        else  if ([[_notificationDict objectForKey:@"notificationtype"] isEqualToString:@"offer"])
        //        {
        //            NSString *urlString =  [_notificationDict objectForKey:@"redirect"];
        //            NSURL *url = [NSURL URLWithString:urlString];
        //            WebViewController *webViewVC = [[WebViewController alloc]initWithNibName:@"BaseViewController" bundle:nil];
        //            webViewVC.url = url;
        //            [[SlideNavigationController sharedInstance] pushViewController:webViewVC animated:YES];
        //        }
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateNotificationCount) name:@"NotificationInActiveState" object:nil];
    
    [self performSelector:@selector(menuDidOpened) withObject:nil afterDelay:0.3];
    
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(menuDidOpened)
                                                name:SlideNavigationControllerDidReveal
                                              object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(menuDidClosed)
                                                name:SlideNavigationControllerDidClose
                                              object:nil];
    
    
    
    payloadList =  [NSMutableDictionary dictionary];
    payLoadForViewDetails = [[NSMutableDictionary alloc]init];
    
    
    
}



-(void) updateNotificationCount {
    
    [self getUnreadNotificationCount];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSLog(@"view didappear call");
    [AppUtility checkIsVersionUpgrade];
    
    self.btnFlight.alpha               = 0;
    self.btnForex.alpha                = 0;
    self.btnIndianHoliday.alpha        = 0;
    self.btnInternationalHoliday.alpha = 0;
    
    [self getUnreadNotificationCount];
    
    [self animateAllMenuButtons];
    [self getTokenID];
    if (flag == NO) {
        [self  SlideShowStart];
    }
    
    
}

-(void)searchDestinationWithDestinationName:(NSString *)destination
{
    
    SearchHolidaysVC *searchVC=[[SearchHolidaysVC alloc]initWithNibName:@"BaseViewController" bundle:nil];
    //searchVC.boolForSearchPackgScreen=true;
    searchVC.bannerdestination = destination;
    searchVC.boolForSearchPackgScreen = true;
    [[SlideNavigationController sharedInstance] pushViewController:searchVC animated:YES];
    /*NSString *strDestination=destination;
     
     NSDictionary *dictOfData = [NSDictionary dictionaryWithObjectsAndKeys:@"",kBudgetKey,@"",kNightToSpendKey,strDestination,kDestinationKey, nil];
     
     [FIRAnalytics logEventWithName:@"Banner_Clicks" parameters:@{kFIRParameterItemName:[NSString stringWithFormat:@"Banner Clicked :%@",[dictOfData objectForKey:@"destination"]]}];
     
     activityLoadingView = [LoadingView loadingViewInView:self.viewForLandingPage.superview withString:@"" andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
     
     if ([self connected])
     {
     dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
     
     NetworkHelper *helper = [NetworkHelper sharedHelper];
     
     NSString *strResponseLocal = [helper getDataFromServerForType:@"webservice" entity:@"holidays" action:@"search" andUserJson:dictOfData];
     
     NSString *strStatus = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponseLocal]valueForKey:@"status"];
     dispatch_async(dispatch_get_main_queue(), ^{
     if ([strStatus isEqualToString:kStatusSuccess])
     {
     NSString * packageDetailsMessage = [[[JsonSerealizer dictonaryOfJsonFromJsonData:strResponseLocal]valueForKey:@"data"]valueForKey:@"packageDetailsMessage"];
     
     if ([@"Data not found." caseInsensitiveCompare:packageDetailsMessage] == NSOrderedSame)
     {
     [self showAlertViewWithTitle:@"Alert" withMessage:@"Sorry, we could not find any package for the given search. Please try with a different search criteria."];
     }
     else
     {
     NSArray *arrayOfData = [[[JsonSerealizer dictonaryOfJsonFromJsonData:strResponseLocal]valueForKey:kServerResponseKeyData]valueForKey:kHolidays];
     
     if([arrayOfData count]>0)
     {
     NSDictionary *dict = [arrayOfData objectAtIndex:0];
     NSString *bannerImagePath = [dict objectForKey:@"packageImagePath"];
     payloadList =  [NSMutableDictionary dictionary];
     [payloadList setObject:bannerImagePath forKey:@"s^BANNER"];
     
     [self netCoreHomePageLogEvent];
     }
     
     
     NSDictionary *filterDict = [[[JsonSerealizer dictonaryOfJsonFromJsonData:strResponseLocal]valueForKey:kServerResponseKeyData]valueForKey:kFilters];
     NSMutableArray *arrayOfHolidays = [[NSMutableArray alloc]init];
     
     for (NSDictionary *dictOfData in arrayOfData)
     {
     Holiday *nwHoliday  = [[Holiday alloc]initWithDataDict:dictOfData];
     [arrayOfHolidays addObject:nwHoliday];
     }
     
     [activityLoadingView removeView];
     
     TabMenuVC *tabMenuVC=[[TabMenuVC alloc]initWithNibName:@"BaseViewController" bundle:nil];
     tabMenuVC.headerName=[NSString stringWithFormat:@"%@ %@",strDestination, @"Holidays"];
     tabMenuVC.searchedCity = destination;
     tabMenuVC.holidayArray = arrayOfHolidays;
     tabMenuVC.filterDict = filterDict;
     tabMenuVC.totalPackages=[NSString stringWithFormat:@"%lu",(unsigned long)[arrayOfData count]];
     
     [[SlideNavigationController sharedInstance] pushViewController:tabMenuVC animated:YES];
     
     }
     }
     else
     {
     NSString *messsage = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponseLocal]valueForKey:@"message"];
     if (messsage)
     {
     if ([messsage isEqualToString:kVersionUpgradeMessage])
     {
     NSDictionary *downloadDict = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponseLocal] valueForKey:@"data"];
     NSString *downloadURL = [downloadDict valueForKey:@"download_url"];
     [self showAlertViewForVersionUpdateWithUrl:downloadURL];
     }
     else
     {
     [super showAlertViewWithTitle:@"Alert" withMessage:messsage];
     [activityLoadingView removeView];
     }
     }
     else
     {
     [super showAlertViewWithTitle:@"Alert" withMessage:kMessageSomeErrorTryAfterSometime];
     [activityLoadingView removeView];
     
     
     }
     
     }
     
     });
     });
     }
     else
     {
     //[super showAlertViewWithTitle:@"Alert" withMessage:@"No Network connection please try after sometime"];
     [self showAlertViewWithTitle:@"Alert" withMessage:kMessageNoInternet];
     
     [activityLoadingView removeView];
     }
     */
    
}

-(void)fetchPackageDetailsWithPackageID:(NSString *)packageID
{
    NSMutableDictionary *dictOfData = [[NSMutableDictionary alloc]init];
    [dictOfData setValue:packageID forKey:@"packageId"];
    
    activityLoadingView = [LoadingView loadingViewInView:self.viewForLandingPage.superview withString:@"" andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
    
    if ([super connected])
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            NetworkHelper *helper = [NetworkHelper sharedHelper];
            NSString *strResponse = [helper getDataFromServerForType:@"webservice" entity:@"package" action:@"details" andUserJson:dictOfData];
            NSString *strStatus = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"status"];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if ([strStatus isEqualToString:kStatusSuccess])
                {
                    [activityLoadingView removeView];
                    
                    NSArray *dataArray = [[JsonSerealizer arrayOfJSonFromJsonData:strResponse] valueForKey:@"data"];
                    NSDictionary *dataDict;
                    if (dataArray.count != 0)
                    {
                        dataDict = dataArray[0];
                        packageDetail = [[HolidayPackageDetail alloc]initWithDataDict:dataDict];
                        
                        
                        if ([activityLoadingView isDescendantOfView:self.view.superview.superview])
                        {
                            [activityLoadingView removeView];
                            activityLoadingView = nil;
                        }
                        
                        [self fireAppIntHoViewDetailsEvent];
                        
                        
                        TabMenuVC *tabMenuVC = [[TabMenuVC alloc]initWithNibName:@"BaseViewController" bundle:nil];
                        tabMenuVC.flag=@"Overview";
                        tabMenuVC.packageDetail = packageDetail;
                        tabMenuVC.completePackageDetail = dataArray;
                        [[SlideNavigationController sharedInstance] pushViewController:tabMenuVC animated:YES];
                        
                        
                    }
                }else
                {
                    [activityLoadingView removeView];
                    NSString *messsage = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"message"];
                    if (messsage)
                    {
                        if ([messsage isEqualToString:kVersionUpgradeMessage])
                        {
                            NSDictionary *downloadDict = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"data"];
                            NSString *downloadURL = [downloadDict valueForKey:@"download_url"];
                            [self showAlertViewForVersionUpdateWithUrl:downloadURL];
                            
                        }
                        else
                        {
                            [self showAlertViewWithTitle:@"Alert" withMessage:messsage];
                        }
                    }else
                    {
                        [self showAlertViewWithTitle:@"Alert" withMessage:kMessageSomeErrorTryAfterSometime];
                    }
                    
                }
                if ([activityLoadingView isDescendantOfView:self.view.superview.superview])
                {
                    [activityLoadingView removeView];
                    activityLoadingView = nil;
                }
            });
        });
    }
    else
    {
        [self showAlertViewWithTitle:@"Alert" withMessage:kMessageNoInternet];
    }
}
-(void) getUnreadNotificationCount
{
    AppDelegate * delegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    
    NSManagedObjectContext *context = [delegate managedObjectContext] ;
    
    
    NSFetchRequest *request1 = [NSFetchRequest fetchRequestWithEntityName:@"Notification"];
    
    request1.predicate = [NSPredicate predicateWithFormat:
                          @"status = %@", @"unread"];
    
    NSError * error;
    
    NSArray * activeIds1 = [context executeFetchRequest:request1 error:&error];
    
    self.notificationCountLabel.text = [NSString stringWithFormat:@"%lu",(unsigned long)activeIds1.count];
    if (activeIds1.count == 0)
    {
        self.notificationCountLabel.alpha = 0;
        [self.notificationButton setImage:[UIImage imageNamed:@"notification_icon"] forState:UIControlStateNormal];
    }else
    {
        self.notificationCountLabel.alpha = 1;
        [self.notificationButton setImage:[UIImage imageNamed:@"notificationIconCount.png"] forState:UIControlStateNormal];
        
    }
    
}

-(void)viewDidAppear:(BOOL)animated
{
    
    /*
     UIImageView* imageViewDefault;
     
     activityLoadingView1 = [LoadingView loadingViewInView:self.viewForLandingPage.superview withString:@"" andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
     if ([arrayOfImages count] == 0)
     {
     [self configureSlideShow];
     
     
     
     //  imageViewDefault = [[UIImageView alloc]initWithFrame:_slideshow.frame];
     //  [imageViewDefault setImage:[UIImage imageNamed:@"defaultBanner.png"]];
     
     [self.viewForLandingPage addSubview:imageViewDefault];
     dispatch_async(dispatch_get_main_queue(), ^(void){
     
     
     [self getArrayOfBannerImages:^(BOOL taskFinished){
     dispatch_async(dispatch_get_main_queue(), ^(void) {
     
     if (taskFinished)
     {
     if ([imageViewDefault isDescendantOfView:self.viewForLandingPage])
     {
     
     [UIView animateWithDuration:0.5 animations:^{
     [imageViewDefault removeFromSuperview];
     }];
     if (arrayOfImages.count == 0)
     {
     [arrayOfImages addObject:[UIImage imageNamed:@"defaultBanner.png"]];
     }
     }
     
     if (self.slideshow.isHidden)
     {
     [imageLoadingView removeFromSuperview];
     
     self.slideshow.hidden = NO;
     }
     
     [self configureSlideShow];
     }
     });
     
     }];
     
     });
     
     if (self.slideshow.isHidden)
     {
     
     self.slideshow.hidden = NO;
     }
     //           [arrayOfImages addObject:[UIImage imageNamed:@"HolidayBannerImage1.jpg"]];
     //           [arrayOfImages addObject:[UIImage imageNamed:@"HolidayBannerImage2.jpg"]];
     //           [arrayOfImages addObject:[UIImage imageNamed:@"HolidayBannerImage3.jpg"]];
     //
     //[self configureSlideShow];
     
     
     }
     else {
     [activityLoadingView1 removeFromSuperview];
     //activityLoadingView = [LoadingView loadingViewInView:self.viewForLandingPage.superview withString:@"" andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
     //        self.slideshow.hidden = NO;
     //        [arrayOfImages addObject:[UIImage imageNamed:@"HolidayBannerImage1.jpg"]];
     //        [arrayOfImages addObject:[UIImage imageNamed:@"HolidayBannerImage2.jpg"]];
     //        [arrayOfImages addObject:[UIImage imageNamed:@"HolidayBannerImage3.jpg"]];
     //[self configureSlideShow];
     }
     
     
     
     //   tmpScheduler= [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(scrollingTimer) userInfo:nil repeats:YES];
     
     [super hideBackButton:YES];
     */
    
}

-(void)SlideShowStart{
    
    //UIImageView* imageViewDefault;
    if([self connected]){
    [activityLoadingViewRT removeFromSuperview];
    //activityLoadingView1 = [LoadingView loadingViewInView:self.viewForLandingPage.superview withString:@"" andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
    if ([arrayOfImages count] == 0)
    {
        flag = YES;
        [self configureSlideShow];
        dispatch_async(dispatch_get_main_queue(), ^(void){
            
            
            [self getArrayOfBannerImages:^(BOOL taskFinished){
                dispatch_async(dispatch_get_main_queue(), ^(void) {
                    
                    if (taskFinished)
                    {
                        if ([imageViewDefault isDescendantOfView:self.viewForLandingPage])
                        {
                            
                            [UIView animateWithDuration:0.5 animations:^{
                                [imageViewDefault removeFromSuperview];
                            }];
                            if (arrayOfImages.count == 0)
                            {
                                [arrayOfImages addObject:[UIImage imageNamed:@"defaultBanner.png"]];
                            }
                        }
                        
                        if (self.slideshow.isHidden)
                        {
                            [imageLoadingView removeFromSuperview];
                            
                            self.slideshow.hidden = NO;
                        }
                        
                        [self configureSlideShow];
                    }else{
                        [imageViewDefault removeFromSuperview];
                        [self SlideShowStart];
                        NSLog(@"slide show called");
                    }
                });
                
            }];
            
        });
        
        if (self.slideshow.isHidden)
        {
            
            self.slideshow.hidden = NO;
        }
        //           [arrayOfImages addObject:[UIImage imageNamed:@"HolidayBannerImage1.jpg"]];
        //           [arrayOfImages addObject:[UIImage imageNamed:@"HolidayBannerImage2.jpg"]];
        //           [arrayOfImages addObject:[UIImage imageNamed:@"HolidayBannerImage3.jpg"]];
        //
        //[self configureSlideShow];
        
        
    }
    else {
        //[activityLoadingView1 removeFromSuperview];
        //activityLoadingView = [LoadingView loadingViewInView:self.viewForLandingPage.superview withString:@"" andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
        //        self.slideshow.hidden = NO;
        //        [arrayOfImages addObject:[UIImage imageNamed:@"HolidayBannerImage1.jpg"]];
        //        [arrayOfImages addObject:[UIImage imageNamed:@"HolidayBannerImage2.jpg"]];
        //        [arrayOfImages addObject:[UIImage imageNamed:@"HolidayBannerImage3.jpg"]];
        //[self configureSlideShow];
    }
    
    [super hideBackButton:YES];
    }else{
        flag = NO;
        //self.slideshow.hidden = NO;
    }
}

-(void)viewDidDisappear:(BOOL)animated{
    
    [tmpScheduler invalidate];
    [super hideBackButton:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)configureSlideShow
{
    
    if (arrayOfImages.count != 0)
    {
        [activityLoadingView1 removeFromSuperview];
        
        _slideshow.delegate = self;
        [_slideshow setDelay:3];
        
        [_slideshow setTransitionDuration:.6];
        [_slideshow setTransitionType:PSSlideShowTransitionSlide];
        [_slideshow setImagesContentMode:UIViewContentModeScaleToFill];
        [_slideshow addImagesFromResources:arrayOfImages];
        [_slideshow addGesture:PSSlideShowGestureSwipe];
        [_slideshow start];
        
        
        UITapGestureRecognizer *gestureRecogniser = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(singleTapClicked)];
        gestureRecogniser.numberOfTapsRequired = 1;
        _slideshow.userInteractionEnabled = YES;
        [_slideshow addGestureRecognizer:gestureRecogniser];
        
        UIPanGestureRecognizer *panRecognizer  = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panDetected:)];
        panRecognizer.delegate = self;
        [_slideshow addGestureRecognizer:panRecognizer];
        
        
    }else{
        
    }
    
}

- (void)panDetected:(UIPanGestureRecognizer *)aPanRecognizer
{
    CGPoint translation = [aPanRecognizer translationInView:aPanRecognizer.view];
    
    if (translation.x > 0)
    {
        //left
        [_slideshow previous];
    }
    else
    {
        //right
        [_slideshow next];
    }
    
}


-(void)singleTapClicked
{
    NSLog(@"%lu",(unsigned long)_slideshow.currentIndex);
    int index = (int)_slideshow.currentIndex;
    /*
     if (index == 0)
     {
     payloadList =  [NSMutableDictionary dictionary];
     //SearchHolidaysVC *searchVC=[[SearchHolidaysVC alloc]initWithNibName:@"BaseViewController" bundle:nil];
     //searchVC.headerName = kInternationalHoliday;  change on 14 Jan 2018
     [payloadList setObject:@"yes" forKey:@"s^INT_HOLIDAYS"];
     
     WebViewController *webViewVC = [[WebViewController alloc]initWithNibName:@"BaseViewController" bundle:nil];
     NSString* internationUrl = @"https://www.thomascook.in/holidays/value-tour-packages";
     webViewVC.headerString = kInternationalHoliday;
     NSURL *url = [NSURL URLWithString:internationUrl];
     webViewVC.url = url;
     [[SlideNavigationController sharedInstance] pushViewController:webViewVC animated:NO];
     
     //[[SlideNavigationController sharedInstance] pushViewController:searchVC animated:YES];
     
     // [self netCoreHomePageLogEvent];
     
     }
     else if (index == 1)
     {
     payloadList =  [NSMutableDictionary dictionary];
     SearchHolidaysVC *searchVC=[[SearchHolidaysVC alloc]initWithNibName:@"BaseViewController" bundle:nil];
     searchVC.headerName = kIndianHoliday;
     [payloadList setObject:@"yes" forKey:@"s^INDIA_HOLIDAYS"];
     [[SlideNavigationController sharedInstance] pushViewController:searchVC animated:YES];
     
     }
     else if (index == 2)
     {
     //        WebViewController *webViewVC = [[WebViewController alloc]initWithNibName:@"BaseViewController" bundle:nil];
     //        NSString *forexUrl = kUrlForForex;
     //        NSURL *url = [NSURL URLWithString:forexUrl];
     //        webViewVC.url = url;
     //        webViewVC.headerString = @"Forex";
     //        payloadList =  [NSMutableDictionary dictionary];
     //        [payloadList setObject:@"yes" forKey:@"s^FOREX"];
     //        [[SlideNavigationController sharedInstance] pushViewController:webViewVC animated:NO];
     
     ForexLandingPageViewController *ForexLandingVc = [[ForexLandingPageViewController alloc] initWithNibName:@"ForexBaseViewController" bundle:nil];
     
     [[SlideNavigationController sharedInstance] pushViewController:ForexLandingVc animated:NO];
     
     
     }
     */
    
    if (imagesDataArray.count  > index )
    {
        NSDictionary *dictForPackageID = [imagesDataArray objectAtIndex:index];
        
        if (dictForPackageID != nil && dictForPackageID.count != 0 )
        {
            //New Code for Package, Destination, Web Url
            if ([[dictForPackageID valueForKey:@"bannerType"] isEqualToString:@"package"])
            {
                NSString *packageID =  [dictForPackageID objectForKey:@"packageID"];
                payloadList =  [NSMutableDictionary dictionary];
                [payloadList setObject:packageID forKey:@"s^BANNER"];
                //[self netCoreHomePageLogEvent];
                [self fetchPackageDetails:packageID];
            }
            else if ([[dictForPackageID valueForKey:@"bannerType"] isEqualToString:@"web"])
            {
                NSString *urlString =  [dictForPackageID objectForKey:@"redirectUrl"];
                NSURL *url = [NSURL URLWithString:urlString];
                WebViewController *webViewVC = [[WebViewController alloc]initWithNibName:@"BaseViewController" bundle:nil];
                webViewVC.url = url;
                webViewVC.headerString = [dictForPackageID objectForKey:@"bannerName"];
                payloadList =  [NSMutableDictionary dictionary];
                [payloadList setObject:[NSString stringWithFormat:@"%@", url] forKey:@"s^BANNER"];
                // [self netCoreHomePageLogEvent];
                [[SlideNavigationController sharedInstance] pushViewController:webViewVC animated:YES];
            }
            else if ([[dictForPackageID valueForKey:@"bannerType"] isEqualToString:@"destination"])
            {
                NSString *destinationString  =  [dictForPackageID objectForKey:@"destinationName"];
                payloadList =  [NSMutableDictionary dictionary];
                [payloadList setObject:destinationString forKey:@"s^BANNER"];
                // [self netCoreHomePageLogEvent];
                [self searchDestinationWithDestinationName:destinationString];
                
                
            }
            else  if ([[dictForPackageID valueForKey:@"bannerType"] isEqualToString:@"blog"])
            {
                NSString *urlString =  [dictForPackageID objectForKey:@"redirectUrl"];
                NSURL *url = [NSURL URLWithString:urlString];
                WebViewController *webViewVC = [[WebViewController alloc]initWithNibName:@"BaseViewController" bundle:nil];
                webViewVC.url = url;
                webViewVC.headerString = [dictForPackageID objectForKey:@"bannerName"];
                payloadList =  [NSMutableDictionary dictionary];
                [payloadList setObject:[NSString stringWithFormat:@"%@", url] forKey:@"s^BANNER"];
                //[self netCoreHomePageLogEvent];
                [[SlideNavigationController sharedInstance] pushViewController:webViewVC animated:YES];
            }
        }
        /*if ([serverUrl isEqualToString:@"http://220.226.201.140:8080/thomascook-holidays-server/InboundGateway"]) {
         
         //New Code for Package, Destination, Web Url
         if ([[dictForPackageID valueForKey:@"detailType"] isEqualToString:@"package"])
         {
         NSString *packageID =  [dictForPackageID objectForKey:@"detail"];
         [self fetchPackageDetailsWithPackageID:packageID];
         }
         else if ([[dictForPackageID valueForKey:@"detailType"] isEqualToString:@"web"])
         {
         NSString *urlString =  [dictForPackageID objectForKey:@"detail"];
         NSURL *url = [NSURL URLWithString:urlString];
         WebViewController *webViewVC = [[WebViewController alloc]initWithNibName:@"BaseViewController" bundle:nil];
         webViewVC.url = url;
         [[SlideNavigationController sharedInstance] pushViewController:webViewVC animated:YES];
         }
         else if ([[dictForPackageID valueForKey:@"detailType"] isEqualToString:@"destination"])
         {
         NSString *destinationString  =  [dictForPackageID objectForKey:@"detail"];
         [self searchDestinationWithDestinationName:destinationString];
         
         }
         else  if ([[dictForPackageID valueForKey:@"detailType"] isEqualToString:@"offer"])
         {
         NSString *urlString =  [dictForPackageID objectForKey:@"detail"];
         NSURL *url = [NSURL URLWithString:urlString];
         WebViewController *webViewVC = [[WebViewController alloc]initWithNibName:@"BaseViewController" bundle:nil];
         webViewVC.url = url;
         [[SlideNavigationController sharedInstance] pushViewController:webViewVC animated:YES];
         }
         }
         else
         {
         
         //OLD Code for single package type
         NSString *strPackageID = [dictForPackageID valueForKey:@"packageId"];
         activityLoadingView = [LoadingView loadingViewInView:self.view.superview.superview
         withString:@""
         andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
         [self fetchPackageDetails:strPackageID];
         
         }*/
        //   }
        
        //        NSString *strPackageID = [[imagesDataArray objectAtIndex:index]valueForKey:@"packageId"];
        //
        //        activityLoadingView = [LoadingView loadingViewInView:self.view.superview.superview
        //                                                  withString:@""
        //                                           andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
        //        [self fetchPackageDetails:strPackageID];
        //  }
        
    }
}
-(void)fetchPackageDetails:(NSString *)strPackageID
{
    NSMutableDictionary *dictOfData = [[NSMutableDictionary alloc]init];
    [dictOfData setValue:strPackageID forKey:@"packageId"];
    activityLoadingView = [LoadingView loadingViewInView:self.viewForLandingPage.superview withString:@"" andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
    
    if ([self connected])
    {
        
        /*       dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
         
         NetworkHelper *helper = [NetworkHelper sharedHelper];
         NSString *strResponse = [helper getDataFromServerForType:@"webservice" entity:@"package" action:@"details" andUserJson:dictOfData];
         NSString *strStatus = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"status"];
         
         dispatch_async(dispatch_get_main_queue(), ^{
         
         if ([activityLoadingView isDescendantOfView:self.view.superview.superview])
         {
         [activityLoadingView removeView];
         activityLoadingView = nil;
         }
         
         if ([strStatus isEqualToString:kStatusSuccess])
         {
         NSArray *dataArray = [[JsonSerealizer arrayOfJSonFromJsonData:strResponse] valueForKey:@"data"];
         NSDictionary *dataDict;
         if (dataArray.count != 0)
         {
         dataDict = dataArray[0];
         packageDetail = [[HolidayPackageDetail alloc]initWithDataDict:dataDict];
         [self fireAppIntHoViewDetailsEvent];
         
         TabMenuVC *tabMenuVC = [[TabMenuVC alloc]initWithNibName:@"BaseViewController" bundle:nil];
         tabMenuVC.flag=@"Overview";
         tabMenuVC.packageDetail = packageDetail;
         tabMenuVC.completePackageDetail = dataArray;
         [[SlideNavigationController sharedInstance] pushViewController:tabMenuVC animated:YES];
         
         
         }
         }else
         {
         NSString *messsage = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"message"];
         if (messsage)
         {
         if ([messsage isEqualToString:kVersionUpgradeMessage])
         {
         NSDictionary *downloadDict = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"data"];
         NSString *downloadURL = [downloadDict valueForKey:@"download_url"];
         [super showAlertViewForVersionUpdateWithUrl:downloadURL];
         
         }
         else
         {
         [self showAlertViewWithTitle:@"Alert" withMessage:messsage];
         }
         }else
         {
         [self showAlertViewWithTitle:@"Alert" withMessage:kMessageSomeErrorTryAfterSometime];
         }
         
         }
         });
         });*/
        
        
        //        @try
        //        {
        //
        //        NetworkHelper *helper = [NetworkHelper sharedHelper];
        //
        //        NSString *pathParam = strPackageID;
        //
        //        NSDictionary *headerDict = [CoreUtility getHeaderDict];
        //
        //        [helper getResponseWithRequestType:@"" withQueryParam:@"" withPathParam:pathParam withJsonParam:nil withHeaders:headerDict withUrl:kAstraUrlPackageDetail success:^(NSDictionary *responseDict)
        //         {
        //             NSLog(@"Response Dict : %@",responseDict);
        //
        //
        //             dispatch_async(dispatch_get_main_queue(), ^(void) {
        //                 [activityLoadingView removeFromSuperview];
        //                 if (responseDict)
        //                 {
        //                     if (responseDict.count>0)
        //                     {
        //                         NSArray *packageArray = (NSArray *)responseDict;
        //                         NSDictionary *dictForCompletePackage = [packageArray objectAtIndex:0];
        //
        //                         HolidayPackageDetail *packageDetailHoliday = [[HolidayPackageDetail alloc] initWithDataDict:dictForCompletePackage];
        //
        //                         //  [self fireAppIntHoViewDetailsEvent]; //
        //
        //                         TabMenuVC *tabMenuVC = [[TabMenuVC alloc]initWithNibName:@"BaseViewController" bundle:nil];
        //
        //                         // tabMenuVC.headerName = objHoliday.strPackageName;
        //
        //                         tabMenuVC.flag = @"Overview";
        //                         tabMenuVC.packageDetail = packageDetailHoliday;
        //                         tabMenuVC.completePackageDetail = [NSArray arrayWithObjects:dictForCompletePackage, nil];
        //                         tabMenuVC.payloadDict = payloadList;
        //                         [[SlideNavigationController sharedInstance] pushViewController:tabMenuVC animated:YES];
        //
        //
        //                     }
        //                     else
        //                     {
        //                         [super showAlertViewWithTitle:@"Alert" withMessage:@"some error occurred"];
        //
        //                     }
        //                 }
        //                 else
        //                 {
        //                     [super showAlertViewWithTitle:@"Alert" withMessage:@"some error occurred"];
        //
        //                 }
        //             });
        //
        //         }
        //                                   failure:^(NSError *error)
        //         {
        //             dispatch_async(dispatch_get_main_queue(), ^(void)
        //                            {
        //                                [activityLoadingView removeFromSuperview];
        //                                [super showAlertViewWithTitle:@"Alert" withMessage:@"some error occurred"];
        //                                NSLog(@"Response Dict : %@",error);
        //                            });
        //
        //
        //         }];
        //
        //        }
        //        @catch (NSException *exception)
        //        {
        //            NSLog(@"%@", exception.reason);
        //            [activityLoadingView removeFromSuperview];
        //        }
        //        @finally
        //        {
        //            NSLog(@"exception finally called");
        //        }
        //
        //    }
        //    else
        //    {
        //        [self showAlertViewWithTitle:@"Alert" withMessage:kMessageNoInternet];
        //    }
        
        @try
        {
            
            
            NetworkHelper *helper = [NetworkHelper sharedHelper];
            
            
            NSString *pathParam = strPackageID;
            
            
            NSDictionary *headerDict = [CoreUtility getHeaderDict];
            
            [helper getResponseWithRequestType:@"" withQueryParam:@"" withPathParam:pathParam withJsonParam:nil withHeaders:headerDict withUrl:kAstraUrlPackageDetail success:^(NSDictionary *responseDict)
             {
                 NSLog(@"Response Dict : %@",responseDict);
                 
                 
                 dispatch_async(dispatch_get_main_queue(), ^(void) {
                     [activityLoadingView removeFromSuperview];
                     if (responseDict)
                     {
                         if (responseDict.count>0)
                         {
                             NSArray *packageArray = (NSArray *)responseDict;
                             NSDictionary *dictForCompletePackage = [packageArray objectAtIndex:0];
                             
                             HolidayPackageDetail *packageDetailHoliday = [[HolidayPackageDetail alloc] initWithDataDict:dictForCompletePackage];
                             
                             //  [self fireAppIntHoViewDetailsEvent]; //
                             
                             TabMenuVC *tabMenuVC = [[TabMenuVC alloc]initWithNibName:@"BaseViewController" bundle:nil];
                             
                             // tabMenuVC.headerName = objHoliday.strPackageName;
                             
                             tabMenuVC.flag = @"Overview";
                             tabMenuVC.packageDetail = packageDetailHoliday;
                             tabMenuVC.completePackageDetail = [NSArray arrayWithObjects:dictForCompletePackage, nil];
                             [[SlideNavigationController sharedInstance] pushViewController:tabMenuVC animated:YES];
                             
                         }else{
                             [super showAlertViewWithTitle:@"Alert" withMessage:@"some error occurred"];
                         }
                     }
                 });
                 
             }
                                       failure:^(NSError *error)
             {
                 dispatch_async(dispatch_get_main_queue(), ^(void)
                                {
                                    [activityLoadingView removeFromSuperview];
                                    [super showAlertViewWithTitle:@"Alert" withMessage:@"some error occurred"];
                                    NSLog(@"Response Dict : %@",[error description]);
                                });
                 
                 
             }];
            
        }
        @catch (NSException *exception)
        {
            NSLog(@"%@", exception.reason);
            [activityLoadingView removeFromSuperview];
        }
        @finally
        {
            NSLog(@"exception finally called");
        }
        
    }
}


-(void)getArrayOfBannerImages:(isCompleted)CompletionBlock
{
    NSDictionary *dict = [[NSDictionary alloc]initWithObjectsAndKeys:@"normalScreen", @"screenSize",nil];
    
    
    NetworkHelper *helper = [NetworkHelper sharedHelper];
    
    if ([self connected])
    {
        // dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        //NSString *strResponse = [helper getDataFromServerForType:@"webservice" entity:@"homePageBanner" action:@"search" andUserJson:dict];
        
        NSString *pathParam = @"";
        
        NSDictionary *headerDict = [CoreUtility getHeaderDict];
        NSString *url = @"tcStaticPages/bannerapiservice/getbannerdata/iphone";
        
        [helper getResponseWithRequestType:@"" withQueryParam:@"" withPathParam:pathParam withJsonParam:nil withHeaders:headerDict withUrl:url success:^(NSDictionary *responseDict)
         {
             NSLog(@"Response Dict : %@",responseDict);
             if (responseDict.count != 0) {
                 imagesDataArray = [responseDict valueForKey:@"bannerDetails"];
                 if (imagesDataArray.count == 0)
                 {
                     arrayOfImages  = [[NSMutableArray alloc]init];
                     [activityLoadingView1 removeFromSuperview];
                     // [arrayOfImages addObject:[UIImage imageNamed:@"defaultBanner.png"]];
                     [self viewDidAppear:true];
                 }else{
                     
                     for (int i = 0; i < [imagesDataArray count]; i++)
                     {
                         NSString *strUrl = [[imagesDataArray objectAtIndex:i]valueForKey:@"bannerLargeImage"];
                         if ([strUrl  isEqual: @"NA"])
                         {
                             strUrl = [[imagesDataArray objectAtIndex:i]valueForKey:@"bannerMediumImage"];
                         }
                         if ([strUrl  isEqual: @"NA"])
                         {
                             strUrl = [[imagesDataArray objectAtIndex:i]valueForKey:@"bannerSmallImage"];
                         }
                         
                       /*  NSURL *url = [NSURL URLWithString:strUrl];
                          NSData *data = [NSData dataWithContentsOfURL:url];
                          UIImage *img = [[UIImage alloc] initWithData:data];
                          
                          if (img != nil)
                          {
                          [arrayOfImages addObject:img];
                          }else{
                          [arrayOfImages addObject:[UIImage imageNamed:@"defaultBanner.png"]];
                          } */ //use sdwebimage
                         
                         [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:strUrl] options:SDWebImageDownloaderUseNSURLCache progress:nil completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
                             
                             if (image && finished) {
                                 [arrayOfImages addObject:image];
                                 NSLog(@"image added");
                                 if ([arrayOfImages count] == [imagesDataArray count]){
                                     CompletionBlock(YES);
                                 }
                                 
                             }else{
                                 [arrayOfImages addObject:[UIImage imageNamed:@"defaultBanner.png"]];
                                 if ([arrayOfImages count] == [imagesDataArray count]){
                                     CompletionBlock(YES);
                                 }
                             }
                         }];
                         
                     }
                     //[activityLoadingView removeFromSuperview];
                     
                     NSLog(@"arrofImages%@",arrayOfImages);
                 }
             }else{
                 CompletionBlock(NO);
                 // [self viewDidLoad];
             }
         }
                                   failure:^(NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^(void)
                            {
                                [activityLoadingView removeFromSuperview];
                                [super showAlertViewWithTitle:@"Alert" withMessage:@"some error occurred"];
                                NSLog(@"Response Dict : %@",error);
                            });
             
             
         }];
        
        
        
        
    }
    else
    {
        [self showAlertViewWithTitle:@"Alert" withMessage:kMessageNoInternet];
        flag = NO;
    }
}

-(void)animateAllMenuButtons
{
    
    [UIView animateWithDuration:0.3 animations:^{
        
        self.btnInternationalHoliday.alpha = 1.0f;
        
    } completion:^(BOOL finished){
        
        [UIView animateWithDuration:0.35 animations:^{
            
            self.btnIndianHoliday.alpha = 1.0f;
            
        } completion:^(BOOL finished){
            
            [UIView animateWithDuration:0.35 animations:^{
                
                self.btnFlight.alpha = 1.0f;
                
            } completion:^(BOOL finished){
                
                [UIView animateWithDuration:0.35 animations:^{
                    
                    self.btnForex.alpha = 1.0f;
                    
                }];
            }];
        }];
    }];
}

#pragma mark SlideMenu
-(void)menuDidOpened
{
    payloadList = [[NSMutableDictionary alloc] init];
    [payloadList setObject:@"yes" forKey:@"s^DRAWER"];
    [self netCoreHomePageLogEvent];
    
}

-(void)menuDidClosed
{
    payloadList = [[NSMutableDictionary alloc] init];
    [payloadList setObject:@"no" forKey:@"s^DRAWER"];
    [self netCoreHomePageLogEvent];
    
}


#pragma mark - Slide Navigation Delegate

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    
    return YES;
}

- (BOOL)slideNavigationControllerShouldDisplayRightMenu
{
    return NO;
}


#pragma mark - PSSlideShow delegate

- (void)PSSlideShowWillShowNext:(PSSlideShow *)slideShow
{
    //NSLog(@"kaSlideShowWillShowNext, index : %@",@(slideShow.currentIndex));
}

- (void)PSSlideShowWillShowPrevious:(PSSlideShow *)slideShow
{
    //NSLog(@"kaSlideShowWillShowPrevious, index : %@",@(slideShow.currentIndex));
}

- (void) PSSlideShowDidShowNext:(PSSlideShow *)slideShow
{
    //NSLog(@"kaSlideShowDidNext, index : %@",@(slideShow.currentIndex));
}

-(void)PSSlideShowDidShowPrevious:(PSSlideShow *)slideShow
{
    //NSLog(@"kaSlideShowDidPrevious, index : %@",@(slideShow.currentIndex));
}

#pragma mark- set Slide time

- (void)scrollingTimer {
    
    
    [_slideshow next];
}


#pragma mark- Button Action

- (IBAction)clickedInternationalHolidays:(id)sender {
    
    // [[Branch getInstance] userCompletedAction:@"customAction"];
    //     ForexLandingPageViewController *ForexLandingVc = [[ForexLandingPageViewController alloc] initWithNibName:@"ForexBaseViewController" bundle:nil];
    //      UINavigationController *navVc = [[UINavigationController alloc]initWithRootViewController:ForexLandingVc];
    //     [self presentViewController:navVc animated:YES completion:nil];
    
    [AppUtility checkIsVersionUpgrade];
    
    
    payloadList =  [NSMutableDictionary dictionary];
    SearchHolidaysVC *searchVC=[[SearchHolidaysVC alloc]initWithNibName:@"BaseViewController" bundle:nil];
    searchVC.headerName = kInternationalHoliday;
    [payloadList setObject:@"yes" forKey:@"s^INT_HOLIDAYS"];
    
    [[SlideNavigationController sharedInstance] pushViewController:searchVC animated:YES];
    
    [self netCoreHomePageLogEvent];
}

- (IBAction)clickedIndiaHolidays:(id)sender {
    
    [AppUtility checkIsVersionUpgrade];
    // [[Branch getInstance] userCompletedAction:@"customAction"];
    payloadList =  [NSMutableDictionary dictionary];
    SearchHolidaysVC *searchVC=[[SearchHolidaysVC alloc]initWithNibName:@"BaseViewController" bundle:nil];
    searchVC.headerName = kIndianHoliday;
    [payloadList setObject:@"yes" forKey:@"s^INDIA_HOLIDAYS"];
    [[SlideNavigationController sharedInstance] pushViewController:searchVC animated:YES];
    
    [self netCoreHomePageLogEvent];
}

- (IBAction)clickedFlightsSearch:(id)sender
{
    /* WebViewController *webViewVC = [[WebViewController alloc]initWithNibName:@"BaseViewController" bundle:nil];
     NSString *forexUrl = kUrlForFlights;
     
     // NSString *forexUrl = @"https://thomascookindia--tls.custhelp.com/cgi-bin/thomascookindia.cfg/php/custom/branch_web_lead.php?first_name=xx&last_name=&email=xx@gmail.com&mobile=9022721321&city=xx&packageId==PKG170650&product=INT%20Holiday%20GIT&utmcsr=homepage&utmccn=(not%20set)&utmcmd=(not%20set)&utmctr=undefined&utmcct=undefined&utmgclid=undefined&opp_summary=%20sds&device=desktop";
     
     NSURL *url = [NSURL URLWithString:forexUrl];
     webViewVC.url = url;
     webViewVC.headerString = @"Flight";
     
     payloadList =  [NSMutableDictionary dictionary];
     [payloadList setObject:@"yes" forKey:@"s^FLIGHTS"];
     [[SlideNavigationController sharedInstance] pushViewController:webViewVC animated:NO];
     
     [self netCoreHomePageLogEvent];*/
    
    [AppUtility checkIsVersionUpgrade];
    
    NSUserDefaults *LoginStatus = [NSUserDefaults standardUserDefaults];
    
    if (![[LoginStatus valueForKey:@"isCSSUserLoggedIn"] boolValue]) {
        
        // [AppUtility setLoginController];
        [self onSelfServiceMenuClick];
        
    }
    else
    {
        
        [AppUtility setTabbarController];
    }
    
    payloadList =  [NSMutableDictionary dictionary];
    [payloadList setObject:@"yes" forKey:@"s^SELF_SERVICE"];
    [self netCoreHomePageLogEvent];
    
    
}

#pragma Mark: onSelfServiceMenuClick Delegates
-(void)onSelfServiceMenuClick
{
    [AppUtility checkIsVersionUpgrade];
    
    ManageHolidayPopUpVC* manageHolidayPopUpVC =  [[ManageHolidayPopUpVC alloc] initWithNibName:@"ManageHolidayPopUpVC" bundle:nil];
    
    manageHolidayPopUpVC.htmlPath = @"Manage Holidays";
    manageHolidayPopUpVC.popuptitle = @"Manage Your Booking";
    manageHolidayPopUpVC.managedHolidayDelegate = self;
    [manageHolidayPopUpVC setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    
    [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:manageHolidayPopUpVC animated:false completion:nil];
    
    //  UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
    
    
    //  [AppDelegate presentViewController:manageHolidayPopUpVC animated:false completion:nil];
    
    
    
    /*  let popupVC: PopUpWebViewViewController = PopUpWebViewViewController(nibName: "PopUpWebViewViewController", bundle: nil)
     popupVC.htmlPath = "bookingPolicy"
     popupVC.popuptitle = "Booking Policy"
     popupVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
     self.present(popupVC, animated: false, completion: nil)*/
}

-(void)onContinue
{
    
    [AppUtility setLoginController];
}

-(void)onCancle
{
    
}

- (IBAction)clickedForex:(id)sender
{
    /*  ForexLandingPageViewController *ForexLandingVc = [[ForexLandingPageViewController alloc] initWithNibName:@"ForexBaseViewController" bundle:nil];
     
     //UINavigationController *navVc = [[UINavigationController alloc]initWithRootViewController:ForexLandingVc];
     
     ForexRightSlideViewController *forexRightVC = [[ForexRightSlideViewController alloc] initWithNibName:@"ForexRightSlideViewController" bundle:nil];
     
     SWRevealViewController *revealController = [[SWRevealViewController alloc] init];
     //revealController.delegate = self;
     
     revealController.rightViewController = forexRightVC;
     revealController.frontViewController = ForexLandingVc;
     
     revealController.rightViewRevealWidth = 300;
     
     UINavigationController *navVc = [[UINavigationController alloc]initWithRootViewController:revealController];
     
     [self presentViewController:navVc animated:NO completion:nil];*/
    
    ForexLandingPageViewController *ForexLandingVc = [[ForexLandingPageViewController alloc] initWithNibName:@"ForexBaseViewController" bundle:nil];
    
    [[SlideNavigationController sharedInstance] pushViewController:ForexLandingVc animated:NO];
    
    
    /* WebViewController *webViewVC = [[WebViewController alloc]initWithNibName:@"BaseViewController" bundle:nil];
     NSString *forexUrl = kUrlForForex;
     NSURL *url = [NSURL URLWithString:forexUrl];
     webViewVC.url = url;
     webViewVC.headerString = @"Forex";
     payloadList =  [NSMutableDictionary dictionary];
     [payloadList setObject:@"yes" forKey:@"s^FOREX"];
     [[SlideNavigationController sharedInstance] pushViewController:webViewVC animated:NO];*/
}



- (IBAction)btnSearchClicked:(id)sender
{
    [FIRAnalytics logEventWithName:@"Home_Page_Find_Holidays" parameters:@{kFIRParameterItemID :@"On click of search button on Home Page"}];
    SearchHolidaysVC *searchVC=[[SearchHolidaysVC alloc]initWithNibName:@"BaseViewController" bundle:nil];
    searchVC.boolForSearchPackgScreen=true;
    searchVC.headerName = kIndianHoliday;
    payloadList =  [NSMutableDictionary dictionary];
    [payloadList setObject:@"yes" forKey:@"s^SEARCH"];
    
    [[SlideNavigationController sharedInstance] pushViewController:searchVC animated:YES];
    
    [self netCoreHomePageLogEvent];
}

//- (IBAction)btnSearchClicked:(id)sender {

//    FlipViewController *flipVC = [[FlipViewController alloc]initWithNibName:@"BaseViewController" bundle:nil];
//    [[SlideNavigationController sharedInstance] pushViewController:flipVC animated:YES];

//    CATransition* transition = [CATransition animation];
//    transition.duration = 1;
//    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
//    transition.type = kCATransitionFade; //kCATransitionMoveIn; //, kCATransitionPush, kCATransitionReveal, kCATransitionFade
//    transition.subtype = kCATransitionFromTop; //kCATransitionFromLeft, kCATransitionFromRight, kCATransitionFromTop, kCATransitionFromBottom
//    [[SlideNavigationController sharedInstance].view.layer addAnimation:transition forKey:nil];

//    [UIView  beginAnimations: @"Showinfo"context: nil];
//    [UIView setAnimationCurve: UIViewAnimationCurveEaseInOut];
//    [UIView setAnimationDuration:0.75];
//    [[SlideNavigationController sharedInstance] pushViewController: flipVC animated:NO];
//    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:[SlideNavigationController sharedInstance].view cache:NO];
//    [UIView commitAnimations];

//}

- (IBAction)onNotificationBtnClick:(id)sender
{
    NSArray *arrofNotifications = [[NetCoreSharedManager sharedInstance] getNotifications];
    NSLog(@"arr of notification %@",arrofNotifications);
    
    
    NotificationViewController *calenderVC = [[NotificationViewController alloc]initWithNibName:@"BaseViewController" bundle:nil];
    [[SlideNavigationController sharedInstance] pushViewController:calenderVC animated:YES];
    payloadList =  [NSMutableDictionary dictionary];
    [payloadList setObject:@"yes" forKey:@"s^NOTIFICATIONS"];
    [self netCoreHomePageLogEvent];
    
}

- (IBAction)onOffersDealsButtonClicked:(id)sender
{
    WebViewController *webViewVC = [[WebViewController alloc]initWithNibName:@"BaseViewController" bundle:nil];
    NSString *forexUrl = kpgURLForDeals;
    NSURL *url = [NSURL URLWithString:forexUrl];
    webViewVC.url = url;
    webViewVC.headerString = @"Deals & Offers";
    payloadList =  [NSMutableDictionary dictionary];
    [payloadList setObject:@"yes" forKey:@"s^DEALS"];
    [[SlideNavigationController sharedInstance] pushViewController:webViewVC animated:NO];
    
    [self netCoreHomePageLogEvent];
}

- (id) init
{
    self = [super init];
    if (self)
    {
        
    }
    return self;
}
#pragma mark NetCore log Events

//App_Home
-(void)netCoreHomePageLogEvent
{
    //    if ([super slideMenuBool])
    //    {
    //        [payloadList setObject:@"yes" forKey:@"s^DRAWER"];
    //    }
    
    if(![payloadList objectForKey:@"s^BANNER"])
    {
        [payloadList setObject:@"" forKey:@"s^BANNER"];
    }
    if (![payloadList objectForKey:@"s^SEARCH"])
    {
        [payloadList setObject:@"no" forKey:@"s^SEARCH"];
    }
    if (![payloadList objectForKey:@"s^DEALS"])
    {
        [payloadList setObject:@"no" forKey:@"s^DEALS"];
    }
    if (![payloadList objectForKey:@"s^DRAWER"])
    {
        [payloadList setObject:@"no" forKey:@"s^DRAWER"];
    }
    if (![payloadList objectForKey:@"s^NOTIFICATIONS"])
    {
        [payloadList setObject:@"no" forKey:@"s^NOTIFICATIONS"];
    }
    if (![payloadList objectForKey:@"s^INT_HOLIDAYS"])
    {
        [payloadList setObject:@"no" forKey:@"s^INT_HOLIDAYS"];
    }
    if (![payloadList objectForKey:@"s^INDIA_HOLIDAYS"])
    {
        [payloadList setObject:@"no" forKey:@"s^INDIA_HOLIDAYS"];
    }
    if (![payloadList objectForKey:@"s^FLIGHTS"])
    {
        // [payloadList setObject:@"no" forKey:@"s^FLIGHTS"]; //09-04-2018
        [payloadList setObject:[NSNull null] forKey:@"s^FLIGHTS"];  //09-04-2018
    }
    if (![payloadList objectForKey:@"s^FOREX"])
    {
        [payloadList setObject:@"no" forKey:@"s^FOREX"];
    }
    if (![payloadList objectForKey:@"s^SELF_SERVICE"])
    {
        [payloadList setObject:@"no" forKey:@"s^SELF_SERVICE"];
    }
    if (![payloadList objectForKey:@"s^SOURCE"])
    {
        [payloadList setObject:@"App" forKey:@"s^SOURCE"];
    }
    
    //[NetCoreAnalyticsVC getNetCorePushNotificationDetailDictionary:payloadList withPayloadCount:102]; //28-02-2018
    
    [NetCoreAnalyticsVC getNetCorePushNotificationDetailDictionary:payloadList withPayloadCount:118]; //28-02-2018
}

//APP_VIEW_DETAILS
-(void)fireAppIntHoViewDetailsEvent
{
    
    [payLoadForViewDetails setObject:packageDetail.strPackageName forKey:@"s^DESTINATION"];
    [payLoadForViewDetails setObject:[NSString stringWithFormat:@"%d",packageDetail.durationNoDays] forKey:@"s^DURATION"];
    [payLoadForViewDetails setObject:[NSString stringWithFormat:@"%d",packageDetail.packagePrise] forKey:@"s^BUDGET"];
    [payLoadForViewDetails setObject:packageDetail.strPackageId forKey:@"s^PACKAGE_ID"];
    [payLoadForViewDetails setObject:@"App" forKey:@"s^SOURCE"];
    [payLoadForViewDetails setObject:[NSNumber numberWithInt:packageDetail.packagePrise] forKey:@"i^PACKAGE_PRICE"];
    
    if ([[packageDetail.strPackageSubType lowercaseString]isEqualToString:@"git"])
    {
        [payLoadForViewDetails setObject:@"no" forKey:@"s^PERSONALIZED_TOUR"];
    }
    else
    {
        [payLoadForViewDetails setObject:@"yes" forKey:@"s^PERSONALIZED_TOUR"];
    }
    
    if(packageDetail.strPackageType != nil && ![packageDetail.strPackageType isEqualToString:@""])
    {
        [payLoadForViewDetails setObject:packageDetail.strPackageType forKey:@"s^TYPE"]; //28-02-2018
    }
    
    //[NetCoreAnalyticsVC getNetCorePushNotificationDetailDictionary:payLoadForViewDetails withPayloadCount:105];
    
    [NetCoreAnalyticsVC getNetCorePushNotificationDetailDictionary:payLoadForViewDetails withPayloadCount:125]; //28-02-2018
    
}

#pragma mark - Astra Services -

-(void)getTokenID
{
    
    if ([AppUtility checkNetConnection]) {
    
    @try
    {
        
        if ([SSKeychain passwordForService:KEYCHAIN_SERVICE account:KEYCHAIN_ACCOUNT] == nil)
        {
            NSString *deviceId = [MobiculeUtilityManager getApplicationUUID];
            [SSKeychain setPassword:deviceId forService:KEYCHAIN_SERVICE account:KEYCHAIN_ACCOUNT];
        }
        
        
        NetworkHelper *networkHelper = [[NetworkHelper alloc] init];
        
        NSDictionary *headerDict = [[NSDictionary alloc] initWithObjectsAndKeys:[SSKeychain passwordForService:KEYCHAIN_SERVICE account:KEYCHAIN_ACCOUNT],@"uniqueId",@"mobicule",@"user", nil];
        
        activityLoadingViewRT = [LoadingView loadingViewInView:self.viewForLandingPage.superview withString:@"" andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            [networkHelper  getResponseWithRequestType:@"" withQueryParam:@"" withPathParam:@"" withJsonParam:nil withHeaders:headerDict withUrl:kAstraUrlNewToken success:^(NSDictionary *responseDict)
             {
                 dispatch_async(dispatch_get_main_queue(), ^(void) {
                     
                     [activityLoadingViewRT removeFromSuperview];
                     
                     NSLog(@"%@",responseDict);
                     
                     if(responseDict)
                     {
                         
                         if (responseDict.count > 0)
                         {
                             NSDictionary *requestId  = [responseDict valueForKey:@"requestId"];
                             NSDictionary *tokenId  = [responseDict valueForKey:@"tokenId"];
                             
                             if (requestId)
                             {
                                 [[NSUserDefaults standardUserDefaults] setObject:requestId forKey:kuserDefaultRequestId];
                                 
                             }
                             
                             if (tokenId)
                             {
                                 [[NSUserDefaults standardUserDefaults] setObject:tokenId forKey:kuserDefaultTokenId];
                                 
                             }
                         }
                         else
                         {
                             [super showAlertViewWithTitle:@"Alert" withMessage:@"some error occurred"];
                         }
                     }
                     else
                     {
                         [super showAlertViewWithTitle:@"Alert" withMessage:@"some error occurred"];
                     }
                 });
             }
                                               failure:^(NSError *error)
             {
                 dispatch_async(dispatch_get_main_queue(), ^(void)
                                {
                                    [activityLoadingViewRT removeFromSuperview];
                                    [super showAlertViewWithTitle:@"Alert" withMessage:@"some error occurred"];
                                    NSLog(@"Response Dict : %@",[error description]);
                                });
             }];
            
        });
    }
    @catch (NSException *exception)
    {
        NSLog(@"%@", exception.reason);
        [activityLoadingViewRT removeFromSuperview];
    }
    @finally
    {
        NSLog(@"exception finally called");
        //[activityLoadingViewRT removeFromSuperview];
    }
    }else{
        [activityLoadingView removeFromSuperview];
    }
}



@end

