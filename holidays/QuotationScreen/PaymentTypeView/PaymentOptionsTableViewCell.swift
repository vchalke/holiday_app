//
//  PaymentOptionsTableViewCell.swift
//  sotc-consumer-application
//
//  Created by Mac on 21/08/18.
//  Copyright © 2018 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit

class PaymentOptionsTableViewCell: UITableViewCell,UITextFieldDelegate {
    
    @IBOutlet weak var payFullPaymentRadioButton: UIButton!
    @IBOutlet weak var payAdvanceRadioButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var amountTextField: UITextField!
    @IBOutlet weak var EnterAmountRadioButton: UIButton!
    @IBOutlet weak var advanceAmountLabel: UILabel!
    @IBOutlet weak var cellHeightForAmount: NSLayoutConstraint!
    @IBOutlet weak var view_AdvancePay: UIView!
    @IBOutlet weak var view_FullPay: UIView!
    @IBOutlet weak var view_AmountToPay: UIView!
    var quotationView : QuotationView = QuotationView.init()
    var pricingModel : PricingModel =  PricingModel.init()
    
    @IBOutlet weak var fullAmountLabel: UILabel!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        self.containerView.fadedShadow()
        self.amountTextField.fadedShadow()
        self.amountTextField.delegate = self
        self.amountTextField.addDoneButtonOnKeyboard()
        self.initializeRadioButton()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func setData(quotationView : QuotationView,pricingModel : PricingModel , pkgDetails:PackageDetailsModel)
    {
        if ((pkgDetails.isAdvancePayable).uppercased() == "Y"){
            self.pricingModel.advancePayableAmount = self.pricingModel.advancePayableFixedAmount
        }
        let numFormatter : NumberFormatter = NumberFormatter.init()
       // numFormatter.positiveFormat = "0"
        numFormatter.numberStyle = NumberFormatter.Style.decimal
        numFormatter.locale = Locale(identifier: "en_IN")
        
        //self.advanceAmountLabel.text = "₹ " + (numFormatter.string(from: NSNumber.init(value:pricingModel.advancePayableAmount)) ?? "0")
        
//        let totalTourCost : Int = Int(pricingModel.totalTourCost) ?? 0 //db fr
//        let main_string = "₹ " + (numFormatter.string(from: NSNumber.init(value: totalTourCost)) ?? "0")
//        var string_to_color = (numFormatter.string(from: NSNumber.init(value:Int(pricingModel.totalTourCost) ?? 0)) ?? "0") //db fr
        
        let totalTourCost = pricingModel.applyPromocodeSuccess ? (Int(pricingModel.totalTourCost) ?? 15000) - Int(pricingModel.promocodeAmount) : Int(pricingModel.totalTourCost) ?? 0
        let main_string = "₹ " + (numFormatter.string(from: NSNumber.init(value: totalTourCost)) ?? "0")
        var string_to_color = (numFormatter.string(from: NSNumber.init(value:totalTourCost)) ?? "0") //db fr
        
        var range = (main_string as NSString).range(of: string_to_color)
        
//        let redColour = UIColor.init(red: 209/255, green: 0/255, blue: 12/255, alpha: 1)
        let blueColour = UIColor.init(red: 0/255, green: 84/255, blue: 165/255, alpha: 1)
        var attributedString = NSMutableAttributedString(string:main_string)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value:blueColour , range: range)  //red color
        self.fullAmountLabel.attributedText = attributedString
        
        let advanceAmount = "₹ " + (numFormatter.string(from: NSNumber.init(value:Int(pricingModel.advancePayableAmount))) ?? "0")
        string_to_color = (numFormatter.string(from: NSNumber.init(value:Int(pricingModel.advancePayableAmount))) ?? "0")
        range = (advanceAmount as NSString).range(of: string_to_color)
        attributedString = NSMutableAttributedString(string:advanceAmount)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: blueColour , range: range)  //red color
        self.advanceAmountLabel.attributedText = attributedString
//        self.view_AdvancePay.isHidden = true  // VJ_Added
//        self.view_FullPay.isHidden = true // VJ_Added
        self.view_AmountToPay.isHidden = true // VJ_Added
        if advanceAmount == "₹ 0"
        {
            self.advanceAmountLabel.superview?.isHidden = true
            self.amountTextField.superview?.isHidden = true
            self.cellHeightForAmount.constant = 80
        }
        else
        {
            self.advanceAmountLabel.superview?.isHidden = false
            self.amountTextField.superview?.isHidden = false
        }
//        self.advanceAmountLabel.superview?.isHidden = true
//        self.fullAmountLabel.superview?.isHidden = true  // VJ_Added
        self.amountTextField.superview?.isHidden = true  // VJ_Added
//        self.quotationView.selectedPaymentType = "ADVANCE" // Manually Done For Advance Payment
        if self.quotationView.selectedPaymentType == "ADVANCE"
        {
            self.selectAdvance()
        }
        else if self.quotationView.selectedPaymentType == "FULL_PAYMENT"
        {
            self.selectFullPayment()
        }
        else
        {
            self.selectUsersChoice()
        }
    }
    func initializeRadioButton()
    {
        self.payAdvanceRadioButton.setBackgroundImage(UIImage.init(named: "circularBoxNoSelectBlue"), for: .normal)
        self.payAdvanceRadioButton.setBackgroundImage(UIImage.init(named: "circularBoxSelectBlue"), for: .selected)
        
        self.payFullPaymentRadioButton.setBackgroundImage(UIImage.init(named: "circularBoxNoSelectBlue"), for: .normal)
        self.payFullPaymentRadioButton.setBackgroundImage(UIImage.init(named: "circularBoxSelectBlue"), for: .selected)
        
        self.EnterAmountRadioButton.setBackgroundImage(UIImage.init(named: "circularBoxNoSelectBlue"), for: .normal)
        self.EnterAmountRadioButton.setBackgroundImage(UIImage.init(named: "circularBoxSelectBlue"), for: .selected)
        
        self.payAdvanceRadioButton.isSelected = false
        self.payFullPaymentRadioButton.isSelected = false
        self.EnterAmountRadioButton.isSelected = false
        
    }
    //MARK:-  textfield Delegates -

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.amountTextField.endEditing(true)
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let numeric : String = "0123456789"
        
        if numeric.contains(string) || string == ""
        {
            var str : String = self.amountTextField.text ?? ""

            if string == ""
            {
                if str.count > 0
                {
                    str.removeLast()
                    self.amountTextField.text = str
                }
            }
            else
            {
               str  = (self.amountTextField.text ?? "") + string
            }
            
            let enteredAmount : Double = Double.init(str ?? "") ?? 0.0
            let totalCost : Double = Double.init(self.pricingModel.totalTourCost ?? "") ?? 0.0
            if enteredAmount > totalCost
            {
                AppUtility.displayAlert(title: "Alert", message: "Amount cannot be greater than full amount.")
            }
            else
            {
                self.amountTextField.text = str
                self.quotationView.usersChoiceAmount = enteredAmount
                self.selectUsersChoice()
            }
            return false
        }
        return false
    }
      //MARK:-  radiobutton methods -
    @IBAction func radioButtonClicked(_ sender: Any)
    {
        let button : UIButton = sender as! UIButton
        if self.payAdvanceRadioButton == button
        {
            self.selectAdvance()
        }
        else if self.payFullPaymentRadioButton == button
        {
            self.selectFullPayment()
        }
        else if self.EnterAmountRadioButton == button
        {
            self.selectUsersChoice()
        }
    }
    func selectAdvance()
    {
        self.payAdvanceRadioButton.isSelected = true
        self.payFullPaymentRadioButton.isSelected = false
        self.EnterAmountRadioButton.isSelected = false
        self.quotationView.selectedPaymentType = "ADVANCE"
        self.quotationView.selectedPaymentAmount = self.pricingModel.advancePayableAmount
    }
    
    func selectFullPayment()
    {
        self.payAdvanceRadioButton.isSelected = false
        self.payFullPaymentRadioButton.isSelected = true
        self.EnterAmountRadioButton.isSelected = false
        self.quotationView.selectedPaymentType = "FULL_PAYMENT"
//        let totalcost = Double(self.pricingModel.totalTourCost) ?? 0.0 //db fr
//        self.quotationView.selectedPaymentAmount = totalcost
        let totalcost = pricingModel.applyPromocodeSuccess ? (Int(pricingModel.totalTourCost) ?? 15000) - Int(pricingModel.promocodeAmount) : Int(pricingModel.totalTourCost) ?? 0
        self.quotationView.selectedPaymentAmount = Double(totalcost)
    }
    
    func selectUsersChoice()
    {
        self.payAdvanceRadioButton.isSelected = false
        self.payFullPaymentRadioButton.isSelected = false
        self.EnterAmountRadioButton.isSelected = true
        self.quotationView.selectedPaymentType = "USER_CHOICE"
        self.quotationView.selectedPaymentAmount = self.quotationView.usersChoiceAmount
    }
    func hideConatentView(flags:Bool){
        self.containerView.isHidden = flags
    }
}
