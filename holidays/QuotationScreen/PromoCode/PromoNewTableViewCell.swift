//
//  PromoNewTableViewCell.swift
//  holidays
//
//  Created by Kush_Team on 25/06/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//
//let colour = UIColor.init(red: 0/255, green: 78/255, blue: 155/255, alpha: 1)//Blue
import UIKit
protocol PromoNewTableViewCellDelegate {
    func reloadTableInPromo()
    func fetchPromocodePricingByDelegate(promocodeText : String, flag:Bool)
    func submitPromocodePricingByDelegate(promocodeText : String)
}
class PromoNewTableViewCell: UITableViewCell,UITextFieldDelegate {

    @IBOutlet weak var promocodeTextfield: UITextField!
    @IBOutlet weak var applyButton: UIButton!
    var quotationViewController : QuotationView = QuotationView.init()
    var pricingModel:PricingModel = PricingModel()
    var delegates : PromoNewTableViewCellDelegate? = nil
    
    override func awakeFromNib() {
            super.awakeFromNib()
            promocodeTextfield.delegate = self
        }
        
        override func setSelected(_ selected: Bool, animated: Bool) {
            super.setSelected(selected, animated: animated)
            
            // Configure the view for the selected state
        }
        func validateFields() -> String
        {
            if self.quotationViewController.selectedPaymentType == "USER_CHOICE" && self.quotationViewController.selectedPaymentAmount == 0.0
            {
                
                return "Please enter valid amount or select another payment type."
            }
            if (self.quotationViewController.selectedPaymentType == "USER_CHOICE" && self.quotationViewController.selectedPaymentAmount < self.pricingModel.advancePayableAmount)
            {
                AppUtility.displayAlert(title: "Alert", message: "Amount cannot be less than advance amount.")
            }
            if self.promocodeTextfield.text?.trimWhiteSpace() == ""
            {
                return "Please enter promocode."
            }
            return ""
        }
        @IBAction func onClickOfApplyButton(_ sender: Any)
        {
            let msg : String = self.validateFields()
            if msg != ""
            {
                AppUtility.displayAlert(title: "Alert", message: msg)
            }
            else
            {
//                self.submitPromocode()
                
//                if (self.pricingModel.applyPromocodeSuccess){
//                    AppUtility.displayToastMessage("You Applied Promo Code Already")
//                }else{
                self.delegates?.fetchPromocodePricingByDelegate(promocodeText: self.promocodeTextfield.text ?? "", flag: true)
//                }
            }
        }
        func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            textField.resignFirstResponder()
            return true
        }
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.pricingModel.applyPromocodeSuccess = (textField.text?.count ?? 0 > 0) ? true : false
        self.pricingModel.promocodeAppliedText = (textField.text?.count ?? 0 > 0) ? self.promocodeTextfield.text ?? "" : ""
    }
        /*
        func fetchPricingByPromocode()
        {
            /*Request -
             {
             "promocode": "SBI3K",
             "onBehalf": "N",              {if logged in - user id else Guest_User}
             "userId": "Guest_User",        {if logged in - roleId else 2 }
             "roleId": "2",
             "quoteId": 784097,
             "paymentType": "F"            (A- advance, F- full - U- user defined)
             }*/
            
            
            
            /* LOGIN Response
             {
             "invalidLoginCount" : 0,
             "message" : "true",
             "userAddressList" : [
             
             ],
             "userDetail" : {
             "custId" : "1023",
             "roleId" : 2,
             "userId" : "parshwanath.chougule@mobicule.com",
             "mobileNo" : "7057959513",
             "age" : 0,
             "fname" : "party",
             "accountType" : "TC",
             "email" : "parshwanath.chougule@mobicule.com",
             "lName" : "chougule",
             "role" : "B2C Customer",
             "userTypeId" : "Customer"
             },
             "accountType" : "TC",
             "familyTreeList" : [
             
             ]
             }*/
            LoadingIndicatorView.show("Loading")
            var dataDict : Dictionary<String,Any> = Dictionary.init()//
            dataDict["promocode"] = self.promocodeTextfield.text
            dataDict["onBehalf"] = "N"
            let userDetails : Dictionary<String,Any>? = UserDefaults.standard.dictionary(forKey: Constant.userProfile.userDetails)
            if (userDetails != nil)
            {
                dataDict["userId"] = userDetails!["userId"] as? String ?? ""
                dataDict["roleId"] = "\(userDetails!["roleId"] as? Int ?? 0)"
            }
            else
            {
                dataDict["userId"] = "Guest_User"
                dataDict["roleId"] = "2"
            }
            
            
            dataDict["quoteId"] = self.quotationViewController.quoteID
            
            if self.quotationViewController.selectedPaymentType == "ADVANCE"
            {
                dataDict["paymentType"] = "A"
            }
            else if self.quotationViewController.selectedPaymentType == "FULL_PAYMENT"
            {
                dataDict["paymentType"] = "F"
            }
            else
            {
                dataDict["paymentType"] = "U"
            }
            
            printLog("Promocode request : ", dataDict.jsonRepresentation())
            //https://services-uatastra.sotc.in/holidayRS/pdpPromocode
    //        "holidayRS/holidayBooking/pdpPromocode"//In SOTC
    //        @"tcHolidayRS/quotation/promocode"// IN TC
            PreBookingCommunicationManager.getPrebookingData(requestType:"POST", queryParam: nil, pathParam:nil, jsonParam: dataDict, headers: nil, requestURL: "holidayRS/holidayBooking/pdpPromocode", returnInCaseOffailure: false)
            {
                (status,response) in
                
                printLog("Status is \(status)")
                
                printLog("Response is \(String(describing: response))")
                if let object = response as? Dictionary<String,Any>
                {
                    
                    let promoCodeAmount : Dictionary<String,Any> = object["promoCodeAmount"] as? Dictionary<String,Any> ?? Dictionary.init()
                    let discount : Double = promoCodeAmount["discountAmount"] as? Double ?? 0.0
                    if discount !=  0
                    {
                         AppUtility.displayToastMessage("Promo Code Applied.")
                        AppUtility.fillPricingModelWithResponseDict(dict: object, pricingModel: self.pricingModel, quotationView: self.quotationViewController)
                        LoadingIndicatorView.hide()
                        self.quotationViewController.expandPromocodeView = false
                        self.delegate?.reloadTable()
                        self.submitPromocode() //changes on 12-04-19
                        
                    }
                    else
                    {
                        
                        let promoCodeAmount : Dictionary<String,Any> = object["promoCodeAmount"] as? Dictionary<String,Any> ?? Dictionary.init()
                        AppUtility.displayAlert(title: "Alert", message: promoCodeAmount["errorMsg"] as? String ?? "Some Error Occurred")
                    }
                   
                    
                }
            }
        }
        */
        
    //    VJ_Addded
        func fetchPricingByPromocode()
            {
                LoadingIndicatorView.show("Loading")
                var dataDict : Dictionary<String,Any> = Dictionary.init()//
                /*
                {
                    appUsed = Desktop;
                    bookingAmount = "30000.000000";
                    bookingAmountType = F;
                    promoCode = "";
                    quotationId = "CMgNfwHVWyzjEJESQmkm%2FA%3D%3D";
                    userId = "jchalke@gmail.com";
                }
                */
//                {&quot;promocode&quot;:&quot;kashtest&quot;,&quot;onBehalf&quot;:&quot;N&quot;,&quot;userId&quot;:&quot;Guest_User&quot;,&quot;roleId&quot;:&quot;2&quot;,&quot;quoteId&quot;:&quot;zERhrrAn5P
//                ihtDBX4ifYaQ%3D%3D&quot;,&quot;paymentType&quot;:&quot;F&quot;,&quot;appUsed&quot;:&quot;Desktop&quot;}
                
                dataDict["promocode"] = self.promocodeTextfield.text
//                dataDict["promocode"] = "kashtest"
                dataDict["onBehalf"] = "N"
                
                let userDetails : Dictionary<String,Any>? = UserDefaults.standard.dictionary(forKey: Constant.userProfile.userDetails)
                if (userDetails != nil){
                    dataDict["userId"] = userDetails!["userId"] as? String ?? ""
                    dataDict["roleId"] = "\(userDetails!["roleId"] as? Int ?? 0)"
                }else{
                    dataDict["userId"] = "Guest_User"
                    dataDict["roleId"] = "2"
                }
 
                dataDict["quoteId"] = self.quotationViewController.quoteID
              
                if self.quotationViewController.selectedPaymentType == "ADVANCE"{
                    dataDict["paymentType"] = "A"
                }else if self.quotationViewController.selectedPaymentType == "FULL_PAYMENT"{
                    dataDict["paymentType"] = "F"
                }else{
                    dataDict["paymentType"] = "U"
                }
//                dataDict["appUsed"] = "Desktop"
                  dataDict["appUsed"] = "APP"
//                self.quotationViewController.selectedPaymentType = "FULL_PAYMENT"
//                self.pricingModel.totalTourCost = "30000"
//                dataDict["bookingAmountType"] = "A"
//                dataDict["bookingAmount"] = "30000"
                
    //            "holidayRS/holidayBooking/pdpPromocode" //kAstraUrlPromocode , @"tcHolidayRS/quotation/promocode"
                printLog("Promocode Apply Service Dict : ", dataDict.jsonRepresentation())
                PreBookingCommunicationManager.getPrebookingData(requestType:"POST", queryParam: nil, pathParam:nil, jsonParam: dataDict, headers: nil, requestURL: kAstraPdpPromocodeURL, returnInCaseOffailure: false)
                {
                    (status,response) in
                    
                    printLog("Status is \(status)")
                    
                    printLog("Response is \(String(describing: response))")
                    if let object = response as? Dictionary<String,Any>
                    {
                        printLog("PromoCodeApplied is \(object)")
                        let promoCodeAmount : Dictionary<String,Any> = object["promoCodeAmount"] as? Dictionary<String,Any> ?? Dictionary.init()
                        let discount : Double = promoCodeAmount["discountAmount"] as? Double ?? 0.0
                        if discount !=  0
                        {
                            LoadingIndicatorView.hide()
                             AppUtility.displayToastMessage("Promo Code Applied.")
                            //Hide when dont update Quote Scren Parameter
//                            AppUtility.fillPricingModelWithResponseDict(dict: object, pricingModel: self.pricingModel, quotationView: self.quotationViewController)
                            self.pricingModel.promocodeFixedAmount = discount // Set API REponse AMount
                            self.pricingModel.discountAdvancePrice = self.pricingModel.promocodeFixedAmount
                            self.pricingModel.promocodeAmount = self.pricingModel.promocodeFixedAmount
                            self.pricingModel.promocodeAppliedText = self.promocodeTextfield.text ?? ""
                            self.pricingModel.applyPromocodeSuccess = true
                            self.delegates?.reloadTableInPromo()
//                            self.submitPromocode() //changes on 12-04-19
                            printLog("Promo Code Applied Two")
                        }
                        else
                        {
                            
                            let promoCodeAmount : Dictionary<String,Any> = object["promoCodeAmount"] as? Dictionary<String,Any> ?? Dictionary.init()
//                            AppUtility.displayAlert(title: "Alert", message: promoCodeAmount["errorMsg"] as? String ?? "Some Error Occurred")
                            
                            AppUtility.displayToastMessage(promoCodeAmount["errorMsg"] as? String ?? "Promocode Not Valid")
                        }
                       
                        
                    }else{
                        AppUtility.displayToastMessage("Promocode Not Valid")
                    }
                }
            }
        func submitPromocode()
        {
            //LoadingIndicatorView.show("Loading")
            var dataDict : Dictionary<String,Any> = Dictionary.init()//
            dataDict["promoCode"] = self.promocodeTextfield.text
            dataDict["onBehalf"] = "N"
            let userDetails : Dictionary<String,Any>? = UserDefaults.standard.dictionary(forKey: Constant.userProfile.userDetails)
            if (userDetails != nil)
            {
                dataDict["userId"] = userDetails!["userId"] as? String ?? ""
                dataDict["roleId"] = "\(userDetails!["roleId"] as? Int ?? 0)"
            }
            else
            {
                dataDict["userId"] = "Guest_User"
                dataDict["roleId"] = "2"
            }
            
            
            dataDict["quotationId"] = self.quotationViewController.quoteID
            
            if self.quotationViewController.selectedPaymentType == "ADVANCE"
            {
                dataDict["bookingAmountType"] = "A"
            }
            else if self.quotationViewController.selectedPaymentType == "FULL_PAYMENT"
            {
                dataDict["bookingAmountType"] = "F"
            }
            else
            {
                dataDict["bookingAmountType"] = "U"
            }
            dataDict["bookingAmount"] = self.quotationViewController.selectedPaymentAmount;
            
            printLog("Promocode request : ", dataDict.jsonRepresentation())
            //https://services-uatastra.sotc.in/holidayRS/pdpPromocode
            PreBookingCommunicationManager.getPrebookingData(requestType:"POST", queryParam: nil, pathParam:nil, jsonParam: dataDict, headers: nil, requestURL: kAstraUrlPromocode, returnInCaseOffailure: false)
            {
                (status,response) in
                
                printLog("Status is \(status)")
                
                printLog("Response is \(String(describing: response))")
                if (status){
                    if let object = response as? Dictionary<String,Any>
                    {
                        if let msg = object[ "message"] as? String{
                            if (msg.uppercased()=="SUCCESS"){
                                AppUtility.displayToastMessage("Promo Code Applied.")
                                self.pricingModel.discountAdvancePrice = self.pricingModel.promocodeFixedAmount
                                self.pricingModel.promocodeAmount = self.pricingModel.promocodeFixedAmount
//                                let isAfterPromoApplyAmount = "\(Double((Int(self.pricingModel.totalTourCost) ?? 15000)) - self.pricingModel.promocodeAmount)"
//                                let isAfterPromoApplyAmount = "\((Double(self.pricingModel.totalTourCost) ?? 15000.0) - self.pricingModel.promocodeAmount)"
//                                self.pricingModel.totalTourCost = isAfterPromoApplyAmount
                                self.pricingModel.applyPromocodeSuccess = true
                                self.delegates?.reloadTableInPromo()
                                printLog("Promo Code Applied One")
                            }
                        }else{
                           AppUtility.displayToastMessage("Promo Code Failed.")
                        }
                    }else{
                       AppUtility.displayToastMessage("Promo Code Failed.")
                    }
                }
               
               
            }
        }
    }
