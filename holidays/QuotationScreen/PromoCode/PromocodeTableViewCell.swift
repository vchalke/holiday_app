//
//  PromocodeTableViewCell.swift
//  sotc-consumer-application
//
//  Created by Mac on 25/09/18.
//  Copyright © 2018 Mobicule Technologies Private Limited. All rights reserved.
//  //SIMAUG8k

import UIKit
protocol PromocodeTableViewCellDelegate {
    func reloadTable()
}
class PromocodeTableViewCell: UITableViewCell,UITextFieldDelegate {
    
    @IBOutlet weak var viewForRoundedcorner: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var promocodeTextfield: UITextField!
    @IBOutlet weak var applyButton: UIButton!
    var quotationViewController : QuotationView = QuotationView.init()
    var pricingModel:PricingModel = PricingModel()
    var delegate : PromocodeTableViewCellDelegate? = nil
    override func awakeFromNib() {
        super.awakeFromNib()
        self.containerView.fadedShadow()
        promocodeTextfield.delegate = self
        //self.applyButton.layer.borderWidth = 1.5
//        self.applyButton.layer.borderColor = UIColor.init(red: 209/255, green: 0/255, blue: 12/255, alpha: 1).cgColor 
         
        let layer = CAShapeLayer()
        let x = (UIApplication.shared.keyWindow?.frame.width)! - 15 - 100 - 15
        let frame = CGRect.init(x: x /*self.applyButton.frame.origin.x + 40*/, y:  self.applyButton.frame.origin.y, width:  self.applyButton.frame.width, height:  self.applyButton.frame.height)
        layer.path = UIBezierPath(roundedRect:frame , byRoundingCorners:[.topRight,.bottomRight], cornerRadii: CGSize(width: 8, height: 8)).cgPath
        layer.strokeColor = AppUtility.hexStringToUIColor(hex: "CC0000").cgColor
        layer.borderWidth = 3
        layer.fillColor = nil
        layer.zPosition = self.containerView.layer.zPosition + 1
       // self.bringSubview(toFront: self.applyButton)
        self.containerView.layer.insertSublayer(layer, at: 0)
        self.applyButton.layer.borderColor = UIColor.init(red: 0/255, green: 78/255, blue: 155/255, alpha: 1).cgColor
        //self.layer.insertSublayer(layer, at: 0)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func validateFields() -> String
    {
        if self.quotationViewController.selectedPaymentType == "USER_CHOICE" && self.quotationViewController.selectedPaymentAmount == 0.0
        {
            
            return "Please enter valid amount or select another payment type."
        }
        if (self.quotationViewController.selectedPaymentType == "USER_CHOICE" && self.quotationViewController.selectedPaymentAmount < self.pricingModel.advancePayableAmount)
        {
            AppUtility.displayAlert(title: "Alert", message: "Amount cannot be less than advance amount.")
        }
        if self.promocodeTextfield.text?.trimWhiteSpace() == ""
        {
            return "Please enter promocode."
        }
        return ""
    }
    @IBAction func onClickOfApplyButton(_ sender: Any)
    {
        let msg : String = self.validateFields()
        if msg != ""
        {
            AppUtility.displayAlert(title: "Alert", message: msg)
        }
        else
        {
            self.fetchPricingByPromocode()
           // self.submitPromocode()
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    /*
    func fetchPricingByPromocode()
    {
        /*Request - 
         {
         "promocode": "SBI3K",
         "onBehalf": "N",              {if logged in - user id else Guest_User}
         "userId": "Guest_User",        {if logged in - roleId else 2 }
         "roleId": "2",
         "quoteId": 784097,
         "paymentType": "F"            (A- advance, F- full - U- user defined)
         }*/
        
        
        
        /* LOGIN Response
         {
         "invalidLoginCount" : 0,
         "message" : "true",
         "userAddressList" : [
         
         ],
         "userDetail" : {
         "custId" : "1023",
         "roleId" : 2,
         "userId" : "parshwanath.chougule@mobicule.com",
         "mobileNo" : "7057959513",
         "age" : 0,
         "fname" : "party",
         "accountType" : "TC",
         "email" : "parshwanath.chougule@mobicule.com",
         "lName" : "chougule",
         "role" : "B2C Customer",
         "userTypeId" : "Customer"
         },
         "accountType" : "TC",
         "familyTreeList" : [
         
         ]
         }*/
        LoadingIndicatorView.show("Loading")
        var dataDict : Dictionary<String,Any> = Dictionary.init()//
        dataDict["promocode"] = self.promocodeTextfield.text
        dataDict["onBehalf"] = "N"
        let userDetails : Dictionary<String,Any>? = UserDefaults.standard.dictionary(forKey: Constant.userProfile.userDetails)
        if (userDetails != nil)
        {
            dataDict["userId"] = userDetails!["userId"] as? String ?? ""
            dataDict["roleId"] = "\(userDetails!["roleId"] as? Int ?? 0)"
        }
        else
        {
            dataDict["userId"] = "Guest_User"
            dataDict["roleId"] = "2"
        }
        
        
        dataDict["quoteId"] = self.quotationViewController.quoteID
        
        if self.quotationViewController.selectedPaymentType == "ADVANCE"
        {
            dataDict["paymentType"] = "A"
        }
        else if self.quotationViewController.selectedPaymentType == "FULL_PAYMENT"
        {
            dataDict["paymentType"] = "F"
        }
        else
        {
            dataDict["paymentType"] = "U"
        }
        
        printLog("Promocode request : ", dataDict.jsonRepresentation())
        //https://services-uatastra.sotc.in/holidayRS/pdpPromocode
//        "holidayRS/holidayBooking/pdpPromocode"//In SOTC
//        @"tcHolidayRS/quotation/promocode"// IN TC
        PreBookingCommunicationManager.getPrebookingData(requestType:"POST", queryParam: nil, pathParam:nil, jsonParam: dataDict, headers: nil, requestURL: "holidayRS/holidayBooking/pdpPromocode", returnInCaseOffailure: false)
        {
            (status,response) in
            
            printLog("Status is \(status)")
            
            printLog("Response is \(String(describing: response))")
            if let object = response as? Dictionary<String,Any>
            {
                
                let promoCodeAmount : Dictionary<String,Any> = object["promoCodeAmount"] as? Dictionary<String,Any> ?? Dictionary.init()
                let discount : Double = promoCodeAmount["discountAmount"] as? Double ?? 0.0
                if discount !=  0
                {
                     AppUtility.displayToastMessage("Promo Code Applied.")
                    AppUtility.fillPricingModelWithResponseDict(dict: object, pricingModel: self.pricingModel, quotationView: self.quotationViewController)
                    LoadingIndicatorView.hide()
                    self.quotationViewController.expandPromocodeView = false
                    self.delegate?.reloadTable()
                    self.submitPromocode() //changes on 12-04-19
                    
                }
                else
                {
                    
                    let promoCodeAmount : Dictionary<String,Any> = object["promoCodeAmount"] as? Dictionary<String,Any> ?? Dictionary.init()
                    AppUtility.displayAlert(title: "Alert", message: promoCodeAmount["errorMsg"] as? String ?? "Some Error Occurred")
                }    
               
                
            }
        }
    }
    */
    
//    VJ_Addded
    func fetchPricingByPromocode()
        {
            LoadingIndicatorView.show("Loading")
            var dataDict : Dictionary<String,Any> = Dictionary.init()//
//            {
//                appUsed = Desktop;
//                bookingAmount = "30000.000000";
//                bookingAmountType = F;
//                promoCode = "";
//                quotationId = "CMgNfwHVWyzjEJESQmkm%2FA%3D%3D";
//                userId = "jchalke@gmail.com";
//            }
            dataDict["appUsed"] = "Desktop"
            dataDict["promocode"] = self.promocodeTextfield.text
            let userDetails : Dictionary<String,Any>? = UserDefaults.standard.dictionary(forKey: Constant.userProfile.userDetails)
            if (userDetails != nil){
                dataDict["userId"] = userDetails!["userId"] as? String ?? ""
            }else{
                dataDict["userId"] = "Guest_User"
            }
            dataDict["quotationId"] = self.quotationViewController.quoteID
            if self.quotationViewController.selectedPaymentType == "ADVANCE"{
                dataDict["bookingAmountType"] = "A"
//                dataDict["bookingAmount"] = self.pricingModel.advancePayableAmount
            }else if self.quotationViewController.selectedPaymentType == "FULL_PAYMENT"{
                dataDict["bookingAmountType"] = "F"
//                dataDict["bookingAmount"] = self.quotationViewController.usersChoiceAmount
//                dataDict["bookingAmount"] = self.pricingModel.totalTourCost
            }else{
                dataDict["bookingAmountType"] = "U"
            }
            dataDict["bookingAmount"] = self.quotationViewController.selectedPaymentAmount
//            "holidayRS/holidayBooking/pdpPromocode" //kAstraUrlPromocode , @"tcHolidayRS/quotation/promocode"
            printLog("Promocode request : ", dataDict.jsonRepresentation())
            PreBookingCommunicationManager.getPrebookingData(requestType:"POST", queryParam: nil, pathParam:nil, jsonParam: dataDict, headers: nil, requestURL: kAstraUrlPromocode, returnInCaseOffailure: false)
            {
                (status,response) in
                
                printLog("Status is \(status)")
                
                printLog("Response is \(String(describing: response))")
                if let object = response as? Dictionary<String,Any>
                {
                    
                    let promoCodeAmount : Dictionary<String,Any> = object["promoCodeAmount"] as? Dictionary<String,Any> ?? Dictionary.init()
                    let discount : Double = promoCodeAmount["discountAmount"] as? Double ?? 0.0
                    if discount !=  0
                    {
                        printLog("responseDictobject is \(object)")
                         AppUtility.displayToastMessage("Promo Code Applied.")
                        AppUtility.fillPricingModelWithResponseDict(dict: object, pricingModel: self.pricingModel, quotationView: self.quotationViewController)
                        LoadingIndicatorView.hide()
                        self.quotationViewController.expandPromocodeView = false
                        self.delegate?.reloadTable()
                        self.submitPromocode() //changes on 12-04-19
                        
                    }
                    else
                    {
                        
                        let promoCodeAmount : Dictionary<String,Any> = object["promoCodeAmount"] as? Dictionary<String,Any> ?? Dictionary.init()
                        AppUtility.displayAlert(title: "Alert", message: promoCodeAmount["errorMsg"] as? String ?? "Some Error Occurred")
                    }
                   
                    
                }
            }
        }
    func submitPromocode()
    {
        //LoadingIndicatorView.show("Loading")
        var dataDict : Dictionary<String,Any> = Dictionary.init()//
        dataDict["promoCode"] = self.promocodeTextfield.text
        dataDict["onBehalf"] = "N"
        let userDetails : Dictionary<String,Any>? = UserDefaults.standard.dictionary(forKey: Constant.userProfile.userDetails)
        if (userDetails != nil)
        {
            dataDict["userId"] = userDetails!["userId"] as? String ?? ""
            dataDict["roleId"] = "\(userDetails!["roleId"] as? Int ?? 0)"
        }
        else
        {
            dataDict["userId"] = "Guest_User"
            dataDict["roleId"] = "2"
        }
        
        
        dataDict["quotationId"] = self.quotationViewController.quoteID
        
        if self.quotationViewController.selectedPaymentType == "ADVANCE"
        {
            dataDict["bookingAmountType"] = "A"
        }
        else if self.quotationViewController.selectedPaymentType == "FULL_PAYMENT"
        {
            dataDict["bookingAmountType"] = "F"
        }
        else
        {
            dataDict["bookingAmountType"] = "U"
        }
        dataDict["bookingAmount"] = self.quotationViewController.selectedPaymentAmount;
        
        printLog("Promocode request : ", dataDict.jsonRepresentation())
        //https://services-uatastra.sotc.in/holidayRS/pdpPromocode
        PreBookingCommunicationManager.getPrebookingData(requestType:"POST", queryParam: nil, pathParam:nil, jsonParam: dataDict, headers: nil, requestURL: "holidayRS/quotation/promocode", returnInCaseOffailure: false)
        {
            (status,response) in
            
            printLog("Status is \(status)")
            
            printLog("Response is \(String(describing: response))")
           
        }
    }
}
