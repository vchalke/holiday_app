//
//  PriceDistributionHeader.swift
//  sotc-consumer-application
//
//  Created by Mac on 20/08/18.
//  Copyright © 2018 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit

class PriceDistributionHeader: UITableViewHeaderFooterView 
{

    @IBOutlet weak var packageNameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var tourCostLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var arrowButton: UIButton!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    func setDataInHeader(quotationView : QuotationView,pricingModel : PricingModel, optionalArray : Array<OptionalModel>){
        
    }
    func setImageAndButtonImg(flag:Bool){
            self.arrowButton.setImage(UIImage.init(named: (flag) ? "cancelInGray" : "dropdowncutout"), for: .normal)
            self.tourCostLabel.isHidden = flag
    }

}
