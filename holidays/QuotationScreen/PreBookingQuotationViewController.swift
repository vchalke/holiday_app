//
//  PreBookingQuotationViewController.swift
//  sotc-consumer-application
//
//  Created by ketan on 14/08/18.
//  Copyright © 2018 Mobicule Technologies Private Limited. All rights reserved.
//
 //SIMAUG8k
import UIKit
protocol LoginDelegate
{
    func goToPassengerDetail()
}
protocol PreBookingQuotationVCDelegate {
    func backFromQuotation()
}
class PreBookingQuotationViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,OptionalDefaultViewDelegate,OptionalDelegate,PromocodeTableViewCellDelegate,LoginDelegate,PromoNewTableViewCellDelegate,DeluxPremiumTableViewCellDelegate{
   
    
   
 var quoteDelegate : PreBookingQuotationVCDelegate? = nil
//    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var continueButton: UIButton!
    var optionalActivityArray : Array<OptionalModel> = Array.init()
    @IBOutlet weak var tableViewQuotation: UITableView!
    var quotationViewController : QuotationView = QuotationView.init()
    var pricingModel:PricingModel = PricingModel()
    var packageDetailModel:PackageDetailsModel = PackageDetailsModel.init()
    var pricingDict:Dictionary<String,Any> = Dictionary.init()
    var tourTypeArray:[String] = []
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.initializeView()
        /*AppUtility.checkVersioning(completionBlock: { (isVersion) in
        })*/
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }

    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            
            self.tableViewQuotation.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height, right: 0)
            
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        //tableViewBottom.constant = 0.0
        self.tableViewQuotation.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.quotationViewController.expandPriceAndRoomingDetail = false // false // 29
        self.quotationViewController.expandPromocodeView = true // false // 29
        self.tableViewQuotation.reloadData()
    }
    func renameContinueButton() {
        self.showPaymentView()
    }
    func getSelectedOptionalActivityArray(optionalArray : Array<OptionalModel>){
        optionalActivityArray = optionalArray
        self.tableViewQuotation.reloadData()
    }
    override func viewWillLayoutSubviews()
    {
        self.continueButton.layer.cornerRadius = 3
    }
    
    override func viewDidLayoutSubviews()
    {
        self.continueButton.layer.cornerRadius = 3
    }
    func showPaymentView()
    {
        /* Previous
        self.quotationViewController.expantPaymentView = true
        self.quotationViewController.expandPromocodeView = true
        self.quotationViewController.expandDefaultOptionalView = false
        self.quotationViewController.expantThreeFiveStarView = false // VJ_Added foe delux Premium
        self.continueButton.setTitle("Continue", for: .normal)
        self.tableViewQuotation.reloadData()
         */
        self.quotationViewController.expantPaymentView = true
        self.quotationViewController.expandPromocodeView = false
        self.quotationViewController.expandDefaultOptionalView = false
        self.quotationViewController.expantThreeFiveStarView = false // VJ_Added foe delux Premium
        self.continueButton.setTitle("CONTINUE", for: .normal)
        self.tableViewQuotation.reloadData()
    }
    func initializeView()
    {
//        self.tableViewQuotation.register(UINib.init(nibName: "PackageDescriptionTableViewCell", bundle: Bundle.main), forCellReuseIdentifier:"PriceDistributionCellIdentifier")
        self.tableViewQuotation.register(UINib.init(nibName: "PackageDescriptionViewCell", bundle: Bundle.main), forCellReuseIdentifier:"PackageDescriptionViewCell")
        let nib = UINib(nibName: "PriceDistributionHeader", bundle: nil)
        self.tableViewQuotation.register(nib, forHeaderFooterViewReuseIdentifier: "PriceDistributionHeader")
        self.tableViewQuotation.register(UINib.init(nibName: "PaymentOptionsTableViewCell", bundle: Bundle.main), forCellReuseIdentifier:"PaymentOptionsTableViewCell")
        self.tableViewQuotation.register(UINib.init(nibName: "OptionalDefaultViewTableViewCell", bundle: Bundle.main), forCellReuseIdentifier:"OptionalDefaultViewTableViewCell")
        self.tableViewQuotation.register(UINib.init(nibName: "DeluxPremiumTableViewCell", bundle: Bundle.main), forCellReuseIdentifier:"DeluxPremiumTableViewCell")
        self.tableViewQuotation.register(UINib.init(nibName: "PromocodeTableViewCell", bundle: Bundle.main), forCellReuseIdentifier:"PromocodeTableViewCell")
        self.tableViewQuotation.register(UINib.init(nibName: "PromoNewTableViewCell", bundle: Bundle.main), forCellReuseIdentifier:"PromoNewTableViewCell")
        /* Previous
        if packageDetailModel.tcilHolidayOptionalsCollection.count > 0
        {
            self.quotationViewController.expandDefaultOptionalView = true
            self.quotationViewController.expantThreeFiveStarView = true // VJ_Added foe delux Premium
            self.quotationViewController.expantPaymentView = false
            self.quotationViewController.expandPromocodeView = false
            self.continueButton.setTitle("CONTINUE WITHOUT ACTIVITIES", for: .normal)
        }
        else
        {
            self.quotationViewController.expandDefaultOptionalView = false
            self.quotationViewController.expantThreeFiveStarView = false // VJ_Added foe delux Premiu
            self.quotationViewController.expantPaymentView = true
            self.quotationViewController.expandPromocodeView = false // true // 30/06/20

        }
        */
        //VJ_Added
//        if self.quotationViewController.expandPromocodeView
//        {
            self.quotationViewController.expandDefaultOptionalView = packageDetailModel.tcilHolidayOptionalsCollection.count > 0
            self.quotationViewController.expantThreeFiveStarView = true // VJ_Added foe delux Premium
            self.quotationViewController.expantPaymentView = false
            self.quotationViewController.expandPromocodeView = true
//        self.continueButton.setTitle((packageDetailModel.tcilHolidayOptionalsCollection.count > 0) ? "CONTINUE WITHOUT ACTIVITIES" : "CONTINUE", for: .normal)
        self.continueButton.setTitle("CONTINUE", for: .normal)
//        }
//        else
//        {
//            self.quotationViewController.expandDefaultOptionalView = false
//            self.quotationViewController.expantThreeFiveStarView = false // VJ_Added foe delux Premiu
//            self.quotationViewController.expantPaymentView = true
//            self.quotationViewController.expandPromocodeView = false // true // 30/06/20
//
//        }
        if packageDetailModel.tcilHolidayOptionalsCollection.count > 0{
            printLog("tcilHolidayOptionalsCollection present")
        }
//        self.containerView.fadedShadow()
//        self.setAdvanceAndFullAmoutDisplay()
        self.setTourTypeArrayInQuotation()
    }
    
    func setTourTypeArrayInQuotation(){
       printLog("packageDetailModel.arrayTourType \(packageDetailModel.arrayTourType)")
        printLog("packageDetailModel.packageClassId \(pricingModel.packageClassId)")
    }
    //MARK:-  Tableview Delegates -
    /*
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 0
        {
            if self.quotationViewController.expandPriceAndRoomingDetail && self.quotationViewController.expandPromocodeView
            {
                return 2
            }
            if self.quotationViewController.expandPriceAndRoomingDetail || self.quotationViewController.expandPromocodeView
            {
                return 1
            }
            return 0
        }
       return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return self.quotationViewController.getSectionCount()
    }
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 100
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
                if section == 0
                {
                    return 70//UITableView.automaticDimension//70
                }
                return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        switch indexPath.section {
        case 0:
            
            switch indexPath.row
            {
            case 0:
               
                if  self.quotationViewController.expandPromocodeView && !self.quotationViewController.expandPriceAndRoomingDetail
                {
//                    let cell = tableViewQuotation.dequeueReusableCell(withIdentifier: "PromocodeTableViewCell") as! PromocodeTableViewCell
//                    cell.delegate = self
                    let cell = tableViewQuotation.dequeueReusableCell(withIdentifier: "PromoNewTableViewCell") as! PromoNewTableViewCell
                    cell.delegates = self
                    cell.quotationViewController = self.quotationViewController
                    cell.pricingModel = self.pricingModel
                    return cell;
                }
                if self.quotationViewController.expandPriceAndRoomingDetail
                {
//                    let cell = tableViewQuotation.dequeueReusableCell(withIdentifier: "PriceDistributionCellIdentifier") as! PackageDescriptionTableViewCell
                    let cell = tableViewQuotation.dequeueReusableCell(withIdentifier: "PackageDescriptionViewCell") as! PackageDescriptionViewCell
                    cell.setData(quotationView:  self.quotationViewController, pricingModel: self.pricingModel, optionalArray:optionalActivityArray)
                    return cell;
                }
                
            default:
//                let cell = tableViewQuotation.dequeueReusableCell(withIdentifier: "PromocodeTableViewCell") as! PromocodeTableViewCell
//                cell.delegate = self
                
                let cell = tableViewQuotation.dequeueReusableCell(withIdentifier: "PromoNewTableViewCell") as! PromoNewTableViewCell
                cell.delegates = self
                cell.quotationViewController = self.quotationViewController
                cell.pricingModel = self.pricingModel
                
//                let cell = tableViewQuotation.dequeueReusableCell(withIdentifier: "PackageDescriptionViewCell") as! PackageDescriptionViewCell
//                cell.setData(quotationView:  self.quotationViewController, pricingModel: self.pricingModel, optionalArray:optionalActivityArray)
                
                return cell;
                
                
            }
      
        case 2: //1
            if self.quotationViewController.expantPaymentView
            {
                let cell = tableViewQuotation.dequeueReusableCell(withIdentifier: "PaymentOptionsTableViewCell") as! PaymentOptionsTableViewCell
                cell.pricingModel = self.pricingModel
                cell.quotationView = self.quotationViewController
                cell.setData(quotationView:  self.quotationViewController, pricingModel: self.pricingModel)
                return cell;
            }
            else
            {
                let cell = tableViewQuotation.dequeueReusableCell(withIdentifier: "OptionalDefaultViewTableViewCell") as! OptionalDefaultViewTableViewCell
                cell.delegate = self
                cell.setData(packageDetailModel: self.packageDetailModel)
                return cell;
            }
         
        case 1://2
        if self.quotationViewController.expantThreeFiveStarView
        {
            let cell = tableViewQuotation.dequeueReusableCell(withIdentifier: "DeluxPremiumTableViewCell") as! DeluxPremiumTableViewCell
            cell.pricingModel = self.pricingModel
            cell.quotationView = self.quotationViewController
            return cell;
        }
            
        default:
//            let cell = tableViewQuotation.dequeueReusableCell(withIdentifier: "PriceDistributionCellIdentifier") as! PackageDescriptionTableViewCell
            let cell = tableViewQuotation.dequeueReusableCell(withIdentifier: "PackageDescriptionViewCell") as! PackageDescriptionViewCell
            return cell;
        }
//        let cell = tableViewQuotation.dequeueReusableCell(withIdentifier: "PriceDistributionCellIdentifier") as! PackageDescriptionTableViewCell
//        let cell = tableViewQuotation.dequeueReusableCell(withIdentifier: "PackageDescriptionViewCell") as! PackageDescriptionViewCell
//        return cell;
        let cell = tableViewQuotation.dequeueReusableCell(withIdentifier: "PaymentOptionsTableViewCell") as! PaymentOptionsTableViewCell
        cell.pricingModel = self.pricingModel
        cell.quotationView = self.quotationViewController
        cell.setData(quotationView:  self.quotationViewController, pricingModel: self.pricingModel)
        return cell;
    }
    */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
        {
//            if section == 0
//            {
//                if self.quotationViewController.expandPromocodeView
//                {
//                    return 3
//                }
//                return 1
//            }
            
//           return !self.quotationViewController.expantPaymentView ?  4 : 2 // 1
            
//            This is Done beacuse its decided to show oops msg after showing calculations
//            On 24_July_20
            if self.pricingModel.onReqeustDate == true || self.packageDetailModel.pkgStatusId != 1 {
                return 1
            }
            return !self.quotationViewController.expantPaymentView ?  4 : 2 // 1 // Working
        }
        
        func numberOfSections(in tableView: UITableView) -> Int
        {
//            return self.quotationViewController.getSectionCount()
//            return !self.quotationViewController.expandPromocodeView ? 2 : self.quotationViewController.expandPriceAndRoomingDetail ?  3 : 3
             return 1
        }
        func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
            return 100
        }
        func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
                    if section == 0
                    {
                        return 70
                    }
                    return 0
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
        {
            /*
             switch indexPath.section {
             case 0:
             
             switch indexPath.row
             {
             case 0:
             /*
             if  self.quotationViewController.expandPromocodeView && !self.quotationViewController.expandPriceAndRoomingDetail
             {
             let cell = tableViewQuotation.dequeueReusableCell(withIdentifier: "PromoNewTableViewCell") as! PromoNewTableViewCell
             cell.delegates = self
             cell.quotationViewController = self.quotationViewController
             cell.pricingModel = self.pricingModel
             return cell;
             }
             */
             if self.quotationViewController.expandPriceAndRoomingDetail
             {
             let cell = tableViewQuotation.dequeueReusableCell(withIdentifier: "PackageDescriptionViewCell") as! PackageDescriptionViewCell
             cell.setData(quotationView:  self.quotationViewController, pricingModel: self.pricingModel, optionalArray:optionalActivityArray)
             return cell;
             }
             
             default:
             let cell = tableViewQuotation.dequeueReusableCell(withIdentifier: "PromoNewTableViewCell") as! PromoNewTableViewCell
             cell.delegates = self
             cell.quotationViewController = self.quotationViewController
             cell.pricingModel = self.pricingModel
             return cell;
             
             
             }
             
             /*
             if self.quotationViewController.expandPriceAndRoomingDetail
             {
             let cell = tableViewQuotation.dequeueReusableCell(withIdentifier: "PackageDescriptionViewCell") as! PackageDescriptionViewCell
             cell.setData(quotationView:  self.quotationViewController, pricingModel: self.pricingModel, optionalArray:optionalActivityArray)
             return cell;
             }
             */
             case 1:
             if self.quotationViewController.expandPriceAndRoomingDetail
             {
             let cell = tableViewQuotation.dequeueReusableCell(withIdentifier: "PromoNewTableViewCell") as! PromoNewTableViewCell
             cell.delegates = self
             cell.quotationViewController = self.quotationViewController
             cell.pricingModel = self.pricingModel
             return cell;
             }
             let cell = tableViewQuotation.dequeueReusableCell(withIdentifier: "DeluxPremiumTableViewCell") as! DeluxPremiumTableViewCell
             cell.pricingModel = self.pricingModel
             cell.quotationView = self.quotationViewController
             return cell;
             
             case 2:
             if self.quotationViewController.expandPriceAndRoomingDetail
             {
             let cell = tableViewQuotation.dequeueReusableCell(withIdentifier: "DeluxPremiumTableViewCell") as! DeluxPremiumTableViewCell
             cell.pricingModel = self.pricingModel
             cell.quotationView = self.quotationViewController
             return cell;
             }
             case 3:
             if packageDetailModel.tcilHolidayOptionalsCollection.count > 0
             {
             let cell = tableViewQuotation.dequeueReusableCell(withIdentifier: "OptionalDefaultViewTableViewCell") as! OptionalDefaultViewTableViewCell
             cell.delegate = self
             cell.setData(packageDetailModel: self.packageDetailModel)
             return cell;
             }
             
             default:
             let cell = tableViewQuotation.dequeueReusableCell(withIdentifier: "PackageDescriptionViewCell") as! PackageDescriptionViewCell
             return cell;
             }
             let cell = tableViewQuotation.dequeueReusableCell(withIdentifier: "PaymentOptionsTableViewCell") as! PaymentOptionsTableViewCell
             cell.pricingModel = self.pricingModel
             cell.quotationView = self.quotationViewController
             cell.setData(quotationView:  self.quotationViewController, pricingModel: self.pricingModel)
             return cell;
             */
            
            
            if (indexPath.row == 0){
                let cell = tableViewQuotation.dequeueReusableCell(withIdentifier: "PackageDescriptionViewCell") as! PackageDescriptionViewCell
                cell.setData(quotationView:  self.quotationViewController, pricingModel: self.pricingModel, optionalArray:optionalActivityArray)
                cell.hideConatentView(flags: !self.quotationViewController.expandPriceAndRoomingDetail)
                return cell;
            }else if (indexPath.row == 1){
                if(self.quotationViewController.expantPaymentView){
                    let cell = tableViewQuotation.dequeueReusableCell(withIdentifier: "PaymentOptionsTableViewCell") as! PaymentOptionsTableViewCell
                    cell.pricingModel = self.pricingModel
                    cell.quotationView = self.quotationViewController
                    cell.setData(quotationView:  self.quotationViewController, pricingModel: self.pricingModel, pkgDetails: self.packageDetailModel)
                    cell.hideConatentView(flags: !self.quotationViewController.expantPaymentView)
                    return cell;
                }
                let cell = tableViewQuotation.dequeueReusableCell(withIdentifier: "PromoNewTableViewCell") as! PromoNewTableViewCell
                cell.delegates = self
                cell.quotationViewController = self.quotationViewController
                cell.pricingModel = self.pricingModel
                return cell;
            }else if (indexPath.row == 2){
                if !(self.packageDetailModel.arrayTourType.count<=1){
                    let cell = tableViewQuotation.dequeueReusableCell(withIdentifier: "DeluxPremiumTableViewCell") as! DeluxPremiumTableViewCell
                    cell.pricingModel = self.pricingModel
                    cell.quotationView = self.quotationViewController
                    cell.packageDetailModel = self.packageDetailModel
                    cell.delegates = self
                    cell.setDataAsDeluxAndPremium()
                    return cell;
                }
            }else if (indexPath.row == 3){
                if ( packageDetailModel.tcilHolidayOptionalsCollection.count > 0){
                let cell = tableViewQuotation.dequeueReusableCell(withIdentifier: "OptionalDefaultViewTableViewCell") as! OptionalDefaultViewTableViewCell
                cell.delegate = self
                cell.setData(packageDetailModel: self.packageDetailModel)
                return cell;
                }
            }
            let cell = tableViewQuotation.dequeueReusableCell(withIdentifier: "PaymentOptionsTableViewCell") as! PaymentOptionsTableViewCell
            cell.pricingModel = self.pricingModel
            cell.quotationView = self.quotationViewController
            cell.setData(quotationView:  self.quotationViewController, pricingModel: self.pricingModel, pkgDetails: self.packageDetailModel)
            cell.hideConatentView(flags: !self.quotationViewController.expantPaymentView)
            return cell;
            
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let cell : PriceDistributionHeader = tableView.dequeueReusableHeaderFooterView(withIdentifier: "PriceDistributionHeader") as! PriceDistributionHeader
        cell.containerView.shadowAtBottom()
        let numFormatter : NumberFormatter = NumberFormatter.init()
        // numFormatter.positiveFormat = "0"
        numFormatter.numberStyle = NumberFormatter.Style.decimal
        numFormatter.locale = Locale(identifier: "en_IN")
//        let isAfterPromoApplyAmount = self.pricingModel.totalTourCost
        let isAfterPromoApplyAmount = pricingModel.applyPromocodeSuccess ? (Int(pricingModel.totalTourCost) ?? 15000) - Int(pricingModel.promocodeAmount) : Int(pricingModel.totalTourCost) ?? 0
//        cell.tourCostLabel.text = "₹ " + (numFormatter.string(from: NSNumber.init(value: Int(isAfterPromoApplyAmount) )) ?? "0") //db fr
        cell.tourCostLabel.attributedText = self.getRuppesString(value: isAfterPromoApplyAmount)
        cell.packageNameLabel.text = self.packageDetailModel.pkgName
        cell.addressLabel.text = pricingModel.pricingDepartureCity + ", " + AppUtility.convertDateToString(date: AppUtility.convertStringToDate(string: pricingModel.pricingTourDate, format: "dd-MM-yyyy"), dateFormat: "dd MMMM,yyyy")
        cell.arrowButton.addTarget(self, action: #selector(self.toggleDetailView(_:)), for: UIControl.Event.touchUpInside)
        let menuItemTapGestureRecognizer = UITapGestureRecognizer(target: self, action:#selector(self.onTapOfCell(_:)))
        menuItemTapGestureRecognizer.numberOfTapsRequired = 1
        menuItemTapGestureRecognizer.numberOfTouchesRequired = 1
        cell.containerView.addGestureRecognizer(menuItemTapGestureRecognizer)
        cell.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
        if self.quotationViewController.expandPriceAndRoomingDetail
        {
            cell.arrowButton.setImage(UIImage.init(named: "cancelInGray"), for: .normal)
            cell.tourCostLabel.isHidden = true
        }
        else
        {
            cell.arrowButton.setImage(UIImage.init(named: "dropdowncutout"), for: .normal)
            cell.tourCostLabel.isHidden = false
        }
        return cell
    }
    @objc func onTapOfCell(_ sender: UITapGestureRecognizer)
    {
        self.quotationViewController.expandPriceAndRoomingDetail = !quotationViewController.expandPriceAndRoomingDetail
        self.tableViewQuotation.reloadData()
        self.tableViewQuotation.reloadSections(IndexSet.init(integer: 0), with: .automatic)
        DispatchQueue.main.async {
            let indexPath = IndexPath(item: 0, section: 0)
           self.tableViewQuotation.scrollToRow(at: indexPath, at: .top, animated: true)
        }
    }
    @objc func toggleDetailView(_ sender : UIButton)
    {
        self.quotationViewController.expandPriceAndRoomingDetail = !quotationViewController.expandPriceAndRoomingDetail
        self.tableViewQuotation.reloadData()
        self.tableViewQuotation.reloadSections(IndexSet.init(integer: 0), with: .automatic)
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if (indexPath.row == 0 && self.quotationViewController.expandPriceAndRoomingDetail){
            return UITableView.automaticDimension
        }else if (indexPath.row == 0 && !self.quotationViewController.expandPriceAndRoomingDetail){
            return 0
        }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if (indexPath.row == 0 && self.quotationViewController.expandPriceAndRoomingDetail){
            return UITableView.automaticDimension
        }else if (indexPath.row == 0 && !self.quotationViewController.expandPriceAndRoomingDetail){
            return 0
        }else if (indexPath.row == 2){
            if (!self.quotationViewController.expantPaymentView){
                if (self.packageDetailModel.arrayTourType.count<=1){
                    return 0
                }else if (self.packageDetailModel.arrayTourType.contains("Delux") || self.packageDetailModel.arrayTourType.contains("Premium")){
                    return UITableView.automaticDimension
                }else{
                    return 0
                }
            }else{
               return UITableView.automaticDimension
            }
        }
        return UITableView.automaticDimension
    }
    func reloadTable()
    {
        self.tableViewQuotation.reloadData()
    }
    func reloadTableInPromo() {
        self.tableViewQuotation.reloadData()
    }
    // Delegated Of Delux And Premium
    func reloadTableInDeluxPremium() {
        self.tableViewQuotation.reloadData()
    }
    func getRuppesString(value:Int)->NSMutableAttributedString{
        let numFormatter : NumberFormatter = NumberFormatter.init()
        numFormatter.numberStyle = NumberFormatter.Style.decimal
        numFormatter.locale = Locale(identifier: "en_IN")
        let boldFontAttributes = [NSAttributedString.Key.foregroundColor: AppUtility.hexStringToUIColor(hex: "#0054A5"), NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 18)]//0037A3
        let normalFontAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14)]
        let partOne = NSMutableAttributedString(string: "₹ ", attributes: normalFontAttributes)
        let partTwo = NSMutableAttributedString(string: (numFormatter.string(from: NSNumber.init(value:value)) ?? "0"), attributes: boldFontAttributes)
        //            let partTwo = NSMutableAttributedString(string: (numFormatter.string(from: NSNumber.init(value:Int(pricingModel.totalTourCost) ?? 0)) ?? "0"), attributes: boldFontAttributes)
        let combinationTwo = NSMutableAttributedString()
        combinationTwo.append(partOne)
        combinationTwo.append(partTwo)
        return combinationTwo
    }
    //MARK:- Navigation Methods -

    func openOptionalActivitiesView()
    {
        let optionalVc : OptionalActivitiesViewController = OptionalActivitiesViewController(nibName: "OptionalActivitiesViewController", bundle: nil)
        optionalVc.pricingModel = self.pricingModel
        optionalVc.packageDetailModel = self.packageDetailModel
        optionalVc.delegate = self
        optionalVc.quotationViewController = self.quotationViewController
        self.navigationController?.pushViewController(optionalVc, animated: true)
    }
    
    func goToPassengerDetailPage()
    {
//        let viewController : PassengerDetailViewController = PassengerDetailViewController(nibName: "PassengerDetailViewController", bundle: nil)
        let viewController : PassengerDetailVC = PassengerDetailVC(nibName: "PassengerDetailVC", bundle: nil)
        viewController.pricingModel = pricingModel
        viewController.packageDetailModel = self.packageDetailModel
        viewController.quotationModel = self.quotationViewController
        viewController.optionalActivityArray = self.optionalActivityArray
        self.navigationController?.pushViewController(viewController, animated: true)

    }
    //MARK:-  login Delegate -

    func goToPassengerDetail()
    {
        self.goToPassengerDetailPage()
    }
    //MARK:-  button action -

    @IBAction func backButtonClicked(_ sender: Any)
    {
        /*
        if packageDetailModel.tcilHolidayOptionalsCollection.count <= 0
        {
            self.navigationController?.popViewController(animated: true)
        }
        else if self.quotationViewController.expantPaymentView == true
        {
            self.quotationViewController.expantPaymentView = false
            self.quotationViewController.expandDefaultOptionalView = true
            self.quotationViewController.expantThreeFiveStarView = true // VJ_Added same as see All Activities
            self.quotationViewController.expandPromocodeView = true // false VJ_Change
            self.optionalActivityArray.removeAll()
            self.tableViewQuotation.reloadData()
        }
        else
        {
            self.navigationController?.popViewController(animated: true)
        }
        */
        if self.quotationViewController.expantPaymentView == true
        {
            self.quotationViewController.expantPaymentView = false
            self.quotationViewController.expandDefaultOptionalView = true
            self.quotationViewController.expantThreeFiveStarView = true // VJ_Added same as see All Activities
            self.quotationViewController.expandPromocodeView = true // false VJ_Change
//            self.pricingModel.totalOptionalCost = 0
            self.pricingModel.totalTourCost = "\((Int(self.pricingModel.totalTourCost) ?? 0) - (Int(self.pricingModel.totalOptionalCost) ))"
            pricingModel.totalOptionalCost = 0
            self.optionalActivityArray.removeAll()
//            self.tableViewQuotation.reloadData()
            self.selectDeluxPremiumInQuote(withSubClassId: self.pricingModel.packageClassId, flag: false)
        }
        else
        {
            
//            self.pricingModel.packageClassId = (self.pricingModel.packageClassId > 0) ? self.pricingModel.packageClassId : 0 // Set When We want same class Id As Selecetd in Quote
            self.pricingModel.promocodeAmount = 0
            self.pricingModel.discountAdvancePrice = 0
            self.pricingModel.applyPromocodeSuccess = false
//            self.pricingModel.packageClassId = self.pricingModel.mainPackageClassId
//            self.navigationController?.popViewController(animated: true)
            self.throwUserBack()
        }
        
    }
    @IBAction func btn_callHelplineNum(_ sender: Any)
    {
        AppUtility.callToTCFromPhaseTwo()
    }
    @IBAction func continueButtonClicked(_ sender: Any)
    {
        //            This is Done beacuse its decided to show oops msg after showing calculations
        //            On 24_July_20
        if self.pricingModel.onReqeustDate == true || self.packageDetailModel.pkgStatusId != 1 {
            self.showOopsAlertOnly()
        }else if (quotationViewController.expantPaymentView == false){
          self.showPaymentView()
        }
        
//        if quotationViewController.expantPaymentView == false
//        {
//           self.showPaymentView()
//        }
            
        else{
            if self.quotationViewController.selectedPaymentType == "USER_CHOICE" && self.quotationViewController.selectedPaymentAmount == 0.0{
//                AppUtility.displayAlert(title: "Alert", message: "Please enter valid amount or select another payment type.")
                self.alertViewControllerSingleton(title: "Alert", message: "Please enter valid amount or select another payment type.", type: .alert, buttonArryName: ["Ok"]) { (strin) in
                    printLog(strin)
                }
            }
            else if (self.quotationViewController.selectedPaymentType == "USER_CHOICE" && self.quotationViewController.selectedPaymentAmount < self.pricingModel.advancePayableAmount)
            {
//                AppUtility.displayAlert(title: "Alert", message: "Amount cannot be less than advance amount.")
                self.alertViewControllerSingleton(title: "Alert", message: "Amount cannot be less than advance amount.", type: .alert, buttonArryName: ["Ok"]) { (strin) in
                    printLog(strin)
                }
            }
            else
            {//Acc to new Design use is login first so no necessary to check user profilr
                /*
                let userDetails : Dictionary<String,Any>? = UserDefaults.standard.dictionary(forKey: Constant.userProfile.userDetails)
                if (userDetails == nil)
                {
//                    let viewController : PreBookingLoginViewController = PreBookingLoginViewController(nibName: "PreBookingLoginViewController", bundle: nil)
//                    viewController.pricingModel = self.pricingModel
//                    viewController.packageDetailModel = self.packageDetailModel
//                    viewController.quotationModel = self.quotationViewController
//                    viewController.delegate = self
//                    self.navigationController?.pushViewController(viewController, animated: true)
                }
                else
                {
//                   self.goToPassengerDetailPage()
//                    self.callConfirmationAPI()
                    self.bookingTypeSubmissionAPIExistAPI()
                }
               */
                self.bookingTypeSubmissionAPIExistAPI()
            }
            
        }
      
        
    }
    
    func bookingTypeSubmissionAPIExistAPI(){
        let user:String = UserDefaults.standard.value(forKey: kLoginEmailId) as? String ?? ""
        var submissionDict : Dictionary<String,Any> = Dictionary.init()
        submissionDict["quotationId"] = self.quotationViewController.quoteID
        if self.quotationViewController.selectedPaymentType == "ADVANCE"{
            submissionDict["bookingAmountType"] = "A"
        }else if self.quotationViewController.selectedPaymentType == "FULL_PAYMENT"{
            submissionDict["bookingAmountType"] = "F"
        }else{
            submissionDict["bookingAmountType"] = "U"
        }
        submissionDict["bookingAmount"] = self.quotationViewController.selectedPaymentAmount
//        submissionDict["promoCode"] = ""
        submissionDict["promoCode"] = self.pricingModel.promocodeAppliedText // Selected Text
        submissionDict["userId"] = user
//        submissionDict["appUsed"] = "Desktop"
        submissionDict["appUsed"] = "APP"
        printLog("Promocode Update Quote Dict : ", submissionDict)
        printLog("First QuoteID is \(self.quotationViewController.quoteID)")
        PreBookingCommunicationManager.getPrebookingData(requestType:"POST", queryParam: nil, pathParam:nil, jsonParam: submissionDict, headers: nil, requestURL:kAstraUrlPromocode , returnInCaseOffailure: false)
        {
            (status,response) in
            
            printLog("status is \(status)")
            printLog("kAstraUrlPromocode response is \(String(describing: response))")
            if status
            {
                
                if let responseDict : Dictionary<String,Any> =  response as? Dictionary<String,Any>
                {
                    if let msg : String = responseDict["message"] as? String
                    {
                        //                        if msg != ""
                        //                        {
                        //                            AppUtility.displayAlert(title: "Alert", message: msg)
                        //                        }
                        if((msg.uppercased()) == "SUCCESS"){
                            self.callConfirmationAPI()
                        }else{
                            self.alertViewControllerSingleton(title: "Alert", message: "Some Error Occured", type: .alert, buttonArryName: ["Ok"]) { (strin) in
                                printLog(strin)
                            }
                        }
                        
                    }
                    else
                    {
                        self.showOopsAlertOnly()
//                        AppUtility.displayAlert(title: "Alert", message: "Oops! Due to limited availability, we are not able to process your request through mobile app. Please click on the below mentioned Call button and share your details \(ContactHelpline.TCHelpLineNumber)")
                    }
                }
            }
            else
            {
                if let responseDict : Dictionary<String,Any> =  response as? Dictionary<String,Any>
                {
                    if let msg : String = responseDict["message"] as? String
                    {
//                        AppUtility.displayAlert(title: "Alert", message: msg)
                        self.alertViewControllerSingleton(title: "Alert", message: msg, type: .alert, buttonArryName: ["Ok"]) { (strin) in
                            printLog(strin)
                        }
                    }
                    else
                    {
//                        AppUtility.displayAlert(title: "Alert", message: "Some error has occurred")
                        self.alertViewControllerSingleton(title: "Alert", message: "Some error has occurred", type: .alert, buttonArryName: ["Ok"]) { (strin) in
                            printLog(strin)
                        }
                    }
                }
                else
                {
//                    AppUtility.displayAlert(title: "Alert", message: "Some error has occurred")
                    self.alertViewControllerSingleton(title: "Alert", message: "Some error has occurred", type: .alert, buttonArryName: ["Ok"]) { (strin) in
                        printLog(strin)
                    }
                }
            }
        }
    }
    
    func bookingTypeSubmissionAPINewAPI(){
        let user:String = UserDefaults.standard.value(forKey: kLoginEmailId) as? String ?? ""
        var submissionDict : Dictionary<String,Any> = Dictionary.init()
        submissionDict["quotationId"] = self.quotationViewController.quoteID
        if self.quotationViewController.selectedPaymentType == "ADVANCE"{
            submissionDict["bookingAmountType"] = "A"
        }else if self.quotationViewController.selectedPaymentType == "FULL_PAYMENT"{
            submissionDict["bookingAmountType"] = "F"
        }else{
            submissionDict["bookingAmountType"] = "U"
        }
        submissionDict["bookingAmount"] = self.quotationViewController.selectedPaymentAmount
        submissionDict["promoCode"] = ""
        submissionDict["userId"] = user
//        submissionDict["appUsed"] = "Desktop"
        submissionDict["appUsed"] = "APP"
        PreBookingCommunicationManager.getPrebookingData(requestType:"POST", queryParam: nil, pathParam:nil, jsonParam: submissionDict, headers: nil, requestURL:kAstraUrlPromocode , returnInCaseOffailure: false)
        {
            (status,response) in
            
            printLog("status is \(status)")
            printLog("kAstraUrlPromocode response is \(String(describing: response))")
            if status
            {
                
                if let responseDict : Dictionary<String,Any> =  response as? Dictionary<String,Any>
                {
                    if let msg : String = responseDict["message"] as? String
                    {
                        //                        if msg != ""
                        //                        {
                        //                            AppUtility.displayAlert(title: "Alert", message: msg)
                        //                        }
                        if((msg.uppercased()) == "SUCCESS"){
                            self.callConfirmationAPI()
                        }else{
//                            AppUtility.displayAlert(title: "Alert", message: "Some Error Occured")
                            self.alertViewControllerSingleton(title: "Alert", message: "Some error has occurred", type: .alert, buttonArryName: ["Ok"]) { (strin) in
                                printLog(strin)
                            }
                        }
                        
                    }
                    else
                    {
//                        AppUtility.displayAlert(title: "Alert", message: "Oops! Due to limited availability, we are not able to process your request through mobile app. Please click on the below mentioned Call button and share your details 18002093344")
                        self.showOopsAlertOnly()
                    }
                }
            }
            else
            {
                if let responseDict : Dictionary<String,Any> =  response as? Dictionary<String,Any>
                {
                    if let msg : String = responseDict["message"] as? String
                    {
//                        AppUtility.displayAlert(title: "Alert", message: msg)
                        self.alertViewControllerSingleton(title: "Alert", message: msg, type: .alert, buttonArryName: ["Ok"]) { (strin) in
                            printLog(strin)
                        }
                    }
                    else
                    {
//                        AppUtility.displayAlert(title: "Alert", message: "Some error has occurred")
                        self.alertViewControllerSingleton(title: "Alert", message: "Some error has occurred", type: .alert, buttonArryName: ["Ok"]) { (strin) in
                            printLog(strin)
                        }
                    }
                }
                else
                {
//                    AppUtility.displayAlert(title: "Alert", message: "Some error has occurred")
                    self.alertViewControllerSingleton(title: "Alert", message: "Some error has occurred", type: .alert, buttonArryName: ["Ok"]) { (strin) in
                        printLog(strin)
                    }
                }
            }
        }
    }
    
    func callConfirmationAPI(){
    
        
        LoadingIndicatorView.show("Loading")
        printLog("Second QuoteID is \(self.quotationViewController.quoteID)")
        let queryParam = "?quotationId=\(self.quotationViewController.quoteID)"
        PreBookingCommunicationManager.getPrebookingData(requestType:"GET", queryParam: queryParam, pathParam:nil, jsonParam: nil, headers: nil, requestURL:kAstraUrlPreConfirmationBooking , returnInCaseOffailure: false)
        {
            (status,response) in
            
            printLog("status is \(status)")
            printLog("callConfirmationAPI response is \(String(describing: response))")
            if status
            {
                
                if let responseDict : Dictionary<String,Any> =  response as? Dictionary<String,Any>
                {
                    if let msg : String = responseDict["message"] as? String
                    {
                        if msg != ""
                        {
//                            AppUtility.displayAlert(title: "Alert", message: msg)
                            self.alertViewControllerSingleton(title: "Alert", message: msg, type: .alert, buttonArryName: ["Ok"]) { (strin) in
                                printLog(strin)
                            }
                        }
                        
                    }
                    else
                    {
                        if(responseDict["message"] as? Int != 0){
                            self.goToPassengerDetailPage()
                        }else{
//                            AppUtility.displayAlert(title: "Alert", message: "Oops! Due to limited availability, we are not able to process your request through mobile app. Please click on the below mentioned Call button and share your details 18002093344")
                            self.showOopsAlertOnly()
                        }
                        
                    }
                }
            }
            else
            {
                if let responseDict : Dictionary<String,Any> =  response as? Dictionary<String,Any>
                {
                    if let msg : String = responseDict["message"] as? String
                    {
//                        AppUtility.displayAlert(title: "Alert", message: msg)
                        self.alertViewControllerSingleton(title: "Alert", message: msg, type: .alert, buttonArryName: ["Ok"]) { (strin) in
                            printLog(strin)
                        }
                    }
                    else
                    {
//                        AppUtility.displayAlert(title: "Alert", message: "Some error has occurred")
                        self.alertViewControllerSingleton(title: "Alert", message: "Some error has occurred", type: .alert, buttonArryName: ["Ok"]) { (strin) in
                            printLog(strin)
                        }
                    }
                }
                else
                {
//                    AppUtility.displayAlert(title: "Alert", message: "Some error has occurred")
                    self.alertViewControllerSingleton(title: "Alert", message: "Some error has occurred", type: .alert, buttonArryName: ["Ok"]) { (strin) in
                        printLog(strin)
                    }
                }
            }
        }
    }
    
    
    //MARK:-  Promocode Delegates
    
    func submitPromocodePricingByDelegate(promocodeText : String){
        //LoadingIndicatorView.show("Loading")
        var dataDict : Dictionary<String,Any> = Dictionary.init()//
        dataDict["promoCode"] = promocodeText
        dataDict["onBehalf"] = "N"
        let userDetails : Dictionary<String,Any>? = UserDefaults.standard.dictionary(forKey: Constant.userProfile.userDetails)
        if (userDetails != nil){
            dataDict["userId"] = userDetails!["userId"] as? String ?? ""
            dataDict["roleId"] = "\(userDetails!["roleId"] as? Int ?? 0)"
        }else{
            dataDict["userId"] = "Guest_User"
            dataDict["roleId"] = "2"
        }
        dataDict["quotationId"] = self.quotationViewController.quoteID
        if self.quotationViewController.selectedPaymentType == "ADVANCE"{
            dataDict["bookingAmountType"] = "A"
        }else if self.quotationViewController.selectedPaymentType == "FULL_PAYMENT"{
            dataDict["bookingAmountType"] = "F"
        }else{
            dataDict["bookingAmountType"] = "U"
        }
        dataDict["bookingAmount"] = self.quotationViewController.selectedPaymentAmount;
        printLog("Promocode request : ", dataDict.jsonRepresentation())
        //https://services-uatastra.sotc.in/holidayRS/pdpPromocode
        PreBookingCommunicationManager.getPrebookingData(requestType:"POST", queryParam: nil, pathParam:nil, jsonParam: dataDict, headers: nil, requestURL: kAstraUrlPromocode, returnInCaseOffailure: false)
        {
            (status,response) in
            
            printLog("Status is \(status)")
            
            printLog("Response is \(String(describing: response))")
            if (status){
                if let object = response as? Dictionary<String,Any>
                {
                    if let msg = object[ "message"] as? String{
                        if (msg.uppercased()=="SUCCESS"){
                            AppUtility.displayToastMessage("Promo Code Applied.")
                            self.pricingModel.discountAdvancePrice = self.pricingModel.promocodeFixedAmount
                            self.pricingModel.promocodeAmount = self.pricingModel.promocodeFixedAmount
                            //                                let isAfterPromoApplyAmount = "\(Double((Int(self.pricingModel.totalTourCost) ?? 15000)) - self.pricingModel.promocodeAmount)"
                            //                                let isAfterPromoApplyAmount = "\((Double(self.pricingModel.totalTourCost) ?? 15000.0) - self.pricingModel.promocodeAmount)"
                            //                                self.pricingModel.totalTourCost = isAfterPromoApplyAmount
                            self.pricingModel.applyPromocodeSuccess = true
                            self.tableViewQuotation.reloadData()
                            printLog("Promo Code Applied One")
                        }
                    }else{
                        AppUtility.displayToastMessage("Promo Code Failed.")
                    }
                }else{
                    AppUtility.displayToastMessage("Promo Code Failed.")
                }
            }
            
            
        }
    }
    
    func fetchPromocodePricingByDelegate(promocodeText : String, flag:Bool){
        LoadingIndicatorView.show("Loading")
        var dataDict : Dictionary<String,Any> = Dictionary.init()//
        /*
         {
         appUsed = Desktop;
         bookingAmount = "30000.000000";
         bookingAmountType = F;
         promoCode = "";
         quotationId = "CMgNfwHVWyzjEJESQmkm%2FA%3D%3D";
         userId = "jchalke@gmail.com";
         }
         */
        dataDict["promocode"] = promocodeText
        //                dataDict["promocode"] = "kashtest"
        dataDict["onBehalf"] = "N"
        let userDetails : Dictionary<String,Any>? = UserDefaults.standard.dictionary(forKey: Constant.userProfile.userDetails)
        if (userDetails != nil){
            dataDict["userId"] = userDetails!["userId"] as? String ?? ""
            dataDict["roleId"] = "\(userDetails!["roleId"] as? Int ?? 0)"
        }else{
            dataDict["userId"] = "Guest_User"
            dataDict["roleId"] = "2"
        }
        dataDict["quoteId"] = self.quotationViewController.quoteID
        if self.quotationViewController.selectedPaymentType == "ADVANCE"{
            dataDict["paymentType"] = "A"
        }else if self.quotationViewController.selectedPaymentType == "FULL_PAYMENT"{
            dataDict["paymentType"] = "F"
        }else{
            dataDict["paymentType"] = "U"
        }
        dataDict["appUsed"] = "APP"
        //            "holidayRS/holidayBooking/pdpPromocode" //kAstraUrlPromocode , @"tcHolidayRS/quotation/promocode"
        printLog("Promocode Apply Service Dict : ", dataDict.jsonRepresentation())
        PreBookingCommunicationManager.getPrebookingData(requestType:"POST", queryParam: nil, pathParam:nil, jsonParam: dataDict, headers: nil, requestURL: kAstraPdpPromocodeURL, returnInCaseOffailure: false)
        {
            (status,response) in
            printLog("Status is \(status)")
            printLog("Response is \(String(describing: response))")
            if let object = response as? Dictionary<String,Any>
            {
                printLog("PromoCodeApplied is \(object)")
                let promoCodeAmount : Dictionary<String,Any> = object["promoCodeAmount"] as? Dictionary<String,Any> ?? Dictionary.init()
                let discount : Double = promoCodeAmount["discountAmount"] as? Double ?? 0.0
                if discount !=  0{
                    LoadingIndicatorView.hide()
                    if (flag){
                        AppUtility.displayToastMessage("Promo Code Applied.")
                    }
                    
                    //Hide when dont update Quote Scren Parameter
                    //                            AppUtility.fillPricingModelWithResponseDict(dict: object, pricingModel: self.pricingModel, quotationView: self.quotationViewController)
                    self.pricingModel.promocodeFixedAmount = discount // Set API REponse AMount
                    self.pricingModel.discountAdvancePrice = self.pricingModel.promocodeFixedAmount
                    self.pricingModel.promocodeAmount = self.pricingModel.promocodeFixedAmount
                    self.pricingModel.promocodeAppliedText = promocodeText
                    self.pricingModel.applyPromocodeSuccess = true
                    self.tableViewQuotation.reloadData()
                    printLog("Promo Code Applied Two")
                }else{
                    
                    let promoCodeAmount : Dictionary<String,Any> = object["promoCodeAmount"] as? Dictionary<String,Any> ?? Dictionary.init()
                    AppUtility.displayToastMessage(promoCodeAmount["errorMsg"] as? String ?? "Promocode Not Valid")
                }
                
                
            }else{
                AppUtility.displayToastMessage("Promocode Not Valid")
            }
        }
    }
    func throwUserBack(){
        self.pricingModel.packageClassId = self.pricingModel.mainPackageClassId
        self.quoteDelegate?.backFromQuotation()
        self.navigationController?.popViewController(animated: true)
    }
    //MARK:-  Calling Fare Calender Again In Quote
    func fetchCalenderInQuotation(withSubClassId:Int, flag:Bool){
        LoadingIndicatorView.show("Loading")
        self.pricingModel.packageClassId = withSubClassId
        CalenderCommManager.fetchFareCaleder(pricingModel: self.pricingModel, packageDetailModel: self.packageDetailModel, selectDateStr: self.pricingModel.pricingTourDate) { (status, dayModel) in
            if status{
                if let isDate = dayModel{
                    if let availDate = isDate.date{
                        self.pricingModel.pricingTourDate = AppUtility.convertDateToString(date: availDate, dateFormat: "dd-MM-yyyy")
                        self.pricingModel.ltProdCode = isDate.ltProdCode
                        self.pricingModel.ltItineraryCode = isDate.ltItineraryCode
                        self.pricingModel.onReqeustDate = isDate.onRequest
                        self.selectDeluxPremiumInQuote(withSubClassId: self.pricingModel.packageClassId, flag: true)
                    }else{
                        self.alertViewControllerSingleton(title: "Alert", message: "Oops! Due to limited availability, we are not able to process your request through mobile app. Please click on the below mentioned Call button and share your details \(ContactHelpline.TCHelpLineNumber)", type: .alert, buttonArryName: ["Ok"]) { (strin) in
                            printLog(strin)
                            self.throwUserBack()
                        }
                    }
                    
                }else{
                    self.alertViewControllerSingleton(title: "Alert", message: "Oops! Due to limited availability, we are not able to process your request through mobile app. Please click on the below mentioned Call button and share your details \(ContactHelpline.TCHelpLineNumber)", type: .alert, buttonArryName: ["Ok"]) { (strin) in
                        printLog(strin)
                        self.throwUserBack()
                    }
                }
                
            }else {
                self.alertViewControllerSingleton(title: "Alert", message: "Oops! Due to limited availability, we are not able to process your request through mobile app. Please click on the below mentioned Call button and share your details \(ContactHelpline.TCHelpLineNumber)", type: .alert, buttonArryName: ["Ok"]) { (strin) in
                    printLog(strin)
                    self.throwUserBack()

//                    if (withSubClassId == self.pricingModel.mainPackageClassId){
//                        self.alertViewControllerSingleton(title: "Alert", message: "Oops! Due to limited availability, we are not able to process your request through mobile app. Please click on the below mentioned Call button and share your details \(ContactHelpline.TCHelpLineNumber)", type: .alert, buttonArryName: ["Ok"]) { (strin) in
//                            self.throwUserBack()
//                        }
//                    }else{
//                      self.selectDeluxPremiumInQuote(withSubClassId: self.pricingModel.mainPackageClassId, flag: true)
//                    }
                }
            }
        }
        
    }
    func showOopsAlertOnly(){
        self.alertViewControllerSingleton(title: "Alert", message: "Oops! Due to limited availability, we are not able to process your request through mobile app. Please click on the below mentioned Call button and share your details \(ContactHelpline.TCHelpLineNumber)", type: .alert, buttonArryName: ["Ok"]) { (strin) in
            printLog(strin)
        }
    }
//MARK:-  DeluxPremium Delegates
    
    func selectDeluxPremium(withSubClassId:Int, flag:Bool){
        
        self.fetchCalenderInQuotation(withSubClassId: withSubClassId, flag: flag)
        
    }
    func selectDeluxPremiumInQuote(withSubClassId:Int, flag:Bool){
        LoadingIndicatorView.show("Loading")
        self.pricingModel.packageClassId = withSubClassId
        let dataDict : Dictionary<String,Any> = AppUtility.getPricingRequestJson(packageDetailModel: self.packageDetailModel, pricingModel: self.pricingModel, optionalArray: nil)
        // "holidayRS/pricing"
        print("DataDict \(dataDict)")
        PreBookingCommunicationManager.getPrebookingData(requestType:"POST", queryParam: nil, pathParam:nil, jsonParam: dataDict, headers: nil, requestURL:kAstraUrlPricing , returnInCaseOffailure: false)
        {
            (status,response) in
            
            printLog("status is \(status)")
            printLog("response is \(String(describing: response))")
            if status
            {
                
                if let responseDict : Dictionary<String,Any> =  response as? Dictionary<String,Any>
                {
                    if let msg : String = responseDict["message"] as? String{
                        if msg != ""{
//                            AppUtility.displayAlert(title: "Alert", message: msg)
                            self.alertViewControllerSingleton(title: "Alert", message: msg, type: .alert, buttonArryName: ["Ok"]) { (strin) in
                            }
                        }
                    }else{
                        printLog("responseDict is \(responseDict)")
                        AppUtility.fillPricingModelWithResponseDict(dict: responseDict, pricingModel: self.pricingModel, quotationView: self.quotationViewController)
                        if(responseDict["totalPrice"]as? Double ?? 0 != 0){
                            self.quotationViewController.selectedPaymentType = "FULL_PAYMENT"
                            self.quotationViewController.selectedPaymentAmount = responseDict["totalPrice"] as? Double ?? 0
                            //                                self.pricingDict = responseDict
                            //                                self.quotationViewController.quoteID = responseDict["quotationId"] as? String ?? ""
                            if(self.pricingModel.applyPromocodeSuccess && self.pricingModel.promocodeAppliedText.count > 0){
                                printLog("PROMOCODE IS ALREADY APPLIED \(self.pricingModel.promocodeAppliedText)")
                                self.fetchPromocodePricingByDelegate(promocodeText: self.pricingModel.promocodeAppliedText, flag: flag)
                            }
                            self.tableViewQuotation.reloadData()
                        }else{
//                            AppUtility.displayAlert(title: "Alert", message: "Oops! Due to limited availability, we are not able to process your request through mobile app. Please click on the below mentioned Call button and share your details 18002093344")
                            self.alertViewControllerSingleton(title: "Alert", message: "Oops! Due to limited availability, we are not able to process your request through mobile app. Please click on the below mentioned Call button and share your details \(ContactHelpline.TCHelpLineNumber)", type: .alert, buttonArryName: ["Ok"]) { (strin) in
                                printLog(strin)
                                self.throwUserBack()
//                                if (withSubClassId == self.pricingModel.mainPackageClassId){
//                                    self.alertViewControllerSingleton(title: "Alert", message: "we are not able to process your request through mobile app. Please click on the below mentioned Call button and share your details \(ContactHelpline.TCHelpLineNumber)", type: .alert, buttonArryName: ["Ok"]) { (strin) in
//                                    }
//                                }else{
//                                  self.selectDeluxPremiumInQuote(withSubClassId: self.pricingModel.mainPackageClassId, flag: true)
//                                }
                                
                            }
                        }
                    }
                }
            }else{
                if let responseDict : Dictionary<String,Any> =  response as? Dictionary<String,Any>{
                    if let msg : String = responseDict["message"] as? String{
//                        AppUtility.displayAlert(title: "Alert", message: msg)
                        self.alertViewControllerSingleton(title: "Alert", message: msg, type: .alert, buttonArryName: ["Ok"]) { (strin) in
                            printLog(strin)
                        }
                    }else{
//                        AppUtility.displayAlert(title: "Alert", message: "Some error has occurred")
                        self.alertViewControllerSingleton(title: "Alert", message: "Some error has occurred", type: .alert, buttonArryName: ["Ok"]) { (strin) in
                            printLog(strin)
                        }
                    }
                }else{
//                    AppUtility.displayAlert(title: "Alert", message: "Some error has occurred")
                    self.alertViewControllerSingleton(title: "Alert", message: "Some error has occurred", type: .alert, buttonArryName: ["Ok"]) { (strin) in
                        printLog(strin)
                    }
                }
            }
        }
    }
}
