//
//  QuotationViewControlModel.swift
//  sotc-consumer-application
//
//  Created by Mac on 21/08/18.
//  Copyright © 2018 Mobicule Technologies Private Limited. All rights reserved.
//

import Foundation
class QuotationView : NSObject
{
    var expandPriceAndRoomingDetail : Bool
    var expandDefaultOptionalView : Bool
    var expantPaymentView : Bool
    var expantThreeFiveStarView : Bool
    var expandPromocodeView : Bool = true
    var selectedPaymentType : String =  ""
    var selectedPaymentAmount : Double =  0.0
    var usersChoiceAmount : Double =  0.0
    var quoteID : String = ""
    override init()
    {
        self.expandPriceAndRoomingDetail = false
        self.expandDefaultOptionalView = false
        self.expantThreeFiveStarView = false
        self.expantPaymentView = true
        self.expandPromocodeView = false
    }
    func getSectionCount() -> Int{
        var count : Int = 1 // VJ_Commented
//        var count : Int = 0 // VJ_Added
        if self.expandPromocodeView
        {
            count = count + 1
        }
        if self.expandDefaultOptionalView
        {
            count = count + 1
        }
        if self.expantThreeFiveStarView
        {
            count = count + 1
        }
        if self.expantPaymentView
        {
            count = count + 1
        }
        return count
    }
}

// PackageDetailsModelClass

class PackageDetailsModel: NSObject
{
    var arrayPackageImagePathList : Array<Any>
    var tcilHolidaySightseeingCollection : Array<Dictionary<String,Any>>
    var tcilHolidayMealCollection : Array<Dictionary<String,Any>>
    var tcilHolidayTimelineCollection : Array<Dictionary<String,Any>>
    var tcilHolidayTransfersCollection : Array<Dictionary<String,Any>>
    var tcilHolidayVisaCollection : Array<Dictionary<String,Any>>
    var tcilHolidayFlightsCollection : Array<Dictionary<String,Any>>
    var tcilHolidayIncludeExcludeCollection : Array<Dictionary<String,Any>>
    var tcilHolidayAccomodationCollection : Array<Dictionary<String,Any>>
    var tcilHolidayItineraryCollection : Array<Dictionary<String,Any>>
    var tcilHolidayPriceCollection: Array<Dictionary<String,Any>>
    var ltPricingCollection: Array<Dictionary<String,Any>>
    var tcilHolidayOptionalsCollection: Array<Dictionary<String,Any>>
    var tcilHolidayPaymentTermsCollection: Array<Dictionary<String,Any>>
    var tcilHolidayOffersCollection: Array<Dictionary<String,Any>>
    var tcilHolidayCategoryCollection: Array<Dictionary<String,Any>>
    var pkgSubtypeId: Dictionary<String,Any>
    
    var flightDefaultMsg : String
    var mealsDefaultMsg : String
    var sightseeingDefaultMsg : String
    var transferDefaultMsg : String
    var visaDefaultMsg : String
    var hotelDefaultMsg : String
    var pkgStatusId: Int
    
    var isMealsIncluded : String
    var isSightseeingIncluded : String
    var isTransferIncluded : String
    var isVisaIncluded : String
    var isHotelIncluded : String
    var isFlightIncluded : String
    
    var isFlightDefaultMsg : String
    var isHotelDefaultMsg : String
    var isSightseeingDefaultMsg : String
    var isTransferDefaultMsg : String
    var isVisaDefaultMsg : String
    var isMealsDefaultMsg : String
    
    var packageId : String
    var pkgName : String
    var ltMarket : String
    var isOfferVisible : String
    var packageThumbnailImage : String

    
    
    var startingPriceStandard : Int
    var startingPriceDelux : Int
    var startingPricePremium : Int
    
    var duration : Int
    var reviewRating : Int
    var arrayTourType : Array<String>
    var packageTypeId : Int
    var packageSubTypeName : String
    var packageSubTypeId : Int
    var productId : Int
    var isPackageClassStandard:String
    var isPackageClassDelux:String
    var isPackageClassPremium:String
    var pkgThumbnailImage:String
    var ltItineraryCode:String
    var continentName: String
    var isAdvancePayable: String
    override init() {
        self.arrayPackageImagePathList = Array.init()
        self.tcilHolidayCategoryCollection = Array.init()
        self.tcilHolidaySightseeingCollection = Array.init()
        self.tcilHolidayMealCollection = Array.init()
        self.tcilHolidayTimelineCollection = Array.init()
        self.tcilHolidayTransfersCollection = Array.init()
        self.tcilHolidayVisaCollection = Array.init()
        self.tcilHolidayFlightsCollection = Array.init()
        self.tcilHolidayIncludeExcludeCollection = Array.init()
        self.tcilHolidayAccomodationCollection = Array.init()
        self.tcilHolidayItineraryCollection = Array.init()
        self.arrayTourType = Array.init()
        self.tcilHolidayPriceCollection = Array.init()
        self.ltPricingCollection = Array.init()
        self.tcilHolidayOptionalsCollection = Array.init()
        self.tcilHolidayPaymentTermsCollection = Array.init()
        self.tcilHolidayOffersCollection = Array.init()
        
        self.pkgSubtypeId = Dictionary.init()
        
        self.flightDefaultMsg =  ""
        self.mealsDefaultMsg =  ""
        self.sightseeingDefaultMsg =  ""
        self.transferDefaultMsg =  ""
        self.visaDefaultMsg =  ""
        self.hotelDefaultMsg = ""
        self.isMealsIncluded =  ""
        self.isSightseeingIncluded =  ""
        self.isTransferIncluded =  ""
        self.isVisaIncluded =  ""
        self.isHotelIncluded =  ""
        self.isFlightIncluded = ""
        
        self.isFlightDefaultMsg =  ""
        self.isHotelDefaultMsg =  ""
        self.isSightseeingDefaultMsg =  ""
        self.isTransferDefaultMsg =  ""
        self.isVisaDefaultMsg =  ""
        self.isMealsDefaultMsg = ""
        
        self.packageId = ""
        self.pkgName = ""
        self.ltMarket =  ""
        self.isOfferVisible = ""
        self.packageThumbnailImage = ""
        
        self.startingPriceStandard = 0
        self.startingPriceDelux = 0
        self.startingPricePremium = 0
        self.pkgStatusId = 0
        
        self.duration = 0
        self.reviewRating = 0
        self.packageSubTypeName = ""
        self.packageTypeId = 0
        self.packageSubTypeId = 0;
        self.productId = 0;
        self.isPackageClassStandard = ""
        self.isPackageClassDelux = ""
        self.isPackageClassPremium = ""
        self.pkgThumbnailImage = ""
        self.ltItineraryCode = ""
        self.continentName = ""
        self.isAdvancePayable = ""
    }
    
    func initWithDictionary(dict:Dictionary<String,Any>)
    {
        let packageDetailDict : Dictionary<String,Any> = dict["packageDetail"] as! Dictionary<String, Any>
        
        self.startingPriceStandard = dict["startingPriceStandard"] as? Int ?? 0
        self.startingPricePremium = dict["startingPricePremium"] as? Int ?? 0
        self.startingPriceDelux = dict["startingPriceDelux"] as? Int ?? 0
        self.isAdvancePayable = dict["isAdvancePayable"] as? String ?? ""
        self.arrayPackageImagePathList = packageDetailDict["tcilHolidayPhotoVideoCollection"] as? Array<Any> ?? Array.init()
        self.tcilHolidaySightseeingCollection = packageDetailDict["tcilHolidaySightseeingCollection"] as? Array<Dictionary<String, Any>> ?? Array.init()
        self.tcilHolidayMealCollection = packageDetailDict["tcilHolidayMealCollection"] as? Array<Dictionary<String, Any>> ?? Array.init()
        self.tcilHolidayTimelineCollection = packageDetailDict["tcilHolidayTimelineCollection"] as? Array<Dictionary<String, Any>> ?? Array.init()
        self.tcilHolidayTransfersCollection = packageDetailDict["tcilHolidayTransfersCollection"] as? Array<Dictionary<String, Any>> ?? Array.init()
        self.tcilHolidayVisaCollection = packageDetailDict["tcilHolidayVisaCollection"] as? Array<Dictionary<String, Any>> ?? Array.init()
        self.tcilHolidayFlightsCollection = packageDetailDict["tcilHolidayFlightsCollection"] as? Array<Dictionary<String, Any>> ?? Array.init()
        self.tcilHolidayIncludeExcludeCollection = packageDetailDict["tcilHolidayIncludeExcludeCollection"] as? Array<Dictionary<String, Any>> ?? Array.init()
        self.tcilHolidayAccomodationCollection = packageDetailDict["tcilHolidayAccomodationCollection"] as? Array<Dictionary<String, Any>> ?? Array.init()
        self.tcilHolidayItineraryCollection = packageDetailDict["tcilHolidayItineraryCollection"] as? Array<Dictionary<String, Any>> ?? Array.init()
        self.tcilHolidayOptionalsCollection = packageDetailDict["tcilHolidayOptionalsCollection"] as? Array<Dictionary<String, Any>> ?? Array.init()
        self.tcilHolidayPaymentTermsCollection = packageDetailDict["tcilHolidayPaymentTermsCollection"] as? Array<Dictionary<String, Any>> ?? Array.init()
        self.tcilHolidayOffersCollection = packageDetailDict["tcilHolidayOffersCollection"] as? Array<Dictionary<String, Any>> ?? Array.init()
        self.tcilHolidayCategoryCollection = packageDetailDict["tcilHolidayCategoryCollection"] as? Array<Dictionary<String, Any>> ?? Array.init()
        
        self.flightDefaultMsg = packageDetailDict["flightDefaultMsg"] as? String ?? ""
        self.mealsDefaultMsg = packageDetailDict["mealsDefaultMsg"] as? String ?? ""
        self.sightseeingDefaultMsg = packageDetailDict["sightseeingDefaultMsg"] as? String ?? ""
        self.transferDefaultMsg = packageDetailDict["transferDefaultMsg"] as? String ?? ""
        self.visaDefaultMsg = packageDetailDict["visaDefaultMsg"] as? String ?? ""
        self.hotelDefaultMsg = packageDetailDict["hotelDefaultMsg"] as? String ?? ""
        
        self.isMealsIncluded = packageDetailDict["isMealsIncluded"] as? String ?? ""
        self.isSightseeingIncluded = packageDetailDict["isSightseeingIncluded"] as? String ?? ""
        self.isTransferIncluded = packageDetailDict["isTransferIncluded"] as? String ?? ""
        self.isVisaIncluded = packageDetailDict["isVisaIncluded"] as? String ?? ""
        self.isHotelIncluded = packageDetailDict["isHotelIncluded"] as? String ?? ""
        self.isFlightIncluded = packageDetailDict["isFlightIncluded"] as? String ?? ""
        
        self.isFlightDefaultMsg = packageDetailDict["isFlightDefaultMsg"] as? String ?? ""
        self.isHotelDefaultMsg = packageDetailDict["isHotelDefaultMsg"] as? String ?? ""
        self.isSightseeingDefaultMsg = packageDetailDict["isSightseeingDefaultMsg"] as? String ?? ""
        self.isTransferDefaultMsg = packageDetailDict["isTransferDefaultMsg"] as? String ?? ""
        self.isVisaDefaultMsg = packageDetailDict["isVisaDefaultMsg"] as? String ?? ""
        self.isMealsDefaultMsg = packageDetailDict["isMealsDefaultMsg"] as? String ?? ""
        self.pkgStatusId = packageDetailDict["pkgStatusId"] as? Int ?? 0
        
        self.pkgSubtypeId  = packageDetailDict["pkgSubtypeId"] as? Dictionary<String, Any> ?? Dictionary.init()
        self.packageId = packageDetailDict["packageId"] as? String ?? ""
        self.pkgName = packageDetailDict["pkgName"] as? String ?? ""
        self.packageThumbnailImage = packageDetailDict["packageThumbnailImage"] as? String ?? ""
        self.ltMarket = packageDetailDict["ltMarket"] as? String ?? ""
        self.duration = packageDetailDict["duration"] as? Int ?? 0
        self.ltItineraryCode = packageDetailDict["ltItineraryCode"] as? String ?? ""
        self.isOfferVisible = packageDetailDict["isOfferVisible"] as? String ?? ""
        if let tcilHolidayCityCollection : Array<Dictionary<String,Any>> = packageDetailDict["tcilHolidayCityCollection"] as? Array<Dictionary<String, Any>>
        {
            if tcilHolidayCityCollection.count > 0
            {
                if let dict: Dictionary<String,Any> = tcilHolidayCityCollection.first
                {
                    if let cityCodeDict : Dictionary<String,Any> = dict["cityCode"]  as? Dictionary<String, Any>
                    {
                        if let tcilMstCountryStateMapping : Dictionary<String,Any> = cityCodeDict["tcilMstCountryStateMapping"]  as? Dictionary<String, Any>
                        {
                            if let tcilMstCountryContinentMappingDict : Dictionary<String,Any> = tcilMstCountryStateMapping["tcilMstCountryContinentMapping"] as? Dictionary<String, Any>
                            {
                                if let countryContinentId :Dictionary<String,Any> = tcilMstCountryContinentMappingDict["countryContinentId"] as? Dictionary<String, Any>
                                {
                                    self.continentName = countryContinentId["continentName"] as? String ?? ""
                                }
                            }
                        }
                    }
                }
            }
            
        }
        //////////////
        
        self.isPackageClassStandard = (packageDetailDict["isPackageClassStandard"] as! String).lowercased()
        
        if self.isPackageClassStandard == "y"
        {
            arrayTourType.append("Standard")
        }
        
        self.isPackageClassDelux = (packageDetailDict["isPackageClassDelux"] as! String).lowercased()
        
        if self.isPackageClassDelux == "y"
        {
            arrayTourType.append("Delux")
        }
        
        self.isPackageClassPremium = (packageDetailDict["isPackageClassPremium"] as! String).lowercased()
        
        if self.isPackageClassPremium == "y"
        {
            arrayTourType.append("Premium")
        }
        
        self.packageSubTypeName = ((packageDetailDict["pkgSubtypeId"] as! Dictionary<String, Any>)["pkgSubtypeName"] as! String).lowercased()
        
        self.packageTypeId = ((packageDetailDict["pkgSubtypeId"] as! Dictionary<String, Any>)["pkgTypeId"] as! Int)   //
        
        self.packageSubTypeId = ((packageDetailDict["pkgSubtypeId"] as! Dictionary<String, Any>)["pkgSubtypeId"] as! Int)
        self.productId = (packageDetailDict["productId"] as! Int)
        
        //  To get unique Hub
        self.tcilHolidayPriceCollection = packageDetailDict["tcilHolidayPriceCollection"] as! Array<Dictionary<String, Any>>
        /*var tempSet  = Set<String>()
        for dict in self.tcilHolidayPriceCollection
        {
            let hubCodeDict : Dictionary<String,Any> = dict["hubCityCode"] as? Dictionary<String,Any> ?? Dictionary.init()
            let packageCity : String = hubCodeDict["cityName"] as? String ?? ""
            tempSet.insert(packageCity)
        }
        var tempArray : Array<Dictionary<String, Any>> = Array .init()
        for hubcity in Array(tempSet)
        {
            let filteredArray : Array<Dictionary<String,Any>> = self.tcilHolidayPriceCollection.filter({ (dict : Dictionary<String,Any>) -> Bool in
                let hubCodeDict : Dictionary<String,Any> = dict["hubCityCode"] as? Dictionary<String,Any> ?? Dictionary.init()
                let packageCity : String = hubCodeDict["cityName"] as? String ?? ""
                if packageCity == hubcity
                {
                    return true
                }
                return false
            })
            
            if filteredArray.count > 0
            {
                tempArray.append(filteredArray.first!)
            }
        }
        //
        
        self.tcilHolidayPriceCollection = tempArray*/
        
        
        self.ltPricingCollection = packageDetailDict["tcilHolidayLtPricingCollection"] as! Array<Dictionary<String, Any>>
        
        self.pkgThumbnailImage = packageDetailDict["packageThumbnailImage"] as? String ?? ""
        
        
    }
}
