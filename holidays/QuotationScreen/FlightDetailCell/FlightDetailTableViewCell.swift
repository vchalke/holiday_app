//
//  FlightDetailTableViewCell.swift
//  holidays
//
//  Created by Ios_Team on 10/09/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

import UIKit

class FlightDetailTableViewCell: UITableViewCell {
    @IBOutlet weak var btn_detailFlights: UIButton!
    @IBOutlet weak var btn_chgFlights: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
