//
//  CalenderCommManager.swift
//  holidays
//
//  Created by Kush_Team on 01/08/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

import Foundation
//class LoginCommNewManager{
class CalenderCommManager : NSObject{
    
    
    
    class func showFailureAlert() {
        AppUtility.displayAlert(title: "Failure", message: "Some error is occurred!")
    }
    
    class func fetchFareCaleder(pricingModel:PricingModel,packageDetailModel:PackageDetailsModel,selectDateStr:String ,completion:@escaping(_ status:Bool,_ responseString:DayModel?) -> Void) {
        
            LoadingIndicatorView.show("Loading")
            //  {"ltItineraryCode":"2018FAR904S1","market":"EFIT","hubCode":"BLR","pkgSubTypeId":6,"pkgClassId":"0","pkgId":"PKG002815","mode":"TCIL"}
            var dataDict : Dictionary<String,Any> = Dictionary.init()//["ltItineraryCode":"2018FAR904S1","market":"EFIT","hubCode":"BLR","pkgSubTypeId":6,"pkgClassId":"0","pkgId":"PKG002815","mode":"TCIL"]
            dataDict[PricingConstants.HubCode] = pricingModel.pricinghubCode
            dataDict[PricingConstants.Market] = packageDetailModel.ltMarket
            dataDict[PricingConstants.Mode] = "TCIL" // HARDCODED ***
            dataDict[PricingConstants.pkgId] = packageDetailModel.packageId
            // Added after checking log of exist TC
             dataDict[PricingConstants.Market] = "-1"
            dataDict["isHsa"] = "N"
            
            var prodITINCodeArray:Array<String> = []
            
            if(packageDetailModel.ltItineraryCode == "-1" || packageDetailModel.ltItineraryCode == "")
            {
                var filteredArray : Array<Dictionary<String,Any>> = Array()
      
                if packageDetailModel.packageSubTypeName == "fit"
                {
                    //arrayDepartureCity = (packageDetailModel?.tcilHolidayPriceCollection)!
                    filteredArray  = AppUtility.filterArrayNestedDirectory(ArrayToBeFilter: packageDetailModel.tcilHolidayPriceCollection, withOuterKey: "hubCityCode", withInnerKey: PricingConstants.CityCode, AndStringValue: pricingModel.pricinghubCode)
                }
                else
                {
                     filteredArray  = AppUtility.filterArrayNestedDirectory(ArrayToBeFilter: packageDetailModel.ltPricingCollection, withOuterKey: "hubCode", withInnerKey: PricingConstants.CityCode, AndStringValue: pricingModel.pricinghubCode)
                    //arrayDepartureCity = (packageDetailModel?.ltPricingCollection)!
                }
               
                let filteredArrayWithClassCode : Array<Dictionary<String,Any>> = AppUtility.filterArray(ArrayToBeFilter: filteredArray, withKey: PackageDetailConstants.PackageClassId, AndStringValue: "\(pricingModel.packageClassId)")

                
                if filteredArrayWithClassCode.count > 0 //old logic
                {
                    //if let dict : Dictionary<String,Any> = filteredArray[0]
                    for dict in filteredArrayWithClassCode {
                        if let itineraryCode : String = dict[PricingConstants.LtItineraryCode] as? String
                        {
                            prodITINCodeArray.append(itineraryCode)
                        }
                    }
                }
            }
            else
            {
                prodITINCodeArray.append(packageDetailModel.ltItineraryCode)
            }
            
    //        if(prodITINCodeArray.count != 0)
    //        {
    //             dataDict[PricingConstants.LtItineraryCode] = prodITINCodeArray
    //
    //        }
    //        else
    //        {
                dataDict[PricingConstants.LtItineraryCode] = prodITINCodeArray //for testing
         //   }
            
            //dataDict[PricingConstants.LtItineraryCode] = packageDetailModel.ltItineraryCode // new logic
            
            dataDict[PricingConstants.pkgClassId] = "\(pricingModel.packageClassId)"
            
            if let pkgSubtypeId : Int = packageDetailModel.pkgSubtypeId[PricingConstants.pkgSubtypeId] as? Int
            {
                dataDict["pkgSubTypeId"] = pkgSubtypeId
            }
            
            printLog("Fare calendar request : ", dataDict.jsonRepresentation())
            //"holidayRS/pdp.compare/fareCalender"
            PreBookingCommunicationManager.getPrebookingData(requestType:"POST", queryParam: nil, pathParam:nil, jsonParam: dataDict, headers: nil, requestURL: kAstraUrlFareCalender, returnInCaseOffailure: false)
            {
                (status,response) in
                
                printLog("status is \(status)")
                
                printLog("response is \(String(describing: response))")
                if let object = response as? Dictionary<String,Any> {
    //                var startDate : Date?
                    if object.count > 0
                    {
                        if let startDateStr : String = object[PricingConstants.CalendarStartDate] as? String
                        {
    //                        startDate = AppUtility.convertStringToDate(string: startDateStr, format: "dd-MM-yyyy")
                            if let endDateStr : String = object[PricingConstants.CalendarEndDate] as? String
                            {
                                let startDate : Date? = AppUtility.convertStringToDate(string: startDateStr, format: "dd-MM-yyyy")
                                let endDate : Date? = AppUtility.convertStringToDate(string:endDateStr, format: "dd-MM-yyyy")
                                printLog(startDate as Any)
                                printLog(endDate as Any)
//                                let calendarModel = CalendarModel.init()
                                var onRequestArray : Array<Dictionary<String,Any>> = Array.init()
                                var bookableArray : Array<Dictionary<String,Any>> =  Array.init()
                                var dbResponseBean : Dictionary<String,Any> = Dictionary.init()
                                dbResponseBean = object[PricingConstants.DbResponseBean] as? Dictionary<String, Any> ?? Dictionary.init()
                                
                                if dbResponseBean.count == 0
                                {
                                    dbResponseBean = object[PricingConstants.ltResponseBean] as? Dictionary<String, Any> ?? Dictionary.init()
                                }
                                
                                
                                bookableArray = dbResponseBean[PricingConstants.Bookable] as? Array<Dictionary<String, Any>> ?? Array.init()
                                let dateFormatter = DateFormatter()
                                dateFormatter.dateFormat = "dd-MM-yyyy"
                                
                                //bookableArray = bookableArray.sorted {($0["date"] as! String) > ($1["date"] as! String)}
                                
                               //  bookableArray = bookableArray.sorted {($0(AppUtility.convertStringToDate(string: String["date"] as! String, format: "dd-MM-yyyy"))) > ($1["date"] as! String)}
                                
                                onRequestArray = dbResponseBean[PricingConstants.OnRequest] as? Array<Dictionary<String, Any>> ?? Array.init()
                                
                                if bookableArray.count == 0 && onRequestArray.count == 0
                                {
                                    self.showFailureAlert()
                                }
                                else{
                                    
                                    printLog(pricingModel.onReqeustDate)
                                    printLog(pricingModel.pricingTourDate)
                                    
                                    var selcetBookableDateArr : Array<Dictionary<String,Any>> = Array.init()
                                    var selcetRequestDateArr : Array<Dictionary<String,Any>> = Array.init()
                                    if (pricingModel.onReqeustDate){
                                        selcetRequestDateArr = onRequestArray.filter({ (modelDict) -> Bool in
//                                            let dateString = (((modelDict["DATE"] as? String)?.count)>0)
                                            if let DATE = modelDict["DATE"] as? String{
                                                return (DATE == selectDateStr)
                                            }
                                            if let date = modelDict["date"] as? String{
                                                return (date == selectDateStr)
                                            }
                                            return false
                                        })
                                    }else{
                                        selcetBookableDateArr = bookableArray.filter({ (modelDict) -> Bool in
                                            if let DATE = modelDict["DATE"] as? String{
                                                return (DATE == selectDateStr)
                                            }
                                            if let date = modelDict["date"] as? String{
                                                return (date == selectDateStr)
                                            }
                                            return false
                                        })
                                    }
                                    printLog(selcetBookableDateArr)
                                    if (selcetBookableDateArr.count>0){
                                        let selectDic = selcetBookableDateArr[0] as? Dictionary<String, Any>
                                        var mainDateString = ""
                                        var mainPrizeValue = 0
                                        if let DATE = selectDic?["DATE"] as? String{
                                            mainDateString = DATE
                                            mainPrizeValue = selectDic?["DR_PRICE"] as? Int ?? 0
                                        }
                                        if let date = selectDic?["date"] as? String{
                                            mainDateString = date
                                            mainPrizeValue = selectDic?["price"] as? Int ?? 0
                                        }
                                        let seleDate : Date? = AppUtility.convertStringToDate(string: mainDateString, format: "dd-MM-yyyy")
                                        let selecetdDateModel = DayModel.init(Date: seleDate, isbookable: true, onrequest: false, price: mainPrizeValue, prodCode: selectDic?["LT_PROD_CODE"] as? String ?? "", ltItineraryCode: selectDic?["PROD_ITIN_CODE"] as? String ?? "")
                                        completion(true,selecetdDateModel)
                                    }else if (selcetRequestDateArr.count>0){
                                        let selectDic = selcetRequestDateArr[0] as? Dictionary<String, Any>
                                        var mainDateString = ""
                                        var mainPrizeValue = 0
                                        if let DATE = selectDic?["DATE"] as? String{
                                            mainDateString = DATE
                                            mainPrizeValue = selectDic?["DR_PRICE"] as? Int ?? 0
                                        }
                                        if let date = selectDic?["date"] as? String{
                                            mainDateString = date
                                            mainPrizeValue = selectDic?["price"] as? Int ?? 0
                                        }
                                        let seleDate : Date? = AppUtility.convertStringToDate(string: mainDateString, format: "dd-MM-yyyy")
                                        let selecetdDateModel = DayModel.init(Date: seleDate, isbookable: false, onrequest: true, price: mainPrizeValue, prodCode: selectDic?["LT_PROD_CODE"] as? String ?? "", ltItineraryCode: selectDic?["PROD_ITIN_CODE"] as? String ?? "")
                                        completion(true,selecetdDateModel)
                                    }else{
                                      completion(false,nil)
                                    }
                                }
                            }else{
                                self.showFailureAlert()
                                completion(false,nil)
                            }
                            
                        }else{
                            self.showFailureAlert()
                            completion(false,nil)
                        }
                    }
                    else{
                      self.showFailureAlert()
                        completion(false,nil)
                    }
                    printLog(object)
                }
            }
        }
}


//let prodCode : String = dict["LT_PROD_CODE"] as? String ?? ""
//let itiCode : String = dict["PROD_ITIN_CODE"] as? String ?? ""
