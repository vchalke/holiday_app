//
//  PreBookingCommunicationManager.swift
//  sotc-consumer-application
//
//  Created by ketan on 17/07/18.
//  Copyright © 2018 Mobicule Technologies Private Limited. All rights reserved.
//
//https://services-uatastra.thomascook.in/tcHolidayRS/holidayBooking/pdpPromocode
import Foundation
////
class PreBookingCommunicationManager : NSObject
{   var Sessionid:String = String.init()
    var Requestid:String = String.init()
    //let UUID_str:String = AppUtility.setUUID()
    
    static var myrequestrequest : DataRequest?
    class func showFailureAlert() {
        AppUtility.displayAlert(title: "Failure", message: "Some error is occurred!")
    }
    class func getPrebookingData(requestType :String, queryParam:String?,pathParam :String?,jsonParam:Dictionary<String,Any>?, headers:Dictionary<String, Any>?,requestURL:String,returnInCaseOffailure:Bool,completion : @escaping(_ status:Bool,_ response:Any?) -> Void)
    {
        if AppUtility.checkNetConnection()
        {
            UserBookingController.getASTRASessionAndTockenID { (status, responseDict) in
                if status
                {
                    let UUIDValue = UIDevice.current.identifierForVendor!.uuidString
                    print("UUID: \(UUIDValue)")
                    let requestID : String = responseDict?["requestId"] as! String
                    let sessionID: String = responseDict?["tokenId"] as! String
                    
                    var headersDict: HTTPHeaders = [
                        "content-type": "application/json",
                        "Accept": "application/json",
                        APIConstants.SessionId:sessionID,
                        APIConstants.RequestID:requestID,
                        APIConstants.UniqueId:AppUtility.setUUID()
                        
                    ]
                    if let  headerDict  = headers
                    {
                        for (key , value) in headerDict
                        {
                            headersDict.updateValue(value as! String, forKey: key)
                        }
                    }
                    //SOTC
//                    var urlString:String = AuthenticationConstant.ASTRA_SERVICES_URL + requestURL
                    //TC
                     var urlString:String = kbaseURLForAstra + requestURL
//                    var urlString:String = ""
//                    urlString = (requestURL == kAstraUrlPricing) ? kbaseURLForAstraNew + requestURL : kbaseURLForAstra + requestURL
                    if let queryString = queryParam
                    {
                        // Test why Quotatiuon ID Contain %25
//                        if (kAstraUrlPreConfirmationBooking == requestURL){
//                             urlString.append(queryString)
//                        }else{
//                            let queryWithoutSpace :String = queryString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
//                            urlString .append(queryWithoutSpace)
//                        }
                        let queryWithoutSpace :String = queryString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                        urlString .append(queryWithoutSpace)
                    }
                    
                    if let pathParamString = pathParam
                    {
                        urlString .append(pathParamString)
                    }
                    
                    
                    var requestTypeString:String
                    
                    if requestType == "GET"
                    {
                        requestTypeString = "GET"
                    }
                    else
                    {
                        requestTypeString = "POST"
                    }
                    
                    
                    request(urlString, method:HTTPMethod(rawValue: requestTypeString)!, parameters: jsonParam, encoding: JSONEncoding.default,headers:headersDict).responseJSON { response in
                        
                        printLog("Request: \(String(describing: response.request))")   // original url request
                        printLog("Response: \(String(describing: response.response))") // http url response
                        
                        
                        if let json = jsonParam
                        {
                            printLog(String(data: try! JSONSerialization.data(withJSONObject: json , options: .prettyPrinted), encoding: .utf8 )!)

                        }
                        
                        LoadingIndicatorView.hide()
                        printLog("Result: \(response.result)")
                        
                        if let json = response.result.value {
                            //printLog("JSON: \(json)") // serialized json response
                            
                            printLog("JSON: \(String(data: try! JSONSerialization.data(withJSONObject: json, options: .prettyPrinted), encoding: .utf8 )!)")
                            
                            
                            if let responseData:Dictionary<String,Any> = json as? Dictionary<String,Any>
                            {
                                completion(true,responseData)
                                return
                            }
                            else if let responseData:Array<Dictionary<String,Any>> = json as? Array<Dictionary<String,Any>>
                            {
                                completion(true,responseData)
                                return
                            }
                            else
                            {
                                if returnInCaseOffailure
                                {
                                    completion(false,nil)
                                }
                                else
                                {
                                    self.showFailureAlert()
                                    completion(false,nil)
                                }
                                return
                            }
                        }
                        
                        if returnInCaseOffailure
                        {
                            completion(false,nil)
                            
                        }
                        else
                        {
                            self.showFailureAlert()
                            completion(false,nil)
                        }
                        return
                        
                    }
                }
                else
                {
                    LoadingIndicatorView.hide()
                    if returnInCaseOffailure
                    {
                        completion(false,nil)
                        
                    }
                    else
                    {
                        self.showFailureAlert()
                        completion(false,nil)
                    }
                }
            }
        }
        else
        {
            LoadingIndicatorView.hide()
             AppUtility.displayAlert(title: "Failure", message: "No Internet Connection")
        }
    }
 /////
    class func getSearchData(requestType :String, queryParam:String?,pathParam :String?,jsonParam:String?, headers:Dictionary<String, Any>?,requestURL:String,returnInCaseOffailure:Bool,completion : @escaping(_ status:Bool,_ response:Any?) -> Void)
    {

            if AppUtility.checkNetConnection()
            {
                UserBookingController.getASTRASessionAndTockenID { (status, responseDict) in
                    if status
                    {
                        
                        let requestID : String = responseDict?["requestId"] as! String
                        let sessionID: String = responseDict?["tokenId"] as! String
                        
                        var headersDict: HTTPHeaders = [
                            "content-type": "application/json",
                            "Accept": "application/json",
                            APIConstants.SessionId:sessionID,
                            APIConstants.RequestID:requestID,
                            APIConstants.UniqueId:AppUtility.setUUID()
                        ]
                        if let  headerDict  = headers
                        {
                            for (key , value) in headerDict
                            {
                                headersDict.updateValue(value as! String, forKey: key)
                            }
                        }
                        
//                        var urlString:String = AuthenticationConstant.ASTRA_SERVICES_URL + "holidayRS" + requestURL//
                        var urlString:String = kbaseURLForAstra + "tcHolidayRS" + requestURL
                        if let queryString = queryParam
                        {
                            urlString .append(queryString)
                        }
                        
                        if let pathParamString = pathParam
                        {
                            urlString .append(pathParamString)
                        }
                        
                        
                        var requestTypeString:String
                        
                        if requestType == "GET"
                        {
                            requestTypeString = "GET"
                        }
                        else
                        {
                            requestTypeString = "POST"
                        }
                        if myrequestrequest != nil
                        {
                            myrequestrequest?.cancel()
                        }
                      
                        myrequestrequest = request(urlString, method:HTTPMethod(rawValue: requestTypeString)!, parameters: nil, encoding: JSONEncoding.default,headers:headersDict).responseJSON { response in
                            DispatchQueue.main.async {
                                
                                printLog("Request: \(String(describing: response.request))")   // original url request
                                printLog("Response: \(String(describing: response.response))") // http url response
                                //printLog(String(data: try! JSONSerialization.data(withJSONObject: object, options: .prettyPrinted), encoding: .utf8 )!)
                                LoadingIndicatorView.hide()
                                printLog("Result: \(response.result)")
                                
                                if let json = response.result.value {
                                    //printLog("JSON: \(json)") // serialized json response
                                    
                                    printLog("JSON: \(String(data: try! JSONSerialization.data(withJSONObject: json, options: .prettyPrinted), encoding: .utf8 )!)")
                                    
                                    
                                    if let responseData:Dictionary<String,Any> = json as? Dictionary<String,Any>
                                    {
                                        completion(true,responseData)
                                        return
                                    }
                                    else if let responseData:Array<Dictionary<String,Any>> = json as? Array<Dictionary<String,Any>>
                                    {
                                        completion(true,responseData)
                                        return
                                    }
                                    else
                                    {
                                        if returnInCaseOffailure
                                        {
                                            completion(false,nil)
                                        }
                                        else{
                                            //self.showFailureAlert()
                                        }
                                        return
                                        
                                    }
                                }
                                
                                if returnInCaseOffailure
                                {
                                    completion(false,nil)
                                }
                                else{
                                    //self.showFailureAlert()
                                }
                                return
                                
                            }
                        }
                      
                    }
                }
            }
            else
            {
                 AppUtility.displayAlert(title: "Failure", message: "No Internet Connection")
            }
    }
   
    //MARK: - URl encoded
    
    class func requestWithUrlEncodedParamter(ForDict requestDict : Dictionary<String,String>,completion : @escaping(_ status:Bool,_ response:Any?) -> Void)
    {
        LoadingIndicatorView.show();
        
        var urlString = String(format:AuthenticationConstant.ASTRA_REQUEST_TO_CALL_URL +  "?")
        for (key,value) in requestDict
        {
            urlString.append(key + "=" + value + "&")
        }
        urlString.removeLast()
        let serviceUrl = URL(string: urlString.replacingOccurrences(of: " ", with: "%20"))
        printLog("serviceUrl---> \(String(describing: serviceUrl))")
        
        URLSession.shared.dataTask(with: serviceUrl!, completionHandler:
            {
                (data, response, error) in
                
                DispatchQueue.main.async { () -> Void in
                    LoadingIndicatorView.hide()
                }
                
                if(error != nil)
                {
                    printLog("error")
                }
                else
                {
                    if let data = data, let stringResponse = String(data: data, encoding: .utf8)
                    {
                        printLog("Response---< \(stringResponse)")
                        completion(true, data)
                        
                    }
                }
        }).resume()
    }  
    //MARK:- Banner last level Data

    class func getData(requestType :String, queryParam:String?,pathParam :String?,jsonParam:Dictionary<String,Any>?, headers:HTTPHeaders?,requestURL:String,returnInCaseOffailure:Bool,url : String,completion : @escaping(_ status:Bool,_ response:Any?) -> Void)
    {
        if AppUtility.checkNetConnection()
        {
            var urlString:String = url
            
            if let queryString = queryParam
            {
                urlString .append(queryString)
            }
            
            if let pathParamString = pathParam
            {
                urlString .append(pathParamString)
            }
            
            
            var requestTypeString:String
            
            if requestType == "GET"
            {
                requestTypeString = "GET"
            }
            else
            {
                requestTypeString = "POST"
            }
            if myrequestrequest != nil
            {
                myrequestrequest?.cancel()
            }
            
              request(urlString, method:HTTPMethod(rawValue: requestTypeString)!, parameters: nil, encoding: JSONEncoding.default,headers:headers).responseJSON { response in
                DispatchQueue.main.async {
                    
                    printLog("Request: \(String(describing: response.request))")   // original url request
                    printLog("Response: \(String(describing: response.response))") // http url response
                    //printLog(String(data: try! JSONSerialization.data(withJSONObject: object, options: .prettyPrinted), encoding: .utf8 )!)
                    LoadingIndicatorView.hide()
                    printLog("Result: \(response.result)")
                    
                    if let json = response.result.value {
                        //printLog("JSON: \(json)") // serialized json response
                        
                        printLog("JSON: \(String(data: try! JSONSerialization.data(withJSONObject: json, options: .prettyPrinted), encoding: .utf8 )!)")
                        
                        
                        if let responseData:Dictionary<String,Any> = json as? Dictionary<String,Any>
                        {
                            completion(true,responseData)
                            return
                        }
                        else if let responseData:Array<Dictionary<String,Any>> = json as? Array<Dictionary<String,Any>>
                        {
                            completion(true,responseData)
                            return
                        }
                        else
                        {
                            if returnInCaseOffailure
                            {
                                completion(false,nil)
                            }
                            else{
                                //self.showFailureAlert()
                            }
                            return
                            
                        }
                    }
                    
                    if returnInCaseOffailure
                    {
                        completion(false,nil)
                    }
                    else{
                        //self.showFailureAlert()
                    }
                    return
                    
                }
            }
        }
    
        
        else
        {
            LoadingIndicatorView.hide()
            AppUtility.displayAlert(title: "Failure", message: "No Internet Connection")
        }
    }
    //MARK:- Profile Data
    @objc class func getProfileData(requestType :String, queryParam:String?,pathParam :String?,jsonParam:Dictionary<String,Any>?, headers:Dictionary<String, Any>?,requestURL:String,returnInCaseOffailure:Bool,completion : @escaping(_ status:Bool,_ response:Any?) -> Void)
    {
        if AppUtility.checkNetConnection()
        {
//            UserBookingController.getASTRASessionAndTockenID { (status, responseDict) in
//                if status
//                {
                    
//                    let requestID : String = responseDict?["requestId"] as! String
//                    let sessionID: String = responseDict?["tokenId"] as! String
//                    let userSessionID:String = UserDefaults.standard.value(forKey: APIResponseConstants.USER_SESSION_ID) as? String ?? ""
//                    let user:String = UserDefaults.standard.value(forKey: APIConstants.User) as? String ?? ""
                    
                    
                     var headersDict: HTTPHeaders = [
                     "content-type": "application/json",
                     "Accept": "application/json",
                     //                        APIConstants.RequestID:UserDefaults.standard.string(forKey: APIConstants.RequestID) ?? "",
                     //                        APIConstants.SessionId:UserDefaults.standard.string(forKey: APIConstants.SessionId) ?? "",
                     APIConstants.RequestID:UserDefaults.standard.string(forKey: kLoginRequestID) ?? "",
                     APIConstants.SessionId:UserDefaults.standard.string(forKey: kLoginTokenID) ?? "",
//                     APIResponseConstants.USER_SESSION_ID:userSessionID,
//                     APIConstants.User:user,
                     APIConstants.UniqueId:AppUtility.setUUID()
                     ]
                     
            /*
                    var headersDict: HTTPHeaders = [
                        "Content-Type": "application/json",
                        "Accept": "application/json",
                        APIConstants.RequestID:UserDefaults.standard.string(forKey: kLoginRequestID) ?? "",
                        APIConstants.SessionId:UserDefaults.standard.string(forKey: kLoginTokenID) ?? ""
//                        APIConstants.UniqueId:UUID
                    ]
            */
                    if let  headerDict  = headers
                    {
                        for (key , value) in headerDict
                        {
                            headersDict.updateValue(value as! String, forKey: key)
                        }
                    }
                    
                    //                    var urlString:String = AuthenticationConstant.ASTRA_SERVICES_URL + requestURL
                    var urlString:String = kbaseURLForAstra + requestURL
                    
                    if let queryString = queryParam
                    {
                        urlString .append(queryString)
                    }
                    
                    if let pathParamString = pathParam
                    {
                        urlString .append(pathParamString)
                    }
                    
                    
                    var requestTypeString:String
                    
                    if requestType == "GET"
                    {
                        requestTypeString = "GET"
                    }
                    else
                    {
                        requestTypeString = "POST"
                    }
                    
                    
                    request(urlString, method:HTTPMethod(rawValue: requestTypeString)!, parameters: jsonParam, encoding: JSONEncoding.default,headers:headersDict).responseJSON { response in
                        
                        printLog("Request: \(String(describing: response.request))")   // original url request
                        printLog("Response: \(String(describing: response.response))") // http url response
                        
                        
                        if let json = jsonParam
                        {
                            printLog(String(data: try! JSONSerialization.data(withJSONObject: json , options: .prettyPrinted), encoding: .utf8 )!)
                            
                        }
                        
                        LoadingIndicatorView.hide()
                        printLog("Result: \(response.result)")
                        
                        if let json = response.result.value {
                            //printLog("JSON: \(json)") // serialized json response
                            
                            printLog("JSON: \(String(data: try! JSONSerialization.data(withJSONObject: json, options: .prettyPrinted), encoding: .utf8 )!)")
                            
                            
                            if let responseData:Dictionary<String,Any> = json as? Dictionary<String,Any>
                            {
                                completion(true,responseData)
                                return
                            }
                            else if let responseData:Array<Dictionary<String,Any>> = json as? Array<Dictionary<String,Any>>
                            {
                                completion(true,responseData)
                                return
                            }
                            else
                            {
                                if returnInCaseOffailure
                                {
                                    completion(false,nil)
                                }
                                else
                                {
                                    self.showFailureAlert()
                                }
                                return
                            }
                        }
                        
                        if returnInCaseOffailure
                        {
                            completion(false,nil)
                            
                        }
                        else
                        {
                            completion(false,nil)
                            self.showFailureAlert()
                            
                        }
                        return
                        
                    }
                    
//                }
//                else
//                {
//                    LoadingIndicatorView.hide()
//                    self.showFailureAlert()
//                }
//            }
        }
        else
        {
            LoadingIndicatorView.hide()
            AppUtility.displayAlert(title: "Failure", message: "No Internet Connection")
        }
    }
    
    //MARK:- Check Booking Done or not after Web Payment
        @objc class func getBookingsConfirmations(requestType :String, queryParam:String?,pathParam :String?,jsonParam:Dictionary<String,Any>?, headers:Dictionary<String, Any>?,requestURL:String,returnInCaseOffailure:Bool,completion : @escaping(_ status:Bool,_ response:Any?) -> Void)
        {
            if AppUtility.checkNetConnection()
            {
                UserBookingController.getASTRASessionAndTockenID { (status, responseDict) in
                    if status
                    {
                        
                        let requestID : String = responseDict?["requestId"] as! String
                        let sessionID: String = responseDict?["tokenId"] as! String
                        var headersDict: HTTPHeaders = [
                            "content-type": "application/json",
                            "Accept": "application/json",
                            APIConstants.RequestID : requestID,
                            APIConstants.SessionId:sessionID
                        ]
                        if let  headerDict  = headers
                        {
                            for (key , value) in headerDict
                            {
                                headersDict.updateValue(value as! String, forKey: key)
                            }
                        }
                        var urlString:String = kbaseURLForAstra + requestURL
                        
                        if let queryString = queryParam
                        {
                            urlString .append("/\(queryString)")
                        }
                        
                        if let pathParamString = pathParam
                        {
                            urlString .append(pathParamString)
                        }
                        
                        
                        var requestTypeString:String
                        
                        if requestType == "GET"
                        {
                            requestTypeString = "GET"
                        }
                        else
                        {
                            requestTypeString = "POST"
                        }
                        
                        
                        request(urlString, method:HTTPMethod(rawValue: requestTypeString)!, parameters: jsonParam, encoding: JSONEncoding.default,headers:headersDict).responseJSON { response in
                            
                            printLog("Request: \(String(describing: response.request))")   // original url request
                            printLog("Response: \(String(describing: response.response))") // http url response
                            
                            
                            if let json = jsonParam
                            {
                                printLog(String(data: try! JSONSerialization.data(withJSONObject: json , options: .prettyPrinted), encoding: .utf8 )!)
                                
                            }
                            
                            LoadingIndicatorView.hide()
                            printLog("Result: \(response.result)")
                            
                            if let json = response.result.value {
                                //printLog("JSON: \(json)") // serialized json response
                                
                                printLog("JSON: \(String(data: try! JSONSerialization.data(withJSONObject: json, options: .prettyPrinted), encoding: .utf8 )!)")
                                
                                
                                if let responseData:Dictionary<String,Any> = json as? Dictionary<String,Any>
                                {
                                    completion(true,responseData)
                                    return
                                }
                                else if let responseData:Array<Dictionary<String,Any>> = json as? Array<Dictionary<String,Any>>
                                {
                                    completion(true,responseData)
                                    return
                                }
                                else
                                {
                                    if returnInCaseOffailure
                                    {
                                        completion(false,nil)
                                    }
                                    else
                                    {
                                        self.showFailureAlert()
                                    }
                                    return
                                }
                            }
                            
                            if returnInCaseOffailure
                            {
                                completion(false,nil)
                                
                            }
                            else
                            {
                                completion(false,nil)
                                self.showFailureAlert()
                                
                            }
                            return
                            
                        }
                        
                    }
                    else
                    {
                        LoadingIndicatorView.hide()
                        self.showFailureAlert()
                    }
                }
            }
            else
            {
                LoadingIndicatorView.hide()
                AppUtility.displayAlert(title: "Failure", message: "No Internet Connection")
            }
        }
    
    //MARK:- Thease methods are moved to LoginCommunicationmanager
//checkaccount
  /*  class func checkAccount(requestType :String, queryParam:String?,pathParam :String?,jsonParam:Dictionary<String,Any>?, headers:Dictionary<String, Any>?,requestURL:String,returnInCaseOffailure:Bool,completion : @escaping(_ status:Bool,_ response:Any?) -> Void)
    {
        if AppUtility.checkNetConnection()
        {
            UserBookingController.getASTRASessionAndTockenID { (status, responseDict) in
                if status
                {
                    
                    let requestID : String = responseDict?["requestId"] as! String
                    let sessionID: String = responseDict?["tokenId"] as! String
                    
                    var headersDict: HTTPHeaders = [
                        "content-type": "application/json",
                        "Accept": "application/json",
                        APIConstants.SessionId:sessionID,
                        APIConstants.RequestID:requestID
                    ]
                    if let  headerDict  = headers
                    {
                        for (key , value) in headerDict
                        {
                            headersDict.updateValue(value as! String, forKey: key)
                        }
                    }
                    
                    var urlString:String = AuthenticationConstant.ASTRA_SERVICES_URL + requestURL
                    
                    if let queryString = queryParam
                    {
                        urlString .append(queryString)
                    }
                    
                    if let pathParamString = pathParam
                    {
                        urlString .append(pathParamString)
                    }
                    
                    
                    var requestTypeString:String
                    
                    if requestType == "GET"
                    {
                        requestTypeString = "GET"
                    }
                    else
                    {
                        requestTypeString = "POST"
                    }
                    
                    
                    request(urlString, method:HTTPMethod(rawValue: requestTypeString)!, parameters: jsonParam, encoding: JSONEncoding.default,headers:headersDict).responseJSON { response in
                        
                        printLog("Request: \(String(describing: response.request ))")   // original url request
                        printLog("Response: \(String(describing: response.response))") // http url response
                        //printLog(String(data: try! JSONSerialization.data(withJSONObject: object, options: .prettyPrinted), encoding: .utf8 )!)
                        
                        
                        printLog("Result: \(response.result)")
                        
                        if let json = response.result.value {
                            //printLog("JSON: \(json)") // serialized json response
                            
                            printLog("JSON: \(String(data: try! JSONSerialization.data(withJSONObject: json, options: .prettyPrinted), encoding: .utf8 )!)")
                            
                            
                            if let responseData:Dictionary<String,Any> = json as? Dictionary<String,Any>
                            {
                                completion(true,responseData)
                                return
                            }
                            else if let responseData:Array<Dictionary<String,Any>> = json as? Array<Dictionary<String,Any>>
                            {
                                completion(true,responseData)
                                return
                            }
                            else
                            {
                                if returnInCaseOffailure
                                {
                                    completion(false,nil)
                                }
                                else
                                {
                                    self.showFailureAlert()
                                }
                                return
                            }
                        }
                        
                        if returnInCaseOffailure
                        {
                            completion(false,nil)
                            
                        }
                        else
                        {
                            self.showFailureAlert()
                            
                        }
                        return
                        
                    }
                }
                else
                {
                    LoadingIndicatorView.hide()
                    self.showFailureAlert()
                }
            }
        }
        else
        {
            LoadingIndicatorView.hide()
            AppUtility.displayAlert(title: "Failure", message: "No Internet Connection")
        }
    }
    
    
    
//savepass
    class func savePass(requestType :String, queryParam:String?,pathParam :String?,jsonParam:Dictionary<String,Any>?, headers:Dictionary<String, Any>?,requestURL:String,returnInCaseOffailure:Bool,completion : @escaping(_ status:Bool,_ response:Any?) -> Void)
    {
        if AppUtility.checkNetConnection()
        {
            UserBookingController.getASTRASessionAndTockenID { (status, responseDict) in
                if status
                {
                    
                    let requestID : String = responseDict?["requestId"] as! String
                    let sessionID: String = responseDict?["tokenId"] as! String
                    UserDefaults.standard.setValue(requestID, forKey: APIConstants.RequestID)
                    UserDefaults.standard.setValue(sessionID, forKey: APIConstants.SessionId)
                    
                   // userDict.updateValue(requestID, forKey: APIConstants.RequestID)
                    //userDict.updateValue(sessionID, forKey: APIConstants.SessionId)
                    
                    var headersDict: HTTPHeaders = [
                        "content-type": "application/json",
                        "Accept": "application/json",
                        APIConstants.SessionId:sessionID,
                        APIConstants.RequestID:requestID
                    ]
                    if let  headerDict  = headers
                    {
                        for (key , value) in headerDict
                        {
                            headersDict.updateValue(value as! String, forKey: key)
                        }
                    }
                    
                    var urlString:String = AuthenticationConstant.ASTRA_SERVICES_URL + requestURL
                    
                    if let queryString = queryParam
                    {
                        urlString .append(queryString)
                    }
                    
                    if let pathParamString = pathParam
                    {
                        urlString .append(pathParamString)
                    }
                    
                    
                    var requestTypeString:String
                    
                    if requestType == "GET"
                    {
                        requestTypeString = "GET"
                    }
                    else
                    {
                        requestTypeString = "POST"
                    }
                    
                    
                    request(urlString, method:HTTPMethod(rawValue: requestTypeString)!, parameters: jsonParam, encoding: JSONEncoding.default,headers:headersDict).responseJSON { response in
                        
                        printLog("Request: \(String(describing: response.request!))")   // original url request
                        printLog("Response: \(String(describing: response.response!))") // http url response
                        //printLog(String(data: try! JSONSerialization.data(withJSONObject: object, options: .prettyPrinted), encoding: .utf8 )!)
                        
                        printLog("Result: \(response.result)")
                        
                        if let json = response.result.value {
                            //printLog("JSON: \(json)") // serialized json response
                            
                            printLog("JSON: \(String(data: try! JSONSerialization.data(withJSONObject: json, options: .prettyPrinted), encoding: .utf8 )!)")
                            
                            
                            if let responseData:Dictionary<String,Any> = json as? Dictionary<String,Any>
                            {
                                completion(true,responseData)
                                return
                            }
                            else if let responseData:Array<Dictionary<String,Any>> = json as? Array<Dictionary<String,Any>>
                            {
                                completion(true,responseData)
                                return
                            }
                            else
                            {
                                if returnInCaseOffailure
                                {
                                    completion(false,nil)
                                }
                                else
                                {
                                    self.showFailureAlert()
                                }
                                return
                                
                            }
                        }
                        
                        if returnInCaseOffailure
                        {
                            completion(false,nil)
                            
                        }
                        else
                        {
                            self.showFailureAlert()
                            
                        }
                        return
                        
                    }
                }
                else
                {
                    LoadingIndicatorView.hide()
                    self.showFailureAlert()
                }
            }
        }
        else
        {
            LoadingIndicatorView.hide()
            AppUtility.displayAlert(title: "Failure", message: "No Internet Connection")
        }
    }

//midlware hit get text1, text2, text3
    class func getCheckData(requestType :String, queryParam:String?,pathParam :String?,jsonParam:Dictionary<String,Any>?, headers:Dictionary<String, Any>?,requestURL:String,returnInCaseOffailure:Bool,completion : @escaping(_ status:Bool,_ response:Any?) -> Void)
    {
        if AppUtility.checkNetConnection()
        {
            
                    var headersDict: HTTPHeaders = [
                        "content-type": "application/json",
                        "Accept": "application/json",
                        "Authorization" : AuthenticationConstant.Mobicule_Authorization
                    ]
                    if let  headerDict  = headers
                    {
                        for (key , value) in headerDict
                        {
                            headersDict.updateValue(value as! String, forKey: key)
                        }
                    }
                    
                    var urlString:String = "http://172.98.192.15:8081/Mobicule-Platform/api/"
                    
                    if let queryString = queryParam
                    {
                        urlString .append(queryString)
                    }
                    
                    if let pathParamString = pathParam
                    {
                        urlString .append(pathParamString)
                    }
                    
                    
                    var requestTypeString:String
                    
                    if requestType == "GET"
                    {
                        requestTypeString = "GET"
                    }
                    else
                    {
                        requestTypeString = "POST"
                    }
                    
                    
                    request(urlString, method:HTTPMethod(rawValue: requestTypeString)!, parameters: jsonParam, encoding: JSONEncoding.default,headers:headersDict).responseJSON { response in
                        
                        printLog("Request: \(String(describing: response.request))")   // original url request
                        printLog("Response: \(String(describing: response.response))") // http url response
                        //printLog(String(data: try! JSONSerialization.data(withJSONObject: object, options: .prettyPrinted), encoding: .utf8 )!)
                        
                        
                        printLog("Result: \(response.result)")
                        
                        if let json = response.result.value {
                            //printLog("JSON: \(json)") // serialized json response
                            
                            printLog("JSON: \(String(data: try! JSONSerialization.data(withJSONObject: json, options: .prettyPrinted), encoding: .utf8 )!)")
                            
                            
                            if let responseData:Dictionary<String,Any> = json as? Dictionary<String,Any>
                            {
                                completion(true,responseData)
                                return
                            }
                            else if let responseData:Array<Dictionary<String,Any>> = json as? Array<Dictionary<String,Any>>
                            {
                                completion(true,responseData)
                                return
                            }
                            else
                            {
                                if returnInCaseOffailure
                                {
                                    completion(false,nil)
                                }
                                else
                                {
                                    self.showFailureAlert()
                                    LoadingIndicatorView.hide()
                                }
                                return
                                
                            }
                        }
                        
                        if returnInCaseOffailure
                        {
                            completion(false,nil)
                            
                        }
                        else
                        {
                            self.showFailureAlert()
                            LoadingIndicatorView.hide()
                            
                        }
                        
                        return
                        
                    }
                
            }
        
        else
        {
            LoadingIndicatorView.hide()
        }
    }
  

    //MARK: - register

class func createUser(requestType :String, queryParam:String?,pathParam :String?,jsonParam:Dictionary<String,Any>?, headers:Dictionary<String, Any>?,requestURL:String,returnInCaseOffailure:Bool,completion : @escaping(_ status:Bool,_ response:Any?) -> Void)
{
    if AppUtility.checkNetConnection()
    {
        

            var headersDict: HTTPHeaders = [
                    "content-type": "application/json",
                    "Accept": "application/json",
                   // APIConstants.SessionId:userDict[APIConstants.SessionId] as! String,
                   // APIConstants.RequestID:userDict[APIConstants.RequestID] as! String
                APIConstants.RequestID:UserDefaults.standard.string(forKey: APIConstants.RequestID)!,
                APIConstants.SessionId:UserDefaults.standard.string(forKey: APIConstants.SessionId)!
                ]
                if let  headerDict  = headers
                {
                    for (key , value) in headerDict
                    {
                        headersDict.updateValue(value as! String, forKey: key)
                    }
                }
                
                var urlString:String = AuthenticationConstant.ASTRA_SERVICES_URL + requestURL
                
                if let queryString = queryParam
                {
                    urlString .append(queryString)
                }
                
                if let pathParamString = pathParam
                {
                    urlString .append(pathParamString)
                }
                
                
                var requestTypeString:String
                
                if requestType == "GET"
                {
                    requestTypeString = "GET"
                }
                else
                {
                    requestTypeString = "POST"
                }
                
               
                printLog("headers:\(headersDict)")
                request(urlString, method:HTTPMethod(rawValue: requestTypeString)!, parameters: jsonParam, encoding: JSONEncoding.default,headers:headersDict).responseJSON { response in
                    
                    if let userSessionValue:String = response.response?.allHeaderFields[APIResponseConstants.USER_SESSION_ID] as? String
                    {
                        printLog("userSession:\(userSessionValue)")
                        UserDefaults.standard.set(userSessionValue, forKey: APIResponseConstants.USER_SESSION_ID)
                    }
                    
                    printLog("Request: \(String(describing: response.request))")   // original url request
                    printLog("Response: \(String(describing: response.response))") // http url response
                    printLog("Result: \(response.result)")
                    
                    LoadingIndicatorView.hide()
                    printLog("Result: \(response.result)")
                    
                    if let json = response.result.value {
                        //printLog("JSON: \(json)") // serialized json response
                        
                        printLog("JSON: \(String(data: try! JSONSerialization.data(withJSONObject: json, options: .prettyPrinted), encoding: .utf8 )!)")
                        
                        
                        if let responseData:Dictionary<String,Any> = json as? Dictionary<String,Any>
                        {
                            completion(true,responseData)
                            return
                        }
                        else if let responseData:Array<Dictionary<String,Any>> = json as? Array<Dictionary<String,Any>>
                        {
                            completion(true,responseData)
                            return
                        }
                        else
                        {
                            if returnInCaseOffailure
                            {
                                completion(false,nil)
                            }
                            else
                            {
                                self.showFailureAlert()
                            }
                            return
                            
                        }
                    }
                    
                    if returnInCaseOffailure
                    {
                        completion(false,nil)
                        
                    }
                    else
                    {
                        self.showFailureAlert()
                        completion(false,nil)
                    }
                    return
                    
                }
            
            
        }
        
            else
            {
                LoadingIndicatorView.hide()
                self.showFailureAlert()
                completion(false,nil)
            }
        
}*/
    

}
