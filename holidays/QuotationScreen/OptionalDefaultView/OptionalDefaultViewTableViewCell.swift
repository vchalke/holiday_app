//
//  OptionalDefaultViewTableViewCell.swift
//  sotc-consumer-application
//
//  Created by Mac on 21/08/18.
//  Copyright © 2018 Mobicule Technologies Private Limited. All rights reserved.
//
//UIColor.init(red: 209/255, green: 0/255, blue: 12/255, alpha: 1)
//UIColor.init(red: 1/255, green: 84/255, blue: 164/255, alpha: 1)
//UIColor.init(red: 0/255, green: 55/255, blue: 163/255, alpha: 1)
import UIKit
protocol OptionalDefaultViewDelegate
{
    func openOptionalActivitiesView()
}

class OptionalDefaultViewTableViewCell: UITableViewCell
{
    @IBOutlet weak var textViewOptionalActivities: UITextView!
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var addActivitiesButton: UIButton!
    var delegate : OptionalDefaultViewDelegate? = nil
    override func awakeFromNib() {
        super.awakeFromNib()
      
        self.addActivitiesButton.layer.borderWidth = 1
        self.addActivitiesButton.layer.cornerRadius = 3
        self.addActivitiesButton.layer.borderColor =  UIColor.init(red: 0/255, green: 55/255, blue: 163/255, alpha: 1).cgColor
        
        self.containerView.fadedShadow()
        
    }
    func setData(packageDetailModel : PackageDetailsModel)
    {
        if packageDetailModel.tcilHolidayOptionalsCollection.count != 0
        {
        var optionalActivities : String = ""
            
      let dictCount =  packageDetailModel.tcilHolidayOptionalsCollection.count
            var finalCount:Int = 0;
//            if dictCount > 1
//            {
//                  finalCount = 2
//            }
//            else
//            {
//                 finalCount = dictCount - 1
//            }
            finalCount = dictCount
         for i in 0..<finalCount
//        for i in 0...finalCount
        {
            if let dict : Dictionary<String,Any> = packageDetailModel.tcilHolidayOptionalsCollection[i]
            {
                let optionalIDDict : Dictionary<String,Any> = dict["optionalsId"] as? Dictionary<String,Any> ?? Dictionary.init()
                if i == 2
                {
                    optionalActivities = optionalActivities + "\u{2022}  " + (optionalIDDict["name"] as? String ?? "") + " And more...\n\n"
                }
                else
                {
                    optionalActivities = optionalActivities + "\u{2022}  " + (optionalIDDict["name"] as? String ?? "") + "\n\n"
                }
            }
        }
        self.textViewOptionalActivities.text = optionalActivities
        self.textViewOptionalActivities.font = UIFont(name: "Lato-Regular", size: CGFloat(14))
        self.textViewOptionalActivities.adjustUITextViewHeight()
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
  
    @IBAction func addActivitiesButtonClicked(_ sender: Any) {
        self.delegate?.openOptionalActivitiesView()
    }
}
