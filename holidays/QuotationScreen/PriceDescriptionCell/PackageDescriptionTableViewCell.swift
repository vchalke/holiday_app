//
//  PackageDescriptionTableViewCell.swift
//  sotc-consumer-application
//
//  Created by ketan on 16/08/18.
//  Copyright © 2018 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit

class PackageDescriptionTableViewCell: UITableViewCell {

    @IBOutlet weak var totalTourperCodeCurrencyLabel: UILabel!
    @IBOutlet weak var tourTypeLabel: UILabel!
    @IBOutlet weak var noOfRoomsLabel: UILabel!
    @IBOutlet weak var noOfTravellerLabel: UILabel!
    @IBOutlet weak var totalTourperCostLabel: UILabel!
    @IBOutlet weak var discountLabel: UILabel!
    @IBOutlet weak var gstLabel: UILabel!
    @IBOutlet weak var optionalViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var totalPriceLabel: UILabel!
    @IBOutlet weak var optionalPriceLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setData(quotationView : QuotationView,pricingModel : PricingModel)
    {

        self.noOfRoomsLabel.text = "\(pricingModel.pricingArrayRooms.count)"
        var totalPax : Int = 0
        for roomModel in pricingModel.pricingArrayRooms
        {
            totalPax = totalPax + roomModel.childWithoutBedCount + roomModel.childWithBedCount + roomModel.adultCount
        }
        self.noOfTravellerLabel.text = "\(pricingModel.getAdultCount()) Adult, " +  "\(pricingModel.getchildCount())" + " Child, " +  "\(pricingModel.getInfantCount()) Infant"
        let numFormatter : NumberFormatter = NumberFormatter.init()
        // numFormatter.positiveFormat = "0"
        numFormatter.numberStyle = NumberFormatter.Style.decimal
        numFormatter.locale = Locale(identifier: "en_IN")
        self.discountLabel.text = "₹ " + (numFormatter.string(from: NSNumber.init(value: Int(pricingModel.discountAdvancePrice) )) ?? "0")
        
        if  pricingModel.totalOptionalCost == 0
        {
            self.optionalViewHeightConstraint.constant = 0
        }
        else
        {
            self.optionalViewHeightConstraint.constant = 20
        }
        self.optionalPriceLabel.text =  "₹ " + (numFormatter.string(from: NSNumber.init(value:Int(pricingModel.totalOptionalCost))) ?? "0")
        
        let main_string = "₹ " + (numFormatter.string(from: NSNumber.init(value:Int(pricingModel.totalTourCost) ?? 0)) ?? "0") //db fr
        let string_to_color = (numFormatter.string(from: NSNumber.init(value:Int(pricingModel.totalTourCost) ?? 0)) ?? "0") //db fr
        let range = (main_string as NSString).range(of: string_to_color)
        let attributedString = NSMutableAttributedString(string:main_string)
//        let colour = UIColor.init(red: 209/255, green: 0/255, blue: 12/255, alpha: 1)//Red
        let colour = UIColor.init(red: 0/255, green: 78/255, blue: 155/255, alpha: 1)//Blue
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: colour , range: range)  //red color
        self.totalPriceLabel.attributedText = attributedString
        
        
        var currencyText : String = "Total Tour Cost "
        var calculationText : String = ""
        for dict in pricingModel.currencyBreakup
        {
            if (dict["currencycode"] as? String ?? "").uppercased() == "INR"
            {
                calculationText = calculationText + "₹ " + (numFormatter.string(from: NSNumber.init(value:Int(dict["amount"] as? Double ?? 0.0))) ?? "0")
            }
            else
            {
                currencyText = currencyText + "\n" + (dict["currencycode"] as? String ?? "") + " calculated @ INR "
                currencyText = currencyText + (numFormatter.string(from: NSNumber.init(value:dict["currencyRate"] as? Double ?? 0.0)) ?? "0")
                //calculationText = calculationText + (dict["currencycode"] as? String ?? "") + " " + (numFormatter.string(from: NSNumber.init(value:Int(dict["amount"] as? Double ?? 0.0))) ?? "0")   + " + "
                calculationText = "\(calculationText)\(dict["currencycode"] as? String ?? "") \(numFormatter.string(from: NSNumber.init(value:Int(dict["amount"] as? Double ?? 0.0))) ?? "0") +"

            }
            
        }
        self.totalTourperCodeCurrencyLabel.text = currencyText
        self.totalTourperCostLabel.text = calculationText
        
        self.gstLabel.text = "₹ " + (numFormatter.string(from: NSNumber.init(value:Int(pricingModel.totalServicetax))) ?? "0")
        
    }
    
}
