//
//  PackageDescriptionViewCell.swift
//  holidays
//
//  Created by Kush_Team on 14/06/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

import UIKit

class PackageDescriptionViewCell: UITableViewCell {

    @IBOutlet weak var main_ContentView: UIView!
    @IBOutlet weak var lbl_tourType: UILabel!
    @IBOutlet weak var lbl_numOfRoom: UILabel!
    @IBOutlet weak var lbl_numOfTravel: UILabel!
    
    @IBOutlet weak var lbl_TotalTourWithINRInfo: UILabel!
    @IBOutlet weak var lbl_TotalTourWithINRCost: UILabel!
    
    @IBOutlet weak var lbl_activityOneCost: UILabel!
    @IBOutlet weak var lbl_activityTwoCost: UILabel!
    @IBOutlet weak var lbl_activityThreeCost: UILabel!
    @IBOutlet weak var lbl_activityFourCost: UILabel!
    @IBOutlet weak var lbl_activityFiveCost: UILabel!
    @IBOutlet weak var lbl_activitySixCost: UILabel!
    @IBOutlet weak var lbl_activityOptionalCost: UILabel!
    
     @IBOutlet weak var lbl_optionalTotalTitleCost: UILabel!
  
    @IBOutlet weak var view_optionalSet: UIView!
    @IBOutlet weak var view_optionalActOne: UIView!
    @IBOutlet weak var view_optionalActTwo: UIView!
    @IBOutlet weak var view_optionalActThree: UIView!
    @IBOutlet weak var view_optionalActFour: UIView!
    @IBOutlet weak var view_optionalActFive: UIView!
    @IBOutlet weak var view_optionalActSix: UIView!
    @IBOutlet weak var view_optionalActTotal: UIView!

    @IBOutlet weak var lbl_optionalActOne: UILabel!
    @IBOutlet weak var lbl_optionalActTwo: UILabel!
    @IBOutlet weak var lbl_optionalActThree: UILabel!
    @IBOutlet weak var lbl_optionalActFour: UILabel!
    @IBOutlet weak var lbl_optionalActFive: UILabel!
    @IBOutlet weak var lbl_optionalActSix: UILabel!
    @IBOutlet weak var lbl_gstPercent: UILabel!
    @IBOutlet weak var lbl_tcsPercent: UILabel!
    @IBOutlet weak var lbl_discount: UILabel!
    @IBOutlet weak var lbl_gstCost: UILabel!
    @IBOutlet weak var lbl_tcsCost: UILabel!
    @IBOutlet weak var lbl_FinalCost: UILabel!
    @IBOutlet weak var lbl_promodiscount: UILabel!
    @IBOutlet weak var lbl_TotalBigPrize: UILabel!
    
    @IBOutlet weak var view_Discount: UIView!
    @IBOutlet weak var view_gst: UIView!
    @IBOutlet weak var view_tcs: UIView!
    @IBOutlet weak var view_finalForAll: UIView!
    @IBOutlet weak var view_Promocode: UIView!
    @IBOutlet weak var view_DiscountSet: UIView!
    
    
    @IBOutlet weak var cnst_OptionalSetHeight: NSLayoutConstraint!
    @IBOutlet weak var cnst_DiscountSetHeight: NSLayoutConstraint!
    
    @IBOutlet var optionalViewArray: [UIView]!
    @IBOutlet var optionalLeftLabelArray: [UILabel]!
    @IBOutlet var optionalRightLabelArray: [UILabel]!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setData(quotationView : QuotationView,pricingModel : PricingModel, optionalArray : Array<OptionalModel>)
    {
        if(pricingModel.applyPromocodeSuccess){
            pricingModel.discountAdvancePrice = pricingModel.promocodeFixedAmount
            pricingModel.promocodeAmount = pricingModel.promocodeFixedAmount
        }
        self.lbl_tourType.text = (pricingModel.packageClassId==2) ? "Premium" : (pricingModel.packageClassId==1) ? "Delux" : "Standard"
        self.lbl_numOfRoom.text = "\(pricingModel.pricingArrayRooms.count)"
        var totalPax : Int = 0
        for roomModel in pricingModel.pricingArrayRooms
        {
            totalPax = totalPax + roomModel.childWithoutBedCount + roomModel.childWithBedCount + roomModel.adultCount
        }
        self.lbl_numOfTravel.text = "\(pricingModel.getAdultCount()) Adult, " +  "\(pricingModel.getchildCount())" + " Child, " +  "\(pricingModel.getInfantCount()) Infant"
        let numFormatter : NumberFormatter = NumberFormatter.init()
        // numFormatter.positiveFormat = "0"
        numFormatter.numberStyle = NumberFormatter.Style.decimal
        numFormatter.locale = Locale(identifier: "en_IN")
        self.lbl_discount.text = "₹ " + (numFormatter.string(from: NSNumber.init(value: Int(pricingModel.discountAdvancePrice) )) ?? "0")
        //            print(pricingModel.)
        var currencyRate = ""
        var optionalActHeight : CGFloat = 0.0
        //            if (!quotationView.expantPaymentView){
        if (optionalArray.count == 0){
            printLog(pricingModel.totalOptionalCost)
            printLog(pricingModel.totalTourCost)
            printLog(pricingModel.promocodeAmount)
            //                pricingModel.totalTourCost = "\((Int(pricingModel.totalTourCost) ?? 0) - (Int(pricingModel.totalOptionalCost) ))"
            //                pricingModel.totalOptionalCost = 0
        }
        self.view_optionalSet.isHidden = (pricingModel.totalOptionalCost == 0) ? true : false
        if  pricingModel.totalOptionalCost != 0
        {
            optionalActHeight = 45.0 + CGFloat(optionalArray.count*45)
            self.view_optionalActOne.isHidden = (optionalArray.count>0) ? false : true
            self.view_optionalActTwo.isHidden = (optionalArray.count>1) ? false : true
            self.view_optionalActThree.isHidden = (optionalArray.count>2) ? false : true
            for i in 0..<optionalArray.count{
                let optionalModel = optionalArray[i]
                let views = optionalViewArray[i]
                views.isHidden = false
                let leftLabel = optionalLeftLabelArray[i]
                let rightLabel = optionalRightLabelArray[i]
                let adultCount = (optionalModel.adultCount>0) ? "\(optionalModel.adultCount) Adult" : ""
                let childCount = (optionalModel.childCount>0) ? "\(optionalModel.childCount) Child" : ""
                let infantCount = (optionalModel.infantCount>0) ? "\(optionalModel.infantCount) Infant" : ""
                //                    leftLabel.text = "Optional Activity Name \(i+1)"+" ("+"\(adultCount)"+"\(childCount)"+"\(infantCount)"+")"
                //                    leftLabel.text = "\(optionalModel.eventName)"+" ("+"\(adultCount)"+"\(childCount)"+"\(infantCount)"+")"
                leftLabel.text = "\(optionalModel.eventName)"
                var prize = (optionalModel.adultCount * Int(optionalModel.adultPrice) ) + (optionalModel.childCount * Int(optionalModel.childPrice))
                prize = prize + (optionalModel.infantCount * Int(optionalModel.infantPrice))
                rightLabel.text = "\(optionalModel.currencyCode)"+" \(Int(prize))"
                currencyRate = "\(Float(optionalModel.currencyRate))"
            }
            
        }else{
            
        }
        self.cnst_OptionalSetHeight.constant = optionalActHeight
        self.lbl_activityOptionalCost.text =  "₹ " + (numFormatter.string(from: NSNumber.init(value:Int(pricingModel.totalOptionalCost))) ?? "0")
        
        let main_string = "₹ " + (numFormatter.string(from: NSNumber.init(value:Int(pricingModel.totalTourCost) ?? 0)) ?? "0") //db fr
        let string_to_color = (numFormatter.string(from: NSNumber.init(value:Int(pricingModel.totalTourCost) ?? 0)) ?? "0") //db fr
        let range = (main_string as NSString).range(of: string_to_color)
        let attributedString = NSMutableAttributedString(string:main_string)
        //        let colour = UIColor.init(red: 209/255, green: 0/255, blue: 12/255, alpha: 1)//Red
        let colour = UIColor.init(red: 0/255, green: 78/255, blue: 155/255, alpha: 1)//Blue
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: colour , range: range)  //red color
        if(pricingModel.applyPromocodeSuccess){
            printLog("PROMOCODE IS ALREADY APPLIED")
        }
        self.lbl_FinalCost.attributedText = attributedString
        
        let boldFontAttributes = [NSAttributedString.Key.foregroundColor: AppUtility.hexStringToUIColor(hex: "#0054A5"), NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 18)]//0037A3
        let normalFontAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14)]
        let partOne = NSMutableAttributedString(string: "₹ ", attributes: normalFontAttributes)
        let isAfterPromoApplyAmount = pricingModel.applyPromocodeSuccess ? (Int(pricingModel.totalTourCost) ?? 15000) - Int(pricingModel.promocodeAmount) : Int(pricingModel.totalTourCost) ?? 0
        let partTwo = NSMutableAttributedString(string: (numFormatter.string(from: NSNumber.init(value:isAfterPromoApplyAmount)) ?? "0"), attributes: boldFontAttributes)
        //            let partTwo = NSMutableAttributedString(string: (numFormatter.string(from: NSNumber.init(value:Int(pricingModel.totalTourCost) ?? 0)) ?? "0"), attributes: boldFontAttributes)
        let combinationTwo = NSMutableAttributedString()
        combinationTwo.append(partOne)
        combinationTwo.append(partTwo)
        self.lbl_TotalBigPrize.attributedText = combinationTwo
        
        
        var currencyText : String = "Total Tour Cost "
        var calculationText : String = ""
        /*
         for dict in pricingModel.currencyBreakup
         {
         if (dict["currencycode"] as? String ?? "").uppercased() == "INR"
         {
         calculationText = calculationText + "₹ " + (numFormatter.string(from: NSNumber.init(value:Int(dict["amount"] as? Double ?? 0.0))) ?? "0")
         }
         else
         {
         currencyText = currencyText + "\n" + (dict["currencycode"] as? String ?? "") + " calculated @ INR "
         currencyText = currencyText + (numFormatter.string(from: NSNumber.init(value:dict["currencyRate"] as? Double ?? 0.0)) ?? "0")
         //calculationText = calculationText + (dict["currencycode"] as? String ?? "") + " " + (numFormatter.string(from: NSNumber.init(value:Int(dict["amount"] as? Double ?? 0.0))) ?? "0")   + " + "
         calculationText = "\(calculationText)\(dict["currencycode"] as? String ?? "") \(numFormatter.string(from: NSNumber.init(value:Int(dict["amount"] as? Double ?? 0.0))) ?? "0") +"
         
         }
         }
         */
        for index in 0..<pricingModel.currencyBreakup.count {
            let dict = pricingModel.currencyBreakup[index]
            if (dict["currencycode"] as? String ?? "").uppercased() == "INR"
            {
                calculationText = calculationText + "₹ " + (numFormatter.string(from: NSNumber.init(value:Int(dict["amount"] as? Double ?? 0.0))) ?? "0")
            }
            else
            {
                let appendStr = (index == pricingModel.currencyBreakup.count-1) ? "" : "+"
                //                    currencyText = currencyText + "\n" + (dict["currencycode"] as? String ?? "") + " calculated @ INR "
                currencyText = currencyText + "\n" + (dict["currencycode"] as? String ?? "") + " @ INR "
                currencyText = currencyText + (numFormatter.string(from: NSNumber.init(value:dict["currencyRate"] as? Double ?? 0.0)) ?? "0")
                calculationText = "\(calculationText)\(dict["currencycode"] as? String ?? "") \(numFormatter.string(from: NSNumber.init(value:Int(dict["amount"] as? Double ?? 0.0))) ?? "0") \(appendStr)"
                
            }
        }
        self.lbl_TotalTourWithINRInfo.text = currencyText
        self.lbl_TotalTourWithINRCost.text = calculationText
        
        
        self.view_finalForAll.isHidden = !pricingModel.applyPromocodeSuccess
        self.view_Promocode.isHidden = !pricingModel.applyPromocodeSuccess
        //            self.view_Promocode.isHidden = true
        //            self.cnst_DiscountSetHeight.constant = (pricingModel.applyPromocodeSuccess) ? 230 : 130;
        self.cnst_DiscountSetHeight.constant = 130
        self.cnst_DiscountSetHeight.constant = self.cnst_DiscountSetHeight.constant+((pricingModel.applyPromocodeSuccess) ? 80 : 0);
        
        
        let discount_string = "(-) " + "₹ " + (numFormatter.string(from: NSNumber.init(value:Int(pricingModel.discountFullPrice) )) ?? "0")
        self.lbl_discount.text = discount_string
        let promo_string = "(-) " + "₹ " + (numFormatter.string(from: NSNumber.init(value:Int(pricingModel.promocodeAmount) )) ?? "0")
        self.lbl_promodiscount.text = promo_string
        self.lbl_optionalTotalTitleCost.text = "Optional Activity Cost In INR"
        //            self.lbl_optionalTotalTitleCost.text = (currencyRate == "0.0") ? "Optional Activity Cost In INR" : "Optional Activity Cost (CHF @ INR \(currencyRate))"
        
        self.lbl_gstCost.text = "(+) " + "₹ " + (numFormatter.string(from: NSNumber.init(value:Int(pricingModel.totalServicetax))) ?? "0")
        self.lbl_tcsPercent.text = "TCS @ " + (numFormatter.string(from: NSNumber.init(value:Int(pricingModel.tcsPercentage))) ?? "0") + "%"
        self.lbl_tcsCost.text = "(+) " + "₹ " + (numFormatter.string(from: NSNumber.init(value:Int(pricingModel.tcsAmount))) ?? "0")
        self.cnst_DiscountSetHeight.constant = self.cnst_DiscountSetHeight.constant+((pricingModel.tcsPercentage>0.0) ? 30 : 0);
        printLog("tcsPercentage \(Int(pricingModel.tcsPercentage)) tcsAmount \(Int(pricingModel.tcsAmount))")
        self.view_tcs.isHidden = !(Int(pricingModel.tcsAmount)>0);
        
//        self.lbl_tcsPercent.text = "TCS @ " + (numFormatter.string(from: NSNumber.init(value:Int(pricingModel.tcsPercentage))) ?? "0 %" + "%")
//        self.lbl_tcsCost.text = "(+) " + "₹ " + (numFormatter.string(from: NSNumber.init(value:Int(pricingModel.tcsAmount))) ?? "0")
    }
    
    func hideConatentView(flags:Bool){
        self.main_ContentView.isHidden = flags
    }
}
