//
//  DeluxPremiumTableViewCell.swift
//  holidays
//
//  Created by Kush_Team on 25/06/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//
//Upgrade to 4 Star Hotel with additional amount of // ₹ 3,000
//Get luxurious premium 5 star hotel with additional amount of // ₹ 5,000
import UIKit
protocol DeluxPremiumTableViewCellDelegate {
    func reloadTableInDeluxPremium()
    func selectDeluxPremium(withSubClassId:Int, flag:Bool)
}
class DeluxPremiumTableViewCell: UITableViewCell {

    var quotationView : QuotationView = QuotationView.init()
    var pricingModel : PricingModel =  PricingModel.init()
    var packageDetailModel:PackageDetailsModel = PackageDetailsModel.init()
    @IBOutlet weak var view_Delux: UIView!
    @IBOutlet weak var view_Premium: UIView!
    var delegates : DeluxPremiumTableViewCellDelegate? = nil
    @IBOutlet weak var lbl_fourStar: UILabel!
    @IBOutlet weak var lbl_fiveStar: UILabel!
    var btnTag : Int = 0
    @IBOutlet weak var btn_fourStar: UIButton!
    @IBOutlet weak var btn_fiveStar: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setDataAsDeluxAndPremium(){//0154A4 0054A5 0037A3
        btn_fourStar.addTarget(self, action:#selector(handleDeluxHotelStar), for: .touchUpInside)
        btn_fiveStar.addTarget(self, action:#selector(handlePremiumHotelStar), for: .touchUpInside)
        let boldFontAttributes = [NSAttributedString.Key.foregroundColor: AppUtility.hexStringToUIColor(hex: "#0037A3"), NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 20)]
        let normalFontAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14)]
//        let partOne = NSMutableAttributedString(string: "₹ ", attributes: normalFontAttributes)
//        let partTwo = NSMutableAttributedString(string: "3,000", attributes: boldFontAttributes)
//        let partThree = NSMutableAttributedString(string: "5,000", attributes: boldFontAttributes)
        let partOne = NSMutableAttributedString(string: "", attributes: normalFontAttributes)
        let partTwo = NSMutableAttributedString(string: "Deluxe", attributes: boldFontAttributes)
        let partThree = NSMutableAttributedString(string: "Premium", attributes: boldFontAttributes)
        let combinationOne = NSMutableAttributedString()
        combinationOne.append(partOne)
        combinationOne.append(partTwo)
        let combinationTwo = NSMutableAttributedString()
        combinationTwo.append(partOne)
        combinationTwo.append(partThree)
        lbl_fourStar.attributedText = combinationOne
        lbl_fiveStar.attributedText = combinationTwo
//        self.view_Delux.isHidden = !(packageDetailModel.startingPriceDelux > 0)
//        self.view_Premium.isHidden = !(packageDetailModel.startingPricePremium > 0)
        printLog(self.pricingModel.mainPackageClassId)
        self.view_Delux.isHidden = ((packageDetailModel.startingPriceDelux > 0) && (self.pricingModel.mainPackageClassId != 1)) ? false : true
        self.view_Premium.isHidden = ((packageDetailModel.startingPricePremium > 0) && (self.pricingModel.mainPackageClassId != 2)) ? false : true
        self.setInitialButtonImage()
    }
    @objc func handlePremiumHotelStar(sender: UIButton){
        printLog("Package Select \(sender.tag)")
        self.setPremiumTag(tagIDs: sender.tag)
        self.setDeluxTag(tagIDs: 1)
        self.selectStandardSubClass()
    }
    @objc func handleDeluxHotelStar(sender: UIButton){
        printLog("Package Select \(sender.tag)")
        self.setDeluxTag(tagIDs: sender.tag)
        self.setPremiumTag(tagIDs: 1)
        self.selectStandardSubClass()
    }
    func setInitialButtonImage(){
        btn_fourStar.tag = (self.pricingModel.packageClassId == 1) ? 1 : 0
        btn_fourStar.setImage(UIImage.init(named: (self.pricingModel.packageClassId == 1) ? "rightcheckBoxSelect" : "checkBoxNoSelect" ), for: .normal)
        btn_fiveStar.tag = (self.pricingModel.packageClassId == 2) ? 1 : 0
        btn_fiveStar.setImage(UIImage.init(named: (self.pricingModel.packageClassId == 2) ? "rightcheckBoxSelect" : "checkBoxNoSelect" ), for: .normal)
    }
    func setDeluxTag(tagIDs : Int){
        btn_fourStar.tag = (tagIDs == 0) ? 1 : 0
        btn_fourStar.setImage(UIImage.init(named: (btn_fourStar.tag == 1) ? "rightcheckBoxSelect" : "checkBoxNoSelect" ), for: .normal)
        if (btn_fourStar.tag==1){
            self.delegates?.selectDeluxPremium(withSubClassId: 1, flag: true)
        }
    }
    func setPremiumTag(tagIDs : Int){
        btn_fiveStar.tag = (tagIDs == 0) ? 1 : 0
        btn_fiveStar.setImage(UIImage.init(named: (btn_fiveStar.tag == 1) ? "rightcheckBoxSelect" : "checkBoxNoSelect" ), for: .normal)
        if (btn_fiveStar.tag==1){
            self.delegates?.selectDeluxPremium(withSubClassId: 2, flag: true)
        }
    }
    func selectStandardSubClass(){
        if (self.btn_fourStar.tag==0 && self.btn_fiveStar.tag==0){
//            self.delegates?.selectDeluxPremium(withSubClassId: 0)
            self.delegates?.selectDeluxPremium(withSubClassId: self.pricingModel.mainPackageClassId, flag: true)
        }
        
    }
//    func butonSelecet(tag:Int){
//        btn_fourStar.setImage(UIImage.init(named: (tag == 1) ? "rightcheckBoxSelect" : "checkBoxNoSelect" ), for: .normal)
//        btn_fiveStar.setImage(UIImage.init(named: (tag == 2) ? "rightcheckBoxSelect" : "checkBoxNoSelect" ), for: .normal)
//        self.packageDetailModel.arrayTourType
//        self.pricingModel.packageClassId = tag
//    }
    
    
    /*
    func selectDeluxPremium(withSubClassId:Int)
        {
            
            LoadingIndicatorView.show("Loading")
            self.pricingModel.packageClassId = withSubClassId
            let dataDict : Dictionary<String,Any> = AppUtility.getPricingRequestJson(packageDetailModel: self.packageDetailModel, pricingModel: self.pricingModel, optionalArray: nil)
            // "holidayRS/pricing"
            print("DataDict \(dataDict)")
            PreBookingCommunicationManager.getPrebookingData(requestType:"POST", queryParam: nil, pathParam:nil, jsonParam: dataDict, headers: nil, requestURL:kAstraUrlPricing , returnInCaseOffailure: false)
            {
                (status,response) in
                
                printLog("status is \(status)")
                printLog("response is \(String(describing: response))")
                if status
                {
                    
                    if let responseDict : Dictionary<String,Any> =  response as? Dictionary<String,Any>
                    {
                        if let msg : String = responseDict["message"] as? String
                        {
                            if msg != ""
                            {
                                AppUtility.displayAlert(title: "Alert", message: msg)
                            }
                            
                        }
                        else
                        {
                            printLog("responseDict is \(responseDict)")
                            AppUtility.fillPricingModelWithResponseDict(dict: responseDict, pricingModel: self.pricingModel, quotationView: self.quotationView)
                            if(responseDict["totalPrice"]as? Double ?? 0 != 0){
                                self.quotationView.selectedPaymentType = "FULL_PAYMENT"
                                self.quotationView.selectedPaymentAmount = responseDict["totalPrice"] as? Double ?? 0
                                if(self.pricingModel.applyPromocodeSuccess){
                                    printLog("PROMOCODE IS ALREADY APPLIED")
                                }
                                self.delegates?.reloadTableInDeluxPremium()
                            }else{
                                AppUtility.displayAlert(title: "Alert", message: "Oops! Due to limited availability, we are not able to process your request through mobile app. Please click on the below mentioned Call button and share your details 18002093344")
                            }
                            
                        }
                    }
                }
                else
                {
                    if let responseDict : Dictionary<String,Any> =  response as? Dictionary<String,Any>
                    {
                        if let msg : String = responseDict["message"] as? String
                        {
                            AppUtility.displayAlert(title: "Alert", message: msg)
                        }
                        else
                        {
                            AppUtility.displayAlert(title: "Alert", message: "Some error has occurred")
                        }
                    }
                    else
                    {
                        AppUtility.displayAlert(title: "Alert", message: "Some error has occurred")
                    }
                }
            }
        }
    */
}
