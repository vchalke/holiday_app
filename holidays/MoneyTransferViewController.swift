//
//  MoneyTransferViewController.swift
//  holidays
//
//  Created by Parshwanath on 09/12/17.
//  Copyright © 2017 Mobicule. All rights reserved.
//

import UIKit

class MoneyTransferViewController: ForexBaseViewController,UITextFieldDelegate,UIWebViewDelegate
{
    //MARK: - Outlets
    @IBOutlet var moneyTransferView: UIView!
    @IBOutlet weak var moneyTransferWebView: UIWebView!
    var previousController : String = ""
     var moduleName : String = ""
    var urlString:String = ""
    
    //MARK: - Controller Methods
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        Bundle.main.loadNibNamed(MONEY_TRANSFER_VC, owner: self, options: nil)
        super.addViewInBaseView(childView: self.moneyTransferView)
        super.setUpHeaderLabel(labelHeaderNameText: moduleName)
        
        if AppUtility.checkNetConnection()
        {
             LoadingIndicatorView.show()
            let url = URL(string: urlString)
            self.moneyTransferWebView.loadRequest(URLRequest(url: url!))
        }
    }
    
    override func viewWillLayoutSubviews()
    {
        super.setSubViewFrame(childView: self.moneyTransferView)
    }
    
    override func backButtonClicked(buttonView:UIButton)
    {
        if self.previousController == "SlideMenuViewController"
        {
            SlideNavigationController.sharedInstance().popToRootViewController(animated: true)
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error)
    {
        LoadingIndicatorView.hide()
        
    }
    
   
    
    func webViewDidFinishLoad(_ webView: UIWebView)
    {
        LoadingIndicatorView.hide()
    }
}
