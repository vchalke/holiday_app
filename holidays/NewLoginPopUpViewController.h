//
//  NewLoginPopUpViewController.h
//  holidays
//
//  Created by Parshwanath on 24/09/18.
//  Copyright © 2018 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RegisterUserViewController.h"
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <GoogleSignIn/GoogleSignIn.h>
#import "SignInViewController.h"
#import "LoadingView.h"


@protocol DismissNewLoginPopUp<NSObject>
@optional
- (void)dismissLoginPopUp;


@end

@class GIDSignInButton;

@interface NewLoginPopUpViewController : UIViewController<registerUserDelegate,GoogleSignInViewDelegate,GIDSignInDelegate,GIDSignInUIDelegate>
{
    
        LoadingView *activityIndicator;
}
@property (weak, nonatomic) IBOutlet UIStackView *mainStackView;

@property (weak, nonatomic) IBOutlet UITextField *emailAddressTxt;
@property (weak, nonatomic) IBOutlet UIView *emailAddressView;

@property (weak, nonatomic) IBOutlet UIView *passwordMainView;
@property (weak, nonatomic) IBOutlet UIView *passwordView;
@property (weak, nonatomic) IBOutlet UITextField *passwordTxt;

@property (weak, nonatomic) IBOutlet UIStackView *loginWithView;
@property (weak, nonatomic) IBOutlet UIView *otpMainView;

@property (weak, nonatomic) IBOutlet UITextField *otpTxt;
@property (weak, nonatomic) IBOutlet UIView *otpView;

@property (weak, nonatomic) IBOutlet UIButton *passwordBtn;
@property (weak, nonatomic) IBOutlet UIButton *otpBtn;

@property (weak, nonatomic) IBOutlet UIButton *register_Login_Toggel_Btn;
@property (weak, nonatomic) IBOutlet UIButton *login_register_Btn;
@property (weak, nonatomic) IBOutlet UIView *orView;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *loginStackViewHeight;
@property (weak, nonatomic) IBOutlet UIButton *guestUserBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *loginStackViewConstantHeight;
@property (weak, nonatomic) IBOutlet UIButton *resendOTPBtn;

@property(strong) NSString * userEmailID ;

@property (strong,nonatomic) id <DismissNewLoginPopUp> delegate;

@property(strong) NSString * currentSelectedeScreen;
@property (weak, nonatomic) IBOutlet UIImageView *otpImageView;


@end





