//
//  PackageListCell.m
//  holidays
//
//  Created by Pushpendra Singh on 13/08/15.
//  Copyright (c) 2015 Mobicule. All rights reserved.
//

#import "PackageListCell.h"
#import "Holiday.h"

@implementation PackageListCell{
    NSMutableArray *arrayForSiteDetails;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    
}

-(void)getSiteArrayDetails:(NSArray *)arrayForSite{
    arrayForSiteDetails=[arrayForSite mutableCopy];
}
-(void)actionForRightbuttonClicked:(UIButton *)index{
     NSInteger rowClick=index.tag;
    Holiday *objForHoliday=[arrayForSiteDetails objectAtIndex:rowClick];
    NSArray *tempSiteArray=objForHoliday.timeLineList;
    if (  _scrollSite.contentOffset.x < (100 * [tempSiteArray count]-1) -_scrollSite.frame.size.width ) {
        
        [UIView animateWithDuration:0.8 animations:^{
            _scrollSite.contentOffset = CGPointMake(_scrollSite.contentOffset.x+50, 0);
        }];
        
    }
}
-(void)actionForLeftbuttonClicked:(UIButton *)index{
    NSInteger rowClick=index.tag;
    Holiday *objForHoliday=[arrayForSiteDetails objectAtIndex:rowClick];
    NSArray *tempSiteArray=objForHoliday.timeLineList;
    
        if (  _scrollSite.contentOffset.x >(40 * [tempSiteArray count]-1) -_scrollSite.frame.size.width ) {
            
            [UIView animateWithDuration:0.8 animations:^{
                _scrollSite.contentOffset = CGPointMake(_scrollSite.contentOffset.x-50, 0);
            }];
            
        
        
    }
    
    

    
}

@end
