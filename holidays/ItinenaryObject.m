//
//  ItinenaryObject.m
//  holidays
//
//  Created by Kush_Tech on 04/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "ItinenaryObject.h"

@implementation ItinenaryObject
-(instancetype)initWithItinenaryDict:(NSDictionary *)dictionary{
    if ([super init])
    {
        
        self.altTag = [dictionary valueForKey:@"altTag"];
        self.highlights = [dictionary valueForKey:@"highlights"];
        self.holidayItineraryId = [[dictionary valueForKey:@"holidayItineraryId"] integerValue];
        self.image = [dictionary valueForKey:@"image"];
//        self.isActive = [dictionary valueForKey:@"isActive"];
        NSString *boolString = [dictionary valueForKey:@"isActive"];
        self.isActive = [[boolString uppercaseString] isEqualToString:@"Y"] ? YES : NO;
        self.itineraryDay = [[dictionary valueForKey:@"itineraryDay"] integerValue];
        self.itineraryDescription = [dictionary valueForKey:@"itineraryDescription"];
        self.mealDescription = [dictionary valueForKey:@"mealDescription"];
        self.mealDescription = [dictionary valueForKey:@"mealDescription"];
        
        
    }
    return self;
    
}
@end
