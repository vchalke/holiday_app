//
//  LandingScreenNewVC.h
//  holidays
//
//  Created by Ios_Team on 29/10/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BottomStacksView.h"
#import "customView.h"
#import "NewMasterVC.h"
NS_ASSUME_NONNULL_BEGIN

@interface LandingScreenNewVC : NewMasterVC
@property (weak, nonatomic) IBOutlet customView *topViews;
@property (weak, nonatomic) IBOutlet BottomStacksView *bottomViews;
@property (weak, nonatomic) IBOutlet UITableView *tableViews;
-(void)jumpToControllerfromViewNum:(NSInteger)firstVCNum toViewNum:(NSInteger)secVCNum;

@end

NS_ASSUME_NONNULL_END
