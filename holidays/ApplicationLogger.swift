//
//  ApplicationLogger.swift
//  sotc-consumer-application
//
//  Created by Saurav Kumar on 15/11/17.
//  Copyright © 2017 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit

struct ApplicationLoggerConstant {
    
    static let isLogsEnabled: Bool = true
}

    
   public func printLog(_ items: Any...)  {
    
    if ApplicationLoggerConstant.isLogsEnabled {
        
        for item in items {
            
             print(item)
        }
        
       
        
    }
   
  }


