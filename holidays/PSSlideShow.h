//
//  PSSlideShow.h
//  holidays
//
//  Created by Pushpendra Singh on 07/08/15.
//  Copyright (c) 2015 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, PSSlideShowTransitionType) {
    PSSlideShowTransitionFade,
    PSSlideShowTransitionSlide
};

typedef NS_ENUM(NSInteger, PSSlideShowGestureType) {
    PSSlideShowGestureTap,
    PSSlideShowGestureSwipe,
    PSSlideShowGestureAll
};

typedef NS_ENUM(NSUInteger, PSSlideShowPosition) {
    PSSlideShowPositionTop,
    PSSlideShowPositionBottom
};

typedef NS_ENUM(NSUInteger, PSSlideShowState) {
    PSSlideShowStateStopped,
    PSSlideShowStateStarted
};

@class PSSlideShow;
@protocol PSSlideShowDelegate <NSObject>
@optional
- (void) PSSlideShowDidShowNext:(PSSlideShow *) slideShow;
- (void) PSSlideShowDidShowPrevious:(PSSlideShow *) slideShow;
- (void) PSSlideShowWillShowNext:(PSSlideShow *) slideShow;
- (void) PSSlideShowWillShowPrevious:(PSSlideShow *) slideShow;
@end

@protocol PSSlideShowDataSource <NSObject>
- (UIImage *)slideShow:(PSSlideShow *)slideShow imageForPosition:(PSSlideShowPosition)position;
@end

@interface PSSlideShow : UIView

@property (nonatomic, unsafe_unretained) IBOutlet id <PSSlideShowDelegate> delegate;
@property (nonatomic, unsafe_unretained) id<PSSlideShowDataSource> dataSource;

@property  float delay;
@property  float transitionDuration;
@property  (readonly, nonatomic) NSUInteger currentIndex;
@property  (atomic) PSSlideShowTransitionType transitionType;
@property  (atomic) UIViewContentMode imagesContentMode;
@property  (strong,nonatomic) NSMutableArray * images;
@property  (readonly, nonatomic) PSSlideShowState state;

- (void) addImagesFromResources:(NSArray *) names;
- (void) emptyAndAddImagesFromResources:(NSArray *)names;
- (void) emptyAndAddImages:(NSArray *)images;
- (void) setImagesDataSource:(NSMutableArray *)array;
- (void) addGesture:(PSSlideShowGestureType)gestureType;
- (void) removeGestures;
- (void) addImage:(UIImage *) image;

- (void) start;
- (void) stop;
- (void) previous;
- (void) next;

@end
