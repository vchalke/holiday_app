//
//  MobileNumberPopUpViewController.m
//  holidays
//
//  Created by ketan on 07/01/16.
//  Copyright © 2016 Mobicule. All rights reserved.
//

#import "MobileNumberPopUpViewController.h"
#import "BookNowViewController.h"
#import "UIViewController+MJPopupViewController.h"
#import "LoadingView.h"
#import <NetCorePush/NetCorePush.h>
//#import <NetCorePush/NetCoreSharedManager.h>

@interface MobileNumberPopUpViewController ()
{
    LoadingView *activityLoadingView;
}

@end

@implementation MobileNumberPopUpViewController
@synthesize packageDetail;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onGoButtonClicked:(id)sender {

    if ([self validatePhoneNumber])
    {
      CoreUtility *coreobj=[CoreUtility sharedclassname];
       NSString *phonNo=self.txtViewMobileNumber.text;
        
        [coreobj.strPhoneNumber addObject:phonNo];
        
       // NSString * numberString = [self.txtViewMobileNumber.text stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
        
        if (nil != self.txtViewMobileNumber.text && ![self.txtViewMobileNumber.text isEqual:[NSNull new]] && ![self.txtViewMobileNumber.text isEqualToString:@""])
        {
            [[NetCoreSharedManager sharedInstance] setUpIdentity:phonNo];
            
            [[NSUserDefaults standardUserDefaults] setValue:self.txtViewMobileNumber.text forKey:@"Holidays_TextField_MobicleNumber"];
            
            [[NSUserDefaults standardUserDefaults] synchronize];
            
        }

        
        if (self.delegate && [self.delegate respondsToSelector:@selector(goButtonClickedWithMobileNumber:)])
        {
            
            
      activityLoadingView = [LoadingView loadingViewInView:self.view.superview.superview
                                                                  withString:@""
                                                           andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
///https://thomascookindia.custhelp.com/cgi-bin/thomascookindia.cfg/php/custom/mobile_app_lead_holiday_ios_app_leads.php?email=narendra_k2@yahoo.com&mobile=9811976670&request_type=Domestic&sub_product_type=GIT&package_id=PKG130755
            
            NSString  *urlString  = KUrlHolidayCreateLead;
            //@"https://thomascookindia.custhelp.com/cgi-bin/thomascookindia.cfg/php/custom/mobile_app_lead_holiday_ios_app_leads.php";
            
            
            NSString *packageSubType = self.packageDetail.strPackageSubType;
            
            if ([[self.packageDetail.strPackageSubType lowercaseString] isEqualToString:@"fit fixed"])
            {
                packageSubType = @"fit";
            }
            
            
            NSString *queryParam = [NSString stringWithFormat:@"?mobile=%@&request_type=%@&sub_product_type=%@&package_id=%@",self.txtViewMobileNumber.text,self.packageDetail.strPackageType,packageSubType,self.packageDetail.strPackageId];
            
            
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
            
            NSString *urlStringTotal = [NSString stringWithFormat:@"%@%@",urlString,queryParam];
            
            NSString * encodedString = [urlStringTotal stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];

            
            NSURL *url = [NSURL URLWithString:encodedString];
            
            [request setURL:url];
            [request setHTTPMethod:@"GET"];
            
            NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
            [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
              {
                  dispatch_async(dispatch_get_main_queue(), ^{
                      
                      [activityLoadingView removeFromSuperview];
                      
                      NSString *requestReply = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
                      
                      NSLog(@"Response :- %@",requestReply);
                      
                    coreobj.leadSubmitted = YES;
                    [self.delegate goButtonClickedWithMobileNumber:self.txtViewMobileNumber.text];
                                        });
                  
              }] resume];
        }
    }
    else
    {
        UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Alert"
                                                                           message:@"Enter Valid Mobile Number"
                                                                    preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
            [alertView dismissViewControllerAnimated:YES completion:nil];
            
        }];
        [alertView addAction:okAction];
        
        [self presentViewController:alertView animated:YES completion:nil];

        
    }
    
}

-(BOOL)validatePhoneNumber
{
    NSString *string = self.txtViewMobileNumber.text;
    NSString *expression = @"^([+][9][1]|[9][1]|[0]){0,1}([7-9]{1})([0-9]{9})$";
    NSError *error = NULL;
    
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
    
    NSTextCheckingResult *match = [regex firstMatchInString:string options:0 range:NSMakeRange(0, [string length])];
    
    if (match){
        NSLog(@"yes");
        return YES;
    }else{
        NSLog(@"no");
        return NO;
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    NSString *currentString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    int length = (int)[currentString length];
    
    if (textField == self.txtViewMobileNumber)
    {
        if ([string rangeOfCharacterFromSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]].location != NSNotFound)
        {
  
            UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Alert"
                                                                    message:@"This field accepts only numeric values"
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                
                [alertView dismissViewControllerAnimated:YES completion:nil];
                
            }];
            [alertView addAction:okAction];

            return FALSE;
        }
        
        if (length >10 )
        {
            return !([currentString length] > 10);
            return FALSE;
        }
        
    }
  
    
    return TRUE;

}
@end
