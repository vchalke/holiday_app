//
//  PassPortInfoTableViewCell.swift
//  holidays
//
//  Created by Kush_Team on 09/07/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

import UIKit
protocol PassPortInfoTableViewCellDelegate {
    func showImageWithString(string:String)
}
class PassPortInfoTableViewCell: UITableViewCell {

    @IBOutlet weak var txt_NameOnPass: UITextField!
    @IBOutlet weak var txt_passportNum: UITextField!
    @IBOutlet weak var txt_expDate: UITextField!
    @IBOutlet weak var txt_dateOfIssue: UITextField!
    @IBOutlet weak var txt_CountryOfIssue: UITextField!
    @IBOutlet weak var btn_passportOne: UIButton!
    @IBOutlet weak var btn_passportTwo: UIButton!
    var getCustInfoModel : CustomerProfileModel = CustomerProfileModel()
    var delegates : PassPortInfoTableViewCellDelegate? = nil
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setProfileModelsDetailsInPassports(profileModel:CustomerProfileModel){
        getCustInfoModel = profileModel
        let dict = profileModel.passportInfo
//        let title = dict["title"] as? String
//        let fName = dict["firstName"] as? String
//        let mName = dict["middleName"] as? String
//        let lName = dict["lastName"] as? String
        self.btn_passportOne.tag = 0
        self.btn_passportTwo.tag = 1
//        self.btn_passportOne.addTarget(self, action: #selector(self.passportClick(_:)), for: .touchUpInside)
//        self.btn_passportTwo.addTarget(self, action: #selector(self.passportClick(_:)), for: .touchUpInside)
        if let title = dict["title"] as? String, let fName = dict["firstName"] as? String, let lName = dict["lastName"] as? String{
            self.txt_NameOnPass.text = "\(title). \(fName) \(lName)"
        }
        self.txt_passportNum.text = dict["passportNo"] as? String
        self.txt_expDate.text = dict["expiryDate"] as? String
        self.txt_dateOfIssue.text = ""
        self.txt_CountryOfIssue.text = dict["countryOfIssue"] as? String
        
    }
//    @objc func passportClick(_ sender:UIButton)
//    {
//        self.delegates?.showImageWithString(string: getCustInfoModel.profilePicUrl)
//    }
}
