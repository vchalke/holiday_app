//
//  NotificationTableViewCell.swift
//  sotc-consumer-application
//
//  Created by Payal on 26/07/17.
//  Copyright © 2017 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell
{

    @IBOutlet var uiImageView: UIImageView!
 
    @IBOutlet var bfNumberLabel: UILabel!
    @IBOutlet var uiNotificationTitleLabel: UILabel!
    
    @IBOutlet var uiNotificationDescLabel: UILabel!
    
    @IBOutlet var uiNotificationDateLabel: UILabel!
    
    @IBOutlet var uibaseView: UIView!
    
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
        
//        uibaseView.layer.borderColor = UIColor(red: 200, green: 200, blue: 200, alpha: 1) .cgColor
        
        uibaseView.layer.borderColor = UIColor.black.cgColor
        
        uibaseView.layer.borderColor = UIColor(red:200/255.0, green:200/255.0, blue:200/255.0, alpha: 1.0) .cgColor
        uibaseView.layer.borderWidth = 0.5
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
