//
//  TourMoreViewController.swift
//  sotc-consumer-application
//
//  Created by Saurav Kumar on 27/07/17.
//  Copyright © 2017 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit


class TourMoreViewController: UIViewController,UIGestureRecognizerDelegate,UIDocumentInteractionControllerDelegate {
    
    
    var presentingView:UIViewController? = nil
    
    @IBOutlet var optionalView: UIView!
    @IBOutlet var deviationView: UIView!
    @IBOutlet var thingsToCarryView: UIView!
    @IBOutlet var helpView: UIView!
    @IBOutlet var holidayKitView: UIView!
    @IBOutlet var ShareExperience: UIView!
    @IBOutlet var FinalTourDetailView: UIView!
    
    
    var tourDetail:Tour?
     var tourStatus:TourStatus?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addUITapGestureRecognizer()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        
        self.view.alpha = 0.0
        self.popoverPresentationController?.containerView?.alpha = 0.0
        
        UIView.animate(withDuration: 0.25, delay: 0.0, options: UIView.AnimationOptions.curveEaseInOut
            , animations: {
                
                self.view.alpha = 1.0
                self.popoverPresentationController?.containerView?.alpha = 1.0
                
        }, completion: nil)
        
        
    }
    
    
    func addUITapGestureRecognizer()  {
        
        let optionalTap = UITapGestureRecognizer(target: self, action: #selector(TourMoreViewController.onOptionalViewClick))
        optionalTap.delegate = self
        optionalView.addGestureRecognizer(optionalTap)
        
        let deviationTap = UITapGestureRecognizer(target: self, action: #selector(TourMoreViewController.onDeviationViewClick))
        deviationTap.delegate = self
        deviationView.addGestureRecognizer(deviationTap)
        
        
        let thingsToCarryTap = UITapGestureRecognizer(target: self, action: #selector(TourMoreViewController.onThingsToCarryViewClick))
        thingsToCarryTap.delegate = self
        thingsToCarryView.addGestureRecognizer(thingsToCarryTap)
        
//        let roomTap = UITapGestureRecognizer(target: self, action: #selector(TourMoreViewController.onRoomsViewClick))
//        roomTap.delegate = self
//        roomsView.addGestureRecognizer(roomTap)
        
        
        let helpTap = UITapGestureRecognizer(target: self, action: #selector(TourMoreViewController.onHelpViewClick))
        helpTap.delegate = self
        helpView.addGestureRecognizer(helpTap)
        
     /*   let holidayKitTap = UITapGestureRecognizer(target: self, action: #selector(TourMoreViewController.onHolidayKitViewClick))
        holidayKitTap.delegate = self
        holidayKitView.addGestureRecognizer(holidayKitTap)*/
        
        let shareExpTap = UITapGestureRecognizer(target: self, action: #selector(TourMoreViewController.onShareExperienceClick))
        shareExpTap.delegate = self
        ShareExperience.addGestureRecognizer(shareExpTap)
        
        let finalTourDeailTap = UITapGestureRecognizer(target: self, action: #selector(TourMoreViewController.onFinalTourDetailViewClick))
        finalTourDeailTap.delegate = self
        FinalTourDetailView.addGestureRecognizer(finalTourDeailTap)
        
        
    }
    
    
    @objc func onOptionalViewClick() {
        
        self.dismiss(animated: true) {
            
            self.removeOverLay()
            
            let validationMessage:String = self.isApplicableToBookPayNow()
            
            
            
            if self.tourStatus == TourStatus.Past
            {
                self.displayAlert(message: "You are not allowed to book optional for past date")
                
            }else if self.tourStatus == TourStatus.Cancelled
            {
                self.displayAlert(message: "You are not allowed to book optional for cancelled booking")
               
            }else if validationMessage != "" {
                
                self.displayAlert(message: validationMessage)
            }
            else
            {
                let viewController:OptionalPackageViewController = OptionalPackageViewController(nibName: "OptionalPackageViewController", bundle: nil)
                viewController.tourDetails = self.tourDetail
            
                self.presentingView?.navigationController?.pushViewController(viewController, animated: true)
            }
            
        }
    }
    
    @objc func onDeviationViewClick() {
        
        self.dismiss(animated: true) {
            
            self.removeOverLay()
            
            if let passengerArray:Array<Passenger> = self.tourDetail?.passengerRelation?.allObjects as? Array<Passenger>
            {
                if passengerArray.count > 0
                {
                    let viewController:DeviationPassengerListViewController = DeviationPassengerListViewController(nibName: "DeviationPassengerListViewController", bundle: nil)
                    
                    viewController.tourDetails = self.tourDetail
                    viewController.tourStatus = self.tourStatus
                    
                    self.presentingView?.navigationController?.pushViewController(viewController, animated: true)
                }else
                {
                     self.displayAlert(message: AlertMessage.NO_DATA)
                }
            }else
            {
                self.displayAlert(message: AlertMessage.NO_DATA)
            }
        }
    }
    
    
    @objc func onThingsToCarryViewClick() {
        
        self.dismiss(animated: true) {
            
            self.removeOverLay()
           /* let viewController:ThingsToCarryViewController = ThingsToCarryViewController(nibName: "ThingsToCarryViewController", bundle: nil)
            
            viewController.tourDetail = self.tourDetail
        
            
            self.presentingView?.navigationController?.pushViewController(viewController, animated: true)*/
            
            self.getHandyTipsDetails()
        }
        
    }
    func onRoomsViewClick() {
        self.dismiss(animated: true) {
            
            self.removeOverLay()
            
            let viewController:RoomListViewController = RoomListViewController(nibName: "RoomListViewController", bundle: nil)
            
            if let passengerArray:Array<Passenger> = self.tourDetail?.passengerRelation?.allObjects as? Array<Passenger>
            {
                if passengerArray.count > 0
                {
             
                    viewController.passengerDetailsArray = passengerArray
                    self.presentingView?.navigationController?.pushViewController(viewController, animated: true)
                    
                }else{
                    self.displayAlert(message: "no room details found")
                }
            }else{
                self.displayAlert(message: AlertMessage.NO_DATA)
            }
        }
        
    }
    @objc func onHelpViewClick() {
        
        self.dismiss(animated: true) {
            self.removeOverLay()
            self.presentingView?.navigationController?.pushViewController(HelpViewController(nibName: "HelpViewController", bundle: nil), animated: true)

           //self.navigationController?.pushViewController(HelpViewController(nibName: "HelpViewController", bundle: nil), animated: true)
        }
        
    }

    //travel gear
    func onHolidayKitViewClick() {
        
        self.dismiss(animated: true) {
            self.removeOverLay()
            
           /* let viewController:HolidayKitViewController = HolidayKitViewController(nibName: "HolidayKitViewController", bundle: nil)
            
            viewController.tourDetail = self.tourDetail
            
            self.presentingView?.navigationController?.pushViewController(viewController, animated: true)*/
            
            self.getHolidayKitDetails()
        }
        
    }
    
    func oldFeedBackRequest()
    {
        let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        
        let mobileNumber:String = UserDefaults.standard.object(forKey: UserDefauldConstant.userMobileNumber) as! String
        
        let dataDetails:Dictionary<String,Any> = ["mobileNumber":mobileNumber,
                                                  "notificationType":"feedback",
                                                  "bfn":tourDetail?.bfNumber ?? ""]
        
        let data:Array<Dictionary<String,Any>> = [dataDetails]
        
        
        
        let  queryParameterMap:Dictionary<String,Any> = [:]
        
        let userDetails:Dictionary<String,Any> = [APIConstants.CheckVersionKey:"false",
                                                  APIConstants.Client:JsonConstant.IOS_CLIENT,
                                                  APIConstants.AppVersion:version!,
                                                  APIConstants.EntityId:"1"]
        
        let requestDict:Dictionary<String,Any> = [ APIConstants.EntityType:"transaction",
                                                   APIConstants.EntityAction:"get",
                                                   APIConstants.Entity:"fetchnotification",APIConstants.User:userDetails,"data":data,"queryParameterMap":queryParameterMap]
        
        
        LoadingIndicatorView.show("Loading")
        
        
        
        
        UserBookingController.sendFeedbackRequestRequest(requestData:requestDict, completion: { (status, message) in
            
            LoadingIndicatorView.hide()
            
            if status
            {
                
                self.dismiss(animated: true) {
                    
                    self.removeOverLay()
                    
                    
                    
                    
                    let viewController:FeedbackWebViewController = FeedbackWebViewController(nibName: "FeedbackWebViewController", bundle: nil)
                    
                    viewController.redirectionURL = message
                     viewController.moduleTitle = Title.FEEDBACK
                    
                    self.presentingView?.navigationController?.pushViewController(viewController, animated: true)
                    
                }
                
            }
            else{
                
                let alert = UIAlertController(title: "Alert", message: message, preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: AlertMessage.TITLE_OK, style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            
            
        })
        
        
    }
    
    
    func sendTC_CSSFeedBackRequest()
    {
        let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        
        
        let surveyName = ""
        let oppId = tourDetail?.opportunityId ?? ""
        let tourCode = tourDetail?.tourCode ?? ""
        let bfn:String = tourDetail?.bfNumber ?? ""
    
    
        let  departureDate = AppUtility.convertDateToString(currentDateFormat: "yyyy-MM-dd", date:tourDetail?.departureDate as Date? ?? Date(), requiredDateFormat: "dd-MM-yyyy")
        
        let  arrivalDate = AppUtility.convertDateToString(currentDateFormat: "yyyy-MM-dd", date:tourDetail?.arrivalDate as Date? ?? Date(), requiredDateFormat: "dd-MM-yyyy")
        
        let dataDetails:Dictionary<String,Any> = [surveyName:"","oppId":oppId,
                                                  "tourCode":tourCode,
                                                  "departureDate":departureDate,
                                                  "arrivalDate":arrivalDate,
                                                  "bfn":bfn]
        
        let data:Array<Dictionary<String,Any>> = [dataDetails]
        
        
        
        let  queryParameterMap:Dictionary<String,Any> = [:]
        
        let userDetails:Dictionary<String,Any> = [APIConstants.CheckVersionKey:"false",
                                                  APIConstants.Client:JsonConstant.IOS_CLIENT,
                                                  APIConstants.AppVersion:version!,
                                                  APIConstants.EntityId:"1"]
        
        let requestDict:Dictionary<String,Any> = [ APIConstants.EntityType:"transaction",
                                                   APIConstants.EntityAction:"get",
                                                   APIConstants.Entity:"feedbackUrl","identifier":"",APIConstants.User:userDetails,"data":data,"queryParameterMap":queryParameterMap]
        
        
        LoadingIndicatorView.show("Loading")
        
        
        UserBookingController.sendFeedbackRequestRequest(requestData:requestDict, completion: { (status, message) in
            
            LoadingIndicatorView.hide()
            
            if status
            {
                
                self.dismiss(animated: true) {
                    
                    self.removeOverLay()
                    
                    
                    let viewController:FeedbackWebViewController = FeedbackWebViewController(nibName: "FeedbackWebViewController", bundle: nil)
                    
                    viewController.redirectionURL = message
                    
                    viewController.moduleTitle = Title.FEEDBACK
                    
                    self.presentingView?.navigationController?.pushViewController(viewController, animated: true)
                    
                }
                
            }
            else{
                
                let alert = UIAlertController(title: "Alert", message: message, preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: AlertMessage.TITLE_OK, style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            
            
        })
        
        
    }
    
    @objc func onShareExperienceClick() {
        
       
        //self.oldFeedBackRequest()
        
    self.sendTC_CSSFeedBackRequest()
        
        
    }
    @objc func onFinalTourDetailViewClick() {
        
        self.dismiss(animated: true) {
            
            self.removeOverLay()
            
            if UserBookingController.isPaymentDonePaymentFlag(by: self.tourDetail!)
            {
                
                if let tourPassengerDoucumets = self.tourDetail?.passengerDocumentRelation?.allObjects as?  Array<TourPassengerDouments>
                    
                {
                    
                   let filteredDocumnetsArray = tourPassengerDoucumets.filter() { ($0.documentType?.uppercased() ?? "").contains("VOUCHER")}
                    
                    
                    if filteredDocumnetsArray.count > 0
                    {
                        if filteredDocumnetsArray.count > 1
                        {
                            let viewController:TicketsViewController = TicketsViewController(nibName: "TicketsViewController", bundle: nil)
                            
                            viewController.passengerTicketDoucumets = filteredDocumnetsArray
                            
                            viewController.moduleName = Title.FINAL_TOUR_DETAILS
                            
                            self.presentingView?.navigationController?.pushViewController(viewController, animated: true)
                            
                        }else
                        {
                            if let singlePassengerDoc:TourPassengerDouments = filteredDocumnetsArray.first
                            {
                                
                                var fileName:String = "Ticket \(0).pdf"
                                
                                let passengerNumber:String = singlePassengerDoc.passengerNumber ?? ""
                                
                                let ticketNumber:String = "Ticket \(0)"
                                
                                let downloadUrl:String = singlePassengerDoc.docummentURL ?? ""
                                
                                if downloadUrl != nil && !downloadUrl.isEmpty && downloadUrl != ""
                                {
                                    fileName = TourDocumentController.getDecodedFileName(url:downloadUrl)
                                }
                                
                                let saveFileUrl:String =  passengerNumber + "/" + ticketNumber + "/" + fileName
                                
                                let documnetUrl:String = TourDocumentController.getDocumentFilePath(url: saveFileUrl, bfnNumber: singlePassengerDoc.bfNumber!, moduleFolder: Title.TICKETS)
                                
                                self.downloadDocument(filePath: documnetUrl, url: downloadUrl,moduleName:Title.FINAL_TOUR_DETAILS )
                                
                            }else
                            {
                                AppUtility.displayAlert(message: AlertMessage.NO_DOCUMENT)
                            }
                            
                        }
                        
                    }else
                    {
                       AppUtility.displayAlert(message: AlertMessage.NO_DOCUMENT)
                    }
                    
                    
                }else
                    
                {
                    
                    AppUtility.displayAlert(message: AlertMessage.NO_DOCUMENT)
                    
                }
                
                
              /*  let viewController:FinalTourDetailsViewController = FinalTourDetailsViewController(nibName: "FinalTourDetailsViewController", bundle: nil)
                
                
                viewController.tourDetail = self.tourDetail
                
                
                self.presentingView?.navigationController?.pushViewController(viewController, animated: true)*/
                
                /* if let passengerDocumnets:Array<TourPassengerDouments> =  self.tourDetail?.passengerDocumentRelation?.allObjects as? Array<TourPassengerDouments>
                 {
                 viewController.passengerDocumnets = passengerDocumnets
                 
                 self.presentingView?.navigationController?.pushViewController(viewController, animated: true)
                 
                 }*/
                
                
                
                
            }else
            {
                self.displayAlert(message: AlertMessage.FinalTourDetails.COMPLETE_PAYMENT)
            }
            
            
            
        }
    }
    
    func removeOverLay() {
        
        
        guard let overlay = (self.presentingView as! TourDetailViewController).overlay else {
            return
        }
        DispatchQueue.main.async() {
            UIView.animate(withDuration: 0.2, animations: {
                overlay.alpha = 0.0
            }, completion: { _ in
                overlay.removeFromSuperview()
            })
        }
        
    }
    
    func displayAlert(message:String)  {
        
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: AlertMessage.TITLE_OK, style: UIAlertAction.Style.default, handler: nil))
        self.presentingView?.present(alert, animated: true, completion: nil)
        
    }
    
    
    //MARK: - HANDY_TIPS
    
    func getHandyTipsDetails()
    {
    
        var url = ""
        
        if let regionCode = tourDetail?.region
        {
            
            let countryCode = regionCode.uppercased()
            
            if countryCode != ""
            {
                switch countryCode {
                    
                case Constant.USA_CONTRY_CODE_HSDEMO,Constant.USA_CONTRY_CODE_SOUTHAMERICA,Constant.USA_CONTRY_CODE_USA,Constant.USA_CONTRY_CODE_USAFIT,Constant.USA_CONTRY_CODE_USAHS,Constant.USA_CONTRY_CODE_USAHSA:
                    
                    url = DocumentsURLConstant.Handy_TIPS_USA
                    break
                    
                case Constant.EXO_CONTRY_CODE_EXO:
                    
                    url = DocumentsURLConstant.Handy_TIPS_EXO
                    break
                case Constant.FAR_CONTRY_CODE_ASIA,Constant.FAR_CONTRY_CODE_ASIAFT,Constant.FAR_CONTRY_CODE_ASIAFIT,Constant.FAR_CONTRY_CODE_ASIAHS,Constant.FAR_CONTRY_CODE_AHSASIA,Constant.FAR_CONTRY_CODE_HSASIA,Constant.FAR_CONTRY_CODE_H22015,Constant.FAR_CONTRY_CODE_HS,Constant.FAR_CONTRY_CODE_IND,Constant.FAR_CONTRY_CODE_INDIAFIT,Constant.FAR_CONTRY_CODE_INDIAHS,Constant.FAR_CONTRY_CODE_INDHS,Constant.FAR_CONTRY_CODE_MAYA,Constant.FAR_CONTRY_CODE_MIDDLEEASTFIT,Constant.FAR_CONTRY_CODE_SU,Constant.FAR_CONTRY_CODE_SU1,Constant.FAR_CONTRY_CODE_TESTSURESH :
                    
                    url = DocumentsURLConstant.Handy_TIPS_FAR
                    break
                    
                case Constant.EUR_CONTRY_CODE_AGD,Constant.EUR_CONTRY_CODE_EUROPE,Constant.EUR_CONTRY_CODE_EUROPEHS,Constant.EUR_CONTRY_CODE_EUROPEFIT,Constant.EUR_CONTRY_CODE_HSEUR:
                    
                    url = DocumentsURLConstant.Handy_TIPS_EUR
                    break
                    
                case Constant.AUS_CONTRY_CODE_ANZ,Constant.AUS_CONTRY_CODE_ANZHS:
                    
                    url = DocumentsURLConstant.Handy_TIPS_AUS
                    break
                case Constant.RSA_CONTRY_CODE_AFRICA,Constant.RSA_CONTRY_CODE_AFRICAHSA,Constant.RSA_CONTRY_CODE_HSAAFRICA,Constant.RSA_CONTRY_CODE_AFRICAFIT:
                    
                    url = DocumentsURLConstant.Handy_TIPS_RSA
                    break
                    
                    
                    
                default:
                    url = ""
                    
                }
                
                
            }
            
        }
        
        // }
        if url != ""
        {
            let documentFolderPath =  TourDocumentController.getTourFileDirectoryPath(bfnNumber: (tourDetail?.bfNumber)!)
            
            let visaDocFilePath = documentFolderPath.bfNumberPath + "/" + "handtTips.pdf"
            
            self.downloadDocument(filePath: visaDocFilePath, url: url,moduleName: Title.HANDY_TIPS)
            
            
        }else
        {
            self.displayAlert(message: AlertMessage.NO_DOCUMENT)
        }
        
        
    }

     func getHolidayKitDetails()
    {
        var url = ""
        
        let documentFolderPath =  TourDocumentController.getTourFileDirectoryPath(bfnNumber: (tourDetail?.bfNumber)!)
        
        let visaDocFilePath = documentFolderPath.bfNumberPath + "/" + "holidayKit.pdf"
        
        let viewController:FeedbackWebViewController = FeedbackWebViewController(nibName: "FeedbackWebViewController", bundle: nil)
        
        viewController.redirectionURL = DocumentsURLConstant.HE_KIT_EUR
        viewController.moduleTitle = Title.HOLIDAY_KIT
        viewController.title = Title.HOLIDAY_KIT
         
       self.presentingView?.navigationController?.pushViewController(viewController, animated: true)
        
       // self.downloadDocument(filePath: DocumentsURLConstant.HE_KIT_EUR, url: url,moduleName: Title.HOLIDAY_KIT)
    }
    
    
    //MARK: - Download Doc

    
    func downloadDocument(filePath:String ,url:String,moduleName:String) {
        
        //  let passengerDocumnet:TourPassengerDouments =  documentArray.first!
        
        LoadingIndicatorView.show("Loading")
        
        NetworkCommunication.downloadDocuments(urlString: url, destinationFilePath: filePath, completion: { (downlodedfilePath, status) in
            
            printLog(status,filePath);
            
            if(status)
            {
                self.viewDownloadedFile(fileurl: URL(fileURLWithPath:filePath),moduleName: moduleName)
                
            }
            else{
                
                let isFilePresent = TourDocumentController.isFileExistInDocumentDirectory(fileNamePath: filePath)
                
                if(isFilePresent.chkFile)
                {
                    self.viewDownloadedFile(fileurl: URL(fileURLWithPath:filePath),moduleName: moduleName)
                    
                }else{
                    
                   // DispatchQueue.main.sync {
                        
                        LoadingIndicatorView.hide()
                        
                        AppUtility.displayAlert(message: AlertMessage.NO_DOCUMENT)
                        
                    //}
                    
                }
                
            }
            
        })
    }
    
    //MARK: UIDocumentInteractionController Delegate
    
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController
    {
        return self
    }
    
    func viewDownloadedFile(fileurl : URL,moduleName:String) -> Void
    {
        if let displayPdfVC = self.presentingView as? TourDetailViewController
         {
        
        let concurrentQueue = DispatchQueue(label: "queuename", attributes: .concurrent)
        concurrentQueue.sync {
            
            //DispatchQueue.main.sync {
            
            let documentController : UIDocumentInteractionController = UIDocumentInteractionController.init(url: fileurl)
            
            //self.documentController.uti =
            
            documentController.name  = moduleName
            
            documentController.delegate = displayPdfVC
            
            documentController.presentPreview(animated: true)
            
            LoadingIndicatorView.hide()
            
            // }
            
        }
        }
        
    }
    
    func isApplicableToBookPayNow() -> String {
        
        if self.tourDetail?.departureDate != nil
        {
            
            if let days:Int = self.getDaysBetweenTwoDates(firstDate: Date(), secondDate: self.tourDetail?.departureDate! as! Date)
            {
                if days < 15
                {
                    return  AlertMessage.OPTIONAL_BOOKING_BEFORE_7_DAY
                    
                }
                
            }
        }
        
        return ""
    }
    
    func getDaysBetweenTwoDates(firstDate:Date,secondDate:Date) -> Int?
    {
        let calendar = NSCalendar.current
        
        // Replace the hour (time) of both dates with 00:00
        let date1 = calendar.startOfDay(for: firstDate)
        let date2 = calendar.startOfDay(for: secondDate)
        
        let components = calendar.dateComponents([.day], from: date1, to: date2)
        
        return components.day
    }
    

    
}
