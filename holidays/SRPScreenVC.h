//
//  SRPScreenVC.h
//  Thomoscook_Holiday
//
//  Created by Kush_Tech on 19/02/20.
//  Copyright © 2020 Kush_Tech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Holiday.h"
#import "NewMasterVC.h"
#import <FirebaseAnalytics/FirebaseAnalytics.h>
#import "TalkToExpertView.h"
#import "DateStringSingleton.h"
#import "HolidayPackageDetail.h"
#import "PackageDetailModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface SRPScreenVC : NewMasterVC<UICollectionViewDelegate,UICollectionViewDataSource,UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UICollectionView *collection_view;
@property (weak, nonatomic) IBOutlet UILabel *lbl_title;
@property (weak, nonatomic) IBOutlet UIButton *btn_Header;
@property (weak, nonatomic) IBOutlet UIButton *btn_Like;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Like;
@property (weak, nonatomic) IBOutlet UIButton *btn_Arrow;
@property (weak, nonatomic) IBOutlet UIButton *btn_Filter;
@property (weak, nonatomic) IBOutlet UIButton *btn_FiveNights;
@property (weak, nonatomic) IBOutlet UIButton *btn_SixNights;
@property (weak, nonatomic) IBOutlet UIButton *btn_EightNights;
@property (weak, nonatomic) IBOutlet UIButton *btn_TenNights;
@property (weak, nonatomic) IBOutlet UIView *top_optionsView;
@property (weak, nonatomic) IBOutlet UIView *talk_toExpView;
@property (weak, nonatomic) IBOutlet TalkToExpertView *talkToexpertView;
@property (weak, nonatomic) IBOutlet UIView *sort_View;
@property (weak, nonatomic) IBOutlet UITableView *sort_TableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cnst_heightOfSortView;


@property (strong,nonatomic)NSArray *arrayOfHolidays;

-(void)fetchPackageDetails:(Holiday *)objHoliday;
@property (strong,nonatomic) NSArray * completePackageDetail;

@property (strong,nonatomic) NSString *headerName;
@property (weak,nonatomic) NSString *flag;
@property (strong,nonatomic)NSArray *holidayArrayInSrp;
@property (strong,nonatomic)NSArray *filteredHolidayArray;

@property (strong,nonatomic)HolidayPackageDetail *srpHolidayPackage;
-(void)fetchPackageDetailsWithHoliday:(Holiday *)objHoliday completionHandler:(void (^)(NSDictionary * _Nullable resPonseDict,NSMutableDictionary * _Nullable payLoadDict, NSError * _Nullable error))handlers;

// For Filter Screen
@property (strong,nonatomic)NSArray *holidayArray;
@property (strong,nonatomic) NSString *searchedCity;
@property(strong,nonatomic)NSString *totalPackages;
@property (strong,nonatomic) NSDictionary *filterDict;


@end
NS_ASSUME_NONNULL_END
