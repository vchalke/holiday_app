//
//  AdressPopUpVC.m
//  holidayssdf
//
//  Created by ketan on 06/11/15.
//  Copyright © 2015 Mobicule. All rights reserved.
//

#import "AdressPopUpVC.h"
#import "UIViewController+MJPopupViewController.h"
#import <FirebaseAnalytics/FirebaseAnalytics.h>
#import "NetCoreAnalyticsVC.h"
#import <NetCorePush/NetCorePush.h>

@interface AdressPopUpVC ()

@end

@implementation AdressPopUpVC
@synthesize delegate;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.adressScrollView setContentSize:CGSizeMake(301, 667)];
    [self.viewForElements setFrame:CGRectMake(0, 0, 301, 667)];
    [self.adressScrollView addSubview:_viewForElements];
    CoreUtility *coreobj=[CoreUtility sharedclassname];
    
    if (coreobj.strPhoneNumber.count != 0)
    {
        NSString *phoneNumber = coreobj.strPhoneNumber[0];
        if (phoneNumber != nil && ![phoneNumber isEqualToString:@""])
        {
            self.txtFieldContactNo.text = phoneNumber;
        }
    }
    
    NSUserDefaults *userIdForGooglePlusSignIn=[NSUserDefaults standardUserDefaults];
    NSString *userId=[userIdForGooglePlusSignIn valueForKey:kLoginEmailId];
    if (userId)
    {
        self.txtFieldEmailID.text = userId;
    }
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.txtFieldAddress1)
    {
        NSLog(@"%@", self.txtFieldAddress1.text);
        
        [self.txtFieldAddress2 becomeFirstResponder];
    }
    else if (textField == self.txtFieldAddress2)
    {
        NSLog(@"%@", self.txtFieldAddress2.text);
        
        [self.txtFieldCity becomeFirstResponder];
    }
    else if (textField == self.txtFieldCity)
    {
        [self.txtFieldState becomeFirstResponder];
    }
    else if (textField == self.txtFieldState)
    {
        [self.txtFieldZip becomeFirstResponder];
    }
    else if (textField == self.txtFieldZip)
    {
        [self.txtFieldContactNo becomeFirstResponder];
    }
    else if (textField == self.txtFieldContactNo)
    {
        [self.txtFieldEmailID becomeFirstResponder];
    }
    else if (textField == self.txtFieldEmailID)
    {
        [self.txtFieldEmailID resignFirstResponder];
    }
    return YES;
}

-(BOOL)checkAllFields
{
    if ([self.txtFieldAddress1.text isEqualToString:@""] || [self.txtFieldAddress2.text isEqualToString:@""] || [self.txtFieldCity.text isEqualToString:@""] || [self.txtFieldState.text isEqualToString:@""] || [self.txtFieldZip.text isEqualToString:@""] || [self.txtFieldContactNo.text isEqualToString:@""] || [self.txtFieldEmailID.text isEqualToString:@""] )
    {
        return NO;
    }
    return YES;
}

- (IBAction)onContinueButtonClicked:(id)sender
{
    
    if ([self checkAllFields])
    {
        if (![self isValidZipCode])
        {
            [self showAlertViewWithTitle:@"Alert" withMessage:@"Enter Valid Zip Code"];
        }
        else if (![self validatePhoneNumber])
        {
            [self showAlertViewWithTitle:@"Alert" withMessage:@"Enter Valid Phone Number"];
        }
        else if (![self isValidEmail])
        {
            [self showAlertViewWithTitle:@"Alert" withMessage:@"Enter Valid Email ID"];
        }
        else if (![self isValidCityName])
        {
            [self showAlertViewWithTitle:@"Alert" withMessage:@"Please Enter Valid City name"];
        }
        else if (![self isValidStateName])
        {
            [self showAlertViewWithTitle:@"Alert" withMessage:@"Please Enter Valid State name"];
        }
        else if (![self isValidGistNumberWitgGist:self.textFieldGSTInNumber.text])
        {
            [self showAlertViewWithTitle:@"Alert" withMessage:@"Please Enter Valid GSTIN Number"];
        }
        else
        {
            [self netCoreAddressForm];  //09-04-2018
            
            if (self.delegate && [self.delegate respondsToSelector:@selector(continueButtonClicked:)]) {
                [self.delegate continueButtonClicked:self];
                [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
                
        }
         }
        
        [FIRAnalytics logEventWithName:@"Address_of_Communication_Traveller_Details" parameters:@{kFIRParameterItemID:[NSString stringWithFormat:@"Address : %@,City : %@,State : %@,Contact Number : %@", self.txtFieldAddress1.text,self.txtFieldCity.text,self.txtFieldState.text,self.txtFieldContactNo.text]}];
        
        
        
    }
    else
    {
        [self showAlertViewWithTitle:@"Alert" withMessage:@"Enter All fields"];
    }
    
    
    
}

-(BOOL)isValidGistNumberWitgGist:(NSString *)gistNumber
{
    if (![gistNumber isEqualToString:@""] )
    {
        if (gistNumber.length == 15)
        {
            NSString *firstTwoCharacterOfGistNumber = [gistNumber substringToIndex:2];
            NSString *firstTwoCharactersOfStateCode = [self.stateCode substringToIndex:2];
            
            if ([[firstTwoCharacterOfGistNumber lowercaseString] isEqualToString:[firstTwoCharactersOfStateCode lowercaseString]])
            {
                return YES;
            }
            
            return NO;
        }
        
        return NO;
    }
    
    return YES;
}

- (IBAction)onCancelButtonClicked:(id)sender
{
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
}

-(void)showAlertViewWithTitle:(NSString *)title withMessage:(NSString *)message
{
    UIAlertController *alertView = [UIAlertController alertControllerWithTitle:title
                                                                       message:message
                                                                preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [alertView dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertView addAction:okAction];
    
    [self presentViewController:alertView animated:YES completion:nil];
}

-(BOOL)isValidZipCode
{
    int iValue;
    if (self.txtFieldZip.text.length == 6 && [[NSScanner scannerWithString:self.txtFieldZip.text] scanInt:&iValue])
    {
        NSLog(@"value entered correctly.");
        return YES ;
    }
    else
    {
        return NO;;
    }
    return NO;
}
-(BOOL)validatePhoneNumber
{
    NSString *string = self.txtFieldContactNo.text;
    NSString *expression = @"^([+][9][1]|[9][1]|[0]){0,1}([7-9]{1})([0-9]{9})$";
    NSError *error = NULL;
    
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
    
    NSTextCheckingResult *match = [regex firstMatchInString:string options:0 range:NSMakeRange(0, [string length])];
    
    if (match){
        NSLog(@"yes");
        return YES;
    }else{
        NSLog(@"no");
        return NO;
    }
  
}

-(BOOL)isValidEmail
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:self.txtFieldEmailID.text];
}


-(BOOL)isValidCityName
{
    NSCharacterSet  *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"];
    for (int i = 0; i < [self.txtFieldCity.text length]; i++)
    {
        
        unichar c = [self.txtFieldCity.text characterAtIndex:i];
        if (![myCharSet characterIsMember:c])
        {
            
            return NO;
        }
        
    }
   return YES;
    
}
-(BOOL)isValidStateName
{
      NSCharacterSet  *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"];
    for (int i = 0; i < [self.txtFieldState.text length]; i++)
    {
        
        unichar c = [self.txtFieldState.text characterAtIndex:i];
        if (![myCharSet characterIsMember:c])
        {
           
            return NO;
        }
    }

    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([string isEqualToString:@""])
    {
        return YES;
    }
    if (textField == self.txtFieldContactNo)
    {
        if([self.txtFieldContactNo.text length]==10)
        {
            return NO;
        }
        else
        {
            return YES;
        }
    }
    
    if (textField == self.txtFieldZip)
    {
        if([self.txtFieldZip.text length]==6)
        {
            return NO;
        }
        else
        {
            return YES;
        }
    }
    else if(textField == self.txtFieldCity)
    {
        
    NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                //UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Numbers not allowed" delegate:self   cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                //[alert show];
                return NO;
            }
                
        }
            
    }
    else if(textField ==self.txtFieldState)
    {
        
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                //UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Numbers not allowed" delegate:self   cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                //[alert show];
                return NO;
            }
            
        }
        
    }
    else if(textField ==self.txtFieldContactNo)
    {
        
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                //UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Characters not allowed" delegate:self   cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                //[alert show];
                return NO;
            }
            
        }
        
    }
    else  if (textField == self.textFieldGSTInNumber)
    {
        NSString *currentString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        int length = (int)[currentString length];
        if (length > 15)
        {
            return NO;
        }
        return YES;
    }

    
    return YES;
        
}

/*NSMutableDictionary *profileDetail = @{ @"ADDRESS" : "<address>", @"CITY" : "<city>", @"STATE" : "<state>", @"PINCODE" : "<pincode>", @"COUNTRY" : "<country>", @"EMAIL" : "<email>", @"GSTIN" : "<gstin>"};

NetCoreAppTracking.sharedInstance().sendEvent(withCustomPayload: 102, payload: profileDetail, block: nil);*/

-(void)netCoreAddressForm
{
    NSMutableDictionary *profileDetail = [[NSMutableDictionary alloc] init];
    [profileDetail setObject:self.txtFieldAddress1.text forKey:@"ADDRESS"];
    [profileDetail setObject:self.txtFieldCity.text forKey:@"CITY"];
    [profileDetail setObject:self.txtFieldState.text forKey:@"STATE"];
    [profileDetail setObject:self.txtFieldZip.text forKey:@"PINCODE"];
    [profileDetail setObject:@"" forKey:@"COUNTRY"];
    [profileDetail setObject:self.txtFieldEmailID.text forKey:@"EMAIL"];
    [profileDetail setObject:self.textFieldGSTInNumber.text forKey:@"GSTIN"];
    
    [NetCoreAnalyticsVC getNetCorePushNotificationDetailDictionary:profileDetail withPayloadCount:102];
    
    CoreUtility *coreobj=[CoreUtility sharedclassname];
    
    if (coreobj.strPhoneNumber.count != 0)
    {
        NSString *phoneNumber = coreobj.strPhoneNumber[0];
        if (phoneNumber != nil && ![phoneNumber isEqualToString:@""])
        {
            [[NetCoreInstallation sharedInstance] netCoreProfilePush:phoneNumber Payload:profileDetail Block:nil];
        }
        else
        {
            [[NetCoreInstallation sharedInstance] netCoreProfilePush:@"" Payload:profileDetail Block:nil];
        }
    }
    else
    {
        [[NetCoreInstallation sharedInstance] netCoreProfilePush:@"" Payload:profileDetail Block:nil];
    }

    
    
    
}


@end
