//
//  NetworkUtility.m
//  mobicule-device-network-layer
//
//  Created by Neetesh Dubey on 10/06/16.
//  Copyright © 2016 Mobicule. All rights reserved.
//

#import "NetworkUtility.h"
#import <zlib.h>
#import "NSString+NSHash.h"
#import "NSData+AESAlgo.h"
#import "NSString+AESAlgo.h"


@implementation NetworkUtility


+(NSData *)gzipInflate:(NSData*)data
{
    if ([data length] == 0) return data;
    
    unsigned full_length = (uInt)[data length];
    
    unsigned half_length= (uInt)[data length]/2;
    
    NSMutableData *decompressed = [NSMutableData dataWithLength: full_length + half_length];
    BOOL done = NO;
    int status;
    
    z_stream strm;
    strm.next_in = (Bytef *)[data bytes];
    strm.avail_in =(uInt)[data length];
    strm.total_out = 0;
    strm.zalloc = Z_NULL;
    strm.zfree = Z_NULL;
    
    if (inflateInit2(&strm, (15+32)) != Z_OK) return nil;
    while (!done)
    {
        // Make sure we have enough room and reset the lengths.
        if (strm.total_out >= [decompressed length])
            [decompressed increaseLengthBy: half_length];
        strm.next_out = [decompressed mutableBytes] + strm.total_out;
        strm.avail_out = (uInt)([decompressed length] - strm.total_out);
        
        // Inflate another chunk.
        status = inflate (&strm, Z_SYNC_FLUSH);
        if (status == Z_STREAM_END) done = YES;
        else if (status != Z_OK) break;
    }
    if (inflateEnd (&strm) != Z_OK) return nil;
    
    // Set real length.
    if (done)
    {
        [decompressed setLength: strm.total_out];
        return [NSData dataWithData: decompressed];
    }
    else return nil;
}

+(NSData *)gzipDeflate:(NSData*)data
{
    if ([data length] == 0) return data;
    
    z_stream strm;
    
    strm.zalloc = Z_NULL;
    strm.zfree = Z_NULL;
    strm.opaque = Z_NULL;
    strm.total_out = 0;
    strm.next_in=(Bytef *)[data bytes];
    strm.avail_in = (uInt)[data length];
    
    if (deflateInit2(&strm, Z_DEFAULT_COMPRESSION, Z_DEFLATED, (15+16), 8, Z_DEFAULT_STRATEGY) != Z_OK) return nil;
    
    NSMutableData *compressed = [NSMutableData dataWithLength:16384];  // 16K chunks for expansion
    
    do {
        
        if (strm.total_out >= [compressed length])
            [compressed increaseLengthBy: 16384];
        
        strm.next_out = [compressed mutableBytes] + strm.total_out;
        strm.avail_out = (uInt)([compressed length] - strm.total_out);
        
        deflate(&strm, Z_FINISH);
        
    } while (strm.avail_out == 0);
    deflateEnd(&strm);
    [compressed setLength: strm.total_out];
    return [NSData dataWithData:compressed];
}

- (nonnull NSString*) toHexString:(unsigned char*) data length: (unsigned int) length {
    NSMutableString* hash = [NSMutableString stringWithCapacity:length * 2];
    for (unsigned int i = 0; i < length; i++) {
        [hash appendFormat:@"%02X", data[i]];
        data[i] = 0;
    }
    return hash;
}

-(NSString *)reverseString :(NSString *)stringToBeReversed
{
    NSMutableString *reversedString = [NSMutableString string];
    NSInteger charIndex = [stringToBeReversed length];
    while (charIndex > 0) {
        charIndex--;
        NSRange subStrRange = NSMakeRange(charIndex, 1);
        [reversedString appendString:[stringToBeReversed substringWithRange:subStrRange]];
        
    }
    return reversedString;
}

-(NSString *)getKey
{
    NSString * key=@"stillgo4it*mobic";
    return key;
}

-(NSString *)generateSecretKey:(NSString *)packageName
{
    NSString * key=[self getKey];
    NSString *reversedString= [self reverseString:packageName];
    NSString *encryptedString = [reversedString AES128EncryptWithKey:key];
    NSString *sentence = [encryptedString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]; //trims sentence off white space
    NSUInteger length= sentence.length;
     unsigned  char *string = (unsigned char *)[encryptedString UTF8String];
    NSString* hexString = [self toHexString:string length:(unsigned int)length];
    return hexString;
}

-(NSString *)generateDigest:(NSString *)requestBody withKey :(NSString *)key
{
    NSString* append = [requestBody stringByAppendingString:key];
    NSString * digest =[append SHA256];
    return digest;
}

	//This Method gives the Digest value by calling
- (NSString *)generateDigestValue : (NSString *)requestBody withPackageName:(NSString *)packageName
{
	NSString *pkgName ;
	
	if (![packageName isEqualToString:@""]) {
		pkgName = packageName;
	}
	else{
		pkgName= [self getPackageName];
	}
	
	NSString * key= [self generateSecretKey:pkgName];
	
 
	NSString * digest=[self generateDigest:requestBody withKey:key];
	
	return digest;
}

- (NSString *)getPackageName{
	
	return [[NSBundle mainBundle] bundleIdentifier];
	
}


-(NSString *)getOTP:(NSString *)otpString{
    NSString * key = [self getKey];
    NSString * otp = [otpString AES128DecryptWithKey:key];
    return otp;
}

-(NSString *)setOTP:(NSString *)sendOTPString{
    NSString * key = [self getKey];
    NSString * otp = [sendOTPString AES128EncryptWithKey:key];
    return otp;
}

@end
