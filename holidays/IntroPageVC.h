//
//  IntroPageVC.h
//  holiday
//
//  Created by Pushpendra Singh on 04/08/15.
//  Copyright (c) 2015 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IntroPageVC : UIViewController<UIPageViewControllerDataSource,UIScrollViewDelegate>

@property(nonatomic,strong) UIPageViewController *pageViewController;
@property (weak, nonatomic) IBOutlet UIButton *btnLetsGo;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewIntroPage;
- (IBAction)onLetsGoBtnClicked:(id)sender;
@property (strong, nonatomic) IBOutlet UIPageControl *pageControl;

@end
