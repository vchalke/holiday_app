//
//  LastMinutesDealsCollectionViewCell.m
//  holidays
//
//  Created by Kush_Tech on 24/02/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "LastMinutesDealsCollectionViewCell.h"
#import "UIImageView+WebCache.h"

@implementation LastMinutesDealsCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(void)showLastMinutesDeals:(WhatsNewObject*)bannerObj{
   
    self.lbl_destination.text = bannerObj.bannerName;
//     self.lbl_prize.text = [self getNumberFormatterString:bannerObj.bannerStrikeoutPrice];
//    self.lbl_prize_low.text = [self getNumberFormatterString:bannerObj.bannerStartingPrice];
    self.lbl_prize.text = bannerObj.bannerStrikeoutPrice;
    self.lbl_prize_low.text = bannerObj.bannerStartingPrice;
    self.lbl_numOfDays.text =bannerObj.bannerDuration;
   NSURL* urlImage=[NSURL URLWithString:bannerObj.bannerSmallImage];
    if (urlImage == nil){
        urlImage=[NSURL URLWithString:bannerObj.mainBannerImgUrl];
    }
    if(urlImage)
    {
        UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        indicator.center = self.banner_image.center;
        [self.banner_image addSubview:indicator];
        [indicator startAnimating];
        [self.banner_image sd_setImageWithURL:urlImage placeholderImage:[UIImage imageNamed:@"defaultBanner.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
            [indicator stopAnimating];
        }];
    }
    
}
@end
