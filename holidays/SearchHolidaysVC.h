//
//  SearchHolidaysVC.h
//  holidays
//
//  Created by Pushpendra Singh on 10/08/15.
//  Copyright (c) 2015 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FirebaseAnalytics/FirebaseAnalytics.h>

#import "BaseViewController.h"
#import "SlideMenuViewController.h"

@interface SearchHolidaysVC : BaseViewController<UITextFieldDelegate,SlideNavigationControllerDelegate,UITableViewDataSource,UITableViewDelegate,dismissView>{

   

}
@property (weak, nonatomic) IBOutlet UILabel *lblContactDetails;
@property (weak, nonatomic) IBOutlet UIButton *btnFindHoliday;

@property (strong, nonatomic) IBOutlet UIView *viewSearchHolidays;
@property (strong, nonatomic) IBOutlet UIView *searchTableView;
@property (strong,nonatomic) NSString *headerName;
@property (strong,nonatomic) NSString *bannerdestination;
@property (strong,nonatomic) NSString *bannertype;
@property (strong,nonatomic) NSString *packageID;
@property (strong,nonatomic) NSString *packageName;
@property(nonatomic,assign) BOOL boolForSearchPackgScreen;
@property (weak, nonatomic) IBOutlet UITableView *searchTable;
@property (weak, nonatomic) IBOutlet UITextField *txtDestination;
@property (weak, nonatomic) IBOutlet UITextField *txtNights;
@property (weak, nonatomic) IBOutlet UITextField *txtBudget;
@property (weak, nonatomic) IBOutlet UITextField *txtMobileNo;
@property (weak, nonatomic) IBOutlet JTMaterialSpinner *materialSpinner;



#pragma mark - UIbutton Action methods
- (IBAction)clickFindHolidays:(id)sender;
- (IBAction)btnNumberOfNightsClicked:(id)sender;
- (IBAction)btnBudgetPerPersonClicked:(id)sender;

- (IBAction)btnOnNightsPopUpClicked:(id)sender;

- (IBAction)btnBudgetOptionClicked:(id)sender;

@property (strong,nonatomic) UILabel *deepLinkLabel;
@end
