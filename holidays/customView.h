//
//  customView.h
//  Holiday
//
//  Created by Kush Thakkar on 15/02/20.
//  Copyright © 2020 Kush Thakkar. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@protocol CustomTopViewDelegaete <NSObject>
@optional
- (void)searchButtonClick;
- (void)jumpToWebView:(NSString*)webUrlString withTitle:(NSString*)webViewTitle;
@end
@interface customView : UIView <UICollectionViewDelegate,UICollectionViewDataSource> {
    NSArray *imageArray;
    NSArray *titleArray;
    
}
@property (nonatomic, weak) id <CustomTopViewDelegaete> custDelegate;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end

NS_ASSUME_NONNULL_END
