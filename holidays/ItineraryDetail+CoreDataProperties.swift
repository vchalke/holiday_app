//
//  ItineraryDetail+CoreDataProperties.swift
//  holidays
//
//  Created by Saurav on 28/11/17.
//  Copyright © 2017 Mobicule. All rights reserved.
//

import Foundation
import CoreData


extension ItineraryDetail {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ItineraryDetail> {
        return NSFetchRequest<ItineraryDetail>(entityName: "ItineraryDetail")
    }

    @NSManaged public var altTag: String?
    @NSManaged public var cityCode: String?
    @NSManaged public var cityName: String?
    @NSManaged public var highlights: String?
    @NSManaged public var holidayItineraryId: String?
    @NSManaged public var image: String?
    @NSManaged public var itineraryDay: String?
    @NSManaged public var itineraryDescription: NSData?
    @NSManaged public var ltCityCode: String?
    @NSManaged public var mealDescription: String?
    @NSManaged public var overnightIconId: String?
    @NSManaged public var overnightStay: String?
    @NSManaged public var packageId: String?
    @NSManaged public var prodITINCode: String?
    @NSManaged public var prodITINCode_packageId_holidayItineraryId: String?
    @NSManaged public var tourRelation: Tour?

}
