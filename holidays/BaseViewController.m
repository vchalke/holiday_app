//
//  BaseViewController.m
//  genie
//
//  Created by ketan on 01/04/15.
//  Copyright (c) 2015 Mobicule. All rights reserved.
//

#import "BaseViewController.h"
#import "Reachability.h"
#import "OverviewVC.h"
#import "TabMenuVC.h"


@interface BaseViewController ()
{
  
}

@end

@implementation BaseViewController
@synthesize backBtnFromOverview;
@synthesize tabMenuVController;
@synthesize delegate;

- (void)viewDidLoad
{
    [super viewDidLoad];
   
    
    [_btnBack addTarget:self action:@selector(onBackButtonClick:) forControlEvents:UIControlEventTouchUpInside];
   }

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[SlideNavigationController sharedInstance] setNavigationBarHidden:YES];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)addViewInBaseView:(UIView *)subView
{
    CGFloat topPadding;
    CGFloat bottomPadding;
    
    if (@available(iOS 11.0, *)) {
        UIWindow *window = UIApplication.sharedApplication.keyWindow;
        topPadding = window.safeAreaInsets.top;
        bottomPadding = window.safeAreaInsets.bottom;
        
    }
    else{
        topPadding = 0;
        bottomPadding = 0;
    }
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height - topPadding - bottomPadding;
    subView.frame = CGRectMake(0, 0, screenWidth, screenHeight);
    self.scrollView.frame = CGRectMake(0, 0, screenWidth, screenHeight);
    [self.scrollView setContentSize:CGSizeMake(screenWidth, screenHeight)];
    self.scrollView.scrollEnabled = false;
    [self.scrollView addSubview:subView];
    
   
}

-(void)applyLeftPaddingToTextfield:(UITextField *)textField
{
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    textField.leftView = paddingView;
    textField.leftViewMode = UITextFieldViewModeAlways;
}

-(void)onBackButtonClick:(id)sender{
    
    
    [self.delegate backButtonAction];
    
    [self backButtonAction];
    
    
    
}

-(void)backButtonAction{

[[SlideNavigationController sharedInstance] popViewControllerAnimated:YES];
}
//- (UIViewController *) getAnyPresentedController
//{
//    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
//    while (topController.presentedViewController) {
//        topController = topController.presentedViewController;
//      //  if([topController isKindOfClass:[SlideMenuViewController class]]){
//            return topController;
//        }
//    }
//    
//    return 0;
//}
- (IBAction)onSlideMenuButtonClicked:(id)sender
{
     _slideMenuBool = YES;
    
//    [[SlideNavigationController sharedInstance] toggleLeftMenu];
    [self.navigationController popViewControllerAnimated:YES];
    //UIViewController *vc=[self getAnyPresentedController];
  //  SlideMenuViewController *slideMenuVc=(SlideMenuViewController *)vc;
    //[slideMenuVc setLoginStatusValue];
}


-(void)hideMenuBar
{
    self.menuBarImageView.alpha = 0;
    self.headerImage.alpha = 0;
    self.HeaderLabel.alpha = 0;
    self.btnBack.alpha = 0;
    self.backIconImage.alpha = 0;
}

-(void)hideMenuButton
{
    self.sliderMenuButton.alpha = 0;
}

-(void)hideBackButton:(BOOL)hide{
    
    _btnBack.hidden = hide;
    self.backIconImage.hidden = hide;

    
}

- (UIColor *)colorWithHex:(UInt32)hex A:(CGFloat)alpha
{
    return [UIColor colorWithRed:((hex & 0xFF0000) >> 16)/255.0
                           green:((hex & 0x00FF00) >> 8)/255.0
                            blue:( hex & 0x0000FF)/255.0
                           alpha:alpha];
}


-(void)setHeaderTitle:(NSString *)title
{
    self.HeaderLabel.text = title;
}

- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}

-(void)showAlertViewWithTitle:(NSString *)title withMessage:(NSString *)message
{
    UIAlertController *alertView = [UIAlertController alertControllerWithTitle:title
                                                                       message:message
                                                                preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [alertView dismissViewControllerAnimated:YES completion:nil];
        
    }];
    
    [alertView addAction:okAction];
    
    [[[[SlideNavigationController sharedInstance] viewControllers ] lastObject] presentViewController:alertView animated:YES completion:nil];
}

-(void)showAlertViewForVersionUpdateWithUrl:(NSString *)urlString
{
    UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Alert"
                                                                       message:@"forcefully upgradation required"
                                                                preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        
        [alertView dismissViewControllerAnimated:YES completion:nil];
        NSURL *url = [NSURL URLWithString:urlString];
        if (url)
        {
            [[UIApplication sharedApplication] openURL:url];
        }
    
    }];
    [alertView addAction:okAction];
    
    [[[[SlideNavigationController sharedInstance] viewControllers ] lastObject] presentViewController:alertView animated:YES completion:nil];
}


@end
