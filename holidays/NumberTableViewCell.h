//
//  NumberTableViewCell.h
//  Holiday
//
//  Created by Kush Thakkar on 14/02/20.
//  Copyright © 2020 Kush Thakkar. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
//@protocol NumberTableViewCellDelegate<NSObject>
//
//@required
//-(void)didSelectButton:(UITableViewCell *)cell;
//
//@end

@interface NumberTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *numberButton;

//@property (nonatomic, weak) id <NumberTableViewCellDelegate> delegate;


- (IBAction)numberButtonPressed:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UIView *upperView;

@property (weak, nonatomic) IBOutlet UIView *bottomView;


@end

NS_ASSUME_NONNULL_END
