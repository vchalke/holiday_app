//
//  AppConstant.swift
//  sotc-consumer-application
//
//  Created by Saurav Kumar on 29/09/17.
//  Copyright © 2017 Mobicule Technologies Private Limited. All rights reserved.
//

import Foundation
import CoreData


//MARK: - ------------ AuthenticationConstant Constant ------------------

//MARK: - Final UAT Conastant
//FaceBookAppId : 162198297725530  =---- forex
//1021596341229809  ---- Holidays

/*
struct AuthenticationConstant {
    
    static let CheckMobileResponseTag = UatURLConstant.CheckMobileResponseTag
    
    static let CheckMobileURL = UatURLConstant.CheckMobileURL
    
    static let GetOTPURL = UatURLConstant.GetOTPURL
    
    static let ValidateOTPURL:String = UatURLConstant.ValidateOTPURL
    
    static let SendOTPURL = UatURLConstant.SendOTPURL
    
    static let SendOTPTOSERVERURL = UatURLConstant.SendOTPtoServerURL
    
    static let SendBranchAddressTOSERVERURL = UatURLConstant.SendBranchAddresstoServerURL
    
    static let AuthenticationUserID = UatURLConstant.AuthenticationUserID
    
    static let AuthenticationPassword = UatURLConstant.AuthenticationPassword
    
    static let MOBICULE_URL = UatURLConstant.MOBICULE_URL
    
    static let MOBICULE_FEEDBACK_URL = UatURLConstant.MOBICULE_FEEDBACK_URL
    
    
    static let TC_CSS_PAY_NOW_URL = UatURLConstant.TC_CSS_PAY_NOW_URL
    
    static let TC_CSS_PAYMENT_AUTH_USERNAME = UatURLConstant.TC_CSS_PAYMENT_AUTH_USERNAME
    static let TC_CSS_PAYMENT_AUTH_PASS = UatURLConstant.TC_CSS_PAYMENT_AUTH_PASS
    
    
    
    static let MOBICULE_URL_SEND_MAIL = UatURLConstant.MOBICULE_URL_SEND_MAIL
    
     static let MOBICULE_SEND_OPTIONAL_MAIL = UatURLConstant.MOBICULE_SEND_OPTIONAL_MAIL_URL
    
    static let Mobicule_Authorization = UatURLConstant.Mobicule_Authorization // keep this as UAT for prod
    
    static let ASTRA_SERVICES_URL = LiveURLConstant.ASTRA_SERVICES_URL
    
    
    static let ASTRA_RESOURCE_URL = LiveURLConstant.ASTRA_RESOURCE_URL
    
    static let ASTRA_REQUEST_TO_CALL_URL = LiveURLConstant.ASTRA_REQUEST_TO_CALL_URL
    
    static let ASTRA_SHARE_URL = LiveURLConstant.ASTRA_SHARE_URL
    
    static let PG_URL = LiveURLConstant.PG_URL
    
    static let Prebooking_Last_Level_Banner_URL = LiveURLConstant.Prebooking_Last_Level_Banner_URL
    
    static let SALT = UatURLConstant.SALT
    
    static let KEY = UatURLConstant.KEY
    
    static let merchantid = UatURLConstant.merchantid
    
    static let PAYU_SUCCESS_URL = UatURLConstant.PAYU_SUCCESS_URL
    
    static let PAYU_FAILUER_URL = UatURLConstant.PAYU_FAILUER_URL
    
    static let PAYU_HASH_CALCULATION_URL = UatURLConstant.PAYU_HASH_CALCULATION_URL // keep this as uat for prod
    
    //static let ENVIRONMENT = UatURLConstant.ENVIRONMENT
    
    static let FIRST_NAME = UatURLConstant.FIRST_NAME
    
    static let LOGO_URL = UatURLConstant.LOGO_URL
    
    static let PRODUCT_INFO = UatURLConstant.PRODUCT_INFO
    
    static let EMAIL = UatURLConstant.EMAIL
    
    static let PHONE = UatURLConstant.PHONE
    
    static let EnableSSLPinning:Bool = true
    
    static let SSLCertificates = UatURLConstant.SSLCertificates
    
}

//MARK: - ------------ DocumentsURLConstant Constant ------------------

struct DocumentsURLConstant{
    
    
    static let PERFORMA_INVOICE_URL = UatURLConstant.PERFORMA_INVOICE_URL
    
    static let LETTER_FORMAT_URL = UatURLConstant.LETTER_FORMAT_URL
    
    static let VISA_REQUIREMENTS_USA_CANADA = UatURLConstant.VISA_REQUIREMENTS_USA_CANADA
    
    static let VISA_REQUIREMENTS_EXO = UatURLConstant.VISA_REQUIREMENTS_EXO
    
    static let VISA_REQUIREMENTS_FAR = UatURLConstant.VISA_REQUIREMENTS_FAR
    
    static let VISA_REQUIREMENTS_EUR = UatURLConstant.VISA_REQUIREMENTS_EUR
    
    //MARK: - HANDY TIPS
    
    static let Handy_TIPS_USA = UatURLConstant.Handy_TIPS_USA
    
    static let Handy_TIPS_EXO = UatURLConstant.Handy_TIPS_EXO
    
    static let Handy_TIPS_FAR = UatURLConstant.Handy_TIPS_FAR
    
    static let Handy_TIPS_EUR = UatURLConstant.Handy_TIPS_EUR
    
    static let Handy_TIPS_AUS = UatURLConstant.Handy_TIPS_AUS
    
    static let Handy_TIPS_RSA = UatURLConstant.Handy_TIPS_RSA
    
    //MARK: - HE KIT
    
    static let HE_KIT_USA = UatURLConstant.HE_KIT_USA
    
    static let HE_KIT_EXO = UatURLConstant.HE_KIT_EXO
    
    static let HE_KIT_FAR = UatURLConstant.HE_KIT_FAR
    
    static let HE_KIT_EUR = UatURLConstant.HE_KIT_EUR
    
    static let HE_KIT_AUS = UatURLConstant.HE_KIT_AUS
    
    static let HE_KIT_RSA = UatURLConstant.HE_KIT_RSA
    
    static let INTERNATIONAL_HOLIDAYS_URL = UatURLConstant.INTERNATIONAL_HOLIDAYS_URL
    
    static let INDIAN_HOLIDAYS_URL = UatURLConstant.INDIAN_HOLIDAYS_URL
    
}

*/

//END UAT Url
//MARK: - Final LIVE Conastant

struct AuthenticationConstant {

    static let CheckMobileResponseTag = LiveURLConstant.CheckMobileResponseTag

    static let CheckMobileURL = LiveURLConstant.CheckMobileURL

    static let GetOTPURL = LiveURLConstant.GetOTPURL

   
    
    static let ValidateOTPURL:String = LiveURLConstant.ValidateOTPURL

    static let SendOTPURL = LiveURLConstant.SendOTPURL

    static let AuthenticationUserID = LiveURLConstant.AuthenticationUserID

    static let AuthenticationPassword = LiveURLConstant.AuthenticationPassword
    
    static let TC_CSS_PAY_NOW_URL = LiveURLConstant.TC_CSS_PAY_NOW_URL
    
    static let MOBICULE_SEND_OPTIONAL_MAIL = LiveURLConstant.MOBICULE_SEND_OPTIONAL_MAIL_URL

    static let MOBICULE_URL = LiveURLConstant.MOBICULE_URL

    static let MOBICULE_URL_SEND_MAIL = LiveURLConstant.MOBICULE_URL_SEND_MAIL

    static let ASTRA_SERVICES_URL = LiveURLConstant.ASTRA_SERVICES_URL
    
    
    static let ASTRA_RESOURCE_URL = LiveURLConstant.ASTRA_RESOURCE_URL
    
    static let ASTRA_REQUEST_TO_CALL_URL = LiveURLConstant.ASTRA_REQUEST_TO_CALL_URL
    
    static let ASTRA_SHARE_URL = LiveURLConstant.ASTRA_SHARE_URL
    
    static let PG_URL = LiveURLConstant.PG_URL

    static let SALT = LiveURLConstant.SALT

    static let KEY = LiveURLConstant.KEY

    static let merchantid = LiveURLConstant.merchantid

    static let PAYU_SUCCESS_URL = LiveURLConstant.PAYU_SUCCESS_URL

    static let PAYU_FAILUER_URL = LiveURLConstant.PAYU_FAILUER_URL

    
   // static let ENVIRONMENT = LiveURLConstant.ENVIRONMENT
    
    static let TC_CSS_PAYMENT_AUTH_USERNAME = LiveURLConstant.TC_CSS_PAYMENT_AUTH_USERNAME
    static let TC_CSS_PAYMENT_AUTH_PASS = LiveURLConstant.TC_CSS_PAYMENT_AUTH_PASS

    static let FIRST_NAME = LiveURLConstant.FIRST_NAME

    static let LOGO_URL = LiveURLConstant.LOGO_URL

    static let PRODUCT_INFO = LiveURLConstant.PRODUCT_INFO

    static let EMAIL = LiveURLConstant.EMAIL

    static let PHONE = LiveURLConstant.PHONE

    static let EnableSSLPinning:Bool = true
    
    static let MOBICULE_FEEDBACK_URL = LiveURLConstant.MOBICULE_FEEDBACK_URL
    
    
     static let SendOTPTOSERVERURL = UatURLConstant.SendOTPtoServerURL // TC-CSS SAME AS UAT
    
    static let SendBranchAddressTOSERVERURL = UatURLConstant.SendBranchAddresstoServerURL // TC-CSS SAME AS UAT
    
    static let PAYU_HASH_CALCULATION_URL = UatURLConstant.PAYU_HASH_CALCULATION_URL // keep this as uat for prod
    
    static let Mobicule_Authorization = UatURLConstant.Mobicule_Authorization // keep this as UAT for prod
 
    static let SSLCertificates =  LiveURLConstant.SSLCertificates


}

//MARK: - ------------ DocumentsURLConstant Constant ------------------

struct DocumentsURLConstant{


    static let PERFORMA_INVOICE_URL = LiveURLConstant.PERFORMA_INVOICE_URL

    static let LETTER_FORMAT_URL = LiveURLConstant.LETTER_FORMAT_URL

    static let VISA_REQUIREMENTS_USA_CANADA = LiveURLConstant.VISA_REQUIREMENTS_USA_CANADA

    static let VISA_REQUIREMENTS_EXO = LiveURLConstant.VISA_REQUIREMENTS_EXO

    static let VISA_REQUIREMENTS_FAR = LiveURLConstant.VISA_REQUIREMENTS_FAR

    static let VISA_REQUIREMENTS_EUR = LiveURLConstant.VISA_REQUIREMENTS_EUR

    //MARK: - HANDY TIPS

    static let Handy_TIPS_USA = LiveURLConstant.Handy_TIPS_USA

    static let Handy_TIPS_EXO = LiveURLConstant.Handy_TIPS_EXO

    static let Handy_TIPS_FAR = LiveURLConstant.Handy_TIPS_FAR

    static let Handy_TIPS_EUR = LiveURLConstant.Handy_TIPS_EUR

    static let Handy_TIPS_AUS = LiveURLConstant.Handy_TIPS_AUS

    static let Handy_TIPS_RSA = LiveURLConstant.Handy_TIPS_RSA

    //MARK: - HE KIT

    static let HE_KIT_USA = LiveURLConstant.HE_KIT_USA

    static let HE_KIT_EXO = LiveURLConstant.HE_KIT_EXO

    static let HE_KIT_FAR = LiveURLConstant.HE_KIT_FAR

    static let HE_KIT_EUR = LiveURLConstant.HE_KIT_EUR

    static let HE_KIT_AUS = LiveURLConstant.HE_KIT_AUS

    static let HE_KIT_RSA = LiveURLConstant.HE_KIT_RSA

}

//END Live
//MARK: - ------------ UAT URL Constant ------------------

struct UatURLConstant
{
    fileprivate static let CheckMobileResponseTag = "CHECK_MOBILE_RESPONSE"
    
    
//    fileprivate static let CheckMobileURL = "http://uat2.thomascook.in/ltweb/services/LtAstraServices?wsdl"
    fileprivate static let CheckMobileURL = "http://uat2.thomascook.in/ltweb/services/LtAstraServices" // new got On  10 july
    
    fileprivate static let GetOTPURL = "CHECK_MOBILE_RESPONSE"
    
    //SOTC
    fileprivate static let ValidateOTPURL:String = "http://otp.xapp.spocbridge.com/api/Token/ValidateToken?ApplicationID=4&UserID=unnikrishnan&Mobile=%@&Token=%@"
    
    fileprivate static let SendOTPURL = "http://otp.xapp.spocbridge.com/api/Token/GenerateToken?ApplicationID=4&UserID=unnikrishnan&Mobile="
    
    
    //http://202.87.33.165/GatewayAPI/rest?method=SendMessage&send_to=9833998977
    
    //&msg=417662%20is%20the%20OTP%20for%20your%20login%20to%20the%20Thomas%20Cook%20App.%20This%20is%20usable%20once%20and%20valid%20for%2030%20mins%20from%20the%20request.%20PLS%20DO%20NOT%20SHARE%20WITH%20ANYONE&msg_type=TEXT&userid=2000146064&auth_scheme=plain&password=PRj7Pk&v=1.1&format=text
    
   fileprivate static let SendOTPtoServerURL = "http://202.87.33.165/GatewayAPI/rest?method=SendMessage&send_to=%@ &msg=%@ is the OTP for your login to the Thomas Cook App. This is usable once and valid for 30 mins from the request. PLS DO NOT SHARE WITH ANYONE &msg_type=TEXT&userid=2000146064&auth_scheme=plain&password=PRj7Pk&v=1.1&format=text"
    
    //fileprivate static let SendOTPtoServerURL = "abc"
    
    fileprivate static let SendBranchAddresstoServerURL = "http://202.87.33.165/GatewayAPI/rest?method=SendMessage&send_to=%@&msg=%@&msg_type=TEXT&userid=2000146064&auth_scheme=plain&password=PRj7Pk&v=1.1&format=text"
    
    fileprivate static let AuthenticationUserID = "Ltapp"
    
    fileprivate static let AuthenticationPassword = "aCac5qLx8HqryH1vn0oEOg=="
//    180.179.69.73:8080
    
    /*
    fileprivate static let MOBICULE_URL = "http://220.226.201.140:8080/tc-css-device-integration-server/api/"
    
      fileprivate static let MOBICULE_FEEDBACK_URL = "http://220.226.201.140:8080/tc-css-device-integration-server/api/"
    
    fileprivate static let MOBICULE_SEND_OPTIONAL_MAIL_URL = "http://220.226.201.140:8080/tc-css-device-integration-server/sendOptionalMailTest"
    //"http://220.226.201.140:8080/tc-css-device-integration-server/sendOptionalMail"
    
    fileprivate static let MOBICULE_URL_SEND_MAIL = "http://220.226.201.140:8080/tc-css-device-integration-server/sendMailTest"
    //"http://220.226.201.140:8080/tc-css-device-integration-server/sendMail"
    
    //"http://172.98.192.15:8081/Mobicule-Platform/sendMail"
   */
    
    // Added By VJ For 180.179.69.73:8080
    
////   fileprivate static let MOBICULE_URL = "http://103.233.79.46:8080/tc-css-device-integration-server/api/"
//    fileprivate static let MOBICULE_URL = "http://180.179.69.73:8080/tc-css-device-integration-server/api/"
//
//      fileprivate static let MOBICULE_FEEDBACK_URL = "http://180.179.69.73:8080/tc-css-device-integration-server/api/"
//
//    fileprivate static let MOBICULE_SEND_OPTIONAL_MAIL_URL = "http://180.179.69.73:8080/tc-css-device-integration-server/sendOptionalMailTest"
//
//    fileprivate static let MOBICULE_URL_SEND_MAIL = "http://180.179.69.73:8080/tc-css-device-integration-server/sendMailTest"
    
    //"http://172.98.192.15:8081/Mobicule-Platform/sendMail"
    
    
    // Added By VJ For 52.172.189.249:8080 On_24_August_2020
    
    fileprivate static let MOBICULE_URL = "http://52.172.189.249:8080/tc-css-device-integration-server/api/"
    
      fileprivate static let MOBICULE_FEEDBACK_URL = "http://52.172.189.249:8080/tc-css-device-integration-server/api/"
    
    fileprivate static let MOBICULE_SEND_OPTIONAL_MAIL_URL = "http://52.172.189.249:8080/tc-css-device-integration-server/sendOptionalMailTest"
    
    fileprivate static let MOBICULE_URL_SEND_MAIL = "http://52.172.189.249:8080/tc-css-device-integration-server/sendMailTest"
    
    
    fileprivate static let  TC_CSS_PAY_NOW_URL:String = "https://uatastra.thomascook.in/tcadmin/portal/tcilcsspay/createCssAdhoc"
    
    fileprivate static let Mobicule_Authorization = "Basic dG1faW5fc3V0aDp0Y2djQDEyMw=="
    
    
    
    //MARK: - UAT Pay U Money
    //SOTC
    fileprivate static let PAYU_SUCCESS_URL:String = "https://guarded-atoll-92892.herokuapp.com/"//"https://www.payumoney.com/mobileapp/payumoney/success.php"
    fileprivate static let PAYU_FAILUER_URL:String = "https://guarded-atoll-92892.herokuapp.com/"//"https://www.payumoney.com/mobileapp/payumoney/failure.php"
    fileprivate static let PAYU_HASH_CALCULATION_URL = "https://test.payumoney.com/payment/op/v1/calculateHashForTest"
    
    fileprivate static let SALT = "salt"
    
    fileprivate static let KEY:String = "gtKFFx"//"40747T"
    
    fileprivate static let merchantid = "396132"
    
   // fileprivate static let ENVIRONMENT:String = ENVIRONMENT_TEST //PUMEnvironment.test;
    fileprivate static let FIRST_NAME = "parshwanath chougule"//txtFldName.text;
    fileprivate static let LOGO_URL = ""; //Merchant logo_url
    fileprivate static let PRODUCT_INFO = "travel";
    fileprivate static let EMAIL = "parshwanath.chougule@mobicuel.com"//txtFldEmail.text;  //user email
    fileprivate static let PHONE = "7588265960"; //user phone
    
    
    //-------------- TC_CSS_PAYMENT_AUTH -----------------
    
    fileprivate static let TC_CSS_PAYMENT_AUTH_USERNAME:String = "delson"
    fileprivate static let TC_CSS_PAYMENT_AUTH_PASS:String = "delson@1"

    
    //MARK: - UAT Visa Documents
    
    //SOTC
    fileprivate static let VISA_REQUIREMENT_URL = "http://172.98.192.15:8081/sotcconsumerapp_images/"
    //SOTC
    fileprivate static let VISA_REQUIREMENTS_USA_CANADA = VISA_REQUIREMENT_URL + "USA_&_CANADA_VISA_REQUIREMENTS.pdf"
    //SOTC
    fileprivate static let VISA_REQUIREMENTS_EXO = VISA_REQUIREMENT_URL + "FAREAST_&_EXOTIC_VISA_REQUIREMENTS.pdf"
    //SOTC
    fileprivate static let VISA_REQUIREMENTS_FAR = VISA_REQUIREMENT_URL + "FAREAST_&_EXOTIC_VISA_REQUIREMENTS.pdf"
    //SOTC
    fileprivate static let VISA_REQUIREMENTS_EUR = VISA_REQUIREMENT_URL + "EUROPE_VISA_DOCUMENTATION_REQUIRED.pdf"
    
    fileprivate static let LETTER_FORMAT_URL = "https://uatastra.thomascook.in/images/MyAccount/css/Letter-Formats.pdf"
    
    //"https://uatastra.thomascook.in/images/MyAccount/css/Formats.PDF"
    
    static let PERFORMA_INVOICE_URL = "http://uat2.thomascook.in/soreport/frameset?__report=ltPInvoice.rptdesign&__format=pdf&BookingFile="
    
   
    
    
    //MARK: - UAT Handy_TIPS Documents
    static let Handy_TIPS_URL = "https://uatastra.thomascook.in/images/MyAccount/css/"
    
    fileprivate static let Handy_TIPS_USA = Handy_TIPS_URL + "USA_Handy_Tips.pdf"
    fileprivate static let Handy_TIPS_EXO = Handy_TIPS_URL + "EXO_Handy_Tips.pdf"
    fileprivate static let Handy_TIPS_FAR = Handy_TIPS_URL + "FAR_Handy_Tips.pdf"
    fileprivate static let Handy_TIPS_EUR = Handy_TIPS_URL + "EUR_Handy_Tips.pdf"
    fileprivate static let Handy_TIPS_AUS = Handy_TIPS_URL + "AUS_Handy_Tips.pdf"
    fileprivate static let Handy_TIPS_RSA = Handy_TIPS_URL + "RSA_Handy_Tips.pdf"
    
    //MARK: - UAT HE_KIT Documents
    fileprivate static let HE_KIT_URL = "https://uatastra.thomascook.in/images/MyAccount/css/"
    
    fileprivate static let HE_KIT_USA = HE_KIT_URL + "USA_HE_Kit.jpg"
    fileprivate static let HE_KIT_EXO = HE_KIT_URL + "EXO_HE_Kit.jpg"
    fileprivate static let HE_KIT_FAR = HE_KIT_URL + "FAR_HE_Kit.jpg"
    fileprivate static let HE_KIT_EUR = HE_KIT_URL + "TravelGear.jpg"
     //fileprivate static let HE_KIT_EUR = HE_KIT_URL + "EUR_HE_Kit.pdf"
    fileprivate static let HE_KIT_AUS = HE_KIT_URL + "AUS_HE_Kit.jpg"
    fileprivate static let HE_KIT_RSA = HE_KIT_URL + "RSA_HE_Kit.jpg"
    
    //MARK: - UAT Holiday URL
    //SOTC
     fileprivate static let INTERNATIONAL_HOLIDAYS_URL = "https://www.thomascook.in/international-tour-packages"
    //SOTC
    fileprivate static let INDIAN_HOLIDAYS_URL = "https://www.thomascook.in/india-tour-packages"
    
     fileprivate static let SSLCertificates = ["lt_sotc_in","resources_thomascook_in","services_thomascook_in","services_uatastra_thomascook_in","thomascookindia_custhelp_com","thomascookindia_tst1_custhelp_com ","uatastra_thomascook_in ","www_thomascook_in","www_thomascooktravelcards_com"]
    
    //MARK: - UAT Phase 2
    
    fileprivate static let ASTRA_SERVICES_URL = "https://services-uatastra.thomascook.in/"
    
    fileprivate static let ASTRA_RESOURCE_URL = "https://resources-uatastra.thomascook.in/"
    
    fileprivate static let ASTRA_REQUEST_TO_CALL_URL = "http://framedhruvlive.thomascook.in/TCLeadWrapper/TCLeadWrapper.asmx/CreateGeneralEnquiry" // live url for both live and UAT
    
    fileprivate static let ASTRA_SHARE_URL = "https://uatastra.thomascook.in/holidays/" // live url for both live and UAT
    
    fileprivate static let PG_URL = "https://uatastra.thomascook.in/paymentGateway.html?"
    
    fileprivate static let Prebooking_Last_Level_Banner_URL = "https://blog.thomascook.in/wp-json/wp/v2/posts"
}

//MARK: - ------------ LIVE URL Constant ------------------

struct LiveURLConstant
{
    
    fileprivate static let CheckMobileResponseTag = "CHECK_MOBILE_RESPONSE"
    
    //http://b2b.thomascook.in/tcportal1.0/services/LtAstraServices?wsdl
    fileprivate static let CheckMobileURL = "http://lt.thomascook.in/ltweb/services/LtAstraServices?wsdl"
    
    fileprivate static let GetOTPURL = "CHECK_MOBILE_RESPONSE"
    //SOTC
    fileprivate static let ValidateOTPURL:String = "http://otp.xapp.spocbridge.com/api/Token/ValidateToken?ApplicationID=6&UserID=rajesh.shetty&Mobile=%@&Token=%@"
    
    //"http://otp.xapp.spocbridge.com/api/Token/ValidateToken?ApplicationID=4&UserID=unnikrishnan&Mobile=%@&Token=%@"
    
    
    //SOTC
    fileprivate static let SendOTPURL = "http://otp.xapp.spocbridge.com/api/Token/GenerateToken?ApplicationID=6&UserID=rajesh.shetty&Mobile=" //"http://otp.xapp.spocbridge.com/api/Token/GenerateToken?ApplicationID=4&UserID=unnikrishnan&Mobile="
    
    
    
    fileprivate static let AuthenticationUserID = "Ltapp_Prod"
    
    fileprivate static let AuthenticationPassword = "F4sAn8NASqkcbvbo7yhZLw=="
    
    fileprivate static let MOBICULE_URL = "http://103.233.79.46:8080/tc-css-device-integration-server/api/"
    
    fileprivate static let MOBICULE_FEEDBACK_URL = "http://103.233.79.46:8080/tc-css-device-integration-server/api/"
    
    fileprivate static let MOBICULE_URL_SEND_MAIL = "http://103.233.79.46:8080/tc-css-device-integration-server/sendMail"
    
    fileprivate static let MOBICULE_SEND_OPTIONAL_MAIL_URL = "http://103.233.79.46:8080/tc-css-device-integration-server/sendOptionalMail"
    
    //  static let Mobicule_Authorization = "F4sAn8NASqkcbvbo7yhZLw=="
    
    //MARK: - LIVE Pay U Money
    //SOTC
    fileprivate static let PAYU_SUCCESS_URL:String = "https://guarded-atoll-92892.herokuapp.com/" //"https://www.payumoney.com/mobileapp/payumoney/success.php"
    fileprivate static let PAYU_FAILUER_URL:String = "https://guarded-atoll-92892.herokuapp.com/"//"https://www.payumoney.com/mobileapp/payumoney/failure.php"
    
    fileprivate static let SALT:String = "hZSUBFuI" //"qrlJMl6wtS" //"hZSUBFuI"
    
    fileprivate static let KEY:String = "ODqJUP" //"vTiZoDyO"
    
    fileprivate static let merchantid = "5963856"//"ODqJUP"
    
    //fileprivate static let ENVIRONMENT:String = ENVIRONMENT_PRODUCTION//PUMEnvironment.production;
    fileprivate static let FIRST_NAME = ""//txtFldName.text;
    fileprivate static let LOGO_URL = ""; //Merchant logo_url
    fileprivate static let PRODUCT_INFO = "";
    fileprivate static let EMAIL = ""//txtFldEmail.text;  //user email
    fileprivate static let PHONE = ""; //user phone
    
    
    //-------------- TC_CSS_PAYMENT_AUTH -----------------
    
     fileprivate static let  TC_CSS_PAY_NOW_URL:String = "https://services.thomascook.in/tcadmin/portal/tcilcsspay/createCssAdhoc"
    
    fileprivate static let TC_CSS_PAYMENT_AUTH_USERNAME:String = "delson"
    fileprivate static let TC_CSS_PAYMENT_AUTH_PASS:String = "delson@1"
    
    //MARK: - LIVE Visa Documents
    
    fileprivate static let PERFORMA_INVOICE_URL = "https://lt.thomascook.in/soreport/frameset?__report=ltPInvoice.rptdesign&__format=pdf&BookingFile="
    
    
    fileprivate static let LETTER_FORMAT_URL = "https://www.thomascook.in/images/MyAccount/css/Letter-Formats.pdf"
    //SOTC
    fileprivate static let VISA_REQUIREMENT_URL = "http://172.98.192.15:8081/sotcconsumerapp_images/"
    //SOTC
    fileprivate static let VISA_REQUIREMENTS_USA_CANADA = VISA_REQUIREMENT_URL + "USA_&_CANADA_VISA_REQUIREMENTS.pdf"
    //SOTC
    fileprivate static let VISA_REQUIREMENTS_EXO = VISA_REQUIREMENT_URL + "FAREAST_&_EXOTIC_VISA_REQUIREMENTS.pdf"
    //SOTC
    fileprivate static let VISA_REQUIREMENTS_FAR = VISA_REQUIREMENT_URL + "FAREAST_&_EXOTIC_VISA_REQUIREMENTS.pdf"
    //SOTC
    fileprivate static let VISA_REQUIREMENTS_EUR = VISA_REQUIREMENT_URL + "EUROPE_VISA_DOCUMENTATION_REQUIRED.pdf"
    
    
    //MARK: - LIVE Handy_TIPS Documents
    fileprivate static let Handy_TIPS_URL = "https://www.thomascook.in/images/MyAccount/css/"
    
    fileprivate static let Handy_TIPS_USA = Handy_TIPS_URL + "USA_Handy_Tips.pdf"
    fileprivate static let Handy_TIPS_EXO = Handy_TIPS_URL + "EXO_Handy_Tips.pdf"
    fileprivate static let Handy_TIPS_FAR = Handy_TIPS_URL + "FAR_Handy_Tips.pdf"
    fileprivate static let Handy_TIPS_EUR = Handy_TIPS_URL + "EUR_Handy_Tips.pdf"
    fileprivate static let Handy_TIPS_AUS = Handy_TIPS_URL + "AUS_Handy_Tips.pdf"
    fileprivate static let Handy_TIPS_RSA = Handy_TIPS_URL + "RSA_Handy_Tips.pdf"
    
    //MARK: - LIVE HE_KIT Documents
    
    fileprivate static let HE_KIT_URL = "https://www.thomascook.in/images/MyAccount/css/"
    
    fileprivate static let HE_KIT_USA = HE_KIT_URL + "USA_HE_Kit.jpg"
    fileprivate static let HE_KIT_EXO = HE_KIT_URL + "EXO_HE_Kit.jpg"
    fileprivate static let HE_KIT_FAR = HE_KIT_URL + "FAR_HE_Kit.jpg"
    fileprivate static let HE_KIT_EUR = HE_KIT_URL + "TravelGear.jpg"
    fileprivate static let HE_KIT_AUS = HE_KIT_URL + "AUS_HE_Kit.jpg"
    fileprivate static let HE_KIT_RSA = HE_KIT_URL + "RSA_HE_Kit.jpg"
    
    //MARK: - LIVE Holiday URL
    //SOTC
    fileprivate static let INTERNATIONAL_HOLIDAYS_URL = "https://www.thomascook.in/international-tour-packages"
    //SOTC
    fileprivate static let INDIAN_HOLIDAYS_URL = "https://www.thomascook.in/india-tour-packages"
   
    fileprivate static let SSLCertificates = ["lt_sotc_in","resources_thomascook_in","services_thomascook_in","services_uatastra_thomascook_in","thomascookindia_custhelp_com","thomascookindia_tst1_custhelp_com","uatastra_thomascook_in","www_thomascook_in","www_thomascooktravelcards_com"]
    
    //MARK: - SOTC PHASE 2
    
    fileprivate static let ASTRA_RESOURCE_URL = "https://resources.thomascook.in/"
    
    fileprivate static let ASTRA_SERVICES_URL = "https://services.thomascook.in/"
    
    
    fileprivate static let ASTRA_REQUEST_TO_CALL_URL = "http://framedhruvlive.thomascook.in/TCLeadWrapper/TCLeadWrapper.asmx/CreateGeneralEnquiry" // live url for both live and UAT
    
    fileprivate static let ASTRA_SHARE_URL = "https://services.thomascook.in/holidays/" // live url for both live and UAT
    
    fileprivate static let PG_URL = "https://www.thomascook.in/paymentGateway.html?"
    
    fileprivate static let Prebooking_Last_Level_Banner_URL = "https://blog.thomascook.in/wp-json/wp/v2/posts"
}


struct GetBFNListConstant {
    
    static let GetBFNListResponse = "ns1:getBFNListResponse"
    static let GetBFNListReturn = "getBFNListReturn"
    static let BookingFileListResponse = "BOOKING_FILE_LIST_RESPONSE"
    static let  BookingList = "BOOKING_LIST"
    static let  BFNumber = "BOOKING_FILE_NO"
    static let  BFName = "BOOKING_FILE_NAME"
    static let  DepartureDate = "DEPARTURE_DATE"
    static let ArrivalDate = "ARRIVAL_DATE"
    static let NumberOfDays = "NO_OF_DAYS"
    static let  ProductITINCode = "PROD_ITIN_CODE"
    static let  TourName = "TOUR_NAME"
    static let  TourCode = "TOUR_CODE"
    
}

struct CommonTagContant {
    
    static let SoapEnvelope = "soapenv:Envelope"
    static let SoapBody = "soapenv:Body"
    static let SoapHeader = "soapenv:Header"
    
    
    
    static let ResponseCodeTag = "RESP_CODE"
    static let ResponseDescriptionTag = "RESP_DESC"
    static let  ResponseSuccessCode = "0"
    static let  ResponseAvailable = "AVAILABLE"
    
}


struct JsonConstant
{
    
    static let IOS_CLIENT = "ios"
    
}

struct SoapActionConstant {
    
    
    static let GetBFNListAction = "getBFNList"
    static let GetBFNDetailAction = "getBookingDetails"
    static let GetBFNOptionalPackageAction = "getBFNOptional"
    
    static let MobileCheckAction = "getBFNList"
    static let SendOTPAction = "getBookingDetails"
    static let SendOTPVerificationAction = "getBFNOptional"
    
    
}


struct CoreDataConstant {
    
    static let dataMergePolicy = NSOverwriteMergePolicy
    
    
}

struct NetworkValidationMessages {
    
    static let InternetNotConnected:(statusCode: Int, status: Bool , message:String) = (statusCode: 200, status: false , message:"Please Check Internet Connection")
    
    static let ValidMobileNumber:(statusCode: Int, status: Bool , message:String) = (statusCode: 200, status: true , message:"Valid Mobile Number")
    
    static let ValidOTP:(statusCode: Int, status: Bool , message:String) = (statusCode: 200, status: true , message:"OTP sent to ")
    
    static let NotValidMobileNumber:(statusCode: Int, status: Bool , message:String) = (statusCode: 200, status: false , message:"Please enter valid mobile number")
    
    static let NotValidOTP:(statusCode: Int, status: Bool , message:String) = (statusCode: 200, status: false , message:"Please enter valid OTP")
    
    
    
}

struct OperationValidationMessage {
    
    var statusCode:Int = 200
    var status:Bool = false
    var message:String = ""
    var data:String = ""
    
}

//MARK: - ------------ Module Title Constant ------------------
public struct Title
{
    public static let MY_BOOKING = "My Bookings"
    
    public static let PAYMENT = "Payment"
    
    public static let VISA = "Visa"
    
    public static let TICKETS = "Air Tickets"
    
    public static let ITINERARY = "Itinerary"
    
    public static let INSURANCE = "Insurance"
    
    public static let OPTIONAL_TOUR = "Optional Tour"
    
    public static let DEVIATION = "Deviation"
    
    public static let HANDY_TIPS = "Handy Tips"
    
    public static let ROOMS = "Rooms"
    
    public static let HOLIDAY_KIT = "Travel Gear"
    
    public static let FEEDBACK = "Feedback"
    
    public static let FINAL_TOUR_DETAILS = "Other Tour Documents"//"Final Tour Details"
    
    public static let FINAL_VOUCHER_DETAILS = "Final Voucher"
    
    public static let BRIEFING_SHEET_DETAILS = "Briefing Sheet"
    
    public static let VIEW_PDF = "View PDF"
    
    public static let PROFILE = "Profile"
    
    public static let PYMENT_CONFIRMATION = "Payment Confirmation Details"
}

struct AlertMessage{
    
    static let CHECK_NET_CONNECTION = "Please check your internet connection"
    
    static let VERSION_UPGRADE_MSG = "A new version is available. Please upgrade your application"
    
    
    static let NO_DATA = "No data found !"
    static let NO_DATA_TITLE = "No data"
    static let DOCUMENT_DOWNLOAD_FAIL = "Unable to download document ,please try again"
    
    static let AMOUNT_LESS_VALIDATION = "Amount should be greater than zero"
    
     static let OPTIONAL_EMAIL_SEND_VALIDATION = "Select at least single optional package"
    
    static let PAYMENT_SUCCESS = "Payment done successfully"
    
    static let PAYMENT_FAILED = "Payment failed, please try again "
    static let PAYMENT_CANCELLED = "Payment cancelled"
    
     static let TRANSACTION_EXPIRED = "Payment transactin expired, please try again"
    
    static let NO_DOCUMENT = "No document available"
    
    static let TITLE_OK = "Ok"
    
    static let TITLE_Alert = "Alert"
    
    static let TITLE_REMARKS = "Remarks"
    
    static let NO_BOOKING_AVAILABEL = "No booking available"
    
    static let TITLE_DONE = "Done"
    
    static let TITLE_CANCEL = "Cancel"
    
    static let TITLE_CREATE_DEVIATION = "Create Deviation"
    
    static let TITLE_TRY_AGAIN = "Try Again"
    
    static let OPTIONAL_BOOKING_BEFORE_7_DAY = "You are not allowed to book optional"
    
    static let ROOTED_DEVICE_MSG = "Cannot proceed with rooted device"

    
      static let PAYMENT_RECIPT_GENERATED = "Payment receipt generated"
    static let  PAYMENT_RECIPT_GENERATED_FAIL = "Payment done successfully but unable to generate payment receipt"
    
    struct Itineary{
        
        static let FILE_ALREDAY_DOWNLOADED = "File already downloaded"
        
        static let FILE_DOWNLOADED = "File downloaded"
        
        static let NO_DATA = "No Itinerary data available"
    }
    
    struct Deviation{
        
        static let PAST_TOUR_DEVIATION = "You are not allowed to create deviation for past date"
        static let CANCELLED_TOUR_DEVIATION = "You are not allowed to create deviation for cancelled booking"
        static let BEFORE_30_DAY = "You are not allowed to create deviation request"
        static let ALREADY_DEVIATION_CREATED = "Your ticket is booked. You will be unable to raise a new Deviation Request"
        
        static let SELECT_PASSENGER = "Please select passenger"
        
        static let SELECT_DEVIATION = "Please select deviation"
        
        static let SELECT_DEVIATION_DATE = "Please select deviation date"
        
        static let DEVIATION_NOTIFICATION_MSG = "We are in receipt of your Deviation Request. Our customer service executive will confirm the status of your request shortly"
        
        static let CREATE_DEVIATION = "Are you sure you want to create deviation?"
        
    }
    
    struct Tickets{
        
        static let COMPLETE_PAYMENT = "Kindly complete your final payments to view Tickets"
        
        static let TICKET_YET_TO_ISSUED = "Your ticket is yet to be issued"
        
    }
    
    struct Insurance{
        
        
        static let INSURANCE_YET_TO_ISSUED = "Your insurance is yet to issued"
        
    }
    
    struct FinalTourDetails {
        
        static let COMPLETE_PAYMENT =  "Kindly complete your final payments to view Final Tour details"
        static let NO_DOCUMNET_DATA = "Documents yet to be uploaded"
        
    }
    
    struct Visa {
        
        static let COMPLETE_INR_PAYMENT = "Kindly complete your full INR payments to view Appointment Letter"
    }
    
    struct Payment
    {
        static let AMOUNT_GREATED_VALIDATION = "Enter amount exceeding the limit"
         static let ROE_DATA_NOT_FOUND = "We could not fetch the ROE rates. Please check your internet connection and try again"//"ROE data not avalilable"
        static let ROE_DATA_UPDATED = "ROE rate fetched successfully"
        static let GET_ROE_DATA_NOT_FOUND = ""//"To get ROE data"
        static let NO_RECIPT_DETAILS_FOUND = "No Receipt details found"
        
    }
    
    struct He_Kit
    {
        static let ALLREADY_PURCHASED = "Already Purchased"
        
        static let PURCHASED = "To Purchase This Holiday Essential Kit. Call Now."
    }
    
    struct Profile {
        
        static let REMARK_VALIDATION = "Please add remark"
    }
    
    struct Optional
    {
        static let NO_DATA = "No optional data available"
    }
    
}


struct HelpEmailIDForRegions {
    
    static let Gujarat:(region: String, emailId: String  ) = (region: "gujarat", emailId: "regionalmanagergujarat@Sotc.in")
    static let South:(region: String, emailId: String) = (region: "south", emailId: "regionalmanagersouth@Sotc.in")
    static let West:(region: String, emailId: String) = (region: "west", emailId: "regionalmanagerwest@Sotc.in")
    static let North:(region: String, emailId: String) = (region: "north", emailId: "regionalmanagernorth@Sotc.in")
    static let East:(region: String, emailId: String) = (region: "east", emailId: "regionalmanagereast@Sotc.in")
}


public enum TourStatus: String {
    
    case UpComing = "upcoming"
    case Past     = "past"
    case Current     = "current"
    case Cancelled     = "cancelled"
    
}

public enum NotificationType: String {
    
    case Tickets = "Air Tickets"
    case Profile     = "Profile"
    case Deviation     = "Deviation"
    case FinalTourDetail = "Tour Details"
    case TourConfirmationVoucher     = "Tour Confirmation Voucher"
    case Feedback     = "Feedback"
    case VisaDocuments     = "Visa Documents"
    case AppointmentLetter = "Appointment Letter"
    case Visas     = "Visas"
     case Visa     = "Visa"
    case Payments  = "Payments"
    case ProfileLocale = "ProfileLocaleNotification"
    case DeviationLocale = "DeviationLocaleNotification"
    
}

struct UserDefauldConstant {
    
    static let userMobileNumber = "mobicleNumber"
    static let isWelcomeScreenDisplayed = "isWelcomeScreenDisplayed"
    static let isUserLoggedIn = "isCSSUserLoggedIn"
    static let defaultUserMobileNumber = " 9821361646" //"8390131180" //"9999999999"
    static let isOTPByPass = false
    static let isRegisterForPushNotification = "isRegisterForCSSPushNotification"
    static let isSubscribeForPushNotification = "isCSSSubscribeForPushNotification"
    static let  pushNotificationDeviceToken = "APNSCSSDeviceToken"
    static let isSubscribeForPushNotificationOnSOTCServer = "isSubscribeForCSSPushNotificationOnSOTCServer"
    
    
    
}

struct OptionalBookingConstant {
    
    static let PAX_NUMBER = "PAX_NO"
    static let OPTIONAL_CODE = "OPTIONAL_CODE"
    static let AMOUNT = "AMOUNT"
    static let REMARKS = "REMARKS"
  
    
}
/*
struct Constant
{
    
    static let AppThemeColor:UIColor = AppUtility.hexStringToUIColor(hex: "18abf1")
    
    static let PAX_TYPE_ADULT = "ADULT"
    
    static let PAX_TYPE_CHILD = "CHILD"
    
    static let PAX_TYPE_INFANT = "INFANT"
    
    static let BOOKED = "BOOKED"
    
    static let DUMMY_VALUE = "test"
    
    static let PAYU_RESPONSE_KEY_AMOUNT = "amount"
    
    static let PAYU_RESPONSE_KEY_PAYMENTID = "mihpayid"
    
    static let PAYU_RESPONSE_KEY_TRANSACTION_ID = "txnid"
    static let PAYU_RESPONSE_KEY_ADDED_ON = "addedon"
    static let PAYU_RESPONSE_KEY_PROD_ON = "productinfo"
    static let PAYU_RESPONSE_KEY_EMAIL_ID = "email"
    static let PAYU_RESPONSE_KEY_PHONE_NUM = "phone"
    static let PAYU_RESPONSE_KEY_NAME = "firstname"
    
    
    static let USA_CONTRY_CODE_USA = "USA"
    static let USA_CONTRY_CODE_HSDEMO = "HSDEMO"
    static let USA_CONTRY_CODE_SOUTHAMERICA = "SOUTHAMERICA"
  
    static let USA_CONTRY_CODE_USAFIT = "USAFIT"
    static let USA_CONTRY_CODE_USAHS = "USAHS"
    static let USA_CONTRY_CODE_USAHSA = "USAHSA"
    
    
    static let EXO_CONTRY_CODE_EXO = "EXO"
    
    static let FAR_CONTRY_CODE_HSASIA = "HSASIA"
    static let FAR_CONTRY_CODE_AHSASIA = "AHSASIA"
    static let FAR_CONTRY_CODE_ASIA = "ASIA"
    static let FAR_CONTRY_CODE_ASIAFT = "ASIAFT"
    static let FAR_CONTRY_CODE_ASIAFIT = "ASIAFIT"
    static let FAR_CONTRY_CODE_ASIAHS = "ASIAHS"
    static let FAR_CONTRY_CODE_H22015 = "H22015"
    static let FAR_CONTRY_CODE_HS = "HS"
    static let FAR_CONTRY_CODE_IND = "IND"
    static let FAR_CONTRY_CODE_INDHS = "INDHS"
    static let FAR_CONTRY_CODE_INDIAFIT = "INDIAFIT"
    static let FAR_CONTRY_CODE_INDIAHS = "INDIAHS"
    static let FAR_CONTRY_CODE_MAYA = "MAYA"
    static let FAR_CONTRY_CODE_MIDDLEEASTFIT = "MIDDLEEASTFIT"
    static let FAR_CONTRY_CODE_SU = "SU"
    static let FAR_CONTRY_CODE_SU1 = "SU1"
    static let FAR_CONTRY_CODE_TESTSURESH = "TESTSURESH"
    
    
    static let EUR_CONTRY_CODE_AGD = "AGD"
    static let EUR_CONTRY_CODE_EUROPE = "EUROPE"
    static let EUR_CONTRY_CODE_EUROPEFIT = "EUROPEFIT"
    static let EUR_CONTRY_CODE_EUROPEHS = "EUROPEHS"
    static let EUR_CONTRY_CODE_HSEUR = "HSEUR"
    
    
    
    static let AUS_CONTRY_CODE_ANZ = "ANZ"
    static let AUS_CONTRY_CODE_ANZHS = "ANZHS"
    
    static let RSA_CONTRY_CODE_AFRICA = "AFRICA"
    static let RSA_CONTRY_CODE_AFRICAHSA = "AFRICAHSA"
    static let RSA_CONTRY_CODE_HSAAFRICA = "HSAAFRICA"
    static let RSA_CONTRY_CODE_AFRICAFIT = "AFRICAFIT"
    
    static let UK_CONTRY_CODE = "UK"
    
    static let PAYU_BIZ_STORYBOARD = "payuBizMain"
    
    static let PAYMENT_DETAILS:[String] = ["Amount","Reference Number","Transaction ID","Transaction Date","Product Description","Booking ID","Travel Date","Name","Email ID","Mobile Number"]
    
    
    struct OptionalTour
    {
        static let LABEL_BOOKED = "Booked"
        
        static let NOT_BOOKED = "NOTBOOKED"
        
        static let CANCELLATION_REQUESTED = "CANCELLATION  REQUESTED"
        
        static let LABEL_SELECTALL = "SelectAll"
    
    }
    
    struct ItiearyDetails
    {
        static let ITIENRARY_DOC = "ITINERARYDOC"
    }
    
    struct Deviation
    {
        static let PRE_DEVIATION = "PRE_DEVIATION"
        
         static let POST_DEVIATION = "POST_DEVIATION"
        
        static let DEVIATION_DROP_DOWN = "DeviationDropDown"
  
    
    }
    
    struct Rooms
    {
        static let CHILD_WITH_BED = "CWB"
        
        static let CHILD_WITH_BED_TEXT = "(Child with bed)"
        
        static let JC = "JC"
        
        static let CHILD_WITH_NO_BED = "CNB"
        
        static let CHILD_WITH_NO_BED_TEXT = "(Child without bed)"
        
        static let ROOM_TYPE_SINGLE = "SR"
        
        static let ROOM_TYPE_DOUBLE = "DR"
        
        static let ROOM_TYPE_TRIPLE = "TR"
        
        static let SINGLE_SHARING_TEXT = "Single Sharing"
        
        static let DOUBLE_SHARING_TEXT = "Double Sharing"
        
        static let TRIPLE_SHARING_TEXT = "Triple Sharing"
        
    }
    
    struct Visa
    {
        static let LETTER_FORMAT_DOC = "letterformat"
        
        static let VISA_COVERING_LETTER_FORMAT_DOC = "visacoveringletter"
        
         static let TC_CSS_NO_VISA_SERVICES = "no visa service"
        
        static let TC_CSS_DOC_PENDING = "docs pending"
        
        static let DOCUMENT_RECIVED_STATUS = "document received"
         static let TC_CSS_DOCUMENT_RECIVED_STATUS_1 = "qc pending"
         static let TC_CSS_DOCUMENT_RECIVED_STATUS_2 = "qc done docs pending"
        static let TC_CSS_DOCUMENT_RECIVED_STATUS_3 = "docs received"
        
        static let DOCUMENT_VERIFIED_STATUS = "document verified"
         static let TC_CSS_DOCUMENT_VERIFIED_STATUS = "qc ok"
        
        static let APPONTMENT_LETTER_STATUS = "appointment letter"
        
        static let SELF_ARRANGED_STATUS = "SELF ARRANGED"
        
        static let HOLDING_VISA_STATUS = "HOLDING VISA"
        
        static let VISA_ACTIONED_STATUS = "VISA ACTIONED"
        
        static let VISA_DONE_STATUS = "DONE"
        static let TC_CSS_VISA_DONE_STATUS = "VISA GRANTED"
        static let TC_CSS_VISA_DONE_STATUS2 = "VISA QC DONE"
        
        
        static let VISA_REJECTED_STATUS = "REJECTED"
        static let TC_CSS_VISA_REJECTED_STATUS_1 = "SUBMISSION FAILED"
        static let TC_CSS_VISA_REJECTED_STATUS_2 = "VISA REJECTED"
       
        
        static let VISA_IN_PROCESS_STATUS = "IN PROCESS"
        
        static let VISA_REJECTED_R_STATUS = "REJECTED-R"
        
        static let VISA_VOID_STATUS = "VOID"
        
        static let VISA_READY_FOR_SUBMIT_STATUS = "READY FOR SUBMISSION"
        
        static let TC_CSS_VISA_APPOINTMENT_SCHEDULED_1 = "APPOINTMENT DONE"
        static let TC_CSS_VISA_APPOINTMENT_SCHEDULED_2 = "SUBMITTED"
        static let TC_CSS_VISA_APPOINTMENT_SCHEDULED_3 = "INTERVIEW CALL"
        static let TC_CSS_VISA_APPOINTMENT_SCHEDULED_4 = "ADDITIONAL DOCS ASKED"
        
        
        static let DOCUMENTS_PENDING_STATUS = "Documents pending"
        
         static let DOCUMENTS_NOT_PROCESSED_BY_TCIL = "Not to be processed by TCIL"
    }
    
    struct Profile {
        
        static let ADRESS_TEXT = "Address"
        
        static let EMAIL_ID_TEXT = "Email ID"
        
        static let CONTACT_NUMBER_TEXT = "Contact Number"
        
        static let AADHAR_TEXT = "Aadhar"
        
         static let PASSPORT_TEXT = "Passport"
        
    }
 
}
*/
struct NotificationConstant {
    
    static let DATA = "data"
    static let MESSAGE = "message"
    static let TITLE = "title"
    static let IMAGE_URL = "imageUrl"
    static let NOTIFICATION_TYPE = "notificationType"
    static let REDIRECT = "redirect"
    static let BFN = "bfn"
    static let SUBTITLE = "subTitle"
    static let CREATED_ON = "createdOn"
    
}

struct RegularExpressions {
     
    static let Numberic:String = "[0-9]"
    static let AlphaNumberic:String = "[a-zA-Z0-9]"
    static let Alphabets:String = "[a-zA-Z]"
    static let SpecialCharactersAllowed:String = "[!@#$%ˆ&*()_+=-`;:'<,>.?/|]"
  
    
 }

struct Constant
{
    static let AppThemeColor:UIColor = AppUtility.hexStringToUIColor(hex: "18abf1")
    
    static let PAX_TYPE_ADULT = "ADULT"
    
    static let PAX_TYPE_CHILD = "CHILD"
    
    static let BOOKED = "BOOKED"
    
    static let DUMMY_VALUE = "test"
    
    static let PAYU_RESPONSE_KEY_AMOUNT = "amount"
    
    static let PAYU_RESPONSE_KEY_PAYMENTID = "mihpayid"
    
    static let PAYU_RESPONSE_KEY_TRANSACTION_ID = "txnid"
    static let PAYU_RESPONSE_KEY_ADDED_ON = "addedon"
    static let PAYU_RESPONSE_KEY_PROD_ON = "productinfo"
    static let PAYU_RESPONSE_KEY_EMAIL_ID = "email"
    static let PAYU_RESPONSE_KEY_PHONE_NUM = "phone"
    static let PAYU_RESPONSE_KEY_NAME = "firstname"
    
    
    static let USA_CONTRY_CODE_USA = "USA"
      static let USA_CONTRY_CODE_HSDEMO = "HSDEMO"
      static let USA_CONTRY_CODE_SOUTHAMERICA = "SOUTHAMERICA"
    
      static let USA_CONTRY_CODE_USAFIT = "USAFIT"
      static let USA_CONTRY_CODE_USAHS = "USAHS"
      static let USA_CONTRY_CODE_USAHSA = "USAHSA"
      
      
      static let EXO_CONTRY_CODE_EXO = "EXO"
      
      static let FAR_CONTRY_CODE_HSASIA = "HSASIA"
      static let FAR_CONTRY_CODE_AHSASIA = "AHSASIA"
      static let FAR_CONTRY_CODE_ASIA = "ASIA"
      static let FAR_CONTRY_CODE_ASIAFT = "ASIAFT"
      static let FAR_CONTRY_CODE_ASIAFIT = "ASIAFIT"
      static let FAR_CONTRY_CODE_ASIAHS = "ASIAHS"
      static let FAR_CONTRY_CODE_H22015 = "H22015"
      static let FAR_CONTRY_CODE_HS = "HS"
      static let FAR_CONTRY_CODE_IND = "IND"
      static let FAR_CONTRY_CODE_INDHS = "INDHS"
      static let FAR_CONTRY_CODE_INDIAFIT = "INDIAFIT"
      static let FAR_CONTRY_CODE_INDIAHS = "INDIAHS"
      static let FAR_CONTRY_CODE_MAYA = "MAYA"
      static let FAR_CONTRY_CODE_MIDDLEEASTFIT = "MIDDLEEASTFIT"
      static let FAR_CONTRY_CODE_SU = "SU"
      static let FAR_CONTRY_CODE_SU1 = "SU1"
      static let FAR_CONTRY_CODE_TESTSURESH = "TESTSURESH"
      
      
      static let EUR_CONTRY_CODE_AGD = "AGD"
      static let EUR_CONTRY_CODE_EUROPE = "EUROPE"
      static let EUR_CONTRY_CODE_EUROPEFIT = "EUROPEFIT"
      static let EUR_CONTRY_CODE_EUROPEHS = "EUROPEHS"
      static let EUR_CONTRY_CODE_HSEUR = "HSEUR"
      
      
      
      static let AUS_CONTRY_CODE_ANZ = "ANZ"
      static let AUS_CONTRY_CODE_ANZHS = "ANZHS"
      
      static let RSA_CONTRY_CODE_AFRICA = "AFRICA"
      static let RSA_CONTRY_CODE_AFRICAHSA = "AFRICAHSA"
      static let RSA_CONTRY_CODE_HSAAFRICA = "HSAAFRICA"
      static let RSA_CONTRY_CODE_AFRICAFIT = "AFRICAFIT"
      
      static let UK_CONTRY_CODE = "UK"
      
      static let PAYU_BIZ_STORYBOARD = "payuBizMain"
      
      static let PAYMENT_DETAILS:[String] = ["Amount","Reference Number","Transaction ID","Transaction Date","Product Description","Booking ID","Travel Date","Name","Email ID","Mobile Number"]
      
      
    
    
    struct Paymnet {
        static let PAYU_RESPONSE = "payUresponse"
        static let OS_VERSION = "osversion"
        static let DEVICE_MODEL = "model"
    }
    
    struct OptionalTour
    {
        static let LABEL_BOOKED = "Booked"
        
        static let NOT_BOOKED = "NOTBOOKED"
        
        static let CANCELLATION_REQUESTED = "CANCELLATION  REQUESTED"
        
        static let LABEL_SELECTALL = "SelectAll"
    
    }
    
    struct ItiearyDetails
    {
        static let ITIENRARY_DOC = "ITINERARYDOC"
    }
    
    
    struct Deviation
    {
        static let PRE_DEVIATION = "PRE_DEVIATION"
        
        static let POST_DEVIATION = "POST_DEVIATION"
        
        static let DEVIATION_DROP_DOWN = "DeviationDropDown"
        
        
    }
    
    struct Rooms
    {
        static let CHILD_WITH_BED = "CWB"
        
        static let CHILD_WITH_BED_TEXT = "(Child with bed)"
        
        static let JC = "JC"
        
        static let CHILD_WITH_NO_BED = "CNB"
        
        static let CHILD_WITH_NO_BED_TEXT = "(Child without bed)"
        
        static let ROOM_TYPE_SINGLE = "SR"
        
        static let ROOM_TYPE_DOUBLE = "DR"
        
        static let ROOM_TYPE_TRIPLE = "TR"
        
        static let SINGLE_SHARING_TEXT = "Single Sharing"
        
        static let DOUBLE_SHARING_TEXT = "Double Sharing"
        
        static let TRIPLE_SHARING_TEXT = "Triple Sharing"
        
    }
    
    struct Visa
    {
        static let LETTER_FORMAT_DOC = "letterformat"
        
        static let VISA_COVERING_LETTER_FORMAT_DOC = "visacoveringletter"
        
         static let TC_CSS_NO_VISA_SERVICES = "no visa service"
        
        static let TC_CSS_DOC_PENDING = "docs pending"
        
        static let DOCUMENT_RECIVED_STATUS = "document received"
         static let TC_CSS_DOCUMENT_RECIVED_STATUS_1 = "qc pending"
         static let TC_CSS_DOCUMENT_RECIVED_STATUS_2 = "qc done docs pending"
        static let TC_CSS_DOCUMENT_RECIVED_STATUS_3 = "docs received"
        
        static let DOCUMENT_VERIFIED_STATUS = "document verified"
         static let TC_CSS_DOCUMENT_VERIFIED_STATUS = "qc ok"
        
        static let APPONTMENT_LETTER_STATUS = "appointment letter"
        
        static let SELF_ARRANGED_STATUS = "SELF ARRANGED"
        
        static let HOLDING_VISA_STATUS = "HOLDING VISA"
        
        static let VISA_ACTIONED_STATUS = "VISA ACTIONED"
        
        static let VISA_DONE_STATUS = "DONE"
        static let TC_CSS_VISA_DONE_STATUS = "VISA GRANTED"
        static let TC_CSS_VISA_DONE_STATUS2 = "VISA QC DONE"
        
        
        static let VISA_REJECTED_STATUS = "REJECTED"
        static let TC_CSS_VISA_REJECTED_STATUS_1 = "SUBMISSION FAILED"
        static let TC_CSS_VISA_REJECTED_STATUS_2 = "VISA REJECTED"
       
        
        static let VISA_IN_PROCESS_STATUS = "IN PROCESS"
        
        static let VISA_REJECTED_R_STATUS = "REJECTED-R"
        
        static let VISA_VOID_STATUS = "VOID"
        
        static let VISA_READY_FOR_SUBMIT_STATUS = "READY FOR SUBMISSION"
        
        static let TC_CSS_VISA_APPOINTMENT_SCHEDULED_1 = "APPOINTMENT DONE"
        static let TC_CSS_VISA_APPOINTMENT_SCHEDULED_2 = "SUBMITTED"
        static let TC_CSS_VISA_APPOINTMENT_SCHEDULED_3 = "INTERVIEW CALL"
        static let TC_CSS_VISA_APPOINTMENT_SCHEDULED_4 = "ADDITIONAL DOCS ASKED"
        
        
        static let DOCUMENTS_PENDING_STATUS = "Documents pending"
        
         static let DOCUMENTS_NOT_PROCESSED_BY_TCIL = "Not to be processed by TCIL"
    }
    
    struct Profile {
        
        static let ADRESS_TEXT = "Address"
        
        static let EMAIL_ID_TEXT = "Email ID"
        
        static let CONTACT_NUMBER_TEXT = "Contact Number"
        
        static let AADHAR_TEXT = "Aadhar"
        
        static let PASSPORT_TEXT = "Passport"
        
    }
    struct userProfile {
        static let userDetails = "userDetail"
        static let first_Name = "First Name"
        static let last_Name = "Last Name"
        static let mobile_Number = "Mobile Number"
        static let email_Address = "Email Address"
        static let address = "Address"
        static let password = "Password"
        static let confirm_password = "Confirm Password"
        static let check_Flag = "Check Flag"
        static let userID = "UserId"
        static let userPassword = "User Password"
        static let image = "Image"
        static let title = "Title"
        static let privacy_policy = "Privacy_Policy"
        static let Sign_UP = "Sign_UP"
        static let Sign_IN = "Sign_IN"
        static let Forgot_Password = "Forgot_Password"
        static let RESEND_OTP_BTN_TITLE = "Resend OTP"
        static let FORGOT_PASSWORD_BTN_TITLE = "Forgot password"
        static let LOGIN_WITH_OTP = "LoginWithOTP"
        static let LOGIN_WITH_PASSWORD = "LoginWithPassword"
        static let OTP = "OTP"
        
        
        
    }
    struct Contact {
        static let first_Name = "First Name"
        static let last_Name = "Last Name"
        static let contact_Number = "Contact Number"
        static let email_Address = "Email Address"
        static let holiday_type = "Holiday Type"
        static let your_City = "Your City"
        static let closet_Location = "Closet Location"
        static let likely_TravelDate = "Likely Travel Date"
        static let message = "Message"
        
    }
}


struct PricingConstants {
    static let CalendarEndDate  = "calendarEndDate"
    static let CalendarStartDate  = "calendarStartDate"
    static let DbResponseBean  = "dbResponseBean"
    static let ltResponseBean  = "ltResponseBean"
    static let Bookable  = "bookable"
    static let OnRequest  = "onRequest"
    static let DATE  = "DATE"
    static let date  = "date"
    static let Price  = "DR_PRICE"
    static let HubCode  = "hubCode"
    static let LtCityCode  = "ltCityCode"
    static let CityCode  = "cityCode"
    static let LtItineraryCode  = "ltItineraryCode"
    static let farecaLtItineraryCode = "farecaLtItineraryCode"
    static let Market  = "market"
    static let Mode  = "mode"
    static let pkgId  = "pkgId"
    static let pkgClassId  = "pkgClassId"
    static let pkgTypeId  = "pkgTypeId"
    static let pkgSubtypeId  = "pkgSubtypeId"
    static let pkgSubClass  = "pkgSubClass"
    static let ltMarket  = "ltMarket"
    static let room  = "room"
    static let optionalsActivities = "optionalsActivities"
    static let hub = "hub"
    static let hubCity = "hubCity"
    static let departureDate = "departureDate"
    static let userMobileNo = "userMobileNo"
    static let ltProdCode = "ltProdCode"
    static let userEmailId = "userEmailId"
    static let enquirySource = "enquirySource"
    static let enquiryCampaign = "enquiryCampaign"
    static let enquiryMedium = "enquiryMedium"
    static let LP = "LP"
    static let bookURL = "bookURL"
    static let regionId = "regionId"
    static let crmEnquiryId = "crmEnquiryId"
    static let crmStatus = "crmStatus"
    static let custStateName = "custStateName"
    static let isGstApplicable = "isGstApplicable"
    static let isTcsApplicable = "isTcsApplicable"
    static let websiteTerm = "websiteTerm"
    static let agentId = "agentId"
    static let isOnBehalf = "isOnBehalf"
    static let isCrmQuoteLead = "isCrmQuoteLead"
}


struct PackageDetailConstants {
    
    static let MealDescription = "mealDescription"
    static let PackageClassId = "packageClassId"
    static let type = "type"
    static let TcilMstMeal = "tcilMstMeal"
    static let PackageDetail = "packageDetail"
    static let CityCode = "cityCode"
    static let CityName = "cityName"
    static let NoOfNights = "noOfNights"
    static let Description = "description"
    static let typeDefaultMsg = "typeDefaultMsg"
    static let isTypeDefaultMsg = "isTypeDefaultMsg"
    static let isTypeIncluded = "isTypeIncluded"
    static let VisaDesc = "visa"
    static let SightseeingId = "sightseeingId"
    static let Name = "name"
    static let Includes = "includes"
    static let Excludes = "excludes"
    static let HotelName = "hotelName"
    static let ItineraryDay = "itineraryDay"
    static let ItineraryDescription = "itineraryDescription"
    static let Image = "image"
    static let AccomodationHotelId = "accomodationHotelId"
    static let tcilMstHolidayAccomodationImageCollection = "tcilMstHolidayAccomodationImageCollection"
    static let StarRating = "starRating"
    static let IconId = "iconId"
    static let IconName  = "iconName"
    static let highlights  = "highlights"
    static let UserSearchHistory  = "UserSearchHistory"
    
    static let Duration = [DurationTypes.LESS_THAN_7_NIGHTS,DurationTypes._8_TO_12_NIGHT,DurationTypes.MORE_THAN_12_NIGHTS]
    
    static let PackageTypes = [PackageType.GROUP,PackageType.ONLY_STAY,PackageType.CUSTOMIZED,PackageType.AIR_INCLUSIVE]
    
    
}
enum DurationTypes:String {
    case LESS_THAN_7_NIGHTS = "Less than 7 nights"
    case _8_TO_12_NIGHT = "8 to 12 night"
    case MORE_THAN_12_NIGHTS = "More than 12 nights"
}
struct ContactHelpline {
    static let TCHelpLineNumber = "18002099100"
}
enum PackageType:String {
    case GROUP = "Group"
    case ONLY_STAY = "Only Stay"
    case CUSTOMIZED = "Customized"
    case AIR_INCLUSIVE = "Air Inclusive"
}

struct AdhocBookingConstant {
    static let Discount_Details = "DISCOUNT_DETAILS"
    static let PAX_NUMBER = "PAX_NO"
    static let ADHOC_DISCOUNT = "ADHOC_DISCOUNT"
    static let REMARKS = "REMARKS"
}

struct HolidayDestinationHomeScreenConstants {
    
    static let SearchString = "searchString"
    static let SearchType = "searchType"
    static let PackageName = "packageName"
    static let PackageList = "pkgnameIdMappingList"
    static let PackageId = "packageId"
    static let LocationCode = "locationCode"
    static let StateCode = "stateCode"
    static let CityCode = "cityCode"
    static let CountryCode = "countryCode"
    static let IndianHolidayType = "Indian Holidays"
    static let InternationalHolidayType = "International Holidays"
    
    static let IndianHolidayPlaceHolder = "e.g Goa, Kerala"
    static let InternationalHolidayPlaceHolder = "e.g Europ, US"
    static let bannerLargeImage = "bannerLargeImage"
    
    // level constants
    
    static let PopularDestination = "PopularDestination"
    static let LastMinuteDeals = "LastMinuteDeals"
    static let HolidayAsPer = "HolidayAsPer"
    static let FirstLevel = "First level"
    static let HolidayType = "HolidayType"
    static let RecentlyViewed = "RecentlyViewed"
    static let TravelGuide = "TravelGuide"
    
}
struct iPhoneScreenSize {

    static let mainSize = UIScreen.main.bounds

}
