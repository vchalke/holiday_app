//
//  VisaCountryMaster+CoreDataProperties.swift
//  holidays
//
//  Created by Saurav on 28/11/17.
//  Copyright © 2017 Mobicule. All rights reserved.
//

import Foundation
import CoreData


extension VisaCountryMaster {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<VisaCountryMaster> {
        return NSFetchRequest<VisaCountryMaster>(entityName: "VisaCountryMaster")
    }

    @NSManaged public var countryName: String?
    @NSManaged public var countryName_bfNumber: String?
    @NSManaged public var tourRelation: Tour?

}
