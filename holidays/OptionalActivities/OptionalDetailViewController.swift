//
//  OptionalDetailViewController.swift
//  sotc-consumer-application
//
//  Created by Mac on 08/08/18.
//  Copyright © 2018 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit
protocol OptionalDetailVcDelegate {
    func selectActivity(optionalObject:OptionalModel)
}
class OptionalDetailViewController: UIViewController {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var selectActivityButton: UIButton!
    
    var optionalObject : OptionalModel? = nil
    var delegate : OptionalDetailVcDelegate? = nil
    var packageDetailModel : PackageDetailsModel? = nil

    @IBOutlet weak var eventNameLabel: UILabel!
    @IBOutlet weak var optionalEventImageview: UIImageView!
    
    @IBOutlet weak var eventDescriptionTextview: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.containerView.fadedShadow()
        self.containerView.layer.cornerRadius = 5
        self.eventNameLabel.text = optionalObject?.eventName ?? ""
//        self.eventDescriptionTextview.attributedText = optionalObject?.eventDescription.convertHtml()
        self.eventDescriptionTextview.text = optionalObject?.eventDescription.convertHtml()
//        var path : String = AuthenticationConstant.ASTRA_SERVICES_URL + "images/holidays/" + (self.packageDetailModel?.packageId ?? "" ) + ((self.optionalObject?.imageName) ?? "")
        let packageID = "\(self.packageDetailModel?.packageId ?? "")/"
        var path : String = AuthenticationConstant.ASTRA_SERVICES_URL + "images/holidays/" + packageID + ((self.optionalObject?.imageName) ?? "")
        
        path = path.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        if let url = URL.init(string: path)
        {
            self.optionalEventImageview.sd_setImage(with: url, placeholderImage: UIImage(named: "defaultImagePrebooking"))
        }
        printLog("Image URL : " , path)
       // https://services-uatastra.sotc.in/images/holidays/PKG000487/imageName
    }
    
    override func viewDidLayoutSubviews() {
        
        self.selectActivityButton.layer.borderColor = UIColor.clear.cgColor
        self.selectActivityButton.layer.cornerRadius = 3
        self.selectActivityButton.layoutIfNeeded()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func selectActivityButtonClicked(_ sender: Any)
    {
        self.delegate?.selectActivity(optionalObject: self.optionalObject!)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func closeButtonClicked(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
