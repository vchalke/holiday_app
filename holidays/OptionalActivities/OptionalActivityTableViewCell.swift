//
//  OptionalActivityTableViewCell.swift
//  sotc-consumer-application
//
//  Created by Mac on 08/08/18.
//  Copyright © 2018 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit
protocol OptionalActivityCellDelegate {
    func isSelected(status : Bool, index : Int)
}
class OptionalActivityTableViewCell: UITableViewCell,UITextFieldDelegate {
    var delegate : OptionalActivityCellDelegate? = nil
    var packageDetailModel : PackageDetailsModel = PackageDetailsModel.init()

    var cellIndex : Int = -1
    var optionalObject : OptionalModel? = nil
    
    var adultlimit :Int = 1
    var childLimit : Int = 0
    var infantLimit : Int = 0
    
    @IBOutlet weak var childPriceLabel: UILabel!
    @IBOutlet weak var editContainerViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var infantTextfield: UITextField!
    @IBOutlet weak var adultTextfield: UITextField!
    @IBOutlet weak var infantPriceLabel: UILabel!
    @IBOutlet weak var childTextfield: UITextField!
    @IBOutlet weak var adultPriceLabel: UILabel!
    @IBOutlet weak var eventTitleLabel: UILabel!
    @IBOutlet weak var selectionRadioButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionRadioButton.treatAsCheckBox()

        self.selectionRadioButton.isSelected = false
        
        if optionalObject != nil
        {
            if optionalObject!.isSelected
            {
                self.selectionRadioButton.isSelected = true
            }
            else
            {
                self.selectionRadioButton.isSelected = false
            }
        }
        self.adultTextfield.rightView = rightViewImage(inputTextField: self.adultTextfield, image: #imageLiteral(resourceName: "dropdowncutout"))
        self.adultTextfield.rightViewMode = .always
        
        self.childTextfield.rightView = rightViewImage(inputTextField: self.childTextfield, image: #imageLiteral(resourceName: "dropdowncutout"))
        self.childTextfield.rightViewMode = .always
        
        self.infantTextfield.rightView = rightViewImage(inputTextField: self.infantTextfield, image: #imageLiteral(resourceName: "dropdowncutout"))
        self.infantTextfield.rightViewMode = .always
        
      /*  let imageView = UIImageView();
        let image = UIImage(named: "downArrowTextField");
        imageView.image = image;
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        imageView.frame = CGRect.init(x: 0, y: 0, width: 20, height: 20)
        let outerview : UIView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 30, height: 20))
        outerview.addSubview(imageView)
        imageView.isUserInteractionEnabled = false
        outerview.isUserInteractionEnabled = false
        self.adultTextfield.rightViewMode = UITextFieldViewMode.always
        self.adultTextfield.rightViewMode = .always
        self.adultTextfield.rightView = outerview
        
        let imageView2 = UIImageView();
        imageView2.image = image;
        imageView2.contentMode = UIViewContentMode.scaleAspectFit
        imageView2.frame = CGRect.init(x: 0, y: 0, width: 20, height: 20)
        let outerview2 : UIView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 30, height: 20))
        outerview2.addSubview(imageView2)
        imageView2.isUserInteractionEnabled = false
        outerview2.isUserInteractionEnabled = false
        self.childTextfield.rightViewMode = UITextFieldViewMode.always
        self.childTextfield.rightViewMode = .always
        self.childTextfield.rightView = outerview2
        
        let imageView3 = UIImageView();
        imageView3.image = image;
        imageView3.contentMode = UIViewContentMode.scaleAspectFit
        imageView3.frame = CGRect.init(x: 0, y: 0, width: 20, height: 20)
        let outerview3 : UIView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 30, height: 20))
        outerview3.addSubview(imageView3)
        imageView3.isUserInteractionEnabled = false
        outerview3.isUserInteractionEnabled = false
        self.infantTextfield.rightViewMode = UITextFieldViewMode.always
        self.infantTextfield.rightViewMode = .always
        self.infantTextfield.rightView = outerview3*/
        
        self.infantTextfield.delegate = self
        self.childTextfield.delegate = self
        self.adultTextfield.delegate = self

        self.infantTextfield.layer.borderWidth = 1
        self.infantTextfield.layer.borderColor = UIColor.lightGray.cgColor
        
        self.childTextfield.layer.borderWidth = 1
        self.childTextfield.layer.borderColor = UIColor.lightGray.cgColor
        
        self.adultTextfield.layer.borderWidth = 1
        self.adultTextfield.layer.borderColor = UIColor.lightGray.cgColor
        
    }
   
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    // MARK: - Initialization -

    func showActionSheet(limit:Int , textfield : UITextField)  {
        let deviationActionSheet:UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        deviationActionSheet.addAction(UIAlertAction(title: AlertMessage.TITLE_CANCEL, style: .cancel, handler:nil))
        
        
        for i in 0..<limit
        {
            deviationActionSheet.addAction(UIAlertAction(title: "\(i+1)" , style: .default, handler: { (action: UIAlertAction!)in
                
                textfield.text = "\(i+1)"
                if textfield == self.adultTextfield
                {
                    self.optionalObject?.adultCount = i + 1
                }
                else if textfield == self.childTextfield
                {
                    self.optionalObject?.childCount = i + 1
                }
                else
                {
                    self.optionalObject?.infantCount = i + 1
                }
            }))
            
        }
        
        let vc : OptionalActivitiesViewController = self.delegate as! OptionalActivitiesViewController
        vc.present(deviationActionSheet, animated: true, completion: nil)
    }
    func setData(pricingModel : PricingModel)
    {
        if optionalObject != nil
        {
            if optionalObject!.isSelected
            {
                self.editContainerViewHeightConstraint.constant = 100
                self.selectionRadioButton.isSelected = true
                
                self.adultTextfield.text = "\(optionalObject!.adultCount)"
                self.childTextfield.text = "\(optionalObject!.childCount)"
                self.infantTextfield.text = "\(optionalObject!.infantCount)"
            }
            else
            {
                self.editContainerViewHeightConstraint.constant = 0
                self.selectionRadioButton.isSelected = false
            }
            
        }
        
        self.adultlimit = pricingModel.getAdultCount()
        self.childLimit = pricingModel.getchildCount()
        self.infantLimit = pricingModel.getInfantCount()
        
        self.eventTitleLabel.text = self.optionalObject?.eventName
        self.adultPriceLabel.text = "\(self.optionalObject?.currencyCode ?? "") \(self.optionalObject?.adultPrice ?? 0)"
        self.childPriceLabel.text = "\(self.optionalObject?.currencyCode ?? "") \(self.optionalObject?.childPrice ?? 0)"
        self.infantPriceLabel.text = "\(self.optionalObject?.currencyCode ?? "") \(self.optionalObject?.infantPrice ?? 0)"

    }
    // MARK: - Button Action -

    @IBAction func selectionRadioButtonClicked(_ sender: Any)
    {
        self.selectionRadioButton.isSelected = !self.selectionRadioButton.isSelected
        if optionalObject != nil
        {
            optionalObject?.isSelected = self.selectionRadioButton.isSelected
            delegate?.isSelected(status: self.selectionRadioButton.isSelected, index: cellIndex)
        }
    }
    
    @IBAction func moreInfoButtonClicked(_ sender: Any)
    {
        let vc : OptionalActivitiesViewController = self.delegate as! OptionalActivitiesViewController
        let detailVc : OptionalDetailViewController = OptionalDetailViewController(nibName: "OptionalDetailViewController", bundle: nil)
        detailVc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        detailVc.optionalObject = self.optionalObject
        detailVc.delegate = vc
        detailVc.packageDetailModel = self.packageDetailModel
        vc.present(detailVc, animated: true, completion: nil)
    }
    
  
    // MARK: - textfield Delegates -

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool{
        if textField == adultTextfield
        {
            self.showActionSheet(limit: adultlimit, textfield: textField)
        }
        else if textField == childTextfield
        {
            if childLimit == 0
            {
                AppUtility.displayAlert(title: "Alert", message: "No child added")
            }
            else
            {
                self.showActionSheet(limit: childLimit, textfield: textField)

            }
        }
        else
        {
            if infantLimit == 0
            {
                AppUtility.displayAlert(title: "Alert", message: "No Infant added")
            }
            else
            {
                self.showActionSheet(limit: infantLimit, textfield: textField)
            }
        }
        return false
    }
    func rightViewImage(inputTextField:UITextField,image:UIImage) -> UIButton {
        let button = UIButton(type: .custom)
        button.setImage(image, for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
        button.frame = CGRect(x: CGFloat(inputTextField.frame.size.width - 25), y: CGFloat(5), width: CGFloat(30), height: CGFloat(30))
        return button
    }
}
