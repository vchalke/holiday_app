//
//  OptionalModel.swift
//  sotc-consumer-application
//
//  Created by Mac on 08/08/18.
//  Copyright © 2018 Mobicule Technologies Private Limited. All rights reserved.
//

import Foundation
class OptionalModel
{
    var isSelected : Bool = false
    var adultCount : Int = 1
    var childCount : Int = 0
    var infantCount : Int = 0
    var childPrice : Int = 0
    var infantPrice: Int = 0
    var adultPrice : Int = 0
    var eventName : String = ""
    var eventDescription : String = ""
    var imageName : String = ""
    var currencyCode: String  = ""
    var currencyRate: Float = 0.0
    var optionalID: Int = 0

    init(optionalDict:Dictionary<String,Any>)
    {
      
 
        let optionalIDDict : Dictionary<String,Any> = optionalDict["optionalsId"] as? Dictionary<String,Any> ?? Dictionary.init()
        self.childPrice = optionalIDDict["childPrice"] as? Int ?? 0
        self.adultPrice = optionalIDDict["adultPrice"] as? Int ?? 0
        self.infantPrice = optionalIDDict["infantPrice"] as? Int ?? 0
        
        self.imageName = optionalIDDict["image"] as? String ?? ""
        self.eventName = optionalIDDict["name"] as? String ?? ""
        self.eventDescription = optionalIDDict["description"] as? String ?? ""
        self.optionalID = optionalIDDict["optionalsId"] as? Int ?? 0
        
        let exchangeRateIdDict : Dictionary<String,Any> = optionalIDDict["exchangeRateId"] as? Dictionary<String,Any> ?? Dictionary.init()
        self.currencyCode = exchangeRateIdDict["currencyCode"] as? String ?? ""
        self.currencyRate = exchangeRateIdDict["currencyRate"] as? Float ?? 0.0
        
    }
    func setSelected()
    {
        self.isSelected = true
    }
    func unselect()
    {
        self.isSelected = false
    }
}

