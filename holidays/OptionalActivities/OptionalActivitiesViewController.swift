//
//  OptionalActivitiesViewController.swift
//  sotc-consumer-application
//
//  Created by Mac on 08/08/18.
//  Copyright © 2018 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit
protocol OptionalDelegate {
    func renameContinueButton()
    func getSelectedOptionalActivityArray(optionalArray : Array<OptionalModel>)
}

class OptionalActivitiesViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,OptionalActivityCellDelegate,OptionalDetailVcDelegate
{
    var selectedOptionalArray : Array<OptionalModel> = Array.init()
    var optionalArray : Array<OptionalModel> = Array.init()
    var pricingModel:PricingModel = PricingModel()
    var packageDetailModel:PackageDetailsModel = PackageDetailsModel.init()
    var quotationViewController : QuotationView = QuotationView.init()
    var delegate : OptionalDelegate? = nil
    @IBOutlet weak var optionalTableView: UITableView!
    @IBOutlet weak var addActivitiesAndGetQuoteButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initializeView()
        //        for _ in 0...5
        //        {
        //            optionalArray.append(OptionalModel.init(optionalDict: <#Dictionary<String, Any>#>))
        //        }
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        self.addActivitiesAndGetQuoteButton.layer.cornerRadius = 3
    }
    
    // MARK: - InitializeView -
    
    func initializeView()
    {
        if self.responds(to: #selector(setter: edgesForExtendedLayout))
        {
            self.edgesForExtendedLayout = []
        }
        self.optionalTableView.register(UINib.init(nibName: "OptionalActivityTableViewCell", bundle: nil), forCellReuseIdentifier: "OptionalActivityTableViewCell")
        for dict in self.packageDetailModel.tcilHolidayOptionalsCollection
        {
            self.optionalArray.append(OptionalModel.init(optionalDict: dict))
        }
    }
    // MARK: - Tableview Delegates -
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.optionalArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell :  OptionalActivityTableViewCell = optionalTableView.dequeueReusableCell(withIdentifier: "OptionalActivityTableViewCell", for: indexPath) as! OptionalActivityTableViewCell
        cell.cellIndex = indexPath.row
        cell.packageDetailModel = self.packageDetailModel
        cell.delegate = self
        if optionalArray[indexPath.row] != nil
        {
            cell.optionalObject = optionalArray[indexPath.row]
        }
        cell.setData(pricingModel : self.pricingModel)
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let cell :  OptionalActivityTableViewCell = optionalTableView.cellForRow(at: indexPath) as! OptionalActivityTableViewCell
        cell.selectionRadioButton.isSelected = !cell.selectionRadioButton.isSelected
        if optionalArray[indexPath.row] != nil
        {
            optionalArray[indexPath.row].isSelected = cell.selectionRadioButton.isSelected
            self.isSelected(status: cell.selectionRadioButton.isSelected, index: indexPath.row)
        }
    }
    
    // MARK: - Optional detail vc Delegate -
    
    func selectActivity(optionalObject: OptionalModel)
    {
        optionalObject.isSelected = true
        self.optionalTableView.reloadData()
    }
    
    // MARK: - Cell Delegate -
    
    func isSelected(status: Bool,index : Int)
    {
        let passModel = optionalArray[index]
        if (!status){
            selectedOptionalArray.removeAll { (model) -> Bool in
                model.optionalID == passModel.optionalID
            }
        }else{
             selectedOptionalArray.append(passModel)
        }
        optionalTableView.reloadData()
    }
    
    // MARK: - Button Action -
    
    @IBAction func backButtonClicked(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func callButtonClicked(_ sender: Any)
    {
        
    }
    @IBAction func addActivitiesAndGetQuoteButtonClicked(_ sender: Any)
    {
        self.calculatePrice()
    }
    //MARK: - Pricing API hit -
    func calculatePrice()
    {
        
        LoadingIndicatorView.show("Loading")
        //"holidayRS/pricing"
        let dataDict : Dictionary<String,Any> = AppUtility.getPricingRequestJson(packageDetailModel: self.packageDetailModel, pricingModel: self.pricingModel, optionalArray: optionalArray)
        PreBookingCommunicationManager.getPrebookingData(requestType:"POST", queryParam: nil, pathParam:nil, jsonParam: dataDict, headers: nil, requestURL: kAstraUrlPricing, returnInCaseOffailure: false)
        {
            (status,response) in
            
            printLog("status is \(status)")
            printLog("response is \(String(describing: response))")
            if status
            {
                
                if let responseDict : Dictionary<String,Any> =  response as? Dictionary<String,Any>
                {
                    if let msg : String = responseDict["message"] as? String
                    {
                        if msg != ""
                        {
                            AppUtility.displayAlert(title: "Alert", message: msg)
                        }
                    }
                    else
                    {
                        AppUtility.fillPricingModelWithResponseDict(dict: responseDict, pricingModel: self.pricingModel, quotationView: self.quotationViewController)
                        self.quotationViewController.expandDefaultOptionalView = false
                        self.quotationViewController.expantPaymentView = true
                        self.quotationViewController.expandPromocodeView = true
                        self.delegate?.renameContinueButton()
                        self.delegate?.getSelectedOptionalActivityArray(optionalArray: self.selectedOptionalArray)
                        self.navigationController?.popViewController(animated: false)
                    }
                }
            }
            else
            {
                if let responseDict : Dictionary<String,Any> =  response as? Dictionary<String,Any>
                {
                    if let msg : String = responseDict["message"] as? String
                    {
                        AppUtility.displayAlert(title: "Alert", message: msg)
                    }
                    else
                    {
                        AppUtility.displayAlert(title: "Alert", message: "Some error has occurred")
                    }
                }
                else
                {
                    AppUtility.displayAlert(title: "Alert", message: "Some error has occurred")
                }
            }
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

