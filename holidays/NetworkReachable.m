//
//  NetworkReachable.m
//  holidays
//
//  Created by Kush_Tech on 07/05/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "NetworkReachable.h"

@implementation NetworkReachable
+(id)sharedInstance {
    static NetworkReachable *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
        
    });
    return sharedInstance;
}

- (id)init {
    if (self = [super init]) {
        [self checkNetworkStatus];
    }
    return self;
}

- (BOOL)checkIsConnected{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}

-(void)postNetworkNotification:(NSString*)status{
    [[NSNotificationCenter defaultCenter]postNotificationName:@"Network_Status" object:status];
}

-(void)obseverNetworkNotification:(void(^) (NSString *isNetwork))updateHandler
{
    [[NSNotificationCenter defaultCenter]addObserverForName:@"Network_Status" object:nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {
        updateHandler(note.object);
    }];
}
-(void)checkNetworkStatus{
    self.reachable = [Reachability reachabilityWithHostName:@"google.com"];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fetchNotification:) name:kReachabilityChangedNotification object:self.reachable];
    [self.reachable startNotifier];
}
-(void)fetchNotification:(NSNotification*)notify{
    Reachability *catchRechability = notify.object;
    if([catchRechability currentReachabilityStatus] != NotReachable){
        [self postNetworkNotification:@"1"];
    }else{
         [self postNetworkNotification:@"0"];
    }
    
}
@end
