//
//  RecentHolidayObject.h
//  holidays
//
//  Created by Kush_Tech on 12/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface RecentHolidayObject : NSObject
//-(instancetype)initWithObjectCore:(RecentHolidayObject *)obj;
-(instancetype)initWithRecentObject:(NSDictionary *)dictionary;
@property(nonatomic,strong)NSString *packageID;
@property(nonatomic,strong)NSString *packageName;
@property(nonatomic,strong)NSString *packageImgUrl;
@property(nonatomic)NSInteger packagePrize;
@end

NS_ASSUME_NONNULL_END
