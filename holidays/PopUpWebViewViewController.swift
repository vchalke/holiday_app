//
//  PopUpWebViewViewController.swift
//  holidays
//
//  Created by Komal Katkade on 1/4/18.
//  Copyright © 2018 Mobicule. All rights reserved.
//

import UIKit

class PopUpWebViewViewController: UIViewController {
    
    @IBOutlet weak var webview: UIWebView!
    @IBOutlet weak var titleLabel: UILabel!
    var htmlPath : String = ""
    var popuptitle : String = ""
    override func viewDidLoad()
    {
        super.viewDidLoad()
        titleLabel.text = popuptitle
//        webview.loadRequest(URLRequest(url: URL(fileURLWithPath: Bundle.main.path(forResource: htmlPath, ofType: "html")!)))
        
        let urlString = Bundle.main.url(forResource: htmlPath, withExtension: "html")
        webview.loadRequest(URLRequest(url: urlString!))
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func closeBtnClicked(_ sender: Any)
    {
        self.dismiss(animated: false, completion: nil)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
