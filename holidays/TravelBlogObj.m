//
//  TravelBlogObj.m
//  holidays
//
//  Created by Kush_Team on 10/06/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "TravelBlogObj.h"

@implementation TravelBlogObj

-(instancetype)initWithTravelArray:(NSDictionary *)arrayObjDict{
   if ([super init]){
       self.link = [arrayObjDict valueForKey:@"link"];
       NSDictionary *titlDict = [arrayObjDict valueForKey:@"title"];
       self.title = [titlDict valueForKey:@"rendered"];
       NSDictionary *contentDict = [arrayObjDict valueForKey:@"content"];
       self.imgUrl = [contentDict valueForKey:@"rendered"];
       self.imgUrl = [self.imgUrl substringFromIndex:[self.imgUrl rangeOfString:@"src="].location+[@"src=" length]+1];
       
       NSLog(@"%@",self.imgUrl);
       if ([self.imgUrl containsString:@"alt="]){
           self.imgUrl = [self.imgUrl substringToIndex:[self.imgUrl rangeOfString:@"alt="].location-2];
       }else{
           self.imgUrl = @"";
       }
       
   }
    return self;
}
@end

//"rendered":"\n<p>In this COVID-19 pandemic, each one should take precautions of ourselves and others. Keeping social distancing and sanitization is mandatory for each individual. Here are the guidelines you need to know before traveling by Air.<\/p>\n\n\n\n<div class=\"wp-block-image\"><figure class=\"aligncenter size-full is-resized\"><img src=\"https:\/\/blog.thomascook.in\/wp-content\/uploads\/2020\/05\/mailer-2-1-scaled.jpg\" alt=\"\" class=\"wp-image-36978\" width=\"582\" height=\"3245\" srcset=\"https:\/\/blog.thomascook.in\/wp-content\/uploads\/2020\/05\/mailer-2-1-scaled.jpg 459w, https:\/\/blog.thomascook.in\/wp-content\/uploads\/2020\/05\/mailer-2-1-54x300.jpg 54w\" sizes=\"(max-width: 582px) 100vw, 582px\" \/><\/figure><\/div>\n",
