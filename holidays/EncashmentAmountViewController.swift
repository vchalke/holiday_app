//
//  EncashmentAmountViewController.swift
//  holidays
//
//  Created by Komal Katkade on 11/30/17.
//  Copyright © 2017 Mobicule. All rights reserved.
//

import UIKit

class EncashmentAmountViewController: ForexBaseViewController,UITextFieldDelegate
{
    @IBOutlet weak var labelTermsAndCond: UILabel!
    @IBOutlet weak var labelAdvanceAmount: UILabel!
    @IBOutlet weak var sellAtBranchRadioButton: UIButton!
    @IBOutlet var paymentView: UIView!
    @IBOutlet var doneBtn: UIButton!
    var sellForexBO : SellForexBO =  SellForexBO.init()
    
    @IBOutlet weak var tickMarkRadioButon: UIButton!
    @IBOutlet weak var makeAdvancePaymentRadioButton: UIButton!
    @IBOutlet weak var labelTotalEncashmentAmount: UILabel!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        Bundle.main.loadNibNamed("EncashmentAmountViewController", owner: self, options: nil)
        
        super.addViewInBaseView(childView: self.paymentView)
        super.setUpHeaderLabel(labelHeaderNameText: "Sell Forex")
        
        super.viewDidLoad()
        labelTotalEncashmentAmount.text = sellForexBO.totalAmountWithTax
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ForexBuyPassangerViewController.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        
        //continueButton.layer.cornerRadius = 10
        setupRadioButtons()
        labelTotalEncashmentAmount.text = sellForexBO.totalAmountWithTax
        
//        labelAdvanceAmount.text = "\(sellForexBO.quoteResponseDict.object(forKey: "advanceAmount") as! NSInteger) INR"
        
        let advanceAmount = "\(sellForexBO.quoteResponseDict.object(forKey: "advanceAmount") as! NSInteger) INR"
        labelAdvanceAmount.text = "Make Advance Payment to Block the Rate \(advanceAmount)"
    }
    
    override func viewDidLayoutSubviews()
    {
        super.setSubViewFrame(childView: self.paymentView)
    }
    
    override func viewWillLayoutSubviews()
    {
        super.setSubViewFrame(childView: self.paymentView)
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        let text = (labelTermsAndCond.text)!
//        let underlineAttriString = NSMutableAttributedString(string: text)
        let underlineAttriString = NSMutableAttributedString(attributedString: labelTermsAndCond.attributedText!)
        
        let range1 = (text as NSString).range(of: "Terms and conditions")
        underlineAttriString.setAttributes(convertToOptionalNSAttributedStringKeyDictionary([convertFromNSAttributedStringKey(NSAttributedString.Key.font) : UIFont(name: "HelveticaNeue-Light", size: CGFloat(13.0))!
            , convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor) : UIColor(red: 0 / 255.0, green: 149 / 255.0, blue: 218 / 255.0, alpha: 1.0)]), range: range1)
        underlineAttriString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: range1)
        
        let range2 = (text as NSString).range(of: "Booking policy")
        underlineAttriString.setAttributes(convertToOptionalNSAttributedStringKeyDictionary([convertFromNSAttributedStringKey(NSAttributedString.Key.font) : UIFont(name: "HelveticaNeue-Light", size: CGFloat(13.0))!
            , convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor) : UIColor(red: 0 / 255.0, green: 149 / 255.0, blue: 218 / 255.0, alpha: 1.0)]), range: range2)
        underlineAttriString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: range2
        )
        labelTermsAndCond.attributedText = underlineAttriString
        
        let tapAddExpe : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(BuyForexPayemtViewController.tapLabel))
        tapAddExpe.delegate = self as? UIGestureRecognizerDelegate
        tapAddExpe.numberOfTapsRequired = 1
        tapAddExpe.numberOfTouchesRequired = 1
        labelTermsAndCond.addGestureRecognizer(tapAddExpe)
    }
    
    //MARK: on tap
    func tapLabel (sender: UITapGestureRecognizer)
    {
        let text = (labelTermsAndCond.text)!
        let termsRange = (text as NSString).range(of: "Terms and conditions")
        let policyRange = (text as NSString).range(of: "Booking policy")
        
        if sender.didTapAttributedTextInLabel(label: labelTermsAndCond, inRange: policyRange)
        {
            let popupVC: PopUpWebViewViewController = PopUpWebViewViewController(nibName: "PopUpWebViewViewController", bundle: nil)
            popupVC.htmlPath = "bookingPolicy"
            popupVC.popuptitle = "Booking Policy"
            popupVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            self.present(popupVC, animated: false, completion: nil)
        }
        else if sender.didTapAttributedTextInLabel(label: labelTermsAndCond, inRange: termsRange)
        {
            let popupVC: TermsAndConditionPopUpViewController = TermsAndConditionPopUpViewController(nibName: "TermsAndConditionPopUpViewController", bundle: nil)
            popupVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            self.present(popupVC, animated: false, completion: nil)
        }
        else
        {
            print("Tapped none")
        }
    }

    func setupRadioButtons()
    {
        makeAdvancePaymentRadioButton.setImage(UIImage.init(named: "ForexRadioButtonOn"), for: UIControl.State.selected)
        makeAdvancePaymentRadioButton.setImage(UIImage.init(named: "ForexRadioButtonOff"), for: UIControl.State.normal)
        makeAdvancePaymentRadioButton.isSelected = false
        
        sellAtBranchRadioButton.setImage(UIImage.init(named: "ForexRadioButtonOn"), for: UIControl.State.selected)
        sellAtBranchRadioButton.setImage(UIImage.init(named: "ForexRadioButtonOff"), for: UIControl.State.normal)
        sellAtBranchRadioButton.isSelected = false
        
        tickMarkRadioButon.setImage(UIImage.init(named: "ForexCheckBoxOn"), for: UIControl.State.selected)
        tickMarkRadioButon.setImage(UIImage.init(named: "ForexCheckBoxOff"), for: UIControl.State.normal)
        tickMarkRadioButon.isSelected = true
    }
    
    func dismissKeyboard()
    {
        view.endEditing(true)
    }
    // MARK:  textfield  delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        self.view.endEditing(true)
        return true
    }
    
    func validateFields()->Bool
    {
        if !sellAtBranchRadioButton.isSelected &&  !makeAdvancePaymentRadioButton.isSelected
        {
            showAlert(message: "Please select type of encashment")
            return false
        }
        if !tickMarkRadioButon.isSelected
        {
            showAlert(message: "Please accept the booking policy and Terms & Conditions")
            return false
        }
        return true
    }
    // MARK:  Button Action
    @IBAction func doneButtonClicked(_ sender: Any)
    {
        if validateFields()
        {
            let branchDetailViewController : BranchDetailViewController = BranchDetailViewController(nibName: "ForexBaseViewController", bundle: nil)
            sellForexBO.isMakeAdvancePaymentSelected = makeAdvancePaymentRadioButton.isSelected
            sellForexBO.isSellAtBranchSelected = sellAtBranchRadioButton.isSelected
            branchDetailViewController.sellForexBO = sellForexBO
            self.navigationController?.pushViewController(branchDetailViewController, animated: true)
        }
    }
    
    @IBAction func sellAtBranchRadioButtonClicked(_ sender: Any)
    {
        makeAdvancePaymentRadioButton.isSelected = false
        sellAtBranchRadioButton.isSelected = true
        sellForexBO.totalAmountWithTax = labelTotalEncashmentAmount.text!
        
        self.doneBtn.isEnabled = true
        self.doneBtn.isUserInteractionEnabled = true
        
        let popupVC: PopUpForSellBranchViewController = PopUpForSellBranchViewController(nibName: "PopUpForSellBranchViewController", bundle: nil)
        popupVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(popupVC, animated: false, completion: nil)
    }
    
    @IBAction func makeAdvancePaymentRadioButtonClicked(_ sender: Any)
    {
        makeAdvancePaymentRadioButton.isSelected = true
        sellAtBranchRadioButton.isSelected = false

//        let advanceAmount = "\(sellForexBO.quoteResponseDict.object(forKey: "advanceAmount") as! NSInteger) INR"
//        sellForexBO.totalAmountWithTax = advanceAmount
        
        if (sellForexBO.quoteResponseDict.object(forKey: "advanceAmount") as! NSInteger) <= 0
        {
            self.doneBtn.isEnabled = false
            self.doneBtn.isUserInteractionEnabled = false
            
        }else
        {
             self.doneBtn.isEnabled = true
            self.doneBtn.isUserInteractionEnabled = true
            
            sellForexBO.totalAmountWithTax = labelTotalEncashmentAmount.text!
            
            let popupVC: PopUpForSellBlockViewController = PopUpForSellBlockViewController(nibName: "PopUpForSellBlockViewController", bundle: nil)
            popupVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            self.present(popupVC, animated: false, completion: nil)
        }
        
      
    }
    
    @IBAction func tickMarkRadioButonClicked(_ sender: Any)
    {
        tickMarkRadioButon.isSelected = !tickMarkRadioButon.isSelected
    }
    
    @IBAction func advancePaymentHelpBtnClicked(_ sender: Any)
    {
        let popupVC: PopUpViewController = PopUpViewController(nibName: "PopUpViewController", bundle: nil)
        popupVC.msg = "By selecting this option, you are blocking the rate of exchange for your Sell (Encash) Forex transaction for the next 48 hours. The blocking amount that you are paying at this moment will be refunded to the source of payment on successful completion of the transaction. For a transaction to be successful the encashment should happen within the next 48 working hours and you must surrender at least 80% of Forex booked in this transaction. In case the transaction is not completed by you within the next 40 working hours, this blocking amount will be retained by Thomas Cook India Ltd.  "
        popupVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(popupVC, animated: false, completion: nil)
    }
    
    @IBAction func sellAtBranchHelpBtnClicked(_ sender: Any)
    {
        let popupVC: PopUpViewController = PopUpViewController(nibName: "PopUpViewController", bundle: nil)
        popupVC.msg = "Sell at Branch option means no blocking of rates of exchange at the time of encashment. After confirming our forex expert will call you and guide about next steps. We do not accept coins for encashment."
        popupVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(popupVC, animated: false, completion: nil)

    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromNSAttributedStringKey(_ input: NSAttributedString.Key) -> String {
	return input.rawValue
}
