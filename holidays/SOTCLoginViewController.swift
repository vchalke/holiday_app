//
//  LoginViewController.swift
//  sotc-consumer-application
//
//  Created by Saurav Kumar on 18/07/17.
//  Copyright © 2017 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit
import CoreData
//Login footer old : *Customers booked for Summer holiday's (Travel date from 1st April 2018 onwards)  on Group departure packages only can avail this service
//old otp label: Enter the OTP you received via SMS

class SOTCLoginViewController: BaseViewController,UITextFieldDelegate,loginCommunicationDelegate{
    
    //MARK: IBOutlet
    @IBOutlet var mobileNumberLabel: UILabel!
    @IBOutlet var mobileNumberTextField: UITextField!
    
     @IBOutlet var emailIdTextField: UITextField!
    @IBOutlet var loginView: UIView!
    
    @IBOutlet var otpView: UIView!
    
    @IBOutlet var mobileNumberView: UIView!
    
    @IBOutlet var otpTextField1: UITextField!
    
    @IBOutlet var otpTextField6: UITextField!
    @IBOutlet var otpTextField5: UITextField!
    @IBOutlet var otpTextField4: UITextField!
    @IBOutlet var otpTextField3: UITextField!
    @IBOutlet var otpTextField2: UITextField!
    @IBOutlet var loginButton: UIButton!
    @IBOutlet var resendOtpButton: UIButton!
    
    @IBOutlet var versionLabel: UILabel!
    
    @IBOutlet weak var mobilenumberViewHeightConstraints: NSLayoutConstraint!
    
    @IBOutlet weak var emailIdView: UIView!
    var isOTPViewPresented = false
    var otp : NSInteger = 0
    var optString:String = ""
    
    
    //MARK: UIViewController Life Cycle
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Bundle.main.loadNibNamed("SOTCLoginViewController", owner:self, options: nil)
        super.addView(inBaseView:self.loginView)
        super.hideBackButton(true)
//        super.setHeaderTitle("Login")// Previous Set Title
        super.setHeaderTitle("My Booking")
        
        let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        
       // versionLabel.text = "Version \(version ?? "")"
        
        mobileNumberLabel.font = UIFont.fontForDevice()
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(SOTCLoginViewController.dismissKeyboard))
        
        //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
        //tap.cancelsTouchesInView = false
        
        self.view.addGestureRecognizer(tap)
        
        mobileNumberTextField.textAlignment = .center
        
        
        self.mobileNumberTextField.becomeFirstResponder()

//        let currentUserNumber = "9820198918" // "9833998977" // "9820198918" // "9999999999" // For Testing Purpose
//        let currentUserNumber = UserDefaults.standard.value(forKey: kuserDefaultTC_MobileNumber) as? String
//        UserDefaults.standard.set(currentUserNumber, forKey: UserDefauldConstant.userMobileNumber)
//        self.mobileNumberTextField.text = "9820198918"
//        self.ByPassOTP()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.loginButton.layer.cornerRadius = (self.loginButton.bounds.height / 2) - 4
        
        if  let logInStatus:String = UserDefaults.standard.value(forKey: kLoginStatus) as? String
        {
            if logInStatus.caseInsensitiveCompare(kLoginSuccess) == .orderedSame
            {
                if let emailID:String = UserDefaults.standard.value(forKey: kLoginEmailId) as? String
                {
                    if !emailID.isEmpty
                    {
                        self.emailIdView.isHidden = true
                        self.mobilenumberViewHeightConstraints.constant = 180/2
                        self.emailIdTextField.text = emailID
                        return
                    }
                }
            }
            
        }
        
        self.emailIdView.isHidden = false
        self.mobilenumberViewHeightConstraints.constant = 180
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    //MARK: Button Click Methods
    
    @IBAction func onResendButtonClick(_ sender: Any) {
        self.generateOtp()
    }
    
    @IBAction func onLoginButtonClick(_ sender: Any) {
        
        let validationResult:String = self.validateMobileAndEmail()
        
        if validationResult != ""
        {
            self.displayAlert(message: validationResult)
        }
        else
        {
            NetCoreSharedManager.sharedInstance().setUpIdentity(self.mobileNumberTextField.text!) // 09-04-2018
            
          /*  let coreobj:CoreUtility = CoreUtility.sharedclassname() as! CoreUtility  // 09-04-2018
            let mobilenNumberString = self.mobileNumberTextField.text!  // 09-04-2018
            coreobj.strPhoneNumber.insert(mobilenNumberString, at: 0)  // 09-04-2018*/

            if !(self.isOTPViewPresented) {
                
                if checkForNewUser() {
                    
//                    let alert = UIAlertController(title: "New User", message: "Login with new user will clear offline data. Do you wish to continue ?", preferredStyle: UIAlertController.Style.alert)
                    let alert = UIAlertController(title: "My Bookings", message: "Do you wish to continue using this Number ?", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: AlertMessage.TITLE_CANCEL, style: UIAlertAction.Style.cancel, handler: { (UIAlertAction) in
                        
                        
                    }))
                    
                    alert.addAction(UIAlertAction(title: "Continue", style: UIAlertAction.Style.default, handler: { (UIAlertAction) in
                        
                        self.doLogin()
                        
                    }))
                    
                    UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
                }
                else
                {
                    doLogin()
                }
                
                
            }
            else
            {
                
                doLogin()
            }
            
            
            
        }
        
    }
    
    func doLogin()
    {
        if self.isOTPViewPresented {
            
            if (AppUtility.checkStirngIsEmpty(value: self.otpTextField1.text!)) && (AppUtility.checkStirngIsEmpty(value: self.otpTextField2.text!)) && (AppUtility.checkStirngIsEmpty(value: self.otpTextField3.text!)) && (AppUtility.checkStirngIsEmpty(value: self.otpTextField4.text!)) && (AppUtility.checkStirngIsEmpty(value: self.otpTextField5.text!)) && (AppUtility.checkStirngIsEmpty(value: self.otpTextField6.text!))
            {
                self.displayAlert(message: NetworkValidationMessages.NotValidOTP.message)
            }
            else
            {
                
                if UserDefauldConstant.isOTPByPass {
                    
                    ByPassOTP()
                }
                else
                {
                    ValidateOTPwithDeviceGeneratedOTP()
                }
                
                
            }
            
        }
        else
        {
 
            LoadingIndicatorView.show("Loading")
            
            //
            // var mobileNumber  =  UserDefauldConstant.defaultUserMobileNumber //self.getMobileNumber() //"8390131180"//self.mobileNumberTextField.text
        
            
            let mobileNumber  = self.mobileNumberTextField.text
            
            LoginController.sendCheckMobileRequest(mobileNumber: mobileNumber!
                , completion: { (status) in
//
                        LoadingIndicatorView.hide()
//
                    if status.status
                    {
                        
                        if self.checkForNewUser()
                        {

                        LoginController.clearDataForOldUser()

                        }
                        
                        if !(UserDefauldConstant.isOTPByPass) {
                            
                            //self.sendOtpResquest();
                            //self.generateOtp();
                            
                            if !self.emailIdView.isHidden
                            {
                                
                                self.sendEmailIdValidationRequest()
                                
                            }else{
                                
                                self.generateOtp();
                                
                                 if !(self.isOTPViewPresented) {
                                 
                                 self.presentOTPAnimation()
                                 
                                 self.isOTPViewPresented = true
                                    
                                    self.mobilenumberViewHeightConstraints.constant = 180
                                 
                                 }
                            }
                            
                        }else{
                            
                                if !(self.isOTPViewPresented) {
                                    
                                    self.presentOTPAnimation()
                                    
                                    self.isOTPViewPresented = true
                                    
                                    self.mobilenumberViewHeightConstraints.constant = 180
                                    
                                }
                        }
                        
                        
                      
                        
                    }
                    else
                    {
                            self.displayAlert(message: status.message)
                    }
            })
            
        }
    }
    
    //MARK: Custom Methods
    func showViewAfterGeneratingOTP()
    {
        if !(self.isOTPViewPresented) {
            
            self.presentOTPAnimation()
            
            self.isOTPViewPresented = true
            
            self.mobilenumberViewHeightConstraints.constant = 180
            
        }
        
    }
    
    func ByPassOTP()
    {
        let userDetailMO:UserProfile = NSEntityDescription.insertNewObject(forEntityName: "UserProfile", into: CoreDataController.getContext()) as! UserProfile
        
        // userDetailMO.mobileNumber = UserDefauldConstant.defaultUserMobileNumber
        
        
        //8510894234
        
        UserDefaults.standard.set("\(self.mobileNumberTextField.text!)", forKey: UserDefauldConstant.userMobileNumber)
        
        
        UserDefaults.standard.set(true, forKey: UserDefauldConstant.isUserLoggedIn)
        
        
        
        
        CoreDataController.getContext().mergePolicy = CoreDataConstant.dataMergePolicy
        
        
        CoreDataController.saveContext()
        
        
        self.setTabbarController()
    }
    
    func ValidateOTPwithDeviceGeneratedOTP()
    {
//        let userotp : NSInteger = ("\(self.otpTextField1.text!)\(self.otpTextField2.text!)\(self.otpTextField3.text!)\(self.otpTextField4.text!)\(self.otpTextField5.text!)\(self.otpTextField6.text!)" as NSString).integerValue
        
         let userotp : String = ("\(self.otpTextField1.text!)\(self.otpTextField2.text!)\(self.otpTextField3.text!)\(self.otpTextField4.text!)\(self.otpTextField5.text!)\(self.otpTextField6.text!)")
        
        //if otp == userotp
            if optString != nil && userotp != nil && !optString.isEmpty && !userotp.isEmpty && userotp.caseInsensitiveCompare(optString) == .orderedSame
        {
            let userDetailMO:UserProfile = NSEntityDescription.insertNewObject(forEntityName: "UserProfile", into: CoreDataController.getContext()) as! UserProfile
            
            UserDefaults.standard.set("\(self.getMobileNumber() /*self.mobileNumberTextField.text!*/)", forKey: UserDefauldConstant.userMobileNumber)
            userDetailMO.mobileNumber = "\(self.getMobileNumber() /*self.mobileNumberTextField.text!*/)"
            
            
            // userDetailMO.mobileNumber = UserDefauldConstant.defaultUserMobileNumber
            // UserDefaults.standard.set(UserDefauldConstant.defaultUserMobileNumber, forKey: UserDefauldConstant.userMobileNumber)
            
            UserDefaults.standard.set(true, forKey: UserDefauldConstant.isUserLoggedIn)
            
            CoreDataController.getContext().mergePolicy = CoreDataConstant.dataMergePolicy
            
            CoreDataController.saveContext()
            
            //self.sendEmailIdValidationRequest()
            
            self.netSelfServiceLogEvent(isloggedIn: "Success") // 09-04-2018
            
            self.setTabbarController()
        }
        else
        {
              self.displayAlert(message: "Oops!Incorrect OTP")
             self.netSelfServiceLogEvent(isloggedIn: "Failed") // 09-04-2018
        }
    }
    
    func ValidateOTP() {
        
        LoadingIndicatorView.show("Loading")
        
        LoginController.sendOTPValidateRequest(mobileNumber: self.getMobileNumber() /*self.mobileNumberTextField.text!*/, otpString: "\(self.otpTextField1.text!)\(self.otpTextField2.text!)\(self.otpTextField3.text!)\(self.otpTextField4.text!)\(self.otpTextField5.text!)\(self.otpTextField6.text!)", completion: { (networkStatus) in
            
            DispatchQueue.main.sync {
                
                LoadingIndicatorView.hide()
                
            }
            
            if networkStatus.status
            {
                DispatchQueue.main.sync {
                    
                    LoadingIndicatorView.hide()
                    
                    
                    let userDetailMO:UserProfile = NSEntityDescription.insertNewObject(forEntityName: "UserProfile", into: CoreDataController.getContext()) as! UserProfile
                    
                    
                    
                    UserDefaults.standard.set("\(self.getMobileNumber() /*self.mobileNumberTextField.text!*/)", forKey: UserDefauldConstant.userMobileNumber)
                    userDetailMO.mobileNumber = "\(self.getMobileNumber() /*self.mobileNumberTextField.text!*/)"
                    
                    
                    // userDetailMO.mobileNumber = UserDefauldConstant.defaultUserMobileNumber
                    // UserDefaults.standard.set(UserDefauldConstant.defaultUserMobileNumber, forKey: UserDefauldConstant.userMobileNumber)
                    
                    UserDefaults.standard.set(true, forKey: UserDefauldConstant.isUserLoggedIn)
                    
                    CoreDataController.getContext().mergePolicy = CoreDataConstant.dataMergePolicy
                    
                    CoreDataController.saveContext()
                    
                    
                    self.setTabbarController()
                    
                    
                }
            }
            else
            {
                
                DispatchQueue.main.sync {
                    
                    self.displayAlert(message: networkStatus.message)
                    
                }
                
            }
            
        })
    }
    
    func setTabbarController() -> Void {
        
          AppUtility.checkAndSubscibeNotificationOnServer()
        
        UITabBarItem.appearance().setTitleTextAttributes(convertToOptionalNSAttributedStringKeyDictionary([convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor): UIColor.gray]), for:.normal)
        
        UITabBarItem.appearance().setTitleTextAttributes(convertToOptionalNSAttributedStringKeyDictionary([convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor): UIColor.black]), for:.selected)
        
        //        let userProfileVC : UserProfileViewController = UserProfileViewController (nibName: "UserProfileViewController", bundle: nil)
        //        userProfileVC.title = "PROFILE"
        //        userProfileVC.tabBarItem.selectedImage = #imageLiteral(resourceName: "profileSelected").withRenderingMode(.alwaysOriginal)
        //        userProfileVC.tabBarItem.image = #imageLiteral(resourceName: "profileUnselected").withRenderingMode(.alwaysOriginal)
        //
        
        let userBookingVC : UserBookingHomeViewController = UserBookingHomeViewController (nibName: "BaseViewController", bundle: nil)
        userBookingVC.title = Title.MY_BOOKING
        userBookingVC.tabBarItem.selectedImage = #imageLiteral(resourceName: "userBookingTabSelected").withRenderingMode(.alwaysOriginal)
        userBookingVC.tabBarItem.image = #imageLiteral(resourceName: "userBookingTabUnSelected").withRenderingMode(.alwaysOriginal)
        
        
        let menuVC : MenuViewController = MenuViewController (nibName: "MenuViewController", bundle: nil)
        menuVC.title = "MORE"
        menuVC.tabBarItem.selectedImage  = #imageLiteral(resourceName: "moreTabSelected").withRenderingMode(.alwaysOriginal)
        menuVC.tabBarItem.image = #imageLiteral(resourceName: "moreTabUnSelected").withRenderingMode(.alwaysOriginal)
        
        
        let notificationVC : SOTCNotificationViewController = SOTCNotificationViewController (nibName: "SOTCNotificationViewController", bundle: nil)
        notificationVC.title = "NOTIFICATION"
        notificationVC.tabBarItem.selectedImage  = #imageLiteral(resourceName: "notificationTabSelected").withRenderingMode(.alwaysOriginal)
        notificationVC.tabBarItem.image = #imageLiteral(resourceName: "notificationTabUnSelected").withRenderingMode(.alwaysOriginal)
        
        
        //  let userProfileNavVC : UINavigationController = UINavigationController(rootViewController: userProfileVC)
        let userBookingNavVC : UINavigationController = UINavigationController(rootViewController: userBookingVC)
        let notificationNavVC : UINavigationController = UINavigationController(rootViewController: notificationVC)
        let menuNavVC : UINavigationController = UINavigationController(rootViewController: menuVC)
        
        
        let applicationTabbar : UITabBarController = UITabBarController()
        
        applicationTabbar.setViewControllers([userBookingNavVC,notificationNavVC, menuNavVC], animated: true)
        
        
//        self.navigationController?.pushViewController(applicationTabbar, animated: true)
        self.navigationController?.pushViewController(userBookingVC, animated: true)
        
//
//        UIView.transition(with: self.view, duration: 2.0, options: UIViewAnimationOptions.curveEaseInOut, animations: {
//            
//            if let appDelegate : AppDelegate = UIApplication.shared.delegate as? AppDelegate
//            {
//                appDelegate.window?.rootViewController = applicationTabbar
//                
//            }
//            
//        }, completion: { (finished: Bool) -> () in
//            
//            // completion
//            
//        })
        
        
        
        
    }
    
    func presentOTPAnimation() {
        
        self.otpView.frame  = CGRect(x: UIScreen.main.bounds.width, y:  self.mobileNumberView.frame.origin.y - 10, width:  self.mobileNumberView.frame.width, height:  self.mobileNumberView.frame.height)
        
        self.view.addSubview(self.otpView)
        
        UIView.animate(withDuration: 2, animations: {
            
        }) { (_) in
            
            
            UIView.animate(withDuration: 2, animations: {
                
                self.mobileNumberView.frame.origin.x = -UIScreen.main.bounds.width
                
                self.otpView.frame  = CGRect(x: 30, y:  self.mobileNumberView.frame.origin.y, width:  self.mobileNumberView.frame.width, height:  self.mobileNumberView.frame.height)
                
                
            }) {(_) in
                
                self.resendOtpButton.isHidden = false
                
                self.resendOtpButton.isUserInteractionEnabled = true
                
                self.mobileNumberView.isHidden = true
                
                 self.otpTextField1.becomeFirstResponder()
                
            }
            
            
            
            //            UIView.animate(withDuration: 2, delay: 1, options: [.curveEaseIn], animations: {
            //
            //
            //
            //
            //            })
        }
        
        
    }
    
    func checkForNewUser() -> Bool   {
        // Not check User is right or wrong just redirect to next screen
        if let userMobileNumber:String = UserDefaults.standard.object(forKey: (UserDefauldConstant.userMobileNumber )) as? String
        {
            
            if userMobileNumber == (self.mobileNumberTextField.text!) {
                
                return false
            }
        }else{
        
            return false
        
        }
        
        return true
        
    }
    
    func displayAlert(message:String)
    {
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: AlertMessage.TITLE_OK, style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func getMobileNumber() -> String
    {
        
        if !((self.mobileNumberTextField.text?.isEmpty)!)
        {
            return self.mobileNumberTextField.text! //.toLengthOf(length: 3)
        }
        
        return ""
        
        
    }
    
    func  generateOtp()
    {
//        otp = NSInteger(arc4random_uniform(999999) + 100000) ;
      //  printLog("OTP : %i",otp)
        var sixDigitNumber: String {
            var result = ""
            repeat {
                result = String(format:"%06d", arc4random_uniform(1000000) )
            } while result.count < 6 //Set<Character>(result.characters).count < 6
            return result
        }
         printLog("sixDigitNumber : ",sixDigitNumber)
        
        if sixDigitNumber.count >= 6
        {
             //otp = NSInteger.init(sixDigitNumber)!
            optString = sixDigitNumber
            printLog("OTP : ",optString)
            self.sendDeviceGeneratedOtpToserver()
        }else
        {
            self.generateOtp()
        }
    }
    
    func sendDeviceGeneratedOtpToserver()
    {
        LoginController.sendGeneratedOTPToServer(mobileNumber:self.getMobileNumber(), otp: "\(optString)"  /*self.mobileNumberTextField.text!*/, completion: { (networkStatus) in
            
           // DispatchQueue.main.sync {
                
                if networkStatus.status
                {
                    
                    self.displayAlert(message: networkStatus.message + "\(self.getMobileNumber())")
                    
                    //self.displayAlert(message: "\(self.otp) is the OTP for your login to the Thomas Cook App. This is usable once and valid for 30 mins from the request. PLS DO NOT SHARE WITH ANYONE")
                    
                   // self.displayAlert(message: "\(self.otp) is the OTP for internal testing")
                    
                }else{
                    
                    self.displayAlert(message: "Please try again")
                    
                }
                
           // }
            
            
        })
        
        
    }
    func sendOtpResquest()
    {
        LoginController.sendOTPRequest(mobileNumber:self.getMobileNumber()  /*self.mobileNumberTextField.text!*/, completion: { (networkStatus) in
            
            DispatchQueue.main.sync {
                
                if networkStatus.status
                {
                    self.displayAlert(message: networkStatus.message + "\(self.getMobileNumber())")
                    
                }else{
                    
                    self.displayAlert(message: networkStatus.message)
                    
                }
                
            }
            
            
        })
        
        
    }

    
    
    //MARK: TextField Delegate Methods
    
    
  
    
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        
        if (textField == mobileNumberTextField ) && (string.count != 0)
        {
            
            let textFieldText: NSString = (textField.text ?? "") as NSString
            
            let txtAfterUpdate = textFieldText.replacingCharacters(in: range, with: string)
            
            
            //Should Resign After entering 10 Digits
            if ((txtAfterUpdate.count) <= 10)  {
                
                if ((txtAfterUpdate.count) == 10)  {
                    
                    self.mobileNumberTextField.text = txtAfterUpdate
                    
                    textField.resignFirstResponder()
                    
                    return false
                }
                else
                {
                    return true
                }
                
            }
            else
            {
                textField.resignFirstResponder()
                return false
            }
        }else if textField == emailIdTextField
        {
             return true
        }
        //OTP Logic In Forword Direction
        else if ((textField == otpTextField1) || (textField == otpTextField2) || (textField == otpTextField3) || (textField == otpTextField4) || (textField == otpTextField5) || (textField == otpTextField6)) && ( ((textField.text?.count)! < 1)) && (string.count > 0)
        {
            
            switch textField {
                
            case otpTextField1:
                
                otpTextField2.becomeFirstResponder()
        
            case otpTextField2:
                
                otpTextField3.becomeFirstResponder()
                
            case otpTextField3:
                
                otpTextField4.becomeFirstResponder()
                
            case otpTextField4:
                
                otpTextField5.becomeFirstResponder()
                
            case otpTextField5:
                
                otpTextField6.becomeFirstResponder()
                
            case otpTextField6:
                
                textField.resignFirstResponder()
    
            default:
                
                textField.resignFirstResponder()
            }
            
            
            textField.text = string
            
            
            return false
        }
        //OTP Logic In Backword Direction
        else if ((textField == otpTextField1) || (textField == otpTextField2) || (textField == otpTextField3) || (textField == otpTextField4) || (textField == otpTextField5) || (textField == otpTextField6)) && ( ((textField.text?.count)! >= 1)) && (string.count == 0)
        {
            switch textField {
                
                
            case otpTextField1:
                
               otpTextField1.text = ""
                
              //  otpTextField2.becomeFirstResponder()
                
                
            case otpTextField2:
                
                
                otpTextField2.text = ""
                
                otpTextField1.becomeFirstResponder()
                
                
            case otpTextField3:
                
                
                otpTextField3.text = ""
                otpTextField2.becomeFirstResponder()
                
            case otpTextField4:
                
                otpTextField4.text = ""
                otpTextField3.becomeFirstResponder()
                
            case otpTextField5:
                
                
                otpTextField5.text = ""
                otpTextField4.becomeFirstResponder()
                
            case otpTextField6:
                
                otpTextField6.text = ""
                otpTextField5.becomeFirstResponder()
        
            default:
                
                textField.resignFirstResponder()
                
            }
            
            return false
        }
        else
        {
            return true
        }
        
    }
    
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        switch textField {
            
        case mobileNumberTextField:
            
            textField.resignFirstResponder()
            
            return true
            
        case emailIdTextField :
            
            textField.resignFirstResponder()
            
            return true
            
        case otpTextField1:
            
            otpTextField2.becomeFirstResponder()
            return false
            
        case otpTextField2:
            
            otpTextField3.becomeFirstResponder()
            return false
            
        case otpTextField3:
            
            otpTextField4.becomeFirstResponder()
            return false
            
        case otpTextField4:
            
            otpTextField5.becomeFirstResponder()
            return false
            
        case otpTextField5:
            
            otpTextField6.becomeFirstResponder()
            return false
            
        case otpTextField6:
            
            
            textField.resignFirstResponder()
            return true
            
        default:
            return true
        }
        
    }
    
 
    func validateMobileAndEmail() -> String
    {
        if !(self.isOTPViewPresented)
        {
            
            if (AppUtility.checkStirngIsEmpty(value: self.mobileNumberTextField.text!))
            {
                return NetworkValidationMessages.NotValidMobileNumber.message
                
            }else if !self.emailIdView.isHidden
            {
                if !AppUtility.isValidEmail(testStr: emailIdTextField.text ?? "")
                {
                    return "Enter valid email ID"
                    
                }
            }
            
        }
        
        
    return ""
    }
    
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    func sendEmailIdValidationRequest()
    {
        let loginCommunication : LoginCommunicationManager = LoginCommunicationManager.init()
        loginCommunication.delegate = self;
        loginCommunication.loginViewController = self;
        loginCommunication.moduleName = "Manage Holidays"//"Self Service"
        
                loginCommunication.loginCheck(withType: "guestLogin", withUserID: emailIdTextField.text ?? "", withPassword: "")
        
    }
    
    //MARK: - LoginCommunicationDelegate
    func guestLoggedInSuccessFully(withUserID userID: String!)
    {
        self.generateOtp();
        
        //DispatchQueue.main.sync {
            
            if !(self.isOTPViewPresented) {
                
                self.presentOTPAnimation()
                
                self.isOTPViewPresented = true
                
                 self.mobilenumberViewHeightConstraints.constant = 180
                
            }
       // }

        
    }
    
    func loggedInFail(withRason reasonMessege: String!) {
        self.displayAlert(message: "Unable to login For Holidays ,please try again")
    }
    
   
    //MARK:-  NetCoreImplementaion
    
    func netSelfServiceLogEvent(isloggedIn:String)
    {
        let payloadList:NSMutableDictionary = NSMutableDictionary.init()
        
        payloadList.setValue(self.emailIdTextField.text, forKey: "s^Email")
        payloadList.setValue(isloggedIn, forKey: "s^Status")
        payloadList.setValue("App", forKey: "s^Source")
        
         NetCoreAnalyticsVC.getNetCorePushNotificationDetailDictionary(payloadList as! [AnyHashable : Any], withPayloadCount: 137)
       
    }

}

extension String {
    
    func toLengthOf(length:Int) -> String {
        if length <= 0 {
            return self
        } else if let to = self.index(self.startIndex, offsetBy: length, limitedBy: self.endIndex) {
            return self.substring(from: to)
            
        } else {
            return ""
        }
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromNSAttributedStringKey(_ input: NSAttributedString.Key) -> String {
	return input.rawValue
}
