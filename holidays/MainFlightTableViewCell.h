//
//  MainFlightTableViewCell.h
//  holidays
//
//  Created by Kush_Tech on 04/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FlightsColectObject.h"
#import <WebKit/WebKit.h>
#import "PackageDetailModel.h"
#import "HolidayPackageDetail.h"
NS_ASSUME_NONNULL_BEGIN
@protocol MainFlightTableViewCellDelegaete <NSObject>
@optional
- (void)showFlightWithSectionTitle:(NSString*)sectionTitle withPackageModel:(PackageDetailModel*)packageModel;
@end

@interface MainFlightTableViewCell : UITableViewCell<UITableViewDataSource,UITableViewDelegate>

{
    
    NSMutableArray *flightsShowArray;
    PackageDetailModel *packageDetailModel;
}
@property (nonatomic, weak) id <MainFlightTableViewCellDelegaete> mainFlightDelaget;
@property (weak, nonatomic) IBOutlet UITableView *table_views;
@property (strong ,nonatomic) NSString *flight_defaultMsg;
@property (strong ,nonatomic) NSArray *flightsdataArray;
@property (weak, nonatomic) IBOutlet UIWebView *webVioews;

-(void)loadFlightDataFromPackageModel:(PackageDetailModel*)packageModel holidayPkgModel:(HolidayPackageDetail*)holiPkgModel  withAray:(NSArray*)flightModelArr;
@end

NS_ASSUME_NONNULL_END
