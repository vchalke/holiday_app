//
//  UserBookingViewController.swift
//  sotc-consumer-application
//
//  Created by Saurav Kumar on 18/07/17.
//  Copyright © 2017 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit
import CoreData

protocol UserBookingListProtocol{
    
    func updateTourData()
    
}


class UserBookingViewController: UIViewController,UITableViewDataSource,UITableViewDelegate ,XMLParserDelegate,UserBookingListProtocol{
    
    @IBOutlet var bookingTableView: UITableView!
    var delegate: UserBookingHomeViewController?
    
    var delegateUserBookingHome:UserBookingTourHomeProtocol? = nil
    
     var refreshControl: UIRefreshControl?
    
     var tourBookedList: [Tour]?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        // self.bookingTableView.register(BookingTableViewCell.self, forCellReuseIdentifier: "BookingTableCell")
         self.configurePoolToRefresh()
        
        self.bookingTableView.register(UINib(nibName:  "BookingTableViewCell", bundle: nil), forCellReuseIdentifier: "BookingTableCell")
        
        
           }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

            return (tourBookedList?.count ?? 0)
       
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if self.title?.lowercased() == "current"  {
            
            return tableView.frame.height
        }
        else if self.title?.lowercased() == "upcoming" && tourBookedList?.count ?? 0 == 1  {
            
            return tableView.frame.height
        }
       
        else
        {
            return 220
            
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "BookingTableCell", for: indexPath) as? BookingTableViewCell
            else {
                
                fatalError("The dequeued cell is not an instance of MealTableViewCell.")
        }
        
      if  let tourDetail:Tour = tourBookedList?[indexPath.row]
      {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        if let departurDate = tourDetail.departureDate
        {
        
            let newDate = dateFormatter.string(from: departurDate as Date)
            cell.tourDateLabel?.text = "\(newDate )"
            
        }
        //pass Date here
        
        
        cell.paymentButton.layer.cornerRadius = (cell.paymentButton.frame.height / 2)
        cell.tourTitleLabel?.text = "\(tourDetail.tourName ?? "")"
        
        cell.bfnNumberLabel.text = "Booking ID : \(tourDetail.bfNumber ?? "")"
        
        if tourDetail.productBookingStatus?.lowercased() == "canceled" || tourDetail.productBookingStatus?.lowercased() == "cancelled"
        {
            cell.paymentButton.isHidden = false
        }
        else{
            
            cell.paymentButton.isHidden = true
        }
        
        let tourPackageImages:Array<TourPackageImages>? = tourDetail.packageImagesRelation?.allObjects as? Array<TourPackageImages>
        
        if(tourPackageImages != nil && tourPackageImages?.count ?? 0 > 0)
        {
            let tourPackageImage = tourPackageImages?.first
            
            let prodITINCode:String = (tourPackageImage?.prodITINCode!)!
            
            let packageId:String = (tourPackageImage?.packageId!)!
            
            let imageId:String = (tourPackageImage?.imageId!)!
            
            let itineraryImageTitle:String = (tourPackageImage?.itineraryImageTitle!)!
            
            let docummentURL = prodITINCode + "/" + packageId + "/" + imageId + "/" + itineraryImageTitle
            
          let filePath = TourDocumentController.getDocumentFilePath(url: docummentURL , bfnNumber: tourDetail.bfNumber!,moduleFolder:Title.MY_BOOKING)
            
            
           
            
        cell.downloadImage(filePath: filePath, url: (tourPackageImage?.path)!)
            
            //cell.tourImageViewBG.image =  #imageLiteral(resourceName: "NOC")
        }else{
        
        }
        
        if self.title?.lowercased() == "current"  {
            
            cell.tourImageViewBG?.contentMode = .scaleToFill
        }
        else if self.title?.lowercased() == "upcoming" && tourBookedList?.count ?? 0 == 1 {
            
            cell.tourImageViewBG?.contentMode = .scaleToFill
        }
        else
        {
       
            
        }
 
       // let cityCoverd:Array<CityCovered>? = (tourDetail.cityCoveredRelation?.allObjects as? Array<CityCovered>)!
        
        let cityCoverd:Array<CityCovered>? = tourDetail.cityCoveredRelation?.allObjects as? Array<CityCovered>
        printLog("cell bfnNumber : \(tourDetail.bfNumber)")
        
        if(cityCoverd != nil && cityCoverd?.count ?? 0 > 0)
        {
            let sortedCityCoverd = cityCoverd?.sorted(by: { (CityCovered1, CityCovered2) -> Bool in
                
                
                Int(CityCovered1.position!)! < Int(CityCovered2.position!)!
                
                
            })
            
            
            cell.citiesCoveredArray = sortedCityCoverd
            
            printLog("sorted city Covered \(sortedCityCoverd)")
            
            
        }
        
        cell.routeCollectionView.reloadData()
        
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        let tourDetailVC : TourDetailViewController = TourDetailViewController(nibName: "TourDetailViewController", bundle: nil)
        
        if self.title?.lowercased() == "current"  {
            
            tourDetailVC.tourStatus = TourStatus.Current
            
        }
        else if self.title?.lowercased() == "upcoming"   {
            
            tourDetailVC.tourStatus = TourStatus.UpComing
            
        }else if self.title?.lowercased() == "past"  {
            
            tourDetailVC.tourStatus = TourStatus.Past
            
        }else
        {
            tourDetailVC.tourStatus = TourStatus.Past
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        if let tourDetail:Tour = tourBookedList?[indexPath.row]
        {
            if tourDetail.productBookingStatus?.lowercased() == "canceled" || tourDetail.productBookingStatus?.lowercased() == "cancelled"
            {
                tourDetailVC.tourStatus = TourStatus.Cancelled
            }
            
            tourDetailVC.tourDetail = tourDetail
            
            delegate?.navigationController?.pushViewController(tourDetailVC, animated: true)
        }
       
        
      //  tourDetailVC.tourDetail =  tourBookedList![indexPath.row]
        
        
        
       // delegate?.navigationController?.pushViewController(tourDetailVC, animated: true)
        
    
    }
    
    
    
    //MARK:- pull to refresh
    
    func configurePoolToRefresh(){
        
        refreshControl = UIRefreshControl()
        refreshControl?.attributedTitle = NSAttributedString(string: "")
        //refreshControl?.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl?.addTarget(self, action: #selector(refresh(sender:)), for: UIControl.Event.valueChanged)
        self.bookingTableView.addSubview(refreshControl!)
        
    }
    
    @objc func refresh(sender:AnyObject) {
        // Code to refresh table view
        
        
        if self.delegateUserBookingHome != nil {
            
            self.delegateUserBookingHome?.fetctAllTourDataFromServer()
            
        }
        
        
        refreshControl?.endRefreshing()
        //self.getBFNListDetails()
        
    }
    
    
   /* func getNewBookingListDetails(tourBookedListDetails: [Tour]?) -> [Tour]
    {
        let answer = zip(tourBookedList!, tourBookedListDetails!).enumerated().filter() {
            $1.0 != $1.1
            }.map{$0.0}
    }*/
    
    func updateTourListViews(tourBookedListDetails: [Tour]?) -> [Tour] {
        
      if  self.title == "Past"
      {
         if let pastBookedArray = tourBookedListDetails?.filter( { (tourDetail: Tour) -> Bool in
            return (tourDetail.departureDate?.timeIntervalSince1970)! < Date().timeIntervalSince1970
         }){
            if (pastBookedArray.count) > 0 {
                
                return pastBookedArray
                
            }
            
        }
        
      }else if self.title == "upcoming"
      {
        if let futureBookedArray = tourBookedListDetails?.filter( { (tourDetail: Tour) -> Bool in
            return (tourDetail.departureDate?.timeIntervalSince1970)! > Date().timeIntervalSince1970  })
        {
            if (futureBookedArray.count) > 0 {
                
                return futureBookedArray
                
        }
            
        }
        
        }
        
        
        
        
        return []
        
    }
    
    //MARK: UserBookingListProtocol Methods
    
    func updateTourData() {
        
        if self.bookingTableView != nil {
            
          self.bookingTableView.reloadData()
        }
        
   
    }
    
    //func insertData()
}
