//
//  LoginCommNewManager.swift
//  holidays
//
//  Created by Kush_Team on 10/07/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//
/*
import UIKit
class LoginCommNewManager: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
*/
import Foundation
import FBSDKLoginKit
//class LoginCommNewManager{
class LoginCommNewManager : NSObject{
    var userDictionary:Dictionary<String,Any> = Dictionary.init()
    
    override init() {
        if let userDic = UserDefaults.standard.value(forKey: kLoginUserDetails) as? Dictionary<String,Any>{
        let firstname = userDic["fname"] as? String ?? ""
        let lastname = userDic["lName"] as? String ?? ""
        let email_id = userDic["email"] as? String ?? ""
//        let email_id = UserDefaults.standard.value(forKey: kLoginEmailId)
        let pasword = UserDefaults.standard.value(forKey: kLoginPasswords)
        userDictionary[Constant.userProfile.userPassword] = pasword
        userDictionary[Constant.userProfile.password] = pasword
        userDictionary[Constant.userProfile.userID] = email_id
        userDictionary[Constant.userProfile.first_Name] = firstname
        userDictionary[Constant.userProfile.last_Name] = lastname
        }
    }
    class func showFailureAlert() {
        AppUtility.displayAlert(title: "Failure", message: "Some error is occurred!")
    }
    
    class func sendOTP(emailId:String,completion:@escaping(_ status:Bool,_ responseString:String,_ response:Any?) -> Void) {
     
        if AppUtility.checkNetConnection()
        {
            UserBookingController.getASTRASessionAndTockenID(completion: { (status, responseDict) in
                
                if status
                {
                    guard let requestID = responseDict?["requestId"] as? String else
                    {
                        completion(false,"Some error occured",nil)
                        return
                    }
                    
                    guard let sessionID: String = responseDict?["tokenId"] as? String else
                    {
                        completion(false,"Some error occured",nil)
                        return
                    }
                   
//                    let urlString:String = AuthenticationConstant.ASTRA_SERVICES_URL + "commonRS/login/sendOTP/" + emailId
                    let urlString:String = kbaseURLForAstra + "commonRS/login/sendOTP/" + emailId
                    let headersDict: HTTPHeaders = [
                        "content-type": "application/json",
                        "Accept": "application/json",
                        APIConstants.SessionId:sessionID,
                        APIConstants.RequestID:requestID,
                        APIConstants.UniqueId:AppUtility.setUUID()
                    ]
                    
                    request(urlString, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headersDict).responseJSON(completionHandler: { response in
                        
                        printLog("Request: \(String(describing: response.request))")   // original url request
                        printLog("Response: \(String(describing: response.response))") // http url response
                        printLog("Result: \(response.result)")
                        
                        if let json = response.result.value {
                            printLog("JSON: \(json)") // serialized json response
                            
                            if let responseData:Dictionary<String,Any> = json as? Dictionary<String,Any>
                            {
                                if let userStatus = responseData["text"] as? String , !userStatus.isEmpty {
                                    
                                    if userStatus.caseInsensitiveCompare("true") == .orderedSame
                                    {
                                        completion(true,"OTP send to registered email ID",responseData)
                                        return
                                        
                                    }else  if userStatus.caseInsensitiveCompare("false") == .orderedSame
                                    {
                                        completion(false,"error in sending OTP",responseData)
                                        return
                                    }
                                    else  if userStatus.caseInsensitiveCompare("blocked") == .orderedSame
                                    {
                                        completion(false," Blocked user for 30mins for sending OTP",responseData)
                                        return
                                    }
                                    
                                }
                                
                            }
                            
                            completion(false,"",nil)
                            return
                        }
                        
                        completion(false,"",nil)
                        return
                        
                    })
                    
                }else
                {
                    completion(false,"",nil)
                    return
                }
                
               
            })
            
        }else{
            
        }
        
    }
    
    func savePass(Type:String,completion: @escaping (Bool,String) -> ()) -> Void {
//    let savePassURl = "commonRS/login/savePass"
    let savePassURl = kAstraUrlSavePass
    var text:String = ""
    LoginCommNewManager.savePass(requestType: "GET", queryParam: nil, pathParam: nil, jsonParam: nil, headers: nil, requestURL: savePassURl, returnInCaseOffailure: false)
    {(status, response) in
        printLog("\(status)")
        printLog("\(String(describing: response!))")
        if status == true
        {
            if let savePassResponse:NSDictionary = response as? NSDictionary
            {
                
                if savePassResponse.count != 0
                {
                    text = savePassResponse.value(forKey: "text")! as! String
                    printLog("text: \(text)")
                    UserDefaults.standard.set(text, forKey: "text")
                    self.checkCode1(key: text,Type: Type){flag,msg in
                        completion(flag,msg)
                    }
                }
                
            }
            else
            {
                completion(false,"Something Went Wrong")
            }
        }
        else{
           completion(false,"Something Went Wrong")
        }
    }
}

//checkCode
    func checkCode1(key:String,Type:String,completion: @escaping (Bool,String) -> ()) -> Void {
    printLog("key:\(key)")
    let str = Type
//    var Data1:Array = [[String: String]]()
//    if str == Constant.userProfile.LOGIN_WITH_PASSWORD ||  str == Constant.userProfile.LOGIN_WITH_OTP{
//        Data1.append(["key":key,"password":userDictionary[Constant.userProfile.userPassword ] as! String])
//
//    }else{
//            Data1.append(["key":key,"password":userDictionary[Constant.userProfile.password] as! String])
//    }
//    let user:Dictionary = ["checkVersion" : "true",
//                           "entityId" : "8",
//                           "client" : "ios",
//                           "version" : "17.0.0"]
//    let Data:Dictionary = [ "user":user,"data" : Data1,"type":"transaction","entity":"encryptPassword","identifier" : "","action" : "get"] as [String : Any]
        
        var Data1 = Dictionary<String, Any>()
        if str == Constant.userProfile.LOGIN_WITH_PASSWORD ||  str == Constant.userProfile.LOGIN_WITH_OTP{
            Data1["key"] = key
            Data1["password"] = userDictionary[Constant.userProfile.userPassword]
            
        }else{
            Data1["key"] = key
            Data1["password"] = userDictionary[Constant.userProfile.password]
        }
        
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        var Data = Dictionary<String, Any>()
        Data["data"] = Data1
        Data["type"] = "sync"
        Data["entity"] = "encrypt"
        Data["identifier"] = ""
        Data["action"] = "generate"
        Data["platform"] = "ios"
        Data["version"] = appVersion ?? "17.0.0"
        printLog("DataDict : \(Data)")
        
    LoginCommNewManager.getCheckData(requestType: "POST", queryParam: nil, pathParam: nil, jsonParam: Data, headers: nil, requestURL: "", returnInCaseOffailure: false){(status,response) in
        printLog("\(status)")
        printLog("\(String(describing: response!))")
//        if status == true
        if (status)
        {
            if let getResponses:NSDictionary = response as? NSDictionary{
                if let Status = (getResponses.value(forKey: "Status") as? String){
                if Status == "SUCCESS" || Status == "Success"
                {
                    if let dictofText:Array = getResponses.value(forKey: "data") as? Array<Any>{
                        if let datsDict : Dictionary<String,Any> = dictofText[0] as? Dictionary<String, Any>{
                            let text1:String = datsDict["text1"] as! String
                            let text2:String = datsDict["text2"] as! String
                            let text3:String = datsDict["text3"] as! String
                            printLog("text1:\(text1),\ntext2:\(text2),\ntext3:\(text3)")
                            if Type == "Sign_UP" {
                                self.createUser(text1: text1, text2: text2, text3: text3){
                                    completion(true, "")
                                }
                            }
                            else{
                                self.checkKredential(text1: text1, text2: text2, text3: text3,type:Type){flag,msg in
                                    completion(flag,msg)
                                }
                            }
                        }else{
                            printLog(Status)
                            completion(false,"responce no found")
                        }
                        
                    }else{
                        printLog(Status)
                        completion(false,"responce no found")
                    }
                }
                else{
                    printLog(Status)
                    completion(false,"responce no found")
                }
            }else{
                printLog("responce no found")
                completion(false,"responce no found")
                }
            }else{
                completion(false,"Something Went Wrong")
            }
        }
        else
        {
            completion(false,"Something Went Wrong")
        }
    }
}

//check kredential
    func checkKredential(text1:String,text2:String,text3:String,type:String,completion: @escaping (Bool,String) -> ()) -> Void {
    let url:String = "TcilMyAccount/login/verifyCrediential"
        
        var Data:Dictionary<String,Any> = Dictionary()
        
       if type == Constant.userProfile.LOGIN_WITH_PASSWORD
       {
        Data = ["userId":userDictionary[Constant.userProfile.userID]!,"text1":text1,"text2":text2,"text3":text3,"otp":""] as Dictionary<String,Any>
        
       }else if type == Constant.userProfile.LOGIN_WITH_OTP
       {
            Data = ["userId":userDictionary[Constant.userProfile.userID]!,"text1":text1,"text2":text2,"text3":text3,"otp":userDictionary[Constant.userProfile.userPassword]!] as Dictionary<String,Any>
        }else
        {
            return
        }
        
        
    LoginCommNewManager.createUser(requestType: "POST", queryParam: nil, pathParam: nil, jsonParam: Data, headers: nil, requestURL: url, returnInCaseOffailure: false){(status,response) in
        printLog("\(status)")
        printLog("\(String(describing: response ?? ""))")
        if let dictofResponse:Dictionary<String,Any> = response as? Dictionary
        {
            
            if (status == true && (dictofResponse["reasonOfMessage"] as? String != nil))
            {
                let reasonOfMessage:String = dictofResponse["reasonOfMessage"] as! String
                AppUtility.displayAlert(title: "Failure", message: reasonOfMessage)
                completion(false, reasonOfMessage)
            }
            else
            {
                var dictofUser:Dictionary<String,Any> = (dictofResponse["userDetail"] as? Dictionary<String,Any> ?? Dictionary.init())
                dictofUser.updateValue(dictofResponse["userAddressList"] as? String  ?? "", forKey: Constant.userProfile.address)
                if !((dictofUser.isEmpty))
                {
                    
                   
                    
                    printLog("userDictionary:\(self.userDictionary)")
                UserDefaults.standard.set("true", forKey: "loginStatus")
                printLog(dictofUser)
                UserDefaults.standard.set(dictofUser, forKey: Constant.userProfile.userDetails)
                
                    completion(true, "")
                }
                else
                {
                    LoadingIndicatorView.hide()
                    AppUtility.displayAlert(message: "user data not found")
                    
                    completion(false, "user data not found")
                }
            }
        }
        else{
           completion(false, "Something Went Wrong")
        }
    }
}

//register
func createUser(text1:String,text2:String,text3:String,completion: @escaping () -> ()) -> Void {
//    let url:String = "commonRS/login/create"
    let url:String = kAstraCreateLogin
    let Data:Dictionary = ["firstName":userDictionary[Constant.userProfile.first_Name]!,"lastName":userDictionary[Constant.userProfile.last_Name]!,"mobileNo":userDictionary[Constant.userProfile.mobile_Number]!,"title":userDictionary[Constant.userProfile.title]!,"userId":userDictionary[Constant.userProfile.email_Address]!,"text1":text1,"text2":text2,"text3":text3] as [String : Any]

    LoginCommNewManager.createUser(requestType: "POST", queryParam: nil, pathParam: nil, jsonParam: Data, headers: nil, requestURL: url, returnInCaseOffailure: false){(status,resopnse) in
        printLog("\(status)")
        printLog("\(String(describing: resopnse!))")
        if status == true
        {
        if let dictofResponse:Dictionary<String,Any> = resopnse as? Dictionary
        {
            UserDefaults.standard.set("true", forKey: "loginStatus")
            let userDetails:Dictionary<String,Any> = dictofResponse["userDetail"] as! Dictionary<String, Any>
            UserDefaults.standard.set(userDetails, forKey: Constant.userProfile.userDetails)
            printLog("\nuser\(String(describing: userDetails["accountType"])),\(String(describing: userDetails["age"])),\(String(describing: userDetails["custId"])),\(String(describing: userDetails["email"])),\(String(describing: userDetails["fname"])),\(String(describing: userDetails["lName"])),\(String(describing: userDetails["mobileNo"])),\(String(describing: userDetails["role"])),\(userDetails["roleId"]),\(userDetails["userId"]),\(String(describing: userDetails["userTypeId"])),")
            completion()
        }
        else
        {
                completion()
            }
        }
        else
        {
            AppUtility.displayAlert(message: "failure")
            completion()
        }
        
    }
}

//check login type
    func checkAccountType(enterEmail:String,Type:String,completion: @escaping (Bool,String) -> () ) -> Void {
//    var url:String = "commonRS/login/user"
    var url:String = kAstraUrlLoginCheck
    url.append("/\(String(describing: enterEmail))")
    printLog("\(url)")
    
    LoginCommNewManager.checkAccount(requestType: "GET", queryParam: nil, pathParam: nil, jsonParam: nil, headers: nil, requestURL: url, returnInCaseOffailure: false)
    {(status, response) in
        printLog("\(status)")
        printLog("\(String(describing: response!))")
        if status == true
        {
            if let responseData :NSDictionary = response as? NSDictionary
            {
                let dictofResponse = responseData as NSDictionary
                printLog("response dict:\(dictofResponse)")
                let message:String = dictofResponse.value(forKey: "message")! as! String
                let accountType:String = dictofResponse.value(forKey: "accountType")! as! String
                if accountType == "NA" && message == "false"
                {
                    if Type == Constant.userProfile.Sign_UP //start create account heat
                    {
                        self.savePass(Type: Type)
                        {flag,msg in
                            completion(flag, msg)
                        }
                        
                    }
                    else if Type == Constant.userProfile.LOGIN_WITH_PASSWORD ||  Type == Constant.userProfile.LOGIN_WITH_OTP {
                        AppUtility.displayAlert(title: "User Not Available", message: "Sign up require")
                        completion(false, "Something Went Wrong")
                        //let registerVC:RegisterViewController = RegisterViewController(nibName:"RegisterViewController",bundle:nil)
                        //UINavigationController.pushViewController(registerVC, animated: true)
                    }
                    else{
                        AppUtility.displayAlert(title: "Failure", message: "User Not Available")
                        completion(false, "Something Went Wrong")
                    }
                }
                else if accountType == "GU" && message == "false"{
                    //AppUtility.displayAlert(title: "Guest User", message: "login in succesfully")
                    UserDefaults.standard.set("true", forKey: "loginStatus")
                    completion(true, "")
                }
//                else if accountType == "TC" && message == "true"
                else if message == "true"
                {
                    if Type == Constant.userProfile.Sign_UP
                    {
                        AppUtility.displayAlert(title: "Failure", message: "user ID not available")
                       completion(false, "Something Went Wrong")
                    }
                    else if Type == Constant.userProfile.LOGIN_WITH_PASSWORD ||  Type == Constant.userProfile.LOGIN_WITH_OTP //start login heat
                    {
                        self.savePass(Type: Type){flag,msg in
                            completion(flag, msg)
                        }
                        
                    }
                    else if Type == Constant.userProfile.Forgot_Password
                    {
                        self.forgotPassword(Emailid: enterEmail){
                            completion(true, "")
                        }
                    }
                }
                else{
                    completion(false, "Something Went Wrong")
                }
            }
        }
        else
        {
            completion(false, "Something Went Wrong")
        }
    }
}
//login with google
func guestUserLogin(completion: @escaping () -> () ) -> Void {
//    let url:String = "commonRS/login/socialLogin"
    let url:String = kAstraSocialLogin
    let Data:Dictionary<String,Any> = ["firstName":userDictionary[Constant.userProfile.first_Name]!,"lastName":userDictionary[Constant.userProfile.last_Name]!,"id":userDictionary["ID"]!,"imgUrl":userDictionary["ImageUrl"]!,"userId":userDictionary[Constant.userProfile.userID]!,"type":"GP"]
    
    LoginCommNewManager.checkAccount(requestType: "POST", queryParam: nil, pathParam: nil, jsonParam: Data, headers: nil, requestURL: url, returnInCaseOffailure: false){(status,resopnse) in
        printLog("\(status)")
        printLog("\(String(describing: resopnse!))")
        if status == true
        {
            if let dictofResponse:Dictionary<String,Any> = resopnse as? Dictionary
            {
                UserDefaults.standard.set("true", forKey: "loginStatus")
                let userDetails:Dictionary<String,Any> = dictofResponse["userDetail"] as! Dictionary<String, Any>
                UserDefaults.standard.set(userDetails, forKey: Constant.userProfile.userDetails)
                printLog("\nuser\(String(describing: userDetails["accountType"])),\(String(describing: userDetails["age"])),\(String(describing: userDetails["custId"])),\(String(describing: userDetails["email"])),\(String(describing: userDetails["fname"])),\(String(describing: userDetails["lName"])),\(String(describing: userDetails["mobileNo"])),\(String(describing: userDetails["role"])),\(userDetails["roleId"]),\(userDetails["userId"]),\(String(describing: userDetails["userTypeId"])),")
                 LoadingIndicatorView.hide()
                completion()
            }
            else
            {
                 LoadingIndicatorView.hide()
                completion()
            }
        }
        else
        {
             LoadingIndicatorView.hide()
            AppUtility.displayAlert(message: "failure")
            completion()
        }
        
    }
}


//logout
func logout(completion : @escaping(_ status:Bool) -> Void){
    let url:String = "commonRS/login/logoutUser"
   //  let userSessionID:String = UserDefaults.standard.value(forKey: APIResponseConstants.USER_SESSION_ID) as? String ?? ""
       let user:String = UserDefaults.standard.value(forKey: APIConstants.User) as? String ?? ""
    //APIResponseConstants.USER_SESSION_ID:userSessionID ?? "",
    var headersDict: HTTPHeaders = [APIConstants.User:user]
    
    LoginCommNewManager.checkAccount(requestType: "GET", queryParam: nil, pathParam: nil, jsonParam: nil, headers: headersDict, requestURL: url, returnInCaseOffailure: false){(status,resopnse) in
        printLog("\(status)")
        LoadingIndicatorView.hide()
        if (status)
        {
            AppUtility.displayAlert(message: "Loged out successfully")
            
            self.userDictionary = Dictionary.init()
            FBSDKAccessToken.setCurrent(nil)
            FBSDKProfile.setCurrent(nil)
            FBSDKLoginManager().logOut()
            completion(true)
            
            return
        }
        printLog("\(String(describing: resopnse ?? ""))")
        completion(false)
         return
        
        
    }
}
    
//forgotpassword
    func forgotPassword(Emailid:String,completion: @escaping () -> ()) -> Void{
        
        let url = "commonRS/login/forgetPassword/"+Emailid
        LoginCommNewManager.checkAccount(requestType: "GET", queryParam: nil, pathParam: nil, jsonParam: nil, headers: nil, requestURL: url, returnInCaseOffailure: false){(status,resopnse) in
            printLog("\(status)")
            LoadingIndicatorView.hide()
            printLog("\(String(describing: resopnse!))")
            completion()
        }
    }
    

        // MARK: - FETCH details for SRP
     @objc class func searchPackages (selectedDict:Dictionary<String,Any>,completion : @escaping(Bool,String,String) -> Void) // search destination by search APi and pick first object to get required query parameter for SRp
          {
              LoadingIndicatorView.show()// selectedDict["continentName"] selectedDict["destinationName"]
            let detsinationName = (selectedDict["destinationName"] as? String ?? "").uppercased().replacingOccurrences(of: " ", with: "%20")
            
//            (selectedDict["destinationName"] as? String ?? "")
            
              PreBookingCommunicationManager.getSearchData(requestType: "GET", queryParam: "?searchAutoSuggest="+detsinationName, pathParam: nil, jsonParam: nil, headers: nil, requestURL: "/autosuggest", returnInCaseOffailure: false)
              {
                  (status,response) in
                  printLog("Auto search status is \(status)")
                  printLog("Auto search response is \(String(describing: response))")
                  if status == true
                  {
                      LoadingIndicatorView.hide()
                      let responseArray : Array<Dictionary<String, Any>>  = response as? Array<Dictionary<String, Any>> ?? Array.init()
                      let filteredArray : Array<Dictionary<String, Any>> = responseArray.filter({ (dict:Dictionary<String, Any>) -> Bool in
                          if let searchType : String = dict[HolidayDestinationHomeScreenConstants.SearchString] as? String
                          {
                              if searchType.caseInsensitiveCompare(selectedDict["destinationName"] as? String ?? "") == .orderedSame
                              {
                                  return true
                              }
                          }
                          return false
                      })
                      if filteredArray.count > 0
                      {
                          if let headerDict : Dictionary<String, Any> = filteredArray.first
                          {
                              var queryParam : String = ""
                              if let headerTitle : String = headerDict[HolidayDestinationHomeScreenConstants.SearchType] as? String
                              {
                                printLog("\(headerDict)")
                                  queryParam.append("searchType=" + headerTitle)
                                  switch(headerTitle)
                                  {
                                  case "CONTINENT":
                                      queryParam.append("&destination=" + (headerDict[HolidayDestinationHomeScreenConstants.SearchString] as? String ?? ""))
                                  case "CITY":
                                      let cityCode : String = "&destination=" +  (headerDict[HolidayDestinationHomeScreenConstants.LocationCode] as? String ?? "")
                                      let stateCode : String = "&stateCode=" +  (headerDict[HolidayDestinationHomeScreenConstants.StateCode] as? String ?? "")
                                      let countryCode : String = "&countryCode=" +  (headerDict[HolidayDestinationHomeScreenConstants.CountryCode] as? String ?? "")
                                      queryParam.append(cityCode)
                                      queryParam.append(stateCode)
                                      queryParam.append(countryCode)
                                      
                                  case "STATE":
                                      let stateCode : String = "&destination=" +  (headerDict[HolidayDestinationHomeScreenConstants.StateCode] as? String ?? "")
                                      let countryCode : String = "&countryCode=" +  (headerDict[HolidayDestinationHomeScreenConstants.CountryCode] as? String ?? "")
                                      queryParam.append(stateCode)
                                      queryParam.append(countryCode)
                                  case "COUNTRY":
                                      let countryCode : String = "&destination=" +  (headerDict[HolidayDestinationHomeScreenConstants.CountryCode] as? String ?? "")
                                      queryParam.append(countryCode)
                                      
                                  case "THEME":
//                                      queryParam.append("&theme=" + (headerDict[HolidayDestinationHomeScreenConstants.SearchString] as? String ?? ""))
//                                      For Theme
                                    let detsinationName = (headerDict[HolidayDestinationHomeScreenConstants.SearchString] as? String ?? "").uppercased().replacingOccurrences(of: " ", with: "%20")
                                     queryParam.append("&theme=" + detsinationName)
                                  default:
                                      queryParam.append("&destination=" + (headerDict[HolidayDestinationHomeScreenConstants.SearchString] as? String ?? ""))
                                  }
    //                              let srpVC : PackageListViewController = PackageListViewController(nibName: "PackageListViewController", bundle: nil)
    //                              srpVC.titleHeader = "\(headerDict[HolidayDestinationHomeScreenConstants.SearchString] as? String ?? "") Holidays"
                                  let titleHEADER = "\(headerDict[HolidayDestinationHomeScreenConstants.SearchString] as? String ?? "") Holidays"
                                  if queryParam.first == "&"
                                  {
                                      queryParam.removeFirst()
                                  }
    //                              srpVC.queryParam = queryParam
    //                              srpVC.hidesBottomBarWhenPushed = true
                                completion(true,titleHEADER,queryParam)
//                                let srpVC : SRPScreenVC = SRPScreenVC(nibName: "SRPScreenVC", bundle: nil)
//                                srpVC.headerName="\(headerDict[HolidayDestinationHomeScreenConstants.SearchString] as? String ?? "") Holidays";
    //                            srpScreenVC.searchedCity = destinationStr;
    //                            srpScreenVC.holidayArrayInSrp = arrayOfHolidays;
    //                            srpScreenVC.totalPackages=[NSString stringWithFormat:@"%lu",(unsigned long)[arrayOfHolidays count]];
    //                            srpScreenVC.completePackageDetail = packageArray;
    //                              self.navigationController?.pushViewController(srpVC, animated: true)
                              }else{
                                completion(false,"","")
                            }
                          }
                      }else{
                          completion(false,"","")
                      }
                  }else{
                      completion(false,"","")
                  }
                  // self.readJson()
              }
              
          }
}


extension LoginCommNewManager
{
    //checkaccount
    class func checkAccount(requestType :String, queryParam:String?,pathParam :String?,jsonParam:Dictionary<String,Any>?, headers:Dictionary<String, Any>?,requestURL:String,returnInCaseOffailure:Bool,completion : @escaping(_ status:Bool,_ response:Any?) -> Void)
    {
        if AppUtility.checkNetConnection()
        {
             LoadingIndicatorView.show()
            
            UserBookingController.getASTRASessionAndTockenID { (status, responseDict) in
                if status
                {
                    
                    
                    let requestID : String = responseDict?["requestId"] as! String
                    let sessionID: String = responseDict?["tokenId"] as! String
                    UserDefaults.standard.setValue(requestID, forKey: APIConstants.RequestID)
                    UserDefaults.standard.setValue(sessionID, forKey: APIConstants.SessionId)
                    UserDefaults.standard.set(requestID, forKey: kLoginRequestID)
                    UserDefaults.standard.set(sessionID, forKey: kLoginTokenID)
                    var headersDict: HTTPHeaders = [
                        "content-type": "application/json",
                        "Accept": "application/json",
                        APIConstants.SessionId:sessionID,
                        APIConstants.RequestID:requestID,
                        APIConstants.UniqueId:AppUtility.setUUID()
                    ]
                    if let  headerDict  = headers
                    {
                        for (key , value) in headerDict
                        {
                            headersDict.updateValue(value as! String, forKey: key)
                        }
                    }
                    
//                    var urlString:String = AuthenticationConstant.ASTRA_SERVICES_URL + requestURL
                    var urlString:String = kbaseURLForAstra + requestURL
                    if let queryString = queryParam
                    {
                        urlString .append(queryString)
                    }
                    
                    if let pathParamString = pathParam
                    {
                        urlString .append(pathParamString)
                    }
                    
                    
                    var requestTypeString:String
                    
                    if requestType == "GET"
                    {
                        requestTypeString = "GET"
                    }
                    else
                    {
                        requestTypeString = "POST"
                    }
                    
                    
                    request(urlString, method:HTTPMethod(rawValue: requestTypeString)!, parameters: jsonParam, encoding: JSONEncoding.default,headers:headersDict).responseJSON { response in
                        
                        printLog("Request: \(String(describing: response.request ))")   // original url request
                        printLog("Response: \(String(describing: response.response))") // http url response
                        //printLog(String(data: try! JSONSerialization.data(withJSONObject: object, options: .prettyPrinted), encoding: .utf8 )!)
                        
                        
                        printLog("Result: \(response.result)")
                        
                        if let json = response.result.value {
                            //printLog("JSON: \(json)") // serialized json response
                            
                            printLog("JSON: \(String(data: try! JSONSerialization.data(withJSONObject: json, options: .prettyPrinted), encoding: .utf8 )!)")
                            
                            
                            if let userSessionValue:String = response.response?.allHeaderFields[APIResponseConstants.USER_SESSION_ID] as? String
                            {
                                printLog("userSession:\(userSessionValue)")
                                UserDefaults.standard.set(userSessionValue, forKey: APIResponseConstants.USER_SESSION_ID)
                            }
                            
                            if let responseData:Dictionary<String,Any> = json as? Dictionary<String,Any>
                            {
                                completion(true,responseData)
                                return
                            }
                            else if let responseData:Array<Dictionary<String,Any>> = json as? Array<Dictionary<String,Any>>
                            {
                                completion(true,responseData)
                                return
                            }
                            else
                            {
                                if returnInCaseOffailure
                                {
                                    completion(false,nil)
                                }
                                else
                                {
                                    completion(false,"")
                                   self.showFailureAlert()
                                }
                                return
                            }
                        }
                        
                        if returnInCaseOffailure
                        {
                            LoadingIndicatorView.hide()
                            completion(false,"")
                            
                        }
                        else
                        {
                            LoadingIndicatorView.hide()
                            completion(false,"")
                            self.showFailureAlert()
                            
                        }
                        return
                        
                    }
                }
                else
                {
                    LoadingIndicatorView.hide()
                    completion(false,"")
                    self.showFailureAlert()
                }
            }
        }
        else
        {
            LoadingIndicatorView.hide()
            AppUtility.displayAlert(title: "Failure", message: "No Internet Connection")
        }
    }
    
    
    
    //savepass
    class func savePass(requestType :String, queryParam:String?,pathParam :String?,jsonParam:Dictionary<String,Any>?, headers:Dictionary<String, Any>?,requestURL:String,returnInCaseOffailure:Bool,completion : @escaping(_ status:Bool,_ response:Any?) -> Void)
    {
        if AppUtility.checkNetConnection()
        {
//            UserBookingController.getASTRASessionAndTockenID { (status, responseDict) in
//                if status
//                {
                    
//                    let requestID : String = responseDict?["requestId"] as! String
//                    let sessionID: String = responseDict?["tokenId"] as! String
//            UserDefaults.standard.setValue(requestID, forKey: APIConstants.RequestID)
//            UserDefaults.standard.setValue(sessionID, forKey: APIConstants.SessionId)
            
                    let requestID : String = UserDefaults.standard.string(forKey: kLoginRequestID) ?? ""
                    let sessionID: String = UserDefaults.standard.string(forKey: kLoginTokenID) ?? ""
                    
                    
                    // userDict.updateValue(requestID, forKey: APIConstants.RequestID)
                    //userDict.updateValue(sessionID, forKey: APIConstants.SessionId)
                    
                    var headersDict: HTTPHeaders = [
                        "content-type": "application/json",
                        "Accept": "application/json",
                        APIConstants.SessionId:sessionID,
                        APIConstants.RequestID:requestID,
                        APIConstants.UniqueId:AppUtility.setUUID()
                    ]
                    if let  headerDict  = headers
                    {
                        for (key , value) in headerDict
                        {
                            headersDict.updateValue(value as! String, forKey: key)
                        }
                    }
                    
//                    var urlString:String = AuthenticationConstant.ASTRA_SERVICES_URL + requestURL
                    var urlString:String = kbaseURLForAstra + requestURL
                    if let queryString = queryParam
                    {
                        urlString .append(queryString)
                    }
                    
                    if let pathParamString = pathParam
                    {
                        urlString .append(pathParamString)
                    }
                    
                    
                    var requestTypeString:String
                    
                    if requestType == "GET"
                    {
                        requestTypeString = "GET"
                    }
                    else
                    {
                        requestTypeString = "POST"
                    }
                    
                    
                    request(urlString, method:HTTPMethod(rawValue: requestTypeString)!, parameters: jsonParam, encoding: JSONEncoding.default,headers:headersDict).responseJSON { response in
                        
                        printLog("Request: \(String(describing: response.request!))")   // original url request
                        printLog("Response: \(String(describing: response.response!))") // http url response
                        //printLog(String(data: try! JSONSerialization.data(withJSONObject: object, options: .prettyPrinted), encoding: .utf8 )!)
                        
                        printLog("Result: \(response.result)")
                        
                        if let json = response.result.value {
                            //printLog("JSON: \(json)") // serialized json response
                            
                            printLog("JSON: \(String(data: try! JSONSerialization.data(withJSONObject: json, options: .prettyPrinted), encoding: .utf8 )!)")
                            
                            
                            if let responseData:Dictionary<String,Any> = json as? Dictionary<String,Any>
                            {
                                completion(true,responseData)
                                return
                            }
                            else if let responseData:Array<Dictionary<String,Any>> = json as? Array<Dictionary<String,Any>>
                            {
                                completion(true,responseData)
                                return
                            }
                            else
                            {
                                if returnInCaseOffailure
                                {
                                    completion(false,nil)
                                }
                                else
                                {
                                    completion(false,"")
                                    self.showFailureAlert()
                                }
                                return
                                
                            }
                        }
                        
                        if returnInCaseOffailure
                        {
                            completion(false,nil)
                            
                        }
                        else
                        {
                            completion(false,"")
                            self.showFailureAlert()
                            
                        }
                        return
                        
                    }
                    
//                }
//                else
//                {
//                    LoadingIndicatorView.hide()
//                    completion(false,nil)
//                    self.showFailureAlert()
//                }
//            }
        }
        else
        {
            LoadingIndicatorView.hide()
            AppUtility.displayAlert(title: "Failure", message: "No Internet Connection")
        }
    }
    
    //midlware hit get text1, text2, text3
    class func getCheckData(requestType :String, queryParam:String?,pathParam :String?,jsonParam:Dictionary<String,Any>?, headers:Dictionary<String, Any>?,requestURL:String,returnInCaseOffailure:Bool,completion : @escaping(_ status:Bool,_ response:Any?) -> Void)
    {
        if AppUtility.checkNetConnection()
        {
            
            var headersDict: HTTPHeaders = [
                "content-type": "application/json",
                "Accept": "application/json",
                APIConstants.UniqueId:AppUtility.setUUID(),
                "Authorization" : AuthenticationConstant.Mobicule_Authorization
            ]
            if let  headerDict  = headers
            {
                for (key , value) in headerDict
                {
                    headersDict.updateValue(value as! String, forKey: key)
                }
            }
            
          //  var urlString:String = "http://172.98.192.15:8081/Mobicule-Platform/api/"
            
//            var urlString:String = AuthenticationConstant.MOBICULE_URL
            var urlString:String = serverUrl
            if let queryString = queryParam
            {
                urlString .append(queryString)
            }
            
            if let pathParamString = pathParam
            {
                urlString .append(pathParamString)
            }
            
            
            var requestTypeString:String
            
            if requestType == "GET"
            {
                requestTypeString = "GET"
            }
            else
            {
                requestTypeString = "POST"
            }
            
            
            request(urlString, method:HTTPMethod(rawValue: requestTypeString)!, parameters: jsonParam, encoding: JSONEncoding.default,headers:headersDict).responseJSON { response in
                
                printLog("Request: \(String(describing: response.request))")   // original url request
                printLog("Response: \(String(describing: response.response))") // http url response
                //printLog(String(data: try! JSONSerialization.data(withJSONObject: object, options: .prettyPrinted), encoding: .utf8 )!)
                
                
                printLog("Result: \(response.result)")
                
                if let json = response.result.value {
                    //printLog("JSON: \(json)") // serialized json response
                    
                    printLog("JSON: \(String(data: try! JSONSerialization.data(withJSONObject: json, options: .prettyPrinted), encoding: .utf8 )!)")
                    
                    
                    if let responseData:Dictionary<String,Any> = json as? Dictionary<String,Any>
                    {
                        completion(true,responseData)
                        return
                    }
                    else if let responseData:Array<Dictionary<String,Any>> = json as? Array<Dictionary<String,Any>>
                    {
                        completion(true,responseData)
                        return
                    }
                    else
                    {
                        if returnInCaseOffailure
                        {
                            completion(false,nil)
                        }
                        else
                        {
                            self.showFailureAlert()
                            LoadingIndicatorView.hide()
                            completion(false,"")
                        }
                        return
                        
                    }
                }
                
                if returnInCaseOffailure
                {
                    completion(false,nil)
                    
                }
                else
                {
                    self.showFailureAlert()
                    LoadingIndicatorView.hide()
                    completion(false,"")
                }
                
                return
                
            }
            
        }
            
        else
        {
            LoadingIndicatorView.hide()
        }
    }
    
    
    //MARK: - register
    
    class func createUser(requestType :String, queryParam:String?,pathParam :String?,jsonParam:Dictionary<String,Any>?, headers:Dictionary<String, Any>?,requestURL:String,returnInCaseOffailure:Bool,completion : @escaping(_ status:Bool,_ response:Any?) -> Void)
    {
        if AppUtility.checkNetConnection()
        {
            var headersDict: HTTPHeaders = [
                "content-type": "application/json",
                "Accept": "application/json",
                // APIConstants.SessionId:userDict[APIConstants.SessionId] as! String,
                // APIConstants.RequestID:userDict[APIConstants.RequestID] as! String
                APIConstants.RequestID:UserDefaults.standard.string(forKey: APIConstants.RequestID)!,
                APIConstants.SessionId:UserDefaults.standard.string(forKey: APIConstants.SessionId)!,
                APIConstants.UniqueId:AppUtility.setUUID()
             
            ]
            if let  headerDict  = headers
            {
                for (key , value) in headerDict
                {
                    headersDict.updateValue(value as! String, forKey: key)
                }
            }
            
//            var urlString:String = AuthenticationConstant.ASTRA_SERVICES_URL + requestURL
            var urlString:String = kbaseURLForAstra + requestURL
            if let queryString = queryParam
            {
                urlString .append(queryString)
            }
            
            if let pathParamString = pathParam
            {
                urlString .append(pathParamString)
            }
            
            
            var requestTypeString:String
            
            if requestType == "GET"
            {
                requestTypeString = "GET"
            }
            else
            {
                requestTypeString = "POST"
            }
            
            
           //printLog("headers:\(headersDict)")
            //printLog("Request body \(jsonParam)")
            request(urlString, method:HTTPMethod(rawValue: requestTypeString)!, parameters: jsonParam, encoding: JSONEncoding.default,headers:headersDict).responseJSON { response in
                
                
                if let userSessionValue:String = response.response?.allHeaderFields[APIResponseConstants.USER_SESSION_ID] as? String
                {
                    printLog("userSession:\(userSessionValue)")
                    UserDefaults.standard.set(userSessionValue, forKey: APIResponseConstants.USER_SESSION_ID)
                }
                
                if let user:String = response.response?.allHeaderFields[APIConstants.User] as? String
                {
                    printLog("userSession:\(user)")
                    UserDefaults.standard.set(user, forKey: APIConstants.User)
                }
                
                if let userSessionValue:String = response.response?.allHeaderFields[APIResponseConstants.USER_SESSION_ID] as? String
                {
                    printLog("userSession:\(userSessionValue)")
                    UserDefaults.standard.set(userSessionValue, forKey: APIResponseConstants.USER_SESSION_ID)
                }
                
                printLog("Request: \(String(describing: response.request))")   // original url request
                printLog("Response: \(String(describing: response.response))") // http url response
                printLog("Result: \(response.result)")
                
                LoadingIndicatorView.hide()
                printLog("Result: \(response.result)")
                
                if let json = response.result.value {
                    //printLog("JSON: \(json)") // serialized json response
                    
                    printLog("JSON: \(String(data: try! JSONSerialization.data(withJSONObject: json, options: .prettyPrinted), encoding: .utf8 )!)")
                    
                    
                    if let responseData:Dictionary<String,Any> = json as? Dictionary<String,Any>
                    {
                        completion(true,responseData)
                        return
                    }
                    else if let responseData:Array<Dictionary<String,Any>> = json as? Array<Dictionary<String,Any>>
                    {
                        completion(true,responseData)
                        return
                    }
                    else
                    {
                        if returnInCaseOffailure
                        {
                            completion(false,nil)
                        }
                        else
                        {
                            self.showFailureAlert()
                            completion(false,"")
                        }
                        return
                        
                    }
                }
                
                if returnInCaseOffailure
                {
                    completion(false,nil)
                    
                }
                else
                {
                    self.showFailureAlert()
                    completion(false,"")
                }
                return
                
            }
            
            
        }
            
        else
        {
            LoadingIndicatorView.hide()
            self.showFailureAlert()
            completion(false,"")
        }
        
    }
    
}
