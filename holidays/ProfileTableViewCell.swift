//
//  ProfileTableViewCell.swift
//  sotc-consumer-application
//
//  Created by Saurav Kumar on 05/11/17.
//  Copyright © 2017 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit

class ProfileTableViewCell: UITableViewCell {

    @IBOutlet var subTitleLabelDetail: UILabel!
    @IBOutlet var imageViewDetail: UIImageView!
    @IBOutlet var titleLabelDetail: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
