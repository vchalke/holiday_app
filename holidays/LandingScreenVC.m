//
//  LandingScreenVC.m
//  holidays
//
//  Created by Kush_Tech on 22/02/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "LandingScreenVC.h"
#import "BannerCollectionViewCell.h"
#import "RecentCollectionViewCell.h"
#import "AdvertiseCollectionViewCell.h"
#import "LastMinutesDealsCollectionViewCell.h"
#import "TravelBlogCollectionViewCell.h"
#import "BannerImgObject.h"
#import "TravelBlogObject.h"
#import "TravelBlogObj.h"
#import "TopWebVC.h"
#import "BankOffersObject.h"

#import "SlideNavigationController.h"
#import <FirebaseAnalytics/FirebaseAnalytics.h>
#import "BaseViewController.h"
#import "SlideMenuViewController.h"
#import "SearchHolidaysVC.h"
#import "NetCoreAnalyticsVC.h"
#import <NetCorePush/NetCorePush.h>
#import "Reachability.h"
#import "NotificationViewController.h"
#import "RecentCollectionReusableView.h"

#import "FilterPackageVC.h"
#import "NotificationViewController.h"
#import "AppDelegate.h"
#import <CoreData/CoreData.h>
#import "WebViewController.h"
#import "NetCoreAnalyticsVC.h"
#import "SSKeychain.h"
#import "MobiculeUtilityManager.h"
#import "Thomas_Cook_Holidays-Swift.h"
#import "SWRevealViewController.h"
#import "ForexRightSlideViewController.h"
#import "SDWebImageDownloader.h"
#import "PDPScreenVC.h"
#import "HolidayLandingScreenVC.h"
#import "MoreOptionsVC.h"
#import "MyNewProfileVC.h"

#import "MyProfileVc.h"
#import "NewNotificationVC.h"
#import "FilterSCrView.h"
//#import "BookingConfirmOne.swift"
#define KEYCHAIN_SERVICE @"Holiday_App_Keychain"
#define KEYCHAIN_ACCOUNT @"com.thomasCookHoliday.app"

#define kEntity @"holidays"
#define kType   @"webservice"
#define kAction  @"search"
#define kHome  @"Home"
#define kHolidays  @"Holidays"

#define SCREEN_WIDTH_SIZE [UIScreen mainScreen].bounds.size.width
#define SCREEN_HEIGHT_SIZE [UIScreen mainScreen].bounds.size.height
#define SCREEN_WIDTH_PIXEL CGRectGetWidth([UIScreen mainScreen].nativeBounds)
#define SCREEN_HEIGHT_PIXEL CGRectGetHeight([UIScreen mainScreen].nativeBounds)
#define SCREEN_SIZE_TC [UIScreen mainScreen].bounds

@interface LandingScreenVC ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,CustomTopViewDelegaete,BottomViewDelegaete,BottomStacksViewDelegaete,ManageHolidayPopUpVCDelegate>
{
    NSInteger totalArrayCount;
    NSMutableArray *bannerImgDataArray;
    NSMutableArray *bannerTopArray;
    NSMutableArray *bannerWhatsNewArray;
    NSMutableArray *bannerLastMinutesArray;
    NSMutableArray *bankOfferDataArray;
    NSMutableArray *travelBlogDataArray;
    NSArray *recentHolidayObjArray;
     LoadingView *activityLoadingViewRT;
    int visibleBannerItem;
    BOOL isAlloeScroll;
    
    NSMutableArray *hLandingSummerArray;
    NSMutableArray *hLandingAventureArray;
    NSMutableArray *hLandingUpcomingArray;
    NSMutableArray *hLandingCheeryBlossomArray;
    NSMutableArray *hLandingPopularDestinationArray;
    NSMutableArray *hLandingCategoryArray;
    NSMutableArray *hLandingHolidayOneArray;
}
@end

@implementation LandingScreenVC

- (void)viewDidLoad {
    [super viewDidLoad];
    totalArrayCount = 7;
    
    // Do any additional setup after loading the view from its nib.
    self.banner_CollectionView.backgroundColor = [UIColor clearColor];
    self.whtasNew_CollectionView.backgroundColor = [UIColor clearColor];
    [self registerNibInCollectionView];
    payloadList =  [NSMutableDictionary dictionary];
    self.bannerPageControl.numberOfPages = totalArrayCount;
    self.whtasNewPageCtrl_CollectionView.numberOfPages = totalArrayCount;
    self.customTopView.custDelegate = self;
//    self.customBottomView.bottomDelegate= self;
    self.customBottomView.bottomStackDelegate= self;
   
    [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@""] forKey:kCurrentTransactionId];
    [self showRecentViewHoliday];
    [self setArray];
    [self callAllViewAPI];
    [self setDistanceBetweenBanners];
//    [self performSelector:@selector(checkUpdateAvaialble) withObject:nil afterDelay:1.0];
}
-(void)registerNibInCollectionView{
    UINib *headerNib = [UINib nibWithNibName:@"RecentCollectionReusableView" bundle:nil];
    [self.recentlyAdded_CollectionView registerNib:headerNib forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"RecentCollectionReusableView"];
    [self.banner_CollectionView registerNib:[UINib nibWithNibName:@"BannerCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"BannerCollectionViewCell"];
    [self.recentlyAdded_CollectionView registerNib:[UINib nibWithNibName:@"RecentCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"RecentCollectionViewCell"];
    [self.whtasNew_CollectionView registerNib:[UINib nibWithNibName:@"BannerCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"BannerCollectionViewCell"];
    [self.advertise_CollectionView registerNib:[UINib nibWithNibName:@"AdvertiseCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"AdvertiseCollectionViewCell"];
    [self.lastMinutesDeals_CollectionView registerNib:[UINib nibWithNibName:@"LastMinutesDealsCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"LastMinutesDealsCollectionViewCell"];
    [self.travelVlog_CollectionView registerNib:[UINib nibWithNibName:@"TravelBlogCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"TravelBlogCollectionViewCell"];
}
-(void)setDistanceBetweenBanners{
    CGFloat upperHeight = [[PixelSingleton sharedInstance]sizeInPixel:100.0 withBase:1980.0];
    NSLog(@"Main Upper Height Pixel Size %0.2f",upperHeight);
    CGFloat lowerHeight = [[PixelSingleton sharedInstance]sizeInPixel:45.0 withBase:1980.0];
    NSLog(@"Main Bottom Height Pixel Size %0.2f",lowerHeight);
//    self.cnst_topToWhatsNew.constant = self.cnst_topToLastMinDeal.constant = self.cnst_topToTravelBlog.constant = upperHeight;
//    self.cnst_bottomToWhatsNew.constant = self.cnst_bottomToLastMinDeal.constant = self.cnst_bottomToTravelBlog.constant = lowerHeight;
    
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    self.navigationController.navigationBar.hidden = YES;
//    NSLog(@"AppUtility.setUUID %@",AppUtility.setUUID);
    [self disableBackSwipeAllow:NO];
    NSLog(@"User Booking Number %@",[[NSUserDefaults standardUserDefaults]valueForKey:@"mobicleNumber"]);
//    NSLog(@"Main Screen Bounds %id %0.2f %0.2f",SCREEN_SIZE_TC,SCREEN_WIDTH_SIZE,SCREEN_HEIGHT_SIZE);
//    NSLog(@"Main Screen Pixels %id %0.2f %0.2f",SCREEN_SIZE_TC,SCREEN_WIDTH_PIXEL,SCREEN_HEIGHT_PIXEL);
//    NSLog(@"Main Pixel Size %0.2f",[[PixelSingleton sharedInstance]sizeInPixel:100.0 withBase:1980.0]);
}

-(void)callAllViewAPI{
    dispatch_async(dispatch_get_main_queue(), ^(void) {
            [self getTokenIDInLandingScreenVC];
            [self performSelector:@selector(getBannerDataFromAPI) withObject:nil afterDelay:1.0];
            [self performSelector:@selector(getBannerServiceAPI) withObject:nil afterDelay:1.0];
            [self performSelector:@selector(getTravelBlogDataFromAPI) withObject:nil afterDelay:1.0];
            [self performSelector:@selector(getBankOffersAPI) withObject:nil afterDelay:1.0];
            
    //        [self performSelector:@selector(getDataFromCustomerProfileAPI) withObject:nil afterDelay:1.0];
        });
}
-(void)setArray{
    bannerImgDataArray = [[NSMutableArray alloc]init];
       bannerWhatsNewArray= [[NSMutableArray alloc]init];
       bannerLastMinutesArray= [[NSMutableArray alloc]init];
       travelBlogDataArray = [[NSMutableArray alloc]init];
       bankOfferDataArray = [[NSMutableArray alloc]init];
     bannerTopArray = [[NSMutableArray alloc]init];
    
     hLandingSummerArray = [[NSMutableArray alloc]init];
    hLandingAventureArray = [[NSMutableArray alloc]init];
    hLandingUpcomingArray = [[NSMutableArray alloc]init];
    hLandingCheeryBlossomArray = [[NSMutableArray alloc]init];
    hLandingCategoryArray = [[NSMutableArray alloc]init];
    hLandingPopularDestinationArray = [[NSMutableArray alloc]init];
    hLandingHolidayOneArray = [[NSMutableArray alloc]init];
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    self.navigationController.navigationBar.hidden = YES;
    [self.customBottomView buttonSelected:0];
    [self showRecentViewHoliday];
    [self.recentlyAdded_CollectionView reloadData];
    self.customBottomView.frame = CGRectMake(self.bottomBaseView.frame.origin.x, self.bottomBaseView.frame.origin.y+2, self.bottomBaseView.frame.size.width-60, self.bottomBaseView.frame.size.height-2);
    [self.bottomBaseView addSubview:self.customBottomView];
}
-(void)showRecentViewHoliday{
    
    recentHolidayObjArray = [[[[CoreDataSingleton sharedInstance] getArrayOfObjectForEntityName:RecentViewHoliday] reverseObjectEnumerator] allObjects];
    self.cnst_topOfRecentView.constant = ([recentHolidayObjArray count] > 0) ? 20 : 0;
    self.cnst_heightOfRecentView.constant = ([recentHolidayObjArray count] > 0) ? 250 : 0;
//    self.cnst_heightOfRecentView.constant = ([recentHolidayObjArray count] > 0) ? [[PixelSingleton sharedInstance]sizeInPixel:900.0 withBase:1980.0] : 0;
    self.recent_view.hidden = ([recentHolidayObjArray count] > 0) ? NO : YES;
}
    
-(void)showAnimationOfRecentAddedViews:(BOOL)flag{
    
//    [UIView animateWithDuration:2.0 delay:0.5 options:(flag==YES)? UIViewAnimationOptionCurveEaseIn:UIViewAnimationOptionCurveEaseOut animations:^{
//        
//    } completion:^(BOOL finished) {
//        self.const_RecentlyViewLbl.constant = (flag==YES)? 0 : 120;
//        } completion:nil];
    
}
-(void)showAnimationOfWithValue:(CGFloat)value{
    [UIView transitionWithView:self.whtasNewPageCtrl_CollectionView duration:5.0 options:UIViewAnimationOptionCurveEaseInOut animations:^(void){
        self.const_RecentlyViewLbl.constant = 120-value;
    } completion:nil];
}
#pragma mark - Collection View Delegates And DataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView == self.banner_CollectionView && bannerTopArray.count>0){// bannerImgDataArray
        return [bannerTopArray count];// bannerImgDataArray
    }else if (collectionView == self.travelVlog_CollectionView && travelBlogDataArray.count>0){
        return [travelBlogDataArray count];
    }else if (collectionView == self.recentlyAdded_CollectionView){
        return [recentHolidayObjArray count];
    }else if (collectionView == self.whtasNew_CollectionView && bannerWhatsNewArray.count>0){
        return [bannerWhatsNewArray count];
    }else if (collectionView == self.lastMinutesDeals_CollectionView && bannerLastMinutesArray.count>0){
        return [bannerLastMinutesArray count];
    }else if (collectionView == self.advertise_CollectionView && bankOfferDataArray.count>0){
        return [bankOfferDataArray count];
    }
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == self.banner_CollectionView){
    BannerCollectionViewCell *cell= (BannerCollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"BannerCollectionViewCell" forIndexPath:indexPath];
//        if ([bannerImgDataArray count]>0){
//            BannerImgObject *bannerObj = [[BannerImgObject alloc] initWithImgArray:[bannerImgDataArray objectAtIndex:indexPath.row]];
//            [cell showBannerDetails:bannerObj];
//        }
        if ([bannerWhatsNewArray count]>0){
            WhatsNewObject *bannerObj = [[WhatsNewObject alloc] initWithImgArray:[bannerTopArray objectAtIndex:indexPath.row]];
            [cell showWhatsNewDetails:bannerObj withData:NO];
        }
        cell.lbl_Title.text = @"";
        cell.lbl_SubTitle.text = @"";
        isAlloeScroll = NO;
        return cell;
        
    }else if (collectionView == self.recentlyAdded_CollectionView){
    RecentCollectionViewCell *cell= (RecentCollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"RecentCollectionViewCell" forIndexPath:indexPath];
        RecentHolidayObject *recentObj = [[RecentHolidayObject alloc]initWithRecentObject:[recentHolidayObjArray objectAtIndex:indexPath.row]];
        [cell loadObjectInCell:recentObj];
        isAlloeScroll = (indexPath.row>=1) ? YES : NO ;
//        [self showAnimationOfRecentAddedViews:indexPath.row>2];
        return cell;
    }else if (collectionView == self.whtasNew_CollectionView){
    BannerCollectionViewCell *cell= (BannerCollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"BannerCollectionViewCell" forIndexPath:indexPath];
        if ([bannerWhatsNewArray count]>0){
            WhatsNewObject *bannerObj = [[WhatsNewObject alloc] initWithImgArray:[bannerWhatsNewArray objectAtIndex:indexPath.row]];
            [cell showWhatsNewDetails:bannerObj withData:NO];
            NSLog(@"WhatsNewObject %@",bannerObj.bannerLevelName);
            self.lbl_WhatsNew.text = bannerObj.bannerLevelName;
        }
        isAlloeScroll = NO;
//        cell.lbl_Title.text = @"Adventure HOLIDAYS";
//        cell.lbl_SubTitle.text = @"Manali | Kullu";
        return cell;
        
    }else if (collectionView == self.advertise_CollectionView){
    AdvertiseCollectionViewCell *cell= (AdvertiseCollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"AdvertiseCollectionViewCell" forIndexPath:indexPath];
        if ([bankOfferDataArray count]>0){
            BankOffersObject *bankObj = [[BankOffersObject alloc] initWithBankDataArray:[bankOfferDataArray objectAtIndex:indexPath.row]];
            [cell setData:bankObj];
        }
       
        isAlloeScroll = NO;
        return cell;
    }else if (collectionView == self.lastMinutesDeals_CollectionView){
    LastMinutesDealsCollectionViewCell *cell= (LastMinutesDealsCollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"LastMinutesDealsCollectionViewCell" forIndexPath:indexPath];
        if ([bannerLastMinutesArray count]>0){
            WhatsNewObject *bannerObj = [[WhatsNewObject alloc] initWithImgArray:[bannerLastMinutesArray objectAtIndex:indexPath.row]];
            [cell showLastMinutesDeals:bannerObj];
            NSLog(@"LastMinutesDealsCollectionViewCell %@",bannerObj.bannerLevelName);
            self.lbl_LastMinuteDeals.text = bannerObj.bannerLevelName;
        }
        isAlloeScroll = NO;
        return cell;
    }else if (collectionView == self.travelVlog_CollectionView){
    TravelBlogCollectionViewCell *cell= (TravelBlogCollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"TravelBlogCollectionViewCell" forIndexPath:indexPath];
         if ([travelBlogDataArray count]>0){
        TravelBlogObj *travelObject = [[TravelBlogObj alloc]initWithTravelArray:[travelBlogDataArray objectAtIndex:indexPath.row]];
        [cell loadTravelBlogObject:travelObject];
         }
        isAlloeScroll = NO;
        return cell;
    }
    
    return nil;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (collectionView == self.banner_CollectionView){
    return CGSizeMake(self.banner_CollectionView.frame.size.width, self.banner_CollectionView.frame.size.height);
        
    }else if (collectionView == self.recentlyAdded_CollectionView){
        return CGSizeMake(self.recentlyAdded_CollectionView.frame.size.width*0.45, self.recentlyAdded_CollectionView.frame.size.height*0.85);
    }else if (collectionView == self.whtasNew_CollectionView){
    return CGSizeMake(self.whtasNew_CollectionView.frame.size.width, self.whtasNew_CollectionView.frame.size.height);
    }else if (collectionView == self.advertise_CollectionView){
//        return CGSizeMake(self.advertise_CollectionView.frame.size.width*0.6, self.advertise_CollectionView.frame.size.height*0.85);
        return CGSizeMake(self.advertise_CollectionView.frame.size.width*0.85, self.advertise_CollectionView.frame.size.height*0.85);
    }else if (collectionView == self.lastMinutesDeals_CollectionView){
        return CGSizeMake(self.lastMinutesDeals_CollectionView.frame.size.width*0.8, self.lastMinutesDeals_CollectionView.frame.size.height*0.92);
    }else if (collectionView == self.travelVlog_CollectionView){
        return CGSizeMake(self.travelVlog_CollectionView.frame.size.width*0.52, self.travelVlog_CollectionView.frame.size.height);
//        return CGSizeMake(self.travelVlog_CollectionView.frame.size.height*0.9, self.travelVlog_CollectionView.frame.size.height*0.9);
    }
    return CGSizeMake(100, 100);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView == self.banner_CollectionView && [bannerTopArray count]>0){// bannerImgDataArray
        //        BannerImgObject *bannerObj = [[BannerImgObject alloc] initWithImgArray:[bannerTopArray objectAtIndex:indexPath.row]];
        //        NSString *myString  = [bannerObj.bannerName stringByReplacingOccurrencesOfString:@"- IOS" withString:@""];
        //        [self jumpToBannerWebView:bannerObj.redirectUrl withTitle:myString withObject:nil];
        WhatsNewObject *bannerObj = [[WhatsNewObject alloc] initWithImgArray:[bannerTopArray objectAtIndex:indexPath.row]];
        [self jumpToBannerWebView:bannerObj.redirectUrl withTitle:bannerObj.bannerName withObject:bannerObj];
    }else if (collectionView == self.recentlyAdded_CollectionView){
        [self jumpToPDPScreen:indexPath.row];
    }else if (collectionView == self.whtasNew_CollectionView && [bannerWhatsNewArray count]>0){
        WhatsNewObject *bannerObj = [[WhatsNewObject alloc] initWithImgArray:[bannerWhatsNewArray objectAtIndex:indexPath.row]];
//        NSString *myString  = [bannerObj.bannerName stringByReplacingOccurrencesOfString:@"- IOS" withString:@""];
            [self jumpToBannerWebView:bannerObj.redirectUrl withTitle:bannerObj.bannerName withObject:bannerObj];
    }else if (collectionView == self.lastMinutesDeals_CollectionView && [bannerLastMinutesArray count]>0){
            WhatsNewObject *bannerObj = [[WhatsNewObject alloc] initWithImgArray:[bannerLastMinutesArray objectAtIndex:indexPath.row]];
    //        NSString *myString  = [bannerObj.bannerName stringByReplacingOccurrencesOfString:@"- IOS" withString:@""];
                [self jumpToBannerWebView:bannerObj.redirectUrl withTitle:bannerObj.bannerName withObject:bannerObj];
        }else if ( collectionView == self.travelVlog_CollectionView&& [travelBlogDataArray count]>0){
        TravelBlogObj *travelObject = [[TravelBlogObj alloc]initWithTravelArray:[travelBlogDataArray objectAtIndex:indexPath.row]];
        [self jumpToBannerWebView:travelObject.link withTitle:travelObject.title withObject:nil];
    }
    
}
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView
           viewForSupplementaryElementOfKind:(NSString *)kind
                                 atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableview = nil;
    if (collectionView == self.recentlyAdded_CollectionView && kind == UICollectionElementKindSectionHeader){
        UICollectionReusableView * reusableview =
        [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader
                               withReuseIdentifier:@"RecentCollectionReusableView"
                                      forIndexPath:indexPath];
    return reusableview;
    }
    return reusableview;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    if (collectionView == self.recentlyAdded_CollectionView){
    return CGSizeMake(120.0,  self.recentlyAdded_CollectionView.frame.size.height);
    }
    return CGSizeMake(0.0,0.0);
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    for (UICollectionViewCell *cell in [self.banner_CollectionView visibleCells]) {
        NSIndexPath *indexPath = [self.banner_CollectionView indexPathForCell:cell];
        self.bannerPageControl.currentPage = indexPath.item;
    }
    for (UICollectionViewCell *cell in [self.whtasNew_CollectionView visibleCells]) {
        NSIndexPath *indexPath = [self.whtasNew_CollectionView indexPathForCell:cell];
        self.whtasNewPageCtrl_CollectionView.currentPage = indexPath.item;
    }
}

//- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
//    for (UICollectionViewCell *cell in [self.recentlyAdded_CollectionView visibleCells]) {
//        NSLog(@"%0.2f %0.2f",cell.frame.origin.x,cell.frame.origin.y);
//        CGFloat offsetX = scrollView.contentOffset.x;
//        [self showAnimationOfWithValue:(offsetX <= 10) ? 0 : (offsetX <= 120) ? scrollView.contentOffset.x:120];
//        NSLog(@"%0.2f %0.2f",scrollView.contentOffset.x,scrollView.contentOffset.y);
//    }
//}

//- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
//    if (collectionView == self.recentlyAdded_CollectionView) { //it's your last cell
//       CGFloat offsetX = collectionView.contentOffset.x;
//               [self showAnimationOfWithValue:(offsetX <= 10) ? 0 : (offsetX <= 120) ? collectionView.contentOffset.x:120];
//       NSLog(@"%0.2f %0.2f",collectionView.contentOffset.x,collectionView.contentOffset.y);
//    }
//}
-(void)jumpToPDPScreen:(NSInteger)index{

    RecentHolidayObject *recentObj = [[RecentHolidayObject alloc]initWithRecentObject:[recentHolidayObjArray objectAtIndex:index]];
    
    
    Holiday *objHoliday=[[Holiday alloc]init];
    objHoliday.strPackageId=recentObj.packageID;
    objHoliday.strPackageName=recentObj.packageName;
    NSLog(@"%@",recentObj.packageID);
    NSLog(@"%@",recentObj.packageName);
//    [self netCoreDataDisplaySearch];
    [self jumpToPdpFromLandingScreen:objHoliday];
}


#pragma mark - Jump To Web Views
-(void)jumpToBannerWebView:(NSString *)webUrlString withTitle:(NSString *)webViewTitle withObject:(WhatsNewObject *)newObj{
    if (![webUrlString isEqualToString:@"NA"]){
        TopWebVC *controler = [[TopWebVC alloc] initWithNibName:@"TopWebVC" bundle:nil];
        controler.web_url_string = webUrlString;
        controler.web_title = webViewTitle;
        [self.navigationController pushViewController:controler animated:YES];
    }else if (newObj){
        if ([[newObj.bannerType uppercaseString] isEqualToString:@"PACKAGE"]){
            Holiday *objHoliday=[[Holiday alloc]init];
            objHoliday.strPackageId=newObj.packageID;
            objHoliday.strPackageName=newObj.bannerName;
            [self jumpToPdpFromLandingScreen:objHoliday];
        }else if ([[newObj.bannerType uppercaseString] isEqualToString:@"DESTINATION"]){
//            if ([[newObj.bannerLevelName uppercaseString] isEqualToString:[kserviceWhatsNew uppercaseString]] || [[newObj.bannerLevelName uppercaseString] isEqualToString:[kserviceTopBanners uppercaseString]]){
//                [self searchDestinationWithSearchType:@"THEME" withDestName:newObj.destinationName];
//            }
            [self jumpToDestinationWiseDictObjcet:newObj];
        }
    }
}
-(void)jumpToDestinationWiseDictObjcet:(WhatsNewObject*)newObject{
//    NSString *destinationName = [[newObject.destinationName uppercaseString] stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    NSDictionary *searchictionary = @{
        @"bannerDuration": newObject.bannerDuration,
        @"bannerLargeImage": newObject.bannerLargeImage,
        @"bannerLevelName": newObject.bannerLevelName,
        @"bannerLevelType": newObject.bannerLevelType,
        @"bannerMediumImage": newObject.bannerMediumImage,
        @"bannerName": newObject.bannerName,
//        @"bannerOrder": newObject.bannerOrder,
//        @"orderOnThePage": newObject.orderOnThePage,
        @"bannerPageName": newObject.bannerPageName,
        @"bannerSmallImage": newObject.bannerSmallImage,
        @"bannerStartingPrice": newObject.bannerStartingPrice,
        @"bannerStrikeoutPrice": newObject.bannerStrikeoutPrice,
        @"bannerType": newObject.bannerType,
        @"destinationName": newObject.destinationName,
//        @"destinationName": destinationName,
        @"redirectUrl": newObject.redirectUrl
    };
    
    /*
    NSDictionary *searchictionary = @{
      @"bannerDuration": @"NA",
      @"bannerLargeImage": @"https://resources-uatastra.thomascook.in/images/holidays/BannerAPI/Mauritius.jpeg",
      @"bannerLevelName": @"Popular Destinations",
      @"bannerLevelType": @" Level 5",
      @"bannerMediumImage": @"https://resources-uatastra.thomascook.in/images/holidays/BannerAPI/Mauritius.jpeg",
      @"bannerName": @"Mauritius",
      @"bannerOrder": @4,
      @"bannerPageName": @"Holidays",
      @"bannerSmallImage": @"https://resources-uatastra.thomascook.in/images/holidays/BannerAPI/Mauritius.jpeg",
      @"bannerStartingPrice": @"NA",
      @"bannerStrikeoutPrice": @"NA",
      @"bannerType": @"destination",
      @"destinationName": @"Mauritius",
      @"orderOnThePage": @6,
      @"redirectUrl": @"NA"
    };
    */
    [LoginCommNewManager searchPackagesWithSelectedDict:searchictionary completion:^(BOOL flag, NSString * title, NSString * query) {
        if(query.length > 0){
        [self searchDestinationSelectDictUsingQueryParamwithText:query withTitleHeader:title];
        }else{
            [self showAlertViewWithTitle:@"Failure" withMessage:@"Sorry, we could not find any package for the given search. Please try with a different search criteria."];
        }
    }];
     
}

#pragma mark - Top View Delegate
-(void)jumpToWebView:(NSString *)webUrlString withTitle:(NSString *)webViewTitle{
    TopWebVC *controler = [[TopWebVC alloc] initWithNibName:@"TopWebVC" bundle:nil];
    controler.web_url_string = webUrlString;
    controler.web_title = webViewTitle;
    [self.navigationController pushViewController:controler animated:YES];
}
#pragma mark - Master Method Jump
-(void)jumpToControllerfromViewNum:(NSInteger)firstVCNum toViewNum:(NSInteger)secVCNum{
      CATransition *transition = [CATransition animation];
    transition.duration = 0.5;
      transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault];
      transition.type = kCATransitionMoveIn;
    transition.subtype = (firstVCNum < secVCNum) ? kCATransitionFromRight : kCATransitionFromLeft;
    [self.navigationController.view.layer addAnimation:transition forKey:nil];
      self.navigationController.navigationBarHidden = NO;
    switch (secVCNum) {
        case 0:{
            LandingScreenVC *landingVC=[[LandingScreenVC alloc]initWithNibName:@"LandingScreenVC" bundle:nil];
            [self.navigationController pushViewController:landingVC animated:YES];
            break;
        }
        case 1:{
//            UserBookingHomeViewController *loginSotc = [[UserBookingHomeViewController alloc] initWithNibName:@"BaseViewController" bundle:nil];
            NSString *bookingMobNumber = [[NSUserDefaults standardUserDefaults]valueForKey:@"mobicleNumber"];
            if (bookingMobNumber == nil || [bookingMobNumber isEqualToString:@""] || bookingMobNumber.length==0){
               SOTCLoginViewController *loginSotc = [[SOTCLoginViewController alloc] initWithNibName:@"BaseViewController" bundle:nil];
                [self.navigationController pushViewController:loginSotc animated:YES];
            }else{
                UserBookingHomeViewController *loginSotc = [[UserBookingHomeViewController alloc] initWithNibName:@"BaseViewController" bundle:nil];
                [self.navigationController pushViewController:loginSotc animated:YES];
            }
            break;
        }
        case 2:{
//            MyNewProfileVC *profileVC=[[MyNewProfileVC alloc]initWithNibName:@"MyNewProfileVC" bundle:nil];
            CustomerProfileVC *profileVC=[[CustomerProfileVC alloc]initWithNibName:@"CustomerProfileVC" bundle:nil];
//            ChangeFlightsVC *profileVC=[[ChangeFlightsVC alloc]initWithNibName:@"ChangeFlightsVC" bundle:nil];
            [self.navigationController pushViewController:profileVC animated:YES];
            break;
        }
        case 3:{
            NewNotificationVC *notifVC=[[NewNotificationVC alloc]initWithNibName:@"NewNotificationVC" bundle:nil];
//            NotificationViewController *notifVC = [[NotificationViewController alloc]initWithNibName:@"BaseViewController" bundle:nil];
            [self.navigationController pushViewController:notifVC animated:YES];
            break;}
        case 4:{
            MoreOptionsVC *optionVC=[[MoreOptionsVC alloc]initWithNibName:@"MoreOptionsVC" bundle:nil];
            [self.navigationController pushViewController:optionVC animated:YES];
            break;}
        default:
           break;
    }
}
#pragma mark - Bottom View Delegate
-(void)homeButtonClick{
    
}
-(void)bookingButtonClick{
/*
    [AppUtility checkIsVersionUpgrade];
    
    NSUserDefaults *LoginStatus = [NSUserDefaults standardUserDefaults];
    
    if (![[LoginStatus valueForKey:@"isCSSUserLoggedIn"] boolValue]) {
        
        // [AppUtility setLoginController];
        dispatch_async(dispatch_get_main_queue(), ^(void) {
        [self onSelfServiceMenuClick];
        });
    }
    else
    {
        [AppUtility setTabbarController];
    }
    
    payloadList =  [NSMutableDictionary dictionary];
    [payloadList setObject:@"yes" forKey:@"s^SELF_SERVICE"];
    [self netCoreHomePageLogEvent];
  */
    [self jumpToControllerfromViewNum:0 toViewNum:1];
}
-(void)profileButtonClick{
//    MyNewProfileVC *profileVC=[[MyNewProfileVC alloc]initWithNibName:@"MyNewProfileVC" bundle:nil];
//    [self.navigationController pushViewController:profileVC animated:YES];
    [self jumpToControllerfromViewNum:0 toViewNum:2];
}
-(void)notificationButtonClick{
//    NSArray *arrofNotifications = [[NetCoreSharedManager sharedInstance] getNotifications];
//    NotificationViewController *calenderVC = [[NotificationViewController alloc]initWithNibName:@"BaseViewController" bundle:nil];
//    [[SlideNavigationController sharedInstance] pushViewController:calenderVC animated:YES];
    payloadList =  [NSMutableDictionary dictionary];
    [payloadList setObject:@"yes" forKey:@"s^NOTIFICATIONS"];
    [self netCoreHomePageLogEvent];
    [self jumpToControllerfromViewNum:0 toViewNum:3];
}

-(void)moreButtonClick{
    [self jumpToControllerfromViewNum:0 toViewNum:4];
    
//    FilterSCrView *controller = [[FilterSCrView alloc] initWithNibName:@"FilterSCrView" bundle:nil];
//    [self.navigationController pushViewController:controller animated:YES];
    
//        BookingConfirmOne *booking=[[BookingConfirmOne alloc]initWithNibName:@"BookingConfirmOne" bundle:nil];
//        [self.navigationController pushViewController:booking animated:YES];
    
//    CustomerProfileVC *profile=[[CustomerProfileVC alloc]initWithNibName:@"CustomerProfileVC" bundle:nil];
//    [self.navigationController pushViewController:profile animated:YES];
}
#pragma mark - Other Actions Button
-(void)searchButtonClick{
    [FIRAnalytics logEventWithName:@"Home_Page_Find_Holidays" parameters:@{kFIRParameterItemID :@"On click of search button on Home Page"}];
    payloadList =  [NSMutableDictionary dictionary];
    [payloadList setObject:@"yes" forKey:@"s^SEARCH"];
    [self netCoreHomePageLogEvent];
    
//        SearchHolidaysVC *searchVC=[[SearchHolidaysVC alloc]initWithNibName:@"BaseViewController" bundle:nil];
//        searchVC.boolForSearchPackgScreen=true;
//        searchVC.headerName = kIndianHoliday;
//        [[SlideNavigationController sharedInstance] pushViewController:searchVC animated:YES];
    NSLog(@"bankOfferDataArray %ld",[bankOfferDataArray count]);
    HolidayLandingScreenVC *searchVC=[[HolidayLandingScreenVC alloc]initWithNibName:@"HolidayLandingScreenVC" bundle:nil];
    searchVC.boolForSearchPackgScreen=true;
    searchVC.headerName = kIndianHoliday;
    searchVC.bankOffersArray = [bankOfferDataArray copy];
    searchVC.whatsNewArray= [bannerWhatsNewArray copy];
    searchVC.topBannerArray = [bannerTopArray copy];
    searchVC.summerArray = [hLandingSummerArray copy];
    searchVC.adventureArray = [hLandingAventureArray copy];
    searchVC.upcomingArray = [hLandingUpcomingArray copy];
    searchVC.cheeryBlossomArray = [hLandingCheeryBlossomArray copy];
    searchVC.popularDestinationArray = [hLandingPopularDestinationArray copy];
    searchVC.categoryArray = [hLandingCategoryArray copy];
    searchVC.holidayOneArray = [hLandingHolidayOneArray copy];
    [self.navigationController pushViewController:searchVC animated:YES];
}

/*
-(void)profileButtonClicked{

    NSUserDefaults *LoginStatus=[NSUserDefaults standardUserDefaults];
    NSString *statusForLogin= [LoginStatus valueForKey:kLoginStatus];
    
    NSUserDefaults *userIdForGooglePlusSignIn=[NSUserDefaults standardUserDefaults];
    NSString *userId=[userIdForGooglePlusSignIn valueForKey:kLoginEmailId];
    
    if ([statusForLogin isEqualToString:kLoginSuccess])
        {
            [FIRAnalytics logEventWithName:@"Sign_Out" parameters:@{kFIRParameterItemID:@"Sign Out"}];
            
            [[NetCoreInstallation sharedInstance] netCorePushLogout:^(NSInteger statusCode) {
                
                NSLog(@"User Logged out from netcore!!");
            }];
           
            NSUserDefaults *LoginStatus=[NSUserDefaults standardUserDefaults];
            [LoginStatus setObject:kLoginFailed forKey:kLoginStatus];
            
          CoreUtility *coreObj=[CoreUtility sharedclassname];
            
            if (coreObj != nil && ![coreObj isEqual:[NSNull new]] && coreObj.strPhoneNumber != nil && ![coreObj.strPhoneNumber isEqual:[NSNull new]] && coreObj.strPhoneNumber.count > 0)
            {
                [coreObj.strPhoneNumber removeAllObjects];
                
            }
            
            [LoginStatus setBool:false forKey:@"isRegisterForPushNotification"];
            
            [LoginStatus setBool:false forKey:@"isCSSUserLoggedIn"];
            
            [LoginStatus setObject:nil forKey:@"mobicleNumber"];
            
            [LoginStatus setObject:@"" forKey:@"userDefaultTC-LoginMobileNumber"];
            
            [LoginStatus setObject:@"" forKey:@"Holidays_TextField_MobicleNumber"];
            
            //NSUserDefaults *userIdForGooglePlusSignIn=[NSUserDefaults standardUserDefaults];
            [LoginStatus removeObjectForKey:kLoginEmailId];
            
            [LoginStatus removeObjectForKey:kPhoneNo];
            
           [[NSUserDefaults standardUserDefaults] synchronize];
            
            [FIRAnalytics logEventWithName:@"Home" parameters:@{kFIRParameterItemID : @"Home"}];
            
            [[SlideNavigationController sharedInstance] popToRootViewControllerAnimated:NO];
            
            [payloadList setObject:@"yes" forKey:@"s^HOME"];
            
            
        }else{
            NewLoginPopUpViewController *loginView = [[NewLoginPopUpViewController alloc] initWithNibName:@"NewLoginPopUpViewController" bundle:nil];
            [[SlideNavigationController sharedInstance] toggleLeftMenu];
             UINavigationController * loginNavigation = [[UINavigationController alloc] initWithRootViewController:loginView];
            [[SlideNavigationController sharedInstance]presentViewController:loginNavigation animated:true completion:^{
            }];
            
        }
    
}
*/
#pragma mark - ManageHolidayPopUpVC Delegate
-(void)onSelfServiceMenuClick
{
    [AppUtility checkIsVersionUpgrade];
    
    ManageHolidayPopUpVC* manageHolidayPopUpVC =  [[ManageHolidayPopUpVC alloc] initWithNibName:@"ManageHolidayPopUpVC" bundle:nil];
    manageHolidayPopUpVC.htmlPath = @"Manage Holidays";
    manageHolidayPopUpVC.popuptitle = @"Manage Your Booking";
    manageHolidayPopUpVC.managedHolidayDelegate = self;
    [manageHolidayPopUpVC setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:manageHolidayPopUpVC animated:false completion:nil];
}
-(void)onContinue
{
//        [AppUtility setLoginController];
    
     SOTCLoginViewController *loginSotc = [[SOTCLoginViewController alloc] initWithNibName:@"BaseViewController" bundle:nil];
    [self.navigationController pushViewController:loginSotc animated:YES];
}

-(void)onCancle
{
    
}
#pragma mark - Top View Delegate

-(void)netCoreHomePageLogEvent
{
    
    if(![payloadList objectForKey:@"s^BANNER"])
    {
        [payloadList setObject:@"" forKey:@"s^BANNER"];
    }
    if (![payloadList objectForKey:@"s^SEARCH"])
    {
        [payloadList setObject:@"no" forKey:@"s^SEARCH"];
    }
    if (![payloadList objectForKey:@"s^DEALS"])
    {
        [payloadList setObject:@"no" forKey:@"s^DEALS"];
    }
    if (![payloadList objectForKey:@"s^DRAWER"])
    {
        [payloadList setObject:@"no" forKey:@"s^DRAWER"];
    }
    if (![payloadList objectForKey:@"s^NOTIFICATIONS"])
    {
        [payloadList setObject:@"no" forKey:@"s^NOTIFICATIONS"];
    }
    if (![payloadList objectForKey:@"s^INT_HOLIDAYS"])
    {
        [payloadList setObject:@"no" forKey:@"s^INT_HOLIDAYS"];
    }
    if (![payloadList objectForKey:@"s^INDIA_HOLIDAYS"])
    {
        [payloadList setObject:@"no" forKey:@"s^INDIA_HOLIDAYS"];
    }
    if (![payloadList objectForKey:@"s^FLIGHTS"])
    {
        // [payloadList setObject:@"no" forKey:@"s^FLIGHTS"]; //09-04-2018
        [payloadList setObject:[NSNull null] forKey:@"s^FLIGHTS"];  //09-04-2018
    }
    if (![payloadList objectForKey:@"s^FOREX"])
    {
        [payloadList setObject:@"no" forKey:@"s^FOREX"];
    }
    if (![payloadList objectForKey:@"s^SELF_SERVICE"])
    {
        [payloadList setObject:@"no" forKey:@"s^SELF_SERVICE"];
    }
    if (![payloadList objectForKey:@"s^SOURCE"])
    {
        [payloadList setObject:@"App" forKey:@"s^SOURCE"];
    }
    
    //[NetCoreAnalyticsVC getNetCorePushNotificationDetailDictionary:payloadList withPayloadCount:102]; //28-02-2018
    
    [NetCoreAnalyticsVC getNetCorePushNotificationDetailDictionary:payloadList withPayloadCount:118]; //28-02-2018
}

#pragma mark- Fetch Package Detail And API Calling Functions

-(void)jumpToPdpFromLandingScreen:(Holiday *)objHoliday
{
    
    activityLoadingViewRT = [LoadingView loadingViewInView:self.view.superview.superview
                                                withString:@""
                                         andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE_WHITE]];
    
    @try
    {
        
        
        NetworkHelper *helper = [NetworkHelper sharedHelper];
        
        
        NSString *pathParam = objHoliday.strPackageId;
        
        
        NSDictionary *headerDict = [CoreUtility getHeaderDict];
        
        [helper getResponseWithRequestType:@"" withQueryParam:@"" withPathParam:pathParam withJsonParam:nil withHeaders:headerDict withUrl:kAstraUrlPackageDetail success:^(NSDictionary *responseDict)
         {
            NSLog(@"Response Dict : %@",responseDict);
            
            
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                [activityLoadingViewRT removeFromSuperview];
                if (responseDict)
                {
                    if (responseDict.count>0)
                    {
                        NSArray *packageArray = (NSArray *)responseDict;
                        NSDictionary *dictForCompletePackage = [packageArray objectAtIndex:0];
                        NSDictionary *packageDetail = [dictForCompletePackage objectForKey:@"packageDetail"];
                        long productId = [[packageDetail valueForKey:@"productId"] longValue];
                        // Unbloacked on 01/09/20
//                        if (productId != 11) {
                            HolidayPackageDetail *packageDetailHoliday = [[HolidayPackageDetail alloc] initWithDataDict:dictForCompletePackage];
                            
                            
                            /*
                             TabMenuVC *tabMenuVC = [[TabMenuVC alloc]initWithNibName:@"BaseViewController" bundle:nil];
                             tabMenuVC.headerName = objHoliday.strPackageName;
                             tabMenuVC.flag = @"Overview";
                             tabMenuVC.packageDetail = packageDetailHoliday;
                             [self fireAppIntHoViewDetailsEvent:packageDetailHoliday];
                             tabMenuVC.completePackageDetail = [NSArray arrayWithObjects:dictForCompletePackage, nil];
                             [[SlideNavigationController sharedInstance] pushViewController:tabMenuVC animated:YES];
                             */
                            
                            PDPScreenVC *controller = [[PDPScreenVC alloc] initWithNibName:@"PDPScreenVC" bundle:nil];
                            controller.headerName = objHoliday.strPackageName;
                            controller.holidayPackageDetailInPdp = packageDetailHoliday;
//                            controller.isFromHomePage = true;
//                 controller.dictForCompletePackage = dictForCompletePackage;
                            controller.completePackageDetail = [NSArray arrayWithObjects:dictForCompletePackage, nil];
                            controller.payloadDict = payloadList;
                            [self.navigationController pushViewController:controller animated:YES];
                            
//                        }else{
//                            [self showAlertWithTitle:@"Alert" message:@"Selected package not availble" style:UIAlertControllerStyleAlert buttNames:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString *str) {
//                                if ([str isEqualToString:@"Ok"]){
//
//                                }
//                            }];
//                        }
                    }else{
                        [self showAlertWithTitle:@"Alert" message:@"Selected package not availble" style:UIAlertControllerStyleAlert buttNames:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString *str) {
                            if ([str isEqualToString:@"Ok"]){
                                
                            }
                        }];
                    }
                }else{
                    [self showAlertWithTitle:@"Alert" message:@"Selected package not availble" style:UIAlertControllerStyleAlert buttNames:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString *str) {
                        if ([str isEqualToString:@"Ok"]){
                            
                        }
                    }];
                }
            });
            
        }
                                   failure:^(NSError *error)
         {
            dispatch_async(dispatch_get_main_queue(), ^(void)
                           {
                [activityLoadingViewRT removeFromSuperview];
                [self showAlertWithTitle:@"Alert" message:@"some error occurred" style:UIAlertControllerStyleAlert buttNames:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString *str) {
                    if ([str isEqualToString:@"Ok"]){
                        
                    }
                }];
                NSLog(@"Response Dict : %@",[error description]);
            });
            
            
        }];
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"%@", exception.reason);
        [activityLoadingViewRT removeFromSuperview];
    }
    @finally
    {
        NSLog(@"exception finally called");
    }
    
}
-(void)scrollBannerCollecionViewProgramm{
    if (visibleBannerItem == [bannerTopArray count]){// bannerImgDataArray
        visibleBannerItem=-1;
        [self.banner_CollectionView reloadData];
    }else{
        visibleBannerItem+=1;
        NSIndexPath *nextItem = [NSIndexPath indexPathForRow:visibleBannerItem inSection:0];
        [self.banner_CollectionView scrollToItemAtIndexPath:nextItem atScrollPosition:UICollectionViewScrollPositionRight animated:YES];
    }
    
    [self performSelector:@selector(scrollBannerCollecionViewProgramm) withObject:nil afterDelay:5.0];
}
-(void)getBannerDataFromAPI{
    /*
    NetworkHelper *helper = [NetworkHelper sharedHelper];
    
    if ([self connected])
    {
        NSString *pathParam = @"";
        
        NSDictionary *headerDict = [CoreUtility getHeaderDict];
        [helper getResponseWithRequestType:@"" withQueryParam:@"" withPathParam:pathParam withJsonParam:nil withHeaders:headerDict withUrl:kGetbannerDataURL success:^(NSDictionary *responseDict)
         {
            NSLog(@"Response Dict get Banner Data API : %@",responseDict);
            if (responseDict.count != 0) {
                bannerImgDataArray = [responseDict valueForKey:@"bannerDetails"];

                dispatch_async(dispatch_get_main_queue(), ^{
                    self.bannerPageControl.numberOfPages = [bannerImgDataArray count];
                      [self.banner_CollectionView reloadData];
                   });
            }else{
                
            }
        }
                                   failure:^(NSError *error)
         {
            dispatch_async(dispatch_get_main_queue(), ^(void)
                           {
                [self showAlertWithTitle:@"Alert" message:@"some error occurred" style:UIAlertControllerStyleAlert buttNames:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString *str) {
                    if ([str isEqualToString:@"Ok"]){
                        
                    }
                }];
                NSLog(@"Response Dict : %@",error);
            });
        }];
        
    }
    else
    {
        [self showAlertWithTitle:@"Alert" message:@"" style:UIAlertControllerStyleAlert buttNames:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString *str) {
            if ([str isEqualToString:@"Ok"]){
                
            }
        }];
    }*/
}

-(void)getBannerServiceAPI{
    NetworkHelper *helper = [NetworkHelper sharedHelper];
    if ([self connected])
    {
        NSString *pathParam = @"";
        NSDictionary *headerDict = [CoreUtility getHeaderDict];
        [helper getResponseWithRequestType:@"" withQueryParam:@"" withPathParam:pathParam withJsonParam:nil withHeaders:headerDict withUrl:kGetbannerServiceURL success:^(NSDictionary *responseDict)
         {
            NSLog(@"Response Dict get Banner Service API : %@",responseDict);
            if (responseDict.count != 0) {
                NSArray *responseArray = [responseDict valueForKey:@"bannerDetails"];
                for (id object in responseArray){
                  WhatsNewObject *newObj = [[WhatsNewObject alloc] initWithImgArray:object];
                    NSLog(@"newObj.bannerLevelName %@",newObj.bannerLevelName);
                    /*
                    if ([[newObj.bannerLevelName uppercaseString] isEqualToString:[kserviceWhatsNew uppercaseString]]){
                        [bannerWhatsNewArray addObject:newObj];
                    }else if ([[newObj.bannerLevelName uppercaseString] isEqualToString:[kserviceLastMinuteDeals uppercaseString]]){
                        [bannerLastMinutesArray addObject:newObj];
                    }else if ([[newObj.bannerLevelName uppercaseString] isEqualToString:[kserviceTopBanners uppercaseString]]){
                        [bannerTopArray addObject:newObj];
                    }else if ([[newObj.bannerLevelName uppercaseString] isEqualToString:[kserviceSummerHolidays uppercaseString]]){
                        [hLandingSummerArray addObject:newObj];
                    }else if ([[newObj.bannerLevelName uppercaseString] isEqualToString:[kserviceAdventureHoliday uppercaseString]]){
                        [hLandingAventureArray addObject:newObj];
                    }else if ([[newObj.bannerLevelName uppercaseString] isEqualToString:[kserviceCategoriesHoliday uppercaseString]]){
                        [hLandingCategoryArray addObject:newObj];
                    }else if ([[newObj.bannerLevelName uppercaseString] isEqualToString:[kserviceUpcomingEvents uppercaseString]]){
                        [hLandingUpcomingArray addObject:newObj];
                    }else if ([[newObj.bannerLevelName uppercaseString] isEqualToString:[kserviceCherryBlossomSpecial uppercaseString]]){
                        [hLandingCheeryBlossomArray addObject:newObj];
                    }else if ([[newObj.bannerLevelName uppercaseString] isEqualToString:[kservicePopularDestinations uppercaseString]]){
                        [hLandingPopularDestinationArray addObject:newObj];
                    }else{
                        NSLog(@"Not Added In ARRAY");
                    }
                    */
                    NSString *newBannerLevelName = [newObj.bannerLevelType stringByReplacingOccurrencesOfString:@" " withString:@""];
//                    NSString *newBannerLevelName = [newObj.bannerLevelType stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                    if ([newBannerLevelName.uppercaseString isEqualToString:levelNumberTwo.uppercaseString] && [newObj.bannerPageName.uppercaseString isEqualToString:@"HOME"]){
                        [bannerWhatsNewArray addObject:newObj];
                    }else if ([newBannerLevelName.uppercaseString isEqualToString:levelNumberThree.uppercaseString] && [newObj.bannerPageName.uppercaseString isEqualToString:@"HOME"]){
                        [bannerLastMinutesArray addObject:newObj];
                    }else if ([newBannerLevelName.uppercaseString isEqualToString:levelNumberOne.uppercaseString] && [newObj.bannerPageName.uppercaseString isEqualToString:@"HOME"]){
                        [bannerTopArray addObject:newObj];
                    }
                    else if ([newBannerLevelName.uppercaseString isEqualToString:levelNumberOne.uppercaseString] && [newObj.bannerPageName.uppercaseString isEqualToString:@"HOLIDAYS"] && newObj.orderOnThePage != 5){
                        [hLandingSummerArray addObject:newObj];
                    }
                    else if ([newBannerLevelName.uppercaseString isEqualToString:levelNumberOne.uppercaseString] && [newObj.bannerPageName.uppercaseString isEqualToString:@"HOLIDAYS-1"] && newObj.orderOnThePage != 5){
                        [hLandingHolidayOneArray addObject:newObj];
                    }
                    else if ([newBannerLevelName.uppercaseString isEqualToString:levelNumberTwo.uppercaseString] && [newObj.bannerPageName.uppercaseString isEqualToString:@"HOLIDAYS"]){
                        [hLandingAventureArray addObject:newObj];
                    }else if ([newBannerLevelName.uppercaseString isEqualToString:levelNumberThree.uppercaseString] && [newObj.bannerPageName.uppercaseString isEqualToString:@"HOLIDAYS"]){
                        [hLandingCategoryArray addObject:newObj];
                    }else if ([newBannerLevelName.uppercaseString isEqualToString:levelNumberFour.uppercaseString] && [newObj.bannerPageName.uppercaseString isEqualToString:@"HOLIDAYS"]){
                        [hLandingUpcomingArray addObject:newObj];
                    }else if ([newBannerLevelName.uppercaseString isEqualToString:levelNumberOne.uppercaseString] && [newObj.bannerPageName.uppercaseString isEqualToString:@"HOLIDAYS"] && newObj.orderOnThePage == 5){
                        [hLandingCheeryBlossomArray addObject:newObj];
                    }else if ([newBannerLevelName.uppercaseString isEqualToString:levelNumberFive.uppercaseString] && [newObj.bannerPageName.uppercaseString isEqualToString:@"HOLIDAYS"]){
                        [hLandingPopularDestinationArray addObject:newObj];
                    }else{
                        NSLog(@"Not Added In ARRAY");
                    }
                    
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.whtasNewPageCtrl_CollectionView.numberOfPages = [bannerWhatsNewArray count];
                      [self.whtasNew_CollectionView reloadData];
                    [self.lastMinutesDeals_CollectionView reloadData];
                    self.bannerPageControl.numberOfPages = [bannerTopArray count];
                    [self.banner_CollectionView reloadData];
                   });
            }else{
                
            }
        }
                                   failure:^(NSError *error)
         {
            dispatch_async(dispatch_get_main_queue(), ^(void)
                           {
                [self showAlertWithTitle:@"Alert" message:@"some error occurred" style:UIAlertControllerStyleAlert buttNames:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString *str) {
                    if ([str isEqualToString:@"Ok"]){
                        
                    }
                }];
                NSLog(@"Response Dict : %@",error);
            });
        }];
        
    }
    else
    {
        [self showAlertWithTitle:@"Alert" message:@"" style:UIAlertControllerStyleAlert buttNames:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString *str) {
            if ([str isEqualToString:@"Ok"]){
                
            }
        }];
    }
}


-(void)getBankOffersAPI{
    NetworkHelper *helper = [NetworkHelper sharedHelper];
    if ([self connected])
    {
        NSString *pathParam = @"";
        NSDictionary *headerDict = [CoreUtility getHeaderDict];
        [helper getResponseWithRequestType:@"" withQueryParam:@"" withPathParam:pathParam withJsonParam:nil withHeaders:headerDict withUrl:KGetByPackageId success:^(NSDictionary *responseDict)
         {
            NSLog(@"Response Dict get Banner Service API : %@",responseDict);
            if (responseDict.count != 0) {
                if ([[[responseDict valueForKey:@"errormsg"] uppercaseString] isEqualToString:@"SUCCESS"]){
                    if ([responseDict valueForKey:@"result"]>0){
                        bankOfferDataArray =[responseDict valueForKey:@"result"];
                    }
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.advertise_CollectionView reloadData];
                   });
            }else{
                
            }
        }
                                   failure:^(NSError *error)
         {
            dispatch_async(dispatch_get_main_queue(), ^(void)
                           {
                [self showAlertWithTitle:@"Alert" message:@"some error occurred" style:UIAlertControllerStyleAlert buttNames:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString *str) {
                    if ([str isEqualToString:@"Ok"]){
                        
                    }
                }];
                NSLog(@"Response Dict : %@",error);
            });
        }];
        
    }
    else
    {
        [self showAlertWithTitle:@"Alert" message:@"" style:UIAlertControllerStyleAlert buttNames:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString *str) {
            if ([str isEqualToString:@"Ok"]){
                
            }
        }];
    }
}

-(void)getTravelBlogDataFromAPI{

    NetworkHelper *helper = [NetworkHelper sharedHelper];
    
    if ([self connected])
    {
        
        NSDictionary *headerDict = [CoreUtility getHeaderDict];
        [helper getResponseWithRequestType:@"" withQueryParam:@"" withPathParam:@"" withJsonParam:nil withHeaders:headerDict withUrl:kTravelBlogURL success:^(NSDictionary *responseDict)
         {
            NSLog(@"Response Dict get Travel Blog API : %@",responseDict);
            if (responseDict.count != 0) {
                NSArray *array = (NSArray *)responseDict;
                travelBlogDataArray = [array copy] ;
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.travelVlog_CollectionView reloadData];
                   });
            }else{
                
            }
        }
                                   failure:^(NSError *error)
         {
            dispatch_async(dispatch_get_main_queue(), ^(void)
                           {
                [self showAlertWithTitle:@"Alert" message:@"some error occurred" style:UIAlertControllerStyleAlert buttNames:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString *str) {
                    if ([str isEqualToString:@"Ok"]){
                        
                    }
                }];
                NSLog(@"Response Dict : %@",error);
            });
        }];
        
    }
    else
    {
        [self showAlertWithTitle:@"Alert" message:@"" style:UIAlertControllerStyleAlert buttNames:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString *str) {
            if ([str isEqualToString:@"Ok"]){
                
            }
        }];
    }
}

-(void)getDataFromCustomerProfileAPI{

    
    NetworkHelper *helper = [NetworkHelper sharedHelper];
    
    if ([self connected])
    {
//        NSDictionary *headerDict = [CoreUtility getHeaderDict];
        NSString *requestId = [[NSUserDefaults standardUserDefaults] valueForKey:kLoginRequestID];
        NSString *tokenId = [[NSUserDefaults standardUserDefaults] valueForKey:kLoginTokenID];
        NSDictionary *headerDict = [[NSDictionary alloc] initWithObjectsAndKeys:requestId,@"requestId",tokenId,@"sessionId" ,nil];
//        [headerDict setValue:@"" forKey:@""]
//        NSString *emailStr = [[NSUserDefaults standardUserDefaults]objectForKey:kLoginEmailId];
        [helper getResponseWithRequestType:@"" withQueryParam:@"" withPathParam:@"" withJsonParam:nil withHeaders:headerDict withUrl:kGetCustInfoURL success:^(NSDictionary *responseDict)
         {
            NSLog(@"Response Dict get Custom Profile API : %@",responseDict);
            if (responseDict.count != 0) {
                
                NSLog(@"Response Dict Availabel : %@",responseDict);
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                   });
            }else{
                
            }
        }
                                   failure:^(NSError *error)
         {
            dispatch_async(dispatch_get_main_queue(), ^(void)
                           {
                [self showAlertWithTitle:@"Alert" message:@"some error occurred" style:UIAlertControllerStyleAlert buttNames:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString *str) {
                    if ([str isEqualToString:@"Ok"]){
                        
                    }
                }];
                NSLog(@"Response Dict : %@",error);
            });
        }];
        
    }
    else
    {
        [self showAlertWithTitle:@"Alert" message:@"" style:UIAlertControllerStyleAlert buttNames:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString *str) {
            if ([str isEqualToString:@"Ok"]){
                
            }
        }];
    }
   
}

- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}

-(void)showAlertWithTitle:(NSString*)title message:(NSString*)message style:(UIAlertControllerStyle)style buttNames:(NSArray*)buttArray completion:(void (^)( NSString *))completion{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:style];
    for (NSString* key in buttArray){
        [alert addAction:[UIAlertAction actionWithTitle:key style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            completion(key);
        }]];
    }
    [self presentViewController:alert animated:YES completion:nil];
}


-(void)getTokenIDInLandingScreenVC{
 
    
    {
        
        if ([AppUtility checkNetConnection]) {
        
        @try
        {
            
            if ([SSKeychain passwordForService:KEYCHAIN_SERVICE account:KEYCHAIN_ACCOUNT] == nil)
            {
                NSString *deviceId = [MobiculeUtilityManager getApplicationUUID];
                [SSKeychain setPassword:deviceId forService:KEYCHAIN_SERVICE account:KEYCHAIN_ACCOUNT];
            }
            
            
            NetworkHelper *networkHelper = [[NetworkHelper alloc] init];
            
            NSDictionary *headerDict = [[NSDictionary alloc] initWithObjectsAndKeys:[SSKeychain passwordForService:KEYCHAIN_SERVICE account:KEYCHAIN_ACCOUNT],@"uniqueId",@"mobicule",@"user", nil];
            
            activityLoadingViewRT = [LoadingView loadingViewInView:self.view withString:@"" andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE_WHITE]];
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                [networkHelper  getResponseWithRequestType:@"" withQueryParam:@"" withPathParam:@"" withJsonParam:nil withHeaders:headerDict withUrl:kAstraUrlNewToken success:^(NSDictionary *responseDict)
                 {
                     dispatch_async(dispatch_get_main_queue(), ^(void) {
                         
                         [activityLoadingViewRT removeFromSuperview];
                         
                         NSLog(@"%@",responseDict);
                         
                         if(responseDict)
                         {
                             
                             if (responseDict.count > 0)
                             {
                                 NSDictionary *requestId  = [responseDict valueForKey:@"requestId"];
                                 NSDictionary *tokenId  = [responseDict valueForKey:@"tokenId"];
                                 
                                 if (requestId)
                                 {
                                     [[NSUserDefaults standardUserDefaults] setObject:requestId forKey:kuserDefaultRequestId];
                                     
                                 }
                                 
                                 if (tokenId)
                                 {
                                     [[NSUserDefaults standardUserDefaults] setObject:tokenId forKey:kuserDefaultTokenId];
                                     
                                 }
                             }
                             else
                             {
                                 [self showAlertViewWithTitle:@"Alert" withMessage:@"some error occurred"];
                             }
                         }
                         else
                         {
                             [self showAlertViewWithTitle:@"Alert" withMessage:@"some error occurred"];
                         }
                     });
                 }
                                                   failure:^(NSError *error)
                 {
                     dispatch_async(dispatch_get_main_queue(), ^(void)
                                    {
                                        [activityLoadingViewRT removeFromSuperview];
                                        [self showAlertViewWithTitle:@"Alert" withMessage:@"some error occurred"];
                                        NSLog(@"Response Dict : %@",[error description]);
                                    });
                 }];
                
            });
        }
        @catch (NSException *exception)
        {
            NSLog(@"%@", exception.reason);
            [activityLoadingViewRT removeFromSuperview];
        }
        @finally
        {
            NSLog(@"exception finally called");
            //[activityLoadingViewRT removeFromSuperview];
        }
        }else{
            [activityLoadingViewRT removeFromSuperview];
        }
    }
    
    
}

-(void)searchDestinationWithSearchType:(NSString *)searchTypeStr withDestName:(NSString*)searchText{
    
    NSDictionary *selecteDestinationDict = @{@"searchString":searchText,
                            @"searchType":searchTypeStr,
                            @"cityName":@"",
                            @"continentName":@"",
                            @"countryCode":@"",
                            @"countryName":@"",
                            @"searchLatitude":@(0),
                            @"searchLongitude":@(0),
                            @"stateCode":@"",
                            @"stateName":@"",
    };
    /*
    NSDictionary *selecteDestinationDict = @{@"searchString":searchTypeStr,
                            @"searchType":searchText,
                            @"cityName":@"",
                            @"continentName":@"",
                            @"countryCode":@"",
                            @"countryName":@"",
                            @"searchLatitude":@(0),
                            @"searchLongitude":@(0),
                            @"stateCode":@"",
                            @"stateName":@"",
    };
    */
    NSString *destinationStr;
    NSString *stringBudgetPerson;
    NSString *stringNoOfNights;
    NSString *monthIndex;
    NSString *headerName = kIndianHoliday;
    LoadingView *activityIndicatorInLSVC = [LoadingView loadingViewInView:self.view.superview.superview
                                              withString:@""
                                       andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE_WHITE]];
  
    NSMutableString *queryParam = [[NSMutableString alloc] init] ;

    NSString *searchType  = [[selecteDestinationDict valueForKey:@"searchType"] uppercaseString];
    
    if (searchText.length !=0 )
    {
    [queryParam appendFormat:@"searchType=%@",searchType];
    
    
    if ([[searchType uppercaseString] isEqualToString:@"COUNTRY"])
    {
        /*
         searchType=COUNTRY
         destination=IN
         */
       NSString *countryCode = [[selecteDestinationDict valueForKey:@"countryCode"] uppercaseString];
        [queryParam appendFormat:@"&destination=%@",countryCode];
    }
    else if ([[searchType uppercaseString] isEqualToString:@"STATE"])
    {
        /*
         searchType=STATE
         destination=MH
         countryCode=IN
         */
        
        NSString *stateCode = [[selecteDestinationDict valueForKey:@"stateCode"] uppercaseString];
        NSString *countryCode = [[selecteDestinationDict valueForKey:@"countryCode"] uppercaseString];
        
        [queryParam appendFormat:@"&destination=%@",stateCode];
        [queryParam appendFormat:@"&countryCode=%@",countryCode];
        
    }
    else if ([[searchType uppercaseString] isEqualToString:@"CITY"])
    {
        /*
         searchType=CITY
         destination=BOM
         countryCode=IN
         stateCode=MH
         */
        
        NSString *cityCode = [[selecteDestinationDict valueForKey:@"locationCode"] uppercaseString];
        NSString *stateCode = [[selecteDestinationDict valueForKey:@"stateCode"] uppercaseString];
        NSString *countryCode = [[selecteDestinationDict valueForKey:@"countryCode"] uppercaseString];
        
        
        [queryParam appendFormat:@"&destination=%@",cityCode];
        [queryParam appendFormat:@"&countryCode=%@",countryCode];
        [queryParam appendFormat:@"&stateCode=%@",stateCode];

    }
    else if ([[searchType uppercaseString] isEqualToString:@"CONTINENT"])
    {
        /*
         searchType=CONTINENT
         destination=asia
         */
//         [queryParam appendFormat:@"&destination=%@",destinationStr];
        [queryParam appendFormat:@"&destination=%@",searchText]; // search as per Service API New One
    }
    else if ([[searchType uppercaseString] isEqualToString:@"THEME"])  // Done by komal 10 Aug 2018
    {
        /*
         "searchString":"Beach",
         "searchType":"THEME",
         */
        NSString *searchString  = [[selecteDestinationDict valueForKey:@"searchString"] uppercaseString];
        [queryParam appendFormat:@"&theme=%@",searchString];
        // To show All Holiday Acc to New PSD We comment This Code On 31_July_20
        /*
         if ([headerName isEqualToString:kIndianHoliday])
         {
             [queryParam appendFormat:@"&pkgType=0"];
         }
         else
         {
             [queryParam appendFormat:@"&pkgType=1"];
         }
         */
    }
    else
    {
         [queryParam appendFormat:@"&destination=%@",destinationStr];
    }
    }
    NetworkHelper *helper = [NetworkHelper sharedHelper];
    
    
    //stringBudgetPerson
    
    NSString *budget1 = @"";
    NSString *budget2 = @"";
    
    if ([stringBudgetPerson isEqualToString:@"250000"])
    {
        budget1 = @"250000";
    }
    else
    {
        NSArray *budgetArray = [stringBudgetPerson componentsSeparatedByString:@"-"];
        
        if (budgetArray)
        {
            if(budgetArray.count == 2)
            {
                budget1 = [budgetArray firstObject];
                budget2 = budgetArray[1];
            }
        }
    }
    
    
    if ([budget1 isEqualToString:@""] && [budget2 isEqualToString:@""])
    {
       // queryParam = [NSMutableString stringWithFormat:@"searchType=%@&destination=%@",searchType,destinationStr];
    }
    else if ([budget1 isEqualToString:@""])
    {
        [queryParam appendFormat:@"&budget2=%@",budget2];
    }
    else if ([budget2 isEqualToString:@""])
    {
         [queryParam appendFormat:@"&budget1=%@",budget1];
    }
    else
    {
         [queryParam appendFormat:@"&budget1=%@&budget2=%@",budget1,budget2];
    }

    
    
    if (monthIndex != 0)
    {
        [queryParam appendString:[NSString stringWithFormat:@"&monthOfTravel=%@",monthIndex]];
    }
    
    @try
    {
        if ([queryParam characterAtIndex:0] == '&')
        {
            queryParam = [[queryParam substringFromIndex:1] mutableCopy];
        }
        NSString *stringWithDecode  = [queryParam stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];

        
        NSDictionary *headerDict = [CoreUtility getHeaderDict];
        
        [helper getResponseWithRequestType:@"" withQueryParam:stringWithDecode withPathParam:@"" withJsonParam:nil withHeaders:headerDict withUrl:kAstraUrlPackageFilterSearch success:^(NSDictionary *responseDict)
         {
             dispatch_async(dispatch_get_main_queue(), ^(void)
                            {
                                [activityIndicatorInLSVC removeFromSuperview];
                                NSLog(@"Response Dict : %@",responseDict);
                                
                                if (responseDict)
                                {
                                    if (responseDict.count>0)
                                    {
                                        NSMutableArray *arrayOfHolidays = [[NSMutableArray alloc] init];
                                        NSArray *packageArray = (NSArray *)responseDict;
                                        
                                        for (NSDictionary *dictOfData in packageArray)
                                        {
                                            NSDictionary *packageDatailDict =[dictOfData valueForKey:@"packageDetail"];
                                            
                                            long productId = [[packageDatailDict valueForKey:@"productId"] longValue];
                                            // Unbloacked on 01/09/20
//                                            if (productId != 11) {
                                                Holiday *nwHoliday  = [[Holiday alloc]initWithDataDict:dictOfData];
                                                if (![nwHoliday isEqual:nil]) {
                                                    [arrayOfHolidays addObject:nwHoliday];
                                                }
//                                            }else{
//                                                NSLog(@"Product blocked");
//                                            }
                                        }
                                        NSString *header = @"";
                                        if (destinationStr == nil) {
                                            header = @"Holidays";
                                        }
                                        else
                                        {
                                            header = [NSString stringWithFormat:@"%@ %@",destinationStr, @"Holidays"];
                                        }
                                        SRPScreenVC *srpScreenVC=[[SRPScreenVC alloc]initWithNibName:@"SRPScreenVC" bundle:nil];
                                        srpScreenVC.headerName=header;
                                        srpScreenVC.searchedCity = destinationStr;
                                        srpScreenVC.holidayArrayInSrp = arrayOfHolidays;
                                        srpScreenVC.totalPackages=[NSString stringWithFormat:@"%lu",(unsigned long)[arrayOfHolidays count]];
                                        srpScreenVC.completePackageDetail = packageArray;
                                        [self.navigationController pushViewController:srpScreenVC animated:YES];
                                       
                                    }
                                    else
                                    {
                                        [super showAlertViewWithTitle:@"error" withMessage:@"Sorry, we could not find any package for the given search. Please try with a different search criteria."];
                                        NSLog(@" response Date Search packageDetailsMessage "  );
                                        
                                    }
                                    
                                }
                                
                            });
         }
                                   failure:^(NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^(void)
                            {
                                [activityIndicatorInLSVC removeFromSuperview];
                                [super showAlertViewWithTitle:@"Alert" withMessage:[error description]];
                                NSLog(@"Response Dict : %@",[error description]);
                            });
             
         }];
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"%@", exception.reason);
        [activityIndicatorInLSVC removeFromSuperview];
    }
    @finally
    {
        NSLog(@"exception finally called");
    }
    
    
    //    NSString *strDuration     = self.txtNights.text;
    //    NSString *strBudget      = self.txtBudget.text;
    
    
    
    if (stringBudgetPerson == nil)
    {
        stringBudgetPerson = @"";
    }
    
    if (stringNoOfNights == nil)
    {
        stringNoOfNights = @"";
    }
    
    
}

-(void)searchDestinationSelectDictUsingQueryParamwithText:(NSString*)queryParameter withTitleHeader:(NSString*)title{
    NSString *destination;
    LoadingView *activityIndicatorInLSVC = [LoadingView loadingViewInView:self.view.superview.superview
           withString:@""
    andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE_WHITE]];
    @try
    {
//        NSString *stringWithDecode  = [queryParameter stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        NSString *stringWithDecode = queryParameter;
        NSDictionary *headerDict = [CoreUtility getHeaderDict];
        NetworkHelper *helper = [NetworkHelper sharedHelper];
        [helper getResponseWithRequestType:@"" withQueryParam:stringWithDecode withPathParam:@"" withJsonParam:nil withHeaders:headerDict withUrl:kAstraUrlPackageFilterSearch success:^(NSDictionary *responseDict)
         {
             dispatch_async(dispatch_get_main_queue(), ^(void)
                            {
                                [activityIndicatorInLSVC removeFromSuperview];
                                NSLog(@"Response Dict : %@",responseDict);
                                
                                if (responseDict)
                                {
                                    if (responseDict.count>0)
                                    {
                                        NSMutableArray *arrayOfHolidays = [[NSMutableArray alloc] init];
                                        NSArray *packageArray = (NSArray *)responseDict;
                                        
                                        for (NSDictionary *dictOfData in packageArray)
                                        {
                                            NSDictionary *packageDatailDict =[dictOfData valueForKey:@"packageDetail"];
                                            
                                            long productId = [[packageDatailDict valueForKey:@"productId"] longValue];
                                            // Unbloacked on 01/09/20
//                                            if (productId != 11) {
                                                Holiday *nwHoliday  = [[Holiday alloc]initWithDataDict:dictOfData];
                                                if (![nwHoliday isEqual:nil]) {
                                                    [arrayOfHolidays addObject:nwHoliday];
                                                }
//                                            }else{
//                                                NSLog(@"Product blocked");
//                                            }
                                        }
                                        NSString *header = @"";
                                        if (destination == nil) {
                                            header = @"Holidays";
                                        }
                                        else
                                        {
                                            header = [NSString stringWithFormat:@"%@ %@",destination, @"Holidays"];
                                        }
                                        if ([arrayOfHolidays count]>0){
                                            SRPScreenVC *srpScreenVC=[[SRPScreenVC alloc]initWithNibName:@"SRPScreenVC" bundle:nil];
                                            srpScreenVC.headerName=title;
                                            srpScreenVC.searchedCity = destination;
                                            srpScreenVC.holidayArrayInSrp = arrayOfHolidays;
                                            srpScreenVC.totalPackages=[NSString stringWithFormat:@"%lu",(unsigned long)[arrayOfHolidays count]];
                                            srpScreenVC.completePackageDetail = packageArray;
                                            [self.navigationController pushViewController:srpScreenVC animated:YES];
                                        }else{
                                           [super showAlertViewWithTitle:@"error" withMessage:@"Sorry, we could not find any package for the given search. Please try with a different search criteria."];
                                        }
                                        
                                       
                                    }
                                    else
                                    {
                                        [super showAlertViewWithTitle:@"error" withMessage:@"Sorry, we could not find any package for the given search. Please try with a different search criteria."];
                                        NSLog(@" response Date Search packageDetailsMessage "  );
                                        
                                    }
                                    
                                }else
                                {
                                    [super showAlertViewWithTitle:@"error" withMessage:@"Sorry, we could not find any package for the given search. Please try with a different search criteria."];
                                    NSLog(@" response Date Search packageDetailsMessage "  );
                                    
                                }
                                
                            });
         }
                                   failure:^(NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^(void)
                            {
                                [activityIndicatorInLSVC removeFromSuperview];
                                [super showAlertViewWithTitle:@"Alert" withMessage:[error description]];
                                NSLog(@"Response Dict : %@",[error description]);
                            });
             
         }];
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"%@", exception.reason);
        [activityIndicatorInLSVC removeFromSuperview];
    }
    @finally
    {
        NSLog(@"exception finally called");
    }
    
     
    
}
/*

 - (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
     if (collectionView == self.dot_CollectionView){
         DotCollectionCell *cell= (DotCollectionCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"DotCollectionCell" forIndexPath:indexPath];
         cell.dott_view.backgroundColor = [UIColor blueColor];
     }
     
 }
 
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    for (UICollectionViewCell *cell in [self.banner_CollectionView visibleCells]) {
        NSIndexPath *indexPath = [self.banner_CollectionView indexPathForCell:cell];
        NSLog(@"Scroll Enable %ld",indexPath.item);
        [self.dot_CollectionView selectItemAtIndexPath:indexPath animated:YES scrollPosition:UICollectionViewScrollPositionNone];
    }
}
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView
           viewForSupplementaryElementOfKind:(NSString *)kind
                                 atIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == self.recentlyAdded_CollectionView){
    UICollectionReusableView *view = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:nil forIndexPath:indexPath];
        view.frame = CGRectMake(0, 0, 100, self.recentlyAdded_CollectionView.frame.size.height);
    [collectionView addSubview:view];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, view.frame.size.width, view.frame.size.height)];
    label.center = view.center;
    label.text = @"Recently Added View";
    label.numberOfLines = 0;
    [view addSubview:label];
    return view;
    }
    return nil;
}
 */
@end



// Formula For Pixel Conversion
/*
 Pixel to point conversion
 1 px = 0.75 points
 100 px = 75 points
 
 Point to Pixel conversion
 75 points = 100 px
 
 One Way
 First Calculate all in pixel then multiply it by 0.75 to convert it into point
 (((IPhone_Height_px * Given_px) / Respective_px)) * 0.75
 */
