//
//  BookNowViewController.h
//  holidays
//
//  Created by ketan on 08/10/15.
//  Copyright © 2015 Mobicule. All rights reserved.
//

#import "BaseViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "HolidayPackageDetail.h"
#import "PackageInternationalGIT.h"
#import "Holiday.h"
#import "CustomIOSAlertView.h"

#import <FirebaseAnalytics/FirebaseAnalytics.h>


@interface BookNowViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,CustomIOSAlertViewDelegate>
{
    LoadingView *activityLoadingView;
}
@property (strong, nonatomic) IBOutlet UIView *bookNowView;
@property (strong, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewForContainer;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewForNumbaer;
- (IBAction)onAddDeleteButtonClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnAddDelete;

@property (strong, nonatomic) IBOutlet UITableViewCell *tableViewCellChildAdd;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintBookingAddPanel;

- (IBAction)onAdultsAddRemoveClicked:(id)sender;
- (IBAction)onChildrenAddRemoveClicked:(id)sender;
- (IBAction)onInfantsAddRemoveClicked:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *lblAdultCount;
@property (weak, nonatomic) IBOutlet UILabel *lblChildrenCount;
@property (weak, nonatomic) IBOutlet UILabel *lblInfantsCount;
@property (weak, nonatomic) IBOutlet UITableView *tblViewForChildCount;

@property (weak, nonatomic) IBOutlet UIButton *btnAdultAdd;
@property (weak, nonatomic) IBOutlet UIButton *btnAdultDelete;
@property (weak, nonatomic) IBOutlet UIButton *btnChildAdd;
@property (weak, nonatomic) IBOutlet UIButton *btnChildDelete;
@property (weak, nonatomic) IBOutlet UIButton *btnInfantDelete;
@property (weak, nonatomic) IBOutlet UIButton *btnInfantAdd;

@property (weak, nonatomic) IBOutlet UITextField *txtPackageType;
@property (weak, nonatomic) IBOutlet UITextField *txtSelectCity;
@property (weak, nonatomic) IBOutlet UITextField *txtSelectDate;
- (IBAction)onEasyPaymentButtonClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *viewEasyPaymentOption;
@property (strong ,nonatomic) NSArray *arrayCities;
@property (strong ,nonatomic) HolidayPackageDetail *packageDetail;


- (IBAction)onCalculateButtonClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *viewForPaymentDetails;
@property (weak, nonatomic) IBOutlet UILabel *labelForPaymentOptions;
@property (weak, nonatomic) IBOutlet UIView *viewForButtonContainers;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintCalculateOffset;
@property (weak, nonatomic) IBOutlet UILabel *lblPriceINR;
@property (weak, nonatomic) IBOutlet UILabel *lblPriceEURO;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalCost;
@property (weak, nonatomic) IBOutlet UILabel *lblServiceTax;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalPriceINCGST;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintCalculatedINR;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintINRLabel;

@property (weak, nonatomic) IBOutlet UIButton *buttonCalculate;
@property (weak, nonatomic) IBOutlet UITableView *tableNonInrComponent;
@property (strong, nonatomic) IBOutlet UITableViewCell *nonINRCell;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintNonInrTableHeight;
- (IBAction)onTermsConditionButtonClicked:(id)sender;
- (IBAction)onCallButtonCalled:(id)sender;
- (IBAction)onEmailButtonClicked:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintTermsConditions;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintBookingMessege;
@property (weak, nonatomic) IBOutlet UIView *viewBookingMessege;

@property (weak, nonatomic) IBOutlet UILabel *lblINRTourComponentTitle;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintGapINRCompanent;
@property (weak, nonatomic) IBOutlet UILabel *labelSaperatorINRGap;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightButtonContainer;
@property (weak, nonatomic) IBOutlet UILabel *labelValueSgst;
@property (weak, nonatomic) IBOutlet UILabel *labelTitleCgst;
@property (weak, nonatomic) IBOutlet UILabel *labelTitleSgst;
@property (weak, nonatomic) IBOutlet UITextField *textFieldState;


@end
