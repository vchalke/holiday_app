//
//  FindBranchVC.h
//  holidays
//
//  Created by ROHIT on 12/12/15.
//  Copyright © 2015 Mobicule. All rights reserved.
//

#import "BaseViewController.h"
#import "SlideMenuViewController.h"
#import <MapKit/MapKit.h>

@interface FindBranchVC : BaseViewController<UITableViewDelegate,UITableViewDataSource,MKMapViewDelegate,CLLocationManagerDelegate,UITextFieldDelegate>{
     KLCPopup *customePopUp;
     LoadingView *activityIndicator;
    CLLocationManager *locationManager;
    
}
@property (strong, nonatomic) IBOutlet UIView *findBranchView;
@property (weak, nonatomic) IBOutlet UIView *selectBranchView;
@property (weak, nonatomic) IBOutlet UIView *nearestBranchView;
@property (weak, nonatomic) IBOutlet UIView *btnHighlightedView;
@property (weak, nonatomic) IBOutlet UIButton *btnSelectABranch;
@property (weak, nonatomic) IBOutlet UIButton *btnNearestBranch;
@property (weak, nonatomic) IBOutlet UITextField *txtSelectCity;
@property (weak, nonatomic) IBOutlet UITextField *txtSelectBranch;
@property (weak, nonatomic) IBOutlet UIButton *btnSMSCheckbox;
@property (weak, nonatomic) IBOutlet UITextView *txtViewForAddress;
@property (strong, nonatomic) IBOutlet UIView *viewForSMSPopUp;
@property (strong, nonatomic) IBOutlet UITextField *txtMobileNumber;
@property (strong, nonatomic) IBOutlet UIButton *btnForSMS;
@property (nonatomic, strong) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UILabel *lblNearestAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblNearestPhone;
@property (weak, nonatomic) IBOutlet UILabel *lblNearestBranchName;
@property (weak, nonatomic) IBOutlet UIButton *buttonpopViewOfSMS;
@property (weak, nonatomic) IBOutlet UIView *viewForPopUpMessage;


- (IBAction)actionForbtnSelectBranch:(id)sender;
- (IBAction)actionForbtnNearestBranch:(id)sender;
- (IBAction)actionForSelectCityPopUp:(id)sender;
- (IBAction)actionForSelectBranchPopUp:(id)sender;
- (IBAction)actionForSMSSend:(id)sender;
- (IBAction)actionForSENDSMS:(id)sender;
- (IBAction)nearestSendAddress:(id)sender;

@property (weak,nonatomic) NSString *headerName;

@end
