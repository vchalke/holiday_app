//
//  RoomListViewController.swift
//  sotc-consumer-application
//
//  Created by Saurav Kumar on 09/08/17.
//  Copyright © 2017 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit

class RoomListViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UITableViewDataSource
{
    
    //MARK : - Outlets
  
    
    @IBOutlet weak var roomCollectionView: UICollectionView!
    
    @IBOutlet weak var roomDetailsTableView: UITableView!
    
    @IBOutlet weak var roomSharingTypeLabel: UILabel!
    var passengerDetailsArray:Array<Passenger>?
    
    var roomNumberArray:Array<String>?
    
    var selectedIndexPath : IndexPath = IndexPath(item: 0, section: 0)
    
    var selectedRoomDetails:Array<Passenger>?
    
    @IBOutlet weak var roomCollectionViewWidth: NSLayoutConstraint!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
       let uniqueRoomNumberArray = getUniqueRoom();
        
        roomNumberArray = self.sortRoom(uniqueRoomArray: uniqueRoomNumberArray)
       
        
        selectedRoomDetails = self.getPassengerInRoom(by:0)
        
        if (selectedRoomDetails?.count)! > 0
        {
            let roomType = self.getRoomSharingType(passengerArray: selectedRoomDetails!)
            
            self.roomSharingTypeLabel.text = roomType
        }
        
        
        self.roomDetailsTableView.register(UITableViewCell.self, forCellReuseIdentifier: "roomDetailsTableView")
                
        self.roomDetailsTableView.register(UINib(nibName: "RoomSectionFooterTableViewCell", bundle: nil), forCellReuseIdentifier: "RoomSectionFooterTableViewCell")
        
        self.configureCollectionViewLayout()
        
        // Do any additional setup after loading the view.
        
        
        self.roomDetailsTableView.tableFooterView = UIView(frame: .zero)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.setRedNavigationBar()
        //self.navigationController?.setNagationRightButton(showRightButton: true, viewController: self)
        self.navigationController?.setnavigatiobBarTitle(title: Title.ROOMS, viewController: self)
        self.navigationController?.setEmergencyNavigationRightButton(showEmergencyButton: true, viewController: self)
        self.navigationController?.setNagationBackButton(showRightButton: true, viewController: self)
        
    }
    
    
    func backButtonClick() -> Void {
        
        self.navigationController?.popViewController(animated: true)
        
        self.navigationController?.setNavigationBarTranslucent()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Set Collection View  Layout
    
    private func configureCollectionViewLayout() {
        
        self.roomCollectionView.register(UINib(nibName: "RoomCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "RoomCollectionViewCell")
        
        self.roomCollectionView.layer.cornerRadius = 5.0
        
        self.roomCollectionView.layer.borderWidth = 1.0
        
        self.roomCollectionView.layer.borderColor = UIColor.black.cgColor
        
        self.roomCollectionView.layer.masksToBounds = true
        
        self.roomCollectionView.allowsMultipleSelection = false
        
        roomCollectionViewWidth.constant = 300
               
        var cellSize = CGSize(width:(self.roomCollectionView.frame.width/3), height:self.roomCollectionView.frame.height + 20)
        
        if (roomNumberArray?.count)! == 1
        {
            roomCollectionViewWidth.constant = 100
            
           cellSize = CGSize(width:100, height:self.roomCollectionView.frame.height + 20)
        }else if (roomNumberArray?.count)! == 2
        {
             roomCollectionViewWidth.constant = 200
            
                cellSize = CGSize(width:100, height:self.roomCollectionView.frame.height + 20)
        }
        
        let layout = UICollectionViewFlowLayout()
        
        layout.scrollDirection = .horizontal //.horizontal
        
        layout.itemSize = cellSize
        
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
        layout.minimumLineSpacing = 0.0
        
        layout.minimumInteritemSpacing = 0.0
        
        self.roomCollectionView.setCollectionViewLayout(layout, animated: true)
        
        self.roomCollectionView.reloadData()
    }
    
    // MARK: - Collection View Data Source
        
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return roomNumberArray!.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RoomCollectionViewCell", for: indexPath as IndexPath) as! RoomCollectionViewCell
        
       cell.roomNumberLabel.text = "Room" + (roomNumberArray?[indexPath.row])!
        
        
        if (self.selectedIndexPath != nil && indexPath == self.selectedIndexPath)
        {
            // if(cell.isSelected){
            
            cell.contentView.backgroundColor = UIColor.black
            cell.roomNumberLabel.textColor = UIColor.white
            
        }
        else{
            
            cell.contentView.backgroundColor = UIColor.white
            cell.roomNumberLabel.textColor = UIColor.lightGray
        }
        
        // Configure the cell
        return cell
    }
    
    //MARK: - Collection View Delegates Implementaion
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let cell = collectionView.cellForItem(at: indexPath) as! RoomCollectionViewCell
        
        cell.contentView.backgroundColor = UIColor.black
        
        self.selectedIndexPath = indexPath
        
        self.roomCollectionView.reloadData()
        
        roomCollectionView.scrollToItem(at: indexPath, at: UICollectionView.ScrollPosition.centeredHorizontally, animated: true)        
        
        
        selectedRoomDetails = self.getPassengerInRoom(by:indexPath.row)
        
        if (selectedRoomDetails?.count)! > 0
        {
            let roomType = self.getRoomSharingType(passengerArray: selectedRoomDetails!)
            
            self.roomSharingTypeLabel.text = roomType
        }
        
        self.roomDetailsTableView.reloadData()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath)
    {
        //        guard let  cell = collectionView.cellForItem(at: indexPath) else {
        //           return
        //        }
        //
        //        cell.contentView.backgroundColor = UIColor.white
    }
    
    //MARK: - Table view Data Source
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
       return 200
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return  selectedRoomDetails!.count
    }
    
    
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
     {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "roomDetailsTableView", for: indexPath) as? UITableViewCell
            else {
                
                fatalError("The dequeued cell is not an instance of MealTableViewCell.")
        }
        
        cell.textLabel?.font = UIFont(name:"Roboto-Regular", size:15)
        
     if let passenger = self.selectedRoomDetails?[indexPath.row]
     {
        
        
         cell.textLabel?.text = " \((passenger.title ?? "" ).capitalized) \((passenger.firstName ?? "").capitalized) \((passenger.middleName ?? "").capitalized) \((passenger.lastName ?? "").capitalized) "
        
        // create an NSMutableAttributedString that we'll append everything to
        let fullString = NSMutableAttributedString(string: (cell.textLabel?.text)!)
        
          
        
        // create our NSTextAttachment
        let image1Attachment = NSTextAttachment()
        
        var bedStatus:String = ""
        
        switch passenger.paxType!.uppercased() {
            
        case Constant.PAX_TYPE_ADULT: break
           // image1Attachment.image = #imageLiteral(resourceName: "adultOptional") //UIImage(named:"adultOptional.png")
            
        case  Constant.PAX_TYPE_CHILD:
            //image1Attachment.image = #imageLiteral(resourceName: "childOptional")
           bedStatus = self.getChildBedStaus(passenger: passenger)

           // image1Attachment.image = self.getChildBedStaus(passenger: passenger)
            //image1Attachment.bounds = CGRect(x:0,y: offsetY,width:image1Attachment.image!.size.width,height:image1Attachment.image!.size.height)
            break
        default: break
           // image1Attachment.image = #imageLiteral(resourceName: "adultOptional")//UIImage(named:"adultOptional.png")
        }
        
        // wrap the attachment in its own attributed string so we can append it
        let image1String = NSAttributedString(attachment: image1Attachment)
         let bedStausLabelAttr = [convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor): UIColor.lightGray, convertFromNSAttributedStringKey(NSAttributedString.Key.font): UIFont(name:"Roboto-Regular", size:15)] as [String : Any]
        
        let bedStatusString = NSAttributedString(string: bedStatus , attributes:convertToOptionalNSAttributedStringKeyDictionary(bedStausLabelAttr))
        
        fullString.append(bedStatusString)
        
        // add the NSTextAttachment wrapper to our full string, then add some more text.
        //fullString.append(image1String)
        //        fullString.append(NSAttributedString(string: "End of text"))
        
     
        cell.textLabel?.attributedText = fullString
        
        }
        
        return cell

     }
    
  /*public func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
   {
    
        let nibArray = Bundle.main.loadNibNamed("RoomSectionFooterTableViewCell", owner: self, options: nil)
    
        let roomDetailsFooterView = nibArray?[0] as? RoomSectionFooterTableViewCell
    
        return roomDetailsFooterView
    
    }
    
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 40 // for example
    }*/
    
    func getChildBedStausImage(passenger:Passenger) -> UIImage
    {
        if let roomtype:String = passenger.roomType
        {
            switch roomtype.uppercased() {
            case Constant.Rooms.CHILD_WITH_BED,Constant.Rooms.JC:
                return  #imageLiteral(resourceName: "bed")
            case Constant.Rooms.CHILD_WITH_NO_BED:
                return #imageLiteral(resourceName: "withoutBed")
            default:
                 return #imageLiteral(resourceName: "withoutBed")
            }
        }
        return UIImage()
    }
    
    func getChildBedStaus(passenger:Passenger) -> String
    {
        if let roomtype:String = passenger.roomType
        {
            switch roomtype.uppercased() {
            case Constant.Rooms.CHILD_WITH_BED,Constant.Rooms.JC:
                return  Constant.Rooms.CHILD_WITH_BED_TEXT//#imageLiteral(resourceName: "bed")
            case Constant.Rooms.CHILD_WITH_NO_BED:
                return Constant.Rooms.CHILD_WITH_NO_BED_TEXT//#imageLiteral(resourceName: "withoutBed")
            default:
                return ""//#imageLiteral(resourceName: "withoutBed")
            }
        }
        return ""
    }
    
    
    
    func getUniqueRoom() -> Array<String> {
        
        let roomNumbers = passengerDetailsArray?.compactMap { $0.roomNumber } ?? []
        
        let roomNumberSet:Set<String> = Set(roomNumbers)
        //array.sorted {$0.localizedStandardCompare($1) == .orderedAscending}
        
        return Array(roomNumberSet)
        
    }
    
    func sortRoom(uniqueRoomArray:Array<String>)-> Array<String>
    {
         let sortedRoomArray = uniqueRoomArray.sorted(by: { (room1, room2) -> Bool in
            
            Int(room1)! < Int(room2)!
            
            })
        
            return sortedRoomArray
            
       
        
       
    }
    
    func getPassengerInRoom(by row:Int) -> Array<Passenger> {
        
        let roomNumber = roomNumberArray?[row]
        
       let roomPassengerDetails = passengerDetailsArray?.filter(){ $0.roomNumber == roomNumber }
        
        return roomPassengerDetails!;
        
    }
    
    func getRoomSharingType(passengerArray:Array<Passenger>) -> String
    {
       let filterPassengerArray = passengerArray.filter(){$0.paxType?.uppercased() == Constant.PAX_TYPE_ADULT }
        
        if filterPassengerArray.count > 0
        {
            let passenger:Passenger =  filterPassengerArray.first!
            
            if let roomType = passenger.roomType
            {
                switch roomType.uppercased() {
                case Constant.Rooms.ROOM_TYPE_SINGLE:
                    
                    return Constant.Rooms.SINGLE_SHARING_TEXT
                    
                case Constant.Rooms.ROOM_TYPE_DOUBLE:
                    
                    return Constant.Rooms.DOUBLE_SHARING_TEXT
                    
                case Constant.Rooms.ROOM_TYPE_TRIPLE:
                    
                    return Constant.Rooms.TRIPLE_SHARING_TEXT
                default:
                    return ""
                }
            }
        
        }
        return ""
    }
    
}



// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromNSAttributedStringKey(_ input: NSAttributedString.Key) -> String {
	return input.rawValue
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}
