//
//  IntroPageContentVC.m
//  holiday
//
//  Created by Pushpendra Singh on 04/08/15.
//  Copyright (c) 2015 Mobicule. All rights reserved.
//

#import "IntroPageContentVC.h"

@interface IntroPageContentVC ()

@end

@implementation IntroPageContentVC

- (void)viewDidLoad {
    [super viewDidLoad];
   
    _imgInfoContent.image=[UIImage imageNamed:_imageName];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
