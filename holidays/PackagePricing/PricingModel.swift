//
//  PricingModel.swift
//  sotc-consumer-application
//
//  Created by ketan on 30/07/18.
//  Copyright © 2018 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit

class PricingModel: NSObject
{
    var pricingTourType :String = ""
    var pricinghubCode:String = ""
    var pricingDepartureCity:String = ""
    var pricingArrayRooms : Array<RoomModel> = []
    var pricingTourDate :String = ""
    var ltProdCode : String = ""
    var pricingMobileNumber :String = ""
    var termsConditionAccept : Bool = false
    var applyPromocodeSuccess : Bool = false
    var selectedTourType : String? = "Standard"
    var packageClassId : Int = 0
    var hubcodeId : Int = 0
    var agreedOnTermsAndCondition : Bool = true
    var ltItineraryCode : String = ""
    var onReqeustDate:Bool = false
    var mainPackageClassId : Int = 0
//    VJ_Added
    var promocodeAmount : Double = 0.0
    var discountFullPrice : Double = 0.0
    // Pricing response

//    VJ_Added Promocode vala Part
//    var promocodeFixedAmount : Double = 15000.0 // Fixed
    var promocodeFixedAmount : Double = 0.0 // It sets When API response Came
    var advancePayableFixedAmount : Double = 30000.0
    var promocodeAppliedText : String = ""
    
    var gSTDescription : Array<Dictionary<String,Any>> = Array.init()
    var currencyBreakup : Array<Dictionary<String,Any>> = Array.init()
    var advancePayableAmount : Double = 0.0
    var totalTourCost : String = ""
    var totalServicetaxAdvanceDiscount : Double = 0.0
    var discountAdvancePrice : Double = 0.0
    var totalServicetax : Double = 0.0
    var totalOptionalCost : Double = 0.0
    var tcsAmount : Double = 0.0
    var tcsPercentage : Double = 0.0
    var isInternational : Bool = false
    func getAdultCount() -> Int {
        var count : Int = 0
        for room in pricingArrayRooms
        {
            count += room.adultCount
        }
        return count
    }
    
    func getchildCount() -> Int {
        var count : Int = 0
        for room in pricingArrayRooms
        {
            count = count + room.childWithoutBedCount + room.childWithBedCount
        }
        return count
    }
    
    func getInfantCount() -> Int {
        var count : Int = 0
        for room in pricingArrayRooms
        {
            count += room.infantCount
        }
        return count
    }
}
