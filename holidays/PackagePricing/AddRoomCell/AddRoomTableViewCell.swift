//
//  AddRoomTableViewCell.swift
//  sotc-consumer-application
//
//  Created by ketan on 30/08/18.
//  Copyright © 2018 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit

protocol addRoomDelegate
{
    func addRoomClicked(_ sender:UIButton)
}

class AddRoomTableViewCell: UITableViewCell {

    var delegate:addRoomDelegate?
    @IBOutlet weak var buttonAddRoom: UIButton!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
        
//        buttonAddRoom.layer.borderColor = AppUtility.hexStringToUIColor(hex: "#CC0000").cgColor
        buttonAddRoom.layer.borderColor = AppUtility.hexStringToUIColor(hex: "#004E9B").cgColor
        buttonAddRoom.roundedCorners()

    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func onAddRoomButtonClicked(_ sender: Any)
    {
        self.delegate?.addRoomClicked(sender as! UIButton)
    }
    
}
