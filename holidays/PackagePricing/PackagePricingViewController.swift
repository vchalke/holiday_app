//
//  PackagePricingViewController.swift
//  sotc-consumer-application
//
//  Created by ketan on 27/07/18.
//  Copyright © 2018 Mobicule Technologies Private Limited. All rights reserved.
//
// 13 Jun Suggestion from Arati mam
// Don,t follow given UI make it same as SOTC or any other way or adjust it at ur end
//Bu7t try to add promo code view
import UIKit
protocol ReloadPricing {
    func reloadTable()
}
@objc protocol PackagePricingViewControllerDelegate : NSObjectProtocol {
    @objc func addMobileForLeadCreation(mobNumber: String)
}
class PackagePricingViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,NumberOfTravellersDelegate,paymentTermsProtocol,TourDateTableViewCellDelegate,addRoomDelegate,FareCalendarDelegate,ReloadPricing,PreBookingQuotationVCDelegate
    
{
   
    
    
//    @IBOutlet weak var headerView: UIView!
    func addAddDeleteRoom(_ sender: UIButton) {
        
    }
    @objc var completePackageDetailDict:[String:Any]=[:]
    @IBOutlet weak var calculatePriceButton: UIButton!
    var packageDetailModel:PackageDetailsModel = PackageDetailsModel()
    var pricingModel:PricingModel = PricingModel()
    var previousPricingModel:PricingModel = PricingModel()
    var indexpathForDropDown : IndexPath = IndexPath(row: 0, section: 2)
    var quotationViewController : QuotationView = QuotationView.init()
    @objc weak var delegates : PackagePricingViewControllerDelegate? = nil
    
    @IBOutlet weak var tableViewForPricing: UITableView!
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        let model = PackageDetailsModel()
        model.initWithDictionary(dict: completePackageDetailDict)
        packageDetailModel = model
        print(model.packageId)
        print(packageDetailModel.packageId)
        
        let roomModel :RoomModel = RoomModel()
        pricingModel.pricingArrayRooms.append(roomModel)

        tableViewForPricing .register(UINib.init(nibName: "MobileNumberTableViewCell", bundle: nil), forCellReuseIdentifier: "cellReuseIdentifierMobileNumber")
        
        tableViewForPricing .register(UINib.init(nibName: "TourDateTableViewCell", bundle: nil), forCellReuseIdentifier: "cellReuseIdentifierTourDate")
         
        tableViewForPricing .register(UINib.init(nibName: "NumberOfTravellersTableViewCell", bundle: nil), forCellReuseIdentifier: "cellReuseIdentifierNumberTravellers")
        
        tableViewForPricing .register(UINib.init(nibName: "DepartureCityTableViewCell", bundle: nil), forCellReuseIdentifier: "cellReuseIdentifierDepartureCity")
        
        tableViewForPricing .register(UINib.init(nibName: "TourTypeTableViewCell", bundle: nil), forCellReuseIdentifier: "cellReuseIdentifierTourType")
        
        tableViewForPricing .register(UINib.init(nibName: "PaymentTermsConditionsTableViewCell", bundle: nil), forCellReuseIdentifier: "cellReuseIdentifierPaymentTerms")
        
        tableViewForPricing .register(UINib.init(nibName: "AddRoomTableViewCell", bundle: nil), forCellReuseIdentifier: "cellReuseIdentifierAddRoomTableViewCell")
        tableViewForPricing.contentInset = UIEdgeInsets(top: -1.0, left: 0.0, bottom: 0.0, right: 0.0);
//        headerView.fadedShadow()
        AppUtility.checkVersioning(completionBlock: { (isVersion) in
            
        })
        //Do any additional setup after loading the view.
        self.setTourTypeArray()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            
            self.tableViewForPricing.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height, right: 0)
            
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        //tableViewBottom.constant = 0.0
        self.tableViewForPricing.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    func setTourTypeArray(){
        printLog("packageDetailModel.arrayTourType \(packageDetailModel.arrayTourType)")
        if (packageDetailModel.arrayTourType .contains("Standard")){
            self.pricingModel.packageClassId = 0
        }else if (packageDetailModel.arrayTourType .contains("Delux")){
            self.pricingModel.packageClassId = 1
        }else if (packageDetailModel.arrayTourType .contains("Premium")){
            self.pricingModel.packageClassId = 2
        }else{
            self.pricingModel.packageClassId = 0
        }
        self.pricingModel.mainPackageClassId = self.pricingModel.packageClassId
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidLayoutSubviews() {
        self.calculatePriceButton.layer.cornerRadius = 3
        self.calculatePriceButton.clipsToBounds = true
    }
    override func viewWillAppear(_ animated: Bool) {
        self.tableViewForPricing.reloadData()
    }
    func validateField() -> String
    {
        if self.pricingModel.pricinghubCode.isEmpty {
            return "Please select departure city."
        }
        if self.pricingModel.pricingTourDate.isEmpty {
                return "Please select departure date."
        
        }
        if self.pricingModel.pricingMobileNumber.isEmpty {
            return "Please enter mobile number."
        }
        
        if !AppUtility.isValidMobileNumber(MobileNumber: self.pricingModel.pricingMobileNumber)
        {
            return "Please enter valid mobile number."
        }
        
        if !self.pricingModel.agreedOnTermsAndCondition {
            return "Please agree on Terms And Condition."
        }
        //            Below Code Is Commented beacuse its decided to show oops msg after showing calculations
        //            On 24_July_20
//        if self.pricingModel.onReqeustDate == true || self.packageDetailModel.pkgStatusId != 1 {
//            return "Oops! Due to limited availability, we are not able to process your request through mobile app. Please click on the below mentioned Call button and share your details 18002093344"
//        }
        if (self.packageDetailModel.productId == 11) {
            return "Oops! Due to limited availability, we are not able to process your request through mobile app. Please click on the below mentioned Call button and share your details \(ContactHelpline.TCHelpLineNumber)"
        }
        return ""
    }
    
    //MARK: - table view delegates -
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 2
        {
            return pricingModel.pricingArrayRooms.count + 1
        }
        else if section == 4
        {
            return 4
        }
        else if section == 0
        {
            return 0
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        if indexPath.section == 0
        {
            let cell : TourTypeTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cellReuseIdentifierTourType", for: indexPath) as! TourTypeTableViewCell
            cell.packageDetailModel = self.packageDetailModel
            cell.pricingModel = self.pricingModel
            cell.reloadDelegate = self
          /*  let button = UIButton(type: .custom)
            button.setImage(#imageLiteral(resourceName: "pricingDownArrow"), for: .normal)
            button.imageEdgeInsets = UIEdgeInsetsMake(0, -16, 0, 0)
            button.frame = CGRect(x: CGFloat(cell.tourTypeTextfield.frame.size.width - 25), y: CGFloat(5), width: CGFloat(30), height: CGFloat(30))
             button.isUserInteractionEnabled = false*/
            cell.tourTypeTextfield.text = self.pricingModel.selectedTourType
            //cell.tourTypeTextfield.rightView = button
           // cell.tourTypeTextfield.rightViewMode = .always
           // cell.setTourType()
            return cell
        }
        else if indexPath.section == 1
        {
            let cell : DepartureCityTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cellReuseIdentifierDepartureCity", for: indexPath) as! DepartureCityTableViewCell
           /* let button = UIButton(type: .custom)
            button.setImage(#imageLiteral(resourceName: "pricingDownArrow"), for: .normal)
            button.imageEdgeInsets = UIEdgeInsetsMake(0, -16, 0, 0)
            button.frame = CGRect(x: CGFloat(cell.textFieldDepartureCity.frame.size.width - 25), y: CGFloat(5), width: CGFloat(30), height: CGFloat(30))
            button.isUserInteractionEnabled = false*/
            cell.textFieldDepartureCity.text = self.pricingModel.pricingDepartureCity
            //cell.textFieldDepartureCity.rightView = button
            cell.reloadDelegate = self
            //cell.textFieldDepartureCity.rightViewMode = .always
            cell.packageDetailModel = packageDetailModel
            cell.pricingModel = pricingModel
            return cell
        }
        else if indexPath.section == 2
        {
            if(indexPath.row != pricingModel.pricingArrayRooms.count)
            {
                
                let cell : NumberOfTravellersTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cellReuseIdentifierNumberTravellers", for: indexPath) as! NumberOfTravellersTableViewCell
                
                cell.delegate = self
               
                if indexPath == indexpathForDropDown
                {
                    //cell.buttonDropDown.setImage(UIImage.init(named: "pricingUpArrow"), for: .normal)
                    //cell.buttonDeleteRoom.isHidden = true
                   // cell.buttonDeleteRoom.isHidden = false
                    cell.constraintForClosedCell.constant = 235;
                    cell.labelFourPeaple.isHidden = false
                }
                else
                {
                    
                    //cell.buttonDropDown.setImage(UIImage.init(named: "pricingDownArrow"), for: .normal)
                    // cell.buttonDeleteRoom.isHidden = true
                    cell.constraintForClosedCell.constant = 235;
                   // cell.buttonDeleteRoom.isHidden = false
                    cell.constraintForClosedCell.constant = 0;
                    cell.labelFourPeaple.isHidden = true
                }
                
//                if pricingModel.pricingArrayRooms.count == 1
//                {
//                    cell.buttonDeleteRoom.isHidden = true
//                }
                
                if indexPath.row == 0
                {
                    cell.buttonDeleteRoom.isHidden = true
                }
                else
                {
                    cell.buttonDeleteRoom.isHidden = false
                }
                
                
                cell.labelRoomNumber.text = "Room \(indexPath.row+1)"
                let roomModel:RoomModel = pricingModel.pricingArrayRooms[indexPath.row]
                roomModel.roomNumber = indexPath.row + 1
                cell.setDataForRooming(roomModel)
                
                return cell
            }
            else
            {
                let cell : AddRoomTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cellReuseIdentifierAddRoomTableViewCell", for: indexPath) as! AddRoomTableViewCell
                cell.delegate = self as? addRoomDelegate
                
                return cell
            }
        }
        else if indexPath.section == 3
        {
            let cell : TourDateTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cellReuseIdentifierTourDate", for: indexPath) as! TourDateTableViewCell
            cell.textFieldTourDate.text = self.pricingModel.pricingTourDate
            cell.delegate = self
            cell.pricingModel = self.pricingModel
            cell.textFieldTourDate.text = self.pricingModel.pricingTourDate
            return cell
        }
        else
        {
            if indexPath.row == 0
            {
                let cell : MobileNumberTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cellReuseIdentifierMobileNumber", for: indexPath) as! MobileNumberTableViewCell
                cell.pricingModel = self.pricingModel
                cell.packageDetailModel = self.packageDetailModel
                cell.setData()
                cell.delegate = self
                return cell
            }
            else if indexPath.row == 1
            {
                let cell :PaymentTermsConditionsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cellReuseIdentifierPaymentTerms", for: indexPath) as! PaymentTermsConditionsTableViewCell
                cell.delegate = self;
                cell.viewContainer.tag = indexPath.row
                cell.packageDetailModel = self.packageDetailModel
                cell.labelTitle.text = "PAYMENT TERMS"
                return cell
            }
            else if indexPath.row == 2
            {
                let cell : PaymentTermsConditionsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cellReuseIdentifierPaymentTerms", for: indexPath) as! PaymentTermsConditionsTableViewCell
                cell.delegate = self;
                cell.viewContainer.tag = indexPath.row
                cell.packageDetailModel = self.packageDetailModel
                cell.labelTitle.text = "CANCELLATION POLICY"
                return cell
            }
            else
            {
                let cell : PaymentTermsConditionsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cellReuseIdentifierPaymentTerms", for: indexPath) as! PaymentTermsConditionsTableViewCell
                cell.delegate = self;
                cell.viewContainer.tag = indexPath.row
                cell.packageDetailModel = self.packageDetailModel
                cell.labelTitle.text = "TERMS & CONDITIONS"
                return cell
            }
            
        }
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 5
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
//        return 30
//        return (section == 0) ? 0 : 30
        return 0
    }
    /*
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let viewHeader = UIView(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.size.width, height: 30))
        let  headerLabel = UILabel(frame: CGRect.init(x: 20, y: 10, width: tableView.frame.size.width-20, height: 20))
        
        headerLabel.font = UIFont.init(name: "Roboto-Bold", size: 19)
        headerLabel.textColor = AppUtility.hexStringToUIColor(hex: "333333")
        
        if section == 0
        {
            headerLabel.text = "Tour Type"
        }
        else if section == 1
        {
            headerLabel.text = "Departure City"
        }
        else if section == 2
        {
            headerLabel.text = "No. of Travellers"
        }
        else if section == 3
        {
            headerLabel.text = "Departure Date"
        }
        else if section == 4
        {
            headerLabel.text = "Mobile"
        }
        
        viewHeader.addSubview(headerLabel)
        
        return viewHeader // done by komal
    }
    */
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.section == 0
        {
            return 0;//65;
        }
        else if indexPath.section == 1 || indexPath.section == 3
        {
            return 62;
        }
        else if indexPath.section == 2
        {
            if indexPath.row == pricingModel.pricingArrayRooms.count
            {
                return 75
            }
            else if indexPath == indexpathForDropDown
            {
                return 306
            }
            
            return 60;
            
        }
        else
        {
            if indexPath.row != 0
            {
                return 65
            }
            return 145;
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.section == 0
        {
//            return 65;
            return 0;
        }
        else if indexPath.section == 1 || indexPath.section == 3
        {
            return 62;
        }
        else if indexPath.section == 2 // Add rooms
        {
            
            if indexPath.row == pricingModel.pricingArrayRooms.count
            {
                return 75
            }
            else if indexPath == indexpathForDropDown
            {
                return 306
            }
            
            return 60;
            
        }
        else
        {
            if indexPath.row != 0
            {
                return 60  //terms conditions
            }
            return 125; // mobile number
        }
    }
    
    //MARK:- Fare calendar Delegate -
    
    func selectedDate(dateModel: DayModel) {
        if dateModel.date != nil
        {
            self.pricingModel.pricingTourDate = AppUtility.convertDateToString(date: dateModel.date ?? Date.init(), dateFormat: "dd-MM-yyyy")
            self.pricingModel.ltProdCode = dateModel.ltProdCode
            self.pricingModel.ltItineraryCode = dateModel.ltItineraryCode
            self.pricingModel.onReqeustDate = dateModel.onRequest
            self.tableViewForPricing.reloadData()
        }
        self.previousPricingModel = self.pricingModel
        printLog(dateModel.ltProdCode)
        printLog(dateModel.ltItineraryCode)
        printLog(dateModel.onRequest)
    }
    //MARK:- tour Date Delegate -
    
    func openFareCalendar()
    {
        let fareCalendarVc : CalendarViewController = CalendarViewController(nibName: "CalendarViewController", bundle: nil)
        fareCalendarVc.pricingModel = self.pricingModel
        fareCalendarVc.packageDetailModel = self.packageDetailModel
        fareCalendarVc.delegate = self
        self.navigationController?.pushViewController(fareCalendarVc, animated: true)
    }
    
    //MARK: - addRoomDelegate -
    
    
    func addAddDeleteMember(_ sender: UIButton)
    {
        guard let cell = sender.superview?.superview?.superview?.superview as? NumberOfTravellersTableViewCell else
        {
            return // or fatalError() or whatever
        }
        
        let indexPath = tableViewForPricing.indexPath(for: cell)
        
        let roomModel :RoomModel = pricingModel.pricingArrayRooms[(indexPath?.row)!]
        var packageType : String = "Domestic"
        if self.packageDetailModel.packageTypeId == 1
        {
            packageType = "International"
        }
        if sender.tag == 12||sender.tag == 22||sender.tag == 32||sender.tag == 42
        {
            if sender.tag == 12
            {
                
                roomModel.validateRoom(packageType: packageType, packageSubtype: self.packageDetailModel.packageSubTypeName, regionID: self.packageDetailModel.continentName, typeOfPassenger: "Adult", action: "add")
            }
            else if sender.tag == 22
            {
                roomModel.validateRoom(packageType: packageType, packageSubtype: self.packageDetailModel.packageSubTypeName, regionID: self.packageDetailModel.continentName, typeOfPassenger: "CNB", action: "add")
            }
            else if sender.tag == 32
            {
                roomModel.validateRoom(packageType: packageType, packageSubtype: self.packageDetailModel.packageSubTypeName, regionID: self.packageDetailModel.continentName, typeOfPassenger: "CWB", action: "add")
            }
            else
            {
                roomModel.validateRoom(packageType: packageType, packageSubtype: self.packageDetailModel.packageSubTypeName, regionID: self.packageDetailModel.continentName, typeOfPassenger: "Infant", action: "add")
            }
            
            
        }
        else
        {
            if sender.tag == 11
            {
                roomModel.validateRoom(packageType: packageType, packageSubtype: self.packageDetailModel.packageSubTypeName, regionID: self.packageDetailModel.continentName, typeOfPassenger: "Adult", action: "remove")
            }
            else if sender.tag == 21
            {
                roomModel.validateRoom(packageType: packageType, packageSubtype: self.packageDetailModel.packageSubTypeName, regionID: self.packageDetailModel.continentName, typeOfPassenger: "CNB", action: "remove")
                
            }
            else if sender.tag == 31
            {
                roomModel.validateRoom(packageType: packageType, packageSubtype: self.packageDetailModel.packageSubTypeName, regionID: self.packageDetailModel.continentName, typeOfPassenger: "CWB", action: "remove")
            }
            else
            {
                roomModel.validateRoom(packageType: packageType, packageSubtype: self.packageDetailModel.packageSubTypeName, regionID: self.packageDetailModel.continentName, typeOfPassenger: "Infant", action: "remove")
            }
            
        }
        
        tableViewForPricing.reloadRows(at: [indexPath!], with: UITableView.RowAnimation.automatic)
    }
    
    //MARK: - PAYMENT TERMS DLEGATE -
    
    func onCellClicked(_ sender: UITapGestureRecognizer)
    {
        printLog("Payment terms clicked")
    }
    
    @IBAction func onBackButtonClicked(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickofCalculatePrice(_ sender: Any)
    {
       
        let msg : String = self.validateField()
        if  msg == ""
        {
            self.calculatePrice()
            
        }
        else
        {
//            AppUtility.displayAlert(title:"Alert", message: msg)
            self.alertViewControllerSingleton(title: "Alert", message: msg, type: .alert, buttonArryName: ["Ok"]) { (strin) in
                printLog(strin)
            }
        }
        self.delegates?.addMobileForLeadCreation(mobNumber: self.pricingModel.pricingMobileNumber)
//        self.jumpToBookingConfirmation()
    }
    func jumpToBookingConfirmation(){
//        let bookingConfirmVC : BookingConfirmOne = BookingConfirmOne(nibName: "BookingConfirmOne", bundle: nil)
//        self.navigationController?.pushViewController(bookingConfirmVC, animated: true)
    }
    @IBAction func btn_callHelpline(_ sender: Any)
    {
        AppUtility.callToTCFromPhaseTwo()
    }
    func dropDownClicked(_ sender: UIButton)
    {
        guard let cell = sender.superview?.superview?.superview as? NumberOfTravellersTableViewCell else
        {
            return // or fatalError() or whatever
        }
        
        let indexPath = tableViewForPricing.indexPath(for: cell)
        
        if indexPath != indexpathForDropDown
        {
            indexpathForDropDown = indexPath ?? IndexPath(row: 0, section: 2)
        }
        else
        {
            indexpathForDropDown = IndexPath()
        }
        // let roomModel :RoomModel = pricingModel.pricingArrayRooms[(indexPath?.row)!]
        
       // tableViewForPricing.reloadSections([(indexPath?.section)!], with: UITableViewRowAnimation.automatic)
        
        tableViewForPricing.reloadRows(at: [indexPath!], with: UITableView.RowAnimation.automatic)
        self.tableViewForPricing.reloadData()
    }
    
    
    func deleteMemberClicked(_ sender: UIButton)
    {
        guard let cell = sender.superview?.superview?.superview as? NumberOfTravellersTableViewCell else
        {
            return // or fatalError() or whatever
        }
        
        let indexPath = tableViewForPricing.indexPath(for: cell)
        
        //  indexpathForDropDown  = IndexPath(row: 0, section: 2)
        
        pricingModel.pricingArrayRooms.remove(at: (indexPath?.row)!)
        
        tableViewForPricing.reloadSections([(indexPath?.section)!], with: UITableView.RowAnimation.automatic)
    }
    
    func addRoomClicked(_ sender: UIButton)
    {
        guard let cell = sender.superview?.superview as? AddRoomTableViewCell else {
            return // or fatalError() or whatever
        }
        
        var indexPath = tableViewForPricing.indexPath(for: cell)
        
        if pricingModel.pricingArrayRooms.count < 4
        {
          let roomModel:RoomModel = RoomModel()
          pricingModel.pricingArrayRooms.append(roomModel)
        }
        
        
        //indexpathForDropDown = IndexPath(row: pricingModel.pricingArrayRooms.count-1, section: 2)
        
        tableViewForPricing.reloadSections([(indexPath?.section)!], with: UITableView.RowAnimation.automatic)
    }
    /*
    //MARK: - Pricing API hit -
    func calculatePrice()
    {
    PreBookingCommunicationManager.getData(requestType: "GET", queryParam: nil, pathParam: nil, jsonParam: nil, headers: nil, requestURL: "", returnInCaseOffailure: false, url: AuthenticationConstant.Prebooking_Last_Level_Banner_URL) {
        (status,response) in
        
       // LoadingIndicatorView.hide()
        
        printLog("status is \(status)")
        
        printLog("Response is \(String(describing: response))")
        if status == true
        {
            
            var responseData : Array<Dictionary<String,Any>> = response as? Array<Dictionary<String,Any>> ?? Array.init()
            
            if responseData.count > 0
            {
                if responseData.count > 3
                {
                    for _ in 0..<(responseData.count - 4)
                    {
                         responseData.removeLast()
                    }
                    
                }
                
                let dict : Dictionary<String , Array<Dictionary<String,Any>>> = Dictionary.init()
                print(dict)
            }
        }
        
    }
        
    let quoteVc : PreBookingQuotationViewController = PreBookingQuotationViewController(nibName: "PreBookingQuotationViewController", bundle: nil)
    quoteVc.pricingModel = self.pricingModel
    quoteVc.packageDetailModel = self.packageDetailModel
    quoteVc.quotationViewController = self.quotationViewController
    self.navigationController?.pushViewController(quoteVc, animated: true)
    }
    */
   
    // Delegates Of PreBookingQuotationVCDelegate
    
    func backFromQuotation(){
        //        let newPricingModel : PricingModel = PricingModel()
        //        self.pricingModel = newPricingModel
        //        self.pricingModel = self.previousPricingModel
        CalenderCommManager.fetchFareCaleder(pricingModel: self.pricingModel, packageDetailModel: self.packageDetailModel, selectDateStr: self.pricingModel.pricingTourDate) { (status, dayModel) in
            if status{
                if let isDate = dayModel{
                    if let availDate = isDate.date{
                        self.pricingModel.pricingTourDate = AppUtility.convertDateToString(date: availDate, dateFormat: "dd-MM-yyyy")
                        self.pricingModel.ltProdCode = isDate.ltProdCode
                        self.pricingModel.ltItineraryCode = isDate.ltItineraryCode
                        self.pricingModel.onReqeustDate = isDate.onRequest
                        self.tableViewForPricing.reloadData()
                    }else{
                        
                    }
                    
                }else{
                    
                }
                
            }else {
                
            }
        }
        self.tableViewForPricing.reloadData()
    }
    func calculatePrice()
    {
        
        LoadingIndicatorView.show("Loading")
        
        let dataDict : Dictionary<String,Any> = AppUtility.getPricingRequestJson(packageDetailModel: self.packageDetailModel, pricingModel: self.pricingModel, optionalArray: nil)
        // "holidayRS/pricing"
        print("DataDict \(dataDict)")
        PreBookingCommunicationManager.getPrebookingData(requestType:"POST", queryParam: nil, pathParam:nil, jsonParam: dataDict, headers: nil, requestURL:kAstraUrlPricing , returnInCaseOffailure: false)
        {
            (status,response) in
            
            printLog("status is \(status)")
            printLog("response is \(String(describing: response))")
            if status
            {
                
                if let responseDict : Dictionary<String,Any> =  response as? Dictionary<String,Any>
                {
                    if let msg : String = responseDict["message"] as? String
                    {
                        if msg != ""
                        {
//                            AppUtility.displayAlert(title: "Alert", message: msg)
                            self.alertViewControllerSingleton(title: "Alert", message: msg, type: .alert, buttonArryName: ["Ok"]) { (strin) in
                                printLog(strin)
                            }
                        }
                        
                    }
                    else
                    {
                        printLog("responseDict is \(responseDict)")
                        AppUtility.fillPricingModelWithResponseDict(dict: responseDict, pricingModel: self.pricingModel, quotationView: self.quotationViewController)
                        if(responseDict["totalPrice"]as? Double ?? 0 != 0){
                            self.quotationViewController.selectedPaymentType = "FULL_PAYMENT"
                            self.quotationViewController.selectedPaymentAmount = responseDict["totalPrice"] as? Double ?? 0
                            let quoteVc : PreBookingQuotationViewController = PreBookingQuotationViewController(nibName: "PreBookingQuotationViewController", bundle: nil)
                            quoteVc.pricingModel = self.pricingModel
                            quoteVc.packageDetailModel = self.packageDetailModel
                            quoteVc.quotationViewController = self.quotationViewController
                            quoteVc.pricingDict = responseDict
//                            let quotationId:String = responseDict["quotationId"] as? String ?? ""
//                            self.qouteCRM_Calculation(qoutationID: quotationId)
                            quoteVc.quoteDelegate = self
                            self.navigationController?.pushViewController(quoteVc, animated: true)
                        }else{
//                            AppUtility.displayAlert(title: "Alert", message: "Oops! Due to limited availability, we are not able to process your request through mobile app. Please click on the below mentioned Call button and share your details 18002093344")
                            self.alertViewControllerSingleton(title: "Error", message: "Oops! Due to limited availability, we are not able to process your request through mobile app. Please click on the below mentioned Call button and share your details 1800 209 9100", type: .alert, buttonArryName: ["Ok"]) { (strin) in
                                printLog(strin)
                            }
                        }
                        
                    }
                }
            }
            else
            {
                if let responseDict : Dictionary<String,Any> =  response as? Dictionary<String,Any>
                {
                    if let msg : String = responseDict["message"] as? String
                    {
//                        AppUtility.displayAlert(title: "Alert", message: msg)
                        self.alertViewControllerSingleton(title: "Alert", message: msg, type: .alert, buttonArryName: ["Ok"]) { (strin) in
                            printLog(strin)
                        }
                    }
                    else
                    {
//                        AppUtility.displayAlert(title: "Alert", message: "Some error has occurred")
                        self.alertViewControllerSingleton(title: "Alert", message: "Some error has occurred", type: .alert, buttonArryName: ["Ok"]) { (strin) in
                            printLog(strin)
                        }
                    }
                }
                else
                {
//                    AppUtility.displayAlert(title: "Alert", message: "Some error has occurred")
                    self.alertViewControllerSingleton(title: "Alert", message: "Some error has occurred", type: .alert, buttonArryName: ["Ok"]) { (strin) in
                        printLog(strin)
                    }
                }
            }
        }
    }
    
    //MARK: - Qoute to CRM
    //Dout is use in SOTC but in TC ?
    func qouteCRM_Calculation(qoutationID:String){
        var requestDict : Dictionary<String,String> = Dictionary.init()
        requestDict["referenceID"] = qoutationID
        requestDict["enquirySource"] = ""
        requestDict["enquiryMedium"] = ""
        requestDict["enquiryCampaign"] = ""
        requestDict["LP"] = ""
        requestDict["bookURL"] = ""
        requestDict["websiteTerm"] = ""
        requestDict["gaClientId"] = ""
//        "holidayRS/crm/quote"
        
        PreBookingCommunicationManager.getPrebookingData(requestType:"POST", queryParam: nil, pathParam:nil, jsonParam: requestDict, headers: nil, requestURL: "", returnInCaseOffailure: false)
        {
            (status,response) in
            
            printLog("status is \(status)")
            printLog("response is \(String(describing: response))")
        }
 
    }
    // MARK: - Navigate to VC
    
    func navigateToDetailView(Title title: String, description: String)
    {
        if (title == "Privacy Policy"){
            let privacyVC : PrivacyAndTermsVC = PrivacyAndTermsVC(nibName: "PrivacyAndTermsVC", bundle: nil)
            privacyVC.fileStr = title
            self.navigationController?.pushViewController(privacyVC, animated: true)
        }else{
            let detailVC : DetailViewController = DetailViewController(nibName: "DetailViewController", bundle: nil)
            detailVC.titleOfVc = title
            detailVC.htmlString = description
            self.navigationController?.pushViewController(detailVC, animated: true)
        }
        
        
    }
     // MARK: - delegate method
    func reloadTable() {
        self.tableViewForPricing.reloadData()
    }
    
}

