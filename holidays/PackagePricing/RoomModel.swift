//
//  RoomModel.swift
//  sotc-consumer-application
//
//  Created by ketan on 31/07/18.
//  Copyright © 2018 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit

class RoomModel: NSObject
{
    var roomNumber :Int  = 0
    var adultCount:Int = 2
    var childWithoutBedCount : Int = 0
    var childWithBedCount:Int = 0
    var infantCount:Int = 0
    
    func checkValidityForFourPassenger()-> Bool
    {
        if adultCount+childWithoutBedCount+childWithBedCount+infantCount < 3 && adultCount+childWithoutBedCount+childWithBedCount+infantCount > 0
        {
            return true
        }
        
        return false
    }
    
    func validateRoom(packageType:String,packageSubtype:String,regionID:String,typeOfPassenger:String,action:String)
    {
        var roomTuple : (adult:Int,CWB:Int,CNB:Int,infant:Int) = (adult:0,CWB:0,CNB:0,infant:0)
        if typeOfPassenger == "Adult"
        {
            if action == "add"
            {
                roomTuple = (adult:self.adultCount + 1,CWB:self.childWithBedCount,CNB:self.childWithoutBedCount,infant:self.infantCount)
            }
            else
            {
                roomTuple = (adult:self.adultCount - 1,CWB:self.childWithBedCount,CNB:self.childWithoutBedCount,infant:self.infantCount)
            }
            
        }
        else if typeOfPassenger == "CWB"
        {
            if action == "add"
            {
                roomTuple = (adult:self.adultCount ,CWB:self.childWithBedCount + 1,CNB:self.childWithoutBedCount,infant:self.infantCount)
            }
            else
            {
                roomTuple = (adult:self.adultCount ,CWB:self.childWithBedCount - 1,CNB:self.childWithoutBedCount,infant:self.infantCount)
            }
            
        }
        else if typeOfPassenger == "CNB"
        {
            if action == "add"
            {
                roomTuple = (adult:self.adultCount ,CWB:self.childWithBedCount,CNB:self.childWithoutBedCount + 1,infant:self.infantCount)
            }
            else
            {
                roomTuple = (adult:self.adultCount ,CWB:self.childWithBedCount,CNB:self.childWithoutBedCount - 1,infant:self.infantCount)
            }
            
        }
        else
        {
            if action == "add"
            {
                roomTuple = (adult:self.adultCount ,CWB:self.childWithBedCount,CNB:self.childWithoutBedCount,infant:self.infantCount + 1)
            }
            else
            {
                roomTuple = (adult:self.adultCount ,CWB:self.childWithBedCount,CNB:self.childWithoutBedCount,infant:self.infantCount - 1)
            }
            
        }
        var flag : Bool = false
        
        if (roomTuple.adult + roomTuple.CWB + roomTuple.CNB + roomTuple.infant) > 4 ||  (roomTuple.adult + roomTuple.CWB + roomTuple.CNB + roomTuple.infant) < 1
        {
            AppUtility.displayAlert(title: "Alert", message: "This rooming combination is not available")
           
        }
        else
        {
            switch packageType
            {
            case "International":
                switch packageSubtype.uppercased()
                {
                case "FIT":
                    flag  =  self.InternationalFIT(typeOfPassenger: roomTuple,regionID:regionID)
                default:
                    flag  =  self.InternationalGIT(typeOfPassenger: roomTuple,regionID:regionID)
                    
                }
                
                
            default:
                switch packageSubtype.uppercased()
                {
                case "FIT":
                     flag  =  self.domesticFIT(typeOfPassenger: roomTuple)
                default:
                    flag  =  self.domesticGIT(typeOfPassenger: roomTuple)
                }
            }
            if flag
            {
                self.adultCount = roomTuple.adult
                self.childWithBedCount = roomTuple.CWB
                self.childWithoutBedCount = roomTuple.CNB
                self.infantCount = roomTuple.infant
            }
            else
            {
                AppUtility.displayAlert(title: "Alert", message: "This rooming combination is not available")
            }
        }
    
    }
    
    func domesticFIT(typeOfPassenger : (adult:Int,CWB:Int,CNB:Int,infant:Int)) -> Bool
    {
        switch typeOfPassenger
        {
        case (1,0,0,_),(2,0,0,_),(3,0,0,_),(1,1,0,_),(1,0,1,_),(1,2,0,_),(1,0,2,_),(2,1,0,_),(2,0,1,_),(2,0,2,_),(3,0,1,_),(2,1,1,_),(1,1,1,_):
            return true
            
        default:
            printLog("Combination not allowed.")
            return false
            
        }
    }
    
    func domesticGIT(typeOfPassenger : (adult:Int,CWB:Int,CNB:Int,infant:Int)) -> Bool
    {
        switch typeOfPassenger
        {
        case (1,0,0,_),(2,0,0,_),(3,0,0,_),(1,1,0,_),(1,0,1,_),(1,2,0,_),(1,0,2,_),(2,1,0,_),(2,0,1,_),(2,0,2,_),(3,0,1,_),(2,1,1,_),(1,1,1,_):
            return true
            
        default:
            printLog("Combination not allowed.")
            return false
            
        }
    }
    
    func InternationalFIT(typeOfPassenger : (adult:Int,CWB:Int,CNB:Int,infant:Int),regionID:String) -> Bool
    {
        
        switch regionID.lowercased()
        {
        case "australia", "new zealand"  , "europe", "asia":
            switch typeOfPassenger
            {
            case (1,0,0,_),(2,0,2,_),(2,0,0,_),(3,0,0,_),(4,0,0,_),(1,1,0,_),(1,0,1,_),(1,2,0,_),(1,0,2,_),(2,1,0,_),(2,0,1,_),(2,1,1,_),(1,1,1,_):
                return true
                
            default:
                printLog("Combination not allowed.")
                return false
                
            }
        default:
            
            switch typeOfPassenger
            {
            case (1,0,0,_),(2,0,2,_),(2,0,0,_),(3,0,0,_),(4,0,0,_),(1,1,0,_),(1,0,1,_),(1,2,0,_),(1,0,2,_),(2,1,0,_),(2,0,1,_),(2,1,1,_),(1,1,1,_):
                return true
                
            default:
                printLog("Combination not allowed.")
                return false
            }
            
            
            
        }
        
    }
    func InternationalGIT(typeOfPassenger : (adult:Int,CWB:Int,CNB:Int,infant:Int),regionID:String) -> Bool
    {
       
        switch regionID.lowercased()
        {
        case "australia", "new zealand"  , "europe", "asia":
            switch typeOfPassenger
            {
                case (1,0,0,_),(1,1,0,_),(2,0,0,_),(2,1,0,_),(2,1,0,_),(3,1,0,_),(1,2,0,_),(2,0,1,_),(3,0,0,_),(1,0,1,_),(3,0,0,_),(1,0,1,_),(3,0,1,_),(1,0,1,_),(3,0,1,_),(2,0,2,_),(1,0,2,_),(2,1,1,_):
                return true
                
            default:
                 printLog("Combination not allowed.")
                return false
               
            }
         default:
            
            switch typeOfPassenger
            {
            case (1,0,0,_),(2,0,0,_),(2,1,0,_),(2,0,1,_),(1,0,1,_),(3,0,0,_),(1,0,2,_),(2,0,2,_),(3,0,1,_):
                return true
                
            default:
                printLog("Combination not allowed.")
                return false
        }
        
       
    
        }
       
    }
}
