//
//  TourTypeTableViewCell.swift
//  sotc-consumer-application
//
//  Created by ketan on 28/07/18.
//  Copyright © 2018 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit

class TourTypeTableViewCell: UITableViewCell,UITextFieldDelegate
{
    
    @IBOutlet weak var textfieldContainerView: UIView!
    @IBOutlet weak var tourTypeTextfield: UITextField!
     var reloadDelegate : ReloadPricing? = nil
    
    var pricingModel:PricingModel = PricingModel()
    var packageDetailModel:PackageDetailsModel?
    
    @IBOutlet weak var textFieldTourType: UITextField!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
        self.textfieldContainerView.fadedShadow()
        self.textfieldContainerView.roundedCorners()
        self.tourTypeTextfield.delegate = self
        
        //        viewStandard.fadedShadow()
        //        viewStandard.roundedCorners()
        //
        //        viewDeluxe.fadedShadow()
        //        viewDeluxe.roundedCorners()
        //
        //        viewPremium.fadedShadow()
        //        viewPremium.roundedCorners()
        //
        //        viewStandard.layer.borderWidth = 2
        //        viewDeluxe.layer.borderWidth = 2
        //        viewPremium.layer.borderWidth = 2
        //
        //        let tapGestureStandard = UITapGestureRecognizer.init(target: self, action: #selector(self.onStandardClicked(_:)))
        //        viewStandard.addGestureRecognizer(tapGestureStandard)
        //
        //
        //          let tapGestureDeluxe = UITapGestureRecognizer.init(target: self, action: #selector(self.onDeluxeClicked(_:)))
        //        viewDeluxe.addGestureRecognizer(tapGestureDeluxe)
        //
        //
        //          let tapGesturePremium = UITapGestureRecognizer.init(target: self, action: #selector(self.onPremiumClicked(_:)))
        //        viewPremium.addGestureRecognizer(tapGesturePremium)
        //
    }
    /*
     func setTourType()
     {
     let totalWidth: CGFloat  = self.frame.size.width-32;
     let segmentWidth :CGFloat = totalWidth/3
     widthConstraintScrollView.constant = segmentWidth * CGFloat((packageDetailModel?.arrayTourType.count)!)
     
     self.layoutIfNeeded()
     self.startingPriceStandard.text = "₹ " + "\(self.packageDetailModel?.startingPriceStandard ?? 0)"
     self.startingPriceDeluxLabel.text = "₹ " +  "\(self.packageDetailModel?.startingPriceDelux ?? 0)"
     self.startingPricePremiumLabel.text = "₹ " +  "\(self.packageDetailModel?.startingPricePremium ?? 0)"
     if let tourType = self.pricingModel.selectedTourType
     {
     self.pricingModel.selectedTourType = tourType
     }
     else
     {
     self.pricingModel.selectedTourType = packageDetailModel?.arrayTourType.first
     }
     
     
     self.setTourTypeColor(self.pricingModel.selectedTourType!)
     
     
     if packageDetailModel?.isPackageClassStandard == "y"
     {
     viewStandard.isHidden = false
     }
     else
     {
     viewStandard.isHidden = true
     }
     
     if packageDetailModel?.isPackageClassDelux == "y"
     {
     viewDeluxe.isHidden = false
     }
     else
     {
     viewDeluxe.isHidden = true
     }
     
     if packageDetailModel?.isPackageClassPremium == "y"
     {
     viewPremium.isHidden = false
     }
     else
     {
     viewPremium.isHidden = true
     }
     
     viewStandard.backgroundColor = AppUtility.hexStringToUIColor(hex: "f2f2f2")
     viewStandard.layer.borderColor = AppUtility.hexStringToUIColor(hex: "CC0000").cgColor
     
     
     //textFieldTourType.text =  packageDetailModel?.arrayTourType.first
     }*/
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: "Please select", message: "Tour type", preferredStyle: .actionSheet)
        
        for tourType in (packageDetailModel?.arrayTourType)!
        {
            let actionButton  = UIAlertAction(title: tourType, style: .default) { _ in
                self.tourTypeTextfield.text = tourType
                switch tourType
                {
                case "Standard":
                    self.pricingModel.selectedTourType = "Standard"
                    self.pricingModel.packageClassId = 0
                   
                case "Premium":
                    self.pricingModel.selectedTourType = "Premium"
                    self.pricingModel.packageClassId = 2
                default:
                    self.pricingModel.selectedTourType = "Deluxe"
                    self.pricingModel.packageClassId = 1
                }
                self.pricingModel.pricingDepartureCity = ""
                self.pricingModel.pricingTourDate = ""
                self.reloadDelegate?.reloadTable()
            }
            actionSheetControllerIOS8.addAction(actionButton)
        }
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            printLog("Cancel")
        }
        actionSheetControllerIOS8.addAction(cancelActionButton)
        
        self.window?.rootViewController?.present(actionSheetControllerIOS8, animated: true, completion: nil)
        
        
        return false
    }
    
    /*
     func onStandardClicked(_ sender :UITapGestureRecognizer?)
     {
     self.removeLayerColor()
     viewStandard.backgroundColor = AppUtility.hexStringToUIColor(hex: "f2f2f2")
     viewStandard.layer.borderColor = AppUtility.hexStringToUIColor(hex: "CC0000").cgColor
     
     viewDeluxe.backgroundColor = UIColor.white
     viewDeluxe.layer.borderColor = AppUtility.hexStringToUIColor(hex: "cdcdcd").cgColor
     
     viewPremium.backgroundColor = UIColor.white
     viewPremium.layer.borderColor = AppUtility.hexStringToUIColor(hex: "cdcdcd").cgColor
     
     self.pricingModel.selectedTourType = "Standard"
     self.pricingModel.packageClassId = 0
     
     
     }
     
     func onDeluxeClicked(_ sender :UITapGestureRecognizer?)
     {
     viewStandard.backgroundColor = UIColor.white
     viewStandard.layer.borderColor = AppUtility.hexStringToUIColor(hex: "cdcdcd").cgColor
     
     viewDeluxe.backgroundColor = AppUtility.hexStringToUIColor(hex: "f2f2f2")
     viewDeluxe.layer.borderColor = AppUtility.hexStringToUIColor(hex: "CC0000").cgColor
     
     viewPremium.backgroundColor = UIColor.white
     viewPremium.layer.borderColor = AppUtility.hexStringToUIColor(hex: "cdcdcd").cgColor
     
     self.pricingModel.selectedTourType = "Deluxe"
     self.pricingModel.packageClassId = 1
     
     }
     
     func onPremiumClicked(_ sender :UITapGestureRecognizer?)
     {
     viewStandard.backgroundColor = UIColor.white
     viewStandard.layer.borderColor = AppUtility.hexStringToUIColor(hex: "cdcdcd").cgColor
     
     viewDeluxe.backgroundColor = UIColor.white
     viewDeluxe.layer.borderColor = AppUtility.hexStringToUIColor(hex: "cdcdcd").cgColor
     
     viewPremium.backgroundColor = AppUtility.hexStringToUIColor(hex: "f2f2f2")
     viewPremium.layer.borderColor = AppUtility.hexStringToUIColor(hex: "CC0000").cgColor
     
     self.pricingModel.selectedTourType = "Premium"
     self.pricingModel.packageClassId = 2
     
     
     }
     
     func removeLayerColor()
     {
     viewStandard.layer.borderColor = UIColor.clear.cgColor
     viewDeluxe.layer.borderColor = UIColor.clear.cgColor
     viewPremium.layer.borderColor = UIColor.clear.cgColor
     }
     
     func setTourTypeColor(_ tourType:String)
     {
     self.pricingModel.selectedTourType = tourType;
     
     if tourType.lowercased() == "standard"
     {
     self.onStandardClicked(nil)
     }
     else if tourType == "deluxe" || tourType == "delux"
     {
     self.onDeluxeClicked(nil)
     }
     else
     {
     self.onPremiumClicked(nil)
     }
     
     }*/   //150618-00004
}
