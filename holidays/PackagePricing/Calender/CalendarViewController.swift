//
//  CalendarViewController.swift
//  sotc-consumer-application
//
//  Created by Mac on 31/07/18.
//  Copyright © 2018 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit
protocol FareCalendarDelegate {
    func selectedDate(dateModel: DayModel)
}
@objc protocol FareCalendarCompareDelegate : NSObjectProtocol {
    @objc func selectedDateInCompare(dateModel: String)
}
class CalendarViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var topNavigationView: UIView!
    @IBOutlet weak var calendarCollectionView: UICollectionView!
    var calendarModel : CalendarModel? = nil
    var pricingModel:PricingModel = PricingModel()
    var packageDetailModel:PackageDetailsModel = PackageDetailsModel()
    var delegate : FareCalendarDelegate? = nil
    @objc var pkgDict : [String:Any]?
    @objc var cityInfodict : [String:Any]?
    @objc var currentPackageClassId : Int = 0
    @objc weak var delegateCompare : FareCalendarCompareDelegate? = nil
    @objc var isFromCompare : Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        if let isPkgDict = pkgDict, let isCityDict = cityInfodict{
            let model = PackageDetailsModel()
            model.initWithDictionary(dict: isPkgDict)
            packageDetailModel = model
            printLog("isCityDict \(isCityDict)")
//            if let totalfup = (isCityDict["packageClassId"] as? NSString)?.intValue {
//                pricingModel.packageClassId = Int(totalfup)
//            }
//            if let hubCityCode = isCityDict["hubCityCode"] as? [String:Any]{
//                pricingModel.pricinghubCode = hubCityCode["cityCode"] as! String
//            }
            pricingModel.packageClassId = currentPackageClassId
            if let hubCityCode = isCityDict["cityCode"] as? String{
                pricingModel.pricinghubCode = hubCityCode
            }
        }
        self.initialize()
        self.fetchFareCalendar()
       // readJson()
    }
    
    /*
     isCityDict ["latitude": 12.97, "isActive": Y, "tcilMstCountryStateMapping": {
         isActive = Y;
         stateName = Karnataka;
         tcilMstCountryContinentMapping =     {
             countryCode = IN;
             countryContinentId =         {
                 continentId = 1;
                 continentName = Asia;
             };
             countryName = India;
             isActive = Y;
         };
         tcilMstCountryStateMappingPK =     {
             countryCode = IN;
             stateCode = "KA ";
         };
     }, "cityName": Bangalore, "longitude": 77.59456299999999, "hubPosition": 3, "ltCityCode": BLR, "cityCode": BLR]
     */
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.doneButton.layer.borderColor = UIColor.clear.cgColor
        self.doneButton.layer.cornerRadius = 3
        self.doneButton.layoutIfNeeded()
    }
    // MARK: - Button Action -
    
    @IBAction func closeButtonClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func doneButtonClicked(_ sender: Any) {
        printLog("Day : 0" + "\(String(describing: calendarModel?.selectedDate?.date))")
        printLog("Rs : 0" + "\(String(describing: calendarModel?.selectedDate?.packageprice))")
        if (isFromCompare){
            if calendarModel != nil && calendarModel?.selectedDate != nil{
                let dateModels : DayModel  = calendarModel?.selectedDate ?? DayModel.init(Date: nil, isbookable: false, onrequest: false, price: 0, prodCode: "", ltItineraryCode: "")
                let dateStr = AppUtility.convertDateToString(date: dateModels.date ?? Date.init(), dateFormat: "dd-MM-yyyy")
                delegateCompare?.selectedDateInCompare(dateModel: dateStr)
            }else{
                let dateModels : DayModel  = DayModel.init(Date: nil, isbookable: false, onrequest: false, price: 0, prodCode: "", ltItineraryCode: "")
                let dateStr = AppUtility.convertDateToString(date: dateModels.date ?? Date.init(), dateFormat: "dd-MM-yyyy")
                delegateCompare?.selectedDateInCompare(dateModel: dateStr)
            }
        }else{
            if calendarModel != nil && calendarModel?.selectedDate != nil{
                delegate?.selectedDate(dateModel:(calendarModel?.selectedDate)! )
            }else{
                delegate?.selectedDate(dateModel: DayModel.init(Date: nil, isbookable: false, onrequest: false, price: 0, prodCode: "", ltItineraryCode: "")  )
            }
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - testing purpose -
    
    private func readJson() {
        do {
            if let file = Bundle.main.url(forResource: "FareCalendar", withExtension: "json") {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                if let object = json as? Dictionary<String,Any> {
                    var startDate : Date? = nil
                    if let startDateStr : String = object[PricingConstants.CalendarStartDate] as? String
                    {
                        startDate = AppUtility.convertStringToDate(string: startDateStr, format: "dd-MM-yyyy")
                        var startMonth : Int = 0
                        if startDate != nil
                        {
                            startMonth = startDate!.getMonth()
                        }
                        if let endDateStr : String = object[PricingConstants.CalendarEndDate] as? String
                        {
                            let endDate : Date = AppUtility.convertStringToDate(string:endDateStr, format: "dd-MM-yyyy")
                            calendarModel = CalendarModel.init()
                            var onRequestArray : Array<Dictionary<String,Any>> = Array.init()
                            var bookableArray : Array<Dictionary<String,Any>> =  Array.init()
                            var dbResponseBean : Dictionary<String,Any> = Dictionary.init()
                            dbResponseBean = object[PricingConstants.DbResponseBean] as? Dictionary<String, Any> ?? Dictionary.init()
                            
                            if dbResponseBean.count == 0
                            {
                                dbResponseBean = object[PricingConstants.ltResponseBean] as? Dictionary<String, Any> ?? Dictionary.init()
                            }
                            
                            bookableArray = dbResponseBean[PricingConstants.Bookable] as? Array<Dictionary<String, Any>> ?? Array.init()
                            onRequestArray = dbResponseBean[PricingConstants.OnRequest] as? Array<Dictionary<String, Any>> ?? Array.init()
                            
                            
                            
                            calendarModel?.initWith(startDate: startDate!, endDate: endDate ,bookableArray:bookableArray,onRequestArray:onRequestArray)
                            //datesArray = AppUtility.getDates(fromDate: self.startDate!, toDate: endDate)
                            self.calendarCollectionView.reloadData()
                        }
                        
                    }
                    
                    printLog(object)
                }
                else if json is [Any]
                {
                    
                } else {
                    printLog("JSON is invalid")
                }
            } else {
                printLog("no file")
            }
        } catch {
            printLog(error.localizedDescription)
        }
    }
    
    // MARK: - Initialize -
    
    func initialize()
    {
        self.calendarCollectionView.register(UINib.init(nibName: "CalendarCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CalendarCollectionViewCell")
        self.calendarCollectionView.register(UINib.init(nibName: "WeekDayCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "WeekDayCollectionViewCell")
        
        let nib = UINib(nibName: "CalendarCollectionReusableView", bundle: nil)
        self.calendarCollectionView.register(nib, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "CalendarCollectionReusableView")
//        let layout = self.calendarCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
//        layout.headerReferenceSize = CGSize(width: self.calendarCollectionView.frame.size.width, height: 40)
        self.topNavigationView.fadedShadow()
    }
    
    func fetchFareCalendar()  {
        LoadingIndicatorView.show("Loading")
        //  {"ltItineraryCode":"2018FAR904S1","market":"EFIT","hubCode":"BLR","pkgSubTypeId":6,"pkgClassId":"0","pkgId":"PKG002815","mode":"TCIL"}
        var dataDict : Dictionary<String,Any> = Dictionary.init()//["ltItineraryCode":"2018FAR904S1","market":"EFIT","hubCode":"BLR","pkgSubTypeId":6,"pkgClassId":"0","pkgId":"PKG002815","mode":"TCIL"]
        dataDict[PricingConstants.HubCode] = pricingModel.pricinghubCode
        dataDict[PricingConstants.Market] = packageDetailModel.ltMarket
        dataDict[PricingConstants.Mode] = "TCIL" // HARDCODED ***
        dataDict[PricingConstants.pkgId] = packageDetailModel.packageId
        // Added after checking log of exist TC
         dataDict[PricingConstants.Market] = "-1"
        dataDict["isHsa"] = "N"
        
        var prodITINCodeArray:Array<String> = []
        
        if(packageDetailModel.ltItineraryCode == "-1" || packageDetailModel.ltItineraryCode == "")
        {
            var filteredArray : Array<Dictionary<String,Any>> = Array()
  
            if packageDetailModel.packageSubTypeName == "fit"
            {
                //arrayDepartureCity = (packageDetailModel?.tcilHolidayPriceCollection)!
                filteredArray  = AppUtility.filterArrayNestedDirectory(ArrayToBeFilter: packageDetailModel.tcilHolidayPriceCollection, withOuterKey: "hubCityCode", withInnerKey: PricingConstants.CityCode, AndStringValue: pricingModel.pricinghubCode)
            }
            else
            {
                 filteredArray  = AppUtility.filterArrayNestedDirectory(ArrayToBeFilter: packageDetailModel.ltPricingCollection, withOuterKey: "hubCode", withInnerKey: PricingConstants.CityCode, AndStringValue: pricingModel.pricinghubCode)
                //arrayDepartureCity = (packageDetailModel?.ltPricingCollection)!
            }
           
            let filteredArrayWithClassCode : Array<Dictionary<String,Any>> = AppUtility.filterArray(ArrayToBeFilter: filteredArray, withKey: PackageDetailConstants.PackageClassId, AndStringValue: "\(pricingModel.packageClassId)")

            
            if filteredArrayWithClassCode.count > 0 //old logic
            {
                //if let dict : Dictionary<String,Any> = filteredArray[0]
                for dict in filteredArrayWithClassCode {
                    if let itineraryCode : String = dict[PricingConstants.LtItineraryCode] as? String
                    {
                        prodITINCodeArray.append(itineraryCode)
                    }
                }
            }
        }
        else
        {
            prodITINCodeArray.append(packageDetailModel.ltItineraryCode)
        }
        
//        if(prodITINCodeArray.count != 0)
//        {
//             dataDict[PricingConstants.LtItineraryCode] = prodITINCodeArray
//
//        }
//        else
//        {
            dataDict[PricingConstants.LtItineraryCode] = prodITINCodeArray //for testing
     //   }
        
        //dataDict[PricingConstants.LtItineraryCode] = packageDetailModel.ltItineraryCode // new logic
        
        dataDict[PricingConstants.pkgClassId] = "\(pricingModel.packageClassId)"
        
        if let pkgSubtypeId : Int = packageDetailModel.pkgSubtypeId[PricingConstants.pkgSubtypeId] as? Int
        {
            dataDict["pkgSubTypeId"] = pkgSubtypeId
        }
        
        printLog("Fare calendar request : ", dataDict.jsonRepresentation())
        //"holidayRS/pdp.compare/fareCalender"
        PreBookingCommunicationManager.getPrebookingData(requestType:"POST", queryParam: nil, pathParam:nil, jsonParam: dataDict, headers: nil, requestURL: kAstraUrlFareCalender, returnInCaseOffailure: false)
        {
            (status,response) in
            
            printLog("status is \(status)")
            
            printLog("response is \(String(describing: response))")
            if let object = response as? Dictionary<String,Any> {
//                var startDate : Date?
                if object.count > 0
                {
                    if let startDateStr : String = object[PricingConstants.CalendarStartDate] as? String
                    {
//                        startDate = AppUtility.convertStringToDate(string: startDateStr, format: "dd-MM-yyyy")
                        if let endDateStr : String = object[PricingConstants.CalendarEndDate] as? String
                        {
                            var startDate : Date? = AppUtility.convertStringToDate(string: startDateStr, format: "dd-MM-yyyy")
                            var endDate : Date? = AppUtility.convertStringToDate(string:endDateStr, format: "dd-MM-yyyy")
                            printLog(endDate?.getWeekday() ?? 100)
                            self.calendarModel = CalendarModel.init()
                            var onRequestArray : Array<Dictionary<String,Any>> = Array.init()
                            var bookableArray : Array<Dictionary<String,Any>> =  Array.init()
                            var dbResponseBean : Dictionary<String,Any> = Dictionary.init()
                            dbResponseBean = object[PricingConstants.DbResponseBean] as? Dictionary<String, Any> ?? Dictionary.init()
                            
                            if dbResponseBean.count == 0
                            {
                                dbResponseBean = object[PricingConstants.ltResponseBean] as? Dictionary<String, Any> ?? Dictionary.init()
                            }
                            
                            
                            bookableArray = dbResponseBean[PricingConstants.Bookable] as? Array<Dictionary<String, Any>> ?? Array.init()
                            let dateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "dd-MM-yyyy"
                            
                            //bookableArray = bookableArray.sorted {($0["date"] as! String) > ($1["date"] as! String)}
                            
                           //  bookableArray = bookableArray.sorted {($0(AppUtility.convertStringToDate(string: String["date"] as! String, format: "dd-MM-yyyy"))) > ($1["date"] as! String)}
                            
                            onRequestArray = dbResponseBean[PricingConstants.OnRequest] as? Array<Dictionary<String, Any>> ?? Array.init()
                            
                            if bookableArray.count == 0 && onRequestArray.count == 0
                            {
                                self.failure()
                            }
                            else
                            {//strat and date change with respective available date
                                /*
                                 if let stDate = bookableArray.first?["date"] as? String{
                                    startDate = AppUtility.convertStringToDate(string:bookableArray.first!["date"] as? String ?? "", format: "dd-MM-yyyy")
                                }else if let stDate = bookableArray.first?["DATE"] as? String{
                                    startDate = AppUtility.convertStringToDate(string:bookableArray.first!["DATE"] as? String ?? "", format: "dd-MM-yyyy")
                                }
                                
                                if let stDate = bookableArray.last?["date"] as? String{
                                    endDate = AppUtility.convertStringToDate(string:bookableArray.last!["date"] as? String ?? "", format: "dd-MM-yyyy")
                                    
                                }else if let stDate = bookableArray.last?["DATE"] as? String{
                                    endDate = AppUtility.convertStringToDate(string:bookableArray.last!["DATE"] as? String ?? "", format: "dd-MM-yyyy")
                                    
                                }
//                                self.calendarModel?.initWith(startDate: startDate!, endDate: endDate,bookableArray:bookableArray,onRequestArray:onRequestArray)
 */
                                
                                if let isStart = startDate {
                                    printLog(isStart)
                                }else{
                                    if let stDate = bookableArray.first?["date"] as? String{
                                        startDate = AppUtility.convertStringToDate(string:bookableArray.first!["date"] as? String ?? "", format: "dd-MM-yyyy")
                                    }else if let stDate = bookableArray.first?["DATE"] as? String{
                                        startDate = AppUtility.convertStringToDate(string:bookableArray.first!["DATE"] as? String ?? "", format: "dd-MM-yyyy")
                                    }
                                }
                                if let isEndDate = endDate {
                                    printLog(isEndDate)
                                }else{
                                    if let stDate = bookableArray.last?["date"] as? String{
                                        endDate = AppUtility.convertStringToDate(string:bookableArray.last!["date"] as? String ?? "", format: "dd-MM-yyyy")
                                        
                                    }else if let stDate = bookableArray.last?["DATE"] as? String{
                                        endDate = AppUtility.convertStringToDate(string:bookableArray.last!["DATE"] as? String ?? "", format: "dd-MM-yyyy")
                                        
                                    }
                                }
                                self.calendarModel?.initWith(startDate: startDate ?? Date(), endDate: endDate ?? Date(),bookableArray:bookableArray,onRequestArray:onRequestArray)
                                //datesArray = AppUtility.getDates(fromDate: self.startDate!, toDate: endDate)
                                self.calendarCollectionView.reloadData()
                                
                            }
                           
                        }
                        else
                        {
                            self.failure()
                        }
                        
                    }
                    else
                    {
                        self.failure()
                    }
                    
                }
                else
                {
                  self.failure()
                }
                printLog(object)
            }
        }
    }
    func failure()  {
        let alert = UIAlertController(title: "Alert", message: "No Dates available", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: AlertMessage.TITLE_OK, style:  UIAlertAction.Style.default, handler: { (action: UIAlertAction!) in
            self.navigationController?.popViewController(animated: true)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    // MARK: - collection View Delegate -
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if calendarModel != nil {
            
            return (calendarModel?.monthArray.count)!
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let monthArray : Array<Any> = calendarModel?.monthArray[section]
        {
            return monthArray.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        var showalert:Bool = true
        switch(indexPath.item)
        {
        case 0...6 :
            let cell :  WeekDayCollectionViewCell = calendarCollectionView.dequeueReusableCell(withReuseIdentifier: "WeekDayCollectionViewCell", for: indexPath) as! WeekDayCollectionViewCell
            if calendarModel != nil {
                if let monthArray : Array<Any> = calendarModel?.monthArray[indexPath.section]
                {
                    if monthArray[indexPath.item] != nil
                    {
                        if let weekDay : String = monthArray[indexPath.item] as? String
                        {
                            cell.weekDayLabel.text = weekDay
                        }
                    }
                    
                }
            }
            return cell
            
        default :
            let cell :  CalendarCollectionViewCell = calendarCollectionView.dequeueReusableCell(withReuseIdentifier: "CalendarCollectionViewCell", for: indexPath) as! CalendarCollectionViewCell
            cell.dayLabel.isHidden = true
            cell.underlineView.isHidden = true
            cell.priceLabel.isHidden = true
            cell.containerView.layer.borderWidth = 0
            
            if calendarModel != nil {
                if let monthArray : Array<Any> = calendarModel?.monthArray[indexPath.section]
                {
                    if monthArray[indexPath.item] != nil
                    {
                        if let dateModel : DayModel = monthArray[indexPath.item] as? DayModel
                        {
//                            if showalert && dateModel.isBookable || dateModel.onRequest {
//                                showalert = false
//                            }
                            if calendarModel?.selectedDate != nil
                            {
                                let selectedDay : DayModel = calendarModel!.selectedDate!
                                if selectedDay.compare(day: dateModel)
                                {
                                    cell.dayLabel.isHidden = false
                                    cell.setDataForSelectedCell(dayModel : dateModel)
                                }
                                else
                                {
                                    cell.dayLabel.isHidden = false
                                    cell.setData(dayModel : dateModel)
                                }
                            }
                            else
                            {
                                cell.dayLabel.isHidden = false
                                cell.setData(dayModel : dateModel)
                            }
                            
                        }
                    }
                    
                }
            }
            
            return cell
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if calendarModel != nil {
            if let monthArray : Array<Any> = calendarModel?.monthArray[indexPath.section]
            {
                if monthArray[indexPath.item] != nil
                {
                    if let dateModel : DayModel = monthArray[indexPath.item] as? DayModel
                    {
                        if dateModel.isBookable
                        {
                            printLog("Date is Bookable")
                            calendarModel?.selectedDate = dateModel
                        }
                        else if dateModel.onRequest
                        {
                            if(dateModel.packageprice != 0){
                                printLog("Date is on request")
                                calendarModel?.selectedDate = dateModel
                                
                            }
                           
                        }
                    }
                    self.calendarCollectionView.reloadData()
                }
                
            }
        }
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = self.calendarCollectionView.frame.size.width*0.13
        return CGSize(width: width, height: 40);
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 50)
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let headerView : CalendarCollectionReusableView  = calendarCollectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "CalendarCollectionReusableView", for: indexPath) as! CalendarCollectionReusableView
        
        var month: Int = 0
        var year : String = ""
        if calendarModel != nil {
            if let monthArray : Array<Any> = calendarModel?.monthArray[indexPath.section]
            {
                if monthArray[indexPath.item] != nil
                {
                    if let dateModel : DayModel = monthArray[monthArray.count - 1] as? DayModel
                    {
                        if let date : Date = dateModel.date
                        {
                            month = date.getMonth()
                            year = " \(date.getYear())"
                        }
                    }
                }
                
            }
        }
        switch(month)
        {
        case 1:
            headerView.monthLabel.text = "January" + year
        case 2:
            headerView.monthLabel.text = "February" + year
        case 3:
            headerView.monthLabel.text = "March" + year
        case 4:
            headerView.monthLabel.text = "April" + year
        case 5:
            headerView.monthLabel.text = "May" + year
        case 6:
            headerView.monthLabel.text = "June" + year
        case 7:
            headerView.monthLabel.text = "July" + year
        case 8:
            headerView.monthLabel.text = "August" + year
        case 9:
            headerView.monthLabel.text = "September" + year
        case 10:
            headerView.monthLabel.text = "October" + year
        case 11:
            headerView.monthLabel.text = "November" + year
        default:
            headerView.monthLabel.text = "December" + year
        }
        headerView.backgroundColor = UIColor.blue
        return headerView
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
