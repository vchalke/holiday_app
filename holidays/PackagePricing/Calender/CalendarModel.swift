//
//  CalendarModel.swift
//  sotc-consumer-application
//
//  Created by Mac on 01/08/18.
//  Copyright © 2018 Mobicule Technologies Private Limited. All rights reserved.
//

import Foundation
class CalendarModel {
    var selectedDate : DayModel? = nil
    var monthArray : [[Any]] = []
    let userCalendar = Calendar.current
    let requestedComponent: Set<Calendar.Component> = [.month,.day,.hour,.minute,.second]

    init() {
    }
    func initWith(startDate: Date ,endDate: Date, bookableArray : Array<Dictionary<String,Any>>, onRequestArray : Array<Dictionary<String,Any>>)  {
        // let day : Int = startDate.getDay()
        // let year : Int = startDate.getYear()
      
        
        
          let numberOfMonths = AppUtility.getDifferenceInMonths(fromDate: startDate, toDate: endDate)  //endDate.getMonth() - startDate.getMonth() + 1
        
//        let timeDifference = userCalendar.dateComponents(requestedComponent, from: startDate, to: endDate)
//        let numberOfMonths = Int(timeDifference.month)
        
//        let numberOfMonths : Int = AppUtility.getMonthCountBetween(fromDate: startDate, toDate: endDate) ?? 0
        
        for i in 0..<numberOfMonths
        {
            var localMonthArray : Array<Any> = Array.init()
            //            if i == 0
            //            {
            //              if let date  : Date = startDate.addMonths(months: i)
            //              {
            //                let month : Int = date.getMonth()
            //
            //                let NumberOfDays : Int = AppUtility.numberOfDays(month: month, year: date.getYear())
            //
            //                for _ in 1..<(date.getDay() - 1)
            //                {
            //                    localMonthArray.append("jkgj")
            //                }
            //                let monthEndDate : Date = date.addDays(days: NumberOfDays - date.getDay() + 1)!
            //
            //             //    let day : Int = date.getDay()
            //
            //                let datesArray = AppUtility.getDates(fromDate: date, toDate: monthEndDate)
            //                for date in datesArray
            //                {
            //                    localMonthArray.append(date)
            //                }
            //                printLog(localMonthArray)
            //              }
            //            }
            //            else
            //            {
            if var date  : Date = startDate.addMonths(months: i)
            {
                let dateString : String = "01-" + String(format: "%02d", date.getMonth()) + "-\(date.getYear())"
                date = AppUtility.convertStringToDate(string: dateString, format: "dd-MM-yyyy")
                let weekday : Int = date.getWeekday()
                let month : Int = date.getMonth()
                
                localMonthArray.append("MON")
                localMonthArray.append("TUE")
                localMonthArray.append("WED")
                localMonthArray.append("THU")
                localMonthArray.append("FRI")
                localMonthArray.append("SAT")
                localMonthArray.append("SUN")

                let NumberOfDays : Int = AppUtility.numberOfDays(month: month, year: date.getYear())
                if weekday > 1
                {
                    for _ in 1..<(weekday-1)
                    {
                        localMonthArray.append("Invalid")   // just to fill with empty cells for showing 1st of month on proper weekday
                    }
                }
                else    // if month is starting from sunday
                {
                    for _ in 0..<6
                    {
                        localMonthArray.append("Invalid")
                    }
                }
                let monthEndDate : Date = date.addDays(days: NumberOfDays - 1 )!
                let datesArray = AppUtility.getDates(fromDate: date, toDate: monthEndDate)  // get all the dates of month
                for day in datesArray
                {
                    let dateString : String = String(format: "%02d", day.getDay()) + "-" + String(format: "%02d", day.getMonth()) + "-\(day.getYear())"
                    let bookableTuple : (isTrue : Bool,price: Int, prodCode: String , prodItiCode:String) = self.checkIfBookableOrOnrequest(date: dateString, bookableArray: bookableArray)
                    let onrequstTuple : (isTrue : Bool,price: Int, prodCode: String, prodItiCode:String) = self.checkIfBookableOrOnrequest(date: dateString, bookableArray: onRequestArray)
                    var price : Int = 0
                    var prodCode : String = ""
                    var ltProdCode :String = ""
                    if bookableTuple.isTrue
                    {
                        price = bookableTuple.price
                        prodCode = bookableTuple.prodCode
                        ltProdCode = bookableTuple.prodItiCode
                    }
                    else if onrequstTuple.isTrue
                    {
                        price = onrequstTuple.price
                        prodCode = onrequstTuple.prodCode
                        ltProdCode = onrequstTuple.prodItiCode
                    }
                   
                    let dateModel : DayModel = DayModel.init(Date: day, isbookable: bookableTuple.isTrue, onrequest: onrequstTuple.isTrue, price: price,prodCode: prodCode, ltItineraryCode: ltProdCode)
                    localMonthArray.append(dateModel)
                }
                printLog(localMonthArray)
            }
            //            }
            self.monthArray.append(localMonthArray)
            
        }
        
    }
    
    private func checkIfBookableOrOnrequest(date: String, bookableArray : Array<Dictionary<String,Any>>) -> (isTrue: Bool,price: Int,prodCode: String, prodItiCode:String) {
        if bookableArray.count > 0
        {
            for dict in bookableArray
            {
                if let dateString : String = dict[PricingConstants.DATE] as? String
                {
                    if date.caseInsensitiveCompare(dateString) == ComparisonResult.orderedSame
                    {
                        let price : Int = dict[PricingConstants.Price] as? Int ?? 0
                        let prodCode : String = dict["LT_PROD_CODE"] as? String ?? ""
                        let itiCode : String = dict["PROD_ITIN_CODE"] as? String ?? ""
                        return (true,price,prodCode,itiCode)
                    }
                }
                else if let dateString : String = dict[PricingConstants.date] as? String
                {
                    if date.caseInsensitiveCompare(dateString) == ComparisonResult.orderedSame
                    {
                        let price : Int = dict["price"] as? Int ?? 0
                        let prodCode : String = dict["LT_PROD_CODE"] as? String ?? ""
                        let itiCode : String = dict["PROD_ITIN_CODE"] as? String ?? ""
                        return (true,price,prodCode,itiCode)
                    }
                }
            }
        }
        return (false,0,"","")
    
    }
}

class DayModel
{
    var date : Date? = nil
    var isBookable : Bool = false
    var onRequest : Bool = false
    var packageprice : Int = 0
    var ltProdCode : String = ""
    var ltItineraryCode: String = ""
   
    
    init(Date : Date? , isbookable : Bool , onrequest : Bool ,price : Int,prodCode:String , ltItineraryCode:String)
    {
        self.date = Date
        self.isBookable = isbookable
        self.onRequest = onrequest
        self.packageprice = price
        self.ltProdCode = prodCode
        self.ltItineraryCode = ltItineraryCode

    }
    
    func  compare(day : DayModel) -> Bool {
        if self.date != day.date {
            return false
        }
        if self.isBookable != day.isBookable {
            return false
        }
        if self.onRequest != day.onRequest {
            return false
        }
        if self.packageprice != day.packageprice {
            return false
        }
        return true
    }
}
