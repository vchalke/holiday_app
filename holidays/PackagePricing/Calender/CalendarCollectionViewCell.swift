//
//  CalendarCollectionViewCell.swift
//  sotc-consumer-application
//
//  Created by Mac on 31/07/18.
//  Copyright © 2018 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit

class CalendarCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var underlineView: UIView!
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func setData(dayModel:DayModel)  {
        
        self.dayLabel.text = ""
        self.containerView.layer.borderWidth = 0

        if let date : Date = dayModel.date
        {
            self.dayLabel.text = "\(date.getDay())"
        }
        let formatter = NumberFormatter()
        formatter.numberStyle = NumberFormatter.Style.decimal
        formatter.locale = Locale(identifier: "en_IN")
        if dayModel.isBookable {
            
            self.underlineView.backgroundColor = UIColor.init(red: 0/255, green:176/255, blue: 163/255, alpha: 1) // green
            self.underlineView.isHidden = false
            self.priceLabel.isHidden = false
            if(dayModel.packageprice == 0){
                self.priceLabel.isHidden = true
                self.underlineView.isHidden = true
            }else{
                self.priceLabel.text = "₹ \(formatter.string(from: NSNumber.init(value: dayModel.packageprice) ) ?? "")"
            }
        }
        else if dayModel.onRequest
        {
            self.underlineView.backgroundColor = UIColor.init(red: 253/255, green: 151/255, blue: 0/255, alpha: 1) // yellow
            self.underlineView.isHidden = false
            self.priceLabel.isHidden = false
            if(dayModel.packageprice == 0){
                self.priceLabel.isHidden = true
                self.underlineView.isHidden = true
            }else{
                self.priceLabel.text = "₹ \(formatter.string(from: NSNumber.init(value: dayModel.packageprice) ) ?? "")"
            }
        }
        else
        {
            self.underlineView.isHidden = true
            self.priceLabel.isHidden = true
        }
    }
    
    func setDataForSelectedCell(dayModel:DayModel)  {
        
        self.dayLabel.text = ""
        self.containerView.roundedCorners(radius: 4, borderWidth: 1.5)
        self.containerView.layer.borderColor = UIColor.init(red: 209/255, green: 0/255, blue: 12/255, alpha: 1).cgColor //red color
        
        if let date : Date = dayModel.date
        {
            self.dayLabel.text = "\(date.getDay())"
        }
        let formatter = NumberFormatter()
        formatter.numberStyle = NumberFormatter.Style.decimal
        formatter.locale = Locale(identifier: "en_IN")
        if dayModel.isBookable {
            
            self.underlineView.backgroundColor = UIColor.init(red: 0/255, green:176/255, blue: 163/255, alpha: 1) // green
            self.underlineView.isHidden = false
            self.priceLabel.isHidden = false
            if(dayModel.packageprice == 0){
                self.priceLabel.isHidden = true
                self.underlineView.isHidden = true
            }else{
                self.priceLabel.text = "₹ \(formatter.string(from: NSNumber.init(value: dayModel.packageprice) ) ?? "")"
            }
        }
        else if dayModel.onRequest
        {
            self.underlineView.backgroundColor = UIColor.init(red: 253/255, green: 151/255, blue: 0/255, alpha: 1) // yellow
            self.underlineView.isHidden = false
            self.priceLabel.isHidden = false
            if(dayModel.packageprice == 0){
                self.priceLabel.isHidden = true
                self.underlineView.isHidden = true
            }else{
                self.priceLabel.text = "₹ \(formatter.string(from: NSNumber.init(value: dayModel.packageprice) ) ?? "")"
            }
        }
        else
        {
            self.underlineView.isHidden = true
            self.priceLabel.isHidden = true
        }
    }
}
