//
//  CalendarCollectionReusableView.swift
//  sotc-consumer-application
//
//  Created by Mac on 31/07/18.
//  Copyright © 2018 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit

class CalendarCollectionReusableView: UICollectionReusableView {
    @IBOutlet weak var monthLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
