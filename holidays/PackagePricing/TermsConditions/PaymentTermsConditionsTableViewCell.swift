//
//  PaymentTermsConditionsTableViewCell.swift
//  sotc-consumer-application
//
//  Created by ketan on 02/08/18.
//  Copyright © 2018 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit
protocol paymentTermsProtocol
{
    func navigateToDetailView(Title title: String, description: String);
}

class PaymentTermsConditionsTableViewCell: UITableViewCell
{
    @IBOutlet weak var labelTitle: UILabel!
    var delegate :paymentTermsProtocol?
    var packageDetailModel : PackageDetailsModel =  PackageDetailsModel.init()
    @IBOutlet weak var viewContainer: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
//        self.viewContainer.layer.borderWidth = 1
//        self.viewContainer.layer.borderColor = AppUtility.hexStringToUIColor(hex: "404040").cgColor
//        self.viewContainer.layer.masksToBounds = true
        
        self.viewContainer.layer.addBorder(edge: .top, color: AppUtility.hexStringToUIColor(hex: "404040"), thickness: 0.5)
        
        let tapGetsure :UITapGestureRecognizer = UITapGestureRecognizer(target: self, action:#selector(self.onTapOfCell(_:)))
        self.viewContainer?.isUserInteractionEnabled = true
        self.viewContainer.addGestureRecognizer(tapGetsure)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func onTapOfCell(_ sender: UITapGestureRecognizer)
    {
        let tag : Int = sender.view!.tag
        if tag  == 1
        {
            var htmlStr : String = ""
            let filteredArray : Array<Dictionary<String,Any>> = AppUtility.filterArray(ArrayToBeFilter:  packageDetailModel.tcilHolidayPaymentTermsCollection, withKey: PackageDetailConstants.PackageClassId, AndIntegerValue: 0)
            if filteredArray.count > 0
            {
                for dict in filteredArray
                {
                    
                    if let paymentTerms : String = dict["paymentTerms"] as? String
                    {
                        htmlStr = htmlStr + paymentTerms
                    }
                }
            }
        
            htmlStr.setFontToHtmlString(fontName: "Roboto-Regular", FontSize: "3", fontColor: "#333333")
            delegate?.navigateToDetailView(Title: "Payment Terms", description: htmlStr)
        }
        else if  tag  == 2
        {
            var htmlStr : String = ""
            let filteredArray : Array<Dictionary<String,Any>> = AppUtility.filterArray(ArrayToBeFilter:  packageDetailModel.tcilHolidayPaymentTermsCollection, withKey: PackageDetailConstants.PackageClassId, AndIntegerValue: 0)
            if filteredArray.count > 0
            {
                for dict in filteredArray
                {
                    
                    if let paymentTerms : String = dict["cancellationPolicy"] as? String
                    {
                        htmlStr = htmlStr + paymentTerms
                    }
                }
            }
            
            htmlStr.setFontToHtmlString(fontName: "Roboto-Regular", FontSize: "3", fontColor: "#333333")
            delegate?.navigateToDetailView(Title: "Cancellation Policy", description: htmlStr)
        }
        else if  tag  == 3
        {
            var htmlStr : String = ""
            var notesHeading : String = ""
            var notesDescription: String = ""
            var descriptionstr = ""
            let filteredArray : Array<Dictionary<String,Any>> = AppUtility.filterArray(ArrayToBeFilter:  packageDetailModel.tcilHolidayPaymentTermsCollection, withKey: PackageDetailConstants.PackageClassId, AndIntegerValue: 0)
            if filteredArray.count > 0
            {
                for dict in filteredArray
                {
                    
                    if let paymentTerms : String = dict["notesHeading"] as? String
                    {
                        notesHeading = paymentTerms
                    }
                    if let Description : String = dict["notesDescription"] as? String //description
                    {
                       notesDescription = Description
                    } 
                    if let Descriptionsarray: Array<Dictionary<String,Any>>   = dict["tcilHolidayTermsConditionsCollection"] as? Array<Dictionary<String, Any>>  //description
                    {
                        for discdict in Descriptionsarray
                        {
                            descriptionstr = discdict["description"] as? String ?? ""
                        }
                    }
                }
                htmlStr = notesHeading + notesDescription + descriptionstr
            }
            
            htmlStr.setFontToHtmlString(fontName: "Roboto-Regular", FontSize: "3", fontColor: "#333333")
            delegate?.navigateToDetailView(Title: "Terms & Conditions", description: htmlStr)
        }
    }
    
}
