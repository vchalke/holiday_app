//
//  DepartureCityTableViewCell.swift
//  sotc-consumer-application
//
//  Created by ketan on 28/07/18.
//  Copyright © 2018 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit

class DepartureCityTableViewCell: UITableViewCell,UITextFieldDelegate
{
    @IBOutlet weak var viewTextFieldContainer: UIView!
    var pricingModel:PricingModel = PricingModel()
    var packageDetailModel:PackageDetailsModel?
    var reloadDelegate : ReloadPricing? = nil
    @IBOutlet weak var textFieldDepartureCity: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        
//        self.viewTextFieldContainer.fadedShadow()
//        self.viewTextFieldContainer.roundedCorners()
        textFieldDepartureCity.delegate = self
        // Initialization code
    }
    
 
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func uniq<S : Sequence, T : Hashable>(source: S) -> [T] where S.Iterator.Element == T {
        var buffer = [T]()
        var added = Set<T>()
        for elem in source {
            if !added.contains(elem) {
                buffer.append(elem)
                added.insert(elem)
            }
        }
        return buffer
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        var arrayDepartureCity :Array<Dictionary<String,Any>>
        
        var filteredArray = Array<Dictionary<String,Any>>()
        if packageDetailModel?.packageSubTypeName == "fit"
        {
            arrayDepartureCity = (packageDetailModel?.tcilHolidayPriceCollection)!
            
            filteredArray = AppUtility.filterArray(ArrayToBeFilter: arrayDepartureCity, withKey: "packageClassId", AndStringValue: "\(self.pricingModel.packageClassId)")
            arrayDepartureCity = self.uniqcity(tcilHolidayPriceCollection: arrayDepartureCity)
        }
        else
        {
            arrayDepartureCity = (packageDetailModel?.ltPricingCollection)!
            
            filteredArray = arrayDepartureCity
        }
        
        

        let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: "Please select", message: "Departure City", preferredStyle: .actionSheet)
        arrayDepartureCity = AppUtility.sortHubCity(hubArray: filteredArray)
        
        
        
        let reducedSubtitles = arrayDepartureCity.reduce([[String: Any]]()) { (result, current) in
            
            
            if packageDetailModel?.packageSubTypeName == "fit"
            {
                
                if result.contains(where: {
                    ($0["hubCityCode"] as! Dictionary<String, Any>)["cityName"] as! String == (current["hubCityCode"] as! Dictionary<String, Any>)["cityName"] as! String }) {
                    return result
                }
            }
            else
            {
                
                if result.contains(where: {
                    ($0["hubCode"] as! Dictionary<String, Any>)["cityName"] as! String == (current["hubCode"] as! Dictionary<String, Any>)["cityName"] as! String }) {
                    return result
                }

                //hubCodeDict = packageCityDict["hubCode"] as! Dictionary
                //packageCity = hubCodeDict["cityName"] as! String
            }
            
            return result + [current]
        }


        
        
        
        for packageCityDict in reducedSubtitles
        {
            printLog(packageCityDict)
            var packageCity:String
            var hubCodeDict:Dictionary<String,Any> = Dictionary.init()
            if packageDetailModel?.packageSubTypeName == "fit"
            {
                hubCodeDict = packageCityDict["hubCityCode"] as! Dictionary
                packageCity = hubCodeDict["cityName"] as! String
            }
            else
            {
                hubCodeDict = packageCityDict["hubCode"] as! Dictionary
                packageCity = hubCodeDict["cityName"] as! String    
            }
            
            
            let actionButton  = UIAlertAction(title: packageCity, style: .default) { _ in
                self.textFieldDepartureCity.text = packageCity
                self.pricingModel.pricingDepartureCity = packageCity
                if let hubcode : String = hubCodeDict[PricingConstants.CityCode] as? String
                {
                    self.pricingModel.pricinghubCode = hubcode
                }
                self.pricingModel.pricingTourDate = ""
                self.reloadDelegate?.reloadTable()
            }
            
            actionSheetControllerIOS8.addAction(actionButton)
            
        }
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            printLog("Cancel")
        }
        actionSheetControllerIOS8.addAction(cancelActionButton)
        
        self.window?.rootViewController?.present(actionSheetControllerIOS8, animated: true, completion: nil)
        
        
        return false
    }
    func uniqcity(tcilHolidayPriceCollection:Array<Dictionary<String,Any>>) -> Array<Dictionary<String,Any>> {
        var tempSet  = Set<String>()
        for dict in tcilHolidayPriceCollection
        {
            let hubCodeDict : Dictionary<String,Any> = dict["hubCityCode"] as? Dictionary<String,Any> ?? Dictionary.init()
            let packageCity : String = hubCodeDict["cityName"] as? String ?? ""
            tempSet.insert(packageCity)
        }
        var tempArray : Array<Dictionary<String, Any>> = Array .init()
        for hubcity in Array(tempSet)
        {
            let filteredArray : Array<Dictionary<String,Any>> = tcilHolidayPriceCollection.filter({ (dict : Dictionary<String,Any>) -> Bool in
                let hubCodeDict : Dictionary<String,Any> = dict["hubCityCode"] as? Dictionary<String,Any> ?? Dictionary.init()
                let packageCity : String = hubCodeDict["cityName"] as? String ?? ""
                if packageCity == hubcity
                {
                    return true
                }
                return false
            })
            
            if filteredArray.count > 0
            {
                tempArray.append(filteredArray.first!)
            }
        }
        //
        
        return tempArray
    }
    
}
