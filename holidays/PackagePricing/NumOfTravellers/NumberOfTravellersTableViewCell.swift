//
//  NumberOfTravellersTableViewCell.swift
//  sotc-consumer-application
//
//  Created by ketan on 28/07/18.
//  Copyright © 2018 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit
protocol NumberOfTravellersDelegate
{
    func addAddDeleteRoom(_ sender:UIButton)
    func addAddDeleteMember(_ sender:UIButton)
    func dropDownClicked(_ sender:UIButton)
    func deleteMemberClicked(_ sender:UIButton)
}
class NumberOfTravellersTableViewCell: UITableViewCell
{

    var packageDetailModel:PackageDetailsModel?
    @IBOutlet weak var infantCount: UILabel!
    @IBOutlet weak var childWithBedCount: UILabel!
    @IBOutlet weak var childCount: UILabel!
    @IBOutlet weak var adultCount: UILabel!
    var delegate : NumberOfTravellersDelegate?
    @IBOutlet weak var buttonAddRoom: UIButton!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var buttonDropDown: UIButton!
    @IBOutlet weak var buttonDeleteRoom: UIButton!
    @IBOutlet weak var constraintForClosedCell: NSLayoutConstraint!
    @IBOutlet weak var labelFourPeaple: UILabel!
    @IBOutlet weak var labelRoomNumber: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.containerView.fadedShadow()
        self.containerView.roundedCorners()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setDataForRooming(_ roomModel :RoomModel)
    {
        self.adultCount.text = "\(roomModel.adultCount)"
        self.childCount.text = "\(roomModel.childWithoutBedCount)"
        self.childWithBedCount.text = "\(roomModel.childWithBedCount)"
        self.infantCount.text = "\(roomModel.infantCount)"
    }
    
    @IBAction func onAddDeleteMemberButtonClicked(_ sender: Any)
    {
        self.delegate?.addAddDeleteMember(sender as! UIButton)
    }
    
    @IBAction func onAddDeleteRoomButtonClicked(_ sender: Any)
    {
        self.delegate?.addAddDeleteRoom(sender as! UIButton)
    }
    @IBAction func onDropDownButtonClicked(_ sender: Any)
    {
        self.delegate?.dropDownClicked(sender as! UIButton)
    }
    @IBAction func onDeleteRoomButtonClicked(_ sender: Any)
    {
        self.delegate?.deleteMemberClicked(sender as! UIButton)
    }
    
    
}
