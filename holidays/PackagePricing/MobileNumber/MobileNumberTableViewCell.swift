//
//  MobileNumberTableViewCell.swift
//  sotc-consumer-application
//
//  Created by ketan on 28/07/18.
//  Copyright © 2018 Mobicule Technologies Private Limited. All rights reserved.
//
//checkbox_empty ImagName
import UIKit

class MobileNumberTableViewCell: UITableViewCell,UITextFieldDelegate {
    
    @IBOutlet weak var labelTermsAndCond: UILabel!
    var pricingModel:PricingModel = PricingModel()
    @IBOutlet weak var textFieldMobileNumber: UITextField!
    @IBOutlet weak var viewTextFieldContainer: UIView!
    @IBOutlet weak var buttonCheckMark: UIButton!
    var delegate :paymentTermsProtocol?
    var packageDetailModel : PackageDetailsModel =  PackageDetailsModel.init()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        self.viewTextFieldContainer.fadedShadow()
//        self.viewTextFieldContainer.roundedCorners()
        self.buttonCheckMark.treatAsCheckBox()
        self.textFieldMobileNumber.delegate = self
        self.textFieldMobileNumber.addDoneButtonOnKeyboard()
        buttonCheckMark.treatAsCheckBox()
        
        
        let text = "I accept the Privacy Policy & Terms and Conditions"
        //        let underlineAttriString = NSMutableAttributedString(string: text)
        let underlineAttriString = NSMutableAttributedString(attributedString: labelTermsAndCond.attributedText!)
        let colour = UIColor.init(red: 0/255, green: 78/255, blue: 155/255, alpha: 1)
        let range1 = (text as NSString).range(of: "Terms and Conditions")
        underlineAttriString.setAttributes([NSAttributedString.Key.font : UIFont(name: "Roboto-Regular", size: CGFloat(12.0))!
            , NSAttributedString.Key.foregroundColor : colour], range: range1)
        underlineAttriString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: range1)
        
        let range2 = (text as NSString).range(of: "Privacy Policy")
        underlineAttriString.setAttributes([NSAttributedString.Key.font : UIFont(name: "Roboto-Regular", size: CGFloat(12.0))!
            , NSAttributedString.Key.foregroundColor : colour], range: range2)
        underlineAttriString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: range2
        )
        labelTermsAndCond.attributedText = underlineAttriString
        
        let tapAddExpe : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.tapLabel))
        tapAddExpe.delegate = self as? UIGestureRecognizerDelegate
        tapAddExpe.numberOfTapsRequired = 1
        tapAddExpe.numberOfTouchesRequired = 1
        labelTermsAndCond.addGestureRecognizer(tapAddExpe)
    }
   
    //MARK: on tap
    func tapLabel (sender: UITapGestureRecognizer)
    {
        
        let text = (labelTermsAndCond.text)!
        let termsRange = (text as NSString).range(of: "Terms and Conditions")
        let policyRange = (text as NSString).range(of: "Privacy Policy")
        
        /*
        if sender.didTapAttributedTextInLabel(label: labelTermsAndCond, inRange: policyRange)
        {
            var privacypolicy : String =
            """
<!DOCTYPE html>
<html>
<head>
<title>Codebeautify.org Text to HTML Converter</title>
</head>
<body>
<p><b>SOTC Travel Limited (Formerly known as SOTC Travel Private Ltd).</b> believes that customer who books or looks for any services at www.sotc.in has the right to know about the privacy policy that is followed by us. For the trust the client has on SOTC, we value the right to your privacy.

The idea of this policy is to educate the user as to what personal information is being captured by us and where the same is being used. It describes the principles and practices that apply to Personal Information (defined below) collected from users of our services (you) on our Site, in telephone or e-mail communications, or in interviews, surveys, sweepstakes, contests, or raffles.
In short
<br>We will not collect Personal Information without your knowledge and permission
<br>We will not knowingly disclose your Personal Information to third parties, except as provided in this Privacy Policy
<br>We will take reasonable steps to protect the security of the Personal Information we collect from you. For unregistered users

<br><h3>For unregistered users</h3>

We encourage you to register with us in order to book services on our website and to take advantage of our customization features. However, you may choose not to register and take advantage of any feature of our site where registration is not required.

If you do not register, then the information we collect from you is limited.

We log your IP address in order to help diagnose problems with our server, administer our Web site and track usage statistics.

Your IP address may vary each time you visit, or it may be the same, depending on whether you access our site through an always-on type of Internet connection (e.g., cable modem or DSL), or through a dial-up connection (e.g., VSNL, MTNL etc). Either way, it would be extremely difficult for us to identify you through your IP address, and we make no attempt to do so.

If you reached our site by clicking on a link or advertisement on another site, then we also log that information. This helps us to maximize our Internet exposure, and to understand the interests of our users. All of this information is collected and used only in the aggregate; that is, it is entered into our database, where we can use it to generate overall reports on our visitors, but not reports about individual visitors.

We also place a small file known as a cookie on your computers hard drive. A cookie may contain information that allows us to track your path through our Web site and to determine whether you have visited us before. However, unless you register with us, it contains no personally identifiable information that would allow us to identify you. Cookies cannot be used to read data off of your hard drive, and cannot retrieve information from any other cookies created by other Web sites. We use cookies in this manner to help us understand how visitors use our site, and to help us to improve our site. You may refuse to accept a cookie from us by following the procedures specific to your Web browser. Although you may do so, you may find that your browser reacts strangely when visiting not only our Web site, but other Web sites as well. Since cookies dont provide us with any information from which we can identify you, we suggest you allow us to place one on your computer.

If you visit our site by clicking-through from a site operated by one of our partners, and you have registered with that partner, then certain information about you that you have provided to that partner may be transmitted to us. You should review the privacy policy of the Web site from which you reached our site in order to determine what information was collected and how you agreed that our partner could use that information. Regardless of what information was transmitted to us, however, we dont keep it unless you register with us.
<br><h3>Registered Users</h3>

All users who choose to register themselves on our site www.sotc.in will be able to do online transactions with us. You will be able to purchase / book our services offered herein.

If you register with us, we will collect personal information from you in addition to the non-personal information described above. That personal information may include your name, email address, mailing address, telephone number, travel preferences, passport number, user name and password. The information we collect may vary, but we only collect the information that you manually enter into our forms. We may store all or some of that information in a cookie file on your hard drive, so that our system will recognize you each time you visit our site. In that way, we can save your preferences from visit to visit and present you with a customized Web site, without requiring you to log into our site every time you visit. To improve services and enhance personalization, we may periodically obtain information about you from other independent third-party sources and add it to your registration information. Additionally, authorized personnel may update your registration information to accurately reflect any new information included in communications received from you.

If you visit our site by clicking-through from a site operated by one of our partners, and you have registered with that partner, then certain information about you that you have provided to that partner may be transmitted to us. You should review the privacy policy of the Web site from which you reached our site in order to determine what information was collected and how you agreed that our partner could use that information. We may or may not retain that information; if we do, then we will only use it in accordance with our privacy policy, regardless of the policy of the partner site from which you came to us.

If you reach our site through one of our partners (whether or not you have registered with our partner), and you choose to register with us, we may be required to give our partner some or all of your registration information. We will only do so in accordance with this policy, but we cannot control how our partner uses the information. If you have questions about our partners privacy policy, you should review their policy before providing information to us. Of course, you can ensure that the personal information you provide to us is not shared with our partners (except in accordance with this policy), by visiting us directly instead of clicking-through from one of our partners.
<br><h3>When you Purchase Products or Services for a third party</h3>

When you Purchase Products or Services for a third party using your Member ID and password, we will collect that third party's name and contact information, and other information as required by the travel service provider(s), so that we can complete the booking.
<br><h3>How we use the personal information we collect</h3>

We use the personal information we collect to help both of us! As we mentioned above, registering with us allows you to personalize our Web site so that it is most useful to you. It also allows you to log into our site automatically each time you visit, rather than manually typing your user name and password every time. By registering you can also track your history of transactions that you have done on our site which is available in the form of a Dossier. We may also use this information to periodically contact you with news or important information or to request your feedback on our site. In addition to these periodic updates, we may email you additional materials, but only if you specifically request them. These might include opt-in newsletters and other materials that you proactively request.

In addition to these communications, if you have provided an email address, we may use your personal information to send you notifications about special offers or to tell you about opportunities available from our partners. You may opt-out of any or all marketing communications in any commercial e-mail we send or at any time, as well as access and update your personal information, by visiting <a href="https://www.sotc.in">www.sotc.in</a>.
<br><h3>Disclosure of your personal information</h3>

SOTC does not sell or rent any personal information to any third party, other than our partners, as discussed in this policy. We may aggregate personal information from all of our users and provide that information in the aggregate to other parties, including advertisers, for marketing and promotional purposes. However, if we do so, that information will not be in a form that will allow any third party to identify you personally.

We may share some of your personally identifiable information with our subsidiaries and sister companies around the world that are committed to providing you with a comprehensive array of travel packages and services to meet your travel needs. Sharing this information allows us to better understand the ways in which our various product and service offerings can assist travelers. It also enables us to provide you with information about travel opportunities in which you might be interested. We do not place restrictions on the use of personal information by our subsidiaries, or affiliates but they will treat it at least as protectively as they treat information they obtain from their other users. They also will comply with applicable laws governing the transmission of promotional communications, and give you an opportunity in any such email that they send to choose not to receive such promotional e-mail messages in the future.

We also may share your personal information with third-parties specifically engaged by SOTC to provide services such as Airlines, Hotels, Ground Handlers, Insurance Companies, Product Companies and advertising agencies, in which case we will require those parties to agree to use any such personally-identifiable data solely for the purpose of providing the specified services to you. We may also use the information to provide to government agencies like RBI, Banks etc as per the laws of the country. As you might expect, SOTC must cooperate with legal authorities, and may in some circumstances be required to disclose personally identifiable information in response to requests from law enforcement authorities, or in response to a subpoena or other legal process. We dont expect this to happen, but if it does, we will provide only the information required. In other words, we will not simply turn over our database in response to a specific legal requirement. We also may share your information in connection with a corporate transaction, such as a divestiture, merger, consolidation, or asset sale, and in the unlikely event of bankruptcy.
<br><h3>How you can review and update your personal information</h3>

You can review and update the personal information you have provided through the registration process by visiting www.sotc.in.
<br><h3>Steps we take to protect your personal information</h3>

SOTC has implemented security procedures to help protect the personal information stored in our systems. For example, we limit access to personal information about you to employees who we believe reasonably need to come into contact with that information. We also employ processes (such as password hashing, login auditing, and idle session termination) to protect against unauthorized access to your personal information.
<br><h3>Minors</h3>

Minors (as defined under the laws of their jurisdiction or residence) are not eligible to register for, use, or Purchase the Products or Services available on our Site. We do not knowingly collect Personal Information from any Minor, and will not use this information if we discover that it has been provided by a Minor.
<br><h3>Retention and storage</h3>

We will retain your Personal Information in our databases in accordance with our document management, retention and destruction policy and applicable laws. This period may extend beyond the end of your relationship with us, but it will be only as long as it is necessary for us to have sufficient information to respond to any issues that may arise later. For example, we may need or be required to retain information to allow you to obtain credit for trip your Purchased but had to cancel. We may also need the retain certain information to prevent fraudulent activity; to protect ourselves against liability, permit us to pursue available remedies or limit any damages that we may sustain; or if we believe in good faith that a law, regulation, rule or guideline requires it.

Your Personal Information will be stored in secured locations, and on servers controlled by SOTC, located either at our offices, or at the offices of our service providers
<br><h3>Third Party Advertisers</h3>

Third parties advertise on our Site. We do not share any Personal Information about you with these advertisers unless you give us permission to do so, separate from any permission you provide during the Member registration process. These advertisers may seek to use cookies and pixel tags to track Session Data about the ads you have seen and types of things in which you appear interested. These advertisers may also use combined information about your visits to our Site and other sites in order to provide advertisements about related goods and services that may be of interest to you.

When you click on one of these advertisers links, you are leaving our Site and entering another site. We are not responsible for such third partys sites. You should carefully review the privacy statements of any other site you visit, because those privacy statements will apply to your visit to that site, and may be very different from our policy.
<br><h3>Opting-Out</h3>

As part of the registration process, we give you the ability to receive via e-mail or direct messaging information about our Products and Services, updates to our Site, customized advertisements and promotions that are targeted to your specific interest, such as flight specials, promotions, contests, sweepstakes and other travel opportunities available on our Site and/or sponsored by our travel service providers and advertisers. We send this information directly ourselves, or via third party service providers.

If you do not opt-out from receiving these communications about our Site, we will send them to you.
<br><h3>Links</h3>

For your convenience, our Site provides links to other sites. When you click on one of these links, you are leaving our Site and entering another site. We are not responsible for such third party sites. You should carefully review the privacy statements of any other sites you visit, because those privacy statements will apply to your visit to such other sites.
<br><h3>Changes to this Privacy Policy</h3>

We reserve the right to change this policy should we deem it advisable to do so. If we make material changes that will affect personal information we have already collected from you, we will make reasonable efforts to notify you of the changes and to give you the opportunity to amend or cancel your registrations.</p>
</body>
</html>
"""
            privacypolicy.setFontToHtmlString(fontName: "Roboto-Regular", FontSize: "3", fontColor: "#333333")
            delegate?.navigateToDetailView(Title: "Privacy Policy", description: privacypolicy)
        }
        else if sender.didTapAttributedTextInLabel(label: labelTermsAndCond, inRange: termsRange)
        {
            var htmlStr : String = ""
//            let filteredArray : Array<Dictionary<String,Any>> = AppUtility.filterArray(ArrayToBeFilter:  packageDetailModel.tcilHolidayPaymentTermsCollection, withKey: PackageDetailConstants.PackageClassId, AndIntegerValue: 0)
//            if filteredArray.count > 0
//            {
//                for dict in filteredArray
//                {
//
//                    if let paymentTerms : String = dict["termsConditions"] as? String
//                    {
//                        htmlStr = htmlStr + paymentTerms
//                    }
//                }
//            }
            htmlStr = """
            <!DOCTYPE html PUBLIC >
            <html xmlns=>
            <head>
            <title>Codebeautify.org Text to HTML Converter</title>
            </head>
            <body>
            <p>
            <b>USER'S RESPONSIBILITY OF COGNIZANCE OF THIS AGREEMENT</b>
            <br/>
            This website is published and maintained by SOTC Travel Limited (Formerly known as SOTC Travel Private Ltd), a company incorporated and existing in accordance with the laws of the Republic of India. This page sets forth the terms and conditions under which SOTC provides the information on this Website, as well as the terms and conditions governing your use of this site. The Users availing services from SOTC shall be a natural or legal person and deemed to have read, understood and expressly accepted the terms and conditions of the agreement, which alone shall govern the desired transaction or provision of such service by SOTC for all purposes, and shall be binding on the user. All rights and liabilities of the User with respect to any services to be provided by SOTC shall be restricted to the scope of this agreement. If you do not accept these terms and conditions, do not continue to use or access this site, or order any searches.
            <br/><br/>
            In addition to this agreement, there are certain terms of service (TOS) specific to the services rendered/products provided by SOTC like the air tickets, MICE, bus, rail, holiday packages, etc. Such TOS will be provided / updated by SOTC which shall be deemed to be a part of this Agreement and in the event of conflict between such TOS and this agreement, the terms of this agreement shall prevail. The user shall be required to read and accept the relevant TOS for the service/ product availed by the user.
            <br/><br/>
            Additionally, the Service Provider itself may provide terms and guidelines that govern particular features, offers or the operating rules and policies applicable to each Service (for example , flights, hotel reservation, Packages , etc). The User shall be responsible for ensuring compliance with the terms and guidelines or operating rules and policies of the services provided with whom User elects to deal, including terms and conditions set forth in a Service Providers’ fare rules, contract of carriage or other rules.
            <br/><br/>
            SOTC’s Services are offered to the User Conditional on acceptance without modification of all the terms, conditions and notices contained in the agreement and the TOS, as may be applicable from time to time. For the removal of doubts, it is clarified that availing of the services by the User constitutes an acknowledgement and acceptance by the User of the Agreement and the TOS. If the User does not agree with any part of such terms, conditions and notices, the User must not avail SOTC’s Services.
            <br/><br/>
            In the event that any of the terms, conditions and notices contained herein conflict with the Additional Terms and guidelines contained within any other SOTC Document, then these terms shall be applicable.
            <br/><br/>
            <b>SITE AND ITS CONTENTS</b>
            <br/>
            This Site is only for your personal use. You shall not distribute, exchange, modify, sell or transmit anything you copy from this Site, including but not limited to any text, images, audio and video, for any business, commercial or public purpose.
            <br/><br/>
            As long as you comply with the terms of these Terms and Conditions of Use, SOTC grants you a non–exclusive, non–transferable, limited right to enter, view and use this Site. You agree not to interrupt or attempt to interrupt the operation of this Site in any way.
            <br/><br/>
            You may only use this site to make legitimate reservations or purchases and shall not use this site for any other purposes, including without limitation, to make any speculative, false or fraudulent reservation or any reservation in anticipation of demand. You shall not make reservation in the name of fictitious person(s). You agree that you will not use any device, software or routine that would interfere or be reasonably expected to interfere with the normal working of this site. You agree that you shall not take any action that imposes a burden or load on our infrastructure that SOTC deems in its sole discretion to be unreasonable or disproportionate to the benefits SOTC obtains from your use of the site. You are prohibited from posting or transmitting any unlawful, threatening, libelous, defamatory, obscene, indecent, inflammatory, pornographic or profane material or any material that could constitute or encourage acts or omissions that would amount to a criminal offense or attempt to commit a criminal offense, or give rise to civil liability, or otherwise violate any law. In addition, you are prohibited from posting or transmitting any information which (a) infringes the rights of others or violates their privacy or publicity rights, (b) is protected by patent, copyright, trademark or other proprietary right, unless with the express written permission of the owner of such right, (c) contains a virus, bug, worm or other harmful item or program that may damage, detrimentally interfere with, surreptitiously intercept and/or expropriate any system, data or important information, (d) create any liability for us or cause us to lose (fully or partially) the services of our ISPs or other suppliers, or (e) is used to unlawfully collude against another person in restraint of trade or competition. You shall be solely liable for any damages resulting from any infringement of copyright, trademark, or other proprietary right, or any other harm resulting from your use of this site.
            <br/><br/>
            <b>COPYRIGHT POLICY</b>
            <br/>
            SOTC holds the copyright in respect of this site, and the content provided in this site, including the text, graphics, button icons, audio and video clips, digital downloads, data compilations and software, may not be copied, reproduced, republished, uploaded, posted, transmitted or distributed without the written permission of SOTC, and/or its third party providers and distributors, except that you may download, display and print the materials presented on this site for your personal, non-commercial use only. You may not use any "robot," "spider" or other automatic device, or a program, algorithm or methodology having similar processes or functionality, or any manual process, to monitor or copy any of the Web pages, data or content found on this site, in any case without the prior written permission of SOTC. You agree that you will not transmit or otherwise transfer any Web pages, data or content found on this site to any other computer, server, Web site, or other medium for mass distribution or for use in any commercial enterprise. Unauthorized use of this site and/or the materials contained on this site may violate applicable copyright, trademark or other intellectual property laws or other laws. You must retain all copyright and trademark notices, including any other proprietary notices, contained in the materials, and you must not alter, obscure or obliterate any of such notices. The use of such materials on any other Web site or in any environment of networked computers is prohibited.<br/><br/>
            
            <b>OWNERSHIP</b>
            <br/>
            All materials on this Site, including but not limited to audio, images, software, text, icons and such like (the “Content”), are protected by copyright under international conventions and copyright laws. You cannot use the Content, except as specified herein. You agree to follow all instructions on this Site limiting the way you may use the Content.
            <br/><br/>
            There are a number of proprietary logos, service marks and trademarks found on this Site whether owned/used by SOTC or otherwise. By displaying them on this Site, SOTC is not granting you any license to utilize those proprietary logos, service marks, or trademarks. Any unauthorized use of the Content may violate copyright laws, trademark laws, the laws of privacy and publicity, and civil and criminal statutes.<br/><br/>
            
            You may download such copy/copies of the Content to be used only by you, to help you make the purchase decision on products and services offered by SOTC. If you download any Content from this Site, you shall not remove any copyright or trademark notices or other notices that go with it.
            <br/><br/>
            <b>SOTC RIGHTS</b>
            <br/>
            If you send any communications or materials to the Site by electronic mail or otherwise, including any comments, data, questions, suggestions or the like, all such communications are, and will be treated by SOTC, as non–confidential.
            <br/><br/>
            <b>USER AGREEMENT</b>
            <br/>
            You hereby give up any and all claim that any use of such material violates any of your rights including moral rights, privacy rights, proprietary or other property rights, publicity rights, rights to credit for material or ideas, or any other right, including the right to approve the way SOTC uses such material.
            <br/><br/>
            Any material submitted to this Site may be adapted, broadcast, changed, copied, disclosed, licensed, performed, posted, published, sold, transmitted or used by SOTC  anywhere in the world, in any medium, forever.
            <br/><br/>
            <b>TRANSMITTED MATERIAL</b>
            <br/>
            Internet transmissions are never completely private or secure. You understand that any message or information you send to this Site may be read or intercepted by others unless there is a special notice that a particular message (for example, credit card information) is encrypted (send in code). Sending a message to SOTC does not cause SOTC to have any special responsibility to you.
            <br/><br/>
            The copyright in the contents of this website belong to SOTC. Accordingly SOTC reserves all rights. Copying of part or all the contents of this website without permission of SOTC is prohibited except to the extent that such copying/printing is necessary for the purposes of availing of the paid services provided.
            <br/><br/>
            
            <b>PROMOTIONS / OFFERS / SCHEMES / CONTESTS</b>
            <br/>
            Please note that in case of special offers, promotions or schemes additional terms and conditions of these offers, promotions, schemes or contests will be applicable in addition to these terms and conditions. You might have to adhere to the payment schedule prescribed under such offers, promotions, schemes or contests in order to be eligible to avail benefits under such offers, promotions, schemes or contests. If you fail to make the payment by the due date and/or do not comply with all the terms and conditions of the offer, promotion, scheme or contest you would not be entitled to receive the benefit under such offer, promotion, scheme or contest. The Promotions / Offers / Schemes / Contests can also be discounted at any time without giving any specific notice
            <br/><br/>
            
            <b>BASIC TRAVEL QUOTA</b> <br/>
            As per the current guidelines of RBI, all residents holding Indian Passport are entitled to avail an amount not exceeding US$ 2, 50, 000 or its equivalent in one financial year for one or more private/leisure visits under the Basic Travel Quota (BTQ). However, the traveler cannot avail in excess of US $ 3,000 per visit or its equivalent in cash, except as specified in the exceptions provided by the RBI guidelines.
            It is mandatory for you to avail the foreign exchange component of the tour cost from under your BTQ entitlement from an authorized dealer licensed to provide such services as per the guidelines issued by RBI from time to time. You shall pay the foreign exchange component of the total tour cost at the prevailing rate to the dealer along with signed BTQ form &amp; A2 form. You can draw foreign exchange for your personal use on tour from the same authorized dealer from your balance BTQ entitlement. This is as per GOI rules &amp; regulations. You may carry this partly in currency, partly in traveler cheques (TC) which are safer and easily encashed for a small service fee. They can also be replaced if they are stolen or lost, if you record TC numbers and retain counterfoil subject to the rules applicable. <br/><br/>
            <b>PAYMENT TERMS FOR FOREX COMPONENT</b><br/>
            1) The Forex(‘FX’) payment is a part of BTQ of the passenger.<br/>
            2) The immediate family/kin, e.g. father can pay for the children; mother can pay for the children stands good. However, the person paying/cardholder should also be among the travellers.<br/>
            3) If husband, wife and two children are travelling together as a family, husband’s card is acceptable.<br/>
            4) The cardholder cannot use his/her card for payment of another person’s tour package.<br/>
            5) The card that is swiped for payment of the tour’s foreign exchange should be of the cardholder himself/herself.<br/>
            6) <b>For packages with FX (Foreign Exchange) component only, initial deposit amount collected in INR for booking, shall be adjusted towards the balance FX component at the time of final FX payment.</b><br/>
            7) <b>The ROE (Rate of Exchange) for the entire FX component of the tour shall be considered as per the prevailing ROE on the day final payment is being made.</b>

            </p>
            </body>
            </html>
            """
            
            htmlStr.setFontToHtmlString(fontName: "Roboto-Regular", FontSize: "3", fontColor: "#333333")
            delegate?.navigateToDetailView(Title: "Terms & Conditions", description: htmlStr)
        }
        else
        {
            printLog("Tapped none")
        }
        */
        
        
            var htmlStr : String = ""
            var notesHeading : String = ""
            var notesDescription: String = ""
            var descriptionstr = ""
            let filteredArray : Array<Dictionary<String,Any>> = AppUtility.filterArray(ArrayToBeFilter:  packageDetailModel.tcilHolidayPaymentTermsCollection, withKey: PackageDetailConstants.PackageClassId, AndIntegerValue: 0)
            if filteredArray.count > 0
            {
                for dict in filteredArray
                {
                    
                    if let paymentTerms : String = dict["notesHeading"] as? String
                    {
                        notesHeading = paymentTerms
                    }
                    if let Description : String = dict["notesDescription"] as? String //description
                    {
                       notesDescription = Description
                    }
                    if let Descriptionsarray: Array<Dictionary<String,Any>>   = dict["tcilHolidayTermsConditionsCollection"] as? Array<Dictionary<String, Any>>  //description
                    {
                        for discdict in Descriptionsarray
                        {
                            descriptionstr = discdict["description"] as? String ?? ""
                        }
                    }
                }
                htmlStr = notesHeading + notesDescription + descriptionstr
            }
            
            htmlStr.setFontToHtmlString(fontName: "Roboto-Regular", FontSize: "3", fontColor: "#333333")
            
        
        
        if sender.didTapAttributedTextInLabel(label: labelTermsAndCond, inRange: policyRange){
            delegate?.navigateToDetailView(Title: "Privacy Policy", description: "")
        }else if sender.didTapAttributedTextInLabel(label: labelTermsAndCond, inRange: termsRange){
//            delegate?.navigateToDetailView(Title: "Terms & Conditions", description: "")
            delegate?.navigateToDetailView(Title: "Terms & Conditions", description: htmlStr)
        }else{
           printLog("Tapped none")
        }
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func setData()
    {
        self.buttonCheckMark.isSelected = self.pricingModel.agreedOnTermsAndCondition
        self.textFieldMobileNumber.text = self.pricingModel.pricingMobileNumber
    }
    //MARK: - Button Action -

    @IBAction func termsAndConditionCheckmarkButtonClicked(_ sender: Any)
    {
        self.pricingModel.agreedOnTermsAndCondition = !self.pricingModel.agreedOnTermsAndCondition
        self.buttonCheckMark.isSelected = self.pricingModel.agreedOnTermsAndCondition
    }
    //MARK: - textfield delegate -

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if (textField == textFieldMobileNumber )
        {
            
            let textFieldText: NSString = (textField.text ?? "") as NSString
            
            var txtAfterUpdate = textFieldText.replacingCharacters(in: range, with: string)
            let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let newStr = compSepByCharInSet.joined(separator: "")
            if newStr.count == 0 && string != ""
            {
                return false
            }
            if ((txtAfterUpdate.count) <= 10)
            {
                
                if ((txtAfterUpdate.count) == 10)
                {
                    
                    self.textFieldMobileNumber.text = txtAfterUpdate
                    self.pricingModel.pricingMobileNumber = txtAfterUpdate
                    textField.resignFirstResponder()
                    return false
                }
                else
                {
                    self.pricingModel.pricingMobileNumber = txtAfterUpdate
                    return true
                }
                
            }
            else
            {
                textField.resignFirstResponder()
                return false
            }
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    
    
}
