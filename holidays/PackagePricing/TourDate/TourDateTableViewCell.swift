//
//  TourDateTableViewCell.swift
//  sotc-consumer-application
//
//  Created by ketan on 28/07/18.
//  Copyright © 2018 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit
protocol TourDateTableViewCellDelegate {
    func openFareCalendar()
}

class TourDateTableViewCell: UITableViewCell,UITextFieldDelegate
{
   var pricingModel:PricingModel = PricingModel()
    var delegate : TourDateTableViewCellDelegate? = nil
    

    @IBOutlet weak var textFieldTourDate: UITextField!
    
    @IBOutlet weak var viewTextFieldContainer: UIView!

    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
//        viewTextFieldContainer.fadedShadow()
//        viewTextFieldContainer.roundedCorners()
        textFieldTourDate.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        if pricingModel.pricinghubCode == ""
        {
            AppUtility.displayAlert(title: "Alert", message: "Select Departure City")
        }
        else
        {
            self.delegate?.openFareCalendar()
        }
        return false
    }
}
