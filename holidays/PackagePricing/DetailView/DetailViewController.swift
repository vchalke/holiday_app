//
//  DetailViewController.swift
//  sotc-consumer-application
//
//  Created by Mac on 12/07/18.
//  Copyright © 2018 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController,UIWebViewDelegate {
    var titleOfVc : String = ""
    var htmlString : String = ""
    var url : String? = nil
//    @IBOutlet weak var topNavigationView: UIView!
    
    @IBOutlet weak var detailWebview: UIWebView!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - initializeView

    func initializeView()
    {
         self.detailWebview.delegate = self
        self.titleLabel.text = titleOfVc
        if url != nil
        {
            LoadingIndicatorView.show()
            self.detailWebview.loadRequest(URLRequest.init(url: URL.init(string: url!)! ))
        }
        else
        {
            self.htmlString.setFontToHtmlString(fontName: "Roboto-Regular", FontSize: "3.5", fontColor: "#333333")
            self.detailWebview.loadHTMLString(htmlString, baseURL: nil)
        }
//        topNavigationView.fadedShadow()
    }
    // MARK: - Button Actions

    @IBAction func onClickOfBackButton(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - webview delgates -
    func webViewDidFinishLoad(_ webView: UIWebView) {
        UIView.animate(withDuration: 0.7, animations: {
            
        }) { (true) in
             LoadingIndicatorView.hide()
        }
       
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        
        LoadingIndicatorView.hide()
        AppUtility.displayAlert(title: "Alert", message: error.localizedDescription)
    }
/*
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
