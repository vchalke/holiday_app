//
//  NotificationViewController.h
//  holidays
//
//  Created by Saurav Kumar on 23/11/15.
//  Copyright © 2015 Mobicule. All rights reserved.
//

#import "BaseViewController.h"
#import "SSKeychain.h"
#import "MobiculeUtilityManager.h"
#import "CarNotificationouselTableViewCell.h"
typedef void(^isCompleted)(BOOL);

@interface NotificationViewController : BaseViewController<SlideNavigationControllerDelegate,MyTableCellProtocoll>
@property (strong, nonatomic) IBOutlet UIView *viewNotification;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic) NSInteger count;
-(void)fetchPackageDetails:(NSString *)packageID;
-(void)searchDestinationWithDestinationName:(NSDictionary *)destination;
@end
