//
//  PaymentPendingCollectionViewCell.swift
//  sotc-consumer-application
//
//  Created by Saurav Kumar on 10/08/17.
//  Copyright © 2017 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit

class PaymentPendingCollectionViewCell: UICollectionViewCell {

    @IBOutlet var buttonStackView: UIStackView!
    @IBOutlet var pendingAmountLabel: UILabel!
 
    @IBOutlet var containnerBackgroundView: UIView!
    @IBOutlet var payButton: UIButton!
    
    @IBOutlet var roeButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
