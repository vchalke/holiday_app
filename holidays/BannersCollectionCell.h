//
//  BannersCollectionCell.h
//  holidays
//
//  Created by Ios_Team on 29/10/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BannersCollectionCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *lbl_name;

@end

NS_ASSUME_NONNULL_END
