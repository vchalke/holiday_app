//
//  Passenger+CoreDataProperties.swift
//  holidays
//
//  Created by Saurav on 28/11/17.
//  Copyright © 2017 Mobicule. All rights reserved.
//

import Foundation
import CoreData


extension Passenger {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Passenger> {
        return NSFetchRequest<Passenger>(entityName: "Passenger")
    }

    @NSManaged public var aadhaarNumber: String?
    @NSManaged public var address: String?
    @NSManaged public var age: String?
    @NSManaged public var bfNumber: String?
    @NSManaged public var documentStatus: String?
    @NSManaged public var emailId: String?
    @NSManaged public var firstName: String?
    @NSManaged public var fullName: String?
    @NSManaged public var lastName: String?
    @NSManaged public var middleName: String?
    @NSManaged public var mobileNumber: String?
    @NSManaged public var passengerNumber: String?
    @NSManaged public var passengerNumber_bfNumber: String?
    @NSManaged public var passportNumber: String?
    @NSManaged public var paxType: String?
    @NSManaged public var roomNumber: String?
    @NSManaged public var roomType: String?
    @NSManaged public var title: String?
    @NSManaged public var tourRelation: Tour?

}
