//
//  DateStringSingleton.m
//  holidays
//
//  Created by Kush_Tech on 24/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "DateStringSingleton.h"

@implementation DateStringSingleton

NSString * const ErrorType_toString[] = {
    [failureInSeverError] = @"Something Went Wrong",
    [failureError] = @"Try again, Something Went Wrong",
    [intenertError] = @"Check Your Internet Connection",
    [emptyData] = @"Data Is Empty"
};

+(id)shared{
    //+ (Contact *)sharedInstance {
    static DateStringSingleton *shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[self alloc] init];
        
    });
    return shared;
}

- (id)init {
    if (self = [super init]) {
        
    }
    return self;
}

-(NSString *)getDateTimeStringFormat:(NSString*)stringFormat DateValue:(NSDate *)dateValue{
    
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormatter setDateFormat:stringFormat];
    NSString *dateString = [dateFormatter stringFromDate:dateValue];
    return dateString;
}
-(NSDate *)getDateFromStringFormat:(NSString*)stringFormat strDateValue:(NSString *)dateValue{
    
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormatter setDateFormat:stringFormat];
    NSDate *date = [dateFormatter dateFromString:dateValue];
    return date;
}
-(NSDate *)getTodaysStartDate{
    
    NSDate *date = [[NSDate date] dateByAddingTimeInterval:-(([self getCurrentHour] * 3600.)+([self getCurrenMinute]*60.0)+[self getCurrentSecond])];
    return date;
}

-(NSDate *)getTodaysLastDate{
    
    NSDate *date = [[NSDate date] dateByAddingTimeInterval:(((24-[self getCurrentHour]) * 3600.)+((60-[self getCurrenMinute])*60.0))];
    return date;
}

-(NSDate *)getTodaysCurrentDate{
    
    NSDate *date = [NSDate date];
    return date;
}

-(NSInteger )getCurrentYear{
    NSInteger currentYear= [[self getDateTimeStringFormat:@"yyyy" DateValue:[NSDate date]]integerValue];
    NSLog(@"currentYear:%ld",currentYear);
    return currentYear;
}

-(NSInteger )getCurrentMonth{
    NSInteger currentMonth= [[self getDateTimeStringFormat:@"MM" DateValue:[NSDate date]]integerValue];
    NSLog(@"currentMonth:%ld",currentMonth);
    return currentMonth;
}

-(NSInteger )getCurrentDay{
    NSInteger currentDay= [[self getDateTimeStringFormat:@"dd" DateValue:[NSDate date]]integerValue];
    return currentDay;
}

-(NSInteger )getCurrentHour{
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *dateComponents = [gregorian components:(NSCalendarUnitHour  | NSCalendarUnitMinute | NSCalendarUnitSecond | NSCalendarUnitDay) fromDate:[NSDate date]];
    NSInteger currentHour= [dateComponents hour];
    return currentHour;
}

-(NSInteger )getCurrenMinute{
    NSInteger currentMinute= [[self getDateTimeStringFormat:@"mm" DateValue:[NSDate date]]integerValue];
    return currentMinute;
}

-(NSInteger )getCurrentSecond{
    NSInteger currentSecond= [[self getDateTimeStringFormat:@"ss" DateValue:[NSDate date]]integerValue];
    return currentSecond;
}
-(NSInteger )getTotalDaysInMonthAtIndex:(NSInteger)num{
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSRange rng = [cal rangeOfUnit:NSCalendarUnitDay
                            inUnit:NSCalendarUnitMonth
                           forDate:[self getMonthDateAtIndex:num ofYearBack:0]];
    NSInteger numberOfDaysInMonth = rng.length;
    return numberOfDaysInMonth;
}
-(NSDate*)getMonthDateAtIndex:(NSInteger)num ofYearBack:(int)numOfYear{
    NSDate *currentYear=[[NSDate date] dateByAddingTimeInterval:24*365*numOfYear*3600];
    NSDateFormatter *formatter1 = [[NSDateFormatter alloc] init];
    [formatter1 setDateFormat:@"yyyy"];
    NSString *currentYearString = [formatter1 stringFromDate:currentYear];
    NSString *firstDateString=[NSString stringWithFormat:@"10 01-%ld-%@",num,currentYearString];
    NSDateFormatter *formatter2 = [[NSDateFormatter alloc] init];
    [formatter2 setDateFormat:@"hh dd-MM-yyyy"];
    NSDate *firstDate = [[NSDate alloc]init];
    firstDate = [formatter2 dateFromString:firstDateString];
    return firstDate;
}
-(NSString*)getMonthNameAtIndex:(NSInteger)num withStrFormat:(NSString*)strFormat ofYearBack:(int)numOfYear{
    NSDateFormatter *formatter2 = [[NSDateFormatter alloc] init];
    [formatter2 setDateFormat:strFormat];
    NSDate *firstDate = [self getMonthDateAtIndex:num ofYearBack:numOfYear];
    NSString *stringDate = [formatter2 stringFromDate:firstDate];
    return stringDate;
    
}
-(NSDate*)getDayNumber:(NSInteger)dayIndex OfMonth:(NSInteger)monthIndex{
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear
                                                                   fromDate:[NSDate date]];
    components.day = dayIndex;
    components.month = monthIndex;
    NSDate *firstDayOfMonthDate = [[NSCalendar currentCalendar] dateFromComponents: components];
    return firstDayOfMonthDate;
    
}
-(NSInteger)getMonthNumberFromDate:(NSDate*)date{
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear
                                                                   fromDate:date];
    return components.month;
}
@end
