//
//  OrderSummaryVC.m
//  holidays
//
//  Created by Designer on 22/12/15.
//  Copyright © 2015 Mobicule. All rights reserved.
//

#import "OrderSummaryVC.h"
#import "KLCPopup.h"
#import "LoginViewPopUp.h"
#import "UIViewController+MJPopupViewController.h"
#import "MJPopupBackgroundView.h"
#define kType @"webservice"
#define kAction @"search"
#define kEntity @"myAccountInfo"
#define kUserName @"userName"
#import "OrderDetailTableViewCell.h"
#import "WebViewController.h"

@interface OrderSummaryVC ()
{
    int flag;
    UIView *buttonView;
    LoadingView *activityIndicator;
    NSArray *arrayForOrderDetails;
}
@end

@implementation OrderSummaryVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
   // [_viewOrderSummary setFrame:CGRectMake(_viewOrderSummary.frame.origin.x , _viewOrderSummary.frame.origin.y+70, _viewOrderSummary.frame.size.width, _viewOrderSummary.frame.size.height)];
    
    [[NSBundle mainBundle]loadNibNamed:@"OrderSummaryVC" owner:self options:nil];
    [super addViewInBaseView:self.viewOrderSummary];
    flag=1;
    super.HeaderLabel.text=_headerName;

    buttonView=[[UIView alloc]init];
   buttonView.frame=CGRectMake(0, 38,self.viewOrderSummary.frame.size.width/3, 2) ;
   buttonView.backgroundColor = [UIColor whiteColor];
    [_btnHoliday addSubview:buttonView];

    
    UISwipeGestureRecognizer *recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didSwipe:)];
    [recognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [self.view addGestureRecognizer:recognizer];
  
    
    recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didSwipe:)];
    [recognizer setDirection:(UISwipeGestureRecognizerDirectionLeft)];
    [self.view addGestureRecognizer:recognizer];
    
    [self fetchOrderDetails];
    
   // CGPoint Location = [[touches anyObject] locationInView:self.view ];
   }

-(void)fetchOrderDetails
{
        
        NSUserDefaults *userIdForGooglePlusSignIn=[NSUserDefaults standardUserDefaults];
        NSString *userId=[userIdForGooglePlusSignIn valueForKey:kLoginEmailId];
        
        
        NSDictionary *dictOfData = [NSDictionary dictionaryWithObjectsAndKeys:userId,kUserName,nil];
        
        activityIndicator = [LoadingView loadingViewInView:self.tableViewOrderDetail.superview withString:@"" andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
        
        if ([super connected])
        {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                NetworkHelper *helper = [NetworkHelper sharedHelper];
                
                NSString *strResponse = [helper getDataFromServerForType:kType entity:kEntity action:kAction andUserJson:dictOfData];
                
                NSString *strStatus = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"status"];
                NSDictionary *dictOfData=[[NSDictionary alloc]init];
                if ([strStatus isEqualToString:kStatusSuccess])
                {
                    dictOfData= [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:kServerResponseKeyData];
                    
                    arrayForOrderDetails = [dictOfData objectForKey:@"holidayBookings"];
                //wishLists
                    dispatch_async(dispatch_get_main_queue(), ^{
                      
                            [activityIndicator removeView];
                      //  self.tableViewOrderDetail.alpha = 0;
                        [self.tableViewOrderDetail reloadData];
                    });
                    
                }
                else
                {
                
                    NSString *messsage = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"message"];
                    dispatch_async(dispatch_get_main_queue(), ^{
                            self.tableViewOrderDetail.alpha = 0;
                                            [activityIndicator removeView];
                        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
                        {
                            
                            if (messsage)
                            {
                                if ([messsage isEqualToString:kVersionUpgradeMessage])
                                {
                                    NSDictionary *downloadDict = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"data"];
                                    NSString *downloadURL = [downloadDict valueForKey:@"download_url"];
                                    [super showAlertViewForVersionUpdateWithUrl:downloadURL];
                                    
                                }
                            }
                            
                            else{
                                UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Info"
                                                                                                   message:@" Favourite Package not available"
                                                                                            preferredStyle:UIAlertControllerStyleAlert];
                                
                                UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                                    
                                    [alertView dismissViewControllerAnimated:YES completion:nil];
                                    
                                }];
                                
                                [alertView addAction:actionOk];
                                
                                [self presentViewController:alertView animated:YES completion:nil];
                            }
                        }
                        else
                        {
                            if (messsage)
                            {
                                if ([messsage isEqualToString:kVersionUpgradeMessage])
                                {
                                    NSDictionary *downloadDict = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"data"];
                                    NSString *downloadURL = [downloadDict valueForKey:@"download_url"];
                                    [super showAlertViewForVersionUpdateWithUrl:downloadURL];
                                }
                            }
                            else{
                                
                                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Info" message:@"Favourite Package not available" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                                
                                [alert show];
                            }
                        }
                    });
                }
                
            });
            
        }
        else
        {
            
            
          
                [activityIndicator removeView];
            
            
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
            {
                UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Info"
                                                                                   message:kMessageNoInternet
                                                                            preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    
                    [alertView dismissViewControllerAnimated:YES completion:nil];
                    
                }];
                
                [alertView addAction:actionOk];
                
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Info"
                                                               message:kMessageNoInternet
                                                              delegate:nil
                                                     cancelButtonTitle:@"OK"
                                                     otherButtonTitles:nil, nil];
                
                [alert show];
            }
        }
        
        
    
}


-(void)didSwipe:(UISwipeGestureRecognizer*)sender{
//
    UISwipeGestureRecognizer *gesture = (UISwipeGestureRecognizer *)sender;
//    UIView *swipedHeaderView = (UIView *)sender.view;
//    UIView* temp = [(UISwipeGestureRecognizer*)sender view];
//    
    
   
    if (gesture.direction == UISwipeGestureRecognizerDirectionLeft)
    {
        if (flag==1) {
            [super addViewInBaseView:self.viewFlight];
            [self.viewFlight setFrame:CGRectMake(self.viewFlight.frame.origin.x, self.viewFlight.frame.origin.y+90, self.viewFlight.frame.size.width, self.viewFlight.frame.size.height)];
            flag=2;
            [_btnFlight addSubview:buttonView];

            }
        
        else if (flag==2) {
            
            [super addViewInBaseView:self.viewHotel];
            [self.viewHotel setFrame:CGRectMake(self.viewHotel.frame.origin.x, self.viewHotel.frame.origin.y+90, self.viewHotel.frame.size.width, self.viewHotel.frame.size.height)];
            flag=3;
            [_btnHotel addSubview:buttonView];


        }
        
    }
    
    if (gesture.direction==UISwipeGestureRecognizerDirectionRight) {
        if (flag==3) {
            [super addViewInBaseView:self.viewFlight];
            [self.viewFlight setFrame:CGRectMake(self.viewFlight.frame.origin.x, self.viewFlight.frame.origin.y+90, self.viewFlight.frame.size.width, self.viewFlight.frame.size.height)];
            flag=2;
            [_btnFlight addSubview:buttonView];


        }
        
        else if(flag==2) {
            [super addViewInBaseView:self.viewOrderSummary];
            flag=1;
            [_btnHoliday addSubview:buttonView];


        }
    }

}

/*
- (void) didSwipe:(UISwipeGestureRecognizer *)recognizer{
    
     // UIView* temp = [(UITapGestureRecognizer*)sender view];
    
    UIView *temp1=[(UISwipeGestureRecognizer*)recognizer view];
 //   temp1.tag=100;
    
    NSInteger tag=100+temp1.tag;
    if([recognizer direction] == UISwipeGestureRecognizerDirectionLeft){
        //Swipe from right to left
        //Do your functions here
        if(self.view)
         [super addViewInBaseView:self.viewOrderSummary];
    }
    else{
        //Swipe from left to right
        //Do your functions here
        
        
        [super addViewInBaseView:self.viewHotel];
        [self.viewHotel setFrame:CGRectMake(self.viewHotel.frame.origin.x, self.viewHotel.frame.origin.y+90, self.viewHotel.frame.size.width, self.viewHotel.frame.size.height)];
    }
}

*/
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


- (IBAction)actionOnLoginBtn:(id)sender {
    
//    
//    LoginViewPopUp *loginView = [[LoginViewPopUp alloc] initWithNibName:@"LoginViewPopUp" bundle:nil];
//    loginView.view.frame = CGRectMake(0, 0, 300, self.viewOrderSummary.frame.size.height - 100);
//    [self presentPopupViewController:loginView animationType:MJPopupViewAnimationFade];

}
- (IBAction)actionOnFlightHeaderBtn:(id)sender {
    
      [super addViewInBaseView:self.viewFlight];
    [self.viewFlight setFrame:CGRectMake(self.viewFlight.frame.origin.x, self.viewFlight.frame.origin.y+90, self.viewFlight.frame.size.width, self.viewFlight.frame.size.height)];
    
    [_btnFlight addSubview:buttonView];

    
}

- (IBAction)actionOnHotelHeaderBtn:(id)sender {
    
      [super addViewInBaseView:self.viewHotel];
    [self.viewHotel setFrame:CGRectMake(self.viewHotel.frame.origin.x, self.viewHotel.frame.origin.y+90, self.viewHotel.frame.size.width, self.viewHotel.frame.size.height)];
     [_btnHotel addSubview:buttonView];
}

- (IBAction)actionOnHolidayHeaderBtn:(id)sender {
      [super addViewInBaseView:self.viewOrderSummary];
     [_btnHoliday addSubview:buttonView];
}

#pragma mark -TableView Delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrayForOrderDetails.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
     static NSString *cellIdentifier= @"Cell";
    OrderDetailTableViewCell *cell= (OrderDetailTableViewCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell==nil) {
        cell=(OrderDetailTableViewCell *)[[[NSBundle mainBundle] loadNibNamed:@"OrderDetailTableViewCell" owner:self options:nil] lastObject];
    }
    
    NSDictionary *dictForOrderDetails = arrayForOrderDetails [indexPath.row];
    float totalCost = [[dictForOrderDetails valueForKey:@"totalPrice"] floatValue];
    float amountPaid = [[dictForOrderDetails valueForKey:@"amountPaid"] floatValue];
    
    cell.labelPackageName.text = [dictForOrderDetails valueForKey:@"packageName"];
    cell.labelDateOfTravel.text = [dictForOrderDetails valueForKey:@"travelDate"];
    cell.labelReferenceID.text = [dictForOrderDetails valueForKey:@"hldRefId"];
    cell.labelTotalCost.text = [NSString stringWithFormat:@"₹ %.2f",totalCost];
    cell.labelAmountPaid.text = [NSString stringWithFormat:@"₹ %.2f",amountPaid];
    cell.labelBalanceAmount.text = [NSString stringWithFormat:@"₹ %.2f",totalCost-amountPaid];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 262.0;
}


- (IBAction)onHotelDetailsClicked:(id)sender {
    
    WebViewController *webViewVC = [[WebViewController alloc]initWithNibName:@"BaseViewController" bundle:nil];
    NSString *forexUrl = @"http://m.thomascook.in/tcm/hotels/?";
    NSURL *url = [NSURL URLWithString:forexUrl];
    webViewVC.url = url;
    webViewVC.headerString = @"Hotels";
    [[SlideNavigationController sharedInstance] pushViewController:webViewVC animated:NO];
}

- (IBAction)onFlightDetailsClicked:(id)sender {
    
    WebViewController *webViewVC = [[WebViewController alloc]initWithNibName:@"BaseViewController" bundle:nil];
    NSString *forexUrl = @"http://m.thomascook.in/tcm/flights/?";
    NSURL *url = [NSURL URLWithString:forexUrl];
    webViewVC.url = url;
    webViewVC.headerString = @"Flight";
    [[SlideNavigationController sharedInstance] pushViewController:webViewVC animated:NO];
}
@end
