//
//  TravelBlogObj.h
//  holidays
//
//  Created by Kush_Team on 10/06/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface TravelBlogObj : NSObject
@property(nonatomic,strong)NSString *link;
@property(nonatomic,strong)NSString *title;
@property(nonatomic,strong)NSString *imgUrl;
-(instancetype)initWithTravelArray:(NSDictionary *)arrayObj;
@end

NS_ASSUME_NONNULL_END
