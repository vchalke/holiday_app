//
//  HelpViewController.swift
//  sotc-consumer-application
//
//  Created by Payal on 12/08/17.
//  Copyright © 2017 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit
import CoreData


class HelpViewController: UIViewController {
    
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        fetchUserBookedTour()
        
        
        
    }
    
    func fetchUserBookedTour()  {
        
        self.regionalView.isHidden = true
        
        do {
            
            var userList: [UserProfile]?
            
            var bookObjectList:[Tour]?
            
            let fetchRequest: NSFetchRequest<UserProfile> = UserProfile.fetchRequest()
            
            let userMobileNumber:String = UserDefaults.standard.value(forKey:UserDefauldConstant.userMobileNumber) as! String
            
            let predicate = NSPredicate(format: "mobileNumber == \(userMobileNumber)")
            
            fetchRequest.predicate = predicate
            
             userList = try CoreDataController.getContext().fetch(fetchRequest)
            
            if (userList?.count)! > 0 {
                
                // let bookedList:NSSet = (userList?.first?.tourRelation)!
                
                bookObjectList = Array((userList?.first?.tourRelation)!) as? [Tour]
                
                bookObjectList?.sort(by: { $0.departureDate?.compare($1.departureDate! as Date) == ComparisonResult.orderedAscending })
                
                
                if let futureBookedArray = bookObjectList?.filter( { (tourDetail: Tour) -> Bool in
                    
                    return (tourDetail.departureDate?.timeIntervalSince1970)! > Date().timeIntervalSince1970  })
                {
                    
                    if let currentTourObject:Tour = futureBookedArray.first {
                        
                        
                        if let regionString = currentTourObject.tourZone    {
                            
                            self.regionalView.isHidden = false
                            
                            switch regionString.lowercased() {
                                
                            case HelpEmailIDForRegions.East.region:
                                
                                self.regionalEmailIDLabel.text = HelpEmailIDForRegions.East.emailId
                                
                            case HelpEmailIDForRegions.West.region:
                                
                                self.regionalEmailIDLabel.text = HelpEmailIDForRegions.West.emailId
                                
                            case HelpEmailIDForRegions.South.region:
                                
                                self.regionalEmailIDLabel.text = HelpEmailIDForRegions.South.emailId
                                
                            case HelpEmailIDForRegions.Gujarat.region:
                                
                                self.regionalEmailIDLabel.text = HelpEmailIDForRegions.Gujarat.emailId
                                
                            case HelpEmailIDForRegions.North.region:
                                
                                self.regionalEmailIDLabel.text = HelpEmailIDForRegions.North.emailId
                                
                            default:
                                
                                self.regionalView.isHidden = true
                            }
                            
                            
                            
                        }
                        else
                        {
                            self.regionalView.isHidden = true
                        }
                        
                    }
                    else
                    {
                        self.regionalView.isHidden = true
                    }
                    
                    
                }
                else
                    
                {
                    self.regionalView.isHidden = true
                }
            }
            else
            {
                self.regionalView.isHidden = true
            }
            
        } catch   {
            printLog(error)
            
            self.regionalView.isHidden = true
        }
        
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBOutlet var regionalEmailIDLabel: UILabel!
    @IBOutlet var regionalView: UIView!
    override func viewWillAppear(_ animated: Bool)
    {
        
      
        
        self.navigationController?.setRedNavigationBar()
        self.navigationController?.setnavigatiobBarTitle(title: "Help", viewController: self)
        self.navigationController?.setEmergencyNavigationRightButton(showEmergencyButton: true, viewController: self)
        self.navigationController?.setNagationBackButton(showRightButton: true, viewController: self)
        
    }
    
}
