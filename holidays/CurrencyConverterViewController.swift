//
//  CurrencyConverterViewController.swift
//  holidays
//
//  Created by Indresh singh on 09/12/17.
//  Copyright © 2017 Mobicule. All rights reserved.
//

import UIKit
import QuartzCore

struct GraphData{
    let xValue:Decimal
    let day:String
    enum SerializationError: Error {
        case missing(String)
        case invalid(String, Any)
    }
    
    init(json:[String:Any]) throws{
        guard let xValue = json["roe"] as? Decimal else { throw SerializationError.missing("X Value Missing")}
        guard let day = json["key"] as? String else {throw SerializationError.missing("Rate Value Missing")}
        self.xValue = xValue
        self.day = day
            
        
    }
}

class CurrencyConverterViewController: ForexBaseViewController, LineChartDelegate, UITextFieldDelegate
{
    @IBOutlet weak var indianCurrencyTextField: UITextField!
    @IBOutlet weak var forgenCurrencyTextField: UITextField!
    var data: [CGFloat] = []
    var xLabels: [String] = []
    var countryCode:String = ""
    var tag:Bool = true
    var currentDateValue: Double = 0.0
    @IBOutlet weak var currentDateLabel: UILabel!
    @IBOutlet weak var currentValueLabel: UILabel!
    
    @IBOutlet weak var graphSegmentedControl: CustomSegmentControl!
    @IBOutlet weak var selectCurrencyButton: UIButton!
    @IBOutlet weak var buyGraphView: LineChart!
    @IBOutlet var currencyConverterView: UIView!
    var currencyRateDict = [[String: Any]]()
    @IBOutlet weak var currencyGraph: UIView!
    
    var previousController : String = ""
    
    var currentSelectedModule = "1"
    
    //MARK: - Controller Methods
    override func viewDidLoad()
    {
        super.viewDidLoad()
        Bundle.main.loadNibNamed(CURRENCY_CONVERTER_VC, owner: self, options: nil)
        super.addViewInBaseView(childView: self.currencyConverterView)
               super.setUpHeaderLabel(labelHeaderNameText: "Currency Converter")
        fetchCurrencyCountry(moduleID:currentSelectedModule,productId:"3",currencyCode:"")
        
        buyGraphView.delegate = self
        
        data = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        // simple line with custom x axis labels
        xLabels = ["FRI","THU","WED","TUE","MON","SUN","SAT"]
        
         self.setupbuyGraphViewForBuyRates(data: data, xLabel: xLabels)
        
        indianCurrencyTextField.delegate = self
        forgenCurrencyTextField.delegate = self
        
        self.showCurrentDate()
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ForexBuyPassangerViewController.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        
        self.addDoneButtonOnKeyboard()
    }
    
    override func backButtonClicked(buttonView:UIButton )
    {
        if self.previousController == "SlideMenuViewController"
        {
            SlideNavigationController.sharedInstance().popToRootViewController(animated: true)
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle       = UIBarStyle.default
        let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(CurrencyConverterViewController.doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.forgenCurrencyTextField.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction()
    {
        self.forgenCurrencyTextField.resignFirstResponder()
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        fetchCurrencyCountry(moduleID:currentSelectedModule,productId:"3",currencyCode:"")
    }

    func showCurrentDate()
    {
        let currentDate = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM yyy"
        self.currentDateLabel.text = formatter.string(from: currentDate)
    }
    
    override func viewWillLayoutSubviews() {
        super.setSubViewFrame(childView: self.currencyConverterView)
    }
    
    //Line chart delegate method.
    func didSelectDataPoint(_ x: CGFloat, yValues: Array<CGFloat>)
    {
        //                label.text = "x: \(x)     y: \(yValues)"
    }
    
    //Redraw chart on device rotation.
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation)
    {
        if let chart = buyGraphView
        {
            chart.setNeedsDisplay()
        }
    }
    
    @IBAction func selectButtonPressed(_ sender: UIButton) {
        tag = false
       
        let alertController : UIAlertController  = UIAlertController.init(title: "Select Currency", message: "", preferredStyle:.actionSheet)
        
        for item in self.currencyRateDict
        {
            if let currencyName = item["currencyName"] as? String{
                let  alertAction: UIAlertAction = UIAlertAction.init(title: currencyName, style: .default, handler:
                {
                    (alert: UIAlertAction!) -> Void in
                    self.selectCurrencyButton.setTitle(currencyName, for: .normal)
                    if let currencyCode = item["currencyCode"] as? String{
                              self.countryCode = currencyCode
                        
                                self.getCurrencyCodeData(currencyCode: self.countryCode)
                                  self.setTextFieldPlaceHolderValue(countryName: self.countryCode)
                             }else
                    {
                      //  self.currentDateValue = 0.0
                        
                    }
                })
                 alertController.addAction(alertAction)
               }
            }
        let  alertCancel: UIAlertAction = UIAlertAction.init(title: "Cancel" , style: .cancel, handler:
        {
            (alert: UIAlertAction!) -> Void in
          })
        alertController.addAction(alertCancel)
        self.present(alertController, animated:true, completion: nil)
    }
    
    func fetchCurrencyCountry(moduleID:String,productId:String,currencyCode:String)
    {
        LoadingIndicatorView.show()
        let pathParameter  : NSString = "/tcForexRS/generic/roe/\(moduleID)/\(productId)" as NSString
        ForexCommunicationManager.sharedInstance.execTask(pathParam:pathParameter , queryParam: "", requestType: "get", jsonDict:NSDictionary()) { (status, response) in
            
             DispatchQueue.main.async{
                
                LoadingIndicatorView.hide()
            
            if( !status || response == nil)
            {
                self.showAlert(message: "No data availabel")
            }
            else
            {
               
                    if let responseDict = response as?  [[String: Any]]
                    {
                        self.currencyRateDict  = responseDict
                        
                    }
                }
            }
         }
     }
    
    func buyGraphDataLoad(type:String, currencyCode: String)
    {
         LoadingIndicatorView.show()
         setTextFieldPlaceHolderValue(countryName: currencyCode)
        //http://services-uatastra.thomascook.in/tcForexRS/generic/rateCards/1/1/AUD
       // https://services-uatastra.thomascook.in/tcForexRS/generic/rateCards

        let pathParameter  : NSString = "/tcForexRS/generic/rateCards/".appending(type)+"/3/".appending(currencyCode) as NSString
        ForexCommunicationManager.sharedInstance.execTask(pathParam:pathParameter , queryParam: "", requestType: "get", jsonDict:NSDictionary()) { (status, response) in
            self.currentDateValue == 0.0
            
            DispatchQueue.main.async {
             LoadingIndicatorView.hide()
          
            if response == nil{
                self.showAlert(message: "Data not available")
                
                
            }else
            {
                var data1: [CGFloat] = []
                var xLabels1: [String] = []
                let responseDict:NSDictionary = response as!  NSDictionary
                print(responseDict)
                if let beanDict:Dictionary<String,Any> =  responseDict["bean"] as? Dictionary<String,Any>
                {
                    if let entryArray:Array<Any> =  beanDict["entry"] as? Array<Any>
                    {
                        if let dayArray:Array<String> =  responseDict["days"] as? Array<String>
                        {
                            for rate in dayArray
                            {
                                xLabels1.append(rate)
                                for rateDay in entryArray
                                {
                                    if let rateDataDict = rateDay as? [String: Any]
                                    {
                                        if let dayname = rateDataDict["key"] as? String
                                        {
                                            if (rate == dayname)
                                            {
                                                if let rate = rateDataDict["value"] as? [String: Any],
                                                    let currentRate = rate["roe"] as? Double
                                                {
                                                    data1.append(CGFloat(currentRate.truncate(places: 2)))
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                   DispatchQueue.main.async {
                        print("Data:\(data1)")
                        print("Days\(xLabels1)")
                    
                    if data1.count > 3 && xLabels1.count > 3{
                        self.setupbuyGraphViewForBuyRates(data: data1.reversed(), xLabel: xLabels1.reversed())
                        self.currentValueLabel.text = "1 \(currencyCode) = \(data1[0]) INR"
                        self.currentDateValue = Double(data1[0])
                        
                    }else {
                        
                         self.showAlert(message: "Data not available.")
                         self.currentDateValue = 0.0
                       
                    }
                    
                    }
                 }
            }
      }
        }
    }
   
    func getCurrencyCodeData(currencyCode: String) {
        buyGraphDataLoad(type: currentSelectedModule, currencyCode: currencyCode)
    }
    
    func clearScreenData()  {
        
        
        forgenCurrencyTextField.text = ""
        indianCurrencyTextField.text = ""
        forgenCurrencyTextField.placeholder = "0"
        indianCurrencyTextField.placeholder = "0"
       // self.setTextFieldPlaceHolderValue(countryName: "")
        self.selectCurrencyButton.setTitle("Select Currency", for: .normal)
        self.currentValueLabel.text = ""
        data = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        // simple line with custom x axis labels
        xLabels = ["FRI","THU","WED","TUE","MON","SUN","SAT"]
        
         self.setupbuyGraphViewForBuyRates(data: data, xLabel: xLabels)
        
        
    }
    
    @IBAction func buySellSegmentChanged(_ sender: CustomSegmentControl) {
       switch  graphSegmentedControl.selectedSegementIndex
                {
                case 0:
                     self.currentSelectedModule = "1"
                     
                     self.clearScreenData()
                     
                     self.fetchCurrencyCountry(moduleID: self.currentSelectedModule, productId: "3", currencyCode: "")
                     
                  /*  if tag{
                        self.setupbuyGraphViewForBuyRates(data: data, xLabel: xLabels)
                                            } else{
                        
                       
                       buyGraphDataLoad(type: self.currentSelectedModule, currencyCode: self.countryCode)
                    }*/
                     
                     
                   break;
                case 1:
                    
                    self.currentSelectedModule = "2"
                    
                    self.clearScreenData()
                    
                    self.fetchCurrencyCountry(moduleID: self.currentSelectedModule, productId: "3", currencyCode: "")
                    
                   /* if tag {
                         self.setupbuyGraphViewForBuyRates(data: data, xLabel: xLabels)
                      
                    }else{
                         buyGraphDataLoad(type: self.currentSelectedModule, currencyCode: self.countryCode)
                         }*/
                   break;
                default:
                    break;
                }

    }

    func setupbuyGraphViewForBuyRates(data: [CGFloat], xLabel: [String])
    {
        
        self.data = data
        self.xLabels = xLabel
        
         buyGraphView.clearAll()
         buyGraphView.animation.enabled = true
        buyGraphView.area = false
        buyGraphView.x.labels.visible = true
        buyGraphView.x.grid.count = 1
        buyGraphView.y.grid.count = 10
        buyGraphView.x.labels.values = xLabels
        buyGraphView.y.labels.visible = true
        buyGraphView.addLine(data)
        
    }
 public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool{
        if textField == forgenCurrencyTextField{
            

            if selectCurrencyButton.titleLabel?.text == "Select Currency" {
                showAlert(message: "Please select currency")
                
            } else{
                
                if (forgenCurrencyTextField.text?.count)! > 0{
                        indianCurrencyTextField.text = "\(String(Double(forgenCurrencyTextField.text!)! * self.currentDateValue))"
                    }
                 }
             }
        else if textField == indianCurrencyTextField{
            if selectCurrencyButton.titleLabel?.text == "Select Currency" {
                showAlert(message: "Please select currency")
            }
            else
            {
                    if (indianCurrencyTextField.text?.count)! > 0{
                        forgenCurrencyTextField.text = "\(String((Double(indianCurrencyTextField.text!)! / self.currentDateValue).truncate(places: 4)))"
                    }
            }
        }
     return true
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
         if textField == forgenCurrencyTextField
         {
            if selectCurrencyButton.titleLabel?.text == "Select Currency"
            {
                showAlert(message: "Please select currency")
                return false
            }
            else
            {
                    let amount = forgenCurrencyTextField.text!
                    if string != "" {
                    indianCurrencyTextField.text = "\(String(Double(amount.appending(string))! * self.currentDateValue))"
                    return true
                    }
                    else{
                        if amount.count > 1{
                            indianCurrencyTextField.text = "\(String((Double(amount.substring(to: amount.index(before: amount.endIndex))))! * self.currentDateValue))"
                        }
                        else{
                            
                            indianCurrencyTextField.text = ""
                            indianCurrencyTextField.placeholder = "0"
                        }
                    }
                }
        }
        else if textField == indianCurrencyTextField{
            if selectCurrencyButton.titleLabel?.text == "Select Currency" {
                showAlert(message: "Please select currency")
                return false
            }
            else
            {
                let amount = indianCurrencyTextField.text!
                if string != "" {
                    forgenCurrencyTextField.text = "\(String((Double(amount.appending(string))! / self.currentDateValue).truncate(places: 3)))"
                    return true
                }
                else{
                    if amount.count > 1{
                        forgenCurrencyTextField.text = "\(String(((Double(amount.substring(to: amount.index(before: amount.endIndex))))! / self.currentDateValue).truncate(places: 3)))"
                    }
                    else{
                        
                        forgenCurrencyTextField.text = ""
                        forgenCurrencyTextField.placeholder = "0"
                    }
                }
          }
    }
       return true
   }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == forgenCurrencyTextField{
            if selectCurrencyButton.titleLabel?.text == "Select Currency" {
                showAlert(message: "Please select currency")
                print("Select Currency")
                return false
            }
        }
          if textField == indianCurrencyTextField{
                if selectCurrencyButton.titleLabel?.text == "Select Currency" {
                    showAlert(message: "Please select currency")
                    print("Select Currency")
                    return false
                }
            }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    func dismissKeyboard()
    {
        view.endEditing(true)
    }
    
    //Adding  the place holder to the text field
    func setTextFieldPlaceHolderValue(countryName: String){
        forgenCurrencyTextField.text = ""
        indianCurrencyTextField.text = ""
        forgenCurrencyTextField.placeholder = "\(countryName)"
        indianCurrencyTextField.placeholder = "INR Amount"
    }
}
