//
//  NumberTableViewCell.m
//  Holiday
//
//  Created by Kush Thakkar on 14/02/20.
//  Copyright © 2020 Kush Thakkar. All rights reserved.
//

#import "NumberTableViewCell.h"

@implementation NumberTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.numberButton.layer.cornerRadius = self.numberButton.bounds.size.height/2;
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)numberButtonPressed:(UIButton *)sender {
//
//    if([self.delegate respondsToSelector:@selector(didSelectButton:)]){
//        [self.delegate didSelectButton:self];
//    }
    
//    [sender setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    sender.backgroundColor = [UIColor blueColor];
}

@end
