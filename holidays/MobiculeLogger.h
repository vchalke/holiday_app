
#import <Foundation/Foundation.h>


#define ENABLE_LOGGER() \
[[MobiculeLogger getInstance] \
setDebugEnabled:YES]

#define DISABLE_LOGGER() \
[[MobiculeLogger getInstance] \
setDebugEnabled:NO]

#define DEBUG_ENABLED \
[[MobiculeLogger getInstance] \
isLogEnabled]



#define kSTART_SEPERATOR_LINE @"\n------------------------------------------------------------"
#define kEND_SEPERATOR_LINE @"------------------------------------------------------------"

#define debug(format,...) \
[[MobiculeLogger getInstance] \
logEvent:@"" \
withClassName:NSStringFromClass([self class]) \
andMethodName:NSStringFromSelector(_cmd) \
andLineNumber:__LINE__ \
andStartMarker:@"" \
andEndMarker:@"" \
andInput:(format), ##__VA_ARGS__]

#define debugMethodStart(format,...) \
[[MobiculeLogger getInstance] \
logEvent:@"\nEntering " \
withClassName:NSStringFromClass([self class]) \
andMethodName:NSStringFromSelector(_cmd) \
andLineNumber:__LINE__ \
andStartMarker:kSTART_SEPERATOR_LINE \
andEndMarker:@"" \
andInput:(format), ##__VA_ARGS__]

#define debugMethodEnd(format,...) \
[[MobiculeLogger getInstance] \
logEvent:@"\nReturning from " \
withClassName:NSStringFromClass([self class]) \
andMethodName:NSStringFromSelector(_cmd) \
andLineNumber:__LINE__ \
andStartMarker:@"" \
andEndMarker:kEND_SEPERATOR_LINE \
andInput:(format), ##__VA_ARGS__]

@interface MobiculeLogger : NSObject

/**
 *  Boolean value to enable or disable Logger
 */
@property BOOL isDebugEnabled;

/**
 *  Singaleton class returns object of MobiculeLogger Class
 *
 *  @return MobiculeLogger Class instance
 */
+ (MobiculeLogger *) getInstance;

/**
 *  Prints description of method from which it is called.
 *
 *  @param event       A unique value to differentiate between two methods
 *  @param className   Class name form it is called
 *  @param methodName  Method name from which it is called
 *  @param lineNumber  Line number of method it executing
 *  @param startMarker Graphical pattern as a seperator at begnning of method
 *  @param endmarker   Graphical pattern as a seperator at end of method
 *  @param input       multiple variable values at that instance.
 */
-(void)logEvent:(NSString *)event
  withClassName:(NSString *)className
  andMethodName:(NSString *)methodName
  andLineNumber:(int)lineNumber
 andStartMarker:(NSString *)startMarker
   andEndMarker:(NSString *)endmarker
       andInput:(NSString*)input, ...;

/**
 *  Enable or Disable Logger
 *
 *  @param enable Boolean to enable or disable the logger
 */
-(void) setDebugEnabled:(BOOL) enable;

/**
 *  Check whether logger is enabled or not
 *
 *  @return return TRUE if it is enabled or NO if not
 */
-(BOOL) isLogEnabled;

@end
