//
//  RequestACallVC.m
//  holidays
//
//  Created by Kush_Team on 05/06/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "RequestACallVC.h"
#import "UIViewController+MJPopupViewController.h"
#import "NetCoreAnalyticsVC.h"
#import <NetCorePush/NetCorePush.h>
@interface RequestACallVC ()<NewMasterVCDelegate>
{
    NSString *packageType;
    
    NSString *requestType;
    
    NSString *subProductType;
    LoadingView *activityLoadingView;
    BOOL isCheck;
}
@end

@implementation RequestACallVC

- (void)viewDidLoad {
//    [super viewDidLoad];
    isCheck =YES;
    [self.btn_checkpolicy addTarget:self action: @selector(checkPolicyClick:) forControlEvents:UIControlEventTouchUpInside];
    self.txt_DepartureCity.text = @"";
    self.txt_PhoneNumber.inputAccessoryView = [self getUIToolBarForKeyBoard];
    self.masterDelgate = self;
    // Do any additional setup after loading the view from its nib.
}
-(void)checkPolicyClick:(id)sender{
    isCheck = !isCheck;
    [self.btn_checkpolicy setImage:[UIImage imageNamed:(isCheck) ? @"rightCheckSelect" : @"rightCheckNoSelect"] forState:UIControlStateNormal];
}

-(void)viewWillAppear:(BOOL)animated
{
    if([[self.requestCallHolidayPackage.strPackageSubType lowercaseString] isEqualToString:@"fit fixed"]|| [[self.requestCallHolidayPackage.strPackageSubType lowercaseString] isEqualToString:@"git"])
    {
        if([[self.requestCallHolidayPackage.strPackageType lowercaseString] isEqualToString:@"international"])
        {
            packageType = kpackageTypeGITInternational;
            
        }
        else
        {
            packageType = kpackageTypeGITDomestic;
        }
    }else
    {
        if([[self.requestCallHolidayPackage.strPackageType lowercaseString] isEqualToString:@"international"])
        {
            packageType = kpackageTypeFITInternational;
        }
        else
        {
            packageType = kpackageTypeFITDomestic;
        }
    }
    
    CoreUtility *coreobj=[CoreUtility sharedclassname];
    
    if (coreobj.strPhoneNumber.count != 0)
    {
        NSString *phoneNumber = coreobj.strPhoneNumber[0];
        if (phoneNumber != nil && ![phoneNumber isEqualToString:@""])
        {
            self.txt_PhoneNumber.text = phoneNumber;
        }
    }
}
- (void)viewDidAppear:(BOOL)animated{
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)submitRequestWithURL:(NSString *)urlString
{
    activityLoadingView = [LoadingView loadingViewInView:self.view.superview
                                             withString:@""
                                      andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSString *newString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url=[NSURL URLWithString:newString];
        NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:url];
        //  [request setHTTPMethod:@"HEAD"];
        
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            NSHTTPURLResponse *response;
            NSData *responseData =  [NSURLConnection sendSynchronousRequest:request returningResponse:&response error: NULL];
            NSString *responseString =   [[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding];
            NSLog(@"request for call%@",responseString);
            [activityLoadingView removeFromSuperview];
            [self netCoreInternationalHolidayEmailFrom];
            if([response statusCode]==200)
            {
                [self showButtonsInAlertWithTitle:@"Alert" msg:@"Request Submitted successfully" style:UIAlertControllerStyleAlert buttArray:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString * _Nonnull strValue) {
                    [self.navigationController popViewControllerAnimated:YES];
                }];
            }
            else
            {
                [self showButtonsInAlertWithTitle:@"Alert" msg:kMessageSomeErrorTryAfterSometime style:UIAlertControllerStyleAlert buttArray:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString * _Nonnull strValue) {
                     [self.navigationController popViewControllerAnimated:YES];
                }];
            }
            [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
        });
        
    });
}

#pragma mark - TextField Delegaet
-(void)setToolBarActionTextField:(UIBarButtonItem *)barButtonItem{
    [self.txt_PhoneNumber resignFirstResponder];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    return YES;
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField == self.txt_DepartureCity){
        NSArray *hubList = self.requestCallHolidayPackage.arrayLtPricingCollection;
        UIAlertController *durationActionSheet = [UIAlertController alertControllerWithTitle:@"Cities" message:@"Select your choice" preferredStyle:UIAlertControllerStyleActionSheet];
        NSArray *filteredarray = hubList;
   
        for (int j =0 ; j < filteredarray.count; j++)
        {
            NSDictionary *cityDict = filteredarray[j];
            NSDictionary *hubCodeDict = [cityDict valueForKey:@"hubCode"];

            NSString *titleString = [hubCodeDict valueForKey:@"cityName"];
            UIAlertAction *action = [UIAlertAction actionWithTitle:titleString style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                self.txt_DepartureCity.text = titleString;
            }];
            [durationActionSheet addAction:action];
        }
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
        
            [self dismissViewControllerAnimated:durationActionSheet completion:nil];
        }];
        
        [durationActionSheet addAction:cancelAction];
        
        [self.view.window.rootViewController presentViewController:durationActionSheet animated:YES completion:nil];
        return NO;
        
    }else if(textField == self.txt_City){
        [self showMultipleOptionsInAlertWithTitle:@"Product" msg:@"Select Package Type" style:UIAlertControllerStyleActionSheet buttArray:[NSArray arrayWithObjects:@"Internatinal",@"Domestic",nil] completion:^(NSString * string) {
            if (![string isEqualToString:@"Cancel"]){
            self.txt_City.text = string;
            }
        }];
        return NO;
    }
    return YES;
}

-(void)setAccomType:(NSString *)accomTypeString
{
    if ([[accomTypeString lowercaseString] isEqualToString:@"standard"])
    {
        self.requestCallHolidayPackage.stringSelectedAccomType = @"0";
    }
    else if ([[accomTypeString lowercaseString] isEqualToString:@"delux"]||[[accomTypeString lowercaseString] isEqualToString:@"deluxe"])
    {
        self.requestCallHolidayPackage.stringSelectedAccomType = @"1";
    }
    else
    {
        self.requestCallHolidayPackage.stringSelectedAccomType = @"2";
    }
}

//APP_EMAIL_FORM

-(void)netCoreInternationalHolidayEmailFrom
{
    NSMutableDictionary *payloadList =  [NSMutableDictionary dictionary];
    [payloadList setObject:[NSString stringWithFormat:@"%@",self.requestCallHolidayPackage.strPackageId] forKey:@"s^PACKAGE_ID"];
    [payloadList setObject:self.txt_Summery.text forKey:@"s^SUMMARY"];
    [payloadList setObject:[NSString stringWithFormat:@"%@",self.requestCallHolidayPackage.strPackageName] forKey:@"s^DESTINATION"];
    [payloadList setObject:@"App" forKey:@"s^SOURCE"];
    [payloadList setObject:self.requestCallHolidayPackage.strPackageType forKey:@"s^TYPE"]; //28-02-2018
    
//  [NetCoreAnalyticsVC getNetCorePushNotificationDetailDictionary:payloadList withPayloadCount:107]; //28-02-2018
    
    [NetCoreAnalyticsVC getNetCorePushNotificationDetailDictionary:payloadList withPayloadCount:127]; //28-02-2018
    
    
    
}



-(void)netRequestCallBackForm  // 09-04-2018
{
    NSMutableDictionary *profileDetail = [[NSMutableDictionary alloc] init];
    [profileDetail setObject:self.txt_FirstName.text forKey:@"FIRST_NAME"];
    [profileDetail setObject:self.txt_LastName.text forKey:@"LAST_NAME"];
    [profileDetail setObject:self.txt_EmailIId.text forKey:@"EMAIL"];
    [profileDetail setObject:self.txt_City.text forKey:@"CITY"];
    [profileDetail setObject:@"view details" forKey:@"FORM_TYPE"];
    
    [NetCoreAnalyticsVC getNetCorePushNotificationDetailDictionary:profileDetail withPayloadCount:102];
    
    CoreUtility *coreobj=[CoreUtility sharedclassname];
    
    if (coreobj.strPhoneNumber.count != 0)
    {
        NSString *phoneNumber = coreobj.strPhoneNumber[0];
        if (phoneNumber != nil && ![phoneNumber isEqualToString:@""]){
            [[NetCoreInstallation sharedInstance] netCoreProfilePush:phoneNumber Payload:profileDetail Block:nil];
        }else{
            [[NetCoreInstallation sharedInstance] netCoreProfilePush:@"" Payload:profileDetail Block:nil];
        }
    }else{
        [[NetCoreInstallation sharedInstance] netCoreProfilePush:@"" Payload:profileDetail Block:nil];
    }
     
}

#pragma mark - Button Action
- (IBAction)btn_backPress:(id)sender {
     [self.navigationController popViewControllerAnimated:YES];
}
-(NSString*)checkAllFilds{
    if ([self.txt_FirstName.text length]==0 || [self.txt_LastName.text length]==0 || [self.txt_PhoneNumber.text length]==0 || [self.txt_EmailIId.text length]==0 || [self.txt_Summery.text length]==0 || [self.txt_City.text length]==0){
        return @"All Fields Are Mandatory";
    }
    if (![self validateEmailWithString:self.txt_EmailIId.text]){
        return @"Enter Valid Email Address";
    }
    if (!isCheck){
        return @"Click on Checkbox that you agree terms and Conditions";
    }
    return @"";
}
- (IBAction)btn_SubmitPress:(id)sender {
    
    NSString *validString = [self checkAllFilds];
    if ([validString isEqualToString:@""]){
        [self subMitPress];
    }else{
        [self showToastsWithTitle:validString];
    }
  
    
}
-(void)subMitPress{
    if([[self.requestCallHolidayPackage.strPackageSubType lowercaseString] isEqualToString:@"fit fixed"]|| [[self.requestCallHolidayPackage.strPackageSubType lowercaseString] isEqualToString:@"git"])
    {
        if([[self.requestCallHolidayPackage.strPackageType lowercaseString] isEqualToString:@"international"])
        {
            packageType = @"INT Holiday GIT";
            
            requestType = @"International";
            
            subProductType = @"GIT";
            
        }else{
            packageType = @"DOM Holiday GIT";
            
            requestType = @"Domestic";
            
            subProductType = @"GIT";
            
        }
    }else{
        if([[self.requestCallHolidayPackage.strPackageType lowercaseString] isEqualToString:@"international"]){
            packageType = @"INT Holiday FIT";
            
            requestType = @"International";
            
            subProductType = @"FIT";
        } else{
            packageType = @"DOM Holiday FIT";
            
            requestType = @"Domestic";
            
            subProductType = @"FIT";
        }
    }
    
    NSString  *urlString  = KUrlHolidayCreateLead;
    
    NSString *requestString =  [NSString stringWithFormat:@"%@?first_name=%@&last_name=%@&email=%@&mobile=%@&package_id=%@&request_type=%@&sub_product_type=%@&opp_summary=%@&city=%@&from_city=%@",urlString,self.txt_FirstName.text,self.txt_LastName.text,self.txt_EmailIId.text,self.txt_PhoneNumber.text,self.requestCallHolidayPackage.strPackageId,requestType,subProductType,self.txt_Summery.text,self.txt_City.text,self.txt_DepartureCity.text];
    
    [self netRequestCallBackForm]; // 09-04-2018
    
    [self submitRequestWithURL:requestString];

}

@end
