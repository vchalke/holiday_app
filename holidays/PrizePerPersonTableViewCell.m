//
//  PrizePerPersonTableViewCell.m
//  holidays
//
//  Created by Kush_Team on 24/06/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "PrizePerPersonTableViewCell.h"

@implementation PrizePerPersonTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.range_Sliders.delegate = self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setupTableViewCell{
    [self applyRangeSliderParameters];
    minPrize = ([self.filterPrizeDict objectForKey:filterMinBudget]) ? [NSString stringWithFormat:@"%@",[self.filterPrizeDict objectForKey:filterMinBudget]] : @"";
    maxPrize = ([self.filterPrizeDict objectForKey:filterMaxBudget]) ? [NSString stringWithFormat:@"%@",[self.filterPrizeDict objectForKey:filterMaxBudget]] : @"";
    if ([minPrize intValue]!=0){
       self.range_Sliders.selectedMinimum = [minPrize intValue];
    }
    if ([maxPrize intValue]!=0){
       self.range_Sliders.selectedMaximum = [maxPrize intValue];
    }
    [self setRangeSliderTextStart:self.range_Sliders.selectedMinimum withEndValeu:self.range_Sliders.selectedMaximum];
    
}
#pragma mark - Did Load Methods
-(NSArray*)getLowToHighPackagePriseArray{
    NSArray* filteredArray = [self.holidayArrayFilter sortedArrayUsingComparator:^NSComparisonResult(Holiday* obj1, Holiday* obj2) {
        if (obj1.packagePrise > obj2.packagePrise) {
           return (NSComparisonResult)NSOrderedDescending;
        }
        if (obj1.packagePrise < obj2.packagePrise) {
           return (NSComparisonResult)NSOrderedAscending;
        }
        return (NSComparisonResult)NSOrderedSame;
    }];
    return filteredArray;
}
-(void)applyRangeSliderParameters{
    NSArray *packagePriseWiseArray = [self getLowToHighPackagePriseArray];
    Holiday *firstObj = [packagePriseWiseArray firstObject];
    Holiday *lastObj = [packagePriseWiseArray lastObject];
    self.range_Sliders.minValue = firstObj.packagePrise;
    self.range_Sliders.maxValue = lastObj.packagePrise;
    self.range_Sliders.selectedMinimum = firstObj.packagePrise;
    self.range_Sliders.selectedMaximum = lastObj .packagePrise;
    [self setRangeSliderTextStart:firstObj.packagePrise withEndValeu:lastObj .packagePrise];
}

#pragma mark - TTRangeSlider Delegate

-(void)rangeSlider:(TTRangeSlider *)sender didChangeSelectedMinimumValue:(float)selectedMinimum andMaximumValue:(float)selectedMaximum{
    NSLog(@"minValue %0.2f",sender.minValue);
    NSLog(@"maxValue %0.2f",sender.maxValue);
    NSLog(@"selectedMinimum %0.2f",sender.selectedMinimum);
    NSLog(@"selectedMaximum %0.2f",sender.selectedMaximum);
    [self.filterPrizeDict setObject:[NSString stringWithFormat:@"%0.2f",sender.selectedMinimum] forKey:filterMinBudget];
    [self.filterPrizeDict setObject:[NSString stringWithFormat:@"%0.2f",sender.selectedMaximum] forKey:filterMaxBudget];
    [self setRangeSliderTextStart:sender.selectedMinimum withEndValeu:sender.selectedMaximum];
}
- (void)didEndTouchesInRangeSlider:(TTRangeSlider *)sender{
    NSLog(@"EndTouchesInRangeSlider selectedMinimum %0.2f",sender.selectedMinimum);
    NSLog(@"EndTouchesInRangeSlider selectedMaximum %0.2f",sender.selectedMaximum);
    
}
-(void)setRangeSliderTextStart:(float)startVal withEndValeu:(float)endVal{
    NSString *firstSTr = [self getNumberFormatterString:startVal];
    NSString *secondSTr = [self getNumberFormatterString:endVal];
    self.lbl_range.text = [NSString stringWithFormat:@"%@ - %@",firstSTr,secondSTr];
}
@end
