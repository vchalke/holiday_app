//
//  ForexCommunicationManager.swift
//  holidays
//
//  Created by ketan on 28/11/17.
//  Copyright © 2017 Mobicule. All rights reserved.
//

import UIKit

class ForexCommunicationManager
{
   private init(){}
    
   static let sharedInstance : ForexCommunicationManager = ForexCommunicationManager()
    
    func execTask(pathParam:NSString, queryParam:NSString ,requestType:NSString, jsonDict:NSDictionary, taskCallback: @escaping (Bool,
        AnyObject?) -> ())
    {
        
        if AppUtility.checkNetConnection()
        {
        
        
        let session = URLSession(configuration: URLSessionConfiguration.default)
        
       var urlComponents = URLComponents(string: kbaseURLForAstra)!  //https://services-uatastra.thomascook.in
        // var urlComponents = URLComponents(string: "https://services-uatastra.thomascook.in")!
        //https://services-uatastra.thomascook.in/tcForexRS/quote/buy/delivery/815
        if pathParam != ""
        {
            urlComponents.path = pathParam as String
        }
        
        if queryParam != ""
        {
            urlComponents.query = queryParam as String
 
        }

        let url = urlComponents.url;
        
        var request = URLRequest(url:url!)
        
         print("\(url!)")
        
        
        if jsonDict.count != 0
        {
          //  let data:NSData! =  NSKeyedArchiver.archivedData(withRootObject: jsonDict) as NSData
            do
            {

            let jsonData = try JSONSerialization.data(withJSONObject: (jsonDict as! [String : Any]), options: .prettyPrinted)
                
                let jsonString : NSString =    NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String as String as NSString
                
                print("Json Request String :\(jsonString) ")

                
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")

            request.httpBody = jsonData as Data
                
            }
            catch let error as NSError
            {
                print(error)
            }
        }

        

        let tokenId  : NSString = UserDefaults.standard.value(forKey: "userDefaultTokenId") as? NSString ?? "";
        let requestId : NSString = UserDefaults.standard.value(forKey: "userDefaultRequestId") as? NSString ?? "";
        
        print("tockenId: = \(tokenId)")
        print("requestId: = \(requestId)")
        
        request.addValue(requestId as String, forHTTPHeaderField: "requestId")
        request.addValue(tokenId as String, forHTTPHeaderField: "sessionId")
        
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")

        if requestType == "get"
        {
            request.httpMethod = "GET"
        }
        else
        {
            request.httpMethod = "POST"
        }

        
        let task = session.dataTask(with: request, completionHandler: {(data, response, error) -> Void in
            
            print("Response : ----- \(String(describing: response))")
            
           // print("Error :- \(error!)")
            
            if error != nil
            {
                taskCallback(false, nil)
                return
            }
            
            if let data = data
            {
               
                let json = try? JSONSerialization.jsonObject(with: data, options: [])
                if let response = response as? HTTPURLResponse , 200...299 ~= response.statusCode
                {
                    print(String(data: data, encoding: .utf8) ?? "not possible")
                    
                    if String(data: data, encoding: .utf8) != nil
                    {
                        taskCallback(true, json as AnyObject?)
                    }
                }
                else
                {
                    taskCallback(false, json as AnyObject?)
                }
            }else
            {
                taskCallback(false, nil)
                return
            }
        })
        task.resume()
            
        }else
        {
            taskCallback(false, nil)
            return
        }
    }
    func execTaskWithAlamofire(pathParam:NSString, queryParam:NSString ,requestType:NSString, jsonDict:NSDictionary, taskCallback: @escaping (Bool,
        AnyObject?) -> ())
    {
     
        if AppUtility.checkNetConnection()
        {
        
        let session = URLSession(configuration: URLSessionConfiguration.default)
        
        var urlComponents = URLComponents(string: kbaseURLForAstra)!
        
        if pathParam != ""
        {
            urlComponents.path = pathParam as String
        }
        
        if queryParam != ""
        {
            urlComponents.query = queryParam as String
            
        }
        
        let url = urlComponents.url;
        
        let tokenId  : NSString = UserDefaults.standard.value(forKey: "userDefaultTokenId") as! NSString;
        let requestId : NSString = UserDefaults.standard.value(forKey: "userDefaultRequestId") as! NSString;
        
        print("sessionId: = \(tokenId)")
         print("requestId: = \(requestId)")
        
        let headerFielda:HTTPHeaders = ["Accept":"application/json","Content-Type":"application/json","requestId":requestId as String,"sessionId":tokenId as String]
        request(url!, method: .post, parameters: jsonDict  as? Parameters, encoding: JSONEncoding.default, headers: headerFielda).responseJSON { response in
            
            printLog("Request: \(String(describing: response.request))")   // original url request
            printLog("Response: \(String(describing: response.response))") // http url response
            printLog("Result: \(response.result)")                         // response serialization result
            
            if let json = response.result.value
            {
                printLog("JSON: \(json)") // serialized json response
                
              //  if let responseData:Dictionary<String,AnyObject> = json as? Dictionary<String,AnyObject>
              //  {
                    //  printLog(responseData["data"])
                    
                   // if let responseData:Dictionary<String,Any> = json as? Dictionary<String,Any>
                //{
                 // }
                    
                    taskCallback(true, json as AnyObject)
                              
                 //}
                

               // }
                
                if let data = response.data, let utf8Text = String(data: data, encoding: .utf8)
                {
                    printLog("Data: \(utf8Text)") // original server data as UTF8 string
                }
            }
            else
            {
                
                taskCallback(false,nil)
                
            }
        }
        
    }else
        {
            taskCallback(false,nil)
            return
        }
    }
    
    
}
