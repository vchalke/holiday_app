//
//  VisaPassInsurance.h
//  holidays
//
//  Created by Kush_Team on 17/07/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface VisaPassInsurance : NSObject
-(instancetype)initWithVisaPassInsuranceModel:(NSDictionary *)dictionary;
@property(nonatomic)NSInteger packageClassId;
@property(nonatomic,strong)NSString *insurance;
@property(nonatomic,strong)NSString *passport;
@property(nonatomic,strong)NSString *visa;
@property(nonatomic,strong)NSString *visaPasInsSrp;
@end

NS_ASSUME_NONNULL_END


