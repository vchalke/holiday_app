//
//  ReloadForexCardViewController.swift
//  holidays
//
//  Created by Parshwanath on 09/12/17.
//  Copyright © 2017 Mobicule. All rights reserved.
//

import UIKit

public let TravlePurposeArray:[String] = ["Personal","Studies","Bussiness"]

class ReloadForexCardViewController: ForexBaseViewController,UITextFieldDelegate {

    //MARK: - Outlets
    @IBOutlet var reloadForexCardView: UIView!
    @IBOutlet weak var cardNumberTextField: UITextField!
    
    @IBOutlet weak var textFieldYear: UITextField!
    @IBOutlet weak var textFieldMonth: UITextField!
    
    @IBOutlet weak var textFieldDate: UITextField!
    @IBOutlet weak var travelPurposeTextField: UITextField!
    
    var selectedDate : Date = Date.init()
    var previousController : String = ""
    
     //MARK: - Controller Methods
    override func viewDidLoad() {
        
        super.viewDidLoad()
        Bundle.main.loadNibNamed(RELOAD_FOREX_CARD_VC, owner: self, options: nil)
        super.addViewInBaseView(childView: reloadForexCardView)

        super.setUpHeaderLabel(labelHeaderNameText: "Reload Forex Card")
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ReloadForexCardViewController.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        
        KeyboardAvoiding.setAvoidingView(self.reloadForexCardView, withTriggerView: self.cardNumberTextField)
       // KeyboardAvoiding.setAvoidingView(self.reloadForexCardView, withTriggerView: self.)
        
        self.addDoneButtonOnKeyboard()
    }
    
    override func backButtonClicked(buttonView:UIButton )
    {
        if self.previousController == "SlideMenuViewController"
        {
            SlideNavigationController.sharedInstance().popToRootViewController(animated: true)
        }
        self.navigationController?.popViewController(animated: true)
    }

    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle       = UIBarStyle.default
        let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(ReloadForexCardViewController.doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.cardNumberTextField.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction()
    {
        self.cardNumberTextField.resignFirstResponder()
    }
    
    override func viewWillLayoutSubviews() {
        
        super.setSubViewFrame(childView: self.reloadForexCardView)
    }
    
    //MARK: - Text Field Delegates
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        switch textField{
            
        case self.travelPurposeTextField:
            self.displayTravelPurposeActionSheet()
            return false;
            
        default:
            print("no case found for")
        }
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        switch textField{
            
        case self.self.cardNumberTextField:
            let currentCharacterCount = self.cardNumberTextField.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            
            let newLength = currentCharacterCount + string.count - range.length
            return newLength <= 16
            
        default:
            print("no case found for")
        }
        
        return true
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
    }
    
    //MARK: - Button Click Event
    
    @IBAction func continueButtonClick(_ sender: UIButton) {
        
       if self.validateReloadCard()
       {
        
        
        let forexBuyPassangerVc: ReloadForexCardDetailsViewController = ReloadForexCardDetailsViewController(nibName: FOREX_BASE_VC, bundle: nil)
        
        let buyForexBo : BuyForexBO = BuyForexBO.init()
        let buyForexOptionViewDetails : BuyForexOptionsViewBO = BuyForexOptionsViewBO.init()
        buyForexOptionViewDetails.purposeOfTravel = (self.travelPurposeTextField.text ?? "")  as NSString
        
        buyForexOptionViewDetails.noOfTraveller = 1
        
        buyForexOptionViewDetails.travelDate = selectedDate as NSDate
        buyForexOptionViewDetails.cardNumber = self.cardNumberTextField.text ?? "0"
        buyForexBo.buyForexOptionViewDetails = buyForexOptionViewDetails
        forexBuyPassangerVc.buyForexBo = buyForexBo
        self.navigationController?.pushViewController(forexBuyPassangerVc, animated: true)
        }
        
    }
    
    @IBAction func onTravelDateBtnClick(_ sender: UIButton) {
        
        self.datePickerTapped()
        
    }
   
    //MARK: Action Sheet
    func displayTravelPurposeActionSheet()  {
        
        
        let passangerActionSheet:UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        passangerActionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:nil))
        
        
        
        for travlePurpose in TravlePurposeArray /*DeviationConstants.PASSANGER_NAMES*/
        {
            
            passangerActionSheet.addAction(UIAlertAction(title: travlePurpose, style: .default, handler: { (action: UIAlertAction!)in
                
                self.travelPurposeTextField.text = travlePurpose
               
                
            }))
            
        }
        
        self.present(passangerActionSheet, animated: true, completion: nil)
        
        
    }
    
    func datePickerTapped()
    {
        let currentDate = Date()
        var dateComponents = DateComponents()
        dateComponents.month = -3
        _ = Calendar.current.date(byAdding: dateComponents, to: currentDate)
        
        let datePicker = DatePickerDialog(textColor: .black,
                                          buttonColor: .black,
                                          font: UIFont.boldSystemFont(ofSize: 17),
                                          showCancelButton: true)
        datePicker.show("Date of Travel",
                        doneButtonTitle: "Done",
                        cancelButtonTitle: "Cancel",
                        //minimumDate: threeMonthAgo,
            //maximumDate: currentDate,
        datePickerMode: .date) { (date) in
            if let dt = date
            {
                let currentDate : Date = Date.init()
                
                
                let calendar = NSCalendar.current
                let date1 = calendar.startOfDay(for: dt)
                let date2 = calendar.startOfDay(for:currentDate)
                
                let components =  calendar.dateComponents([.day], from: date1, to: date2)
                
                if components.day! > -2 || components.day! < (-61)
                {
                    self.showAlert(message: "Dear Customer, since your travel date is immediate, we cannot proceed ahead with this transaction. Our forex expert will contact you for further assistance on the contact details shared with us. You can also call us on toll free no 1800 2099 100.")
                    self.textFieldDate.text = ""
                    self.textFieldMonth.text = ""
                    self.textFieldYear.text = ""
                }
                else
                {
                    let formatter = DateFormatter()
                    formatter.dateFormat = "MM/dd/yyyy"
                    self.selectedDate = dt
                    let year = calendar.component(.year, from: dt)
                    let month = calendar.component(.month, from: dt)
                    let day = calendar.component(.day, from: dt)
                    self.textFieldDate.text = "\(day)"
                    self.textFieldMonth.text = "\(month)"
                    self.textFieldYear.text = "\(year)"
                    
                }
                
            }
        }
    }
    @objc func dismissKeyboard()
    {
        view.endEditing(true)
    }

    
    func validateReloadCard() -> Bool {
        
        if !self.cardNumberTextField.hasText && (self.cardNumberTextField.text?.isEmpty)!
        {
            showAlert(message: "please enter card number")
            
            return false

        }else if !(self.cardNumberTextField.text?.first == "5") || (self.cardNumberTextField.text?.count)! < 16
        {
                showAlert(message: "please enter valid card number")
          
        }else if !self.travelPurposeTextField.hasText
        {
            showAlert(message: "please select travel purpose")
            return false
        }else if !self.textFieldDate.hasText
        {
            showAlert(message: "please select travel date")
            return false
        }
        return true
            
    }
   
    @IBAction func dateOptionalButtonPress(_ sender: UIButton) {
        //        self.blurView.alpha = 1
        //        self.dateView.alpha = 1
        let popupVC: PopUpViewController = PopUpViewController(nibName: "PopUpViewController", bundle: nil)
        popupVC.msg = TRAVEL_DATE_TOOL_TIP
        popupVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(popupVC, animated: false, completion: nil)
        
    }
    
    @IBAction func purposeOptionalButtonPress(_ sender: UIButton) {
        
        //        self.blurView.alpha = 1
        //        self.purposeView.alpha = 1
        let popupVC: PopUpViewController = PopUpViewController(nibName: "PopUpViewController", bundle: nil)
        popupVC.msg = PURPOSE_OF_TRAVEL_TOOL_TIP
        popupVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(popupVC, animated: false, completion: nil)
        
    }
    

}
