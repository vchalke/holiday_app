//
//  SRPCollectionCell.h
//  Thomoscook_Holiday
//
//  Created by Kush_Tech on 19/02/20.
//  Copyright © 2020 Kush_Tech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Holiday.h"
#import "WishListHolidayObject.h"
#import "CoreDataSingleton.h"
#import "NewMasterCollectionViewCell.h"
#import "WebUrlConstants.h"
NS_ASSUME_NONNULL_BEGIN
@protocol SRPCollectionCellDelegate <NSObject>
@optional
- (void)downButtonClick:(UICollectionViewCell *)cell;
- (void)setSelectedIndex:(UICollectionViewCell *)cell withTag:(NSInteger)tagNum;
- (void)likeDislikePressInCell:(UICollectionViewCell *)cell withFlag:(BOOL)flag;
- (void)offersButtonClick:(UICollectionViewCell *)cell withHoliday:(Holiday*)hObject;
@end
@interface SRPCollectionCell : NewMasterCollectionViewCell{
    NSArray *allViewArray;
    NSArray *allImgNamesArray;
    NSArray *allImgViewArray;
    NSArray *allButtonArray;
    NSInteger clickIndexOfOptions;
    int labelNumOFLines;
    NSInteger selectedTag;
}
@property (nonatomic, weak) id <SRPCollectionCellDelegate> srpDelgate;
@property (weak, nonatomic) IBOutlet UIImageView *tour_imgView;
@property (weak, nonatomic) IBOutlet UIView *hert_View;
@property (weak, nonatomic) IBOutlet UILabel *lbl_PackageName;
@property (weak, nonatomic) IBOutlet UILabel *lbl_dayCount;
@property (weak, nonatomic) IBOutlet UILabel *lbl_destination;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_lblDestination;
@property (weak, nonatomic) IBOutlet UITextView *txt_destination;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_txtDestination;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_lblStrikeOutPrize;
@property (weak, nonatomic) IBOutlet UIButton *btn_dropDown;
@property (weak, nonatomic) IBOutlet UIButton *btn_Offers;
@property (weak, nonatomic) IBOutlet UILabel *lbl_maxprize;
@property (weak, nonatomic) IBOutlet UILabel *lbl_minprize;
@property (weak, nonatomic) IBOutlet UIStackView *stack_view;

@property (weak, nonatomic) IBOutlet UIView *flightView;
@property (weak, nonatomic) IBOutlet UIView *hotelView;
@property (weak, nonatomic) IBOutlet UIView *sightSeeingView;
@property (weak, nonatomic) IBOutlet UIView *visaView;
@property (weak, nonatomic) IBOutlet UIView *transferView;
@property (weak, nonatomic) IBOutlet UIView *mealsView;

@property (weak, nonatomic) IBOutlet UIImageView *flightImgView;
@property (weak, nonatomic) IBOutlet UIImageView *hotelImgView;
@property (weak, nonatomic) IBOutlet UIImageView *sightSeeingImgView;
@property (weak, nonatomic) IBOutlet UIImageView *visaImgView;
@property (weak, nonatomic) IBOutlet UIImageView *transferImgView;
@property (weak, nonatomic) IBOutlet UIImageView *mealsImgView;

@property (weak, nonatomic) IBOutlet UIButton *flightButton;
@property (weak, nonatomic) IBOutlet UIButton *hotelButton;
@property (weak, nonatomic) IBOutlet UIButton *sightSeeingButton;
@property (weak, nonatomic) IBOutlet UIButton *visaButton;
@property (weak, nonatomic) IBOutlet UIButton *transferButton;
@property (weak, nonatomic) IBOutlet UIButton *mealsButton;
@property (weak, nonatomic) IBOutlet UIImageView *heart_imgView;
@property (weak, nonatomic) IBOutlet UIButton *btn_like;

@property (weak, nonatomic) IBOutlet UIWebView *web_views;

@property (nonatomic, strong) NSArray *siteArray;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_stckView;
@property (weak, nonatomic) IBOutlet UIView *baseInfoView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cnst_webViewHeight;

-(void)showHolidayPackage:(Holiday *)holidayObj withCurrentIndex:(NSInteger)index withTag:(NSInteger)tagNum;
@property (nonatomic ,strong)Holiday *holidayObjectInSrpCell;
@property (nonatomic ,strong)NSString *wishListImgString;
@property (nonatomic ,strong)NSMutableArray *flightCollection;
@property (nonatomic ,strong)NSMutableArray *hotelCollection;
@property (nonatomic ,strong)NSMutableArray *sightseenCollection;
@property (nonatomic ,strong)NSMutableArray *visaCollection;
@property (nonatomic ,strong)NSMutableArray *transferCollection;
@property (nonatomic ,strong)NSMutableArray *accombdationCollection;
@property (nonatomic ,strong)NSMutableArray *mealCollection;
@property (nonatomic)NSInteger indexNumber;
@property (nonatomic)NSInteger selectedIndexNumber;


@end

NS_ASSUME_NONNULL_END
