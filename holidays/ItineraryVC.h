//
//  ItineraryVC.h
//  holidays
//
//  Created by Pushpendra Singh on 19/08/15.
//  Copyright (c) 2015 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HolidayPackageDetail.h"
#import <MapKit/MapKit.h>
#import "Holiday.h"

@interface ItineraryVC : UIViewController<MKMapViewDelegate,UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIButton *btnSideArrow;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollNumber;
@property (weak, nonatomic) IBOutlet UIView *viewForDetailsInfo;
@property (weak, nonatomic) IBOutlet UIWebView *iternaryWebView;
@property (strong,nonatomic) HolidayPackageDetail *holidayPackageDetail;
@property (weak, nonatomic) IBOutlet UIImageView *iternaryImageView;
@property (weak, nonatomic) IBOutlet UILabel *txtIternary;
@property (weak, nonatomic) IBOutlet UILabel *txtNameCity;
@property (weak, nonatomic) IBOutlet MKMapView *iternaryMapView;
- (IBAction)onMapViewButtonClicked:(id)sender;
@property (nonatomic, retain) MKPolyline *routeLine; //your line
@property (nonatomic, retain) MKPolylineView *routeLineView; //overlay view
- (IBAction)onSideArrowClicked:(id)sender;





@end
