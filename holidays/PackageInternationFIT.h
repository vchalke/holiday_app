//
//  PackageInternationFIT.h
//  holidays
//
//  Created by ketan on 23/10/15.
//  Copyright © 2015 Mobicule. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PackageInternationFIT : NSObject
- (instancetype)initWithTravellerArray:(NSArray *)travellerArray;
-(id)calaculatePackageInternationalFIT;
@property (strong,nonatomic) NSArray *travellerArray;
@property (strong,nonatomic) NSString *travellerRegion;

@end
