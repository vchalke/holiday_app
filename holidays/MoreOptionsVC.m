//
//  MoreOptionsVC.m
//  holidays
//
//  Created by Kush_Team on 26/05/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "MoreOptionsVC.h"
#import "ProfileTblViewCell.h"
#import "ProfileHeaderViewCell.h"
#import "TopWebVC.h"
#import "CoreDataSingleton.h"
#import "NewLoginVC.h"

@interface MoreOptionsVC ()<BottomViewDelegaete,BottomStacksViewDelegaete,UITableViewDelegate,UITableViewDataSource,ProfileHeaderViewCellDelegaete>
{
    NSArray *imgArray;
    NSArray *titleArray;
}
@end

@implementation MoreOptionsVC

- (void)viewDidLoad {
    // Do any additional setup after loading the view from its nib.
//    BottomView *bottonview = [[[NSBundle mainBundle] loadNibNamed:@"BottomView" owner:self options:nil] firstObject];
//    bottonview.frame = CGRectMake(0, 0, self.botton_view.frame.size.width, self.botton_view.frame.size.height);
//    bottonview.bottomDelegate = self;
//    [self.botton_view addSubview:bottonview];
//    self.botton_view.bottomDelegate = self;
    self.navigationController.navigationBar.hidden = YES;
    self.botton_view.bottomStackDelegate = self;
    [self.botton_view buttonSelected:4];
    imgArray = [NSArray arrayWithObjects:@"MoreContactUS",@"forexBlue",@"MoreRateUS",@"MoreStore",@"MoreReferFriend", nil];
    titleArray = [NSArray arrayWithObjects:@"Contact Us",@"Buy Forex",@"Rate Us",@"Store Locator",@"Share The App", nil];
    
    [self.table_views registerNib:[UINib nibWithNibName:@"ProfileHeaderViewCell" bundle:nil] forCellReuseIdentifier:@"ProfileHeaderViewCell"];
    [self.table_views registerNib:[UINib nibWithNibName:@"ProfileTblViewCell" bundle:nil] forCellReuseIdentifier:@"ProfileTblViewCell"];
}
- (void)viewWillAppear:(BOOL)animated{
    
}- (void)viewDidAppear:(BOOL)animated{
    
}
#pragma mark - Table view data source and delegated
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [titleArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ProfileTblViewCell* infoCell = (ProfileTblViewCell*)[tableView dequeueReusableCellWithIdentifier:@"ProfileTblViewCell"];
    infoCell.img_Icons.image = [UIImage imageNamed:[imgArray objectAtIndex:indexPath.row]];
    infoCell.lbl_title.text = [titleArray objectAtIndex:indexPath.row];
    infoCell.const_firtsViewWidth.constant = infoCell.frame.size.width*0.5;
    infoCell.const_secondViewWidth.constant = 0;
    infoCell.view_secondOne.hidden = YES;
    [infoCell setConstraints:25.0];
    return infoCell;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    ProfileHeaderViewCell *newHeaderView = (ProfileHeaderViewCell*)[tableView dequeueReusableCellWithIdentifier:@"ProfileHeaderViewCell"];
    newHeaderView.headerDelegaet = self;
   
//    NSString *loginStatus = [[NSUserDefaults standardUserDefaults] valueForKey:kLoginStatus];
//    NSString *emailId = [[NSUserDefaults standardUserDefaults] valueForKey:kLoginEmailId];
//    NSString *fname = [[NSUserDefaults standardUserDefaults] valueForKey:kUserFirstName];
//    newHeaderView.lbl_userName.text = fname;
//    newHeaderView.lbl_emailId.text = ([loginStatus isEqualToString:kLoginSuccess]) ? emailId : @"Email-ID" ;
//    newHeaderView.btn_logout.hidden = !([loginStatus isEqualToString:kLoginSuccess]);
//    [newHeaderView setProfileImage:[[NSUserDefaults standardUserDefaults]objectForKey:kUserImageString]];
    
    [newHeaderView setUserProfileInfo];
    return newHeaderView;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return  50;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 120;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0){
        [self callForHolidayEnquiry];
    }else if (indexPath.row == 1){
        TopWebVC *controler = [[TopWebVC alloc] initWithNibName:@"TopWebVC" bundle:nil];
        controler.web_url_string = @"https://www.thomascook.in/foreign-exchange/buy-forex-online";
        controler.web_title = @"Buy Forex";
        [self.navigationController pushViewController:controler animated:YES];
    }else if (indexPath.row == 2){
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:@"https://apps.apple.com/in/app/thomas-cook-holiday-packages/id1111576845?action=write-review"] options:@{} completionHandler:nil];
    }else if (indexPath.row == 3){
        TopWebVC *controler = [[TopWebVC alloc] initWithNibName:@"TopWebVC" bundle:nil];
        controler.web_url_string = @"https://www.thomascook.in/store-locator";
        controler.web_title = @"Store Locator";
        [self.navigationController pushViewController:controler animated:YES];
    }else if (indexPath.row == 4){
        NSString *string = [NSString stringWithFormat:@"Thomas Cook - Holiday Packages by Thomas Cook India Ltd."];
        NSString *appStoreUrl = [NSString stringWithFormat:@"App Url : https://apps.apple.com/in/app/thomas-cook-holiday-packages/id1111576845"];
        NSArray *itemsToShare = @[string,appStoreUrl];
        UIActivityViewController *activityIndication = [[UIActivityViewController alloc]initWithActivityItems:itemsToShare applicationActivities:nil];
         [self presentViewController:activityIndication animated:YES completion:nil];
    }
}

#pragma mark - Bottom View Delegate
-(void)homeButtonClick{
     [self jumpToControllerfromViewNum:5 toViewNum:0];
}
- (void)bookingButtonClick{
    [self jumpToControllerfromViewNum:5 toViewNum:1];
}
-(void)profileButtonClick{
     [self jumpToControllerfromViewNum:5 toViewNum:2];
}
-(void)notificationButtonClick{
     [self jumpToControllerfromViewNum:5 toViewNum:3];
}
-(void)moreButtonClick{
    
}

#pragma mark - Header Table View Delegate
-(void)logOutButtonClicked{
    [self showButtonsInAlertWithTitle:@"Logout" msg:@"Are you sure you want to logout?" style:UIAlertControllerStyleAlert buttArray:[NSArray arrayWithObjects:@"Yes",@"No", nil] completion:^(NSString * strValue) {
        if([strValue isEqualToString:@"Yes"]){
            [self logOutPressed];
        }
    }];
}
-(void)logOutPressed{
        [FIRAnalytics logEventWithName:@"Sign_Out" parameters:@{kFIRParameterItemID:@"Sign Out"}];
        
        [[NetCoreInstallation sharedInstance] netCorePushLogout:^(NSInteger statusCode) {
            NSLog(@"User Logged out from netcore!!");
        }];
        [[CoreDataSingleton sharedInstance]deleteEntitiesWithName:@"CompareListHoliday"];
        [[CoreDataSingleton sharedInstance]deleteEntitiesWithName:@"Notification"];
        [[CoreDataSingleton sharedInstance]deleteEntitiesWithName:@"RecentViewHoliday"];
        [[CoreDataSingleton sharedInstance]deleteEntitiesWithName:@"WishListHoliday"];
        
        NSUserDefaults *LoginStatus=[NSUserDefaults standardUserDefaults];
        [LoginStatus setObject:kLoginFailed forKey:kLoginStatus];
        
      CoreUtility *coreObj=[CoreUtility sharedclassname];
        
        if (coreObj != nil && ![coreObj isEqual:[NSNull new]] && coreObj.strPhoneNumber != nil && ![coreObj.strPhoneNumber isEqual:[NSNull new]] && coreObj.strPhoneNumber.count > 0)
        {
            [coreObj.strPhoneNumber removeAllObjects];
            
        }
        
        [LoginStatus setBool:false forKey:@"isRegisterForPushNotification"];
        
        [LoginStatus setBool:false forKey:@"isCSSUserLoggedIn"];
        
        [LoginStatus setObject:nil forKey:@"mobicleNumber"];
        
        [LoginStatus setObject:@"" forKey:@"userDefaultTC-LoginMobileNumber"];
        
        [LoginStatus setObject:@"" forKey:@"Holidays_TextField_MobicleNumber"];
    
    [[NSUserDefaults standardUserDefaults]setValue:@"" forKey:@"mobicleNumber"];
    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"isCSSUserLoggedIn"];
    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"isRegisterForCSSPushNotification"];
    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"isCSSSubscribeForPushNotification"];
    
        
        //NSUserDefaults *userIdForGooglePlusSignIn=[NSUserDefaults standardUserDefaults];
        [LoginStatus removeObjectForKey:kLoginEmailId];
        [LoginStatus removeObjectForKey:kLoginPasswords];
    [LoginStatus removeObjectForKey:kLoginUserDetails];
        [LoginStatus removeObjectForKey:kPhoneNo];
        
       [[NSUserDefaults standardUserDefaults] synchronize];
        
        [FIRAnalytics logEventWithName:@"Home" parameters:@{kFIRParameterItemID : @"Home"}];
        
//        [[SlideNavigationController sharedInstance] popToRootViewControllerAnimated:NO];
        
//        [payloadList setObject:@"yes" forKey:@"s^HOME"];
        
        NewLoginVC *loginVC=[[NewLoginVC alloc]initWithNibName:@"NewLoginVC" bundle:nil];
        [self.navigationController pushViewController:loginVC animated:YES];
   
}
@end
