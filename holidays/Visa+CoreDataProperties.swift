//
//  Visa+CoreDataProperties.swift
//  holidays
//
//  Created by Saurav on 28/11/17.
//  Copyright © 2017 Mobicule. All rights reserved.
//

import Foundation
import CoreData


extension Visa {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Visa> {
        return NSFetchRequest<Visa>(entityName: "Visa")
    }

    @NSManaged public var bfNumber: String?
    @NSManaged public var bfNumber_passengerNumber_visaCountry: String?
    @NSManaged public var passengerNumber: String?
    @NSManaged public var visaCountry: String?
    @NSManaged public var visaRequestStatus: String?
    @NSManaged public var visaStatus: String?
    @NSManaged public var visaType: String?
    @NSManaged public var tourRelation: Tour?

}
