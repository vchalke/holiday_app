//
//  FlightsTableViewCell.m
//  holidays
//
//  Created by Kush_Tech on 03/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "FlightsTableViewCell.h"

@implementation FlightsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setFilghtsdataSelected:(FlightsColectObject*)flighModel{
    NSString *flightMessage = flighModel.flightMsg;
    NSAttributedString *attrStr = [[NSAttributedString alloc] initWithData:[flightMessage dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:nil error:nil];
    self.lbl_defaultMsg.text = [flighModel.airlineName isEqualToString:@""] ? attrStr.description : flighModel.airlineName;
    self.lbl_defaultSubTitle.text = [NSString stringWithFormat:@"%@ - %@",flighModel.arrivalCity,flighModel.departureCity];
    self.lbl_defaultDuration.text = [NSString stringWithFormat:@"Duration : %@",flighModel.flightDuration];
}
@end
