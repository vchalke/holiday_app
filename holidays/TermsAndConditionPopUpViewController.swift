//
//  TermsAndConditionPopUpViewController.swift
//  holidays
//
//  Created by Swapnil on 08/01/18.
//  Copyright © 2018 Mobicule. All rights reserved.
//

import UIKit

class TermsAndConditionPopUpViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func onClickOfCloseButton(_ sender: Any)
    {
        self.dismiss(animated: false, completion: nil)
    }
    
    func dismissView()
    {
        self.dismiss(animated: false, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
