//
//  AppDelegate.m
//  holidays
//
//  Created by vaibhav on 07/08/15.
//  Copyright (c) 2015 Mobicule. All rights reserved.
//

#import <CoreData/CoreData.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>

#import "AppDelegate.h"
#import "SlideMenuViewController.h"
#import "LandingPageViewController.h"
#import "IntroPageVC.h"
#import "BookNowViewController.h"
#import "DefaultHeaderRequestBuilder.h"
//#import "LoginViewPopUp.h"
#import "PackageListVC.h"
#import "WebViewController.h"
#import "OverviewVC.h"
#import "NotificationViewController.h"
#import "LoadingView.h"
#import "MobiculeUtilityManager.h"
#import "SSKeychain.h"
#import "Reachability.h"
#import "Firebase.h"
#import <FirebaseAppIndexing/FirebaseAppIndexing.h>
#import "SearchHolidaysVC.h"
#import "LoginViewController.h"
#import "Thomas_Cook_Holidays-Swift.h"
#import "ReviewPopUpViewController.h"
// Vijay Added
#import "LandingScreenVC.h"
#import "LandingScreenNewVC.h"
#import "PDPScreenVC.h"
#import "HolidayLandingScreenVC.h"
#import "CalenderScreenVC.h"
#import "NewLoginVC.h"
#import "NewSignUpVC.h"
#import <UserNotifications/UserNotifications.h>
#import <UserNotificationsUI/UserNotificationsUI.h>

#define KEYCHAIN_SERVICE @"Holiday_App_Keychain"
#define KEYCHAIN_ACCOUNT @"com.thomasCookHoliday.app"


#define SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

@import FirebaseInstanceID;
@import FirebaseAnalytics;
@import FirebaseAppIndexing;
@import MobileCoreServices;
@import FirebaseMessaging;

// f1wRWS-ldM4:APA91bFi6rQzXLLcrF8yQiydsBj9XC4kYEgLI5Fi64iiixE3vK1-hUdQ1PMUZxay9l5yc_QpQAmxCs8qTf-HxRNB9aLarmruNbIRvYhjeoecrqB83g6pkc41-dYTO6Cai8xwY-YG8Uer

@interface AppDelegate ()<NetCorePushTaskManagerDelegate>
{
//  LoginViewPopUp *loginViewPopUp;
    NSDictionary * dictionary ;
    LoadingView *activityLoadingView;
    NSString * mydeviceToken;
    LoginViewController *loginVC;
    BOOL isFromAppDidiFinishLaunching; // to handle net core registration on fresh app launch
    UIImageView *overlayImageView;
    
    BOOL isFromNotificationClick;
}

@end

@implementation AppDelegate

static NSString * const kClientID =
@"327645202876-3pmqts5pkaaqqvhh1gjh52k7sbuv468a.apps.googleusercontent.com";
//@"509311649159-v6mvsgn706f56j7or5h8jmrafl2i3gd7.apps.googleusercontent.com";
//cbcc220fb64b753ee8f5cc6e21d0d4a5
//60dec903ce10e27a212133ff7f446caa
//static NSString *kNetCore_AppID = @"60dec903ce10e27a212133ff7f446caa";
 NSString *kNetCore_UATAppID = @"cbcc220fb64b753ee8f5cc6e21d0d4a5";
 NSString *kNetCore_LiveAppID = @"d579e8df95b8f57b1761d93dc7fb9313";

//static NSString *kNetCore_AppID = @"cbcc220fb64b753ee8f5cc6e21d0d4a5";// UAT ID NETCORE
static NSString *kNetCore_AppID = @"d579e8df95b8f57b1761d93dc7fb9313";// LIVE ID NETCORE

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [AppUtility checkIsDeviceRooted];

    [FIRApp configure];
    
    CoreUtility *coreObj=[CoreUtility sharedclassname];
    coreObj.strPhoneNumber=[[NSMutableArray alloc]init];
   
    NSString *deviceUdid = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    [[NSUserDefaults standardUserDefaults] setObject:deviceUdid forKey:kUserDeviceUniqueID];
    [[NSUserDefaults standardUserDefaults] synchronize];
    DISABLE_LOGGER();
    //start static data
    
    [GIDSignIn sharedInstance].clientID = kClientID;
    
    [GIDSignIn sharedInstance].delegate = self;
    
    [[FBSDKApplicationDelegate sharedInstance] application:application didFinishLaunchingWithOptions:launchOptions];
    // Vijay Commented
//    LandingPageViewController * landingVC = [[LandingPageViewController alloc]initWithNibName:@"BaseViewController" bundle:nil];
    
    // Vijay Added
//    LandingScreenVC * landingVC = [[LandingScreenVC alloc]initWithNibName:@"LandingScreenVC" bundle:nil];
    
//    HolidayLandingScreenVC * landingVC = [[HolidayLandingScreenVC alloc]initWithNibName:@"HolidayLandingScreenVC" bundle:nil];

//    NewLoginVC * landingVC = [[NewLoginVC alloc]initWithNibName:@"NewLoginVC" bundle:nil];
    
    SlideMenuViewController *slideMenu = [[SlideMenuViewController alloc]initWithNibName:@"BaseViewController" bundle:nil];
    
    UILocalNotification *localNotif = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    if (localNotif)
    {
        NSDictionary * notificationDictionary = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
        
        if(DEBUG_ENABLED)
        {
            debug(@"\n AppDelegate notificationDictionary: %@ ",notificationDictionary );
        }
        
        NSString *messageStringAppDelegate = [NSString stringWithFormat:@"%@",[notificationDictionary objectForKey:@"message"]];
        
        if(DEBUG_ENABLED)
        {
            debug(@"\n messageStringAppDelegate: %@ ",messageStringAppDelegate );
        }
        
       NSString * dictionaryString = (NSString * ) [notificationDictionary objectForKey:@"message"];
        
       dictionary = (NSDictionary *) [JsonSerealizer dictonaryOfJsonFromJsonData:dictionaryString];
        
        
        if (dictionary != nil && dictionary.count > 0 )
        {
        
            [self SaveNotificationIntoDatabase:dictionary];
        
        
            NSLog(@"notificationType : %@", dictionary);
            
            NSLog(@" Notification Data :  %@",localNotif);
        
        }
        else
        {
            NSLog(@" Notification Data  nil:  %@",localNotif);
        }
        
//         landingVC.notificationDict = dictionary;
    
     }
    else
    {
//            landingVC.notificationDict = nil;
    }
    /*
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"FirstTime"] length]==0)
    {
        
        IntroPageVC *inVC=[[IntroPageVC alloc]initWithNibName:@"IntroPageVC" bundle:nil];
        _window.rootViewController=inVC;
        
    }
    
    else
    {
        SlideNavigationController *slideNavigationVC = [[SlideNavigationController alloc]initWithRootViewController:landingVC];
        
        [SlideNavigationController sharedInstance].leftMenu = slideMenu;
        
        self.window.rootViewController = slideNavigationVC;
        

    }
   */
    dispatch_async(dispatch_get_main_queue(), ^{
        [NotificationController setUUID];
    });
    
    //CSS APP
//    [NotificationController setUUID];
    
    //OLD Notification Code
    isFromAppDidiFinishLaunching = true;
     [self handleRemoteNotification];
    NSString *loginStatus = [[NSUserDefaults standardUserDefaults] valueForKey:kLoginStatus];
    NSString *transactionId = [[NSUserDefaults standardUserDefaults] valueForKey:kCurrentTransactionId];
    if ([transactionId length]>0){
        BookingConfirmOne *booking=[[BookingConfirmOne alloc]initWithNibName:@"BookingConfirmOne" bundle:nil];
        self.window.rootViewController = [[UINavigationController alloc] initWithRootViewController: booking];
    }else if ([loginStatus isEqualToString:kLoginSuccess]){
        
        LandingScreenVC *landingscrenVC = [[LandingScreenVC alloc]initWithNibName:@"LandingScreenVC" bundle:nil];
//        LandingScreenNewVC *landingscrenVC = [[LandingScreenNewVC alloc]initWithNibName:@"LandingScreenNewVC" bundle:nil];
        self.window.rootViewController = [[UINavigationController alloc] initWithRootViewController: landingscrenVC];
    }else{
        NewLoginVC *loginVC = [[NewLoginVC alloc]initWithNibName:@"NewLoginVC" bundle:nil];
        self.window.rootViewController = [[UINavigationController alloc] initWithRootViewController: loginVC];
    }
    [self.window makeKeyAndVisible];
    
    
    NSUInteger iTuneAppleId = 1111576845;
    
    [[FIRAppIndexing sharedInstance] registerApp:iTuneAppleId];
    
    
//========================= NetCore PushNotification Code =====================================
    
    [[NetCoreSharedManager sharedInstance] setUpApplicationId : kNetCore_AppID];
    [[NetCoreSharedManager sharedInstance] setUpAppGroup:@"group.com.thomasCookHoliday.app"];
    [[NetCoreSharedManager sharedInstance] handleApplicationLaunchEvent:launchOptions forApplicationId:kNetCore_AppID];
    [NetCorePushTaskManager sharedInstance].delegate = (id)self;
    // iOS 10 and above //uncommented on 28-02-2018
    
  /*  UNAuthorizationOptions options = UNAuthorizationOptionAlert + UNAuthorizationOptionSound + UNAuthorizationOptionBadge;

    UNUserNotificationCenter.currentNotificationCenter.delegate = self;
    [UNUserNotificationCenter.currentNotificationCenter requestAuthorizationWithOptions:options completionHandler:^(BOOL granted, NSError * _Nullable error) {
        if (!granted) {
            NSLog(@"Something went wrong");
        }
        else
        {
            [[UIApplication sharedApplication] registerForRemoteNotifications];
        }
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }];
    
    //28-02-2018
    UIUserNotificationType allNotificationTypes = (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
    
    UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
    
    [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    [[UIApplication sharedApplication] registerForRemoteNotifications];*/ //---- 09-04-2018
    
    NSLog( @"launchOptions :%@",launchOptions);
    
//    if (launchOptions != nil)
//    {
//         NSLog( @"launchOptions != nil" );
//        [[NetCorePushTaskManager sharedInstance] handelApplicationLaunchEvent:launchOptions];
//    }
    
        NSArray *arrofNotifications = [[NetCoreSharedManager sharedInstance] getNotifications];
    NSLog(@"arr of notification %@",arrofNotifications);
    
    return YES;

}

//========================/* Notification code For iOS 10 */=============================================

/*-(void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler
{
    
    //Called when a notification is delivered to a foreground app.
    [NotificationController updateAplicationIconBadgeNumber];
    
    NSLog( @"for handling push in foreground" );
    NSLog(@"Userinfo %@",notification.request.content.userInfo);
    
    completionHandler(UNNotificationPresentationOptionAlert);
    completionHandler(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge);
    
    //28-02-2018
  
    
}

-(void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)())completionHandler
{
    
    //Called to let your app know which action was selected by the user for a given notification.
    [NotificationController updateAplicationIconBadgeNumber];
    
    
   [NotificationController moveToRespectiveViewControllerWithNotificationType:@"Deviation" bfNumber:@"G180000126" webViewRedirectionURL:@"" message:@""];
    
    completionHandler ();
    
}*/
+ (BOOL)allowsAnyHTTPSCertificateForHost:(NSString *)host
{
   return YES;
}
-(void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler
{
    
    //Called when a notification is delivered to a foreground app.
    dispatch_async(dispatch_get_main_queue(), ^{
    [NotificationController updateAplicationIconBadgeNumber];
    });
    
    NSLog( @"for handling push in foreground" );
    NSLog(@"Userinfo %@",notification.request.content.userInfo);
    
    completionHandler(UNNotificationPresentationOptionAlert);
    
    NSString * dictionaryString = (NSString * ) [notification.request.content.userInfo objectForKey:@"message"];
    
    dictionary = (NSDictionary *) [JsonSerealizer dictonaryOfJsonFromJsonData:dictionaryString];
    
    if(DEBUG_ENABLED)
    {
        debugMethodStart(@"------------------------------");
        debug(@"didReceiveRemoteNotification");
        debugMethodEnd(@"------------------------------");
    }
    
    if(DEBUG_ENABLED)
    {
        debug(@"\n Received Notification userInfo: %@ ", notification.request.content.userInfo );
        debug(@"\n Received Notification aps: %@ ", [notification.request.content.userInfo valueForKey:@"aps"]);
        debug(@"\n Received Notification message: %@ ", [notification.request.content.userInfo valueForKey:@"message"]);
       
    }
    
    if (dictionary != nil)
    {
        
        [self SaveNotificationIntoDatabase:dictionary];
        
    }

    UILocalNotification *notif = [[UILocalNotification alloc] init];
    notif.applicationIconBadgeNumber = dictionary.count;
    
    if([UIApplication sharedApplication].applicationState == UIApplicationStateActive)
    {
        NSLog(@"active");
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NotificationInActiveState" object:self];
    }
    
     completionHandler(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge);
    
    //28-02-2018
    [[NetCorePushTaskManager sharedInstance] userNotificationWillPresentNotification:notification];

}


-(void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)())completionHandler

{
    
    //Called to let your app know which action was selected by the user for a given notification.
    dispatch_async(dispatch_get_main_queue(), ^{
    [NotificationController updateAplicationIconBadgeNumber];
    });
    
    
    NSLog(@"Userinfo %@",response.notification.request.content.userInfo);
    
    NSLog(@"did recieve notification for userInfo dictionary %@", response.notification.request.content.userInfo);

    
    NSString * dictionaryString = (NSString * ) [response.notification.request.content.userInfo objectForKey:@"message"];

    dictionary = (NSDictionary *) [JsonSerealizer dictonaryOfJsonFromJsonData:dictionaryString];
    
    NSLog(@"notification dictionary %@",dictionary);
    
    if(DEBUG_ENABLED)
    {
        debugMethodStart(@"------------------------------");
        debug(@"didReceiveRemoteNotification");
        debugMethodEnd(@"------------------------------");
    }
    
    if(DEBUG_ENABLED)
    {
        debug(@"\n Received Notification userInfo: %@ ", response.notification.request.content.userInfo );
        debug(@"\n Received Notification aps: %@ ", [response.notification.request.content.userInfo valueForKey:@"aps"]);
        debug(@"\n Received Notification message: %@ ", [response.notification.request.content.userInfo valueForKey:@"message"]);

    }
    
    if (dictionary != nil)
    {
        
        [self SaveNotificationIntoDatabase:dictionary];
        
    }
    
        if(DEBUG_ENABLED)
        {
            NSLog(@"Inactive");
            debug(@"\n Received Notification dictionary: %@ ", dictionary );
        }
        
       // NotificationViewController *notificationVC = [[NotificationViewController alloc]initWithNibName:@"BaseViewController" bundle:nil];
        
       // [[SlideNavigationController sharedInstance] pushViewController:notificationVC animated:YES];
    
    // NetCore APNS Code
    NSLog( @"Handle push from background or closed" );
    // if you set a member variable in didReceiveRemoteNotification, you  will know if this is from closed or background
    NSLog(@"%@", response.notification.request.content.userInfo);
    
    //28-02-2018
    [[NetCorePushTaskManager sharedInstance] userNotificationdidReceiveNotificationResponse:response];
    
    //completionHandler ();
    
}

#pragma mark - handleRemoteNotification
//OLD Notification Code
- (void)handleRemoteNotification
{
 //   BOOL registeredWithCMSServer = [[NSUserDefaults standardUserDefaults] boolForKey:@"tokenSubmitToMobiculeServer"];
    [self registerForRemoteNotifications];
}


-(void) registerWithCMSServer
{
    if ([SSKeychain passwordForService:KEYCHAIN_SERVICE account:KEYCHAIN_ACCOUNT] == nil)
    {
        
        NSString *deviceId = [MobiculeUtilityManager getApplicationUUID];
        [SSKeychain setPassword:deviceId forService:KEYCHAIN_SERVICE account:KEYCHAIN_ACCOUNT];
        
    }

    
    NSMutableDictionary *dataDictionary = [[NSMutableDictionary alloc]init];
    [dataDictionary setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"DeviceToken"] forKey:@"deviceid"];
    [dataDictionary setValue:@"ios" forKey:@"platform"];
    [dataDictionary setValue:[[UIDevice currentDevice] systemVersion] forKey:@"osVersion"];
    [dataDictionary setValue: [SSKeychain passwordForService:KEYCHAIN_SERVICE account:KEYCHAIN_ACCOUNT] forKey:@"macAddress"];
    [dataDictionary setValue:[[UIDevice currentDevice] model] forKey:@"model"];
    [dataDictionary setValue:@"holiday" forKey:@"appType"];

    
    CoreUtility * coreObj=[CoreUtility sharedclassname];
    
    if(coreObj.strPhoneNumber.count>0)
    {
           [dataDictionary setValue:[coreObj.strPhoneNumber objectAtIndex:0] forKey:@"mobileNo"];
    }
    else
    {
           [dataDictionary setValue:@"" forKey:@"mobileNo"];
    }

    //"mobileNo":"9004942341"

    
    [self performSelectorInBackground:@selector(submitDeviceTokenToCMSServer:) withObject:dataDictionary];
    
}

-(void) registerForRemoteNotifications
{
    
    if(DEBUG_ENABLED)
    {
        debugMethodStart(@" Registering With APNS for Remote Notification");
    }
    
    //NEW Notification Code
  /* [[NSNotificationCenter defaultCenter] addObserver:self
                                            selector:@selector(tokenRefreshCallBack:)
                                                name:kFIRInstanceIDTokenRefreshNotification
                                               object:nil];
    
    UIUserNotificationType allNotificationTypes = (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
    
    UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
    
    [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    
    [[UIApplication sharedApplication] registerForRemoteNotifications];
    */
    
    
     //OLD Notification Code
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(isRegisteredForRemoteNotifications)])
    {
        // iOS 10.0 Notifications
        
        NSLog(@"isRegisteredForRemoteNotifications");
        
        if(SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"10.0"))
        {
            UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
            center.delegate = self;
            [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
                if( !error )
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[UIApplication sharedApplication] registerForRemoteNotifications];
                    });
                    
                }
            }];
            
            if(DEBUG_ENABLED)
            {
                debugMethodStart(@"Registering With APNS for Remote Notification for ios 10 and greater.");
            }

        }
        else if (SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"8.0"))
        {
            [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
            
            [[UIApplication sharedApplication] registerForRemoteNotifications];
            
            if(DEBUG_ENABLED)
            {
                debugMethodStart(@"Registering With APNS for Remote Notification for ios 8 and greater.");
            }

        }
    
    }
    else
    {
        // iOS < 8 Notifications
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
        (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound)];
    }
    
    
    
    
}

//NEW Notification Code
/*- (void)tokenRefreshCallBack:(NSNotification *)notification {
    
    // Note that this callback will be fired everytime a new token is generated, including the first
    // time. So if you need to retrieve the token as soon as it is available this is where that
    // should be done.
    
    NSString *refreshedToken = [[FIRInstanceID instanceID] token];
    
    NSLog(@"InstanceID token: %@", refreshedToken);
    
    // Connect to FCM since connection may have failed when attempted before having a token.
    [self connectToFcm];
    
    // TODO: If necessary send token to application server.
}*/
 

- (void)connectToFcm {
    
    [[FIRMessaging messaging] connectWithCompletion:^(NSError * _Nullable error) {
        
        if (error != nil) {
            NSLog(@"Unable to connect to FCM. %@", error);
        } else {
            NSLog(@"Connected to FCM.");
        }
        
    }];
 
    
    /*
     if ([SSKeychain passwordForService:KEYCHAIN_SERVICE account:KEYCHAIN_ACCOUNT] == nil)
     {
     
     NSString *deviceId = [MobiculeUtilityManager getApplicationUUID];
     [SSKeychain setPassword:deviceId forService:KEYCHAIN_SERVICE account:KEYCHAIN_ACCOUNT];
     
     }
     
     
     NSMutableDictionary *dataDictionary = [[NSMutableDictionary alloc]init];
     [dataDictionary setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"DeviceToken"] forKey:@"deviceid"];
     [dataDictionary setValue:@"ios" forKey:@"platform"];
     [dataDictionary setValue:[[UIDevice currentDevice] systemVersion] forKey:@"osVersion"];
     [dataDictionary setValue: [SSKeychain passwordForService:KEYCHAIN_SERVICE account:KEYCHAIN_ACCOUNT] forKey:@"macAddress"];
     [dataDictionary setValue:[[UIDevice currentDevice] model] forKey:@"model"];
     
     
     
     [self performSelectorInBackground:@selector(submitDeviceTokenToCMSServer:) withObject:dataDictionary];
     */
}

//NEW Notification Code
/*-(void) application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    [[FIRInstanceID instanceID] setAPNSToken:deviceToken type:FIRInstanceIDAPNSTokenTypeUnknown];
}*/


//OLD Notification Code
- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    
    NSLog(@"didRegisterForRemoteNotificationsWithDeviceToken");
    if(DEBUG_ENABLED)
    {
        debug(@"My token is: %@", deviceToken);
    }
    
    
    if ([SSKeychain passwordForService:KEYCHAIN_SERVICE account:KEYCHAIN_ACCOUNT] == nil)
        {
            
        NSString *deviceId = [MobiculeUtilityManager getApplicationUUID];
        [SSKeychain setPassword:deviceId forService:KEYCHAIN_SERVICE account:KEYCHAIN_ACCOUNT];
    
        }
    
    mydeviceToken = [[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    
    mydeviceToken = [mydeviceToken stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    [NotificationController registerNotificationForCSS:application didRegisterForRemoteNotificationsWithDeviceToken:mydeviceToken]; // --- CSS Notification Registration
    
    
    [[NSUserDefaults standardUserDefaults] setObject:mydeviceToken forKey:@"DeviceToken"];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"RegisteredWithAPNS"];
    
    //[self registerWithCMSServer];
    
    NSMutableDictionary *dataDictionary = [[NSMutableDictionary alloc]init];
    [dataDictionary setValue:mydeviceToken forKey:@"deviceid"];
    [dataDictionary setValue:@"ios" forKey:@"platform"];
    [dataDictionary setValue:[[UIDevice currentDevice] systemVersion] forKey:@"osVersion"];
    [dataDictionary setValue:[SSKeychain passwordForService:KEYCHAIN_SERVICE account:KEYCHAIN_ACCOUNT] forKey:@"macAddress"];
    [dataDictionary setValue:@"holiday" forKey:@"appType"];
    
    [dataDictionary setValue:[[UIDevice currentDevice] model] forKey:@"model"];
    
    NSLog(@"dataDictionary : %@",dataDictionary);
    
    [self performSelectorInBackground:@selector(submitDeviceTokenToCMSServer:) withObject:dataDictionary];
    
    //NetCore PushNotificationCode
    
    [[NSUserDefaults standardUserDefaults] setObject:mydeviceToken forKey:@"APNSDeviceToken"];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isRegisterForPushNotification"];
    
    // Register device token with third party SDK as per their document//28-02-2018
  //  [[NetCoreSharedManager sharedInstance] setDeviceToken:deviceToken];
    
    
    //strEmail = your application identity
    
    CoreUtility *coreobj=[CoreUtility sharedclassname];
     NSLog(@"coreobj.strPhoneNumber.count:%ld",(long)coreobj.strPhoneNumber.count);
    
    if (isFromAppDidiFinishLaunching)
    {
        isFromAppDidiFinishLaunching = false;
        
         NSString * mobileNumber = [[NSUserDefaults standardUserDefaults] valueForKey:@"mobicleNumber"];
        
         NSString * holiday_txt_mobileNumber = [[NSUserDefaults standardUserDefaults] valueForKey:@"Holidays_TextField_MobicleNumber"];
        
        NSString * tc_MobileNumber = [[NSUserDefaults standardUserDefaults] valueForKey:@"userDefaultTC-LoginMobileNumber"];
        
        if  (tc_MobileNumber != nil && ![tc_MobileNumber isEqual:[NSNull new]] && ![tc_MobileNumber isEqualToString:@""])
        {
            
            [[NetCoreInstallation sharedInstance] netCorePushRegisteration:tc_MobileNumber withDeviceToken:deviceToken Block:^(NSInteger statusCode) {
                NSLog(@"statusCode:%ld",(long)statusCode);

            }];
            
        }else if (holiday_txt_mobileNumber != nil && ![holiday_txt_mobileNumber isEqual:[NSNull new]] && ![holiday_txt_mobileNumber isEqualToString:@""])
        {
            [[NetCoreInstallation sharedInstance] netCorePushRegisteration:holiday_txt_mobileNumber withDeviceToken:deviceToken Block:^(NSInteger statusCode) {
                NSLog(@"statusCode:%ld",(long)statusCode);
                
            }];
            
        }else if (mobileNumber != nil && ![mobileNumber isEqual:[NSNull new]] && ![mobileNumber isEqualToString:@""])
        {
            
            [[NetCoreInstallation sharedInstance] netCorePushRegisteration:mobileNumber withDeviceToken:deviceToken Block:^(NSInteger statusCode) {
                NSLog(@"statusCode:%ld",(long)statusCode);
                
            }];
            
        }
        else
        {
            [[NetCoreInstallation sharedInstance] netCorePushRegisteration:@"" withDeviceToken:deviceToken Block:^(NSInteger statusCode) {
                NSLog(@"statusCode:%ld",(long)statusCode);
                
            }];
            
        }
        
    }
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}



#pragma mark -fail Notification

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error NS_AVAILABLE_IOS(3_0)
{

    NSLog(@"FailToRegisterForRemoteNotificationsWithError: %@",[error localizedDescription]);
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isRegisterForPushNotification"];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
   
    
    
    /*func application ( _ application : UIApplication,
                      didFailToRegisterForRemoteNotificationsWithError error : Error ) {
        // manage notification token failure process as per third party SDK as per their
        document
    }*/
    
}

-(void)submitDeviceTokenToCMSServer:(NSDictionary *) dataDict
{
    if(DEBUG_ENABLED)
    {
        debug(@"subscribeDict : %@", dataDict);
    }
 
    
    NetworkHelper *helper = [NetworkHelper sharedHelper];
    
    NSString *strResponse = [helper getDataFromServerForType:@"user" entity:@"notification" action:@"register" andUserJson:dataDict];
    
    
    
    NSString *strStatus = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"status"];
    
    
    if ([strStatus caseInsensitiveCompare:kStatusSuccess] == NSOrderedSame)
    {
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"tokenSubmitToMobiculeServer"];
           [[NSUserDefaults standardUserDefaults] synchronize];
        
        
    }
    
}


//NEW Notification Code

/*-(void) application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    NSLog(@" userInfo : %@ ", userInfo);
    
    NSMutableDictionary *dictOfNotificationData = [[NSMutableDictionary alloc] init];
    
    [dictOfNotificationData setObject:[userInfo valueForKey:@"createdOn"] forKey:@"createdOn"];
    
    [dictOfNotificationData setObject:[userInfo valueForKey:@"notificationId"] forKey:@"notificationId"];
    
    [dictOfNotificationData setObject:[userInfo valueForKey:@"notificationtype"] forKey:@"notificationtype"];
    
    [dictOfNotificationData setObject:[userInfo valueForKey:@"imageUrl"] forKey:@"imageUrl"];
    
    [dictOfNotificationData setObject:[userInfo valueForKey:@"redirect"] forKey:@"redirect"];
    
    [dictOfNotificationData setObject:[userInfo valueForKey:@"me"] forKey:@"me"];
    
    NSLog(@" dictOfNotificationData : %@ ", dictOfNotificationData);
    
    if(DEBUG_ENABLED)
    {
        debugMethodStart(@"------------------------------");
        debug(@"didReceiveRemoteNotification");
        debugMethodEnd(@"------------------------------");
    }
    
    if(DEBUG_ENABLED)
    {
        debug(@"\n Received Notification : %@ ", userInfo );
        debug(@"\n Received Notification : %@ ", [userInfo valueForKey:@"aps"]);
        
    }
    
    if (dictOfNotificationData != nil)
    {
        
        [self SaveNotificationIntoDatabase:dictOfNotificationData];
        
    }
    
    UILocalNotification *notif = [[UILocalNotification alloc] init];
    notif.applicationIconBadgeNumber = dictionary.count;
    if([UIApplication sharedApplication].applicationState == UIApplicationStateActive)
    {
        NSLog(@"active");
        
        [[NSNotificationCenter defaultCenter]postNotificationName:@"NotificationInActiveState" object:self];
        
    }
    
    else if(application.applicationState == UIApplicationStateInactive)
    {
        
        NSLog(@"Inactive");

        if ([[dictionary objectForKey:@"notificationtype"]  isEqualToString:@"package"])
        {
            NSString *packageID =  [dictionary objectForKey:@"redirect"];
            [self fetchPackageDetailsWithPackageID:packageID];
        }
        else if ([[dictionary objectForKey:@"notificationtype"] isEqualToString:@"web"])
        {
            NSString *urlString =  [dictionary objectForKey:@"redirect"];
            NSURL *url = [NSURL URLWithString:urlString];
            WebViewController *webViewVC = [[WebViewController alloc]initWithNibName:@"BaseViewController" bundle:nil];
            webViewVC.url = url;
            [[SlideNavigationController sharedInstance] pushViewController:webViewVC animated:YES];
        }
        else if ([[dictionary objectForKey:@"notificationtype"] isEqualToString:@"destination"])
        {
            NSString *destinationString  =  [dictionary objectForKey:@"redirect"];
            [self searchDestinationWithDestinationName:destinationString];
            
        }
        else  if ([[dictionary objectForKey:@"notificationtype"] isEqualToString:@"offer"])
        {
            NSString *urlString =  [dictionary objectForKey:@"redirect"];
            NSURL *url = [NSURL URLWithString:urlString];
            WebViewController *webViewVC = [[WebViewController alloc]initWithNibName:@"BaseViewController" bundle:nil];
            webViewVC.url = url;
            [[SlideNavigationController sharedInstance] pushViewController:webViewVC animated:YES];
        }
    }
}
 */


//OLD Notification Code
/*
- (void)application:(UIApplication*)application didReceiveRemoteNotification:(NSDictionary*)userInfo
{
    NSLog(@"did recieve local notification %@", userInfo);
    [[NetCorePushTaskManager sharedInstance] didReceiveLocalNotification:userInfo];
}
*/


- (void)application:(UIApplication*)application didReceiveRemoteNotification:(NSDictionary*)userInfo
{
    
    NSLog(@"did recieve notification for userInfo dictionary %@", userInfo);
    dispatch_async(dispatch_get_main_queue(), ^{
    [NotificationController updateAplicationIconBadgeNumber];
    });
    /*
    NSString * dictionaryString = (NSString * ) [userInfo objectForKey:@"message"];
    
    NSLog(@"notification dictionaryString %@",dictionaryString);
    
    dictionary = (NSDictionary *) [JsonSerealizer dictonaryOfJsonFromJsonData:dictionaryString];

    if(DEBUG_ENABLED)
    {
        debugMethodStart(@"------------------------------");
        debug(@"didReceiveRemoteNotification");
        debugMethodEnd(@"------------------------------");
    }
    
    if(DEBUG_ENABLED)
    {
        debug(@"\n Received Notification userInfo: %@ ", userInfo );
        debug(@"\n Received Notification aps: %@ ", [userInfo valueForKey:@"aps"]);
        debug(@"\n Received Notification message: %@ ", [userInfo valueForKey:@"message"]);
        
    }

  
    if (dictionary != nil)
    {
        
        [self SaveNotificationIntoDatabase:dictionary];
        
    }
    UILocalNotification *notif = [[UILocalNotification alloc] init];
    notif.applicationIconBadgeNumber = dictionary.count;
    if([UIApplication sharedApplication].applicationState == UIApplicationStateActive)
    {
         NSLog(@"active");
        
        [[NSNotificationCenter defaultCenter]postNotificationName:@"NotificationInActiveState" object:self];
        
//        UIAlertView * remoteNotificationAlert = [[UIAlertView alloc] initWithTitle:@"Remote Notification Received" message:   [NSString stringWithFormat:@"UIApplicationStateActive %@", userInfo] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//        [remoteNotificationAlert show];
        
        
    }
    
    else if(application.applicationState == UIApplicationStateInactive)
    {
        
        NSLog(@"Inactive");
        
        //           UIAlertView * remoteNotificationAlert = [[UIAlertView alloc] initWithTitle:@"Remote Notification Received" message:   [NSString stringWithFormat:@"UIApplicationStateInactive %@", userInfo] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        //           [remoteNotificationAlert show];
        
        
        if(DEBUG_ENABLED)
        {
            debug(@"\n Received Notification dictionary: %@ ", dictionary );
        }
        
//         NotificationViewController *notificationVC = [[NotificationViewController alloc]initWithNibName:@"BaseViewController" bundle:nil]; //netcore notification method handling
// 
//        [[SlideNavigationController sharedInstance] pushViewController:notificationVC animated:YES];
        
        
    }
    //    else if (application.applicationState == UIApplicationStateBackground) {
    //
    //            NSLog(@"Background");
    //
    //            UIAlertView * remoteNotificationAlert = [[UIAlertView alloc] initWithTitle:@"Remote Notification Received" message:[NSString stringWithFormat:@"UIApplicationStatebackground %@",userInfo] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    //            [remoteNotificationAlert show];
    //
    //
    //
    //
    //
    //        }
    //    else {
    //
    //            NSLog(@"Active");
    //
    //            UIAlertView * remoteNotificationAlert = [[UIAlertView alloc] initWithTitle:@"Remote Notification Received" message:[NSString stringWithFormat:@" else %@",userInfo] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    //            [remoteNotificationAlert show];
    //        
    //    
    //        }
*/
    // NetCore APNS code
   [[NetCorePushTaskManager sharedInstance] didReceiveRemoteNotification:userInfo];
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
     NSLog(@"did recieve local notification %@", notification);
    [[NetCorePushTaskManager sharedInstance] didReceiveLocalNotification:notification.userInfo];
}


#pragma mark -navigate from notification

-(void)searchDestinationWithDestinationName:(NSString *)destination
{
   /* NSString *strDestination=destination;
    
    NSDictionary *dictOfData = [NSDictionary dictionaryWithObjectsAndKeys:@"",kBudgetKey,@"",kNightToSpendKey,strDestination,kDestinationKey, nil];
    
    activityLoadingView = [LoadingView loadingViewInView:self.window withString:@"" andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
    
    if ([self connected])
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            NetworkHelper *helper = [NetworkHelper sharedHelper];
            
            NSString *strResponseLocal = [helper getDataFromServerForType:@"webservice" entity:@"holidays" action:@"search" andUserJson:dictOfData];
            
            NSString *strStatus = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponseLocal]valueForKey:@"status"];
            dispatch_async(dispatch_get_main_queue(), ^{
                if ([strStatus isEqualToString:kStatusSuccess])
                {
                    NSString * packageDetailsMessage = [[[JsonSerealizer dictonaryOfJsonFromJsonData:strResponseLocal]valueForKey:@"data"]valueForKey:@"packageDetailsMessage"];
                    
                    if ([@"Data not found." caseInsensitiveCompare:packageDetailsMessage] == NSOrderedSame)
                    {
                       [self showAlertViewWithTitle:@"Alert" withMessage:@"Sorry, we could not find any package for the given search. Please try with a different search criteria."];
                    }
                    else
                    {
                        NSArray *arrayOfData = [[[JsonSerealizer dictonaryOfJsonFromJsonData:strResponseLocal]valueForKey:kServerResponseKeyData]valueForKey:kHolidays];
                        NSDictionary *filterDict = [[[JsonSerealizer dictonaryOfJsonFromJsonData:strResponseLocal]valueForKey:kServerResponseKeyData]valueForKey:kFilters];
                        NSMutableArray *arrayOfHolidays = [[NSMutableArray alloc]init];
                        
                        for (NSDictionary *dictOfData in arrayOfData)
                        {
                            Holiday *nwHoliday  = [[Holiday alloc]initWithDataDict:dictOfData];
                            [arrayOfHolidays addObject:nwHoliday];
                        }
                        
                        [activityLoadingView removeView];
                        
                        TabMenuVC *tabMenuVC=[[TabMenuVC alloc]initWithNibName:@"BaseViewController" bundle:nil];
                        tabMenuVC.headerName=[NSString stringWithFormat:@"%@ %@",strDestination, @"Holidays"];
                        tabMenuVC.searchedCity = destination;
                        tabMenuVC.holidayArray = arrayOfHolidays;
                        tabMenuVC.filterDict = filterDict;
                        tabMenuVC.totalPackages=[NSString stringWithFormat:@"%lu",(unsigned long)[arrayOfData count]];
                        
                        [[SlideNavigationController sharedInstance] pushViewController:tabMenuVC animated:YES];

                    }
                }
                else
                {
                    NSString *messsage = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponseLocal]valueForKey:@"message"];
                    if (messsage)
                    {
                        if ([messsage isEqualToString:kVersionUpgradeMessage])
                        {
                            NSDictionary *downloadDict = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponseLocal] valueForKey:@"data"];
                            NSString *downloadURL = [downloadDict valueForKey:@"download_url"];
                          //  [self showAlertViewForVersionUpdateWithUrl:downloadURL];
                        }
                        else
                        {
                            [self showAlertViewWithTitle:@"Alert" withMessage:messsage];
                            [activityLoadingView removeView];
                        }
                    }
                    else
                    {
                        [self showAlertViewWithTitle:@"Alert" withMessage:kMessageSomeErrorTryAfterSometime];
                        [activityLoadingView removeView];
                        
                    }
                    
                }
                
            });
        });
    }
    else
    {
        [self showAlertViewWithTitle:@"Alert" withMessage:kMessageNoInternet];
        [activityLoadingView removeView];
    }
    */
    
    
    NetworkHelper *helper = [NetworkHelper sharedHelper];
    
    activityLoadingView = [LoadingView loadingViewInView:self.window
                                                            withString:@""
                                                     andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
    
    /*
    NSMutableString *queryParam = [[NSMutableString alloc] init] ;
    
    [queryParam appendFormat:@"&destination=%@",destination];
    
    */
    @try
    {
        
        NSString *stringWithDecode  = [destination stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        
        
        NSDictionary *headerDict = [CoreUtility getHeaderDict];
        
        [helper getResponseWithRequestType:@"" withQueryParam:stringWithDecode withPathParam:@"" withJsonParam:nil withHeaders:headerDict withUrl:kAstraUrlPackageFilterSearch success:^(NSDictionary *responseDict)
         {
             dispatch_async(dispatch_get_main_queue(), ^(void)
                            {
                                [activityLoadingView removeFromSuperview];
                                NSLog(@"Response Dict : %@",responseDict);
                                
                                if (responseDict)
                                {
                                    if (responseDict.count>0)
                                    {
                                        NSMutableArray *arrayOfHolidays = [[NSMutableArray alloc] init];
                                        NSArray *packageArray = (NSArray *)responseDict;
                                        
                                        for (NSDictionary *dictOfData in packageArray)
                                        {
                                            Holiday *nwHoliday  = [[Holiday alloc]initWithDataDict:dictOfData];
                                            [arrayOfHolidays addObject:nwHoliday];
                                        }
                                        
                                        
                                        [activityLoadingView removeView];
                                        
                                        
                                        TabMenuVC *tabMenuVC=[[TabMenuVC alloc]initWithNibName:@"BaseViewController" bundle:nil];
                                        
                                        //tabMenuVC.headerName=[NSString stringWithFormat:@"%@ %@",stringWithDecode, @"Holidays"];
                                        
                                        NSArray *listItems = [[NSArray alloc]initWithArray:[destination componentsSeparatedByString:@"="]];
                                        if ([listItems count] != 0){
                                        NSString *headerlabel = listItems[[listItems count] - 1];
                                        tabMenuVC.headerName = headerlabel;
                                        }else{
                                           tabMenuVC.headerName = @"";
                                        }
                                        
                                        tabMenuVC.searchedCity = destination;
                                        tabMenuVC.holidayArray = arrayOfHolidays;
                                        tabMenuVC.totalPackages=[NSString stringWithFormat:@"%lu",(unsigned long)[arrayOfHolidays count]];
                                        tabMenuVC.completePackageDetail = packageArray;
                                        [[SlideNavigationController sharedInstance] pushViewController:tabMenuVC animated:YES];
                                    }
                                    else
                                    {
                                        [self showAlertViewWithTitle:@"error" withMessage:@"Sorry, we could not find any package for the given search. Please try with a different search criteria."];
                                        NSLog(@" response Date Search packageDetailsMessage "  );
                                        
                                    }
                                }
                                else
                                {
                                    [self showAlertViewWithTitle:@"Alert" withMessage:@"Some error occurred"];
                                    
                                }
                                
                            });
         }
                                   failure:^(NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^(void)
                            {
                                [activityLoadingView removeFromSuperview];
                                [self showAlertViewWithTitle:@"Alert" withMessage:[error description]];
                                NSLog(@"Response Dict : %@",[error description]);
                            });
             
         }];
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"%@", exception.reason);
        [activityLoadingView removeFromSuperview];
    }
    @finally
    {
        NSLog(@"exception finally called");
    }
    
    

    
}

- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}


-(void)fetchPackageDetailsWithPackageID:(NSString *)packageID
{
    if ([self connected])
    {
        @try
        {
            
            NetworkHelper *helper = [NetworkHelper sharedHelper];
            
            NSString *pathParam = packageID;
            
            NSDictionary *headerDict = [CoreUtility getHeaderDict];
            
            [helper getResponseWithRequestType:@"" withQueryParam:@"" withPathParam:pathParam withJsonParam:nil withHeaders:headerDict withUrl:kAstraUrlPackageDetail success:^(NSDictionary *responseDict)
             {
                 NSLog(@"Response Dict : %@",responseDict);
                 
                 
                 dispatch_async(dispatch_get_main_queue(), ^(void) {
                     [activityLoadingView removeFromSuperview];
                     if (responseDict)
                     {
                         if (responseDict.count>0)
                         {
                             NSArray *packageArray = (NSArray *)responseDict;
                             NSDictionary *dictForCompletePackage = [packageArray objectAtIndex:0];
                             
                             HolidayPackageDetail *packageDetailHoliday = [[HolidayPackageDetail alloc] initWithDataDict:dictForCompletePackage];
                             
                             //  [self fireAppIntHoViewDetailsEvent]; //
                             
                             TabMenuVC *tabMenuVC = [[TabMenuVC alloc]initWithNibName:@"BaseViewController" bundle:nil];
                             
                             //tabMenuVC.headerName = objHoliday.strPackageName;
                             
                             tabMenuVC.flag = @"Overview";
                             tabMenuVC.packageDetail = packageDetailHoliday;
                             tabMenuVC.completePackageDetail = [NSArray arrayWithObjects:dictForCompletePackage, nil];
                             [[SlideNavigationController sharedInstance] pushViewController:tabMenuVC animated:YES];
                             
                             // [self doMyOperationsWithResponse:packageArray];
                             
                         }
                         else
                         {
                             [self showAlertViewWithTitle:@"Alert" withMessage:@"some error occurred"];
                             
                         }
                     }
                     else
                     {
                         [self showAlertViewWithTitle:@"Alert" withMessage:@"some error occurred"];
                         
                     }
                 });
                 
             }
             
                                       failure:^(NSError *error)
             {
                 dispatch_async(dispatch_get_main_queue(), ^(void)
                                {
                                    [activityLoadingView removeFromSuperview];
                                    [self showAlertViewWithTitle:@"Alert" withMessage:@"some error occurred"];
                                    NSLog(@"Response Dict : %@",[error description]);
                                });
                 
                 
             }];
            
        }
        @catch (NSException *exception)
        {
            NSLog(@"%@", exception.reason);
            [activityLoadingView removeFromSuperview];
        }
        @finally
        {
            NSLog(@"exception finally called");
        }
}
}

-(void)showAlertViewWithTitle:(NSString *)title withMessage:(NSString *)message
{
    UIAlertController *alertView = [UIAlertController alertControllerWithTitle:title
                                                                       message:message
                                                                preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [alertView dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertView addAction:okAction];
    
    [self.window.rootViewController presentViewController:alertView animated:YES completion:nil];
}

-(void) SaveNotificationIntoDatabase:(NSDictionary * ) notificationData
{
  
    
    if(DEBUG_ENABLED)
    {
        debug(@"\n notificationData save into database: %@ ",notificationData );
    }
    NSManagedObjectContext *context = [self managedObjectContext];
    
    //remove old object
    
    
    NSFetchRequest * fetchRequestAll = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity1 = [NSEntityDescription
                                   entityForName:@"Notification" inManagedObjectContext:context];
    [fetchRequestAll setEntity:entity1];
    
    NSError * error;
    NSArray *fetchedAllObjects = [context executeFetchRequest:fetchRequestAll error:&error];
    
    if (fetchedAllObjects.count >= 10) {
        
      [context deleteObject:[fetchedAllObjects objectAtIndex:0]];
   
        [context save:&error];
    }
   
    
    
    
     //check notification already present or not
    
   NSManagedObject *notificationObject = [NSEntityDescription
                                       insertNewObjectForEntityForName:@"Notification"
                                       inManagedObjectContext:context];
    
    // NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Notification"];
//    request.predicate = [NSPredicate predicateWithFormat:
//                         @"notificationId = %@", [notificationData objectForKey:@"notificationId"]];
//    NSError *error1;
//
//    NSArray *activeIds = [context executeFetchRequest:request error:&error1];
//
//
//    if (activeIds == nil) {
//
//         handle error
//    }
//     else
//     {
//
//        for (NSManagedObject *object in activeIds) {
//
//            [context deleteObject:object];
//        }
//
//        [context save:&error1];
   // }
    
    NSString *idForNotification = [notificationData objectForKey:@"notificationId"];
    
    NSError *errorFORsameID;
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:@"Notification" inManagedObjectContext:context]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat: @"id LIKE  %@", idForNotification];
    [request setPredicate:predicate];
    
    NSArray *arrayactiveIds = [context executeFetchRequest:request error:&errorFORsameID];
    
    
    if (arrayactiveIds != nil)
    {
        
        for (NSManagedObject *object in arrayactiveIds)
        {
            
            [context deleteObject:object];
        }

    }
    
    
    [context save:&errorFORsameID];

    
    
    
    
     //add notification
    
    
    
//    notificationData save into database:
//  {
//        createdOn = 1487067660038;
//        imageUrl = "";
//        me =     {
//            body = "Book Online & Get Flat 20,000 off on Andaman Holiday. Code: TCAND20 . Offer for selected Departures. T&C";
//            subtitle = "";
//            title = "Go Andaman: Rs 20,000 off";
//        };
//        notificationId = 8;
//        notificationtype = destination;
//        redirect = Andaman;
//        title = "Go Andaman: Rs 20,000 off";
//    }


    
    if ([notificationData objectForKey:@"notificationId"] != nil && ![[notificationData objectForKey:@"notificationId"] isKindOfClass:[NSNull class]] ) {
        
        [notificationObject setValue:[notificationData objectForKey:@"notificationId"] forKey:@"id"];
        
    }
    
    if ([notificationData objectForKey:@"me"] != nil && ![[notificationData objectForKey:@"me"] isKindOfClass:[NSNull class]] ) {
        
        NSDictionary *meDictionary = [notificationData objectForKey:@"me"];
        NSString *messageBody =  [meDictionary objectForKey:@"body"];
     
        
        if(DEBUG_ENABLED)
        {
            debug(@"\n message saved into database: %@ ",messageBody );
        }
        
        [notificationObject setValue:messageBody forKey:@"message"];
        
    }
    if ([notificationData objectForKey:@"notificationtype"] != nil && ![[notificationData objectForKey:@"notificationtype"] isKindOfClass:[NSNull class]] ) {
        
          [notificationObject setValue:[notificationData objectForKey:@"notificationtype"] forKey:@"notificationType"];
        
    }
    if ([notificationData objectForKey:@"redirect"] != nil && ![[notificationData objectForKey:@"redirect"] isKindOfClass:[NSNull class]] ) {
        
            [notificationObject setValue:[notificationData objectForKey:@"redirect"] forKey:@"redirect"];
        
    }
    
    if ([notificationData objectForKey:@"title"] != nil && ![[notificationData objectForKey:@"title"] isKindOfClass:[NSNull class]]  )
    {
        
        NSString *titleString = [NSString stringWithFormat:@"%@", [notificationData objectForKey:@"title"]];
        
        if(DEBUG_ENABLED)
        {
            debug(@"\n title saved into database: %@ ",titleString );
        }
        
        
        [notificationObject setValue:titleString forKey:@"title"];

        
    }
    
    
//    if ([notificationData objectForKey:@"createdOn"] != nil && ![[notificationData objectForKey:@"createdOn"] isKindOfClass:[NSNull class]] ) {
//        
//         [notificationObject setValue:[notificationData objectForKey:@"createdOn"] forKey:@"createdOn"];
//        
//    }
    
    NSDate *date = [NSDate date];
    
    NSString * datestring = [NSString stringWithFormat:@"%lld", [@(floor([date timeIntervalSince1970] * 1000)) longLongValue]];
    
    [notificationObject setValue:datestring forKey:@"createdOn"];
  
    NSString *notifiacationString = [JsonSerealizer stringRepresenationFromJSonDataObject:notificationData] ;
    
    
    
    if(DEBUG_ENABLED)
    {
        debug(@"\n notifiacationString saved into database: %@ ",notifiacationString );
    }
    
//    notifiacationString saved into database: {
//        "title" : "Go Andaman: Rs 20,000 off",
//        "redirect" : "Andaman",
//        "me" : {
//            "subtitle" : "",
//            "body" : "Book Online & Get Flat 20,000 off on Andaman Holiday. Code: TCAND20 . Offer for selected Departures. T&C",
//            "title" : "Go Andaman: Rs 20,000 off"
//        },
//        "notificationId" : "8",
//        "imageUrl" : "",
//        "createdOn" : "1487069160023",
//        "notificationtype" : "destination"
//    }
    
    [notificationObject setValue:notifiacationString forKey:@"data"];
    [notificationObject setValue:@"unread" forKey:@"status"];
   
   
    NSError * error11;
    if (![context save:&error11]) {
        NSLog(@"Whoops, couldn't save: %@", [error11 localizedDescription]);
    }
 
    
    
     
//    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
//    NSEntityDescription *entity = [NSEntityDescription
//                                   entityForName:@"Notification" inManagedObjectContext:context];
//    [fetchRequest setEntity:entity];
//
//    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
//
//    for (NSManagedObject *info in fetchedObjects) {
//
//        NSLog(@"id: %@", [info valueForKey:@"id"]);
//
//    }
//
    
       

  
    
}




- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    //Vj_Commented
//    overlayImageView = [[UIImageView alloc] initWithFrame:self.window.frame];
//    [overlayImageView setImage:[UIImage imageNamed:@"page1.jpg"]];
//    [self.window addSubview:overlayImageView];
    
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    NSLog(@"Application Did Enter Background");
    [[FIRMessaging messaging] disconnect];
    
    dispatch_async(dispatch_get_main_queue(), ^{
    [NotificationController updateAplicationIconBadgeNumber];
    });
    
   // NSTimer *t = [NSTimer scheduledTimerWithTimeInterval: 10.0 target: self selector:@selector(onTick:) userInfo: nil repeats:NO];
    
    
    

    
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    
     [AppUtility checkIsDeviceRooted];
     [AppUtility checkIsVersionUpgrade];
     NSLog(@"Application Will Enter Foreground");
    NSString *loginStatus = [[NSUserDefaults standardUserDefaults] valueForKey:kLoginStatus];
    NSString *transactionId = [[NSUserDefaults standardUserDefaults] valueForKey:kCurrentTransactionId];
    if ([transactionId length]>0 && [loginStatus isEqualToString:kLoginSuccess]){
        BookingConfirmOne *booking=[[BookingConfirmOne alloc]initWithNibName:@"BookingConfirmOne" bundle:nil];
        self.window.rootViewController = [[UINavigationController alloc] initWithRootViewController: booking];
        [self.window makeKeyAndVisible];
    }
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 0];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    
    if (overlayImageView != nil)
    {
        [overlayImageView removeFromSuperview];
        overlayImageView = nil;
    }
    
     [FBSDKAppEvents activateApp];
    
    if (self.currentDeepLink)
    {
        [self openLink:self.currentDeepLink];
        self.currentDeepLink = nil;
    }
    
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    //NEW Notification Code
    [self connectToFcm];
    
   // isFromAppDidiFinishLaunching = false;
    [self registerForRemoteNotifications];
    dispatch_async(dispatch_get_main_queue(), ^{
        [AppUtility checkAndSubscibeNotificationOnServer];
        [NotificationController updateAplicationIconBadgeNumber];
    });
//    [AppUtility checkAndSubscibeNotificationOnServer];
    
   // [NotificationController resetROEDataOnController];
    
//    [NotificationController updateAplicationIconBadgeNumber];
    
    
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    dispatch_async(dispatch_get_main_queue(), ^{
    [NotificationController updateAplicationIconBadgeNumber];
    });
}



- (BOOL)openLink:(NSURL *)urlLink
{
    NSLog(@"openLink method get called");
    return true;
}


#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "mobicule.jhgfdr" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"holidays" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}


- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it.
    
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"holidays.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    
    // important part starts here
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys: [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption, [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
    
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error])
    {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

- (NSManagedObjectContext *)managedObjectContext {
    
    
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil)
    {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext
{
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil)
    {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error])
        {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

-(BOOL) application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString *,id> *)options
{
    if ([[url scheme] isEqualToString:@"fb1021596341229809"])
    {
        return [self application:app openURL:url sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey] annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];
    }
    else
    {
        return [[GIDSignIn sharedInstance] handleURL:url
                                   sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                          annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];
    }
    
    /*return [self application:app openURL:url sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey] annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];*/

}

/*- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    
    NSLog(@"%@",[url scheme]);
    if ([[url scheme] isEqualToString:@"fb1021596341229809"])
    {
               return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                              openURL:url
                                                    sourceApplication:sourceApplication
                                                           annotation:annotation
                ];
    }
    else
    {
        return [[GIDSignIn sharedInstance] handleURL:url
                                   sourceApplication:sourceApplication
                                          annotation:annotation];

    }
 }

- (BOOL)application:(UIApplication *)app
            openURL:(NSURL *)url
            options:(NSDictionary<NSString *,id> *)options
{
    
    if (![[url scheme] isEqualToString:@"fb1021596341229809"])
    {
        return [[GIDSignIn sharedInstance] handleURL:url
                                   sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                          annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];
    }
    else
    {
        return FALSE;
    }
    
}
 
 */

// [START handle_link]

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    
    if ([[url scheme] isEqualToString:@"fb1021596341229809"])
    {
        /*
        BOOL handled = [[FBSDKApplicationDelegate sharedInstance] application:application
                                                                      openURL:url
                                                            sourceApplication:sourceApplication
                                                                   annotation:annotation
                        ];
        return handled;
        */
        
        return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                              openURL:url
                                                    sourceApplication:sourceApplication
                                                           annotation:annotation];
    }
    else
    {    
        self.currentDeepLink = url;
        
      return YES;
    
    }
    return [[GIDSignIn sharedInstance] handleURL:url sourceApplication:sourceApplication annotation:annotation];
    
}

// PushManagerDelegate Method
//thomasCook://com.thomasCookHoliday.app/package_list/searchType=CONTINENT&destination=Europe?__sta=vhg.uosvpxUYQIlzjokqisfjst%7CVHV&__stm_medium=apn&__stm_source=smartech&__stm_id=242
-(void)handleNotificationOpenAction:(NSDictionary *)userInfo DeepLinkType: (NSString *)strType
{
    NSURL *urlForDeepLink = [NSURL URLWithString:strType];
    NSLog(@"TNT: userinfo%@",userInfo);
    NSLog(@"TNT: deelinktype%@",strType);
    NSLog(@"TNT: urlfor deeplink%@",urlForDeepLink);
    isFromNotificationClick = YES; /// handle Notification TAP
    
   
    if ([[urlForDeepLink path] containsString:@"package_details"])
    {
        NSString *stringWithoutHash = [[urlForDeepLink path] stringByReplacingOccurrencesOfString:@"/" withString:@""];
         NSString *packageId = [stringWithoutHash stringByReplacingOccurrencesOfString:@"package_details" withString:@""];
        
        [self fetchPackageDetailsWithPackageID:packageId];
        
    }
    else if ([[urlForDeepLink path] containsString:@"package_list"])
    {
        NSString *stringWithoutHash = [[urlForDeepLink path] stringByReplacingOccurrencesOfString:@"/" withString:@""];
        NSString *destination = [stringWithoutHash stringByReplacingOccurrencesOfString:@"package_list" withString:@""];
        [self searchDestinationWithDestinationName:destination];
        

    }
    else if ([[urlForDeepLink path] containsString:@"web"])
    {
        NSString *stringWithoutHash = [[urlForDeepLink path] stringByReplacingOccurrencesOfString:@"/" withString:@""];
        NSString *destination = [stringWithoutHash stringByReplacingOccurrencesOfString:@"web" withString:@""];
        NSURL *url = [NSURL URLWithString:destination];
        WebViewController *webViewVC = [[WebViewController alloc]initWithNibName:@"BaseViewController" bundle:nil];
        webViewVC.url = url;
        [[SlideNavigationController sharedInstance] pushViewController:webViewVC animated:YES];

    }else if ([[urlForDeepLink path] containsString:@"css_payment"])
    {
       if ([[NSUserDefaults standardUserDefaults] valueForKey: @"isCSSUserLoggedIn"]) {
        NSString *stringWithoutHash = [[urlForDeepLink path] stringByReplacingOccurrencesOfString:@"/" withString:@":"];
        NSArray *bffarray = [stringWithoutHash componentsSeparatedByString:@":"];
        NSString *bffnumber = [bffarray objectAtIndex:bffarray.count - 1];
           NSLog(@"----bff:%@",bffnumber);
        [NotificationController moveToRespectiveViewControllerWithNotificationType:@"payments" bfNumber:bffnumber webViewRedirectionURL:@"" message:@""];
        
        }
        
    }else if ([[urlForDeepLink path] containsString:@"css_ticket"])
    {
        if ([[NSUserDefaults standardUserDefaults] valueForKey: @"isCSSUserLoggedIn"]) {
            NSString *stringWithoutHash = [[urlForDeepLink path] stringByReplacingOccurrencesOfString:@"/" withString:@":"];
            NSArray *bffarray = [stringWithoutHash componentsSeparatedByString:@":"];
            NSString *bffnumber = [bffarray objectAtIndex:bffarray.count - 1];
            NSLog(@"----bff:%@",bffnumber);
            [NotificationController moveToRespectiveViewControllerWithNotificationType:@"air tickets" bfNumber:bffnumber webViewRedirectionURL:@"" message:@""];
            
        }
        
    }else if ([[urlForDeepLink path] containsString:@"css_tour_document"])
    {
        if ([[NSUserDefaults standardUserDefaults] valueForKey: @"isCSSUserLoggedIn"]) {
            NSString *stringWithoutHash = [[urlForDeepLink path] stringByReplacingOccurrencesOfString:@"/" withString:@":"];
            NSArray *bffarray = [stringWithoutHash componentsSeparatedByString:@":"];
            NSString *bffnumber = [bffarray objectAtIndex:bffarray.count - 1];
            NSLog(@"----bff:%@",bffnumber);
            [NotificationController moveToRespectiveViewControllerWithNotificationType:@"tour details" bfNumber:bffnumber webViewRedirectionURL:@"" message:@""];
            
        }
        
    }else if ([[urlForDeepLink path] containsString:@"css_profile"])
    {//Profile
        if ([[NSUserDefaults standardUserDefaults] valueForKey: @"isCSSUserLoggedIn"]) {
            
            NSString *stringWithoutHash = [[urlForDeepLink path] stringByReplacingOccurrencesOfString:@"/" withString:@":"];
            NSArray *bffarray = [stringWithoutHash componentsSeparatedByString:@":"];
            NSString *bffnumber = [bffarray objectAtIndex:bffarray.count - 1];
            NSLog(@"----bff:%@",bffnumber);
            [NotificationController moveToRespectiveViewControllerWithNotificationType:@"profile" bfNumber:bffnumber webViewRedirectionURL:@"" message:@""];
            
        
            
        }
        
    }else if ([[urlForDeepLink path] containsString:@"css_deviation"])
    {
        if ([[NSUserDefaults standardUserDefaults] valueForKey: @"isCSSUserLoggedIn"]) {
            NSString *stringWithoutHash = [[urlForDeepLink path] stringByReplacingOccurrencesOfString:@"/" withString:@":"];
            NSArray *bffarray = [stringWithoutHash componentsSeparatedByString:@":"];
            NSString *bffnumber = [bffarray objectAtIndex:bffarray.count - 1];
            NSLog(@"----bff:%@",bffnumber);
            [NotificationController moveToRespectiveViewControllerWithNotificationType:@"deviation" bfNumber:bffnumber webViewRedirectionURL:@"" message:@""];
            
        }
        
    }else if ([[urlForDeepLink path] containsString:@"css_visa"])
    {
        if ([[NSUserDefaults standardUserDefaults] valueForKey: @"isCSSUserLoggedIn"]) {
            NSString *stringWithoutHash = [[urlForDeepLink path] stringByReplacingOccurrencesOfString:@"/" withString:@":"];
            NSArray *bffarray = [stringWithoutHash componentsSeparatedByString:@":"];
            NSString *bffnumber = [bffarray objectAtIndex:bffarray.count - 1];
            NSLog(@"----bff:%@",bffnumber);
            [NotificationController moveToRespectiveViewControllerWithNotificationType:@"visas" bfNumber:bffnumber webViewRedirectionURL:@"" message:@""];
            
        }
        
    }else if ([[urlForDeepLink path] containsString:@"css_feedback"])
    {
        if ([[NSUserDefaults standardUserDefaults] valueForKey: @"isCSSUserLoggedIn"]) {
            NSString *stringWithoutHash = [[urlForDeepLink path] stringByReplacingOccurrencesOfString:@"/" withString:@":"];
            NSArray *bffarray = [stringWithoutHash componentsSeparatedByString:@":"];
            NSString *bffnumber = [bffarray objectAtIndex:bffarray.count - 1];
            NSLog(@"----bff:%@",bffnumber);
            [NotificationController moveToRespectiveViewControllerWithNotificationType:@"feedback" bfNumber:bffnumber webViewRedirectionURL:@"www.google.com" message:@""];
            
        }
        
    }
    
}

// [START handle_universal_link]

- (BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void (^)(NSArray * _Nullable))restorationHandler
{
    
    if ([userActivity.activityType isEqualToString: NSUserActivityTypeBrowsingWeb])
    {
        self.currentDeepLink = userActivity.webpageURL;
        
        if ([self.currentDeepLink.query containsString:@"www.thomascook.in"])
        {
            SearchHolidaysVC *searchVC=[[SearchHolidaysVC alloc]initWithNibName:@"BaseViewController" bundle:nil];
            searchVC.boolForSearchPackgScreen=true;
            
            [[SlideNavigationController sharedInstance] pushViewController:searchVC animated:YES];
        }
        else if ([self.currentDeepLink.query containsString:@"m.thomascook.in"])
        {
            SearchHolidaysVC *searchVC=[[SearchHolidaysVC alloc]initWithNibName:@"BaseViewController" bundle:nil];
            searchVC.boolForSearchPackgScreen=true;
            
            [[SlideNavigationController sharedInstance] pushViewController:searchVC animated:YES];
        }
        else if ([self.currentDeepLink.query containsString:@"/tcportal"])
        {
            SearchHolidaysVC *searchVC=[[SearchHolidaysVC alloc]initWithNibName:@"BaseViewController" bundle:nil];
            searchVC.boolForSearchPackgScreen=true;
            
            [[SlideNavigationController sharedInstance] pushViewController:searchVC animated:YES];
        }
        else if ([self.currentDeepLink.query containsString:@"/tcm/holidays"])
        {
            SearchHolidaysVC *searchVC=[[SearchHolidaysVC alloc]initWithNibName:@"BaseViewController" bundle:nil];
            searchVC.boolForSearchPackgScreen=true;
            
            [[SlideNavigationController sharedInstance] pushViewController:searchVC animated:YES];
        }
        else
        {
            NSLog(@"link not found");
        }

    }
    else
    {
        NSLog(@"NO user activity");
    }
    
    [[NSNotificationCenter defaultCenter]postNotificationName:UIApplicationDidBecomeActiveNotification object:nil];
    
    return true;
}



//- (BOOL)application:(UIApplication *)app
//            openURL:(NSURL *)url
//            options:(NSDictionary *)options
//{
//    return [[GIDSignIn sharedInstance] handleURL:url
//                               sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
//                                      annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];
//}

//- (void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user withError:(NSError *)error {
//    // Perform any operations on signed in user here.
//    
//    NSString *userId = user.userID;                  // For client-side use only!
//    NSString *idToken = user.authentication.idToken; // Safe to send to the server
//    NSString *fullName = user.profile.name;
//    NSString *givenName = user.profile.givenName;
//    NSString *familyName = user.profile.familyName;
//    NSString *email = user.profile.email;
//    // [START_EXCLUDE]
//    NSDictionary *statusText = @{@"statusText":
//                                     [NSString stringWithFormat:@"Signed in user: %@",
//                                      email]};
//    [[NSNotificationCenter defaultCenter]
//     postNotificationName:@"ToggleAuthUINotification"
//     object:nil
//     userInfo:statusText];
//    // [END_EXCLUDE]
//}
//// [END signin_handler]
//
//- (void)signIn:(GIDSignIn *)signIn didDisconnectWithUser:(GIDGoogleUser *)user
//     withError:(NSError *)error
//{
//    // Perform any operations when the user disconnects from app here.
//    // [START_EXCLUDE]
//    NSDictionary *statusText = @{@"statusText": @"Disconnected user" };
//    [[NSNotificationCenter defaultCenter]
//     postNotificationName:@"ToggleAuthUINotification"
//     object:nil
//     userInfo:statusText];
//    // [END_EXCLUDE]
//}
//// [END disconnect_handler]
//
@end
