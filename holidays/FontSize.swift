//
//  FontSize.swift
//  sotc-consumer-application
//
//  Created by Saurav Kumar on 24/07/17.
//  Copyright © 2017 Mobicule Technologies Private Limited. All rights reserved.
//

import Foundation
import UIKit

extension UIFont
{
    
    class func fontForDevice() -> UIFont {
        
        let screenSize = UIScreen.main.bounds
   
        
        if screenSize.width == 320 {
            
            
            return UIFont.systemFont(ofSize: 12)
        }
        else if screenSize.width == 480
        {
              return UIFont.systemFont(ofSize: 15)
        }
        else
        {
            return UIFont.systemFont(ofSize: 17)
        }
        
    }
    
    
}

