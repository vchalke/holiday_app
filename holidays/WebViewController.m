//
//  ViewController.m
//  holidays
//
//  Created by ketan on 28/12/15.
//  Copyright © 2015 Mobicule. All rights reserved.
//

#import "WebViewController.h"

@interface WebViewController ()

@end

@implementation WebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSBundle mainBundle]loadNibNamed:@"WebViewController" owner:self options:nil];
    [super addViewInBaseView:self.viewForWebView];
    super.HeaderLabel.text=_headerString;
  /*  self.webViewForWebController.delegate = self;
    activityLoadingView = [LoadingView loadingViewInView:_viewForWebView
                                             withString:@""
                                      andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]]; */
    if (_url)
    {
         [self.webViewForWebController loadRequest:[NSURLRequest requestWithURL:_url]];
    }
  
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
   // [activityLoadingView removeView];
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
  // [activityLoadingView removeView];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
