//
//  RequestACallVC.h
//  holidays
//
//  Created by Kush_Team on 05/06/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPKeyboardAvoidingScrollView.h"
#import "HolidayPackageDetail.h"
#import "NewMasterVC.h"
NS_ASSUME_NONNULL_BEGIN

@interface RequestACallVC : NewMasterVC

@property (strong, nonatomic) IBOutlet UIView *view_ForRequestCall;
@property (strong ,nonatomic) HolidayPackageDetail *requestCallHolidayPackage;
@property (weak, nonatomic) IBOutlet UITextField *txt_FirstName;
@property (weak, nonatomic) IBOutlet UITextField *txt_LastName;
@property (weak, nonatomic) IBOutlet UITextField *txt_PhoneNumber;
@property (weak, nonatomic) IBOutlet UITextField *txt_EmailIId;
@property (weak, nonatomic) IBOutlet UITextField *txt_City;
@property (weak, nonatomic) IBOutlet UITextField *txt_Summery;
@property (weak, nonatomic) IBOutlet UITextField *txt_DepartureCity;
@property (weak, nonatomic) IBOutlet UIButton *btn_checkpolicy;
@end

NS_ASSUME_NONNULL_END
