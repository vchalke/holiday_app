//
//  ColourClass.h
//  holidays
//
//  Created by Kush_Tech on 22/02/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ColourClass : UIColor
+ (UIColor *) colorWithHexString: (NSString *) hexString;
+ (UIColor *)colorWithHexString:(NSString *)hexString setAlpha:(float)alpha;
+ (UIColor *) colorFromString: (NSString *) nameString;
+ (CGFloat) colorComponentFrom: (NSString *) string start: (NSUInteger) start length: (NSUInteger) length;
@end

NS_ASSUME_NONNULL_END
