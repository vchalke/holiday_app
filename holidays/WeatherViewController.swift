//
//  WeatherViewController.swift
//  sotc-consumer-application
//
//  Created by Saurav Kumar on 24/10/17.
//  Copyright © 2017 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit

class WeatherViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource{

    
    
    
    @IBOutlet var todayWindSpeedLabel: UILabel!
    @IBOutlet var todayTemperatureLabel: UILabel!
    @IBOutlet var todaySunriseTimeLabel: UILabel!
    @IBOutlet var todayConditionLabel: UILabel!
    @IBOutlet var todayTemperature: UILabel!
    @IBOutlet var cityButton: UIButton!
    @IBOutlet var weatherTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        self.weatherTableView.register(UINib(nibName:  "WeatherTableViewCell", bundle: nil), forCellReuseIdentifier: "WeatherTableViewCell")
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onCancelButtonClick(_ sender: Any) {
        
        
        self.dismiss(animated: true) { 
            
            
        }
    }
    @IBAction func onCityButtonClick(_ sender: Any) {
    }

    //MARK:TableView Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
       
        
        return 4
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 70
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "WeatherTableViewCell", for: indexPath) as? WeatherTableViewCell
            else {
                
                fatalError("The dequeued cell is not an instance of MealTableViewCell.")
        }
        
        
        /*
        let receipt = paymentReceiptList?[indexPath.row]
        
        
        
        let numberFormatter = NumberFormatter()
        
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        
        _ = numberFormatter.string(from: NSNumber(value:Float64((receipt?.receiptAmount!)!)! ))
        
        
        
        
        let tourDateLabelAttr = [NSForegroundColorAttributeName: UIColor.black, NSFontAttributeName: cell.receiptAmount.font] as [String : Any]
        let fisrtLetterAttr = [NSForegroundColorAttributeName: cell.receiptButton.titleLabel?.textColor!, NSFontAttributeName: cell.receiptAmount.font] as [String : Any]
        
        
        let rupee = NSMutableAttributedString(string: "\u{20B9}", attributes: fisrtLetterAttr)
        let amount = NSMutableAttributedString(string: "\(receipt?.receiptAmount ?? "" )", attributes: tourDateLabelAttr)
        let combination = NSMutableAttributedString()
        combination.append(rupee)
        combination.append(amount)
        cell.receiptAmount.attributedText = combination
        
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let receiptDate = dateFormatter.string(from: (receipt?.receiptDate! as! NSDate) as Date)
        
        
        cell.receiptDate.text = "\(receiptDate)"
        cell.receiptType.text = "Paid by \(receipt?.receiptType ?? "")"
        
        cell.receiptButton.layer.borderColor = (cell.receiptButton.titleLabel?.textColor)?.cgColor
        cell.receiptButton.layer.borderWidth = 1.0
        cell.receiptButton.layer.cornerRadius  =  5.0
        
        //cell.receiptButton.addTarget(self, action: #selector(receiptBtnClick), for: .touchUpInside)
        
        */
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
    
    }
    
    
}
