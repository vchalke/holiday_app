//
//  SightSeeingTableViewCell.m
//  holidays
//
//  Created by Kush_Team on 24/07/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "SightSeeingTableViewCell.h"

@implementation SightSeeingTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)loadDSightSeeingFromDetailModel:(SightSeenObject*)sightModel{
    self.lbl_Title.text = sightModel.cityName;
    self.lbl_subTitle.text = sightModel.name;
}
-(void)loadDSightSeeingFromDetailDict:(NSDictionary*)sightDict withArray:(NSString*)key{
    self.lbl_Title.text = key;
    self.lbl_subTitle.text = (NSString *)[sightDict objectForKey:key];;
}
@end
