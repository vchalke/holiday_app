//
//  CollectionReusableView.m
//  holidays
//
//  Created by Kush_Tech on 24/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "CollectionReusableView.h"

@implementation CollectionReusableView

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (UICollectionViewLayoutAttributes *)preferredLayoutAttributesFittingAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes
{
    [super preferredLayoutAttributesFittingAttributes:layoutAttributes];
    UICollectionViewLayoutAttributes *attributes = [layoutAttributes copy];
    attributes.size = CGSizeMake(self.base_Views.frame.size.width, 100);
    return attributes;
}
@end
