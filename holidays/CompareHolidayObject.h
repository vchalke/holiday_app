//
//  CompareHolidayObject.h
//  holidays
//
//  Created by Kush_Tech on 22/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CompareHolidayObject : NSObject
-(instancetype)initWithCompareHolidayObject:(NSDictionary *)dictionary;
@property(nonatomic,strong)NSString *packageData;
@property(nonatomic,strong)NSString *packageID;
@property(nonatomic,strong)NSString *packageName;
@property(nonatomic,strong)NSString *packageImgUrl;
@property(nonatomic)NSInteger packagePrize;
@end

NS_ASSUME_NONNULL_END
