//
//  BankOffersObject.h
//  holidays
//
//  Created by Kush_Team on 13/06/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BankOffersObject : NSObject
@property(nonatomic,strong)NSString *holidayoffername;
@property(nonatomic,strong)NSString *holidayoffertodate;
@property(nonatomic,strong)NSString *holidayoffferfromdate;
@property(nonatomic)NSInteger holidayofferid;
@property(nonatomic,strong)NSString *isActive;
@property(nonatomic,strong)NSString *imagepath;
-(instancetype)initWithBankDataArray:(NSDictionary *)arrayObject;
@end


/*
 {
   "holidayofferid": 18,
   "holidayoffername": "Up to INR 2 800 instant savings",
   "holidayoffertodate": "30-06-2026",
   "holidayoffferfromdate": "12-06-2020",
   "imagepath": "https://resources-uatastra.thomascook.in/images/holidays/offerApi/HSBC.PNG",
   "isActive": "Y"
 }
 */
