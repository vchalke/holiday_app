//
//  CalenderCollectionViewCell.m
//  holidays
//
//  Created by Kush_Tech on 24/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "CalenderCollectionViewCell.h"
#import "CalenderObject.h"

@implementation CalenderCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(void)loadDateDataInCell:(NSDate*)date withIndex:(NSInteger)index withCalederDateDict:(NSDictionary*)dataDict{
    CalenderObject *calenderObj = [[CalenderObject alloc]initWithCalenderObjectDict:dataDict];
    NSInteger dateType = [self getValueFromDateObject:calenderObj withDateStr:[[DateStringSingleton shared]getDateTimeStringFormat:@"dd-MM-yyyy" DateValue:date] isValue:NO];
    NSLog(@"%ld",dateType);
    indexDate = date;
    NSString *datetring = [NSString stringWithFormat:@"%@",[[DateStringSingleton shared]getDateTimeStringFormat:@"dd" DateValue:date]];
       NSLog(@"%@",datetring);
    self.base_view.hidden = ([date compare:[NSDate date]] == NSOrderedAscending);
    self.lbl_date.text = ([date compare:[NSDate date]] == NSOrderedAscending) ? @"" : datetring;
    NSInteger prize = [self getValueFromDateObject:calenderObj withDateStr:[[DateStringSingleton shared]getDateTimeStringFormat:@"dd-MM-yyyy" DateValue:date] isValue:YES];
    self.lbl_prize.text = [self getNumberFormatterString:prize];
    
//    self.lbl_dashLine.backgroundColor = (index % 2) ? [UIColor colorWithRed:21.0/255.0 green:155.0/255.0 blue:89.0/255.0 alpha:1.0] : [UIColor colorWithRed:255.0/255.0 green:149.0/255.0 blue:0.0/255.0 alpha:1.0];
//    self.lbl_prize.hidden = (index % 2) ? YES : NO;
//    self.lbl_dashLine.hidden = (index % 2) ? YES : NO;
    
    UIColor *greenColour = [UIColor colorWithRed:21.0/255.0 green:155.0/255.0 blue:89.0/255.0 alpha:1.0];
    UIColor *yellowColour = [UIColor colorWithRed:255.0/255.0 green:149.0/255.0 blue:0.0/255.0 alpha:1.0];
    self.lbl_dashLine.backgroundColor = (dateType == 2) ?  yellowColour : (dateType == 1) ? greenColour : [UIColor clearColor];
    self.lbl_prize.hidden = (dateType == 0) ? YES : NO;
    self.lbl_dashLine.hidden = (dateType == 0) ? YES : NO;
    [self.base_view.layer setBorderColor:(([date compare:self.selectedDate] == NSOrderedSame && [[NSDate date] compare:self.selectedDate] == NSOrderedAscending) ? UIColor.redColor.CGColor : UIColor.whiteColor.CGColor)];
    
}
-(NSInteger)getValueFromDateObject:(CalenderObject*)calObject withDateStr:(NSString*)dateStr isValue:(BOOL)value{
    NSArray *bookableArray = calObject.bookable;
    for (NSDictionary *bookableObj in bookableArray){
        CalenderObject *requestObj = [[CalenderObject alloc] initWithBookableAndRequestObjectDict:bookableObj withPackgeSubType:calObject.pkgSubType];
        if ([requestObj.date isEqualToString:dateStr]){
            return (value) ? requestObj.price : 1;
        }
    }
    NSArray *onRequestArray = calObject.onRequest;
    for (NSDictionary *onRequestObj in onRequestArray){
        CalenderObject *requestObj = [[CalenderObject alloc] initWithBookableAndRequestObjectDict:onRequestObj withPackgeSubType:calObject.pkgSubType];
        if ([requestObj.date isEqualToString:dateStr]){
            return (value) ? requestObj.price : 2;
        }
    }
    return 0;
}
//-(NSInteger)getPrizeForDateFromObj:(CalenderObject*)calObject withDateStr:(NSString*)dateStr{
//
//    for (NSDictionary * bookableObj in calObject.bookable){
//        CalenderObject *requestObj = [[CalenderObject alloc] initWithBookableAndRequestObjectDict:bookableObj];
//        if ([requestObj.date isEqualToString:dateStr]){
//            return requestObj.price;
//        }
//    }
//    for (NSDictionary * onRequestObj in calObject.onRequest){
//        CalenderObject *requestObj = [[CalenderObject alloc] initWithBookableAndRequestObjectDict:onRequestObj];
//        if ([requestObj.date isEqualToString:dateStr]){
//            return requestObj.price;
//        }
//    }
//    return 0;
//}
@end
