//
//  ThemeCollObjSRP.h
//  holidays
//
//  Created by Kush_Tech on 20/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ThemeCollObjSRP : NSObject
-(instancetype)initWithThemeObjectDict:(NSDictionary *)dictionary;
-(instancetype)initWithPackageObjectDict:(NSDictionary *)dictionary;
@property(nonatomic,strong)NSDictionary *tcilHolidayThemePK;
@property(nonatomic,strong)NSDictionary *tcilMstHolidayThemes;
@property(nonatomic,strong)NSString *isActive;
@property(nonatomic,strong)NSString *name;


@property(nonatomic,strong)NSString *pkgSubtypeName;
@property(nonatomic)NSInteger pkgSubtypeId;
@property(nonatomic)NSInteger pkgTypeId;

@end

NS_ASSUME_NONNULL_END
