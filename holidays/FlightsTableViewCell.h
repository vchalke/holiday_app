//
//  FlightsTableViewCell.h
//  holidays
//
//  Created by Kush_Tech on 03/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FlightsColectObject.h"
NS_ASSUME_NONNULL_BEGIN

@interface FlightsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbl_defaultMsg;
@property (weak, nonatomic) IBOutlet UILabel *lbl_defaultSubTitle;
@property (weak, nonatomic) IBOutlet UILabel *lbl_defaultDuration;
@property (weak, nonatomic) IBOutlet UIImageView *img_Flights;

@property (strong ,nonatomic) NSString *fdefaultMsg;
- (void)setFilghtsdataSelected:(FlightsColectObject*)flighModel;
@end

NS_ASSUME_NONNULL_END
