//
//  MyNewProfileVC.h
//  holidays
//
//  Created by Kush_Team on 27/05/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BottomView.h"
#import "NewMasterVC.h"
#import "BottomStacksView.h"
#import "LandingScreenVC.h"
NS_ASSUME_NONNULL_BEGIN

@interface MyNewProfileVC : LandingScreenVC
@property (weak, nonatomic) IBOutlet UITableView *table_Views;
//@property (weak, nonatomic) IBOutlet BottomView *botton_view;
@property (weak, nonatomic) IBOutlet BottomStacksView *botton_view;
@end

NS_ASSUME_NONNULL_END
