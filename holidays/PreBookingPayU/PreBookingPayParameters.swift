//
//  PreBookingPayParameters.swift
//  sotc-consumer-application
//
//  Created by Mac on 26/09/18.
//  Copyright © 2018 Mobicule Technologies Private Limited. All rights reserved.
//

import Foundation
class PreBookingPayUParameters : NSObject
{
    var customerName : String = ""
    var emailID : String = ""
    var phoneNo : String = ""
    var amount : String = ""
    override init()
    {
        self.customerName = ""
        self.emailID = ""
        self.phoneNo = ""
        self.amount = ""
    }
}
