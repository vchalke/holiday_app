//
//  SellForexLoginViewController.swift
//  holidays
//
//  Created by Komal Katkade on 11/30/17.
//  Copyright © 2017 Mobicule. All rights reserved.
//

import UIKit

class SellForexLoginViewController: ForexBaseViewController,UITextFieldDelegate,loginCommunicationDelegate,GoogleSignInViewDelegate,SignUpViewControllerDelegate
{
    @IBOutlet weak var textfieldEmailID: UITextField!
    @IBOutlet weak var textfieldPassword: UITextField!
    @IBOutlet weak var existingMemberRadioButton: UIButton!
    @IBOutlet weak var labelTotalEncashmentAmount: UILabel!
    @IBOutlet var sellForexLoginVC: UIView!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var ProceedAsGuestRadioButton: UIButton!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var forgotPasswordButton: UIButton!
    @IBOutlet weak var newUserRegisterButton: UIButton!
    var sellForexBO : SellForexBO =  SellForexBO.init()
    
    // MARK: loging communication delegate
    func guestLoggedInSuccessFully(withUserID userID: String!)
    {
        let sellForxVC : SellerDetailsViewController = SellerDetailsViewController.init(nibName: "ForexBaseViewController", bundle: nil)
        //sellForexBO.emailID = textfieldEmailID.text!
        sellForexBO.emailID = userID
        sellForxVC.sellForexBO = sellForexBO
        self.navigationController?.pushViewController(sellForxVC, animated: true)
    }
    
    func userloggedInSuccessFull(withUserDict userID: [AnyHashable : Any]!)
    {
        let sellForxVC : SellerDetailsViewController = SellerDetailsViewController.init(nibName: "ForexBaseViewController", bundle: nil)
        sellForxVC.sellForexBO = sellForexBO
        self.navigationController?.pushViewController(sellForxVC, animated: true)
    }
    
    func loggedInFail(withRason reasonMessege: String!)
    {
        
    }
    
    func userloggedInSignUpSuccessFull(withUserDict userID: String!)
    {
        let sellForxVC : SellerDetailsViewController = SellerDetailsViewController.init(nibName: "ForexBaseViewController", bundle: nil)
        sellForexBO.emailID = userID
        sellForxVC.sellForexBO = sellForexBO
        self.navigationController?.pushViewController(sellForxVC, animated: true)
    }

    override func viewDidLoad()
    {
        super.viewDidLoad()
        Bundle.main.loadNibNamed("SellForexLoginViewController", owner: self, options: nil)
        
        super.addViewInBaseView(childView: self.sellForexLoginVC)
        super.setUpHeaderLabel(labelHeaderNameText: "Sell Forex")
        
        super.viewDidLoad()
         labelTotalEncashmentAmount.text = sellForexBO.totalAmountWithTax
        textfieldEmailID.delegate = self
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ForexBuyPassangerViewController.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        setupRadioButtons()
        KeyboardAvoiding.setAvoidingView(self.sellForexLoginVC, withTriggerView: self.textfieldEmailID)
        // continueButton.layer.cornerRadius = 10
        
        self.passwordView.isHidden = true
        forgotPasswordButton.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidLayoutSubviews()
    {
        super.setSubViewFrame(childView: self.sellForexLoginVC)
    }
    
    override func viewWillLayoutSubviews()
    {
        super.setSubViewFrame(childView: self.sellForexLoginVC)
    }
    
    func setupRadioButtons() 
    {
        ProceedAsGuestRadioButton.setImage(UIImage.init(named: "ForexRadioButtonOn"), for: UIControl.State.selected)
        ProceedAsGuestRadioButton.setImage(UIImage.init(named: "ForexRadioButtonOff"), for: UIControl.State.normal)
        ProceedAsGuestRadioButton.isSelected = true
        
        existingMemberRadioButton.setImage(UIImage.init(named: "ForexRadioButtonOn"), for: UIControl.State.selected)
        existingMemberRadioButton.setImage(UIImage.init(named: "ForexRadioButtonOff"), for: UIControl.State.normal)
        existingMemberRadioButton.isSelected = false
    }
    
    func validateFields() -> Bool
    {
        if (textfieldEmailID.text?.isEmpty)!
        {
            showAlert(message: "Enter E-mail ID")
            return false
        }
        if !isValidEmail(testStr: textfieldEmailID.text!)
        {
            showAlert(message: "Enter valid E-mail ID")
            return false
        }
        if (textfieldPassword.text?.isEmpty)!
        {
            showAlert(message: "Enter Password")
            return false
        }
        return true
    }
    
    func dismissKeyboard()
    {
        view.endEditing(true)
    }
    
    // MARK: Google Plus
    func signInWithGooglePlus()
    {
        let signInVC : SignInViewController = SignInViewController.init(nibName: "SignInViewController", bundle: nil)
        signInVC.googleViewDelegate = self
//        self.present(signInVC, animated: true, completion: nil)
        self.navigationController?.pushViewController(signInVC, animated: true)
    }
    
    func didFinish(withGoogleLogin userEmailID: String!)
    {
        print("userEmailID---> \(userEmailID)")
        if userEmailID != "error"
        {
            UserDefaults.standard.setValue(userEmailID, forKey: kuserDefaultUserId)
            let LoginStatus: UserDefaults? = UserDefaults.standard
            LoginStatus?.set(kLoginSuccess, forKey: kLoginStatus)
            let userIdForGooglePlusSignIn: UserDefaults? = UserDefaults.standard
            userIdForGooglePlusSignIn?.set(userEmailID, forKey: kLoginEmailId)
        
            let sellForxVC : SellerDetailsViewController = SellerDetailsViewController.init(nibName: "ForexBaseViewController", bundle: nil)
            sellForexBO.emailID = userEmailID
            sellForxVC.sellForexBO = sellForexBO
            self.navigationController?.pushViewController(sellForxVC, animated: true)
        }
    }
    
    func didFinish(withGoogleLoginWithuserData userEmailID :[AnyHashable : Any]!)
    {
        print("userEmailID---> \(userEmailID)")
    }
    
    // MARK:  textfield  delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        self.view.endEditing(true)
        return true
    }
 
    // MARK: button action
    @IBAction func continueButtonClicked(_ sender: Any)
    {
//        if validateFields()
//        {
//            let sellerDetailsViewController : SellerDetailsViewController = SellerDetailsViewController(nibName: "ForexBaseViewController", bundle: nil)
//            sellForexBO.emailID = textfieldEmailID.text!
//            sellerDetailsViewController.sellForexBO = sellForexBO
//            self.navigationController?.pushViewController(sellerDetailsViewController, animated: true)
//        }
        
        let loginCommunication : LoginCommunicationManager = LoginCommunicationManager.init()
        loginCommunication.delegate = self;
        loginCommunication.loginViewController = self;
        
        if ProceedAsGuestRadioButton.isSelected
        {
            if isValidEmail(testStr: textfieldEmailID.text!)
            {
                loginCommunication.loginCheck(withType: "guestLogin", withUserID: textfieldEmailID.text, withPassword: "")
            }
            else
            {
                showAlert(message: "Enter valid email ID")
            }
        }
        else
        {
            if validateFields()
            {
                sellForexBO.emailID = textfieldEmailID.text!
                loginCommunication.loginCheck(withType: "signIn", withUserID: textfieldEmailID.text, withPassword: textfieldPassword.text)
            }
        }
    }
    
    @IBAction func proceedAsguestHelpBtnClicked(_ sender: Any)
    {
        let popupVC: PopUpViewController = PopUpViewController(nibName: "PopUpViewController", bundle: nil)
        popupVC.msg = "Login as a member or register yourself to get special offers."
        popupVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(popupVC, animated: false, completion: nil)
    }
    
    @IBAction func existingMemberHelpBtnClicked(_ sender: Any)
    {
        let popupVC: PopUpViewController = PopUpViewController(nibName: "PopUpViewController", bundle: nil)
        popupVC.msg = "Get special offers as a registered member. Save time by using saved address & travellers details."
        popupVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(popupVC, animated: false, completion: nil)
    }
    
    @IBAction func proceedAsGuestRadioButtonClicked(_ sender: Any)
    {
        self.passwordView.isHidden = true
        ProceedAsGuestRadioButton.isSelected = true
        existingMemberRadioButton.isSelected = false
        forgotPasswordButton.isHidden = true
    }
    
    @IBAction func existingMemberRadioButtonClicked(_ sender: Any)
    {
        self.passwordView.isHidden = false
        ProceedAsGuestRadioButton.isSelected = false
        existingMemberRadioButton.isSelected = true
        forgotPasswordButton.isHidden = false
    }
    
    @IBAction func loginWithGPlus(_ sender: UIButton)
    {
        self.signInWithGooglePlus()
    }
    
    @IBAction func loginWithFacebookClicked(_ sender: UIButton)
    {
        let loginCommunication : LoginCommunicationManager = LoginCommunicationManager.init()
        loginCommunication.delegate = self;
        loginCommunication.loginViewController = self;
        loginCommunication.setUpFacebookLogin()
    }
    
    @IBAction func newUserRegistrationButtonClicked(_ sender: UIButton)
    {
        let signUpVC: SignUpViewController = SignUpViewController(nibName: "SignUpViewController", bundle: nil)
        signUpVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        signUpVC.delegate = self;
        self.present(signUpVC, animated: false, completion: nil)
    }
    
    @IBAction func forgotPasswordButtonClicked(_ sender: UIButton)
    {
        let forgotPassVC: ForgotPassViewController = ForgotPassViewController(nibName: "ForgotPassViewController", bundle: nil)
        forgotPassVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(forgotPassVC, animated: false, completion: nil)
    }
}
