//
//  BuyForexPassengerInfoBO.swift
//  holidays
//
//  Created by Komal Katkade on 12/9/17.
//  Copyright © 2017 Mobicule. All rights reserved.
//

import Foundation
class BuyForexPassengerInfoBO
{
    var travellerArray : [TravellerBO] = []
    var stateOfCustomer : NSString = ""
    var branchOfCustomer : NSString = ""
    var gstStateCode : NSString = ""
    var branchCode : NSString = ""
    var gstBranchCode : NSString = ""
    var totalAmount : NSInteger = 0
    var isUnionTerritoryState : NSString = ""
    var isUnionTerritoryBranch : NSString = ""
    var freeDeliveryCheckAMount : NSInteger = 0
    init(travellerArray:NSArray,stateOfCustomer:NSString,branchOfCustomer:NSString,totalAmount:NSInteger,stateCode: NSString,branchCode: NSString,isUnionTerritoryState: NSString,isUnionTerritoryBranch: NSString,gstBranchCode : NSString )
    {
        self.travellerArray = travellerArray as! [TravellerBO]
        self.stateOfCustomer = stateOfCustomer
        self.branchOfCustomer = branchOfCustomer
        self.totalAmount = totalAmount
         self.gstStateCode = stateCode
         self.branchCode = branchCode
        self.gstBranchCode = gstBranchCode
         self.isUnionTerritoryState = isUnionTerritoryState
         self.isUnionTerritoryBranch = isUnionTerritoryBranch
    }
}
