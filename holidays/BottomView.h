//
//  BottomView.h
//  Holiday
//
//  Created by Kush Thakkar on 15/02/20.
//  Copyright © 2020 Kush Thakkar. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@protocol BottomViewDelegaete <NSObject>
@optional
- (void)homeButtonClick;
- (void)bookingButtonClick;
- (void)profileButtonClick;
- (void)notificationButtonClick;
- (void)moreButtonClick;
@end
@interface BottomView : UIView {
    NSArray *imgArray;
    NSArray *selectImgArray;
    NSArray *imgObjArray;
    NSArray *lblObjArray;
    
}
@property (nonatomic, weak) id <BottomViewDelegaete> bottomDelegate;
@property (weak, nonatomic) IBOutlet UIImageView *img_home;
@property (weak, nonatomic) IBOutlet UIImageView *img_booking;
@property (weak, nonatomic) IBOutlet UIImageView *img_myAccount;
@property (weak, nonatomic) IBOutlet UIImageView *img_notification;
@property (weak, nonatomic) IBOutlet UIImageView *img_more;

@property (weak, nonatomic) IBOutlet UILabel *lbl_home;
@property (weak, nonatomic) IBOutlet UILabel *lbl_booking;
@property (weak, nonatomic) IBOutlet UILabel *lbl_myAccount;
@property (weak, nonatomic) IBOutlet UILabel *lbl_notification;
@property (weak, nonatomic) IBOutlet UILabel *lbl_more;

-(void)buttonSelected:(NSInteger)index;

@end

NS_ASSUME_NONNULL_END
