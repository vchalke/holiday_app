//
//  CoreDataSingleton.m
//  holidays
//
//  Created by Kush_Tech on 12/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "CoreDataSingleton.h"


@implementation CoreDataSingleton
+(id)sharedInstance {
    //+ (Contact *)sharedInstance {
    static CoreDataSingleton *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
        
    });
    return sharedInstance;
}

- (id)init {
    if (self = [super init]) {
        app_delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    }
    return self;
}
-(NSManagedObjectContext*)getManagedObjectContext{
    return [app_delegate managedObjectContext];
}
-(NSFetchRequest*)getFetchRequesrForEntityName:(NSString*)entityName{
    NSFetchRequest *request = [[NSFetchRequest alloc]initWithEntityName:entityName];
    return request;
}
-(NSArray*)getArrayOfObjectForEntityName:(NSString*)entityName{
    NSManagedObjectContext *managedobjectcontext = [self getManagedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:entityName inManagedObjectContext:managedobjectcontext]];
    NSError *error = Nil;
    id whatAreYou = [managedobjectcontext executeRequest:request error:&error];
    NSMutableArray *arrayData= [[whatAreYou finalResult] mutableCopy];
    NSLog(@"%@",arrayData);
//    for (NSManagedObject *managedObject in arrayData) {
//        NSLog(@"%@ %@ %@ %@",[managedObject valueForKey:@"packageID"],  [managedObject valueForKey:@"packageInfo"], [managedObject valueForKey:@"packageName"], [managedObject valueForKey:@"packagePrize"]);
//    }
    NSArray *results = [[self getManagedObjectContext] executeFetchRequest:[self getFetchRequesrForEntityName:entityName] error:&error];
    return results;
}
-(void)saveRecentObjectInCoreDataForEntity:(NSString*)entityName withObject:(RecentHolidayObject*)recentObj{
    NSManagedObjectContext *context = [self getManagedObjectContext];
    NSManagedObject *newContext = [NSEntityDescription insertNewObjectForEntityForName:entityName inManagedObjectContext:context];
    NSLog(@"%@",recentObj.packageID);
    NSLog(@"%@",recentObj.packageName);
    [newContext setValue:recentObj.packageID forKey:@"packageID"];
    [newContext setValue:recentObj.packageName forKey:@"packageName"];
    [newContext setValue:recentObj.packageImgUrl forKey:@"packageImgUrl"];
    [newContext setValue:[NSNumber numberWithInteger:recentObj.packagePrize] forKey:@"packagePrize"];
    NSError *error = Nil;
    if (![context save:&error]){
        NSLog(@"Save Failed %@ %@",error,[error localizedDescription]);
    }else{
        NSLog(@"Save Succeed");
    }
}
-(void)saveWishListObjectInCoreDataForEntity:(NSString*)entityName withObject:(WishListHolidayObject*)wishListObj{
    NSManagedObjectContext *context = [self getManagedObjectContext];
    NSManagedObject *newContext = [NSEntityDescription insertNewObjectForEntityForName:entityName inManagedObjectContext:context];
    [newContext setValue:wishListObj.packageData forKey:@"packageData"];
    [newContext setValue:wishListObj.packageID forKey:@"packageID"];
    [newContext setValue:wishListObj.packageName forKey:@"packageName"];
    [newContext setValue:wishListObj.packageImgUrl forKey:@"packageImgUrl"];
    [newContext setValue:[NSNumber numberWithInteger:wishListObj.packagePrize] forKey:@"packagePrize"];
    NSError *error = Nil;
    if (![context save:&error]){
        NSLog(@"Save Failed %@ %@",error,[error localizedDescription]);
    }else{
        NSLog(@"Save Succeed");
    }
}
-(void)saveCompareListObjectInCoreDataForEntity:(NSString*)entityName withObject:(CompareHolidayObject*)compareListObj{
    NSManagedObjectContext *context = [self getManagedObjectContext];
    NSManagedObject *newContext = [NSEntityDescription insertNewObjectForEntityForName:entityName inManagedObjectContext:context];
    [newContext setValue:compareListObj.packageData forKey:@"packageData"];
    [newContext setValue:compareListObj.packageID forKey:@"packageID"];
    [newContext setValue:compareListObj.packageName forKey:@"packageName"];
    [newContext setValue:compareListObj.packageImgUrl forKey:@"packageImgUrl"];
    [newContext setValue:[NSNumber numberWithInteger:compareListObj.packagePrize] forKey:@"packagePrize"];
    NSError *error = Nil;
    if (![context save:&error]){
        NSLog(@"Save Failed %@ %@",error,[error localizedDescription]);
    }else{
        NSLog(@"Save Succeed");
    }
}
-(BOOL)checkIsPackagePresentInEntityWithName:(NSString*)entityName withPackageID:(NSString*)packageString{
    NSArray *array = [self getArrayOfObjectForEntityName:entityName];
    if ([entityName isEqualToString:RecentViewHoliday]){
        for (RecentHolidayObject *obj in array){
            if ([packageString isEqualToString:obj.packageID]){
                return true;
            }
        }
    }else if ([entityName isEqualToString:WishListHoliday]){
        for (WishListHolidayObject *obj in array){
            if ([packageString isEqualToString:obj.packageID]){
                return true;
            }
        }
    }else if ([entityName isEqualToString:CompareListHoliday]){
        for (CompareHolidayObject *obj in array){
            if ([packageString isEqualToString:obj.packageID]){
                return true;
            }
        }
    }
    return false;
}
-(NSArray*)getdeletedObjectFromPackageListArray:(NSArray*)wishListsArray withPackID:(NSString*)packageString forEntityName:(NSString*)entityName{
    NSArray *array = [[NSArray alloc] init];
    array = [wishListsArray filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id  _Nullable evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
        
        if ([entityName isEqualToString:RecentViewHoliday]){
            RecentHolidayObject *obj = (RecentHolidayObject*)evaluatedObject;
            return [obj.packageID isEqualToString:packageString];
        }else if ([entityName isEqualToString:WishListHoliday]){
            WishListHolidayObject *obj = (WishListHolidayObject*)evaluatedObject;
            return [obj.packageID isEqualToString:packageString];
        }
        CompareHolidayObject *obj = (CompareHolidayObject*)evaluatedObject;
        return [obj.packageID isEqualToString:packageString];
    }]];
    return array;
}
- (void)deleteEntitiesWithName:(NSString *)entityName {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:entityName];
    [fetchRequest setIncludesPropertyValues:NO];
    NSManagedObjectContext *theContext = [self getManagedObjectContext];
    NSError *error;
    NSArray *fetchedObjects = [theContext executeFetchRequest:fetchRequest error:&error];
    for (NSManagedObject *object in fetchedObjects)
    {
        [theContext deleteObject:object];
    }
    error = nil;
    [theContext save:&error];
}
@end
