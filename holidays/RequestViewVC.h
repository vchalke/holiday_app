//
//  RequestViewVC.h
//  holidays
//
//  Created by Kush_Team on 25/06/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPKeyboardAvoidingScrollView.h"
#import "HolidayPackageDetail.h"
#import "NewMasterVC.h"
NS_ASSUME_NONNULL_BEGIN

@interface RequestViewVC : NewMasterVC
@property (strong ,nonatomic) HolidayPackageDetail *requestHolidayPackage;
@property (weak, nonatomic) IBOutlet UITextField *txt_PhoneNumber;
@property (weak, nonatomic) IBOutlet UITextField *txt_EmailIId;
@property (weak, nonatomic) IBOutlet UITextField *txt_Summery;
@end

NS_ASSUME_NONNULL_END
