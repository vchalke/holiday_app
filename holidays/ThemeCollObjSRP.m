//
//  ThemeCollObjSRP.m
//  holidays
//
//  Created by Kush_Tech on 20/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "ThemeCollObjSRP.h"

@implementation ThemeCollObjSRP
-(instancetype)initWithThemeObjectDict:(NSDictionary *)dictionary{
    if ([super init])
    { 
        self.tcilHolidayThemePK = [dictionary valueForKey:@"tcilHolidayThemePK"];
        self.tcilMstHolidayThemes = [dictionary valueForKey:@"tcilMstHolidayThemes"];
        self.isActive = [dictionary valueForKey:@"isActive"];
        self.name = [self.tcilMstHolidayThemes valueForKey:@"name"];
    }
    return self;
}
-(instancetype)initWithPackageObjectDict:(NSDictionary *)dictionary{
    if ([super init])
    {
        self.pkgSubtypeName = [dictionary valueForKey:@"pkgSubtypeName"];
        self.pkgTypeId = [[dictionary valueForKey:@"pkgTypeId"] integerValue];
        self.pkgSubtypeId = [[dictionary valueForKey:@"pkgSubtypeId"] integerValue];
        
    }
    return self;
}
@end
