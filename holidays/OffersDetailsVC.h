//
//  OffersDetailsVC.h
//  holidays
//
//  Created by Kush_Team on 01/09/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Holiday.h"
NS_ASSUME_NONNULL_BEGIN

@interface OffersDetailsVC : UIViewController
@property (strong, nonatomic) Holiday *holidayInOffers;
@end

NS_ASSUME_NONNULL_END
