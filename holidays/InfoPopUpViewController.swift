//
//  InfoPopUpViewController.swift
//  sotc-consumer-application
//
//  Created by Saurav Kumar on 26/09/17.
//  Copyright © 2017 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit

class InfoPopUpViewController: UIViewController {

    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var viewTitle: UILabel!
    
    @IBOutlet weak var viewTitleHeightConstasints: NSLayoutConstraint!
    var viewTilteString:String = ""
    
    var descriptionString:String =  ""
    var moduleName:String = ""
    
    
      var presentingView:UIViewController? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.viewTitle.text = viewTilteString
        self.descriptionTextView.text = descriptionString
        
        if moduleName == "optionalinfo"
        {
            viewTitleHeightConstasints.constant = 0
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
