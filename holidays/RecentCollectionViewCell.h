//
//  RecentCollectionViewCell.h
//  holidays
//
//  Created by Kush_Tech on 24/02/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RecentHolidayObject.h"
NS_ASSUME_NONNULL_BEGIN

@interface RecentCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbl_title;
@property (weak, nonatomic) IBOutlet UILabel *lbl_prize;
@property (weak, nonatomic) IBOutlet UIImageView *packageImgView;

-(void)loadObjectInCell:(RecentHolidayObject*)recentObj;
@end

NS_ASSUME_NONNULL_END
