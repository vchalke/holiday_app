//
//  AppDelegate.h
//  holidays
//
//  Created by vaibhav on 07/08/15.
//  Copyright (c) 2015 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleSignIn/GoogleSignIn.h>

#import "MyProfileVc.h"

#import <UserNotifications/UserNotifications.h>
#import <UserNotificationsUI/UserNotificationsUI.h>

#import <NetCorePush/NetCorePush.h>



@interface AppDelegate : UIResponder <UIApplicationDelegate, UNUserNotificationCenterDelegate, GIDSignInDelegate,GIDSignInUIDelegate,NetCorePushTaskManagerDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;


- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

- (NSManagedObjectContext *)managedObjectContext ;
- (void) registerWithCMSServer;

@property (strong,nonatomic) NSURL *currentDeepLink;

@end

