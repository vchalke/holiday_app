//
//  Deviation+CoreDataProperties.swift
//  holidays
//
//  Created by Saurav on 28/11/17.
//  Copyright © 2017 Mobicule. All rights reserved.
//

import Foundation
import CoreData


extension Deviation {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Deviation> {
        return NSFetchRequest<Deviation>(entityName: "Deviation")
    }

    @NSManaged public var bfNumber: String?
    @NSManaged public var newDate: NSDate?
    @NSManaged public var passengerNumber: String?
    @NSManaged public var sector: String?
    @NSManaged public var startDate: NSDate?
    @NSManaged public var status: String?
    @NSManaged public var tourRelation: NSSet?

}

// MARK: Generated accessors for tourRelation
extension Deviation {

    @objc(addTourRelationObject:)
    @NSManaged public func addToTourRelation(_ value: Tour)

    @objc(removeTourRelationObject:)
    @NSManaged public func removeFromTourRelation(_ value: Tour)

    @objc(addTourRelation:)
    @NSManaged public func addToTourRelation(_ values: NSSet)

    @objc(removeTourRelation:)
    @NSManaged public func removeFromTourRelation(_ values: NSSet)

}
