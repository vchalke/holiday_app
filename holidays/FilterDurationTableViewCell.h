//
//  FilterDurationTableViewCell.h
//  holidays
//
//  Created by Kush_Team on 24/06/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FilterDurationTableViewCell : UITableViewCell
{
    NSString *filterDurationDay;
}
@property (weak, nonatomic) IBOutlet UIButton *btn_first;
@property (weak, nonatomic) IBOutlet UIButton *btn_Second;
@property (weak, nonatomic) IBOutlet UIButton *btn_Third;
@property (weak, nonatomic) IBOutlet UIButton *btn_Fourth;
-(void)setupTableViewCell;
@property (strong,nonatomic) NSMutableDictionary *filterDurationDict;
@end

NS_ASSUME_NONNULL_END
