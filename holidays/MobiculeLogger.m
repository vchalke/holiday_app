

#import "MobiculeLogger.h"

@implementation MobiculeLogger

static MobiculeLogger *uniqueInstance = nil;

+ (MobiculeLogger *) getInstance
{
    @synchronized(self)
    {
        if (uniqueInstance == nil)
        {
            uniqueInstance = [[self alloc] init];
        }
    }
    
    return uniqueInstance;
}

+ (id) allocWithZone:(NSZone *) zone
{
    @synchronized(self)
    {
        if (uniqueInstance == nil)
        {
            uniqueInstance = [super allocWithZone:zone];
            return uniqueInstance;
        }
    }
    return nil;
}

- (id)copyWithZone:(NSZone *)zone
{
    return self;
}


-(void)logEvent:(NSString *)event
  withClassName:(NSString *)className
  andMethodName:(NSString *)methodName
  andLineNumber:(int)lineNumber
 andStartMarker:(NSString *)startMarker
   andEndMarker:(NSString *)endmarker
       andInput:(NSString*)input, ...;
{
    
    va_list argList;
    NSString *formatStr;
    
    va_start(argList, input);
    formatStr = [[NSString alloc] initWithFormat:input arguments:argList];
    va_end(argList);
    
    
    NSLog(@"%@%@[%@:%@:%d] :\n%@\%@", startMarker, event, className, methodName, lineNumber, formatStr, endmarker);
    
}

-(void) setDebugEnabled:(BOOL) enable
{
    _isDebugEnabled = enable;
}

-(BOOL) isLogEnabled
{
    return _isDebugEnabled;
}

@end
