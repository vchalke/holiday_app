//
//  UserBookingNetworkManager.swift
//  sotc-consumer-application
//
//  Created by Saurav Kumar on 06/09/17.
//  Copyright © 2017 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit

class UserBookingNetworkManager: NSObject ,XMLParserDelegate,URLSessionDelegate{
    
    lazy var xmlResponse:String = ""
    
       
    func sendSoapRequest(soapRequestString:String,soapAction:String, completion: @escaping (_ response:OperationValidationMessage) -> Void) {
        
        let urlString = String(format: AuthenticationConstant.CheckMobileURL)
        
        let serviceUrl = URL(string: urlString)
        
        //        let loginParams = String(format: LOGIN_PARAMETERS1, "test", "Hi World")
        
        
        
        var request = URLRequest(url: serviceUrl!)
        
        request.httpMethod = "POST"
        
        request.setValue("application/xml", forHTTPHeaderField: "Content-Type")
        request.setValue("\(urlString)#\(soapAction)", forHTTPHeaderField: "SOAPAction")
        
        
        request.httpBody = soapRequestString .data(using: .utf8)
        
       // let session = URLSession.shared
        
          let session = URLSession(configuration: .default, delegate: self, delegateQueue: nil)
        
        session.dataTask(with: request) { (data, response, error) in
            
              var opeationMessage = OperationValidationMessage()
            
            
            if let error = error{
                
                
                printLog(error)
                
                opeationMessage.status = false
                opeationMessage.message = error.localizedDescription
                
                completion(opeationMessage)
                
            }else if response != nil {
               
                let statusCode = (response as! HTTPURLResponse).statusCode
                
                if statusCode == 200
                {
                    
                    if let data1 = data {
                        
                        // let xmlString1:String = String(data: data1, encoding: String.Encoding.utf8)!
                        
                        let parser:XMLParser = XMLParser(data: data1)
                        
                        parser.delegate = self
                        parser.parse()
                        
                       
                        printLog(self.xmlResponse)
                        
                        
                        
                        opeationMessage.data = self.xmlResponse
                        opeationMessage.message = "Data Fetch SuccessFully"
                        opeationMessage.status = true
                        opeationMessage.statusCode = 200
                        
                        
                       printLog(opeationMessage)
                        
                        completion(opeationMessage)
                        
                        
                    }
                    else
                    {
                        opeationMessage.data = ""
                        opeationMessage.message = "Failed to fetch data"
                        opeationMessage.status = false
                        opeationMessage.statusCode = statusCode
                        
                         printLog(opeationMessage)
                        
                             completion(opeationMessage)
                        
                    }
                }
                else
                {
                    opeationMessage.data = ""
                    opeationMessage.message = "Failed to fetch data"
                    opeationMessage.status = false
                    opeationMessage.statusCode = statusCode
                    
                     printLog(opeationMessage)
                    
                         completion(opeationMessage)
                    
                    
                }
            }
            else
            {
                opeationMessage.data = ""
                opeationMessage.message = "Failed to fetch data"
                opeationMessage.status = false
                opeationMessage.statusCode = 0
                
                 printLog(opeationMessage)
                
                completion(opeationMessage)
            }
            
            
            
            }.resume()
    }
    //Mark: Request with middleware
   
    
    func sendSoapRequestMiddleware(soapRequestString:String,soapAction:String, requestKey:String, completion: @escaping (_ response:OperationValidationMessage) -> Void) {
        
        let urlString = String(format: AuthenticationConstant.CheckMobileURL)
        
        let serviceUrl = URL(string: urlString)
        
        //        let loginParams = String(format: LOGIN_PARAMETERS1, "test", "Hi World")
        
        
        var opeationMessage = OperationValidationMessage()
        var request = URLRequest(url: serviceUrl!)
        
        request.httpMethod = "POST"
        
        request.setValue("application/xml", forHTTPHeaderField: "Content-Type")
        request.setValue("\(urlString)#\(soapAction)", forHTTPHeaderField: "SOAPAction")
        // request.setValue("", forHTTPHeaderField: "digest");
        
        
        request.httpBody = soapRequestString .data(using: .utf8)
        
        
        getDataViaMiddleware(bookListRequest: soapRequestString,requestFor: requestKey){ (Status, data, xmlStr) in
            
            printLog("Status--->\(Status)")
            printLog("Status--->\(data)")
            
            let parser:XMLParser = XMLParser(data: data)
            
            parser.delegate = self
            parser.parse()
            
            
            printLog(self.xmlResponse)
            
            
            
            opeationMessage.data = xmlStr
            opeationMessage.message = "Data Fetch SuccessFully"
            opeationMessage.status = true
            opeationMessage.statusCode = 200
            
            
            printLog(opeationMessage)
            
            completion(opeationMessage)
        }
        
        
        /*  let session = URLSession.shared
         session.dataTask(with: request) { (data, response, error) in
         
         var opeationMessage = OperationValidationMessage()
         
         
         if let error = error{
         
         
         
         printLog(error)
         
         opeationMessage.status = false
         opeationMessage.message = error.localizedDescription
         
         completion(opeationMessage)
         
         }else if response != nil {
         
         let statusCode = (response as! HTTPURLResponse).statusCode
         
         if statusCode == 200
         {
         
         if let data1 = data {
         
         // let xmlString1:String = String(data: data1, encoding: String.Encoding.utf8)!
         
         let parser:XMLParser = XMLParser(data: data1)
         
         parser.delegate = self
         parser.parse()
         
         
         printLog(self.xmlResponse)
         
         
         
         opeationMessage.data = self.xmlResponse
         opeationMessage.message = "Data Fetch SuccessFully"
         opeationMessage.status = true
         opeationMessage.statusCode = 200
         
         
         printLog(opeationMessage)
         
         completion(opeationMessage)
         
         
         }
         else
         {
         opeationMessage.data = ""
         opeationMessage.message = "Failed to fetch data"
         opeationMessage.status = false
         opeationMessage.statusCode = statusCode
         
         printLog(opeationMessage)
         
         completion(opeationMessage)
         
         }
         }
         else
         {
         opeationMessage.data = ""
         opeationMessage.message = "Failed to fetch data"
         opeationMessage.status = false
         opeationMessage.statusCode = statusCode
         
         printLog(opeationMessage)
         
         completion(opeationMessage)
         
         
         }
         }
         else
         {
         opeationMessage.data = ""
         opeationMessage.message = "Failed to fetch data"
         opeationMessage.status = false
         opeationMessage.statusCode = 0
         
         printLog(opeationMessage)
         
         completion(opeationMessage)
         }
         
         
         
         }.resume()*/
    }
    
    //MARK:-middleware request:
    
    func getDataViaMiddleware(bookListRequest:String,requestFor:String,completion : @escaping(_ status:Bool, _ data:Data, _ xmlStr:String) -> Void) {
        
        
        let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        
        
        //"BOOKING_FILE_LIST_RESPONSE"
        let data:Array<Dictionary<String,Any>> = [["request":bookListRequest,"searchKey":requestFor]]
        
        //var itienearyCode:Array<String> = ["2017EUR103S1"]
        
        let  queryParameterMap:Dictionary<String,Any> = ["soapaction":""]
        
        let userDetails:Dictionary<String,Any> = [APIConstants.CheckVersionKey:"false",
                                                  APIConstants.Client:JsonConstant.IOS_CLIENT,
                                                  APIConstants.AppVersion:version!]
        
        let requestDict:Dictionary<String,Any> = [ APIConstants.EntityType:"transaction",
                                                   APIConstants.EntityAction:"get",
                                                   APIConstants.Entity:"middleWareWebservice",
                                                   "identifier":"contentMiddleWareWebservice",
                                                   APIConstants.User:userDetails,"data":data,"queryParameterMap":queryParameterMap]
        
        
        
        
        let requestString = requestDict.jsonRepresentation()
        
        
        printLog("requestDict:\(requestDict)")
        
        let digest:String = NetworkUtility().generateDigestValue(requestString, withPackageName: "com.mobicule")
        printLog("digest:\(digest)")
        
        let headers: HTTPHeaders = [
            "content-type": "application/json",
            "Authorization": AuthenticationConstant.Mobicule_Authorization,
            "digest":digest
        ]
        //AuthenticationConstant.MOBICULE_URL
        //http://172.98.192.15:8081/Mobicule-Platform_MiddleWare_SOTC/api/
        request(AuthenticationConstant.MOBICULE_URL, method: .post, parameters: requestDict, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            
            
            printLog("Request: \(String(describing: response.request))")
            printLog("Request httpbody: \(String(describing: response.request?.httpBody))")   // original url request
            printLog("Response: \(String(describing: response.response))") // http url response
            printLog("Result: \(response.result)")                         // response serialization result
            var xmldata:Data?
            if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                printLog("Data: \(utf8Text)") // original server data as UTF8 string
            }
            
            
            if let json = response.result.value {
                printLog("JSON: \(json)") // serialized json response
                
                if let responseData:Dictionary<String,Any> = json as? Dictionary<String,Any>
                {
                    // printLog(responseData["data"])
                    
                    
                    let dict:Dictionary<String,Any> = responseData["data"] as! Dictionary<String, Any>
                    
                    xmldata = (dict["newResponse"] as! String).data(using: .utf8)
                    let xmlString = dict["newResponse"] as! String
                    
                    printLog(xmlString)
                    
                    
                    completion(true,xmldata!,xmlString)
                    
                    return
                    
                }
                
            }
            completion(false,xmldata ?? Data()," ")
        }
    }
    
    func sendSoapgetCurrentFxRatesRequest(soapRequestString:String,soapAction:String, completion: @escaping (_ responseDataString:String) -> Void) {
        
        let urlString = String(format: "http://uat2.thomascook.in/tcportal1.0/services/ForexService?wsdl")
        
        let serviceUrl = URL(string: urlString)
        
        //        let loginParams = String(format: LOGIN_PARAMETERS1, "test", "Hi World")
        
        
        
        var request = URLRequest(url: serviceUrl!)
        
        request.httpMethod = "POST"
        
        request.setValue("application/xml", forHTTPHeaderField: "Content-Type")
        request.setValue("\(urlString)#\(soapAction)", forHTTPHeaderField: "SOAPAction")
        
        
        request.httpBody = soapRequestString .data(using: .utf8)
        
      //  let session = URLSession.shared
        
          let session = URLSession(configuration: .default, delegate: self, delegateQueue: nil)
        
        session.dataTask(with: request) { (data, response, error) in
            
            if response != nil {
                
                
                let statusCode = (response as! HTTPURLResponse).statusCode
                
                if statusCode == 200
                    
                {
                    
                    if let data1 = data {
                        
                        // let xmlString1:String = String(data: data1, encoding: String.Encoding.utf8)!
                        
                        let parser:XMLParser = XMLParser(data: data1)
                        
                        parser.delegate = self
                        parser.parse()
                        
                        completion(self.xmlResponse)
                        
                        
                    }
                    else
                    {
                        completion("error")
                    }
                }
                else
                {
                    completion("error")
                }
            }
            else
            {
                completion("error")
            }
            
            
            
            }.resume()
    }
    
    
    class func convertJsonStringIntoDictionary(jsonString : String) -> Dictionary<String,AnyObject> {
        
        
        if let data = jsonString.data(using: .utf8) {
            
            let json = try? JSONSerialization.jsonObject(with: data, options: [])
            
            if let dictionary : Dictionary<String,AnyObject> = json as? [String: AnyObject] {
                
                var dictionaryToBeReturn : Dictionary<String,AnyObject> = [String:AnyObject]()
                
                
                for (key,value) in dictionary {
                    
                    
                    if let value1 : [AnyObject] = value as? [AnyObject] {
                        
                        
                        var innerArray : [AnyObject] =   [AnyObject]()
                        
                        for contentItemObject in value1 {
                            
                            var dictionary1 : Dictionary<String,String> = [String:String]()
                            
                            
                            if let value2 : Dictionary<String,AnyObject> = contentItemObject as? [String: AnyObject] {
                                
                                for (key2,value3) in value2 {
                                    
                                    
                                    if let value4 : String = value3 as? String {
                                        
                                        dictionary1[key2] = value4 ;
                                    }
                                    else
                                    {
                                        dictionary1[key2] = "\(value3)";
                                    }
                                    
                                }
                                
                                innerArray.append(dictionary1 as AnyObject)
                            }
                            
                        }
                        
                        dictionaryToBeReturn[key] = innerArray as AnyObject;
                        
                        
                    }
                    else
                    {
                        
                        dictionaryToBeReturn[key] = value as? String as AnyObject;
                    }
                    
                    
                    
                }
                
                
                return dictionaryToBeReturn
                
            }
        }
        
        let dictionary :Dictionary? = [String:AnyObject]()
        
        return dictionary!
        
        
    }
    
    
    
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        
        if (challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust) {
            if let serverTrust = challenge.protectionSpace.serverTrust {
                var secresult = SecTrustResultType.invalid
                let status = SecTrustEvaluate(serverTrust, &secresult)
                
                if (errSecSuccess == status) {
                    if let serverCertificate = SecTrustGetCertificateAtIndex(serverTrust, 0) {
                        let serverCertificateData = SecCertificateCopyData(serverCertificate)
                        let data = CFDataGetBytePtr(serverCertificateData);
                        let size = CFDataGetLength(serverCertificateData);
                        let cert1 = NSData(bytes: data, length: size)
                        
                        for singleCertificate in AuthenticationConstant.SSLCertificates {
                            
                            let file_der = Bundle.main.path(forResource: singleCertificate, ofType: "cer")
                            
                            if let file = file_der {
                                if let cert2 = NSData(contentsOfFile: file) {
                                    if cert1.isEqual(to: cert2 as Data) {
                                        completionHandler(URLSession.AuthChallengeDisposition.useCredential, URLCredential(trust:serverTrust))
                                        return
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
        // Pinning failed
        completionHandler(URLSession.AuthChallengeDisposition.cancelAuthenticationChallenge, nil)
        
    }
    
    public func parserDidStartDocument(_ parser: XMLParser)
    {
        printLog("************Started")
    }
    
    
    public func parserDidEndDocument(_ parser: XMLParser)
    {
        printLog("***********End*************")
        
        // delegate?.sendSoapResponseData(responseData: xmlResponse)
        
        // printLog(xmlResponse)
        
    }
    
    
    public func parser(_ parser: XMLParser, foundNotationDeclarationWithName name: String, publicID: String?, systemID: String?)
    {
        
    }
    
    public func parser(_ parser: XMLParser, foundUnparsedEntityDeclarationWithName name: String, publicID: String?, systemID: String?, notationName: String?)
    {
        
    }
    
    
    public func parser(_ parser: XMLParser, foundAttributeDeclarationWithName attributeName: String, forElement elementName: String, type: String?, defaultValue: String?)
    {
        
    }
    
    
    public func parser(_ parser: XMLParser, foundElementDeclarationWithName elementName: String, model: String)
    {
        
    }
    
    
    public func parser(_ parser: XMLParser, foundInternalEntityDeclarationWithName name: String, value: String?)
    {
        
    }
    
    
    public func parser(_ parser: XMLParser, foundExternalEntityDeclarationWithName name: String, publicID: String?, systemID: String?)
    {
        
    }
    
    public func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:])
    {
        printLog(elementName)
    }
    
    
    public func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?)
    {
        printLog(elementName)
    }
    
    
    public func parser(_ parser: XMLParser, didStartMappingPrefix prefix: String, toURI namespaceURI: String)
    {
        printLog(prefix)
    }
    
    
    public func parser(_ parser: XMLParser, didEndMappingPrefix prefix: String)
    {
        printLog(prefix)
    }
    
    
    public func parser(_ parser: XMLParser, foundCharacters string: String)
    {
        
        xmlResponse.append(string)
    }
    
    
    public func parser(_ parser: XMLParser, foundIgnorableWhitespace whitespaceString: String)
    {
        
    }
    
    
    public func parser(_ parser: XMLParser, foundProcessingInstructionWithTarget target: String, data: String?)
    {
        
    }
    
    
    public func parser(_ parser: XMLParser, foundComment comment: String)
    {
        
    }
    
    
    public func parser(_ parser: XMLParser, foundCDATA CDATABlock: Data)
    {
        
    }
    
    //
    //    public func parser(_ parser: XMLParser, resolveExternalEntityName name: String, systemID: String?) -> Data?
    //    {
    //
    //    }
    
    
    public func parser(_ parser: XMLParser, parseErrorOccurred parseError: Error)
    {
        printLog(parseError.localizedDescription)
    }
    
    public func parser(_ parser: XMLParser, validationErrorOccurred validationError: Error)
    {
        printLog(validationError.localizedDescription)
    }
    
}
