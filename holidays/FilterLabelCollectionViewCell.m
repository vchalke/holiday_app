//
//  FilterLabelCollectionViewCell.m
//  holidays
//
//  Created by Kush_Team on 24/06/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "FilterLabelCollectionViewCell.h"
#import "ColourClass.h"
@implementation FilterLabelCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.baseViews.layer.cornerRadius = self.baseViews.frame.size.height*0.5;
}
-(void)setButtonColour:(BOOL)flag{
    [self.btn_view setTitleColor:(flag) ? [ColourClass colorWithHexString:@"FFFFFF"] : [ColourClass colorWithHexString:@"4D4D4D"] forState:UIControlStateNormal];
    self.baseViews.backgroundColor = (flag) ? [ColourClass colorWithHexString:@"0046A0"] : [ColourClass colorWithHexString:@"FFFFFF"];
    
}
//0046A0 Blue
//4D4D4D gray
@end
