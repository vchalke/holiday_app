//
//  SellForexProductViewController.swift
//  holidays
//
//  Created by Komal Katkade on 11/29/17.
//  Copyright © 2017 Mobicule. All rights reserved.
//

import UIKit

class SellForexProductViewController: ForexBaseViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,SellForexTableViewCellDelegate
{
    @IBOutlet weak var heightConstraintOfColumnHeaders: NSLayoutConstraint!
    @IBOutlet weak var heightConstraintOfToplabel: NSLayoutConstraint!
    
    @IBOutlet weak var textFieldMobileNumber: UITextField!
    @IBOutlet var sellForexProductView: UIView!
    @IBOutlet weak var sellForexTableView: UITableView!
    
    @IBOutlet weak var buttonSellForex: UIButton!
    //@IBOutlet weak var sellForexScrollView: UIScrollView!
    @IBOutlet weak var sellForexScrollView: TPKeyboardAvoidingScrollView!
    @IBOutlet weak var heightConstraintOfSellForexTableview: NSLayoutConstraint!
    @IBOutlet weak var contactDetailsView: UIView!
    @IBOutlet var sellForexView: UIView!
    @IBOutlet weak var textFieldCustomerState: UITextField!
    
    @IBOutlet weak var textFieldBranch: UITextField!
    
    @IBOutlet weak var activeTextField: UITextField?
    
    
    var gstBranchCode : String = ""
    var branchCode : String = ""
    var gstStateCode : String = ""
    var isUnionTerritoryBranch : String = ""
    var isUnionTerritoryState : String = ""
    
    
    let largeProductViewHeight : NSInteger = 319
    let minimizedProductViewHeght : NSInteger = 40
    let tableviewHeaderHeight : NSInteger = 30
    let tableviewFooterHeight : NSInteger = 60
    let deleteButtonViewHeight : NSInteger = 40
    let minimizedTravellerViewHeight : NSInteger = 40
    let stateBranchViewHeight : NSInteger = 185
    var contactDetailViewHeight : NSInteger = 460
    var topLabelHeight : NSInteger = 40
    var columnHeaderHeight : NSInteger = 40

    
    var productArray : [ProductBO] = []
    var currencyArray : [NSDictionary] = []
    var arrayForProductList : [NSDictionary] = [];
    var arrayForCustomerState : [NSDictionary] = [];
    var arrayForBranch : [NSDictionary] = [];
    var arrayForGSTBranch : [NSDictionary] = [];
    var sellForexBO : SellForexBO =  SellForexBO.init()
    var previousScreen : String = ""
    var overallTravellerLimit : NSInteger = 250000
    var previousController : String = ""
    var dollarRate : Float = 0.0
    override func viewDidLoad()
    {
        Bundle.main.loadNibNamed("SellForexProductViewController", owner: self, options: nil)
        
        super.addViewInBaseView(childView: self.sellForexProductView)
        super.setUpHeaderLabel(labelHeaderNameText: "Sell Forex")
        super.viewDidLoad()
        self.sellForexTableView.register(UINib(nibName: "sellForexproduxtViewCell", bundle: nil), forCellReuseIdentifier:  "sellForexproductCell")
        self.sellForexTableView.isScrollEnabled = false
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ForexBuyPassangerViewController.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        
        self.sellForexScrollView.addSubview(self.sellForexView)
        
        if let tc_MobileNumber:String = UserDefaults.standard.value(forKey: "userDefaultTC-LoginMobileNumber") as? String
        {
            self.textFieldMobileNumber.text = tc_MobileNumber
        }
      
        if previousScreen == "Pending Transaction"
        {
            textFieldMobileNumber.text = sellForexBO.mobileNumber
            heightConstraintOfSellForexTableview.constant = CGFloat(productArray.count * minimizedProductViewHeght  + tableviewFooterHeight )
            self.sellForexScrollView.contentSize = CGSize.init(width: 0, height: heightConstraintOfSellForexTableview.constant + CGFloat(contactDetailViewHeight ) + CGFloat(tableviewFooterHeight + columnHeaderHeight + 50) );
            self.sellForexView.frame = CGRect.init(x: 0, y: 0, width: Int(self.sellForexScrollView.frame.size.width), height:  Int(self.sellForexScrollView.contentSize.height + 50))
        }
        else
        {
            let product : ProductBO = ProductBO.init()
            heightConstraintOfColumnHeaders.constant = 0
            productArray.append(product)
            heightConstraintOfSellForexTableview.constant = CGFloat( largeProductViewHeight + tableviewFooterHeight + 30 )
            self.sellForexScrollView.contentSize = CGSize.init(width: 0, height: heightConstraintOfSellForexTableview.constant + CGFloat(contactDetailViewHeight ) + CGFloat(tableviewFooterHeight) + 50 );
            self.sellForexView.frame = CGRect.init(x: 0, y: 0, width: Int(self.sellForexScrollView.frame.size.width), height:  Int(self.sellForexScrollView.contentSize.height + 50))
        }
      
        
        self.fetchProductList()
        self.fetchCustomerState()
        self.fetchCustomerBranch()
        textFieldBranch.delegate = self
        textFieldCustomerState.delegate = self
        textFieldMobileNumber.delegate = self
       
         //KeyboardAvoiding.setAvoidingView(self.sellForexScrollView, withTriggerView: self.textFieldBranch)
         //KeyboardAvoiding.setAvoidingView(self.sellForexScrollView, withTriggerView: self.textFieldCustomerState)
        //KeyboardAvoiding.setAvoidingView(self.sellForexScrollView, withTriggerView: self.textFieldMobileNumber)
        
        self.addDoneButtonOnKeyboard()
    }
    
    override func backButtonClicked(buttonView:UIButton)
    {
        if self.previousController == "SlideMenuViewController"
        {
            SlideNavigationController.sharedInstance().popToRootViewController(animated: true)
        }
        self.navigationController?.popViewController(animated: true)
    }

    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle       = UIBarStyle.default
        let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(SellForexProductViewController.doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.textFieldMobileNumber.inputAccessoryView = doneToolbar
        self.textFieldCustomerState.inputAccessoryView = doneToolbar
        self.textFieldBranch.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction()
    {
        self.textFieldMobileNumber.resignFirstResponder()
        self.textFieldCustomerState.resignFirstResponder()
        self.textFieldBranch.resignFirstResponder()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.buttonSellForex.layer.cornerRadius = 10
        self.setNotificationKeyboard()
    }
    
    override func viewDidLayoutSubviews()
    {
        super.setSubViewFrame(childView: self.sellForexProductView)
        
        self.sellForexScrollView.contentSize = CGSize.init(width: 0, height: heightConstraintOfSellForexTableview.constant + CGFloat(contactDetailViewHeight) + CGFloat( topLabelHeight ) +  heightConstraintOfColumnHeaders.constant + 50)
        self.sellForexView.frame = CGRect.init(x: 0, y: 0, width: Int(self.sellForexScrollView.frame.size.width), height: Int(sellForexScrollView.contentSize.height + 50))
    }
    
    override func viewWillLayoutSubviews()
    {
        super.setSubViewFrame(childView: self.sellForexProductView)
        
        self.sellForexScrollView.contentSize = CGSize.init(width: 0, height: heightConstraintOfSellForexTableview.constant + CGFloat(contactDetailViewHeight) + CGFloat( topLabelHeight ) +  heightConstraintOfColumnHeaders.constant + 50)
        self.sellForexView.frame = CGRect.init(x: 0, y: 0, width: Int(self.sellForexScrollView.frame.size.width), height: Int(sellForexScrollView.contentSize.height + 50))
        
    }
    
    func dismissKeyboard()
    {
        view.endEditing(true)
    }
    
    func markAsOldProduct() -> Void {
        for i in 0..<productArray.count
        {
            productArray[i].markAsOldProduct()
            
        }
        
    }
    
    func changeProductStatus() -> Void
    {
        for  i in (0..<productArray.count)
        {
            productArray[i].markAsOldProduct()
        }
    }
    
    @objc func addProduct(buttonView:UIButton )
    {
        if(validateAllProducts())
        {
        if productArray.count < 3
        {
                markAsOldProduct()
                let product : ProductBO = ProductBO.init()
                productArray.append(product)
                //  selectedProduct = productArray.count - 1
                heightConstraintOfSellForexTableview.constant = CGFloat(productArray.count * minimizedProductViewHeght + largeProductViewHeight + tableviewFooterHeight )
                self.sellForexTableView.reloadData()
            }
        else
        {
            showAlert(message: "Cannot add more than 3 product.")
        }
    }
    
    }
    
    func setTableHeight()
    {
        if productArray.count > 1
        {
            heightConstraintOfColumnHeaders.constant = 40
        }
        else
        {
            heightConstraintOfColumnHeaders.constant = 0
        }
        self.sellForexScrollView.contentSize = CGSize.init(width: 0, height: heightConstraintOfSellForexTableview.constant + CGFloat(contactDetailViewHeight) + CGFloat( topLabelHeight ) +  heightConstraintOfColumnHeaders.constant + 50)
        self.sellForexView.frame = CGRect.init(x: 0, y: 0, width: Int(self.sellForexScrollView.frame.size.width), height: Int(sellForexScrollView.contentSize.height + 50))
        
    }

    func isValidProduct()->Bool
    {
        let product : ProductBO = productArray.last!
        if product.productName.isEqual(to: "")
        {
             showAlert(message: "Please select product.")
            return false
        }
        if product.currency.isEqual(to: "")
        {
             showAlert(message: "Please select currency")
            return false
        }
        if product.fxAmount == 0
        {
             showAlert(message: "Please enter amount")
            return false
        }
     /*   if product.currency.contains("Japanese") || product.currency.contains("Thai")
        {
            if  product.fxAmount % 100 != 0 || product.fxAmount == 0
            {
                product.setFXAmount(fxAmount: 0)
                product.setINRAmount(inrAmount: 0)
                //travellerArray[selectedSection].replaceProduct(atIndex: travellerArray[selectedSection].getProductCount()-1, newProduct: product)
                showAlert(message: "FXAmount should be multiple of 100.")
                return false
            }
            
        }
        else
        {
            if  product.fxAmount % 50 != 0 || product.fxAmount == 0
            {
                product.setFXAmount(fxAmount: 0)
                product.setINRAmount(inrAmount: 0)
                // travellerArray[selectedSection].replaceProduct(atIndex: travellerArray[selectedSection].getProductCount()-1, newProduct: product)
                showAlert(message: "FXAmount should be multiple of 50.")
                return false
            }
        }*/
        if product.inrAmount == 0
        {
            return false
        }
        if product.isUpdateButtonVisible == true
        {
             showAlert(message: "Update opened product")
            return false
        }
        if getTravellerAmount() > overallTravellerLimit
        {
            showAlert(message: "As per RBI norms, Total Forex amount per traveller cannot be more than 2,50,000 USD equivalent in a single financial year. Please revise the amount to proceed further")
            return false
        }
        product.markAsOldProduct()
        return true
    }
//    func getTravellerAmount() -> NSInteger
//    {
//        var travellerAmount : NSInteger = 0
//        for  i in (0..<productArray.count)
//        {
//            let inrAmt : NSInteger = productArray[i].inrAmount
//            travellerAmount = travellerAmount + inrAmt
//        }
//        return travellerAmount
//    }
    
    func getTravellerAmount() -> NSInteger /*getTravellerAmountInDollars*/
    {
        var travellerAmount : Float = 0.0
        for  i in (0..<productArray.count)
        {
            let amountInDollars : Float = productArray[i].amountInDollars
            travellerAmount = travellerAmount + amountInDollars
        }
        return  NSInteger(round(travellerAmount))
    }
    func isValidProductForUpdate(index:NSInteger)->Bool
    {
        let product : ProductBO = productArray[index]
        if product.productName.isEqual(to: "")
        {
            showAlert(message: "Please select product.")
            return false
        }
        if product.currency.isEqual(to: "")
        {
            showAlert(message: "Please select currency")
            return false
        }
        if product.fxAmount == 0
        {
            showAlert(message: "Please enter amount")
            return false
        }
        if getTravellerAmount() > overallTravellerLimit
        {
            showAlert(message: "As per RBI norms, Total Forex amount per traveller cannot be more than 2,50,000 USD equivalent in a single financial year. Please revise the amount to proceed further")
            return false
        }
        /*   if product.currency.contains("Japanese") || product.currency.contains("Thai")
         {
         if  product.fxAmount % 100 != 0 || product.fxAmount == 0
         {
         product.setFXAmount(fxAmount: 0)
         product.setINRAmount(inrAmount: 0)
         //travellerArray[selectedSection].replaceProduct(atIndex: travellerArray[selectedSection].getProductCount()-1, newProduct: product)
         showAlert(message: "FXAmount should be multiple of 100.")
         return false
         }
         
         }
         else
         {
         if  product.fxAmount % 50 != 0 || product.fxAmount == 0
         {
         product.setFXAmount(fxAmount: 0)
         product.setINRAmount(inrAmount: 0)
         // travellerArray[selectedSection].replaceProduct(atIndex: travellerArray[selectedSection].getProductCount()-1, newProduct: product)
         showAlert(message: "FXAmount should be multiple of 50.")
         return false
         }
         }*/
        if product.inrAmount == 0
        {
            return false
        }
        product.markAsOldProduct()
        return true
    }

    func validateAllProducts() -> Bool {
        for i in 0..<productArray.count
        {
            let product : ProductBO = productArray[i]
            
            if product.productName.isEqual(to: "")
            {
                showAlert(message: "Please select product")
                return false
            }
            if product.currency.isEqual(to: "")
            {
                showAlert(message: "Please select currency")
                return false
            }
            if product.fxAmount == 0
            {
                showAlert(message: "Please enter the amount")
                return false
            }
            /*  if product.currency.contains("Japanese") || product.currency.contains("Thai")
             {
             if  product.fxAmount % 100 != 0 || product.fxAmount == 0
             {
             product.setFXAmount(fxAmount: 0)
             product.setINRAmount(inrAmount: 0)
             //travellerArray[selectedSection].replaceProduct(atIndex: travellerArray[selectedSection].getProductCount()-1, newProduct: product)
             showAlert(message: "FXAmount should be multiple of 100.")
             return false
             }
             
             }
             else
             {
             if  product.fxAmount % 50 != 0 || product.fxAmount == 0
             {
             product.setFXAmount(fxAmount: 0)
             product.setINRAmount(inrAmount: 0)
             // travellerArray[selectedSection].replaceProduct(atIndex: travellerArray[selectedSection].getProductCount()-1, newProduct: product)
             showAlert(message: "FXAmount should be multiple of 50")
             return false
             }
             }*/
            if getTravellerAmount() > overallTravellerLimit
            {
                showAlert(message: "As per RBI norms, Total Forex amount per traveller cannot be more than 2,50,000 USD equivalent in a single financial year. Please revise the amount to proceed further")
                return false
            }
            if product.isUpdateButtonVisible == true
            {
                showAlert(message: "Update opened product")
                return false
            }
            if product.inrAmount == 0
            {
                return false
            }
        }
        return true

    }
    
    func validateFields() -> Bool
    {
        if validateAllProducts() == false
        {
            return false
        }
        if ((textFieldMobileNumber.text?.isEmpty)! || ((textFieldMobileNumber.text?.trimmingCharacters(in: CharacterSet(charactersIn: " ")))?.isEmpty)!)
        {
            showAlert(message: "Please enter Mobile number")
            return false
        }
        if !isValidMobileNo(testStr: textFieldMobileNumber.text!)
        {
            showAlert(message: "Please enter valid Mobile number")
            return false
        }
        if (textFieldBranch.text?.isEmpty)!  || ((textFieldBranch.text?.trimmingCharacters(in: CharacterSet(charactersIn: " ")))?.isEmpty)! || (textFieldBranch.text?.count)! < 3
        {
            showAlert(message: "Please Select Branch City")
            return false
        }
        if (textFieldCustomerState.text?.isEmpty)! || ((textFieldCustomerState.text?.trimmingCharacters(in: CharacterSet(charactersIn: " ")))?.isEmpty)! || (textFieldCustomerState.text?.count)! < 3
        {
            showAlert(message: "Please Select Customer State")
            return false
        }
        return true
    }
    
    // MARK: Create Lead
    func createLead(mobileNumber : String)
    {
        //        https://thomascookindia--tst1.custhelp.com/cgi-bin/thomascookindia.cfg/php/custom/mobile_iosapp_forex_lead.php?mobile=9811976670&requirement_type=Buy&sub_type=Send Money
        
        let url = URL(string: "\(kUrlForCreateLead)mobile=\(String(describing: mobileNumber))&requirement_type=Sell")
        print("url---> \(String(describing: url))")
        
        URLSession.shared.dataTask(with: url!, completionHandler:
            {
                (data, response, error) in
                
                if(error != nil)
                {
                    print("error")
                }
                else
                {
                    if let data = data, let stringResponse = String(data: data, encoding: .utf8)
                    {
                        print("Response---< \(stringResponse)")
                    }
                }
        }).resume()
    }
    
    // MARK: button action
    @IBAction func sellForexButtonClicked(_ sender: Any)
    {
        if validateFields()
        {
            self.createLead(mobileNumber: self.textFieldMobileNumber.text!)
            
            sellForexBO.productArray = productArray
            sellForexBO.mobileNumber = textFieldMobileNumber.text!
            sellForexBO.customerState = textFieldCustomerState.text!
            sellForexBO.customerbranch = textFieldBranch.text!
            let sellForexConfirmOrderVc : SellForexConfirmOrderViewController = SellForexConfirmOrderViewController(nibName: "ForexBaseViewController", bundle: nil)
            
            sellForexConfirmOrderVc.sellForexBO = sellForexBO
            self.navigationController?.pushViewController(sellForexConfirmOrderVc, animated: true)
        }
    }
    
    // MARK: - textfield methods
    //    func textFieldDidBeginEditing(_ textField: UITextField) {
    //        let scrollPoint : CGPoint = CGPoint.init(x: 0, y: textField.frame.origin.y)
    //        sellForexScrollView.setContentOffset(scrollPoint, animated: true)
    //    }
    //
    //    func textFieldDidEndEditing(_ textField: UITextField) {
    //         sellForexScrollView.setContentOffset(CGPoint.zero, animated: true)
    //    }
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        activeTextField=textField;
    }
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        activeTextField=nil;
    }
    
    @objc func textFieldProductNameDidChange(textField: UITextField)
    {
        // productArray[textField.tag].setProductName(productName: textField.text! as NSString)
        textField.resignFirstResponder()
        textField.endEditing(true)
        
        let alertController : UIAlertController  = UIAlertController.init(title: "Product", message: "Select Product", preferredStyle:.actionSheet)
        
        for dict in arrayForProductList
        {
            let productname = dict.object(forKey: "productName")
            
            let productId = dict.object(forKey: "productId");
            let  alertAction: UIAlertAction = UIAlertAction.init(title: productname as? String, style: .default, handler:
            {
                
                (alert: UIAlertAction!) -> Void in
                textField.text = productname as? String
                let product : ProductBO = self.productArray[textField.tag];
                
                let indexPath : IndexPath = IndexPath.init(row: 0 , section: textField.tag)
                
                
                
                let cell : sellForexproduxtViewCell = self.sellForexTableView.cellForRow(at: indexPath) as! sellForexproduxtViewCell
                
                cell.textFieldCurrency.text = ""
                cell.textFieldFXAmt.placeholder = "FX Amount"
                // clear currency on change of product
                product.setCurrency(currency: "")
                product.setProductName(productName: textField.text! as NSString)
                product.setProductId(productID: productId as! NSInteger)
                self.productArray[textField.tag].setProductName(productName: textField.text! as NSString)
              
            })
            
            alertController.addAction(alertAction)
        }
        
        let  alertCancel: UIAlertAction = UIAlertAction.init(title: "Cancel" , style: .cancel, handler:
        {
            
            (alert: UIAlertAction!) -> Void in
            //textField.text = productname as? String
        })
        
        alertController .addAction(alertCancel)
        
        self.present(alertController, animated:true, completion: nil)
    }
    
    @objc func textFieldCurrencyDidChange(textField: UITextField)
    {
        let indexPath : IndexPath = IndexPath.init(row: 0 , section: textField.tag)
        let cell : sellForexproduxtViewCell = self.sellForexTableView.cellForRow(at: indexPath) as! sellForexproduxtViewCell
        
        // productArray[textField.tag].setCurrency(currency:textField.text! as NSString)
        textField.resignFirstResponder()
        textField.endEditing(true)
        self.currencyArray.removeAll()
        let product : ProductBO =  self.productArray[textField.tag];
        
        if(cell.textFieldProduct.text == "")
        {
            showAlert(message: "Please select product")
        }
        else
        {
            self.fetchCurrencyList(forProductID: product.productID!, textField: textField)
        }
    }
    
    @objc func textFieldFXAmountBeginEditing(textField: UITextField)
    {
        let indexPath : IndexPath = IndexPath.init(row: 0 , section: textField.tag)
        
        let cell : sellForexproduxtViewCell = self.sellForexTableView.cellForRow(at: indexPath) as! sellForexproduxtViewCell
//        cell.textFieldFXAmt.delegate = self
//        cell.textFieldFXINRAmt.delegate = self
        if(cell.textFieldCurrency.text == "")
        {
            showAlert(message: "Please select currency")
        }
    }
    
    @objc func textFieldFXAmountDidChange(textField: UITextField)
    {
        let indexPath : IndexPath = IndexPath.init(row: 0 , section: textField.tag)
        
        let cell : sellForexproduxtViewCell = self.sellForexTableView.cellForRow(at: indexPath) as! sellForexproduxtViewCell
//        cell.textFieldFXAmt.delegate = self
//        cell.textFieldFXINRAmt.delegate = self
        if cell.textFieldCurrency.text == ""
        {
            showAlert(message: "Please select currency")
        }
        else
        {
            let product : ProductBO = productArray[textField.tag]
            
            if((textField.text) != "")
            {
                let fxamount : NSInteger = NSInteger.init( textField.text!)!
                let inrAmt =   Float.init(fxamount) * product.getROE()
                product.amountInDollars = inrAmt/dollarRate
                cell.textFieldFXINRAmt.text = "\(NSInteger.init(round(inrAmt)))"
                product.setFXAmount(fxAmount:fxamount)
                product.setINRAmount(inrAmount: NSInteger(inrAmt))
            }
            else
            {
                product.setFXAmount(fxAmount: 0)
                product.setINRAmount(inrAmount: 0)
                cell.textFieldFXINRAmt.text = ""
            }
            productArray[textField.tag] = product

        }
        //  cell.heightConstraintOfconversionView.constant = 40
        
        //  productArray[textField.tag].setFXAmount(fxAmount: NSInteger.init( textField.text!)!)
    }
    
    func textFieldINRAMountDidChange(textField: UITextField)
    {
//        productArray[textField.tag].setINRAmount(inrAmount: NSInteger.init( textField.text!)!)
        
        let indexPath : IndexPath = IndexPath.init(row: 0 , section: textField.tag)
        
        let cell : sellForexproduxtViewCell = self.sellForexTableView.cellForRow(at: indexPath) as! sellForexproduxtViewCell
        if cell.textFieldCurrency.text == ""
        {
            showAlert(message: "Please select currency")
        }
        else
        {
            let product : ProductBO = productArray[textField.tag]
            
            if((textField.text) != "")
            {
                let fxamount : NSInteger = NSInteger.init( textField.text!)!
                let inrAmt =   Float.init(fxamount) / product.getROE()
                cell.textFieldFXAmt.text = "\(NSInteger(inrAmt))"
                product.setFXAmount(fxAmount:fxamount)
                product.setINRAmount(inrAmount: NSInteger(inrAmt))
            }
            else
            {
                product.setFXAmount(fxAmount: 0)
                product.setINRAmount(inrAmount: 0)
                cell.textFieldFXAmt.text = ""
            }
            productArray[textField.tag] = product
        }
    }
    
    // MARK: tableview  delegate
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return productArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell :  sellForexproduxtViewCell = tableView.dequeueReusableCell(withIdentifier: "sellForexproductCell", for: indexPath) as! sellForexproduxtViewCell
        cell.cellDelegate = self
        let product : ProductBO = productArray[indexPath.section]
        
        cell.textFieldCurrency.text = product.getCurrency() as String
        cell.textFieldProduct.text = product.getProductName() as String
        cell.textFieldFXINRAmt.delegate = self
        cell.textFieldFXAmt.delegate = self
        cell.textFieldFXAmt.text = product.getFXAmount() as String
        cell.textFieldFXINRAmt.text = product.getINRAmount() as String
       // KeyboardAvoiding.setAvoidingView(cell, withTriggerView:cell.textFieldFXAmt)
        
        if(product.fxAmount != 0)
        {
            cell.textFieldFXAmt.text = NSString.init(format: "%i", product.fxAmount) as String
            cell.conversionLabel.text = "1 \(product.getCurrencyCode()) = \(product.getROE()) INR"
            cell.conversionLabel.isHidden = false
            cell.minimizedProductName.text = product.productName as String
            cell.labelFxAmount.text = "\(product.getFXAmount() as String) \(product.currencyCode as String)"
            cell.labelINRAmt.text = "\(product.getINRAmount()) INR" as String
        }
        else
        {
            cell.textFieldFXAmt.text = ""
        }
        
        if(product.inrAmount != 0)
        {
            cell.textFieldFXINRAmt.text = NSString.init(format: "%i", product.inrAmount) as String
        }
        else
        {
            cell.textFieldFXINRAmt.text = ""
        }
        
        
        //set data
        cell.textFieldFXAmt.tag = indexPath.section
        cell.textFieldFXINRAmt.tag = indexPath.section
        cell.textFieldCurrency.tag = indexPath.section
        cell.textFieldProduct.tag = indexPath.section
        
        cell.labelProductNoInMinimiezedView.text = NSString.init(format:"%i" , indexPath.section + 1) as String
        cell.labelProductNoInLargeView.text = NSString.init(format: "%i", indexPath.section + 1) as String
        cell.labelProductNoInEditView.text = NSString.init(format: "Product %i", indexPath.section + 1) as String
        
        cell.textFieldProduct.addTarget(self, action:  #selector(textFieldProductNameDidChange(textField:)), for: UIControl.Event.editingDidBegin)
        cell.textFieldCurrency.addTarget(self, action:  #selector(textFieldCurrencyDidChange(textField:)), for: UIControl.Event.editingDidBegin)
        cell.textFieldFXAmt.addTarget(self, action:  #selector(textFieldFXAmountBeginEditing(textField:)), for: UIControl.Event.editingDidBegin)
        cell.textFieldFXAmt.addTarget(self, action:  #selector(textFieldFXAmountDidChange(textField:)), for: UIControl.Event.editingChanged)
        
//        cell.textFieldFXINRAmt.addTarget(self, action: #selector(textFieldINRAMountDidChange(textField:)), for: UIControlEvents.editingChanged)
        
        //  cell.conversionLabel.text = "1 \(product.getCurrencyCode()) = \(product.getROE()) INR"
        
        /* if(product.isNewProduct || selectedProduct == indexPath.section)
         {
         cell.heightConstraintOfExpandedView.constant = CGFloat(largeProductViewHeight)
         cell.expandedViewProductView.isHidden = false
         cell.minimizedView.isHidden = true
         cell.heightConstraintOfMinimizedView.constant = 0
         }
         else
         {
         cell.heightConstraintOfExpandedView.constant = 0
         cell.expandedViewProductView.isHidden = true
         cell.minimizedView.isHidden = false
         cell.heightConstraintOfMinimizedView.constant = CGFloat(minimizedProductViewHeight)
         }*/
        if(product.isUpdateButtonVisible)
        {
            cell.heightConstraintOfExpandedView.constant = CGFloat(largeProductViewHeight)
            cell.minimizedView.isHidden = true
            cell.heightConstraintOfMinimizedView.constant = 0
            cell.updateButtonView.isHidden = false
            cell.heightConstraintOfDeleteButtonView.constant = CGFloat(deleteButtonViewHeight)
            cell.heightConstraintOfconversionView.constant = 30
            cell.conversionLabel.isHidden = false
            
        }
        else if(product.isNewProduct)
        {
//            cell.labelProductNoInLargeView.text = NSString.init(format: "%i", indexPath.row + 1) as String
//            print("labelProductNoInLargeView---> \(NSString.init(format: "%i", indexPath.row + 1) as String)")
            
            cell.heightConstraintOfExpandedView.constant = CGFloat(largeProductViewHeight)
            cell.minimizedView.isHidden = true
            cell.heightConstraintOfMinimizedView.constant = 0
            cell.updateButtonView.isHidden = true
            cell.heightConstraintOfDeleteButtonView.constant = 0
            cell.textFieldCurrency.text = ""
            cell.heightConstraintOfconversionView.constant = 30
            cell.conversionLabel.isHidden = true
        }
        else
        {
            cell.heightConstraintOfExpandedView.constant = 0
            cell.minimizedView.isHidden = false
            cell.heightConstraintOfMinimizedView.constant = CGFloat(minimizedProductViewHeght)
            cell.updateButtonView.isHidden = true
            cell.heightConstraintOfDeleteButtonView.constant = 0
            cell.conversionLabel.isHidden = true
            cell.heightConstraintOfconversionView.constant = 0
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        let product : ProductBO = productArray[indexPath.section]
        
        if(product.isUpdateButtonVisible)
        {
            return CGFloat(largeProductViewHeight + 100 )
        }
        else if product.isNewProduct //|| selectedProduct == indexPath.section
        {
            return  CGFloat(largeProductViewHeight + 31)
        }
        
        return CGFloat(minimizedProductViewHeght)
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if(productArray.count - 1 == section)
        {
            return CGFloat(tableviewFooterHeight)
        }
        
        return 0
        
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
    {
        let view : UIView = UIView.init(frame: CGRect(x: 0, y: 0, width:UIScreen.main.bounds.width, height: 60))
        let button : UIButton = UIButton.init(frame:  CGRect(x:40, y: 10, width:UIScreen.main.bounds.width - 60, height: 30))
        button.setTitle("Add Another Currency / Product", for: UIControl.State.normal)
        button.addTarget(self, action:  #selector(addProduct(buttonView:)), for: .touchUpInside)
        view.backgroundColor = UIColor.white;
        button.backgroundColor = UIColor.lightGray
        button.setTitleColor(UIColor.white, for: UIControl.State.normal)
        button.titleLabel?.font =  UIFont(name: "System", size: 11)
        button.layer.cornerRadius = 15
        button.setTitleColor(UIColor.white, for: UIControl.State.normal)
        button.tag = section
        view.addSubview(button)
        return view
    }
    
    /*   func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
     {
     if(productArray[selectedProduct].isEmpty())
     {
     productArray.remove(at: selectedProduct)
     }
     selectedProduct = indexPath.section
     markAsOldProduct()
     heightConstraintOfSellForexTableview.constant = CGFloat(productArray.count * minimizedProductViewHeght + largeProductViewHeight + tableviewFooterHeight )
     productArray[indexPath.section].markAsOpen()
     self.sellForexTableView.reloadData()
     }
     */
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        self.setTableHeight()
    }
    
    // MARK: Fetch product list
    
    
    func fetchProductList () -> Void {
        LoadingIndicatorView.show()
        ForexCommunicationManager.sharedInstance.execTask(pathParam: "/tcForexRS/generic/product/2", queryParam: "", requestType: "get", jsonDict:NSDictionary())
        {
            (status, response) in
            DispatchQueue.main.async { () -> Void in
                LoadingIndicatorView.hide()
                
            if( !status || response == nil)
            {
                self.showAlert(message: "Some error has occurred")
            }
            else
            {
                
                if(response != nil)
                {
                    let jsonArray : NSArray = response as! NSArray
                    
                    for i in (0..<jsonArray.count)
                    {
                        let dict : NSDictionary = jsonArray[i] as! NSDictionary
                        self.arrayForProductList.append(dict)
                    }
                }
                else
                {
                    self.showAlert(message: "Some error has occurred")
                }
            }
            
        }
    }
    }
    
    
    // MARK: Fetch Currency List
    
    func fetchCurrencyList (forProductID:NSInteger, textField: UITextField) -> Void
    {
        LoadingIndicatorView.show()
        let pathParameter  : NSString = "/tcForexRS/generic/roe/2/".appending(NSString.init(format: "%i", forProductID) as String ) as (String) as NSString
        ForexCommunicationManager.sharedInstance.execTask(pathParam:pathParameter , queryParam: "", requestType: "get", jsonDict:NSDictionary())
        {
            (status, response) in
            DispatchQueue.main.async { () -> Void in
                LoadingIndicatorView.hide()
                
            if( !status || response == nil)
            {
                self.showAlert(message: "Some error has occurred")
            }
            else
            {
                
                if(response != nil)
                {
                    let jsonArray : NSArray = response as! NSArray
                    for i in (0..<jsonArray.count)
                    {
                        let dict : NSDictionary = jsonArray[i] as! NSDictionary
                        self.currencyArray.append(dict)
                    }
                        for dict in self.currencyArray
                        {
                            let currencyname = dict.object(forKey: "currencyName")
                            
                        
                            let currencyCode : String = (dict.object(forKey: "currencyCode") as? String)!
                            if currencyCode.contains("USD")
                            {
                                
                                let roe = dict.object(forKey: "roe") as! Float
                                self.dollarRate = roe
                               // self.overallTravellerLimit = NSInteger(round( roe * 250000))
                            }
                        }
                   
                        let alertController : UIAlertController  = UIAlertController.init(title: "Currency", message: "Select Currency", preferredStyle:.actionSheet)
                        
                        let indexPath : IndexPath = IndexPath.init(row: 0 , section: textField.tag)
                        
                        let cell : sellForexproduxtViewCell = self.sellForexTableView.cellForRow(at: indexPath) as! sellForexproduxtViewCell
                        
                        if cell.textFieldProduct.text == ""
                        {
                            self.showAlert(message: "Please select product")
                        }
                        else
                        {
                            for dict in self.currencyArray
                            {
                                let currencyname = dict.object(forKey: "currencyName")
                                let isNotroAcc = dict.object(forKey: "isNostroAcc")
                                let currencyid = dict.object(forKey: "currencyId")
                                let  alertAction: UIAlertAction = UIAlertAction.init(title: currencyname as? String, style: .default, handler:
                                {
                                    (alert: UIAlertAction!) -> Void in
                                    textField.text = currencyname as? String
                                    
                                    cell.textFieldFXAmt.placeholder = dict.object(forKey: "currencyCode") as? String
                                    cell.textFieldFXAmt.text = ""
                                    cell.textFieldFXINRAmt.text = ""
                                    let product : ProductBO =  self.productArray[textField.tag];
                                    product.setCurrency(currency: textField.text! as NSString)
                                    product.setroe(roe: dict.object(forKey: "roe") as! Float)
                                    product.setCurrencyCode(currencyCode : dict.object(forKey: "currencyCode")  as! NSString)
                                    product.setIsNostroAcc(astroAvail: isNotroAcc as! NSString)
                                    product.setCurrencyId(currencyId: currencyid as! NSInteger)
                                    cell.conversionLabel.text = "1 \(dict.object(forKey: "currencyCode") as! NSString) = \(product.getROE()) INR"
                                    cell.conversionLabel.isHidden = false
                                    
                                    //self.productArray[textField.tag].setCurrency(currency:textField.text! as NSString)
                                })
                                
                                alertController.addAction(alertAction)
                            }
                            
                            
                            let  alertCancel: UIAlertAction = UIAlertAction.init(title: "Cancel" , style: .cancel, handler:
                            {
                                
                                (alert: UIAlertAction!) -> Void in
                                //textField.text = productname as? String
                            })
                            
                            alertController .addAction(alertCancel)
                            
                            self.present(alertController, animated:true, completion: nil)
                        }

                     }
                else
                {
                    self.showAlert(message: "Some error has occurred")
                }
                }
            }
        }
    }
    
    // MARK: Fetch Customer State
    
    func fetchCustomerState()
    {
        let pathParameter  : NSString = "/tcForexRS/generic/gstState"
        ForexCommunicationManager.sharedInstance.execTask(pathParam:pathParameter , queryParam: "", requestType: "get", jsonDict:NSDictionary())
        {
            (status, response) in
               DispatchQueue.main.async { () -> Void in
            if(response != nil)
            {
                let jsonArray : NSArray = response as! NSArray
                //                for i in (0..<jsonArray.count)
                //                {
                //                    let dict : NSDictionary = jsonArray[i] as! NSDictionary
                //                    self.arrayForCustomerState.append(dict)
                //                }
                
                self.arrayForCustomerState = NSMutableArray.init(array: jsonArray)  as! [NSDictionary]
            }
            else
            {
                self.showAlert(message: "Some error has occurred")
            }
            }
        }
        
    }
    
    
    // MARK: fetch Branch
    func fetchCustomerBranch()
    {
        let pathParameter  : NSString = "/tcForexRS/generic/gstCity"
        ForexCommunicationManager.sharedInstance.execTask(pathParam:pathParameter , queryParam: "", requestType: "get", jsonDict:NSDictionary())
        {
            (status, response) in
             DispatchQueue.main.async { () -> Void in
            if(response != nil)
            {
                let jsonArray : NSArray = response as! NSArray
                //                for i in (0..<jsonArray.count)
                //                {
                //                    let dict : NSDictionary = jsonArray[i] as! NSDictionary
                //                    self.arrayForBranch.append(dict)
                //                }
                self.arrayForBranch = NSMutableArray.init(array: jsonArray) as! [NSDictionary];
            }
            else
            {
                self.showAlert(message: "Some error has occurred")
            }
            }
        }
            
        
    }
    
    
    
    // MARK: text field delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField == textFieldMobileNumber
        {
            let textFieldText: NSString = (textField.text ?? "") as NSString
            
            let txtAfterUpdate = textFieldText.replacingCharacters(in: range, with: string)
            
            if txtAfterUpdate.count > 10
            {
                return false
            }
        }
        else if textField == textFieldCustomerState
        {
            let totalString  = "\(textField.text ?? "")\(string)"
            
            if totalString.count > 2
            {
                if !string.isEmpty
                {
                     textField.resignFirstResponder()
                    // let resultPredicate = NSPredicate(format: "stateName contains[c] %@", totalString)
                    //let filteredArray = arrayForCustomerState.filter { $0["name"] as! String == "Mark" }
                    
                    let filteredArray = arrayForCustomerState.filter
                    {
                        guard let dictionary = $0 as? [String: Any],
                            let name  = dictionary["stateName"] as? String else
                        {
                            return false
                        }
                        return name.localizedCaseInsensitiveContains(totalString)
                    }
                    
                    if filteredArray.isEmpty
                    {
                        showAlert(message: "Please Enter Valid Customer State")
                    }
                    else
                    {
                        let alertController : UIAlertController  = UIAlertController.init(title: "Customer State", message: "Select Customer State", preferredStyle:.actionSheet)
                        
                        for dict in filteredArray
                        {
                            let productname = dict.object(forKey: "stateName")
                            let isUnion = dict.object(forKey: "isUnionTerritory")
                            let stateCode = dict.object(forKey: "gstStateCode")
                            
                            let  alertAction: UIAlertAction = UIAlertAction.init(title: productname as? String, style: .default, handler:
                            {
                                
                                (alert: UIAlertAction!) -> Void in
                                
                                textField.text = productname as? String
                                self.isUnionTerritoryState = isUnion as! String;
                                self.gstStateCode = stateCode as! String
                                if stateCode != nil
                                {
                                    self.sellForexBO.gstStateCode = stateCode as! String
                                }
                                
                                if isUnion != nil
                                {
                                    self.sellForexBO.isUnionTerritoryState = isUnion as! String
                                }
                                
                                self.view.endEditing(true)
                                
                            })
                            
                            alertController.addAction(alertAction)
                        }
                        let  alertCancel: UIAlertAction = UIAlertAction.init(title: "Cancel" , style: .cancel, handler:
                        {
                            
                            (alert: UIAlertAction!) -> Void in
                            self.view.endEditing(true)
                            //textField.text = productname as? String
                        })
                        alertController.addAction(alertCancel)
                        self.present(alertController, animated:true, completion: nil)
                    }
                }
            }
            
        }
        else if textField == textFieldBranch
        {
            let totalString  = "\(textField.text ?? "")\(string)"
            
            if totalString.count > 2
            {
                if !string.isEmpty
                {
                     textField.resignFirstResponder()
                    let filteredArray = arrayForBranch.filter
                    {
                        guard let dictionary = $0 as? [String: Any],
                        let name  = dictionary["cityName"] as? String else
                        {
                            return false
                        }
                        return name.localizedCaseInsensitiveContains(totalString)
                    }
                    
                    if filteredArray.isEmpty
                    {
                        showAlert(message: "Please Enter Valid Branch City")
                    }
                    else
                    {
                        let alertController : UIAlertController  = UIAlertController.init(title: "Customer Branch", message: "Select Customer Branch", preferredStyle:.actionSheet)
                        
                        for dict in filteredArray
                        {
                            //                        let productname = dict.object(forKey: "branchName")
                            let cityname = dict.object(forKey: "cityName")
                            let gstBranchCode = dict.object(forKey: "cityCode")
                            let branchCode = dict.object(forKey: "gstStateCode")
                            let isUnion = dict.object(forKey: "isUnionTerritory")
                            
                            
                            let  alertAction: UIAlertAction = UIAlertAction.init(title: cityname as? String, style: .default, handler:
                            {
                                (alert: UIAlertAction!) -> Void in
                                
                                textField.text = cityname as? String
                                self.gstBranchCode = gstBranchCode as! String
                                self.branchCode = branchCode as! String
                                
                                self.isUnionTerritoryBranch = isUnion as! String
                                self.sellForexBO.branchCode = branchCode as! String
                                if gstBranchCode != nil
                                {
                                    self.sellForexBO.gstBranchCode = gstBranchCode as! String
                                }
                                if isUnion != nil
                                {
                                    self.sellForexBO.isUnionTerritoryBranch = isUnion as! String
                                }
                                self.view.endEditing(true)
                                self.getGstBranchCode()
                            })
                            
                            alertController.addAction(alertAction)
                        }
                        let alertCancel: UIAlertAction = UIAlertAction.init(title: "Cancel" , style: .cancel, handler:
                        {
                            (alert: UIAlertAction!) -> Void in
                            self.view.endEditing(true)
                            //textField.text = productname as? String
                        })
                        alertController.addAction(alertCancel)
                        self.present(alertController, animated:true, completion: nil)
                    }
                }
            }
        }
        else
        {
            let indexPath : IndexPath = IndexPath.init(row: 0, section: textField.tag) // row is 0 because product is handled by section . Each section contains only one row
            let cell : sellForexproduxtViewCell = self.sellForexTableView.cellForRow(at: indexPath) as! sellForexproduxtViewCell
                
            if textField == cell.textFieldFXAmt
            {
                let textFieldText: NSString = (textField.text ?? "") as NSString
                let txtAfterUpdate = textFieldText.replacingCharacters(in: range, with: string)
                
                if txtAfterUpdate.count > 6
                {
                    return false
                }
            }
            if textField == cell.textFieldFXINRAmt
            {
                let textFieldText: NSString = (textField.text ?? "") as NSString
                let txtAfterUpdate = textFieldText.replacingCharacters(in: range, with: string)
                
                if txtAfterUpdate.count > 7
                {
                    return false
                }
            }
        }
        return true;
    }
    
    func getGstBranchCode()
    {
        
        let pathParameter  : NSString = "/tcForexRS/generic/branchdetails/\(self.gstBranchCode)" as NSString
        
        ForexCommunicationManager.sharedInstance.execTask(pathParam:pathParameter , queryParam: "", requestType: "get", jsonDict:NSDictionary.init())
        {
            (status, response) in
             DispatchQueue.main.async { () -> Void in
            if(response != nil)
            {
                let jsonArray : NSArray = response as! NSArray
                
                self.arrayForGSTBranch = NSMutableArray.init(array: jsonArray) as! [NSDictionary];
            }
            else
            {
                self.showAlert(message: "Some error has occurred")
            }
        }
        }
        
    }
    
    
    func sellForexValidation() -> Bool
    {
        if !isValidProduct()
        {
            let alert = UIAlertController(title: "Alert", message: "fill details of product", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: { UIAlertAction in
                alert.dismiss(animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
            
            return false
        }
        else if ((self.textFieldMobileNumber.text?.count)! <= 0) && !(self.textFieldMobileNumber.text?.trimmingCharacters(in: CharacterSet(charactersIn: " ")).count == 10)
        {
            
            let alert = UIAlertController(title: "Alert", message: "Please enter mobile number", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: { UIAlertAction in
                alert.dismiss(animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
            return false
        }
        else if  (self.gstStateCode.trimmingCharacters(in: CharacterSet(charactersIn: " ")).count <= 0)
        {
            
            let alert = UIAlertController(title: "Alert", message: "Please enter customer state", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: { UIAlertAction in
                alert.dismiss(animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
            return false
        }
        else if (self.branchCode.trimmingCharacters(in: CharacterSet(charactersIn: " ")).count <= 0)
        {
            
            let alert = UIAlertController(title: "Alert", message: "Please enter branch state", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: { UIAlertAction in
                alert.dismiss(animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
            return false
        }
        
        return true
    }
    
    // MARK: tableview cell delegate
    func didPressINRHelpButton(_ tag: Int)
    {
        let popupVC: PopUpViewController = PopUpViewController(nibName: "PopUpViewController", bundle: nil)
        popupVC.msg = "Please enter the forex amount you want to encash."
        popupVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(popupVC, animated: false, completion: nil)
    }
    
    func didPressCurrencyHelpButton(_ tag: Int)
    {
        let popupVC: PopUpViewController = PopUpViewController(nibName: "PopUpViewController", bundle: nil)
        popupVC.msg = "Please select the currency you want to encash"
        popupVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(popupVC, animated: false, completion: nil)
    }
    
    func didPressProductNameHelpButton(_ tag: Int)
    {
        let popupVC: PopUpViewController = PopUpViewController(nibName: "PopUpViewController", bundle: nil)
        popupVC.msg = "You can encash your foreign exchange currency leftover from your foreign tour. Also, you can encash Thomas Cook Forex Card (BPC). Travellers cheque and any other currency you have bought."
        popupVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(popupVC, animated: false, completion: nil)
    }

    func didPressEditButton(_ tag: Int)
    {
        var flag = 0
        if productArray.count == 1 && (productArray.last?.isEmpty())!
        {
            showAlert(message: "fill product.")
        }
        else
        {
            if productArray.count > 1 && (productArray.last?.isEmpty())!
            {
                productArray.removeLast()
            }
            
            for  i in (0..<productArray.count)
            {
                if productArray[i].isUpdateButtonVisible
                {
                    showAlert(message: "update open product.")
                    flag = 1
                    break
                }
            }
            
            if flag == 0
            {
                productArray[tag].isUpdateButtonVisible = true
                changeProductStatus()
                heightConstraintOfSellForexTableview.constant = CGFloat(productArray.count * minimizedProductViewHeght + largeProductViewHeight + tableviewFooterHeight  + 100)
                setTableHeight()
                self.sellForexTableView.reloadData()
            }
        }
    }
    
    func didPressUpdateButton(tag: Int)
    {
        if   isValidProductForUpdate(index: tag)
        {
            let product : ProductBO = productArray[tag]
            product.isUpdateButtonVisible = false
            productArray[ tag] =  product
            changeProductStatus()
            heightConstraintOfSellForexTableview.constant = CGFloat(productArray.count * minimizedProductViewHeght + tableviewFooterHeight)
            self.sellForexTableView.reloadData()
        }
    }
    func didPressDeleteProductButton(_ tag: Int)
    {
        if productArray.count == 1
        {
            showAlert(message: "Cannot delete last product")
        }
        else
        {
            productArray.remove(at: tag)
            heightConstraintOfSellForexTableview.constant = CGFloat(productArray.count * minimizedProductViewHeght + 60)
            
        }
        self.sellForexTableView.reloadData()
    }
    
    // MARK: - Keyboard Handling Methods
    @objc func keyboardWasShown(notification: NSNotification)
    {
        var info = notification.userInfo!
        let keyboardSize = (info[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsets.init(top: 0.0, left: 0.0, bottom: keyboardSize!.height+10, right: 0.0)
        self.sellForexScrollView.contentInset = contentInsets
        self.sellForexScrollView.scrollIndicatorInsets = contentInsets
        var aRect : CGRect = self.contactDetailsView.frame
        aRect.size.height -= keyboardSize!.height
        if let activeField = self.activeTextField
        {
            if (!aRect.contains(activeField.frame.origin))
            {
                self.sellForexScrollView.scrollRectToVisible(activeField.frame, animated: true)
            }
        }
    }
    
    @objc func keyboardWillBeHidden(notification: NSNotification)
    {
        let contentInsets : UIEdgeInsets = UIEdgeInsets.init(top: 0.0, left: 0.0,bottom: 0.0, right: 0.0)
        self.sellForexScrollView.contentInset = contentInsets
        self.sellForexScrollView.scrollIndicatorInsets = contentInsets
        self.view.endEditing(true)
    }
    
    func setNotificationKeyboard ()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
}
