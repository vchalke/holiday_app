//
//  SignUpViewController.m
//  holidays
//
//  Created by Swapnil on 10/01/18.
//  Copyright © 2018 Mobicule. All rights reserved.
//

#import "SignUpViewController.h"
#import "Reachability.h"
#import "NetCoreAnalyticsVC.h"
#import "RegisterUserViewController.h"
#import "UIViewController+MJPopupViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface SignUpViewController ()<registerUserDelegate>
{
    LoadingView *activityLoadingView;
}

@end

@implementation SignUpViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.loginView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"login_bg.jpg"]];
    
    self.userIdView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.userIdView.layer.borderWidth = 0.5f;
    self.userIdView.layer.cornerRadius = 3;
    
    self.passwordView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.passwordView.layer.borderWidth = 0.5f;
    self.passwordView.layer.cornerRadius = 3;
    
    self.reEnterPasswordView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.reEnterPasswordView.layer.borderWidth = 0.5f;
    self.reEnterPasswordView.layer.cornerRadius = 3;
    
    [self addDoneButtonOnKeyboard];
}

-(void)addDoneButtonOnKeyboard
{
    UIToolbar* keyboardToolbar = [[UIToolbar alloc] init];
    [keyboardToolbar sizeToFit];
    UIBarButtonItem *flexBarButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                      target:nil action:nil];
    UIBarButtonItem *doneBarButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                      target:self action:@selector(doneButtonAction)];
    keyboardToolbar.items = @[flexBarButton, doneBarButton];
    self.textFieldUserId.inputAccessoryView = keyboardToolbar;
    self.textFieldPassword.inputAccessoryView = keyboardToolbar;
    self.textFieldReEnterPassword.inputAccessoryView = keyboardToolbar;
}

-(void)doneButtonAction
{
    [self.textFieldUserId resignFirstResponder];
    [self.textFieldPassword resignFirstResponder];
    [self.textFieldReEnterPassword resignFirstResponder];
}

-(void)showAlertViewWithTitle:(NSString *)title withMessage:(NSString *)message
{
    UIAlertController *alertView = [UIAlertController alertControllerWithTitle:title
                                                                       message:message
                                                                preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [alertView dismissViewControllerAnimated:YES completion:nil];
        
    }];
    
    [alertView addAction:okAction];
    
    [self presentViewController:alertView animated:YES completion:nil];
}

-(BOOL) checkEmailIdValidation
{
    if ([self.textFieldUserId.text isEqualToString:@""])
    {
        return NO;
    }
    else if(![self validateEmailWithString])
    {
        [self showAlertViewWithTitle:@"Alert" withMessage:@"please enter a valid emailId"];
        return NO;
    }
    return YES;
}

- (BOOL)validateEmailWithString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:self.textFieldUserId.text];
    
}

#pragma mark - Astra login flow -
-(void)loginCheckWithType:(NSString *)signInType
{
    //abc@test.com/
        activityLoadingView = [LoadingView loadingViewInView:self.view.superview.superview
                                                  withString:@""
                                           andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
    
    NSLog(@"signInType----> %@",signInType);
    
    @try
    {
        
        NetworkHelper *helper = [NetworkHelper sharedHelper];
        
        NSString *pathParam = _textFieldUserId.text;
        
        NSDictionary *headerDict = [CoreUtility getHeaderDict];
        
        [helper getResponseWithRequestType:@"" withQueryParam:@"" withPathParam:pathParam withJsonParam:nil withHeaders:headerDict withUrl:kAstraUrlLoginCheck success:^(NSDictionary *responseDict)
         {
             NSLog(@"Response Dict-----> : %@",responseDict);
             
             
             dispatch_async(dispatch_get_main_queue(), ^(void)
                            {
                                [activityLoadingView removeFromSuperview];
                                if (responseDict)
                                {
                                    if (responseDict.count>0)
                                    {
                                        if ([signInType isEqualToString:@"signIn"] || [signInType isEqualToString:@"signUp"])
                                        {
                                            [self savePasswordWithType:signInType];
                                        }
                                    }
                                    
                                }
                            });
             
        }
        failure:^(NSError *error)
        {
            dispatch_async(dispatch_get_main_queue(), ^(void)
            {
                [activityLoadingView removeFromSuperview];
                NSLog(@"Response Dict Error---> : %@",[error description]);
            });
        }];
    }
    @catch (NSException *exception)
    {
        NSLog(@"%@", exception.reason);
        [activityLoadingView removeFromSuperview];
    }
    @finally
    {
        NSLog(@"exception finally called");
    }
}

-(void)savePasswordWithType:(NSString *)signInType
{
    activityLoadingView = [LoadingView loadingViewInView:self.view.superview.superview
                                                  withString:@""
                                           andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
    
    NSLog(@"signInType_savePasswordWithType---> %@",signInType);
    @try
    {
        NetworkHelper *helper = [NetworkHelper sharedHelper];
        
        NSDictionary *headerDict = [CoreUtility getHeaderDict];
        
        [helper getResponseWithRequestType:@"" withQueryParam:@"" withPathParam:@"" withJsonParam:nil withHeaders:headerDict withUrl:kAstraUrlSavePass success:^(NSDictionary *responseDict)
         {
             NSLog(@"Response Dict savePasswordWithType---> : %@",responseDict);
             
             dispatch_async(dispatch_get_main_queue(), ^(void)
             {
                 [activityLoadingView removeFromSuperview];
                 if (responseDict)
                 {
                     if (responseDict.count>0)
                     {
                         NSString *keyString = [responseDict valueForKey:@"text"];
                         NSLog(@"keyString---> %@",keyString);
                         
                         [self fetchEncryptionPassWithKey:keyString withloginType:signInType];
                     }
                 }
             });
             
        }
        failure:^(NSError *error)
        {
            dispatch_async(dispatch_get_main_queue(), ^(void)
            {
                [activityLoadingView removeFromSuperview];
                NSLog(@"Response Dict savePasswordWithType Failure--->  : %@",[error description]);
            });
        }];
    }
    
    @catch (NSException *exception)
    {
        NSLog(@"%@", exception.reason);
        [activityLoadingView removeFromSuperview];
    }
    
    @finally
    {
        NSLog(@"exception finally called");
    }
}

-(void)fetchEncryptionPassWithKey:(NSString *)key withloginType:(NSString *)loginType
{
    activityIndicator = [LoadingView loadingViewInView:self.view withString:@"" andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
    
    
    //            "key": "495kcG744o"
    NSLog(@"fetchEncryptionPassWithKey_key %@ withloginType---> %@",key,loginType);
    
    NSDictionary *dict = [[NSDictionary alloc]initWithObjectsAndKeys:key, @"key",_textFieldPassword.text,@"password",nil];
    NSLog(@"dict_fetchEncryptionPassWithKey---> %@",dict);
    
    NetworkHelper *helper = [NetworkHelper sharedHelper];
    if([CoreUtility connected])
    {
        NSLog(@"in_CoreUtility");
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            NSString *strResponse = [helper getDataFromServerForType:@"sync" entity:@"encrypt" action:@"generate" andUserJson:dict];
            NSLog(@"strResponse---> %@",strResponse);
            
            dispatch_async(dispatch_get_main_queue(), ^(void)
            {
                [activityIndicator removeFromSuperview];
                               
                NSLog(@"in_CoreUtility---->");
                if (![strResponse isEqualToString:@"No Internet"])
                {
                    NSLog(@"No Internet---->");
                    NSString *strStatus = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"Status"];
                    NSString *strMessage = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"message"];
                   
                    if ([[strStatus lowercaseString] isEqualToString:kStatusSuccess])
                    {
                       
                        NSDictionary *dataDict = [[[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"data"] firstObject];
                        NSLog(@"dataDict----> %@",dataDict);
                       
                        RegisterUserViewController *registerUserVC = [[RegisterUserViewController alloc] initWithNibName:@"RegisterUserViewController" bundle:nil];
                        registerUserVC.dictforEncryption = dataDict;
                        registerUserVC.userId = self.textFieldUserId.text;
                        registerUserVC.delegate = self;
                        registerUserVC.signUpVC = @"signUpVC";
                        [self presentViewController:registerUserVC animated:YES completion:nil];
                    }
                    else
                    {
                        UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Alert"
                                                                                          message:strMessage
                                                                                   preferredStyle: UIAlertControllerStyleAlert];
                       
                        UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                        {
                           [alertView dismissViewControllerAnimated:YES completion:nil];
                           
                       }];
                       [alertView addAction:actionOk];
                       [self presentViewController:alertView animated:YES completion:nil];
                   }
               }
            });
        });
        
    }
    else
    {
        UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Alert"
                                                                           message:kMessageNoInternet
                                                                    preferredStyle: UIAlertControllerStyleAlert];
        
        UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
            [alertView dismissViewControllerAnimated:YES completion:nil];
            
        }];
        
        [alertView addAction:actionOk];
        [self presentViewController:alertView animated:YES completion:nil];
    }
}

-(void)dismissWithSuccessFullRegistration
{
    NSLog(@"dismissWithSuccessFullRegistration");
     [self netCoreDataForLoginWithEmailID:self.textFieldUserId.text];  //09-04-2018
    [self setUserDetailsAndClosePopUp];
}


-(void)netCoreDataForLoginWithEmailID:(NSString *)emailID
{
    
    NSMutableDictionary *payloadList =  [NSMutableDictionary dictionary];
    [payloadList setObject:emailID forKey:@"EMAIL"];
    
    [NetCoreAnalyticsVC getNetCorePushNotificationDetailDictionary:payloadList withPayloadCount:102];
}

-(void)setUserDetailsAndClosePopUp
{
    [[NSUserDefaults standardUserDefaults]setValue:self.textFieldUserId.text forKey:kuserDefaultUserId];
    [[NSUserDefaults standardUserDefaults]setValue:self.textFieldPassword.text forKey:kuserDefaultPassword];
    
    NSUserDefaults *LoginStatus=[NSUserDefaults standardUserDefaults];
    [LoginStatus setObject:kLoginSuccess  forKey:kLoginStatus];
    
    NSUserDefaults *userIdForGooglePlusSignIn=[NSUserDefaults standardUserDefaults];
    [userIdForGooglePlusSignIn setObject:self.textFieldUserId.text forKey:kLoginEmailId];
    NSString *userId=[userIdForGooglePlusSignIn valueForKey:kLoginEmailId];
    NSLog(@"userId---> %@",userId);
 
    [self.delegate userloggedInSignUpSuccessFullWithUserDict:userId];
    
    [self dismissViewControllerAnimated:NO completion:nil];
}

#pragma mark - text field delegate -
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([string isEqualToString:@""])
    {
        return YES;
    }
    if (textField == self.textFieldUserId)
    {
        if([self.textFieldUserId.text length]<=100)
        {
            //        CGRect rect = self.textFieldUserId.rightView.frame;
            //        // only when adding on the end of textfield && it's a space
            //        if (range.location == self.textFieldUserId.text.length && [string isEqualToString:@" "]) {
            //            rect.size.width += 25; // depends on font!
            //        }
            //        else if (string.length == 0) { // backspace
            //            rect.size.width -= 25;
            //            rect.size.width = MAX(0, rect.size.width);
            //        }
            //        else
            //        {
            //            rect.size.width = 0;
            //        }
            //        self.textFieldUserId.rightView.frame = rect;
            
            return YES;
        }
        else
        {
            return NO;
        }
    }
    
    if (textField == self.textFieldPassword)
    {
        if([self.textFieldPassword.text length]<=25)
        {
            CGRect rect = self.textFieldPassword.rightView.frame;
            if (range.location == self.textFieldPassword.text.length && [string isEqualToString:@" "])
            {
                rect.size.width += 25;
            }
            else if (string.length == 0) {
                rect.size.width -= 25;
                rect.size.width = MAX(0, rect.size.width);
            }
            else
            {
                rect.size.width = 0;
            }
            self.textFieldPassword.rightView.frame = rect;
            
            return YES;
        }
        else
        {
            return NO;
        }
    }
    
    if (textField == self.textFieldReEnterPassword)
    {
        if([self.textFieldReEnterPassword.text length]<=25)
        {
            CGRect rect = self.textFieldReEnterPassword.rightView.frame;
            
            if (range.location == self.textFieldReEnterPassword.text.length && [string isEqualToString:@" "]) {
                rect.size.width += 25;
            }
            else if (string.length == 0)
            {
                rect.size.width -= 25;
                rect.size.width = MAX(0, rect.size.width);
            }
            else
            {
                rect.size.width = 0;
            }
            self.textFieldReEnterPassword.rightView.frame = rect;
            
            return YES;
        }
        else
        {
            return NO;
        }
    }
    
    return YES;
    
}

#pragma mark - Button Actions
- (IBAction)closeButtonClicked:(UIButton *)sender
{
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)signUpButtonClicked:(UIButton *)sender
{
    if ([self checkEmailIdValidation] && ![self.textFieldPassword.text isEqualToString:@""] && ![self.textFieldReEnterPassword.text isEqualToString:@""])
    {
        
        if ([self.textFieldPassword.text isEqualToString:self.textFieldReEnterPassword.text]) {
            
            //[self registerUser];
            [self loginCheckWithType:@"signUp"];
            
        }
        else
            [self showAlertViewWithTitle:@"Alert" withMessage:@"Entered password and re-entered password not matched"];
    }
    else
    {
        if ([self.textFieldUserId.text isEqualToString:@""])
        {
            [self showAlertViewWithTitle:@"Alert" withMessage:@"Enter username"];
        }
        else if ([self.textFieldPassword.text isEqualToString:@""] ||[self.textFieldReEnterPassword.text isEqualToString:@""] )
        {
            [self showAlertViewWithTitle:@"Alert" withMessage:@"Enter password"];
        }
    }
}

@end
