//
//  INetworkManager.h
//  mobicule-sync-core
//
//  Created by Kishan on 16/07/15.
//  Copyright (c) 2015 Mobicule. All rights reserved.
//

#import <Foundation/Foundation.h>


#define SERVER_NOT_ACCESS      @"404"
#define INTERNET_NOT_ACCESS    @"No Internet"
#define NETWORK_FAILED         @"Network Failed"
#define TIMED_OUT_ERROR        @"Connection Timed Out"

// Entity Sync Constant
#define SERVER_NOT_FOUND       @"Server Not Found"
#define NO_INTERNET_ACCESS     @"No Internet Access"
#define CONNECTION_TIMED_OUT   @"Connection Timed Out"


@protocol INetworkManager <NSObject>

/**
 *  This is use for sent post request.
 *
 *  @param request pass request data that set data in request body.
 *
 *  @return Response is string type associated with particular request.
 */
- (NSString *)sendPostRequest:(NSString*)request;
- (NSString *)sendPostRequest1:(NSString*)request withCompletionHandler:(void (^)(NSString* response))handler ;
/**
 *  This is use for sent get request.
 *
 *  @param request pass empty request data string because no need to set data in request body.
 *
 *  @return Response is string type associated with particular request.
 */
- (NSString *)sendGetRequest:(NSString*)request;         

/**
 *  Set the network component configuration in Network Manager interface.
 *
 *  @param isLogEnable set log configuration for network Component.
 *
 *  @return nothing
 */
- (void)setIsLogEnabled:(BOOL)isLogEnabled;

/**
 *  Set the network component configuration in Network Manager interface.
 *
 *  @param authorization set authorization configuration in network Component.
 *
 *  @return nothing
 */
- (void)setAuthorization:(NSString*)authorization;

/**
 *  Set the network component configuration in Network Manager interface.
 *
 *  @param URL set URL configuration in network Component.
 *
 *  @return nothing
 */
- (void)setServerUrl:(NSString*)URL;

/**
 *  This is validate URL.
 *
 *  @return status
 */
- (BOOL)validateConfiguration ;
@end
