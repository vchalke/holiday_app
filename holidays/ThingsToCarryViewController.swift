//
//  ThingsToCarryViewController.swift
//  sotc-consumer-application
//
//  Created by Saurav Kumar on 09/08/17.
//  Copyright © 2017 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit

class ThingsToCarryViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate  {

    
    
 
    
    
    @IBOutlet weak var scrollViewImageParent: UIScrollView!
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet var topLabel: UILabel!
    @IBOutlet var thingsToCarryTableView: UITableView!
    @IBOutlet var bottomLabel: UILabel!
    var tourDetail:Tour?
    
    let titleArray : [String] = ["Sun screen lotion","Hand sanitizer","Sun glasses","Cap","Sports shoes","Umbrella","Windcheater","Camera","Woolens (If required)"]
    
    let imgArray = [#imageLiteral(resourceName: "sunScreenToCarry"),#imageLiteral(resourceName: "handSanitizerToCarry"),#imageLiteral(resourceName: "sunGlasstoCarry"),#imageLiteral(resourceName: "capToCarry"),#imageLiteral(resourceName: "sportsShoesToCarry"),#imageLiteral(resourceName: "umbrellaToCarry"),#imageLiteral(resourceName: "windcheaterToCarry"),#imageLiteral(resourceName: "cameraToCarry"),#imageLiteral(resourceName: "wooleansToCarry")]
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //thingsToCarryTableView.register(UITableViewCell.self, forCellReuseIdentifier: "cellIndentifier")
        
        // image zoom on double tap
        // if we do this on image view it will occupy more space and hence inorder to save that space scrollview is used
        scrollViewImageParent.maximumZoomScale=4
        scrollViewImageParent.minimumZoomScale=1.0
        scrollViewImageParent.bounces=true
        scrollViewImageParent.bouncesZoom=true
        scrollViewImageParent.contentSize = CGSize(width: imageView.frame.size.width,  height: imageView.frame.size.height)
        scrollViewImageParent.showsHorizontalScrollIndicator=true
        scrollViewImageParent.showsVerticalScrollIndicator=true
        scrollViewImageParent.delegate = self
        self.scrollViewImageParent.addSubview(imageView)
        self.view.addSubview(scrollViewImageParent)
        
        self.getHandyTipsDetails()
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView?
    {
        return self.imageView
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.setRedNavigationBar()
        self.navigationController?.setnavigatiobBarTitle(title: Title.HANDY_TIPS, viewController: self)
        self.navigationController?.setEmergencyNavigationRightButton(showEmergencyButton: true, viewController: self)
        self.navigationController?.setNagationBackButton(showRightButton: true, viewController: self)
        
    }
    
    
    func backButtonClick() -> Void {
        
        
     
        
        self.navigationController?.popViewController(animated: true)
        
        self.navigationController?.setNavigationBarTranslucent()
        
    }
    
    //MARK: UINavigationController Delegate
   
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //Mark -- UITableViewDataSource
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 50
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return titleArray.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellIndentifier", for: indexPath)
        
        cell.textLabel? .font = UIFont(name:"Roboto-Medium", size:15)
        
        cell.textLabel? .text = titleArray[indexPath.row]
        cell.imageView?.image = imgArray[indexPath.row]
        
        if cell.textLabel? .text == "Woolens (If required)"
        {
            
            let myString = cell.textLabel? .text
            var myMutableString = NSMutableAttributedString()
            myMutableString = NSMutableAttributedString(string: myString! , attributes: convertToOptionalNSAttributedStringKeyDictionary([convertFromNSAttributedStringKey(NSAttributedString.Key.font):UIFont(name: "Roboto-Regular", size: 15.0)!]))
            
            myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.init(red: 62/255.0, green: 129/255.0, blue: 197/255.0, alpha: 1.0), range: NSRange(location:8,length:13))
            
            cell.textLabel?.attributedText = myMutableString
            
        }
        
        
        
        
        return cell;
    }

    func getHandyTipsDetails()
    {
        // if let visaDocUrl:String =  tourDetails?.visaDocUrl
        // {
        //"http://uat2.thomascook.in/soto/lt/productitineraryDownload.do?itineraryCode=2017EUR228S1&filePath=Master Documentation checklist for Visa.pdf&docType=Visa Requirement"
        
        /* let documentFolderPath =  TourDocumentController.getTourFileDirectoryPath(bfnNumber: (tourDetails?.bfNumber)!)
         
         let visaDocFilePath = documentFolderPath.bfNumberPath + "/" + "visaDocUrl.pdf"
         
         self.downloadDocument(filePath: visaDocFilePath, url: visaDocUrl)
         
         }else{
         
         self.displayAlert(message: "No documnet details found")
         }*/
        var url = ""
        
        if let regionCode = tourDetail?.region
        {
            
            let countryCode = regionCode.uppercased()
            
            if countryCode != ""
            {
                switch countryCode {
                    
                case Constant.USA_CONTRY_CODE_USA,Constant.USA_CONTRY_CODE_USAHS,Constant.USA_CONTRY_CODE_HSDEMO,Constant.USA_CONTRY_CODE_USAFIT,Constant.USA_CONTRY_CODE_USAHSA,Constant.USA_CONTRY_CODE_SOUTHAMERICA:
                    
                    url = DocumentsURLConstant.Handy_TIPS_USA
                    break
                    
                case Constant.EXO_CONTRY_CODE_EXO:
                    
                    url = DocumentsURLConstant.Handy_TIPS_EXO
                    break
                case Constant.FAR_CONTRY_CODE_ASIA,Constant.FAR_CONTRY_CODE_ASIAFT,Constant.FAR_CONTRY_CODE_ASIAHS,Constant.FAR_CONTRY_CODE_MAYA,Constant.FAR_CONTRY_CODE_INDIAHS,Constant.FAR_CONTRY_CODE_INDIAFIT,Constant.FAR_CONTRY_CODE_MIDDLEEASTFIT,Constant.RSA_CONTRY_CODE_AFRICAHSA,Constant.RSA_CONTRY_CODE_AFRICA,Constant.RSA_CONTRY_CODE_HSAAFRICA:
                    
                    url = DocumentsURLConstant.Handy_TIPS_FAR
                    break
                    
                case Constant.EUR_CONTRY_CODE_AGD,Constant.EUR_CONTRY_CODE_EUROPE,Constant.EUR_CONTRY_CODE_EUROPEHS:
                    
                    url = DocumentsURLConstant.Handy_TIPS_EUR
                    break
                    
                case Constant.AUS_CONTRY_CODE_ANZ,Constant.AUS_CONTRY_CODE_ANZHS:
                    
                    url = DocumentsURLConstant.Handy_TIPS_AUS
                    break
                case Constant.RSA_CONTRY_CODE_HSAAFRICA,Constant.RSA_CONTRY_CODE_AFRICA,Constant.RSA_CONTRY_CODE_AFRICAHSA:
                    
                    url = DocumentsURLConstant.Handy_TIPS_RSA
                    break
                    
                    
                    
                default:
                    url = ""
                    
                }
                
                
            }
            
        }
        
        // }
        if url != ""
        {
            let documentFolderPath =  TourDocumentController.getTourFileDirectoryPath(bfnNumber: (tourDetail?.bfNumber)!)
            
            let visaDocFilePath = documentFolderPath.bfNumberPath + "/" + "handtTips.jpg"
            
            self.downloadDocument(filePath: visaDocFilePath, url: url)
            
            
        }else
        {
            self.displayAlert(message: AlertMessage.NO_DOCUMENT)
        }
        
        
    }

    
    func filterTourCode(tourCode:String,uptoLength:Int) -> String
    {
        
        if !((tourCode.isEmpty))
        {
            let first4 = tourCode.substring(to:tourCode.index(tourCode.startIndex, offsetBy: 3))
            
            return first4
            
        }
        
        return ""
        
        
    }
    
    func downloadDocument(filePath:String ,url:String) {
        
        //  let passengerDocumnet:TourPassengerDouments =  documentArray.first!
        
        LoadingIndicatorView.show("Loading")
        
        NetworkCommunication.downloadDocuments(urlString: url, destinationFilePath: filePath, completion: { (downlodedfilePath, status) in
            LoadingIndicatorView.hide()
            printLog(status,filePath);
            
            if(status)
            {
                self.imageView.image = UIImage(contentsOfFile: filePath)
            }
            else{
                
                let isFilePresent = TourDocumentController.isFileExistInDocumentDirectory(fileNamePath: filePath)
                
                if(isFilePresent.chkFile)
                {
                    //self.imageView.image = UIImage(contentsOfFile: filePath)
                    
                    
                    
                }else{
                        self.downloadFaildAlert(message: AlertMessage.DOCUMENT_DOWNLOAD_FAIL)
                   
                    
                }
                
            }
            
            
            
            
        })
    }
    
    
    func displayAlert(message:String)
    {
        let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: AlertMessage.TITLE_OK, style: UIAlertAction.Style.default, handler:{
            alert -> Void in
            
            self.navigationController?.popViewController(animated: true)
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func downloadFaildAlert(message:String)
    {
        let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: AlertMessage.TITLE_CANCEL, style: UIAlertAction.Style.default, handler: {
            alert -> Void in
            
            self.navigationController?.popViewController(animated: true)
        }))
        
        alert.addAction(UIAlertAction(title: AlertMessage.TITLE_TRY_AGAIN, style: UIAlertAction.Style.default, handler: {
            alert -> Void in
            
            self.getHandyTipsDetails()
            
        }))
        
        
        
        self.present(alert, animated: true, completion: nil)
    }
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromNSAttributedStringKey(_ input: NSAttributedString.Key) -> String {
	return input.rawValue
}
