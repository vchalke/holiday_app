//
//  ForexDataController.swift
//  holidays
//
//  Created by Saurav on 09/12/17.
//  Copyright © 2017 Mobicule. All rights reserved.
//

import UIKit

class ForexDataController: NSObject {
    
    
 
    func execTaskWithAlamofire(pathParam:NSString, queryParam:NSString ,requestType:NSString, jsonDict:NSDictionary, taskCallback: @escaping (Bool,
        AnyObject?) -> ())
    {
        
        var urlComponents = URLComponents(string: "https://services-uatastra.thomascook.in")!
        
        if pathParam != ""
        {
            urlComponents.path = pathParam as String
        }
        
        if queryParam != ""
        {
            urlComponents.query = queryParam as String
            
        }
        
        let url = urlComponents.url;
        
        let tokenId  : NSString = UserDefaults.standard.value(forKey: "userDefaultTokenId") as! NSString;
        let requestId : NSString = UserDefaults.standard.value(forKey: "userDefaultRequestId") as! NSString;
        
        
        let headerFielda:HTTPHeaders = ["Accept":"application/json","Content-Type":"application/json","requestId":requestId as String,"sessionId":tokenId as String]
        
        request(url!, method: HTTPMethod.post, parameters: (jsonDict as! Parameters), encoding: JSONEncoding.default, headers: headerFielda).responseString { (dataResponse) in
            
            print("Response : ----- \(String(describing: dataResponse))")
            
            
        }
        
    }
    


}
