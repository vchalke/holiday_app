//
//  CalendarViewController.h
//  holidays
//
//  Created by ketan on 19/10/15.
//  Copyright © 2015 Mobicule. All rights reserved.
//

#import "BaseViewController.h"
#import "RadioButton.h"
#import "HorizontalScrollCell.h"
#import "BookNowViewController.h"
#import "HolidayPackageDetail.h"

@protocol calenderDelegate;
@interface CalendarViewController : BaseViewController<UICollectionViewDataSource, UICollectionViewDelegate,dismissView>
{
    LoadingView *activityLoadingView;
}
@property (strong ,nonatomic) HolidayPackageDetail *packageDetail;
@property (strong, nonatomic) IBOutlet UIView *calendarView;
@property (strong,nonatomic) NSArray *roomRecordArray;
- (IBAction)onTabButtonsClicked:(id)sender;
@property (strong, nonatomic) IBOutlet UICollectionView *collection;
@property BOOL isMRP;
@property (strong,nonatomic) NSDictionary *dataDict;
- (IBAction)onProceedButtonClicked:(id)sender;
@property (strong,nonatomic) id<calenderDelegate> delegate;
@property (strong ,nonatomic) NSArray *arrayTravellerCalculation;
@property (strong,nonatomic)  NSString *packageId;
@property (strong,nonatomic) NSString *hubName;
@property (nonatomic) int accomType;
- (IBAction)onDownArrowButtonClick:(id)sender;
- (IBAction)onUpArrowButtonClick:(id)sender;
@property (strong,nonatomic) NSDictionary *selectedHubDict;
@property (strong,nonatomic) NSDictionary *selectedStateDict;
@end





@protocol calenderDelegate
@optional
- (void)SetDataForCalendarDate:(NSDictionary *)dateDict withCalculationData:(NSDictionary *)calculationData;

@required



@end
