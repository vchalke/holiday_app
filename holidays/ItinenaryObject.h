//
//  ItinenaryObject.h
//  holidays
//
//  Created by Kush_Tech on 04/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ItinenaryObject : NSObject
-(instancetype)initWithItinenaryDict:(NSDictionary *)dictionary;
@property(nonatomic,strong)NSString *altTag;
@property(nonatomic,strong)NSString *highlights;
@property(nonatomic)NSInteger holidayItineraryId;
@property(nonatomic,strong)NSString *image;
@property (nonatomic) BOOL isActive;
@property(nonatomic)NSInteger itineraryDay;
@property(nonatomic,strong)NSString *itineraryDescription;
@property(nonatomic,strong)NSString *mealDescription;
@property(nonatomic,strong)NSString *overnightStay;

@end

NS_ASSUME_NONNULL_END
