//
//  FilterThemeTableViewCell.h
//  holidays
//
//  Created by Kush_Team on 24/06/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FilterThemeTableViewCell : UITableViewCell<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
{
    NSArray *themesArray;
    NSString *selectTheme;
}
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
-(void)setupTableViewCell;

@property (strong,nonatomic) NSMutableDictionary *themeFilterDict;

@end

NS_ASSUME_NONNULL_END
