//
//  PackageDetailTableViewCell.h
//  holidays
//
//  Created by Kush_Tech on 03/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HolidayPackageDetail.h"
NS_ASSUME_NONNULL_BEGIN

@protocol PackageDetailTableViewCellDelegaete <NSObject>
@optional
- (void)clickViewIndex:(NSInteger)viewIndex inclusionStr:(NSString*)string;
@end


@interface PackageDetailTableViewCell : UITableViewCell{
    int labelNumOFLines;
    NSMutableArray *titlesAray;
}
@property (nonatomic, weak) id <PackageDetailTableViewCellDelegaete> packageDetailDelegate;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Title;
@property (weak, nonatomic) IBOutlet UILabel *lbl_duration;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Destination;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_StackViewWidth;
@property (weak, nonatomic) IBOutlet UIView *contentsViews;
@property (weak, nonatomic) IBOutlet UIView *topInfoeViews;
@property (weak, nonatomic) IBOutlet UIView *flightView;
@property (weak, nonatomic) IBOutlet UIView *hotelView;
@property (weak, nonatomic) IBOutlet UIView *sightView;
@property (weak, nonatomic) IBOutlet UIView *visaView;
@property (weak, nonatomic) IBOutlet UIView *transferView;
@property (weak, nonatomic) IBOutlet UIView *mealView;

@property (weak, nonatomic) IBOutlet UIButton *btn_Flights;
@property (weak, nonatomic) IBOutlet UIButton *btn_Hotels;
@property (weak, nonatomic) IBOutlet UIButton *btn_Sightseeing;
@property (weak, nonatomic) IBOutlet UIButton *btn_Visa;
@property (weak, nonatomic) IBOutlet UIButton *btn_Transfer;
@property (weak, nonatomic) IBOutlet UIButton *btn_Meals;
@property (weak, nonatomic) IBOutlet UIButton *btn_upDownToggle;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cnst_LabelHeight;

@property (nonatomic, strong) NSArray *siteArray;

@property (strong ,nonatomic) HolidayPackageDetail *packageDetail;
-(void)showAllPackageDetail:(HolidayPackageDetail*)detailModel;
@end

NS_ASSUME_NONNULL_END
