//
//  CalenderCollectionViewCell.h
//  holidays
//
//  Created by Kush_Tech on 24/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DateStringSingleton.h"
#import "NewMasterCollectionViewCell.h"
NS_ASSUME_NONNULL_BEGIN

@interface CalenderCollectionViewCell : NewMasterCollectionViewCell{
    NSDate *indexDate;
}
@property (weak, nonatomic) IBOutlet UIView *base_view;
@property (weak, nonatomic) IBOutlet UILabel *lbl_date;
@property (weak, nonatomic) IBOutlet UILabel *lbl_dashLine;
@property (weak, nonatomic) IBOutlet UILabel *lbl_prize;
@property (strong, nonatomic) NSDate *selectedDate;
-(void)loadDateDataInCell:(NSDate*)date withIndex:(NSInteger)index withCalederDateDict:(NSDictionary*)dataDict;
@end

NS_ASSUME_NONNULL_END
