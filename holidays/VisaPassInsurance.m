//
//  VisaPassInsurance.m
//  holidays
//
//  Created by Kush_Team on 17/07/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "VisaPassInsurance.h"

@implementation VisaPassInsurance
-(instancetype)initWithVisaPassInsuranceModel:(NSDictionary *)dictionary{
    if ([super init])
    {
        self.insurance = [dictionary valueForKey:@"insurance"];
        self.packageClassId = [[dictionary valueForKey:@"packageClassId"]integerValue];
        self.passport = [dictionary valueForKey:@"passport"];
        self.visa = [dictionary valueForKey:@"visa"];
        self.visaPasInsSrp = [dictionary valueForKey:@"visaPasInsSrp"];
    }
    return self;
}
@end
/*
 {
   "holidayVisaId": 138,
   "insurance": "<p>asdsad</p>",
   "isActive": "Y",
   "packageClassId": "1",
   "passport": "<p>passport not required</p>",
   "visa": "<p>visa included</p>",
   "visaPasInsSrp": "<p>asdad</p>"
 },
 */

