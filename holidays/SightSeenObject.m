//
//  SightSeenObject.m
//  holidays
//
//  Created by Kush_Tech on 09/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "SightSeenObject.h"

@implementation SightSeenObject



-(instancetype)initWithSightSeenObjectDict:(NSDictionary *)dictionary{
    if ([super init])
    {
        self.packageClassId = [[dictionary valueForKey:@"packageClassId"] integerValue];
        NSDictionary *newDict = [dictionary valueForKey:@"sightseeingId"];
        NSDictionary *newCityDict = [newDict valueForKey:@"cityCode"];
        self.cityName = [newCityDict valueForKey:@"cityName"];
        self.name = ([newDict valueForKey:@"name"]) ? [newDict valueForKey:@"name"] : @"";
        self.descriptionName = ([newDict valueForKey:@"description"]) ? [newDict valueForKey:@"description"] : @"";
    }
    
    return self;
}
@end
