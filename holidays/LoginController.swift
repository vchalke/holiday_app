//
//  LoginController.swift
//  sotc-consumer-application
//
//  Created by Saurav Kumar on 09/08/17.
//  Copyright © 2017 Mobicule Technologies Private Limited. All rights reserved.
//

import Foundation

class LoginController: NSObject {
    
    
    class  func sendCheckMobileRequest(mobileNumber:String , completion: @escaping (_ status:(statusCode: Int, status: Bool , message:String)) -> Void)   {
        
        let requestString  = XMLParserRequestBuilder.CheckMobileNumberSoapRequest(mobileNumber: mobileNumber)
        
        
        let loginNetworkManager:LoginNetworkManager = LoginNetworkManager()
        
        
       // loginNetworkManager.sendCheckMobileNumberSoapRequest(soapRequestString: requestString) { (String,status) in
             loginNetworkManager.sendCheckMobileNumberSoapRequestwithMiddleware(soapRequestString: requestString) { (String,status) in
            
            printLog(String)
            
            if(status)
            {
                
                if String.trimmingCharacters(in: ["\n", " "]) != ""
                {
                    
                    do {
                        
                        let soapRequest = try AEXMLDocument(xml: String)
                        
                        printLog(soapRequest[APIConstants.CheckMobileResponseTag][APIConstants.ResponseCodeTag].string )
                        
                        printLog(soapRequest[APIConstants.CheckMobileResponseTag][APIConstants.ResponseDescriptionTag].string )
                        
                        if !(soapRequest[APIConstants.CheckMobileResponseTag][APIConstants.ResponseCodeTag].string.isEmpty) && (soapRequest[APIConstants.CheckMobileResponseTag][APIConstants.ResponseCodeTag].string == APIConstants.ResponseSuccessCode) && (soapRequest[APIConstants.CheckMobileResponseTag][APIConstants.ResponseDescriptionTag].string.lowercased() == APIConstants.ResponseAvailable.lowercased() )
                        {
                            completion(NetworkValidationMessages.ValidMobileNumber)
                        }
                        else
                        {
                            completion(NetworkValidationMessages.NotValidMobileNumber)
                        }
                        
                        
                    } catch   {
                        
                        completion(NetworkValidationMessages.NotValidMobileNumber)
                        
                    }
                }
                else
                {
                    completion(NetworkValidationMessages.NotValidMobileNumber)
                }
                
                
            }else{
                
                completion(NetworkValidationMessages.InternetNotConnected)
                
            }
            
            
        }
        
        
    }
    class func sendGeneratedOTPToServer(mobileNumber:String ,otp:String, completion: @escaping (_ networkStatus:(statusCode: Int, status: Bool , message:String)) -> Void)   {
        
        
        let loginNetworkManager:LoginNetworkManager = LoginNetworkManager()
        
        
       // loginNetworkManager.sendOTPtoServer(mobileNumber: mobileNumber,otp: otp) { (String,status) in
            loginNetworkManager.validateOTPViaMiddleware(mobileNumber: mobileNumber,otp: otp) { (String,status,message) in
            printLog(String)
            
            if status
            {
                completion(NetworkValidationMessages.ValidOTP)
                
            }else{
                
                completion(NetworkValidationMessages.InternetNotConnected)
            }
        }
        
    }
    class func sendOTPRequest(mobileNumber:String , completion: @escaping (_ networkStatus:(statusCode: Int, status: Bool , message:String)) -> Void)   {
        
        
        let loginNetworkManager:LoginNetworkManager = LoginNetworkManager()
        
        
        loginNetworkManager.sendOTPRequest(mobileNumber: mobileNumber) { (String,status) in
            
            printLog(String)
            
            if status
            {
                completion(NetworkValidationMessages.ValidOTP)
                
            }else{
                
                completion(NetworkValidationMessages.InternetNotConnected)
            }
        }
        
    }
    
    class func sendOTPValidateRequest(mobileNumber:String , otpString:String,completion: @escaping (_ networkStatus:(statusCode: Int, status: Bool , message:String)) -> Void)   {
        
        
        let loginNetworkManager:LoginNetworkManager = LoginNetworkManager()
        
        
        loginNetworkManager.sendOTPValidateRequest(mobileNumber: mobileNumber, otpString: otpString) { (networkStatus) in
            
           
                completion(networkStatus)
            
        }
        
        
    }
    
    
    class func clearDataForOldUser()
    {
        TourDocumentController.deleteAllFileForCurrentUser()
        
        UserDefaults.standard.set(false, forKey: UserDefauldConstant.isSubscribeForPushNotification)
        
        NotificationController.removeAllNotification()
    }
    
    
}
