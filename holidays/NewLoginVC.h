//
//  NewLoginVC.h
//  holidays
//
//  Created by Kush_Team on 27/05/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "NewMasterVC.h"
#import "RegisterUserViewController.h"
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <GoogleSignIn/GoogleSignIn.h>
#import "SignInViewController.h"
#import "Reachability.h"
#import <AuthenticationServices/AuthenticationServices.h>
NS_ASSUME_NONNULL_BEGIN
extern NSString* const setCurrentIdentifier;
@interface NewLoginVC : NewMasterVC<ASAuthorizationControllerDelegate,ASAuthorizationControllerPresentationContextProviding>
@property (strong ,nonatomic) NSDictionary *notificationDict;
@property (weak, nonatomic) IBOutlet UIView *main_baseBottomViews;
@property (weak, nonatomic) IBOutlet UIView *main_baseViews;
@property (weak, nonatomic) IBOutlet UIView *main_baseViewOne;
@property (weak, nonatomic) IBOutlet UIView *main_baseViewTwo;
@property (weak, nonatomic) IBOutlet UIView *main_baseViewThree;
@property (weak, nonatomic) IBOutlet UIView *main_baseViewThree_Otp;
@property (weak, nonatomic) IBOutlet UIView *main_baseViewThree_pass;

@property (weak, nonatomic) IBOutlet UIView *sign_AppLeView;

@property (weak, nonatomic) IBOutlet UITextField *txt_email;
@property (weak, nonatomic) IBOutlet UITextField *txt_password;
@property (weak, nonatomic) IBOutlet UITextField *txt_otp;

@property (weak, nonatomic) IBOutlet UIButton *btn_passWord;
@property (weak, nonatomic) IBOutlet UIButton *btn_otp;
@property (weak, nonatomic) IBOutlet UIButton *btn_showPassword;
@property (weak, nonatomic) IBOutlet UIButton *btn_continue;
@property (weak, nonatomic) IBOutlet UIButton *btn_shareFacebook;
@property (weak, nonatomic) IBOutlet UIButton *btn_shareGooglr;
@property (weak, nonatomic) IBOutlet UIButton *btn_logins;
@property (weak, nonatomic) IBOutlet UIButton *btn_resend_otp;
@property (weak, nonatomic) IBOutlet UIButton *btn_signUp;
@property (weak, nonatomic) IBOutlet UIButton *btn_forgotPass;

@property (strong, nonatomic) NSString *emailField;
@property (strong, nonatomic) NSString *passField;
@property (strong, nonatomic) NSString *imgUrlField;
@property (strong, nonatomic) NSString *firstNameField;
@property(strong) NSString * UserEmailID;

-(void)loginCheckWithType:(NSString *)signInType withEmail:(NSString*)emailString withPass:(NSString*)password;
-(void)netCoreDataForLoginWithEmailID:(NSString *)emailID;
-(void)verifyCredentialsWithText1:(NSString *)text1 withText2:(NSString *)text2 withText3:(NSString *)text3 withloginType:(NSString *)loginType withOtp:(NSString*)otp withEmail:(NSString*)emailId;
-(void)savePasswordWithType:(NSString *)signInType;
@end

NS_ASSUME_NONNULL_END
