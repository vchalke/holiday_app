//
//  PixelSingleton.h
//  holidays
//
//  Created by Kush_Team on 24/08/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PixelSingleton : NSObject
+ (id)sharedInstance;
- (id)init;
-(CGFloat)sizeInPixel:(CGFloat)pixel withBase:(CGFloat)basePixel;
@end

NS_ASSUME_NONNULL_END
