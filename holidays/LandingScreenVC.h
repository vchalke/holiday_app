//
//  LandingScreenVC.h
//  holidays
//
//  Created by Kush_Tech on 22/02/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "customView.h"
#import "BottomView.h"
#import "NewMasterVC.h"
#import "CoreDataSingleton.h"
#import "Holiday.h"
#import "BottomStacksView.h"
#import "WhatsNewObject.h"
NS_ASSUME_NONNULL_BEGIN

@interface LandingScreenVC : NewMasterVC{
    NSMutableDictionary *payloadList;
}
@property (strong ,nonatomic) NSDictionary *notificationDict;
@property (weak, nonatomic) IBOutlet UIPageControl *bannerPageControl;
@property (weak, nonatomic) IBOutlet UICollectionView *banner_CollectionView;
@property (weak, nonatomic) IBOutlet UICollectionView *recentlyAdded_CollectionView;
@property (weak, nonatomic) IBOutlet UICollectionView *whtasNew_CollectionView;
@property (weak, nonatomic) IBOutlet UIPageControl *whtasNewPageCtrl_CollectionView;
@property (weak, nonatomic) IBOutlet UICollectionView *advertise_CollectionView;
@property (weak, nonatomic) IBOutlet UICollectionView *lastMinutesDeals_CollectionView;
@property (weak, nonatomic) IBOutlet UICollectionView *travelVlog_CollectionView;

@property (weak, nonatomic) IBOutlet UIView *banner_view;
@property (weak, nonatomic) IBOutlet UIView *recent_view;
@property (weak, nonatomic) IBOutlet UIView *recent_baseview;
@property (weak, nonatomic) IBOutlet UIView *whatsNew_view;
@property (weak, nonatomic) IBOutlet UIView *advertise_view;
@property (weak, nonatomic) IBOutlet UIView *lastDeals_view;
@property (weak, nonatomic) IBOutlet UIView *travelVlog_view;

// Constraints

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_RecentlyViewLbl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_htOfBankOffers;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cnst_topOfRecentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cnst_heightOfRecentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cnst_topToWhatsNew;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cnst_bottomToWhatsNew;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cnst_topToLastMinDeal;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cnst_bottomToLastMinDeal;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cnst_topToTravelBlog;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cnst_bottomToTravelBlog;



@property (weak, nonatomic) IBOutlet customView *customTopView;
@property (weak, nonatomic) IBOutlet UIView *bottomBaseView;
//@property (weak, nonatomic) IBOutlet BottomView *customBottomView;
@property (weak, nonatomic) IBOutlet BottomStacksView *customBottomView;


@property (weak, nonatomic) IBOutlet UILabel *lbl_WhatsNew;
@property (weak, nonatomic) IBOutlet UILabel *lbl_LastMinuteDeals;

-(void)jumpToControllerfromViewNum:(NSInteger)firstVCNum toViewNum:(NSInteger)secVCNum;
-(void)jumpToPdpFromLandingScreen:(Holiday *)objHoliday;
-(void)searchDestinationWithSearchType:(NSString *)searchTypeStr withDestName:(NSString*)searchText;
-(void)jumpToDestinationWiseDictObjcet:(WhatsNewObject*)newObject;
-(void)searchDestinationSelectDictUsingQueryParamwithText:(NSString*)queryParameter;
@end

NS_ASSUME_NONNULL_END
