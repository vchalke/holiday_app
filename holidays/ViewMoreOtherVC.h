//
//  ViewMoreOtherVC.h
//  holidays
//
//  Created by Ios_Team on 04/09/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HolidayPackageDetail.h"
#import "PackageDetailModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ViewMoreOtherVC : UIViewController
@property (strong ,nonatomic) PackageDetailModel *packageModel;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Title;
@property (strong ,nonatomic) NSString *sectionTitLe;
@property (strong ,nonatomic) HolidayPackageDetail *holidayPkgModelInViewMore;
@property (weak, nonatomic) IBOutlet UITableView *tbl_OtherViews;
@property (weak, nonatomic) IBOutlet UIWebView *webViews;

@end

NS_ASSUME_NONNULL_END
