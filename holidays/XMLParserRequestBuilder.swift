//
//  XMLParserRequestBuilder.swift
//  sotc-consumer-application
//
//  Created by Saurav Kumar on 02/09/17.
//  Copyright © 2017 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit

class XMLParserRequestBuilder: NSObject,XMLParserDelegate {
    
    class func getBFNListSoapRequestString(mobileNumber:String) -> String
    {
        
        let soapRequest = AEXMLDocument()
        
        let envelope = soapRequest.addChild(name: APIConstants.Envelope, attributes: APIConstants.EnvelopeAttributes)
        
        _ = envelope.addChild(name: APIConstants.Header)
        
        
        let body = envelope.addChild(name: APIConstants.Body)
        
        
        let act = body.addChild(name: APIConstants.ActionGetBFNList, value: "", attributes: APIConstants.ActAttributes)
        
        
        
        let soapRequestBody = AEXMLDocument()
        
        
        let bookingDetailsRequest = soapRequestBody.addChild(name:APIConstants.BodyTitleGetBFNList)
        
        let authenticate = bookingDetailsRequest.addChild(name: APIConstants.Authentication)
        
        let criteria = bookingDetailsRequest.addChild(name: APIConstants.Criteria)
        
        
        authenticate.addChild(name: APIConstants.UserID, value: AuthenticationConstant.AuthenticationUserID)
        authenticate.addChild(name: APIConstants.Password, value: AuthenticationConstant.AuthenticationPassword)
        
        criteria.addChild(name: APIConstants.MobileNumber, value: mobileNumber/*"8510894234"*/)
        
        var bodyRequestData:String = "<![CDATA[" + soapRequestBody.xml + "]]>"
        
        
        
        // replace the other four special characters
        
        for (char, echar) in APIConstants.EscapeChars {
            
            bodyRequestData = bodyRequestData.replacingOccurrences(of: char, with: echar, options: .literal)
        }
        
        
        _ = act.addChild(name: APIConstants.RequestXML, value:bodyRequestData, attributes: APIConstants.RequestAttributes)
        
        
        printLog(soapRequest.xml.xmlUnEscaped)
        
        return soapRequest.xml.xmlUnEscaped
        
        
        
    }
    // VJ_Added_Start on 14/08/20
    class func getAdhocSoapRequestString(bfNumber:String,bookedOptionalDetails:Array<Dictionary<String,String>>) -> String
    {
        
        let soapRequest = AEXMLDocument()
        
        let envelope = soapRequest.addChild(name: APIConstants.Envelope, attributes: APIConstants.EnvelopeAttributes)
        
        _ = envelope.addChild(name: APIConstants.Header)
        
        
        let body = envelope.addChild(name: APIConstants.Body)
        
        
        let act = body.addChild(name: APIConstants.ActionBookAdhocDiscount, value: "", attributes: APIConstants.ActAttributes)
        
        
        
        let soapRequestBody = AEXMLDocument()
        
        
        let bookingDetailsRequest = soapRequestBody.addChild(name:APIConstants.BodyAdhocBookDiscount)
        
        let authenticate = bookingDetailsRequest.addChild(name: APIConstants.Authentication)
        
        //let criteria = bookingDetailsRequest.addChild(name: APIConstants.Criteria)
        
        
        authenticate.addChild(name: APIConstants.UserID, value: AuthenticationConstant.AuthenticationUserID)
        authenticate.addChild(name: APIConstants.Password, value: AuthenticationConstant.AuthenticationPassword)
        
        bookingDetailsRequest.addChild(name: APIConstants.BookingFileNumber, value: bfNumber)
        
        
        for bookedOptional in bookedOptionalDetails
        {
            
            let discountDetail = bookingDetailsRequest.addChild(name: AdhocBookingConstant.Discount_Details)
            
            discountDetail.addChild(name: AdhocBookingConstant.PAX_NUMBER ,value:bookedOptional[AdhocBookingConstant.PAX_NUMBER])
            
            discountDetail.addChild(name: AdhocBookingConstant.ADHOC_DISCOUNT,value:bookedOptional[AdhocBookingConstant.ADHOC_DISCOUNT])
            
            discountDetail.addChild(name: AdhocBookingConstant.REMARKS,value:bookedOptional[AdhocBookingConstant.REMARKS])
            
        }
        
        var bodyRequestData:String = "<![CDATA[" + soapRequestBody.xml + "]]>"
        
        
        
        // replace the other four special characters
        
        for (char, echar) in APIConstants.EscapeChars {
            
            bodyRequestData = bodyRequestData.replacingOccurrences(of: char, with: echar, options: .literal)
        }
        
        
        _ = act.addChild(name: APIConstants.RequestXML, value:bodyRequestData, attributes: APIConstants.RequestAttributes)
        
        
        printLog(soapRequest.xml.xmlUnEscaped)
        
        return soapRequest.xml.xmlUnEscaped
        
        
        
    }
    // VJ_Added_End
    
    class func getNotificationRequestString(bfNumber:String) -> String
    {
        
        
        let soapRequest = AEXMLDocument()
        
    
        
        let envelope = soapRequest.addChild(name: APIConstants.Envelope, attributes: APIConstants.EnvelopeAttributes)
        
        _ = envelope.addChild(name: APIConstants.Header)
        
        
        
        let body = envelope.addChild(name: APIConstants.Body)
        
        
        let act = body.addChild(name: APIConstants.ActionSetNotificationFlag, value: "", attributes: APIConstants.ActAttributes)
        
        
        
        let soapRequestBody = AEXMLDocument()
        
        
        let bookingDetailsRequest = soapRequestBody.addChild(name:APIConstants.BodyTitleSetNotificationFlag)
        
        let authenticate = bookingDetailsRequest.addChild(name: APIConstants.Authentication)
        
        let criteria = bookingDetailsRequest.addChild(name: APIConstants.Criteria)
        
        
        authenticate.addChild(name: APIConstants.UserID, value: AuthenticationConstant.AuthenticationUserID)
        authenticate.addChild(name: APIConstants.Password, value: AuthenticationConstant.AuthenticationPassword)
        
        criteria.addChild(name: APIConstants.BookingFileNumber, value: bfNumber)
        
        var bodyRequestData:String = "<![CDATA[" + soapRequestBody.xml + "]]>"
        
        
        
        // replace the other four special characters
        
        for (char, echar) in APIConstants.EscapeChars {
            
            bodyRequestData = bodyRequestData.replacingOccurrences(of: char, with: echar, options: .literal)
        }
        
        
        _ = act.addChild(name: APIConstants.RequestXML, value:bodyRequestData, attributes: APIConstants.RequestAttributes)
        
        
        printLog(soapRequest.xml.xmlUnEscaped)
        
        return soapRequest.xml.xmlUnEscaped
        
        
        
    }
    
    
    
    class func CheckMobileNumberSoapRequest(mobileNumber:String) -> String
    {
        
        
        let soapRequest = AEXMLDocument()
        
  
        
        let envelope = soapRequest.addChild(name: APIConstants.Envelope, attributes: APIConstants.EnvelopeAttributes)
        
        _ = envelope.addChild(name: APIConstants.Header)
        
        
        
        let body = envelope.addChild(name: APIConstants.Body)
        
        
        let act = body.addChild(name: APIConstants.ActionCheckMobileNo, value: "", attributes: APIConstants.ActAttributes)
        
        
        
        let soapRequestBody = AEXMLDocument()
        
        
        let bookingDetailsRequest = soapRequestBody.addChild(name:APIConstants.BodyTitleCheckMobileNo)
        
        let authenticate = bookingDetailsRequest.addChild(name: APIConstants.Authentication)
        
        let criteria = bookingDetailsRequest.addChild(name: APIConstants.Criteria)
        
        
        authenticate.addChild(name: APIConstants.UserID, value: AuthenticationConstant.AuthenticationUserID)
        authenticate.addChild(name: APIConstants.Password, value: AuthenticationConstant.AuthenticationPassword)
        
        criteria.addChild(name: APIConstants.MobileNumber, value: mobileNumber)
        
        var bodyRequestData:String = "<![CDATA[" + soapRequestBody.xml + "]]>"
        
        
        
        // replace the other four special characters
        
        for (char, echar) in APIConstants.EscapeChars {
            
            bodyRequestData = bodyRequestData.replacingOccurrences(of: char, with: echar, options: .literal)
        }
        
        
        _ = act.addChild(name: APIConstants.RequestXML, value:bodyRequestData, attributes: APIConstants.RequestAttributes)
        
        
        printLog(soapRequest.xml.xmlUnEscaped)
        
        return soapRequest.xml.xmlUnEscaped
        
        
        
    }
    
    class func getBFNDetailSoapRequestString(bfNumber:String) -> String
    {
        
        
        let soapRequest = AEXMLDocument()
        
    
        
        let envelope = soapRequest.addChild(name: APIConstants.Envelope, attributes: APIConstants.EnvelopeAttributes)
        
        _ = envelope.addChild(name: APIConstants.Header)
        
        
        
        let body = envelope.addChild(name: APIConstants.Body)
        
        
        let act = body.addChild(name: APIConstants.ActionGetBookingDetails, value: "", attributes: APIConstants.ActAttributes)
        
        
        
        let soapRequestBody = AEXMLDocument()
        
        
        let bookingDetailsRequest = soapRequestBody.addChild(name:APIConstants.BodyTitleGetBookingDetails)
        
        let authenticate = bookingDetailsRequest.addChild(name: APIConstants.Authentication)
        
        let criteria = bookingDetailsRequest.addChild(name: APIConstants.Criteria)
        
        
        authenticate.addChild(name: APIConstants.UserID, value: AuthenticationConstant.AuthenticationUserID)
        authenticate.addChild(name: APIConstants.Password, value: AuthenticationConstant.AuthenticationPassword)
        
        criteria.addChild(name: APIConstants.BookingFileNumber, value: bfNumber)
        
        var bodyRequestData:String = "<![CDATA[" + soapRequestBody.xml + "]]>"
        
        
        
        // replace the other four special characters
        
        for (char, echar) in APIConstants.EscapeChars {
            
            bodyRequestData = bodyRequestData.replacingOccurrences(of: char, with: echar, options: .literal)
        }
        
        
        _ = act.addChild(name: APIConstants.RequestXML, value:bodyRequestData, attributes: APIConstants.RequestAttributes)
        
        
        printLog(soapRequest.xml.xmlUnEscaped)
        
        return soapRequest.xml.xmlUnEscaped
        
        
        
    }
    
    
    class func getBFNOptionalPackageSoapRequestString(bfNumber:String) -> String
    {
        
        
        let soapRequest = AEXMLDocument()
        
  
        
        let envelope = soapRequest.addChild(name: APIConstants.Envelope, attributes: APIConstants.EnvelopeAttributes)
        
        _ = envelope.addChild(name: APIConstants.Header)
        
        
        
        let body = envelope.addChild(name: APIConstants.Body)
        
        
        let act = body.addChild(name: APIConstants.ActionGetBFNOptional, value: "", attributes: APIConstants.ActAttributes)
        
        
        
        let soapRequestBody = AEXMLDocument()
        
        
        let bookingDetailsRequest = soapRequestBody.addChild(name:APIConstants.BodyTitleGetBFNOptional)
        
        let authenticate = bookingDetailsRequest.addChild(name: APIConstants.Authentication)
        
        let criteria = bookingDetailsRequest.addChild(name: APIConstants.Criteria)
        
        
        authenticate.addChild(name: APIConstants.UserID, value: AuthenticationConstant.AuthenticationUserID)
        authenticate.addChild(name: APIConstants.Password, value: AuthenticationConstant.AuthenticationPassword)
        
        criteria.addChild(name: APIConstants.BookingFileNumber, value: bfNumber)
        
        var bodyRequestData:String = "<![CDATA[" + soapRequestBody.xml + "]]>"
        
        
        
        // replace the other four special characters
        
        for (char, echar) in APIConstants.EscapeChars {
            
            bodyRequestData = bodyRequestData.replacingOccurrences(of: char, with: echar, options: .literal)
        }
        
        
        _ = act.addChild(name: APIConstants.RequestXML, value:bodyRequestData, attributes: APIConstants.RequestAttributes)
        
        
        printLog(soapRequest.xml.xmlUnEscaped)
        
        return soapRequest.xml.xmlUnEscaped
        
        
        
    }
    
    /*
    class func getCurrentFxRatesSoapRequestString() -> String
    {
        
        
        let soapRequest = AEXMLDocument()
        
        let attributes = ["xmlns:soapenv" : "http://schemas.xmlsoap.org/soap/envelope/", "xmlns:ser" : "http://services.broadvision.com"]
        
        let envelope = soapRequest.addChild(name: "soapenv:Envelope", attributes: attributes)
        
        let header = envelope.addChild(name: "soapenv:Header")
        
        
        
        let body = envelope.addChild(name: "soapenv:Body")
        
        
        let act = body.addChild(name: "ser:getCurrentFxRates", value: "")
        
        let soapRequestBody = AEXMLDocument()
        
        
        let bookingDetailsRequest = soapRequestBody.addChild(name:"ForexRateRequest")
        
        let authenticate = bookingDetailsRequest.addChild(name: "ForexRate")
        
        // let criteria = bookingDetailsRequest.addChild(name: "CRITERIA")
        
        
        authenticate.addChild(name: "userId", value: "Ltapp")
        authenticate.addChild(name: "password", value: "aCac5qLx8HqryH1vn0oEOg==")
        
        // criteria.addChild(name: "MOBILE_NO", value: "8510894234")
        
        var bodyRequestData:String = "<![CDATA[" + soapRequestBody.xml + "]]>"
        
        
        
        // replace the other four special characters
        let escapeChars = ["<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"no\"?>" : "" ,"\n":"","\t":""]
        
        for (char, echar) in escapeChars {
            
            bodyRequestData = bodyRequestData.replacingOccurrences(of: char, with: echar, options: .literal)
        }
        
        
        _ = act.addChild(name: "requestXML", value:bodyRequestData)
        
        
        printLog(soapRequest.xml.xmlUnEscaped)
        
        return soapRequest.xml.xmlUnEscaped
        
        
        
        
    }
    
    */
    
    class func getPaymentReceiptGenerationSoapRequestString(bfNumber:String,paymentId:String,amount:String,bookingFileName:String,reciptGenerationFXDetails:(isFX:Bool,fxCurrencyType:String,roeRate:String,fxAmount:String,isA2Attached:Bool)) -> String
    {
        
        
        let soapRequest = AEXMLDocument()
        
      
        
        let envelope = soapRequest.addChild(name: APIConstants.Envelope, attributes: APIConstants.EnvelopeAttributes)
        
        _ = envelope.addChild(name: APIConstants.Header)
        
        
        
        let body = envelope.addChild(name: APIConstants.Body)
        
        
        let act = body.addChild(name: APIConstants.ActionGetLtPortalReciept, value: "", attributes: APIConstants.ActAttributes)
        
        
        
        let soapRequestBody = AEXMLDocument()
        
        
        let bookingDetailsRequest = soapRequestBody.addChild(name:APIConstants.BodyTitleGetLtPortalReciept)
        
        let authenticate = bookingDetailsRequest.addChild(name: APIConstants.Authentication)
        
        let receiptDetails = bookingDetailsRequest.addChild(name: APIConstants.ReceiptDetails)
        
        
        authenticate.addChild(name: APIConstants.UserID, value: AuthenticationConstant.AuthenticationUserID)
        authenticate.addChild(name: APIConstants.Password, value: AuthenticationConstant.AuthenticationPassword)
        
        receiptDetails.addChild(name:APIConstants.TransactionID , value: paymentId)
        receiptDetails.addChild(name:"BOOKING_FILE_NUMBER"  , value: bfNumber)
        receiptDetails.addChild(name:APIConstants.BookingFileName  , value: bookingFileName)
        receiptDetails.addChild(name: APIConstants.BookinAmount , value: amount)
        receiptDetails.addChild(name: APIConstants.ReceiptForFx , value: String(reciptGenerationFXDetails.isFX))
        receiptDetails.addChild(name: APIConstants.FxCurrency, value: reciptGenerationFXDetails.fxCurrencyType)
        receiptDetails.addChild(name: APIConstants.ROE, value: reciptGenerationFXDetails.roeRate)
        receiptDetails.addChild(name: APIConstants.FxAmount, value: reciptGenerationFXDetails.fxAmount)
        receiptDetails.addChild(name: APIConstants.A2Attached, value: String(reciptGenerationFXDetails.isA2Attached))
        
        var bodyRequestData:String = "<![CDATA[" + soapRequestBody.xml + "]]>"
        
        
        
        // replace the other four special characters
        
        for (char, echar) in APIConstants.EscapeChars {
            
            bodyRequestData = bodyRequestData.replacingOccurrences(of: char, with: echar, options: .literal)
        }
        
        
        _ = act.addChild(name: APIConstants.RequestXML, value:bodyRequestData, attributes: APIConstants.RequestAttributes)
        
        
        printLog(soapRequest.xml.xmlUnEscaped)
        
        return soapRequest.xml.xmlUnEscaped
        
        
        
    }
    
    class func getBookingOptionalRequestString(bfNumber:String,bookedOptionalDetails:Array<Dictionary<String,String>>) -> String
    {
        
        
        let soapRequest = AEXMLDocument()
        
      
        
        let envelope = soapRequest.addChild(name: APIConstants.Envelope, attributes: APIConstants.EnvelopeAttributes)
        
        _ = envelope.addChild(name: APIConstants.Header)
        
        
        let body = envelope.addChild(name: APIConstants.Body)
        
        
        let act = body.addChild(name: APIConstants.ActionBookOptional, value: "", attributes: APIConstants.ActAttributes)
        
        
        
        let soapRequestBody = AEXMLDocument()
        
        
        let bookingDetailsRequest = soapRequestBody.addChild(name:APIConstants.BodyTitleBookOptional)
        
        let authenticate = bookingDetailsRequest.addChild(name: APIConstants.Authentication)
        
        let criteria = bookingDetailsRequest.addChild(name: APIConstants.Criteria)
        
        
        authenticate.addChild(name: APIConstants.UserID, value: AuthenticationConstant.AuthenticationUserID)
        authenticate.addChild(name: APIConstants.Password, value: AuthenticationConstant.AuthenticationPassword)
        
        criteria.addChild(name: APIConstants.BookingFileNumber, value: bfNumber/*"8510894234"*/)
        
        
        for bookedOptional in bookedOptionalDetails
        {
            
            let optionalDetail = bookingDetailsRequest.addChild(name: APIConstants.OptionalDetails)
            
            optionalDetail.addChild(name: OptionalBookingConstant.PAX_NUMBER ,value:bookedOptional[OptionalBookingConstant.PAX_NUMBER])
            
            optionalDetail.addChild(name: OptionalBookingConstant.OPTIONAL_CODE ,value:bookedOptional[OptionalBookingConstant.OPTIONAL_CODE])
            
            optionalDetail.addChild(name: OptionalBookingConstant.AMOUNT,value:bookedOptional[OptionalBookingConstant.AMOUNT])
            
            optionalDetail.addChild(name: OptionalBookingConstant.REMARKS,value:bookedOptional[OptionalBookingConstant.REMARKS])
            
        }
        
        var bodyRequestData:String = "<![CDATA[" + soapRequestBody.xml + "]]>"
        
        
        // replace the other four special characters
        
        for (char, echar) in APIConstants.EscapeChars {
            
            bodyRequestData = bodyRequestData.replacingOccurrences(of: char, with: echar, options: .literal)
        }
        
        
        _ = act.addChild(name: APIConstants.RequestXML, value:bodyRequestData, attributes: APIConstants.RequestAttributes)
        
        
        printLog(soapRequest.xml.xmlUnEscaped)
        
        return soapRequest.xml.xmlUnEscaped
        
        
    }
}
