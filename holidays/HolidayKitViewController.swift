//
//  HolidayKitViewController.swift
//  sotc-consumer-application
//
//  Created by Saurav Kumar on 09/08/17.
//  Copyright © 2017 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit

class HolidayKitViewController: UIViewController,UIScrollViewDelegate {

    
 
    
    @IBOutlet weak var scrollViewImageParent: UIScrollView!
    
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet var holidayKitImageSlideShow: ImageSlideshow!
    
    @IBOutlet weak var holidayKitStatusLabel: UILabel!
    @IBOutlet var homeKitRequestStatus: UILabel!
   
        var tourDetail:Tour?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.setHolidayKitHeaderImages()
        
        // image zoom on double tap
        // if we do this on image view it will occupy more space and hence inorder to save that space scrollview is used
        scrollViewImageParent.maximumZoomScale=4
        scrollViewImageParent.minimumZoomScale=1.0
        scrollViewImageParent.bounces=true
        scrollViewImageParent.bouncesZoom=true
        scrollViewImageParent.contentSize = CGSize(width: imageView.frame.size.width,  height: imageView.frame.size.height)
        scrollViewImageParent.showsHorizontalScrollIndicator=true
        scrollViewImageParent.showsVerticalScrollIndicator=true
        scrollViewImageParent.delegate = self
        self.scrollViewImageParent.addSubview(imageView)
        self.view.addSubview(scrollViewImageParent)
        
        self.getHolidayKitDetails()
    }
    
    func getHolidayKitDetails()
    {
        // if let visaDocUrl:String =  tourDetails?.visaDocUrl
        // {
        //"http://uat2.thomascook.in/soto/lt/productitineraryDownload.do?itineraryCode=2017EUR228S1&filePath=Master Documentation checklist for Visa.pdf&docType=Visa Requirement"
        
        /* let documentFolderPath =  TourDocumentController.getTourFileDirectoryPath(bfnNumber: (tourDetails?.bfNumber)!)
         
         let visaDocFilePath = documentFolderPath.bfNumberPath + "/" + "visaDocUrl.pdf"
         
         self.downloadDocument(filePath: visaDocFilePath, url: visaDocUrl)
         
         }else{
         
         self.displayAlert(message: "No documnet details found")
         }*/
        var url = ""
        
         let documentFolderPath =  TourDocumentController.getTourFileDirectoryPath(bfnNumber: (tourDetail?.bfNumber)!)
        
        let visaDocFilePath = documentFolderPath.bfNumberPath + "/" + "holidayKit.jpg"
        
        self.downloadDocument(filePath: DocumentsURLConstant.HE_KIT_EUR, url: url)
        
      /*  if let tourCode = tourDetail?.tourCode
        {
            
            let countryCode = filterTourCode(tourCode: tourCode,uptoLength: 3)
            
            if countryCode != ""
            {
                if countryCode.uppercased().contains(Constant.USA_CONTRY_CODE) {
                    
                    url = DocumentsURLConstant.HE_KIT_USA
                    
                } else if countryCode.uppercased().contains(Constant.EXO_CONTRY_CODE) {
                    
                   url = DocumentsURLConstant.HE_KIT_EXO
                    
                } else if countryCode.uppercased().contains(Constant.FAR_CONTRY_CODE) {
                    
                     url = DocumentsURLConstant.HE_KIT_FAR
                    
                } else if countryCode.uppercased().contains(Constant.EUR_CONTRY_CODE) {
                    
                     url = DocumentsURLConstant.HE_KIT_EUR
                }
                else if countryCode.uppercased().contains(Constant.AUS_CONTRY_CODE) {
                    
                    url = DocumentsURLConstant.HE_KIT_AUS
                }else if countryCode.uppercased().contains(Constant.RSA_CONTRY_CODE) {
                    
                    url = DocumentsURLConstant.HE_KIT_RSA
                }
                
                
            }
            
        }
        
        // }
        if url != ""
        {
            let documentFolderPath =  TourDocumentController.getTourFileDirectoryPath(bfnNumber: (tourDetail?.bfNumber)!)
            
            let visaDocFilePath = documentFolderPath.bfNumberPath + "/" + "holidayKit.jpg"
            
            self.downloadDocument(filePath: visaDocFilePath, url: url)
            
            
        }else
        {
            self.displayAlert(message: AlertMessage.NO_DOCUMENT)
        }*/
        
        
    }
    
    func filterTourCode(tourCode:String,uptoLength:Int) -> String
    {
        
        if !((tourCode.isEmpty))
        {
            let first4 = tourCode.substring(to:tourCode.index(tourCode.startIndex, offsetBy: 3))
            
            return first4
            
        }
        
        return ""
        
        
    }
    
    func downloadDocument(filePath:String ,url:String) {
        
        //  let passengerDocumnet:TourPassengerDouments =  documentArray.first!
        
        LoadingIndicatorView.show("Loading")
        
        NetworkCommunication.downloadDocuments(urlString: url, destinationFilePath: filePath, completion: { (downlodedfilePath, status) in
            
             LoadingIndicatorView.hide()
            
            printLog(status,filePath);
            
            if(status)
            {
                 self.imageView.image = UIImage(contentsOfFile: filePath)
            }
            else{
                
                let isFilePresent = TourDocumentController.isFileExistInDocumentDirectory(fileNamePath: filePath)
                
                if(isFilePresent.chkFile)
                {
                    self.imageView.image = UIImage(contentsOfFile: filePath)
                    
                }else{
                        self.downloadFaildAlert(message: AlertMessage.DOCUMENT_DOWNLOAD_FAIL)
                    
                }
                
            }
            
            
        })
    }
    
    
    func displayAlert(message:String)
    {
        let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: AlertMessage.TITLE_OK, style: UIAlertAction.Style.default, handler:{
            alert -> Void in
            
            self.navigationController?.popViewController(animated: true)
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func downloadFaildAlert(message:String)
    {
        let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: AlertMessage.TITLE_CANCEL, style: UIAlertAction.Style.default, handler: {
            alert -> Void in
            
            self.navigationController?.popViewController(animated: true)
        }))
        
        alert.addAction(UIAlertAction(title: AlertMessage.TITLE_TRY_AGAIN, style: UIAlertAction.Style.default, handler: {
            alert -> Void in
            
            self.getHolidayKitDetails()
           
        }))
        
       
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView?
    {
        return self.imageView
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
 
        self.navigationController?.setRedNavigationBar()
        self.navigationController?.setnavigatiobBarTitle(title: Title.HOLIDAY_KIT, viewController: self)
        self.navigationController?.setEmergencyNavigationRightButton(showEmergencyButton: true, viewController: self)
        self.navigationController?.setNagationBackButton(showRightButton: true, viewController: self)
        
        if "Yes".lowercased() == tourDetail?.heKitFlag?.lowercased()
        {
           // self.holidayKitStatusLabel.text = AlertMessage.He_Kit.ALLREADY_PURCHASED// homeKitRequestStatus.text = "Requested"
            
              self.holidayKitStatusLabel.text = AlertMessage.He_Kit.PURCHASED
        }
        else
        {
             self.holidayKitStatusLabel.text = AlertMessage.He_Kit.PURCHASED
            //homeKitRequestStatus.text = "Not requested"
        }
    }
   
    func backButtonClick() -> Void
    {
        self.navigationController?.popViewController(animated: true)
        self.navigationController?.setNavigationBarTranslucent()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //MARK: -  Holiday Kit Header Image SlideShow Implementaion
    private func setHolidayKitHeaderImages()  {
        
        let imgArray = [ImageSource(image:#imageLiteral(resourceName: "holidayKit")),ImageSource(image:#imageLiteral(resourceName: "Cinqterrier")),ImageSource(image:#imageLiteral(resourceName: "holidayKit"))]
        
        holidayKitImageSlideShow.backgroundColor = UIColor.white
        holidayKitImageSlideShow.slideshowInterval = 5.0
        holidayKitImageSlideShow.pageControlPosition = PageControlPosition.insideScrollView
        holidayKitImageSlideShow.pageControl.currentPageIndicatorTintColor = UIColor.lightGray
        holidayKitImageSlideShow.pageControl.pageIndicatorTintColor = UIColor.black
        holidayKitImageSlideShow.contentScaleMode = UIView.ContentMode.scaleToFill
        
        // optional way to show activity indicator during image load (skipping the line will show no activity indicator)
        holidayKitImageSlideShow.activityIndicator = DefaultActivityIndicator()
        holidayKitImageSlideShow.currentPageChanged = { page in
            printLog("HlidayKit current page:", page)
        }
        
        // can be used with other sample sources as `afNetworkingSource`, `alamofireSource` or `sdWebImageSource` or `kingfisherSource`
        holidayKitImageSlideShow.setImageInputs(imgArray)
        
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(self.tourDetailsHeaderImageHeaderDidTap))
        holidayKitImageSlideShow.addGestureRecognizer(recognizer)
    }
    
    @objc func tourDetailsHeaderImageHeaderDidTap() {
        let fullScreenController = holidayKitImageSlideShow.presentFullScreenController(from: self)
        // set the activity indicator for full screen controller (skipping the line will show no activity indicator)
        fullScreenController.slideshow.activityIndicator = DefaultActivityIndicator(style: .white, color: nil)
    }

}
