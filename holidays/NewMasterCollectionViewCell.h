//
//  NewMasterCollectionViewCell.h
//  holidays
//
//  Created by Kush_Tech on 18/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NewMasterCollectionViewCell : UICollectionViewCell
- (NSAttributedString *)attributedStringForBulletTexts:(NSArray *)stringList
                                              withFont:(UIFont *)font
                                          bulletString:(NSString *)bullet
                                           indentation:(CGFloat)indentation
                                           lineSpacing:(CGFloat)lineSpacing
                                      paragraphSpacing:(CGFloat)paragraphSpacing
                                             textColor:(UIColor *)textColor
                                           bulletColor:(UIColor *)bulletColor;
-(NSString*)getNumberFormatterString:(NSInteger)number;
-(UIImage *)coloredImage:(UIImage *)firstImage withColor:(UIColor *)color;
-(NSString*)convertJsonDictToString:(NSDictionary*)jsonDict;
@end

NS_ASSUME_NONNULL_END
