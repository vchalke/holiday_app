//
//  FilterPackageTableViewCell.h
//  holidays
//
//  Created by Kush_Team on 24/06/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FilterPackageTableViewCell : UITableViewCell
{
    NSString *isStay;
    NSString *isAirInclusive;
    NSString *isGrouped;
    NSString *isCustomized;
}
@property (strong,nonatomic) NSMutableDictionary *filterPackageDict;
@property (weak, nonatomic) IBOutlet UISwitch *switch_OnlyStay;
@property (weak, nonatomic) IBOutlet UISwitch *switch_airInclusive;
@property (weak, nonatomic) IBOutlet UISwitch *switch_groupTour;
@property (weak, nonatomic) IBOutlet UISwitch *switch_customized;
-(void)setupTableViewCell;
@end

NS_ASSUME_NONNULL_END
