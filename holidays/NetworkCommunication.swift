    //
//  NetworkCommunication.swift
//  sotc-consumer-application
//
//  Created by Saurav Kumar on 20/07/17.
//  Copyright © 2017 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit

class NetworkCommunication: NSObject {

    class func downloadDocuments(urlString:String , destinationFilePath:String, completion: @escaping (_ responseDataString:String,_ status:Bool) -> Void)  {
        
        
        
        
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            
            let url =  URL(fileURLWithPath: destinationFilePath)// NSURL(string: destinationFilePath)
            
            return (url, [.removePreviousFile, .createIntermediateDirectories])
        }
        
        
        download(urlString.replacingOccurrences(of: " ", with: "%20"), to: destination).response { response in
            printLog(response)
           // manager.session.invalidateAndCancel()
            if response.error == nil, let downloadedFilePath = response.destinationURL?.path {
                
                completion(downloadedFilePath,true);
                
            }else
            {
                completion(String(describing: response.error), false);
            }
    
        }
    
    }
}
