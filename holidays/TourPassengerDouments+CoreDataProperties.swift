//
//  TourPassengerDouments+CoreDataProperties.swift
//  holidays
//
//  Created by Saurav on 28/11/17.
//  Copyright © 2017 Mobicule. All rights reserved.
//

import Foundation
import CoreData


extension TourPassengerDouments {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<TourPassengerDouments> {
        return NSFetchRequest<TourPassengerDouments>(entityName: "TourPassengerDouments")
    }

    @NSManaged public var bfNumber: String?
    @NSManaged public var bfNumber_documentType_passengerNumber: String?
    @NSManaged public var documentType: String?
    @NSManaged public var docummentURL: String?
    @NSManaged public var passengerNumber: String?
    @NSManaged public var tourRelation: NSSet?

}

// MARK: Generated accessors for tourRelation
extension TourPassengerDouments {

    @objc(addTourRelationObject:)
    @NSManaged public func addToTourRelation(_ value: Tour)

    @objc(removeTourRelationObject:)
    @NSManaged public func removeFromTourRelation(_ value: Tour)

    @objc(addTourRelation:)
    @NSManaged public func addToTourRelation(_ values: NSSet)

    @objc(removeTourRelation:)
    @NSManaged public func removeFromTourRelation(_ values: NSSet)

}
