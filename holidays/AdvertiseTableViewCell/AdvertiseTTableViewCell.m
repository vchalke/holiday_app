//
//  AdvertiseTTableViewCell.m
//  holidays
//
//  Created by Kush_Tech on 17/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "AdvertiseTTableViewCell.h"
#import "AdvertiseCollectionViewCell.h"

@implementation AdvertiseTTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setupCollectionViewTitle:(NSString*)title subTitle:(NSString*)subTitle
{
    NSLog(@"self.bannerOfferArray %ld",[self.dataArray count]);
    [self.collections_views registerNib:[UINib nibWithNibName:@"AdvertiseCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"AdvertiseCollectionViewCell"];
    self.collections_views.delegate = self;
    self.collections_views.dataSource = self;
    [self.collections_views reloadData];
    
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [self.dataArray count];
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    AdvertiseCollectionViewCell *cell= (AdvertiseCollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"AdvertiseCollectionViewCell" forIndexPath:indexPath];
    if ([self.dataArray count]>0){
        BankOffersObject *bannerObj = [[BankOffersObject alloc] initWithBankDataArray:[self.dataArray objectAtIndex:indexPath.row]];
        [cell setData:bannerObj];
    }
    
    return cell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
//    CGFloat width = collectionView.frame.size.width / 3 - 10;
//    return CGSizeMake(width, 90);
    return CGSizeMake(self.collections_views.frame.size.width*0.91, self.collections_views.frame.size.height*0.9);
}
@end
