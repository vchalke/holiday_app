//
//  AdvertiseTTableViewCell.h
//  holidays
//
//  Created by Kush_Tech on 17/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AdvertiseTTableViewCell : UITableViewCell<UICollectionViewDataSource,UICollectionViewDelegate>
@property (weak, nonatomic) IBOutlet UICollectionView *collections_views;
@property (weak, nonatomic)NSArray *dataArray;
-(void)setupCollectionViewTitle:(NSString*)title subTitle:(NSString*)subTitle;

@end

NS_ASSUME_NONNULL_END
