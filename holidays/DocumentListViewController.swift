//
//  DocumentListViewController.swift
//  sotc-consumer-application
//
//  Created by Parshwanath on 21/09/17.
//  Copyright © 2017 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit


struct DocumentDetails {
    
    var displayTitle:String?
    var subtitle:String?
    var documnetUrl:String?
    var downloadUrl:String?
    
    init() {
        
    }
    
    
    
   
}

protocol DocumentListViewDelegate {
    func selectedDocument(documnetUrl:String, downloadUrl:String)
}

class DocumentListViewController: UIViewController,UITableViewDelegate,UITableViewDataSource   {

    @IBOutlet weak var documentListTableView: UITableView!
    var presentingView:UIViewController? = nil
    
    var delegate:DocumentListViewDelegate?
    
    var documnetListArray:Array<DocumentDetails>?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.documentListTableView.register(UITableViewCell.self, forCellReuseIdentifier: "documentListViewControllerTableView")
        
         self.documentListTableView.tableFooterView = UIView(frame: .zero)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

 
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return documnetListArray!.count
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 40
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "documentListViewControllerTableView", for: indexPath) as? UITableViewCell
            else {
                
               // fatalError("The dequeued cell is not an instance of MealTableViewCell.")
        }
        
        cell.textLabel? .font = UIFont(name:"Roboto-Light", size:15)
        
        if let document = documnetListArray?[indexPath.row] {
            
            cell.textLabel?.text = document.displayTitle
            
        }
        
        // cell.textLabel?.text = "Vikas Patil"
        
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
         self.dismiss(animated: true, completion: {
        
        var document:DocumentDetails = self.documnetListArray![indexPath.row]
        
        self.delegate?.selectedDocument(documnetUrl:document.documnetUrl!,downloadUrl:document.downloadUrl! )
        
       /// self.downloadDocumnet(filePath: document.documnetUrl!,url: document.downloadUrl!)
        
        })
      
    }
    
    func downloadDocument(filePath:String ,url:String) {
        
        //  let passengerDocumnet:TourPassengerDouments =  documentArray.first!
        
        
        NetworkCommunication.downloadDocuments(urlString: url, destinationFilePath: filePath, completion: { (downlodedfilePath, status) in
            
            printLog(status,filePath);
            
            if(status)
            {
                
               
            }
            else{
                
                
                
            }
            
            
        })
    }

    
    
}
