//
//  BookNowViewController.m
//  holidays
//
//  Created by ketan on 08/10/15.
//  Copyright © 2015 Mobicule. All rights reserved.
//

#import "BookNowViewController.h"
#import "RoomsDataModel.h"
#import "CalendarViewController.h"
#import "PackageInternationalGIT.h"
#import "PackageInternationFIT.h"
#import "PackageDomesticFit.h"
#import "TravellerInformationModel.h"
#import "PassangerDetailViewController.h"
#import "TermsAndCondViewController.h"
#import "RequestForCallPopUp.h"
#import "UIViewController+MJPopupViewController.h"
//#import "LoginViewPopUp.h"
#import "NewLoginPopUpViewController.h"
#import "UITextField+categoryTextField.h"
#import "NetCoreAnalyticsVC.h"
//#import "Thomas_Cook_Holidays-Swift.h"

#define taxPercentage 9.0f
@interface BookNowViewController ()
{
    UITapGestureRecognizer *tmpTap;
    UIView *tapview;
    UILabel *preLabel;
    NSMutableArray *numberArray;
    NSMutableArray *arrayChildCount;
    NSMutableArray *tapGestureArray;
    NSArray *packageTypeArray;
    NSMutableArray *roomRecordArray;
    int accomType;
    NSString *departFrom;
    NSArray *travellerArrayForCalculation;
    NSDictionary *calculationDict;
    int maxAdultCount;
    int maxChildCount;
    int maxInfantCount;
    NSString *packageType;
    NSString *bookingAmountString;
    NSString *totalPackageAmountString;
    NSMutableArray *nonINRArray;
    NSArray *arrayForALLCurrency;
    NSDictionary *dictForDate;
    NSString *bookingAmountForBookingSubmit;
    NSArray *stateDataArray;
    NSString *selectedStateCode;
    NSDictionary *selectedStateDict;
    NSDictionary *gstTaxDetails;
    NSDictionary *selectedHubDict;
    NSString *selectedLtItineraryCode;
    NSMutableArray *ItineraryCodeArray;
    NSString *amountType;
}

@end

@implementation BookNowViewController
@synthesize packageDetail;
@synthesize arrayCities;


- (void)viewDidLoad
{
    [super viewDidLoad];
    
   
    //[AppUtility checkIsVersionUpgrade];

    maxAdultCount = 4;
    maxChildCount = 2;
    maxInfantCount = 2;
    arrayChildCount = [[NSMutableArray alloc]init];
    tapGestureArray = [[NSMutableArray alloc]init];
    roomRecordArray = [[NSMutableArray alloc]init];
    packageTypeArray = packageDetail.arrayAccomTypeList;
    amountType = @"A";
    //packageTypeArray = [[NSArray alloc] initWithObjects:@"Standard", @"Delux", @"Premium", nil];
    
    [[NSBundle mainBundle]loadNibNamed:@"BookNowViewController" owner:self options :nil];
    [super addViewInBaseView:self.bookNowView];
    
    [self.scrollViewForContainer addSubview:self.containerView];
    CGRect containerRect = self.containerView.frame;
    containerRect.size.width = self.scrollView.frame.size.width;
    self.containerView.frame = containerRect;
    NSLog(@"containerView modified %@",NSStringFromCGRect(self.containerView.frame));
    numberArray = [[NSMutableArray alloc]initWithObjects:@"1",nil];
    
    UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapNumber:)];
    tap.numberOfTapsRequired=1;
    RoomsDataModel *roomModel = [[RoomsDataModel alloc]init];
    [roomRecordArray addObject:roomModel];
    [tapGestureArray addObject:tap];
    tapview=[[UIView alloc]init];
    tapview.layer.borderColor=[UIColor blackColor].CGColor;
    tapview.layer.borderWidth=1.0;
    [self plotMenuButtons];
    [self tapNumber:tap];
    
    // [super applyLeftPaddingToTextfield:self.txtPackageType];
    // [super applyLeftPaddingToTextfield:self.txtSelectCity];
    // [super applyLeftPaddingToTextfield:self.txtSelectDate];
      [self SetDataWithRespectToPackageType];
    [self getStateList];
}

-(void)viewWillAppear:(BOOL)animated
{
    [CoreUtility reloadRequestID];
    //self.txtSelectDate.contentMode = UIEdgeInsetsMake(-4,-8,0,0);
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    self.txtSelectDate.leftView = paddingView;
    self.txtSelectDate.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *paddingView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    self.txtSelectCity.leftView = paddingView1;
    self.txtSelectCity.leftViewMode = UITextFieldViewModeAlways;
    
    [super setHeaderTitle:self.packageDetail.strPackageName];
    if([[self.packageDetail.strPackageSubType lowercaseString] isEqualToString:@"fit fixed"]|| [[self.packageDetail.strPackageSubType lowercaseString] isEqualToString:@"git"])
    {
        if([[self.packageDetail.strPackageType lowercaseString] isEqualToString:@"international"])
        {
            packageType = kpackageTypeGITInternational;
            
        }
        else
        {
            packageType = kpackageTypeGITDomestic;
        }
    }else
    {
        if([[self.packageDetail.strPackageType lowercaseString] isEqualToString:@"international"])
        {
            packageType = kpackageTypeFITInternational;
        }
        else
        {
            packageType = kpackageTypeFITDomestic;
        }
    }
    if(self.packageDetail.stringSelectedAccomType == nil || [self.packageDetail.stringSelectedAccomType isEqualToString:@""])
    {
        if (packageTypeArray.count != 0)
        {
            NSString *packageTypeLocal = (NSString *)packageTypeArray[0];
            self.txtPackageType.text = packageTypeLocal;
            [self setAccomType:self.txtPackageType.text];
        }
        
    }else
    {
        if ([self.packageDetail.stringSelectedAccomType isEqualToString:@"0"])
        {
            self.txtPackageType.text = @"Standard";
            accomType = 0;
        }
        else if ([self.packageDetail.stringSelectedAccomType isEqualToString:@"1"])
        {
            self.txtPackageType.text = @"Deluxe";
            accomType = 1;
        }
        else
        {
            self.txtPackageType.text = @"Premium";
            accomType = 2;
        }
    }
  
    
    
    
}

-(void)getStateList
{
    /*
     {
     "type": "sync",
     "entity": "stateCodeDetails",
     "action": "get",
     "platform": "android",
     "version":  "1.0",
     "data": {
     }
     }
     */
    
//    activityLoadingView = [LoadingView loadingViewInView:self.view.superview.superview
//                                              withString:@""
//                                       andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//        
//        NetworkHelper *helper = [NetworkHelper sharedHelper];
//        NSString *strResponse = [helper getDataFromServerForType:@"sync" entity:@"stateCodeDetails" action:@"get" andUserJson:[[NSDictionary alloc] init]];
//        NSDictionary *dataDict = [JsonSerealizer dictonaryOfJsonFromJsonData:strResponse];
//        NSString *strStatus = [dataDict valueForKey:@"status"];
//        dispatch_async(dispatch_get_main_queue(), ^(void) {
//            
//            [activityLoadingView removeFromSuperview];
//            if ([strStatus isEqualToString:@"success"])
//            {
//                stateDataArray  = [dataDict valueForKey:@"data"];
//            }
//            
//            
//        });
//    });
    
  
    
    
    @try
    {
    activityLoadingView = [LoadingView loadingViewInView:self.view.superview.superview
                                              withString:@""
                                       andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];

    NSDictionary *headerDict = [CoreUtility getHeaderDict];
    
    NetworkHelper *helper = [NetworkHelper sharedHelper];
    
    NSString *pathParam = @"4";
    
    [helper getResponseWithRequestType:@"GET" withQueryParam:@"" withPathParam:pathParam withJsonParam:nil withHeaders:headerDict withUrl:kAstraUrlGstStates success:^(NSDictionary *responseDict)
     {
         NSLog(@"Response Dict : %@",responseDict);
         
         dispatch_async(dispatch_get_main_queue(), ^(void)
                        {
                            [activityLoadingView removeFromSuperview];
                            if (responseDict)
                            {
                                if (responseDict.count>0)
                                {
                                    stateDataArray = (NSArray *)responseDict;
                                }
                                else
                                {
                                    [super showAlertViewWithTitle:@"Alert" withMessage:@"some error occurred"];
                                    
                                }
                            }
                            else
                            {
                                [super showAlertViewWithTitle:@"Alert" withMessage:@"some error occurred"];
                                
                            }
                        });
         
     }
                               failure:^(NSError *error)
     {dispatch_async(dispatch_get_main_queue(), ^(void)
                     {
                         [activityLoadingView removeFromSuperview];
                         [super showAlertViewWithTitle:@"Alert" withMessage:@"some error occurred"];
                         NSLog(@"Response Dict : %@",error);
                     });
         
     }];
    }
    @catch (NSException *exception)
    {
        NSLog(@"%@", exception.reason);
        [activityLoadingView removeFromSuperview];
    }
    @finally
    {
        NSLog(@"exception finally called");
    }
    


}

-(void)getDetailsWithPackageType:(NSString *)packageTypeFIT WithPrice:(NSString *)price
{
    NSString *modelID;
    if ([packageTypeFIT isEqualToString:@"yes"])
    {
        modelID = @"7";
    }
    else
    {
        modelID = @"9";
    }

    
    NSMutableDictionary *dictForRequest = [[NSMutableDictionary alloc] init];
    [dictForRequest setObject:modelID forKey:@"modelId"];
    [dictForRequest setObject:price forKey:@"amount"];
    [dictForRequest setObject:selectedStateCode forKey:@"custStateCode"];

    activityLoadingView = [LoadingView loadingViewInView:self.view.superview.superview
                                              withString:@""
                                       andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NetworkHelper *helper = [NetworkHelper sharedHelper];
        NSString *strResponse = [helper getDataFromServerForType:@"webservice" entity:@"gstTaxDetails" action:@"get" andUserJson:dictForRequest];
        NSDictionary *dataDict = [JsonSerealizer dictonaryOfJsonFromJsonData:strResponse];
        NSString *strStatus = [dataDict valueForKey:@"status"];
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            [activityLoadingView removeFromSuperview];
            if ([strStatus isEqualToString:@"success"])
            {
                NSDictionary *dictForGstDetails  = [dataDict valueForKey:@"data"];
                
                gstTaxDetails = [dictForGstDetails copy];
                
                NSString *sgstTaxAmount = [dictForGstDetails valueForKey:@"sgstTaxAmount"];
                NSString *cgstTaxAmount = [dictForGstDetails valueForKey:@"cgstTaxAmount"];
                NSString *igstTaxAmount = [dictForGstDetails valueForKey:@"igstTaxAmount"];

                double cgstValue = [cgstTaxAmount doubleValue];
                double sgstValue = [sgstTaxAmount doubleValue];
                double igstValue = [igstTaxAmount doubleValue];
                
                double totalCost = [price doubleValue];
                self.lblTotalCost.text = [NSString stringWithFormat:@"₹ %.1f",round(totalCost)];
                
                if ([packageTypeFIT isEqualToString:@"yes"])
                {
                    
                    if ([[_textFieldState.text lowercaseString] isEqualToString:@"maharashtra"])
                    {
                        self.lblServiceTax.hidden = NO;
                        self.labelTitleCgst.hidden = NO;
                        self.labelTitleSgst.text = @"SGST Tax Amount";
                        
                       self.lblServiceTax.text = [NSString stringWithFormat:@"₹ %.1f",round(cgstValue)];
                       self.labelValueSgst.text = [NSString stringWithFormat:@"₹ %.1f",round(sgstValue)];
                        
                     self.lblTotalPriceINCGST.text = [NSString stringWithFormat:@"₹ %.1f",round(totalCost+cgstValue+sgstValue)];
                        
                        UILabel *labelForEasyPaymentCost2 = (UILabel *)[self.viewForButtonContainers viewWithTag:10];
                        labelForEasyPaymentCost2.text = [NSString stringWithFormat:@"₹ %.1f",round(totalCost+cgstValue+sgstValue)];
                        self.packageDetail.stringAmountINRFIT = [NSString stringWithFormat:@"%f",totalCost];
                        totalPackageAmountString = [NSString stringWithFormat:@"%f",round(totalCost+cgstValue+sgstValue)];
                    }
                    else
                    {
                        self.lblServiceTax.hidden = YES;
                        self.labelTitleCgst.hidden = YES;
                        self.labelTitleSgst.text = @"IGST Tax Amount";
                        self.labelValueSgst.text = [NSString stringWithFormat:@"₹ %.1f",round(igstValue)];

                        self.lblTotalPriceINCGST.text = [NSString stringWithFormat:@"₹ %.1f",round(totalCost+igstValue)];
                        
                        UILabel *labelForEasyPaymentCost2 = (UILabel *)[self.viewForButtonContainers viewWithTag:10];
                        labelForEasyPaymentCost2.text = [NSString stringWithFormat:@"₹ %.1f",round(totalCost+igstValue)];
                        self.packageDetail.stringAmountINRFIT = [NSString stringWithFormat:@"%f",totalCost];
                        totalPackageAmountString = [NSString stringWithFormat:@"%f",round(totalCost+igstValue)];

                    }
                    
                    /*
                     if maha
                     show cgst and sgst
                     else
                     show igst
                     */
                }
                else
                {
                    
                    self.lblTotalCost.text = [NSString stringWithFormat:@"₹ %.1f",round(totalCost)];
                    
                    if ([[_textFieldState.text lowercaseString] isEqualToString:@"maharashtra"])
                    {
                        self.lblServiceTax.hidden = NO;
                        self.labelTitleCgst.hidden = NO;
                        self.labelTitleSgst.text = @"SGST Tax Amount";
                        
                        self.lblServiceTax.text = [NSString stringWithFormat:@"₹ %.1f",round(cgstValue)];
                        self.labelValueSgst.text = [NSString stringWithFormat:@"₹ %.1f",round(sgstValue)];
                        
                        self.lblTotalPriceINCGST.text = [NSString stringWithFormat:@"₹ %.1f",round(totalCost+cgstValue+sgstValue)];
                        UILabel *labelForEasyPaymentCost2 = (UILabel *)[self.viewForButtonContainers viewWithTag:10];
                        labelForEasyPaymentCost2.text = [NSString stringWithFormat:@"₹ %.1f",round(totalCost+cgstValue+sgstValue)];
                        totalPackageAmountString = [NSString stringWithFormat:@"%f",round(totalCost+cgstValue+sgstValue)];

                    }
                    else
                    {
                        self.lblServiceTax.hidden = YES;
                        self.labelTitleCgst.hidden = YES;
                        self.labelTitleSgst.text = @"IGST Tax Amount";
                        self.labelValueSgst.text = [NSString stringWithFormat:@"₹ %.1f",round(igstValue)];
                        
                        self.lblTotalPriceINCGST.text = [NSString stringWithFormat:@"₹ %.1f",round(totalCost+igstValue)];
                        
                        UILabel *labelForEasyPaymentCost2 = (UILabel *)[self.viewForButtonContainers viewWithTag:10];
                        labelForEasyPaymentCost2.text = [NSString stringWithFormat:@"₹ %.1f",round(totalCost+igstValue)];
                        totalPackageAmountString = [NSString stringWithFormat:@"%f",round(totalCost+igstValue)];

                        
                    }
                    
                  
                }
            }
        });
    });

}


#pragma mark - SetData With respect to package -

-(void)SetDataWithRespectToPackageType
{
    NSLog(@"%@",roomRecordArray)  ;
    
    
    if ([packageType isEqualToString:kpackageTypeFITDomestic] || [packageType isEqualToString:kpackageTypeGITDomestic] )
    {
        maxAdultCount = 3;
        // maxInfantCount = 2;
        
        if ([self.lblAdultCount.text intValue] == 3)
        {
            maxChildCount = 1;
            
            if (arrayChildCount.count >= 2)
            {
                [arrayChildCount removeLastObject];
            }
            
            if (arrayChildCount.count!=0)
            {
                NSMutableDictionary *childDict = [arrayChildCount objectAtIndex:0];
                if(childDict.count != 0)
                {
                    [childDict setObject:@"n" forKey:@"isEdit"];
                }
                
                if (arrayChildCount.count > 1)
                {
                    [arrayChildCount exchangeObjectAtIndex:0 withObjectAtIndex:1];
                }
                
            }
        }
        else if([self.lblAdultCount.text intValue] == 2)
        {
            if (arrayChildCount.count == 2)
            {
                NSMutableDictionary *childDict = [arrayChildCount objectAtIndex:0];
                if(childDict.count != 0)
                {
                    [childDict setObject:@"n" forKey:@"isEdit"];
                }
                
                if (arrayChildCount.count > 1)
                {
                    [arrayChildCount exchangeObjectAtIndex:0 withObjectAtIndex:1];
                }
            }
        }
        else
        {
            if (arrayChildCount.count!=0)
            {
                NSMutableDictionary *childDict = [arrayChildCount objectAtIndex:0];
                if(childDict.count != 0)
                {
                    [childDict setObject:@"y" forKey:@"isEdit"];
                }
            }
            maxChildCount = 2;
        }
        
    }
    else if ([packageType isEqualToString:kpackageTypeGITInternational])
    {
        // Aus/NZ/Europe/Asia
        
        if ([[packageDetail.stringRegion lowercaseString] containsString:@"australia"]||[[packageDetail.stringRegion lowercaseString] containsString:@"new zealand"]||[[packageDetail.stringRegion lowercaseString] isEqualToString:@"anz"]||[[packageDetail.stringRegion lowercaseString] isEqualToString:@"europe"]||[[packageDetail.stringRegion lowercaseString] isEqualToString:@"asia"])
        {
            
            maxAdultCount = 3;
            //  maxInfantCount = 2;
            
            if ([self.lblAdultCount.text intValue] == 4) // no option for child
            {
                [arrayChildCount removeAllObjects];
                maxChildCount = 0;
                maxInfantCount = 0;
                self.lblInfantsCount.text = @"0";
            }
            else if ([self.lblAdultCount.text intValue] == 3) // only one child with bed and wo
            {
                if (arrayChildCount.count == 2)
                {
                    [arrayChildCount removeLastObject];
                }
                
                if (arrayChildCount.count!=0)
                {
                    NSMutableDictionary *childDict = [arrayChildCount firstObject];
                    if(childDict.count  != 0)
                    {
                        [childDict setObject:@"y" forKey:@"isEdit"];
                    }
                    
                    if (arrayChildCount.count > 1)
                    {
                        [arrayChildCount exchangeObjectAtIndex:0 withObjectAtIndex:1];
                    }
                }
                
                maxChildCount = 1;
                //  maxInfantCount = 0;
                
            }
            else
            {
                if (arrayChildCount.count!=0)
                {
                    for (int i =0; i < arrayChildCount.count ; i++)
                    {
                        NSMutableDictionary *childDict = [arrayChildCount objectAtIndex:i];
                        if(childDict.count  != 0)
                        {
                            [childDict setObject:@"y" forKey:@"isEdit"];
                        }
                    }
                }
                maxChildCount = 2;
            }
            
            
        }//US/Africa
        else if ([[packageDetail.stringRegion lowercaseString] isEqualToString:@"us"]||[[packageDetail.stringRegion lowercaseString] isEqualToString:@"usa"] || [[packageDetail.stringRegion lowercaseString] isEqualToString:@"africa"] ||
                 [[packageDetail.stringRegion lowercaseString] containsString:@"america"])
        {
            maxAdultCount = 3;
            // maxInfantCount = 2;
            
            if ([self.lblAdultCount.text intValue] == 4) // no option for child
            {
                [arrayChildCount removeAllObjects];
                maxChildCount = 0;
                maxInfantCount = 0;
                self.lblInfantsCount.text = @"0";
            }
            else if ([self.lblAdultCount.text intValue] == 3) // no option for child
            {
               // [arrayChildCount removeAllObjects];
               // maxChildCount = 0;
               // maxInfantCount = 0;
                maxChildCount = 1;
              //  maxChildCount = 0;
            }
            else if ([self.lblAdultCount.text intValue] == 2) // no option for child
            {
                maxChildCount = 2;
            }
            else
            {
                
            }
                
                
            
        }
        
        if([[self.packageDetail.strPackageSubType lowercaseString] isEqualToString:@"fit fixed"]) //block child when fit fixed and Fit package
        {
            if ([self.lblAdultCount.text intValue] >= 3)
            {
                [arrayChildCount removeAllObjects];
                maxChildCount = 0;
            }
            else
            {
                maxChildCount = 2;
            }
        }
        
    }
    else if ([packageType isEqualToString:kpackageTypeFITInternational])
    {
        maxAdultCount = 3;
        
        maxChildCount = 2;
        
        maxInfantCount = 2;
        if ([self.lblAdultCount.text intValue] == 4)
        {
            [arrayChildCount removeAllObjects];
            maxChildCount = 0;
            maxInfantCount = 0;
        }
        else if ([self.lblAdultCount.text intValue] >= 3)
        {
            //            if (arrayChildCount.count >= 2)
            //            {
            //                [arrayChildCount removeLastObject];
            //            }
            //            maxChildCount = 1;
            //            if (arrayChildCount.count!=0)
            //            {
            //                NSMutableDictionary *childDict = [arrayChildCount objectAtIndex:0];
            //                if(childDict.count != 0)
            //                {
            //                    [childDict setObject:@"n" forKey:@"isEdit"];
            //                }
            //            }
            [arrayChildCount removeAllObjects];
            maxChildCount = 0;
            
        }
        else if([self.lblAdultCount.text intValue] == 2)
        {
            if (arrayChildCount.count == 2)
            {
                NSMutableDictionary *childDict = [arrayChildCount objectAtIndex:0];
                if(childDict.count != 0)
                {
                    [childDict setObject:@"n" forKey:@"isEdit"];
                }
                
                if (arrayChildCount.count > 1)
                {
                    [arrayChildCount exchangeObjectAtIndex:0 withObjectAtIndex:1];
                }
            }
        }
        else
        {
            //            if (arrayChildCount.count == 1)
            //            {
            //                NSMutableDictionary *childDict = [arrayChildCount objectAtIndex:0];
            //                if(childDict.count != 0)
            //                {
            //                    [childDict setObject:@"y" forKey:@"isEdit"];
            //                }
            //            }
            //            else if (arrayChildCount.count == 2)
            //            {
            //                NSMutableDictionary *childDict = [arrayChildCount objectAtIndex:0];
            //                if(childDict.count != 0)
            //                {
            //                    [childDict setObject:@"n" forKey:@"isEdit"];
            //                }
            //            }
            // maxChildCount = 2;
            //  maxInfantCount = 2;
        }
        
    }
    
    
    
    [self viewWillLayoutSubviews];
    
    // [UIView animateWithDuration:0.4 animations:^{
    
    [self.view layoutIfNeeded];
    [self.tblViewForChildCount reloadData];
    // }];
    
}

-(void)resetAllChildrenData
{
    for (int i = 0; i<arrayChildCount.count; i++)
    {
        NSMutableDictionary *childDict = [arrayChildCount objectAtIndex:i];
        if(childDict.count  != 0)
        {
            [childDict setObject:@"y" forKey:@"isEdit"];
        }
    }
}

-(void)setAddDeleteButtonAlpha
{
    int adultCount =  [self.lblAdultCount.text intValue];
    int infantCount =  [self.lblInfantsCount.text intValue];
    self.lblChildrenCount.text = [NSString stringWithFormat:@"%lu",(unsigned long)arrayChildCount.count];
    
    
    
    if (adultCount == maxAdultCount)
    {
        _btnAdultAdd.alpha = 0;
    }
    else
    {
        _btnAdultAdd.alpha = 1;
    }
    
    if (adultCount == 1)
    {
        _btnAdultDelete.alpha = 0;
    }
    else
    {
        _btnAdultDelete.alpha = 1;
    }
    
    if (infantCount == maxInfantCount)
    {
        _btnInfantAdd.alpha = 0;
    }
    else
    {
        _btnInfantAdd.alpha = 1;
    }
    
    if (infantCount == 0)
    {
        _btnInfantDelete.alpha = 0;
    }
    else
    {
        _btnInfantDelete.alpha = 1;
    }
    
    if (arrayChildCount.count == maxChildCount)
    {
        _btnChildAdd.alpha = 0;
    }
    else
    {
        _btnChildAdd.alpha = 1;
    }
    
    if(arrayChildCount.count == 0)
    {
        _btnChildDelete.alpha = 0;
    }
    else
    {
        _btnChildDelete.alpha = 1;
    }
    
    [self viewWillLayoutSubviews];
    
    [UIView animateWithDuration:0.4 animations:^{
        
        [self.view layoutIfNeeded];
    }];
    [self.tblViewForChildCount reloadData];
}

-(void)viewWillLayoutSubviews
{
    self.constraintBookingAddPanel.constant =  116+62 + ((arrayChildCount.count-1)*54);
    NSLog(@"constraintBookingAddPanel %f",self.constraintBookingAddPanel.constant);
    [self.bookNowView setNeedsUpdateConstraints];
    [self.bookNowView layoutSubviews];
    
    CGRect contentRect = CGRectZero;
    for (UIView *view in self.containerView.subviews) {
        contentRect = CGRectUnion(contentRect, view.frame);
    }
    self.scrollViewForContainer.contentSize = CGSizeMake(contentRect.size.width, contentRect.size.height+80);
    CGRect containerRect = self.containerView.frame;
    containerRect.size.height = contentRect.size.height + ((arrayChildCount.count)*54) + 50;
    self.containerView.frame = containerRect;
    NSLog(@"containerView frame %@",NSStringFromCGRect(self.containerView.frame));
    NSLog(@"scrollView frame %@",NSStringFromCGRect(self.scrollViewForContainer.frame));
}

-(void)plotMenuButtons
{
    [[_scrollViewForNumbaer subviews]
     makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    for (int i=0; i< numberArray.count; i++)
    {
        NSString *titleString = [NSString stringWithFormat:@"%@",numberArray[i]];
        if ([titleString isEqualToString:@"addButton"])
        {
            UIImageView *imageWithDeleteButton  = [[UIImageView alloc] initWithFrame:CGRectMake(i*40, 4, 22, 22)];
            imageWithDeleteButton.image = [UIImage imageNamed:@"deleteIcon"];
            imageWithDeleteButton.userInteractionEnabled = YES;
            [_scrollViewForNumbaer addSubview:imageWithDeleteButton];
            UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onAddDeleteButtonClicked:)];
            tap.view.tag = 1;
            imageWithDeleteButton.tag = 2;
            tap.numberOfTapsRequired=1;
            [imageWithDeleteButton addGestureRecognizer:tap];
            
            
            //            UILabel *addLabel = [[UILabel alloc]initWithFrame:CGRectMake(i*40, 0, 30, 30)];
            //            addLabel.text = @"+";
            //            addLabel.userInteractionEnabled = YES;
            //            [_scrollViewForNumbaer addSubview:addLabel];
            //            UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onAddDeleteButtonClicked:)];
            //            tap.view.tag = 1;
            //            addLabel.tag = 1;
            //            tap.numberOfTapsRequired=1;
            //            [addLabel addGestureRecognizer:tap];
        }
        else
        {
            UILabel *lblNumber=[[UILabel alloc]initWithFrame:CGRectMake(i*40, 0, 30, 30)];
            lblNumber.text= [NSString stringWithFormat:@"%d",i+1];
            lblNumber.tag = 1;
            lblNumber.textAlignment=NSTextAlignmentCenter;
            lblNumber.textColor=[UIColor lightGrayColor];
            [lblNumber setUserInteractionEnabled:YES];
            UITapGestureRecognizer *tap= [tapGestureArray objectAtIndex:i];
            tap.numberOfTapsRequired=1;
            id lab= [tmpTap view];
            UILabel *label=(UILabel *)lab;
            if([label.text isEqualToString:lblNumber.text])
            {
                lblNumber = label;
                lblNumber.textColor=[UIColor whiteColor];
            }
            [lblNumber addGestureRecognizer:tap];
            [_scrollViewForNumbaer addSubview:lblNumber];
        }
        
    }
    if (tmpTap)
    {
        [self tapNumber:tmpTap];
    }
}




-(void)tapNumber:(UITapGestureRecognizer*)tap{
    NSLog(@"Tap label");
    if ([[tap view] tag] == 3)
    {
        //  tmpTap = tap;
        id lab= [tap view];
        UILabel *label=(UILabel *)lab;
        NSString *indexString = label.text;
        int indexCount = [indexString intValue];
        [self saveDataWithIndex:indexCount];
        [[tap view] setTag:1];
    }
    else
    {
        tmpTap = tap;
        id lab= [tap view];
        UILabel *label=(UILabel *)lab;
        label.textColor=[UIColor whiteColor];
        
        if (preLabel == label)
        {
            preLabel.textColor=[UIColor whiteColor];
        }
        else
        {
            preLabel.textColor=[UIColor grayColor];
        }
        NSString *indexString = label.text;
        int indexCount = [indexString intValue];
        [self saveDataWithIndex:indexCount];
        preLabel=label;
        tapview.frame=label.frame;
        tapview.layer.cornerRadius=tapview.bounds.size.height/2;
        tapview.backgroundColor = [UIColor colorFromHexString:@"#0197D4"];
        tapview.clipsToBounds=YES;
        [_scrollViewForNumbaer addSubview:tapview];
        [_scrollViewForNumbaer bringSubviewToFront:label];
    }
    //    NSLog(@"Tap label");
    //
    //    id lab= [tap view];
    //    tmpTap = tap;
    //    UILabel *label=(UILabel *)lab;
    //    label.textColor=[UIColor whiteColor];
    //    label.layer.cornerRadius=label.bounds.size.height/2;
    //    label.clipsToBounds=YES;
    //    label.backgroundColor = [UIColor blackColor];
    //
    //    if (preLabel)
    //    {
    //        preLabel.backgroundColor = [UIColor whiteColor];
    //        preLabel.textColor = [UIColor lightGrayColor];
    //    }
    //
    //    preLabel=label;
    //    [self.scrollViewForNumbaer setNeedsDisplay];
    
}

-(void)saveDataWithIndex:(int)index
{
    if (preLabel)
    {
        int previousIndex = [preLabel.text intValue];
        
        if (roomRecordArray.count >= previousIndex )
        {
            RoomsDataModel *roomModel = roomRecordArray[previousIndex-1];
            roomModel.adultCount = [self.lblAdultCount.text intValue];
            roomModel.infantCount = [self.lblInfantsCount.text intValue];
            roomModel.arrayChildrensData = arrayChildCount;
            [roomRecordArray replaceObjectAtIndex:previousIndex-1 withObject:roomModel];
        }
        
    }
    
    RoomsDataModel *roomModel = roomRecordArray[index-1];
    self.lblAdultCount.text = [NSString stringWithFormat:@"%d",roomModel.adultCount];
    self.lblInfantsCount.text = [NSString stringWithFormat:@"%d",roomModel.infantCount];
    arrayChildCount = roomModel.arrayChildrensData;
    self.lblChildrenCount.text = [NSString stringWithFormat:@"%lu",(unsigned long)[arrayChildCount count]];
    [self.tblViewForChildCount reloadData];
    if (roomModel.adultCount == maxAdultCount)
    {
        _btnAdultAdd.alpha = 0;
    }else
    {
        _btnAdultAdd.alpha = 1;
    }
    
    if (roomModel.adultCount == 1)
    {
        _btnAdultDelete.alpha = 0;
    }
    else
    {
        _btnAdultDelete.alpha = 1;
    }
    
    if (roomModel.infantCount == maxInfantCount)
    {
        _btnInfantAdd.alpha = 0;
    }
    else
    {
        _btnInfantAdd.alpha = 1;
    }
    
    if (roomModel.infantCount == 0)
    {
        _btnInfantDelete.alpha = 0;
    }
    else
    {
        _btnInfantDelete.alpha = 1;
    }
    
    if (arrayChildCount.count == maxChildCount)
    {
        _btnChildAdd.alpha = 0;
    }
    else
    {
        _btnChildAdd.alpha = 1;
    }
    
    if(arrayChildCount.count == 0)
    {
        _btnChildDelete.alpha = 0;
    }
    else
    {
        _btnChildDelete.alpha = 1;
    }
    
    [self viewWillLayoutSubviews];
    [self.tblViewForChildCount reloadData];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)adjustUiForReCalculate
{
    self.txtSelectDate.text = @"Select Date";
    self.textFieldState.text = @"Select Customer State";
    self.buttonCalculate.alpha = 1;
    [self.buttonCalculate setTitle:@"Calculate" forState:UIControlStateNormal];
    self.constraintCalculateOffset.constant = 20;
    _viewForPaymentDetails.hidden = YES;
    _labelForPaymentOptions.hidden = YES;
    _viewForButtonContainers.hidden = YES;
    _constraintTermsConditions.constant = 41;
    _viewBookingMessege.hidden = YES;
    
    [self.view layoutIfNeeded];
}

- (IBAction)onAddDeleteButtonClicked:(id)sender
{
    [self adjustUiForReCalculate];
    int tag;
    if ([sender isKindOfClass:[UITapGestureRecognizer class]])
    {
        tag = (int)[sender view].tag;
    }
    else
    {
        tag = (int)[sender tag];
    }
    
    int count = (int)[numberArray count];
    if (tag == 1)
    {
        if (count == 1)
        {
            if (![[numberArray objectAtIndex:count-1] isEqualToString:@"addButton"])
            {
                // NSIndexPath *indexPath = [NSIndexPath  indexPathForRow:numberArray.count-1 inSection:0];
                [numberArray addObject:[NSString stringWithFormat:@"%d",count+1]];
                UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapNumber:)];
                tap.numberOfTapsRequired=1;
                [tapGestureArray insertObject:tap atIndex:numberArray.count-1];
                
                RoomsDataModel *roomModel = [[RoomsDataModel alloc]init];
                [roomRecordArray addObject:roomModel];
                
                [numberArray addObject:@"addButton"];
                //[_btnAddDelete setImage:[UIImage imageNamed:@"deleteIcon"] forState:UIControlStateNormal];
                _btnAddDelete.tag = 1;
            }
        }
        else
        {
            if (numberArray.count < 5)
            {
                NSIndexPath *indexPath = [NSIndexPath  indexPathForRow:numberArray.count-1 inSection:0];
                NSLog(@"INDEX PATH %ld",(long)indexPath.row);
                [numberArray insertObject:[NSString stringWithFormat:@"%d",count] atIndex:indexPath.row];
                UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapNumber:)];
                tap.numberOfTapsRequired=1;
                [tapGestureArray insertObject:tap atIndex:indexPath.row];
                
                RoomsDataModel *roomModel = [[RoomsDataModel alloc]init];
                [roomRecordArray insertObject:roomModel atIndex:indexPath.row];
                if (numberArray.count == 5)
                {
                    _btnAddDelete.hidden = YES;
                }
                else
                {
                    _btnAddDelete.hidden = NO;
                }
                
            }
            
        }
        
    }else
    {
        _btnAddDelete.hidden = NO;
        if (count == 3)
        {
            UITapGestureRecognizer *tap = tapGestureArray[1];
            [[tap view] setTag:3];
            [self tapNumber:tap];
            
            [numberArray removeLastObject];
            [numberArray removeLastObject];
            [_btnAddDelete setImage:[UIImage imageNamed:@"add"] forState:UIControlStateNormal];
            [_btnAddDelete setTag:1];
            [tapGestureArray removeLastObject];
            [roomRecordArray removeLastObject];
            UITapGestureRecognizer *tapNew = tapGestureArray[0];
            [self tapNumber:tapNew];
        }
        else
        {
            id lab= [tmpTap view];
            UILabel *label=(UILabel *)lab;
            NSString *indexText = label.text ;
            int intIndex = [indexText intValue];
            NSIndexPath *indexPath = [NSIndexPath  indexPathForRow:intIndex-1 inSection:0];
            
            if (indexPath.row == numberArray.count-2)
            {
                UITapGestureRecognizer *tap = tapGestureArray[indexPath.row-1];
                [[tap view] setTag:2];
                [self tapNumber:tap];
            }
            else
            {
                UITapGestureRecognizer *tap = tapGestureArray[indexPath.row+1];
                [[tap view] setTag:3];
                [self tapNumber:tap];
            }
            
            [numberArray removeObjectAtIndex:indexPath.row];
            [tapGestureArray removeObjectAtIndex:indexPath.row];
            [roomRecordArray removeObjectAtIndex:indexPath.row];
            
        }
    }
    [self plotMenuButtons];
    
}
#pragma mark -tableView data Source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.tblViewForChildCount)
    {
        return arrayChildCount.count;
    }
    else
    {
        return nonINRArray.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView ==  self.tblViewForChildCount)
    {
        static NSString *cellMainNibID = @"textPostCell";
        
        _tableViewCellChildAdd = [tableView dequeueReusableCellWithIdentifier:cellMainNibID];
        if (_tableViewCellChildAdd == nil)
        {
            
            [[NSBundle mainBundle] loadNibNamed:@"AddChildCell" owner:self options:nil];
        }
        UITextField *textField  = (UITextField *)[_tableViewCellChildAdd viewWithTag:10];
        UIButton *button  = (UIButton *)[_tableViewCellChildAdd viewWithTag:11];
        UILabel *bedSelectedLabel = (UILabel *)[_tableViewCellChildAdd viewWithTag:12];
        [button addTarget:self action:@selector(onBedRequredCheckMarkClicked:) forControlEvents:UIControlEventTouchUpInside];
        UILabel *numberLabel = (UILabel *)[_tableViewCellChildAdd viewWithTag:9];
        
        numberLabel.text = [NSString stringWithFormat:@"Age of child %d",(int)indexPath.row+1];
        NSMutableDictionary *arrayTempDict = [arrayChildCount objectAtIndex:indexPath.row];
        if (arrayTempDict.count == 0)
        {
            [arrayTempDict setObject:textField forKey:@"textField"];
            [arrayTempDict setObject:button forKey:@"button"];
            if ([packageType isEqualToString:kpackageTypeGITInternational])
            {
                if ([[packageDetail.stringRegion lowercaseString] isEqualToString:@"us"]||[[packageDetail.stringRegion lowercaseString] isEqualToString:@"usa"] || [[packageDetail.stringRegion lowercaseString] isEqualToString:@"africa"]||[[packageDetail.stringRegion lowercaseString] containsString:@"america"])
                {
                    [arrayTempDict setObject:@"n" forKey:@"isEdit"]; //no room option for this type
                    
//                    if (indexPath.row == 1)
//                    {
//                         [arrayTempDict setObject:@"n" forKey:@"isEdit"]; //no room option for this type
//                    }
                }
                else //europe
                {
//                    if ([self.lblAdultCount.text intValue] == 3)
//                    {
//                        [arrayTempDict setObject:@"n" forKey:@"isEdit"];
//                    }
//                    else
//                    {
//                        [arrayTempDict setObject:@"y" forKey:@"isEdit"];
//                    }
                }
            }
            else
            {
                [arrayTempDict setObject:@"y" forKey:@"isEdit"];
            }
            
            [arrayChildCount replaceObjectAtIndex:indexPath.row withObject:arrayTempDict];
        }
        else
        {
            UITextField *textFieldLocal = [arrayTempDict objectForKey:@"textField"];
            UIButton *buttonLocal = [arrayTempDict objectForKey:@"button"];
            textField.text = textFieldLocal.text;
            if ([[arrayTempDict valueForKey:@"isEdit"] isEqualToString:@"n"])
            {
                [button setSelected:NO];
                button.userInteractionEnabled = NO;
                bedSelectedLabel.textColor = [UIColor colorFromHexString:@"#999999"];
                
            }
            else
            {
                bedSelectedLabel.textColor  = [UIColor blackColor];
                textField.textColor  = [UIColor blackColor];
                button.userInteractionEnabled = YES;
                if([buttonLocal isSelected])
                {
                    [button setSelected:YES];
                }
                else
                {
                    [button setSelected:NO];
                }
            }
            
        }
        if ([packageType isEqualToString:kpackageTypeFITDomestic]||[packageType isEqualToString:kpackageTypeGITDomestic])
        {
            if([textField.text intValue] < 5)
            {
                bedSelectedLabel.textColor  = [UIColor colorFromHexString:@"#999999"];
            }else
            {
                bedSelectedLabel.textColor  = [UIColor blackColor];
            }
        }
        
        return _tableViewCellChildAdd;
        
      //  [self SetDataWithRespectToPackageType];
    }
    else
    {
        static NSString *simpleTableIdentifier = @"nonINRCellIdentifier";
        
        
        _nonINRCell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        if (_nonINRCell == nil)
        {
            
            [[NSBundle mainBundle] loadNibNamed:@"NonINRComponentCell" owner:self options:nil];
        }
        
        NSDictionary *nonINRDict = nonINRArray[indexPath.row];
        
        // UILabel *labelForTitle = (UILabel *)[_nonINRCell viewWithTag:1];
        UILabel *labelForNonINRPrice = (UILabel *)[_nonINRCell viewWithTag:2];
        UILabel *labelForTourComponentText = (UILabel *)[_nonINRCell viewWithTag:3];
        NSString *currencyString = [nonINRDict valueForKey:@"currencycode"];
        NSString *exchangeValue = [nonINRDict valueForKey:@"currencyRate"];


        
        if (![[currencyString lowercaseString] isEqualToString:@"inr"])
        {
            labelForTourComponentText.text = [NSString stringWithFormat:@"%@ Tour Component (calculated @ INR %@)",currencyString,exchangeValue];
            labelForNonINRPrice.text = [NSString stringWithFormat:@"%@  %@",[nonINRDict valueForKey:@"amount"],currencyString];
            //labelForTitle.text = [NSString stringWithFormat:@"(calculated @ INR %@)",exchangeValue];
        }
        else
        {
            labelForTourComponentText.text = [NSString stringWithFormat:@"%@ Tour Component",currencyString];
            labelForNonINRPrice.text = [NSString stringWithFormat:@"₹ %@",[nonINRDict valueForKey:@"amount"]];
            //labelForTitle.text = @"";
        }
        
        
        //  labelForNonINRPrice.text = [NSString stringWithFormat:@"%@  %@",[nonINRDict valueForKey:@"total"],currencyString];
        
        return _nonINRCell;
        
    }
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.tblViewForChildCount)
    {
        return 54;
    }
    else
    {
        
        return 43;
    }
    
}

-(NSIndexPath*)GetIndexPathFromSender:(id)sender
{
    if(!sender)
    {
        return nil;
    }
    
    if([sender isKindOfClass:[UITableViewCell class]])
    {
        UITableViewCell *cell = sender;
        return [_tblViewForChildCount indexPathForCell:cell];
    }
    
    return [self GetIndexPathFromSender:((UIView*)[sender superview])];
}

#pragma mark -clickEvents

-(void)onBedRequredCheckMarkClicked:(id )sender
{
    [self adjustUiForReCalculate];
    UIButton *buttonLocal = (UIButton *)sender;
    NSIndexPath *indexPath = [self GetIndexPathFromSender:sender];
    UITableViewCell *cell = (UITableViewCell *)[_tblViewForChildCount cellForRowAtIndexPath:indexPath];
    UITextView *textView = (UITextView *)[cell viewWithTag:10];
    
    if ([packageType isEqualToString:kpackageTypeFITDomestic]||[packageType isEqualToString:kpackageTypeGITDomestic])
    {
        int age = [textView.text intValue];
        if (age >= 5)
        {
            NSDictionary *dict = arrayChildCount[indexPath.row];
            UIButton *button = [dict valueForKey:@"button"];
            if ([buttonLocal isSelected])
            {
                [button setSelected:NO];
                [buttonLocal setSelected:NO];
                
            }
            else
            {
                [button setSelected:YES];
                [buttonLocal setSelected:YES];
            }
        }
    }
    else
    {
        NSDictionary *dict = arrayChildCount[indexPath.row];
        UIButton *button = [dict valueForKey:@"button"];
        if ([buttonLocal isSelected])
        {
            [button setSelected:NO];
            [buttonLocal setSelected:NO];
            
        }
        else
        {
            [button setSelected:YES];
            [buttonLocal setSelected:YES];
        }
    }
    
    
    [self.bookNowView setNeedsDisplay];
    
}

- (IBAction)onAdultsAddRemoveClicked:(id)sender
{
    
    [self adjustUiForReCalculate];
    NSString *adultCount = self.lblAdultCount.text;
    if ([sender tag] == 1)
    {
        
        if ([adultCount intValue]>1)
        {
            self.lblAdultCount.text = [NSString stringWithFormat:@"%d",[adultCount intValue]-1];
            NSString *adultCount = self.lblAdultCount.text;
            _btnAdultAdd.alpha = 1;
            if ([adultCount intValue]  ==1)
            {
                _btnAdultDelete.alpha = 0;
            }
        }
        
    }
    else if ([adultCount intValue] < maxAdultCount)
    {
        
        self.lblAdultCount.text = [NSString stringWithFormat:@"%d",[adultCount intValue]+1];
        NSString *adultCount = self.lblAdultCount.text;
        _btnAdultDelete.alpha = 1;
        if ([adultCount intValue] == maxAdultCount)
        {
            _btnAdultAdd.alpha = 0;
        }
        
    }
    
    
    int adultCountCalc =  [self.lblAdultCount.text intValue];
    int infantCount =  [self.lblInfantsCount.text intValue];
    int childCount = [self.lblChildrenCount.text intValue];
    
    
    
    
    if (adultCountCalc == 4)
    {
        maxInfantCount = 0;
        self.lblInfantsCount.text = @"0";
        maxChildCount = 0;
        [arrayChildCount removeAllObjects];
        
    }else if (adultCountCalc == 3)
    {
        if (childCount+infantCount >= 2 )
        {
            self.lblInfantsCount.text = @"0";
            [arrayChildCount removeAllObjects];
        }
        else
        {
            maxChildCount = 2;
            maxInfantCount = 2;
        }
        
    }
    else if (adultCountCalc == 2)
    {
        if (childCount+infantCount >= 3 )
        {
            self.lblInfantsCount.text = @"0";
            [arrayChildCount removeAllObjects];
        }
        else if (childCount == 2)
        {
            maxInfantCount = 0;
            self.lblInfantsCount.text = @"0";
        }
        else if (infantCount == 2)
        {
            [arrayChildCount removeAllObjects];
            maxChildCount = 0;
        } else if (childCount == 1 && infantCount == 1)
        {
            maxChildCount = childCount;
            maxInfantCount = infantCount;
        }
    }else if (adultCountCalc == 1)
    {
        if (childCount+infantCount >= 4 )
        {
            self.lblInfantsCount.text = @"0";
            [arrayChildCount removeAllObjects];
        }
    }
    [self SetDataWithRespectToPackageType];
    [self setAddDeleteButtonAlpha];
    
}

- (IBAction)onChildrenAddRemoveClicked:(id)sender
{
    
    [self adjustUiForReCalculate];
    NSString *adultCount = self.lblChildrenCount.text;
    if ([sender tag] == 1)
    {
        
        if ([adultCount intValue]>0)
        {
            //  self.lblChildrenCount.text = [NSString stringWithFormat:@"%d",[adultCount intValue]-1];
            [arrayChildCount removeLastObject];
            self.lblChildrenCount.text = [NSString stringWithFormat:@"%lu",(unsigned long)[arrayChildCount count]];
            _btnChildAdd.alpha = 1;
            if (arrayChildCount.count  == 0)
            {
                _btnChildDelete.alpha = 0;
            }
        }
        
    }else if ([adultCount intValue] < maxChildCount)
    {
        
        NSMutableDictionary *arrayTempDict = [[NSMutableDictionary alloc]init];
        [arrayChildCount addObject:arrayTempDict];
        self.lblChildrenCount.text = [NSString stringWithFormat:@"%lu",(unsigned long)[arrayChildCount count]];
        _btnChildDelete.alpha = 1;
        if (arrayChildCount.count == maxChildCount)
        {
            _btnChildAdd.alpha = 0;
        }
    }
    
    
    
    int adultCountCalc =  [self.lblAdultCount.text intValue];
    int infantCount =  [self.lblInfantsCount.text intValue];
    int childCount = [self.lblChildrenCount.text intValue];
    
    if (childCount == 1)
    {
        if (adultCountCalc + infantCount  >= 3)
        {
            maxInfantCount = infantCount;
            maxChildCount = childCount;
        }
        else
        {
            maxInfantCount = 2;
        }
        
    }else if (childCount == 2)
    {
        if (adultCountCalc + infantCount  == 2)
        {
            maxInfantCount = infantCount;
            maxChildCount = childCount;
        }
    }
    [self SetDataWithRespectToPackageType];
    [self setAddDeleteButtonAlpha];
}

- (IBAction)onInfantsAddRemoveClicked:(id)sender
{
    [self adjustUiForReCalculate];
    NSString *adultCount = self.lblInfantsCount.text;
    if ([sender tag] == 1)
    {
        if ([adultCount intValue]>0)
        {
            self.lblInfantsCount.text = [NSString stringWithFormat:@"%d",[adultCount intValue]-1];
            NSString *adultCount = self.lblInfantsCount.text;
            _btnInfantAdd.alpha = 1;
            if ([adultCount intValue]  == 0)
            {
                _btnInfantDelete.alpha = 0;
            }
        }
        
    }else if ([adultCount intValue] < maxInfantCount)
    {
        
        self.lblInfantsCount.text = [NSString stringWithFormat:@"%d",[adultCount intValue]+1];
        NSString *adultCount = self.lblInfantsCount.text;
        _btnInfantDelete.alpha = 1;
        if ([adultCount intValue] == maxInfantCount)
        {
            _btnInfantAdd.alpha = 0;
        }
        
    }
    [self SetDataWithRespectToPackageType];
    int adultCountCalc =  [self.lblAdultCount.text intValue];
    int infantCount =  [self.lblInfantsCount.text intValue];
    int childCount = [self.lblChildrenCount.text intValue];
    
    if (infantCount == 1)
    {
        if (adultCountCalc + childCount  >= 3)
        {
            maxChildCount = childCount;
            maxInfantCount = 1;
        }
        
    }else if (infantCount == 2)
    {
        if (adultCountCalc + childCount == 2)
        {
            
            maxChildCount = childCount;
            maxInfantCount = 2;
        }
        
    }
    
    [self setAddDeleteButtonAlpha];
}

-(void)setAccomType:(NSString *)accomTypeString
{
    if ([[accomTypeString lowercaseString] isEqualToString:@"standard"])
    {
        self.packageDetail.stringSelectedAccomType = @"0";
    }
    else if ([[accomTypeString lowercaseString] isEqualToString:@"delux"]||[[accomTypeString lowercaseString] isEqualToString:@"deluxe"])
    {
        self.packageDetail.stringSelectedAccomType = @"1";
    }
    else
    {
        self.packageDetail.stringSelectedAccomType = @"2";
    }
}

#pragma mark -textField Delegate

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    //[self adjustUiForReCalculate];
    if (textField ==  self.txtPackageType)
    {
        UIAlertController *durationActionSheet = [UIAlertController alertControllerWithTitle:@"Package Type" message:@"Select your choice" preferredStyle:UIAlertControllerStyleActionSheet];
        
        for (int j =0 ; j < packageTypeArray.count; j++)
        {
            NSString *titleString = packageTypeArray[j];
            UIAlertAction *action = [UIAlertAction actionWithTitle:titleString style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                self.txtSelectCity.text = @"Departure City";
                self.txtPackageType.text = packageTypeArray[j];
                if ([[self.txtPackageType.text lowercaseString] isEqualToString:@"standard"])
                {
                    accomType = 0;
                    
                    [self adjustUiForReCalculate];
                }
                else if ([[self.txtPackageType.text lowercaseString] isEqualToString:@"delux"]||[[self.txtPackageType.text lowercaseString] isEqualToString:@"deluxe"])
                {
                    accomType = 1;
                    
                    [self adjustUiForReCalculate];
                }
                else
                {
                    accomType = 2;
                    
                    [self adjustUiForReCalculate];
                }
                
                [self setAccomType:self.txtPackageType.text];
            }];
            
            [durationActionSheet addAction:action];
        }
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
            
            [self dismissViewControllerAnimated:durationActionSheet completion:nil];
        }];
        
        [durationActionSheet addAction:cancelAction];
        
        [self.view.window.rootViewController presentViewController:durationActionSheet animated:YES completion:nil];
        return NO;
        
    }
    else if (textField == self.txtSelectCity)
    {
        [self adjustUiForReCalculate];
        
        
            NSArray *hubList = [NSArray new];
        
            if([packageType isEqualToString:kpackageTypeFITDomestic]||[packageType isEqualToString:kpackageTypeFITInternational])
               {
                   hubList = self.packageDetail.arraytcilHolidayPriceCollection;

               }
            else
               {
                   hubList = self.packageDetail.arrayLtPricingCollection;
               }

        
        UIAlertController *durationActionSheet = [UIAlertController alertControllerWithTitle:@"Cities" message:@"Select your choice" preferredStyle:UIAlertControllerStyleActionSheet];
        NSArray *filteredarray;
        
        filteredarray = hubList;
        
        
        filteredarray = [hubList filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(packageClassId == %@)", [NSString stringWithFormat:@"%d",accomType]]];
        
        
        NSArray* uniqueValues;
        
        if([packageType isEqualToString:kpackageTypeFITDomestic]||[packageType isEqualToString:kpackageTypeFITInternational])
        {
         
            uniqueValues = [filteredarray valueForKeyPath:[NSString stringWithFormat:@"@distinctUnionOfObjects.hubCityCode.cityName"]];
            
        }
        else
        {
            
            uniqueValues = [filteredarray valueForKeyPath:[NSString stringWithFormat:@"@distinctUnionOfObjects.hubCode.cityName"]];
            
        }

        
        NSMutableArray * uniqueDataArray = [[NSMutableArray alloc] init];
        
        for (NSString * city in uniqueValues)
        {
            
            NSExpression *lhs;

            if([packageType isEqualToString:kpackageTypeFITDomestic]||[packageType isEqualToString:kpackageTypeFITInternational])
            {
                
                lhs = [NSExpression expressionForKeyPath:@"hubCityCode.cityName"];
                
            }
            else
            {
                lhs = [NSExpression expressionForKeyPath:@"hubCode.cityName"];
                
            }
            
            NSExpression *rhs = [NSExpression expressionForConstantValue:city];
            NSPredicate * finalPredicate = [NSComparisonPredicate
                                            predicateWithLeftExpression:lhs
                                            rightExpression:rhs
                                            modifier:NSDirectPredicateModifier
                                            type:NSContainsPredicateOperatorType
                                            options:NSCaseInsensitivePredicateOption];
            
            NSArray * filteredChannelArray   = [[filteredarray filteredArrayUsingPredicate:finalPredicate] mutableCopy];
            
            
            if (filteredChannelArray.count > 0) {
                
                [uniqueDataArray addObject:[filteredChannelArray objectAtIndex:0]];
            }
            
            
        }
        
        NSSortDescriptor *nameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"hubCode.cityName"  ascending:YES];
        
        NSArray *sortDescriptors = @[nameDescriptor];
        
        NSArray * ordered = [uniqueDataArray sortedArrayUsingDescriptors:sortDescriptors];
        
        
//        }
        
        for (int j =0 ; j < ordered.count; j++)
        {
            NSDictionary *cityDict = ordered[j];
            
            NSDictionary *hubCodeDict;
            if([packageType isEqualToString:kpackageTypeFITDomestic]||[packageType isEqualToString:kpackageTypeFITInternational])
            {
                hubCodeDict = [cityDict valueForKey:@"hubCityCode"];

            }
            else
            {
                hubCodeDict = [cityDict valueForKey:@"hubCode"];

            }
                
            
            NSString *titleString = [hubCodeDict valueForKey:@"cityName"];
            
            NSString *departFromLocal = [hubCodeDict valueForKey:@"cityCode"];
            
              NSString *ltCodeString = [cityDict valueForKey:@"ltItineraryCode"];
            
            
            UIAlertAction *action = [UIAlertAction actionWithTitle:titleString style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
            {
                departFrom = departFromLocal;
                self.txtSelectCity.text = titleString;
                selectedHubDict = hubCodeDict;
                selectedLtItineraryCode = ltCodeString;
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"hubCode.cityName contains[c] %@ ",self.txtSelectCity.text];
                NSArray *filteredData = [filteredarray filteredArrayUsingPredicate:predicate];
                //NSArray *filteredData = [filteredarray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(cityName[c] %@)", self.txtSelectCity.text]];
                ItineraryCodeArray = [[NSMutableArray alloc]init];
                NSLog(@"%@",filteredData);
                for(int i =0; i<filteredData.count;i++){
                    NSDictionary *cityDict = filteredData[i];
                    selectedLtItineraryCode = [cityDict valueForKey:@"ltItineraryCode"];
                    [ItineraryCodeArray addObject:selectedLtItineraryCode];
                }
                
                
            }];
            
            [durationActionSheet addAction:action];
        }
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
            
            [self dismissViewControllerAnimated:durationActionSheet completion:nil];
        }];
        
        [durationActionSheet addAction:cancelAction];
        
        [self.view.window.rootViewController presentViewController:durationActionSheet animated:YES completion:nil];
        return NO;
    }
    else if ([textField tag] == 10)
    {
        [self adjustUiForReCalculate];
        UIAlertController *durationActionSheet = [UIAlertController alertControllerWithTitle:@"Age" message:@"Select your choice" preferredStyle:UIAlertControllerStyleActionSheet];
        for (int j = 2 ; j < 12; j++)
        {
            NSString *titleString = [NSString stringWithFormat:@"%d",j];
            UIAlertAction *action = [UIAlertAction actionWithTitle:titleString style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                NSIndexPath *indexPath = [self GetIndexPathFromSender:textField];
                NSDictionary *dict = arrayChildCount[indexPath.row];
                UITextField *textFieldNew = [dict valueForKey:@"textField"];
                textFieldNew.text = titleString;
                textField.text = titleString;
                
                if ([packageType isEqualToString:kpackageTypeFITDomestic]||[packageType isEqualToString:kpackageTypeGITDomestic])
                {
                    int age = [titleString intValue];
                    if (age < 5)
                    {
                        NSDictionary *dict = arrayChildCount[indexPath.row];
                        UIButton *button = [dict valueForKey:@"button"];
                        if ([button isSelected])
                        {
                            [button setSelected:NO];
                        }
                    }
                    [[self tblViewForChildCount] reloadData];
                }
                [self.bookNowView setNeedsDisplay];
            }];
            
            [durationActionSheet addAction:action];
        }
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
            
            [self dismissViewControllerAnimated:durationActionSheet completion:nil];
        }];
        
        [durationActionSheet addAction:cancelAction];
        
        [self.view.window.rootViewController presentViewController:durationActionSheet animated:YES completion:nil];
        
        return NO;
    }
    else if (textField == self.txtSelectDate)
    {
        [self adjustUiForReCalculate];
        if (![self.txtSelectCity.text isEqualToString:@"Departure City"])
        {
            NSString *currentIndex = preLabel.text;
            int currentIndexCount = [currentIndex intValue];
            RoomsDataModel *roomModel = roomRecordArray[currentIndexCount- 1];
            roomModel.adultCount = [self.lblAdultCount.text intValue];
            roomModel.infantCount = [self.lblInfantsCount.text intValue];
            roomModel.arrayChildrensData = arrayChildCount;
            [roomRecordArray replaceObjectAtIndex:currentIndexCount- 1 withObject:roomModel];
            
//            BOOL isValid = [self checkValidityOfCombinations];
//            if (isValid)
//            {
//                if([[self.packageDetail.strPackageSubType lowercaseString] isEqualToString:@"fit"])
//                {
                   [self checkValidityOfCombinations];
                    [self fetchCalendarForFareCalendar];
//                }
//                else //if ([[self.packageDetail.packageMRP lowercaseString] isEqualToString:@"true"])
//                {
//                    [self fetchCalendarForFareCalendarMRP];
//                }
//            }else
//            {
//                UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Alert"
//                                                                                   message:@"Combination Not Allowed"
//                                                                            preferredStyle:UIAlertControllerStyleAlert];
//                
//                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
//                    [alertView dismissViewControllerAnimated:YES completion:nil];
//                }];
//                
//                [alertView addAction:okAction];
//                
//                [self presentViewController:alertView animated:YES completion:nil];
//            }
            
            return NO;
        }else
        {
            UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Alert"
                                                                               message:@"Please Select Departure City"
                                                                        preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [alertView dismissViewControllerAnimated:YES completion:nil];
            }];
            
            [alertView addAction:okAction];
            
            [self presentViewController:alertView animated:YES completion:nil];
            
            return NO;
        }
        
    }
    
    else if (textField == _textFieldState)
    {
        
        if ([_txtSelectCity.text isEqualToString:@"Departure City"])
        {
            [super showAlertViewWithTitle:@"Alert" withMessage:@"Select Departure City"];

        }
        else if ([_txtSelectDate.text isEqualToString:@"Select Date"])
        {
            [super showAlertViewWithTitle:@"Alert" withMessage:@"Select Date"];

        }
        else if ([_txtPackageType.text isEqualToString:@""])
        {
            [super showAlertViewWithTitle:@"Alert" withMessage:@"Select Package Type"];
        }
        else
        {
            UIAlertController *durationActionSheet = [UIAlertController alertControllerWithTitle:@"Cities" message:@"Select your choice" preferredStyle:UIAlertControllerStyleActionSheet];
            NSSortDescriptor *valueDescriptor = [[NSSortDescriptor alloc] initWithKey:@"gstState" ascending:YES];
            NSArray *descriptors = [NSArray arrayWithObject:valueDescriptor];
            NSArray *sortedArray = [stateDataArray sortedArrayUsingDescriptors:descriptors];
            NSArray *filteredarray = sortedArray;
            
            for (int j =0 ; j < filteredarray.count; j++)
            {
                NSDictionary *stateDict = filteredarray[j];
                NSString *stateName = [stateDict valueForKey:@"gstState"];
                NSString *stateCodeLocal = [stateDict valueForKey:@"gstStateCode"];
                
                UIAlertAction *action = [UIAlertAction actionWithTitle:stateName style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                {
                    selectedStateCode = stateCodeLocal;
                    _textFieldState.text = stateName;
                    selectedStateDict = stateDict;
                    
                    //  [self adjustUiForReCalculate];
                    
                /*    if([[self.packageDetail.strPackageSubType lowercaseString] isEqualToString:@"fit"])
                    {
                        [self calculationOfIndivisualAmountForFIT];
                        [self calculationForPackageForFIT];
                        self.packageDetail.ltProdCode = [calculationDict valueForKey:@"ltProductCode"];
                    }
                    else
                    {
                        [self calculateIndivisualAmountForNONFIT];
                        [self calculationForPackageForNONFIT];
                        self.packageDetail.ltProdCode = [dictForDate valueForKey:@"ltProductCode"];
                    }*/
                    
                //    [self calculationOfBookingAmount];
                    
                    
                    
                }];
                [durationActionSheet addAction:action];
            }
            
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
                
                [self dismissViewControllerAnimated:durationActionSheet completion:nil];
            }];
            
            [durationActionSheet addAction:cancelAction];
            
            [self.view.window.rootViewController presentViewController:durationActionSheet animated:YES completion:nil];
            
        }
    
        return NO;

    }
    
    return YES;
}

-(BOOL)checkValidityOfCombinations
{
    NSLog(@"%@",roomRecordArray)  ;
    id travellerInfo;
    
    //    if ([[self.packageDetail.packageMRP lowercaseString] isEqualToString:@"true"])
    //    {
    
    if([[self.packageDetail.strPackageSubType lowercaseString] isEqualToString:@"fit fixed"]|| [[self.packageDetail.strPackageSubType lowercaseString] isEqualToString:@"git"])
    {
        if([[self.packageDetail.strPackageType lowercaseString] isEqualToString:@"international"])
        {
            PackageInternationalGIT *packageInternational = [[PackageInternationalGIT alloc]initWithTravellerArray:roomRecordArray];
            packageInternational.travellerRegion = self.packageDetail.stringRegion;
            travellerInfo = [packageInternational calaculatePackageInternationalGIT];
        }
        else
        {
            PackageDomesticFit *packageInternational = [[PackageDomesticFit alloc]initWithTravellerArray:roomRecordArray];
            travellerInfo = [packageInternational calaculatePackageInternationalFIT];
        }
    }
    else
    {
        if([[self.packageDetail.strPackageType lowercaseString] isEqualToString:@"international"])
        {
            PackageInternationFIT *packageInternational = [[PackageInternationFIT alloc]initWithTravellerArray:roomRecordArray];
            packageInternational.travellerRegion = self.packageDetail.stringRegion;
            travellerInfo = [packageInternational calaculatePackageInternationalFIT];
        }
        else
        {
            PackageDomesticFit *packageInternational = [[PackageDomesticFit alloc]initWithTravellerArray:roomRecordArray];
            travellerInfo = [packageInternational calaculatePackageInternationalFIT];
        }
        
        
    }
    // return YES;
    //  }
    
    NSLog(@"%@",roomRecordArray)  ;
    if ([travellerInfo isKindOfClass:[NSString class]]||travellerInfo == nil )
    {
        return NO;
    }
    else if([travellerInfo isKindOfClass:[NSArray class]])
    {
        travellerArrayForCalculation = (NSArray*)travellerInfo;
        if (travellerArrayForCalculation.count == 0)
        {
            return NO;
        }
        return YES;
    }
    
    return NO;
}

-(void)fetchCalendarForFareCalendarMRP
{
    NSDate *today = [[NSDate alloc] init];
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
    [offsetComponents setYear:1];
    NSDate *nextYear = [gregorian dateByAddingComponents:offsetComponents toDate:today options:0];
    NSDateFormatter *formatter  = [[NSDateFormatter alloc]init];
    formatter.dateFormat = @"dd-MM-yyyy";
    NSString *fromDate = [formatter stringFromDate:today];
    NSString *toDate = [formatter stringFromDate:nextYear];
    
    NSMutableDictionary *dictOfData = [[NSMutableDictionary alloc]init];
    [dictOfData setValue:self.packageDetail.strPackageId forKey:@"packageId"];
    [dictOfData setValue:fromDate forKey:@"fromDate"];
    [dictOfData setValue:toDate forKey:@"toDate"];
    [dictOfData setValue:departFrom forKey:@"hub"];
    
    if ([CoreUtility connected])
    {
    activityLoadingView = [LoadingView loadingViewInView:self.view.superview.superview
                                              withString:@""
                                       andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
    
    
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NetworkHelper *helper = [NetworkHelper sharedHelper];
            NSString *strResponse = [helper getDataFromServerForType:@"webservice" entity:@"holiday" action:@"fareCalendarMRP" andUserJson:dictOfData];
            NSString *strStatus = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"status"];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if ([strStatus isEqualToString:kStatusSuccess])
                {
                    NSDictionary *dataDict = [[JsonSerealizer arrayOfJSonFromJsonData:strResponse] valueForKey:@"data"];
                    
                    
                    CalendarViewController *calenderVC = [[CalendarViewController alloc]initWithNibName:@"BaseViewController" bundle:nil];
                    calenderVC.dataDict = dataDict;
                    calenderVC.arrayTravellerCalculation = travellerArrayForCalculation;
                    calenderVC.selectedHubDict = selectedHubDict;
                    calenderVC.selectedStateDict = selectedStateDict;
                    if ([[self.packageDetail.packageMRP lowercaseString] isEqualToString:@"true"])
                    {
                        calenderVC.isMRP = YES;
                    }
                    else
                    {
                        calenderVC.isMRP = NO;
                    }
                    calenderVC.packageDetail = self.packageDetail; //need to replace
                    calenderVC.roomRecordArray = roomRecordArray;
                    calenderVC.delegate = self;
                    calenderVC.hubName = departFrom;
                    calenderVC.packageId = self.packageDetail.strPackageId;
                    calenderVC.accomType = accomType;
                    [[SlideNavigationController sharedInstance] pushViewController:calenderVC animated:YES];
                    
                    
                    
                    
                }else
                {
                    NSString *messsage = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"message"];
                    if (messsage)
                    {
                        if ([messsage isEqualToString:kVersionUpgradeMessage])
                        {
                            NSDictionary *downloadDict = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"data"];
                            NSString *downloadURL = [downloadDict valueForKey:@"download_url"];
                            [super showAlertViewForVersionUpdateWithUrl:downloadURL];
                        }else
                        {
                            [self showAlertViewWithTitle:@"Alert" withMessage:messsage];
                        }
                    }else
                    {
                        [super showAlertViewWithTitle:@"Alert" withMessage:kMessageSomeErrorTryAfterSometime];
                    }
                    
                }
                if ([activityLoadingView isDescendantOfView:self.view.superview.superview])
                {
                    [activityLoadingView removeView];
                    activityLoadingView = nil;
                }
                
            });
            
        });
    }
    else
    {
        [super showAlertViewWithTitle:@"Alert" withMessage:kMessageNoInternet];
        
    }
    
    
}

-(void)fetchCalendarForFareCalendar
{
    
    if([CoreUtility connected])
    {

    @try
    {
        int totalChildCount = 0;
        int totalAdultCount = 0;
        
        for (int i = 0; i<roomRecordArray.count; i++)
        {
            RoomsDataModel *dataModel = roomRecordArray[i];
            totalAdultCount = totalAdultCount + dataModel.adultCount;
            totalChildCount = totalChildCount + (int)dataModel.arrayChildrensData.count;
        }
        
        NSMutableDictionary *dictOfData = [[NSMutableDictionary alloc]init];
        [dictOfData setValue:self.packageDetail.strPackageId forKey:@"packageId"];
        [dictOfData setValue:self.txtPackageType.text forKey:@"accomType"];
        [dictOfData setValue:[NSString stringWithFormat:@"%d",totalAdultCount+totalChildCount] forKey:@"noOfPax"];
        [dictOfData setValue:departFrom forKey:@"departFrom"];
        
        
        
            
            activityLoadingView = [LoadingView loadingViewInView:self.view.superview.superview
                                                      withString:@""
                                               andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
            
            
            NetworkHelper *helper = [NetworkHelper sharedHelper];
            
            
            
            NSMutableArray *arrayForItinarayCode = [[NSMutableArray alloc] init];
        
        
        
        if (![packageType isEqualToString:kpackageTypeFITInternational] && ![packageType isEqualToString:kpackageTypeFITDomestic])
        {
          //  [arrayForItinarayCode addObject:selectedLtItineraryCode];
            [arrayForItinarayCode addObjectsFromArray:ItineraryCodeArray];
        }
       
            

        
//            for (int i = 0; i<packageDetail.arrayLtPricingCollection.count; i++)
//            {
//                NSDictionary *ltCodeDict = packageDetail.arrayLtPricingCollection[i];
//                
//                NSDictionary *hubCodeDict = [ltCodeDict valueForKey:@"hubCode"];
//                
//                NSString *titleString = [hubCodeDict valueForKey:@"cityName"];
//
//                if ([_txtSelectCity.text isEqualToString:titleString])
//                {
//                    NSString *ltCodeString = [ltCodeDict valueForKey:@"ltItineraryCode"];
//                    
//                    [arrayForItinarayCode addObject:ltCodeString];
//
//                }
//            }
        
            
            NSMutableDictionary *jsonDict = [[NSMutableDictionary alloc] init];
            [jsonDict setObject:arrayForItinarayCode forKey:@"ltItineraryCode"];
            //[jsonDict setObject:packageDetail.ltMarket forKey:@"market"];
            [jsonDict setObject:@"-1" forKey:@"market"];
        
            [jsonDict setObject:departFrom forKey:@"hubCode"];
            [jsonDict setObject:[NSNumber numberWithInteger:[packageDetail.strPackageSubTypeID integerValue]] forKey:@"pkgSubTypeId"];
            [jsonDict setObject:self.packageDetail.stringSelectedAccomType forKey:@"pkgClassId"];
            [jsonDict setObject:packageDetail.strPackageId forKey:@"pkgId"];
            [jsonDict setObject:@"TCIL" forKey:@"mode"];
            [jsonDict setObject:@"N" forKey:@"isHsa"];
            
            
            NSDictionary *headerDict = [CoreUtility getHeaderDict];
            
            [helper getResponseWithRequestType:@"POST" withQueryParam:@"" withPathParam:@"" withJsonParam:jsonDict withHeaders:headerDict withUrl:kAstraUrlFareCalender success:^(NSDictionary *responseDict)
             {
                 NSLog(@"Response Dict : %@",responseDict);
                 
                 
                 dispatch_async(dispatch_get_main_queue(), ^(void)
                                {
                                    [activityLoadingView removeFromSuperview];
                                    if (responseDict)
                                    {
                                        if (responseDict.count>0)
                                        {
                                            CalendarViewController *calenderVC = [[CalendarViewController alloc]initWithNibName:@"BaseViewController" bundle:nil];
                                            calenderVC.dataDict = responseDict;
                                            if ([[self.packageDetail.packageMRP lowercaseString] isEqualToString:@"true"])
                                            {
                                                calenderVC.isMRP = YES;
                                            }
                                            else
                                            {
                                                calenderVC.isMRP = NO;
                                            }
                                            
                                            calenderVC.delegate = self;
                                            calenderVC.packageDetail = self.packageDetail;
                                            calenderVC.arrayTravellerCalculation = travellerArrayForCalculation;
                                            calenderVC.roomRecordArray = roomRecordArray;
                                            calenderVC.hubName = departFrom;
                                            calenderVC.packageId = self.packageDetail.strPackageId;
                                            calenderVC.accomType = accomType;
                                            [[SlideNavigationController sharedInstance] pushViewController:calenderVC animated:YES];
                                        }
                                        else
                                        {
                                            [super showAlertViewWithTitle:@"Alert" withMessage:@"Dates not available for this package."];
                                            
                                        }
                                    }
                                    else
                                    {
                                        [super showAlertViewWithTitle:@"Alert" withMessage:@"Dates not available for this package."];
                                        
                                    }
                                });
                 
             }
                                       failure:^(NSError *error)
             {dispatch_async(dispatch_get_main_queue(), ^(void)
                             {
                                 [activityLoadingView removeFromSuperview];
                                 [super showAlertViewWithTitle:@"Alert" withMessage:@"Dates not available for this package."];
                                 NSLog(@"Response Dict : %@",error);
                             });
                 
             }];
    }
    @catch (NSException *exception)
    {
        NSLog(@"%@", exception.reason);
        [activityLoadingView removeFromSuperview];
    }
    @finally
    {
        NSLog(@"exception finally called");
        
    }
    
        
        
    
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//        NetworkHelper *helper = [NetworkHelper sharedHelper];
//        NSString *strResponse = [helper getDataFromServerForType:@"webservice" entity:@"fareCalendar" action:@"search" andUserJson:dictOfData];
//        NSString *strStatus = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"status"];
//        dispatch_async(dispatch_get_main_queue(), ^{
//            if ([strStatus isEqualToString:kStatusSuccess])
//            {
//                NSDictionary *dataDict = [[JsonSerealizer arrayOfJSonFromJsonData:strResponse] valueForKey:@"data"];
//                
//                CalendarViewController *calenderVC = [[CalendarViewController alloc]initWithNibName:@"BaseViewController" bundle:nil];
//                calenderVC.dataDict = dataDict;
//                if ([[self.packageDetail.packageMRP lowercaseString] isEqualToString:@"true"])
//                {
//                    calenderVC.isMRP = YES;
//                }
//                else
//                {
//                    calenderVC.isMRP = NO;
//                }
//                calenderVC.delegate = self;
//                calenderVC.packageDetail = self.packageDetail;
//                calenderVC.arrayTravellerCalculation = travellerArrayForCalculation;
//                calenderVC.roomRecordArray = roomRecordArray;
//                calenderVC.hubName = departFrom;
//                calenderVC.packageId = self.packageDetail.strPackageId;
//                calenderVC.accomType = accomType;
//                [[SlideNavigationController sharedInstance] pushViewController:calenderVC animated:YES];
//                
//                
//            }else
//            {
//                NSString *messsage = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"message"];
//                if (messsage)
//                {
//                    if ([messsage isEqualToString:kVersionUpgradeMessage])
//                    {
//                        NSDictionary *downloadDict = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"data"];
//                        NSString *downloadURL = [downloadDict valueForKey:@"download_url"];
//                        [super showAlertViewForVersionUpdateWithUrl:downloadURL];
//                    }else
//                    {
//                        [self showAlertViewWithTitle:@"Alert" withMessage:messsage];
//                    }
//                }else
//                {
//                    [super showAlertViewWithTitle:@"Alert" withMessage:kMessageSomeErrorTryAfterSometime];
//                }
//                
//            }
//           
//                [activityLoadingView removeView];
//                activityLoadingView = nil;
//           
//            
//        });
//    });
    }
    else
    {
        [super showAlertViewWithTitle:@"Alert" withMessage:kMessageNoInternet];
    }
    
}

- (IBAction)onEasyPaymentButtonClicked:(id)sender
{
    UILabel *labelForEasyPayment1 = (UILabel *)[self.viewForButtonContainers viewWithTag:7];
    UILabel *labelForEasyPayment2 = (UILabel *)[self.viewForButtonContainers viewWithTag:9];
    UILabel *labelForEasyPaymentCost1 = (UILabel *)[self.viewForButtonContainers viewWithTag:8];
    UILabel *labelForEasyPaymentCost2 = (UILabel *)[self.viewForButtonContainers viewWithTag:10];
    if ([sender tag]==5)
    {
        labelForEasyPayment1.textColor = [UIColor colorFromHexString:@"#ffffff"];
        labelForEasyPaymentCost1.textColor = [UIColor colorFromHexString:@"#ffffff"];
        labelForEasyPayment2.textColor = [UIColor colorFromHexString:@"#888888"];
        labelForEasyPaymentCost2.textColor = [UIColor colorFromHexString:@"#8e8e8e"];
        bookingAmountForBookingSubmit = bookingAmountString ;
        
        amountType = @"A";
        
    }else
    {
        labelForEasyPayment1.textColor = [UIColor colorFromHexString:@"#888888"];
        labelForEasyPaymentCost1.textColor = [UIColor colorFromHexString:@"#8e8e8e"];
        labelForEasyPayment2.textColor = [UIColor colorFromHexString:@"#ffffff"];
        labelForEasyPaymentCost2.textColor = [UIColor colorFromHexString:@"#ffffff"];
        bookingAmountForBookingSubmit = totalPackageAmountString;
         amountType = @"F";
    }
    
}

#pragma mark -calculation Methods

- (IBAction)onCalculateButtonClicked:(id)sender
{
    
    if ([self.txtSelectCity.text isEqualToString:@"Departure City"])
    {
        [super showAlertViewWithTitle:@"Alert" withMessage:@"Select Departure City"];
    }
    else if ([self.txtSelectDate.text isEqualToString:@"Select Date"])
    {
        [super showAlertViewWithTitle:@"Alert" withMessage:@"Select Date"];
    }
    else if ([self.textFieldState.text isEqualToString:@"Select Customer State"])
    {
        [super showAlertViewWithTitle:@"Alert" withMessage:@"Select Customer State"];
    }
    else
    {
        
        UIButton *button = (UIButton *)sender;
        if([[button.titleLabel.text lowercaseString] isEqualToString:@"calculate"])
        {
            
            
             [self calculatePriceAstra];
            
            //28-02-2018
            [self netCoreInternationalHolidayCalculate];
            
          //  [self netCoreInternationalHolidayCalculateContinue];
            
          //  [FIRAnalytics logEventWithName:@"Calculate" parameters:@{kFIRParameterItemID :@"Calculate"}];
            
//            if ([self.packageDetail.strPackageType caseInsensitiveCompare:@"international"]==NSOrderedSame)
//            {
//                [FIRAnalytics logEventWithName:@"International_Holidays_Calculate_Continue_PDP_details" parameters:@{kFIRParameterItemName:[NSString stringWithFormat:@"PackageName :%@,PackageType :%@,PackageID :%@, DepartureCity :%@,DepartureDate : %@,AdultCount : %@,InfantCount : %@,ChildrenCount : %@",self.packageDetail.strPackageName,self.packageDetail.strPackageType,self.packageDetail.strPackageId,self.txtSelectCity.text,self.txtSelectDate.text,self.lblAdultCount.text,self.lblInfantsCount.text,self.lblChildrenCount.text]}];
//            }
//            else
//            {
//                [FIRAnalytics logEventWithName:@"India_Holidays_Calculate_Continue_PDP_details" parameters:@{kFIRParameterItemName:[NSString stringWithFormat:@"PackageName :%@,PackageType :%@,PackageID :%@, DepartureCity :%@,DepartureDate : %@,AdultCount : %@,InfantCount : %@,ChildrenCount : %@",self.packageDetail.strPackageName,self.packageDetail.strPackageType,self.packageDetail.strPackageId,self.txtSelectCity.text,self.txtSelectDate.text,self.lblAdultCount.text,self.lblInfantsCount.text,self.lblChildrenCount.text]}];
//            }
            
            
//            if (calculationDict != nil)
//            {
            
//                if ([[self.packageDetail.stringBookingOnline lowercaseString] isEqualToString:@"n"] || [[self.packageDetail.stringIsOnReq lowercaseString]  isEqualToString:@"y"])
//                {
//                    _constraintTermsConditions.constant = 41+_constraintTermsConditions.constant+50;
//                    _viewBookingMessege.hidden = NO;
//                    _constraintHeightButtonContainer.constant = 0;
//                    self.constraintCalculateOffset.constant = self.viewForPaymentDetails.frame.size.height+15;
//                    self.buttonCalculate.alpha = 0;
//                    _labelForPaymentOptions.hidden = YES;
//                    
//                }
//                else
//                {
//                    _constraintTermsConditions.constant = 41;
//                    _viewBookingMessege.hidden = YES;
//                    self.buttonCalculate.alpha = 1;
//                    [button setTitle:@"Continue" forState:UIControlStateNormal];
//                    _constraintHeightButtonContainer.constant = 89;
//                    self.constraintCalculateOffset.constant = 178 + self.viewForPaymentDetails.frame.size.height;
//                    _labelForPaymentOptions.hidden = NO;
//                    
//                    if([[button.titleLabel.text lowercaseString] isEqualToString:@"Continue"])
//                    {

//                    if ([self.packageDetail.strPackageType caseInsensitiveCompare:@"international"]==NSOrderedSame)
//                    {
//                        [FIRAnalytics logEventWithName:@"International_Holidays_Calculate_Continue_PDP_details" parameters:@{kFIRParameterItemName:[NSString stringWithFormat:@"PackageName :%@,PackageType :%@,PackageID :%@, DepartureCity :%@,DepartureDate : %@,AdultCount : %@,InfantCount : %@,ChildrenCount : %@",self.packageDetail.strPackageName,self.packageDetail.strPackageType,self.packageDetail.strPackageId,self.txtSelectCity.text,self.txtSelectDate.text,self.lblAdultCount.text,self.lblInfantsCount.text,self.lblChildrenCount.text]}];
//                    }
//                    else
//                    {
//                        [FIRAnalytics logEventWithName:@"India_Holidays_Calculate_Continue_PDP_details" parameters:@{kFIRParameterItemName:[NSString stringWithFormat:@"PackageName :%@,PackageType :%@,PackageID :%@, DepartureCity :%@,DepartureDate : %@,AdultCount : %@,InfantCount : %@,ChildrenCount : %@",self.packageDetail.strPackageName,self.packageDetail.strPackageType,self.packageDetail.strPackageId,self.txtSelectCity.text,self.txtSelectDate.text,self.lblAdultCount.text,self.lblInfantsCount.text,self.lblChildrenCount.text]}];
//                    }
        //            }
                    
      //          }
                
                /*
                 if ([[self.packageDetail.stringBookingOnline lowercaseString] isEqualToString:@"n"] && [[self.packageDetail.stringIsOnReq lowercaseString]  isEqualToString:@"y"])
                 {
                 //dont show calc
                 _constraintTermsConditions.constant = 41+_constraintTermsConditions.constant+50;
                 _viewBookingMessege.hidden = NO;
                 _constraintHeightForEasyPayment.constant = 0;
                 
                 }
                 else if ([[self.packageDetail.stringBookingOnline lowercaseString] isEqualToString:@"y"] && [[self.packageDetail.stringIsOnReq lowercaseString]  isEqualToString:@"y"])
                 {
                 //show calc
                 _constraintTermsConditions.constant = 41+_constraintTermsConditions.constant+50;
                 _viewBookingMessege.hidden = NO;
                 _constraintHeightForEasyPayment.constant = 89;
                 }
                 else if ([[self.packageDetail.stringBookingOnline lowercaseString] isEqualToString:@"n"] && [[self.packageDetail.stringIsOnReq lowercaseString]  isEqualToString:@"n"])
                 {
                 // show calc
                 _constraintTermsConditions.constant = 41+_constraintTermsConditions.constant+50;
                 _viewBookingMessege.hidden = NO;
                 _constraintHeightForEasyPayment.constant = 89;
                 }
                 //               else if ([[self.packageDetail.stringBookingOnline lowercaseString] isEqualToString:@"y"] || [[self.packageDetail.stringIsOnReq lowercaseString]  isEqualToString:@"n"])
                 //               {
                 //                  //regular
                 //               }
                 else
                 {
                 _constraintHeightForEasyPayment.constant = 89;
                 _constraintTermsConditions.constant = 41;
                 _viewBookingMessege.hidden = YES;
                 [button setTitle:@"Continue" forState:UIControlStateNormal];
                 }
                 */
                
                
     //           _viewForPaymentDetails.hidden = NO;
                
       //         _viewForButtonContainers.hidden = NO;
                // self.constraintCalculateOffset.constant = 178 + self.viewForPaymentDetails.frame.size.height;
                
//                [UIView animateWithDuration:0.4 animations:^{
//                    
//                    [self.view layoutIfNeeded];
//                }];
//            }
            
        }
        else
        {
            [self netCoreInternationalHolidayCalculateContinue];
            
//            if( packageDetail.pkgStatusId == 2)
//          
//            {
            
            
                NSUserDefaults *LoginStatus=[NSUserDefaults standardUserDefaults];
                NSString *statusForLogin= [LoginStatus valueForKey:kLoginStatus];
                
                if (![statusForLogin isEqualToString:kLoginSuccess])
                {
                   /* LoginViewPopUp *loginView = [[LoginViewPopUp alloc] initWithNibName:@"LoginViewPopUp" bundle:nil];
                    
                    [LoginViewPopUp sharedInstance].object=loginView;
                    loginView.view.frame = CGRectMake(0, 0, 300, self.bookNowView.frame.size.height - 100);
                    loginView.isForGuestUser = TRUE;
                    [self presentPopupViewController:loginView animationType:MJPopupViewAnimationFade];*/
                    
                    NewLoginPopUpViewController *loginView = [[NewLoginPopUpViewController alloc] initWithNibName:@"NewLoginPopUpViewController" bundle:nil];
                   
                    //loginView.currentSelectedeScreen = @"Guest User";
                    
                     UINavigationController * loginNavigation = [[UINavigationController alloc] initWithRootViewController:loginView];
                    
                    [self presentViewController:loginNavigation animated:true completion:^{
                        
                    }];
                }
                else
                {
                    //[self preconfirmationBookingAstra];
                    [self bookingTypeSubmission];
                   
                }
           // }
        }
    }
    
    [UIView animateWithDuration:0.4 animations:^{
        
        [self.view layoutIfNeeded];
    }];

}

- (void)SetDataForCalendarDate:(NSDictionary *)dateDict withCalculationData:(NSDictionary *)calculationData
{
    
   
    self.txtSelectDate.text = [dateDict valueForKey:@"DATE"];
   // calculationDict = calculationData;
    dictForDate = dateDict;
    
   
    
    //_textFieldState.text = @"Maharashtra";
    
  /*  NSString *filePath = [[NSBundle mainBundle] pathForResource:@"pricingResponse" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:filePath];
    
    NSDictionary *pricingDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];

    calculationDict = pricingDict;
    
    if([[self.packageDetail.strPackageSubType lowercaseString] isEqualToString:@"fit"])
    {
        //[self calculationOfIndivisualAmountForFIT];
        [self calculationForPackageForFIT];
       // self.packageDetail.ltProdCode = [calculationDict valueForKey:@"ltProductCode"];
    }
    else
    {
        //[self calculateIndivisualAmountForNONFIT];
        [self calculationForPackageForNONFIT];
       // self.packageDetail.ltProdCode = [dictForDate valueForKey:@"ltProductCode"];
    }
    
  //  [self calculationOfBookingAmount];
    */
}


-(void)calculationOfIndivisualAmountForFIT
{
    NSMutableDictionary *dictForCalculation ;
    NSArray *arrayForUSDandINR  = [calculationDict valueForKey:@"priceList"];
    NSPredicate *predicateStringUSD = [NSPredicate predicateWithFormat:@"%K != %@", @"currency", @"INR"];
    NSPredicate *predicateStringINR = [NSPredicate predicateWithFormat:@"%K == %@", @"currency", @"INR"];
    NSArray *arrayUSD = [arrayForUSDandINR filteredArrayUsingPredicate:predicateStringUSD];
    NSArray *arrayINR = [arrayForUSDandINR filteredArrayUsingPredicate:predicateStringINR];
    
    
    if (arrayINR.count != 0)
    {
        dictForCalculation = arrayINR[0];
    }
    else
    {
        dictForCalculation = arrayUSD[0];
    }
    
    for (int i = 0; i< travellerArrayForCalculation.count; i++)
    {
        TravellerInformationModel *travellerInfoModelInstance = travellerArrayForCalculation[i];
        NSString *travellerRoomingType = travellerInfoModelInstance.travellerRoomingType;
        NSString *tsValue = [dictForCalculation valueForKey:@"ts"];
        NSString *roomType = travellerRoomingType;
        NSString *currency = [dictForCalculation valueForKey:@"currency"];
        float exchRate = [[dictForCalculation valueForKey:@"exRate"] floatValue];
        int tsValueInt = [tsValue intValue];
        float indivisualValue;
        
        if ([travellerRoomingType isEqualToString:kTravellerRoomTypeDR])
        {
            if (tsValueInt != 0)
            {
                roomType = kTravellerRoomTypeTS;
            }
        }
        
        
        //        if ([packageType isEqualToString:kpackageTypeFITDomestic]||[packageType isEqualToString:kpackageTypeGITDomestic])//if traveller child CNB age is 2 to 5
        //        {
        if ([travellerRoomingType isEqualToString:kTravellerRoomTypeCNB])
        {
            if (travellerInfoModelInstance.travellerAge >=2 && travellerInfoModelInstance.travellerAge<=4)
            {
                roomType = @"cnb2To5YrsPrice";
            }else
            {
                roomType = kTravellerRoomTypeCNB;
            }
        }
        //      }
        
        if (travellerInfoModelInstance.travellerRoomNumber != 1)
        {
            if ([travellerRoomingType isEqualToString:kTravellerRoomTypeSR])
            {
                roomType = @"srOtherPrice";
            }
        }
        
        if ([self isValidRoomCombinationForNonMRPWithRoomingType:roomType withCalcDict:dictForCalculation])//if room type 0 and -1 do not allowd
        {
            
            if ([currency isEqualToString:kCurrency_japanese_yen] || [currency isEqualToString:kCurrency_thai_baht] )
            {
                NSString *typeValue = [dictForCalculation valueForKey:roomType];
                indivisualValue  = ([typeValue floatValue]/100) * exchRate;
            }
            else
            {
                NSString *typeValue = [dictForCalculation valueForKey:roomType];
                indivisualValue = [typeValue floatValue] * exchRate;
            }
            
            travellerInfoModelInstance.indivisualPrice = indivisualValue;
            
        }
        else
        {
            UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Alert"
                                                                               message:[NSString stringWithFormat:@"Person combination is not allowed for room number %d",travellerInfoModelInstance.travellerRoomNumber]
                                                                        preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [alertView dismissViewControllerAnimated:YES completion:nil];
            }];
            
            [alertView addAction:okAction];
            
            [self presentViewController:alertView animated:YES completion:nil];
            
            calculationDict = nil;
            
            return;
            
            
        }
    }
}

-(BOOL)isValidRoomCombinationForNonMRPWithRoomingType:(NSString *)roomType withCalcDict:(NSDictionary *)dictCalc
{
    NSArray *arrayForUSDandINR  = [calculationDict valueForKey:@"priceList"];
    NSArray *arrayForData =  [arrayForUSDandINR valueForKey:roomType];
    
    for (int i = 0; i<arrayForData.count; i++)
    {
        NSString *valueString  = arrayForData[i];
        if ([valueString isEqualToString:@"0"])
        {
            if ([roomType isEqualToString:@"cnb2To5YrsPrice"] || [roomType isEqualToString:kTravellerRoomTypeInfant]) //for cnb2To5YrsPrice pass for calc even if value is 0
            {
                if ([packageType isEqualToString:kpackageTypeFITDomestic])
                {
                    return YES;
                }
            }
        }
        else if (![valueString isEqualToString:@"0"] && ![valueString isEqualToString:@"-1"])
        {
            return YES;
        }
    }
    
    return NO;
    
    
    
    //    if ([[dictCalc valueForKey:roomType] intValue] == 0 || [[dictCalc valueForKey:roomType] intValue] == -1 )
    //    {
    //        if ([roomType isEqualToString:@"cnb2To5YrsPrice"] || [roomType isEqualToString:kTravellerRoomTypeInfant]) //for cnb2To5YrsPrice pass for calc even if value is 0
    //        {
    //            if ([packageType isEqualToString:kpackageTypeFITDomestic])
    //            {
    //                if([[dictCalc valueForKey:roomType] intValue] == 0)
    //                {
    //                    return YES;
    //                }
    //                else
    //                {
    //                    return NO;
    //                }
    //            }
    //        }
    //        return NO;
    //    }
    //    else
    //    {
    //        return YES;
    //    }
}

-(void)calculateIndivisualAmountForNONFIT
{
    int noOfSR = 0;
    int noOfDr = 0;
    int noOfTr = 0;
    int noOfCwb = 0;
    int noOfCnb = 0;
    int noOfInf = 0;
    
    //    for (int i = 0; i< travellerArrayForCalculation.count; i++)
    //    {
    //        TravellerInformationModel *travellerInfoModelInstance = travellerArrayForCalculation[i];
    //        NSString *travellerRoomingType = travellerInfoModelInstance.travellerRoomingType;
    //
    //        if ([travellerRoomingType isEqualToString:kTravellerRoomTypeCNB])
    //        {
    //            noOfCnb = noOfCnb + 1;
    //        }else if ([travellerRoomingType isEqualToString:kTravellerRoomTypeCWB])
    //        {
    //            noOfCwb = noOfCwb + 1;
    //        }else if ([travellerRoomingType isEqualToString:kTravellerRoomTypeSR])
    //        {
    //            noOfSR = noOfSR + 1;
    //        }else if ([travellerRoomingType isEqualToString:kTravellerRoomTypeDR])
    //        {
    //            noOfDr = noOfDr + 1 ;
    //        }else if ([travellerRoomingType isEqualToString:kTravellerRoomTypeTR])
    //        {
    //            noOfTr = noOfTr + 1 ;
    //        }
    //        else if ([travellerRoomingType isEqualToString:kTravellerRoomTypeInfant])
    //        {
    //            noOfInf = noOfInf + 1 ;
    //        }
    //
    //    }
    
    for (int i = 0; i< roomRecordArray.count; i++)
    {
        RoomsDataModel *dataModel = roomRecordArray[i];
        int infantCount = dataModel.infantCount;
        noOfInf = noOfInf + infantCount;
        noOfSR = noOfSR + dataModel.SRCount;
        noOfDr = noOfDr + dataModel.DRCount;
        noOfTr = noOfTr + dataModel.TRCount;
        noOfCwb = noOfCwb + dataModel.CWBCount;
        noOfCnb = noOfCnb + dataModel.CNBCount;
    }
    
    
    NSArray *arrayForUSDandINR = [calculationDict valueForKey:@"priceList"];
    
    NSPredicate *predicateStringINR = [NSPredicate predicateWithFormat:@"%K == %@", @"currency", @"INR"];
    NSArray *arrayINR = [arrayForUSDandINR filteredArrayUsingPredicate:predicateStringINR];
    
    NSDictionary *INRDict;
    if (arrayINR.count != 0)
    {
        INRDict = arrayINR[0];
    }
    float srValue = [[INRDict valueForKey:@"sr"] floatValue];
    float drValue = [[INRDict valueForKey:@"dr"] floatValue];
    float trValue = [[INRDict valueForKey:@"tr"] floatValue];
    float cnbValue = [[INRDict valueForKey:@"cnb"] floatValue];
    float cwbValue = [[INRDict valueForKey:@"cwb"] floatValue];
    float infValue = [[INRDict valueForKey:@"inf"] floatValue];
    float exchangeRate = [[INRDict valueForKey:@"exRate"] floatValue];
    NSString *tsValue = [INRDict valueForKey:@"ts"];
    int tsValueInt = [tsValue intValue];
    
    if (INRDict.count != 0)
    {
        for (int index = 0; index < travellerArrayForCalculation.count ; index++)
        {
            TravellerInformationModel *travellerInstance = travellerArrayForCalculation[index];
            NSString *travellerRoomingType = travellerInstance.travellerRoomingType;
            NSString *roomType  = travellerRoomingType;
            if ([travellerRoomingType isEqualToString:kTravellerRoomTypeDR])
            {
                if (tsValueInt != 0)
                {
                    roomType = kTravellerRoomTypeTS;
                }
            }
            
            if ([packageType isEqualToString:kpackageTypeFITDomestic]||[packageType isEqualToString:kpackageTypeGITDomestic])//if traveller child CNB age is 2 to 5
            {
                if ([travellerRoomingType isEqualToString:kTravellerRoomTypeCNB])
                {
                    if (travellerInstance.travellerAge >=2 && travellerInstance.travellerAge<=4)
                    {
                        roomType = @"cnb2To5YrsPrice";
                    }else
                    {
                        roomType = kTravellerRoomTypeCNB;
                    }
                }
            }
            
            
            if (travellerInstance.travellerRoomNumber != 1)
            {
                if ([travellerRoomingType isEqualToString:kTravellerRoomTypeSR])
                {
                    roomType = @"srOtherPrice";
                }
            }
            
            BOOL isValid = [self  checkValidationForNonFITWithRoomType:roomType];
            if (isValid)
            {
                if ([travellerInstance.travellerRoomingType isEqualToString:kTravellerRoomTypeSR])
                {
                    float indivisualSRValue = srValue/noOfSR;
                    travellerInstance.indivisualPrice = indivisualSRValue*exchangeRate;
                }else if ([travellerInstance.travellerRoomingType isEqualToString:kTravellerRoomTypeDR])
                {
                    float indivisualDrValue = (drValue/noOfDr*2);
                    travellerInstance.indivisualPrice = indivisualDrValue*exchangeRate;
                }
                else if ([travellerInstance.travellerRoomingType isEqualToString:kTravellerRoomTypeTR])
                {
                    float indivisualTrValue = (trValue/noOfTr*3);
                    travellerInstance.indivisualPrice = indivisualTrValue*exchangeRate;
                }
                else if ([travellerInstance.travellerRoomingType isEqualToString:kTravellerRoomTypeCNB])
                {
                    travellerInstance.indivisualPrice = (cnbValue/noOfCnb) *exchangeRate;
                }
                else if ([travellerInstance.travellerRoomingType isEqualToString:kTravellerRoomTypeCWB])
                {
                    travellerInstance.indivisualPrice = (cwbValue/noOfCwb) *exchangeRate;
                }else if ([travellerInstance.travellerRoomingType isEqualToString:kTravellerRoomTypeInfant])
                {
                    travellerInstance.indivisualPrice = (infValue/noOfInf) *exchangeRate;
                }
                
            }
            else
            {
                UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Alert" message:[NSString stringWithFormat:@"Person combination is not allowed for room number %d",travellerInstance.travellerRoomNumber]  preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    [alertView dismissViewControllerAnimated:YES completion:nil];
                }];
                
                [alertView addAction:okAction];
                
                [self presentViewController:alertView animated:YES completion:nil];
                
                calculationDict = nil;
                return;
            }
        }
    }
}

-(BOOL)checkValidationForNonFITWithRoomType:(NSString *)roomtype
{
    NSArray *arrayForUSDandINR = [calculationDict valueForKey:@"priceList"];
    for (int i = 0; i < arrayForUSDandINR.count; i++)
    {
        NSDictionary *currencyDict = arrayForUSDandINR[i];
        int valueForRoomtype = [[currencyDict valueForKey:roomtype] intValue];
        if (valueForRoomtype == 0 || valueForRoomtype == -1)
        {
            return NO;
        }
    }
    
    return YES;
}


-(void)calculationOfBookingAmount
{
    int noOfChildrens = 0;
    int noOfAdults = 0;
    for (int i = 0; i<roomRecordArray.count; i++)
    {
        RoomsDataModel *dataModel = roomRecordArray[i];
        NSArray *childrentCount = dataModel.arrayChildrensData;
        int adultCount = dataModel.adultCount;
        noOfChildrens = noOfChildrens + (int)childrentCount.count;
        noOfAdults = noOfAdults + adultCount;
    }
    
    NSDictionary *bookingAmountDict =  self.packageDetail.dictBookingAmt;
    
    float childrenAmount = [[bookingAmountDict valueForKey:@"childAmt"] floatValue];
    float adultAmount = [[bookingAmountDict valueForKey:@"adultAmt"] floatValue];
    
    if ([[[bookingAmountDict valueForKey:@"amtType"] lowercaseString ] isEqualToString:@"amt"])
    {
        float totalAmount = (noOfAdults * adultAmount) + (noOfChildrens * childrenAmount);
        UILabel *labelForEasyPaymentCost1 = (UILabel *)[self.viewForButtonContainers viewWithTag:8];
        labelForEasyPaymentCost1.text = [NSString stringWithFormat:@"₹ %0.1f",totalAmount];
        bookingAmountForBookingSubmit = [NSString stringWithFormat:@"%f",totalAmount];
        bookingAmountString = [NSString stringWithFormat:@"%f",totalAmount];
    }
    else if ([[[bookingAmountDict valueForKey:@"amtType"] lowercaseString ] isEqualToString:@"percentage"])
    {
        float totalAdultsAmount = 0;
        float totalChildrenAmount = 0;
        for (int j = 0; j< travellerArrayForCalculation.count; j++)
        {
            TravellerInformationModel *travellerInfoModelInstance = travellerArrayForCalculation[j];
            if ([travellerInfoModelInstance.travellerType isEqualToString:kTravellerTypeAdult])
            {
                totalAdultsAmount = totalAdultsAmount + travellerInfoModelInstance.indivisualPrice;
            }
            else if ([travellerInfoModelInstance.travellerType isEqualToString:kTravellerTypeChild])
            {
                totalChildrenAmount = totalChildrenAmount + travellerInfoModelInstance.indivisualPrice;
            }
        }
        float advanceAdultAmount = 0.0;
        float advanceChildAmount = 0.0;
        
        if (adultAmount > 0.0 && childrenAmount > 0.0) {
            
            advanceAdultAmount = totalAdultsAmount * (adultAmount / 100);
            
            advanceChildAmount = totalChildrenAmount * (advanceChildAmount / 100);
            
        }
        
        float advanceAmount = advanceAdultAmount + advanceChildAmount;
        UILabel *labelForEasyPaymentCost1 = (UILabel *)[self.viewForButtonContainers viewWithTag:8];
        labelForEasyPaymentCost1.text = [NSString stringWithFormat:@"₹ %0.1f",advanceAmount];
        bookingAmountString = [NSString stringWithFormat:@"%f",advanceAmount];
        bookingAmountForBookingSubmit = [NSString stringWithFormat:@"%f",advanceAmount];
        
    }
    
    UIButton *buttonAdvancePayment  = (UIButton *)[_viewForButtonContainers viewWithTag:5];
    UIButton *buttonFullPayment  = (UIButton *)[_viewForButtonContainers viewWithTag:6];
    
    if ([[[bookingAmountDict valueForKey:@"bookingType"] lowercaseString] isEqualToString:@"partial"])
    {
        [buttonFullPayment setSelected:NO];
        buttonFullPayment.userInteractionEnabled = NO;
        [self onEasyPaymentButtonClicked:buttonAdvancePayment];
    }
    else if ([[[bookingAmountDict valueForKey:@"bookingType"] lowercaseString] isEqualToString:@"full"])
    {
        [buttonAdvancePayment setSelected:NO];
        buttonAdvancePayment.userInteractionEnabled = NO;
        [self onEasyPaymentButtonClicked:buttonFullPayment];
    }
    
}

-(void)percentageINRPriceForAdvancePayment
{
    int noOfSR = 0;
    int noOfDr = 0;
    int noOfTr = 0;
    int noOfCwb = 0;
    int noOfCnb = 0;
    int noOfInf = 0;
    
    for (int i = 0; i< travellerArrayForCalculation.count; i++)
    {
        TravellerInformationModel *travellerInfoModelInstance = travellerArrayForCalculation[i];
        NSString *travellerRoomingType = travellerInfoModelInstance.travellerRoomingType;
        
        if ([travellerRoomingType isEqualToString:kTravellerRoomTypeCNB])
        {
            noOfCnb = noOfCnb + 1;
        }else if ([travellerRoomingType isEqualToString:kTravellerRoomTypeCWB])
        {
            noOfCwb = noOfCwb + 1;
        }else if ([travellerRoomingType isEqualToString:kTravellerRoomTypeSR])
        {
            noOfSR = noOfSR + 1;
        }
        else if ([travellerRoomingType isEqualToString:kTravellerRoomTypeDR])
        {
            noOfDr = noOfDr + 1 ;
        }
        else if ([travellerRoomingType isEqualToString:kTravellerRoomTypeTR])
        {
            noOfTr = noOfTr + 1 ;
        }
        else if ([travellerRoomingType isEqualToString:kTravellerRoomTypeInfant])
        {
            noOfInf = noOfInf + 1 ;
        }
        
    }
    
    NSArray *arrayForUSDandINR = [calculationDict valueForKey:@"priceList"];
    
    NSPredicate *predicateStringINR = [NSPredicate predicateWithFormat:@"%K == %@", @"currency", @"INR"];
    NSArray *arrayINR = [arrayForUSDandINR filteredArrayUsingPredicate:predicateStringINR];
    
    NSDictionary *INRDict;
    if (arrayINR.count != 0)
    {
        INRDict = arrayINR[0];
    }
    float srValue = [[INRDict valueForKey:@"sr"] floatValue];
    float drValue = [[INRDict valueForKey:@"dr"] floatValue];
    float trValue = [[INRDict valueForKey:@"tr"] floatValue];
    //float qrValue = [[INRDict valueForKey:@"qr"] floatValue];
    float cnbValue = [[INRDict valueForKey:@"cnb"] floatValue];
    float cwbValue = [[INRDict valueForKey:@"cwb"] floatValue];
    float infValue = [[INRDict valueForKey:@"inf"] floatValue];
    float exchangeRate = [[INRDict valueForKey:@"exRate"] floatValue];
    
    if (INRDict.count != 0)
    {
        
        for (int index = 0; index < travellerArrayForCalculation.count ; index++)
        {
            TravellerInformationModel *travellerInstance = travellerArrayForCalculation[index];
            if ([travellerInstance.travellerRoomingType isEqualToString:kTravellerRoomTypeSR])
            {
                float indivisualSRValue = srValue/noOfSR;
                travellerInstance.indivisualPrice = indivisualSRValue*exchangeRate;
            }else if ([travellerInstance.travellerRoomingType isEqualToString:kTravellerRoomTypeDR])
            {
                float indivisualDrValue = (drValue/noOfDr*2);
                travellerInstance.indivisualPrice = indivisualDrValue*exchangeRate;
            }
            else if ([travellerInstance.travellerRoomingType isEqualToString:kTravellerRoomTypeTR])
            {
                float indivisualTrValue = (trValue/noOfTr*3);
                travellerInstance.indivisualPrice = indivisualTrValue*exchangeRate;
            }
            else if ([travellerInstance.travellerRoomingType isEqualToString:kTravellerRoomTypeCNB])
            {
                travellerInstance.indivisualPrice = cnbValue *exchangeRate;
            }
            else if ([travellerInstance.travellerRoomingType isEqualToString:kTravellerRoomTypeCWB])
            {
                travellerInstance.indivisualPrice = cwbValue *exchangeRate;
            }else if ([travellerInstance.travellerRoomingType isEqualToString:kTravellerRoomTypeInfant])
            {
                travellerInstance.indivisualPrice = infValue *exchangeRate;
            }
        }
    }
}

-(void)calculationForPackageForFIT
{
  /*  NSArray *arrayForUSDandINR  = [calculationDict valueForKey:@"priceList"];
    NSPredicate *predicateStringUSD = [NSPredicate predicateWithFormat:@"%K != %@", @"currency", @"INR"];
    NSArray *arrayUSD = [arrayForUSDandINR filteredArrayUsingPredicate:predicateStringUSD];
    nonINRArray = [[NSMutableArray alloc]initWithArray:arrayUSD];
    
    arrayForALLCurrency = [[NSArray alloc]initWithArray:arrayForUSDandINR];
    
    float totalCost = 0.0;
    float totalCostForInr = 0.0;
    
    totalCostForInr = totalCost;
    
    NSMutableArray *arrayNonInrModified = [[NSMutableArray alloc]init];
    
    for (int i = 0; i<arrayForUSDandINR.count; i++)
    {
        NSDictionary *dictForCurrency = arrayForUSDandINR[i];
        NSMutableDictionary *nonInrDict = [[NSMutableDictionary alloc]initWithDictionary:dictForCurrency];
        float totalPerCurrency = 0.0;
        float totalWoExchangeRate = 0.0;
        for (int j = 0; j< travellerArrayForCalculation.count; j++)
        {
            TravellerInformationModel *travellerInstance = travellerArrayForCalculation[j];
            float indivisualValue = [self getValueForNonInrComponentWithTravellerInstance:travellerInstance WithCalcDict:nonInrDict];
            float exchangeRate =  [[nonInrDict valueForKey:@"exRate"] floatValue];
            totalPerCurrency = totalPerCurrency + (indivisualValue * exchangeRate);
            totalWoExchangeRate = totalWoExchangeRate +  indivisualValue;
        }
        
        totalCost = totalCost + totalPerCurrency;
        [nonInrDict setValue:[NSString stringWithFormat:@"%0.2f",totalWoExchangeRate] forKey:@"total"];
        [arrayNonInrModified addObject:nonInrDict];
    }
    
    
    float taxValue = round((totalCost*taxPercentage)/100);
    self.lblTotalCost.text = [NSString stringWithFormat:@"₹ %.1f",round(totalCost)];
    self.lblServiceTax.text = [NSString stringWithFormat:@"₹ %.1f",round(taxValue)];
    self.lblTotalPriceINCGST.text = [NSString stringWithFormat:@"₹ %.1f",round(taxValue+totalCost)];
    
    if (arrayNonInrModified.count == 0)
    {
        nonINRArray = [[NSMutableArray alloc]init];
    }
    else
    {
        nonINRArray = [[NSMutableArray alloc]initWithArray:arrayNonInrModified];
        
    }
    
    _constraintGapINRCompanent.constant = 0;
    
    
    UILabel *labelForEasyPaymentCost2 = (UILabel *)[self.viewForButtonContainers viewWithTag:10];
    labelForEasyPaymentCost2.text = [NSString stringWithFormat:@"₹ %.1f",round(taxValue+totalCost)];
    self.packageDetail.stringAmountINRFIT = [NSString stringWithFormat:@"%f",totalCost];
    totalPackageAmountString = [NSString stringWithFormat:@"%f",round(taxValue+totalCost)];
    [_tableNonInrComponent reloadData];
    self.constraintNonInrTableHeight.constant = (nonINRArray.count * 43);
    [self.view setNeedsDisplay];
    
    [self getDetailsWithPackageType:@"yes" WithPrice:[NSString stringWithFormat:@"%f",totalCost]];*/
    
    
    //Astra Logic
    /*Advance :
    Booking Amount – advancePayableAmount
    Total Price – tourCost – discountAdvancePrice + totalServicetaxAdvanceDiscount
    
    Full :
    Booking Amount – tourCost – discountFullPrice + totalServicetaxFullDiscount
    Total Price – tourCost – discountFullPrice + totalServicetaxFullDiscount*/
    
    _constraintGapINRCompanent.constant = 0;

    nonINRArray = [calculationDict valueForKey:@"currencyBreakup"];

    double tourCost = [[calculationDict valueForKey:@"tourCost"] doubleValue];

    double discountAdvancePrice = [[calculationDict valueForKey:@"discountAdvancePrice"] doubleValue];
    
    double advancePayable = [[calculationDict valueForKey:@"advancePayableAmount"] doubleValue];
    
    double totalServicetaxAdvanceDiscount = [[calculationDict valueForKey:@"totalServicetaxAdvanceDiscount"] doubleValue];


    UILabel *labelForEasyPaymentCost2 = (UILabel *)[self.viewForButtonContainers viewWithTag:10];
    
    labelForEasyPaymentCost2.text = [NSString stringWithFormat:@"₹ %.1f",round(tourCost - discountAdvancePrice + totalServicetaxAdvanceDiscount )];
    
    self.lblTotalPriceINCGST.text = [NSString stringWithFormat:@"₹ %.1f",round(tourCost - discountAdvancePrice + totalServicetaxAdvanceDiscount)];

    
    UILabel *labelForEasyPaymentCost1 = (UILabel *)[self.viewForButtonContainers viewWithTag:8];
    labelForEasyPaymentCost1.text = [NSString stringWithFormat:@"₹ %0.1f",advancePayable];
    
    
    bookingAmountForBookingSubmit = [NSString stringWithFormat:@"%f",advancePayable];
    bookingAmountString = [NSString stringWithFormat:@"%f",advancePayable];

    totalPackageAmountString = [NSString stringWithFormat:@"%0.1f",tourCost];

    
    UIButton *buttonAdvancePayment  = (UIButton *)[_viewForButtonContainers viewWithTag:5];
    UIButton *buttonFullPayment  = (UIButton *)[_viewForButtonContainers viewWithTag:6];
    
    if (advancePayable == 0)
    {
        [buttonAdvancePayment setSelected:NO];
        buttonAdvancePayment.userInteractionEnabled = NO;
        [self onEasyPaymentButtonClicked:buttonFullPayment];
    }
    
    
    
    NSArray *gstDesc = [calculationDict valueForKey:@"gSTDescription"];
    
    double cgstValue = 0.00;
    
    double sgstValue = 0.00;
    
    double igstValue = 0.00;
    
    for (int i = 0; i < gstDesc.count; i++)
    {
        NSDictionary *gstDict = [gstDesc objectAtIndex:i];
        NSString *gstType = [gstDict valueForKey:@"gstType"];
        
        if ([[gstType lowercaseString] isEqualToString:@"cgst"])
         {
            cgstValue = [[gstDict valueForKey:@"gstAmount"] doubleValue];
    
         }
        else if ([[gstType lowercaseString] isEqualToString:@"sgst"])
        {
            sgstValue = [[gstDict valueForKey:@"gstAmount"] doubleValue];

        }
        else if ([[gstType lowercaseString] isEqualToString:@"igst"])
        {
            igstValue = [[gstDict valueForKey:@"gstAmount"] doubleValue];
        }
    }
    
    
    if ([[_textFieldState.text lowercaseString] isEqualToString:@"maharashtra"])
    {
        self.lblServiceTax.hidden = NO;
        self.labelTitleCgst.hidden = NO;
        self.labelTitleSgst.text = @"SGST Tax Amount";
        
        self.lblServiceTax.text = [NSString stringWithFormat:@"₹ %.1f",round(cgstValue)];
        self.labelValueSgst.text = [NSString stringWithFormat:@"₹ %.1f",round(sgstValue)];
        
    }
    else
    {
        self.lblServiceTax.hidden = YES;
        self.labelTitleCgst.hidden = YES;
        self.labelTitleSgst.text = @"IGST Tax Amount";
        self.labelValueSgst.text = [NSString stringWithFormat:@"₹ %.1f",round(igstValue)];
        
    }
    
    //tourCost
    
    self.lblTotalCost.text = [NSString stringWithFormat:@"₹ %.1f",(tourCost)];
    
    [_tableNonInrComponent reloadData];
    
    self.constraintNonInrTableHeight.constant = (nonINRArray.count * 43);
    
    }

-(float)getValueForNonInrComponentWithTravellerInstance:(TravellerInformationModel *)travellerInstance WithCalcDict:(NSDictionary *)calcDict
{
    NSString *travellerRoomingType = travellerInstance.travellerRoomingType;
    NSString *tsValue = [calcDict valueForKey:@"ts"];
    NSString *roomType = travellerRoomingType;
    NSString *currency = [calcDict valueForKey:@"currency"];
    int tsValueInt = [tsValue intValue];
    float indivisualValue = 0.0;
    
    if ([travellerRoomingType isEqualToString:kTravellerRoomTypeDR])
    {
        if (tsValueInt != 0)
        {
            roomType = kTravellerRoomTypeTS;
        }
    }
    
    //    if ([packageType isEqualToString:kpackageTypeFITDomestic]||[packageType isEqualToString:kpackageTypeGITDomestic])//if traveller child CNB age is 2 to 5
    //    {
    if ([travellerRoomingType isEqualToString:kTravellerRoomTypeCNB])
    {
        if (travellerInstance.travellerAge >=2 && travellerInstance.travellerAge<=4)
        {
            roomType = @"cnb2To5YrsPrice";
        }else
        {
            roomType = kTravellerRoomTypeCNB;
        }
    }
    //   }
    
    if (travellerInstance.travellerRoomNumber != 1)
    {
        if ([travellerRoomingType isEqualToString:kTravellerRoomTypeSR])
        {
            roomType = @"srOtherPrice";
        }
    }
    
    if ([self isValidRoomCombinationForNonMRPWithRoomingType:roomType withCalcDict:calcDict])//if room type 0 and -1 do not allowd
    {
        
        if ([currency isEqualToString:kCurrency_japanese_yen] || [currency isEqualToString:kCurrency_thai_baht] )
        {
            NSString *typeValue = [calcDict valueForKey:roomType];
            indivisualValue  = ([typeValue floatValue]/100);
        }
        else
        {
            NSString *typeValue = [calcDict valueForKey:roomType];
            indivisualValue = [typeValue floatValue] ;
        }
        
        return indivisualValue;
        
    }
    else
    {
        UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Alert"
                                                                           message:[NSString stringWithFormat:@"Person combination is not allowed for room number %d",travellerInstance.travellerRoomNumber]
                                                                    preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [alertView dismissViewControllerAnimated:YES completion:nil];
        }];
        
        [alertView addAction:okAction];
        
        [self presentViewController:alertView animated:YES completion:nil];
        
        calculationDict = nil;
        
        return 0.0;
        
        
    }
}

-(void)calculationForPackageForNONFIT
{
   /* NSArray *arrayForUSDandINR = [calculationDict valueForKey:@"priceList"];
    arrayForALLCurrency = arrayForUSDandINR;
    
    nonINRArray = [[NSMutableArray alloc]initWithArray:arrayForALLCurrency];
    
    
    float totalCost = 0.0;
    
    if (arrayForALLCurrency.count != 0)
    {
        for (int i = 0 ; i< arrayForALLCurrency.count; i++)
        {
            NSDictionary *nonINRDict = arrayForALLCurrency[i];
            NSString *totalValueForNonINR = [nonINRDict valueForKey:@"total"];
            NSString *exchangeRateForNonINR = [nonINRDict valueForKey:@"exRate"];
            float totalCalculatedValue = ( [totalValueForNonINR floatValue] * [exchangeRateForNonINR floatValue]);
            totalCost = totalCost + totalCalculatedValue;
        }
        _lblPriceINR.alpha = 1;
        _lblINRTourComponentTitle.alpha = 1;
        _labelSaperatorINRGap.alpha = 1;
        _constraintGapINRCompanent.constant = 17;
    }
    else
    {
        _lblPriceINR.alpha = 0;
        _lblINRTourComponentTitle.alpha = 0;
        _labelSaperatorINRGap.alpha = 0;
        _constraintGapINRCompanent.constant = 0;
    }
    
    float taxValue = round((totalCost * taxPercentage)/100);
    self.lblTotalCost.text = [NSString stringWithFormat:@"₹ %.1f",round(totalCost)];
    self.lblServiceTax.text = [NSString stringWithFormat:@"₹ %.1f",round(taxValue)];
    self.lblTotalPriceINCGST.text = [NSString stringWithFormat:@"₹ %.1f",round(taxValue+totalCost)];
    UILabel *labelForEasyPaymentCost2 = (UILabel *)[self.viewForButtonContainers viewWithTag:10];
    labelForEasyPaymentCost2.text = [NSString stringWithFormat:@"₹ %.1f",round(taxValue+totalCost)];
    totalPackageAmountString = [NSString stringWithFormat:@"%f",round(taxValue+totalCost)];
    [_tableNonInrComponent reloadData];
    self.constraintNonInrTableHeight.constant = ( nonINRArray.count * 43);
    [self.view layoutIfNeeded];
    
    [self getDetailsWithPackageType:@"no" WithPrice:[NSString stringWithFormat:@"%f",totalCost]];*/
    
    
//    Advance :
//    Booking Amount – advancePayableAmount
//    Total Price – totalPrice
//    
//    Full :
//    Booking Amount – totalPrice
//    Total Price – totalPrice
    
    
    _constraintGapINRCompanent.constant = 0;
    
    nonINRArray = [calculationDict valueForKey:@"currencyBreakup"];
    
    double tourCost = [[calculationDict valueForKey:@"totalPrice"] doubleValue];
    
    double advancePayable = [[calculationDict valueForKey:@"advancePayableAmount"] doubleValue];

    
    UILabel *labelForEasyPaymentCost2 = (UILabel *)[self.viewForButtonContainers viewWithTag:10];
    
    labelForEasyPaymentCost2.text = [NSString stringWithFormat:@"₹ %.1f",round(tourCost )];
    
    self.lblTotalPriceINCGST.text = [NSString stringWithFormat:@"₹ %.1f",round(tourCost)];
    
    
    NSArray *gstDesc = [calculationDict valueForKey:@"gSTDescription"];
    
    double cgstValue = 0.00;
    
    double sgstValue = 0.00;
    
    double igstValue = 0.00;
    
    for (int i = 0; i < gstDesc.count; i++)
    {
        NSDictionary *gstDict = [gstDesc objectAtIndex:i];
        NSString *gstType = [gstDict valueForKey:@"gstType"];
        
        if ([[gstType lowercaseString] isEqualToString:@"cgst"])
        {
            cgstValue = [[gstDict valueForKey:@"gstAmount"] doubleValue];
            
        }
        else if ([[gstType lowercaseString] isEqualToString:@"sgst"])
        {
            sgstValue = [[gstDict valueForKey:@"gstAmount"] doubleValue];
            
        }
        else if ([[gstType lowercaseString] isEqualToString:@"igst"])
        {
            igstValue = [[gstDict valueForKey:@"gstAmount"] doubleValue];
        }
    }
    
    if ([[_textFieldState.text lowercaseString] isEqualToString:@"maharashtra"])
    {
        self.lblServiceTax.hidden = NO;
        self.labelTitleCgst.hidden = NO;
        self.labelTitleSgst.text = @"SGST Tax Amount";
        
        self.lblServiceTax.text = [NSString stringWithFormat:@"₹ %.1f",round(cgstValue)];
        self.labelValueSgst.text = [NSString stringWithFormat:@"₹ %.1f",round(sgstValue)];
        
    }
    else
    {
        self.lblServiceTax.hidden = YES;
        self.labelTitleCgst.hidden = YES;
        self.labelTitleSgst.text = @"IGST Tax Amount";
        self.labelValueSgst.text = [NSString stringWithFormat:@"₹ %.1f",round(igstValue)];
        
    }
    
    double tourCostWithoutGST = [[calculationDict valueForKey:@"tourCost"] doubleValue];
    
    self.lblTotalCost.text = [NSString stringWithFormat:@"₹ %.1f",(tourCostWithoutGST)];
    
    
    UILabel *labelForEasyPaymentCost1 = (UILabel *)[self.viewForButtonContainers viewWithTag:8];
    labelForEasyPaymentCost1.text = [NSString stringWithFormat:@"₹ %0.1f",advancePayable];
    bookingAmountForBookingSubmit = [NSString stringWithFormat:@"%f",advancePayable];
    
    bookingAmountString = [NSString stringWithFormat:@"%f",advancePayable];
    totalPackageAmountString = [NSString stringWithFormat:@"%0.1f",tourCost];

    UIButton *buttonAdvancePayment  = (UIButton *)[_viewForButtonContainers viewWithTag:5];
    UIButton *buttonFullPayment  = (UIButton *)[_viewForButtonContainers viewWithTag:6];
    
    if (advancePayable  == 0)
    {
        [buttonAdvancePayment setSelected:NO];
        buttonAdvancePayment.userInteractionEnabled = NO;
        [self onEasyPaymentButtonClicked:buttonFullPayment];
    }
    

    [_tableNonInrComponent reloadData];
    
    self.constraintNonInrTableHeight.constant = (nonINRArray.count * 43);
    
    
    
}
- (IBAction)onTermsConditionButtonClicked:(id)sender {
    
    TermsAndCondViewController *termsConditionVC = [[TermsAndCondViewController alloc]initWithNibName:@"BaseViewController" bundle:nil];
    
    NSArray *termsConditionArray = packageDetail.arrayTermsAndConditions;
    
    NSDictionary *termsAndConditionDict ;
    
    for (int i = 0; i<termsConditionArray.count; i++)
    {
        NSDictionary *termsDict = [ termsConditionArray objectAtIndex:i];
        
        NSInteger pkgClassID = [[termsDict  valueForKey:@"packageClassId"] integerValue];
        
        if (accomType == pkgClassID)
        {
            termsAndConditionDict = termsDict;
        }
        
    }
    
    NSMutableString *htmlString =  [[NSMutableString alloc] init];
    
    
    if (termsAndConditionDict)
    {
        NSString *cancellationPolicy = [termsAndConditionDict valueForKey:@"cancellationPolicy"];
        NSString *paymentTerms = [termsAndConditionDict valueForKey:@"paymentTerms"];
        
        [htmlString appendFormat:@"<p class=\"p1\">&nbsp;</p> <p class=\"p1\"><span class=\"s1\">Cancellation Policy</span></p> <p class=\"p1\">&nbsp;</p>"];
        [htmlString appendString:cancellationPolicy];
        
         [htmlString appendFormat:@"<p class=\"p1\">&nbsp;</p> <p class=\"p1\"><span class=\"s1\">Payment Terms</span></p> <p class=\"p1\">&nbsp;</p>"];
        [htmlString appendString:paymentTerms];
        
        NSArray *tcilHolidayTermsConditionsCollection = [termsAndConditionDict valueForKey:@"tcilHolidayTermsConditionsCollection"];
        
        if (tcilHolidayTermsConditionsCollection)
        {
            if (tcilHolidayTermsConditionsCollection.count != 0)
            {
                
                for (int i = 0; i<tcilHolidayTermsConditionsCollection.count; i++)
                {
                    NSDictionary *conditionDict = [tcilHolidayTermsConditionsCollection objectAtIndex:i];
                    
                     [htmlString appendFormat:@"<p class=\"p1\">&nbsp;</p> <p class=\"p1\"><span class=\"s1\">Description</span></p> <p class=\"p1\">&nbsp;</p>"];
                    
                    NSString *descriptionString  = [conditionDict valueForKey:@"description"];
                       
                    [htmlString appendString:descriptionString];
                    
                }
            }
        }
    }
    
    
    
    termsConditionVC.requestUrlString = htmlString;
    [[SlideNavigationController sharedInstance] pushViewController:termsConditionVC animated:NO];
    
}

- (IBAction)onCallButtonCalled:(id)sender
{
    if ([self.packageDetail.strPackageType caseInsensitiveCompare:@"international"]==NSOrderedSame)
    {
        [FIRAnalytics logEventWithName:@"International_Holidays_Call_Clicked_PDP" parameters:@{kFIRParameterItemName:self.packageDetail.strPackageName}];
        [FIRAnalytics logEventWithName:@"International_Holidays_Call_Clicked_PDP" parameters:@{kFIRParameterItemName:self.packageDetail.strPackageType}];
    }
    else
    {
        [FIRAnalytics logEventWithName:@"Indian_Holidays_Call_Clicked_PDP" parameters:@{kFIRParameterItemName:self.packageDetail.strPackageName}];
        
        [FIRAnalytics logEventWithName:@"Indian_Holidays_Call_Clicked_PDP" parameters:@{kFIRParameterItemName:self.packageDetail.strPackageType}];

    }
    
    CustomIOSAlertView *alertView = [[CustomIOSAlertView alloc] init];
    CoreUtility *coreObj=[CoreUtility sharedclassname];
    coreObj.statusWhichPopUpCalled=0;
    [alertView setContainerView:[self createDemoView]];
    
    
    // Modify the parameters
    [alertView setButtonTitles:[NSMutableArray arrayWithObjects:@"Close1", @"Close2", @"Close3", nil]];
    [alertView setDelegate:self];
    
    // You may use a Block, rather than a delegate.
    [alertView setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
        NSLog(@"Block: Button at position %d is clicked on alertView %d.", buttonIndex, (int)[alertView tag]);
        [alertView close];
    }];
    
    [alertView setUseMotionEffects:true];
    
    // And launch the dialog
    [alertView show];
}

- (IBAction)onEmailButtonClicked:(id)sender
{
    if ([self.packageDetail.strPackageType caseInsensitiveCompare:@"International"]==NSOrderedSame)
    {
        [FIRAnalytics logEventWithName:@"International_Holidays_Email_PDP" parameters:@{kFIRParameterItemName:self.packageDetail.strPackageName}];
        [FIRAnalytics logEventWithName:@"International_Holidays_Email PDP" parameters:@{kFIRParameterItemName:self.packageDetail.strPackageType}];
        [FIRAnalytics logEventWithName:@"International_Holidays_Email_PDP" parameters:@{kFIRParameterItemName:self.packageDetail.strPackageId}];
    }
    else
    {
        [FIRAnalytics logEventWithName:@"Indian_Holidays_Email_PDP" parameters:@{kFIRParameterItemName:self.packageDetail.strPackageName}];
        [FIRAnalytics logEventWithName:@"Indian_Holidays_Email_PDP" parameters:@{kFIRParameterItemName:self.packageDetail.strPackageType}];
        [FIRAnalytics logEventWithName:@"Indian_Holidays_Email_PDP" parameters:@{kFIRParameterItemName:self.packageDetail.strPackageId}];

    }
    
    
    RequestForCallPopUp *ReqViewController = [[RequestForCallPopUp alloc] initWithNibName:@"RequestForCallPopUp" bundle:nil];
    ReqViewController.packageDetail = self.packageDetail;
    ReqViewController.view.frame = CGRectMake(0, 0, 300, self.bookNowView.frame.size.height - 100);
    [self presentPopupViewController:ReqViewController animationType:MJPopupViewAnimationFade];
}

#pragma mark - Pop VIew For Call Us

- (void)customIOS7dialogButtonTouchUpInside: (CustomIOSAlertView *)alertView clickedButtonAtIndex: (NSInteger)buttonIndex
{
    NSLog(@"Delegate: Button at position %d is clicked on alertView %d.", (int)buttonIndex, (int)[alertView tag]);
    [alertView close];
}

- (UIView *)createDemoView
{
    UIView *demoView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 290, 200)];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 30, 30)];
    [imageView setImage:[UIImage imageNamed:@"customer_support.png"]];
    UILabel *lblCallus=[[UILabel alloc]initWithFrame:CGRectMake(50, 10, 200, 40)];
    //Old Contact Number
    //lblCallus.text=@"1800 2099 100";
    
    //New Contact Number
    lblCallus.text=  @"8652908370"; //@"18002000464,7039003560"; //new mobile number 4th Nov 2019
    
    [demoView addSubview:lblCallus];
    [demoView addSubview:imageView];
    
    
    return demoView;
}

#pragma mark NetCore LogEvents

//APP_CALCULATE_CONTINUE

-(void)netCoreInternationalHolidayCalculateContinue
{
    int noOfChild = 0;
    int noOfInf = 0;
    int noOfAdult = 0;
    
    for (int i = 0; i< travellerArrayForCalculation.count; i++)
    {
        TravellerInformationModel *travellerInfoModelInstance = travellerArrayForCalculation[i];
        NSString *travellerRoomingType = travellerInfoModelInstance.travellerType;
        
        if ([travellerRoomingType isEqualToString:kTravellerTypeChild])
        {
            noOfChild = noOfChild + 1;
        }
        else if ([travellerRoomingType isEqualToString:kTravellerTypeAdult])
        {
            noOfAdult = noOfAdult + 1;
        }
        else if ([travellerRoomingType isEqualToString:kTravellerTypeInfant])
        {
            noOfInf = noOfInf + 1;
        }
    }

    
    NSString *paymentOption = @"";
    if ([bookingAmountForBookingSubmit isEqualToString:bookingAmountString])
    {
        paymentOption = @"Booking Amount";
    }
    else
    {
         paymentOption = @"Total Amount";
    }
    
    
    NSMutableDictionary *payloadList =  [NSMutableDictionary dictionary];
    [payloadList setObject:packageType forKey:@"s^PACKAGE_TYPE"];
    [payloadList setObject:[NSNumber numberWithInteger:roomRecordArray.count] forKey:@"i^ROOM"];
    [payloadList setObject:[NSNumber numberWithInteger:noOfAdult] forKey:@"i^ADULTS"];
    [payloadList setObject:[NSNumber numberWithInteger:noOfChild] forKey:@"i^CHILDREN"];
    [payloadList setObject:[NSNumber numberWithInteger:noOfInf] forKey:@"i^INFANTS"];
    [payloadList setObject:[NSString stringWithFormat:@"%@",self.txtSelectCity.text] forKey:@"s^DEPARTURE_CITY"];
    [payloadList setObject:[NSString stringWithFormat:@"%@", self.packageDetail.strPackageId] forKey:@"s^PACKAGE_ID"];
    [payloadList setObject:paymentOption forKey:@"s^PAYMENT_OPTION"];
   // [payloadList setObject:bookingAmountForBookingSubmit forKey:@"s^PAYMENT_AMOUNT"];//28-02-2018
    [payloadList setObject:[NSNumber numberWithInteger:[bookingAmountForBookingSubmit integerValue]] forKey:@"i^PAYMENT_AMOUNT"];//changed s^PAYMENT_AMOUNT to i^PAYMENT_AMOUNT : 28-02-2018
    [payloadList setObject:@"App" forKey:@"s^SOURCE"];
    
    if(packageDetail.strPackageType != nil && ![packageDetail.strPackageType isEqualToString:@""])
    {
        [payloadList setObject:packageDetail.strPackageType forKey:@"s^TYPE"]; //uncommented on  28-02-2018
    }
    
   // [NetCoreAnalyticsVC getNetCorePushNotificationDetailDictionary:payloadList withPayloadCount:109]; //28-02-2018
    [NetCoreAnalyticsVC getNetCorePushNotificationDetailDictionary:payloadList withPayloadCount:129]; //28-02-2018
    NSLog(@"Data submitted to the netcore:129");
}

//APP_CALCULATE
-(void)netCoreInternationalHolidayCalculate
{
    
    int noOfChild = 0;
    int noOfInf = 0;
    int noOfAdult = 0;
    
    for (int i = 0; i< travellerArrayForCalculation.count; i++)
    {
        TravellerInformationModel *travellerInfoModelInstance = travellerArrayForCalculation[i];
        NSString *travellerRoomingType = travellerInfoModelInstance.travellerType;
        
        if ([travellerRoomingType isEqualToString:kTravellerTypeChild])
        {
            noOfChild = noOfChild + 1;
        }
        else if ([travellerRoomingType isEqualToString:kTravellerTypeAdult])
        {
            noOfAdult = noOfAdult + 1;
        }
        else if ([travellerRoomingType isEqualToString:kTravellerTypeInfant])
        {
            noOfInf = noOfInf + 1;
        }
    }
    
    
    
    NSMutableDictionary *payloadList =  [NSMutableDictionary dictionary];
    [payloadList setObject:packageType forKey:@"s^PACKAGE_TYPE"];
    [payloadList setObject:[NSNumber numberWithInteger:roomRecordArray.count] forKey:@"i^ROOM"];
    [payloadList setObject:[NSNumber numberWithInteger:noOfAdult] forKey:@"i^ADULTS"];
    [payloadList setObject:[NSNumber numberWithInteger:noOfChild] forKey:@"i^CHILDREN"];
    [payloadList setObject:[NSNumber numberWithInteger:noOfInf] forKey:@"i^INFANTS"];
    [payloadList setObject:[NSString stringWithFormat:@"%@",self.txtSelectCity.text] forKey:@"s^DEPARTURE_CITY"];
    [payloadList setObject:[NSString stringWithFormat:@"%@", self.packageDetail.strPackageId] forKey:@"s^PACKAGE_ID"];
    [payloadList setObject:@"App" forKey:@"s^SOURCE"];
    [payloadList setObject:packageDetail.strPackageType forKey:@"s^TYPE"];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    NSDate *oldFormatDate =  [dateFormatter dateFromString:_txtSelectDate.text];
    
    
    NSDateFormatter *dateFormatterNew = [[NSDateFormatter alloc] init];
    [dateFormatterNew setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *newDateString = [dateFormatterNew stringFromDate:oldFormatDate];
    // NSDate *newDate = [dateFormatterNew dateFromString:newDateString];
    
    [payloadList setObject:newDateString forKey:@"d^DATE"]; //YYYY-MM-DD HH:MM:SS
    [payloadList setObject:_textFieldState.text forKey:@"s^STATE"];
    
    
    [NetCoreAnalyticsVC getNetCorePushNotificationDetailDictionary:payloadList withPayloadCount:128];
}

#pragma mark - astra integration -

-(void)preconfirmationBookingAstra
{
    
    @try
    {
        
  activityLoadingView = [LoadingView loadingViewInView:self.view.superview.superview
                                                  withString:@""
                                           andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];

    NSDictionary *headerDict = [CoreUtility getHeaderDict];
    
    NetworkHelper *helper = [NetworkHelper sharedHelper];
    
    NSString *queryParam = [NSString stringWithFormat:@"quotationId=%@",[calculationDict valueForKey:@"quotationId"]];
    
    [helper getResponseWithRequestType:@"GET" withQueryParam:queryParam withPathParam:@"" withJsonParam:nil withHeaders:headerDict withUrl:kAstraUrlPreConfirmationBooking success:^(NSDictionary *responseDict)
     {
         NSLog(@"Response Dict : %@",responseDict);
         
         dispatch_async(dispatch_get_main_queue(), ^(void)
                        {
                            [activityLoadingView removeFromSuperview];
                            if (responseDict)
                            {
                                if (responseDict.count>0)
                                {
                                    
                            PassangerDetailViewController *passangerVC = [[PassangerDetailViewController alloc]initWithNibName:@"BaseViewController" bundle:nil];
                                   passangerVC.DictJourneyDate = dictForDate ;
                                   passangerVC.gstTaxDetails = gstTaxDetails;
                                   passangerVC.stateCode = selectedStateCode;
                                   passangerVC.travellerArrayForCalculation = travellerArrayForCalculation;
                                   passangerVC.hubName = departFrom;
                                   passangerVC.bookingAmountString = bookingAmountForBookingSubmit;
                                   passangerVC.totalAmountString = totalPackageAmountString;
                                   passangerVC.accomType = accomType;
                                   passangerVC.roomRecordArray = roomRecordArray;
                                   passangerVC.packageDetail = self.packageDetail;
                                   passangerVC.currencyArray = arrayForALLCurrency;
                                   passangerVC.selectedStateDict = selectedStateDict;
                                   passangerVC.dictForCalculation = calculationDict;
                                   passangerVC.amountType = amountType;
                                   [[SlideNavigationController sharedInstance] pushViewController:passangerVC animated:YES];
                        
                                    
                                }else{
                                    [activityLoadingView removeFromSuperview];
                                    [super showAlertViewWithTitle:@"Alert" withMessage:@"some error occurred"];
                                }
                            }else{
                                [activityLoadingView removeFromSuperview];
                                [super showAlertViewWithTitle:@"Alert" withMessage:@"some error occurred"];
                                   
                            }
                        });
     }
                               failure:^(NSError *error)
     {
         dispatch_async(dispatch_get_main_queue(), ^(void)
                     {
                         [activityLoadingView removeFromSuperview];
                         [super showAlertViewWithTitle:@"Alert" withMessage:@"some error occurred"];
                         NSLog(@"Response Dict : %@",error);
                     });
         
     }];
    }
    @catch (NSException *exception)
    {
        NSLog(@"%@", exception.reason);
        [activityLoadingView removeFromSuperview];
    }
    @finally
    {
        NSLog(@"exception finally called");
    }
    
}
-(void)bookingTypeSubmission{
    NSDictionary *headerDict = [CoreUtility getHeaderDict];
    
    NetworkHelper *helper = [NetworkHelper sharedHelper];
    
    NSString *userid = [[NSUserDefaults standardUserDefaults] valueForKey:kuserDefaultUserId];
    
      NSMutableDictionary *jsonDict = [[NSMutableDictionary alloc] init];
    [jsonDict setValue:[calculationDict valueForKey:@"quotationId"] forKey:@"quotationId"];
    [jsonDict setValue:amountType forKey:@"bookingAmountType"];
    [jsonDict setValue:bookingAmountString forKey:@"bookingAmount"];
    [jsonDict setValue:@"" forKey:@"promoCode"];
    [jsonDict setValue:userid forKey:@"userId"];
    [jsonDict setValue:@"Desktop" forKey:@"appUsed"];
    
    
    [helper getResponseWithRequestType:@"POST" withQueryParam:@"" withPathParam:@"" withJsonParam:jsonDict withHeaders:headerDict withUrl:kAstraUrlPromocode success:^(NSDictionary *responseDict)
    {
        NSLog(@"Response Dict : %@",responseDict);
        
        dispatch_async(dispatch_get_main_queue(), ^(void)
                       {
                           [activityLoadingView removeFromSuperview];
                           
                           if (responseDict)
                           {
                               if (responseDict.count>0)
                               {
                                   NSLog(@"responseDict-->%@",responseDict);
                                   if ([[[responseDict valueForKey:@"message"] uppercaseString] isEqualToString:@"SUCCESS"]){
                                       
                                       [self preconfirmationBookingAstra];
                                   }else
                                   {
                                       [super showAlertViewWithTitle:@"Alert" withMessage:@"some error occurred"];
                                       
                                   }
                                   
                               }
                               else
                               {
                                   [super showAlertViewWithTitle:@"Alert" withMessage:@"some error occurred"];
                                   
                               }
                           }
                           else
                           {
                               [super showAlertViewWithTitle:@"Alert" withMessage:@"some error occurred"];
                               
                           }
                       });
        
    }
                              failure:^(NSError *error)
    {dispatch_async(dispatch_get_main_queue(), ^(void)
                    {
                        [activityLoadingView removeFromSuperview];
                        [super showAlertViewWithTitle:@"Alert" withMessage:@"some error occurred"];
                        NSLog(@"Response Dict : %@",error);
                    });
        
    }];
  
    
    
}

-(void)calculatePriceAstra
{
    /* 
     "{
     ""mode"":""TCIL"",
     ""ltMarket"":""-1"", ""continent"":""Asia"",""isHsa"":""N"",
     ""room"":[{""roomNo"":1,""noAdult"":2,""noCwb"":0,""noCnbJ"":0,""noCnbS"":0,""inf"":0,""pax"":2}],
     ""optionalsActivities"":[], : Non mandatory
     ""pkgId"":""PKG001424"",
     ""pkgSubType"":2,
     ""pkgSubClass"":""2"",
     ""hub"":""IDR"",
     ""hubCity"":""Indore"",
     ""departureDate"":""23-10-2017"",
     ""ltProdCode"":"""",
     ""userMobileNo"":""9964434636"",
     ""userEmailId"":""aa@hh.ll"",
     ""enquirySource"":""Direct"", : GA - Non mandatory
     ""enquiryMedium"":""Direct"", : GA - Non mandatory
     ""enquiryCampaign"":""not set"", : GA - Non mandatory
     ""LP"":""/"", : GA - Non mandatory
     ""bookURL"":""/holidays/jal-mahotsav?pkgId=PKG001424&packageClas"", : GA - Non mandatory
     ""ltItineraryCode"":""-1"",
     ""regionId"":2,
     ""crmEnquiryId"":"""", Non mandatory
     ""crmStatus"":""N"",
     ""custState"":""27"",
     ""custStateName"":""Maharashtra"",
     ""isUnionTerritory"":""N"",
     ""isGstApplicable"":true
     }"
     */
    
    
    @try
    {
    
    activityLoadingView = [LoadingView loadingViewInView:self.view.superview.superview
                                              withString:@""
                                       andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];

    
    NSMutableArray *arrayForRooming = [[NSMutableArray alloc] init];
    for (int i = 0; i<roomRecordArray.count; i++)
    {
        
        int numberOfAdult = 0;
        int numberOfCwb = 0;
        int numberOfCnbJ = 0;
        int numberOfCnbS = 0;
        int numberOfInfants = 0;
        int totalPax = 0;
        
        
        RoomsDataModel *roomsModelInsatance = roomRecordArray[i];
        numberOfAdult = roomsModelInsatance.adultCount;
        numberOfInfants = roomsModelInsatance.infantCount;
        NSArray *childArray = roomsModelInsatance.arrayChildrensData;
  //      int childWithCNBCount = 0;
    //    NSMutableArray *childDictArray = [[NSMutableArray alloc]init];
      //  int childWithCWBCount = 0;
        
        for (int j = 0; j<childArray.count; j++)
        {
            NSDictionary *childDict = childArray[j];
            UIButton *button = [childDict valueForKey:@"button"];
            UILabel *labelAge = [childDict valueForKey:@"textField"];
            
          //  NSDictionary *dictForChild ;
            if ([button isSelected]) {
                
                //childWithCWBCount = childWithCWBCount + 1;
                //dictForChild = [[NSMutableDictionary alloc] initWithObjectsAndKeys:labelAge.text,@"CWB", nil];
                numberOfCwb ++;

            }
            else
            {
                //childWithCNBCount = childWithCNBCount + 1;
                //dictForChild = [[NSMutableDictionary alloc] initWithObjectsAndKeys:labelAge.text,@"CNB", nil];
                
                if ([packageType isEqualToString:kpackageTypeGITInternational]||[packageType isEqualToString:kpackageTypeFITInternational])
                {
                    numberOfCnbS ++;
                }
                else
                {
                    if ([labelAge.text intValue] < 5)
                    {
                        numberOfCnbJ ++;
                    }
                    else
                    {
                        numberOfCnbS ++;
                    }
                    
                }
            }
           // [childDictArray addObject:dictForChild];
        }
        
        
        totalPax = numberOfCnbS + numberOfCnbJ + numberOfAdult + numberOfInfants + numberOfCwb;
        
        
        
        
//        NSPredicate *pred = [NSPredicate predicateWithBlock:^BOOL(TravellerInformationModel* evaluatedObject, NSDictionary *bindings) {
//            
//            
//            if (evaluatedObject.travellerRoomNumber == i+1 )
//            {
//                return true;
//            }
//            else
//            {
//                return false;
//            }
//            
//        }]; 

      /*  NSArray *results = [travellerArrayForCalculation filteredArrayUsingPredicate:pred];
        

        for (int j = 0; j<results.count; j++)
        {
            totalPax ++;
            TravellerInformationModel * modelPassanger = results[j];
            
            if ([modelPassanger.travellerType isEqualToString:kTravellerTypeAdult])
            {
                numberOfAdult++;
            }
            
            if ([modelPassanger.travellerType isEqualToString:kTravellerTypeInfant])
            {
                numberOfInfants++;
            }
            
            if ([modelPassanger.travellerType isEqualToString:kTravellerTypeChild])
            {
                
                if ([modelPassanger.travellerRoomingType isEqualToString:kTravellerRoomTypeCNB])
                {
                     if ([packageType isEqualToString:kpackageTypeGITInternational]||[packageType isEqualToString:kpackageTypeFITInternational])
                    {
                        numberOfCnbS ++;
                    }
                     else
                     {
                         if (modelPassanger.travellerAge < 5)
                         {
                             numberOfCnbJ ++;
                         }
                         else
                         {
                             numberOfCnbS ++;
                         }

                     }
                    
                }
                else
                {
                    numberOfCwb ++;
                }
            }
        }*/
        
        NSMutableDictionary *roomDict = [[NSMutableDictionary alloc] init];
        [roomDict setObject:[NSNumber numberWithInt:i+1] forKey:@"roomNo"];
        [roomDict setObject:[NSNumber numberWithInt:numberOfAdult] forKey:@"noAdult"];
        [roomDict setObject:[NSNumber numberWithInt:numberOfCwb] forKey:@"noCwb"];
        [roomDict setObject:[NSNumber numberWithInt:numberOfCnbJ] forKey:@"noCnbJ"];
        [roomDict setObject:[NSNumber numberWithInt:numberOfCnbS] forKey:@"noCnbS"];
        [roomDict setObject:[NSNumber numberWithInt:numberOfInfants] forKey:@"inf"];
        [roomDict setObject:[NSNumber numberWithInt:totalPax] forKey:@"pax"];

        [arrayForRooming addObject:roomDict];
        
    }

    
    CoreUtility *coreobj=[CoreUtility sharedclassname];
    NSString *phoneNumber = @"";
    
    if (coreobj.strPhoneNumber.count != 0)
    {
        phoneNumber = coreobj.strPhoneNumber[0];
    }
    
    NSString *emailIDString = [[NSUserDefaults standardUserDefaults] objectForKey:kLoginEmailId];

    
    NSMutableDictionary *jsonDict = [[NSMutableDictionary alloc] init];
        
    [jsonDict setObject:packageDetail.packageMode forKey:@"mode"];
    
   // [jsonDict setObject:packageDetail.ltMarket forKey:@"ltMarket"];
    [jsonDict setObject:@"" forKey:@"ltMarket"];
    
    [jsonDict setObject:packageDetail.packageContinent forKey:@"continent"];
    [jsonDict setObject:packageDetail.isHSA forKey:@"isHsa"];
    [jsonDict setObject:[NSArray new] forKey:@"optionalsActivities"];
    [jsonDict setObject:arrayForRooming forKey:@"room"];
    [jsonDict setObject:packageDetail.strPackageId forKey:@"pkgId"];
    [jsonDict setObject:[NSNumber numberWithInteger:[packageDetail.strPackageSubTypeID integerValue]] forKey:@"pkgSubType"];
    [jsonDict setObject:[NSString stringWithFormat:@"%d",accomType] forKey:@"pkgSubClass"]; //fit git
    [jsonDict setObject:[selectedHubDict valueForKey:@"cityCode"] forKey:@"hub"];
    [jsonDict setObject:[selectedHubDict valueForKey:@"cityName"] forKey:@"hubCity"];
    [jsonDict setObject:[dictForDate valueForKey:@"DATE"] forKey:@"departureDate"];
    [jsonDict setObject:[dictForDate valueForKey:@"LT_PROD_CODE"] forKey:@"ltProdCode"];
    [jsonDict setObject:phoneNumber forKey:@"userMobileNo"];
    [jsonDict setObject:@"" forKey:@"userEmailId"];

    if (emailIDString)
    {
        [jsonDict setObject:emailIDString forKey:@"userEmailId"];
    }
    
    [jsonDict setObject:@"Direct" forKey:@"enquirySource"];//
    [jsonDict setObject:@"Direct" forKey:@"enquiryMedium"];//
    [jsonDict setObject:@"" forKey:@"enquiryCampaign"];//
    [jsonDict setObject:@"" forKey:@"LP"];//
    [jsonDict setObject:@"" forKey:@"bookURL"];//
    //[jsonDict setObject:packageDetail.ltItineraryCode forKey:@"ltItineraryCode"];
    [jsonDict setObject:[dictForDate valueForKey:@"PROD_ITIN_CODE"] forKey:@"ltItineraryCode"];
        
    NSArray *farecaLtItineraryCodeArray = [[NSArray alloc] initWithObjects:[dictForDate valueForKey:@"PROD_ITIN_CODE"], nil];
        
    [jsonDict setObject:farecaLtItineraryCodeArray forKey:@"farecaLtItineraryCode"];
    
    
    [jsonDict setObject:@"" forKey:@"regionId"]; //
        
    [jsonDict setObject:packageDetail.productID forKey:@"productId"];
    
    
    if ([packageDetail.strPackageType isEqualToString:@"domestic"])
    {
        [jsonDict setObject:@"2" forKey:@"regionId"];
    }
    else
    {
        if ([[packageDetail.packageContinent uppercaseString] isEqualToString:@"AMERICA"]||[[packageDetail.packageContinent uppercaseString] isEqualToString:@"SOUTH AMERICA"]||[[packageDetail.packageContinent uppercaseString] isEqualToString:@"NORTH AMERICA"] ||[[packageDetail.packageContinent uppercaseString] isEqualToString:@"AFRICA"])
        {
            [jsonDict setObject:@"1" forKey:@"regionId"];
        }
        else
        {
            [jsonDict setObject:@"0" forKey:@"regionId"];
        }
    }


    [jsonDict setObject:@"" forKey:@"crmEnquiryId"]; //
    [jsonDict setObject:@"" forKey:@"crmStatus"];//?? - empty
    
    [jsonDict setObject:[selectedStateDict valueForKey:@"gstStateCode"] forKey:@"custState"];
    [jsonDict setObject:[selectedStateDict valueForKey:@"gstState"] forKey:@"custStateName"];
    [jsonDict setObject:[selectedStateDict valueForKey:@"isUnionTerritory"] forKey:@"isUnionTerritory"];
    
    [jsonDict setObject:@"true" forKey:@"isGstApplicable"]; //? ?
    
    NSDictionary *headerDict = [CoreUtility getHeaderDict];
    
    NetworkHelper *helper = [NetworkHelper sharedHelper];
    
    [helper getResponseWithRequestType:@"POST" withQueryParam:@"" withPathParam:@"" withJsonParam:jsonDict withHeaders:headerDict withUrl:kAstraUrlPricing success:^(NSDictionary *responseDict)
     {
         NSLog(@"Response Dict : %@",responseDict);
         
         dispatch_async(dispatch_get_main_queue(), ^(void)
                        {
                            [activityLoadingView removeFromSuperview];
                            
                            if (responseDict)
                            {
                                if (responseDict.count>0)
                                {
                                    NSDictionary *pricingDict = responseDict;
                                    
                                    calculationDict = pricingDict;
                                    
                                    if (calculationDict!=nil && calculationDict.count!=0)
                                    {
                                        
                                        NSString *messege = [calculationDict valueForKey:@"message"];
                                        
                                        if (messege == nil )
                                        {
                                            
                                             double tourCost = [[calculationDict valueForKey:@"tourCost"] doubleValue];
                                            
                                            if (tourCost != 0)
                                            {
                                                if([[self.packageDetail.strPackageSubType lowercaseString] isEqualToString:@"fit"])
                                                {
                                                    [self calculationForPackageForFIT];
                                                }
                                                else
                                                {
                                                    [self calculationForPackageForNONFIT];
                                                }
                                                
                                                
                                                if (packageDetail.pkgStatusId != 1 || [[self.packageDetail.stringIsOnReq lowercaseString] isEqualToString:@"y"])
                                                {
                                                    _viewForPaymentDetails.hidden = NO;
                                                    // _constraintTermsConditions.constant = 150;
                                                    _viewBookingMessege.hidden = NO;
                                                    _constraintHeightButtonContainer.constant = 0;
                                                    self.constraintCalculateOffset.constant = 170 + self.viewForPaymentDetails.frame.size.height;
                                                    self.buttonCalculate.alpha = 0;
                                                    _labelForPaymentOptions.hidden = YES;
                                                    _viewForButtonContainers.hidden = NO;
                                                    
                                                }
                                                else
                                                {
                                                    
                                                    _constraintTermsConditions.constant = 41;
                                                    _viewBookingMessege.hidden = YES;
                                                    self.buttonCalculate.alpha = 1;
                                                    [_buttonCalculate setTitle:@"Continue" forState:UIControlStateNormal];
                                                    _constraintHeightButtonContainer.constant = 89;
                                                   // self.constraintCalculateOffset.constant = 190 + self.viewForPaymentDetails.frame.size.height;
                                                    
                                                    self.constraintCalculateOffset.constant = self.constraintNonInrTableHeight.constant + 127+ 190;
                                                    _labelForPaymentOptions.hidden = NO;
                                                    _viewForPaymentDetails.hidden = NO;
                                                    _viewForButtonContainers.hidden = NO;
                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^(void) {
                                                          [self.view layoutIfNeeded];
            
                                                    });
                                                    
                                                }
                                                
                                                [UIView animateWithDuration:0.4 animations:^{
                                                    
                                                    [self.view layoutIfNeeded];
                                                }];
                                            }
                                            else
                                            {
                                                [super showAlertViewWithTitle:@"Alert" withMessage:@"Price not available for this package"];

                                            }

                                        }
                                        else
                                        {
                                            [super showAlertViewWithTitle:@"Alert" withMessage:messege];
                                        }
                                            
                                        

                                    }
                                    
                                }
                                else
                                {
                                    [super showAlertViewWithTitle:@"Alert" withMessage:@"some error occurred"];
                                    
                                }
                            }
                            else
                            {
                                [super showAlertViewWithTitle:@"Alert" withMessage:@"some error occurred"];
                                
                            }
                        });
         
     }
                               failure:^(NSError *error)
     {dispatch_async(dispatch_get_main_queue(), ^(void)
                     {
                         [activityLoadingView removeFromSuperview];
                         [super showAlertViewWithTitle:@"Alert" withMessage:@"some error occurred"];
                         NSLog(@"Response Dict : %@",error);
                     });
         
     }];
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"%@", exception.reason);
        [activityLoadingView removeFromSuperview];
    }
    @finally
    {
        NSLog(@"exception finally called");
    }
    
}

@end
