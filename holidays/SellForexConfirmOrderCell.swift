//
//  SellForexConfirmOrderCell.swift
//  holidays
//
//  Created by Komal Katkade on 11/30/17.
//  Copyright © 2017 Mobicule. All rights reserved.
//

import UIKit

class SellForexConfirmOrderCell: UITableViewCell {

    @IBOutlet weak var labelProductNo: UILabel!
    @IBOutlet weak var labelINR: UILabel!
    @IBOutlet weak var labelCurrency: UILabel!
    @IBOutlet weak var labelProduct: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
