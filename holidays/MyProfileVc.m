//
//  MyProfileVCViewController.m
//  holidays
//
//  Created by Designer on 19/12/15.
//  Copyright © 2015 Mobicule. All rights reserved.
//

#import "MyProfileVc.h"
#import "KLCPopup.h"
#import "LoginViewPopUp.h"
#import "UIViewController+MJPopupViewController.h"
#import "MJPopupBackgroundView.h"
#import "SearchHolidaysVC.h"
#import "OrderSummaryVC.h"
#import "MyFavouriteVC.h"
typedef void(^myCompletion)(BOOL);

@interface MyProfileVc ()
{
    KLCPopup *customePopUp;
    
}
@end

@implementation MyProfileVc

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    [[NSBundle mainBundle]loadNibNamed:@"MyProfileVc" owner:self options:nil];
    [super addViewInBaseView:self.loginView];
   // LoginViewPopUp *loginObject=[LoginViewP;
    
    NSUserDefaults *LoginStatus=[NSUserDefaults standardUserDefaults];
    NSString *statusForLogin= [LoginStatus valueForKey:kLoginStatus];
    
    NSUserDefaults *userIdForGooglePlusSignIn=[NSUserDefaults standardUserDefaults];
    NSString *userId=[userIdForGooglePlusSignIn valueForKey:kLoginEmailId];
    NSString *phoneNo=[userIdForGooglePlusSignIn valueForKey:kPhoneNo];
    
    
    if ([statusForLogin isEqualToString:kLoginSuccess])
    {
        _profileViewAfterLogin.hidden=NO;
        _emailId.text=userId;
        _labelPhoneNumber.text = phoneNo;
    }
    else
    {
     _profileViewAfterLogin.hidden=YES;
    }
    super.HeaderLabel.text=_headerName;
    
    
//    CoreUtility *coreobj=[CoreUtility sharedclassname];
//    
//    if (coreobj.strPhoneNumber.count != 0)
//    {
//        NSString *phoneNumber = coreobj.strPhoneNumber[0];
//        if (phoneNumber != nil && ![phoneNumber isEqualToString:@""])
//        {
//            self.labelPhoneNumber.text = phoneNumber;
//        }
//    }

    
//    if(_IsMyProfileLoginSuccess)
//        _profileViewAfterLogin.hidden=NO;
//     [self.view bringSubviewToFront:];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
   // NSUserDefaults *setUserDefault=[NSUserDefaults standardUserDefaults];
    
//    if([[setUserDefault objectForKey:@"AfterProfileLoginSuccessfully"] isEqualToString:@"login"])
//    {
//        _profileViewAfterLogin.hidden=NO;
//    }
    
}


- (IBAction)actionOnBtnMyFavourites:(id)sender {
    
    
    MyFavouriteVC *favouriteVC=[[MyFavouriteVC alloc] initWithNibName:@"BaseViewController" bundle:nil];
    favouriteVC.headerName=@"My Favourite";
    [[SlideNavigationController sharedInstance]pushViewController:favouriteVC animated:YES];

    
    
}

- (IBAction)actionOnFavouriteSearchBtn:(id)sender {
    
    SearchHolidaysVC *searchVC=[[SearchHolidaysVC alloc]initWithNibName:@"BaseViewController" bundle:nil];
    searchVC.boolForSearchPackgScreen=true;
    
    [[SlideNavigationController sharedInstance] pushViewController:searchVC animated:YES];

}

- (IBAction)actionOnMyBookingBtn:(id)sender {
    
    OrderSummaryVC *orderVC=[[OrderSummaryVC alloc]initWithNibName:@"BaseViewController" bundle:nil];
    orderVC.headerName = kOrderSummary;
    [[SlideNavigationController sharedInstance]pushViewController:orderVC animated:YES];
}

- (IBAction)actionForLogin:(id)sender {
    
   /* LoginViewPopUp *loginView = [[LoginViewPopUp alloc] initWithNibName:@"LoginViewPopUp" bundle:nil];
   // loginView.delegate=self;
    loginView.view.frame = CGRectMake(0, 0, 300, self.loginView.frame.size.height - 100);
    [self presentPopupViewController:loginView animationType:MJPopupViewAnimationFade];*/
    
    NewLoginPopUpViewController * loginView = [[NewLoginPopUpViewController alloc] initWithNibName:@"NewLoginPopUpViewController" bundle:nil];
    //[self.navigationController pushViewController:loginView animated:true];
    UINavigationController * loginNavigation = [[UINavigationController alloc] initWithRootViewController:loginView];
    
   
    
    [self presentViewController:loginNavigation animated:true completion:^{
        
    }];
   
    
}
-(void) myMethod:(myCompletion) compblock
{
       compblock(YES);
}
- (void) alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    _profileViewAfterLogin.hidden=NO;
}

-(void)dismissLoginPopUp{

 
    NSUserDefaults *LoginStatus=[NSUserDefaults standardUserDefaults];
    NSString *statusForLogin= [LoginStatus valueForKey:kLoginStatus];
    
    NSUserDefaults *userIdForGooglePlusSignIn=[NSUserDefaults standardUserDefaults];
    NSString *userId=[userIdForGooglePlusSignIn valueForKey:kLoginEmailId];
    NSString *phoneNo=[userIdForGooglePlusSignIn valueForKey:kPhoneNo];
    
    if ([statusForLogin isEqualToString:kLoginSuccess]) {
        _profileViewAfterLogin.hidden=NO;
        _emailId.text=userId;
        _labelPhoneNumber.text = phoneNo;
    }

}

@end
