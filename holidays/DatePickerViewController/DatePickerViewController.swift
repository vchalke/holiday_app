//
//  DatePickerViewController.swift
//  sotc-consumer-application
//
//  Created by darshan on 25/09/18.
//  Copyright © 2018 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit
protocol DatePickerViewControllerDelegate:class {
    func date(_ controller: DatePickerViewController, text: String)
}

class DatePickerViewController: UIViewController
{
    @IBOutlet weak var viewofField: UIView!
    @IBOutlet weak var datepicker: UIDatePicker!
    weak var delegate:DatePickerViewControllerDelegate?
    var datetype:String?
    var travellertype:String?
    override func viewDidLoad() {
        
        super.viewDidLoad()
        viewofField.roundedCorners()
        viewofField.fadedShadow()
        if datetype == "birthdate"
        {
            if travellertype == "ADULT" {
                datepicker.maximumDate =  Calendar.current.date(byAdding: .year, value: -12, to: Date())
            }else if travellertype == "CNB" || travellertype == "CWB"{
                datepicker.maximumDate =  Calendar.current.date(byAdding: .year, value: -2, to: Date())
                datepicker.minimumDate =  Calendar.current.date(byAdding: .year, value: -12, to: Date())
            }else if travellertype == "INFANT"{
                datepicker.maximumDate =  Calendar.current.date(byAdding: .year, value: 0, to: Date())
                datepicker.minimumDate =  Calendar.current.date(byAdding: .year, value: -2, to: Date())
            }else{
                datepicker.maximumDate =  Calendar.current.date(byAdding: .year, value: 0, to: Date())
            }
            
        }else{
            datepicker.minimumDate =  Calendar.current.date(byAdding: .day, value: 0, to: Date())
        }
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func jonClickCancelBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func onClickDoneButton(_ sender: Any) {
       printLog(datepicker.date)
        if datetype == "birthdate"{
            let str = AppUtility.convertDateToString(date: datepicker.date, dateFormat: "dd-MM-yyyy")
            delegate?.date(self, text: str)
            self.dismiss(animated: true, completion: nil)
        }else{
            let str = AppUtility.convertDateToString(date: datepicker.date)
            delegate?.date(self, text: str)
            self.dismiss(animated: true, completion: nil)
        }
    }
    @IBOutlet weak var onClickCancelBtn: UIButton!
}
