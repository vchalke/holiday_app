//
//  OffersDetailsVC.m
//  holidays
//
//  Created by Kush_Team on 01/09/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "OffersDetailsVC.h"
#import "OffersCollectionObject.h"

@interface OffersDetailsVC ()
@property (weak, nonatomic) IBOutlet UITextView *txt_views;

@end

@implementation OffersDetailsVC

- (void)viewDidLoad {
    
//    if (self.holidayInOffers.tcilHolidayOffersCollection.count >0){
//        OffersCollectionObject *offerObj = [[OffersCollectionObject alloc]initWithCollectionObject:self.holidayInOffers.tcilHolidayOffersCollection[0]];
//        if([offerObj.isOfferVisible.uppercaseString isEqualToString:@"Y"]){
//
//        }else{
//
//        }
//    }
    if (self.holidayInOffers.Offers.length >0){
//        NSMutableAttributedString * string = [[NSMutableAttributedString alloc] init];
//        [string appendAttributedString:[[NSAttributedString alloc]initWithString:[NSString stringWithFormat:@"  •    %@\n\n",self.holidayInOffers.Offers]]];
        NSAttributedString * string = [[NSAttributedString alloc] initWithData:[self.holidayInOffers.Offers dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
        self.txt_views.text = string.string;
//        [self.txt_views sizeToFit];
    }else{
        self.txt_views.text = @"";
    }
    
}

- (IBAction)btn_CrossPress:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
