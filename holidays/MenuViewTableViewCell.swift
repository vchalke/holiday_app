//
//  MenuViewTableViewCell.swift
//  sotc-consumer-application
//
//  Created by Saurav Kumar on 25/09/17.
//  Copyright © 2017 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit

class MenuViewTableViewCell: UITableViewCell {

 
    @IBOutlet var deviationButton: UIButton!
    @IBOutlet var menuDetailIcon: UIImageView!
    @IBOutlet var menuItemTitleLabel: UILabel!
    @IBOutlet var menuItemImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
