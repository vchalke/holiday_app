//
//  MyFavouriteVC.h
//  holidays
//
//  Created by ROHIT on 28/12/15.
//  Copyright © 2015 Mobicule. All rights reserved.
//

#import "BaseViewController.h"
//@protocol dismissLoginPopUp;
@protocol DismissNewLoginPopUp;
@interface MyFavouriteVC : BaseViewController<UITableViewDelegate,UITableViewDataSource,DismissNewLoginPopUp/*dismissLoginPopUp*/>{
     LoadingView *activityIndicator;
    
}
@property (strong, nonatomic) IBOutlet UIView *MyFavouriteView;
@property (weak, nonatomic) IBOutlet UITableView *tableviewForMyFav;
@property (weak, nonatomic) IBOutlet UIView *viewForBeforeLogin;

@property (weak,nonatomic) NSString *headerName;
- (IBAction)actionForLogin:(id)sender;

@end
