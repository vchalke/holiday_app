//
//  ManageHolidayPopUpVC.swift
//  holidays
//
//  Created by Parshwanath on 17/04/18.
//  Copyright © 2018 Mobicule. All rights reserved.
//

import UIKit;

@objc public protocol ManageHolidayPopUpVCDelegate:NSObjectProtocol {
    @objc func onContinue()
     @objc func onCancle()
}

class  ManageHolidayPopUpVC: UIViewController,UIWebViewDelegate{

    
    @IBOutlet weak var webview: UIWebView!
    @IBOutlet weak var titleLabel: UILabel!
    var htmlPath : String = ""
    var popuptitle : String = ""
   weak var managedHolidayDelegate:ManageHolidayPopUpVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.webview.scrollView.bounces = false
        titleLabel.text = popuptitle
        //        webview.loadRequest(URLRequest(url: URL(fileURLWithPath: Bundle.main.path(forResource: htmlPath, ofType: "html")!)))
        
        LoadingIndicatorView.show()
        
        let urlString = Bundle.main.url(forResource: htmlPath, withExtension: "html")
        webview.loadRequest(URLRequest(url: urlString!))
    }

    override func didReceiveMemoryWarning() {
           LoadingIndicatorView.hide()
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
     public func webViewDidStartLoad(_ webView: UIWebView)
    {
        
    }
    
   
     public func webViewDidFinishLoad(_ webView: UIWebView)
     {
           LoadingIndicatorView.hide()
    }
    
   
     public func webView(_ webView: UIWebView, didFailLoadWithError error: Error)
     {
                   LoadingIndicatorView.hide()
        
    }

    @IBAction func onCancelBtnClick(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
       self.managedHolidayDelegate?.onCancle()
    }
    
    @IBAction func onContinueBtnClick(_ sender: UIButton) {
     self.dismiss(animated: false, completion: nil)
        self.managedHolidayDelegate?.onContinue()
    }
    
    @IBAction func closeBtnClicked(_ sender: Any)
    {
        self.dismiss(animated: false, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
