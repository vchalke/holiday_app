//
//  AccomodationObject.h
//  holidays
//
//  Created by Kush_Tech on 09/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface AccomodationObject : NSObject
-(instancetype)initWithAccomodationObjectDict:(NSDictionary *)dictionary;
-(instancetype)initWithAccomodationObjectOtherDict:(NSDictionary *)dictionary;
@property(nonatomic,strong)NSString *hotelName;
@property(nonatomic,strong)NSString *country;
@property(nonatomic,strong)NSString *state;
@property(nonatomic)NSInteger starRating;
@end

NS_ASSUME_NONNULL_END
