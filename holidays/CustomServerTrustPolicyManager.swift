//
//  CustomServerTrustPolicyManager.swift

import UIKit

class CustomServerTrustPolicyManager: ServerTrustPolicyManager
{
    override func serverTrustPolicy(forHost host: String) -> ServerTrustPolicy?
    {
        // Check if we have a policy already defined, otherwise just kill the connection
        if let policy = super.serverTrustPolicy(forHost: host)
        {
            return policy
        }
        else
        {
            return .customEvaluation({ (_, _) -> Bool in
                return false
            })
        }
    }
}
