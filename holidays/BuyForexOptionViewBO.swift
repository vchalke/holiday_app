//
//  BuyForexOptionViewBO.swift
//  holidays
//
//  Created by Komal Katkade on 12/8/17.
//  Copyright © 2017 Mobicule. All rights reserved.
//

import Foundation

class BuyForexOptionsViewBO
{
    var purposeOfTravel : NSString = ""
    var noOfTraveller : NSInteger = 0
    var travelDate : NSDate?
    var contactDetail :  String = ""
    var cardNumber:String = ""
    init()
    {
        self.purposeOfTravel = ""
        self.noOfTraveller = 0
      
        self.contactDetail = ""
    }
    
}
