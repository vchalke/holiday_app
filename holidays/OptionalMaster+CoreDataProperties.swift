//
//  OptionalMaster+CoreDataProperties.swift
//  holidays
//
//  Created by Saurav on 28/11/17.
//  Copyright © 2017 Mobicule. All rights reserved.
//

import Foundation
import CoreData


extension OptionalMaster {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<OptionalMaster> {
        return NSFetchRequest<OptionalMaster>(entityName: "OptionalMaster")
    }

    @NSManaged public var adultSP: String?
    @NSManaged public var bfNumber: String?
    @NSManaged public var childSP: String?
    @NSManaged public var infantSP: String?
    @NSManaged public var optionalCode: String?
    @NSManaged public var optionalCode_bfNumber: String?
    @NSManaged public var optionalCurrency: String?
    @NSManaged public var optionalDescription: String?
    @NSManaged public var tourRelation: Tour?

}
