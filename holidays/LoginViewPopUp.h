//
//  LoginViewPopUp.h
//  holidays
//
//  Created by ketan on 10/11/15.
//  Copyright © 2015 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FirebaseAuth/FirebaseAuth.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "SlideMenuViewController.h"
#import "SignInViewController.h"
#import <GoogleSignIn/GoogleSignIn.h>
#import "LoadingView.h"

@class GIDSignInButton;

@protocol dismissLoginPopUp;

@interface LoginViewPopUp : UIViewController<GoogleSignInViewDelegate,GIDSignInDelegate,GIDSignInUIDelegate> //, FBSDKLoginButtonDelegate>
{
    LoadingView *activityIndicator;
    KLCPopup *customePopUp;
}

- (IBAction)onAlreadyMemberClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *buttonAlreadyMember;
- (IBAction)onSignInButtonClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *buttonSignIn;
- (IBAction)onGuestUserButtonClicked:(id)sender;
- (IBAction)onRegisterUserButtonClicked:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintPassword;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintReenterPass;
- (IBAction)onForgotPasswordClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *textFieldUserId;
@property (weak, nonatomic) IBOutlet UITextField *textFieldPassword;
@property (weak, nonatomic) IBOutlet UITextField *textFieldReEnterPassword;
@property (weak, nonatomic) IBOutlet UIScrollView *loginScrollView;
@property (strong, nonatomic) IBOutlet UIView *loginView;
@property (weak, nonatomic) IBOutlet UIButton *btnLoginWithGooglePlus;


//@property (strong, nonatomic) IBOutlet FBSDKLoginButton *btnLoginWithFacebook;

- (IBAction)onLoginWithFacebookButtonClicked:(id)sender;
- (IBAction)onLoginWithGooglePlusButtonClicked:(id)sender;
+ (LoginViewPopUp *)sharedInstance;
@property(strong,nonatomic) id object;
@property(strong,nonatomic) NSString *loginStatus;
@property (strong,nonatomic) id <dismissLoginPopUp> delegate;
@property (strong, nonatomic) IBOutlet UIImageView *loginImageView;
@property (strong, nonatomic) IBOutlet UIImageView *passwordImageView;

@property (assign,getter=isForGuestUser) BOOL isForGuestUser;

- (IBAction)onLoginWithGooglePlus:(id)sender;
 

@property(strong) NSString * userEmailID ;



@end

@protocol dismissLoginPopUp<NSObject>
@optional
- (void)dismissLoginPopUp;


@end

