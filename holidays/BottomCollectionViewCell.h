//
//  BottomCollectionViewCell.h
//  holidays
//
//  Created by Kush_Tech on 24/02/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BottomCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *img_view;

@property (weak, nonatomic) IBOutlet UILabel *lbl_title;
@end

NS_ASSUME_NONNULL_END
