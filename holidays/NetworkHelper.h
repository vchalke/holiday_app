//
//  NetworkHelper.h
//  holidays
//
//  Created by vaibhav on 18/09/15.
//  Copyright (c) 2015 Mobicule. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetworkHelper : NSObject

+(NetworkHelper *)sharedHelper;
@property(strong,nonatomic)NSMutableArray * pinnedCertificates;
-(NSString *)getDataFromServerForType:(NSString *)type entity:(NSString *)entity action:(NSString *)action andUserJson:(NSDictionary *)userJSon;
-(NSString *)getDataFromServerForType1:(NSString *)type entity:(NSString *)entity action:(NSString *)action andUserJson:(NSDictionary *)userJSon withCompletionHandler:(void (^)(NSString* resultString))handler;
-(void)getResponseWithRequestType:(NSString *)requestType withQueryParam:(NSString *)queryParam withPathParam:(NSString *)pathParam withJsonParam:(NSDictionary *)jsonParam withHeaders:(NSDictionary *)headersDict withUrl:(NSString *)urlForRequest success:(void (^)(NSDictionary *responseDict))success failure:(void(^)(NSError* error))failure;

@end
