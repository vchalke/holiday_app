//
//  ItineraryDetailsView.h
//  holidays
//
//  Created by Kush Thakkar on 22/02/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ItineraryDetailsView : UIView

@property (weak, nonatomic) IBOutlet UIButton *cancelButton;

@property (weak, nonatomic) IBOutlet UITextView *itineraryDetailTextView;

@end

NS_ASSUME_NONNULL_END
