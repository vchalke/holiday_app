//
//  LoginCommunicationManager.h
//  holidays
//
//  Created by ketan on 21/12/17.
//  Copyright © 2017 Mobicule. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>

@protocol loginCommunicationDelegate <NSObject>

@optional
- (void)guestLoggedInSuccessFullyWithUserID:(NSString *)userID;
-(void)UserloggedInSuccessFullWithUserDict:(NSDictionary *)userID;
-(void)loggedInFailWithRason:(NSString *)reasonMessege;

@end

@interface LoginCommunicationManager : NSObject
@property (weak,nonatomic) id <loginCommunicationDelegate> delegate;
@property (nonatomic,strong) UIViewController *loginViewController;
@property (nonatomic,strong) NSString *moduleName;
-(void)loginCheckWithType:(NSString *)signInType withUserID:(NSString *)userId withPassword:(NSString *)password;
-(void)socialLoginWithFirstName:(NSString *)firstName withLastName:(NSString *)lastName withImageURl:(NSString *)imageURL withType:(NSString *)type withUserID:(NSString *)userID withID:(NSString *)Id;
-(void)setUpFacebookLogin;

@end
