//
//  PendingTransactionViewController.swift
//  holidays
//
//  Created by ketan on 11/01/18.
//  Copyright © 2018 Mobicule. All rights reserved.
//

import UIKit

class PendingTransactionViewController: ForexBaseViewController,UITableViewDelegate,UITableViewDataSource,pendingPopUpDelegate {
   
    @IBOutlet weak var buttonBuy: UIButton!
    @IBOutlet weak var underlineLabelBuy: UILabel!
    @IBOutlet var pendingTransactionView: UIView!
    
    @IBOutlet weak var buttonCell: UIButton!
    
    @IBOutlet weak var underlineLabelSell: UILabel!
    
    @IBOutlet weak var buttonForexCard: UIButton!
    
    @IBOutlet weak var underlineLabelForexCard: UILabel!
    
    var arrayForSell : NSArray = []
    var arrayForBuy : NSArray = []
    var arrayForReload :NSArray = []
    var arrayForTable :NSArray = []
    var currentIndex: Int = 1;
    
    var mobileAndEmailString = NSString()
    var previousController : String = ""
    
    @IBOutlet weak var tableViewPendingTrans: UITableView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        Bundle.main.loadNibNamed("PendingTransactionViewController", owner: self, options: nil)
        
        super.addViewInBaseView(childView: self.pendingTransactionView)
        super.setUpHeaderLabel(labelHeaderNameText: "Pending Transaction")
        

        let pendingTrans :PendingTransactionPopUpViewController = PendingTransactionPopUpViewController(nibName: "PendingTransactionPopUpViewController", bundle: nil)
        pendingTrans.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext;
        pendingTrans.delegate = self
        pendingTrans.pendingTransactionViewController = self
        self.present(pendingTrans, animated: true, completion: nil)
    }
    
    override func backButtonClicked(buttonView:UIButton)
    {
        if self.previousController == "SlideMenuViewController"
        {
            SlideNavigationController.sharedInstance().popToRootViewController(animated: true)
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillLayoutSubviews()
    {
        super.setSubViewFrame(childView: self.pendingTransactionView)
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrayForTable.count;
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        //dateOfTravel
        var cell =  tableView.dequeueReusableCell(withIdentifier:"identifier")
        
        if cell == nil
        {
             cell = Bundle.main.loadNibNamed("PendingTransTableViewCell", owner: self, options: nil)?[0] as! PendingTransTableViewCell
        }
        
        let dict :NSDictionary = arrayForTable[indexPath.row] as! NSDictionary;
        
        let labelNoOfTraveller : UILabel  = cell?.viewWithTag(1) as! UILabel
        
        let labeldateOftravel : UILabel  = cell?.viewWithTag(2) as! UILabel

        labelNoOfTraveller.text = "\(dict.value(forKey: "totalNoOfTraveller") ?? "")"
        
        labeldateOftravel.text = dict.value(forKey: "dateOfTravel") as? String

        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 78
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        if currentIndex == 1 //|| currentIndex == 3
        {
            
            self.fetchProductList(didSelectRowAt: indexPath,moduleID: "1")
            
           /* let dictOfForex : NSDictionary = arrayForTable[indexPath.row] as! NSDictionary
            
            var buyForexModule : BuyForexBO = BuyForexBO()
            
            var buyForexOptionModule: BuyForexOptionsViewBO =  BuyForexOptionsViewBO()
            
            buyForexModule.emailID = dictOfForex .value(forKey: "bookerEmailId") as! String
            
            buyForexOptionModule.purposeOfTravel = ""
            buyForexOptionModule.noOfTraveller = dictOfForex.value(forKey: "totalNoOfTraveller") as! NSInteger
            
            //buyForexOptionModule.travelDate = dictOfForex.value(forKey: "dateOfTravel")
            buyForexOptionModule.contactDetail = dictOfForex.value(forKey: "bookerMobileNo") as! String
            
            let travellerArray :NSArray = dictOfForex.value(forKey: "tcilForexQuoteTravellerCollection") as! NSArray
            
            let arrayMutableForTraveller : NSMutableArray = NSMutableArray()
            
            let travelDateString: NSString! = dictOfForex.value(forKey: "dateOfTravel") as! NSString
            
            var traveldate:NSDate = NSDate()
            
            if(!travelDateString.isEqual(to: ""))
            {
                let dateFormater:DateFormatter = DateFormatter()
                dateFormater.dateFormat = "dd-MM-yyyy"
                if (dateFormater.date(from: travelDateString as String)) != nil
                {
                traveldate  = dateFormater.date(from: travelDateString as String)! as NSDate
                }
                else
                {
                     dateFormater.dateFormat = "yyyy-MM-dd HH:mm:ss Z"
                     traveldate  = dateFormater.date(from: travelDateString as String)! as NSDate
                     dateFormater.dateFormat = "dd-MM-yyyy"
                    let dateStr : String = dateFormater.string(from: traveldate as Date)
                    traveldate  = dateFormater.date(from: dateStr as String)! as NSDate
                }
                buyForexOptionModule.travelDate = traveldate as NSDate
            }
            buyForexModule.buyForexOptionViewDetails = buyForexOptionModule

            
            for var dictForTraveller in travellerArray
            {
                let arrayForProducts = (dictForTraveller as! NSDictionary).value(forKey: "tcilForexQuoteTravellerProductCollection") as! NSArray
                
                let travellerModule : TravellerBO = TravellerBO()
                //  travellerModule.travellerTitle = (dictForTraveller as! NSDictionary).value(forKey: "") as! String;
                // travellerModule.travellerFirstName = (dictForTraveller as! NSDictionary).value(forKey: "") as! String;
                //travellerModule.travellerLastName = (dictForTraveller as! NSDictionary).value(forKey: "") as! String;
                //travellerModule.travellerDateOfTravel = (dictForTraveller as! NSDictionary).value(forKey: "") as? NSDate;
                //travellerModule.travellerDateOfBirth = (dictForTraveller as! NSDictionary).value(forKey: "") as? NSDate;
                //travellerModule.travellerAadharNumber = (dictForTraveller as! NSDictionary).value(forKey: "") as! String;
                //travellerModule.travellerGSTINNumber = (dictForTraveller as! NSDictionary).value(forKey: "") as! String;
                
                /*
                 var travellerTitle : String = String()
                 
                 var travellerFirstName : String = String()
                 
                 var travellerLastName : String = String()
                 
                 var travellerDateOfTravel : NSDate? = NSDate()
                 
                 var travellerDateOfBirth : NSDate? = nil
                 
                 var travellerAadharNumber : String = String()
                 
                 var travellerGSTINNumber : String = String()
                 */
                
                
                for var dictForProduct in arrayForProducts
                {
                    // ProductBO
                    /*
                     var currency:NSString = "";
                     var productName:NSString = "";
                     var productID:NSInteger? = nil;
                     var fxAmount:NSInteger = 0;
                     var inrAmount:NSInteger = 0;
                     var isNewProduct :  Bool = true
                     var isUpdateButtonVisible = false
                     var currencyCode:NSString = ""
                     var roe : Float = 0
                     var isNostroAcc : NSString? = ""
                     var currencyID:NSInteger? = nil;
                     */
                    
                    let productModule: ProductBO = ProductBO()
                    productModule.currency = (dictForProduct as! NSDictionary).value(forKey: "currencyName") as! NSString
                    productModule.productName = (dictForProduct as! NSDictionary).value(forKey: "productName") as! NSString
                    productModule.productID = (dictForProduct as! NSDictionary).value(forKey: "productId") as? NSInteger
                    productModule.fxAmount = (dictForProduct as! NSDictionary).value(forKey: "amount") as! NSInteger
                    productModule.inrAmount = (dictForProduct as! NSDictionary).value(forKey: "equivalentInr") as! NSInteger
                    productModule.isNewProduct = false
                    productModule.isUpdateButtonVisible = false
                    productModule.currencyCode = (dictForProduct as! NSDictionary).value(forKey: "currencyCode") as! NSString
                    productModule.roe = (dictForProduct as! NSDictionary).value(forKey: "roe") as! Float
                    productModule.isNostroAcc = (dictForProduct as! NSDictionary).value(forKey: "nostroAvailable") as? NSString
                    productModule.currencyID = (dictForProduct as! NSDictionary).value(forKey: "currencyId") as? NSInteger
                    
                    travellerModule.addProduct(product: productModule)
                }
                arrayMutableForTraveller.add(travellerModule)
            }
            
            
            //let stateOfCustomer : NSString = dictOfForex.value(forKey: "") as! NSString
            
            var buyForexPassengerInfo : BuyForexPassengerInfoBO  = BuyForexPassengerInfoBO(travellerArray: arrayMutableForTraveller, stateOfCustomer: "", branchOfCustomer: "", totalAmount: 12, stateCode: "", branchCode: "", isUnionTerritoryState: "", isUnionTerritoryBranch: "", gstBranchCode: "")
            
            buyForexModule.buyForexPassengerInfo = buyForexPassengerInfo;
            // buyForexPassengerInfo.travellerArray = arrayMutableForTraveller
            
            if(currentIndex == 1)
            {
                let forexBuyPassangerVc: ForexBuyPassangerViewController = ForexBuyPassangerViewController(nibName: "ForexBaseViewController", bundle: nil)
                forexBuyPassangerVc.buyForexBo = buyForexModule
                
                forexBuyPassangerVc.previousScreen = "Pending Transaction"
                forexBuyPassangerVc.travellerArray = arrayMutableForTraveller as! [TravellerBO];
                self.navigationController?.pushViewController(forexBuyPassangerVc, animated: true)
            }
            else
            {
                
                let forexBuyPassangerVc: ReloadForexCardDetailsViewController = ReloadForexCardDetailsViewController(nibName: FOREX_BASE_VC, bundle: nil)
                
                let buyForexBo : BuyForexBO = buyForexModule
                let buyForexOptionViewDetails : BuyForexOptionsViewBO = BuyForexOptionsViewBO.init()
                buyForexOptionViewDetails.contactDetail = dictOfForex.value(forKey: "bookerMobileNo") as! String
                forexBuyPassangerVc.previousScreen = "Pending Transaction"
                buyForexOptionViewDetails.noOfTraveller = 1
                buyForexOptionViewDetails.travelDate = traveldate as NSDate
                buyForexOptionViewDetails.cardNumber = dictOfForex.value(forKey: "reloadCardNo") as! String;
                buyForexBo.buyForexOptionViewDetails = buyForexOptionViewDetails
                
                forexBuyPassangerVc.buyForexBo = buyForexBo
                forexBuyPassangerVc.travellerArray = arrayMutableForTraveller as! [TravellerBO];
                self.navigationController?.pushViewController(forexBuyPassangerVc, animated: true)

            }
            
            */

        }else if currentIndex == 3 {
            
             self.fetchProductList(didSelectRowAt: indexPath,moduleID: "3")
        }
        else if (currentIndex == 2)
        {
            let dictOfForex : NSDictionary = arrayForTable[indexPath.row] as! NSDictionary
            
            var sellForexModule : SellForexBO = SellForexBO()
            
            /*
             var productArray : [ProductBO] = []
             var mobileNumber : String = ""
             var customerState : String = ""
             var customerbranch : String = ""
             var gstStateCode : String = ""
             var branchCode : String = ""
             var gstBranchCode : String = ""
             var isUnionTerritoryState : String = ""
             var isUnionTerritoryBranch : String = ""

             */
            
            
            sellForexModule.emailID = dictOfForex .value(forKey: "bookerEmailId") as! String
            //buyForexOptionModule.travelDate = dictOfForex.value(forKey: "dateOfTravel")
            sellForexModule.mobileNumber = dictOfForex.value(forKey: "bookerMobileNo") as! String
            
            
            
            let travellerArray :NSArray = dictOfForex.value(forKey: "tcilForexQuoteTravellerCollection") as! NSArray
            
            
            var arrayMutableForTraveller : NSMutableArray = NSMutableArray()
            
            for var dictForTraveller in travellerArray
            {
                let arrayForProducts = (dictForTraveller as! NSDictionary).value(forKey: "tcilForexQuoteTravellerProductCollection") as! NSArray
                
                for var dictForProduct in arrayForProducts
                {
                    // ProductBO
                    /*
                     var currency:NSString = "";
                     var productName:NSString = "";
                     var productID:NSInteger? = nil;
                     var fxAmount:NSInteger = 0;
                     var inrAmount:NSInteger = 0;
                     var isNewProduct :  Bool = true
                     var isUpdateButtonVisible = false
                     var currencyCode:NSString = ""
                     var roe : Float = 0
                     var isNostroAcc : NSString? = ""
                     var currencyID:NSInteger? = nil;
                     */
                    
                    let productModule: ProductBO = ProductBO()
                    productModule.currency = (dictForProduct as! NSDictionary).value(forKey: "currencyName") as! NSString
                    productModule.productName = (dictForProduct as! NSDictionary).value(forKey: "productName") as! NSString
                    productModule.productID = (dictForProduct as! NSDictionary).value(forKey: "productId") as? NSInteger
                    productModule.fxAmount = (dictForProduct as! NSDictionary).value(forKey: "amount") as! NSInteger
                    productModule.inrAmount = (dictForProduct as! NSDictionary).value(forKey: "equivalentInr") as! NSInteger
                    productModule.isNewProduct = false
                    productModule.isUpdateButtonVisible = false
                    productModule.currencyCode = (dictForProduct as! NSDictionary).value(forKey: "currencyCode") as! NSString
                    productModule.roe = (dictForProduct as! NSDictionary).value(forKey: "roe") as! Float
                    productModule.isNostroAcc = (dictForProduct as! NSDictionary).value(forKey: "nostroAvailable") as? NSString
                    productModule.currencyID = (dictForProduct as! NSDictionary).value(forKey: "currencyId") as? NSInteger
                    
                    arrayMutableForTraveller.add(productModule)
                }
            }
            
            sellForexModule.productArray = arrayMutableForTraveller as! [ProductBO]
            
            let forexSellPassangerVc: SellForexProductViewController = SellForexProductViewController(nibName: "ForexBaseViewController", bundle: nil)
            //let contactDetail : String = dictOfForex.value(forKey: "bookerMobileNo") as! String
            forexSellPassangerVc.sellForexBO = sellForexModule
            forexSellPassangerVc.productArray = arrayMutableForTraveller as! [ProductBO]
            forexSellPassangerVc.previousScreen = "Pending Transaction"
            
            self.navigationController?.pushViewController(forexSellPassangerVc, animated: true)

        }
        

    }
    

    @IBAction func onBuyButtonClicked(_ sender: Any)
    {
        currentIndex = 1;
        underlineLabelBuy.backgroundColor = UIColor.white
        underlineLabelSell.backgroundColor = AppUtility.hexStringToUIColor(hex: "#0095DA")
        underlineLabelForexCard.backgroundColor = AppUtility.hexStringToUIColor(hex: "#0095DA")
        self.getPendingTransactions(withentity: "buy")
    }
    
    @IBAction func onCellButtonClicked(_ sender: Any)
    {
        currentIndex = 2;
        
        underlineLabelBuy.backgroundColor = AppUtility.hexStringToUIColor(hex: "#0095DA")
        underlineLabelSell.backgroundColor = UIColor.white
        underlineLabelForexCard.backgroundColor = AppUtility.hexStringToUIColor(hex: "#0095DA")
        self.getPendingTransactions(withentity: "sell")
    }

    @IBAction func onForexCardButtonClicked(_ sender: Any)
    {
        currentIndex = 3;

        underlineLabelBuy.backgroundColor = AppUtility.hexStringToUIColor(hex: "#0095DA")
        underlineLabelSell.backgroundColor = AppUtility.hexStringToUIColor(hex: "#0095DA")
        underlineLabelForexCard.backgroundColor = UIColor.white
        self.getPendingTransactions(withentity: "reload")
    }
    
    func onClosePopUp(With email: String, phoneNumber: NSString)
    {
        if !email.isEmpty && !phoneNumber.isEqual(to: "")
        {
            mobileAndEmailString = "retrieveQuote/\(phoneNumber)/\(email)" as NSString
        }
        else if (!phoneNumber.isEqual(to: ""))
        {
            mobileAndEmailString = "retrieveQuoteMob/\(phoneNumber)" as NSString
        }
        else if (!email.isEqual(""))
        {
            //retrieveQuoteEmail/sdf@asdg.dfg
            mobileAndEmailString = "retrieveQuoteEmail/\(email)" as NSString
        }
        
        self.getPendingTransactions(withentity: "buy")

    }
    
    func getPendingTransactions(withentity  entity :String) -> ()
    {
        
        LoadingIndicatorView.show("Loading")

        DispatchQueue.global(qos: .background).async
            {
                let pathParameter  : NSString = "/tcForexRS/quote/\(entity)/\(self.mobileAndEmailString)" as NSString
        ForexCommunicationManager.sharedInstance.execTask(pathParam:pathParameter , queryParam: "", requestType: "get", jsonDict:NSDictionary())
        {
            (status, response) in
            
            DispatchQueue.main.async
                {
                    LoadingIndicatorView.hide()
                    
                    if(response != nil)
                    {
                        if let jsonArray : NSArray = response as? NSArray
                        {
                            
                            if entity == "buy"
                            {
                                self.arrayForBuy = jsonArray
                            }
                            else if entity == "sell"
                            {
                                self.arrayForSell = jsonArray
                            }
                            else
                            {
                                self.arrayForReload = jsonArray
                            }
                            
                            self.arrayForTable = jsonArray
                            
                            self.tableViewPendingTrans.reloadData()
                        }
                        
                        //  self.arrayForCustomerState = NSMutableArray.init(array: jsonArray)  as! [NSDictionary]
                    }
                    else
                    {
                        self.showAlert(message: "Some error has occurred")
                    }
                
            }
        }

    }
          }
   
    //Mark:- fake hit for calculating cash limit
    func fetchProductList (didSelectRowAt indexPath: IndexPath,moduleID:String) -> Void
    {
        LoadingIndicatorView.show();
        
        let pathParam:NSString = NSString.init(format: "/tcForexRS/generic/product/%@", moduleID)
        
        ForexCommunicationManager.sharedInstance.execTask(pathParam: pathParam, queryParam: "", requestType: "get", jsonDict:NSDictionary())
        {
            (status, response) in
             DispatchQueue.main.async { () -> Void in
                LoadingIndicatorView.hide()
                let productArray : Array<NSDictionary>;
                if( !status || response == nil)
                {
                    self.showAlert(message: "Some error has occurred")
                }
                else
                {
                    let jsonArray : NSArray = response as! NSArray
                 if let filteredArray:Array<Dictionary<String,Any>> = jsonArray.filter({ (product:Any) -> Bool in
                        
                         if let productDict:NSDictionary = product  as? NSDictionary
                         {
                        if let productName:String = productDict.object(forKey: "productName") as? String
                        {
                            return productName.lowercased().contains("cash")
                        }
                        }
                        return false
                    }) as? Array<Dictionary<String,Any>>
                 {
                    
                    if filteredArray.count > 0
                    {
                       if let dict : Dictionary<String,Any> = filteredArray[0] as? Dictionary<String,Any>
                        {
                            if let productID:Int = dict["productId"] as? Int
                            {
                               self.fetchCurrencyList(ProductID: productID,didSelectRowAt: indexPath,moduleID: moduleID)
                                
                            }
                        }
                        
                    }
                }
            }
        }
    }
}
    
    func fetchCurrencyList (ProductID:Int,didSelectRowAt indexPath: IndexPath,moduleID:String)
    {
        LoadingIndicatorView.show();
        
         let pathParameter  : NSString = NSString.init(format: "/tcForexRS/generic/roe/%@/%i", moduleID,ProductID)
        
        //let pathParameter  : NSString = "/tcForexRS/generic/roe/1/".appending(String(ProductID)) as NSString
        ForexCommunicationManager.sharedInstance.execTask(pathParam:pathParameter , queryParam: "", requestType: "get", jsonDict:NSDictionary())
        {
            (status, response) in
            DispatchQueue.main.async { () -> Void in
                LoadingIndicatorView.hide()
                
                if(response != nil)
                {
                    let jsonArray : NSArray = response as! NSArray
                    var currencyArray : [NSDictionary] = []
                    for i in (0..<jsonArray.count)
                    {
                        let dict : NSDictionary = jsonArray[i] as! NSDictionary
                        currencyArray.append(dict)
                    }
                    for dict in currencyArray
                    {
                     
                        
                        
                        let currencyCode : String = (dict.object(forKey: "currencyCode") as? String)!
                        if currencyCode.contains("USD")
                        {
                            let roe = dict.object(forKey: "roe") as! Float
                                let inr : NSInteger = NSInteger(round( roe * 3000))
                                let singleTravellerLimit = inr
                                let forFreeDelivery :NSInteger = NSInteger(round( roe * 500))
                                switch(moduleID)
                                {
                                case "1","3":
                                       self.fill_RedirectBuy_ReloadForex(didSelectRowAt: indexPath, singleTravellerLimit: singleTravellerLimit, forFreeDelivery: forFreeDelivery,dollerLimit:roe)
                                    break
                                case "2":
                                    self.fill_RedirectSellForex(didSelectRowAt: indexPath, singleTravellerLimit: singleTravellerLimit, forFreeDelivery: forFreeDelivery,dollerLimit:roe)
                                    break
                               
                                default:
                                    printLog("INVALID MODULE")
                            }
                            
                            
                         
                           
                        }
                    }
                }
            }
        }
    }
    
    func fill_RedirectBuy_ReloadForex(didSelectRowAt indexPath: IndexPath,singleTravellerLimit:NSInteger,forFreeDelivery:NSInteger,dollerLimit:Float)
    {
        let dictOfForex : NSDictionary = arrayForTable[indexPath.row] as! NSDictionary
        
        var buyForexModule : BuyForexBO = BuyForexBO()
        
        var buyForexOptionModule: BuyForexOptionsViewBO =  BuyForexOptionsViewBO()
        
        buyForexModule.emailID = dictOfForex .value(forKey: "bookerEmailId") as! String
        
        buyForexOptionModule.purposeOfTravel = ""
        buyForexOptionModule.noOfTraveller = dictOfForex.value(forKey: "totalNoOfTraveller") as! NSInteger
        
        //buyForexOptionModule.travelDate = dictOfForex.value(forKey: "dateOfTravel")
        buyForexOptionModule.contactDetail = dictOfForex.value(forKey: "bookerMobileNo") as! String
        
        let travellerArray :NSArray = dictOfForex.value(forKey: "tcilForexQuoteTravellerCollection") as! NSArray
        
        let arrayMutableForTraveller : NSMutableArray = NSMutableArray()
        
        let travelDateString: NSString! = dictOfForex.value(forKey: "dateOfTravel") as! NSString
        
        var traveldate:NSDate = NSDate()
        
        if(!travelDateString.isEqual(to: ""))
        {
            let dateFormater:DateFormatter = DateFormatter()
            dateFormater.dateFormat = "dd-MM-yyyy"
            if (dateFormater.date(from: travelDateString as String)) != nil
            {
                traveldate  = dateFormater.date(from: travelDateString as String)! as NSDate
            }
            else
            {
                dateFormater.dateFormat = "yyyy-MM-dd HH:mm:ss Z"
                traveldate  = dateFormater.date(from: travelDateString as String)! as NSDate
                dateFormater.dateFormat = "dd-MM-yyyy"
                let dateStr : String = dateFormater.string(from: traveldate as Date)
                traveldate  = dateFormater.date(from: dateStr as String)! as NSDate
            }
            buyForexOptionModule.travelDate = traveldate as NSDate
        }
        buyForexModule.buyForexOptionViewDetails = buyForexOptionModule
        
        
        for var dictForTraveller in travellerArray
        {
            let arrayForProducts = (dictForTraveller as! NSDictionary).value(forKey: "tcilForexQuoteTravellerProductCollection") as! NSArray
            
            let travellerModule : TravellerBO = TravellerBO()
            //  travellerModule.travellerTitle = (dictForTraveller as! NSDictionary).value(forKey: "") as! String;
            // travellerModule.travellerFirstName = (dictForTraveller as! NSDictionary).value(forKey: "") as! String;
            //travellerModule.travellerLastName = (dictForTraveller as! NSDictionary).value(forKey: "") as! String;
            //travellerModule.travellerDateOfTravel = (dictForTraveller as! NSDictionary).value(forKey: "") as? NSDate;
            //travellerModule.travellerDateOfBirth = (dictForTraveller as! NSDictionary).value(forKey: "") as? NSDate;
            //travellerModule.travellerAadharNumber = (dictForTraveller as! NSDictionary).value(forKey: "") as! String;
            //travellerModule.travellerGSTINNumber = (dictForTraveller as! NSDictionary).value(forKey: "") as! String;
            
            /*
             var travellerTitle : String = String()
             
             var travellerFirstName : String = String()
             
             var travellerLastName : String = String()
             
             var travellerDateOfTravel : NSDate? = NSDate()
             
             var travellerDateOfBirth : NSDate? = nil
             
             var travellerAadharNumber : String = String()
             
             var travellerGSTINNumber : String = String()
             */
            
            
            for var dictForProduct in arrayForProducts
            {
                // ProductBO
                /*
                 var currency:NSString = "";
                 var productName:NSString = "";
                 var productID:NSInteger? = nil;
                 var fxAmount:NSInteger = 0;
                 var inrAmount:NSInteger = 0;
                 var isNewProduct :  Bool = true
                 var isUpdateButtonVisible = false
                 var currencyCode:NSString = ""
                 var roe : Float = 0
                 var isNostroAcc : NSString? = ""
                 var currencyID:NSInteger? = nil;
                 */
                
                let productModule: ProductBO = ProductBO()
                productModule.currency = (dictForProduct as! NSDictionary).value(forKey: "currencyName") as! NSString
                productModule.productName = (dictForProduct as! NSDictionary).value(forKey: "productName") as! NSString
                productModule.productID = (dictForProduct as! NSDictionary).value(forKey: "productId") as? NSInteger
                productModule.fxAmount = (dictForProduct as! NSDictionary).value(forKey: "amount") as! NSInteger
                productModule.inrAmount = (dictForProduct as! NSDictionary).value(forKey: "equivalentInr") as! NSInteger
                productModule.isNewProduct = false
                productModule.isUpdateButtonVisible = false
                productModule.amountInDollars = 0;
                productModule.currencyCode = (dictForProduct as! NSDictionary).value(forKey: "currencyCode") as! NSString
                productModule.roe = (dictForProduct as! NSDictionary).value(forKey: "roe") as! Float
                productModule.isNostroAcc = (dictForProduct as! NSDictionary).value(forKey: "nostroAvailable") as? NSString
                productModule.currencyID = (dictForProduct as! NSDictionary).value(forKey: "currencyId") as? NSInteger
                
                travellerModule.addProduct(product: productModule)
            }
            arrayMutableForTraveller.add(travellerModule)
        }
        
        
        //let stateOfCustomer : NSString = dictOfForex.value(forKey: "") as! NSString
        
        var buyForexPassengerInfo : BuyForexPassengerInfoBO  = BuyForexPassengerInfoBO(travellerArray: arrayMutableForTraveller, stateOfCustomer: "", branchOfCustomer: "", totalAmount: 12, stateCode: "", branchCode: "", isUnionTerritoryState: "", isUnionTerritoryBranch: "", gstBranchCode: "")
        
        buyForexModule.buyForexPassengerInfo = buyForexPassengerInfo;
        // buyForexPassengerInfo.travellerArray = arrayMutableForTraveller
        
        if(currentIndex == 1)
        {
            let forexBuyPassangerVc: ForexBuyPassangerViewController = ForexBuyPassangerViewController(nibName: "ForexBaseViewController", bundle: nil)
            forexBuyPassangerVc.buyForexBo = buyForexModule
            
            forexBuyPassangerVc.previousScreen = "Pending Transaction"
            forexBuyPassangerVc.singleTravellerLimit = singleTravellerLimit
            forexBuyPassangerVc.dollarRate = dollerLimit
            forexBuyPassangerVc.forFreeDelivery = forFreeDelivery
            forexBuyPassangerVc.travellerArray = arrayMutableForTraveller as! [TravellerBO];
            self.navigationController?.pushViewController(forexBuyPassangerVc, animated: true)
        }
        else
        {
            
            let forexBuyPassangerVc: ReloadForexCardDetailsViewController = ReloadForexCardDetailsViewController(nibName: FOREX_BASE_VC, bundle: nil)
            
            let buyForexBo : BuyForexBO = buyForexModule
            let buyForexOptionViewDetails : BuyForexOptionsViewBO = BuyForexOptionsViewBO.init()
            buyForexOptionViewDetails.contactDetail = dictOfForex.value(forKey: "bookerMobileNo") as! String
            forexBuyPassangerVc.previousScreen = "Pending Transaction"
            buyForexOptionViewDetails.noOfTraveller = 1
            buyForexOptionViewDetails.travelDate = traveldate as NSDate
            buyForexOptionViewDetails.cardNumber = dictOfForex.value(forKey: "reloadCardNo") as! String;
            buyForexBo.buyForexOptionViewDetails = buyForexOptionViewDetails
            
            forexBuyPassangerVc.buyForexBo = buyForexBo
            forexBuyPassangerVc.travellerArray = arrayMutableForTraveller as! [TravellerBO];
            self.navigationController?.pushViewController(forexBuyPassangerVc, animated: true)
            
        }
        
        
        
    }
    
    func fill_RedirectSellForex(didSelectRowAt indexPath: IndexPath,singleTravellerLimit:NSInteger,forFreeDelivery:NSInteger,dollerLimit:Float)
    {
        let dictOfForex : NSDictionary = arrayForTable[indexPath.row] as! NSDictionary
        
        var sellForexModule : SellForexBO = SellForexBO()
        
        /*
         var productArray : [ProductBO] = []
         var mobileNumber : String = ""
         var customerState : String = ""
         var customerbranch : String = ""
         var gstStateCode : String = ""
         var branchCode : String = ""
         var gstBranchCode : String = ""
         var isUnionTerritoryState : String = ""
         var isUnionTerritoryBranch : String = ""
         
         */
        
        
        sellForexModule.emailID = dictOfForex .value(forKey: "bookerEmailId") as! String
        //buyForexOptionModule.travelDate = dictOfForex.value(forKey: "dateOfTravel")
        sellForexModule.mobileNumber = dictOfForex.value(forKey: "bookerMobileNo") as! String
        
        
        
        let travellerArray :NSArray = dictOfForex.value(forKey: "tcilForexQuoteTravellerCollection") as! NSArray
        
        
        var arrayMutableForTraveller : NSMutableArray = NSMutableArray()
        
        for var dictForTraveller in travellerArray
        {
            let arrayForProducts = (dictForTraveller as! NSDictionary).value(forKey: "tcilForexQuoteTravellerProductCollection") as! NSArray
            
            for var dictForProduct in arrayForProducts
            {
                // ProductBO
                /*
                 var currency:NSString = "";
                 var productName:NSString = "";
                 var productID:NSInteger? = nil;
                 var fxAmount:NSInteger = 0;
                 var inrAmount:NSInteger = 0;
                 var isNewProduct :  Bool = true
                 var isUpdateButtonVisible = false
                 var currencyCode:NSString = ""
                 var roe : Float = 0
                 var isNostroAcc : NSString? = ""
                 var currencyID:NSInteger? = nil;
                 */
                
                let productModule: ProductBO = ProductBO()
                productModule.currency = (dictForProduct as! NSDictionary).value(forKey: "currencyName") as! NSString
                productModule.productName = (dictForProduct as! NSDictionary).value(forKey: "productName") as! NSString
                productModule.productID = (dictForProduct as! NSDictionary).value(forKey: "productId") as? NSInteger
                productModule.fxAmount = (dictForProduct as! NSDictionary).value(forKey: "amount") as! NSInteger
                productModule.inrAmount = (dictForProduct as! NSDictionary).value(forKey: "equivalentInr") as! NSInteger
                productModule.isNewProduct = false
                productModule.isUpdateButtonVisible = false
                productModule.currencyCode = (dictForProduct as! NSDictionary).value(forKey: "currencyCode") as! NSString
                productModule.roe = (dictForProduct as! NSDictionary).value(forKey: "roe") as! Float
                productModule.isNostroAcc = (dictForProduct as! NSDictionary).value(forKey: "nostroAvailable") as? NSString
                productModule.currencyID = (dictForProduct as! NSDictionary).value(forKey: "currencyId") as? NSInteger
                
                arrayMutableForTraveller.add(productModule)
            }
        }
        
        sellForexModule.productArray = arrayMutableForTraveller as! [ProductBO]
        
        let forexSellPassangerVc: SellForexProductViewController = SellForexProductViewController(nibName: "ForexBaseViewController", bundle: nil)
        //let contactDetail : String = dictOfForex.value(forKey: "bookerMobileNo") as! String
        forexSellPassangerVc.sellForexBO = sellForexModule
        forexSellPassangerVc.productArray = arrayMutableForTraveller as! [ProductBO]
        forexSellPassangerVc.previousScreen = "Pending Transaction"
        forexSellPassangerVc.dollarRate = dollerLimit
        
        self.navigationController?.pushViewController(forexSellPassangerVc, animated: true)
        
    }
    
    
    
}
