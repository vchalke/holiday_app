//
//  VisaViewController.swift
//  sotc-consumer-application
//
//  Created by Saurav Kumar on 09/08/17.
//  Copyright © 2017 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit

class VisaViewController: UIViewController, UITableViewDelegate,UITableViewDataSource,UIDocumentInteractionControllerDelegate,DocumentListViewDelegate,UIPopoverPresentationControllerDelegate {
    
    @IBOutlet weak var visaTypeTextField: FloatLabelTextField!
    @IBOutlet var visaProgressTableView: UITableView!
    
    
    var tourDetails:Tour?
    
    var isfromNotificationHomeVc:Bool = false
    
    var visaDetailsDataArray:Array<VisaDetailsData>!
    
    var overlay: UIView?
    var sourceView: UIView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.visaProgressTableView.register(UINib(nibName:  "VisaTableViewCell", bundle: nil), forCellReuseIdentifier: "visaProgressView")
        
        
        self.visaProgressTableView.tableFooterView = UIView(frame: .zero)
        
        self.setDefaultVisaDetails()
        
        self.visaProgressTableView.reloadData()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.setRedNavigationBar()
        self.navigationController?.setnavigatiobBarTitle(title: Title.VISA, viewController: self)
        self.navigationController?.setVisaRightButton(showRightButton: true, viewController: self, showVisaDocButton: true)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        if self.navigationController?.viewControllers.firstIndex(of: self) == nil && self.isfromNotificationHomeVc
        {
            SlideNavigationController.sharedInstance().popViewController(animated: true)
        }
        
    }
    
    func backButtonClick() -> Void {
        
        
        self.navigationController?.popViewController(animated: true)
        
        self.navigationController?.setNavigationBarTranslucent()
        
    }
    
    //MARK: - UITextField Delegate Implementaion
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if visaDetailsDataArray != nil && visaDetailsDataArray.count > 0
        {
            return visaDetailsDataArray.count
        }
        
        return 0
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 230
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "visaProgressView", for: indexPath) as? VisaTableViewCell
            else {
                
                fatalError("The dequeued cell is not an instance of MealTableViewCell.")
        }
        
        //       let  cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "visaProgressView")
        //        // }
        
        
        
        var visaDetails = visaDetailsDataArray[indexPath.row]
        
        
        if visaDetails.visaProccessStatus == VisaProccessStatus.Failed
        {
            cell.visaProccessFailed = true
            
        }
        else
            
        {
            cell.visaProccessFailed = false
        }
        
        
        
        
        cell.completedTillIndex = visaDetails.documentProgress
        
        
        if  visaDetails.visaProggressStates.count > 3
        {
            cell.hideInterviewScheduled = false
        }
        else
        {
            cell.hideInterviewScheduled = true
        }
        
        
        
        cell.showProgressBar()
        cell.visaDetail = visaDetails
        cell.userNameLabel.text = visaDetails.passengerName
        
        if visaDetails.isVisaRequired
        {
            
            
            
            cell.visaRequiredDocView.isHidden = false // doc view display
            
            cell.visaReqiredLabel.isHidden = true //label hiden
            
            //
            //        cell.letterFormatView.isHidden = true
            //        cell.appointmentLetterView.isHidden = true
            //        if visaDetails.isCoveringLetter
            //        {
            //            cell.letterFormatView.isHidden = false
            //        }
            
            if visaDetails.isAppointmentLetter
            {
                cell.appointmentLetterView.isHidden = false
                
                cell.documentStackViewWidthConst.constant = 160.0
                
                 cell.appointmentLetterBtn.addTarget(self, action: #selector(onAppointmentLetterButtonClick(button:)), for: .touchUpInside)
            }
            else
            {
                cell.appointmentLetterView.isHidden = true
                cell.documentStackViewWidthConst.constant = 80.0
            }
            
            if visaDetails.isDocumentPending
            {
                cell.visaReqiredLabel.isHidden = false
                cell.visaReqiredLabel.text = visaDetails.visaNotRequiresStatus
            }
            
            cell.letterFormatBtn.addTarget(self, action: #selector(self.onLetterFormatButtonClick(button:)), for: .touchUpInside)
            
            cell.coveringLetterButton.addTarget(self, action: #selector(self.onCoveringLetterButtonClick(button:)), for: .touchUpInside)
            
            
            
        }else{
            
            cell.visaRequiredDocView.isHidden = true
            cell.visaReqiredLabel.isHidden = false
            cell.visaReqiredLabel.text = visaDetails.visaNotRequiresStatus
            
            
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    //MARK: - UITextField Delegate Implementaion

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        
        let visaTypeArray:Array<String> = self.getUniqueVisaCountry() //self.getUniqueVisaType()
        
        if visaTypeArray.count > 1
        {
            
            let deviationActionSheet:UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            
            deviationActionSheet.addAction(UIAlertAction(title: AlertMessage.TITLE_CANCEL, style: .cancel, handler:nil))
            
            
            for visaCountry in visaTypeArray
            {
                deviationActionSheet.addAction(UIAlertAction(title: visaCountry , style: .default, handler: { (action: UIAlertAction!)in
                    
                    self.visaTypeTextField.text = visaCountry
                    
                    /*  let filtedVisaDetails = self.getVisaDetailsByVisaCountry(visaCountry: visaCountry)
                     
                     if filtedVisaDetails.count > 0
                     {
                     self.visaDetailsDataArray = self.createVisaDetailsWithStatus(filterdVisaArray:filtedVisaDetails)
                     
                     self.visaProgressTableView.reloadData()
                     } -- old logic */
                    
                    // self.visaDetailsDataArray = self.createVisaDetailsWithStatus(visaCountry: visaCountry)
                    
                    
                    
                    let tableRows = self.visaProgressTableView.indexPathsForVisibleRows
                    
                    /*  if tableRows != nil
                     {
                     self.visaProgressTableView.deleteRows(at: tableRows!, with: UITableViewRowAnimation.top)
                     }*/
                    
                    if self.visaDetailsDataArray.count > 0
                    {
                        self.visaDetailsDataArray.removeAll()
                        
                    }
                    
                    self.visaDetailsDataArray = self.createVisaDetailsWithStatus(visaCountry: visaCountry)
                    
                    self.visaProgressTableView.reloadData()
                    
                }))
                
                
            }
            
            self.present(deviationActionSheet, animated: true, completion: nil)
        }
        
         self.visaTypeTextField.resignFirstResponder()
        
        return false
        
        
    }
    

    
    public func textFieldDidBeginEditing(_ textField: UITextField)
    {
       
        
    }
    
    //MARK: - Filter Methods
    
    func getVisaDetailsByVisaType(visaType:String)->Array<Visa> {
        
        if let visaDetailsArray:Array<Visa> = self.tourDetails?.visaRelation?.allObjects as? Array<Visa>
        {
            
            let filteredVisaArray =  visaDetailsArray.filter(){$0.visaType == visaType}
            
            return filteredVisaArray
            
        }
        
        return []
        
    }
    
    
    func getVisaDetailsByVisaCountry(visaCountry:String)->Array<Visa> {
        
        if let visaDetailsArray:Array<Visa> = self.tourDetails?.visaRelation?.allObjects as? Array<Visa>
        {
            
            let filteredVisaArray =  visaDetailsArray.filter(){$0.visaCountry == visaCountry}
            
            return filteredVisaArray
            
        }
        
        return []
        
    }
    
    func getVisaDetails(by visaCountry:String , passengerNumber:String)->Visa? {
        
        if let visaDetailsArray:Array<Visa> = self.tourDetails?.visaRelation?.allObjects as? Array<Visa>
        {
            
            let filteredVisa =  visaDetailsArray.filter(){$0.visaCountry == visaCountry && $0.passengerNumber == passengerNumber }.first
            
            return filteredVisa
            
        }
        
        return nil
        
    }
    
     //MARK: - Visa Creation Methods NEW
    
  /*  func createVisaDetailsWithStatusNew(visaCountry:String) ->Array<VisaDetailsData>
    {
        var visaDetailsArray:Array<VisaDetailsData> = []
        
        if var passengerArray:Array<Passenger> = self.tourDetails?.passengerRelation?.allObjects as? Array<Passenger>
        {
            let sortedPassengerArray = passengerArray.sorted(){ $0.passengerNumber! < $1.passengerNumber!}
            
            for passenger in sortedPassengerArray
            {
                var visaData:VisaDetailsData = VisaDetailsData()
                 visaData.passengerName = "\((passenger.passengerNumber ?? ""))_\((passenger.firstName ?? "").capitalized) \((passenger.middleName ?? "").capitalized) \((passenger.lastName ?? "").capitalized)"  // /*passenger.passengerNumber! + "_" +  */ passenger.firstName! + " " + passenger.lastName!
                
                
                let visaLetterFormatDocument = self.getVisaDocument(documentType: Constant.Visa.LETTER_FORMAT_DOC, passengerNumber: passenger.passengerNumber!,visaCountry: visaCountry)
                
                visaData.isLetterFormat = true
                visaData.letterFormatURL = visaLetterFormatDocument.documnetUrl
                visaData.letterFormatDocumentPath = visaLetterFormatDocument.documnetFilePath
                visaData.isCoveringLetter = false
                
               
                
                if self.isInterviewScheduleDisplay() // display Interview schedulde
                {
                    visaData.visaProggressStates = [VisaProccessTitle.DocumentPending.rawValue,VisaProccessTitle.DocumentsVerified.rawValue,VisaProccessTitle.InterviewScheduled.rawValue,VisaProccessTitle.VisaApplicationProcessed.rawValue]
                    
                    
                    
                    let appointmnetDocument = getVisaAppointmentLetterDocument(documentType: Constant.Visa.APPONTMENT_LETTER_STATUS, passengerNumber: passenger.passengerNumber!,visaCountry: visaCountry)
                    
                    
                    
                    if  let filteredVisa = self.getVisaDetails(by: visaCountry, passengerNumber: passenger.passengerNumber!)
                    {
                        let visaRequestStatus:String = filteredVisa.visaStatus?.lowercased() ?? ""
                        
                        
                        visaData.isAppointmentLetter = appointmnetDocument.isDocumnetPresent
                        
                        visaData.appointmentLetters = appointmnetDocument.visaAppointmentLetters
                        
                        //1.Complete / Rejected
                        if visaRequestStatus == Constant.Visa.VISA_DONE_STATUS || visaRequestStatus == Constant.Visa.TC_CSS_VISA_DONE_STATUS || visaRequestStatus == Constant.Visa.VISA_REJECTED_STATUS || visaRequestStatus == Constant.Visa.TC_CSS_VISA_REJECTED_STATUS_1 || visaRequestStatus == Constant.Visa.TC_CSS_VISA_REJECTED_STATUS_2
                        {
                            
                            switch visaRequestStatus {
                            case Constant.Visa.VISA_DONE_STATUS,Constant.Visa.TC_CSS_VISA_DONE_STATUS:
                                visaData.visaProccessStatus = VisaProccessStatus.Completed
                            case Constant.Visa.VISA_REJECTED_STATUS,Constant.Visa.TC_CSS_VISA_REJECTED_STATUS_1,Constant.Visa.TC_CSS_VISA_REJECTED_STATUS_2:
                                visaData.visaProccessStatus = VisaProccessStatus.Failed
                            default:
                                ""
                            }
                            
                            visaData.documentProgress = 3
                            visaData.isVisaRequired = true
                            
                            
                            
                        }else if visaRequestStatus == Constant.Visa.TC_CSS_VISA_APPOINTMENT_SCHEDULED_1 || visaRequestStatus == Constant.Visa.TC_CSS_VISA_APPOINTMENT_SCHEDULED_2 || visaRequestStatus == Constant.Visa.TC_CSS_VISA_APPOINTMENT_SCHEDULED_3 || visaRequestStatus == Constant.Visa.TC_CSS_VISA_APPOINTMENT_SCHEDULED_4
                        {
                            
                             visaData.visaProccessStatus = VisaProccessStatus.Completed
                            visaData.documentProgress = 2
                            visaData.isVisaRequired = true
                            
                            
                        }else if( visaRequestStatus == Constant.Visa.DOCUMENT_VERIFIED_STATUS ||
                            visaRequestStatus == Constant.Visa.TC_CSS_DOCUMENT_VERIFIED_STATUS)
                            
                        {
                         
                            visaData.visaProccessStatus = VisaProccessStatus.Completed
                            visaData.documentProgress = 1
                            visaData.isAppointmentLetter = false
                            visaData.isVisaRequired = true
                            
                            
                        }else if( visaRequestStatus == Constant.Visa.DOCUMENT_RECIVED_STATUS || visaRequestStatus == Constant.Visa.TC_CSS_DOCUMENT_RECIVED_STATUS_1 || visaRequestStatus == Constant.Visa.TC_CSS_DOCUMENT_RECIVED_STATUS_2 ||visaRequestStatus == TC_CSS_DOCUMENT_RECIVED_STATUS_3)
                        {
                            visaData.visaProccessStatus = VisaProccessStatus.Completed
                             visaData.documentProgress = 0
                            visaData.isAppointmentLetter = false
                            visaData.isVisaRequired = true
                            
                        }else
                            {
                                visaData.isDocumentPending = true
                                visaData.visaProccessStatus = VisaProccessStatus.NotStarted
                                visaData.documentProgress = -1 //-1
                                visaData.isAppointmentLetter = false
                                 //visaData.isVisaRequired = false
                                visaData.visaNotRequiresStatus = Constant.Visa.DOCUMENTS_PENDING_STATUS
                        }
                        
                        
                    }else{
                    
                        visaData.isAppointmentLetter = false
                        //visaData.isVisaRequired = true
                        //visaData.isLetterFormat = true
                        visaData.isDocumentPending = true
                        visaData.letterFormatURL = visaLetterFormatDocument.documnetUrl
                        visaData.letterFormatDocumentPath = visaLetterFormatDocument.documnetFilePath
                        visaData.documentProgress =  -1
                        visaData.visaProccessStatus = VisaProccessStatus.NotStarted
                        visaData.visaNotRequiresStatus = Constant.Visa.DOCUMENTS_PENDING_STATUS
                        visaData.isAppointmentLetter = false
                        visaData.appointmentLetters = []
                    
                    }
                  
                    
               /* Interview Schedulde */ }else
                {
                    
                    visaData.visaProggressStates = [VisaProccessTitle.DocumentPending.rawValue,VisaProccessTitle.DocumentsVerified.rawValue,VisaProccessTitle.VisaApplicationProcessed.rawValue]
                    
                    visaData.isAppointmentLetter = false
                    
                    visaData.appointmentLetters = []
                    
                    visaData.isAppointmentLetter = false
                    
                    if  let filteredVisa = self.getVisaDetails(by: visaCountry, passengerNumber: passenger.passengerNumber!)
                    {
                        let visaRequestStatus:String = filteredVisa.visaRequestStatus?.lowercased() ?? ""
                        
                        
                        
                        //1.Complete / Rejected
                        if visaRequestStatus == Constant.Visa.VISA_DONE_STATUS || visaRequestStatus == Constant.Visa.TC_CSS_VISA_DONE_STATUS2 || visaRequestStatus == Constant.Visa.TC_CSS_VISA_DONE_STATUS || visaRequestStatus == Constant.Visa.VISA_REJECTED_STATUS || visaRequestStatus == Constant.Visa.TC_CSS_VISA_REJECTED_STATUS_1 || visaRequestStatus == Constant.Visa.TC_CSS_VISA_REJECTED_STATUS_2
                        {
                            
                            switch visaRequestStatus {
                            case Constant.Visa.VISA_DONE_STATUS,Constant.Visa.TC_CSS_VISA_DONE_STATUS,Constant.Visa.TC_CSS_VISA_DONE_STATUS2:
                                visaData.visaProccessStatus = VisaProccessStatus.Completed
                            case Constant.Visa.VISA_REJECTED_STATUS,Constant.Visa.TC_CSS_VISA_REJECTED_STATUS_1,Constant.Visa.TC_CSS_VISA_REJECTED_STATUS_2:
                                visaData.visaProccessStatus = VisaProccessStatus.Failed
                            default:
                                ""
                            }
                            
                            visaData.documentProgress = 3
                            visaData.isVisaRequired = true
                            
                            
                            
                        }else if visaRequestStatus == Constant.Visa.TC_CSS_VISA_APPOINTMENT_SCHEDULED_1 || visaRequestStatus == Constant.Visa.TC_CSS_VISA_APPOINTMENT_SCHEDULED_2 || visaRequestStatus == Constant.Visa.TC_CSS_VISA_APPOINTMENT_SCHEDULED_3 || visaRequestStatus == Constant.Visa.TC_CSS_VISA_APPOINTMENT_SCHEDULED_4
                        {
                            
                            visaData.visaProccessStatus = VisaProccessStatus.Completed
                            visaData.documentProgress = 2
                            visaData.isVisaRequired = true
                            
                            
                        }else if( visaRequestStatus == Constant.Visa.DOCUMENT_VERIFIED_STATUS ||
                            visaRequestStatus == Constant.Visa.TC_CSS_DOCUMENT_VERIFIED_STATUS)
                            
                        {
                            
                            visaData.visaProccessStatus = VisaProccessStatus.Completed
                            visaData.documentProgress = 1
                            visaData.isAppointmentLetter = false
                            visaData.isVisaRequired = true
                            
                            
                        }else if( visaRequestStatus == Constant.Visa.DOCUMENT_RECIVED_STATUS || visaRequestStatus == Constant.Visa.TC_CSS_DOCUMENT_RECIVED_STATUS_1 || visaRequestStatus == Constant.Visa.TC_CSS_DOCUMENT_RECIVED_STATUS_2 || visaRequestStatus == TC_CSS_DOCUMENT_RECIVED_STATUS_3)
                        {
                            visaData.visaProccessStatus = VisaProccessStatus.Completed
                            visaData.documentProgress = 0
                            visaData.isAppointmentLetter = false
                            visaData.isVisaRequired = true
                            
                        }else
                        {
                            visaData.isDocumentPending = true
                            visaData.visaProccessStatus = VisaProccessStatus.NotStarted
                            visaData.documentProgress = -1 //-1
                            visaData.isAppointmentLetter = false
                            //visaData.isVisaRequired = false
                            visaData.visaNotRequiresStatus = Constant.Visa.DOCUMENTS_PENDING_STATUS
                        }
                        
                        
                    }else{
                        
                        visaData.isAppointmentLetter = false
                        //visaData.isVisaRequired = true
                        //visaData.isLetterFormat = true
                        visaData.isDocumentPending = true
                        visaData.letterFormatURL = visaLetterFormatDocument.documnetUrl
                        visaData.letterFormatDocumentPath = visaLetterFormatDocument.documnetFilePath
                        visaData.documentProgress =  -1
                        visaData.visaProccessStatus = VisaProccessStatus.NotStarted
                        visaData.visaNotRequiresStatus = Constant.Visa.DOCUMENTS_PENDING_STATUS
                    
                    }
                    
                }
                
            }
        }
        
         return visaDetailsArray
    }*/
    
    
    //MARK: - Visa Creation Methods
    
    
    func createVisaDetailsWithStatus(visaCountry:String) ->Array<VisaDetailsData>
    {
        var visaDetailsArray:Array<VisaDetailsData> = []
        
        if let passengerArray:Array<Passenger> = self.tourDetails?.passengerRelation?.allObjects as? Array<Passenger>
        {
            let sortedPassengerArray = passengerArray.sorted(){ $0.passengerNumber! < $1.passengerNumber!}
            
            for passenger in sortedPassengerArray
            {
                
                var visaData:VisaDetailsData = VisaDetailsData()
                
                visaData.passengerName = "\((passenger.firstName ?? "").capitalized) \((passenger.middleName ?? "").capitalized) \((passenger.lastName ?? "").capitalized)"  // /*passenger.passengerNumber! + "_" +  */ passenger.firstName! + " " + passenger.lastName!
                
                /* let appointmnetDocument = self.getVisaDocument(documnetType: "visa appointment letter", passengerNumber: passenger.passengerNumber!)
                 
                 let visaCoveringDocument = self.getVisaDocument(documnetType: Constant.Visa.VISA_COVERING_LETTER_FORMAT_DOC, passengerNumber: passenger.passengerNumber!)*/
                
                let visaLetterFormatDocument = self.getVisaDocument(documentType: Constant.Visa.LETTER_FORMAT_DOC, passengerNumber: passenger.passengerNumber!,visaCountry: visaCountry)
                
                
                //  if  var documnetStatus:String = passenger.documentStatus //--- commented for TC_CSS
                
                if  let filteredVisa = self.getVisaDetails(by: visaCountry, passengerNumber: passenger.passengerNumber!)
                {
                    let documnetStatus:String = filteredVisa.visaRequestStatus?.lowercased() ?? ""
                    
                    if documnetStatus != "" //( documnetStatus == Constant.Visa.DOCUMENT_RECIVED_STATUS || documnetStatus == Constant.Visa.TC_CSS_DOCUMENT_RECIVED_STATUS_1 || documnetStatus == Constant.Visa.TC_CSS_DOCUMENT_RECIVED_STATUS_2 || documnetStatus == TC_CSS_DOCUMENT_RECIVED_STATUS_3|| documnetStatus == Constant.Visa.DOCUMENT_VERIFIED_STATUS || documnetStatus == Constant.Visa.TC_CSS_DOCUMENT_VERIFIED_STATUS)
                    {
                        
                        switch documnetStatus {
                        case Constant.Visa.DOCUMENT_RECIVED_STATUS,Constant.Visa.TC_CSS_DOCUMENT_RECIVED_STATUS_1,Constant.Visa.TC_CSS_DOCUMENT_RECIVED_STATUS_2,Constant.Visa.TC_CSS_DOCUMENT_RECIVED_STATUS_3:
                             visaData.documentProgress = 0
                            visaData.isAppointmentLetter = false
                            visaData.isCoveringLetter = false
                            visaData.isVisaRequired = true
                            visaData.isLetterFormat = true
                            visaData.letterFormatURL = visaLetterFormatDocument.documnetUrl
                            visaData.letterFormatDocumentPath = visaLetterFormatDocument.documnetFilePath
                            // visaData.visaNotRequiresStatus = Constant.Visa.DOCUMENTS_PENDING_STATUS
                            //visaData.isDocumentPending = true
                            printLog("No visa Data for this")
                            visaData.visaProccessStatus = VisaProccessStatus.Completed
                            
                        case Constant.Visa.DOCUMENT_VERIFIED_STATUS,Constant.Visa.TC_CSS_DOCUMENT_VERIFIED_STATUS:
                            visaData.documentProgress = 1
                            visaData.isAppointmentLetter = false
                            visaData.isCoveringLetter = false
                            visaData.isVisaRequired = true
                            visaData.isLetterFormat = true
                            visaData.letterFormatURL = visaLetterFormatDocument.documnetUrl
                            visaData.letterFormatDocumentPath = visaLetterFormatDocument.documnetFilePath
                            //visaData.visaNotRequiresStatus = Constant.Visa.DOCUMENTS_PENDING_STATUS
                            //visaData.isDocumentPending = true
                            printLog("No visa Data for this")
                            visaData.visaProccessStatus = VisaProccessStatus.Completed
                            
                        case Constant.Visa.TC_CSS_NO_VISA_SERVICES:
                            visaData.documentProgress = -1 //-1
                            visaData.isAppointmentLetter = false
                            visaData.isCoveringLetter = false
                            visaData.isVisaRequired = true
                            visaData.isLetterFormat = true
                            visaData.letterFormatURL = visaLetterFormatDocument.documnetUrl
                            visaData.letterFormatDocumentPath = visaLetterFormatDocument.documnetFilePath
                            visaData.visaNotRequiresStatus = Constant.Visa.DOCUMENTS_NOT_PROCESSED_BY_TCIL
                            visaData.isDocumentPending = true
                            printLog("No visa Data for this")
                            visaData.visaProccessStatus = VisaProccessStatus.NotStarted
                            
                        case Constant.Visa.TC_CSS_DOC_PENDING:
                            visaData.documentProgress = -1 //-1
                            visaData.isAppointmentLetter = false
                            visaData.isCoveringLetter = false
                            visaData.isVisaRequired = true
                            visaData.isLetterFormat = true
                            visaData.letterFormatURL = visaLetterFormatDocument.documnetUrl
                            visaData.letterFormatDocumentPath = visaLetterFormatDocument.documnetFilePath
                            visaData.visaNotRequiresStatus = Constant.Visa.DOCUMENTS_PENDING_STATUS
                            visaData.isDocumentPending = true
                            printLog("No visa Data for this")
                            visaData.visaProccessStatus = VisaProccessStatus.NotStarted
                            
                        default:
                            visaData.documentProgress = -1 //-1
                            visaData.isAppointmentLetter = false
                            visaData.isCoveringLetter = false
                            visaData.isVisaRequired = true
                            visaData.isLetterFormat = true
                            visaData.letterFormatURL = visaLetterFormatDocument.documnetUrl
                            visaData.letterFormatDocumentPath = visaLetterFormatDocument.documnetFilePath
                            visaData.visaNotRequiresStatus = ""//Constant.Visa.DOCUMENTS_PENDING_STATUS
                            visaData.isDocumentPending = true
                            printLog("No visa Data for this")
                            visaData.visaProccessStatus = VisaProccessStatus.NotStarted
                        }
                        
                        /* if visaCountry == ""
                         {
                         
                         visaData.visaProggressStates = [VisaProccessTitle.DocumentPending.rawValue,VisaProccessTitle.DocumentsVerified.rawValue,VisaProccessTitle.InterviewScheduled.rawValue,VisaProccessTitle.VisaApplicationProcessed.rawValue]
                         
                         visaData.isAppointmentLetter = false
                         visaData.isCoveringLetter = false
                         visaData.isVisaRequired = false
                         visaData.visaNotRequiresStatus = "Visa not required"
                         visaData.visaProccessStatus = VisaProccessStatus.NotStarted
                         
                         visaData.documentProgress = -1 //-1 // show lable as visa not required
                         
                         }
                         else*/
                        //{
                        
                        /* if let filteredVisa = self.getVisaDetails(by: visaCountry, passengerNumber: passenger.passengerNumber!)
                         {*/ //--- commented for TC_CSS
                        
                        let visaStatus = filteredVisa.visaStatus
                        
                        
                        if visaStatus?.uppercased() == Constant.Visa.SELF_ARRANGED_STATUS || visaStatus?.uppercased() == Constant.Visa.HOLDING_VISA_STATUS || visaStatus?.uppercased() == Constant.Visa.VISA_ACTIONED_STATUS
                        {
                            if self.isInterviewScheduleDisplay()
                            {
                                visaData.visaProggressStates = [VisaProccessTitle.DocumentPending.rawValue,VisaProccessTitle.DocumentsVerified.rawValue,VisaProccessTitle.InterviewScheduled.rawValue,VisaProccessTitle.VisaApplicationProcessed.rawValue]
                            }else
                            {
                                visaData.visaProggressStates = [VisaProccessTitle.DocumentPending.rawValue,VisaProccessTitle.DocumentsVerified.rawValue,VisaProccessTitle.VisaApplicationProcessed.rawValue]
                            }
                            
                            visaData.isAppointmentLetter = false
                            visaData.isCoveringLetter = false
                            visaData.documentProgress = -1 //-1
                            visaData.isVisaRequired = false
                            visaData.visaNotRequiresStatus = visaStatus
                            visaData.visaProccessStatus = VisaProccessStatus.NotStarted
                            
                            if visaStatus?.uppercased() != Constant.Visa.SELF_ARRANGED_STATUS
                            {
                                visaData.isLetterFormat = true
                                visaData.isVisaRequired = true
                                visaData.letterFormatURL = visaLetterFormatDocument.documnetUrl
                                visaData.letterFormatDocumentPath = visaLetterFormatDocument.documnetFilePath
                                visaData.isDocumentPending = true
                            }
                            
                            
                        }else
                        {
                            
                            if var visaRequestStatus = filteredVisa.visaRequestStatus
                            {
                                visaRequestStatus = visaRequestStatus.uppercased()
                                
                                
                                visaData.visaProggressStates = [VisaProccessTitle.DocumentPending.rawValue,VisaProccessTitle.DocumentsVerified.rawValue,VisaProccessTitle.VisaApplicationProcessed.rawValue]
                                
                                
                                if visaRequestStatus == Constant.Visa.VISA_DONE_STATUS || visaRequestStatus == Constant.Visa.TC_CSS_VISA_DONE_STATUS2 || visaRequestStatus == Constant.Visa.TC_CSS_VISA_DONE_STATUS || visaRequestStatus == Constant.Visa.VISA_REJECTED_STATUS || visaRequestStatus == Constant.Visa.TC_CSS_VISA_REJECTED_STATUS_1 || visaRequestStatus == Constant.Visa.TC_CSS_VISA_REJECTED_STATUS_2
                                {
                                    
                                    switch visaRequestStatus {
                                    case Constant.Visa.VISA_DONE_STATUS,Constant.Visa.TC_CSS_VISA_DONE_STATUS,Constant.Visa.TC_CSS_VISA_DONE_STATUS2:
                                        visaData.visaProccessStatus = VisaProccessStatus.Completed
                                    case Constant.Visa.VISA_REJECTED_STATUS,Constant.Visa.TC_CSS_VISA_REJECTED_STATUS_1,Constant.Visa.TC_CSS_VISA_REJECTED_STATUS_2:
                                        visaData.visaProccessStatus = VisaProccessStatus.Failed
                                    default:
                                        ""
                                    }
                                    // check tour code and show interview scheduld
                                    
                                    /*   let visaCoveringDocument = self.getVisaDocument(documentType: Constant.Visa.VISA_COVERING_LETTER_FORMAT_DOC, passengerNumber: passenger.passengerNumber!,visaCountry:visaCountry )
                                     
                                     visaData.isCoveringLetter = true//visaCoveringDocument.isDocumnetPresent
                                     
                                     visaData.coveringLetterUrl = visaCoveringDocument.documnetUrl
                                     
                                     visaData.coveringLetterDocumentPath = visaCoveringDocument.documnetFilePath*/
                                    
                                    visaData.isLetterFormat = true
                                    visaData.letterFormatURL = visaLetterFormatDocument.documnetUrl
                                    visaData.letterFormatDocumentPath = visaLetterFormatDocument.documnetFilePath
                                    visaData.isDocumentPending = false
                                    
                                    if self.isInterviewScheduleDisplay()
                                    {
                                        
                                        visaData.visaProggressStates = [VisaProccessTitle.DocumentPending.rawValue,VisaProccessTitle.DocumentsVerified.rawValue,VisaProccessTitle.InterviewScheduled.rawValue,VisaProccessTitle.VisaApplicationProcessed.rawValue]
                                        
                                        //let appointmnetDocument = self.getVisaDocument(documentType: Constant.Visa.APPONTMENT_LETTER_STATUS, passengerNumber: passenger.passengerNumber!,visaCountry: visaCountry) -- commented fo TC_CSS
                                        
                                       let appointmnetDocument = getVisaAppointmentLetterDocument(documentType: Constant.Visa.APPONTMENT_LETTER_STATUS, passengerNumber: passenger.passengerNumber!,visaCountry: visaCountry)
                                        
                                        visaData.isAppointmentLetter = false //appointmnetDocument.isDocumnetPresent
                                        
                                        visaData.appointmentLetters = [] //appointmnetDocument.visaAppointmentLetters
                                        
                                       // visaData.appointmentLetterUrl = appointmnetDocument.documnetUrl -- commented fo TC_CSS
                                        
                                        
                                        //visaData.appointmentLetterDocumentPath = appointmnetDocument.documnetFilePath -- commented fo TC_CSS
                                        
                                        
                                        visaData.documentProgress = 3
                                        
                                        
                                        visaData.isVisaRequired = true
                                        
                                        
                                    }else
                                    {
                                        visaData.documentProgress = 2
                                        visaData.isVisaRequired = true
                                        
                                        printLog("Done/Reject but no visa required ")
                                        
                                        visaData.visaNotRequiresStatus = ""
                                        
                                    }
                                    
                                    
                                    
                                    
                                    
                                }else  if visaRequestStatus == Constant.Visa.TC_CSS_VISA_APPOINTMENT_SCHEDULED_1 || visaRequestStatus == Constant.Visa.TC_CSS_VISA_APPOINTMENT_SCHEDULED_2 || visaRequestStatus == Constant.Visa.TC_CSS_VISA_APPOINTMENT_SCHEDULED_3 || visaRequestStatus == Constant.Visa.TC_CSS_VISA_APPOINTMENT_SCHEDULED_4
                                    
                                    
                                    //if visaRequestStatus == Constant.Visa.VISA_IN_PROCESS_STATUS || visaRequestStatus == Constant.Visa.VISA_REJECTED_R_STATUS || visaRequestStatus == Constant.Visa.VISA_VOID_STATUS  || visaRequestStatus == Constant.Visa.VISA_READY_FOR_SUBMIT_STATUS  //-- commeted for TC_CSS
                                    
                                {
                                    
                                    /* let visaCoveringDocument = self.getVisaDocument(documentType: Constant.Visa.VISA_COVERING_LETTER_FORMAT_DOC, passengerNumber: passenger.passengerNumber!,visaCountry: visaCountry)
                                     
                                     visaData.isCoveringLetter = true //visaCoveringDocument.isDocumnetPresent
                                     
                                     visaData.coveringLetterUrl = visaCoveringDocument.documnetUrl
                                     
                                     visaData.coveringLetterDocumentPath = visaCoveringDocument.documnetFilePath*/
                                    
                                    visaData.isLetterFormat = true
                                    visaData.letterFormatURL = visaLetterFormatDocument.documnetUrl
                                    visaData.letterFormatDocumentPath = visaLetterFormatDocument.documnetFilePath
                                    visaData.isDocumentPending = false
                                    
                                    if self.isInterviewScheduleDisplay()
                                    {
                                        
                                        visaData.visaProggressStates = [VisaProccessTitle.DocumentPending.rawValue,VisaProccessTitle.DocumentsVerified.rawValue,VisaProccessTitle.InterviewScheduled.rawValue,VisaProccessTitle.VisaApplicationProcessed.rawValue]
                                    
                                        
                                        //let appointmnetDocument = self.getVisaDocument(documentType: Constant.Visa.APPONTMENT_LETTER_STATUS, passengerNumber: passenger.passengerNumber!,visaCountry: visaCountry) -- commented fo TC_CSS
                                        
                                        let appointmnetDocument = getVisaAppointmentLetterDocument(documentType: Constant.Visa.APPONTMENT_LETTER_STATUS, passengerNumber: passenger.passengerNumber!,visaCountry: visaCountry)
                                        
                                        visaData.visaProccessStatus = VisaProccessStatus.Completed
                                        
                                        /* if appointmnetDocument.isDocumnetPresent
                                         {
                                         visaData.visaProccessStatus = VisaProccessStatus.Completed
                                         
                                         visaData.documentProgress = 2
                                         }else
                                         {
                                         visaData.documentProgress = 1
                                         
                                         }*/ //-- commeted for TC_CSS
                                        
                                        visaData.visaProccessStatus = VisaProccessStatus.Completed
                                        
                                        visaData.documentProgress = 2
                                        
                                        visaData.isAppointmentLetter = appointmnetDocument.isDocumnetPresent
                                        
                                        visaData.appointmentLetters = appointmnetDocument.visaAppointmentLetters
                                        
                                        //visaData.appointmentLetterUrl = appointmnetDocument.documnetUrl -- commented fo TC_CSS
                                        
                                        //visaData.appointmentLetterDocumentPath = appointmnetDocument.documnetFilePath -- commented fo TC_CSS
                                        
                                        visaData.isVisaRequired = true
                                        
                                    }else
                                    {
                                        
                                        visaData.visaProccessStatus = VisaProccessStatus.Completed
                                        
                                        visaData.documentProgress = 1
                                        
                                        visaData.isVisaRequired = true
                                        printLog("IN PROCESS wihout visa req")
                                        visaData.visaNotRequiresStatus = ""
                                        
                                    }
                                    
                                    
                                    
                                }else{
                                    
                                    if self.isInterviewScheduleDisplay()
                                    {
                                        visaData.visaProggressStates = [VisaProccessTitle.DocumentPending.rawValue,VisaProccessTitle.DocumentsVerified.rawValue,VisaProccessTitle.InterviewScheduled.rawValue,VisaProccessTitle.VisaApplicationProcessed.rawValue]
                                    }else
                                    {
                                        visaData.visaProggressStates = [VisaProccessTitle.DocumentPending.rawValue,VisaProccessTitle.DocumentsVerified.rawValue,VisaProccessTitle.VisaApplicationProcessed.rawValue]
                                    }
                                    
                                   /* visaData.isAppointmentLetter = false
                                    visaData.isCoveringLetter = false
                                    visaData.documentProgress =  -1
                                    visaData.isVisaRequired = true
                                    printLog("Visa Request Status is other ")
                                    visaData.visaNotRequiresStatus = Constant.Visa.DOCUMENTS_PENDING_STATUS
                                    visaData.isLetterFormat = true
                                    visaData.isDocumentPending = true
                                    visaData.letterFormatURL = visaLetterFormatDocument.documnetUrl
                                    visaData.letterFormatDocumentPath = visaLetterFormatDocument.documnetFilePath
                                    visaData.visaProccessStatus = VisaProccessStatus.NotStarted*/
                                    
                                }
                            }else
                            {
                                if self.isInterviewScheduleDisplay()
                                {
                                    visaData.visaProggressStates = [VisaProccessTitle.DocumentPending.rawValue,VisaProccessTitle.DocumentsVerified.rawValue,VisaProccessTitle.InterviewScheduled.rawValue,VisaProccessTitle.VisaApplicationProcessed.rawValue]
                                }else
                                {
                                    visaData.visaProggressStates = [VisaProccessTitle.DocumentPending.rawValue,VisaProccessTitle.DocumentsVerified.rawValue,VisaProccessTitle.VisaApplicationProcessed.rawValue]
                                }
                                
                              /*  visaData.isAppointmentLetter = false
                                visaData.isCoveringLetter = false
                                visaData.documentProgress =  -1
                                visaData.isVisaRequired = true
                                visaData.isLetterFormat = true
                                visaData.letterFormatURL = visaLetterFormatDocument.documnetUrl
                                visaData.letterFormatDocumentPath = visaLetterFormatDocument.documnetFilePath
                                visaData.visaNotRequiresStatus = Constant.Visa.DOCUMENTS_PENDING_STATUS
                                visaData.isDocumentPending = true
                                visaData.visaProccessStatus = VisaProccessStatus.NotStarted
                                printLog("Visa Request Status is empty")*/
                            }
                            
                        }
                        
                        /*  }
                         else
                         {//
                         if self.isInterviewScheduleDisplay()
                         {
                         visaData.visaProggressStates = [VisaProccessTitle.DocumentPending.rawValue,VisaProccessTitle.DocumentsVerified.rawValue,VisaProccessTitle.InterviewScheduled.rawValue,VisaProccessTitle.VisaApplicationProcessed.rawValue]
                         }else
                         {
                         visaData.visaProggressStates = [VisaProccessTitle.DocumentPending.rawValue,VisaProccessTitle.DocumentsVerified.rawValue,VisaProccessTitle.VisaApplicationProcessed.rawValue]
                         }
                         
                         /* visaData.isAppointmentLetter = false
                         visaData.isCoveringLetter = false
                         visaData.isVisaRequired = false
                         visaData.visaNotRequiresStatus = "Document pending"
                         printLog("No visa Data for this")
                         
                         visaData.visaProccessStatus = VisaProccessStatus.NotStarted
                         visaData.documentProgress =   -1 //confirm : is interviewe scheduld */
                         
                         
                         
                         }*/ // --- commented for TC_CSS
                        // }
                        
                    }else{
                        
                        if self.isInterviewScheduleDisplay()
                        {
                            visaData.visaProggressStates = [VisaProccessTitle.DocumentPending.rawValue,VisaProccessTitle.DocumentsVerified.rawValue,VisaProccessTitle.InterviewScheduled.rawValue,VisaProccessTitle.VisaApplicationProcessed.rawValue]
                        }else
                        {
                            visaData.visaProggressStates = [VisaProccessTitle.DocumentPending.rawValue,VisaProccessTitle.DocumentsVerified.rawValue,VisaProccessTitle.VisaApplicationProcessed.rawValue]
                        }
                        
                        visaData.isAppointmentLetter = false
                        visaData.isCoveringLetter = false
                        visaData.isVisaRequired = true
                        visaData.isLetterFormat = true
                        visaData.isDocumentPending = true
                        visaData.letterFormatURL = visaLetterFormatDocument.documnetUrl
                        visaData.letterFormatDocumentPath = visaLetterFormatDocument.documnetFilePath
                        visaData.visaNotRequiresStatus = ""//Constant.Visa.DOCUMENTS_PENDING_STATUS
                        visaData.isDocumentPending = true
                        
                        visaData.documentProgress =   -1
                        
                        visaData.visaProccessStatus = VisaProccessStatus.NotStarted
                        
                    }
                    
                }else{
                    
                    if self.isInterviewScheduleDisplay()
                    {
                        visaData.visaProggressStates = [VisaProccessTitle.DocumentPending.rawValue,VisaProccessTitle.DocumentsVerified.rawValue,VisaProccessTitle.InterviewScheduled.rawValue,VisaProccessTitle.VisaApplicationProcessed.rawValue]
                    }else
                    {
                        visaData.visaProggressStates = [VisaProccessTitle.DocumentPending.rawValue,VisaProccessTitle.DocumentsVerified.rawValue,VisaProccessTitle.VisaApplicationProcessed.rawValue]
                    }
                    
                    visaData.isAppointmentLetter = false
                    visaData.isCoveringLetter = false
                    visaData.isVisaRequired = true
                    visaData.isLetterFormat = true
                    visaData.isDocumentPending = true
                    visaData.letterFormatURL = visaLetterFormatDocument.documnetUrl
                    visaData.letterFormatDocumentPath = visaLetterFormatDocument.documnetFilePath
                    visaData.documentProgress =  -1
                    visaData.visaProccessStatus = VisaProccessStatus.NotStarted
                    visaData.visaNotRequiresStatus = ""//Constant.Visa.DOCUMENTS_PENDING_STATUS
                    
                } // else visa documnetStatus is nil/empty
                
                visaDetailsArray.append(visaData)
                
            }
        }
        
        return visaDetailsArray
        
    }
    
    
    
    func getVisaAppointmentLetterDocument(documentType:String,passengerNumber:String,visaCountry:String) -> (visaAppointmentLetters:[DocumentDetails],isDocumnetPresent:Bool)
    {
        
        var visaAppointmentLetters:[DocumentDetails] = []
        
        var isDocumnetPresent:Bool = false
        
        if let passengerDocumnetsArray:Array<TourPassengerDouments> = self.tourDetails?.passengerDocumentRelation?.allObjects as? Array<TourPassengerDouments>
        {
            let passengerDocumentArray:Array<TourPassengerDouments> =  passengerDocumnetsArray.filter(){ /*$0.passengerNumber! == passengerNumber &&*/ $0.documentType?.lowercased() == documentType}
            
            
            if passengerDocumentArray.count > 0
            {
                isDocumnetPresent = true
                
                var index:Int = 0
                
                for passengerDocument in passengerDocumentArray
                {
                    index = index + 1
                    
                    let ticketNumber:String = "\(documentType)_\( index)"
                    
                    let fileName:String = "\(documentType)_\(index).pdf"
                    
                    var visaAppointmentLetterData:DocumentDetails = DocumentDetails()
                    
                    //let passengerDocument:TourPassengerDouments = passengerDocumentArray.first!
                    
                    if passengerDocument.docummentURL != nil && !(passengerDocument.docummentURL?.isEmpty)!
                    {
                        let documentPath = TourDocumentController.createFolderStructureForBFNumber(bfNumber: (tourDetails?.bfNumber)!)
                        
                        let fileDocumentPath = documentPath.visaPath + "/" + passengerNumber + "/" + visaCountry + fileName
                        
                        
                        
                        visaAppointmentLetterData.displayTitle = "Visa \(index)"
                        visaAppointmentLetterData.downloadUrl = passengerDocument.docummentURL!
                        visaAppointmentLetterData.documnetUrl = fileDocumentPath
                        
                      
                        
                        visaAppointmentLetters.append(visaAppointmentLetterData)
                    }
                }
            }
            
        }
        return (visaAppointmentLetters,isDocumnetPresent)
    }
    
    
    
    func getVisaDocument(documentType:String,passengerNumber:String,visaCountry:String) -> (documnetUrl:String,documnetFilePath:String,isDocumnetPresent:Bool)
    {
        if documentType == Constant.Visa.VISA_COVERING_LETTER_FORMAT_DOC || documentType == Constant.Visa.LETTER_FORMAT_DOC
        {
            
            let documentPath = TourDocumentController.createFolderStructureForBFNumber(bfNumber: (tourDetails?.bfNumber)!)
            
            let fileDocumentPath = documentPath.visaPath /*+ "" + passengerNumber */ + "/" + visaCountry + documentType + ".pdf"
            
            
            return (DocumentsURLConstant.LETTER_FORMAT_URL,fileDocumentPath,true)
            
            
        }
        
        return (documnetUrl:"","",false)
    }
    
    func getUniqueVisaType() -> Array<String> {
        
        if let visaDetailsArray:Array<Visa> = self.tourDetails?.visaRelation?.allObjects as? Array<Visa>
        {
            
            let uniqueVisaType = visaDetailsArray.compactMap { $0.visaType }
            
            let uniqueVisaSet:Set<String> = Set(uniqueVisaType)
            //array.sorted {$0.localizedStandardCompare($1) == .orderedAscending}
            
            return Array(uniqueVisaSet)
            
        }else{
            
            return []
            
        }
        
    }
    
    func getUniqueVisaCountry() -> Array<String> {
        
        
        if self.tourDetails?.visaCountryRelation?.allObjects != nil && (self.tourDetails?.visaCountryRelation?.allObjects.count)! > 0 {
            
            let uniqueVisaCountry = (self.tourDetails?.visaCountryRelation?.allObjects as! Array<VisaCountryMaster>).compactMap { $0.countryName }
            
            let uniqueVisaSet:Set<String> = Set(uniqueVisaCountry)
            
            
            
            return  self.sortVisaCountry(visaCountry:Array(uniqueVisaSet))
            
        }
        else if let visaDetailsArray:Array<Visa> = self.tourDetails?.visaRelation?.allObjects as? Array<Visa>
        {
            
            let uniqueVisaCountry = visaDetailsArray.compactMap { $0.visaCountry }
            
            let uniqueVisaSet:Set<String> = Set(uniqueVisaCountry)
            //array.sorted {$0.localizedStandardCompare($1) == .orderedAscending}
            
            return self.sortVisaCountry(visaCountry:Array(uniqueVisaSet))
            
        }else{
            
            return []
            
        }
        
    }
    
    func sortVisaCountry(visaCountry:Array<String>) -> Array<String> {
        
        if visaCountry != nil && visaCountry.count > 0
        {
            let sortedVisaCountry = visaCountry.sorted { (visaCountry, visaCountry1) -> Bool in
                
                visaCountry < visaCountry1
                
            }
            
            return sortedVisaCountry
        }
        
        return []
        
    }
    
    func setDefaultVisaDetails()
    {
        let visaTypeArray:Array<String> = self.getUniqueVisaCountry()//self.getUniqueVisaType()
        
        if visaTypeArray.count > 0
        {
            self.visaTypeTextField.text = visaTypeArray.first!
            
            /* let filtedVisaDetails = self.getVisaDetailsByVisaCountry(visaCountry: visaTypeArray.first!)
             
             if filtedVisaDetails.count > 0
             {
             self.visaDetailsDataArray = self.createVisaDetailsWithStatus(filterdVisaArray:filtedVisaDetails)
             
             } -- old requ logic  */
            
            self.visaDetailsDataArray = self.createVisaDetailsWithStatus(visaCountry: visaTypeArray.first!)
            
            
        }else
        {
            self.visaDetailsDataArray = self.createVisaDetailsWithStatus(visaCountry: "")
        }
    }
    
    //MARK: - Custom Button Click
    
    @objc func onLetterFormatButtonClick(button:UIButton)  {
        
        
        
        let buttonPosition = button.convert(CGPoint(), to: visaProgressTableView)
        
        if  let indexPath = self.visaProgressTableView.indexPathForRow(at: buttonPosition)
        {
            if let visaData = visaDetailsDataArray?[(indexPath.row)]
            {
                if visaData.isLetterFormat
                {
                    self.downloadDocument(filePath: visaData.letterFormatDocumentPath!, url: visaData.letterFormatURL!)
                }else
                {
                    self.displayAlert(message: AlertMessage.NO_DOCUMENT)
                }
            }else
            {
                self.displayAlert(message: AlertMessage.NO_DOCUMENT)
            }
        }else
        {
            self.displayAlert(message: AlertMessage.NO_DOCUMENT)
        }
        
    }
    
    @objc func onCoveringLetterButtonClick(button:UIButton)  {
        
        
        
        let buttonPosition = button.convert(CGPoint(), to: visaProgressTableView)
        
        if  let indexPath = self.visaProgressTableView.indexPathForRow(at: buttonPosition)
        {
            if let visaData = visaDetailsDataArray?[(indexPath.row)]
            {
                if visaData.isCoveringLetter
                {
                    self.downloadDocument(filePath: visaData.coveringLetterDocumentPath!, url: visaData.coveringLetterUrl!)
                }else
                {
                    self.displayAlert(message: AlertMessage.NO_DOCUMENT)
                }
            }else
            {
                self.displayAlert(message: AlertMessage.NO_DOCUMENT)
            }
        }else
        {
            self.displayAlert(message: AlertMessage.NO_DOCUMENT)
        }
        
    }
    
    @objc func onAppointmentLetterButtonClick(button:UIButton)  {
        
        
       /* if UserBookingController.isPaymentDone(by: "INR", tourDetails: self.tourDetails!)   //isPaymentDonePaymentFlag(by: self.tourDetails!)
        {*/// ---  Commented for TC_CSS APP
            
            let buttonPosition = button.convert(CGPoint(), to: visaProgressTableView)
        
        self.sourceView = button
            
            if  let indexPath = self.visaProgressTableView.indexPathForRow(at: buttonPosition)
            {
                if let visaData = visaDetailsDataArray?[(indexPath.row)]
                {
                    if visaData.isAppointmentLetter
                    {
                        if visaData.appointmentLetters?.count ?? 0 > 1
                        {
                            self.presentPopover(passengerDocumnets: visaData.appointmentLetters ?? [])
                            
                        }else if visaData.appointmentLetters?.count ?? 0 == 1
                        {
                            let documnet:DocumentDetails = (visaData.appointmentLetters?.first)!
                            
                            //self.viewDownloadedFile(fileurl: URL(fileURLWithPath:documnet.documnetUrl!))
                            
                            self.downloadDocument(filePath: documnet.documnetUrl!, url: documnet.downloadUrl!)
                            
                        //self.downloadDocument(filePath: visaData.appointmentLetterDocumentPath!, url: visaData.appointmentLetterUrl!)
                            
                        }else
                        {
                            
                            self.displayAlert(message: AlertMessage.NO_DOCUMENT)
                        }
                    }else
                    {
                        self.displayAlert(message: AlertMessage.NO_DOCUMENT)
                    }
                }else
                {
                    self.displayAlert(message: AlertMessage.NO_DOCUMENT)
                }
            }else
            {
                self.displayAlert(message: AlertMessage.NO_DOCUMENT)
            }
       /* }else
        {
            self.displayAlert(message: AlertMessage.Visa.COMPLETE_INR_PAYMENT)
        }*/
        //}
    }
    
    //MARK: UIDocumentInteractionController Delegate
    
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController
    {
        return self
    }
    
    func viewDownloadedFile(fileurl : URL) -> Void
    {
        
        let concurrentQueue = DispatchQueue(label: "queuename", attributes: .concurrent)
        concurrentQueue.sync {
            
            //DispatchQueue.main.sync {
            
            let documentController : UIDocumentInteractionController = UIDocumentInteractionController.init(url: fileurl)
            
            //self.documentController.uti =
            documentController.name  = Title.VIEW_PDF
            documentController.delegate = self
            
            documentController.presentPreview(animated: true)
            
            LoadingIndicatorView.hide()
            
            // }
            
        }
        
    }
    
    func downloadDocument(filePath:String ,url:String) {
        
        //  let passengerDocumnet:TourPassengerDouments =  documentArray.first!
        
        LoadingIndicatorView.show("Loading")
        
        NetworkCommunication.downloadDocuments(urlString: url, destinationFilePath: filePath, completion: { (downlodedfilePath, status) in
            
            printLog(status,filePath);
            
            if(status)
            {
                self.viewDownloadedFile(fileurl: URL(fileURLWithPath:filePath))
                
            }
            else{
                
                let isFilePresent = TourDocumentController.isFileExistInDocumentDirectory(fileNamePath: filePath)
                
                if(isFilePresent.chkFile)
                {
                    self.viewDownloadedFile(fileurl: URL(fileURLWithPath:filePath))
                    
                }else{
                    
                    // DispatchQueue.main.sync {
                    
                    LoadingIndicatorView.hide()
                    
                    self.displayAlert(message: AlertMessage.DOCUMENT_DOWNLOAD_FAIL)
                    // }
                    
                }
                
            }
            
            
            
            
        })
    }
    
    
    func displayAlert(message:String)
    {
        let alert = UIAlertController(title: "Alert", message:message , preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: AlertMessage.TITLE_OK, style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func visaDocumentButtonClick()
    {
        // if let visaDocUrl:String =  tourDetails?.visaDocUrl
        // {
        //"http://uat2.thomascook.in/soto/lt/productitineraryDownload.do?itineraryCode=2017EUR228S1&filePath=Master Documentation checklist for Visa.pdf&docType=Visa Requirement"
        
        /* let documentFolderPath =  TourDocumentController.getTourFileDirectoryPath(bfnNumber: (tourDetails?.bfNumber)!)
         
         let visaDocFilePath = documentFolderPath.bfNumberPath + "/" + "visaDocUrl.pdf"
         
         self.downloadDocument(filePath: visaDocFilePath, url: visaDocUrl)
         
         }else{
         
         self.displayAlert(message: AlertMessage.NO_DOCUMENT)
         }*/
   
        
     /*   if let tourCode = tourDetails?.tourCode
        {
            
            let countryCode = filterTourCode(tourCode: tourCode,uptoLength: 3)
            
            if countryCode != ""
            {
                if countryCode.uppercased().contains(Constant.USA_CONTRY_CODE) {
                    
                    url = DocumentsURLConstant.VISA_REQUIREMENTS_USA_CANADA ;
                    
                } else if countryCode.uppercased().contains(Constant.EXO_CONTRY_CODE) {
                    
                    url = DocumentsURLConstant.VISA_REQUIREMENTS_EXO;
                    
                } else if countryCode.uppercased().contains(Constant.FAR_CONTRY_CODE) {
                    
                    url = DocumentsURLConstant.VISA_REQUIREMENTS_FAR;
                    
                } else if countryCode.uppercased().contains(Constant.EUR_CONTRY_CODE) {
                    
                    url = DocumentsURLConstant.VISA_REQUIREMENTS_EUR;
                }
                
            }
            
        }*/
        
        // }
        
        
         if let letterFormatUrl:String = DocumentsURLConstant.LETTER_FORMAT_URL  //self.tourDetails?.visaDocUrl
         {
        
        /*if url != ""
        {*/
            let documentFolderPath =  TourDocumentController.getTourFileDirectoryPath(bfnNumber: (tourDetails?.bfNumber)!)
            
            let visaDocFilePath = documentFolderPath.visaPath + "/" + "visaDocUrl.pdf"
            
            self.downloadDocument(filePath: visaDocFilePath, url: letterFormatUrl)
            
            
        }else
        {
            self.displayAlert(message: AlertMessage.NO_DOCUMENT)
        }
        
        
    }
    
    func isVisaRequired (visaCountry:String) -> Bool
    {
        let visaNotRequredCountries:Array<String> = self.getVisaNotRequiredCountries()
        
        if visaNotRequredCountries.count > 0
        {
            
            if visaNotRequredCountries.contains(visaCountry.lowercased())
            {
                return false
            }
        }
        
        return true
    }
    
    func isInterviewScheduleDisplay() -> Bool
    {
        if let regionCode = tourDetails?.region
        {
            
            let countryCode = regionCode //filterTourCode(tourCode: tourCode,uptoLength: 3)
            
            if countryCode != ""
            {
                if countryCode.uppercased().contains(Constant.EUR_CONTRY_CODE_AGD) ||  countryCode.uppercased().contains(Constant.EUR_CONTRY_CODE_EUROPE) ||  countryCode.uppercased().contains(Constant.EUR_CONTRY_CODE_EUROPEHS) || countryCode.uppercased().contains(Constant.USA_CONTRY_CODE_HSDEMO) || countryCode.uppercased().contains(Constant.USA_CONTRY_CODE_SOUTHAMERICA) || countryCode.uppercased().contains(Constant.USA_CONTRY_CODE_USA) || countryCode.uppercased().contains(Constant.USA_CONTRY_CODE_USAFIT) || countryCode.uppercased().contains(Constant.USA_CONTRY_CODE_USAHS) || countryCode.uppercased().contains(Constant.USA_CONTRY_CODE_USAHSA)

                {
                    return true
                }
            }
            
        }
        
        return false
        
    }
    
    func filterTourCode(tourCode:String,uptoLength:Int) -> String
    {
        
        if !((tourCode.isEmpty))
        {
            let first4 = tourCode.substring(to:tourCode.index(tourCode.startIndex, offsetBy: 3))
            
            return first4
            
        }
        
        return ""
        
        
    }
    
    func getVisaNotRequiredCountries() -> Array<String>
    {
        
        return ["thailand",
                
                "mauritius",
                
                "ecuador",
                
                "british virgin islands",
                
                "cook islands","bhutan",
                
                "seychelles",
                
                "dominica",
                
                "haiti",
                
                "el salvador","cambodia",
                
                "togo",
                
                "bolivia",
                
                "st lucia",
                
                "fiji","maldives",
                
                "kenya",
                
                "guyana",
                
                "jamaica",
                
                "micronesia","macau",
                
                "tanzania",
                
                "saint kitts and nevis",
                
                "vanuatu","indonesia",
                
                "ethiopia",
                
                "saint vincent and the grenadines",
                
                "samoa","iraq",
                
                "madagascar",
                
                "grenada",
                
                "tuvalu","nepal",
                
                "mozambique",
                
                "trinidad & tobago",
                
                "nieu","laos",
                
                "uganda",
                
                "montserrat",
                
                "palau","jordan",
                
                "guinea-bissau",
                
                "nicaragua","timor leste",
                
                "cape verde",
                
                "turks & caicos",
                
                "comoros islands"]
        
    }
    
 func isVisaDocPresent() -> Bool {
        
        if let letterFormatUrl:String = self.tourDetails?.visaDocUrl
        {
            if !letterFormatUrl.isEmpty && letterFormatUrl != ""
            {
                return true
            
            }
        }
        
        return false
        
    }
    
    
    func presentPopover(passengerDocumnets:Array<DocumentDetails>) {
        
        
        let popoverContentController = DocumentListViewController(nibName: "DocumentListViewController", bundle: nil)
        
        popoverContentController.delegate = self
        
        popoverContentController.presentingView = self
        // Set your popover size.
        popoverContentController.preferredContentSize = CGSize(width: /*(UIScreen.main.bounds.width/2) + 100*/210, height: /*(UIScreen.main.bounds.height/3) + 100*/200)
        
        // Set the presentation style to modal so that the above methods get called.
        popoverContentController.modalPresentationStyle = .popover
        
        // Set the popover presentation controller delegate so that the above methods get called.
        popoverContentController.popoverPresentationController!.delegate = self
        
        popoverContentController.documnetListArray = passengerDocumnets
        
        // Present the popover.
        self.present(popoverContentController, animated: true, completion: nil)
        
    }
    
    //MARK: UIPopoverPresentationController Delegate
    
    
    dynamic func presentationController(_ presentationController: UIPresentationController, willPresentWithAdaptiveStyle style: UIModalPresentationStyle, transitionCoordinator: UIViewControllerTransitionCoordinator?) {
        // add a semi-transparent view to parent view when presenting the popover
        let parentView = presentationController.presentingViewController.view
        
        let overlay = UIView(frame: (parentView?.bounds)!)
        overlay.backgroundColor = UIColor(white: 0.0, alpha: 0.4)
        parentView?.addSubview(overlay)
        
        let views: [String: UIView] = ["parentView": parentView!, "overlay": overlay]
        
        parentView?.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[overlay]|", options: [], metrics: nil, views: views))
        parentView?.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[overlay]|", options: [], metrics: nil, views: views))
        
        overlay.alpha = 0.0
        
        transitionCoordinator?.animate(alongsideTransition: { _ in
            overlay.alpha = 1.0
        }, completion: nil)
        
        self.overlay = overlay
    }
    
    func prepareForPopoverPresentation(_ popoverPresentationController: UIPopoverPresentationController) {
        
        popoverPresentationController.permittedArrowDirections = .any
        popoverPresentationController.sourceView =  self.sourceView//self.view
        popoverPresentationController.sourceRect = CGRect(x: 20, y: 0, width: 0, height: 0)
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    func popoverPresentationControllerDidDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) {
        
        
        guard let overlay = overlay else {
            return
        }
        DispatchQueue.main.async() {
            UIView.animate(withDuration: 0.2, animations: {
                overlay.alpha = 0.0
            }, completion: { _ in
                overlay.removeFromSuperview()
            })
        }
        
    }
    
    func removeOverLay() {
        
        
        guard let overlay = self.overlay else {
            return
        }
        DispatchQueue.main.async() {
            UIView.animate(withDuration: 0.2, animations: {
                overlay.alpha = 0.0
            }, completion: { _ in
                overlay.removeFromSuperview()
            })
        }
        
    }
    
    //DocumentListViewDelegate delegate methods
    
    func selectedDocument(documnetUrl:String, downloadUrl:String)
    {
        self.removeOverLay();
        
        self.downloadDocument(filePath: documnetUrl, url: downloadUrl)
        
        
    }
    
    
}

struct VisaAppointmentLetterData
{
    var appointmentLetterUrl:String?
    var appointmentLetterDocumentPath:String?
    
    init() { }
    
}

struct VisaDetailsData {
    
    var passengerName:String?
    var visaNotRequiresStatus:String?
    var coveringLetterUrl:String?
    var letterFormatURL:String?
    var appointmentLetterUrl:String?
    var isVisaRequired:Bool = false // to display doc pending View
    var isCoveringLetter:Bool = false
    var isLetterFormat:Bool = false
    var isDocumentPending:Bool = false
    var isAppointmentLetter:Bool = false // to display Appointment Letter btn
    var coveringLetterDocumentPath:String?
    var letterFormatDocumentPath:String?
    var appointmentLetterDocumentPath:String?
    var documentProgress:Int = -1
    
    
    var appointmentLetters:[DocumentDetails]?
    
    var visaProccessStatus:VisaProccessStatus?
    lazy var visaProggressStates = [String]()
    
    
    init() { }
    
}

public enum VisaProccessStatus: String {
    
    case NotStarted = "NotStarted"
    case InProgress     = "InProgress"
    case Completed    = "Completed"
    case Failed    = "Failed"
    
}
public enum VisaProccessTitle: String {
    
    case DocumentPending = "Documents Recived"
    case DocumentsVerified     = "Documents Verified"
    case InterviewScheduled    = "Interview Scheduled"
    case VisaApplicationProcessed    = "Visa Application Processed"
    
}



