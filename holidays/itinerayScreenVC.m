//
//  itinerayScreenVC.m
//  holidays
//
//  Created by Kush Thakkar on 21/02/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//
//0054A5
#import "itinerayScreenVC.h"
#import "NumberTableViewCell.h"
#import "PlaceCollectionViewCell.h"
#import "BookNowViewController.h"
#import "UIImageView+WebCache.h"
#import "ItineraryDetailsView.h"
#import "ColourClass.h"
//#define DEFAULT_BLUE_COLOUR @"#0054A5"
#define DEFAULT_BLUE_COLOUR @"#005FAE"
@interface itinerayScreenVC ()
{
    ItineraryDetailsView *itineraryDetailsView;
    BOOL flag;
    NSInteger selectedOption;
}
@end

@implementation itinerayScreenVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.tableView registerNib:[UINib nibWithNibName:@"NumberTableViewCell" bundle:nil] forCellReuseIdentifier:@"NumberTableViewCell"];
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"PlaceCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"PlaceCollectionViewCell"];
    
    
    NSSortDescriptor *brandDescriptor = [[NSSortDescriptor alloc] initWithKey:@"itineraryDay" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:brandDescriptor];
    NSArray *sortedArray = [_holidayPackageDetail.arrayItinerary sortedArrayUsingDescriptors:sortDescriptors];

    _holidayPackageDetail.arrayItinerary = sortedArray;
    
    NSLog(@"%@",_holidayPackageDetail.arrayItinerary);
    selectedOption = 0;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    //Srtting first row selected
    [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
}


- (IBAction)backButtonPressed:(UIButton *)sender {
    [self jumpToPreviousViewController];
}

#pragma mark - UITableView DataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [_holidayPackageDetail.arrayItinerary count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NumberTableViewCell* cell = (NumberTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"NumberTableViewCell"];
//    NSDictionary * dict = _holidayPackageDetail.arrayItinerary[indexPath.row];
//    [cell.numberButton setTitle: [NSString stringWithFormat:@"%@",(NSNumber *)dict[@"itineraryDay"]] forState:UIControlStateNormal];
    /*
    NSIndexPath* selection = [tableView indexPathForSelectedRow];
    NumberTableViewCell *cellSelected = [tableView cellForRowAtIndexPath:indexPath];
    cellSelected.numberButton.tag = indexPath.row;
    [cellSelected.numberButton addTarget:self action:@selector(buttonSelected:) forControlEvents:UIControlEventTouchUpInside];
    
    if(selection && selection.row == indexPath.row){
        [cell.numberButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        cell.numberButton.backgroundColor = [ColourClass colorWithHexString:@"#0054A5"];
    }else {
        [cell.numberButton setTitleColor:[ColourClass colorWithHexString:@"#0054A5"] forState:UIControlStateNormal];
        cell.numberButton.backgroundColor = [UIColor whiteColor];
    }
     */
    cell.backgroundColor = [UIColor clearColor];
    [cell.numberButton setTitle:[NSString stringWithFormat:@"%ld",indexPath.row+1] forState:UIControlStateNormal];
    cell.numberButton.tag = indexPath.row;
//    [cell.numberButton setTitleColor:(indexPath.row == selectedOption) ? [UIColor whiteColor] : [UIColor colorWithRed:0 green:83/255.0 blue:169/255.0 alpha:1.0] forState:UIControlStateNormal];
//    cell.numberButton.backgroundColor = (indexPath.row == selectedOption) ? [UIColor colorWithRed:0 green:83/255.0 blue:169/255.0 alpha:1.0] : [UIColor whiteColor];
    
    
    [cell.numberButton addTarget:self action:@selector(buttonSelected:) forControlEvents:UIControlEventTouchUpInside];
    [cell.numberButton setTitleColor:(indexPath.row == selectedOption) ? [UIColor whiteColor] : [UIColor colorWithRed:0 green:83/255.0 blue:169/255.0 alpha:1.0] forState:UIControlStateNormal];
    cell.numberButton.backgroundColor = (indexPath.row == selectedOption) ? [UIColor colorWithRed:0 green:83/255.0 blue:169/255.0 alpha:1.0] : [UIColor whiteColor];
//    [cell.numberButton setTitleColor:(indexPath.row == selectedOption) ? [UIColor whiteColor] : [ColourClass colorWithHexString:@"#0054A5"] forState:UIControlStateNormal];
//    cell.numberButton.backgroundColor = (indexPath.row == selectedOption) ? [ColourClass colorWithHexString:@"#0054A5"] : [UIColor whiteColor];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 100;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(NumberTableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
//    int arrayCount = (int)_holidayPackageDetail.arrayItinerary.count - 1;
    if(indexPath.row == 0){
        cell.upperView.hidden = YES;
    }
    else if (indexPath.row == [_holidayPackageDetail.arrayItinerary count]-1){
        cell.bottomView.hidden = YES;
    }else{
        cell.upperView.hidden = NO;
        cell.bottomView.hidden = NO;
    }

}
/*

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
   
    
    NumberTableViewCell *cellSelected = [tableView cellForRowAtIndexPath:indexPath];
    
    for(int i = 0 ; i<[_holidayPackageDetail.arrayItinerary count] ; i++){
        NumberTableViewCell *cellUnselected = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
        
        if(cellSelected!=cellUnselected){

            [cellUnselected.numberButton setTitleColor:[ColourClass colorWithHexString:@"#0037A3"] forState:UIControlStateNormal];
            cellUnselected.numberButton.backgroundColor = [UIColor whiteColor];
            
        }else{
            
            [cellSelected.numberButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            cellSelected.numberButton.backgroundColor = [ColourClass colorWithHexString:@"#0037A3"];
            [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForItem:indexPath.row inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
            [self.collectionView
            selectItemAtIndexPath:[NSIndexPath indexPathForItem:indexPath.row inSection:0]
                         animated:YES
                   scrollPosition:UICollectionViewScrollPositionCenteredVertically];
            
        }
    }
}
*/
#pragma mark - Helper Methods

-(void)buttonSelected:(UIButton*)sender
{
    /*
    NumberTableViewCell *cellSelected = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathWithIndex:sender.tag]];
    
    for(int i = 0 ; i<[_holidayPackageDetail.arrayItinerary count] ; i++){
        NumberTableViewCell *cellUnselected = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
        if(cellSelected!=cellUnselected){
            [cellUnselected.numberButton setTitleColor:[ColourClass colorWithHexString:@"#0037A3"] forState:UIControlStateNormal];
            cellUnselected.numberButton.backgroundColor = [UIColor whiteColor];
        }else{
            [cellSelected.numberButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            cellSelected.numberButton.backgroundColor = [ColourClass colorWithHexString:@"#0037A3"];
            [self.collectionView
            selectItemAtIndexPath:[NSIndexPath indexPathForItem:[NSIndexPath indexPathWithIndex:sender.tag].row inSection:0]
                         animated:YES
                   scrollPosition:UICollectionViewScrollPositionCenteredVertically];
            
        }
    }
    */
    selectedOption = sender.tag;
    [self.collectionView
     selectItemAtIndexPath:[NSIndexPath indexPathForItem:sender.tag inSection:0]
                 animated:YES
           scrollPosition:UICollectionViewScrollPositionCenteredVertically];
    [self.tableView reloadData];
    
}

-(void)addButtonClicked:(UIButton*)sender
{
    NSDictionary * dict = _holidayPackageDetail.arrayItinerary[sender.tag];

//    NSAttributedString *hotelStr = [[NSAttributedString alloc] initWithData:[dict[@"overnightStay"] dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
//
//    NSAttributedString *mealStr = [[NSAttributedString alloc] initWithData:[dict[@"mealDescription"] dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    
    NSAttributedString *descStr = [[NSAttributedString alloc] initWithData:[dict[@"itineraryDescription"] dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    
    itineraryDetailsView = [[[NSBundle bundleForClass:[self class]]loadNibNamed:@"ItineraryDetailsView" owner:self
    options:nil]firstObject];
    itineraryDetailsView.frame = CGRectMake(0,0, self.view.frame.size.width,self.view.frame.size.height);
//    itineraryDetailsView.itineraryDetailTextView.text = [NSString stringWithFormat:@"%@\n\n%@",hotelStr.string,mealStr.string];
    itineraryDetailsView.itineraryDetailTextView.text = [NSString stringWithFormat:@"%@\n",descStr.string];
    itineraryDetailsView.itineraryDetailTextView.editable = NO;
    itineraryDetailsView.itineraryDetailTextView.selectable = NO;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        itineraryDetailsView.cancelButton.layer.cornerRadius = itineraryDetailsView.cancelButton.frame.size.height*0.5;
    });
    
    
    [itineraryDetailsView.cancelButton addTarget:self action:@selector(cancelButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [UIView transitionWithView:self.view
    duration:1.0
    options: UIViewAnimationOptionCurveEaseIn
                    animations:^{ [self.view addSubview:itineraryDetailsView];  }
                    completion:NULL];

}

-(void)cancelButtonClicked:(UIButton*)sender{
    
    [UIView transitionWithView:self.view
    duration:1.0
    options: UIViewAnimationOptionCurveEaseOut
    animations:^{  itineraryDetailsView.frame = CGRectMake(self.view.frame.size.width*2,0, self.view.frame.size.width,self.view.frame.size.height); }
    completion:NULL];
    
}

#pragma mark - collectionview datasource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return [_holidayPackageDetail.arrayItinerary count];
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    PlaceCollectionViewCell *selectedCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PlaceCollectionViewCell" forIndexPath:indexPath];
    
    if(!selectedCell){
        selectedCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PlaceCollectionViewCell" forIndexPath:indexPath];
    }
    
    selectedCell.addButton.tag = indexPath.row;
    [selectedCell.addButton addTarget:self action:@selector(addButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    NSDictionary * dict = _holidayPackageDetail.arrayItinerary[indexPath.row];
    NSString* encodedText = [dict[@"image"] stringByReplacingOccurrencesOfString:@" " withString:@"_"];
    NSString *stringUrl = [NSString stringWithFormat:@"%@/images/holidays/%@/itinerary/%@",kUrlForImage,self.holidayPackageDetail.strPackageId,encodedText];
    NSString *placeNumber = [NSString stringWithFormat:@"%ld",indexPath.row+1];
    NSString *cityName = [NSString stringWithFormat:@"%@",dict[@"cityCode"][@"cityName"]];
    selectedCell.placeNumLabel.text = placeNumber;
    selectedCell.placeNameLabel.text = cityName;
    NSURL *url = [NSURL URLWithString:stringUrl];
    if (url != nil)
    {
        [selectedCell.placeImageView sd_setImageWithURL:url
                       placeholderImage:[UIImage imageNamed:@"defaultBanner"] options:/* DISABLES CODE */ (0) == 0 ? SDWebImageRefreshCached : 0];
    }
    
//    NSAttributedString *infoStr = [[NSAttributedString alloc] initWithData:[dict[@"itineraryDescription"] dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
//    selectedCell.infoTextView.text  = [NSString stringWithFormat:@"%@",infoStr.string];
    
    UIFont *font = [UIFont fontWithName:@"Lato-Regular" size:14.0];
    NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:font
                                                                forKey:NSFontAttributeName];
    NSAttributedString *hotelStr = [[NSAttributedString alloc] initWithData:[dict[@"overnightStay"] dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSFontAttributeName : attrsDictionary } documentAttributes:nil error:nil];
    NSAttributedString *mealStr = [[NSAttributedString alloc] initWithData:[dict[@"mealDescription"] dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSFontAttributeName : attrsDictionary } documentAttributes:nil error:nil];
    selectedCell.infoTextView.text  = [NSString stringWithFormat:@"%@\n\n%@",hotelStr.string,mealStr.string];
    
    
    selectedCell.infoTextView.editable = NO;
    selectedCell.infoTextView.selectable = NO;
    
    return selectedCell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {

    return CGSizeMake(self.collectionView.bounds.size.width,self.collectionView.bounds.size.height);
    
}

- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout *)collectionViewLayout
        minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
/*
    for (UICollectionViewCell *cell in [self.collectionView visibleCells]) {

        NSIndexPath *indexPath = [self.collectionView indexPathForCell:cell];

        for(int i=0 ; i<[_holidayPackageDetail.arrayItinerary count] ; i++){
            if(i==indexPath.row){
                NumberTableViewCell *cellSelected = [self.tableView cellForRowAtIndexPath:indexPath];
                [cellSelected.numberButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                cellSelected.numberButton.backgroundColor = [ColourClass colorWithHexString:@"#0037A3"];
                [self.tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
            }else{
                NumberTableViewCell *cellUnselected = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:i inSection:0]];
                [cellUnselected.numberButton setTitleColor:[ColourClass colorWithHexString:@"#0037A3"] forState:UIControlStateNormal];
                cellUnselected.numberButton.backgroundColor = [UIColor whiteColor];
            }
        }
        if(indexPath.row >((int)[_holidayPackageDetail.arrayItinerary count]/2) || indexPath.row < ((int)[_holidayPackageDetail.arrayItinerary  count]/2))
        {
            [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionNone animated:YES];
        }
    }
    */
    
    if(scrollView.contentOffset.x>=0){
        CGFloat viewHty = _collectionView.frame.size.height;
        int currentPage = scrollView.contentOffset.y / viewHty;
        NSLog(@"currentPage %i",currentPage);
        selectedOption = currentPage;
        [self.collectionView reloadData];
        [self.tableView reloadData];
    }
    
}


@end
