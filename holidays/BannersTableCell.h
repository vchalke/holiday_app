//
//  BannersTableCell.h
//  holidays
//
//  Created by Ios_Team on 29/10/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BannersTableCell : UITableViewCell<UICollectionViewDelegate,UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UILabel *lbl_BannerName;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionViews;
-(void)setupCollectionView;
@end

NS_ASSUME_NONNULL_END
