//
//  FlightsColectObject.m
//  holidays
//
//  Created by Kush_Team on 13/07/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "FlightsColectObject.h"

@implementation FlightsColectObject
-(instancetype)initWithFlightObjectDict:(NSDictionary *)dictionary{
    if ([super init])
    {
        self.airlineImage = [dictionary valueForKey:@"airlineImage"];
        self.airlineName = [dictionary valueForKey:@"airlineName"];
        self.arrivalCity = [dictionary valueForKey:@"arrivalCity"];
        self.arrivalTime = [dictionary valueForKey:@"arrivalTime"];
        self.departureCity = [dictionary valueForKey:@"departureCity"];
        self.departureTime = [dictionary valueForKey:@"departureTime"];
        self.destinationCity = [dictionary valueForKey:@"destinationCity"];
        self.flightDuration = [dictionary valueForKey:@"flightDuration"];
        
        self.flightMsg = [dictionary valueForKey:@"flightMsg"];
        self.flightNo = [dictionary valueForKey:@"flightNo"];
        
        self.holidayFlightsId = [[dictionary valueForKey:@"holidayFlightsId"] integerValue];
        
        
        self.hubCity = [dictionary valueForKey:@"hubCity"];
        self.hubCityCode = [dictionary valueForKey:@"hubCityCode"];
        self.isActive = [dictionary valueForKey:@"isActive"];
        self.isInternalFlight = [dictionary valueForKey:@"isInternalFlight"];
        
        self.journeyTypeId = [[dictionary valueForKey:@"journeyTypeId"] integerValue];
         self.layoverTime = [dictionary valueForKey:@"layoverTime"];
        self.position = [[dictionary valueForKey:@"position"] integerValue];
        
        self.totalSegId = [[dictionary valueForKey:@"totalSegId"] integerValue];
    }
    return self;
}

@end
