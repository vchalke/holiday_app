//
//  FilterScreenVC.m
//  holidays
//
//  Created by Kush_Tech on 17/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "FilterScreenVC.h"

#import "Holiday.h"
#import "ThemeCollObjSRP.h"

#define kDepartureCityTitle @"Departure City"
#define kDurationTitle   @"Duration"
#define kThemeTitle  @"Theme"

@interface FilterScreenVC ()
{
    NSMutableArray *packTypeObjArray;
}
@end

@implementation FilterScreenVC

- (void)viewDidLoad {
    //    [super viewDidLoad];
    
    packTypeObjArray = [[NSMutableArray alloc]init];
    self.deptCountryArray = [[NSMutableArray alloc]init];
    self.deptDurationArray = [[NSMutableArray alloc]init];
    self.themesArray = [[NSMutableArray alloc]init];
    
    [self.btn_group addTarget:self action:@selector(navigatePics:) forControlEvents:UIControlEventTouchUpInside];
    [self.btn_customized addTarget:self action:@selector(navigatePics:) forControlEvents:UIControlEventTouchUpInside];
    [self.btn_onlyStay addTarget:self action:@selector(navigatePics:) forControlEvents:UIControlEventTouchUpInside];
    [self.btn_airInclusive addTarget:self action:@selector(navigatePics:) forControlEvents:UIControlEventTouchUpInside];
    
    NSDictionary *filetrDict = [[NSUserDefaults standardUserDefaults] objectForKey:filterOptionsDict];
    [self applyRangeSliderParameters];
    if (filetrDict != nil){
        [self setAllDataInFiltersScreen];
    }
    
    
}


-(void)navigatePics:(UIButton *)sender
{
    UIButton* myButton = (UIButton*)sender;
    if (myButton.tag == 1){
        [myButton setTag:0];
    }else{
       [myButton setTag:1];
    }
    [self applyColoursToViewInStack];
}

#pragma mark - TextFieldDelegaet
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    NSLog(@"scrollView %0.2f",scrollView.contentOffset.y);
    if (scrollView.contentOffset.y > 180 ) {
        [self.scrollViews setContentOffset:CGPointMake(0,180)];
    }
}

#pragma mark - TextField Delegaet
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    /*
    if (textField==self.txt_departureCity){
        if ([[self getArrayForIndex:textField.tag] count] > 0){
        [self jumpToSubFilteredScreenWithTitle:kDepartureCityTitle withArray:[self getArrayForIndex:textField.tag] withSelectedArray:([self.txt_departureCity.text length] > 0) ? [self.txt_departureCity.text componentsSeparatedByString:@", "] : [NSArray array]];
        }else{
            [self showAlertViewWithTitle:@"Empty !" withMessage:@"No Themes for these holiday Packages"];
        }
    }else if (textField==self.txt_duration){
        NSArray *themeArray = [NSArray arrayWithObjects:@"Less than 7 nights",@"8 to 12 nights",@"More than 12 nights", nil];
        [self jumpToSubFilteredScreenWithTitle:kDurationTitle withArray:themeArray withSelectedArray:([self.txt_duration.text length] > 0) ? [self.txt_duration.text componentsSeparatedByString:@", "] : [NSArray array]];
    }else if (textField==self.txt_Theme){
        if ([[self getArrayForIndex:textField.tag] count] > 0){
        [self jumpToSubFilteredScreenWithTitle:kThemeTitle withArray:[self getArrayForIndex:textField.tag] withSelectedArray:([self.txt_Theme.text length] > 0) ? [self.txt_Theme.text componentsSeparatedByString:@", "] : [NSArray array]];
        }else{
            [self showAlertViewWithTitle:@"Empty !" withMessage:@"No Themes for these holiday Packages"];
        }
    }
    */
    
    
    NSArray *titleArray = [NSArray arrayWithObjects:kDepartureCityTitle,kDepartureCityTitle,kDurationTitle,kThemeTitle, nil];
    if ([[self getArrayForIndex:textField.tag] count] > 0){
    [self jumpToSubFilteredScreenWithTitle:[titleArray objectAtIndex:textField.tag] withArray:[self getArrayForIndex:textField.tag] withSelectedArray:([textField.text length] > 0) ? [textField.text componentsSeparatedByString:@", "] : [NSArray array]];
    }else{
        [self showAlertViewWithTitle:@"Empty !" withMessage:@"No Themes for these holiday Packages"];
    }
    
}
-(void)jumpToSubFilteredScreenWithTitle:(NSString*)title withArray:(NSArray*)listArray withSelectedArray:(NSArray*)passArray{
    FilteredOptionsVC *controller = [[FilteredOptionsVC alloc] initWithNibName:@"FilteredOptionsVC" bundle:nil];
    controller.titles = title;
    controller.filterOptionsDelgate = self;
    controller.allListArray = listArray;
    controller.selectedOptionsArray = [passArray mutableCopy];
    [self presentViewController:controller animated:YES completion:nil];
}

-(NSArray*)getArrayForIndex:(NSInteger)tagId{
    NSMutableArray *themeArray = [[NSMutableArray alloc] init];
    if (tagId==2){
        [themeArray addObjectsFromArray:[NSArray arrayWithObjects:@"Less than 7 nights",@"8 to 12 nights",@"More than 12 nights", nil]];
        return themeArray;
    }
    for(int i = 0; i<self.holidayArrayInFilter.count; i++){
        Holiday *objHoliday = [self.holidayArrayInFilter objectAtIndex:i];
         NSLog(@"arrayOfHubList %@",objHoliday.arrayOfHubList);
        if (tagId == 3){
//            for (id object in objHoliday.themeCollectionArray) {
//                ThemeCollObjSRP* themeObject = [[ThemeCollObjSRP alloc]initWithThemeObjectDict:object];
//                if (![themeArray containsObject:themeObject.name]){
//                    [themeArray addObject:themeObject.name];
//                }
//            }
            for (NSString *objectStr in objHoliday.arrayOfThemeList) {
                if (![themeArray containsObject:objectStr]){
                    [themeArray addObject:objectStr];
                }
            }
            
        }else if (tagId==0){
            for (NSString *objectStr in objHoliday.arrayOfHubList) {
                if (![themeArray containsObject:objectStr]){
                    [themeArray addObject:objectStr];
                }
            }
        }
    }
    return themeArray;
}

-(NSArray*)getArrayOfSelectedIndexFromUserDefaults:(NSInteger)tagId{
    NSArray *selectArray = [[NSArray alloc]init];
    NSDictionary *filetrDict = [[NSUserDefaults standardUserDefaults] objectForKey:filterOptionsDict];
    if (tagId == 0){
    selectArray = (filetrDict != nil) ? [filetrDict objectForKey:filterDepartureCity] : [NSArray array];
    }else if (tagId == 2){
    selectArray = (filetrDict != nil) ? [filetrDict objectForKey:filterDuration] : [NSArray array];
    }else if (tagId == 3){
    selectArray = (filetrDict != nil) ? [filetrDict objectForKey:filterTheme] : [NSArray array];
    }
    return selectArray;
}
#pragma mark - Did Load Methods
-(NSArray*)getLowToHighPackagePriseArray{
    NSArray* filteredArray = [self.holidayArrayInFilter sortedArrayUsingComparator:^NSComparisonResult(Holiday* obj1, Holiday* obj2) {
        if (obj1.packagePrise > obj2.packagePrise) {
           return (NSComparisonResult)NSOrderedDescending;
        }
        if (obj1.packagePrise < obj2.packagePrise) {
           return (NSComparisonResult)NSOrderedAscending;
        }
        return (NSComparisonResult)NSOrderedSame;
    }];
    return filteredArray;
}
-(void)applyRangeSliderParameters{
    NSArray *packagePriseWiseArray = [self getLowToHighPackagePriseArray];
    Holiday *firstObj = [packagePriseWiseArray firstObject];
    Holiday *lastObj = [packagePriseWiseArray lastObject];
    self.range_slider.minValue = firstObj.packagePrise;
    self.range_slider.maxValue = lastObj.packagePrise;
    self.range_slider.selectedMinimum = firstObj.packagePrise;
    self.range_slider.selectedMaximum = lastObj .packagePrise;
}
-(void)setAllDataInFiltersScreen{
    
    NSDictionary *filetrDict = [[NSUserDefaults standardUserDefaults] objectForKey:filterOptionsDict];
    self.txt_departureCity.text = (filetrDict != nil) ? [[filetrDict objectForKey:filterDepartureCity] componentsJoinedByString:@", "] : @"";
     self.txt_duration.text = (filetrDict != nil) ? [[filetrDict objectForKey:filterDuration] componentsJoinedByString:@", "] : @"";
     self.txt_Theme.text = (filetrDict != nil) ? [[filetrDict objectForKey:filterTheme] componentsJoinedByString:@", "] : @"";
    self.range_slider.selectedMinimum = (filetrDict != nil) ? [[NSString stringWithFormat:@"%@",[filetrDict objectForKey:filterMinBudget]] floatValue] : 0;
    self.range_slider.selectedMaximum = (filetrDict != nil) ? [[NSString stringWithFormat:@"%@",[filetrDict objectForKey:filterMaxBudget]] floatValue] : 0;
    [self.btn_group setTag:(filetrDict != nil) ? [[NSString stringWithFormat:@"%@",[filetrDict objectForKey:filterByGroup]] integerValue] : 0];
    [self.btn_onlyStay setTag:(filetrDict != nil) ? [[NSString stringWithFormat:@"%@",[filetrDict objectForKey:filterByStay]] integerValue] : 0];
    [self.btn_customized setTag:(filetrDict != nil) ? [[NSString stringWithFormat:@"%@",[filetrDict objectForKey:filterByCustomized]] integerValue] : 0];
    [self.btn_airInclusive setTag:(filetrDict != nil) ? [[NSString stringWithFormat:@"%@",[filetrDict objectForKey:filterByAirInclusive]] integerValue] : 0];
    [self applyColoursToViewInStack];
    NSLog(@"%@",filetrDict);
}

-(void)applyColoursToViewInStack{
    NSArray *imgsArray = [NSArray arrayWithObjects:self.groupImgView,self.onlyStayImgView,self.customizedImgView,self.airInclusiveImgView, nil];
    NSArray *imgsNamesArray = [NSArray arrayWithObjects:@"filterGroupGray",@"filterOnlyStayGray",@"filterCustomizedGray",@"filterAirGray", nil];
    NSArray *lblArray = [NSArray arrayWithObjects:self.lbl_groupView,self.lbl_onlyStayView,self.lbl_customizedView,self.lbl_airInclusiveView, nil];
    NSArray *btnArray = [NSArray arrayWithObjects:self.btn_group,self.btn_onlyStay,self.btn_customized,self.btn_airInclusive, nil];
    for (int i = 0; i<btnArray.count; i++){
        UIImageView *imgView = [imgsArray objectAtIndex:i];
        UILabel *label = [lblArray objectAtIndex:i];
        UIButton *btnn = [btnArray objectAtIndex:i];
        if (btnn.tag == 1){
            imgView.image = [self coloredImage:[UIImage imageNamed:[imgsNamesArray objectAtIndex:i]] withColor:[UIColor colorWithRed:0 green:83/255.0 blue:169/255.0 alpha:1.0]];
            label.textColor = [UIColor colorWithRed:0 green:83/255.0 blue:169/255.0 alpha:1.0];
        }else{
            imgView.image = [self coloredImage:[UIImage imageNamed:[imgsNamesArray objectAtIndex:i]] withColor:[UIColor lightGrayColor]];
            label.textColor = [UIColor darkGrayColor];
        }
    }
}

-(void)saveAllDataInUserDefaults{
    NSMutableDictionary *editFilterDict = [[NSMutableDictionary alloc]init];
    
    [editFilterDict setObject:self.deptCountryArray forKey:filterDepartureCity];
    [editFilterDict setObject:self.deptDurationArray  forKey:filterDuration];
    [editFilterDict setObject:self.themesArray  forKey:filterTheme];
    [editFilterDict setObject:[NSString stringWithFormat:@"%0.2f",self.range_slider.selectedMinimum] forKey:filterMinBudget];
    [editFilterDict setObject:[NSString stringWithFormat:@"%0.2f",self.range_slider.selectedMaximum]forKey:filterMaxBudget];
    [editFilterDict setObject:[NSString stringWithFormat:@"%ld",(long)self.btn_group.tag] forKey:filterByGroup];
    [editFilterDict setObject:[NSString stringWithFormat:@"%ld",(long)self.btn_onlyStay.tag] forKey:filterByStay];
    [editFilterDict setObject:[NSString stringWithFormat:@"%ld",(long)self.btn_customized.tag]forKey:filterByCustomized];
    [editFilterDict setObject:[NSString stringWithFormat:@"%ld",(long)self.btn_airInclusive.tag] forKey:filterByAirInclusive];
    [[NSUserDefaults standardUserDefaults]setObject:editFilterDict forKey:filterOptionsDict];
}

#pragma mark - FilterOptionsVC Delegate

-(void)showSelectedOptionsFor:(NSString *)string withArray:(NSArray *)array{
    if ([string isEqualToString:kDepartureCityTitle]){
        self.deptCountryArray = array;
        self.txt_departureCity.text = ([array count]>0) ? [array componentsJoinedByString:@", "] : @"";
    }else if ([string isEqualToString:kDurationTitle]){
        self.deptDurationArray = array;
        self.txt_duration.text = ([array count]>0) ? [array componentsJoinedByString:@", "] : @"";
    }else if ([string isEqualToString:kThemeTitle]){
        self.themesArray = array;
        self.txt_Theme.text = ([array count]>0) ? [array componentsJoinedByString:@", "] : @"";
    }
}


#pragma mark - TTRangeSlider Delegate

-(void)rangeSlider:(TTRangeSlider *)sender didChangeSelectedMinimumValue:(float)selectedMinimum andMaximumValue:(float)selectedMaximum{
    NSLog(@"minValue %0.2f",sender.minValue);
    NSLog(@"maxValue %0.2f",sender.maxValue);
    NSLog(@"selectedMinimum %0.2f",sender.selectedMinimum);
    NSLog(@"selectedMaximum %0.2f",sender.selectedMaximum);
}

#pragma mark - Button Actions

- (IBAction)btn_ApplyPress:(id)sender {
    [self saveAllDataInUserDefaults];
    [self.filterDelgate applySelectedFilterDictionaryInSRP];
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)btn_BackPress:(id)sender {
    [self.filterDelgate applySelectedFilterDictionaryInSRP];
   [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)btn_ClearPress:(id)sender {
   [[NSUserDefaults standardUserDefaults]removeObjectForKey:filterOptionsDict];
    [self setAllDataInFiltersScreen];
    [self applyRangeSliderParameters];
}
@end
