//
//  TopCollectionWebVC.swift
//  holidays
//
//  Created by Kush_Tech on 03/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

import UIKit
import WebKit
class TopCollectionWebVC: NewMasterSwiftVC {
    var urlString:String?
    var titleString = ""
    
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var webView: WKWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.lbl_title.text = titleString
        if let isUrl = urlString{
            if let requesturl = NSURL(string: isUrl){
                let request = NSURLRequest(url: requesturl as URL)
                webView.load(request as URLRequest)
            }
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    @IBAction func btn_backPress(_ sender: Any) {
        self.jumpingToPrevious()
    }
}
