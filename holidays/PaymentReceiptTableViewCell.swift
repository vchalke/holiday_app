//
//  PaymentReceiptTableViewCell.swift
//  sotc-consumer-application
//
//  Created by Saurav Kumar on 10/08/17.
//  Copyright © 2017 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit

class PaymentReceiptTableViewCell: UITableViewCell {

    @IBOutlet var receiptType: UILabel!
    @IBOutlet var receiptDate: UILabel!
    @IBOutlet var receiptAmount: UILabel!
    
    @IBOutlet var receiptButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
