//
//  ColourClass.m
//  holidays
//
//  Created by Kush_Tech on 22/02/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "ColourClass.h"

@implementation ColourClass
+ (UIColor *) colorFromString: (NSString *) nameString{
    NSLog(@"%@",nameString);
     return [UIColor colorWithRed: 133.0/255.0 green: 251.0/255.0 blue: 253.0/255.0 alpha: 1.0];
}



+ (CGFloat) colorComponentFrom: (NSString *) string start: (NSUInteger) start length: (NSUInteger) length {
    NSString *substring = [string substringWithRange: NSMakeRange(start, length)];
    NSString *fullHex = length == 2 ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
    unsigned hexComponent;
    [[NSScanner scannerWithString: fullHex] scanHexInt: &hexComponent];
    return hexComponent / 255.0;
}

+ (UIColor *)colorWithHexString:(NSString *)hexString
{
    unsigned int hex;
    [[NSScanner scannerWithString:hexString] scanHexInt:&hex];
    int r = (hex >> 16) & 0xFF;
    int g = (hex >> 8) & 0xFF;
    int b = (hex) & 0xFF;
    
    return [UIColor colorWithRed:r / 255.0f
                           green:g / 255.0f
                            blue:b / 255.0f
                           alpha:1.0];
}
+ (UIColor *)colorWithHexString:(NSString *)hexString setAlpha:(float)setalpha
{
    unsigned int hex;
    [[NSScanner scannerWithString:hexString] scanHexInt:&hex];
    int r = (hex >> 16) & 0xFF;
    int g = (hex >> 8) & 0xFF;
    int b = (hex) & 0xFF;
    
    return [UIColor colorWithRed:r / 255.0f
                           green:g / 255.0f
                            blue:b / 255.0f
                           alpha:setalpha];
}
@end
