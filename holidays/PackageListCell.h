//
//  PackageListCell.h
//  holidays
//
//  Created by Pushpendra Singh on 13/08/15.
//  Copyright (c) 2015 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PackageListCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblPackageName;
@property (weak, nonatomic) IBOutlet UILabel *lblNumberOfNights;
@property (weak, nonatomic) IBOutlet UILabel *lblNumberOfDays;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollSite;
@property (weak, nonatomic) IBOutlet UILabel *lblTripCost;
@property (weak, nonatomic) IBOutlet UIButton *btnViewDetails;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintWidthFlight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintWidthhotel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintWidthFood;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintWidthVisa;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintWidthSight;

@property (weak, nonatomic) IBOutlet UILabel *lblInclusion;
@property (weak, nonatomic) IBOutlet UIView *viewGroupTour;
@property (weak, nonatomic) IBOutlet UILabel *groupTourLabel;
@property (weak, nonatomic) IBOutlet UIImageView *ImagegroupTour;
@property (weak, nonatomic) IBOutlet UIButton *btnSideArrowLeft;
@property (weak, nonatomic) IBOutlet UIButton *btnSideArrowRight;
-(void)actionForRightbuttonClicked:(UIButton *)index;
-(void)actionForLeftbuttonClicked:(UIButton *)index;
-(void)getSiteArrayDetails:(NSArray *)arrayForSite;

@end
