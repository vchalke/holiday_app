//
//  CarNotificationouselTableViewCell.m
//  holidays
//
//  Created by darshan on 04/01/19.
//  Copyright © 2019 Mobicule. All rights reserved.
//

#import "CarNotificationouselTableViewCell.h"
#import "SDWebImageDownloader.h"
#import "SDImageCache.h"
#import "WebViewController.h"
#import "TabMenuVC.h"
#import "BaseViewController.h"

#define DATE_FORMAT_12_HR @"dd-MMM-yyyy hh:mm:ss a"
@implementation CarNotificationouselTableViewCell
@synthesize delegate;

- (void)awakeFromNib {
    [super awakeFromNib];
    _count = 0;
      UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureTap:)];
    [self.imageViewatCL addGestureRecognizer:gesture];
    self.imageViewatCL.userInteractionEnabled = true;
    self.cellView.layer.cornerRadius = 10;
    self.PreviousButton.layer.cornerRadius = 15;
    self.NextButton.layer.cornerRadius = 15;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)OnClickNextButtom:(id)sender {
    
    
    NSDictionary *MessageNotificationData = [self.dataDict objectForKey:@"MessageData"];
    NSDictionary *payloadNotificationData = [MessageNotificationData objectForKey:@"payload"];
    
    NSArray *carouselNotificationdata = [[NSArray alloc] initWithArray:[payloadNotificationData objectForKey:@"carousel"]];
    if (_count <carouselNotificationdata.count - 1) {
        _count++;
    }
    
    NSLog(@"%@",[carouselNotificationdata objectAtIndex:0]);
    self.imagedeeplink = [[carouselNotificationdata objectAtIndex:_count] valueForKey:@"imgDeeplink"];
    NSString *imgMsg = [[carouselNotificationdata objectAtIndex:_count] valueForKey:@"imgMsg"];
    NSString *imgTitle = [[carouselNotificationdata objectAtIndex:_count] valueForKey:@"imgTitle"];
    NSString *imgUrl = [[carouselNotificationdata objectAtIndex:_count] valueForKey:@"imgUrl"];

    self.imgTitle.text = [NSString stringWithFormat:@"Title: %@", imgTitle];
    self.imgMessage.text = [NSString stringWithFormat:@"Message: %@", imgMsg];
    
    [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:imgUrl] options:SDWebImageDownloaderUseNSURLCache progress:nil completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
        
        if (image && finished) {
            self.imageViewatCL.image = image;
        }else{
            self.imageViewatCL.image  = [UIImage imageNamed:@"defaultBanner.png"];
        }
    }];
}
- (IBAction)OnClickPreviousButton:(id)sender {
    NSDictionary *MessageNotificationData = [self.dataDict objectForKey:@"MessageData"];
    NSDictionary *payloadNotificationData = [MessageNotificationData objectForKey:@"payload"];
    
    NSArray *carouselNotificationdata = [[NSArray alloc] initWithArray:[payloadNotificationData objectForKey:@"carousel"]];
    if (_count > 0) {
        _count--;
    }
    
    NSLog(@"%@",[carouselNotificationdata objectAtIndex:0]);
   self.imagedeeplink = [[carouselNotificationdata objectAtIndex:_count] valueForKey:@"imgDeeplink"];
    NSString *imgMsg = [[carouselNotificationdata objectAtIndex:_count] valueForKey:@"imgMsg"];
    NSString *imgTitle = [[carouselNotificationdata objectAtIndex:_count] valueForKey:@"imgTitle"];
    NSString *imgUrl = [[carouselNotificationdata objectAtIndex:_count] valueForKey:@"imgUrl"];
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyMMddHHmmss"];
    NSDate *date = [dateFormatter dateFromString:[self.dataDict objectForKey:@"DeliverTime"]];
    
    
    [dateFormatter setDateFormat:DATE_FORMAT_12_HR];
    NSString *dateString=[dateFormatter stringFromDate:date];
    
    
    self.imgTitle.text = [NSString stringWithFormat:@"Title: %@", imgTitle];
    self.imgMessage.text = [NSString stringWithFormat:@"Message: %@", imgMsg];
    
    [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:imgUrl] options:SDWebImageDownloaderUseNSURLCache progress:nil completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
        
        if (image && finished) {
            self.imageViewatCL.image = image;
        }else{
            self.imageViewatCL.image  = [UIImage imageNamed:@"defaultBanner.png"];
        }
    }];
}
-(void)fetchData{
   

    NSDictionary *MessageNotificationData = [self.dataDict objectForKey:@"MessageData"];
    NSDictionary *payloadNotificationData = [MessageNotificationData objectForKey:@"payload"];
    NSDictionary *apsNotificationData = [MessageNotificationData objectForKey:@"aps"];
    
    NSArray *carouselNotificationdata = [[NSArray alloc] initWithArray:[payloadNotificationData objectForKey:@"carousel"]];
        NSLog(@"%@",[carouselNotificationdata objectAtIndex:0]);
    self.imagedeeplink = [[carouselNotificationdata objectAtIndex:_count] valueForKey:@"imgDeeplink"];
     NSString *imgMsg = [[carouselNotificationdata objectAtIndex:_count] valueForKey:@"imgMsg"];
     NSString *imgTitle = [[carouselNotificationdata objectAtIndex:_count] valueForKey:@"imgTitle"];
     NSString *imgUrl = [[carouselNotificationdata objectAtIndex:_count] valueForKey:@"imgUrl"];
    
    NSDictionary *alertNotificationdata = [apsNotificationData objectForKey:@"alert"];
    NSString *title = [alertNotificationdata objectForKey:@"title"];
    NSString *message = [alertNotificationdata objectForKey:@"body"];
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyMMddHHmmss"];
    NSDate *date = [dateFormatter dateFromString:[self.dataDict objectForKey:@"DeliverTime"]];
    
    
    [dateFormatter setDateFormat:DATE_FORMAT_12_HR];
    NSString *dateString=[dateFormatter stringFromDate:date];
    
    self.NotificationTitle.text = title;
    self.NotificationMsg.text = message;
    self.NotificationDate.text = dateString;
    self.imgTitle.text = [NSString stringWithFormat:@"Title: %@", imgTitle];
    self.imgMessage.text = [NSString stringWithFormat:@"Message: %@", imgMsg];
    
    [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:imgUrl] options:SDWebImageDownloaderUseNSURLCache progress:nil completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
        
        if (image && finished) {
            self.imageViewatCL.image = image;
        }else{
            self.imageViewatCL.image  = [UIImage imageNamed:@"defaultBanner.png"];
        }
    }];
}

- (void)tapGestureTap:(UITapGestureRecognizer *)sender {
    NSLog(@"count%ld",(long)_count);
    NSString *DeepLink = [[NSString alloc]init];
    DeepLink = [[[self.dataDict valueForKey:@"MessageData"] valueForKey:@"payload"]valueForKey:@"deeplink"];
    NSURL *urlForDeepLink = [[NSURL alloc] initWithString:self.imagedeeplink];
    if (self.imagedeeplink.length == 0){
        urlForDeepLink = [[NSURL alloc] initWithString:DeepLink];
    }
    [self.delegate didPressButton:urlForDeepLink];
}







@end
