//
//  TourDetailViewController.swift
//  sotc-consumer-application
//
//  Created by Saurav Kumar on 26/07/17.
//  Copyright © 2017 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit
import Foundation
import CoreData





class TourDetailViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UIPopoverPresentationControllerDelegate,UIGestureRecognizerDelegate,UIDocumentInteractionControllerDelegate {
    
    //MARK: - Outlets
    
    @IBOutlet var noCityAvailableLabel: UILabel!
    @IBOutlet var routeCollectionView: UICollectionView!
    
    @IBOutlet var paymentView: UIView!
    @IBOutlet weak var claimDiscount: UIButton!
    @IBOutlet var visaView: UIView!
    @IBOutlet var ticketsView: UIView!
    @IBOutlet var itineraryView: UIView!
    @IBOutlet var insuranceView: UIView!
    @IBOutlet var moreView: UIView!
    
    @IBOutlet var tourTitleLabel: UILabel!
    @IBOutlet var tourDateLabel: UILabel!
    @IBOutlet var tourDateButton: UIButton!
    @IBOutlet var cityToBecoberedLabel: UILabel!
    @IBOutlet var passengerStatusLabel: UIButton!
    
    var tourDetail:Tour?
    var tourStatus:TourStatus?
    
    lazy var cityCoverd:Array<CityCovered> = []
    
    
    
    var selectedView:String?
    
    @IBOutlet weak var tourDetailHeaderImageSlideShow: ImageSlideshow!
    
    var overlay: UIView?
    
    //MARK: UIViewController Delegate
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
//        SlideNavigationController.sharedInstance().navigationController?.navigationBar.isHidden = true
        
        self.setUpUI()
        
        self.getTourDetail()
        
        
        
      if let cityCoverd = tourDetail?.cityCoveredRelation?.allObjects as? Array<CityCovered>
      {
        
        let sortedCity = cityCoverd.sorted { (CityCovered1, CityCovered2) -> Bool in
            
            Int(CityCovered1.position!)! < Int(CityCovered2.position!)!
            
        }
        
        if sortedCity.count > 0
        {
            self.cityCoverd = sortedCity
            self.noCityAvailableLabel.isHidden =  true
            self.routeCollectionView.isHidden = false
            
            routeCollectionView.reloadData()
        }
        else
        {
            self.noCityAvailableLabel.isHidden =  false
            self.routeCollectionView.isHidden = true
        }
        
        }
        
        
        
        self.routeCollectionView.register(UINib(nibName: "TourRouteCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "TourRouteCollectionViewCell")
        
        
        let cellSize = CGSize(width:130 , height:self.routeCollectionView.frame.height-2)
        let layout = UICollectionViewFlowLayout()
        
        layout.scrollDirection = .horizontal //.horizontal
        layout.itemSize = cellSize
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumLineSpacing = 10.0
        layout.minimumInteritemSpacing = 10.0
        routeCollectionView.setCollectionViewLayout(layout, animated: true)
        
        self.addUITapGestureRecognizer()
        
        
        self.sendNotificationRequest()
        
        
    }
    
    func sendNotificationRequest()  {
        
        if !(UserDefaults.standard.bool(forKey: "\(tourDetail?.bfNumber ?? "")_\(UserDefauldConstant.isSubscribeForPushNotificationOnSOTCServer)")) {
            
            UserBookingController.sendNotificationRequest(bfNumber: (tourDetail?.bfNumber)!) { (operationValidationMessage) in
                
                if operationValidationMessage.status
                {
                    
                    UserDefaults.standard.set(true, forKey: "\(self.tourDetail?.bfNumber ?? "")_\(UserDefauldConstant.isSubscribeForPushNotificationOnSOTCServer)")
                }
                else{
                    
                    UserDefaults.standard.set(false, forKey: "\(self.tourDetail?.bfNumber ?? "")_\(UserDefauldConstant.isSubscribeForPushNotificationOnSOTCServer)")
                    
                }
                
            }
            
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        self.showNavigationBarTranslucent()
        
        //UIView.animate(withDuration: 1.0, animations: {
            
            /*self.tabBarController?.tabBar.isHidden = true
            
            self.navigationController?.setNavigationBarTranslucent()
            // self.navigationController?.setNagationBackButton(showRightButton: true, viewController: self)
            
            self.navigationController?.setnavigatiobBarTitle(title: " ", viewController: self)
            
            self.navigationController?.setNagationRightButton(showRightButton: true, viewController: self)*/
            
          //  self.navigationController?.navigationItem.hidesBackButton = true
            
       // }) { (Bool) in
            
            
            
        //}
        
        
        
    }
    
    
    deinit {
        // remove the overlay with animation.
        // the delegate method popoverPresentationControllerDidDismissPopover(_:) is not called
        // if the popover is dismissed programmatically,
        // so the removal goes here.
        
        guard let overlay = overlay else {
            return
        }
        DispatchQueue.main.async() {
            
            UIView.animate(withDuration: 0.2, animations: {
                overlay.alpha = 0.0
            }, completion: { _ in
                overlay.removeFromSuperview()
            })
        }
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: UINavigationController Delegate
    
    func backButtonClick() -> Void {
        
        
        self.navigationController?.popViewController(animated: true)
        self.navigationController?.setRedNavigationBar()
        
    }
    
    
    
    
    func setUpUI() {
        
        let tourDateLabelAttr = [convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor): UIColor.black, convertFromNSAttributedStringKey(NSAttributedString.Key.font): self.tourDateLabel.font] as [String : Any]
        let fisrtLetterAttr = [convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor): self.tourTitleLabel.textColor, convertFromNSAttributedStringKey(NSAttributedString.Key.font): self.tourDateLabel.font] as [String : Any]
        
        
        let tourPartOne = NSMutableAttributedString(string: "T", attributes: convertToOptionalNSAttributedStringKeyDictionary(fisrtLetterAttr))
        let tourPartTwo = NSMutableAttributedString(string: "OUR START DATE", attributes: convertToOptionalNSAttributedStringKeyDictionary(tourDateLabelAttr))
        let tourCombination = NSMutableAttributedString()
        tourCombination.append(tourPartOne)
        tourCombination.append(tourPartTwo)
        self.tourDateLabel.attributedText = tourCombination
        
        
        let cityPartOne = NSMutableAttributedString(string: "C", attributes: convertToOptionalNSAttributedStringKeyDictionary(fisrtLetterAttr))
        let cityPartTwo = NSMutableAttributedString(string: "ITIES TO BE COVERED", attributes: convertToOptionalNSAttributedStringKeyDictionary(tourDateLabelAttr))
        let cityCombination = NSMutableAttributedString()
        cityCombination.append(cityPartOne)
        cityCombination.append(cityPartTwo)
        self.cityToBecoberedLabel.attributedText = cityCombination
        
        let passengerPartOne = NSMutableAttributedString(string: "P", attributes: convertToOptionalNSAttributedStringKeyDictionary(fisrtLetterAttr))
        let passengerPartTwo = NSMutableAttributedString(string: "ASSENGER STATUS", attributes: convertToOptionalNSAttributedStringKeyDictionary(tourDateLabelAttr))
        let passengerCombination = NSMutableAttributedString()
        passengerCombination.append(passengerPartOne)
        passengerCombination.append(passengerPartTwo)
        self.passengerStatusLabel.setAttributedTitle(passengerCombination, for: UIControl.State.normal)
        
        //To create Document Folder Strucure
        let _ :(bfNumberPath:String,paymentPath:String,ticketsPath:String,InsurancePath:String,FinalTourPath:String,visaPath:String,bookingListPath: String,itienearyTourPath:String) =
        TourDocumentController.createFolderStructureForBFNumber(bfNumber: (self.tourDetail?.bfNumber)!)
        
        self.tourTitleLabel.text = "\( self.tourDetail?.tourName ?? "")"
        
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMMM"
        let departureDate = dateFormatter.string(from: (self.tourDetail?.departureDate! as! NSDate) as Date)
        // let arrivalDate = dateFormatter.string(from: (self.tourDetail?.arrivalDate! as! NSDate) as Date)
        
        
        let dateFormatterYear = DateFormatter()
        dateFormatterYear.dateFormat = "yyyy"
        let departureDateYear = dateFormatterYear.string(from: (self.tourDetail?.departureDate! as! NSDate) as Date)
        
        
        let dateLabelAttr = [convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor): self.tourDateButton.titleLabel?.textColor! ?? UIColor.black , convertFromNSAttributedStringKey(NSAttributedString.Key.font): self.tourDateButton.titleLabel?.font! ?? UIFont.systemFontSize] as [String : Any]
        let yearAttr = [convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor):  UIColor.gray , convertFromNSAttributedStringKey(NSAttributedString.Key.font): self.tourDateButton.titleLabel?.font! ?? UIFont.systemFontSize] as [String : Any]
        
        //                    let date = NSMutableAttributedString(string: "\(departureDate) - \(arrivalDate)", attributes: dateLabelAttr)
        let date = NSMutableAttributedString(string: "\(departureDate) ", attributes: convertToOptionalNSAttributedStringKeyDictionary(dateLabelAttr))
        let year = NSMutableAttributedString(string: " \(departureDateYear)", attributes: convertToOptionalNSAttributedStringKeyDictionary(yearAttr))
        let tourDateCombination = NSMutableAttributedString()
        tourDateCombination.append(date)
        tourDateCombination.append(year)
        self.tourDateButton.setAttributedTitle(tourDateCombination, for: UIControl.State.normal)
        
        self.setBannerImage()
        
        // VJ_Added On 14/08/20
        printLog("Product--->:\(tourDetail?.productBookingStatus)")
        if tourDetail?.productBookingStatus?.uppercased() == "BOOKED" {
            
        
        if self.tourStatus == TourStatus.UpComing{
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM/yyyy"
            let claimDiscoountDate = formatter.date(from: "25/11/2019")! as NSDate
            
            //if(tourDetail?.bookingdate != nil){
               // if((self.tourDetail?.bookingdate?.isGreaterThanDate(dateToCompare: claimDiscoountDate))!){
                    self.tourDetailHeaderImageSlideShow.addSubview(claimDiscount)
               // }
            //}
            
            
        }
        
        }
    }
    
    
    //MARK: Data Fetch Methods from Server
    /*
    // Comment Previous Function
    func getTourDetail() {
        
        LoadingIndicatorView.show("Loading")
        
        UserBookingController.fetchTourDetails(tourObject: self.tourDetail!, completion: { (messageTourDetail, tourObject) in
            
            if messageTourDetail.status
            {
                
                printLog(self.tourDetail?.description ?? "" )
                
                CoreDataController.getContext().refresh(self.tourDetail!, mergeChanges: true)
                
            }
            
            LoadingIndicatorView.hide()
            
            
        })
        
 
    }
    */
    
   // Added New Function
// VJ_Added_Start 14/08/20
       func getTourDetail() {
           
           LoadingIndicatorView.show("Loading")
           
           UserBookingController.fetchTourDetails(tourObject: self.tourDetail!, completion: { (messageTourDetail, tourObject) in
               
               if messageTourDetail.status
               {
                   
                   printLog(self.tourDetail?.description ?? "" )
                   
                   CoreDataController.getContext().refresh(self.tourDetail!, mergeChanges: true)
                   
                   if self.tourStatus == TourStatus.UpComing{
                       
                       UserBookingController.getcheckDiscountAplied(bfnumber: (self.tourDetail?.bfNumber)!){(status,message) in
                           if(status){
                              if( message == "ValidDiscount" ){
                               
                                   if self.tourDetail?.productBookingStatus?.uppercased() == "BOOKED" {
                                   if self.tourStatus == TourStatus.UpComing{
                                       let formatter = DateFormatter()
                                       formatter.dateFormat = "dd/MM/yyyy"
                                    // Commented On 11/09/20
//                                       let claimDiscoountDate = formatter.date(from: "25/11/2019")! as NSDate
                                    // Added On 11/09/20 // DateChangeOn 22/09/20
//                                       let claimDiscoountDate = formatter.date(from: "31/12/2021")! as NSDate
                                    // Added On 26/11/2020
                                    let claimDiscoountDate = formatter.date(from: "01/11/2020")! as NSDate
                                       
                                    if((self.tourDetail?.bookingDate?.isGreaterThan_Date(dateToCompare: claimDiscoountDate as Date))!){
//                                    if((self.tourDetail?.departureDate?.isGreaterThanDate(dateToCompare: claimDiscoountDate))!){
                                       self.tourDetailHeaderImageSlideShow.addSubview(self.claimDiscount)
                                       self.claimDiscount.isHidden = false
                                       }
                                   
                                   }
                                   }
                                   LoadingIndicatorView.hide()
                               }else{
                                   self.claimDiscount.isHidden = true
                                   LoadingIndicatorView.hide()
                               }
                           
                               //getcheckDiscountAplied
                           }else{
                               LoadingIndicatorView.hide()
                           }
                       }
                   }else{
                       LoadingIndicatorView.hide()
                   }
                   
                  
                   
               }else{
                   LoadingIndicatorView.hide()
               }
               
              
               
               
           })
           
    
       }
    // VJ_Added_End
    
    //MARK: UIPopoverPresentationController Delegate
    
    
    dynamic func presentationController(_ presentationController: UIPresentationController, willPresentWithAdaptiveStyle style: UIModalPresentationStyle, transitionCoordinator: UIViewControllerTransitionCoordinator?) {
        // add a semi-transparent view to parent view when presenting the popover
        let parentView = presentationController.presentingViewController.view
        
        let overlay = UIView(frame: (parentView?.bounds)!)
        overlay.backgroundColor = UIColor(white: 0.0, alpha: 0.4)
        parentView?.addSubview(overlay)
        
        let views: [String: UIView] = ["parentView": parentView!, "overlay": overlay]
        
        parentView?.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[overlay]|", options: [], metrics: nil, views: views))
        parentView?.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[overlay]|", options: [], metrics: nil, views: views))
        
        overlay.alpha = 0.0
        
        transitionCoordinator?.animate(alongsideTransition: { _ in
            overlay.alpha = 1.0
        }, completion: nil)
        
        self.overlay = overlay
    }
    
    func prepareForPopoverPresentation(_ popoverPresentationController: UIPopoverPresentationController) {
        
        switch selectedView! {
        case "more":
            
            popoverPresentationController.permittedArrowDirections = .any
            popoverPresentationController.sourceView = moreView
            popoverPresentationController.sourceRect = CGRect(x: 20, y: 0, width: 0, height: 0)
            
        case "info":
            
            popoverPresentationController.permittedArrowDirections = .any
            popoverPresentationController.sourceView = self.view
            popoverPresentationController.sourceRect = CGRect(x: UIScreen.main.bounds.maxX - 80, y: 67, width: 0, height: 0)
            
        case Title.DEVIATION:
            
            popoverPresentationController.permittedArrowDirections = .any
            popoverPresentationController.sourceView = passengerStatusLabel
            popoverPresentationController.sourceRect = CGRect(x: 50, y: 0, width: 0, height: 0)
            
        default:
            break
        }
        
        
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    func popoverPresentationControllerDidDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) {
        
        guard let overlay = overlay else {
            return
        }
        DispatchQueue.main.async() {
            UIView.animate(withDuration: 0.2, animations: {
                overlay.alpha = 0.0
            }, completion: { _ in
                overlay.removeFromSuperview()
            })
        }
        
    }
    
    func presentPopover() {
        
        selectedView = "more"
        
        let popoverContentController = TourMoreViewController(nibName: "TourMoreViewController", bundle: nil)
        
        popoverContentController.presentingView = self
        // Set your popover size.
        popoverContentController.preferredContentSize = CGSize(width: UIScreen.main.bounds.width - 20, height: (UIScreen.main.bounds.height/2) + 100)
        
        // Set the presentation style to modal so that the above methods get called.
        popoverContentController.modalPresentationStyle = .popover
        popoverContentController.tourStatus = self.tourStatus
        // Set the popover presentation controller delegate so that the above methods get called.
        popoverContentController.popoverPresentationController!.delegate = self
        
        popoverContentController.tourDetail = self.tourDetail
        
        // Present the popover.
        self.present(popoverContentController, animated: true, completion: nil)
        
    }
    
    func presentInfoPopover() {
        
        selectedView = "info"
        
        let popoverContentController = InfoPopUpViewController(nibName: "InfoPopUpViewController", bundle: nil)
        
        popoverContentController.presentingView = self
        // Set your popover size.
        popoverContentController.preferredContentSize = CGSize(width: UIScreen.main.bounds.width - 20, height: (UIScreen.main.bounds.height/2))
        
        // Set the presentation style to modal so that the above methods get called.
        popoverContentController.modalPresentationStyle = .popover
        
        // Set the popover presentation controller delegate so that the above methods get called.
        popoverContentController.popoverPresentationController!.delegate = self
        
        
        
        // Present the popover.
        self.present(popoverContentController, animated: true, completion: nil)
        
    }
    
    func presentPassengerPopover() {
        
        selectedView = Title.DEVIATION
        
        let passengerArray = self.getPassengers()
        if passengerArray.count > 0
        {
        let popoverContentController = PassengerPopUpViewController(nibName: "PassengerPopUpViewController", bundle: nil)
        
        
        
        popoverContentController.passangerDetailsArray = passengerArray
            
        popoverContentController.deviationDetailsArray = Array((self.tourDetail?.deviationRelation)!) as? [Deviation]
        
        popoverContentController.tourDetails = tourDetail
        popoverContentController.moduleName = "BookingDetatils"
        popoverContentController.tourStatus = self.tourStatus
        
        popoverContentController.presentingView = self
        // Set your popover size.
        popoverContentController.preferredContentSize = CGSize(width: UIScreen.main.bounds.width - 20, height: 240.0)
        
        // Set the presentation style to modal so that the above methods get called.
        popoverContentController.modalPresentationStyle = .popover
        
        // Set the popover presentation controller delegate so that the above methods get called.
        popoverContentController.popoverPresentationController!.delegate = self
            
        // Present the popover.
        self.present(popoverContentController, animated: true, completion: nil)
        }else
        {
            AppUtility.displayAlertController(title: "No data", message: "No data found !")
        }
        
    }
    
    func getPassengers() -> Array<Passenger>
    {
    
       if let passengerArray :Array<Passenger> = self.tourDetail?.passengerRelation?.allObjects as? Array<Passenger>
        {
            
            if passengerArray.count > 0
            {
                let sortedPassengerArray:Array<Passenger> =  passengerArray.sorted(by: { (Passenger1, Passenger2) -> Bool in
                    
                    Passenger1.passengerNumber! < Passenger2.passengerNumber!
                })
            
                return sortedPassengerArray
                
            }
        }
        
        return []
        }
    
    //MARK: UITapGestureRecognizer Methods
    
    func addUITapGestureRecognizer()  {
        
        let paymentTap = UITapGestureRecognizer(target: self, action: #selector(TourDetailViewController.onPaymentViewClick))
        paymentTap.delegate = self
        paymentView.addGestureRecognizer(paymentTap)
        
        let visaTap = UITapGestureRecognizer(target: self, action: #selector(TourDetailViewController.onVisaViewClick))
        visaTap.delegate = self
        visaView.addGestureRecognizer(visaTap)
        
        
        let ticketTap = UITapGestureRecognizer(target: self, action: #selector(TourDetailViewController.onTicketsViewClick))
        ticketTap.delegate = self
        ticketsView.addGestureRecognizer(ticketTap)
        
        let ItineraryTap = UITapGestureRecognizer(target: self, action: #selector(TourDetailViewController.onItineraryViewClick))
        ItineraryTap.delegate = self
        itineraryView.addGestureRecognizer(ItineraryTap)
        
        
        let insuranceTap = UITapGestureRecognizer(target: self, action: #selector(TourDetailViewController.onInsuranceViewClick))
        insuranceTap.delegate = self
        insuranceView.addGestureRecognizer(insuranceTap)
        
        let moreTap = UITapGestureRecognizer(target: self, action: #selector(TourDetailViewController.onMoreViewClick))
        moreTap.delegate = self
        moreView.addGestureRecognizer(moreTap)
        
        
    }
    
    @objc func onPaymentViewClick() {
        
        
        
        if let paymentData:Array<Payment> = self.tourDetail?.paymentRelation?.allObjects as? Array<Payment>
        {
            
            if paymentData.count > 0
            {
                
                let viewController:PaymentViewController = PaymentViewController(nibName: "PaymentViewController", bundle: nil)
                
                viewController.tourDetail = self.tourDetail
                
                viewController.tourStatus = self.tourStatus
                
                self.navigationController?.pushViewController(viewController, animated: true)
                
                
            }
            else {
                
                AppUtility.displayAlertController(title: "No data", message: "No data found !")
            }
            
        }
        else
            
        {
            AppUtility.displayAlertController(title: "No data", message: "No data found !")
        }
        
    }
    
    
    
    @objc func onVisaViewClick() {
        
        
        let visaDetailsDoucumets:Array<Visa>? = (tourDetail?.visaRelation?.allObjects as?  Array<Visa>)!
        let visaCountryArray:Array<VisaCountryMaster>? = (tourDetail?.visaCountryRelation?.allObjects as?  Array<VisaCountryMaster>)!
        
        if (visaDetailsDoucumets != nil && (visaDetailsDoucumets?.count)! > 0) || (visaCountryArray != nil && (visaCountryArray?.count)! > 0)
            
        {
            let viewController:VisaViewController = VisaViewController(nibName: "VisaViewController", bundle: nil)
            
            viewController.tourDetails = self.tourDetail
            
            self.navigationController?.pushViewController(viewController, animated: true)
            
        }else{
            
            
            AppUtility.displayAlertController(title: "No data", message: "No data found !")
            
        }
        
        
        
    }
    
    
    
    @objc func onTicketsViewClick() {
        
        
        
        if UserBookingController.isPaymentDonePaymentFlag(by:self.tourDetail!)
        {
            
            
            
            let viewController:TicketsViewController = TicketsViewController(nibName: "TicketsViewController", bundle: nil)
            
          /*  if let passengerData:Array<Passenger> = self.tourDetail?.passengerRelation?.allObjects as? Array<Passenger>
            {
                
                if passengerData.count > 0
                {
                    
                    
                    viewController.passengerDetailsArray = passengerData.sorted(by: { (Passenger1, Passenger2) -> Bool in
                        
                        Passenger1.passengerNumber! < Passenger2.passengerNumber!
                        
                    })
                    
                    
                    if let tourPassengerDoucumets = tourDetail?.passengerDocumentRelation?.allObjects as?  Array<TourPassengerDouments>
                        
                    {
                        
                        let filteredDocumnetsArray = tourPassengerDoucumets.filter() { $0.documentType?.uppercased() == "TICKET" }
                        
                        
                        
                        viewController.passengerTicketDoucumets = filteredDocumnetsArray
                        
                        self.navigationController?.pushViewController(viewController, animated: true)
                        
                        
                        
                    }else
                        
                    {
                        
                        self.displatAlert(message: "Your ticket is yet to issued")
                        
                    }
                }
                else {
                    
                    AppUtility.displayAlertController(title: "No data", message: "No data found !")
                }
                
            }
            else
                
            {
                AppUtility.displayAlertController(title: "No data", message: "No data found !")
            }*/
            
            
            if let tourPassengerDoucumets = tourDetail?.passengerDocumentRelation?.allObjects as?  Array<TourPassengerDouments>
                
            {
                
                let filteredDocumnetsArray = tourPassengerDoucumets.filter() { ($0.documentType?.uppercased() ?? "").contains("VOUCHER")}
                
                
                if filteredDocumnetsArray.count > 0
                {
                    if filteredDocumnetsArray.count > 1
                    {
                    
                        viewController.passengerTicketDoucumets = filteredDocumnetsArray
                        
                        viewController.moduleName = Title.TICKETS
                    
                        self.navigationController?.pushViewController(viewController, animated: true)
                        
                    }else
                    {
                       if let singlePassengerDoc:TourPassengerDouments = filteredDocumnetsArray.first
                       {
                        
                        var fileName:String = "Ticket \(0).pdf"
                        
                        let passengerNumber:String = singlePassengerDoc.passengerNumber ?? ""
                        
                        let ticketNumber:String = "Ticket \(0)"
                        
                        let downloadUrl:String = singlePassengerDoc.docummentURL ?? ""
                        
                        if downloadUrl != nil && !downloadUrl.isEmpty && downloadUrl != ""
                        {
                            fileName = TourDocumentController.getDecodedFileName(url:downloadUrl)
                        }
                        
                        let saveFileUrl:String =  passengerNumber + "/" + ticketNumber + "/" + fileName
                       
                        let documnetUrl:String = TourDocumentController.getDocumentFilePath(url: saveFileUrl, bfnNumber: singlePassengerDoc.bfNumber!, moduleFolder: Title.TICKETS)
                        
                        self.downloadDocument(filePath: documnetUrl, url: downloadUrl)
                        
                        }else
                       {
                             self.displatAlert(message: "Your ticket is yet to issued")
                        }
                        
                    }
                    
                }else
                {
                    self.displatAlert(message: "Your ticket is yet to issued")
                }
                
                
            }else
                
            {
                
                self.displatAlert(message: "Your ticket is yet to issued")
                
            }
            
            
            
        }else
        {
            
            self.displatAlert(message: AlertMessage.Tickets.COMPLETE_PAYMENT)
            
        }
        
    }
    
    
    
    @objc func onItineraryViewClick() {
        
        let cityCovered:CityCovered? = nil
        
        self.redirectToItinearyView(cityCovered: cityCovered)
        
        
    }
    
    
    func redirectToItinearyView(cityCovered:CityCovered?)  {
        
        
        
        
        let viewController:ItineraryViewController = ItineraryViewController(nibName: "ItineraryViewController", bundle: nil)
        
        
        
        if let itineraryDetail:Array<ItineraryDetail>  = tourDetail?.itineraryRelation?.allObjects as? Array<ItineraryDetail>
            
        {
            
            if itineraryDetail.count > 0
                
            {
                
                
                /*if let itienearyImages:Array<ItineraryImages> = tourDetail?.tourImagesRelation?.allObjects as?  Array<ItineraryImages>
                 
                 //{
                 
                 let itienearyDetails = itineraryDetail.sorted(by: { (ItineraryDetail1, ItineraryDetail2) -> Bool in
                 
                 
                 
                 Int(ItineraryDetail1.itineraryDay!)! < Int(ItineraryDetail2.itineraryDay!)!
                 
                 })
                 
                 
                 
                 if itienearyDetails.count > 0
                 
                 {
                 
                 viewController.itienearyDetails = itienearyDetails
                 
                 
                 
                 viewController.itienearyImageDetails = itienearyImages
                 
                 
                 
                 viewController.bfnNumber = tourDetail?.bfNumber*/
                
                
                viewController.tourDetails = self.tourDetail
                viewController.cityCovered = cityCovered
                
                self.navigationController?.pushViewController(viewController, animated: true)
                
                
                
                /*   }
                 
                 else
                 
                 {
                 
                 self.displatAlert(message: "no itinerary detail found")
                 
                 }
                 
                 
                 }else
                 
                 {
                 
                 self.displatAlert(message: "no itinerary detail found")
                 
                 }*/
                
            }else
                
            {
                
                AppUtility.displayAlertController(title: AlertMessage.NO_DATA_TITLE, message: AlertMessage.Itineary.NO_DATA)
                
            }
            
        }else{
            
            AppUtility.displayAlertController(title: AlertMessage.NO_DATA_TITLE, message: AlertMessage.Itineary.NO_DATA)
            
        }
        
    }
    
    @objc func onInsuranceViewClick() {
        
        
        
        let viewController:InsuranceViewController = InsuranceViewController(nibName: "InsuranceViewController", bundle: nil)
        
        
        
        if let passengerArray:Array<Passenger> = (tourDetail?.passengerRelation?.allObjects as? Array<Passenger>)?.sorted(by: { (Passenger1, Passenger2) -> Bool in
            
            
            
            Passenger1.passengerNumber! < Passenger2.passengerNumber!
            
        })
            
        {
            
            if passengerArray.count > 0
                
            {
                viewController.passangerDetailsArray = passengerArray
                
                if let insuraceDetailsArray:Array<Insurance> = (tourDetail?.insuranceRelation?.allObjects as? Array<Insurance>)?.sorted(by: { (Passenger1, Passenger2) -> Bool in
                    
                    
                    
                    Passenger1.passengerNumber! < Passenger2.passengerNumber!
                    
                })
                    
                {
                    
                    
                    
                    if insuraceDetailsArray.count > 0
                        
                    {
                        
                        
                        
                        viewController.insuraceDetailsArray = insuraceDetailsArray
                        
                        
                        
                        self.navigationController?.pushViewController(viewController, animated: true)
                        
                    }else
                        
                    {
                        
                        self.displatAlert(message: "Your insurance is yet to be issued")
                        
                    }
                    
                }else
                    
                {
                    
                    self.displatAlert(message: "Your insurance is yet to be issued")
                    
                }
                
                
                
            }else
                
            {
                
                self.displatAlert(message: "Your insurance is yet to be issued")
                
            }
            
            
            
        }else{
            
            self.displatAlert(message: "Your insurance is yet to be issued")
            
        }
        
        
        
        
        
        
        
    }
    
    
    
    @objc func onMoreViewClick() {
        
        
        
        self.presentPopover()
        
    }
    
    //MARK: UICollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return cityCoverd.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TourRouteCollectionViewCell", for: indexPath as IndexPath) as! TourRouteCollectionViewCell
        
        
        if let city:CityCovered =  cityCoverd[indexPath.row]
        {
            
            let dayAttr = [convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor):AppUtility.hexStringToUIColor(hex: "c6c6c6") , convertFromNSAttributedStringKey(NSAttributedString.Key.font): cell.routeTitleLabel.font! ] as [String : Any]
            
            let cityAttr = [convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor):  UIColor.black , convertFromNSAttributedStringKey(NSAttributedString.Key.font): cell.routeTitleLabel.font! ] as [String : Any]
            
            let day = NSMutableAttributedString(string: "\(city.noOfNights!)N ", attributes: convertToOptionalNSAttributedStringKeyDictionary(dayAttr))
            let cityName = NSMutableAttributedString(string: " \(city.cityName!)", attributes: convertToOptionalNSAttributedStringKeyDictionary(cityAttr))
            let cityNameCombination = NSMutableAttributedString()
            cityNameCombination.append(day)
            cityNameCombination.append(cityName)
            
            cell.routeTitleLabel.attributedText = cityNameCombination
            
            //  cell.routeTitleLabel.text = "\(city.noOfNights!)N \(city.cityName!) "
        }
        
        
        
        cell.routeTitleLabel.layer.cornerRadius = 15.0
        cell.routeTitleLabel.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        cell.routeTitleLabel.layer.borderWidth = 1.0
        
        
        return cell      //return your cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // When user selects the cell
        
        
        if let cityCovered:CityCovered =  cityCoverd[indexPath.row] as? CityCovered
        {
            self.redirectToItinearyView(cityCovered: cityCovered)
        }
        
        
        
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        if let city:CityCovered =  cityCoverd[indexPath.row]
        {
            if let numerOfNights:String = city.noOfNights
            {
                var  cityCoverd = numerOfNights + "N"
                
                if let cityName:String = city.cityName
                {
                    cityCoverd = cityCoverd + cityCoverd
                    
                    if let font =  UIFont(name:"Roboto-Light", size:17)
                    {
                        
                        let width = "\(numerOfNights)N \(cityName) - ".width(withConstraintedHeight: 30, font:font);
                        
                        return CGSize(width: width , height: 30)
                    }
                    
                    
                    
                }
                
            }
            
        }
        
        return  CGSize(width: 130.0, height:30)
    }
    
    //MARK: -  Tour Detail Header Image SlideShow Implementaion
    private func setHeaderImages(_ uiImages:[ImageSource])  {
        
        
        tourDetailHeaderImageSlideShow.backgroundColor = UIColor.white
        tourDetailHeaderImageSlideShow.slideshowInterval = 5.0
        tourDetailHeaderImageSlideShow.pageControlPosition = PageControlPosition.insideScrollView
        tourDetailHeaderImageSlideShow.pageControl.currentPageIndicatorTintColor = UIColor.lightGray
        tourDetailHeaderImageSlideShow.pageControl.pageIndicatorTintColor = UIColor.black
        tourDetailHeaderImageSlideShow.contentScaleMode = UIView.ContentMode.scaleToFill
        
        // optional way to show activity indicator during image load (skipping the line will show no activity indicator)
        tourDetailHeaderImageSlideShow.activityIndicator = DefaultActivityIndicator()
        tourDetailHeaderImageSlideShow.currentPageChanged = { page in
            printLog("Tour Details current page:", page)
        }
        
        // can be used with other sample sources as `afNetworkingSource`, `alamofireSource` or `sdWebImageSource` or `kingfisherSource`
        tourDetailHeaderImageSlideShow.setImageInputs(uiImages)
        
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(self.tourDetailsHeaderImageHeaderDidTap))
        tourDetailHeaderImageSlideShow.addGestureRecognizer(recognizer)
        self.view.layoutIfNeeded()
    }
    
    @objc func tourDetailsHeaderImageHeaderDidTap() {
        let fullScreenController = tourDetailHeaderImageSlideShow.presentFullScreenController(from: self)
        // set the activity indicator for full screen controller (skipping the line will show no activity indicator)
        fullScreenController.slideshow.activityIndicator = DefaultActivityIndicator(style: .white, color: nil)
    }
    
    //MARK:-Deviation Details
    @IBAction func onDeviationDetailsBtnClick(_ sender: UIButton) {
        
        presentPassengerPopover()
    }
    
    
    
    
    public func setBannerImage() {
        
        let tourPackageImages:Array<TourPackageImages> = (self.tourDetail!.packageImagesRelation?.allObjects as? Array<TourPackageImages>)!
        
        var imagesFilePath:Array<String>?
        
        var imageSources:Array<ImageSource> = []
        
        
        if(tourPackageImages != nil && tourPackageImages.count > 0)
        {
            
            let filePath = TourDocumentController.getTourFileDirectoryPath(bfnNumber: (tourDetail?.bfNumber!)! )
            
            imagesFilePath = AppUtility.getAllImagePresentInDirectory(directoryPath: filePath.bookingListPath)
            
            /*  for item in imagesFilePath! {
             
             
             if let image = UIImage(contentsOfFile: item)
             {
             imageSources.append(ImageSource(image: image))                   }
             }*/
            
            if imagesFilePath != nil && (imagesFilePath?.count)! > 0
            {
                if let item = imagesFilePath?.first
                {
                    
                    if let image = UIImage(contentsOfFile: item)
                    {
                        imageSources.append(ImageSource(image: image))
                    }
                }
            }
            
            if imageSources.count > 0 {
                
                self.setHeaderImages(imageSources)
                
            }else{
                imageSources.append(ImageSource(image: #imageLiteral(resourceName: "defaultImage")))
                
                self.setHeaderImages(imageSources)
            }
            
            
            
        }else{
            imageSources.append(ImageSource(image: #imageLiteral(resourceName: "defaultImage")))
            
            self.setHeaderImages(imageSources)
        }
        
        
        
    }
    
    func displatAlert(message:String)
    {
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: AlertMessage.TITLE_OK, style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    //MARK: TourDetailProtocol
    func onBackShowMoreOptionView() {
        
        
        self.presentPopover()
    }
    
    func showNavigationBarTranslucent()  {
        
        self.navigationController?.navigationBar.isHidden = false
        //self.navigationController?.setRedNavigationBar()
         self.navigationController?.setNavigationBarTranslucent()
        self.navigationController?.setnavigatiobBarTitle(title: " ", viewController: self)
        //self.navigationController?.setNagationRightButton(showRightButton: true, viewController: self)
        //   self.navigationController?.setNagationBackButton(showRightButton: true, viewController: self)
        
    }
    
    //MARK: - Download Doc
    
    func downloadDocument(filePath:String ,url:String) {
        
        //  let passengerDocumnet:TourPassengerDouments =  documentArray.first!
        
        LoadingIndicatorView.show("Loading")
        
        NetworkCommunication.downloadDocuments(urlString: url, destinationFilePath: filePath, completion: { (downlodedfilePath, status) in
            
            printLog(status,filePath);
            
            if(status)
            {
                self.viewDownloadedFile(fileurl: URL(fileURLWithPath:filePath))
                
            }
            else{
                
                let isFilePresent = TourDocumentController.isFileExistInDocumentDirectory(fileNamePath: filePath)
                
                if(isFilePresent.chkFile)
                {
                    self.viewDownloadedFile(fileurl: URL(fileURLWithPath:filePath))
                    
                }else{
                    
                    DispatchQueue.main.sync {
                        
                        LoadingIndicatorView.hide()
                        
                      AppUtility.displayAlert(message: AlertMessage.NO_DOCUMENT)
                        
                    }
                    
                }
                
            }
            
        })
    }
    
    //MARK: UIDocumentInteractionController Delegate
    
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController
    {
        return self
    }

    func viewDownloadedFile(fileurl : URL) -> Void
    {
        
        let concurrentQueue = DispatchQueue(label: "queuename", attributes: .concurrent)
        concurrentQueue.sync {
            
            //DispatchQueue.main.sync {
            
            let documentController : UIDocumentInteractionController = UIDocumentInteractionController.init(url: fileurl)
            
            //self.documentController.uti =
            
            documentController.name  = Title.VIEW_PDF
            
            documentController.delegate = self
            
            documentController.presentPreview(animated: true)
            
            LoadingIndicatorView.hide()
            
            // }
            
        }
        
    }
    
    
    @IBAction func ontapClaimDiscount(_ sender: Any) {
        
        let destination:String = self.tourDetail?.region ?? " ";
        
        let destinationPkg:String = self.tourDetail?.tourZone ?? " ";
       
        
            LoadingIndicatorView.show()
        
        UserBookingController.getcheckDiscountService(destinationName: destination){(status,responseData) in
            printLog("\(responseData)")
            if(responseData.count>0){
                
                let discountArr:[Dictionary<String,Any>] = responseData["data"] as! [Dictionary<String,Any>]
                
                let discountDict:Dictionary<String,Any> = discountArr.first as! Dictionary<String,Any>
                
                let passengerArray = self.getPassengers()
                var bookedOptionDetails:Array<Dictionary<String,String>> = Array<Dictionary<String,String>>()
                
                for  passenger in passengerArray{   //discount send only against master pax.
                    
                    if passenger.paxType?.lowercased() != "inf" && passenger.paxType?.lowercased() != "infant"{
                        
                    
                    
                    var dict:Dictionary<String,String> = Dictionary()
                    
                  
                    
                    dict[AdhocBookingConstant.PAX_NUMBER] = passenger.passengerNumber
                    
                    let amount:String = discountDict["amount"] as? String ?? " "
                    
                    if(amount == "1000"){
                         dict[AdhocBookingConstant.ADHOC_DISCOUNT] = "\(discountDict["currency"] as? String ?? " ")=\(discountDict["amount"] as? String ?? " ")"
                        
                    }else if(amount == "2000"){
                         dict[AdhocBookingConstant.ADHOC_DISCOUNT] = "\(discountDict["currency"] as? String ?? " ")=\(discountDict["amount"] as? String ?? " ")" //USD harcode value remove
                        
                    }else if(amount == "3000"){
                         dict[AdhocBookingConstant.ADHOC_DISCOUNT] = "\(discountDict["currency"] as? String ?? " ")=\(discountDict["amount"] as? String ?? " ")"
                        
                    }else{
                        dict[AdhocBookingConstant.ADHOC_DISCOUNT] = "\(discountDict["currency"] as? String ?? " ")=\(discountDict["amount"] as? String ?? " ")"
                    }
                   
                    dict[AdhocBookingConstant.REMARKS] = "CSS App Discount"
                    bookedOptionDetails.append(dict)
                }
                }
                
                if bookedOptionDetails != nil && bookedOptionDetails.count > 0
                {
                    
                   UserBookingController.sendAdhocRequest(bfNumber: (self.tourDetail?.bfNumber)!, bookedOptionalDetails: bookedOptionDetails) { (status) in
                    
                    if(status.statusCode == 200){
                        UserBookingController.getSubmittedDiscount(bfnumber: (self.tourDetail?.bfNumber)!){(checkstatus) in
                            if(checkstatus){
                                
                                self.displatAlert(message: "Discount successfully applied against booking")
                                self.getTourDetail()
                            }else{
                                printLog("error-----")
                                self.displatAlert(message: "Discount successfully not submitted, please contact to customercare")
                                
                                
                            }
                        }
                        printLog(status)
                    }else{
                        printLog(status)
                        self.displatAlert(message: "Discount successfully not submitted, please contact to customercare")
                        
                    }
                    
                    
                    }
                    
                
                }
            }
            
        };
        
        
       

    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromNSAttributedStringKey(_ input: NSAttributedString.Key) -> String {
	return input.rawValue
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}
