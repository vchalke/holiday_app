//
//  MyFavouriteVC.m
//  holidays
//
//  Created by ROHIT on 28/12/15.
//  Copyright © 2015 Mobicule. All rights reserved.
//

#import "MyFavouriteVC.h"
#import "MyFavouriteCell.h"
#import "Holiday.h"
#import "HolidayPackageDetail.h"
#import "TabMenuVC.h"
//#import "LoginViewPopUp.h"
#import "NewLoginPopUpViewController.h"
#import "UIViewController+MJPopupViewController.h"
#import "NetCoreAnalyticsVC.h"


#define kType @"webservice"
#define kAction @"search"
#define kEntity @"myAccountInfo"
#define kUserName @"userName"

@interface MyFavouriteVC ()
{
    NSMutableArray *arrayForWishList;
    NSMutableDictionary *payLoadForViewDetails;
    HolidayPackageDetail *packageDetail;
}

@end

@implementation MyFavouriteVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSBundle mainBundle]loadNibNamed:@"MyFavouriteVC" owner:self options:nil];
    [super addViewInBaseView:_MyFavouriteView];

    super.HeaderLabel.text=_headerName;
    [self.tableviewForMyFav reloadData];
    payLoadForViewDetails = [[NSMutableDictionary alloc] init];
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    arrayForWishList=[[NSMutableArray alloc]init];
    _tableviewForMyFav.delegate=self;
    _tableviewForMyFav.dataSource=self;
    
//    NSUserDefaults *LoginStatus=[NSUserDefaults standardUserDefaults];
//    NSString *statusForLogin= [LoginStatus valueForKey:kLoginStatus];
    
    [self dismissLoginPopUp];
//    if ([statusForLogin isEqualToString:kLoginSuccess]) {
//        [self getWishList];
//    }
    

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma Uitablview Delegate and DataSource Methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (arrayForWishList.count>0) {
        return [arrayForWishList count];
    }
    return 0;
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier= @"Cell";
    
    MyFavouriteCell *cell=(MyFavouriteCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell==nil) {
       // cell=[[MyFavouriteCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
         cell=(MyFavouriteCell *)[[[NSBundle mainBundle] loadNibNamed:@"MyFavouriteCell" owner:self options:nil] lastObject];
    }
        if (arrayForWishList.count>0) {
        
   NSMutableDictionary *tempDict=[arrayForWishList objectAtIndex:indexPath.row];
    if ([[tempDict valueForKey:@"airFlag"] isEqualToString:@"true"]) {
        //cell.btnFlight.hidden=NO;
        //cell.btnVisa.hidden=NO;
        cell.constraintWidthFlight.constant = 40;
    }
    if ([[tempDict valueForKey:@"mealsFlag"] isEqualToString:@"true"]) {
        //cell.btnLunch.hidden=NO;
        cell.constraintWidthFood.constant = 40;
    }
    if ([[tempDict valueForKey:@"sightSeeingFlag"] isEqualToString:@"true"]) {
        //cell.btnSight.hidden=NO;
        cell.constraintWidthSight.constant = 40;
    }
    if ([[tempDict valueForKey:@"accomFlag"]isEqualToString:@"true"]) {
        //cell.btnHotel.hidden=NO;
        cell.constraintWidthhotel.constant = 40;
    }
    if ([tempDict valueForKey:@"tourMngerFlag"])
    {
         cell.constraintWidthVisa.constant = 40;
    }
           
        cell.lblpriceList.text=[NSString stringWithFormat:@"%@.%@",@"Rs",[tempDict valueForKey:@"startingPrice"]];
    cell.lblPackageName.text=[tempDict valueForKey:@"pkgName"];
           [cell.btnViewDetails addTarget:self action:@selector(clickViewDetail1:) forControlEvents:UIControlEventTouchUpInside];
    }
     return cell;

}

#pragma Get WishList Api Methods

-(void)getWishList{
    
    
    NSUserDefaults *userIdForGooglePlusSignIn=[NSUserDefaults standardUserDefaults];
    NSString *userId=[userIdForGooglePlusSignIn valueForKey:kLoginEmailId];
 
    
    NSDictionary *dictOfData = [NSDictionary dictionaryWithObjectsAndKeys:userId,kUserName,nil];
    
    activityIndicator = [LoadingView loadingViewInView:self.MyFavouriteView.superview withString:@"" andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
    
    if ([super connected])
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            NetworkHelper *helper = [NetworkHelper sharedHelper];
            
            NSString *strResponse = [helper getDataFromServerForType:kType entity:kEntity action:kAction andUserJson:dictOfData];
            
            NSString *strStatus = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"status"];
            NSDictionary *dictOfData=[[NSDictionary alloc]init];
            if ([strStatus isEqualToString:kStatusSuccess])
            {
                dictOfData= [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:kServerResponseKeyData];
                arrayForWishList=[dictOfData objectForKey:@"wishLists"];
                if (arrayForWishList.count>0) {
                    [self.tableviewForMyFav reloadData];

                }
                               //wishLists
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    if ([activityIndicator isDescendantOfView:self.MyFavouriteView.superview])
                    {
                        [self.tableviewForMyFav reloadData];
                        [activityIndicator removeView];
                    }
                    
                    
                });
                
            }
            else
            {
                NSString *messsage = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"message"];
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    if ([activityIndicator isDescendantOfView:self.MyFavouriteView.superview])
                    {
                        [activityIndicator removeView];
                    }
                                       if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
                    {
                        
                        if (messsage)
                        {
                            if ([messsage isEqualToString:kVersionUpgradeMessage])
                            {
                                NSDictionary *downloadDict = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"data"];
                                NSString *downloadURL = [downloadDict valueForKey:@"download_url"];
                                [super showAlertViewForVersionUpdateWithUrl:downloadURL];

                            }
                        }

                        else{
                             UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Info"
                                                                                           message:@" Favourite Package not available"
                                                                                    preferredStyle:UIAlertControllerStyleAlert];
                        
                              UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                            
                                 [alertView dismissViewControllerAnimated:YES completion:nil];
                            
                             }];
                        
                             [alertView addAction:actionOk];
                        
                            [self presentViewController:alertView animated:YES completion:nil];
                        }
                    }
                    else
                    {
                        if (messsage)
                        {
                            if ([messsage isEqualToString:kVersionUpgradeMessage])
                            {
                                NSDictionary *downloadDict = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"data"];
                                NSString *downloadURL = [downloadDict valueForKey:@"download_url"];
                                [super showAlertViewForVersionUpdateWithUrl:downloadURL];
                            }
                        }
                        else{

                            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Info" message:@"Favourite Package not available" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                        
                             [alert show];
                        }
                    }
                });
            }
            
        });
        
    }
    else
    {
        
        
        if([activityIndicator isDescendantOfView:self.MyFavouriteView.superview])
        {
            [activityIndicator removeView];
        }
        
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
        {
            UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Info"
                                                                               message:kMessageNoInternet
                                                                        preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                
                [alertView dismissViewControllerAnimated:YES completion:nil];
                
            }];
            
            [alertView addAction:actionOk];
            
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Info"
                                                           message:kMessageNoInternet
                                                          delegate:nil
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles:nil, nil];
            
            [alert show];
        }
    }

    
}
#pragma mark- View Detail Action

-(void)clickViewDetail1:(UIButton*)sender{
    
    CGPoint center= sender.center;
    CGPoint rootViewPoint = [sender.superview convertPoint:center toView:self.tableviewForMyFav];
    NSIndexPath *indexPath = [self.tableviewForMyFav indexPathForRowAtPoint:rootViewPoint];
    NSLog(@"%ld",(long)indexPath.row);
    Holiday *objHoliday = [arrayForWishList objectAtIndex:indexPath.row];
   // objHoliday.strPackageId=[[arrayForWishList objectAtIndex:indexPath.row]valueForKey:@"pkgId" ];
    activityIndicator= [LoadingView loadingViewInView:self.view.superview.superview
                                              withString:@""
                                       andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
    
    [self performSelectorInBackground:@selector(fetchPackageDetails:) withObject:objHoliday];
}

-(void)fetchPackageDetails:(Holiday *)objHoliday
{
    NSDictionary *dictOfData = [[NSDictionary alloc]init];
   // [dictOfData setValue:objHoliday.strPackageId forKey:@"packageId"];
    dictOfData=[NSDictionary dictionaryWithObjectsAndKeys:[objHoliday valueForKey:@"pkgId"],@"packageId",nil];
    if([CoreUtility connected])
    {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NetworkHelper *helper = [NetworkHelper sharedHelper];
        NSString *strResponse = [helper getDataFromServerForType:@"webservice" entity:@"package" action:@"details" andUserJson:dictOfData];
        NSString *strStatus = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"status"];
        
        if ([strStatus isEqualToString:kStatusSuccess])
        {
            NSArray *dataArray = [[JsonSerealizer arrayOfJSonFromJsonData:strResponse] valueForKey:@"data"];
            NSDictionary *dataDict;
            if (dataArray.count != 0)
            {
                dataDict = dataArray[0];
                
                packageDetail = [[HolidayPackageDetail alloc]initWithDataDict:dataDict];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    if ([activityIndicator isDescendantOfView:self.view.superview.superview])
                    {
                        [activityIndicator removeView];
                        activityIndicator= nil;
                    }
                    TabMenuVC *tabMenuVC = [[TabMenuVC alloc]initWithNibName:@"BaseViewController" bundle:nil];
                    tabMenuVC.headerName =[objHoliday valueForKey:@"pkgName"]; //objHoliday.strPackageName;
                    tabMenuVC.flag=@"Overview";
                    tabMenuVC.packageDetail = packageDetail;
                    tabMenuVC.completePackageDetail = dataArray;
                    [[SlideNavigationController sharedInstance] pushViewController:tabMenuVC animated:YES];
                });
                
            }
        }else{
            NSString *messsage = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"message"];
            if (messsage)
            {
                if ([messsage isEqualToString:kVersionUpgradeMessage])
                {
                    NSDictionary *downloadDict = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"data"];
                    NSString *downloadURL = [downloadDict valueForKey:@"download_url"];
                    [super showAlertViewForVersionUpdateWithUrl:downloadURL];

                }
                else
                {
                    [super showAlertViewWithTitle:@"Alert" withMessage:messsage];
                }
            }else
            {
                [super showAlertViewWithTitle:@"Alert" withMessage:kMessageSomeErrorTryAfterSometime];
            }
            
        }
        
    });
    }
    else
    {
        [super showAlertViewWithTitle:@"Alert" withMessage:kMessageNoInternet];
    }
}

#pragma mark- Check Login Status

-(void)dismissLoginPopUp{
    
    
    NSUserDefaults *LoginStatus=[NSUserDefaults standardUserDefaults];
    NSString *statusForLogin= [LoginStatus valueForKey:kLoginStatus];
    

    
    if ([statusForLogin isEqualToString:kLoginSuccess]) {
        
        _tableviewForMyFav.hidden=NO;
         [self getWishList];
    
    }
    
}


- (IBAction)actionForLogin:(id)sender {
  /*  LoginViewPopUp *loginViewObj = [[LoginViewPopUp alloc] initWithNibName:@"LoginViewPopUp" bundle:nil];
   // loginViewObj.delegate=self;
      loginViewObj.view.frame = CGRectMake(0, 0, 300, self.MyFavouriteView.frame.size.height- 100);
    //[self presentPopupViewController:loginViewObj animationType:MJPopupViewAnimationFade];
    [self presentPopupViewController:loginViewObj animationType:MJPopupViewAnimationFade];*/ //-- 25 feb 2018
    
    NewLoginPopUpViewController *loginView = [[NewLoginPopUpViewController alloc] initWithNibName:@"NewLoginPopUpViewController" bundle:nil];
    [[SlideNavigationController sharedInstance] toggleLeftMenu];
    
    UINavigationController * loginNavigation = [[UINavigationController alloc] initWithRootViewController:loginView];
    
    [[SlideNavigationController sharedInstance]presentViewController:loginNavigation animated:true completion:^{
        
    }];
    
   // [[SlideNavigationController sharedInstance]pushViewController:loginView animated:YES];
}

-(void)fireAppIntHoViewDetailsEvent
{
    [payLoadForViewDetails setObject:packageDetail.strPackageName forKey:@"s^DESTINATION"];
    [payLoadForViewDetails setObject:[NSString stringWithFormat:@"%d",packageDetail.durationNoDays] forKey:@"s^DURATION"];
    [payLoadForViewDetails setObject:[NSString stringWithFormat:@"%d",packageDetail.packagePrise] forKey:@"s^BUDGET"];
    [payLoadForViewDetails setObject:packageDetail.strPackageId forKey:@"s^PACKAGE_ID"];
    [payLoadForViewDetails setObject:@"App" forKey:@"s^SOURCE"];
    [payLoadForViewDetails setObject:[NSNumber numberWithInt:packageDetail.packagePrise] forKey:@"i^PACKAGE_PRICE"];
    
    if ([[packageDetail.strPackageSubType lowercaseString]isEqualToString:@"git"])
    {
        [payLoadForViewDetails setObject:@"no" forKey:@"s^PERSONALIZED_TOUR"];
    }
    else
    {
        [payLoadForViewDetails setObject:@"yes" forKey:@"s^PERSONALIZED_TOUR"];
    }
    
    if(packageDetail.strPackageType != nil && ![packageDetail.strPackageType isEqualToString:@""])
    {
        [payLoadForViewDetails setObject:packageDetail.strPackageType forKey:@"s^TYPE"];
    }
    
    //[NetCoreAnalyticsVC getNetCorePushNotificationDetailDictionary:payLoadForViewDetails withPayloadCount:105];
    
    [NetCoreAnalyticsVC getNetCorePushNotificationDetailDictionary:payLoadForViewDetails withPayloadCount:125];
}


@end
