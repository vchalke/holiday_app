//
//  FilteredOptionsVC.m
//  holidays
//
//  Created by Kush_Tech on 18/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "FilteredOptionsVC.h"
#import "HeaderTwoLineTableViewCell.h"
@interface FilteredOptionsVC ()
{
    NSArray *titleArray;
}
@end

@implementation FilteredOptionsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    self.selectedOptionsArray = [[NSMutableArray alloc]init];
    [self.table_Views registerNib:[UINib nibWithNibName:@"HeaderTwoLineTableViewCell" bundle:nil] forCellReuseIdentifier:@"HeaderTwoLineTableViewCell"];
    self.lbl_headingTitle.text = self.titles;
    
    
    
//    if ([self.title isEqualToString:@"Departure City"]){
//        titleArray = [NSArray arrayWithObjects:@"Mumbai",@"Jaipur",@"Bhuwaneshwar",@"Jabalpur",@"Cochin",@"Pune",@"Moscow",@"Vadodara", nil];
//    }else if ([self.title isEqualToString:@"Duration"]){
//        titleArray = [NSArray arrayWithObjects:@"Less than 7 Nights",@"8 to 12 Nights",@"More than 12 nights", nil];
//    }else if ([self.title isEqualToString:@"Theme"]){
//        titleArray = [NSArray arrayWithObjects:@"Family",@"Romantic",@"Youth",@"Cruise",@"Kid Special",@"Wild Life",@"Honeymoon",@"Popular",@"Beach",@"Adventure", nil];
//    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)btn_OkPress:(id)sender {
    
    [self.filterOptionsDelgate showSelectedOptionsFor:self.titles withArray:self.selectedOptionsArray];
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)btn_BackPress:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.allListArray count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
     static NSString *cellIdentifier = @"HeaderTwoLineTableViewCell";
       HeaderTwoLineTableViewCell *cell = (HeaderTwoLineTableViewCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.lbl_titleOne.text = [self.allListArray objectAtIndex:indexPath.row];
    cell.accessoryType =  [self.selectedOptionsArray containsObject:[self.allListArray objectAtIndex:indexPath.row]] ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone ;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"selectedOptionsArray %@",self.selectedOptionsArray);
    if ([self.selectedOptionsArray containsObject:[self.allListArray objectAtIndex:indexPath.row]]){
        [self.selectedOptionsArray removeObject:[self.allListArray objectAtIndex:indexPath.row]];
    }else{
    [self.selectedOptionsArray addObject:[self.allListArray objectAtIndex:indexPath.row]];
    }
    [self.table_Views reloadData];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}


@end
