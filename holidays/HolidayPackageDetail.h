//
//  HolidayPackageDetail.h
//  holidays
//
//  Created by ketan on 06/10/15.
//  Copyright © 2015 Mobicule. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HolidayPackageDetail : NSObject

-(instancetype)initWithDataDict:(NSDictionary *)dataDict;

@property (strong,nonatomic) NSArray *arrayItinerary;
@property (strong,nonatomic) NSArray *arrayHubList;
@property (strong,nonatomic) NSString *packageMRP;
@property (strong,nonatomic) NSString *isHSA;
@property (strong,nonatomic) NSString *ltItineraryCode;

@property (strong,nonatomic) NSString *stringRegion;
@property (strong,nonatomic) NSString *packageContinent;

@property (strong,nonatomic) NSDictionary *dictBookingAmt;
@property (strong ,nonatomic) NSString *stringTermsConditions;
@property (strong ,nonatomic) NSArray *arrayTermsAndConditions;
@property (strong,nonatomic) NSArray *arrayAccomTypeList;

@property(nonatomic,strong)NSArray *arrayOfHubList;
@property(nonatomic,strong)NSString *strPackageDesc;
@property(nonatomic,strong)NSString *strPackageName;
@property (nonatomic) BOOL mealsFlag;
@property (nonatomic) BOOL airFlag;
@property (nonatomic) BOOL tourMngerFlag;//as Visa Flag
@property (nonatomic) BOOL accomFlag;
@property (nonatomic) BOOL offerAvailableFlag;
@property (nonatomic) BOOL sightSeeingFlag;
@property (nonatomic) int durationNoDays;
@property (nonatomic) int packagePrise;
@property (nonatomic,strong) NSString *Offers;
@property (nonatomic , strong) NSString *strPackageSubType;
@property (nonatomic , strong) NSString *strPackageSubTypeID;
@property (nonatomic,strong) NSArray *timeLineList;
@property (nonatomic,strong) NSString *strPackageId;
@property (nonatomic,strong) NSString *strPackageType;
@property (nonatomic,strong) NSString *stringBookingOnline;
@property (nonatomic ,strong) NSArray *arrayPackageImagePathList;
@property (nonatomic ,strong) NSArray *arrayImages;
@property (nonatomic,strong) NSString *stringIsOnReq;
@property (nonatomic,strong) NSString *stringNotesDesc;
@property (nonatomic,strong) NSString *stringAmountINRFIT;
@property (nonatomic,strong) NSString *stringSelectedAccomType;
@property (nonatomic,strong) NSString *stringPopularFlag;
@property (nonatomic,strong) NSString *ltProdCode;
@property (nonatomic,strong) NSString *ltMarket;
@property (nonatomic ,strong) NSArray *arrayLtPricingCollection;
@property (nonatomic ,strong) NSArray *arraytcilHolidayPriceCollection;
@property (nonatomic ,strong)NSArray *categoryCollection;
@property (nonatomic ,strong)NSMutableArray *mealCollection;
@property (nonatomic ,strong)NSMutableArray *flightCollection;
@property (nonatomic ,strong)NSMutableArray *visaCollection;
@property (nonatomic ,strong)NSMutableArray *hotelCollection;
@property (nonatomic ,strong)NSMutableArray *sightseenCollection;
@property (nonatomic ,strong)NSMutableArray *accombdationCollection;
@property (nonatomic ,strong)NSMutableArray *tcilHolidayOffersCollection;
@property (nonatomic ,strong)NSMutableArray *tcilHolidayFlightsCollection;
@property (nonatomic) NSInteger pkgStatusId;
@property (nonatomic,strong) NSString * productID;
@property (strong,nonatomic) NSString *packageMode;

//Vijay Added
@property (nonatomic) BOOL transferFlag;
@property(nonatomic,strong)NSArray *arrayOfThemeList;
@property(nonatomic,strong)NSDictionary *packageDetailDictObj;

@property (nonatomic) int startingPriceStandard;
@property (nonatomic) int startingPriceDelux;
@property (nonatomic) int startingPricePremium;
@property (nonatomic) int strikeoutPriceStandard;
@property (nonatomic) int strikeoutPriceDelux;
@property (nonatomic) int strikeoutPricePremium;
@property (nonatomic ,strong)NSMutableArray *transferCollection;
@property (nonatomic) NSInteger mainPkgSubClassId;
@property (strong,nonatomic) NSString *flightDefaultMsg;
@property (strong,nonatomic) NSString *hotelDefaultMsg;
@property (strong,nonatomic) NSString *isSightseeingDefaultMsg;
@end
