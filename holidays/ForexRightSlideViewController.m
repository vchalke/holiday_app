//
//  ForexRightSlideViewController.m
//  holidays
//
//  Created by ketan on 10/01/18.
//  Copyright © 2018 Mobicule. All rights reserved.
//

#import "ForexRightSlideViewController.h"
#import "UIColor+HexToColor.h"


@interface ForexRightSlideViewController ()
{
    NSMutableArray      *sectionTitleArray;
    NSMutableDictionary *sectionContentDict;
    NSMutableArray      *arrayForBool,*arrayOfMenuImages;
    NSIndexPath *preIndexPath;
    KLCPopup *customePopUp;
    int keyBoardHeight;
    int txtFieldheight;
    UIView *popUpViewFrame;
    NSMutableDictionary *payloadList;

}
@end

@implementation ForexRightSlideViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (!sectionTitleArray)
    {
        sectionTitleArray = [NSMutableArray arrayWithObjects:@"Home", @"Pending Transaction", @"Foreign Exchange Products", @"Holiday & Other Products", @"Manage Holidays"/*@"Self Service"*//*@"My Account"*/, @"Login",nil];
    }
    
    if (!arrayOfMenuImages)
    {
        arrayOfMenuImages = [NSMutableArray arrayWithObjects:@"home", @"holidays",@"forex_list",@"flight_list", @"hotel",@"visa_menuIcon", nil];
    }
    if (!arrayForBool)
    {
        arrayForBool = [[NSMutableArray alloc]init];
        
        for (int i = 0 ; i < [sectionTitleArray count]; i++)
        {
            [arrayForBool addObject:[NSNumber numberWithBool:NO]];
        }
    }
    
    if (!sectionTitleArray)
    {
        sectionTitleArray = [NSMutableArray arrayWithObjects:@"Home", @"Holiday", @"Forex", @"Flight", @"Hotels", @"Visa",@"Insurance",@"My Favourite",@"Manage Holidays"/*@"Self Service"*//*@"My Account"*/,@"Customer Support", nil];
    }
    
    if (!arrayOfMenuImages)
    {
        arrayOfMenuImages = [NSMutableArray arrayWithObjects:@"home", @"holidays",@"forex_list",@"flight_list",
                             @"hotel",@"visa",@"insurance",@"heart_footer@2x",@"my_account",
                             @"customer_support", nil];
    }
    
    if (!sectionContentDict)
    {
        sectionContentDict  = [[NSMutableDictionary alloc] init];
        NSArray *array0     = [NSArray new];
        
        [sectionContentDict setValue:array0 forKey:[sectionTitleArray objectAtIndex:0]];
        NSArray *array1     = [NSArray arrayWithObjects:@"International Holidays", @"Indian Holidays",nil];
        
        [sectionContentDict setValue:array1 forKey:[sectionTitleArray objectAtIndex:1]];
        NSArray *array2     = [NSArray arrayWithObjects:@"Foreign Exchange",@"Buy Forex",@"Sell Forex",@"Money Transfer",@"Reload Forex Card",@"Currency Converter",@"Live Rates", nil];
        
        [sectionContentDict setValue:array2 forKey:[sectionTitleArray objectAtIndex:2]];
        
        
        NSArray *array3     = [NSArray new];
        
        [sectionContentDict setValue:array3 forKey:[sectionTitleArray objectAtIndex:3]];
        NSArray *array4     = [NSArray new];
        
        [sectionContentDict setValue:array4 forKey:[sectionTitleArray objectAtIndex:4]];
        NSArray *array5     = [NSArray new];
        
        [sectionContentDict setValue:array5 forKey:[sectionTitleArray objectAtIndex:5]];
        NSArray *array6     = [NSArray new];
        
        [sectionContentDict setValue:array6 forKey:[sectionTitleArray objectAtIndex:6]];
        NSArray *array7     = [NSArray new];
        
        //@"My Bookings",
        [sectionContentDict setValue:array7 forKey:[sectionTitleArray objectAtIndex:7]];
        NSArray *array8     = [NSArray arrayWithObjects:@"Profile", nil];
        
        // @"Visit Us",
        [sectionContentDict setValue:array8 forKey:[sectionTitleArray objectAtIndex:8]];
        NSArray *array9    = [NSArray arrayWithObjects:@"Call Us",@"Visit Us",nil];
        
        [sectionContentDict setValue:array9 forKey:[sectionTitleArray objectAtIndex:9]];
        //@"Email Us",@"Partnership enquiry",@"Escalation"
    }
    
    payloadList =  [NSMutableDictionary dictionary];
    

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - TableView Delegates and Data Source

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [sectionTitleArray count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([[arrayForBool objectAtIndex:section] boolValue])
    {
        return [[sectionContentDict valueForKey:[sectionTitleArray objectAtIndex:section]] count];
    }
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIImageView *imageUpDownArrow;
    
    UIView *headerView         = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    headerView.tag             = section;
    headerView.backgroundColor = [UIColor whiteColor];
    
    if (section == 8 || section == 9)
    {
        headerView.backgroundColor = [UIColor whiteColor];
    }
    
    UIImageView *imageForSectionHeader = [[UIImageView alloc]initWithFrame:CGRectMake(17, 11, 22, 22)];
    UILabel     *lblForSectionHeader   = [[UILabel alloc]initWithFrame:CGRectMake(60, 12, self.view.frame.size.width - 55,22 )];
    
    UIImageView *imageViewBottom = [[UIImageView alloc]initWithFrame:CGRectMake(0, 43, self.view.frame.size.width, 1)];
    
    imageViewBottom.image = [UIImage imageNamed:@"horizonatl_divider"];
    
    lblForSectionHeader.textAlignment = NSTextAlignmentLeft;
    imageForSectionHeader.image       = [UIImage imageNamed:[arrayOfMenuImages objectAtIndex:section]];
    
    lblForSectionHeader.font          = [UIFont fontWithName:TITILLUM_REGULAR size:17.0];
    lblForSectionHeader.textColor     = [UIColor darkGrayColor];
    lblForSectionHeader.text          = [sectionTitleArray objectAtIndex:section];
    
    [headerView addSubview:lblForSectionHeader];
    [headerView addSubview:imageForSectionHeader];
    [headerView addSubview:imageViewBottom];
    
    BOOL manyCells = [[arrayForBool objectAtIndex:section]boolValue];
    
    if (manyCells)
    {
        headerView.backgroundColor = [UIColor whiteColor];
       // NSString *strImageName     = [NSString stringWithFormat:@"%@_highlighted.png",[arrayOfMenuImages objectAtIndex:section ]];
        
       // imageForSectionHeader.image = [UIImage imageNamed:strImageName];
        lblForSectionHeader.textColor = [UIColor darkGrayColor];
    }
    
    UITapGestureRecognizer *headerTapped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sectionHeaderTapped:)];
    [headerView addGestureRecognizer:headerTapped];
    
    if (section == 1 || section == 8 || section == 9|| section == 2 )
    {
        CGRect rectForUpDownArrow = CGRectMake((self.tblViewMenu.frame.size.width * 0.68), 13, 15,15);
        imageUpDownArrow = [[UIImageView alloc]initWithImage:manyCells ?[UIImage imageNamed:@"minus"] : [UIImage imageNamed:@"plus_light_gray"]];
        imageUpDownArrow.contentMode = UIViewContentModeScaleAspectFit;
        imageUpDownArrow.autoresizingMask    = UIViewAutoresizingFlexibleLeftMargin;
        imageUpDownArrow.frame               = rectForUpDownArrow;
        [headerView addSubview:imageUpDownArrow];
        
    }
    
    return headerView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    
    
    BOOL myCells = [[arrayForBool objectAtIndex:indexPath.section]boolValue];
    
    if (myCells)
    {
        NSArray *arrayOfcontent = [sectionContentDict valueForKey:[sectionTitleArray objectAtIndex:indexPath.section]];
        
        cell.contentView.backgroundColor = [UIColor whiteColor];
        
        UILabel *lblCellText  = [[UILabel alloc]initWithFrame:CGRectMake(40, 15, 150, 15)];
        lblCellText.font      = [UIFont fontWithName:TITILLUM_REGULAR size:15];
        lblCellText.text      = [arrayOfcontent objectAtIndex:indexPath.row];
        lblCellText.textColor = [UIColor darkGrayColor];
        
//        UIImageView *imageViewForArrow = [[UIImageView alloc]initWithFrame:CGRectMake(20, 17, 10, 10)];
//        imageViewForArrow.image = [UIImage imageNamed:@"arrow"];
        
    //    [cell.contentView addSubview:imageViewForArrow];
        [cell.contentView addSubview:lblCellText];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
}


#pragma mark - gesture tapped
- (void)sectionHeaderTapped:(UITapGestureRecognizer *)gestureRecognizer
{
    payloadList =  [NSMutableDictionary dictionary];
    
    NSLog(@"%ld",gestureRecognizer.view.tag);
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:gestureRecognizer.view.tag];
    BOOL collapsed  = [[arrayForBool objectAtIndex:indexPath.section] boolValue];
    
    if (gestureRecognizer.view.tag == 0)
    {
        
    }
    else if (gestureRecognizer.view.tag == 1  || gestureRecognizer.view.tag == 8 || gestureRecognizer.view.tag == 9 ||gestureRecognizer.view.tag == 2)
    {
        
        if (preIndexPath != indexPath)
        {
            [self collapseExpanded:preIndexPath];
        }
        
        if (indexPath.row == 0)
        {
            preIndexPath=indexPath;
            
            collapsed       = !collapsed;
            [arrayForBool replaceObjectAtIndex:indexPath.section withObject:[NSNumber numberWithBool:collapsed]];
            
            //reload specific section animated
            NSRange range   = NSMakeRange(indexPath.section, 1);
            NSIndexSet *sectionToReload = [NSIndexSet indexSetWithIndexesInRange:range];
            
            if (collapsed)
            {
                [self.tblViewMenu reloadSections:sectionToReload withRowAnimation:UITableViewRowAnimationRight];
            }
            else
            {
                [self.tblViewMenu reloadSections:sectionToReload withRowAnimation:UITableViewRowAnimationLeft];
            }
        }
        
    }
    else if (gestureRecognizer.view.tag == 5)
    {
   
        
    }
    else if (gestureRecognizer.view.tag == 6)
    {
   
       
    }
    else if (gestureRecognizer.view.tag == 3)//flights
    {
        
   
    }
    else if (gestureRecognizer.view.tag == 4)//hotels
    {
     
    }
    else if (gestureRecognizer.view.tag == 2)//forex
    {
        
    }
    if(gestureRecognizer.view.tag==7)
    {
        
        
    }
    
    if (gestureRecognizer.view.tag != 1  && gestureRecognizer.view.tag != 8 && gestureRecognizer.view.tag != 9)
    {
        
        
    }
    
}

-(void)collapseExpanded:(NSIndexPath*)index{
    
    [arrayForBool replaceObjectAtIndex:index.section withObject:[NSNumber numberWithBool:NO]];
    NSRange range   = NSMakeRange(index.section, 1);
    NSIndexSet *sectionToReload = [NSIndexSet indexSetWithIndexesInRange:range];
    [self.tblViewMenu reloadSections:sectionToReload withRowAnimation:UITableViewRowAnimationLeft];
    
}


@end
