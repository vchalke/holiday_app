//
//  CustomerProfileVC.swift
//  holidays
//
//  Created by Kush_Team on 22/06/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

import UIKit

class CustomerProfileVC: UIViewController,UITableViewDelegate,UITableViewDataSource,TravelDocTableViewCellDelegate {

    @IBOutlet weak var main_ImgBaseView: UIView!
    @IBOutlet weak var btn_close: UIButton!
    
    @IBOutlet weak var img_profile: UIImageView!
    @IBOutlet weak var tableView_profiles: UITableView!
    var profileDetails:[String:Any]?
    var userDetail:Dictionary<String,Any> = Dictionary.init()
    var getCustInfoModel : CustomerProfileModel = CustomerProfileModel()
    var loginCommClasss = LoginCommNewManager.init()
//    var loginCommClasss : LoginCommNewManager?
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView_profiles.register(UINib(nibName: "ProfileHeaderTableCell", bundle: Bundle.main), forCellReuseIdentifier: "ProfileHeaderTableCell")
        self.tableView_profiles.register(UINib(nibName: "AccDetailsTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "AccDetailsTableViewCell")
        self.tableView_profiles.register(UINib(nibName: "GeneralInfoTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "GeneralInfoTableViewCell")
        self.tableView_profiles.register(UINib(nibName: "TravelDocTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "TravelDocTableViewCell")
        self.getProfileDetailsAPI()
        self.main_ImgBaseView.isHidden = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        main_ImgBaseView.addGestureRecognizer(tap)
    }
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        self.main_ImgBaseView.isHidden = true // handling code
    }
    @IBAction func btn_backPress(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 3
    }
    
    func numberOfSections(in tableView: UITableView) -> Int{
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        if (indexPath.row==0){
            let cell :AccDetailsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "AccDetailsTableViewCell", for: indexPath) as! AccDetailsTableViewCell
//            cell.setProfileDetails(profileDict: profileDetails)
            cell.setProfileModelsDetails(profileModel:self.getCustInfoModel)
            return cell
        }else if (indexPath.row==1){
            let cell :GeneralInfoTableViewCell = tableView.dequeueReusableCell(withIdentifier: "GeneralInfoTableViewCell", for: indexPath) as! GeneralInfoTableViewCell
//            cell.setProfileDetails(profileDict: profileDetails)
             cell.setProfileModelsDetails(profileModel:self.getCustInfoModel)
            return cell
        }
        let cell :TravelDocTableViewCell = tableView.dequeueReusableCell(withIdentifier: "TravelDocTableViewCell", for: indexPath) as! TravelDocTableViewCell
        cell.delegate = self
        cell.setTableViewProfileModelsDetails(profileModel:self.getCustInfoModel)
//        cell.hideFirstView(flag: true)
//        cell.hideSecondView(flag: true)
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 210
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableCell(withIdentifier: "ProfileHeaderTableCell" ) as! ProfileHeaderTableCell
        DispatchQueue.main.async {
//            if #available(iOS 11.0, *) {
//                headerView.setProfileDetails(profileDict: self.profileDetails)
//            }
            headerView.setProfileModelsDetails(profileModel:self.getCustInfoModel)
        }
        return headerView
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            if (indexPath.row==0){
                return 300
            }else if (indexPath.row==1){
                return 240
            }
            return 1200
    }
    //MARK:-  Passports Delegates
    func showPassportVisaDetails() {
        self.tableView_profiles.reloadData()
    }
    func showImageInMainViewWithString(dictionary:Dictionary<String,Any>, withTag:Int){
        let path = (withTag == 0) ? dictionary["page1Path"] as? String : dictionary["page2Path"] as? String
        if var isPath = path{
            if (isPath.contains("http") == false){
                isPath = "https://resources-uatastra.thomascook.in/images/profile/passport/\(isPath)"
            }
            isPath = isPath.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
            if let url = URL.init(string: isPath){
                self.img_profile.sd_setImage(with: url, placeholderImage: UIImage(named: "ProfileActive"))
                self.main_ImgBaseView.isHidden = false
            }
            self.main_ImgBaseView.isHidden = false
        }
        
    }
    
    
    @IBAction func btn_pressHideImg(_ sender: Any) {
        self.main_ImgBaseView.isHidden = true
    }
    
    
    func getProfileDetailsAPI(){
        
        LoadingIndicatorView.show()
        
        PreBookingCommunicationManager.getProfileData(requestType: "GET", queryParam: nil, pathParam: nil, jsonParam: nil, headers: nil, requestURL: kGetCustInfoURL, returnInCaseOffailure: false) { (status, responseData) in
            LoadingIndicatorView.hide()
            if status{
                
                 if let profileDetailsDict:Dictionary<String,Any> = responseData as? Dictionary<String,Any>{
                 self.userDetail = profileDetailsDict
                 print("userDetail \(profileDetailsDict)")
                 self.getCustInfoModel.initWithDictionary(profiledict:profileDetailsDict)
                 print("userDetail firstName \(self.getCustInfoModel.firstName)")
                 print("userDetail lastName \(self.getCustInfoModel.lastName)")
                 print("userDetail profilePicUrl \(self.getCustInfoModel.profilePicUrl)")
                 print("userDetail generalInfo \(self.getCustInfoModel.generalInfo)")
                 print("userDetail addresses \(self.getCustInfoModel.addresses)")
                 DispatchQueue.main.async {
                 self.tableView_profiles.reloadData()
                 }
                 }else{
                 self.alertViewControllerSingleton(title: "Failure", message: "Please Login Again to see your Profile Info", type: .alert, buttonArryName: ["Ok"]) { (strin) in
                 printLog(strin)
                 }
                 printLog("Something Went Wrong One")
                 }
                 
                /*
                if let email_id = UserDefaults.standard.value(forKey: kLoginEmailId) as? String, let pasword = UserDefaults.standard.value(forKey: kLoginPasswords) as? String{
                    if (email_id.count != 0 || pasword.count != 0){
                        //                        self.loginCommClasss = LoginCommNewManager.init()
                        //                        self.loginCommClasss?.checkAccountType(enterEmail: email_id , Type:
                        self.loginCommClasss.checkAccountType(enterEmail: email_id , Type: Constant.userProfile.LOGIN_WITH_PASSWORD) { (flag, msg) in
                            LoadingIndicatorView.hide()
                            if (flag){
                                printLog("Got Success After ReLogin")
                                self.getProfileDetailsAPI()
                            }else{
                                self.alertViewControllerSingleton(title: "Failure", message: "Please Login Again to see your Profile Info", type: .alert, buttonArryName: ["Ok"]) { (strin) in
                                    printLog(strin)
                                }
                            }
                        }
                    }else{
                        self.alertViewControllerSingleton(title: "Failure", message: "Please Login with Password to see your Profile Info", type: .alert, buttonArryName: ["Ok"]) { (strin) in
                            printLog(strin)
                        }
                    }
                }else{
                    self.alertViewControllerSingleton(title: "Failure", message: "Please Login with Password to see your Profile Info", type: .alert, buttonArryName: ["Ok"]) { (strin) in
                        printLog(strin)
                    }
                }
                */
            }else{
                
                if let email_id = UserDefaults.standard.value(forKey: kLoginEmailId) as? String, let pasword = UserDefaults.standard.value(forKey: kLoginPasswords) as? String{
                    if (email_id.count != 0 || pasword.count != 0){
//                        self.loginCommClasss = LoginCommNewManager.init()
//                        self.loginCommClasss?.checkAccountType(enterEmail: email_id , Type:
                        self.loginCommClasss.checkAccountType(enterEmail: email_id , Type: Constant.userProfile.LOGIN_WITH_PASSWORD) { (flag, msg) in
                            LoadingIndicatorView.hide()
                            if (flag){
                                printLog("Got Success After ReLogin")
                                self.getProfileDetailsAPI()
                            }else{
                                self.alertViewControllerSingleton(title: "Failure", message: "Please Login Again to see your Profile Info", type: .alert, buttonArryName: ["Ok"]) { (strin) in
                                    printLog(strin)
                                }
                            }
                        }
                    }else{
                        self.alertViewControllerSingleton(title: "Failure", message: "Please Login with Password to see your Profile Info", type: .alert, buttonArryName: ["Ok"]) { (strin) in
                            printLog(strin)
                        }
                    }
                }else{
                    self.alertViewControllerSingleton(title: "Failure", message: "Please Login with Password to see your Profile Info", type: .alert, buttonArryName: ["Ok"]) { (strin) in
                        printLog(strin)
                    }
                }
                
            }
        }
    }
}

extension UIViewController{
    func alertViewControllerSingleton(title : String, message : String,type : UIAlertController.Style,buttonArryName : [String], completion :@escaping (String)->()){
        let alert = UIAlertController.init(title: title, message: message, preferredStyle: type)
        buttonArryName.forEach { (btnName) in
            alert.addAction(UIAlertAction(title: btnName, style: .default, handler: { (UIAlertAction) in
                self.dismiss(animated:true,completion:nil)
                completion(btnName)
            }))
            
        }
        self.present(alert, animated: true, completion: nil)
    }
}

class CustomerProfileModel: NSObject
{
    var lastName : String
    var firstName : String
    var mobileNo : String
    var profilePicUrl : String
    var accountType : String
    var title : String
    var emailId : String
    var gender : String
    var addresses : Array<Dictionary<String,Any>>
    var passportInfo : Dictionary<String,Any>
    var generalInfo : Dictionary<String,Any>
    var dob : String
    var anniversaryDate : String
    var maritalStatus : String
    var profileGSTInfo : Dictionary<String,Any>
    
    override init() {
        self.lastName = ""
        self.firstName = ""
        self.mobileNo = ""
        self.profilePicUrl = ""
        self.accountType = ""
        self.title = ""
        self.emailId = ""
        self.gender = ""
        self.addresses = Array.init()
        self.passportInfo = Dictionary.init()
        self.generalInfo = Dictionary.init()
        self.dob = ""
        self.anniversaryDate = ""
        self.maritalStatus = ""
        self.profileGSTInfo = Dictionary.init()
    }
    
    func initWithDictionary(profiledict:Dictionary<String,Any>)
    {
//        let packageDetailDict : Dictionary<String,Any> = dict["packageDetail"] as! Dictionary<String, Any>
//        self.arrayPackageImagePathList = packageDetailDict["tcilHolidayPhotoVideoCollection"] as? Array<Any> ?? Array.init()
//        self.tcilHolidaySightseeingCollection = packageDetailDict["tcilHolidaySightseeingCollection"] as? Array<Dictionary<String, Any>> ?? Array.init()
        
      
        self.lastName = profiledict["lastName"] as? String ?? ""
        self.firstName = profiledict["firstName"] as? String ?? ""
        self.mobileNo = profiledict["mobileNo"] as? String ?? ""
        self.profilePicUrl = profiledict["profilePicUrl"] as? String ?? ""
        self.accountType = profiledict["accountType"] as? String ?? ""
        self.title = profiledict["title"] as? String ?? ""
        self.emailId = profiledict["emailId"] as? String ?? ""
        self.gender = profiledict["gender"] as? String ?? ""
        
        self.addresses = profiledict["addresses"] as? Array<Dictionary<String,Any>> ?? Array.init()
        
        self.passportInfo = profiledict["passportInfo"] as? Dictionary<String, Any> ?? Dictionary.init()
        
        self.generalInfo = profiledict["generalInfo"] as? Dictionary<String, Any> ?? Dictionary.init()
        self.dob = self.generalInfo["dob"] as? String ?? ""
        self.anniversaryDate = self.generalInfo["anniversaryDate"] as? String ?? ""
        self.maritalStatus = self.generalInfo["maritalStatus"] as? String ?? ""
        
        self.profileGSTInfo = profiledict["profileGSTInfo"] as? Dictionary<String, Any> ?? Dictionary.init()
    }
}

/*
 Profile Api Calling Process  Every Time First Call
 Call that new Profile API and show data Acc to response Success { Show Data as per Profile
 }Failure { Then follow same Api calling process Of Login with pass
 Success {
 Call that new Profile API and show data Acc to response Success { Show Data as per Profile
 } Failure{
 title: "Failure", message: "Please Login Again to see your Profile Info"
 }
 } Failure { title: "Failure", message: "Please Login with Password to see your Profile Info"
 }
 }
 */

