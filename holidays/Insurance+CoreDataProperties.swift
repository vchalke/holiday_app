//
//  Insurance+CoreDataProperties.swift
//  holidays
//
//  Created by Saurav on 28/11/17.
//  Copyright © 2017 Mobicule. All rights reserved.
//

import Foundation
import CoreData


extension Insurance {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Insurance> {
        return NSFetchRequest<Insurance>(entityName: "Insurance")
    }

    @NSManaged public var bfNumber: String?
    @NSManaged public var passengerNumber: String?
    @NSManaged public var policyNumber: String?
    @NSManaged public var policyURL: String?
    @NSManaged public var tourRelation: Tour?

}
