//
//  LiveRatesTableViewCell.swift
//  holidays
//
//  Created by Swapnil on 21/12/17.
//  Copyright © 2017 Mobicule. All rights reserved.
//

import UIKit

class LiveRatesTableViewCell: UITableViewCell {

    @IBOutlet weak var labelCurrName: UILabel!
    @IBOutlet weak var labelBuyRate: UILabel!
    @IBOutlet weak var labelSellRate: UILabel!
    @IBOutlet weak var labelCurrCode: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
