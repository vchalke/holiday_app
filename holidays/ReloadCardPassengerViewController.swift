//
//  ReloadCardPassengerViewController.swift
//  holidays
//
//  Created by Parshwanath on 07/01/18.
//  Copyright © 2018 Mobicule. All rights reserved.
//

import UIKit

class ReloadCardPassengerViewController: ForexBaseViewController,UITextFieldDelegate
{
    @IBOutlet weak var totalPayableAmount: UILabel!
    
    @IBOutlet weak var textfieldAadharNo: UITextField!
    @IBOutlet weak var textfieldGSTINNo: UITextField!
    @IBOutlet weak var containerScrollView: TPKeyboardAvoidingScrollView!
    @IBOutlet var passangerDetailContainerView: UIView!
    @IBOutlet weak var textFieldLastName: UITextField!
    @IBOutlet weak var textFieldFirstName: UITextField!
    @IBOutlet weak var textFieldTitle: UITextField!
    @IBOutlet weak var textFldDOBYear: UITextField!
    @IBOutlet weak var textFldDOBMonth: UITextField!
    @IBOutlet weak var textFldDOBDate: UITextField!
    @IBOutlet weak var textFldDOTYear: UITextField!
    @IBOutlet weak var textFldDOTMonth: UITextField!
    @IBOutlet weak var textFldDOTDate: UITextField!
    @IBOutlet weak var buttonContainerView: UIView!
    @IBOutlet weak var tickMarkButton: UIButton!
    @IBOutlet weak var textFieldMobileNo: UITextField!
    @IBOutlet weak var buttonFourthTraveller: UIButton!
    @IBOutlet weak var textFieldEmail: UITextField!
    @IBOutlet weak var buttonThirdTraveller: UIButton!
    @IBOutlet weak var buttonSecondTraveller: UIButton!
    @IBOutlet weak var buttonFirstTraveller: UIButton!
    @IBOutlet var buyForexPassangerDetailView: UIView!
    var buyForexBo : BuyForexBO = BuyForexBO.init()
    var currentSelectedIndex = 1;
    @IBOutlet weak var activeTextField: UITextField?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        Bundle.main.loadNibNamed("ReloadCardPassengerViewController", owner: self, options: nil)
        
        super.addViewInBaseView(childView: self.passangerDetailContainerView)
        containerScrollView.addSubview(self.buyForexPassangerDetailView)
        
        super.setUpHeaderLabel(labelHeaderNameText: "Reload Forex")
        
        for buttonIndex in 1..<(buyForexBo.buyForexPassengerInfo?.travellerArray.count)!+1
        {
            let button : UIButton = buttonContainerView.viewWithTag(buttonIndex) as! UIButton;
            button.layer.borderColor = UIColor.gray.cgColor
            button.isHidden = false
            if buttonIndex == 1
            {
                button.backgroundColor = AppUtility.hexStringToUIColor(hex: "#0095DA")
            }
            else
            {
                button.backgroundColor = UIColor.white;
            }
        }
        
        textFieldEmail.text = buyForexBo.emailID
        textFieldMobileNo.text =  buyForexBo.buyForexOptionViewDetails!.contactDetail
        textfieldAadharNo.delegate = self
        tickMarkButton.setImage(UIImage.init(named: "ForexCheckBoxOn"), for: UIControl.State.selected)
        tickMarkButton.setImage(UIImage.init(named: "ForexCheckBoxOff"), for: UIControl.State.normal)
        tickMarkButton.isSelected = true
        
        totalPayableAmount.text = "\(buyForexBo.buyForexPayment!.totalPayableAmount) INR"
        let travller : TravellerBO = (buyForexBo.buyForexPassengerInfo!.travellerArray[0])
        
        
        self.textFieldFirstName.text = travller.travellerFirstName;
        self.textFieldLastName.text = travller.travellerLastName;
        self.textFieldTitle.text = travller.travellerTitle
//        let dot : NSDate = travller.travellerDateOfTravel!;
        
//        let calendar = Calendar.current
//        let year = calendar.component(.year, from: dot as Date)
//        let month = calendar.component(.month, from: dot as Date)
//        let day = calendar.component(.day, from: dot as Date)
//        self.textFldDOTDate.text = "\(day)"
//        self.textFldDOTMonth.text = "\(month)"
//        self.textFldDOTYear.text = "\(year)"
        if travller.travellerDateOfBirth != nil
        {
            let dob : NSDate = travller.travellerDateOfBirth!
            
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM/yyyy"
            
            let calendar = Calendar.current
            let year = calendar.component(.year, from: dob as Date)
            let month = calendar.component(.month, from: dob as Date)
            let day = calendar.component(.day, from: dob as Date)
            self.textFldDOBDate.text = "\(day)"
            self.textFldDOBMonth.text = "\(month)"
            self.textFldDOBYear.text = "\(year)"
        }
        
        let travelDate : NSDate = (buyForexBo.buyForexOptionViewDetails?.travelDate)!
        let calendar = Calendar.current
        let year = calendar.component(.year, from: travelDate as Date)
        let month = calendar.component(.month, from: travelDate as Date)
        let day = calendar.component(.day, from: travelDate as Date)
        self.textFldDOTDate.text = "\(day)"
        self.textFldDOTMonth.text = "\(month)"
        self.textFldDOTYear.text = "\(year)"
        
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ForexBuyPassangerViewController.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
//        KeyboardAvoiding.setAvoidingView(self.passangerDetailContainerView, withTriggerView: self.textFieldFirstName)
//        KeyboardAvoiding.setAvoidingView(self.passangerDetailContainerView, withTriggerView: self.textFieldLastName)
//        KeyboardAvoiding.setAvoidingView(self.passangerDetailContainerView, withTriggerView: self.textFieldTitle)
//        KeyboardAvoiding.setAvoidingView(self.passangerDetailContainerView, withTriggerView: self.textFieldEmail)
//        KeyboardAvoiding.setAvoidingView(self.passangerDetailContainerView, withTriggerView: self.textFieldMobileNo)
        
        textFieldMobileNo.delegate = self
        textFieldEmail.delegate = self
        
        self.addDoneButtonOnKeyboard()
    }
    
//    override func viewWillAppear(_ animated: Bool)
//    {
//        self.setNotificationKeyboard()
//    }
    override func backButtonClicked(buttonView:UIButton)
    {
        let loginStatus = UserDefaults.standard.string(forKey: kLoginStatus)
        print("loginStatus---> \(String(describing: loginStatus))")
        
        if loginStatus == kLoginSuccess
        {
            let viewControllers: [UIViewController] = self.navigationController!.viewControllers
            for aViewController in viewControllers
            {
                if aViewController is ReloadCardPaymentViewController
                {
                    self.navigationController!.popToViewController(aViewController, animated: true)
                }
            }
        }
    }
    
    override func viewWillLayoutSubviews()
    {
        super.setSubViewFrame(childView: self.passangerDetailContainerView)
        
        self.buyForexPassangerDetailView.frame = CGRect.init(x: 0, y: 0, width: containerScrollView.frame.size.width, height: 800)
        
        self.containerScrollView.contentSize = CGSize.init(width: 0, height: 800);
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    func dismissKeyboard()
    {
        view.endEditing(true)
    }
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle       = UIBarStyle.default
        let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(ReloadCardPassengerViewController.doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.textFieldFirstName.inputAccessoryView = doneToolbar
        self.textFieldLastName.inputAccessoryView = doneToolbar
        self.textfieldAadharNo.inputAccessoryView = doneToolbar
        self.textfieldGSTINNo.inputAccessoryView = doneToolbar
        self.textFieldEmail.inputAccessoryView = doneToolbar
        self.textFieldMobileNo.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction()
    {
        self.textFieldFirstName.resignFirstResponder()
        self.textFieldLastName.resignFirstResponder()
        self.textfieldAadharNo.resignFirstResponder()
        self.textfieldGSTINNo.resignFirstResponder()
        self.textFieldEmail.resignFirstResponder()
        self.textFieldMobileNo.resignFirstResponder()
    }
    
    @IBAction func onClickOfTravellerNumber(_ sender: Any)
    {
        let button : UIButton = sender as! UIButton
        let tag =  button.tag
        currentSelectedIndex = tag
        let travller : TravellerBO = (buyForexBo.buyForexPassengerInfo!.travellerArray[tag-1])
        
        
        self.textFieldFirstName.text = travller.travellerFirstName
        self.textFieldLastName.text = travller.travellerLastName
        self.textFieldTitle.text = travller.travellerTitle
        self.textfieldAadharNo.text = travller.travellerAadharNumber
        
        let dot : NSDate = travller.travellerDateOfTravel!
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        
        //        let dotString : String  = (buyForexBo.buyForexOptionViewDetails?.travelDate)!
        //
        //        let dot : NSDate = formatter.date(from: dotString)! as NSDate
        
        let calendar = Calendar.current
        let year = calendar.component(.year, from: dot as Date)
        let month = calendar.component(.month, from: dot as Date)
        let day = calendar.component(.day, from: dot as Date)
        self.textFldDOTDate.text = "\(day)"
        self.textFldDOTMonth.text = "\(month)"
        self.textFldDOTYear.text = "\(year)"
        
        if travller.travellerDateOfBirth != nil
        {
            let dob : NSDate = travller.travellerDateOfBirth!
            
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM/yyyy"
            
            //        let dotString : String  = (buyForexBo.buyForexOptionViewDetails?.travelDate)!
            //
            //        let dot : NSDate = formatter.date(from: dotString)! as NSDate
            
            let calendar = Calendar.current
            let year = calendar.component(.year, from: dob as Date)
            let month = calendar.component(.month, from: dob as Date)
            let day = calendar.component(.day, from: dob as Date)
            self.textFldDOBDate.text = "\(day)"
            self.textFldDOBMonth.text = "\(month)"
            self.textFldDOBYear.text = "\(year)"
            
        }
        else
        {
            self.textFldDOBDate.text = ""
            self.textFldDOBMonth.text = ""
            self.textFldDOBYear.text = ""
            
        }
        for buttonIndex in 1..<5
        {
            let button : UIButton = buttonContainerView.viewWithTag(buttonIndex) as! UIButton;
            button.layer.borderColor = UIColor.gray.cgColor
            
            if buttonIndex != tag
            {
                button.backgroundColor = UIColor.white;
            }
            else
            {
                button.backgroundColor = AppUtility.hexStringToUIColor(hex: "#0095DA")
            }
        }
        
    }
    @IBAction func onDateOfTravelClicked(_ sender: Any)
    {
        let currentDate = Date()
        var dateComponents = DateComponents()
        dateComponents.month = -3
        _ = Calendar.current.date(byAdding: dateComponents, to: currentDate)
        
        let datePicker = DatePickerDialog(textColor: .black,
                                          buttonColor: .black,
                                          font: UIFont.boldSystemFont(ofSize: 17),
                                          showCancelButton: true)
        datePicker.show("Date of Travel",
                        doneButtonTitle: "Done",
                        cancelButtonTitle: "Cancel",
                        //minimumDate: threeMonthAgo,
            //maximumDate: currentDate,
        datePickerMode: .date) { (date) in
            if let dt = date
            {
                let currentDate : Date = Date.init()
                let calendar = NSCalendar.current
                let date1 = calendar.startOfDay(for: dt)
                let date2 = calendar.startOfDay(for:currentDate)
                let components =  calendar.dateComponents([.day], from: date1, to: date2)
                if components.day! > -2 || components.day! < (-61)
                {
                    self.showAlert(message: "Dear Customer, since your travel date is immediate, we cannot proceed ahead with this transaction. Our forex expert will contact you for further assistance on the contact details shared with us. You can also call us on toll free no 1800 2099 100.")
                    self.textFldDOTDate.text = "DD"
                    self.textFldDOTMonth.text = "MM"
                    self.textFldDOTYear.text = "YYYY"
                }
                else
                {
                    let formatter = DateFormatter()
                    formatter.dateFormat = "MM/dd/yyyy"
                    let calendar = Calendar.current
                    let year = calendar.component(.year, from: dt)
                    let month = calendar.component(.month, from: dt)
                    let day = calendar.component(.day, from: dt)
                    self.textFldDOTDate.text = "\(day)"
                    self.textFldDOTMonth.text = "\(month)"
                    self.textFldDOTYear.text = "\(year)"
                    self.buyForexBo.buyForexPassengerInfo?.travellerArray[self.currentSelectedIndex-1].travellerDateOfTravel = dt as NSDate;
                }
            }
        }
        
    }
    
    @IBAction func onDateOfBirthClicked(_ sender: Any)
    {
        let currentDate = Date()
        var dateComponents = DateComponents()
        dateComponents.month = -3
        _ = Calendar.current.date(byAdding: dateComponents, to: currentDate)
        
        let datePicker = DatePickerDialog(textColor: .black,
                                          buttonColor: .black,
                                          font: UIFont.boldSystemFont(ofSize: 17),
                                          showCancelButton: true)
        datePicker.show("Birth Date",
                        doneButtonTitle: "Done",
                        cancelButtonTitle: "Cancel",
                        //minimumDate: threeMonthAgo,
            //maximumDate: currentDate,
        datePickerMode: .date) { (date) in
            if let dt = date
            {
                let gregorian = Calendar(identifier: .gregorian)
                let ageComponents = gregorian.dateComponents([.year], from: dt, to: Date())
                let age = ageComponents.year!
                if age < 18
                {
                    self.showAlert(message: "Date of birth can not be less than 18 years")
                    self.textFldDOBDate.text = "DD"
                    self.textFldDOBMonth.text = "MM"
                    self.textFldDOBYear.text = "YYYY"
                }
                else
                {
                    self.buyForexBo.buyForexPassengerInfo?.travellerArray[self.currentSelectedIndex-1].travellerDateOfBirth = dt as NSDate;
                    
                    let formatter = DateFormatter()
                    formatter.dateFormat = "MM/dd/yyyy"
                    let calendar = Calendar.current
                    let year = calendar.component(.year, from: dt)
                    let month = calendar.component(.month, from: dt)
                    let day = calendar.component(.day, from: dt)
                    self.textFldDOBDate.text = "\(day)"
                    self.textFldDOBMonth.text = "\(month)"
                    self.textFldDOBYear.text = "\(year)"
                }
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        let totalString = "\(textField.text!)\(string)"
        
        if textField == textFieldMobileNo
        {
            
            let textFieldText: NSString = (textField.text ?? "") as NSString
            
            let txtAfterUpdate = textFieldText.replacingCharacters(in: range, with: string)
            
            
            if txtAfterUpdate.count > 10 {
                
                return false
                
            }
            
        }
        if textField == textFieldFirstName
        {
            buyForexBo.buyForexPassengerInfo?.travellerArray[currentSelectedIndex-1].travellerFirstName = totalString;
        }
        else if textField == textFieldLastName
        {
            buyForexBo.buyForexPassengerInfo?.travellerArray[currentSelectedIndex-1].travellerLastName = totalString;
            
        }
        if textField == textfieldAadharNo
        {
            
            let textFieldText: NSString = (textField.text ?? "") as NSString
            
            let txtAfterUpdate = textFieldText.replacingCharacters(in: range, with: string)
            
            
            if txtAfterUpdate.count > 12 {
                
                return false
                
            }
            
        }
        
        if textField == textfieldGSTINNo
        {
            
            let textFieldText: NSString = (textField.text ?? "") as NSString
            
            let txtAfterUpdate = textFieldText.replacingCharacters(in: range, with: string)
            
            
            if txtAfterUpdate.count > 15 {
                
                return false
                
            }
            
        }
        
        if textField == textfieldAadharNo
        {
            buyForexBo.buyForexPassengerInfo?.travellerArray[currentSelectedIndex-1].travellerAadharNumber = totalString;
            
        }
        
        
        return true;
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        if textField == textFieldTitle
        {
            let arrayForTravelPurpose  =  ["Mr.","Mrs.","Ms.","Dr."]
            
            let alertController : UIAlertController  = UIAlertController.init(title: "Title", message: "Select Title", preferredStyle:.actionSheet)
            
            for travellPurpose in arrayForTravelPurpose
            {
                let  alertAction: UIAlertAction = UIAlertAction.init(title: travellPurpose, style: .default, handler:
                {
                    
                    (alert: UIAlertAction!) -> Void in
                    self.textFieldTitle.text = travellPurpose
                    self.buyForexBo.buyForexPassengerInfo?.travellerArray[self.currentSelectedIndex-1].travellerTitle = travellPurpose;
                    
                })
                
                alertController.addAction(alertAction)
            }
            self.present(alertController, animated:true, completion: nil)
            
            return false
            
        }
        
        
        return true
    }
    
    func validateField()-> Bool
    {
        for i in 0..<(buyForexBo.buyForexPassengerInfo?.travellerArray.count)!
        {
            let traveller : TravellerBO = (buyForexBo.buyForexPassengerInfo?.travellerArray[i])!
            if ((traveller.travellerTitle.trimmingCharacters(in: CharacterSet(charactersIn: " "))).isEmpty)
            {
                showAlert(message: "Please select Title for Traveller \(i+1)" as NSString)
                return false
            }
            if ((traveller.travellerFirstName.trimmingCharacters(in: CharacterSet(charactersIn: " "))).isEmpty)
            {
                showAlert(message: "Please enter First Name of Traveller \(i+1)" as NSString)
                return false
            }
            if ((traveller.travellerLastName.trimmingCharacters(in: CharacterSet(charactersIn: " "))).isEmpty)
            {
                showAlert(message: "Please enter Last Name of Traveller \(i+1)" as NSString)
                return false
            }
            if(traveller.travellerDateOfBirth == nil)
            {
                showAlert(message: "Please select Date of Birth of Traveller \(i+1)" as NSString)
                return false
            }
            if (textFldDOTDate.text == "DD") || (textFldDOTMonth.text == "MM") || (textFldDOTYear.text == "YYYY")
            {
                showAlert(message: "Please select date of travel.")
                return false
            }
            if (!(traveller.travellerAadharNumber.isEmpty))
            {
                let str : String = (textfieldAadharNo.text?.trimmingCharacters(in: CharacterSet(charactersIn: " ")))!
                if (str.count < 12 )
                {
                    showAlert(message: "Please enter 12 digit Aadhaar Card No.of traveller \(i+1)" as NSString)
                    return false
                }
            }
            
        }
        
        if (!(textfieldGSTINNo.text?.isEmpty)!)
        {
            let str : String = (textfieldGSTINNo.text?.trimmingCharacters(in: CharacterSet(charactersIn: " ")))!
            if (str.count < 14 )
            {
                showAlert(message: "Please enter 14 digit GSTIN No.")
                return false
            }
        }
        if (textFldDOTDate.text == "DD") || (textFldDOTMonth.text == "MM") || (textFldDOTYear.text == "YYYY")
        {
            showAlert(message: "Please select date of travel.")
            return false
        }
        
        if !isValidEmail(testStr: textFieldEmail.text!)
        {
            showAlert(message: "Please enter valid EmailID.")
            return false
        }
        if !isValidMobileNo(testStr: textFieldMobileNo.text!)
        {
            showAlert(message: "Please enter valid mobile number.")
            return false
        }
        return true
    }
    
    @IBAction func onContinueButtonClicked(_ sender: Any)
    {
        if validateField()
        {
            let buyForxVC : ReloadCardLocationViewController = ReloadCardLocationViewController.init(nibName: "ForexBaseViewController", bundle: nil)
            let mobileNO : NSString = textFieldMobileNo.text! as NSString
            let buyForexPassengerDetail : BuyForexPassengerDetailBO = BuyForexPassengerDetailBO.init(mobileNo: mobileNO , areAllTravellersAreIndian: tickMarkButton.isSelected, emailID: textFieldEmail.text!)
            buyForexBo.addBuyForexPassengerDetails(buyForexPassengerDetails: buyForexPassengerDetail)
            buyForxVC.buyForexBo = buyForexBo
            self.navigationController?.pushViewController(buyForxVC, animated: true)
        }
    }
    
    @IBAction func onClickOfTickMarkRadioButton(_ sender: Any)
    {
        tickMarkButton.isSelected =  !tickMarkButton.isSelected
    }
    
    // MARK: - Keyboard Handling Methods
    @objc func keyboardWasShown(notification: NSNotification)
    {
        var info = notification.userInfo!
        let keyboardSize = (info[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsets.init(top: 0.0, left: 0.0, bottom: keyboardSize!.height+10, right: 0.0)
        self.containerScrollView.contentInset = contentInsets
        self.containerScrollView.scrollIndicatorInsets = contentInsets
        var aRect : CGRect = self.buyForexPassangerDetailView.frame
        aRect.size.height -= keyboardSize!.height
        if let activeField = self.activeTextField
        {
            if (!aRect.contains(activeField.frame.origin))
            {
                self.containerScrollView.scrollRectToVisible(activeField.frame, animated: true)
            }
        }
    }
    
    @objc func keyboardWillBeHidden(notification: NSNotification)
    {
        let contentInsets : UIEdgeInsets = UIEdgeInsets.init(top: 0.0, left: 0.0,bottom: 0.0, right: 0.0)
        self.containerScrollView.contentInset = contentInsets
        self.containerScrollView.scrollIndicatorInsets = contentInsets
        self.view.endEditing(true)
    }
    
    func setNotificationKeyboard ()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
}
