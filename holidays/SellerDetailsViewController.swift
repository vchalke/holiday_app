//
//  SellerDetailsViewController.swift
//  holidays
//
//  Created by Komal Katkade on 11/29/17.
//  Copyright © 2017 Mobicule. All rights reserved.
//

import UIKit

class SellerDetailsViewController: ForexBaseViewController ,UITextFieldDelegate{
    @IBOutlet weak var textfieldTitle: UITextField!

    @IBOutlet weak var labelTotalEncashmentAmount: UILabel!
    @IBOutlet weak var textfieldLastName: UITextField!
    @IBOutlet weak var textfieldFirstName: UITextField!
    @IBOutlet weak var textfieldMonth: UILabel!
    @IBOutlet var sellerDetailFormView: UIView!
    @IBOutlet weak var textfieldAadharNo: UITextField!
    @IBOutlet weak var textfieldYear: UILabel!
    @IBOutlet weak var buttonTickMark: UIButton!
    @IBOutlet weak var textfieldGSTINNo: UITextField!

    @IBOutlet weak var textfieldDay: UILabel!
    @IBOutlet weak var sellerDetailScrollview:TPKeyboardAvoidingScrollView! // UIScrollView!
    @IBOutlet var sellerDetailView: UIView!
    @IBOutlet weak var buttonContinue: UIButton!
    @IBOutlet weak var textfieldMobileNo: UITextField!
    @IBOutlet weak var textfieldEmailID: UITextField!
    
    @IBOutlet weak var activeTextField: UITextField?
    
     var sellForexBO : SellForexBO =  SellForexBO.init()
    var productArray : [ProductBO] = []

    override func viewDidLoad()
    {
        super.viewDidLoad()

        Bundle.main.loadNibNamed("SellerDetailsViewController", owner: self, options: nil)
        
        super.addViewInBaseView(childView: self.sellerDetailView)
        super.viewDidLoad()
        self.sellerDetailScrollview.addSubview(self.sellerDetailFormView)
        self.sellerDetailScrollview.contentSize = CGSize.init(width: 0, height: 700)
        self.sellerDetailFormView.frame = CGRect.init(x: 0, y: 0, width: Int(self.sellerDetailScrollview.frame.size.width), height:  Int(self.sellerDetailScrollview.contentSize.height))
        
        buttonTickMark.setImage(UIImage.init(named: "ForexCheckBoxOn"), for: UIControl.State.selected)
        buttonTickMark.setImage(UIImage.init(named: "ForexCheckBoxOff"), for: UIControl.State.normal)
        buttonTickMark.isSelected = false
        
        labelTotalEncashmentAmount.text = sellForexBO.totalAmountWithTax
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ForexBuyPassangerViewController.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        
        productArray = sellForexBO.productArray
        textfieldAadharNo.delegate = self
        textfieldEmailID.delegate = self
        textfieldGSTINNo.delegate = self
        textfieldMobileNo.delegate = self
        textfieldTitle.delegate = self
        textfieldLastName.delegate = self
        textfieldFirstName.delegate = self
        
        textfieldEmailID.text = sellForexBO.emailID
        textfieldMobileNo.text = sellForexBO.mobileNumber
        
       /* KeyboardAvoiding.setAvoidingView(self.sellerDetailView, withTriggerView: self.textfieldMobileNo)
        KeyboardAvoiding.setAvoidingView(self.sellerDetailView, withTriggerView: self.textfieldEmailID)
        KeyboardAvoiding.setAvoidingView(self.sellerDetailView, withTriggerView: self.textfieldGSTINNo)
        KeyboardAvoiding.setAvoidingView(self.sellerDetailView, withTriggerView: self.textfieldAadharNo)
        KeyboardAvoiding.setAvoidingView(self.sellerDetailView, withTriggerView: self.textfieldTitle)
        KeyboardAvoiding.setAvoidingView(self.sellerDetailView, withTriggerView: self.textfieldLastName)
        KeyboardAvoiding.setAvoidingView(self.sellerDetailView, withTriggerView: self.textfieldFirstName)*/
        
        self.addDoneButtonOnKeyboard()
        
        self.textfieldFirstName.text = sellForexBO.firstName
        self.textfieldLastName.text = sellForexBO.lastName
        self.textfieldTitle.text = sellForexBO.title
        self.textfieldAadharNo.text = sellForexBO.aadhaarNo
        self.textfieldGSTINNo.text = sellForexBO.GSTINNo
        
        let dob : NSDate = sellForexBO.dateOfBirth
        
            
            let formatter = DateFormatter()
            formatter.dateFormat = "MM/dd/yyyy"
            let calendar = Calendar.current
            let year = calendar.component(.year, from: dob as Date)
            let month = calendar.component(.month, from: dob as Date)
            let day = calendar.component(.day, from: dob as Date)
            self.textfieldDay.text = "\(day)"
            self.textfieldMonth.text = "\(month)"
            self.textfieldYear.text = "\(year)"
        
    }
    
    override func backButtonClicked(buttonView:UIButton)
    {
        let loginStatus = UserDefaults.standard.string(forKey: kLoginStatus)
        print("loginStatus---> \(String(describing: loginStatus))")
        
        if loginStatus == kLoginSuccess
        {
            let viewControllers: [UIViewController] = self.navigationController!.viewControllers
            for aViewController in viewControllers
            {
                if aViewController is SellForexConfirmOrderViewController
                {
                    self.navigationController!.popToViewController(aViewController, animated: true)
                }
            }
        }
    }
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle       = UIBarStyle.default
        let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(SellerDetailsViewController.doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.textfieldFirstName.inputAccessoryView = doneToolbar
        self.textfieldLastName.inputAccessoryView = doneToolbar
        self.textfieldAadharNo.inputAccessoryView = doneToolbar
        self.textfieldGSTINNo.inputAccessoryView = doneToolbar
        self.textfieldEmailID.inputAccessoryView = doneToolbar
        self.textfieldMobileNo.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction()
    {
        self.textfieldFirstName.resignFirstResponder()
        self.textfieldLastName.resignFirstResponder()
        self.textfieldAadharNo.resignFirstResponder()
        self.textfieldGSTINNo.resignFirstResponder()
        self.textfieldEmailID.resignFirstResponder()
        self.textfieldMobileNo.resignFirstResponder()
    }
    
    override func viewDidLayoutSubviews()
    {
        super.setSubViewFrame(childView: self.sellerDetailView)
        self.sellerDetailFormView.frame = CGRect.init(x: 0, y: 0, width: Int(self.sellerDetailScrollview.frame.size.width), height:  Int(self.sellerDetailScrollview.contentSize.height))
    }
    
    override func viewWillLayoutSubviews()
    {
        super.setSubViewFrame(childView: self.sellerDetailView)
        self.sellerDetailFormView.frame = CGRect.init(x: 0, y: 0, width: Int(self.sellerDetailScrollview.frame.size.width), height:  Int(self.sellerDetailScrollview.contentSize.height))
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.buttonContinue.layer.cornerRadius = 10
        self.setNotificationKeyboard()

    }
    func dismissKeyboard()
    {
        view.endEditing(true)
    }
    func validateFields() -> Bool {
        if (textfieldTitle.text?.isEmpty)!
        {
            showAlert(message: "Please select Title")
            return false
        }
        if (textfieldFirstName.text?.isEmpty)!
        {
            showAlert(message: "Please enter First Name")
            return false
        }
        if (textfieldLastName.text?.isEmpty)!
        {
            showAlert(message: "Please enter Last Name")
            return false
        }
//        if (textfieldDay.text?.isEmpty)!
//        {
//            showAlert(message: "Please select date of birth.")
//            return false
//        }
        if (textfieldDay.text == "DD") || (textfieldMonth.text == "MM") || (textfieldYear.text == "YYYY")
        {
            showAlert(message: "Please select Date of Birth.")
            return false
        }
        if !isValidDateOfBirth(date: sellForexBO.dateOfBirth as Date)
        {
            showAlert(message: "Date of birth can not be less than 18 years")
            return false
        }
        if (!(textfieldAadharNo.text?.isEmpty)!)
        {
            let str : String = (textfieldAadharNo.text?.trimmingCharacters(in: CharacterSet(charactersIn: " ")))!
            if (str.count < 12 )
            {
                showAlert(message: "Please enter 12 digit Aadhaar Card No.")
                return false
            }
        }
        if (!(textfieldGSTINNo.text?.isEmpty)!)
        {
            let str : String = (textfieldGSTINNo.text?.trimmingCharacters(in: CharacterSet(charactersIn: " ")))!
            if (str.count < 14 )
            {
                showAlert(message: "Please enter 14 digit GSTIN No.")
                return false
            }
        }
        if (
            textfieldEmailID.text?.isEmpty)!
        {
            showAlert(message: "Enter E-mail ID")
            return false
        }
        
        if !isValidEmail(testStr: textfieldEmailID.text!)
        {
            showAlert(message: "Enter valid E-mail ID")
            return false
        }

        if ((textfieldMobileNo.text?.isEmpty)! || ((textfieldMobileNo.text?.trimmingCharacters(in: CharacterSet(charactersIn: " ")))?.isEmpty)!)
        {
            showAlert(message: "Enter Mobile number")
            return false
        }
        
        if !isValidMobileNo(testStr: textfieldMobileNo.text!)
        {
            showAlert(message: "Enter valid Mobile number")
            return false
        }
        
     

        return true
    }
    
    //MARK: textfield delegates
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        activeTextField=textField;
    }
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        activeTextField=nil;
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textfieldAadharNo
        {
            
            let textFieldText: NSString = (textField.text ?? "") as NSString
            
            let txtAfterUpdate = textFieldText.replacingCharacters(in: range, with: string)
            
            
            if txtAfterUpdate.count > 12 {
                
                return false
                
            }
            
        }

        if textField == textfieldGSTINNo
        {
            
            let textFieldText: NSString = (textField.text ?? "") as NSString
            
            let txtAfterUpdate = textFieldText.replacingCharacters(in: range, with: string)
            
            
            if txtAfterUpdate.count > 15 {
                
                return false
                
            }
            
        }
        if textField == textfieldMobileNo
        {
            
            let textFieldText: NSString = (textField.text ?? "") as NSString
            
            let txtAfterUpdate = textFieldText.replacingCharacters(in: range, with: string)
            
            
            if txtAfterUpdate.count > 10 {
                
                return false
                
            }
            
        }

        return true
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        if textField == textfieldTitle
        {
            let arrayForTravelPurpose  =  ["Mr.","Mrs.","Ms.","Dr."]
            
            let alertController : UIAlertController  = UIAlertController.init(title: "Title", message: "Select Title", preferredStyle:.actionSheet)
            
            for travellPurpose in arrayForTravelPurpose
            {
                let  alertAction: UIAlertAction = UIAlertAction.init(title: travellPurpose, style: .default, handler:
                {
                    
                    (alert: UIAlertAction!) -> Void in
                    self.textfieldTitle.text = travellPurpose
                    
                })
                
                alertController.addAction(alertAction)
            }
            self.present(alertController, animated:true, completion: nil)
            
            return false
            
        }
        
        
        return true
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
     //MARK: button Action
    @IBAction func buttonTickMarkClicked(_ sender: Any)
    {
        buttonTickMark.isSelected =   !buttonTickMark.isSelected
    }
    
    @IBAction func buttonContinueClicked(_ sender: Any)
    {
        if validateFields()
        {
            let encashmentAmountViewController : EncashmentAmountViewController = EncashmentAmountViewController(nibName: "ForexBaseViewController", bundle: nil)
            sellForexBO.title = textfieldTitle.text!
            sellForexBO.lastName = textfieldLastName.text!
            sellForexBO.firstName = textfieldFirstName.text!
            if !(textfieldAadharNo.text?.isEmpty)!
            {
               sellForexBO.aadhaarNo = textfieldAadharNo.text!
            }
            if !(textfieldGSTINNo.text?.isEmpty)!
            {
                sellForexBO.GSTINNo = textfieldGSTINNo.text!
            }
            sellForexBO.emailID = textfieldEmailID.text!
         //   sellForexBO.date =
            sellForexBO.mobileNumber = textfieldMobileNo.text!
            encashmentAmountViewController.sellForexBO = sellForexBO
            self.navigationController?.pushViewController(encashmentAmountViewController, animated: true)

        }
    }
    @IBAction func onDateOfBirthClicked(_ sender: Any)
    {
        let currentDate = Date()
        var dateComponents = DateComponents()
        dateComponents.month = -0
        dateComponents.year = -118
        
        let gregorian: NSCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
        let minDate: NSDate = gregorian.date(byAdding: dateComponents as DateComponents, to: currentDate as Date, options: NSCalendar.Options(rawValue: 0))! as NSDate
        
        _ = Calendar.current.date(byAdding: dateComponents, to: currentDate)
        
        let datePicker = DatePickerDialog(textColor: .black,
                                          buttonColor: .black,
                                          font: UIFont.boldSystemFont(ofSize: 17),
                                          showCancelButton: true)
        datePicker.show("Select Date of Birth",
                        doneButtonTitle: "Done",
                        cancelButtonTitle: "Cancel",
                        minimumDate: minDate as Date,
            //maximumDate: currentDate,
        datePickerMode: .date) { (date) in
            if let dt = date
                {
                if !self.isValidDateOfBirth(date: dt)
                {
                    self.showAlert(message: "Date of birth can not be less than 18 years")
                    self.textfieldDay.text = "DD"
                    self.textfieldMonth.text = "MM"
                    self.textfieldYear.text = "YYYY"
                }
                else
                {

                    self.sellForexBO.dateOfBirth =  dt as NSDate;
                
                let formatter = DateFormatter()
                formatter.dateFormat = "MM/dd/yyyy"
                let calendar = Calendar.current
                let year = calendar.component(.year, from: dt)
                let month = calendar.component(.month, from: dt)
                let day = calendar.component(.day, from: dt)
                self.textfieldDay.text = "\(day)"
                self.textfieldMonth.text = "\(month)"
                self.textfieldYear.text = "\(year)"
                }
            }
        }
        
    }
    
    //MARK: Keyboard Handling Methods
    @objc func keyboardWasShown(notification: NSNotification)
    {
        var info = notification.userInfo!
        let keyboardSize = (info[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsets.init(top: 0.0, left: 0.0, bottom: 200, right: 0.0)
        self.sellerDetailScrollview.contentInset = contentInsets
        self.sellerDetailScrollview.scrollIndicatorInsets = contentInsets
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        if let activeField = self.activeTextField
        {
            if (!aRect.contains(activeField.frame.origin))
            {
                self.sellerDetailScrollview.scrollRectToVisible(activeField.frame, animated: true)
            }
        }
    }
    // when keyboard hide reduce height of scroll view
    @objc func keyboardWillBeHidden(notification: NSNotification)
    {
        let contentInsets : UIEdgeInsets = UIEdgeInsets.init(top: 0.0, left: 0.0,bottom: 0.0, right: 0.0)
        self.sellerDetailScrollview.contentInset = contentInsets
        self.sellerDetailScrollview.scrollIndicatorInsets = contentInsets
        self.view.endEditing(true)
    }
    
    // Notification when keyboard show
    func setNotificationKeyboard ()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
}
