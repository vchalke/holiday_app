//
//  FlightsColectObject.h
//  holidays
//
//  Created by Kush_Team on 13/07/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FlightsColectObject : NSObject
-(instancetype)initWithFlightObjectDict:(NSDictionary *)dictionary;
@property(nonatomic,strong)NSString *airlineImage;
@property(nonatomic,strong)NSString *airlineName;
@property(nonatomic,strong)NSString *arrivalCity;
@property(nonatomic,strong)NSString *arrivalTime;
@property(nonatomic,strong)NSString *departureCity;
@property(nonatomic,strong)NSString *departureTime;
@property(nonatomic,strong)NSString *destinationCity;
@property(nonatomic,strong)NSString *flightDuration;
@property(nonatomic,strong)NSString *flightMsg;
@property(nonatomic,strong)NSString *flightNo;
@property(nonatomic)NSInteger holidayFlightsId;
@property(nonatomic,strong)NSString *hubCity;
@property(nonatomic,strong)NSString *hubCityCode;
@property(nonatomic,strong)NSString *isActive;
@property(nonatomic,strong)NSString *isInternalFlight;
@property(nonatomic)NSInteger journeyTypeId;
@property(nonatomic,strong)NSString *layoverTime;
@property(nonatomic)NSInteger position;
@property(nonatomic)NSInteger totalSegId;


@end

NS_ASSUME_NONNULL_END

