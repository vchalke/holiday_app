//
//  FilterLabelCollectionViewCell.h
//  holidays
//
//  Created by Kush_Team on 24/06/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FilterLabelCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIView *baseViews;
@property (weak, nonatomic) IBOutlet UIButton *btn_view;
-(void)setButtonColour:(BOOL)flag;
@end

NS_ASSUME_NONNULL_END
