//
//  CollectionReusableView.h
//  holidays
//
//  Created by Kush_Tech on 24/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CollectionReusableView : UICollectionReusableView
@property (weak, nonatomic) IBOutlet UILabel *lbl_hederTitle;
@property (weak, nonatomic) IBOutlet UIView *base_Views;

@end

NS_ASSUME_NONNULL_END
