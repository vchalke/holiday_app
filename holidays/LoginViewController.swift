//
//  LoginViewController.swift
//  sotc-consumer-application
//
//  Created by Saurav Kumar on 18/07/17.
//  Copyright © 2017 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit
import CoreData


class LoginViewController: UIViewController,UITextFieldDelegate{
    
    //MARK: IBOutlet
    @IBOutlet var mobileNumberLabel: UILabel!
    @IBOutlet var mobileNumberTextField: UITextField!
    
    @IBOutlet var otpView: UIView!
    
    @IBOutlet var mobileNumberView: UIView!
    
    @IBOutlet var otpTextField1: UITextField!
    
    @IBOutlet var otpTextField6: UITextField!
    @IBOutlet var otpTextField5: UITextField!
    @IBOutlet var otpTextField4: UITextField!
    @IBOutlet var otpTextField3: UITextField!
    @IBOutlet var otpTextField2: UITextField!
    @IBOutlet var loginButton: UIButton!
    @IBOutlet var resendOtpButton: UIButton!
    
    @IBOutlet var versionLabel: UILabel!
    
    var isOTPViewPresented = false
    
    
    //MARK: UIViewController Life Cycle
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        
        versionLabel.text = "Version \(version ?? "")"
        
        mobileNumberLabel.font = UIFont.fontForDevice()
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.dismissKeyboard))
        
        //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
        //tap.cancelsTouchesInView = false
        
        self.view.addGestureRecognizer(tap)
        
        mobileNumberTextField.textAlignment = .center
        
        
        self.mobileNumberTextField.becomeFirstResponder()
       
     
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        self.loginButton.layer.cornerRadius = (self.loginButton.bounds.height / 2) - 4
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK: Button Click Methods
    
    
    @IBAction func onResendButtonClick(_ sender: Any) {
        
        self.sendOtpResquest()
    }
    
    
    @IBAction func onLoginButtonClick(_ sender: Any) {
        
        
        if (AppUtility.checkStirngIsEmpty(value: self.mobileNumberTextField.text!))
        {
            self.displayAlert(message: NetworkValidationMessages.NotValidMobileNumber.message)
        }
        else
        {
            
            
            if !(self.isOTPViewPresented) {
                
                if checkForNewUser() {
                    
                    let alert = UIAlertController(title: "New User", message: "Login with new user will clear offline data. Do you wish to continue ?", preferredStyle: UIAlertControllerStyle.alert)
                    
                    alert.addAction(UIAlertAction(title: AlertMessage.TITLE_CANCEL, style: UIAlertActionStyle.cancel, handler: { (UIAlertAction) in
                        
                        
                    }))
                    
                    alert.addAction(UIAlertAction(title: "Continue", style: UIAlertActionStyle.default, handler: { (UIAlertAction) in
                        
                        self.doLogin()
                        
                    }))
                    
                    UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
                }
                else
                {
                    doLogin()
                }
                
                
            }
            else
            {
                
                doLogin()
            }
            
            
            
        }
        
    }
    
    func doLogin()  {
        
        
        
        if self.isOTPViewPresented {
            
            if (AppUtility.checkStirngIsEmpty(value: self.otpTextField1.text!)) && (AppUtility.checkStirngIsEmpty(value: self.otpTextField2.text!)) && (AppUtility.checkStirngIsEmpty(value: self.otpTextField3.text!)) && (AppUtility.checkStirngIsEmpty(value: self.otpTextField4.text!)) && (AppUtility.checkStirngIsEmpty(value: self.otpTextField5.text!)) && (AppUtility.checkStirngIsEmpty(value: self.otpTextField6.text!))
            {
                self.displayAlert(message: NetworkValidationMessages.ValidOTP.message)
            }
            else
                
            {
                
                if UserDefauldConstant.isOTPByPass {
                    
                    ByPassOTP()
                }
                else
                {
                    ValidateOTP()
                }
                
                
            }
            
        }
        else
        {
 
            LoadingIndicatorView.show("Loading")
            
            //
            // var mobileNumber  =  UserDefauldConstant.defaultUserMobileNumber //self.getMobileNumber() //"8390131180"//self.mobileNumberTextField.text
            
            var mobileNumber  = self.mobileNumberTextField.text
            
            LoginController.sendCheckMobileRequest(mobileNumber: mobileNumber!
                , completion: { (status) in
                    
                    DispatchQueue.main.sync {
                        
                        LoadingIndicatorView.hide()
                        
                    }
                    if status.status
                    {
                        
                        if self.checkForNewUser()
                        {
                            
                        LoginController.clearDataForOldUser()
                            
                        }
                        
                        if !(UserDefauldConstant.isOTPByPass) {
                            
                            self.sendOtpResquest();
                        }
                        
                        
                        DispatchQueue.main.sync {
                            
                            if !(self.isOTPViewPresented) {
                                
                                self.presentOTPAnimation()
                                
                                self.isOTPViewPresented = true
                                
                            }
                        }
                        
                    }
                    else
                    {
                        
                        DispatchQueue.main.sync {
                            
                            self.displayAlert(message: status.message)
                            
                        }
                        
                    }
            })
            
        }
    }
    
    //MARK: Custom Methods
    
    func ByPassOTP()
    {
        let userDetailMO:UserProfile = NSEntityDescription.insertNewObject(forEntityName: "UserProfile", into: CoreDataController.getContext()) as! UserProfile
        
        // userDetailMO.mobileNumber = UserDefauldConstant.defaultUserMobileNumber
        
        
        //8510894234
        
        UserDefaults.standard.set("\(self.mobileNumberTextField.text!)", forKey: UserDefauldConstant.userMobileNumber)
        
        UserDefaults.standard.set(UserDefauldConstant.defaultUserMobileNumber, forKey: UserDefauldConstant.userMobileNumber)
        
        UserDefaults.standard.set(true, forKey: UserDefauldConstant.isUserLoggedIn)
        
        
        
        
        CoreDataController.getContext().mergePolicy = CoreDataConstant.dataMergePolicy
        
        
        CoreDataController.saveContext()
        
        
        self.setTabbarController()
    }
    
    func ValidateOTP() {
        
        LoadingIndicatorView.show("Loading")
        
        LoginController.sendOTPValidateRequest(mobileNumber: self.getMobileNumber() /*self.mobileNumberTextField.text!*/, otpString: "\(self.otpTextField1.text!)\(self.otpTextField2.text!)\(self.otpTextField3.text!)\(self.otpTextField4.text!)\(self.otpTextField5.text!)\(self.otpTextField6.text!)", completion: { (networkStatus) in
            
            DispatchQueue.main.sync {
                
                LoadingIndicatorView.hide()
                
            }
            
            if networkStatus.status
            {
                DispatchQueue.main.sync {
                    
                    LoadingIndicatorView.hide()
                    
                    
                    let userDetailMO:UserProfile = NSEntityDescription.insertNewObject(forEntityName: "UserProfile", into: CoreDataController.getContext()) as! UserProfile
                    
                    
                    
                    UserDefaults.standard.set("\(self.getMobileNumber() /*self.mobileNumberTextField.text!*/)", forKey: UserDefauldConstant.userMobileNumber)
                    userDetailMO.mobileNumber = "\(self.getMobileNumber() /*self.mobileNumberTextField.text!*/)"
                    
                    
                    // userDetailMO.mobileNumber = UserDefauldConstant.defaultUserMobileNumber
                    // UserDefaults.standard.set(UserDefauldConstant.defaultUserMobileNumber, forKey: UserDefauldConstant.userMobileNumber)
                    
                    UserDefaults.standard.set(true, forKey: UserDefauldConstant.isUserLoggedIn)
                    
                    CoreDataController.getContext().mergePolicy = CoreDataConstant.dataMergePolicy
                    
                    CoreDataController.saveContext()
                    
                    
                    self.setTabbarController()
                    
                    
                }
            }
            else
            {
                
                DispatchQueue.main.sync {
                    
                    self.displayAlert(message: networkStatus.message)
                    
                }
                
            }
            
        })
        
    }
    
    func setTabbarController() -> Void {
        
        if (UserDefaults.standard.bool(forKey: UserDefauldConstant.isRegisterForPushNotification)) {
            
            if !(UserDefaults.standard.bool(forKey: UserDefauldConstant.isSubscribeForPushNotification)) {
                
                NotificationController.sendNotificationSubscriberRequest { (Bool, String) in
                    
                    if Bool
                    {
                        UserDefaults.standard.set(true, forKey: UserDefauldConstant.isSubscribeForPushNotification)
                    }
                    else
                    {
                        UserDefaults.standard.set(false, forKey: UserDefauldConstant.isSubscribeForPushNotification)
                        
                    }
                    
                    
                }
            }
            
        }
        else
        {
            
        }
        
        
        
        
        UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.gray], for:.normal)
        
        UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.black], for:.selected)
        
        //        let userProfileVC : UserProfileViewController = UserProfileViewController (nibName: "UserProfileViewController", bundle: nil)
        //        userProfileVC.title = "PROFILE"
        //        userProfileVC.tabBarItem.selectedImage = #imageLiteral(resourceName: "profileSelected").withRenderingMode(.alwaysOriginal)
        //        userProfileVC.tabBarItem.image = #imageLiteral(resourceName: "profileUnselected").withRenderingMode(.alwaysOriginal)
        //
        
        let userBookingVC : UserBookingHomeViewController = UserBookingHomeViewController (nibName: "UserBookingHomeViewController", bundle: nil)
        userBookingVC.title = Title.MY_BOOKING
        userBookingVC.tabBarItem.selectedImage = #imageLiteral(resourceName: "userBookingTabSelected").withRenderingMode(.alwaysOriginal)
        userBookingVC.tabBarItem.image = #imageLiteral(resourceName: "userBookingTabUnSelected").withRenderingMode(.alwaysOriginal)
        
        
        let menuVC : MenuViewController = MenuViewController (nibName: "MenuViewController", bundle: nil)
        menuVC.title = "MORE"
        menuVC.tabBarItem.selectedImage  = #imageLiteral(resourceName: "moreTabSelected").withRenderingMode(.alwaysOriginal)
        menuVC.tabBarItem.image = #imageLiteral(resourceName: "moreTabUnSelected").withRenderingMode(.alwaysOriginal)
        
        
        let notificationVC : NotificationViewController = NotificationViewController (nibName: "NotificationViewController", bundle: nil)
        notificationVC.title = "NOTIFICATION"
        notificationVC.tabBarItem.selectedImage  = #imageLiteral(resourceName: "notificationTabSelected").withRenderingMode(.alwaysOriginal)
        notificationVC.tabBarItem.image = #imageLiteral(resourceName: "notificationTabUnSelected").withRenderingMode(.alwaysOriginal)
        
        
        //  let userProfileNavVC : UINavigationController = UINavigationController(rootViewController: userProfileVC)
        let userBookingNavVC : UINavigationController = UINavigationController(rootViewController: userBookingVC)
        let notificationNavVC : UINavigationController = UINavigationController(rootViewController: notificationVC)
        let menuNavVC : UINavigationController = UINavigationController(rootViewController: menuVC)
        
        
        let applicationTabbar : UITabBarController = UITabBarController()
        
        applicationTabbar.setViewControllers([userBookingNavVC,notificationNavVC, menuNavVC], animated: true)
        
        
        
        UIView.transition(with: self.view, duration: 2.0, options: UIViewAnimationOptions.curveEaseInOut, animations: {
            
            if let appDelegate : AppDelegate = UIApplication.shared.delegate as? AppDelegate
            {
                appDelegate.window?.rootViewController = applicationTabbar
                
            }
            
        }, completion: { (finished: Bool) -> () in
            
            // completion
            
        })
        
        
        
        
    }
    
    func presentOTPAnimation() {
        
        self.otpView.frame  = CGRect(x: UIScreen.main.bounds.width, y:  self.mobileNumberView.frame.origin.y, width:  self.mobileNumberView.frame.width, height:  self.mobileNumberView.frame.height)
        
        self.view.addSubview(self.otpView)
        
        UIView.animate(withDuration: 2, animations: {
            
        }) { (_) in
            
            
            UIView.animate(withDuration: 2, animations: {
                
                self.mobileNumberView.frame.origin.x = -UIScreen.main.bounds.width
                
                self.otpView.frame  = CGRect(x: 30, y:  self.mobileNumberView.frame.origin.y, width:  self.mobileNumberView.frame.width, height:  self.mobileNumberView.frame.height)
                
                
            }) {(_) in
                
                self.resendOtpButton.isHidden = false
                
                self.resendOtpButton.isUserInteractionEnabled = true
                
                self.mobileNumberView.isHidden = true
                
                 self.otpTextField1.becomeFirstResponder()
                
            }
            
            
            
            //            UIView.animate(withDuration: 2, delay: 1, options: [.curveEaseIn], animations: {
            //
            //
            //
            //
            //            })
        }
        
        
    }
    
    
    func checkForNewUser() -> Bool   {
        
        if let userMobileNumber:String = UserDefaults.standard.object(forKey: (UserDefauldConstant.userMobileNumber )) as? String
        {
            
            if userMobileNumber == (self.mobileNumberTextField.text!) {
                
                return false
            }
        }else{
        
            return false
        
        }
        
        return true
        
    }
    func displayAlert(message:String)
    {
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: AlertMessage.TITLE_OK, style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func getMobileNumber() -> String
    {
        
        if !((self.mobileNumberTextField.text?.isEmpty)!)
        {
            return self.mobileNumberTextField.text! //.toLengthOf(length: 3)
        }
        
        return ""
        
        
    }
    func sendOtpResquest()
    {
        LoginController.sendOTPRequest(mobileNumber:self.getMobileNumber()  /*self.mobileNumberTextField.text!*/, completion: { (networkStatus) in
            
            DispatchQueue.main.sync {
                
                if networkStatus.status
                {
                    self.displayAlert(message: networkStatus.message + "\(self.getMobileNumber())")
                    
                }else{
                    
                    self.displayAlert(message: networkStatus.message)
                    
                }
                
            }
            
            
        })
        
        
    }

    
    
    //MARK: TextField Delegate Methods
    
    
  
    
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        
        if (textField == mobileNumberTextField ) && (string.characters.count != 0)
        {
            
            let textFieldText: NSString = (textField.text ?? "") as NSString
            
            let txtAfterUpdate = textFieldText.replacingCharacters(in: range, with: string)
            
            
            //Should Resign After entering 10 Digits
            if ((txtAfterUpdate.characters.count) <= 10)  {
                
                if ((txtAfterUpdate.characters.count) == 10)  {
                    
                    self.mobileNumberTextField.text = txtAfterUpdate
                    
                    textField.resignFirstResponder()
                    
                    return false
                }
                else
                {
                    return true
                }
                
            }
            else
            {
                textField.resignFirstResponder()
                return false
            }
        }
            
            
        //OTP Logic In Forword Direction
        else if ((textField == otpTextField1) || (textField == otpTextField2) || (textField == otpTextField3) || (textField == otpTextField4) || (textField == otpTextField5) || (textField == otpTextField6)) && ( ((textField.text?.characters.count)! < 1)) && (string.characters.count > 0)
        {
            
            switch textField {
                
            case otpTextField1:
                
                otpTextField2.becomeFirstResponder()
        
            case otpTextField2:
                
                otpTextField3.becomeFirstResponder()
                
            case otpTextField3:
                
                otpTextField4.becomeFirstResponder()
                
            case otpTextField4:
                
                otpTextField5.becomeFirstResponder()
                
            case otpTextField5:
                
                otpTextField6.becomeFirstResponder()
                
            case otpTextField6:
                
                textField.resignFirstResponder()
    
            default:
                
                textField.resignFirstResponder()
            }
            
            
            textField.text = string
            
            
            return false
        }
        //OTP Logic In Backword Direction
        else if ((textField == otpTextField1) || (textField == otpTextField2) || (textField == otpTextField3) || (textField == otpTextField4) || (textField == otpTextField5) || (textField == otpTextField6)) && ( ((textField.text?.characters.count)! >= 1)) && (string.characters.count == 0)
        {
            switch textField {
                
                
            case otpTextField1:
                
               otpTextField1.text = ""
                
              //  otpTextField2.becomeFirstResponder()
                
                
            case otpTextField2:
                
                
                otpTextField2.text = ""
                
                otpTextField1.becomeFirstResponder()
                
                
            case otpTextField3:
                
                
                otpTextField3.text = ""
                otpTextField2.becomeFirstResponder()
                
            case otpTextField4:
                
                otpTextField4.text = ""
                otpTextField3.becomeFirstResponder()
                
            case otpTextField5:
                
                
                otpTextField5.text = ""
                otpTextField4.becomeFirstResponder()
                
            case otpTextField6:
                
                otpTextField6.text = ""
                otpTextField5.becomeFirstResponder()
        
            default:
                
                textField.resignFirstResponder()
                
            }
            
            return false
        }
        else
        {
            return true
        }
        
    }
    
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        switch textField {
            
        case mobileNumberTextField:
            
            textField.resignFirstResponder()
            
            return true
            
        case otpTextField1:
            
            otpTextField2.becomeFirstResponder()
            return false
            
        case otpTextField2:
            
            otpTextField3.becomeFirstResponder()
            return false
            
        case otpTextField3:
            
            otpTextField4.becomeFirstResponder()
            return false
            
        case otpTextField4:
            
            otpTextField5.becomeFirstResponder()
            return false
            
        case otpTextField5:
            
            otpTextField6.becomeFirstResponder()
            return false
            
        case otpTextField6:
            
            
            textField.resignFirstResponder()
            return true
            
        default:
            return true
        }
        
    }
    
 
    
    
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    

    
    
}

/*extension String {
    
    func toLengthOf(length:Int) -> String {
        if length <= 0 {
            return self
        } else if let to = self.index(self.startIndex, offsetBy: length, limitedBy: self.endIndex) {
            return self.substring(from: to)
            
        } else {
            return ""
        }
    }
}*/
