//
//  LoginCommunicationManager.m
//  holidays
//
//  Created by ketan on 21/12/17.
//  Copyright © 2017 Mobicule. All rights reserved.
//

#import "LoginCommunicationManager.h"
#import "UIViewController+MJPopupViewController.h"
#import "LoadingView.h"
#import "NetCoreAnalyticsVC.h"
#import <NetCorePush/NetCorePush.h>


@implementation LoginCommunicationManager
{
    LoadingView *activityLoadingView;
//    LoginCommunicationManager *loginOBJ;
}


-(void)loginCheckWithType:(NSString *)signInType withUserID:(NSString *)userId withPassword:(NSString *)password
{
    //abc@test.com/
    activityLoadingView = [LoadingView loadingViewInView:[UIApplication sharedApplication].keyWindow
                                              withString:@""
                                       andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
    
    @try
    {
        
        NetworkHelper *helper = [NetworkHelper sharedHelper];
        
        NSString *pathParam = userId;
        
        NSDictionary *headerDict = [CoreUtility getHeaderDict];
        
        [helper getResponseWithRequestType:@"" withQueryParam:@"" withPathParam:pathParam withJsonParam:nil withHeaders:headerDict withUrl:kAstraUrlLoginCheck success:^(NSDictionary *responseDict)
         {
             NSLog(@"Response Dict : %@",responseDict);
             
             
             dispatch_async(dispatch_get_main_queue(), ^(void)
                            {
                                [activityLoadingView removeFromSuperview];
                                if (responseDict)
                                {
                                    if (responseDict.count>0)
                                    {
                                        if ([signInType isEqualToString:@"signIn"] || [signInType isEqualToString:@"signUp"])
                                        {
                                            [self savePasswordWithType:signInType withPassword:password withUserId:userId];
                                        }
                                        else if ([signInType isEqualToString:@"guestLogin"])
                                        {
                                            
                                              [self netCoreDataForLoginWithEmailID:userId]; //09-04-2018 --
                                            
                                            [self setUserDetailsAndClosePopUpWithUserID:userId withPassword:password];
                                            [self.delegate guestLoggedInSuccessFullyWithUserID:userId];
                                            
                                            
                                            
                                            //if (self.moduleName != nil && [self.moduleName isEqualToString:@"Self Service"])
                                            if (self.moduleName != nil && [self.moduleName isEqualToString:@"Manage Holidays"])
                                            {
                                          
                                                
                                                
                                            }else
                                            {
                                                UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Success"
                                                                                                                   message:@"User Signed in Successfully"
                                                                                                            preferredStyle: UIAlertControllerStyleAlert];
                                                
                                                
                                                
                                                UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                                                           {
                                                                               
                                                                               //                                                                           [self setUserDetailsAndClosePopUpWithUserID:userId withPassword:password];
                                                                               //                                                                           [self.delegate guestLoggedInSuccessFullyWithUserID:userId];
                                                                               
                                                                               //  [self setUserDetailsAndClosePopUp];
                                                                           }];
                                                
                                                [alertView addAction:actionOk];
                                                
                                                [_loginViewController presentViewController:alertView animated:YES completion:nil];
                                            }
                                            
                                            
                                            
                                            
                                        }
                                    }
                                    else
                                    {
                                         [self.delegate loggedInFailWithRason:@"Some error Occured"];
                                    }
                                }
                                else
                                {
                                    [self.delegate loggedInFailWithRason:@"Some error Occured"];
                                }
                                    
                            });
             
         }
                                   failure:^(NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^(void)
                            {
                                [activityLoadingView removeFromSuperview];
                                 [self.delegate loggedInFailWithRason:[error description]];
                                NSLog(@"Response Dict : %@",[error description]);
                            });
             
             
             
         }];
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"%@", exception.reason);
        [activityLoadingView removeFromSuperview];
    }
    @finally
    {
        NSLog(@"exception finally called");
    }
}

-(void)savePasswordWithType:(NSString *)signInType withPassword:(NSString *)password withUserId:(NSString *)userID

{
    activityLoadingView = [LoadingView loadingViewInView:[UIApplication sharedApplication].keyWindow
                                              withString:@""
                                       andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
    
    @try
    {
        
        NetworkHelper *helper = [NetworkHelper sharedHelper];
        
        
        
        NSDictionary *headerDict = [CoreUtility getHeaderDict];
        
        [helper getResponseWithRequestType:@"" withQueryParam:@"" withPathParam:@"" withJsonParam:nil withHeaders:headerDict withUrl:kAstraUrlSavePass success:^(NSDictionary *responseDict)
         {
             NSLog(@"Response Dict : %@",responseDict);
             
             
             dispatch_async(dispatch_get_main_queue(), ^(void) {
                 [activityLoadingView removeFromSuperview];
                 if (responseDict)
                 {
                     if (responseDict.count>0)
                     {
                         NSString *keyString = [responseDict valueForKey:@"text"];
                         
                         [self fetchEncryptionPassWithKey:keyString withloginType:signInType withPassword:password withUserID:userID];
                         
                     }
                     else
                     {
                         [self.delegate loggedInFailWithRason:@"Some error Occured"];
                     }
                         
                 }
                 else
                 {
                     [self.delegate loggedInFailWithRason:@"Some error Occured"];
                 }
                     
             });
             
         }
                                   failure:^(NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^(void)
                            {
                                [activityLoadingView removeFromSuperview];
                                [self.delegate loggedInFailWithRason:[error description]];
                                NSLog(@"Response Dict : %@",[error description]);
                            });
             
             
             
         }];
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"%@", exception.reason);
        [activityLoadingView removeFromSuperview];
    }
    @finally
    {
        NSLog(@"exception finally called");
    }
    
}

-(void)fetchEncryptionPassWithKey:(NSString *)key withloginType:(NSString *)loginType withPassword:(NSString *)password withUserID :(NSString *)userID
{
    activityLoadingView = [LoadingView loadingViewInView:[UIApplication sharedApplication].keyWindow withString:@"" andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
    
    
    //            "key": "495kcG744o"
    
    NSDictionary *dict = [[NSDictionary alloc]initWithObjectsAndKeys:key, @"key",password,@"password",nil];
    
    NetworkHelper *helper = [NetworkHelper sharedHelper];
    if([CoreUtility connected])
    {
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            NSString *strResponse = [helper getDataFromServerForType:@"sync" entity:@"encrypt" action:@"generate" andUserJson:dict];
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                [activityLoadingView removeFromSuperview];
                if (![strResponse isEqualToString:@"No Internet"])
                {
                    NSString *strStatus = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"Status"];
                    NSString *strMessage = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"message"];
                    
                    if ([[strStatus lowercaseString] isEqualToString:kStatusSuccess])
                    {
                        
                        NSDictionary *dataDict = [[[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"data"] firstObject];
                        
                        if ([loginType isEqualToString:@"signIn"])
                        {
                            NSString *text1 = [dataDict valueForKey:@"text1"];
                            NSString *text2 = [dataDict valueForKey:@"text2"];
                            NSString *text3 = [dataDict valueForKey:@"text3"];
                            
                            [self verifyCredentialsWithText1:text1 withText2:text2 withText3:text3 withloginType:loginType withUserID:userID withPassword:password];
                            
                        }
                        else
                        {
                            
                            //                            RegisterUserViewController *registerUserVC = [[RegisterUserViewController alloc] initWithNibName:@"RegisterUserViewController" bundle:nil];
                            //                            registerUserVC.dictforEncryption = dataDict;
                            //                            registerUserVC.userId = self.textFieldUserId.text;
                            //                            registerUserVC.delegate = self;
                            // [self presentViewController:registerUserVC animated:YES completion:nil];
                            
                            //   [[SlideNavigationController sharedInstance] pushViewController:registerUserVC animated:YES];
                        }
                        
                    }
                    else
                    {
                        UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Alert"
                                                                                           message:strMessage
                                                                                    preferredStyle: UIAlertControllerStyleAlert];
                        
                        UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                        {
                            
                             [self.delegate loggedInFailWithRason:strMessage];
                            [alertView dismissViewControllerAnimated:YES completion:nil];
                            
                        }];
                        [alertView addAction:actionOk];
                        [_loginViewController presentViewController:alertView animated:YES completion:nil];
                    }
                }
                else
                {
                    [activityLoadingView removeFromSuperview];
                     [self.delegate loggedInFailWithRason:@"Please check your internet connection"];
                }
            });
        });
        
    }
    else
    {
        UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Alert"
                                                                           message:kMessageNoInternet
                                                                    preferredStyle: UIAlertControllerStyleAlert];
        
        UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [activityLoadingView removeFromSuperview];
            [alertView dismissViewControllerAnimated:YES completion:nil];
            
        }];
        
        [alertView addAction:actionOk];
        [_loginViewController presentViewController:alertView animated:YES completion:nil];
    }
    
}

-(void)verifyCredentialsWithText1:(NSString *)text1 withText2:(NSString *)text2 withText3:(NSString *)text3 withloginType:(NSString *)loginType withUserID:(NSString *)userID withPassword:(NSString *)password
{
    
    activityLoadingView = [LoadingView loadingViewInView:[UIApplication sharedApplication].keyWindow
                                              withString:@""
                                       andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
    
    @try
    {
        
        NetworkHelper *helper = [NetworkHelper sharedHelper];
        
        //{"userId":"abc@test.com","text1":"3a9887c2b5e0e4a75c5bf659efbd1ff7","text2":"3bf1a427961d08a4539e59ffb138706e","text3":"d5gPsB4M+k3QqINVL+un7w=="}
        
        
        NSMutableDictionary *dictForRequest = [[NSMutableDictionary alloc] init];
        [dictForRequest setObject:userID forKey:@"userId"];
        [dictForRequest setObject:text1 forKey:@"text1"];
        [dictForRequest setObject:text2 forKey:@"text2"];
        [dictForRequest setObject:text3 forKey:@"text3"];
        
        NSDictionary *headerDict = [CoreUtility getHeaderDict];
        
        [helper getResponseWithRequestType:@"POST" withQueryParam:@"" withPathParam:@"" withJsonParam:dictForRequest withHeaders:headerDict withUrl:kAstraVerifyCredentials success:^(NSDictionary *responseDict)
         {
             NSLog(@"Response Dict : %@",responseDict);
             
             
             dispatch_async(dispatch_get_main_queue(), ^(void) {
                 [activityLoadingView removeFromSuperview];
                 if (responseDict)
                 {
                     if (responseDict.count>0)
                     {
                         if ([[[responseDict valueForKey:@"message"] lowercaseString] isEqualToString:@"false"])
                         {
                             NSString *reasonOfMessage = [responseDict valueForKey:@"reasonOfMessage"];
                             
                             UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Alert"
                                                                                                message:reasonOfMessage
                                                                                         preferredStyle: UIAlertControllerStyleAlert];
                             
                             UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                             {
                                 [self.delegate loggedInFailWithRason:reasonOfMessage];
                                 
                                 [alertView dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
                             [alertView addAction:actionOk];
                             [_loginViewController presentViewController:alertView animated:YES completion:nil];
                             
                         }
                         else
                         {
                              [self netCoreDataForLoginWithEmailID:userID]; //09-04-2018 --
                             
                             
                             NSDictionary * userDetailsDict = [responseDict valueForKey:@"userDetail"]; //09-04-2018 --
                             
                             if( userDetailsDict && ![userDetailsDict isEqual:[NSNull new]]) //09-04-2018 --
                             {
                                 
                                 NSString * userMobileNumberString = [userDetailsDict valueForKey:@"mobileNo"];
                                 
                                 if(nil != userMobileNumberString && ![userMobileNumberString isEqual:[NSNull new]] && ![userMobileNumberString  isEqualToString:@""])
                                 {
                                     
                                     [[NSUserDefaults standardUserDefaults]setValue:userMobileNumberString  forKey:kuserDefaultTC_MobileNumber]; //09-04-2018 --
                                     [[NSUserDefaults standardUserDefaults]synchronize];
                                     
                                     [[NetCoreSharedManager sharedInstance] setUpIdentity:[userDetailsDict valueForKey:@"mobileNo"]]; //24-04-2018 --
                                 }
                                 
                                 
                             }
                             
                             [self setUserDetailsAndClosePopUpWithUserID:userID withPassword:password]; 

                             [self.delegate UserloggedInSuccessFullWithUserDict:responseDict];
                             
                             // [self setUserDetailsAndClosePopUp];
                         }
                     }
                     else
                     {
                         [self.delegate loggedInFailWithRason:@"Some error Occured"];
                     }
                         
                 }
                 else
                 {
                     [self.delegate loggedInFailWithRason:@"Some error Occured"];
                 }
             });
             
         }
                                   failure:^(NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^(void)
                            {
                                [activityLoadingView removeFromSuperview];
                                NSLog(@"Response Dict : %@",[error description]);
                            });
             
             
             
         }];
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"%@", exception.reason);
        [activityLoadingView removeFromSuperview];
    }
    @finally
    {
        NSLog(@"exception finally called");
    }
}


-(void)socialLoginWithFirstName:(NSString *)firstName withLastName:(NSString *)lastName withImageURl:(NSString *)imageURL withType:(NSString *)type withUserID:(NSString *)userID withID:(NSString *)Id
{
    activityLoadingView = [LoadingView loadingViewInView:[UIApplication sharedApplication].keyWindow
                                              withString:@""
                                       andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
    
    @try
    {
        
        NetworkHelper *helper = [NetworkHelper sharedHelper];
        NSMutableDictionary *dictForRequest = [[NSMutableDictionary alloc] init];
        [dictForRequest setObject:firstName forKey:@"firstName"];
        [dictForRequest setObject:Id forKey:@"id"];
        [dictForRequest setObject:imageURL forKey:@"imgUrl"];
        [dictForRequest setObject:lastName forKey:@"lastName"];
        [dictForRequest setObject:type forKey:@"type"];
        [dictForRequest setObject:userID forKey:@"userId"];
        
        NSDictionary *headerDict = [CoreUtility getHeaderDict];
        
        [helper getResponseWithRequestType:@"POST" withQueryParam:@"" withPathParam:@"" withJsonParam:dictForRequest withHeaders:headerDict withUrl:kAstraSocialLogin success:^(NSDictionary *responseDict)
         {
             NSLog(@"Response Dict : %@",responseDict);
             
             
             dispatch_async(dispatch_get_main_queue(), ^(void) {
                 [activityLoadingView removeFromSuperview];
                 if (responseDict)
                 {
                     if (responseDict.count>0)
                     {
                         
                          [self netCoreDataForLoginWithEmailID:userID]; //09-04-2018 --
                         
                         UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Success" message:@"User Signed in Successfully"preferredStyle: UIAlertControllerStyleAlert];

                         UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                                    {
                                                        [self setUserDetailsAndClosePopUpWithUserID:userID withPassword:@""];
                                                        
                                                         [self.delegate guestLoggedInSuccessFullyWithUserID:userID];
                                                        //  [self setUserDetailsAndClosePopUp];
                                                    }];
                         
                         [alertView addAction:actionOk];
                         
                         [_loginViewController presentViewController:alertView animated:YES completion:nil];
                         
                     }
                 }
                
             });
             
         }
                                   failure:^(NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^(void)
                            {
                                [activityLoadingView removeFromSuperview];
                                NSLog(@"Response Dict : %@",[error description]);
                            });
             
             
             
         }];
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"%@", exception.reason);
        [activityLoadingView removeFromSuperview];
    }
    @finally
    {
        NSLog(@"exception finally called");
    }
}

#pragma mark - FacebookLogin
-(void)setUpFacebookLogin
{
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    login.loginBehavior = FBSDKLoginBehaviorWeb;
    [login logOut];
    [login logInWithReadPermissions: @[@"public_profile",@"email"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error)
    {
         if (error)
         {
             NSLog(@"error----> %@",error);
             UIAlertView * calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Unable to retreive emailid from facebook.please enter your email id here..!" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
             [calert show];
             NSLog(@"Process error");
//             [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
//             loginOBJ.view.frame = CGRectMake(0, 0, 300, 455);
//             [self presentPopupViewController:loginOBJ animationType:MJPopupViewAnimationFade];
             
             
         }
         else if (result.isCancelled)
         {
             NSLog(@"Cancelled");
             UIAlertView * calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Unable to retreive emailid from facebook.please enter your email id" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
             [calert show];
//             [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
//             loginOBJ.view.frame = CGRectMake(0, 0, 300, 455);
//             [self presentPopupViewController:loginOBJ animationType:MJPopupViewAnimationFade];
             
             
         }
         else
         {
             [self getBasicInfoWithResult:result];
             //             NSLog(@"Logged in");
             //             UIAlertView * calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Unable to retreive emailid from facebook.please enter your email id" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
             //             [calert show];
             //             [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
             //             loginOBJ.view.frame = CGRectMake(0, 0, 300, 455);
             //             [self presentPopupViewController:loginOBJ animationType:MJPopupViewAnimationFade];
         }
         
     }];
    
//    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
//    
//    if ([[[UIDevice currentDevice] systemVersion] floatValue] <= 9) {
//        // After iOS9 we can not use it anymore
//        login.loginBehavior = FBSDKLoginBehaviorSystemAccount;
//    } else {
//        login.loginBehavior = FBSDKLoginBehaviorWeb;
//    }
//    
//    NSArray *permission = [[NSArray alloc] initWithObjects:@"email",@"public_profile",@"user_friends", nil];
//    NSLog( @"### running FB sdk version: %@", [FBSDKSettings sdkVersion] );
//    
//    [login logInWithReadPermissions:permission fromViewController:(UIViewController *)self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
////        [self removeActivityIndicatorWithBg:activityIndicator];
//        
//        if (error) {
//            NSLog(@"Process error");
//        } else if (result.isCancelled) {
//            NSLog(@"Cancelled");
//        } else {
//            NSLog(@"Logged in%@",result.grantedPermissions);
//        }
//    }];
}

-(void)getBasicInfoWithResult:(FBSDKLoginManagerLoginResult *)result
{
    // loginOBJ.view.frame = CGRectMake(0, 0, 300, 455);
    
    if ([result.grantedPermissions containsObject:@"email"])
    {
        if ([FBSDKAccessToken currentAccessToken])
        {
            NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
            // [parameters setValue:@"name,picture.type(large)" forKey:@"fields"];
            [parameters setValue:@"email" forKey:@"fields"];//public_profile
            //[parameters setValue:@"public_profile" forKey:@"fields"];
            
            [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:parameters]
             startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error)
             {
                 if (!error)
                 {
                     NSLog(@"fetched user:%@", result);
                     //  NSString *name = [[result valueForKey:@"name"] lowercaseString];
                     // NSString *username = [name stringByReplacingOccurrencesOfString:@" " withString:@""];
                     // NSArray *picture_arr = [result objectForKey:@"picture"];
                     //    NSArray *data_arr = [picture_arr valueForKey:@"data"];
                     NSString *email = [result valueForKey:@"email"];
                     //  NSString *fbID = [result valueForKey:@"id"];
                     
                     if (email)
                     {
                         
                         NSUserDefaults *userIdForGooglePlusSignIn=[NSUserDefaults standardUserDefaults];
                         [userIdForGooglePlusSignIn setObject:email forKey:kLoginEmailId];
                         
                         
                         //[self fetchMobileNumber];
                         
                         //                         UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Success"
                         //                                                                                            message:@"User Signed in Successfully"
                         //                                                                                     preferredStyle: UIAlertControllerStyleAlert];
                         //
                         //                         UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                         //
                         //                             [alertView dismissViewControllerAnimated:YES completion:nil];
                         //                             [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
                         //
                         //                             if (self.delegate && [self.delegate respondsToSelector:@selector(dismissLoginPopUp)])
                         //                             {
                         //                                 [self.delegate dismissLoginPopUp];
                         //
                         //                             }
                         //
                         //                         }];
                         //
                         //                         [alertView addAction:actionOk];
                         //
                         //                         [self presentViewController:alertView animated:YES completion:nil];
                         
                         //                         self.textFieldUserId.text = [NSString stringWithFormat:@"%@", email];
                         
                         [self socialLoginWithFirstName:@"" withLastName:@"" withImageURl:@"" withType:@"FB" withUserID:email withID:@"fb1021596341229809"];
                     }
                     else
                     {
                         UIAlertView * calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Unable to retreive emailid from facebook please enter your email id" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
                         [calert show];
                         //                         [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
//                         loginOBJ.view.frame = CGRectMake(0, 0, 300, 455);
                         //                         [self presentPopupViewController:loginOBJ animationType:MJPopupViewAnimationFade];
                     }
                 }
                 else
                 {
                     NSLog(@"Unable to Login");
                 }
                 
             }];
        }
    }
}

-(void)setUserDetailsAndClosePopUpWithUserID:(NSString *)userID withPassword:(NSString *)password
{
    [[NSUserDefaults standardUserDefaults]setValue:userID forKey:kuserDefaultUserId]
    ;
    [[NSUserDefaults standardUserDefaults]setValue:password forKey:kuserDefaultPassword]
    ;
    
    NSUserDefaults *LoginStatus=[NSUserDefaults standardUserDefaults];
    [LoginStatus setObject:kLoginSuccess  forKey:kLoginStatus];
    
    NSUserDefaults *userIdForGooglePlusSignIn=[NSUserDefaults standardUserDefaults];
    
    [userIdForGooglePlusSignIn setObject:userID forKey:kLoginEmailId];
}

-(void)netCoreDataForLoginWithEmailID:(NSString *)emailID
{
    
    NSMutableDictionary *payloadList =  [NSMutableDictionary dictionary];
    [payloadList setObject:emailID forKey:@"EMAIL"];
    
    [NetCoreAnalyticsVC getNetCorePushNotificationDetailDictionary:payloadList withPayloadCount:102];
}

@end
