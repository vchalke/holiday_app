//
//  MealsObject.h
//  holidays
//
//  Created by Kush_Tech on 17/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MealsObject : NSObject
-(instancetype)initWithMealsObjectDict:(NSDictionary *)dictionary;
@property(nonatomic,strong)NSDictionary *tcilMstMeal;
@property(nonatomic)NSInteger packageClassId;
@property(nonatomic,strong)NSString *mealDescription;
@property(nonatomic,strong)NSString *mealName;
@end

NS_ASSUME_NONNULL_END
