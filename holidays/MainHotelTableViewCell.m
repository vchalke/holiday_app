//
//  MainHotelTableViewCell.m
//  holidays
//
//  Created by Kush_Tech on 04/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "MainHotelTableViewCell.h"
#import "ChildHotelTableViewCell.h"
#import "SightSeeingTableViewCell.h"
#import "WebUrlConstants.h"
@implementation MainHotelTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
     [self.main_tableView registerNib:[UINib nibWithNibName:@"ChildHotelTableViewCell" bundle:nil] forCellReuseIdentifier:@"ChildHotelTableViewCell"];
      [self.main_tableView registerNib:[UINib nibWithNibName:@"SightSeeingTableViewCell" bundle:nil] forCellReuseIdentifier:@"SightSeeingTableViewCell"];
        self.main_tableView.delegate = self;
        self.main_tableView.dataSource = self;
    
}

-(void)loadHotelDataFromPackageModel:(PackageDetailModel*)packageModel forHolidayPackageDetail:(HolidayPackageDetail *)holiPackageDetailsModel{
    
//    NSPredicate * predicate1 =  [NSPredicate predicateWithFormat:@"(packageClassId == %@) ", self.packageDetail.stringSelectedAccomType ];
//    NSArray *filteredArray = [packageModel.hotelCollectionArray filteredArrayUsingPredicate:predicate1];
    packageDetailModel = packageModel;
    hotelObjectsArray = [[NSMutableArray alloc]init];
    self.btn_ViewMore.hidden = self.isShowViewMore;
//    NSArray *filteredArray = packageModel.hotelCollectionArray;
//    NSLog(@"filteredArray Count %ld",[filteredArray count]);
    
    NSSortDescriptor *brandDescriptor = [[NSSortDescriptor alloc] initWithKey:@"position" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:brandDescriptor];
    NSArray *filteredArray = [packageModel.hotelCollectionArray sortedArrayUsingDescriptors:sortDescriptors];
    NSMutableArray *araay = [[NSMutableArray alloc]init];
    for (id object in filteredArray) {
        HolidayAccomodationObject *hotelObj = [[HolidayAccomodationObject alloc]initWithAccomodationDict:object];
        
//        if ((hotelObj.position-1)>[hotelObjectsArray count]){
//            [araay addObject:hotelObj];
//        }else{
//        [araay insertObject:hotelObj atIndex:hotelObj.position-1];
//        }
        
        if (hotelObj.packageClassId == holiPackageDetailsModel.mainPkgSubClassId){
            [hotelObjectsArray addObject:hotelObj];
        }
    }
    
    
    
//    for (HolidayAccomodationObject *hotelObj in araay) {
//        if (hotelObj.packageClassId == holiPackageDetailsModel.mainPkgSubClassId){
//            [hotelObjectsArray addObject:hotelObj];
//        }
//    }
    
//    if ([hotelObjectsArray count] > 0){
//        NSSortDescriptor *sortByName = [NSSortDescriptor sortDescriptorWithKey:@"position"
//            ascending:YES];
//        NSArray *sortDescriptors = [NSArray arrayWithObject:sortByName];
//        NSArray *sortedObjArray = [hotelObjectsArray sortedArrayUsingDescriptors:sortDescriptors];
//        [hotelObjectsArray removeAllObjects];
//        hotelObjectsArray = [sortedObjArray mutableCopy];
//        [self.main_tableView reloadData];
//    }
    
    if ([hotelObjectsArray count]>0){
        self.main_tableView.hidden = NO;
        [self.main_tableView reloadData];
    }else{
        NSMutableString *mutString = [[NSMutableString alloc]init];
        self.main_tableView.hidden = YES;
        if (holiPackageDetailsModel.hotelDefaultMsg.length > 2){
            [mutString appendString:[NSString stringWithFormat:@"%@\n",holiPackageDetailsModel.hotelDefaultMsg]];
        }else{
            for (NSDictionary *object in holiPackageDetailsModel.hotelCollection) {
                NSInteger pkgClasId = [[object objectForKey:@"packageClassId"] integerValue];
                if (pkgClasId == holiPackageDetailsModel.mainPkgSubClassId){
                    [mutString appendString:[NSString stringWithFormat:@"%@\n",object[@"typeDefaultMsg"]]];
                }
            }
            for (NSDictionary *object in holiPackageDetailsModel.accombdationCollection) {
                NSInteger pkgClasId = [[object objectForKey:@"packageClassId"] integerValue];
                if (pkgClasId == holiPackageDetailsModel.mainPkgSubClassId){
                    [mutString appendString:[NSString stringWithFormat:@"%@\n",object[@"typeDefaultMsg"]]];
                }
            }
        }
        NSString *htmlStringObj = [NSString stringWithFormat:@"<span style=\"font-family: Lato-Regular; font-size: 15\">%@</span>", [mutString copy]];
        [self.webViews loadHTMLString:htmlStringObj baseURL:nil];
    }
    
    
}
-(void)loadSightSeeingDataFromPackageModel:(PackageDetailModel*)packageModel forHolidayPackageDetail:(HolidayPackageDetail *)holiPackageDetailsModel{
    self.btn_ViewMore.hidden = self.isShowViewMore;
    packageDetailModel = packageModel;
    sightObjectsArray = [[NSMutableArray alloc]init];
    sightObjectsDict = [[NSMutableDictionary alloc]init];
//    NSMutableArray *newDictArray = [[NSMutableArray alloc]init];
//    NSDictionary *newDict = [[NSDictionary alloc]init];
    
    /*
    NSArray *filteredArray = packageModel.sightseenCollectionArray;
    for (id object in filteredArray) {
        SightSeenObject *sightObj = [[SightSeenObject alloc]initWithSightSeenObjectDict:object];
        
        if (sightObj.packageClassId == holiPackageDetailsModel.mainPkgSubClassId){
            [sightObjectsArray addObject:sightObj];
        }
        
        
        if ([sightObjectsDict objectForKey:sightObj.cityName]){
            
        }
        [sightObjectsDict setValue:sightObj.cityName forKey:sightObj.name];
    }
    [self.main_tableView reloadData];
    */
    NSArray *filteredArray = packageModel.sightseenCollectionArray;
    if ([filteredArray count]>0){
        self.main_tableView.hidden = NO;
        for (id object in filteredArray) {
            SightSeenObject *sightObj = [[SightSeenObject alloc]initWithSightSeenObjectDict:object];
            if (sightObj.packageClassId == holiPackageDetailsModel.mainPkgSubClassId){
                [sightObjectsArray addObject:sightObj];
            }
//            if ([sightObjectsDict objectForKey:sightObj.cityName]){
//
//            }
//            [sightObjectsDict setValue:sightObj.cityName forKey:sightObj.name];
        }
        [self.main_tableView reloadData];
    }else{
        NSMutableString *mutString = [[NSMutableString alloc]init];
        self.main_tableView.hidden = YES;
        if (holiPackageDetailsModel.isSightseeingDefaultMsg.length > 2){
            [mutString appendString:[NSString stringWithFormat:@"%@\n",holiPackageDetailsModel.isSightseeingDefaultMsg]];
        }else{
            for (NSDictionary *object in holiPackageDetailsModel.sightseenCollection) {
                NSInteger pkgClasId = [[object objectForKey:@"packageClassId"] integerValue];
                if (pkgClasId == holiPackageDetailsModel.mainPkgSubClassId){
                    [mutString appendString:[NSString stringWithFormat:@"%@\n",object[@"typeDefaultMsg"]]];
                }
            }
        }
        NSString *htmlStringObj = [NSString stringWithFormat:@"<span style=\"font-family: Lato-Regular; font-size: 15\">%@</span>", [mutString copy]];
        [self.webViews loadHTMLString:htmlStringObj baseURL:nil];
    }
    
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)btn_ViewMorePress:(id)sender {
    [self.mainHotelDelegate showHotelWithSectionTitle:(_packageNum==1) ? @"Hotels" : @"Sightseeing" withPackageModel:packageDetailModel];
}

#pragma mark - UITableView DataSource Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSLog(@"filteredArray Count In Cell %lu",(unsigned long)[hotelObjectsArray count]);
    
    if(_packageNum==1){
      return hotelObjectsArray.count;
    }
    return sightObjectsArray.count;
//    return sightObjectsDict.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    
    if(_packageNum==1){
        ChildHotelTableViewCell* cell = (ChildHotelTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"ChildHotelTableViewCell"];
        [cell loadDataInChildFromDetailModel:[hotelObjectsArray objectAtIndex:indexPath.row] withHideAddess:YES];
        return cell;
    }
     SightSeeingTableViewCell* cell = (SightSeeingTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"SightSeeingTableViewCell"];
    [cell loadDSightSeeingFromDetailModel:[sightObjectsArray objectAtIndex:indexPath.row]];
//    [cell loadDSightSeeingFromDetailDict:sightObjectsDict withArray:[[sightObjectsDict allKeys] objectAtIndex:indexPath.row]];
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(_packageNum==1){
      return 95;
    }
    return 60;
}
@end
