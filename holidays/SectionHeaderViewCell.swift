//
//  SectionHeaderViewCell.swift
//  holidays
//
//  Created by Komal Katkade on 11/25/17.
//  Copyright © 2017 Mobicule. All rights reserved.
//

import UIKit
protocol  BuyForexTableViewHeaderDelegate: class {
    func didPressDeleteTravellerButton(_ tag: Int)
}
class SectionHeaderViewCell: UITableViewCell {
   
    @IBOutlet weak var ButtonDelete: UIButton!
    @IBOutlet weak var widthConstraintOfDeleteButton: NSLayoutConstraint!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var labelTraveller: UILabel!
    @IBOutlet weak var labelTravellerAmount: UILabel!
     weak var headerDelegate: BuyForexTableViewHeaderDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func deleteButtonClicked(_ sender: Any) {
       headerDelegate?.didPressDeleteTravellerButton(self.tag)

    }
}
