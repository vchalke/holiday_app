//
//  PassangerDetailCell.h
//  holidays
//
//  Created by ketan on 02/11/15.
//  Copyright © 2015 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RadioButton.h"

@interface PassangerDetailCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UITextField *textFieldDOB;
@property (weak, nonatomic) IBOutlet UITextField *textFieldMealType;
@property (weak, nonatomic) IBOutlet UITextField *textFieldTitle;
@property (weak, nonatomic) IBOutlet RadioButton *buttonMale;
@property (weak, nonatomic) IBOutlet RadioButton *buttonFemale;
@property (weak, nonatomic) IBOutlet UITextField *textFieldNationality;
@property (weak, nonatomic) IBOutlet UITextField *textFieldfirstName;
@property (weak, nonatomic) IBOutlet UITextField *textFieldLastName;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ConstraintMale;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintGender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintFemale;
@property (weak, nonatomic) IBOutlet UILabel *deviderLabel;
@property (weak, nonatomic) IBOutlet UITextField *textFieldAadharNumber;


@end
