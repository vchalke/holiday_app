//
//  CarNotificationouselTableViewCell.h
//  holidays
//
//  Created by darshan on 04/01/19.
//  Copyright © 2019 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CarNotificationouselTableViewCell;

@protocol MyTableCellProtocoll <NSObject>

-(void) didPressButton:(NSURL*)imageURL;

@end

@interface CarNotificationouselTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *NotificationTitle;
@property (weak, nonatomic) IBOutlet UILabel *NotificationMsg;
@property (weak, nonatomic) IBOutlet UILabel *NotificationDate;
@property (weak, nonatomic) IBOutlet UIButton *NextButton;
@property (weak, nonatomic) IBOutlet UIButton *PreviousButton;
@property (strong, nonatomic) NSMutableArray *dataArray;
@property (strong, nonatomic) NSDictionary *dataDict;
@property (strong, nonatomic) NSString *imagedeeplink;
@property (nonatomic) NSInteger count;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewatCL;
@property (weak, nonatomic) IBOutlet UILabel *imgTitle;
@property (weak, nonatomic) IBOutlet UIView *cellView;
@property (weak, nonatomic) IBOutlet UILabel *imgMessage;
@property (nonatomic, weak) id <MyTableCellProtocoll> delegate;
-(void)fetchData;
@end
