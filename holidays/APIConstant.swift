//
//  SoapAPIConstant.swift
//  sotc-consumer-application
//
//  Created by Saurav Kumar on 05/09/17.
//  Copyright © 2017 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit

//
//  SoapAPIConstant.swift
//  sotc-consumer-application
//
//  Created by Saurav Kumar on 05/09/17.
//  Copyright © 2017 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit


struct APIConstants {
    
    
    ////////////////////// SOAP API CONSTANTS //////////////////////////////////////////////////////
    
//    static let EnvelopeAttributes = ["xmlns:xsi" : "http://www.w3.org/2001/XMLSchema-instance", "xmlns:xsd" : "http://www.w3.org/2001/XMLSchema","xmlns:soapenv" : "http://schemas.xmlsoap.org/soap/envelope/" ,"xmlns:act" :"http://action.soto.broadvision.com"]
    static let EnvelopeAttributes = ["xmlns:xsi" : "http://www.w3.org/2001/XMLSchema-instance", "xmlns:xsd" : "http://www.w3.org/2001/XMLSchema","xmlns:soapenv" : "http://schemas.xmlsoap.org/soap/envelope/" ,"xmlns:act" :"http://action.lt.broadvision.com"]// New on 10July
    static let EscapeChars = ["<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"no\"?>" : "" ,"\n":"","\t":""]
    static let ActAttributes = ["soapenv:encodingStyle" : "http://schemas.xmlsoap.org/soap/encoding/"]
    static let RequestAttributes = ["xsi:type" : "soapenc:string" , "xmlns:soapenc":"http://schemas.xmlsoap.org/soap/encoding/"]
    
    static let Envelope =  "soapenv:Envelope"
    static let Header = "soapenv:Header"
    static let Body =  "soapenv:Body"
    static let RequestXML =  "requestXML"
    static let Authentication = "AUTHENTICATION"
    static let Criteria =  "CRITERIA"
    static let UserID =  "USER_ID"
    static let Password =  "PASSWORD"
    static let BookingFileNumber = "BOOKING_FILE_NO"
    static let MobileNumber = "MOBILE_NO"
    static let OptionalDetails =  "OPTIONAL_DETAILS"
    static let ReceiptDetails = "RECEIPT_DETAILS"
    
    static let TransactionID = "TRANSACTION_ID"
    static let BookingFileName = "BOOKING_FILE_NAME"
    static let BookinAmount = "BOOKING_AMOUNT"
    static let ReceiptForFx = "RECEIPT_FOR_FX"
    static let FxCurrency = "FX_CURRENCY"
    static let ROE = "ROE"
    static let FxAmount = "FX_AMOUNT"
    static let A2Attached = "A2_ATTACHED"
    
    static let ActionGetBFNList =  "act:getBFNList"
    static let ActionSetNotificationFlag = "act:setNotificationFlag"
    static let ActionCheckMobileNo = "act:checkMobileNo"
    static let ActionGetBookingDetails = "act:getBookingDetails"
    static let ActionGetBFNOptional = "act:getBFNOptional"
    static let ActionGetLtPortalReciept = "act:getLtPortalReciept"
    static let ActionBookOptional = "act:bookOptional"
    static let ActionBookAdhocDiscount = "act:bookAdhocDiscount"
    
    static let BodyTitleGetBFNList =  "BOOKING_FILE_LIST_REQUEST"
    static let BodyTitleSetNotificationFlag = "SET_NOTIFICATION_REQUEST"
    static let BodyTitleCheckMobileNo = "CHECK_MOBILE_REQUEST"
    static let BodyTitleGetBookingDetails = "BOOKING_DETAILS_REQUEST"
    static let BodyTitleGetBFNOptional = "BOOKING_OPTIONAL_REQUEST"
    static let BodyTitleGetLtPortalReciept = "LT_PORTAL_RECEIPT"
    static let BodyTitleBookOptional = "BOOK_OPTIONAL_REQUEST"
    static let BodyAdhocBookDiscount = "BOOK_ADHOC_REQUEST"
    
    
    static let CheckMobileNumberSOAPAction = "#checkMobileNo"
    
    static let ContentType = "Content-Type"
    static let Authorization = "Authorization"
    static let SOAPAction = "SOAPAction"
    
    
    static let CheckMobileResponseTag = "CHECK_MOBILE_RESPONSE"
    static let ResponseCodeTag = "RESP_CODE"
    static let ResponseDescriptionTag = "RESP_DESC"
    static let  ResponseSuccessCode = "0"
    static let  ResponseAvailable = "AVAILABLE"
    
    ////////////////////// JSON API CONSTANTS //////////////////////////////////////////////////////
    
    
    static let Status = "status"
    static let Success =  "success"
    static let Platform = "platform"
    static let DeviceId = "deviceId"
    static let DeviceModel =  "model"
    static let OSVersion = "osVersion"
    static let MacAddress = "macAddress"
    static let ImeiNumber = "imeiNumber"
    static let Notification_MobileNumber = "mobileNumber"
    static let Client = "client"
    static let AppVersion = "version"
    static let EntityId = "entityId"
    static let EntityType = "type"
    static let EntityAction = "action"
    static let Entity = "entity"
    static let User = "user"
    static let Data = "data"
    static let UniqueId = "uniqueId"
    static let RequestID = "requestId"
    static let SessionId = "sessionId"
    static let USER_VALUE = "mobicule"
    static let QueryParameterMap = "queryParameterMap"
    
    static let LtItineraryCode:String = "ltItineraryCode"
    
    static let CheckVersionKey:String = "checkVersion"
    
    
    static let PlatformType = "ios"
    static let CheckVersionType = "false"
    
}

// Old_One
/*
struct APIResponseConstants {
    
    
    static let BOOKING_FILE_LIST_RESPONSE = "BOOKING_FILE_LIST_RESPONSE"
    static let BOOKING_LIST = "BOOKING_LIST"
    static let BOOKING_FILE_NO = "BOOKING_FILE_NO"
    static let BOOKING_FILE_NAME = "BOOKING_FILE_NAME"
    static let DEPARTURE_DATE = "DEPARTURE_DATE"
    static let ARRIVAL_DATE = "ARRIVAL_DATE"
    static let NO_OF_DAYS = "NO_OF_DAYS"
    static let PROD_ITIN_CODE = "PROD_ITIN_CODE"
    static let PRODUCT_BOOKING_STATUS = "PRODUCT_BOOKING_STATUS"
    static let TOUR_NAME = "TOUR_NAME"
    static let REGION = "REGION"
    static let ZONE = "ZONE"
    static let TOUR_CODE =  "TOUR_CODE"
    static let PAYMENT_FLAG = "PAYMENT_FLAG"
}
 */

// New_One
struct APIResponseConstants {
    
    
    static let BOOKING_FILE_LIST_RESPONSE = "BOOKING_FILE_LIST_RESPONSE"
    static let BOOKING_LIST = "BOOKING_LIST"
    static let BOOKING_FILE_NO = "BOOKING_FILE_NO"
    static let BOOKING_FILE_NAME = "BOOKING_FILE_NAME"
    static let DEPARTURE_DATE = "DEPARTURE_DATE"
    static let ARRIVAL_DATE = "ARRIVAL_DATE"
    static let NO_OF_DAYS = "NO_OF_DAYS"
    static let PROD_ITIN_CODE = "PROD_ITIN_CODE"
    static let PRODUCT_BOOKING_STATUS = "PRODUCT_BOOKING_STATUS"
    static let TOUR_NAME = "TOUR_NAME"
    static let REGION = "REGION"
    static let ZONE = "ZONE"
    static let TOUR_CODE =  "TOUR_CODE"
    static let PAYMENT_FLAG = "PAYMENT_FLAG"
     static let USER_SESSION_ID = "userSession"
    
    struct UserProfile {
        
        static let ADDRESS = "addresses"
        static let EMAIL_ID = "emailId"
        static let FIRST_NAME = "firstName"
        static let LAST_NAME = "lastName"
        static let CONTACT_NUMBER = "mobileNo"
        static let PAN_NUMBER = "panNo"
        static let PASSPORT_INFO = "passportInfo"
        
        static let GENERAL_INFO = "generalInfo"
        
        static let CITY = "city"
        static let STATE = "state"
        
        static let PASSPORT_NUMBER = "passportNo"
       
        
    }

}


public enum ContentType: String {
    
    case ApplicationText = "application/text"
    case ApplicationXML = "application/xml"
    case ApplicationJson = "application/json"
    
}
public enum EntityType: String {
    
    case Transaction = "transaction"
    
}
public enum EntityAction: String {
    
    case Add = "add"
    
    
}






//old Saurav
/*struct APIConstants {
    
    
    ////////////////////// SOAP API CONSTANTS //////////////////////////////////////////////////////
    
    static let EnvelopeAttributes = ["xmlns:xsi" : "http://www.w3.org/2001/XMLSchema-instance", "xmlns:xsd" : "http://www.w3.org/2001/XMLSchema","xmlns:soapenv" : "http://schemas.xmlsoap.org/soap/envelope/" ,"xmlns:act" :"http://action.soto.broadvision.com"]
    static let EscapeChars = ["<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"no\"?>" : "" ,"\n":"","\t":""]
    static let ActAttributes = ["soapenv:encodingStyle" : "http://schemas.xmlsoap.org/soap/encoding/"]
    static let RequestAttributes = ["xsi:type" : "soapenc:string" , "xmlns:soapenc":"http://schemas.xmlsoap.org/soap/encoding/"]
    
    static let Envelope =  "soapenv:Envelope"
    static let Header = "soapenv:Header"
    static let Body =  "soapenv:Body"
    static let RequestXML =  "requestXML"
    static let Authentication = "AUTHENTICATION"
    static let Criteria =  "CRITERIA"
    static let UserID =  "USER_ID"
    static let Password =  "PASSWORD"
    static let BookingFileNumber = "BOOKING_FILE_NO"
    static let MobileNumber = "MOBILE_NO"
    static let OptionalDetails =  "OPTIONAL_DETAILS"
    static let ReceiptDetails = "RECEIPT_DETAILS"
    
    static let TransactionID = "TRANSACTION_ID"
    static let BookingFileName = "BOOKING_FILE_NAME"
    static let BookinAmount = "BOOKING_AMOUNT"
    static let ReceiptForFx = "RECEIPT_FOR_FX"
    static let FxCurrency = "FX_CURRENCY"
    static let ROE = "ROE"
    static let FxAmount = "FX_AMOUNT"
    static let A2Attached = "A2_ATTACHED"
    
    static let ActionGetBFNList =  "act:getBFNList"
    static let ActionSetNotificationFlag = "act:setNotificationFlag"
    static let ActionCheckMobileNo = "act:checkMobileNo"
    static let ActionGetBookingDetails = "act:getBookingDetails"
    static let ActionGetBFNOptional = "act:getBFNOptional"
    static let ActionGetLtPortalReciept = "act:getLtPortalReciept"
    static let ActionBookOptional = "act:bookOptional"
    
    static let BodyTitleGetBFNList =  "BOOKING_FILE_LIST_REQUEST"
    static let BodyTitleSetNotificationFlag = "SET_NOTIFICATION_REQUEST"
    static let BodyTitleCheckMobileNo = "CHECK_MOBILE_REQUEST"
    static let BodyTitleGetBookingDetails = "BOOKING_DETAILS_REQUEST"
    static let BodyTitleGetBFNOptional = "BOOKING_OPTIONAL_REQUEST"
    static let BodyTitleGetLtPortalReciept = "LT_PORTAL_RECEIPT"
    static let BodyTitleBookOptional = "BOOK_OPTIONAL_REQUEST"
    
    
    
    static let CheckMobileNumberSOAPAction = "#checkMobileNo"
    
    static let ContentType = "Content-Type"
    static let Authorization = "Authorization"
    static let SOAPAction = "SOAPAction"
    
    
    static let CheckMobileResponseTag = "CHECK_MOBILE_RESPONSE"
    static let ResponseCodeTag = "RESP_CODE"
    static let ResponseDescriptionTag = "RESP_DESC"
    static let  ResponseSuccessCode = "0"
    static let  ResponseAvailable = "AVAILABLE"
    
    ////////////////////// JSON API CONSTANTS //////////////////////////////////////////////////////

    
    static let Status = "status"
    static let Success =  "success"
    static let Platform = "platform"
    static let DeviceId = "deviceId"
    static let DeviceModel =  "model"
    static let OSVersion = "osVersion"
    static let MacAddress = "macAddress"
    static let ImeiNumber = "imeiNumber"
    static let CheckVersion = "checkVersion"
    static let Client = "client"
    static let AppVersion = "version"
    static let EntityId = "entityId"
    static let EntityType = "type"
    static let EntityAction = "action"
    static let Entity = "entity"
    static let User = "user"
    static let Data = "data"
    static let QueryParameterMap = "queryParameterMap"
    
    
    
    static let PlatformType = "ios"
    static let CheckVersionType = "false"
 
}

struct APIResponseConstants {
    
    
    static let BOOKING_FILE_LIST_RESPONSE = "BOOKING_FILE_LIST_RESPONSE"
    static let BOOKING_LIST = "BOOKING_LIST"
    static let BOOKING_FILE_NO = "BOOKING_FILE_NO"
    static let BOOKING_FILE_NAME = "BOOKING_FILE_NAME"
    static let DEPARTURE_DATE = "DEPARTURE_DATE"
    static let ARRIVAL_DATE = "ARRIVAL_DATE"
    static let NO_OF_DAYS = "NO_OF_DAYS"
    static let PROD_ITIN_CODE = "PROD_ITIN_CODE"
    static let PRODUCT_BOOKING_STATUS = "PRODUCT_BOOKING_STATUS"
    static let TOUR_NAME = "TOUR_NAME"
    static let REGION = "REGION"
    static let ZONE = "ZONE"
    static let TOUR_CODE =  "TOUR_CODE"
    static let PAYMENT_FLAG = "PAYMENT_FLAG"

}



public enum ContentType: String {
    
    case ApplicationText = "application/text"
    case ApplicationXML = "application/xml"
    case ApplicationJson = "application/json"
    
}
public enum EntityType: String {
    
    case Transaction = "transaction"
    
}
public enum EntityAction: String {
    
    case Add = "add"
    
    
}*/



