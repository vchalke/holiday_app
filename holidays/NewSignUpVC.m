//
//  NewSignUpVC.m
//  holidays
//
//  Created by Kush_Team on 28/05/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "NewSignUpVC.h"
#import "UIViewController+MJPopupViewController.h"
#import "NetCoreAnalyticsVC.h"
#import "LandingScreenVC.h"
#import "NetCoreAnalyticsVC.h"
#import "SSKeychain.h"
#import "MobiculeUtilityManager.h"
#import "TopWebVC.h"

#define KEYCHAIN_SERVICE @"Holiday_App_Keychain"
#define KEYCHAIN_ACCOUNT @"com.thomasCookHoliday.app"

#define kEntity @"holidays"
#define kType   @"webservice"
#define kAction  @"search"

@interface NewSignUpVC ()
{
    LoadingView *activityIndicator;
    BOOL isCheckPolicy;
    NSDictionary *dictforEncryption;
    
     BOOL isSecurePassTextOne;
     BOOL isSecurePassTextTwo;
}
@end

@implementation NewSignUpVC

- (void)viewDidLoad {
    [self.navigationController.navigationBar setHidden:YES];
    isCheckPolicy = YES;
    isSecurePassTextOne = YES;
    isSecurePassTextTwo = YES;
    [self disableBackSwipeAllow:NO];
    [self setButtonSelectorsInSignIn];
    [self setToolBarForTextField];
    dictforEncryption = [[NSDictionary alloc]init];
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction:)];
    tapRecognizer.numberOfTapsRequired = 1;
    [self.lbl_privacxyPolicy addGestureRecognizer:tapRecognizer];
}
- (void)viewWillAppear:(BOOL)animated{
    
}
- (void)tapAction:(UITapGestureRecognizer *)tap{
    
}
- (void)viewDidAppear:(BOOL)animated{
    UIBezierPath *maskPath = [UIBezierPath
                              bezierPathWithRoundedRect:self.main_baseBottomViews.bounds
                              byRoundingCorners:(UIRectCornerTopRight | UIRectCornerBottomRight)
                              cornerRadii:CGSizeMake(20, 20)
                              ];
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = self.main_baseBottomViews.bounds;
    maskLayer.path = maskPath.CGPath;
    self.main_baseBottomViews.layer.mask = maskLayer;
    
//    self.txt_name.text = @"jchalke";
//    self.txt_emailId.text = @"jchalke@gmail.com";
//    self.txt_passwords.text = @"jchalke";
//    self.txt_confPass.text = @"jchalke";
//    self.txt_Mobile.text = @"8779756524";
}
-(void)setButtonSelectors{

    
}
-(void)passShowOneClick:(UIButton*)sender{
    isSecurePassTextOne = !isSecurePassTextOne;
    [self.txt_passwords setSecureTextEntry:isSecurePassTextOne];
    [self.btn_PasswordSecureOne setImage:[UIImage imageNamed:(isSecurePassTextOne) ? @"LoginShowHidePassOne" : @"LoginHidePassOne"] forState:UIControlStateNormal];
}
-(void)passShowTwoClick:(UIButton*)sender{
    isSecurePassTextTwo = !isSecurePassTextTwo;
    [self.txt_confPass setSecureTextEntry:isSecurePassTextTwo];
    [self.btn_PasswordSecureTwo setImage:[UIImage imageNamed:(isSecurePassTextTwo) ? @"LoginShowHidePassOne" : @"LoginHidePassOne"] forState:UIControlStateNormal];
}
-(void)jumpToWebViewINLogiin:(NSString *)webUrlString withTitle:(NSString *)webViewTitle{
    TopWebVC *controler = [[TopWebVC alloc] initWithNibName:@"TopWebVC" bundle:nil];
    controler.web_url_string = webUrlString;
    controler.web_title = webViewTitle;
    [self.navigationController pushViewController:controler animated:YES];
}

-(void)setToolBarForTextField{
    UIToolbar* keyboardToolbar = [[UIToolbar alloc] init];
    [keyboardToolbar sizeToFit];
    UIBarButtonItem *flexBarButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                      target:nil action:nil];
    UIBarButtonItem *doneBarButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                      target:self action:@selector(yourTextViewDoneButtonPressed)];
    keyboardToolbar.items = @[flexBarButton, doneBarButton];
    self.txt_Mobile.inputAccessoryView = keyboardToolbar;
}
-(void)yourTextViewDoneButtonPressed{
    [self.txt_Mobile resignFirstResponder];
}
-(void)setButtonSelectorsInSignIn{
    [self.btn_checkPolicy addTarget:self action: @selector(checkPolicyClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.btn_register addTarget:self action: @selector(registerClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.btn_login addTarget:self action: @selector(loginClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.btn_PasswordSecureOne addTarget:self action: @selector(passShowOneClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.btn_PasswordSecureTwo addTarget:self action: @selector(passShowTwoClick:) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - TextField Delegaet

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    return YES;
}

#pragma mark - Button Action Using Selector

-(void)checkPolicyClicked:(id)sender{
    isCheckPolicy = !isCheckPolicy;
    [self.btn_checkPolicy setImage:[UIImage imageNamed:(isCheckPolicy) ? @"rightCheckSelect" : @"rightCheckNoSelect"] forState:UIControlStateNormal];
}

-(void)registerClicked:(id)sender{
    NSString *validString = [self isCheckValidity];
    if ([validString isEqualToString:@""]){
        //         [self showToastsWithTitle:@"Register Successfully"];
        [self loginCheckWithType:@"signUp"];
    }else{
        [self showToastsWithTitle:validString];
    }
}

-(void)loginClicked:(id)sender{
    NewLoginVC *loginUp=[[NewLoginVC alloc]initWithNibName:@"NewLoginVC" bundle:nil];
    [self.navigationController pushViewController:loginUp animated:YES];
}
- (IBAction)btn_BackPress:(id)sender {
        [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)btn_PrivacyPress:(id)sender {
    TopWebVC *controler = [[TopWebVC alloc] initWithNibName:@"TopWebVC" bundle:nil];
//    controler.web_url_string = @"https://www.thomascook.in/privacy-policy#:~:text=We%20do%20not%20use%20your,or%20permitted%20to%20by%20law).&text=We%20will%20only%20process%20(i.e.,a%20legal%20basis%20for%20processing."
    controler.web_url_string = @"https://www.thomascook.in/privacy-policy";
    controler.web_title = @"Privacy Policy";
    [self.navigationController pushViewController:controler animated:YES];
}

-(NSString*)isCheckValidity{
    NSLog(@"txt_emailId %@",self.txt_emailId.text);
    NSLog(@"txt_passwords %@",self.txt_passwords.text);
    NSLog(@"txt_confPass %@",self.txt_confPass.text);
    NSLog(@"txt_Mobile %@",self.txt_Mobile.text);
    
    if ([self.txt_emailId.text length]==0 || [self.txt_passwords.text length]==0 || [self.txt_name.text length]==0 || [self.txt_confPass.text length]==0 || [self.txt_Mobile.text length]==0){
        return @"All Fields Are Mandatory";
    }
    if (![self validateEmailWithString:self.txt_emailId.text]){
        return @"Enter Valid Email Address";
    }
    if ([self.txt_passwords.text length]<7){
        return @"Password contain more than 7 character";
    }
    if (![self.txt_passwords.text isEqualToString:self.txt_confPass.text]){
        return @"Password and Confirm Password must be same";
    }
    if (![self validatePhone:self.txt_Mobile.text]){
        return @"Enter Valid Mobile Number";
    }
    if(!isCheckPolicy){
        return @"Click on Checkbox that you agree terms and Conditions";
    }
    return @"";
}

#pragma mark - SignIn Flow API

-(void)loginCheckWithType:(NSString *)signInType
{
    //abc@test.com/
    activityIndicator = [LoadingView loadingViewInView:self.view.superview.superview
                                            withString:@""
                                     andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
    
    NSLog(@"signInType----> %@",signInType);
    
    @try
    {
        
        NetworkHelper *helper = [NetworkHelper sharedHelper];
        
//        NSString *pathParam = self.txt_passwords.text;
        NSString *pathParam = self.txt_emailId.text;
        NSDictionary *headerDict = [CoreUtility getHeaderDict];
        
        [helper getResponseWithRequestType:@"" withQueryParam:@"" withPathParam:pathParam withJsonParam:nil withHeaders:headerDict withUrl:kAstraUrlLoginCheck success:^(NSDictionary *responseDict)
         {
            NSLog(@"Response Dict-----> : %@",responseDict);
            
            
            dispatch_async(dispatch_get_main_queue(), ^(void)
                           {
                [activityIndicator removeFromSuperview];
                if (responseDict)
                {
                    if (responseDict.count>0)
                    {
                        if ([signInType isEqualToString:@"signIn"] || [signInType isEqualToString:@"signUp"])
                        {
                            [self savePasswordWithType:signInType];
                        }
                    }
                    
                }
            });
            
        }
                                   failure:^(NSError *error)
         {
            dispatch_async(dispatch_get_main_queue(), ^(void)
                           {
                [activityIndicator removeFromSuperview];
                NSLog(@"Response Dict Error---> : %@",[error description]);
            });
        }];
    }
    @catch (NSException *exception)
    {
        NSLog(@"%@", exception.reason);
        [activityIndicator removeFromSuperview];
    }
    @finally
    {
        NSLog(@"exception finally called");
    }
}

-(void)savePasswordWithType:(NSString *)signInType
{
    activityIndicator = [LoadingView loadingViewInView:self.view.superview.superview
                                            withString:@""
                                     andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
    
    NSLog(@"signInType_savePasswordWithType---> %@",signInType);
    @try
    {
        NetworkHelper *helper = [NetworkHelper sharedHelper];
        
        NSDictionary *headerDict = [CoreUtility getHeaderDict];
        
        [helper getResponseWithRequestType:@"" withQueryParam:@"" withPathParam:@"" withJsonParam:nil withHeaders:headerDict withUrl:kAstraUrlSavePass success:^(NSDictionary *responseDict)
         {
            NSLog(@"Response Dict savePasswordWithType---> : %@",responseDict);
            
            dispatch_async(dispatch_get_main_queue(), ^(void)
                           {
                [activityIndicator removeFromSuperview];
                if (responseDict)
                {
                    if (responseDict.count>0)
                    {
                        NSString *keyString = [responseDict valueForKey:@"text"];
                        NSLog(@"keyString---> %@",keyString);
                        
                        [self fetchEncryptionPassWithKey:keyString withloginType:signInType];
                    }
                }
            });
            
        }
                                   failure:^(NSError *error)
         {
            dispatch_async(dispatch_get_main_queue(), ^(void)
                           {
                [activityIndicator removeFromSuperview];
                NSLog(@"Response Dict savePasswordWithType Failure--->  : %@",[error description]);
            });
        }];
    }
    
    @catch (NSException *exception)
    {
        NSLog(@"%@", exception.reason);
        [activityIndicator removeFromSuperview];
    }
    
    @finally
    {
        NSLog(@"exception finally called");
    }
}

-(void)fetchEncryptionPassWithKey:(NSString *)key withloginType:(NSString *)loginType
{
    activityIndicator = [LoadingView loadingViewInView:self.view withString:@"" andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
    
    
    //            "key": "495kcG744o"
    NSLog(@"fetchEncryptionPassWithKey_key %@ withloginType---> %@",key,loginType);
    
    NSDictionary *dict = [[NSDictionary alloc]initWithObjectsAndKeys:key, @"key",self.txt_passwords.text,@"password",nil];
    NSLog(@"dict_fetchEncryptionPassWithKey---> %@",dict);
    
    NetworkHelper *helper = [NetworkHelper sharedHelper];
    if([CoreUtility connected])
    {
        NSLog(@"in_CoreUtility");
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            NSString *strResponse = [helper getDataFromServerForType:@"sync" entity:@"encrypt" action:@"generate" andUserJson:dict];
            NSLog(@"strResponse---> %@",strResponse);
            
            dispatch_async(dispatch_get_main_queue(), ^(void)
                           {
                [activityIndicator removeFromSuperview];
                
                NSLog(@"in_CoreUtility---->");
                if (![strResponse isEqualToString:@"No Internet"])
                {
                    NSLog(@"No Internet---->");
                    NSString *strStatus = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"Status"];
                    NSString *strMessage = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"message"];
                    
                    if ([[strStatus lowercaseString] isEqualToString:kStatusSuccess])
                    {
                        
                        NSDictionary *dataDict = [[[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"data"] firstObject];
                        NSLog(@"dataDict----> %@",dataDict);
                        dictforEncryption = dataDict;
                        [self createNewUser];
                        //                        RegisterUserViewController *registerUserVC = [[RegisterUserViewController alloc] initWithNibName:@"RegisterUserViewController" bundle:nil];
                        //                        registerUserVC.dictforEncryption = dataDict;
                        //                        registerUserVC.userId = self.txt_emailId.text;
                        //                        registerUserVC.delegate = self;
                        //                        registerUserVC.signUpVC = @"signUpVC";
                        //                        [self presentViewController:registerUserVC animated:YES completion:nil];
                    }
                    else
                    {
                        [self showButtonsInAlertWithTitle:@"Alert" msg:strMessage style:UIAlertControllerStyleAlert buttArray:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString * _Nonnull strValue) {
                            if ([strValue isEqualToString:@"Ok"]){
                                
                            }
                        }];
                    }
                }
            });
        });
        
    }
    else
    {
        [self showButtonsInAlertWithTitle:@"Alert" msg:kMessageNoInternet style:UIAlertControllerStyleAlert buttArray:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString * _Nonnull strValue) {
            if ([strValue isEqualToString:@"Ok"]){
                
            }
        }];
        
    }
}

-(void)dismissWithSuccessFullRegistration
{
    NSLog(@"dismissWithSuccessFullRegistration");
    [self netCoreDataForLoginWithEmailID:self.txt_emailId.text];  //09-04-2018
    [self setUserDetailsAndClosePopUp];
}


-(void)netCoreDataForLoginWithEmailID:(NSString *)emailID
{
    
    NSMutableDictionary *payloadList =  [NSMutableDictionary dictionary];
    [payloadList setObject:emailID forKey:@"EMAIL"];
    
    [NetCoreAnalyticsVC getNetCorePushNotificationDetailDictionary:payloadList withPayloadCount:102];
}

-(void)setUserDetailsAndClosePopUp
{
    [[NSUserDefaults standardUserDefaults]setValue:self.txt_emailId.text forKey:kuserDefaultUserId];
    [[NSUserDefaults standardUserDefaults]setValue:self.txt_passwords.text forKey:kuserDefaultPassword];
    [[NSUserDefaults standardUserDefaults]setValue:self.txt_emailId.text forKey:kLoginEmailId];
    [[NSUserDefaults standardUserDefaults]setValue:self.txt_passwords.text forKey:kLoginPasswords];
    
    NSUserDefaults *LoginStatus=[NSUserDefaults standardUserDefaults];
    [LoginStatus setObject:kLoginSuccess  forKey:kLoginStatus];
    
    NSUserDefaults *userIdForGooglePlusSignIn=[NSUserDefaults standardUserDefaults];
    [userIdForGooglePlusSignIn setObject:self.txt_emailId.text forKey:kLoginEmailId];
    NSString *userId=[userIdForGooglePlusSignIn valueForKey:kLoginEmailId];
    NSLog(@"userId---> %@",userId);
    
    //    [self.delegate userloggedInSignUpSuccessFullWithUserDict:userId];
    
    [self dismissViewControllerAnimated:NO completion:nil];
}

-(void)createNewUser
{
    
    activityIndicator = [LoadingView loadingViewInView:self.view.superview.superview
                                            withString:@""
                                     andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
    
    @try
    {
        
        NSString *text1 = [dictforEncryption valueForKey:@"text1"];
        NSString *text2 = [dictforEncryption valueForKey:@"text2"];
        NSString *text3 = [dictforEncryption valueForKey:@"text3"];
        
        
        NetworkHelper *helper = [NetworkHelper sharedHelper];
        
        NSMutableDictionary *dictForRequest = [[NSMutableDictionary alloc] init];
        [dictForRequest setObject:self.txt_name.text forKey:@"firstName"];
        [dictForRequest setObject:@"" forKey:@"lastName"];
        [dictForRequest setObject:self.txt_Mobile.text forKey:@"mobileNo"];
        [dictForRequest setObject:@"" forKey:@"title"];
        [dictForRequest setObject:self.txt_emailId.text forKey:@"userId"];
        [dictForRequest setObject:text1 forKey:@"text1"];
        [dictForRequest setObject:text2 forKey:@"text2"];
        [dictForRequest setObject:text3 forKey:@"text3"];
        
        
        NSDictionary *headerDict = [CoreUtility getHeaderDict];
        
        [helper getResponseWithRequestType:@"POST" withQueryParam:@"" withPathParam:@"" withJsonParam:dictForRequest withHeaders:headerDict withUrl:kAstraCreateLogin success:^(NSDictionary *responseDict)
         {
            NSLog(@"Response Dict : %@",responseDict);
            
            dispatch_async(dispatch_get_main_queue(), ^(void)
                           {
                [activityIndicator removeFromSuperview];
                if (responseDict)
                {
                    if (responseDict.count>0)
                    {
                        
                        if ([[[responseDict valueForKey:@"message"] lowercaseString] isEqualToString:@"false"])
                        {
                            NSString *reasonOfMessage = [responseDict valueForKey:@"reasonOfMessage"];
                            [self showButtonsInAlertWithTitle:@"Alert" msg:reasonOfMessage style:UIAlertControllerStyleAlert buttArray:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString * _Nonnull strValue) {
                                if ([strValue isEqualToString:@"Ok"]){
                                    
                                }
                            }];
                            
                        }
                        else
                        {
                            
                            /*
                             {"accountType":"TC","familyTreeList":[],"invalidLoginCount":0,"message":"true","reasonOfMessage":"Account created","userAddressList":[],"userDetail":{"accountType":"TC","age":0,"custId":"1309","email":"ketanbswami@gmail.com","fname":"ketan","lName":"swami","mobileNo":"9858585458","role":"B2C Customer","roleId":2,"userId":"ketanbswami@gmail.com","userTypeId":"Customer"}}
                             */
                            NSDictionary * userDetailsDict = [responseDict valueForKey:@"userDetail"]; //09-04-2018 --
                            
                            if( userDetailsDict && ![userDetailsDict isEqual:[NSNull new]]) //09-04-2018 --
                            {
                                NSString * mobileNoString =[userDetailsDict valueForKey:@"mobileNo"];
                                if (nil != mobileNoString && ![mobileNoString isEqual:[NSNull new]] && ![mobileNoString isEqualToString:@""])
                                {
                                    [[NSUserDefaults standardUserDefaults]setValue:mobileNoString forKey:kuserDefaultTC_MobileNumber]; //09-04-2018 --
                                    [[NSUserDefaults standardUserDefaults]setValue:userDetailsDict forKey:kLoginUserDetails];
                                    [[NSUserDefaults standardUserDefaults]setValue:[userDetailsDict valueForKey:@"fname"] forKey:kUserFirstName];
//                                    [[NSUserDefaults standardUserDefaults]setValue:mobileNoString forKey:@"mobicleNumber"];
                                    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"isCSSUserLoggedIn"];
                                    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"isRegisterForCSSPushNotification"];
                                    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"isCSSSubscribeForPushNotification"];
                                    [[NSUserDefaults standardUserDefaults] synchronize];
                                    //                                       [[NetCoreSharedManager sharedInstance] setUpIdentity:mobileNoString]; //24-04-2018 --
                                }
                            }
                            CoreUtility * coreObj=[CoreUtility sharedclassname];
                            [coreObj.strPhoneNumber addObject:self.txt_Mobile.text];
                            [self showButtonsInAlertWithTitle:@"Success" msg:@"User Registered Successfully" style:UIAlertControllerStyleAlert buttArray:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString * _Nonnull strValue) {
                                if ([strValue isEqualToString:@"Ok"]){
                                    [self dismissWithSuccessFullRegistration];
                                    LandingScreenVC *landing=[[LandingScreenVC alloc]initWithNibName:@"LandingScreenVC" bundle:nil];
                                    [self.navigationController pushViewController:landing animated:YES];
                                }
                            }];
                        }
                    }
                }
            });
            
        }
                                   failure:^(NSError *error)
         {
            dispatch_async(dispatch_get_main_queue(), ^(void)
                           {
                [activityIndicator removeFromSuperview];
                NSLog(@"Response Dict : %@",[error description]);
            });
        }];
    }
    @catch (NSException *exception)
    {
        NSLog(@"%@", exception.reason);
        [activityIndicator removeFromSuperview];
    }
    @finally
    {
        NSLog(@"exception finally called");
    }
    
    
}

/*
 #pragma mark - Login Flow API
 
 -(void)loginCheckWithTypeInSign:(NSString *)signInType
 {
 //abc@test.com/
 activityLoadingView = [LoadingView loadingViewInView:self.view.superview.superview
 withString:@""
 andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
 
 @try
 {
 NetworkHelper *helper = [NetworkHelper sharedHelper];
 NSString *pathParam = self.txt_emailId.text;
 NSDictionary *headerDict = [CoreUtility getHeaderDict];
 [helper getResponseWithRequestType:@"" withQueryParam:@"" withPathParam:pathParam withJsonParam:nil withHeaders:headerDict withUrl:kAstraUrlLoginCheck success:^(NSDictionary *responseDict)
 {
 NSLog(@"Response Dict : %@",responseDict);
 dispatch_async(dispatch_get_main_queue(), ^(void)
 {
 [activityLoadingView removeFromSuperview];
 if (responseDict)
 {
 if (responseDict.count>0)
 {
 if ([signInType isEqualToString:@"signIn"] || [signInType isEqualToString:@"signUp"]) {
 [self savePasswordWithType:signInType withEmail:self.txt_emailId.text];
 }
 }
 }
 });
 }
 failure:^(NSError *error)
 {
 dispatch_async(dispatch_get_main_queue(), ^(void)
 {
 [activityLoadingView removeFromSuperview];
 NSLog(@"Response Dict : %@",[error description]);
 });
 }];
 }
 @catch (NSException *exception)
 {
 NSLog(@"%@", exception.reason);
 [activityLoadingView removeFromSuperview];
 }
 @finally
 {
 NSLog(@"exception finally called");
 [activityLoadingView removeFromSuperview];
 }
 }
 
 -(void)savePasswordWithType:(NSString *)signInType withEmail:(NSString*)emails{
 
 activityLoadingView = [LoadingView loadingViewInView:self.view.superview.superview
 withString:@""
 andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
 
 @try
 {
 NetworkHelper *helper = [NetworkHelper sharedHelper];
 NSDictionary *headerDict = [CoreUtility getHeaderDict];
 [helper getResponseWithRequestType:@"" withQueryParam:@"" withPathParam:@"" withJsonParam:nil withHeaders:headerDict withUrl:kAstraUrlSavePass success:^(NSDictionary *responseDict)
 {
 NSLog(@"Response Dict : %@",responseDict);
 dispatch_async(dispatch_get_main_queue(), ^(void) {
 [activityLoadingView removeFromSuperview];
 
 if (responseDict){
 if (responseDict.count>0){
 NSString *keyString = [responseDict valueForKey:@"text"];
 [self fetchEncryptionPassWithKey:keyString withloginType:signInType withEmail:emails];
 }
 }
 });
 }
 failure:^(NSError *error)
 {
 dispatch_async(dispatch_get_main_queue(), ^(void)
 {
 [activityLoadingView removeFromSuperview];
 NSLog(@"Response Dict : %@",[error description]);
 });
 }];
 }
 @catch (NSException *exception)
 {
 NSLog(@"%@", exception.reason);
 [activityLoadingView removeFromSuperview];
 }
 @finally
 {
 NSLog(@"exception finally called");
 [activityLoadingView removeFromSuperview];
 }
 
 }
 
 -(void)fetchEncryptionPassWithKey:(NSString *)key withloginType:(NSString *)loginType withEmail:(NSString*)emailID{
 activityLoadingView = [LoadingView loadingViewInView:self.view withString:@"" andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
 //            "key": "495kcG744o"
 NSDictionary *dict = [[NSDictionary alloc]initWithObjectsAndKeys:key, @"key",self.txt_password.text,@"password",nil];
 
 NetworkHelper *helper = [NetworkHelper sharedHelper];
 if([CoreUtility connected])
 {
 
 dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
 
 NSString *strResponse = [helper getDataFromServerForType:@"sync" entity:@"encrypt" action:@"generate" andUserJson:dict];
 dispatch_async(dispatch_get_main_queue(), ^(void) {
 [activityLoadingView removeFromSuperview];
 if (![strResponse isEqualToString:@"No Internet"]){
 NSString *strStatus = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"Status"];
 NSString *strMessage = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"message"];
 
 if ([[strStatus lowercaseString] isEqualToString:kStatusSuccess]){
 
 NSDictionary *dataDict = [[[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"data"] firstObject];
 
 if ([loginType isEqualToString:@"signIn"]){
 NSString *text1 = [dataDict valueForKey:@"text1"];
 NSString *text2 = [dataDict valueForKey:@"text2"];
 NSString *text3 = [dataDict valueForKey:@"text3"];
 
 [self verifyCredentialsWithText1:text1 withText2:text2 withText3:text3 withloginType:loginType withOtp:@"" withEmail:emailID];
 
 }else{
 // Jump To register View Controller
 }
 }
 else{
 [self showButtonsInAlertWithTitle:@"Alert" msg:strMessage style:UIAlertControllerStyleAlert buttArray:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString * _Nonnull strValue) {
 
 }];
 }
 }
 });
 });
 
 }
 else
 {
 [activityLoadingView removeFromSuperview];
 [self showButtonsInAlertWithTitle:@"Alert" msg:kMessageNoInternet style:UIAlertControllerStyleAlert buttArray:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString * _Nonnull strValue) {
 
 }];
 }
 
 }
 
 
 -(void)verifyCredentialsWithText1:(NSString *)text1 withText2:(NSString *)text2 withText3:(NSString *)text3 withloginType:(NSString *)loginType withOtp:(NSString*)otp withEmail:(NSString*)emailId {
 
 activityLoadingView = [LoadingView loadingViewInView:self.view.superview.superview
 withString:@""
 andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
 
 @try
 {
 
 NetworkHelper *helper = [NetworkHelper sharedHelper];
 
 
 
 NSMutableDictionary *dictForRequest = [[NSMutableDictionary alloc] init];
 [dictForRequest setObject:emailId forKey:@"userId"];
 [dictForRequest setObject:text1 forKey:@"text1"];
 [dictForRequest setObject:text2 forKey:@"text2"];
 [dictForRequest setObject:text3 forKey:@"text3"];
 
 if ([loginType caseInsensitiveCompare:kSignInWithOtp] == NSOrderedSame)
 {
 [dictForRequest setObject:otp forKey:@"otp"];
 
 }
 
 NSDictionary *headerDict = [CoreUtility getHeaderDict];
 
 [helper getResponseWithRequestType:@"POST" withQueryParam:@"" withPathParam:@"" withJsonParam:dictForRequest withHeaders:headerDict withUrl:kAstraVerifyCredentials success:^(NSDictionary *responseDict)
 {
 NSLog(@"Response Dict : %@",responseDict);
 
 
 dispatch_async(dispatch_get_main_queue(), ^(void) {
 [activityLoadingView removeFromSuperview];
 if (responseDict)
 {
 if (responseDict.count>0)
 {
 if ([[[responseDict valueForKey:@"message"] lowercaseString] isEqualToString:@"false"])
 {
 NSString *reasonOfMessage = [responseDict valueForKey:@"reasonOfMessage"];
 
 [self showButtonsInAlertWithTitle:@"Alert" msg:reasonOfMessage style:UIAlertControllerStyleAlert buttArray:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString * _Nonnull strValue) {
 if ([strValue isEqualToString:@"Ok"]){
 [self setUserDetailsAndClosePopUp];
 }
 }];
 
 }
 else{
 [self netCoreDataForLoginWithEmailID:emailId]; //09-04-2018
 
 NSDictionary * userDetailsDict = [responseDict valueForKey:@"userDetail"]; //09-04-2018 --
 
 if( userDetailsDict && ![userDetailsDict isEqual:[NSNull new]]) //09-04-2018 --
 {
 NSString * mobileNoString =[userDetailsDict valueForKey:@"mobileNo"];
 
 if (nil != mobileNoString && ![mobileNoString isEqual:[NSNull new]] && ![mobileNoString isEqualToString:@""])
 {
 
 [[NSUserDefaults standardUserDefaults]setValue:mobileNoString forKey:kuserDefaultTC_MobileNumber]; //09-04-2018 --
 
 [[NSUserDefaults standardUserDefaults] synchronize];
 
 //                                       [[NetCoreSharedManager sharedInstance] setUpIdentity:mobileNoString]; //24-04-2018 --
 }
 }
 
 [self showButtonsInAlertWithTitle:@"Alert" msg:@"Login successful" style:UIAlertControllerStyleAlert buttArray:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString * _Nonnull strValue) {
 if ([strValue isEqualToString:@"Ok"]){
 [self setUserDetailsAndClosePopUp];
 LandingScreenVC *landing=[[LandingScreenVC alloc]initWithNibName:@"LandingScreenVC" bundle:nil];
 [self.navigationController pushViewController:landing animated:YES];
 }
 }];
 
 
 }
 }
 }
 });
 
 }
 failure:^(NSError *error)
 {
 dispatch_async(dispatch_get_main_queue(), ^(void)
 {
 [activityLoadingView removeFromSuperview];
 NSLog(@"Response Dict : %@",[error description]);
 });
 
 
 
 }];
 
 }
 @catch (NSException *exception)
 {
 NSLog(@"%@", exception.reason);
 [activityLoadingView removeFromSuperview];
 }
 @finally
 {
 NSLog(@"exception finally called");
 [activityLoadingView removeFromSuperview];
 }
 }
 
 #pragma mark - After Calling API save Value in Userdefaults
 
 -(void)setUserDetailsAndClosePopUp
 {
 [[NSUserDefaults standardUserDefaults]setValue:self.txt_emailId.text forKey:kuserDefaultUserId];
 [[NSUserDefaults standardUserDefaults]setValue:self.txt_passwords.text forKey:kuserDefaultPassword];
 NSUserDefaults *LoginStatus=[NSUserDefaults standardUserDefaults];
 [LoginStatus setObject:kLoginSuccess  forKey:kLoginStatus];
 NSUserDefaults *userIdForGooglePlusSignIn=[NSUserDefaults standardUserDefaults];
 
 [userIdForGooglePlusSignIn setObject:self.txt_emailId.text forKey:kLoginEmailId];
 
 //  [[NetCoreSharedManager sharedInstance] setUpIdentity:self.textFieldUserId.text];
 
 //[self fetchMobileNumber];
 
 [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
 
 }
 */
@end
