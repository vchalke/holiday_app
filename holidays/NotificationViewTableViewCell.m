//
//  NotificationViewTableViewCell.m
//  holidays
//
//  Created by darshan on 29/11/18.
//  Copyright © 2018 Mobicule. All rights reserved.
//

#import "NotificationViewTableViewCell.h"

@implementation NotificationViewTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    _cellView.layer.cornerRadius = 10;
    // Configure the view for the selected state
}

@end
