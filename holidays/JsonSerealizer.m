//
//  JsonSerealizer.m
//  holidays
//
//  Created by vaibhav on 16/09/15.
//  Copyright (c) 2015 Mobicule. All rights reserved.
//

#import "JsonSerealizer.h"

@implementation JsonSerealizer


+(NSString *)stringRepresenationFromJSonDataObject:(id)dataObject
{
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dataObject
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    return jsonString;
}


+(NSDictionary *)dictonaryOfJsonFromJsonData:(id)dataObject
{
    NSError *error;
    NSData *data;
    
    if ([dataObject isKindOfClass:[NSString class]])
    {
        data =[dataObject dataUsingEncoding:NSASCIIStringEncoding];
    }
    else
    {
        data = dataObject;
    }
    NSDictionary *jsonDict;
    if(data != nil){
    jsonDict = [NSJSONSerialization JSONObjectWithData:data
                                                       options:0
                                                         error:&error];
    }
    return jsonDict;
}


+(NSArray *)arrayOfJSonFromJsonData:(id)dataObject
{
    NSError *error;
    
    NSData *data;
    
    if ([dataObject isKindOfClass:[NSString class]])
    {
        data =[dataObject dataUsingEncoding:NSASCIIStringEncoding];
    }
    else 
    {
        data = [[self stringRepresenationFromJSonDataObject:dataObject]dataUsingEncoding:NSASCIIStringEncoding];
    }
    
    NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:data
                                                         options:0
                                                           error:&error];
    
    return jsonArray;
}
@end
