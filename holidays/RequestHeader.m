//
//  HeaderConfiguration.m
//  mobicule-device-network-layer
//
//  Created by Kishan Gupta on 04/12/15.
//  Copyright (c) 2015 Mobicule. All rights reserved.
//

#import "RequestHeader.h"


@implementation RequestHeader


- (instancetype)init {
    
    self = [super init];
    
    if (self) {
        
        headerFields = [[NSMutableDictionary alloc]init];
        _requestBody = nil;
    }
    return self;
}

- (void)addHeaderFields:(NSString*)value withKey:(NSString*)key {
    
    [headerFields setValue:value forKey:key];
    
}

- (void)setHeaderValueForFieldAccept:(NSString*)value {
    
    [headerFields setValue:value forKey:ACCEPT];

}

- (void)setHeaderValueForFieldContentType:(NSString*)value {
    
    [headerFields setValue:value forKey:CONTENT_TYPE];

}

- (void)setHeaderValueForFieldAuthorization:(NSString*)value {

    [headerFields setValue:value forKey:AUTHORIZATION];

}

- (void)setRequestBody:(NSString*)requestBody  {
    
    _requestBody = requestBody;
}

- (NSString*)getRequestBody {
    
    return _requestBody;
}
-(NSDictionary*)getHeaderFields {
 
    return headerFields;
}


@end
