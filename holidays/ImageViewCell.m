//
//  ImageViewCell.m
//  holidays
//
//  Created by Designer on 06/01/16.
//  Copyright © 2016 Mobicule. All rights reserved.
//

#import "ImageViewCell.h"

@implementation ImageViewCell

@synthesize imageView;
@synthesize selectorView;

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
