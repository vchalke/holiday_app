//
//  TourPackageImages+CoreDataProperties.swift
//  holidays
//
//  Created by Saurav on 28/11/17.
//  Copyright © 2017 Mobicule. All rights reserved.
//

import Foundation
import CoreData


extension TourPackageImages {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<TourPackageImages> {
        return NSFetchRequest<TourPackageImages>(entityName: "TourPackageImages")
    }

    @NSManaged public var entityType: String?
    @NSManaged public var imageId: String?
    @NSManaged public var imageId_packageId_prodITINCode: String?
    @NSManaged public var imageOrder: String?
    @NSManaged public var itineraryImageTitle: String?
    @NSManaged public var packageId: String?
    @NSManaged public var path: String?
    @NSManaged public var prodITINCode: String?
    @NSManaged public var tourRelation: Tour?

}
