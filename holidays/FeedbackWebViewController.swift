//
//  FeedbackWebViewController.swift
//  sotc-consumer-application
//
//  Created by Saurav Kumar on 04/11/17.
//  Copyright © 2017 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit

class FeedbackWebViewController: UIViewController {

    @IBOutlet var webView: UIWebView!
    
    var redirectionURL:String = ""
    var moduleTitle:String = ""
    
    var isfromNotificationHomeVc:Bool = false
   
    @IBOutlet weak var heKitDisclaimerHeightConstraints: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        LoadingIndicatorView.show("Loading")
      
        self.title = moduleTitle
        
        self.createWebViewRequest()
        
       
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setRedNavigationBar()
        self.navigationController?.setnavigatiobBarTitle(title: moduleTitle, viewController: self)
       
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        if self.navigationController?.viewControllers.firstIndex(of: self) == nil && self.isfromNotificationHomeVc
        {
            SlideNavigationController.sharedInstance().popViewController(animated: true)
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

 
    func webViewDidStartLoad(_ webView: UIWebView) {
         
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error)
    {
        LoadingIndicatorView.hide()
        
        if self.moduleTitle == Title.PAYMENT
        {
            
        }else{
        
        let alert = UIAlertController(title: "Alert", message: "Failed to load requested URL.Please try again!", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: AlertMessage.TITLE_OK, style: UIAlertAction.Style.default, handler: { (UIAlertAction) in
            
            self.navigationController?.popViewController(animated: true)
            
        }))
        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
        }
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView?
    {
        return self.webView
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView)
    {
        LoadingIndicatorView.hide()
    }
    
    func createWebViewRequest() {
    
        var request = URLRequest(url: URL(string: redirectionURL)!)
        
    self.webView.scrollView.bounces = false
        
        switch self.moduleTitle {
        case Title.PAYMENT:
            
            let authorizationString:String = AuthenticationConstant.TC_CSS_PAYMENT_AUTH_USERNAME + ":" + AuthenticationConstant.TC_CSS_PAYMENT_AUTH_PASS
            let authBase64 = TourDocumentController.encodeBase64(value: authorizationString)
             request.addValue("Basic \(authBase64)", forHTTPHeaderField: "Authorization")
            webView.loadRequest(request)
            break
            
        case Title.HOLIDAY_KIT:
            
            
          /*  var htmlString = String(format:"<html><body><img src='%@'",redirectionURL)
            htmlString = htmlString + " width='300%' height='900%'></body></html>"
                
                webView.scalesPageToFit = true
                webView.loadHTMLString(htmlString, baseURL: nil)*/
            
            self.heKitDisclaimerHeightConstraints.constant = 42
            
            let htmlString:String = "<html><body><img src='\(redirectionURL)' width='100%' height='100%'></body></html>";
            
            webView.loadHTMLString(htmlString, baseURL: nil)
            
            
            break;
            
        default:
            printLog("Default request")
            webView.loadRequest(request)
            
        }
        
        
        

    }

}
