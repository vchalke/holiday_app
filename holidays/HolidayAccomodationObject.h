//
//  HolidayAccomodationObject.h
//  holidays
//
//  Created by Kush_Tech on 06/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HolidayAccomodationObject : NSObject
-(instancetype)initWithAccomodationDict:(NSDictionary *)dictionary;

@property(nonatomic,strong)NSString *hotelName;
@property(nonatomic,strong)NSString *cityName;
@property(nonatomic,strong)NSString *address;
@property(nonatomic,strong)NSDictionary *accomodationHotelId;
@property(nonatomic,strong)NSDictionary *accomodationOtherId;
@property(nonatomic,strong)NSString *accomodationId;
@property(nonatomic,strong)NSString *country;
@property(nonatomic,strong)NSString *state;
@property(nonatomic)NSInteger accomodationTypeId;
@property (nonatomic) BOOL isActive;
@property(nonatomic)NSInteger noOfNights;
@property(nonatomic)NSInteger packageClassId;
@property(nonatomic)NSInteger position;
@property(nonatomic)NSInteger starRating;

@end

NS_ASSUME_NONNULL_END
