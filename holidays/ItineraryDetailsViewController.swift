//
//  ItineraryDetailsViewController.swift
//  sotc-consumer-application
//
//  Created by Parshwanath on 12/08/17.
//  Copyright © 2017 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit

class ItineraryDetailsViewController: UIViewController {

    
    @IBOutlet var cityNameLabel: UILabel!
    @IBOutlet var detailView: UIView!
    @IBOutlet var itineraryImagesContainner: UIView!
    @IBOutlet var itineraryDescriptionWebView: UIWebView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var slideshow: ImageSlideshow = ImageSlideshow()
    
    weak var parentController : UIViewController?
    var tourDetails:Tour?
    var itinearyImages:Array<ItineraryImages>?
    var itienaryDetail:ItineraryDetail?
    var filePath:String = ""
    
   
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
     LoadingIndicatorView.show("Loading")

        // Do any additional setup after loading the view.
        
   
        if let cityName:String = itienaryDetail?.cityName {
            
            cityNameLabel.text = cityName
        }
        
        if let ItineraryDescription:String = String(data: (itienaryDetail?.itineraryDescription!)! as Data, encoding: .utf8)
        {
              itineraryDescriptionWebView.loadHTMLString(ItineraryDescription , baseURL: URL(string: ""))
        }
        
  
      
     slideshow.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 210)
     itineraryImagesContainner.addSubview(slideshow)
    detailView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
    self.scrollView.addSubview(detailView)
     self.scrollView.contentSize = CGSize(width:  UIScreen.main.bounds.width, height:  UIScreen.main.bounds.height)
        
        self.downloadImages()
        
        
       itineraryDescriptionWebView.scrollView.isScrollEnabled = false
      
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func webViewDidStartLoad(_ webView: UIWebView) {
     
 
        
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error)
    {
               LoadingIndicatorView.hide()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView)
    {
         LoadingIndicatorView.hide()
        
        let height = webView.scrollView.contentSize.height
        
       
        
        detailView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: height + 300 )
        self.scrollView.contentSize = CGSize(width: 320, height: height + 300)
    }

  private func setHeaderImages(imageArray:Array<ImageSource>)  {
    
    
            slideshow.backgroundColor = UIColor.white
            slideshow.slideshowInterval = 5.0
            slideshow.pageControlPosition = PageControlPosition.insideScrollView
            slideshow.pageControl.currentPageIndicatorTintColor = UIColor.lightGray
            slideshow.pageControl.pageIndicatorTintColor = UIColor.black
            slideshow.contentScaleMode = UIView.ContentMode.scaleToFill
    
            // optional way to show activity indicator during image load (skipping the line will show no activity indicator)
            slideshow.activityIndicator = DefaultActivityIndicator()
            slideshow.currentPageChanged = { page in
                printLog("Itienary current page:", page)
            }
    
            slideshow.setImageInputs(imageArray)
    
            let recognizer = UITapGestureRecognizer(target: self, action: #selector(ItineraryDetailsViewController.itineraryImageHeaderDidTap))
            slideshow.addGestureRecognizer(recognizer)
        }
    
        @objc func itineraryImageHeaderDidTap() {
    
            let fullScreenController = slideshow.presentFullScreenController(from: self)
            // set the activity indicator for full screen controller (skipping the line will show no activity indicator)
    
            fullScreenController.slideshow.activityIndicator = DefaultActivityIndicator(style: .white, color: nil)
            
        }
    
    func downloadImages()
    {
         
        if(itinearyImages != nil &&  (itinearyImages?.count)! > 0)
        {
            
            if let itinearyImage:ItineraryImages = itinearyImages?.first!
            {
                
                let packageId:String = itinearyImage.packageId!
                
                let imageId:String = itinearyImage.imageId!
                
                let itineraryDay:String = itinearyImage.imageOrder!
                
                let itineraryImageTitle:String = itinearyImage.itineraryImageTitle!
                
                
                let docummentURL =  packageId + "/" + imageId + "/" + itineraryDay + "/" + itineraryImageTitle
                
                if let bfnNumber = tourDetails?.bfNumber
                {
                    let tourDocumnet = TourDocumentController.getDocumentFilePath(url:docummentURL , bfnNumber: bfnNumber, moduleFolder: Title.ITINERARY)
                    
                    
                    let isFilePresent = TourDocumentController.isFileExistInDocumentDirectory(fileNamePath: tourDocumnet)
                    
                    if(isFilePresent.chkFile)
                    {
                     
                        if let imageSource:ImageSource =  ImageSource(imageString:tourDocumnet)
                        {
                        
                            let imgArray:Array<ImageSource> = [imageSource]
                            
                              self.setHeaderImages(imageArray: imgArray)
                        }else{
                            let  imgArray = [ImageSource(image: #imageLiteral(resourceName: "defaultImage"))]
                            
                            self.setHeaderImages(imageArray: imgArray)
                        }
                        
                    }else{
                        
                        NetworkCommunication.downloadDocuments(urlString: itinearyImage.path!, destinationFilePath: tourDocumnet, completion: { (filePath, status) in
                            
                            printLog(status,filePath);
                            
                            if(status)
                            {
                                
                                if let imageSource:ImageSource =  ImageSource(imageString:filePath)
                                {
                                let imgArray:Array<ImageSource> = [imageSource]
                                
                                self.setHeaderImages(imageArray: imgArray)
                                }else{
                                    
                                    let  imgArray = [ImageSource(image: #imageLiteral(resourceName: "defaultImage"))]
                                    
                                    self.setHeaderImages(imageArray: imgArray)
                                }
                                
                            }
                            else{
                                
                                let  imgArray = [ImageSource(image: #imageLiteral(resourceName: "defaultImage"))]
                                
                                 self.setHeaderImages(imageArray: imgArray)
                                
                            }
                            
                        })
                        
                    }
                    
                  
                }
                
            }
            
            
            
        }
        
        
        
        
    }

}
