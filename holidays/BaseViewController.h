//
//  BaseViewController.h
//  genie
//
//  Created by ketan on 01/04/15.
//  Copyright (c) 2015 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideNavigationController.h"
@protocol dismissView;
@interface BaseViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *MenuView;
@property BOOL slideMenuBool;
@property (weak, nonatomic) IBOutlet UIImageView *headerImage;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIImageView *menuBarImageView;
@property (weak, nonatomic) IBOutlet UIButton *sliderMenuButton;
@property (weak, nonatomic) IBOutlet UILabel *HeaderLabel;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property(nonatomic,assign) BOOL backBtnFromOverview;
@property id tabMenuVController;
- (UIColor *)colorWithHex:(UInt32)hex A:(CGFloat)alpha;

@property (weak, nonatomic) IBOutlet UIImageView *backIconImage;


- (IBAction)onSlideMenuButtonClicked:(id)sender;

-(void)setHeaderTitle:(NSString *)title;
-(void)addViewInBaseView:(UIView *)subView;
-(void)hideMenuBar;
-(void)hideMenuButton;
-(void)hideBackButton:(BOOL)hide;
-(BOOL)connected;
-(void)showAlertViewWithTitle:(NSString *)title withMessage:(NSString *)message;
-(void)showAlertViewForVersionUpdateWithUrl:(NSString *)urlString;
@property id tabMenuVCView;
@property (strong,nonatomic) id <dismissView> delegate;
-(void)applyLeftPaddingToTextfield:(UITextField *)textField;
@end


@protocol dismissView<NSObject>
@optional
-(void)backButtonAction;

@property (strong,nonatomic) NSString *clicked;


@end
