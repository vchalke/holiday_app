//
//  NetworkHelper.m
//  holidays
//
//  Created by vaibhav on 18/09/15.
//  Copyright (c) 2015 Mobicule. All rights reserved.
//

#import "NetworkHelper.h"
#import "Thomas_Cook_Holidays-Swift.h"


@implementation NetworkHelper


+(NetworkHelper *)sharedHelper
{
    static NetworkHelper *helper = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        helper = [[NetworkHelper alloc]init];
    });
    
    return helper;
}



-(NSString *)getDataFromServerForType:(NSString *)type entity:(NSString *)entity action:(NSString *)action andUserJson:(NSDictionary *)userJSon
{
    
  
    
    NSString *strResponse = nil;
    ENABLE_LOGGER();
    DefaultHeaderRequestBuilder *requestBuilder = [[DefaultHeaderRequestBuilder alloc]initWithType:type withEntity:entity withAction:action withUserJson:userJSon];
    NSString *strRequest = [requestBuilder build];
    NetworkManager *nwrkManager = [[NetworkManager alloc]init];
    [nwrkManager setIsLogEnabled:YES];
    [nwrkManager setServerUrl:serverUrl];
    strResponse = [nwrkManager sendPostRequest:strRequest];
    return strResponse;
}


-(NSString *)getDataFromServerForType1:(NSString *)type entity:(NSString *)entity action:(NSString *)action andUserJson:(NSDictionary *)userJSon withCompletionHandler:(void (^)(NSString *finalResponse))handler{
    
    __block NSString *strResponse = nil;
    ENABLE_LOGGER();
    
    DefaultHeaderRequestBuilder *requestBuilder = [[DefaultHeaderRequestBuilder alloc]initWithType:type withEntity:entity withAction:action withUserJson:userJSon];
    NSString *strRequest = [requestBuilder build];
    NetworkManager *nwrkManager = [[NetworkManager alloc]init];
    [nwrkManager setIsLogEnabled:YES];
    [nwrkManager setServerUrl:serverUrl];
    
    strResponse = [nwrkManager sendPostRequest1:strRequest withCompletionHandler:^(NSString *response) {
        if (response != nil) {
            
            strResponse = response;
            
            handler(strResponse);
        }
    }];
    return strResponse;
    
}


-(void)getResponseWithRequestType:(NSString *)requestType withQueryParam:(NSString *)queryParam withPathParam:(NSString *)pathParam withJsonParam:(NSDictionary *)jsonParam withHeaders:(NSDictionary *)headersDict withUrl:(NSString *)urlForRequest success:(void (^)(NSDictionary *responseDict))success failure:(void(^)(NSError* error))failure
{
    
     if ([AppUtility checkNetConnection])
     {
    
    NSMutableString *urlForRequestWithParams = [[NSMutableString alloc]init];
    
//    if ([urlForRequest isEqualToString:@"https://thomascookindia.custhelp.com/cgi-bin/thomascookindia.cfg/php/custom/dom_int_insurance_opp_ios_app_lead.php"] || [urlForRequest isEqualToString:@"https://thomascookindia.custhelp.com/cgi-bin/thomascookindia.cfg/php/custom/dom _int_visa_opp_ios_app_lead.php"])
//    {
//        
//    }
//    else
//    {
//        [urlForRequestWithParams appendFormat:@"%@", kbaseURLForAstra];

//    }
    
         if (![urlForRequest isEqualToString:kTravelBlogURL]){
             [urlForRequestWithParams appendFormat:@"%@", kbaseURLForAstra];
         }
     [self setPinnedCertificates:[NSArray arrayWithObjects:@"lt_sotc_in.cer",@"resources_thomascook_in.cer",@"services_thomascook_in.cer",@"services_uatastra_thomascook_in.cer",@"thomascookindia_custhelp_com.cer",@"thomascookindia_tst1_custhelp_com.cer",@"uatastra_thomascook_in.cer",@"www_thomascook_in.cer",@"www_thomascooktravelcards_com.cer",nil]];
         
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    [urlForRequestWithParams appendFormat:@"%@", urlForRequest];
    
    if (![queryParam isEqualToString:@""])
    {
        [urlForRequestWithParams appendFormat:@"?%@",queryParam];
    }
    
    if (![pathParam isEqualToString:@""])
    {
        [urlForRequestWithParams appendFormat:@"/%@",pathParam];
    }
    
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlForRequestWithParams]]; //commented for ssl pining 19-09-2018
         
    //NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlForRequestWithParams] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0]; //changed for ssl pining 19-09-2018
    
    
    if ([requestType isEqualToString:@"POST"])
    {
         [urlRequest setHTTPMethod:@"POST"];
    }
    else
    {
         [urlRequest setHTTPMethod:@"GET"];
    }
    
   
    [urlRequest setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [urlRequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
   
    
    
    if (headersDict)
    {
        if (headersDict.count!=0)
        {
            NSArray *allKeys = [headersDict allKeys];
            
            for (int index = 0; index< allKeys.count; index++)
            {
                NSString *key = [allKeys objectAtIndex:index];
                NSString *value = [headersDict valueForKey:key];
                [urlRequest setValue:value forHTTPHeaderField:key];

            }
        }
    }

    NSLog(@"URL:- %@",urlRequest);
    if (jsonParam)
    {
        if (jsonParam.count != 0)
        {
            //NSString *jsonRequest = [NSString stringWithFormat:@"my dictionary is %@", jsonParam];
            
            NSLog(@"Request Json %@",jsonParam);
            
            NSData *requestData = [NSJSONSerialization dataWithJSONObject:jsonParam options:0 error:nil];

            [urlRequest setHTTPBody: requestData];
        }
    }
    
         NSLog(@"UrlRequest %@",urlRequest);

  //  NSURLSession *session = [NSURLSession sharedSession]; //commented for ssl pining 19-09-2018
         
    NSURLSession *session = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate:self delegateQueue: nil]; //changed for ssl pining 19-09-2018
        
    
    // Asynchronously API is hit here
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:urlRequest
                                            completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                NSLog(@"%@",data);
                                                if (error)
                                                    failure(error);
                                                else
                                                {
                                                    NSString* newStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];

                                                    
                                                    NSLog(@"Response :-  %@",newStr);
                                                    
                                                    NSDictionary *json  = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                    NSLog(@"%@",json);
                                                    success(json);
                                                }
                                            }];
    [dataTask resume];    // Executed First

    
}
    
}


/*
 [self  getResponseWithRequestType:@"" withQueryParam:@"" withPathParam:@"" withJsonParam:nil withHeaders:nil withUrl:nil success:^(NSDictionary *responseDict) {
 NSLog(@"%@",responseDict);
 } failure:^(NSError *error) {
 // error handling here ...
 }];

 */

#pragma mark -CA certificate validation

-(void)URLSession:(NSURLSession *)session didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition, NSURLCredential *))completionHandler {
    
    NSURLSessionAuthChallengeDisposition disposition = NSURLSessionAuthChallengePerformDefaultHandling;
    __block NSURLCredential *credential = nil;
    
    if([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]) {
        
        SecTrustRef serverTrust = [[challenge protectionSpace] serverTrust];
        NSString *domain = [[challenge protectionSpace] host];
        SecTrustResultType trustResult;
        
        // Validate the certificate chain with the device's trust store anyway
        // This *might* give use revocation checking
        SecTrustEvaluate(serverTrust, &trustResult);
        
        // Look for a pinned certificate in the server's certificate chain
        if ([self verifyPinnedCertificateForTrust:serverTrust andDomain:domain]) {
            
            credential = [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust];
            
            if (credential) {
                disposition = NSURLSessionAuthChallengeUseCredential;
            } else {
                disposition = NSURLSessionAuthChallengePerformDefaultHandling;
            }
            completionHandler(disposition,credential);
            
        }
        else {
            // The certificate wasn't found in the certificate chain; cancel the connection
            
            completionHandler(NSURLSessionAuthChallengeCancelAuthenticationChallenge, [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust]);
        }
    }
    else {
        // Certificate chain validation failed; cancel the connection
        
        completionHandler(NSURLSessionAuthChallengeCancelAuthenticationChallenge, [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust]);
    }
}

- (BOOL)verifyPinnedCertificateForTrust:(SecTrustRef)serverTrust andDomain:(NSString*)domain {
    
    
    if ((serverTrust == NULL) || (domain == nil)) {
        return NO;
    }
    
    // Do we have certificates pinned for this domain ?
    
    NSArray *trustedCertificates = [_pinnedCertificates mutableCopy];
    
    if ((trustedCertificates == nil) || ([trustedCertificates count] < 1)) {
        return NO;
    }
    
    // For each pinned certificate, check if it is part of the server's cert trust chain
    // We only need one of the pinned certificates to be in the server's trust chain
    // create pinned certificate array
    NSMutableArray *pinnedCertificates = [NSMutableArray array];
    
    for (NSData *certificateData in trustedCertificates) {
        if (certificateData != nil) {
            
            id  pinnnedCertRef = (__bridge_transfer id)SecCertificateCreateWithData(NULL, (__bridge CFDataRef)certificateData);
            
            if (pinnnedCertRef != nil) {
                
                [pinnedCertificates addObject:pinnnedCertRef];
            }
        }
    }
    
    // Check the anchor/CA certificate
    
    OSStatus status = SecTrustSetAnchorCertificates(serverTrust, (__bridge CFArrayRef)pinnedCertificates);
    
    if (status != 0) {
        
        return NO;
    }
    
    if(!iSServerTrustIsValid(serverTrust))
    {
        return NO;
    }
    
    
    // obtain the chain after being validated, which should contain the pinned certificate in the last position (if it's the Root CA)
    
    NSArray *serverCertificates = certificateTrustChainForServerTrust(serverTrust);
    
    for (NSData *trustChainCertificate in [serverCertificates reverseObjectEnumerator]) {
        if ([self.pinnedCertificates containsObject:trustChainCertificate]) {
            
            return YES;
        }
    }
    
    return NO;
    
}

static BOOL iSServerTrustIsValid(SecTrustRef serverTrust)
{
    
    BOOL isValid = NO;
    SecTrustResultType result;
    SecTrustEvaluate(serverTrust, &result);
    
    isValid = (result == kSecTrustResultUnspecified || result == kSecTrustResultProceed);
    
    // following if block required in case of self signed certificate
    
     //   if (_allowInvalidHostName) {
            if(result == kSecTrustResultRecoverableTrustFailure)
            {
                CFDataRef errDataRef = SecTrustCopyExceptions(serverTrust);
                SecTrustSetExceptions(serverTrust, errDataRef);
                SecTrustEvaluate(serverTrust, &result);
    
                isValid = (result == kSecTrustResultUnspecified || result == kSecTrustResultProceed);
            }
       // }
    
    return isValid;
}

static NSArray * certificateTrustChainForServerTrust(SecTrustRef serverTrust) {
    CFIndex certificateCount = SecTrustGetCertificateCount(serverTrust);
    NSMutableArray *trustChain = [NSMutableArray arrayWithCapacity:(NSUInteger)certificateCount];
    
    for (CFIndex i = 0; i < certificateCount; i++) {
        SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, i);
        [trustChain addObject:(__bridge_transfer NSData *)SecCertificateCopyData(certificate)];
    }
    
    return [NSArray arrayWithArray:trustChain];
}

- (void)setPinnedCertificates:(NSArray *)certificateNames
{
    
    NSMutableArray *pinnedCertificates = [[NSMutableArray alloc]init];
    
    NSBundle *bundle = [NSBundle bundleForClass:[self class]];
    
    for (NSString * certName in certificateNames)
    {
        
        if([certName isEqualToString:@"services_uatastra_thomascook_in.cer"]){
            NSLog(@"certificate match");
            
        }
        NSString * pinnedCertPath = [bundle pathForResource:[certName stringByDeletingPathExtension] ofType:[certName pathExtension]];
        
        NSData *certificateData = [NSData dataWithContentsOfFile:pinnedCertPath];
        if (pinnedCertPath != nil && certificateData != nil ) {
            [pinnedCertificates addObject:certificateData];
        }
        
    }
    _pinnedCertificates = pinnedCertificates;
}

@end
