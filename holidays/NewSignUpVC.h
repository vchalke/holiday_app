//
//  NewSignUpVC.h
//  holidays
//
//  Created by Kush_Team on 28/05/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewLoginVC.h"
NS_ASSUME_NONNULL_BEGIN

@interface NewSignUpVC : NewMasterVC
//@property (strong ,nonatomic) NSDictionary *notificationDict;
@property (weak, nonatomic) IBOutlet UIView *main_baseBottomViews;

@property (weak, nonatomic) IBOutlet UITextField *txt_name;
@property (weak, nonatomic) IBOutlet UITextField *txt_emailId;
@property (weak, nonatomic) IBOutlet UITextField *txt_passwords;
@property (weak, nonatomic) IBOutlet UITextField *txt_confPass;
@property (weak, nonatomic) IBOutlet UITextField *txt_Mobile;

@property (weak, nonatomic) IBOutlet UILabel *lbl_privacxyPolicy;
@property (weak, nonatomic) IBOutlet UIButton *btn_checkPolicy;
@property (weak, nonatomic) IBOutlet UIButton *btn_register;
@property (weak, nonatomic) IBOutlet UIButton *btn_login;

@property (weak, nonatomic) IBOutlet UIButton *btn_PasswordSecureOne;
@property (weak, nonatomic) IBOutlet UIButton *btn_PasswordSecureTwo;

@end

NS_ASSUME_NONNULL_END
