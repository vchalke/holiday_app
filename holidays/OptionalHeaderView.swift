//
//  OptionalHeaderView.swift
//  sotc-consumer-application
//
//  Created by Parshwanath on 18/09/17.
//  Copyright © 2017 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit

class OptionalHeaderView: UITableViewHeaderFooterView {

    //@IBOutlet weak var footerLabelHeightConstraints: NSLayoutConstraint!
    
    @IBOutlet weak var optionalHeaderLabel: UILabel!
    
    @IBOutlet weak var infoButton: UIButton!
   // @IBOutlet weak var footerLabel: UILabel!
    
    @IBOutlet weak var dropDownImageView: UIImageView!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
