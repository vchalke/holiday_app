//
//  PackageDetailModel.m
//  holidays
//
//  Created by Kush_Tech on 04/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "PackageDetailModel.h"

@implementation PackageDetailModel

-(void)loadArray{
    self.hotelCollectionArray = [[NSMutableArray alloc]init];
    self.itineraryCollectionArray = [[NSMutableArray alloc]init];
    self.mealCollectionArray = [[NSMutableArray alloc]init];
    self.transfersCollectionArray = [[NSMutableArray alloc]init];
    self.flightsCollectionArray = [[NSMutableArray alloc]init];
    self.visaCollectionArray = [[NSMutableArray alloc]init];
    self.sightseenCollectionArray = [[NSMutableArray alloc]init];
    self.includeexcludeCollectionArray = [[NSMutableArray alloc]init];
}
-(instancetype)initWithPackageDetailDict:(NSDictionary *)dictionary{
    if ([super init])
    {
//        [self loadArray];
        self.hotelCollectionArray = [dictionary valueForKey:@"tcilHolidayAccomodationCollection"];
        self.itineraryCollectionArray = [dictionary valueForKey:@"tcilHolidayItineraryCollection"];
        self.mealCollectionArray = [dictionary valueForKey:@"tcilHolidayMealCollection"];
        self.transfersCollectionArray = [dictionary valueForKey:@"tcilHolidayTransfersCollection"];
        self.flightsCollectionArray = [dictionary valueForKey:@"tcilHolidayFlightsCollection"];
        self.visaCollectionArray = [dictionary valueForKey:@"tcilHolidayVisaCollection"];
        self.sightseenCollectionArray = [dictionary valueForKey:@"tcilHolidaySightseeingCollection"];
        self.includeexcludeCollectionArray = [dictionary valueForKey:@"tcilHolidayIncludeExcludeCollection"];
        
//        NSLog(@"self.itineraryCollectionArray %@",self.hotelCollectionArray);
//        NSLog(@"self.itineraryCollectionArray %@",self.itineraryCollectionArray);
//        NSLog(@"self.mealCollectionArray %@",self.mealCollectionArray);
//        NSLog(@"self.transfersCollectionArray %@",self.transfersCollectionArray);
//        NSLog(@"self.flightsCollectionArray %@",self.flightsCollectionArray);
//        NSLog(@"self.visaCollectionArray %@",self.visaCollectionArray);
//        NSLog(@"self.sightseenCollectionArray %@",self.sightseenCollectionArray);
//        NSLog(@"self.includeexcludeCollectionArray %@",self.includeexcludeCollectionArray);
        
    }
    return self;
}
@end
