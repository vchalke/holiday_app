//
//  VisaTableViewCell.swift
//  sotc-consumer-application
//
//  Created by Saurav Kumar on 10/08/17.
//  Copyright © 2017 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit

class VisaTableViewCell: UITableViewCell {
    
    
    @IBOutlet var documentStackViewWidthConst: NSLayoutConstraint!
    
    @IBOutlet var step4Title: UILabel!
    @IBOutlet var step3Title: UILabel!
    @IBOutlet var step2Title: UILabel!
    @IBOutlet var step1Title: UILabel!
    @IBOutlet var step4LeadingView: UIView!
    @IBOutlet var step3trailingView: UIView!
    @IBOutlet var step3LeadingView: UIView!
    @IBOutlet var step2trailingView: UIView!
    @IBOutlet var step2LeadingView: UIView!
    @IBOutlet var step1trailingView: UIView!
    @IBOutlet var step4ImageView: UIImageView!
    @IBOutlet var step3ImageView: UIImageView!
    @IBOutlet var step2ImageView: UIImageView!
    @IBOutlet var step1ImageView: UIImageView!
    @IBOutlet var step4StackView: UIStackView!
    @IBOutlet var step3StackView: UIStackView!
    @IBOutlet var step2StackView: UIStackView!
    @IBOutlet var step1StackView: UIStackView!
    
    
    var completedTillIndex = -1
    var failedIndex = -1
    
    let animationDurationForEachStep = 0.5
    
    var hideInterviewScheduled = true
    
    var visaProccessFailed = false
    let  greenColor = AppUtility.hexStringToUIColor(hex:"39ea77")
    let grayColor = AppUtility.hexStringToUIColor(hex:"EBEBF1")
    
    
    
    @IBOutlet var userNameLabel: UILabel!
 
    
    var visaDetail:VisaDetailsData?
    
    @IBOutlet weak var appointmentLetterView: UIView!
    
    @IBOutlet var coveringLetterButton: UIButton!
    @IBOutlet weak var letterFormatView: UIView!
    @IBOutlet weak var letterFormatBtn: UIButton!
    
    @IBOutlet weak var appointmentLetterBtn: UIButton!
    
    var maxIndex = -1
    
    var showAnimation = false
    
    @IBOutlet weak var visaRequiredDocView: UIStackView!
    
    @IBOutlet weak var visaReqiredLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        step1ImageView.layer.cornerRadius = 10
        step2ImageView.layer.cornerRadius = 10
        step3ImageView.layer.cornerRadius = 10
        step4ImageView.layer.cornerRadius = 10
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        
        
        self.step1ImageView.image = nil
        self.step2ImageView.image = nil
        self.step3ImageView.image = nil
        self.step4ImageView.image = nil
        
        step4LeadingView.backgroundColor = grayColor
        step3trailingView.backgroundColor = grayColor
        step3LeadingView.backgroundColor = grayColor
        step2trailingView.backgroundColor = grayColor
        step2LeadingView.backgroundColor = grayColor
        step1trailingView.backgroundColor = grayColor
        
        
        self.step1ImageView.backgroundColor = grayColor
        self.step2ImageView.backgroundColor = grayColor
        self.step3ImageView.backgroundColor = grayColor
        self.step4ImageView.backgroundColor = grayColor
    }
    func showProgressBar() {
        
        
        
        if hideInterviewScheduled
        {
            step3StackView.isHidden = true
            
        }
        
        if showAnimation {
            
            switch completedTillIndex {
            case  0:
                addStep1Animation()
            case  1:
                addStep2Animation()
            case  2:
                addStep3Animation()
            case  3:
                addStep4Animation()
            default:
                
                showGrayProgressBar()
                
            }

        }
        else
        {
            switch completedTillIndex {
            case  0:
                addStep1()
            case  1:
                addStep2()
            case  2:
                addStep3()
            case  3:
                addStep4()
            default:
                
                showGrayProgressBar()
                
            }

        }
        
        
        
        
        
    }
    
    func showGrayProgressBar() {
        
        
        
        self.step1ImageView.image = nil
        self.step2ImageView.image = nil
        self.step3ImageView.image = nil
        self.step4ImageView.image = nil
        
        step4LeadingView.backgroundColor = grayColor
        step3trailingView.backgroundColor = grayColor
        step3LeadingView.backgroundColor = grayColor
        step2trailingView.backgroundColor = grayColor
        step2LeadingView.backgroundColor = grayColor
        step1trailingView.backgroundColor = grayColor
        
        
        self.step1ImageView.backgroundColor = grayColor
        self.step2ImageView.backgroundColor = grayColor
        self.step3ImageView.backgroundColor = grayColor
        self.step4ImageView.backgroundColor = grayColor
        
        
        
    }
    
    func addStep1Animation()  {
        
        UIView.animate(withDuration: animationDurationForEachStep, animations: {
            
            self.step1ImageView.backgroundColor = self.self.greenColor
            self.step1ImageView.image = #imageLiteral(resourceName: "visaTick")
            
            
            if self.visaProccessFailed
            {
                self.step1ImageView.backgroundColor = UIColor.red
                self.step1ImageView.image = #imageLiteral(resourceName: "visaCancel")
            }
            else
            {
                self.step1ImageView.backgroundColor = self.self.greenColor
                self.step1ImageView.image = #imageLiteral(resourceName: "visaTick")
            }
            
        }) { (Bool) in
            
            
            
            
        }
        
    }
    
    func addStep2Animation()  {
        
        
        UIView.animate(withDuration: animationDurationForEachStep, animations: {
            
            self.step1ImageView.backgroundColor = self.greenColor
            self.step1ImageView.image = #imageLiteral(resourceName: "visaTick")
            
        }) { (Bool) in
            
            UIView.animate(withDuration: self.animationDurationForEachStep, animations: {
                
                
                
                self.step1trailingView.backgroundColor = self.greenColor
                self.step2LeadingView.backgroundColor = self.greenColor
                
                
            }) { (Bool) in
                
                UIView.animate(withDuration: self.animationDurationForEachStep, animations: {
                    if self.visaProccessFailed
                    {
                        
                        self.step2ImageView.image = #imageLiteral(resourceName: "visaCancel")
                    }
                    else
                    {
                        self.step2ImageView.backgroundColor = self.self.greenColor
                        self.step2ImageView.image = #imageLiteral(resourceName: "visaTick")
                    }
                    
                    
                    
                    
                }) { (Bool) in
                    
                    
                }
                
                
            }
        }
        
        
        
    }
    
    
    func addStep3Animation()  {
        
        
        UIView.animate(withDuration: animationDurationForEachStep, animations: {
            
            self.step1ImageView.backgroundColor = self.self.greenColor
            self.step1ImageView.image = #imageLiteral(resourceName: "visaTick")
            
        }) { (Bool) in
            
            UIView.animate(withDuration: self.animationDurationForEachStep, animations: {
                
                
                
                self.step1trailingView.backgroundColor = self.greenColor
                self.step2LeadingView.backgroundColor = self.greenColor
                
                
            }) { (Bool) in
                
                UIView.animate(withDuration: self.animationDurationForEachStep, animations: {
                    
                    self.step2ImageView.backgroundColor = self.self.greenColor
                    self.step2ImageView.image = #imageLiteral(resourceName: "visaTick")
                    
                    
                }) { (Bool) in
                    
                    
                    if self.hideInterviewScheduled
                    {
                        UIView.animate(withDuration: self.animationDurationForEachStep, animations: {
                            
                            
                            
                            self.step2trailingView.backgroundColor = self.greenColor
                            self.step4LeadingView.backgroundColor = self.greenColor
                            
                            
                        }) { (Bool) in
                            
                            UIView.animate(withDuration: self.animationDurationForEachStep, animations: {
                                
                                
                                if self.visaProccessFailed
                                {
                                    
                                    self.step4ImageView.image = #imageLiteral(resourceName: "visaCancel")
                                }
                                else
                                {
                                    self.step4ImageView.backgroundColor = self.self.greenColor
                                    self.step4ImageView.image = #imageLiteral(resourceName: "visaTick")
                                }
                                
                                
                                
                                
                                
                            }) { (Bool) in
                                
                                
                            }
                            
                            
                        }
                        
                    }
                    else
                    {
                        
                        UIView.animate(withDuration: self.animationDurationForEachStep, animations: {
                            
                            
                            
                            self.step2trailingView.backgroundColor = self.greenColor
                            self.step3LeadingView.backgroundColor = self.greenColor
                            
                            
                        }) { (Bool) in
                            
                            UIView.animate(withDuration: self.animationDurationForEachStep, animations: {
                                
                                
                                if self.visaProccessFailed
                                {
                                    
                                    self.step3ImageView.image = #imageLiteral(resourceName: "visaCancel")
                                }
                                else
                                {
                                    self.step3ImageView.backgroundColor = self.self.greenColor
                                    self.step3ImageView.image = #imageLiteral(resourceName: "visaTick")
                                }
                                
                                
                                
                                
                                
                            }) { (Bool) in
                                
                                
                            }
                            
                            
                        }
                        
                    }
                    
                    
                    
                    
                    
                    
                    
                    
                }
                
                
            }
        }
        
        
        
    }
    
    
    func addStep4Animation()  {
        
        UIView.animate(withDuration: animationDurationForEachStep, animations: {
            
            self.step1ImageView.backgroundColor = self.greenColor
            self.step1ImageView.image = #imageLiteral(resourceName: "visaTick")
            
        }) { (Bool) in
            
            UIView.animate(withDuration: self.animationDurationForEachStep, animations: {
                
                
                
                self.step1trailingView.backgroundColor = self.greenColor
                self.step2LeadingView.backgroundColor = self.greenColor
                
                
            }) { (Bool) in
                
                UIView.animate(withDuration: self.animationDurationForEachStep, animations: {
                    
                    self.step2ImageView.backgroundColor = self.self.greenColor
                    self.step2ImageView.image = #imageLiteral(resourceName: "visaTick")
                    
                    
                }) { (Bool) in
                    
                    if self.hideInterviewScheduled
                    {
                        
                        UIView.animate(withDuration: self.animationDurationForEachStep, animations: {
                            
                            
                            
                            self.step2trailingView.backgroundColor = self.greenColor
                            self.step4LeadingView.backgroundColor = self.greenColor
                            
                            
                        }) { (Bool) in
                            
                            UIView.animate(withDuration: self.animationDurationForEachStep, animations: {
                                
                                
                                if self.visaProccessFailed
                                {
                                    
                                    self.step4ImageView.image = #imageLiteral(resourceName: "visaCancel")
                                }
                                else
                                {
                                    self.step4ImageView.backgroundColor = self.self.greenColor
                                    self.step4ImageView.image = #imageLiteral(resourceName: "visaTick")
                                }
                                
                                
                                
                            }) { (Bool) in
                                
                                
                            }
                            
                            
                        }
                        
                        
                    }
                    else
                    {
                        
                        
                        UIView.animate(withDuration: self.animationDurationForEachStep, animations: {
                            
                            
                            
                            self.step2trailingView.backgroundColor = self.greenColor
                            self.step3LeadingView.backgroundColor = self.greenColor
                            
                            
                        }) { (Bool) in
                            
                            UIView.animate(withDuration: self.animationDurationForEachStep, animations: {
                                
                                
                                self.step3ImageView.backgroundColor = self.self.greenColor
                                self.step3ImageView.image = #imageLiteral(resourceName: "visaTick")
                                
                                
                            }) { (Bool) in
                                
                                
                                
                                
                                UIView.animate(withDuration: self.animationDurationForEachStep, animations: {
                                    
                                    
                                    
                                    self.step3trailingView.backgroundColor = self.greenColor
                                    self.step4LeadingView.backgroundColor = self.greenColor
                                    
                                    
                                }) { (Bool) in
                                    
                                    UIView.animate(withDuration: self.animationDurationForEachStep, animations: {
                                        
                                        
                                        if self.visaProccessFailed
                                        {
                                            
                                            self.step4ImageView.image = #imageLiteral(resourceName: "visaCancel")
                                        }
                                        else
                                        {
                                            self.step4ImageView.backgroundColor = self.self.greenColor
                                            self.step4ImageView.image = #imageLiteral(resourceName: "visaTick")
                                        }
                                        
                                        
                                        
                                    }) { (Bool) in
                                        
                                        
                                    }
                                    
                                    
                                }
                                
                                
                            }
                            
                        }
                        
                    }
                    
                    
                }
                
                
            }
        }
        
    }
    
    
    func addStep1() {
        
        self.step1ImageView.backgroundColor = self.self.greenColor
        self.step1ImageView.image = #imageLiteral(resourceName: "visaTick")
        
        
        if self.visaProccessFailed
        {
            self.step1ImageView.backgroundColor = UIColor.red
            self.step1ImageView.image = #imageLiteral(resourceName: "visaCancel")
        }
        else
        {
            self.step1ImageView.backgroundColor = self.self.greenColor
            self.step1ImageView.image = #imageLiteral(resourceName: "visaTick")
        }
        
    }
    
    func addStep2()  {
        
        self.step1ImageView.backgroundColor = self.greenColor
        self.step1ImageView.image = #imageLiteral(resourceName: "visaTick")
        self.step1trailingView.backgroundColor = self.greenColor
        self.step2LeadingView.backgroundColor = self.greenColor
        
        if self.visaProccessFailed
        {
            
            self.step2ImageView.image = #imageLiteral(resourceName: "visaCancel")
        }
        else
        {
            self.step2ImageView.backgroundColor = self.self.greenColor
            self.step2ImageView.image = #imageLiteral(resourceName: "visaTick")
        }
        
        
    }
    
    
    func addStep3()  {
        
        
        self.step1ImageView.backgroundColor = self.self.greenColor
        self.step1ImageView.image = #imageLiteral(resourceName: "visaTick")
        self.step1trailingView.backgroundColor = self.greenColor
        self.step2LeadingView.backgroundColor = self.greenColor
        self.step2ImageView.backgroundColor = self.self.greenColor
        self.step2ImageView.image = #imageLiteral(resourceName: "visaTick")
        
        if self.hideInterviewScheduled
        {
            
            self.step2trailingView.backgroundColor = self.greenColor
            self.step4LeadingView.backgroundColor = self.greenColor
            
            if self.visaProccessFailed
            {
                
                self.step4ImageView.image = #imageLiteral(resourceName: "visaCancel")
            }
            else
            {
                self.step4ImageView.backgroundColor = self.self.greenColor
                self.step4ImageView.image = #imageLiteral(resourceName: "visaTick")
            }
            
        }
            
        else
        {
            
            
            self.step2trailingView.backgroundColor = self.greenColor
            self.step3LeadingView.backgroundColor = self.greenColor
            
            if self.visaProccessFailed
            {
                
                self.step3ImageView.image = #imageLiteral(resourceName: "visaCancel")
            }
            else
            {
                self.step3ImageView.backgroundColor = self.self.greenColor
                self.step3ImageView.image = #imageLiteral(resourceName: "visaTick")
            }
            
            
            
            
            
        }
    }
    
    
    func addStep4()  {
        
        self.step1ImageView.backgroundColor = self.greenColor
        self.step1ImageView.image = #imageLiteral(resourceName: "visaTick")
        self.step1trailingView.backgroundColor = self.greenColor
        self.step2LeadingView.backgroundColor = self.greenColor
        
        
        
        self.step2ImageView.backgroundColor = self.self.greenColor
        self.step2ImageView.image = #imageLiteral(resourceName: "visaTick")
        
        if self.hideInterviewScheduled
        {
            
            UIView.animate(withDuration: self.animationDurationForEachStep, animations: {
                
                
                
                self.step2trailingView.backgroundColor = self.greenColor
                self.step4LeadingView.backgroundColor = self.greenColor
                
                
            }) { (Bool) in
                
                UIView.animate(withDuration: self.animationDurationForEachStep, animations: {
                    
                    
                    if self.visaProccessFailed
                    {
                        
                        self.step4ImageView.image = #imageLiteral(resourceName: "visaCancel")
                    }
                    else
                    {
                        self.step4ImageView.backgroundColor = self.self.greenColor
                        self.step4ImageView.image = #imageLiteral(resourceName: "visaTick")
                    }
                    
                    
                    
                }) { (Bool) in
                    
                    
                }
                
                
            }
            
            
        }
        else
        {
            
            
            
            self.step2trailingView.backgroundColor = self.greenColor
            self.step3LeadingView.backgroundColor = self.greenColor
            
            
            
            self.step3ImageView.backgroundColor = self.self.greenColor
            self.step3ImageView.image = #imageLiteral(resourceName: "visaTick")
            
            
            
            
            self.step3trailingView.backgroundColor = self.greenColor
            self.step4LeadingView.backgroundColor = self.greenColor
            
            
            
            
            if self.visaProccessFailed
            {
                
                self.step4ImageView.image = #imageLiteral(resourceName: "visaCancel")
            }
            else
            {
                self.step4ImageView.backgroundColor = self.self.greenColor
                self.step4ImageView.image = #imageLiteral(resourceName: "visaTick")
            }
            
            
        }
        
        
        
    }
    
    
}
