//
//  UserProfileViewController.swift
//  sotc-consumer-application
//
//  Created by Saurav Kumar on 18/07/17.
//  Copyright © 2017 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit

class UserProfileViewController: UIViewController {
    
     var pageMenu : CAPSPageMenu?
    
    var passangerDetailsArray:Array<Passenger>?
    var tourStatus:TourStatus?
    var selectedPassenger:Passenger? = nil
    var isfromNotificationHomeVc:Bool = false
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.configureCAPSPPageMenu()
        
        // Do any additional setup after loading the view.
        
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
       
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(true)
        if self.passangerDetailsArray?.count ?? 0 > 0
        {
            var pageIndex = 0
            
            if self.selectedPassenger != nil
            {
                pageIndex = self.getPageIndex(by: self.selectedPassenger!)
            }
            self.pageMenu?.moveToPage(pageIndex)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.setRedNavigationBar()
        self.navigationController?.setnavigatiobBarTitle(title: Title.PROFILE, viewController: self)
        
              
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        if self.navigationController?.viewControllers.firstIndex(of: self) == nil && self.isfromNotificationHomeVc
        {
            SlideNavigationController.sharedInstance().popViewController(animated: true)
        }
        
    }
    
    func configureCAPSPPageMenu()  {
        
        var userProfileDetailsControllerArray : [UIViewController] = []
        
        for passengerDetails in passangerDetailsArray! {
            
            let userProfileDetailsVC : UserProfileDetailsViewController = UserProfileDetailsViewController(nibName: "UserProfileDetailsViewController", bundle: nil)
           // let userProfileModule:UserProfile  = userProfileDetailsArray[index]
            
             if let passenger = passengerDetails as? Passenger {
            
            userProfileDetailsVC.title = (passenger.firstName ?? "").capitalized
            userProfileDetailsVC.passaenger = passenger
           // userProfileDetailsVC.delegate = self
                userProfileDetailsVC.tourStatus = self.tourStatus
            }
            userProfileDetailsControllerArray.append(userProfileDetailsVC)
        }

        
        if userProfileDetailsControllerArray.count > 0  {
            
            self.pageMenu = CAPSPageMenu(viewControllers: userProfileDetailsControllerArray, frame: CGRect(x: 0.0, y: -1, width: self.view.frame.width, height: self.view.frame.height), pageMenuOptions: [CAPSPageMenuOption.scrollMenuBackgroundColor(Constant.AppThemeColor),CAPSPageMenuOption.menuItemWidthBasedOnTitleTextWidth(true) ,CAPSPageMenuOption.menuItemFont(UIFont(name:"Roboto", size:15)!),CAPSPageMenuOption.unselectedMenuItemLabelColor(UIColor.groupTableViewBackground),CAPSPageMenuOption.menuHeight(50),CAPSPageMenuOption.menuItemMargin(20)])
            
           
            
              self.view.addSubview(pageMenu!.view)
            
            //pageMenu?.delegate = self as CAPSPageMenuDelegate;
          /*  var pageIndex = 0
            
            if self.selectedPassenger != nil
            {
                pageIndex = self.getPageIndex(by: self.selectedPassenger!)
            }
            self.pageMenu?.moveToPage(pageIndex)*/
            
          
            
        }
        
     
        
    }
    
    func getPageIndex(by passenger:Passenger) -> Int {
        
        do{
        let index = try passangerDetailsArray?.firstIndex(where: { (passenger1:Passenger) -> Bool in
            passenger1.passengerNumber ==  passenger.passengerNumber
        })
            
        

        return index ?? 0
            
        }catch{
            return 0
        }
    }
    
}




