//
//  RequestForCallPopUp.h
//  holidays
//
//  Created by ketan on 31/12/15.
//  Copyright © 2015 Mobicule. All rights reserved.
//

#import "BaseViewController.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "HolidayPackageDetail.h"

@interface RequestForCallPopUp : BaseViewController<UITextFieldDelegate>
{
    LoadingView *activityLoadingView;
}
@property (strong, nonatomic) IBOutlet UIView *viewForRequestCall;

@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollViewRequestCall;
@property (strong ,nonatomic) HolidayPackageDetail *packageDetail;
@property (weak, nonatomic) IBOutlet UITextField *txtFirstName;
@property (weak, nonatomic) IBOutlet UITextField *txtLastName;
@property (weak, nonatomic) IBOutlet UITextField *txtPhoneNumber;
@property (weak, nonatomic) IBOutlet UITextField *txtEmailIId;
@property (weak, nonatomic) IBOutlet UITextField *txtCity;
@property (weak, nonatomic) IBOutlet UITextField *txtSummery;
@property (weak, nonatomic) IBOutlet UITextField *txtDepartureCity;
- (IBAction)onSubmitButtonClicked:(id)sender;

@end
