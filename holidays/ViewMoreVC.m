//
//  ViewMoreVCViewController.m
//  holidays
//
//  Created by Kush_Tech on 04/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "ViewMoreVC.h"
#import "WebUrlConstants.h"
#import "IncludeExcludeObject.h"
#import "MealsObject.h"
#import "SightSeenObject.h"
#import "VisaPassInsurance.h"
#import "TransferObject.h"
#import "HolidayAccomodationObject.h"
//#import "FlightsColectObject.h"
@interface ViewMoreVC ()

@end

@implementation ViewMoreVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.lbl_first.text = self.sectionTitLe;
    self.view_Offer.hidden = !self.isOfferApplicable;
    self.lbl_prizes.text = self.lblaPrize;
    /*
    if ([[self.sectionTitLe uppercaseString] isEqualToString:sInclusion] ||  [[self.sectionTitLe uppercaseString] isEqualToString:sExclusion]){
        self.first_view.hidden = YES;
        self.const_FirstView = [self setMultiplier:self.const_FirstView  withMultiplier:0.0];
        BOOL bolFlag = [[self.sectionTitLe uppercaseString] isEqualToString:sExclusion];
        [self setBottomToRight:bolFlag];
        self.lbl_descriptionText.text = [self getStringForSection:self.sectionTitLe];
    }else{
        self.second_view.hidden = YES;
        self.const_SecondView = [self setMultiplier:self.const_SecondView  withMultiplier:0.0];
        self.lbl_descriptionText.text = self.firstDescription;
    }
    */
    
    
//   if ([[self.sectionTitLe uppercaseString] isEqualToString:sInclusion] ||  [[self.sectionTitLe uppercaseString] isEqualToString:sExclusion]){
//       self.first_view.hidden = YES;
//       self.const_FirstView = [self setMultiplier:self.const_FirstView  withMultiplier:0.0];
//   }else{
//       self.second_view.hidden = YES;
//       self.const_SecondView = [self setMultiplier:self.const_SecondView  withMultiplier:0.0];
//   }
    
    if ([[self.sectionTitLe uppercaseString] isEqualToString:sInclusion] ||  [[self.sectionTitLe uppercaseString] isEqualToString:sExclusion]){
        self.first_view.hidden = YES;
        self.const_FirstView = [self setMultiplier:self.const_FirstView  withMultiplier:0.0];
    }else{
        self.second_view.hidden = YES;
        self.const_SecondView = [self setMultiplier:self.const_SecondView  withMultiplier:0.0];
    }
    
    
}
- (void)viewDidAppear:(BOOL)animated{
    
    if (![[self.sectionTitLe uppercaseString] isEqualToString:sExclusion] && ![[self.sectionTitLe uppercaseString] isEqualToString:sInclusion]){
        NSString *htmlStr = [self getStringForSection:[self.sectionTitLe uppercaseString]];
        [self showStringInWebView:htmlStr];
    }else{
        BOOL bolFlag = [[self.sectionTitLe uppercaseString] isEqualToString:sExclusion];
        [self setBottomToRight:bolFlag];
    }
//    NSArray *array = [self getArrayForSection:self.sectionTitLe];
//    [self showArrayOfStringInWebView:array];
    
//    NSString *arrayStr = [[self getStringForSection:self.sectionTitLe] copy];
//    [self showStringInWebView:arrayStr];
    
}
-(void)showArrayOfStringInWebView:(NSArray*)strArray{
//   Roboto-Regular
    for (NSString *htmlString in strArray) {
        NSString *htmlStringObj = [NSString stringWithFormat:@"<span style=\"font-family: Lato-Regular; font-size: 15\">%@</span>", htmlString];
        [self.web_viewa loadHTMLString:htmlStringObj baseURL:nil];
    }
}
-(void)showStringInWebView:(NSString*)strings{
    
        NSString *htmlStringObj = [NSString stringWithFormat:@"<span style=\"font-family: Lato-Regular; font-size: 15\">%@</span>", strings];
//        [self.web_viewa loadHTMLString:htmlStringObj baseURL:nil];
    [self.web_viewa loadHTMLString:[htmlStringObj stringByReplacingOccurrencesOfString:@"\n" withString:@"<br/>"] baseURL:nil];
    
//    [self.web_viewa loadHTMLString:htmlStringObj baseURL:nil];

   
}

-(NSMutableString*)getStringForSection:(NSString*)string{
    NSMutableString *mutString = [[NSMutableString alloc]init];
    if ([string isEqualToString:sExclusion] || [string isEqualToString:sInclusion]){
//        for (id object in self.packageModel.includeexcludeCollectionArray) {
            for (id object in self.packageModel.includeexcludeCollectionArray) {
                IncludeExcludeObject *itinery = [[IncludeExcludeObject alloc]initWithIncludeExcludeDict:object];
                if (itinery.packageClassId == self.holidayPkgModelInViewMore.mainPkgSubClassId){
                    NSString *discription;
                    if ([[string uppercaseString] isEqualToString:sInclusion]){
                        discription = itinery.includes;
                    }else{
                        discription = itinery.excludes;
                    }
//                    [mutString appendString:[NSString stringWithFormat:@"%@\n",discription]];
                    [mutString appendString:[NSString stringWithFormat:@"%@",discription]];
                }
            }
//        }
    }
    if ([string isEqualToString:sMeals]){
        for (id object in self.packageModel.mealCollectionArray) {
            MealsObject *mealObj = [[MealsObject alloc]initWithMealsObjectDict:object];
            if(mealObj.packageClassId == self.holidayPkgModelInViewMore.mainPkgSubClassId){
              [mutString appendString:[NSString stringWithFormat:@"%@\n",mealObj.mealDescription]];
            }
            
        }
    }
    if ([string isEqualToString:sSightSeeing]){
        if ([self.packageModel.sightseenCollectionArray count]>0){
            for (id object in self.packageModel.sightseenCollectionArray) {
                SightSeenObject *sightObj = [[SightSeenObject alloc]initWithSightSeenObjectDict:object];
                if(sightObj.packageClassId == self.holidayPkgModelInViewMore.mainPkgSubClassId){
                    NSAttributedString *myBoldString = [[NSAttributedString alloc] initWithString:sightObj.cityName
                                                                                       attributes:@{ NSFontAttributeName: [UIFont fontWithName:@"Lato-Heavy" size:16.0f] }];
                    NSString *mainStr = [NSString stringWithFormat:@"%@",myBoldString.string];
                    if (sightObj.name){
                        mainStr = [NSString stringWithFormat:@"%@\n%@",mainStr,sightObj.name];}
                    if (sightObj.descriptionName){
                        mainStr = [NSString stringWithFormat:@"%@\n%@",mainStr,sightObj.descriptionName];}
                    [mutString appendString:[NSString stringWithFormat:@"%@\n",mainStr]];
                }
            }
        }else{
            
            if (self.holidayPkgModelInViewMore.isSightseeingDefaultMsg.length > 2){
                [mutString appendString:[NSString stringWithFormat:@"%@\n",self.holidayPkgModelInViewMore.isSightseeingDefaultMsg]];
            }else{
                for (NSDictionary *object in self.holidayPkgModelInViewMore.sightseenCollection) {
                    [mutString appendString:[NSString stringWithFormat:@"%@\n",object[@"typeDefaultMsg"]]];
                }
            }
        }
        
    }
    if ([string isEqualToString:sHotels]){
//        NSMutableArray *hotelObjectsArray = [[NSMutableArray alloc]init];
//        NSMutableArray *araay = [[NSMutableArray alloc]init];
//        for (id object in self.packageModel.hotelCollectionArray) {
//            HolidayAccomodationObject *hotelObj = [[HolidayAccomodationObject alloc]initWithAccomodationDict:object];
//            if ((hotelObj.position-1)>[hotelObjectsArray count]){
//                [araay addObject:hotelObj];
//            }else{
//                [araay insertObject:hotelObj atIndex:hotelObj.position-1];
//            }
//        }
        NSMutableArray *araay = [[NSMutableArray alloc]init];
        NSSortDescriptor *brandDescriptor = [[NSSortDescriptor alloc] initWithKey:@"position" ascending:YES];
            NSArray *sortDescriptors = [NSArray arrayWithObject:brandDescriptor];
            NSArray *filteredArray = [self.packageModel.hotelCollectionArray sortedArrayUsingDescriptors:sortDescriptors];
        for (id object in filteredArray) {
            HolidayAccomodationObject *hotelObj = [[HolidayAccomodationObject alloc]initWithAccomodationDict:object];
            if (hotelObj.packageClassId == self.holidayPkgModelInViewMore.mainPkgSubClassId){
                [araay addObject:hotelObj];
            }
        }
        if([araay count]>0){
            for (HolidayAccomodationObject *hotelObj in araay) {
                if (hotelObj.packageClassId == self.holidayPkgModelInViewMore.mainPkgSubClassId){
                    NSString *mainStr = [NSString stringWithFormat:@"\n%@ | %ld Nights",hotelObj.cityName,hotelObj.noOfNights];
                    mainStr = [NSString stringWithFormat:@"%@\n%@ / Similar",mainStr,hotelObj.hotelName];
                    mainStr = [NSString stringWithFormat:@"%@\n%@",mainStr,hotelObj.address];
                    [mutString appendString:[NSString stringWithFormat:@"%@\n",mainStr]];
                }
            }
        }else{
            if (self.holidayPkgModelInViewMore.hotelDefaultMsg.length > 2){
                [mutString appendString:[NSString stringWithFormat:@"%@\n",self.holidayPkgModelInViewMore.hotelDefaultMsg]];
            }else{
                for (NSDictionary *object in self.holidayPkgModelInViewMore.hotelCollection) {
                    NSInteger pkgClasId = [[object objectForKey:@"packageClassId"] integerValue];
                    if (pkgClasId == self.holidayPkgModelInViewMore.mainPkgSubClassId){
                        [mutString appendString:[NSString stringWithFormat:@"%@\n",object[@"typeDefaultMsg"]]];
                    }
                }
                for (NSDictionary *object in self.holidayPkgModelInViewMore.accombdationCollection) {
                    NSInteger pkgClasId = [[object objectForKey:@"packageClassId"] integerValue];
                    if (pkgClasId == self.holidayPkgModelInViewMore.mainPkgSubClassId){
                        [mutString appendString:[NSString stringWithFormat:@"%@\n",object[@"typeDefaultMsg"]]];
                    }
                }
            }
        }
    }
    if ([string isEqualToString:sVisaTransferInsurance]){
        for (id object in self.packageModel.visaCollectionArray) {
            VisaPassInsurance *visobject = [[VisaPassInsurance alloc]initWithVisaPassInsuranceModel:object];
            if(visobject.packageClassId == self.holidayPkgModelInViewMore.mainPkgSubClassId){
              [mutString appendString:[NSString stringWithFormat:@"%@\n",visobject.visaPasInsSrp]];
            }   
        }
    }
    if ([string isEqualToString:sTransfer]){
        for (id object in self.packageModel.transfersCollectionArray) {
            TransferObject *transfer = [[TransferObject alloc]initWithTransferDict:object];
            if (transfer.packageClassId == self.holidayPkgModelInViewMore.mainPkgSubClassId){
                [mutString appendString:[NSString stringWithFormat:@"%@\n",transfer.transferdescription]];
            }
            
        }
    }
    if ([string isEqualToString:sFlights]){
        if ([self.packageModel.flightsCollectionArray count]>0){
            for (id object in self.packageModel.flightsCollectionArray) {
                FlightsColectObject *flighObject = [[FlightsColectObject alloc]initWithFlightObjectDict:object];
                NSString *mainStr = [NSString stringWithFormat:@"\n%@",flighObject.airlineName];
                mainStr = [NSString stringWithFormat:@"%@\n%@ - %@",mainStr,flighObject.departureCity,flighObject.destinationCity];
                mainStr = [NSString stringWithFormat:@"%@\nDuration : %@",mainStr,flighObject.flightDuration];
                [mutString appendString:[NSString stringWithFormat:@"%@\n",mainStr]];
            }
        }else{
            NSString *blankStr = (self.holidayPkgModelInViewMore.flightDefaultMsg.length > 0)? self.holidayPkgModelInViewMore.flightDefaultMsg : @"Return Economy class airfare as per the Itinenary";
            [mutString appendString:[NSString stringWithFormat:@"%@\n",blankStr]];
            
        }
        
    }
    return mutString;
}
-(NSArray*)getArrayForSection:(NSString*)string{
    NSMutableArray *mutStringArray = [[NSMutableArray alloc]init];
    if ([self.sectionTitLe isEqualToString:sExclusion] || [self.sectionTitLe isEqualToString:sInclusion]){
        for (id object in self.packageModel.includeexcludeCollectionArray) {
            IncludeExcludeObject *itinery = [[IncludeExcludeObject alloc]initWithIncludeExcludeDict:object];
            NSString *discription;
            if ([[string uppercaseString] isEqualToString:sInclusion]){
                discription = itinery.includes;
            }else{
                discription = itinery.excludes;
            }
            [mutStringArray addObject:discription];
        }
    }
    if ([self.sectionTitLe isEqualToString:sMeals]){
        for (id object in self.packageModel.mealCollectionArray) {
            MealsObject *mealObj = [[MealsObject alloc]initWithMealsObjectDict:object];
            [mutStringArray addObject:mealObj.mealDescription];
        }
    }
    if ([self.sectionTitLe isEqualToString:sSightSeeing]){
        
         for (id object in self.packageModel.sightseenCollectionArray) {
         SightSeenObject *sightObj = [[SightSeenObject alloc]initWithSightSeenObjectDict:object];
         NSString *mainStr = [NSString stringWithFormat:@"%@",sightObj.cityName];
         if (sightObj.name){
         mainStr = [NSString stringWithFormat:@"%@\n%@",mainStr,sightObj.name];}
         if (sightObj.descriptionName){
         mainStr = [NSString stringWithFormat:@"%@\n%@",mainStr,sightObj.descriptionName];}
         [mutStringArray addObject:mainStr];
         }
    }
    return [mutStringArray copy];
}

- (IBAction)btn_firstBackPress:(id)sender {
    [self jumpToPreviousViewController];
}
- (IBAction)btn_secondBackPress:(id)sender {
    [self jumpToPreviousViewController];
}
- (IBAction)btn_inclusionPress:(id)sender {
    [self setBottomToRight:NO];
}
- (IBAction)btn_exclusionPress:(id)sender {
    [self setBottomToRight:YES];
}

-(void)setBottomToRight:(BOOL)isRight{
    
    [UIView animateWithDuration:1.0 delay:0.0 options:UIViewAnimationOptionCurveLinear animations:^{
//        CGRect frameRect = CGRectMake(isRight ? self.btn_exclusion.frame.origin.x : self.btn_inclusion.frame.origin.x, self.second_view.frame.size.height-5, self.btn_inclusion.frame.size.width, 4);
        self.slidingViewOne.hidden = isRight;
        self.slidingViewTwo.hidden = !isRight;
//        NSArray *array = (isRight) ? [self getArrayForSection:sExclusion] : [self getArrayForSection:sInclusion];
//        [self showArrayOfStringInWebView:array];
        NSString *htmlStr = (isRight) ? [self getStringForSection:sExclusion] : [self getStringForSection:sInclusion];
        NSString *htmlStringObj = [NSString stringWithFormat:@"<span style=\"font-family: Lato-Regular; font-size: 15\">%@</span>", htmlStr];
            [self.web_viewa loadHTMLString:htmlStringObj baseURL:nil];
//        [self showStringInWebView:htmlStr];
    } completion:nil];
}
@end
