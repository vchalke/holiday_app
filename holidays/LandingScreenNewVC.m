//
//  LandingScreenNewVC.m
//  holidays
//
//  Created by Ios_Team on 29/10/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "LandingScreenNewVC.h"
#import "NewNotificationVC.h"
#import <FirebaseAnalytics/FirebaseAnalytics.h>
#import "NetCoreAnalyticsVC.h"
#import <NetCorePush/NetCorePush.h>
#import "Reachability.h"
#import "NetworkHelper.h"
#import "NotificationViewController.h"
#import "AppDelegate.h"
#import <CoreData/CoreData.h>
#import "Thomas_Cook_Holidays-Swift.h"
#import "MoreOptionsVC.h"
#import "SDWebImageDownloader.h"
#import "PDPScreenVC.h"
#import "HolidayLandingScreenVC.h"
#import "BannersTableCell.h"

@interface LandingScreenNewVC ()<BottomStacksViewDelegaete,CustomTopViewDelegaete,UITableViewDataSource,UITableViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource>{
    NSMutableArray *titleArr;
    NSMutableDictionary *payloadListDict;
    
    NSMutableArray *topBannerImgArray;
    NSMutableArray *whatsNewArray;
    NSMutableArray *lmdBannerImgArray;
    NSMutableArray *bankOfferDataArray;
    NSMutableArray *tbImgArray;
    NSMutableArray *hLandingTopImgArray;
    
    NSMutableArray *hLandingSummerImgArray;
    NSMutableArray *hLandingAventureImgArray;
    NSMutableArray *hLandingUpcomImgArray;
    NSMutableArray *hLandingCheeryImgArray;
    NSMutableArray *hLandingPopularImgArray;
    NSMutableArray *hLandingCateImgArray;
    NSMutableArray *hLandingHolidayOneImgArray;
}

@end

@implementation LandingScreenNewVC

- (void)viewDidLoad {
    [super viewDidLoad];
//    titleArr = [[NSMutableArray alloc] init];
    titleArr = [[NSMutableArray alloc] initWithObjects:@"",@"", nil];
    self.topViews.custDelegate = self;
    self.bottomViews.bottomStackDelegate= self;
    [self initializeArray];
    [self initTableCollectionView];
    [self callAllAPIs];
}
-(void)initTableCollectionView{
    [self.tableViews registerNib:[UINib nibWithNibName:@"BannersTableCell" bundle:nil]forCellReuseIdentifier:@"BannersTableCell"];
}
-(void)initializeArray{
    topBannerImgArray = [[NSMutableArray alloc]init];
       whatsNewArray= [[NSMutableArray alloc]init];
       lmdBannerImgArray= [[NSMutableArray alloc]init];
       tbImgArray = [[NSMutableArray alloc]init];
       bankOfferDataArray = [[NSMutableArray alloc]init];
     hLandingTopImgArray = [[NSMutableArray alloc]init];
    
     hLandingSummerImgArray = [[NSMutableArray alloc]init];
    hLandingAventureImgArray = [[NSMutableArray alloc]init];
    hLandingUpcomImgArray = [[NSMutableArray alloc]init];
    hLandingCheeryImgArray = [[NSMutableArray alloc]init];
    hLandingPopularImgArray = [[NSMutableArray alloc]init];
    hLandingCateImgArray = [[NSMutableArray alloc]init];
    hLandingHolidayOneImgArray = [[NSMutableArray alloc]init];
}
-(void)callAllAPIs{
    dispatch_async(dispatch_get_main_queue(), ^(void) {
            [self getTokenIDInMaster];
            [self performSelector:@selector(getAllBannerDataAPI) withObject:nil afterDelay:1.0];
            [self performSelector:@selector(getTravelBlogDataAPI) withObject:nil afterDelay:1.0];
            [self performSelector:@selector(getBankOffersDataAPI) withObject:nil afterDelay:1.0];
            
    //        [self performSelector:@selector(getDataFromCustomerProfileAPI) withObject:nil afterDelay:1.0];
        });
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    self.navigationController.navigationBar.hidden = YES;
//    NSLog(@"AppUtility.setUUID %@",AppUtility.setUUID);
    [self disableBackSwipeAllow:NO];
    NSLog(@"User Booking Number %@",[[NSUserDefaults standardUserDefaults]valueForKey:@"mobicleNumber"]);
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    self.navigationController.navigationBar.hidden = YES;
    [self.bottomViews buttonSelected:0];
}

#pragma mark - Table view data source and delegated
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [titleArr count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BannersTableCell* infoCell = (BannersTableCell*)[tableView dequeueReusableCellWithIdentifier:@"BannersTableCell"];
    infoCell.lbl_BannerName.text = [titleArr objectAtIndex:indexPath.row];
    [infoCell setupCollectionView];
    return infoCell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 250;
}

#pragma mark - BottomStacks ViewDelegaete Delegate
-(void)homeButtonClick{
    
}
-(void)bookingButtonClick{
    [self jumpToControllerfromViewNum:0 toViewNum:1];
}
-(void)profileButtonClick{
    [self jumpToControllerfromViewNum:0 toViewNum:2];
}
-(void)notificationButtonClick{
    payloadListDict =  [NSMutableDictionary dictionary];
    [payloadListDict setObject:@"yes" forKey:@"s^NOTIFICATIONS"];
    [self netCoreHomePageLogEvent];
    [self jumpToControllerfromViewNum:0 toViewNum:3];
}

-(void)moreButtonClick{
    [self jumpToControllerfromViewNum:0 toViewNum:4];
}

#pragma mark - Jump From Bottom Options
-(void)jumpToControllerfromViewNum:(NSInteger)firstVCNum toViewNum:(NSInteger)secVCNum{
      CATransition *transition = [CATransition animation];
    transition.duration = 0.5;
      transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault];
      transition.type = kCATransitionMoveIn;
    transition.subtype = (firstVCNum < secVCNum) ? kCATransitionFromRight : kCATransitionFromLeft;
    [self.navigationController.view.layer addAnimation:transition forKey:nil];
      self.navigationController.navigationBarHidden = NO;
    switch (secVCNum) {
        case 0:{
            LandingScreenNewVC *landingVC=[[LandingScreenNewVC alloc]initWithNibName:@"LandingScreenNewVC" bundle:nil];
            [self.navigationController pushViewController:landingVC animated:YES];
            break;
        }
        case 1:{
            NSString *bookingMobNumber = [[NSUserDefaults standardUserDefaults]valueForKey:@"mobicleNumber"];
            if (bookingMobNumber == nil || [bookingMobNumber isEqualToString:@""] || bookingMobNumber.length==0){
               SOTCLoginViewController *loginSotc = [[SOTCLoginViewController alloc] initWithNibName:@"BaseViewController" bundle:nil];
                [self.navigationController pushViewController:loginSotc animated:YES];
            }else{
                UserBookingHomeViewController *loginSotc = [[UserBookingHomeViewController alloc] initWithNibName:@"BaseViewController" bundle:nil];
                [self.navigationController pushViewController:loginSotc animated:YES];
            }
            break;
        }
        case 2:{
            CustomerProfileVC *profileVC=[[CustomerProfileVC alloc]initWithNibName:@"CustomerProfileVC" bundle:nil];
            [self.navigationController pushViewController:profileVC animated:YES];
            break;
        }
        case 3:{
            NewNotificationVC *notifVC=[[NewNotificationVC alloc]initWithNibName:@"NewNotificationVC" bundle:nil];
//            NotificationViewController *notifVC = [[NotificationViewController alloc]initWithNibName:@"BaseViewController" bundle:nil];
            [self.navigationController pushViewController:notifVC animated:YES];
            break;}
        case 4:{
            MoreOptionsVC *optionVC=[[MoreOptionsVC alloc]initWithNibName:@"MoreOptionsVC" bundle:nil];
            [self.navigationController pushViewController:optionVC animated:YES];
            break;}
        default:
           break;
    }
}

#pragma mark - API Calling
-(void)getAllBannerDataAPI{
    NetworkHelper *helper = [NetworkHelper sharedHelper];
    if ([self checkInternet]){
        NSString *pathParam = @"";
        NSDictionary *headerDict = [CoreUtility getHeaderDict];
        [helper getResponseWithRequestType:@"" withQueryParam:@"" withPathParam:pathParam withJsonParam:nil withHeaders:headerDict withUrl:kGetbannerServiceURL success:^(NSDictionary *responseDict){
            NSLog(@"Response Dict get Banner Service API : %@",responseDict);
            if (responseDict.count != 0) {
                NSArray *responseArray = [responseDict valueForKey:@"bannerDetails"];
                 for (int i =0; i<[responseArray count]; i++){
                  WhatsNewObject *newObj = [[WhatsNewObject alloc] initWithImgArray:[responseArray objectAtIndex:i]];
                    NSLog(@"newObj.bannerLevelName %@",newObj.bannerLevelName);
                    NSString *newBannerLevelName = [newObj.bannerLevelType stringByReplacingOccurrencesOfString:@" " withString:@""];
//                    NSString *newBannerLevelName = [newObj.bannerLevelType stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                    if ([newBannerLevelName.uppercaseString isEqualToString:levelNumberTwo.uppercaseString] && [newObj.bannerPageName.uppercaseString isEqualToString:@"HOME"]){
                        [whatsNewArray addObject:newObj];
                        if (i==0){
                            [titleArr addObject:newObj.bannerLevelName];
                        }
                    }else if ([newBannerLevelName.uppercaseString isEqualToString:levelNumberThree.uppercaseString] && [newObj.bannerPageName.uppercaseString isEqualToString:@"HOME"]){
                        [lmdBannerImgArray addObject:newObj];
                        if (i==0){
                            [titleArr addObject:newObj.bannerLevelName];
                        }
                    }else if ([newBannerLevelName.uppercaseString isEqualToString:levelNumberOne.uppercaseString] && [newObj.bannerPageName.uppercaseString isEqualToString:@"HOME"]){
                        [topBannerImgArray addObject:newObj];
                    }
                    else if ([newBannerLevelName.uppercaseString isEqualToString:levelNumberOne.uppercaseString] && [newObj.bannerPageName.uppercaseString isEqualToString:@"HOLIDAYS"] && newObj.orderOnThePage != 5){
                        [hLandingSummerImgArray addObject:newObj];
                    }
                    else if ([newBannerLevelName.uppercaseString isEqualToString:levelNumberOne.uppercaseString] && [newObj.bannerPageName.uppercaseString isEqualToString:@"HOLIDAYS-1"] && newObj.orderOnThePage != 5){
                        [hLandingHolidayOneImgArray addObject:newObj];
                    }
                    else if ([newBannerLevelName.uppercaseString isEqualToString:levelNumberTwo.uppercaseString] && [newObj.bannerPageName.uppercaseString isEqualToString:@"HOLIDAYS"]){
                        [hLandingAventureImgArray addObject:newObj];
                    }else if ([newBannerLevelName.uppercaseString isEqualToString:levelNumberThree.uppercaseString] && [newObj.bannerPageName.uppercaseString isEqualToString:@"HOLIDAYS"]){
                        [hLandingCateImgArray addObject:newObj];
                    }else if ([newBannerLevelName.uppercaseString isEqualToString:levelNumberFour.uppercaseString] && [newObj.bannerPageName.uppercaseString isEqualToString:@"HOLIDAYS"]){
                        [hLandingUpcomImgArray addObject:newObj];
                    }else if ([newBannerLevelName.uppercaseString isEqualToString:levelNumberOne.uppercaseString] && [newObj.bannerPageName.uppercaseString isEqualToString:@"HOLIDAYS"] && newObj.orderOnThePage == 5){
                        [hLandingCheeryImgArray addObject:newObj];
                    }else if ([newBannerLevelName.uppercaseString isEqualToString:levelNumberFive.uppercaseString] && [newObj.bannerPageName.uppercaseString isEqualToString:@"HOLIDAYS"]){
                        [hLandingPopularImgArray addObject:newObj];
                    }else{
                        NSLog(@"Not Added In ARRAY");
                    }
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.tableViews reloadData];
                   });
            }else{
                
            }
        }
             failure:^(NSError *error){
            dispatch_async(dispatch_get_main_queue(), ^(void){
                [self showButtonsInAlertWithTitle:@"Alert" msg:@"some error occurred" style:UIAlertControllerStyleAlert buttArray:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString * str) {
                    if ([str isEqualToString:@"Ok"]){
                        
                    }
                }];
                NSLog(@"Response Dict : %@",error);
            });
        }];
    }else{
        [self showButtonsInAlertWithTitle:@"Alert" msg:@"" style:UIAlertControllerStyleAlert buttArray:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString * str) {
            if ([str isEqualToString:@"Ok"]){
                
            }
        }];
    }
}


-(void)getBankOffersDataAPI{
    NetworkHelper *helper = [NetworkHelper sharedHelper];
    if ([self checkInternet]){
        NSString *pathParam = @"";
        NSDictionary *headerDict = [CoreUtility getHeaderDict];
        [helper getResponseWithRequestType:@"" withQueryParam:@"" withPathParam:pathParam withJsonParam:nil withHeaders:headerDict withUrl:KGetByPackageId success:^(NSDictionary *responseDict){
            NSLog(@"Response Dict get Banner Service API : %@",responseDict);
            if (responseDict.count != 0) {
                if ([[[responseDict valueForKey:@"errormsg"] uppercaseString] isEqualToString:@"SUCCESS"]){
                    if ([responseDict valueForKey:@"result"]>0){
                        bankOfferDataArray =[responseDict valueForKey:@"result"];
                    }
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.tableViews reloadData];
                   });
            }else{
                
            }
        }
           failure:^(NSError *error){
            dispatch_async(dispatch_get_main_queue(), ^(void){
                [self showButtonsInAlertWithTitle:@"Alert" msg:@"some error occurred" style:UIAlertControllerStyleAlert buttArray:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString * str) {
                    if ([str isEqualToString:@"Ok"]){
                        
                    }
                }];
            });
        }];
    }else{
        [self showButtonsInAlertWithTitle:@"Alert" msg:@"" style:UIAlertControllerStyleAlert buttArray:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString * str) {
            if ([str isEqualToString:@"Ok"]){
                
            }
        }];
    }
}

-(void)getTravelBlogDataAPI{
    NetworkHelper *helper = [NetworkHelper sharedHelper];
    if ([self checkInternet]) {
        NSDictionary *headerDict = [CoreUtility getHeaderDict];
        [helper getResponseWithRequestType:@"" withQueryParam:@"" withPathParam:@"" withJsonParam:nil withHeaders:headerDict withUrl:kTravelBlogURL success:^(NSDictionary *responseDict)
         {
            NSLog(@"Response Dict get Travel Blog API : %@",responseDict);
            if (responseDict.count != 0) {
                NSArray *array = (NSArray *)responseDict;
                tbImgArray = [array copy] ;
                [titleArr addObject:@"Travel Blog"];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.tableViews reloadData];
                   });
            }else{
                
            }
        }
                                   failure:^(NSError *error){
            dispatch_async(dispatch_get_main_queue(), ^(void){
                [self showButtonsInAlertWithTitle:@"Alert" msg:@"some error occurred" style:UIAlertControllerStyleAlert buttArray:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString * str) {
                    if ([str isEqualToString:@"Ok"]){
                        
                    }
                }];
                NSLog(@"Response Dict : %@",error);
            });
        }];
    }else{
        [self showButtonsInAlertWithTitle:@"Alert" msg:@"" style:UIAlertControllerStyleAlert buttArray:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString * str) {
            if ([str isEqualToString:@"Ok"]){
                
            }
        }];
    }
}


-(void)netCoreHomePageLogEvent{
    
    if(![payloadListDict objectForKey:@"s^BANNER"])
    {
        [payloadListDict setObject:@"" forKey:@"s^BANNER"];
    }
    if (![payloadListDict objectForKey:@"s^SEARCH"])
    {
        [payloadListDict setObject:@"no" forKey:@"s^SEARCH"];
    }
    if (![payloadListDict objectForKey:@"s^DEALS"])
    {
        [payloadListDict setObject:@"no" forKey:@"s^DEALS"];
    }
    if (![payloadListDict objectForKey:@"s^DRAWER"])
    {
        [payloadListDict setObject:@"no" forKey:@"s^DRAWER"];
    }
    if (![payloadListDict objectForKey:@"s^NOTIFICATIONS"])
    {
        [payloadListDict setObject:@"no" forKey:@"s^NOTIFICATIONS"];
    }
    if (![payloadListDict objectForKey:@"s^INT_HOLIDAYS"])
    {
        [payloadListDict setObject:@"no" forKey:@"s^INT_HOLIDAYS"];
    }
    if (![payloadListDict objectForKey:@"s^INDIA_HOLIDAYS"])
    {
        [payloadListDict setObject:@"no" forKey:@"s^INDIA_HOLIDAYS"];
    }
    if (![payloadListDict objectForKey:@"s^FLIGHTS"])
    {
        // [payloadList setObject:@"no" forKey:@"s^FLIGHTS"]; //09-04-2018
        [payloadListDict setObject:[NSNull null] forKey:@"s^FLIGHTS"];  //09-04-2018
    }
    if (![payloadListDict objectForKey:@"s^FOREX"])
    {
        [payloadListDict setObject:@"no" forKey:@"s^FOREX"];
    }
    if (![payloadListDict objectForKey:@"s^SELF_SERVICE"])
    {
        [payloadListDict setObject:@"no" forKey:@"s^SELF_SERVICE"];
    }
    if (![payloadListDict objectForKey:@"s^SOURCE"])
    {
        [payloadListDict setObject:@"App" forKey:@"s^SOURCE"];
    }
    
    //[NetCoreAnalyticsVC getNetCorePushNotificationDetailDictionary:payloadList withPayloadCount:102]; //28-02-2018
    
    [NetCoreAnalyticsVC getNetCorePushNotificationDetailDictionary:payloadListDict withPayloadCount:118]; //28-02-2018
}

@end
