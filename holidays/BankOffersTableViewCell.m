//
//  BankOffersTableViewCell.m
//  holidays
//
//  Created by Kush_Team on 29/07/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "BankOffersTableViewCell.h"

@implementation BankOffersTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setBankOffer{
    self.bankOfferColecttion.delegate = self;
    self.bankOfferColecttion.dataSource = self;
    [self.bankOfferColecttion registerNib:[UINib nibWithNibName:@"gridViewCollectionCell" bundle:nil] forCellWithReuseIdentifier:@"gridViewCollectionCell"];
}

@end
