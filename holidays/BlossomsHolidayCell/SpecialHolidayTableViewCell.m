//
//  SpecialHolidayTableViewCell.m
//  holidays
//
//  Created by Kush_Tech on 16/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "SpecialHolidayTableViewCell.h"

@implementation SpecialHolidayTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    

}
-(void)setupCollectionViewTitle:(NSString*)title subTitle:(NSString*)subTitle{
//    WhatsNewObject *object = [self.cherryBlossomArray objectAtIndex:0];
//    self.lbl_bannerTitle.text = (object.bannerLevelName) ? object.bannerLevelName : @"Cherry Blossom Holidays";
    self.lbl_bannerTitle.text = title;
//    [self.btn_viewAllReposit setTitle:[NSString stringWithFormat:@"View All %@",(title.length > 0) ? title : @"Holidays"] forState:UIControlStateNormal];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    [self.collectionView registerNib:[UINib nibWithNibName:@"gridViewCollectionCell" bundle:nil] forCellWithReuseIdentifier:@"gridViewCollectionCell"];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)btn_ViewAllPress:(id)sender {
//    [self.delegate jumpToParticularHoliday:0];
    [self.delegate jumpToTheWebViewFromCherryBlossom:self.buttonObject.redirectUrl withTitle:self.buttonObject.bannerName withObject:self.buttonObject];
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [self.cherryBlossomArray count]; //4;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    gridViewCollectionCell *cell = (gridViewCollectionCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"gridViewCollectionCell" forIndexPath:indexPath];
    cell.view_belowImgView.hidden = YES;
//    NSArray *imgArray = [NSArray arrayWithObjects:@"natureFour",@"natureTwo",@"natureOne",@"natureThree", nil];
//       cell.img_MainView.image = [UIImage imageNamed:[imgArray objectAtIndex:indexPath.row]];
    [cell showCherryBlossom:[self.cherryBlossomArray objectAtIndex:indexPath.row]];
    return  cell;
    
}
-(void)setButtonDataUsingArray:(NSArray*)dataArray{
    for (id object in dataArray){
        WhatsNewObject *wObject = object;
        if (wObject.bannerOrder==2 && wObject.orderOnThePage==1){
            [self.btn_viewAllReposit setTitle:[NSString stringWithFormat:@"%@",wObject.bannerName] forState:UIControlStateNormal];
            self.buttonObject = wObject;
        }
    }
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return CGSizeMake(self.collectionView.frame.size.width*0.5, self.collectionView.frame.size.height*0.95);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
//    [self.categoryDelegate selectedCategory:[categoryArray objectAtIndex:indexPath.row] andSearchText:[searchTextArray objectAtIndex:indexPath.row]];
    WhatsNewObject *obj = [self.cherryBlossomArray objectAtIndex:indexPath.row];
    [self.delegate jumpToTheWebViewFromCherryBlossom:obj.redirectUrl withTitle:obj.bannerName withObject:obj];
}
@end
