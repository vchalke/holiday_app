//
//  SpecialHolidayTableViewCell.h
//  holidays
//
//  Created by Kush_Tech on 16/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "gridViewCollectionCell.h"
#import "WhatsNewObject.h"
NS_ASSUME_NONNULL_BEGIN
@protocol SpecialHolidayTableViewCellDelegaete <NSObject>
@optional
-(void)jumpToTheWebViewFromCherryBlossom:(NSString *)webUrlString withTitle:(NSString *)webViewTitle withObject:(WhatsNewObject *)newObject;
-(void)jumpToParticularHoliday:(NSInteger)tagValue;
@end
@interface SpecialHolidayTableViewCell : UITableViewCell<UICollectionViewDelegate,UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UIButton *btn_viewAllReposit;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UILabel *lbl_bannerTitle;
@property (weak, nonatomic)NSArray *cherryBlossomArray;
@property (weak, nonatomic)WhatsNewObject *buttonObject;
@property (nonatomic, weak) id <SpecialHolidayTableViewCellDelegaete> delegate;
-(void)setupCollectionViewTitle:(NSString*)title subTitle:(NSString*)subTitle;
-(void)setButtonDataUsingArray:(NSArray*)dataArray;
@end

NS_ASSUME_NONNULL_END
