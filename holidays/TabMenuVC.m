//
//  TabMenuVC.m
//  holidays
//
//  Created by Pushpendra Singh on 12/08/15.
//  Copyright (c) 2015 Mobicule. All rights reserved.
//

#import "TabMenuVC.h"
#import "PSPageMenu.h"
#import "PackageListVC.h"
#import "FilterPackageVC.h"
#import "OverviewVC.h"
#import "ItineraryVC.h"
#import "DetailsVC.h"
#import "MapTabViewController.h"
#import "BookNowViewController.h"
#import "KLCPopup.h"
#import "RequestForCallPopUp.h"
#import "UIViewController+MJPopupViewController.h"
//#import <NetCorePush/NetCorePush.h>
#import "NetCoreAnalyticsVC.h"
#import "AppDelegate.h"
#import "itinerayScreenVC.h"
//    Vijay Added
//#import "SRPScreenVC.h"

@interface TabMenuVC ()<PSPageMenuDelegate>
{
    PackageListVC *packageListVC;
    MapTabViewController *mapViewVC;
     KLCPopup *customePopUp;
    OverviewVC *overviewVC;
//    Vijay Added
//    itinerayScreenVC *overviewVC;
    UITextField *emailIdText;
    MobileNumberPopUpViewController *mobileNumberViewController;
     NSMutableDictionary *payloadListSortBy, *payloadListWishList, *payLoadSearchResult, *payLoadViewPageDetail ;
    
//    Vijay Added
//    SRPScreenVC *packageListVC;
}

@property (nonatomic) PSPageMenu *pageMenu;
@end

@implementation TabMenuVC{
    id tmpID;
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [CoreUtility reloadRequestID];

    
    [[NSBundle mainBundle]loadNibNamed:@"TabMenuVC" owner:self options:nil];
    [super addViewInBaseView:self.TabMenuView];
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                            selector:@selector(packageFilterDidComplete:)
                                                name:kDataDidFilteredNotification
                                              object:nil];
    
    NSMutableArray *controllerArray = [NSMutableArray array];
    
    BaseViewController *_baseViewObj=[[BaseViewController alloc]initWithNibName:@"BaseViewController" bundle:nil];
    _baseViewObj.delegate=self;
    
    
    payloadListSortBy =  [NSMutableDictionary dictionary];
    payloadListWishList = [NSMutableDictionary dictionary];
    payLoadSearchResult = [NSMutableDictionary dictionary];
    payLoadViewPageDetail = [NSMutableDictionary dictionary];
    
    if([_flag isEqualToString:@"Overview"])
    {
        [payLoadViewPageDetail setObject:@"yes" forKey:@"s^OVERVIEW"];
        [self netCoreViewDetailPage];

        
         super.HeaderLabel.text = _packageDetail.strPackageName;
        //super.HeaderLabel.text = @"";
        _buttonBookNow.alpha = 1;
        _buttonCall.alpha = 1;
        _buttonMail.alpha = 1;
        _buttonLike.alpha = 1;
        _buttonShare.alpha = 1;
        [self.TabMenuView bringSubviewToFront:_buttonBookNow];
        
        overviewVC = [[OverviewVC alloc] initWithNibName:@"OverviewVC" bundle:nil];
        overviewVC.packageDetail = self.packageDetail;
        overviewVC.title = @"OVERVIEW";
        overviewVC.completePackageDetail = self.completePackageDetail;
        overviewVC.tabMenuVC=self;
//        Vijay Added
        itinerayScreenVC *itinerayVC = [[itinerayScreenVC alloc] initWithNibName:@"itinerayScreenVC" bundle:nil];
//        ItineraryVC *itinerayVC = [[ItineraryVC alloc] initWithNibName:@"ItineraryVC" bundle:nil];
        itinerayVC.title = @"ITINERARY";
        itinerayVC.holidayPackageDetail = self.packageDetail;

        
        DetailsVC *detailVC = [[DetailsVC alloc] initWithNibName:@"DetailsVC" bundle:nil];
        detailVC.title = @"DETAILS";
        detailVC.completePackageDetail =  self.completePackageDetail;
        detailVC.holidayPackageDetail = self.packageDetail;
        
        [controllerArray addObject:overviewVC];
        [controllerArray addObject:detailVC];
        [controllerArray addObject:itinerayVC];

    }
    else
    {
        
        [payLoadSearchResult setObject:@"yes" forKey:@"s^SORT_BY"];
        
        [self netCoreDataDisplaySearchResult];
        
        
        CGFloat topPadding;
        CGFloat bottomPadding;
        
        if (@available(iOS 11.0, *)) {
            UIWindow *window = UIApplication.sharedApplication.keyWindow;
            topPadding = window.safeAreaInsets.top;
            bottomPadding = window.safeAreaInsets.bottom;
            
        }
        else{
            topPadding = 0;
            bottomPadding = 0;
        }
        
        
        CGRect frame=CGRectMake(([[UIScreen mainScreen]bounds].size.width)-60, topPadding+4, 70, 40);
       // CGRect frame = CGRectMake(250 , 4, 70, 40);
        _totalPackageLabel=[[UILabel alloc]initWithFrame:frame];
        _totalPackageLabel.numberOfLines=2;
        [_totalPackageLabel setFont:[UIFont fontWithName:TITILLUM_REGULAR size:11]];
        
        [_totalPackageLabel setTextColor:[UIColor whiteColor]];
       // totalPackageLabel.text= @"package";

        _totalPackageLabel.text=[NSString stringWithFormat:@"      %@\n%@",_totalPackages,@"Packages"];
        [super.self.view addSubview:_totalPackageLabel];
        
        super.HeaderLabel.text = _headerName;
       
        
        _buttonBookNow.alpha = 0;
        _buttonCall.alpha = 0;
        _buttonMail.alpha = 0;
        _buttonLike.alpha = 0;
        _buttonShare.alpha = 0;
        
        packageListVC = [[PackageListVC alloc] initWithNibName:@"PackageListVC" bundle:nil];
//        Vijay_Added
//        packageListVC = [[SRPScreenVC alloc] initWithNibName:@"SRPScreenVC" bundle:nil];
        packageListVC.title = @"SORT BY";
        packageListVC.completePackageDetail = _completePackageDetail;
        tmpID=packageListVC;
        
        packageListVC.arrayOfHolidays =  _holidayArray;
        
        FilterPackageVC *filterVC = [[FilterPackageVC alloc] initWithNibName:@"FilterPackageVC" bundle:nil];
        filterVC.filterDict = self.filterDict;
        filterVC.arrayOfHolidays = self.holidayArray;
        filterVC.title = @"FILTERS";
        CoreUtility *coreObj=[CoreUtility sharedclassname];
        coreObj.obj=packageListVC;
        
    
        mapViewVC = [[MapTabViewController alloc]initWithNibName:@"MapTabViewController" bundle:nil];
        mapViewVC.title = @"MAP";
        mapViewVC.arrayOfHolidays = _holidayArray;
        mapViewVC.searchedCity = self.searchedCity;
    
    
        [controllerArray addObject:packageListVC];
        [controllerArray addObject:filterVC];
        [controllerArray addObject:mapViewVC];
        
    }
   
    NSDictionary *parameters = @{PSPageMenuOptionMenuItemSeparatorWidth: @(4.3),
                                 PSPageMenuOptionUseMenuLikeSegmentedControl: @(YES),
                                 PSPageMenuOptionMenuItemSeparatorPercentageHeight: @(0.1)
                                 };
    
    // Initialize page menu with controller array, frame, and optional parameters
     if([_flag isEqualToString:@"Overview"]){
         
           _pageMenu = [[PSPageMenu alloc] initWithViewControllers:controllerArray frame:CGRectMake(0.0f, 48.0f, self.TabMenuView.frame.size.width, self.TabMenuView.frame.size.height - 96.0f) options:parameters];
     }
     else
     {
           _pageMenu = [[PSPageMenu alloc] initWithViewControllers:controllerArray frame:CGRectMake(0.0f, 48.0f, self.TabMenuView.frame.size.width, self.TabMenuView.frame.size.height - 48.0f) options:parameters];
     }
  
    
    NSLog(@"_psMenu%@",NSStringFromCGRect(_pageMenu.view.frame));
    [_pageMenu.controllerScrollView setScrollEnabled:YES];
    _pageMenu.identifyDelegate=tmpID;
    _pageMenu.delegate = self; // added on 26-07-2017
    
    //Lastly add page menu as subview of base view controller view
    //or use pageMenu controller in you view hierachy as desired
    
    [self.TabMenuView addSubview:_pageMenu.view];
    
   // _pageMenu.menuScrollView.hidden=YES;
   // _pageMenu.controllerScrollView.hidden=YES;
    
}

#pragma mark - delegate method of PSPageMenu -

- (void)didMoveToPage:(UIViewController *)controller index:(NSInteger)index
{
    
    payLoadSearchResult = [[NSMutableDictionary alloc]init];
    payLoadViewPageDetail = [[NSMutableDictionary alloc]init];
    
    if ([controller isKindOfClass:[packageListVC class]])
    {
       NSLog(@"controller:%@ index:%ld",controller,(long)index);
        
        [payLoadSearchResult setObject:@"yes" forKey:@"s^SORT_BY"];
        
        [self netCoreDataDisplaySearchResult];
    }
    else if ([controller isKindOfClass:[FilterPackageVC class]])
    {
         NSLog(@"controller:%@ index:%ld",controller,(long)index);
        
        [payLoadSearchResult setObject:@"yes" forKey:@"s^FILTERS"];
        
        [self netCoreDataDisplaySearchResult];
    }
    else if ([controller isKindOfClass:[mapViewVC class]])
    {
        [payLoadSearchResult setObject:@"yes" forKey:@"s^MAP"];
        
        [self netCoreDataDisplaySearchResult];

         NSLog(@"controller:%@ index:%ld",controller,(long)index);
    }
    else if ([controller isKindOfClass:[overviewVC class]])
    {
         NSLog(@"controller:%@ index:%ld",controller,(long)index);
        [payLoadViewPageDetail setObject:@"yes" forKey:@"s^OVERVIEW"];
        [self netCoreViewDetailPage];
    }
    else if ([controller isKindOfClass:[DetailsVC class]])
    {
         NSLog(@"controller:%@ index:%ld",controller,(long)index);
        [payLoadViewPageDetail setObject:@"yes" forKey:@"s^DETAILS"];
        [self netCoreViewDetailPage];

    }
//    else if ([controller isKindOfClass:[ItineraryVC class]])
//    Vijay Added
    else if ([controller isKindOfClass:[itinerayScreenVC class]])
    {
         NSLog(@"controller:%@ index:%ld",controller,(long)index);
        [payLoadViewPageDetail setObject:@"yes" forKey:@"s^ITINERARY"];
        [self netCoreViewDetailPage];
    }
}



- (void)willMoveToPage:(UIViewController *)controller index:(NSInteger)index
{
    NSLog(@"controller:%@ index:%ld",controller,(long)index);
    
  /*  if(index == 0 && [controller isEqual:@"PackageListVC"])
    {
        [payLoadSearchResult setObject:@"yes" forKey:@"s^SORT_BY"];
        
        if ([payLoadSearchResult objectForKey:@"s^SORT_BY"])
        {
            [self netCoreDataDisplaySearchResult];
        }
        else
        {
            [payloadListSortBy setObject:@"yes" forKey:@"s^SORT_OPTION"];
            
            if ([payloadListSortBy objectForKey:@"s^SORT_OPTION"])
            {
               // [self netCoreAppPackageSortBy];
            }

        }
    }
    if(index == 1 && [controller isEqual:@"FilterPackageVC"])
    {
        [payLoadSearchResult setObject:@"yes" forKey:@"s^FILTERS"];
        [self netCoreDataDisplaySearchResult];
    }
    if(index == 2 && [controller isEqual:@"MapTabViewController"])
    {
        [payLoadSearchResult setObject:@"yes" forKey:@"s^MAP"];
        [self netCoreDataDisplaySearchResult];
    }
    if(index == 0 && [controller isEqual:@"OverviewVC"])
    {
        [payLoadViewPageDetail setObject:@"yes" forKey:@"s^OVERVIEW"];
        [self netCoreViewDetailPage];
    }
    if(index == 1 && [controller isEqual:@"DetailsVC"])
    {
        [payLoadViewPageDetail setObject:@"yes" forKey:@"s^DETAILS"];
        [self netCoreViewDetailPage];
    }
    if(index == 2 && [controller isEqual:@"ItineraryVC"])
    {
        [payLoadViewPageDetail setObject:@"yes" forKey:@"s^ITINERARY"];
        [self netCoreViewDetailPage];
    }
    */
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)packageFilterDidComplete:(NSNotification *)notification
{
    NSArray *arrayOfData = [notification.userInfo valueForKey:@"data"];
    NSString *totalPackages=[notification.userInfo valueForKey:@"totalPackages"];
    _totalPackageLabel.text=[NSString stringWithFormat:@"      %@\n%@",totalPackages,@"Packages"];

    
    packageListVC.arrayOfHolidays = arrayOfData;
    mapViewVC.arrayOfHolidays=arrayOfData;
    CoreUtility *coreObj=[CoreUtility sharedclassname];
   
    
    if ( coreObj.pageNumber==1) {
         [self.pageMenu moveToPage:0];
    }
    else if(coreObj.pageNumber==2)
         [self.pageMenu moveToPage:2];

   
    
}


- (IBAction)onBookNowButtonClicked:(id)sender {
  
//    if([self.packageDetail.strPackageType caseInsensitiveCompare:@"International"]==NSOrderedSame)
//    {
//        [FIRAnalytics logEventWithName:@"International_holidays_BookNow_PDP" parameters:@{kFIRParameterItemName:self.packageDetail.strPackageName}];
//         [FIRAnalytics logEventWithName:@"International_holidays_BookNow_PDP" parameters:@{kFIRParameterItemName:self.packageDetail.strPackageType}];
//         [FIRAnalytics logEventWithName:@"International_holidays_BookNow_PDP" parameters:@{kFIRParameterItemName:self.packageDetail.strPackageId}];
//    }
//    else
//    {
//        [FIRAnalytics logEventWithName:@"Indian_holidays_BookNow_PDP" parameters:@{kFIRParameterItemName:self.packageDetail.strPackageName}];
//         [FIRAnalytics logEventWithName:@"Indian_holidays_BookNow_PDP" parameters:@{kFIRParameterItemName:self.packageDetail.strPackageType}];
//         [FIRAnalytics logEventWithName:@"Indian_holidays_BookNow_PDP" parameters:@{kFIRParameterItemName:self.packageDetail.strPackageId}];
//    }
    
    
    
    CoreUtility *coreobj=[CoreUtility sharedclassname];
    
    if (coreobj.strPhoneNumber.count>0)
    {
          NSString *phoneNumber = coreobj.strPhoneNumber[0];
        [self netCoreBookNowEventWihtDetails:phoneNumber];
        
        if (coreobj.leadSubmitted)
        {
            BookNowViewController *bookVC = [[BookNowViewController alloc]initWithNibName:@"BaseViewController" bundle:nil];
            bookVC.packageDetail = self.packageDetail;
            [[SlideNavigationController sharedInstance] pushViewController:bookVC animated:NO];
        }
        else
        {
          
            NSMutableDictionary *dictOfData = [[NSMutableDictionary alloc]init];
            [dictOfData setValue:self.packageDetail.strPackageId forKey:@"package_id"];
            [dictOfData setValue:@"" forKey:@"email"];
            [dictOfData setValue:phoneNumber forKey:@"mobile"];
            [dictOfData setValue:self.packageDetail.strPackageSubType forKey:@"sub_product_type"];
            [dictOfData setValue:self.packageDetail.strPackageType forKey:@"request_type"];
            
            
            activityLoadingView = [LoadingView loadingViewInView:self.view.superview.superview withString:@"" andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
            
            ///https://thomascookindia.custhelp.com/cgi-bin/thomascookindia.cfg/php/custom/mobile_app_lead_holiday_ios_app_leads.php?email=narendra_k2@yahoo.com&mobile=9811976670&request_type=Domestic&sub_product_type=GIT&package_id=PKG130755
            
            NSString  *urlString  =KUrlHolidayCreateLead;
            //@"https://thomascookindia.custhelp.com/cgi-bin/thomascookindia.cfg/php/custom/mobile_app_lead_holiday_ios_app_leads.php";
            
            NSString *packageSubType = self.packageDetail.strPackageSubType;
            
            if ([[self.packageDetail.strPackageSubType lowercaseString] isEqualToString:@"fit fixed"])
            {
                packageSubType = @"fit";
            }

            
        NSString *queryParam = [NSString stringWithFormat:@"?mobile=%@&request_type=%@&sub_product_type=%@&package_id=%@",phoneNumber,self.packageDetail.strPackageType,packageSubType,self.packageDetail.strPackageId];
            
            
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
            
            NSString *urlStringTotal = [NSString stringWithFormat:@"%@%@",urlString,queryParam];
            
            NSString * encodedString = [urlStringTotal stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
            
            
            NSURL *url = [NSURL URLWithString:encodedString];
            
            [request setURL:url];
            [request setHTTPMethod:@"GET"];
            
            NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
            [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
              {
                  dispatch_async(dispatch_get_main_queue(), ^{
                      
                      [activityLoadingView removeFromSuperview];
                      
                      NSString *requestReply = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
                      
                      NSLog(@"Response :- %@",requestReply);
                      
                      coreobj.leadSubmitted = YES;
                      
                      BookNowViewController *bookVC = [[BookNowViewController alloc]initWithNibName:@"BaseViewController" bundle:nil];
                      bookVC.packageDetail = self.packageDetail;
                      [[SlideNavigationController sharedInstance] pushViewController:bookVC animated:NO];
                  });
                  
              }] resume];

        }
       
    }
    else
    {
        NSUserDefaults *userData = [NSUserDefaults standardUserDefaults];
        NSString *statusForLogin= [userData valueForKey:kLoginStatus];
        
        mobileNumberViewController = [[MobileNumberPopUpViewController alloc] initWithNibName:@"MobileNumberPopUpViewController" bundle:nil];
        mobileNumberViewController.delegate = self;
        mobileNumberViewController.packageDetail = self.packageDetail;
        mobileNumberViewController.view.frame = CGRectMake(0, 0, 309, 173);
        
        if ([statusForLogin isEqualToString:kLoginSuccess])
        {
            NSString *phoneNo = [userData valueForKey:kPhoneNo];
            mobileNumberViewController.txtViewMobileNumber.text = phoneNo;
        }
        [self presentPopupViewController:mobileNumberViewController animationType:MJPopupViewAnimationFade];
    }
    
     [payLoadViewPageDetail setObject:@"yes" forKey:@"s^BOOK_NOW"];
     [self netCoreViewDetailPage];
    
    

    
}


- (IBAction)onAddToWishListButtonClicked:(id)sender
{
    
    if ([_packageDetail.strPackageType caseInsensitiveCompare:@"international"]==NSOrderedSame)
    {
        [FIRAnalytics logEventWithName:@"International_Holidays_Wishlist_PDP" parameters:@{kFIRParameterItemName:_packageDetail.strPackageType}];
        [FIRAnalytics logEventWithName:@"International_Holidays_Wishlist_PDP" parameters:@{kFIRParameterItemName:_packageDetail.strPackageName}];

    }
    else
    {
        [FIRAnalytics logEventWithName:@"Indian_Holidays_Wishlist_PDP" parameters:@{kFIRParameterItemName:_packageDetail.strPackageType}];
        [FIRAnalytics logEventWithName:@"Indian_Holidays_Wishlist_PDP" parameters:@{kFIRParameterItemName:_packageDetail.strPackageName}];
    
    }
    
    NSUserDefaults *LoginStatus=[NSUserDefaults standardUserDefaults];
    NSString *statusForLogin= [LoginStatus valueForKey:kLoginStatus];
    
    if ([statusForLogin isEqualToString:kLoginSuccess])
    {
        NSUserDefaults *userIdForGooglePlusSignIn=[NSUserDefaults standardUserDefaults];
        NSString *userId=[userIdForGooglePlusSignIn valueForKey:kLoginEmailId];

        activityLoadingView = [LoadingView loadingViewInView:self.view.superview.superview
                                                  withString:@""
                                           andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
        NSArray *arrayImages = _packageDetail.arrayPackageImagePathList;
        NSString *imageUrlString;
        if (arrayImages.count != 0)
        {
            NSDictionary *imageDict = arrayImages[0];
            
            
            NSString* imageName = [[imageDict valueForKey:@"path"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            
            
           imageUrlString = [NSString stringWithFormat:@"https://resources-uatastra.thomascook.in/images/holidays/%@/photos/%@",_packageDetail.strPackageId,imageName];

        }
        NSMutableDictionary *dictOfData = [[NSMutableDictionary alloc]init];
        [dictOfData setValue:_packageDetail.strPackageId forKey:@"pkgId"];
        [dictOfData setValue:_packageDetail.strPackageName forKey:@"pkgName"];
        [dictOfData setValue:userId forKey:@"userId"];
        [dictOfData setValue:imageUrlString forKey:@"pkgImage"];
        [dictOfData setValue:[NSString stringWithFormat:@"%d",_packageDetail.packagePrise] forKey:@"startingPrice"];
        
        if ([super connected]) {
            
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            NetworkHelper *helper = [NetworkHelper sharedHelper];
            NSString *strResponse = [helper getDataFromServerForType:@"webservice" entity:@"wishlist" action:@"add" andUserJson:dictOfData];
            NSString *strStatus = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"status"];
            
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSArray *dataDict = [[JsonSerealizer arrayOfJSonFromJsonData:strResponse] valueForKey:@"data"];
                
                if ([strStatus isEqualToString:kStatusSuccess])
                {
                    if (dataDict.count != 0)
                    {
                        [self showAlertViewWithTitle:@"Alert" withMessage:[NSString stringWithFormat:@"%@ added in Wish List",_packageDetail.strPackageName]];
                    }else
                    {
                        NSString *strMessage = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"message"];
                        [self showAlertViewWithTitle:@"Alert" withMessage:strMessage];
                    }
                    
                    
                }else
                {
                    NSString *messsage = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"message"];
                    if (messsage)
                    {
                        if ([messsage isEqualToString:kVersionUpgradeMessage])
                        {
                            NSDictionary *downloadDict = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"data"];
                            NSString *downloadURL = [downloadDict valueForKey:@"download_url"];
                            [super showAlertViewForVersionUpdateWithUrl:downloadURL];

                        }else
                        {
                            [super showAlertViewWithTitle:@"Alert" withMessage:messsage];
                        }
                    }else
                    {
                        [super showAlertViewWithTitle:@"Alert" withMessage:kMessageSomeErrorTryAfterSometime];
                    }
                    
                }
                
                if ([activityLoadingView isDescendantOfView:self.view.superview.superview])
                {
                    [activityLoadingView removeView];
                    activityLoadingView = nil;
                }
            });
            
        });
        }
        else
        {
            [super showAlertViewWithTitle:@"Alert" withMessage:kMessageNoInternet];
        }

    }
    else
    {
        //[super showAlertViewWithTitle:@"Alert" withMessage:@"Please Login first"];
        
        
        customePopUp = [KLCPopup popupWithContentView:[self popUpViewForEmailID]
                                             showType:KLCPopupShowTypeGrowIn
                                          dismissType:KLCPopupDismissTypeFadeOut
                                             maskType:KLCPopupMaskTypeDimmed
                             dismissOnBackgroundTouch:YES
                                dismissOnContentTouch:NO];
       
        [customePopUp show];
        

        
    }
    
    [self netCoreAddToWishList];
    
}

-(UIView *)popUpViewForEmailID
{
    UIView *popUpView = [UIView new];
    popUpView.frame = CGRectMake(0, 0, 250, 220);
    popUpView.backgroundColor = [UIColor whiteColor];
    popUpView.layer.cornerRadius = 1.0;
    UIImage *image = [UIImage imageNamed: @"logo@2x.png"];
    UIImageView *imageView=[[UIImageView alloc]init];
    [imageView setFrame:CGRectMake(75, 25, 100, 30)];
    [imageView setImage:image];
    [popUpView addSubview:imageView];
    
    emailIdText=[[UITextField alloc]init];
    emailIdText.frame=CGRectMake(12, popUpView.center.y, 230, 30);
    
    emailIdText.layer.masksToBounds=YES;
    emailIdText.layer.borderColor=[[UIColor grayColor]CGColor];
    emailIdText.layer.borderWidth= 1.0f;
    [emailIdText setFont:[UIFont fontWithName:TITILLUM_REGULAR size:14]];
 
   // emailIdText.layer.borderColor=(__bridge CGColorRef _Nullable)([UIColor darkGrayColor]);

   // emailIdText.backgroundColor=[UIColor redColor];
    
    [popUpView addSubview:emailIdText];
    
    UILabel *label=[[UILabel alloc]init];
    [label setFrame:CGRectMake(22, popUpView.center.y-40, 180, 30)];
    label.text=@"Enter your Email-id";
    [popUpView addSubview:label];
    
    UIButton *goBtn=[[UIButton alloc]init];
    goBtn.frame=CGRectMake(85, popUpView.center.y+50, 80, 30);
    [goBtn addTarget:nil action:@selector(GoBtnaction:) forControlEvents:UIControlEventTouchUpInside];
    
    [goBtn setBackgroundImage:[UIImage imageNamed:@"btnFindHoliday@2x.png"] forState:UIControlStateNormal];
    goBtn.backgroundColor=[UIColor orangeColor];
    [goBtn setTitle:@"Go" forState:UIControlStateNormal];
    [popUpView addSubview: goBtn];
    
    return popUpView;
    
    
}


-(IBAction)GoBtnaction:(id)sender{
   
    if([self.packageDetail.strPackageType caseInsensitiveCompare:@"International"]==NSOrderedSame)
    {
        [FIRAnalytics logEventWithName:@"International_holidays_BookNow_Go_Click_PDP" parameters:@{kFIRParameterItemName:self.packageDetail.strPackageName}];
        [FIRAnalytics logEventWithName:@"International_holidays_BookNow_Go_Click_PDP" parameters:@{kFIRParameterItemName:self.packageDetail.strPackageType}];
        [FIRAnalytics logEventWithName:@"International_holidays_BookNow_Go_Click_PDP" parameters:@{kFIRParameterItemName:self.packageDetail.strPackageId}];

    }
    else
    {
        [FIRAnalytics logEventWithName:@"Indian_holidays_BookNow_Go_Click_PDP" parameters:@{kFIRParameterItemName:self.packageDetail.strPackageName}];
        [FIRAnalytics logEventWithName:@"Indian_holidays_BookNow_Go_Click_PDP" parameters:@{kFIRParameterItemName:self.packageDetail.strPackageType}];
        [FIRAnalytics logEventWithName:@"Indian_holidays_BookNow_Go_Click_PDP" parameters:@{kFIRParameterItemName:self.packageDetail.strPackageId}];
    }
    NSString *emailId=emailIdText.text;
    NSUserDefaults *userIdForGooglePlusSignIn=[NSUserDefaults standardUserDefaults];
   [userIdForGooglePlusSignIn setValue:emailId forKey:kLoginEmailId];
    
NSString* userId=[userIdForGooglePlusSignIn valueForKey:kLoginEmailId];
    
    activityLoadingView = [LoadingView loadingViewInView:self.view.superview.superview
                                              withString:@""
                                       andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
    NSArray *arrayImages = _packageDetail.arrayPackageImagePathList;
    NSString *imageUrlString;
    if (arrayImages.count != 0)
    {
        NSDictionary *imageDict = arrayImages[0];
        
        
        NSString* imageName = [[imageDict valueForKey:@"path"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        
        imageUrlString = [NSString stringWithFormat:@"https://resources-uatastra.thomascook.in/images/holidays/%@/photos/%@",_packageDetail.strPackageId,imageName];
    }
    NSMutableDictionary *dictOfData = [[NSMutableDictionary alloc]init];
    [dictOfData setValue:_packageDetail.strPackageId forKey:@"pkgId"];
    [dictOfData setValue:_packageDetail.strPackageName forKey:@"pkgName"];
    [dictOfData setValue:userId forKey:@"userId"];
    [dictOfData setValue:imageUrlString forKey:@"pkgImage"];
    [dictOfData setValue:[NSString stringWithFormat:@"%d",_packageDetail.packagePrise] forKey:@"startingPrice"];
    
    if ([super connected]) {
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            NetworkHelper *helper = [NetworkHelper sharedHelper];
            NSString *strResponse = [helper getDataFromServerForType:@"webservice" entity:@"wishlist" action:@"add" andUserJson:dictOfData];
            NSString *strStatus = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"status"];
            
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSArray *dataDict = [[JsonSerealizer arrayOfJSonFromJsonData:strResponse] valueForKey:@"data"];
                
                if ([strStatus isEqualToString:kStatusSuccess])
                {
                    if (dataDict.count != 0)
                    {
                        [self showAlertViewWithTitle:@"Alert" withMessage:[NSString stringWithFormat:@"%@ added in Wish List",_packageDetail.strPackageName]];
                    }else
                    {
                        NSString *strMessage = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"message"];
                        [self showAlertViewWithTitle:@"Alert" withMessage:strMessage];
                    }
                    
                    
                }else
                {
                    NSString *messsage = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"message"];
                    if (messsage)
                    {
                        if ([messsage isEqualToString:kVersionUpgradeMessage])
                        {
                            NSDictionary *downloadDict = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"data"];
                            NSString *downloadURL = [downloadDict valueForKey:@"download_url"];
                            [super showAlertViewForVersionUpdateWithUrl:downloadURL];
                            
                        }else
                        {
                            [super showAlertViewWithTitle:@"Alert" withMessage:messsage];
                        }
                    }else
                    {
                        [super showAlertViewWithTitle:@"Alert" withMessage:kMessageSomeErrorTryAfterSometime];
                    }
                    
                }
                
                if ([activityLoadingView isDescendantOfView:self.view.superview.superview])
                {
                    [activityLoadingView removeView];
                    activityLoadingView = nil;
                }
            });
            
        });
    }
    else
    {
        [super showAlertViewWithTitle:@"Alert" withMessage:kMessageNoInternet];
    }

    
}

- (IBAction)onEmailButtonClicked:(id)sender {
   
    if ([_packageDetail.strPackageType caseInsensitiveCompare:@"international"]==NSOrderedSame)
    {
        [FIRAnalytics logEventWithName:@"International_Holidays_Email_PDP" parameters:@{kFIRParameterItemID:_packageDetail.strPackageName}];
        [FIRAnalytics logEventWithName:@"International_Holidays_Email_PDP" parameters:@{kFIRParameterItemID:_packageDetail.strPackageType}];
    }
    else{
        [FIRAnalytics logEventWithName:@"Indian_Holidays_Email_PDP" parameters:@{kFIRParameterItemID:_packageDetail.strPackageName}];
        [FIRAnalytics logEventWithName:@"Indian_Holidays_Email_PDP" parameters:@{kFIRParameterItemID:_packageDetail.strPackageType}];
    }
   
    RequestForCallPopUp *ReqViewController = [[RequestForCallPopUp alloc] initWithNibName:@"RequestForCallPopUp" bundle:nil];
    ReqViewController.packageDetail = self.packageDetail;
    ReqViewController.view.frame = CGRectMake(0, 0, 300, self.TabMenuView.frame.size.height - 100);
    [self presentPopupViewController:ReqViewController animationType:MJPopupViewAnimationFade];
    
    [payLoadViewPageDetail setObject:@"yes" forKey:@"s^EMAIL_FORM"];
    [self netCoreViewDetailPage];
}

- (IBAction)onCallUsButtonClicked:(id)sender {
    
    CustomIOSAlertView *alertView = [[CustomIOSAlertView alloc] init];
    CoreUtility *coreObj=[CoreUtility sharedclassname];
    coreObj.statusWhichPopUpCalled=0;
    [alertView setContainerView:[self createDemoView]];
    
    
    // Modify the parameters
    [alertView setButtonTitles:[NSMutableArray arrayWithObjects:@"Close1", @"Close2", @"Close3", nil]];
    [alertView setDelegate:self];
    
    // You may use a Block, rather than a delegate.
    [alertView setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
        NSLog(@"Block: Button at position %d is clicked on alertView %d.", buttonIndex, (int)[alertView tag]);
        [alertView close];
    }];
    
    [alertView setUseMotionEffects:true];
    
    // And launch the dialog
    [alertView show];
    
//    NSString *phNo = @"18002000464";
//    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",phNo]];
//
//    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
//        [[UIApplication sharedApplication] openURL:phoneUrl];
//    } else
//    {
//        UIAlertView * calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Call facility is not available!!!" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
//        [calert show];
//    }
    
    [payLoadViewPageDetail setObject:@"yes" forKey:@"s^CALL"];
    [self netCoreViewDetailPage];
}

- (IBAction)onShareButtonClicked:(id)sender {
  
    if([_packageDetail.strPackageType caseInsensitiveCompare:@"International"]==NSOrderedSame)
    {
        [FIRAnalytics logEventWithName:@"International_Holidays_Share_PDP" parameters:@{kFIRParameterItemName:[NSString stringWithFormat:@"Package Name: %@,Package Type: %@,Package ID: %@",_packageDetail.strPackageName,_packageDetail.strPackageType,_packageDetail.strPackageId]}];
    }
    else
    {
        [FIRAnalytics logEventWithName:@"Indian_Holidays_Share_PDP" parameters:@{kFIRParameterItemName:[NSString stringWithFormat:@"Package Name: %@,Package Type: %@,Package ID: %@",_packageDetail.strPackageName,_packageDetail.strPackageType,_packageDetail.strPackageId]}];

    }
    
    NSString *shearingText = [NSString stringWithFormat:@"Package Name :%@ \nPackage Price: %d ",_packageDetail.strPackageName,_packageDetail.packagePrise];
    NSURL *shareUrl = [NSURL URLWithString:@"http://www.thomascook.in"];
    //NSArray *imageArray =_packageDetail.arrayPackageImagePathList;
    [self shareText:shearingText andImageArray:nil andUrl:shareUrl];
    
  

}

- (void)shareText:(NSString *)text andImageArray:(NSArray *)imageArray andUrl:(NSURL *)url
{
    NSMutableArray *sharingItems = [NSMutableArray new];
    
    if (text) {
        [sharingItems addObject:text];
    }
    if (url) {
        [sharingItems addObject:url];
    }
    if (imageArray.count != 0)
    {
        for (int i = 0; i<imageArray.count; i++)
        {
            NSString *imageUrl = [imageArray objectAtIndex:i];
            UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imageUrl]]];
            if (image) {
                [sharingItems addObject:image];
            }
            
        }
    }
    
    UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];
    [self presentViewController:activityController animated:YES completion:nil];
}
#pragma mark -Mobile Number Pop Up Delegate

- (void)goButtonClickedWithMobileNumber:(NSString *)mobileNumber
{
        [self netCoreBookNowEventWihtDetails:mobileNumber];
    
      [mobileNumberViewController dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    
      AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
      [appDelegate registerWithCMSServer];
    
        BookNowViewController *bookVC = [[BookNowViewController alloc]initWithNibName:@"BaseViewController" bundle:nil];
        bookVC.packageDetail = self.packageDetail;
        [[SlideNavigationController sharedInstance] pushViewController:bookVC animated:YES];
    
}

-(void)backButtonAction{
  
    if (overviewVC.backBtnFromOverview==YES) {
        [overviewVC.imageViewInTable removeFromSuperview];
        overviewVC.backBtnFromOverview=NO;
    }
    else
    {
        [[SlideNavigationController sharedInstance] popViewControllerAnimated:YES];
    }
    
    
    // OverviewVC *overViewCont=(OverviewVC *)_tabMenuVCView;
    //  [overViewCont.imageViewInTable removeFromSuperview];
//    OverviewVC 
//    [OverviewVC.imageViewInTable removeFromSuperview];
    
    
}

-(void)setLabel:(NSString*)PackageCount{

    _totalPackageLabel.text=PackageCount;
}

#pragma mark - Pop VIew For Call Us
- (void)customIOS7dialogButtonTouchUpInside: (CustomIOSAlertView *)alertView clickedButtonAtIndex: (NSInteger)buttonIndex
{
    NSLog(@"Delegate: Button at position %d is clicked on alertView %d.", (int)buttonIndex, (int)[alertView tag]);
    [alertView close];
}

- (UIView *)createDemoView
{
    UIView *demoView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 290, 200)];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 30, 30)];
    [imageView setImage:[UIImage imageNamed:@"customer_support.png"]];
    UILabel *lblCallus=[[UILabel alloc]initWithFrame:CGRectMake(50, 10, 200, 40)];
    
    //Old Contact Number
    //lblCallus.text=@"1800 2099 100";
    
    //New Contact Number
    lblCallus.text= @"8652908370"; //@"18002000464,7039003560"; //new mobile number 4th Nov 2019
    
    [demoView addSubview:lblCallus];
    [demoView addSubview:imageView];
    
    
    return demoView;
}

#pragma mark netCore logEvent

//App_Package_SortBy

-(void)netCoreAppPackageSortBy
{
    if (_payloadDict !=nil)
    {
        NSString *destination = [_payloadDict objectForKey:@"s^DESTINATION"];
        
        if (![payloadListSortBy objectForKey:@"s^DESTINATION"])
        {
            [payloadListSortBy setObject:destination forKey:@"s^DESTINATION"];
        }
    }
    
    if (![payloadListSortBy objectForKey:@"s^SORT_OPTION"])
    {
          [payloadListSortBy setObject:@"no" forKey:@"s^SORT_OPTION"];
    }
    [payloadListSortBy setObject:@"" forKey:@"s^BUDGET"];
    [payloadListSortBy setObject:@"App" forKey:@"s^SOURCE"];
    
    [NetCoreAnalyticsVC getNetCorePushNotificationDetailDictionary:payloadListSortBy withPayloadCount:113];
}

//Add to Wishlist

-(void)netCoreAddToWishList
{
    
    if (_payloadDict !=nil)
    {
        NSString *destination = [_payloadDict objectForKey:@"s^DESTINATION"];

        if (![payloadListWishList objectForKey:@"s^DESTINATION"])
        {
            [payloadListWishList setObject:destination forKey:@"s^DESTINATION"];
        }
    }
    
    [payloadListWishList setObject:[NSString stringWithFormat:@"%@",self.packageDetail.strPackageId] forKey:@"s^PACKAGE_ID"];
    [payloadListWishList setObject:_packageDetail.strPackageName forKey:@"s^PACKAGE_NAME"];
    [payloadListWishList setObject:[NSNumber numberWithFloat:_packageDetail.packagePrise] forKey:@"i^PRICE"];
    [payloadListWishList setObject:@"App" forKey:@"s^SOURCE"];
    
    if ([self.completePackageDetail count]>0)
    {
        NSDictionary *packageDict = [[NSDictionary alloc] init];
        packageDict = [self.completePackageDetail objectAtIndex:0];
        
        NSString *packageType = [packageDict objectForKey:@"pkgType"];
        
        if(packageType != nil && ![packageType isEqualToString:@""])//28-02-2018
        [payLoadViewPageDetail setObject:packageType forKey:@"s^TYPE"];
    }

    
    [NetCoreAnalyticsVC getNetCorePushNotificationDetailDictionary:payloadListWishList withPayloadCount:35];

}

//APP_FIND_HOLIDAYS_RESULT

-(void)netCoreDataDisplaySearchResult
{
    if (_payloadDict !=nil)
    {
//        NSString *viewDetail = [_payloadDict objectForKey:@"s^VIEW_DETAILS"];
        NSString *destination = [_payloadDict objectForKey:@"s^DESTINATION"];
        
//        if (![payLoadSearchResult objectForKey:@"s^VIEW_DETAILS"])
//        {
//            [payLoadSearchResult setObject:viewDetail forKey:@"s^VIEW_DETAILS"];
//        }
        if (![payLoadSearchResult objectForKey:@"s^DESTINATION"])
        {
            [payLoadSearchResult setObject:destination forKey:@"s^DESTINATION"];
        }
    }
    
    if (![payLoadSearchResult objectForKey:@"s^SORT_BY_OPTION"])
    {
        [payLoadSearchResult setObject:@"no" forKey:@"s^SORT_BY_OPTION"];
    }
    if (![payLoadSearchResult objectForKey:@"s^FILTERS"])
    {
        [payLoadSearchResult setObject:@"no" forKey:@"s^FILTERS"];
    }
    if (![payLoadSearchResult objectForKey:@"s^MAP"])
    {
        [payLoadSearchResult setObject:@"no" forKey:@"s^MAP"];
    }
    if (![payLoadSearchResult objectForKey:@"s^SOURCE"])
    {
        [payLoadSearchResult setObject:@"App" forKey:@"s^SOURCE"];
    }
    
    if ([self.completePackageDetail count]>0)
    {
        NSDictionary *packageDict = [[NSDictionary alloc] init];
        packageDict = [self.completePackageDetail objectAtIndex:0];
        
        
        NSString *packageType = [packageDict objectForKey:@"pkgType"];
     
        if(packageType != nil && ![packageType isEqualToString:@""])//28-02-2018
        [payLoadViewPageDetail setObject:packageType forKey:@"s^TYPE"];
    }
    
//    [NetCoreAnalyticsVC getNetCorePushNotificationDetailDictionary:payLoadSearchResult withPayloadCount:104];
    
    [NetCoreAnalyticsVC getNetCorePushNotificationDetailDictionary:payLoadSearchResult withPayloadCount:123]; //28-02-2018
}

//APP_VIEW_DETAILS_PAGE
//Net Core Detail Page

-(void)netCoreViewDetailPage
{
    if (![payLoadViewPageDetail objectForKey:@"s^OVERVIEW"])
    {
        [payLoadViewPageDetail setObject:@"no" forKey:@"s^OVERVIEW"];
    }
    if (![payLoadViewPageDetail objectForKey:@"s^DETAILS"])
    {
        [payLoadViewPageDetail setObject:@"no" forKey:@"s^DETAILS"];
    }
    
    if (![payLoadViewPageDetail objectForKey:@"s^ITINERARY"])
    {
         [payLoadViewPageDetail setObject:@"no" forKey:@"s^ITINERARY"];
    }
    
    //28-02-2018
    /*if(![payLoadViewPageDetail objectForKey:@"s^BOOK_NOW"])
    {
        [payLoadViewPageDetail setObject:@"no" forKey:@"s^BOOK_NOW"];
    }*/
    
    if(![payLoadViewPageDetail objectForKey:@"s^EMAIL_FORM"])
    {
        [payLoadViewPageDetail setObject:@"no" forKey:@"s^EMAIL_FORM"];
    }
    
    if(![payLoadViewPageDetail objectForKey:@"s^CALL"])
    {
        [payLoadViewPageDetail setObject:@"no" forKey:@"s^CALL"];
    }
    
    [payLoadViewPageDetail setObject:_packageDetail.strPackageId forKey:@"s^PACKAGE_ID"];
    [payLoadViewPageDetail setObject:@"App" forKey:@"s^SOURCE"];
    [payLoadViewPageDetail setObject:[NSNumber numberWithInt:_packageDetail.packagePrise] forKey:@"i^PACKAGE_PRICE"];
    
    if ([self.completePackageDetail count]>0)
    {
        NSDictionary *packageDict = [[NSDictionary alloc] init];
        packageDict = [self.completePackageDetail objectAtIndex:0];
        
        NSString *packageType = [packageDict objectForKey:@"pkgType"];
        NSArray *inclusionArray = [packageDict objectForKey:@"inclusionList"];
        NSArray *excludeArray = [packageDict objectForKey:@"excludeList"];
        NSString *thingsNote = [packageDict objectForKey:@"thingsNote"];
        
        
         [payLoadViewPageDetail setObject:@"no" forKey:@"s^INCLUSION"];
        
        if (inclusionArray)
        {
            if (inclusionArray.count !=0)
            {
                [payLoadViewPageDetail setObject:@"yes" forKey:@"s^INCLUSION"];
            }
        }
        
        [payLoadViewPageDetail setObject:@"no" forKey:@"s^EXCLUSION"];
        
        if (excludeArray)
        {
            if (excludeArray.count !=0)
            {
                [payLoadViewPageDetail setObject:@"yes" forKey:@"s^EXCLUSION"];
            }
        }

        if (thingsNote != nil && ![thingsNote isEqualToString:@""])
        {
            [payLoadViewPageDetail setObject:thingsNote forKey:@"s^THINGS_TO_NOTE"];//28-02-2018
        }
        
        [payLoadViewPageDetail setObject:@"" forKey:@"s^THINGS"];//28-02-2018
        
        
        if (packageType != nil && ![packageType isEqualToString:@""])
        {
            [payLoadViewPageDetail setObject:packageType forKey:@"s^TYPE"];//28-02-2018
        }
        
    }
    
//     [NetCoreAnalyticsVC getNetCorePushNotificationDetailDictionary:payLoadViewPageDetail withPayloadCount:106];
    
    [NetCoreAnalyticsVC getNetCorePushNotificationDetailDictionary:payLoadViewPageDetail withPayloadCount:126]; //28-02-2018
}


-(void)netCoreBookNowEventWihtDetails:(NSString*)mobileNumber
{
    NSMutableDictionary *payloadList =  [NSMutableDictionary dictionary];
    [payloadList setObject:mobileNumber forKey:@"s^MobileNo"];
    [payloadList setObject:self.packageDetail.strPackageType forKey:@"s^PackageType"];
    [payloadList setObject:self.packageDetail.strPackageId forKey:@"s^PackageID"];
    [payloadList setObject:self.packageDetail.stringRegion forKey:@"s^Destination"];
    [payloadList setObject:self.packageDetail.strPackageName forKey:@"s^PackageName"];
    [payloadList setObject:[NSNumber numberWithInt:self.packageDetail.packagePrise] forKey:@"i^PackagePrice"];
    
    
    [NetCoreAnalyticsVC getNetCorePushNotificationDetailDictionary:payloadList withPayloadCount:141]; //28-02-2018
    
  
    
}


@end
