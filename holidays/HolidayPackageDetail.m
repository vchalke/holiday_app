//
//  HolidayPackageDetail.m
//  holidays
//
//  Created by ketan on 06/10/15.
//  Copyright © 2015 Mobicule. All rights reserved.
//

#import "HolidayPackageDetail.h"
#import "WebUrlConstants.h"
@implementation HolidayPackageDetail

-(instancetype)initWithDataDict:(NSDictionary *)dataDict
{
     NSDictionary *packageDatailDict =[dataDict valueForKey:@"packageDetail"];
    self.packageDetailDictObj = [dataDict valueForKey:@"packageDetail"];
    if ([super init])
    {
        self.arrayItinerary = [packageDatailDict valueForKey:@"tcilHolidayItineraryCollection"];//tcilHolidayItineraryCollection
        self.arrayHubList = [dataDict valueForKey:@"hubList"];
        
        self.packageMRP = [dataDict valueForKey:@"packageMrp"]; //need in pricing
        self.flightDefaultMsg = [packageDatailDict valueForKey:@"flightDefaultMsg"];
        self.hotelDefaultMsg = [packageDatailDict valueForKey:@"hotelDefaultMsg"];
        self.isSightseeingDefaultMsg = [packageDatailDict valueForKey:@"isSightseeingDefaultMsg"];
       // self.stringRegion = [packageDatailDict valueForKey:@"ltRegionName"] ; //needed in pricing
        
        self.isHSA =  [packageDatailDict valueForKey:@"isHsa"];
        
        self.ltItineraryCode = [packageDatailDict valueForKey:@"ltItineraryCode"];
        
        self.dictBookingAmt = [dataDict valueForKey:@"bookingAmt"];//needed in pricing
        
        self.stringTermsConditions = [dataDict valueForKey:@"termsAndConditions"];//needed in pricing
        
        NSMutableArray *arrayPackageClassId = [[NSMutableArray alloc]init];
        
        if ([[[packageDatailDict valueForKey:@"isPackageClassStandard"] lowercaseString] isEqualToString:@"y"])
        {
            [arrayPackageClassId addObject:@"Standard"];
        }
        
        if ([[[packageDatailDict valueForKey:@"isPackageClassPremium"] lowercaseString] isEqualToString:@"y"])
        {
            [arrayPackageClassId addObject:@"Premium"];
        }

        if ([[[packageDatailDict valueForKey:@"isPackageClassDelux"] lowercaseString] isEqualToString:@"y"])
        {
            [arrayPackageClassId addObject:@"Deluxe"];
        }
        
        _categoryCollection = [packageDatailDict valueForKey:@"tcilHolidayCategoryCollection"]; //changes for tcilholiday collection
        
        self.arrayAccomTypeList = arrayPackageClassId;
        
        _strPackageDesc      = [dataDict valueForKey:@"packageDesc"];//?
        
        _strPackageName      = [packageDatailDict valueForKey:@"pkgName"];
        
        _durationNoDays      = [[packageDatailDict valueForKey:@"duration"]intValue];
        
        
        NSDictionary *packageSubtype = [packageDatailDict valueForKey:@"pkgSubtypeId"];
        
        _strPackageSubType   = [packageSubtype valueForKey:@"pkgSubtypeName"];
        
        _strPackageSubTypeID = [NSString stringWithFormat:@"%@",[packageSubtype valueForKey:@"pkgSubtypeId"]] ;
        
        NSNumber *packageTypeNumber = (NSNumber *)[packageSubtype valueForKey:@"pkgTypeId"];
        
        NSInteger packageTypeinteger = [packageTypeNumber integerValue];
        
        
        //0 -domestic
        
        //1 - international
        
        if (packageTypeinteger == 1)
        {
            _strPackageType = @"international";
        }
        else
        {
            _strPackageType = @"domestic";
        }
        
        
        _arrayTermsAndConditions = [packageDatailDict valueForKey:@"tcilHolidayPaymentTermsCollection"];
        
       //  _strPackageType      = [packageDatailDict valueForKey:@"pkgType"];
        
        _timeLineList        =  [packageDatailDict valueForKey:@"tcilHolidayTimelineCollection"];
        
        _strPackageId        = [packageDatailDict valueForKey:@"packageId"];
        
        _packagePrise        = [[dataDict valueForKey:@"startingPriceStandard"] intValue];
        
        _arrayPackageImagePathList = [packageDatailDict valueForKey:@"tcilHolidayPhotoVideoCollection"];
        
        _arrayImages = [[dataDict valueForKey:@"packagePhotosList"] valueForKey:@"photoUrl"];
        
        _arrayLtPricingCollection = [packageDatailDict valueForKey:@"tcilHolidayLtPricingCollection"];
        
        
        if (_arrayLtPricingCollection.count != 0)
        {
            
        }
        _tcilHolidayFlightsCollection = [packageDatailDict valueForKey:@"tcilHolidayFlightsCollection"];
        _arraytcilHolidayPriceCollection = [packageDatailDict valueForKey:@"tcilHolidayPriceCollection"];
_tcilHolidayOffersCollection = [packageDatailDict valueForKey:@"tcilHolidayOffersCollection"];
       _mealCollection = [[NSMutableArray alloc] init];
        _visaCollection = [[NSMutableArray alloc] init];
        _hotelCollection = [[NSMutableArray alloc] init];
       _sightseenCollection = [[NSMutableArray alloc] init];
      _accombdationCollection = [[NSMutableArray alloc] init];
        
        
        if(_categoryCollection.count>0){
            if (([packageDatailDict valueForKey:@"isFlightIncluded"]!=nil && [[[packageDatailDict valueForKey:@"isFlightIncluded"]lowercaseString] isEqualToString:@"y"]))
            {
                _airFlag = YES;
            }else
            {
                _airFlag = NO;
            }
            
            for (NSDictionary * typedict in _categoryCollection){
                NSLog(@"%@",typedict);
                NSString *key = @" ";
                NSString *isIncluded= @" ";
                NSString *packageClassid = @" ";
                key = [typedict valueForKey:@"type"];
                
                isIncluded = [typedict valueForKey:@"isTypeIncluded"];
                packageClassid = [typedict valueForKey:@"packageClassId"];
                //packageClassId
                
                
                ((void (^)())@{
                               @"Meal" : ^{
                    if ([[isIncluded uppercaseString] isEqualToString:@"Y"]) {
                        _mealsFlag = YES;
                        [_mealCollection addObject:typedict];
                    }
                    
                    
                },
//                               @"Flight" : ^{
//                    if ([[isIncluded uppercaseString] isEqualToString:@"Y"]) {
//                        _airFlag = YES;
//                    }
//
//
//                    [flightCollection addObject:typedict];
//                },
                               @"Visa" : ^{
                    if ([[isIncluded uppercaseString] isEqualToString:@"Y"]) {
                        _tourMngerFlag = YES;
                        [_visaCollection addObject:typedict];
                    }
                    
                    
                   
                },
                               @"Hotel" : ^{
                    if ([[isIncluded uppercaseString] isEqualToString:@"Y"]) {
                        _accomFlag = YES;
                        [_hotelCollection addObject:typedict];
                    }
                    
                    
                   
                },
                               @"Sightseeing" : ^{
                    if ([[isIncluded uppercaseString] isEqualToString:@"Y"]) {
                        _sightSeeingFlag = YES;
                        [_sightseenCollection addObject:typedict];
                    
                    }
                    
                    
                },
                               @"Accomodation" : ^{
                    if ([[isIncluded uppercaseString] isEqualToString:@"Y"]) {
                        _accomFlag = YES;
                        [_accombdationCollection addObject:typedict];
                    }
                    
                    
                   
                },
                               @"Transfer" : ^{
                                   if ([[isIncluded uppercaseString] isEqualToString:@"Y"]) {
                                       _transferFlag = YES;
                                       [_transferCollection addObject:typedict];
                                   }
                                   
                                   
                                  
                               },
                               }[key] ?: ^{
                                   NSLog(@"default");
                               })();
                
            }
        }else{
            
            if (([packageDatailDict valueForKey:@"isMealsIncluded"]!=nil &&[[[packageDatailDict valueForKey:@"isMealsIncluded"]lowercaseString] isEqualToString:@"y"]))
            {
                _mealsFlag = YES;
            }else
            {
                _mealsFlag = NO;
            }
            if (([packageDatailDict valueForKey:@"isFlightIncluded"]!=nil && [[[packageDatailDict valueForKey:@"isFlightIncluded"]lowercaseString] isEqualToString:@"y"]))
            {
                _airFlag = YES;
            }else
            {
                _airFlag = NO;
            }
            
            if (([packageDatailDict valueForKey:@"isVisaIncluded"]!=nil && [[[packageDatailDict valueForKey:@"isVisaIncluded"]lowercaseString] isEqualToString:@"y"]))
            {
                _tourMngerFlag = YES;
            }
            else
            {
                _tourMngerFlag = NO;
            }
            
            if (([packageDatailDict valueForKey:@"isHotelIncluded"]!=nil && [[[packageDatailDict valueForKey:@"isHotelIncluded"]lowercaseString] isEqualToString:@"y"]))
            {
                _accomFlag = YES;
            }else
            {
                _accomFlag = NO;
            }
            
            if (([packageDatailDict valueForKey:@"isSightseeingIncluded"]!=nil && [[[packageDatailDict valueForKey:@"isSightseeingIncluded"]lowercaseString] isEqualToString:@"y"]))
            {
                _sightSeeingFlag = YES;
            }
            else
            {
                _sightSeeingFlag = NO;
            }
            
            // Vijay Added
            if (([packageDatailDict valueForKey:@"isTransferIncluded"]!=nil && [[[packageDatailDict valueForKey:@"isTransferIncluded"]lowercaseString] isEqualToString:@"y"]))
            {
                _transferFlag = YES;
            }
            else
            {
                _transferFlag = NO;
            }
            
        }
        
        
       /* if (([packageDatailDict valueForKey:@"isMealsIncluded"]!=nil &&[[[packageDatailDict valueForKey:@"isMealsIncluded"]lowercaseString] isEqualToString:@"y"]) ||([packageDatailDict valueForKey:@"isMealsDefaultMsg"]!=nil && [[[packageDatailDict valueForKey:@"isMealsDefaultMsg"]lowercaseString] isEqualToString:@"y"]) )
        {
            _mealsFlag = YES;
        }
        else
        {
            _mealsFlag = NO;
        }
        
        
        if (([packageDatailDict valueForKey:@"isFlightIncluded"]!=nil && [[[packageDatailDict valueForKey:@"isFlightIncluded"]lowercaseString] isEqualToString:@"y"]) ||([packageDatailDict valueForKey:@"isFlightDefaultMsg"]!=nil && [[[packageDatailDict valueForKey:@"isFlightDefaultMsg"]lowercaseString] isEqualToString:@"y"]))
        {
            _airFlag = YES;
        }
        else
        {
            _airFlag = NO;
        }
        
        if (([packageDatailDict valueForKey:@"isVisaIncluded"]!=nil && [[[packageDatailDict valueForKey:@"isVisaIncluded"]lowercaseString] isEqualToString:@"y"]) ||([packageDatailDict valueForKey:@"isVisaDefaultMsg"]!=nil && [[[packageDatailDict valueForKey:@"isVisaDefaultMsg"]lowercaseString] isEqualToString:@"y"]))
        {
            _tourMngerFlag = YES;
        }
        else
        {
            _tourMngerFlag = NO;
        }
        
        if (([packageDatailDict valueForKey:@"isHotelIncluded"]!=nil && [[[packageDatailDict valueForKey:@"isHotelIncluded"]lowercaseString] isEqualToString:@"y"]) || ([packageDatailDict valueForKey:@"isHotelDefaultMsg"]!=nil && [[[packageDatailDict valueForKey:@"isHotelDefaultMsg"]lowercaseString] isEqualToString:@"y"]))
        {
            _accomFlag = YES;
        }
        else
        {
            _accomFlag = NO;
        }
        
        if (([packageDatailDict valueForKey:@"isSightseeingIncluded"]!=nil && [[[packageDatailDict valueForKey:@"isSightseeingIncluded"]lowercaseString] isEqualToString:@"y"])||([packageDatailDict valueForKey:@"isSightseeingDefaultMsg"]!=nil && [[[packageDatailDict valueForKey:@"isSightseeingDefaultMsg"]lowercaseString] isEqualToString:@"y"]))
        {
            _sightSeeingFlag = YES;
        }
        else
        {
            _sightSeeingFlag = NO;
        }
        */
        
        self.stringBookingOnline = [dataDict valueForKey:@"onlineBooking"];//??
        
        self.stringNotesDesc = [dataDict valueForKey:@"notesDesc"];//??
        
        self.stringPopularFlag = [dataDict valueForKey:@"tcilRecomends"];//??
        
        _ltMarket = [packageDatailDict valueForKey:@"ltMarket"];
        
        _pkgStatusId = [[packageDatailDict valueForKey:@"pkgStatusId"] integerValue];
        
        _productID = [packageDatailDict valueForKey:@"productId"];
        
        NSArray *cityArray = [packageDatailDict valueForKey:@"tcilHolidayCityCollection"];
        
        NSDictionary *cityDict = [cityArray firstObject];
        
        _packageContinent = [[[[[cityDict valueForKey:@"cityCode"] valueForKey:@"tcilMstCountryStateMapping"] valueForKey:@"tcilMstCountryContinentMapping"] valueForKey:@"countryContinentId"] valueForKey:@"continentName"];
        
        self.stringRegion = _packageContinent;
        
        _packageMode  = [[packageDatailDict valueForKey:@"tcilMode"] valueForKey:@"mode"];
        self.Offers = [packageDatailDict valueForKey:@"offers"];
        self.startingPriceStandard = [[dataDict valueForKey:@"startingPriceStandard"] intValue];
        self.startingPriceDelux = [[dataDict valueForKey:@"startingPriceDelux"] intValue];
        self.startingPricePremium = [[dataDict valueForKey:@"startingPricePremium"] intValue];
        self.strikeoutPriceStandard = [[dataDict valueForKey:@"strikeoutPriceStandard"] intValue];
        self.strikeoutPriceDelux = [[dataDict valueForKey:@"strikeoutPriceDelux"] intValue];
        self.strikeoutPricePremium = [[dataDict valueForKey:@"strikeoutPricePremium"] intValue];
        
        
        NSString *packageTypeString = (self.startingPriceStandard > 0) ? standardPackage : (self.startingPriceDelux > 0) ? deluxePackage : (self.startingPricePremium > 0) ? premiumPackage : standardPackage;
        
        if ([packageTypeString isEqualToString:standardPackage]){
            self.mainPkgSubClassId = 0;
        }else if ([packageTypeString isEqualToString:deluxePackage]){
            self.mainPkgSubClassId = 1;
        }else{
            self.mainPkgSubClassId = 2;
        }
        
        
    }
    return self;
}

@end
