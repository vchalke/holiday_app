//
//  ProfileTblViewCell.h
//  holidays
//
//  Created by Kush_Team on 26/05/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageView+WebCache.h"
NS_ASSUME_NONNULL_BEGIN

@interface ProfileTblViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *img_Icons;
@property (weak, nonatomic) IBOutlet UILabel *lbl_title;
@property (weak, nonatomic) IBOutlet UIView *view_firstOne;
@property (weak, nonatomic) IBOutlet UIView *view_secondOne;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_firtsViewWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_secondViewWidth;
@property (weak, nonatomic) IBOutlet UILabel *lbl_firstOne;
@property (weak, nonatomic) IBOutlet UILabel *lbl_secondOne;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_imgHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_imgWidth;
-(void)setConstraints:(CGFloat)widht;
-(void)setProfileImageFromStore:(NSString*)imgStr;
@end

NS_ASSUME_NONNULL_END
