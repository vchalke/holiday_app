//
//  TopWebVC.h
//  holidays
//
//  Created by Kush_Tech on 03/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>
#import "NewMasterVC.h"
#import "HolidayPackageDetail.h"
NS_ASSUME_NONNULL_BEGIN

@interface TopWebVC : NewMasterVC
@property (weak, nonatomic) IBOutlet WKWebView *web_views;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Title;
@property (nonatomic, strong)NSString *web_title;
@property (nonatomic, strong)NSString *web_url_string;
@property (nonatomic, strong)NSURL *web_url;
@property (nonatomic)BOOL isFromPdpCalculate;
@property (strong ,nonatomic) HolidayPackageDetail *holidayPkgDetailInPdpCalculate;
@property (nonatomic, strong)NSArray *stringArray;
@end

NS_ASSUME_NONNULL_END
