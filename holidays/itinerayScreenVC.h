//
//  itinerayScreenVC.h
//  holidays
//
//  Created by Kush Thakkar on 21/02/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HolidayPackageDetail.h"
#import "NewMasterVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface itinerayScreenVC : NewMasterVC <UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource,UIScrollViewDelegate> 

@property (strong,nonatomic) HolidayPackageDetail *holidayPackageDetail;

@property (nonatomic, assign) CGFloat lastContentOffset;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Title;

- (IBAction)backButtonPressed:(UIButton *)sender;

@end

NS_ASSUME_NONNULL_END
