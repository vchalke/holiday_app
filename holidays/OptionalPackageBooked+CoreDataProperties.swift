//
//  OptionalPackageBooked+CoreDataProperties.swift
//  holidays
//
//  Created by Saurav on 28/11/17.
//  Copyright © 2017 Mobicule. All rights reserved.
//

import Foundation
import CoreData


extension OptionalPackageBooked {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<OptionalPackageBooked> {
        return NSFetchRequest<OptionalPackageBooked>(entityName: "OptionalPackageBooked")
    }

    @NSManaged public var bfNumber: String?
    @NSManaged public var bookStatus: String?
    @NSManaged public var optionalCode: String?
    @NSManaged public var optionalCode_bfNumber: String?
    @NSManaged public var passengerNumber: String?
    @NSManaged public var tourRelation: NSSet?

}

// MARK: Generated accessors for tourRelation
extension OptionalPackageBooked {

    @objc(addTourRelationObject:)
    @NSManaged public func addToTourRelation(_ value: Tour)

    @objc(removeTourRelationObject:)
    @NSManaged public func removeFromTourRelation(_ value: Tour)

    @objc(addTourRelation:)
    @NSManaged public func addToTourRelation(_ values: NSSet)

    @objc(removeTourRelation:)
    @NSManaged public func removeFromTourRelation(_ values: NSSet)

}
