//
//  OptionalPackageViewController.swift
//  sotc-consumer-application
//
//  Created by Saurav Kumar on 09/08/17.
//  Copyright © 2017 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit
import CoreData


class OptionalPackageViewController: UIViewController ,UITableViewDelegate ,UITableViewDataSource,UIGestureRecognizerDelegate,UIPopoverPresentationControllerDelegate{
    
    
    //MARK: - Outlets
    @IBOutlet var optionalPackageTableView: UITableView!
    
    @IBOutlet var sectionHeaderView1: UIView!
    @IBOutlet var sectionHeaderView2: UIView!
    @IBOutlet var sectionHeaderView3: UIView!
    var sourceView: UIView?
    
    @IBOutlet weak var foreignCurrencyLabel: UILabel!
    @IBOutlet weak var INDCurrencyLabel: UILabel!
    
    
    
    var overlay: UIView?
    
    //MARK: - Class Veriables
    
    var tourDetails:Tour?
    
    //var utils : Utils = Utils()
    var currencyArray:Array<CurrencyData>?
    lazy var formatterArray:Array<NumberFormatter> = []
    var sectionOpenStatus:[(isOpen: Bool, section: Int ,row : [(isSelected: Bool, index: Int ,passenger:Passenger?,bookingStatus:String )],optional : String ,optionalPrice:OptionalMaster,totalPackagePrice:Double)] = []
    var sectionOpenTest:[(isOpen: Bool, section: Int ,row : [(isSelected: Bool, index: Int ,passenger:Passenger,bookingStatus:String )],optional : String ,optionalPrice:OptionalMaster,totalPackagePrice:Double)] = []
    
    var reciptGenerationFXDetails:(isFX:Bool,fxCurrencyType:String,roeRate:String,fxAmount:String,isA2Attached:Bool) = (isFX:false,fxCurrencyType:"",roeRate:"",fxAmount:"",isA2Attached:false)
    
    
    //MARK: - Controller Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        // Do any additional setup after loading the view.
        
        // self.optionalPackageTableView.register(UITableViewCell.self, forCellReuseIdentifier: "optionalPackageCell")
        
        if !checkIsDataAvailable()
        {
            self.noDataAlert(message: AlertMessage.Optional.NO_DATA)
            
        }else{
            
            
            self.optionalPackageTableView.register(UINib(nibName: "OptionalHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: "optionalHeader")
            
            //self.optionalPackageTableView.register(UINib(nibName: "optionalPackageTableView", bundle: nil), : "optional")
            
            for locale in NSLocale.availableLocaleIdentifiers {
                
                let formatter = NumberFormatter()
                formatter.locale = NSLocale(localeIdentifier: locale) as Locale?
                self.formatterArray.append(formatter)
                
            }
            
           // self.currencyArray = self.getExchangeRate();
            
            self.fetchROEData()
            
            self.generateOptionalDetailsData()
            // sectionOpenTest = sectionOpenStatus
            
            //self.configurePayUNotification()
            
        }
        
       
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.setRedNavigationBar()
        self.navigationController?.setnavigatiobBarTitle(title: Title.OPTIONAL_TOUR, viewController: self)
        self.navigationController?.setEmergencyNavigationRightButton(showEmergencyButton: true, viewController: self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func uiTapGestureRecognizerClick(_ sender: UITapGestureRecognizer) {
        
        let tappedView = sender.view!
        
        self.sectionOpenStatus[tappedView.tag].isOpen = !self.sectionOpenStatus[tappedView.tag].isOpen
        
        self.optionalPackageTableView.beginUpdates()
        
        self.optionalPackageTableView.reloadSections([tappedView.tag], with: .fade)
        
        self.optionalPackageTableView.endUpdates()
        
        
        /*  if sectionOpenStatus[(tappedView.tag )].isOpen == false
         {
         for i in 0..<sectionOpenStatus.count {
         
         sectionOpenStatus[i].isOpen = false
         
         }
         
         sectionOpenStatus[(tappedView.tag )].isOpen = true
         
         }
         else
         {
         sectionOpenStatus[(tappedView.tag )].isOpen = false
         }
         
         /*UIView.transition(with: tappedView,
         duration: 0.35,
         options: .transitionFlipFromLeft,
         animations: {  self.optionalPackageTableView.reloadData() })*/
         
         self.optionalPackageTableView.reloadData()*/
        
    }
    
    @objc func infoButtonClick(_ sender:UIButton)
    {
        
        self.sourceView = sender
        
        if let optionalMaster:OptionalMaster = self.sectionOpenStatus[sender.tag].optionalPrice as? OptionalMaster
        {
            
            self.showAlertViewWithTitle(title: "", message: optionalMaster.optionalDescription!)
            // self.presentInfoPopover(description: optionalMaster.optionalDescription!)
        }
        
    }
    
    //MARK: - TableView DataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return sectionOpenStatus.count
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        if sectionOpenStatus[section].isOpen {
            
            //  printLog(sectionOpenStatus[section].row)
            //printLog(sectionOpenStatus[section].row.count)
            
            return sectionOpenStatus[section].row.count
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        guard let cell = tableView.dequeueReusableHeaderFooterView(withIdentifier: "optionalHeader") as? OptionalHeaderView
            else {
                
                fatalError("The dequeued cell is not an instance of MealTableViewCell.")
        }
        
        if sectionOpenStatus[section].isOpen && sectionOpenStatus[section].row.count > 0
        {
            
            UIView.animate(withDuration: 5.0, animations: {
                cell.dropDownImageView.transform = CGAffineTransform(rotationAngle: ( 180.0 * .pi ) / 180.0)
            })
            
            //if sectionOpenStatus[section].row.count > 0 {
            
            /*    var adultPrice = sectionOpenStatus[section].optionalPrice.adultSP!
             var childtPrice = sectionOpenStatus[section].optionalPrice.childSP!
             
             cell.footerLabelHeightConstraints.constant = 40;
             
             let fullString = NSMutableAttributedString()
             
             let offsetY : CGFloat = -8.0
             
             // create our NSTextAttachment
             let adultAttachment = NSTextAttachment()
             let childAttachment = NSTextAttachment()
             
             adultAttachment.image = #imageLiteral(resourceName: "adultOptional") //UIImage(named:"adultOptional.png")
             childAttachment.image = #imageLiteral(resourceName: "childOptional")
             
             
             // wrap the attachment in its own attributed string so we can append it
             let adultString = NSAttributedString(attachment: adultAttachment)
             let childString = NSAttributedString(attachment: childAttachment)
             
             adultAttachment.bounds = CGRect(x:0,y: offsetY,width:adultAttachment.image!.size.width-10,height:adultAttachment.image!.size.height-10)
             childAttachment.bounds = CGRect(x:0,y: offsetY,width:childAttachment.image!.size.width-10,height:childAttachment.image!.size.height-10)
             
             // add the NSTextAttachment wrapper to our full string, then add some more text.
             fullString.append(adultString)
             
             let adultAttribultestring = NSAttributedString(string: adultPrice + "    ")
             
             
             let childAttribultestring = NSAttributedString(string: childtPrice)
             
             fullString.append(adultAttribultestring);
             
             fullString.append(childString)
             
             fullString.append(childAttribultestring)
             
             
             
             
             //        fullString.append(NSAttributedString(string: "End of text"))
             
             // draw the result in a label
             cell.footerLabel?.attributedText = fullString*/
            // }
            
        }else{
            UIView.animate(withDuration: 5.0, animations: {
                cell.dropDownImageView.transform = CGAffineTransform(rotationAngle: ( 180.0 * .pi ) * 180.0)
            })
            
            // cell.footerLabelHeightConstraints.constant = 0;
            
        }
        
        cell.tag = section
        
        if let optionalMaster:OptionalMaster = self.sectionOpenStatus[section].optionalPrice as? OptionalMaster
        {
            cell.optionalHeaderLabel?.text = optionalMaster.optionalDescription ?? ""
        }
        
        //cell.optionalHeaderLabel?.text = sectionOpenStatus[section].optional
        
        cell.infoButton.isHidden = true
        
        cell.infoButton.isUserInteractionEnabled = false
        
        cell.infoButton.addTarget(self, action: #selector(self.infoButtonClick(_:)), for: .touchUpInside)
        cell.infoButton.tag = section
        
        let heraderView1Tap = UITapGestureRecognizer(target: self, action: #selector(self.uiTapGestureRecognizerClick(_:)))
        heraderView1Tap.delegate = self
        cell.addGestureRecognizer(heraderView1Tap)
        
        return cell;
        /* let headerView = UIView(frame: CGRect(x:0, y: 0, width: tableView.frame.width, height: 70))
         headerView.backgroundColor = UIColor.white
         
         let packageTitleLabel  = UILabel(frame: CGRect(x: 12, y: 5, width: ((headerView.frame.width / 5) * 3) -  4, height: 20))
         
         packageTitleLabel.text = sectionOpenStatus[section].optional//"Package \(section + 1)"
         
         packageTitleLabel.font = UIFont(name: "Roboto Regular", size: 10)
         
         
         let infoButton : UIButton = UIButton(frame: CGRect(x: packageTitleLabel.frame.width , y: 5, width: (headerView.frame.width / 5) - 4, height: 20))
         
         infoButton.contentMode = UIViewContentMode.center
         infoButton.setImage(#imageLiteral(resourceName: "infoOptional"), for: UIControlState.normal)
         infoButton.imageView?.contentMode = UIViewContentMode.scaleAspectFit
         
         let dropDownImage : UIImageView = UIImageView(frame: CGRect(x: infoButton.frame.width + packageTitleLabel.frame.width  , y: 5, width: (headerView.frame.width / 5) - 4, height: 20))
         
         dropDownImage.contentMode = UIViewContentMode.scaleAspectFit
         dropDownImage.image = #imageLiteral(resourceName: "DorpDownTourDetail")
         
         let headerSpereatorView = UIView(frame: CGRect(x:5, y: 59, width: tableView.frame.width - 10, height: 1 ))
         headerSpereatorView.backgroundColor = UIColor.lightGray
         
         // headerView.addSubview(headerSpereatorView)
         headerView.addSubview(packageTitleLabel)
         headerView.addSubview(infoButton)
         headerView.addSubview(dropDownImage)
         
         
         if sectionOpenStatus[section].isOpen
         {
         
         if sectionOpenStatus[section].row.count > 0 {
         
         let footerLabel  = UILabel(frame:  CGRect(x: 12, y: 40, width: tableView.frame.width, height: 20))
         
         footerLabel.font = UIFont(name: "Roboto Regular", size: 10)
         
         var adultPrice = sectionOpenStatus[section].optionalPrice.adultSP!
         var childtPrice = sectionOpenStatus[section].optionalPrice.childSP!
         
         //  footerLabel.font.withSize(10)
         
         //footerLabel.backgroundColor = UIColor.white
         
         //NSMutableAttributedString()
         
         let fullString = NSMutableAttributedString()
         
         let offsetY : CGFloat = -8.0
         
         // create our NSTextAttachment
         let adultAttachment = NSTextAttachment()
         let childAttachment = NSTextAttachment()
         
         adultAttachment.image = #imageLiteral(resourceName: "adultOptional") //UIImage(named:"adultOptional.png")
         childAttachment.image = #imageLiteral(resourceName: "adultOptional")
         
         // wrap the attachment in its own attributed string so we can append it
         let adultString = NSAttributedString(attachment: adultAttachment)
         let childString = NSAttributedString(attachment: childAttachment)
         
         adultAttachment.bounds = CGRect(x:0,y: offsetY,width:adultAttachment.image!.size.width-10,height:adultAttachment.image!.size.height-10)
         childAttachment.bounds = CGRect(x:0,y: offsetY,width:childAttachment.image!.size.width-10,height:childAttachment.image!.size.height-10)
         
         // add the NSTextAttachment wrapper to our full string, then add some more text.
         fullString.append(adultString)
         
         let adultAttribultestring = NSAttributedString(string: adultPrice + " ")
         
         
         let childAttribultestring = NSAttributedString(string: childtPrice)
         
         fullString.append(adultAttribultestring);
         
         fullString.append(childString)
         
         fullString.append(childAttribultestring)
         
         
         
         
         //        fullString.append(NSAttributedString(string: "End of text"))
         
         // draw the result in a label
         footerLabel.attributedText = fullString
         
         headerView.addSubview(footerLabel)
         
         }
         }
         
         headerView.tag = section
         
         let heraderView1Tap = UITapGestureRecognizer(target: self, action: #selector(self.uiTapGestureRecognizerClick(_:)))
         heraderView1Tap.delegate = self
         headerView.addGestureRecognizer(heraderView1Tap)
         
         
         return headerView*/
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        /* if sectionOpenStatus[section].isOpen {
         
         if sectionOpenStatus[section].row.count > 0 {
         
         return 50
         
         }
         else
         {
         return 50
         }
         }*/
        
        return 60
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 45
    }
    
    //MARK: - TableView DataSource Helper
    
    func getOptionalDetailsTableViewCell(tableView: UITableView,indexPath:IndexPath) -> UITableViewCell
    {
        
        // var cell = tableView.dequeueReusableCell(withIdentifier: "optionalPackageCell")
        
        // if cell == nil {
        
        var  cell = UITableViewCell(style: UITableViewCell.CellStyle.value1, reuseIdentifier: "optionalPackageCell")
        // }
        
        let passenger:Passenger? = sectionOpenStatus[indexPath.section].row[indexPath.row].passenger
       
        
        cell.textLabel?.text = "\(passenger?.firstName ?? "") ".capitalized + "\(passenger?.middleName ??  "") ".capitalized + "\(passenger?.lastName ??  "")  ".capitalized
        
        let bookinStatus = sectionOpenStatus[indexPath.section].row[indexPath.row].bookingStatus
        
        let fullString = NSMutableAttributedString(string: (cell.textLabel?.text)!)
        
        
        let offsetY : CGFloat = -5.0
        
        // create our NSTextAttachment
        let image1Attachment = NSTextAttachment()
        
       
        
        switch (passenger?.paxType ?? "").uppercased() {
        case Constant.PAX_TYPE_ADULT:
            image1Attachment.image = #imageLiteral(resourceName: "adultOptional") //UIImage(named:"adultOptional.png")
            
        case  Constant.PAX_TYPE_CHILD:
            image1Attachment.image = #imageLiteral(resourceName: "childOptional")
            
//        case  Constant.PAX_TYPE_INFANT:
//            image1Attachment.image = #imageLiteral(resourceName: "childOptional")//#imageLiteral(resourceName: "adultOptional")
            
        default:
            image1Attachment.image = #imageLiteral(resourceName: "adultOptional")//UIImage(named:"adultOptional.png")
        }
        
        // wrap the attachment in its own attributed string so we can append it
        let image1String = NSAttributedString(attachment: image1Attachment)
        
        image1Attachment.bounds = CGRect(x:0,y: offsetY,width:image1Attachment.image!.size.width-10,height:image1Attachment.image!.size.height-13)
        
        // add the NSTextAttachment wrapper to our full string, then add some more text.
        fullString.append(image1String)
        //        fullString.append(NSAttributedString(string: "End of text"))
        
        
        // draw the result in a label
        cell.textLabel?.attributedText = fullString
        
        //
        
        let customAccessoryView:UIView = UIView(frame:CGRect(x: 0, y: 0, width: 100, height: 50) )
        
        customAccessoryView.isUserInteractionEnabled = false
        
        let selectAllButton = UIButton(type: .custom)
        selectAllButton.frame = CGRect(x: 70, y: 15, width: 20, height: 20)
        // saveButton.addTarget(self, action: "accessoryButtonTapped:", forControlEvents: .TouchUpInside)
        selectAllButton.isUserInteractionEnabled = false
        
        let selectAllLabel:UILabel = UILabel(frame: CGRect(x: 0, y:10, width: 100, height: 30))
        
        selectAllLabel.isUserInteractionEnabled = false
        
        selectAllLabel.textAlignment = .right
        
        if bookinStatus.uppercased() == Constant.BOOKED
        {
            //cell.accessoryType = UITableViewCellAccessoryType.checkmark
            //cell.detailTextLabel?.text = Constant.OptionalTour.LABEL_BOOKED
            //cell.detailTextLabel?.font = UIFont(name: "Roboto-Light", size: 15)
            //cell.detailTextLabel?.textColor = AppUtility.hexStringToUIColor(hex: "#EB3338")
            //cell.accessoryType = .none
            
            selectAllLabel.text = Constant.OptionalTour.LABEL_BOOKED
            
            selectAllLabel.textColor = AppUtility.hexStringToUIColor(hex: "#EB3338")
            
            selectAllLabel.font = UIFont(name: "Roboto-Light", size: 14)
            
            customAccessoryView.addSubview(selectAllLabel)
            
            cell.isUserInteractionEnabled = false
            
        }else if sectionOpenStatus[indexPath.section].row[indexPath.row].isSelected == true {
            
            // cell.accessoryType = .checkmark
            
            //cell.tintColor = UIColor.blue
            
            selectAllButton.setImage(#imageLiteral(resourceName: "checkbox_tick"), for: .normal)
            selectAllButton.semanticContentAttribute = .forceLeftToRight
            customAccessoryView.addSubview(selectAllButton)
            
        }else{
            
            selectAllButton.setImage(#imageLiteral(resourceName: "checkbox_empty"), for: .normal)
            selectAllButton.semanticContentAttribute = .forceLeftToRight
            customAccessoryView.addSubview(selectAllButton)
            
            //cell.accessoryType = .checkmark
            // cell.tintColor = UIColor.gray
            // cell.accessoryType = .none
        }
        
        
        cell.accessoryView = customAccessoryView
        
        cell.textLabel?.font = UIFont(name: "Roboto-Light", size: 15)
        cell.textLabel?.textColor = AppUtility.hexStringToUIColor(hex: "#010101")
        
        
        
        return cell
    }
    
    func getOptionalDetailsHeaderTableViewCell(tableView: UITableView,indexPath:IndexPath) -> UITableViewCell
    {
        
        // var cell = tableView.dequeueReusableCell(withIdentifier: "optionalPackageSelectAllCell")
        
        //if cell == nil {
        
        let  cell = UITableViewCell(style: UITableViewCell.CellStyle.value1, reuseIdentifier: "optionalPackageSelectAllCell")
        // }
        
        
        
        
        cell.accessoryType = UITableViewCell.AccessoryType.checkmark
        
        // cell.detailTextLabel?.text = "Select All"
        
        cell.detailTextLabel?.textColor = AppUtility.hexStringToUIColor(hex: "#8b8b8b")
        
        cell.detailTextLabel?.font = UIFont(name: "Roboto-Light", size: 14)
        
        cell.textLabel?.font = UIFont(name: "Roboto-Light", size: 15)
        
        cell.textLabel?.textColor = AppUtility.hexStringToUIColor(hex: "#8b8b8b")
        
        
        
        let adultPrice = sectionOpenStatus[indexPath.section].optionalPrice.adultSP!
        let childtPrice = sectionOpenStatus[indexPath.section].optionalPrice.childSP!
        
        let bookinStatus = sectionOpenStatus[indexPath.section].row[indexPath.row].bookingStatus
        
        //cell.footerLabelHeightConstraints.constant = 40;
        
        let fullString = NSMutableAttributedString()
        
        let offsetY : CGFloat = -5.0
        
        // create our NSTextAttachment
        let adultAttachment = NSTextAttachment()
        let childAttachment = NSTextAttachment()
        
        adultAttachment.image = #imageLiteral(resourceName: "adultOptional") //UIImage(named:"adultOptional.png")
        childAttachment.image = #imageLiteral(resourceName: "childOptional")
        
        
        // wrap the attachment in its own attributed string so we can append it
        let adultString = NSAttributedString(attachment: adultAttachment)
        let childString = NSAttributedString(attachment: childAttachment)
        
        adultAttachment.bounds = CGRect(x:0,y: offsetY,width:adultAttachment.image!.size.width-10,height:adultAttachment.image!.size.height-10)
        childAttachment.bounds = CGRect(x:0,y: offsetY,width:childAttachment.image!.size.width-10,height:childAttachment.image!.size.height-10)
        
        // add the NSTextAttachment wrapper to our full string, then add some more text.
        fullString.append(adultString)
        
        let adultamountCurrencyString = self.getCurencyAmount(currency: sectionOpenStatus[indexPath.section].optionalPrice.optionalCurrency!, amount:adultPrice)
        
        let adultAttribultestring = self.getAttributedString(value: (currencySymbol:"  " + adultamountCurrencyString.currencySymbol , amount:adultamountCurrencyString.amount + "  "), label: cell.textLabel!)//NSAttributedString(string: adultPrice + "    ")
        
        
        let childAmountCurrencyString = self.getCurencyAmount(currency: sectionOpenStatus[indexPath.section].optionalPrice.optionalCurrency!, amount:childtPrice)
        
        let childAttribultestring = self.getAttributedString(value: (currencySymbol:"  " +  childAmountCurrencyString.currencySymbol , amount:childAmountCurrencyString.amount + "  " ), label: cell.textLabel!)//NSAttributedString(string: adultPrice + "    ")
        
        // let childAttribultestring = //NSAttributedString(string: childtPrice)
        
        fullString.append(adultAttribultestring);
        
        fullString.append(childString)
        
        fullString.append(childAttribultestring)
        
        let customAccessoryView:UIView = UIView(frame:CGRect(x: 0, y: 0, width: 100, height: 50) )
        customAccessoryView.isUserInteractionEnabled = false
        
        let selectAllButton = UIButton(type: .custom)
        selectAllButton.frame = CGRect(x: 70, y: 15, width:20, height: 20)
        selectAllButton.isUserInteractionEnabled = false
        
        // saveButton.addTarget(self, action: "accessoryButtonTapped:", forControlEvents: .TouchUpInside)
        
        
        
        let selectAllLabel:UILabel = UILabel(frame: CGRect(x: 0, y: 15, width: 65, height: 20))
        
        selectAllLabel.isUserInteractionEnabled = false
        
        selectAllLabel.textAlignment = .right
        
        if bookinStatus.uppercased() == Constant.BOOKED
        {
            
            //selectAllLabel.frame = CGRect(x: 0, y:10, width: 100, height: 30)
            selectAllLabel.frame = CGRect(x: 0, y:10, width: 0, height: 0)
            //cell.tintColor = UIColor.blue
            //selectAllButton.setImage(#imageLiteral(resourceName: "checkbox_tick"), for: .normal)
           // selectAllLabel.text = Constant.OptionalTour.LABEL_BOOKED
            
            selectAllLabel.textColor = AppUtility.hexStringToUIColor(hex: "#EB3338")
            
            selectAllLabel.font = UIFont(name: "Roboto-Light", size: 14)
            
            cell.isUserInteractionEnabled = false
            
        }else   if sectionOpenStatus[indexPath.section].row[indexPath.row].isSelected == true {
            
            //cell.tintColor = UIColor.blue
            selectAllButton.setImage(#imageLiteral(resourceName: "checkbox_tick"), for: .normal)
            
            selectAllLabel.textColor = AppUtility.hexStringToUIColor(hex: "#8b8b8b")
            
            selectAllLabel.font = UIFont(name: "Roboto-Light", size: 14)
            
            selectAllLabel.text = Constant.OptionalTour.LABEL_SELECTALL
        }
        else
        {
            selectAllButton.setImage(#imageLiteral(resourceName: "checkbox_empty"), for: .normal)
            selectAllLabel.textColor = AppUtility.hexStringToUIColor(hex: "#8b8b8b")
            
            selectAllLabel.font = UIFont(name: "Roboto-Light", size: 14)
            
            selectAllLabel.text = Constant.OptionalTour.LABEL_SELECTALL
            //cell.tintColor = UIColor.gray
            // cell.accessoryType = .none
            
        }
        
        //selectAllButton.semanticContentAttribute = .forceLeftToRight
        
        
        
        customAccessoryView.addSubview(selectAllLabel)
        customAccessoryView.addSubview(selectAllButton)
        
        cell.accessoryView = customAccessoryView
        
        cell.textLabel?.attributedText = fullString
        
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        /*  guard let cell = tableView.dequeueReusableCell(withIdentifier: "optionalPackageCell", for: indexPath) as? UITableViewCell
         else {
         
         fatalError("The dequeued cell is not an instance of MealTableViewCell.")
         }*/
        
        // cell?.detailTextLabel?.text = "dadasda"
        
        
        if indexPath.row == 0
        {
            return self.getOptionalDetailsHeaderTableViewCell(tableView: tableView, indexPath: indexPath)
            
        }else{
            
            return self.getOptionalDetailsTableViewCell(tableView: tableView, indexPath: indexPath)
        }
        
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
    {
        if sectionOpenStatus[section].isOpen == true
        {
            let footerView = UIView(frame: CGRect(x:0, y: 0, width: tableView.frame.width, height: 40))
            
            
            let packageTotalAmountLabel  = UILabel(frame: CGRect(x: 12, y: 5, width: ((footerView.frame.width / 5) * 3) -  4, height: 35))
            
            
            /* func filterCurrency(tourCode:String,uptoLength:Int,startIndex:Int) -> String
             {
             if !((tourCode.isEmpty))
             {
             let first4 = tourCode.substring(to:tourCode.index(startIndex, offsetBy: uptoLength))
             
             return first4
             
             }
             
             return ""
             
             
             }*/
            
            let amount = self.getCurencyAmount(currency: sectionOpenStatus[section].optionalPrice.optionalCurrency!, amount: String( sectionOpenStatus[section].totalPackagePrice) )
            //"₹" + String( sectionOpenStatus[section].totalPackagePrice) //"Package \(section + 1)"
            
            
            
            
            footerView.backgroundColor = AppUtility.hexStringToUIColor(hex: "#f4f4f4")
            
            packageTotalAmountLabel.font = UIFont(name: "Roboto-Light", size: 15)
            
            packageTotalAmountLabel.backgroundColor = AppUtility.hexStringToUIColor(hex: "#f4f4f4")
            
            packageTotalAmountLabel.textColor = AppUtility.hexStringToUIColor(hex: "#333333")
            
            
            packageTotalAmountLabel.attributedText =  self.getAttributedString(value: amount, label: packageTotalAmountLabel)
            let footerBorderView = UIView(frame: CGRect(x: 0.0, y: footerView.bounds.height - 1, width: footerView.frame.width, height: 1.0))
            footerBorderView.backgroundColor = UIColor.groupTableViewBackground
            
            footerView.addSubview(packageTotalAmountLabel)
            footerView.addSubview(footerBorderView)
            
            return footerView
            
        }
        
        let footerView = UIView(frame: CGRect(x:12, y: 0, width: tableView.frame.width, height: 0.8))
        footerView.backgroundColor = UIColor.groupTableViewBackground
        
        return footerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if sectionOpenStatus[section].isOpen
        {
            return 40
        }
        
        return 0.8
        
    }
    
    //MARK: - TableView delegates Helper
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        if sectionOpenStatus[indexPath.section].isOpen == true
        {
            
            if indexPath.row == 0
            {
                let bookinStatus = sectionOpenStatus[indexPath.section].row[indexPath.row].bookingStatus
                
                if  bookinStatus.uppercased() == Constant.OptionalTour.NOT_BOOKED
                {
                    if sectionOpenStatus[indexPath.section].row[indexPath.row].isSelected == true {
                        
                        sectionOpenStatus[indexPath.section].row[indexPath.row].isSelected = false
                        
                        self.updateSectionOnDeSelectAll(indexPath:indexPath)
                        
                    }else{
                        
                        sectionOpenStatus[indexPath.section].row[indexPath.row].isSelected = true
                        
                        self.updateSectionOnSelectAll(indexPath:indexPath)
                    }
                    
                }
                
            }else{
                
                let bookinStatus = sectionOpenStatus[indexPath.section].row[indexPath.row].bookingStatus
                
                if  bookinStatus.uppercased() == Constant.OptionalTour.NOT_BOOKED
                {
                    let optionalMaster:OptionalMaster = sectionOpenStatus[indexPath.section].optionalPrice
                    if let passenger:Passenger = sectionOpenStatus[indexPath.section].row[indexPath.row].passenger
                    {
                    
                    if sectionOpenStatus[indexPath.section].row[indexPath.row].isSelected == true {
                        
                        self.updateOnRowDeSelected(indexPath: indexPath,optionalMaster:optionalMaster,passenger:passenger)
                        
                        // let cell = tableView.cellForRow(at: indexPath)
                        
                        // cell?.accessoryType = .none
                        // cell?.accessoryType = .checkmark
                        // cell?.tintColor = UIColor.lightGray
                        
                    }
                    else
                    {
                        
                        self.updateOnRowSelected(indexPath: indexPath,optionalMaster:optionalMaster,passenger:passenger)
                        
                        // let cell = tableView.cellForRow(at: indexPath)
                        
                        // cell?.accessoryType = .checkmark
                        
                        /// cell?.tintColor = UIColor.blue
                    }
                    
                    
                    
                }
                }
                
                
                sectionOpenStatus[indexPath.section].row[0].isSelected = self.checkIsAllRowSelected(indexPath:indexPath)
                
                
            }
            
            
            
        }
        
        let grandTotal:Double = self.getGrandTotal()
        
        
        
        
        let currencyAmount = self.getCurencyAmount(currency: sectionOpenStatus[indexPath.section].optionalPrice.optionalCurrency!, amount: String( grandTotal) )
        
        
        
        self.foreignCurrencyLabel.text = currencyAmount.currencySymbol + currencyAmount.amount  //self.getAttributedString(value: amount, label: self.foreignCurrencyLabel)
        
        let currencyRate = self.getTodaysRate(by: sectionOpenStatus[indexPath.section].optionalPrice.optionalCurrency!)
        
        if sectionOpenStatus[indexPath.section].optionalPrice.optionalCurrency?.uppercased() == "INR"
        {
            //let currencyText:(String,String) = ("₹",String(grandTotal))
            
            self.INDCurrencyLabel.text =  "₹" + String(Int64(grandTotal))  //self.getAttributedString(value:currencyText, label: self.foreignCurrencyLabel)
            
            self.reciptGenerationFXDetails.isFX = false
            self.reciptGenerationFXDetails.roeRate = ""
            self.reciptGenerationFXDetails.fxAmount = ""
            self.reciptGenerationFXDetails.fxCurrencyType = ""
            
        }else
        {
            // let currencyText:(String,String) = ("₹",String(grandTotal * Double(currencyRate.rate)! ))
            
            let finalTotal:String = String((grandTotal * Double(currencyRate.rate)!)).numberFormatterDecimal() //.rounded()
            
            self.INDCurrencyLabel.text =  "₹" + String(finalTotal)  //self.getAttributedString(value:currencyText, label: self.foreignCurrencyLabel)
            
            self.reciptGenerationFXDetails.isFX = true
            self.reciptGenerationFXDetails.roeRate = currencyRate.rate
            self.reciptGenerationFXDetails.fxAmount = currencyAmount.amount
            self.reciptGenerationFXDetails.fxCurrencyType = sectionOpenStatus[indexPath.section].optionalPrice.optionalCurrency ?? ""
            
            
        }
        
        
        
        let sectionIndex = IndexSet(integer: indexPath.section)
        
        self.optionalPackageTableView.reloadSections(sectionIndex, with: .none)
        
        
    }
    
    
    //MARK: - Mofdify TableView  data Helper
    
    func updateOnRowSelected(indexPath:IndexPath,optionalMaster:OptionalMaster,passenger:Passenger)
    {
        sectionOpenStatus[indexPath.section].row[indexPath.row].isSelected = true
        
        let totalOptioanlPrice:Double = self.getTotalOptionalPrice(passenger: passenger, price: optionalMaster)
        
        sectionOpenStatus[indexPath.section].totalPackagePrice = sectionOpenStatus[indexPath.section].totalPackagePrice + totalOptioanlPrice
    }
    
    func updateOnRowDeSelected(indexPath:IndexPath,optionalMaster:OptionalMaster,passenger:Passenger)
    {
        sectionOpenStatus[indexPath.section].row[indexPath.row].isSelected = false
        
        let totalOptioanlPrice:Double = self.getTotalOptionalPrice(passenger: passenger, price: optionalMaster)
        
        sectionOpenStatus[indexPath.section].totalPackagePrice = sectionOpenStatus[indexPath.section].totalPackagePrice - totalOptioanlPrice
    }
    
    func checkIsAllRowSelected(indexPath:IndexPath) -> Bool
    {
        for index in 1..<sectionOpenStatus[indexPath.section].row.count
        {
            let newIndexPath = IndexPath(row: index, section: indexPath.section)
            
            let bookinStatus = sectionOpenStatus[newIndexPath.section].row[newIndexPath.row].bookingStatus
            
            if  bookinStatus.uppercased() == Constant.OptionalTour.NOT_BOOKED && sectionOpenStatus[newIndexPath.section].row[newIndexPath.row].isSelected == false
            {
                return false
            }
        }
        
        return true
    }
    
    
    func updateSectionOnDeSelectAll(indexPath:IndexPath)
    {
        for index in 1..<sectionOpenStatus[indexPath.section].row.count
        {
            let newIndexPath = IndexPath(row: index, section: indexPath.section)
            
            let bookinStatus = sectionOpenStatus[newIndexPath.section].row[newIndexPath.row].bookingStatus
            
            if  bookinStatus.uppercased() == Constant.OptionalTour.NOT_BOOKED
            {
                let optionalMaster:OptionalMaster = sectionOpenStatus[newIndexPath.section].optionalPrice
               if let passenger:Passenger = sectionOpenStatus[newIndexPath.section].row[newIndexPath.row].passenger
               {
                
                if sectionOpenStatus[newIndexPath.section].row[newIndexPath.row].isSelected == true {
                    
                    self.updateOnRowDeSelected(indexPath: newIndexPath,optionalMaster:optionalMaster,passenger:passenger)
                    
                }
                }
                
            }
            
        }
    }
    
    func updateSectionOnSelectAll(indexPath:IndexPath)
    {
        for index in 1..<sectionOpenStatus[indexPath.section].row.count
        {
            let newIndexPath = IndexPath(row: index, section: indexPath.section)
            
            let bookinStatus = sectionOpenStatus[newIndexPath.section].row[newIndexPath.row].bookingStatus
            
            if  bookinStatus.uppercased() == Constant.OptionalTour.NOT_BOOKED
            {
                let optionalMaster:OptionalMaster = sectionOpenStatus[newIndexPath.section].optionalPrice
                
                if let passenger:Passenger = sectionOpenStatus[newIndexPath.section].row[newIndexPath.row].passenger
                {
                
                if sectionOpenStatus[newIndexPath.section].row[newIndexPath.row].isSelected == false {
                    
                    self.updateOnRowSelected(indexPath: newIndexPath,optionalMaster:optionalMaster,passenger:passenger)
                    
                }
                }
                
            }
            
        }
    }
    
    //MARK: - Optional Data Creation
    
    func generateOptionalDetailsData() {
        
        let uniqueOptionalCodes:Array<String> = self.getUniqueOptionalCodeFromOptionalMaster()
        
        let sotrtedUniqueOptioanlCodes:Array<String>  = uniqueOptionalCodes.sorted(){$0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedAscending}
        
        var optionaIndex = 0
        
        for optionalCode in sotrtedUniqueOptioanlCodes
        {
            
            var rowTuple:[(isSelected: Bool, index: Int ,passenger:Passenger?,bookingStatus:String )] = []
            var optionalDetailsIndex = 0;
            
            var totalPackagePrice = 0.0
            
            let indexPath  = IndexPath(row: 0, section: optionaIndex)
            
            if let optionalPrice:OptionalMaster = self.getOptionalPrice(optionalCode: optionalCode)
            {
                
                if let passengerArray:Array<Passenger> = self.tourDetails?.passengerRelation?.allObjects as? Array<Passenger>
                {
                    rowTuple.append((isSelected: false, index: optionalDetailsIndex ,passenger:nil,bookingStatus:Constant.OptionalTour.NOT_BOOKED))
                    
                    optionalDetailsIndex += 1
                    
                    var isSelectAll = false
                    
                    let sortedPassengerArray:Array<Passenger> = passengerArray.sorted(by: { (passenger1:Passenger, passenger2:Passenger) -> Bool in
                        passenger1.passengerNumber! < passenger2.passengerNumber!
                    })
                    
                    for passenger in sortedPassengerArray
                    {
                        
                        var bookStatus:String = Constant.OptionalTour.NOT_BOOKED
                        
                        if let optionalBookedPackage:OptionalPackageBooked = self.getOptionaBookedPackage(by: optionalCode, passengerNumber: passenger.passengerNumber!)
                        {
                            
                            //let passengerDetails:Passenger = self.getPassenger(passengerNumber:optionalBooked.passengerNumber!)
                            
                            
                            if (optionalBookedPackage.bookStatus  != nil && !(optionalBookedPackage.bookStatus?.isEmpty)!)
                            {
                                if optionalBookedPackage.bookStatus?.uppercased() ?? Constant.OptionalTour.NOT_BOOKED  == Constant.BOOKED
                                {
                                    bookStatus = optionalBookedPackage.bookStatus!
                                }
                                
                                //totalPackagePrice = totalPackagePrice + self.getTotalOptionalPrice(passenger: passenger, price: optionalPrice)
                            }
                            
                        }
                        
                        
                        if bookStatus.uppercased() == Constant.OptionalTour.NOT_BOOKED || bookStatus.uppercased() == Constant.OptionalTour.CANCELLATION_REQUESTED  ||   bookStatus.uppercased() == Constant.BOOKED
                        {
                            rowTuple.append((isSelected: false, index: optionalDetailsIndex ,passenger:passenger,bookingStatus:bookStatus))
                            
                            optionalDetailsIndex += 1
                        }
                        
                    }
                    
                    
                }
                
                
                self.sectionOpenStatus.append((isOpen: false, section: optionaIndex, row:rowTuple , optional: optionalCode,optionalPrice:optionalPrice,totalPackagePrice.rounded()))
                
                
                //to check is all optional are selected
                
                if self.checkIsAllRowSelected(indexPath:indexPath)
                {
                    
                    self.sectionOpenStatus[indexPath.section].row[0].isSelected = true
                    self.sectionOpenStatus[indexPath.section].row[0].bookingStatus = Constant.BOOKED
                    
                }
            }
            
            optionaIndex += 1
            //  sectionOpenStatus.append((isOpen: true, section: optionaIndex, row: rowTuple,))
        }
        
        
        
    }
    
    
    func checkIsOptionalPackageBooked() -> Bool
    {
        if let optionalPackageBookedArray:Array<OptionalMaster> = self.tourDetails?.optionalMasterRelation?.allObjects as? Array<OptionalMaster>
        {
            
            if optionalPackageBookedArray.count > 0
            {
                return true
            }
        }
        
        return false
        
    }
    
    func checkIsPassengerData() -> Bool
    {
        if let passengerArray:Array<Passenger> = self.tourDetails?.passengerRelation?.allObjects as? Array<Passenger>
        {
            
            if passengerArray.count > 0
            {
                return true
            }
        }
        
        return false
        
    }
    
    func checkIsDataAvailable()->Bool
    {
        if checkIsOptionalPackageBooked() && checkIsPassengerData()
        {
            return true
        }
        
        return false
    }
    
    //MARK: - Filter Data
    
    func getUniqueOptionalCode() -> Array<String> {
        
        if let optionalPackageBookedArray:Array<OptionalPackageBooked> = self.tourDetails?.bookedOptionalPackageRelation?.allObjects as? Array<OptionalPackageBooked>
        {
            
            let optionalCode = optionalPackageBookedArray.compactMap { $0.optionalCode }
            
            let optionalCodeSet:Set<String> = Set(optionalCode)
            //array.sorted {$0.localizedStandardCompare($1) == .orderedAscending}
            
            return Array(optionalCodeSet)
        }
        
        return []
        
    }
    
    func getUniqueOptionalCodeFromOptionalMaster() -> Array<String> {
        
        if let optionalPackageBookedArray:Array<OptionalMaster> = self.tourDetails?.optionalMasterRelation?.allObjects as? Array<OptionalMaster>
        {
            
            let optionalCode = optionalPackageBookedArray.compactMap { $0.optionalCode }
            
            let optionalCodeSet:Set<String> = Set(optionalCode)
            //array.sorted {$0.localizedStandardCompare($1) == .orderedAscending}
            
            return Array(optionalCodeSet)
        }
        
        return []
        
    }
    
    func getPassenger(passengerNumber:String) -> Passenger {
        
        if let passengerArray:Array<Passenger> = self.tourDetails?.passengerRelation?.allObjects as? Array<Passenger>
        {
            return (passengerArray.filter(){ $0.passengerNumber == passengerNumber }.first)!
        }
        
        return Passenger()
        
        
    }
    
    func getOptionalPrice(optionalCode:String) -> OptionalMaster? {
        
        if let optionalMaster:Array<OptionalMaster> = self.tourDetails?.optionalMasterRelation?.allObjects as? Array<OptionalMaster>
        {
            return (optionalMaster.filter(){ $0.optionalCode == optionalCode}.first)!
        }
        
        return nil
    }
    
    
    func getOptionaBookedPackage(by optionalCode:String ,passengerNumber:String ) -> OptionalPackageBooked?
    {
        
        if let optionalPackageBookedArray:Array<OptionalPackageBooked> = self.tourDetails?.bookedOptionalPackageRelation?.allObjects as? Array<OptionalPackageBooked>
        {
            
            // let filterdOptionalArray:Array<OptionalPackageBooked> = (optionalPackageBookedArray.filter(){ $0.optionalCode == optionalCode &&  $0.passengerNumber ==  passengerNumber})
            //{
            
            
            let filterdOptionalArray:Array<OptionalPackageBooked> = optionalPackageBookedArray.filter({ (optinalBookedPackage:OptionalPackageBooked) -> Bool in
                
                
                (optinalBookedPackage.passengerNumber == passengerNumber) && (optinalBookedPackage.optionalCode == optionalCode)
            })
            
            if filterdOptionalArray.count > 0
            {
                return filterdOptionalArray.first!
            }
            // }
            
        }
        return nil
    }
    
    func getTotalOptionalPrice(passenger:Passenger , price:OptionalMaster) -> Double {
        
        if let paxType = passenger.paxType
        {
            
            switch paxType.uppercased() {
            case Constant.PAX_TYPE_ADULT:
                return Double(price.adultSP ?? "0.0")!
            case Constant.PAX_TYPE_CHILD:
                return Double(price.childSP ?? "0.0")!
//            case Constant.PAX_TYPE_INFANT:
//                return Double(price.infantSP ?? "0.0")!
            default:
                return 0
            }
            
        }
        
        return 0
        
    }
    
    func getGrandTotal() -> (Double) {
        
        var grandTotal = 0.0
        
        for sectionData in sectionOpenStatus
        {
            grandTotal = grandTotal + sectionData.totalPackagePrice
            
        }
        let grandTotoalString:String = String(grandTotal).numberFormatterDecimal()
        
        return Double(grandTotoalString)!//grandTotal.rounded()
        
    }
    
    func getCurencyAmount(currency:String?,amount:String) -> (currencySymbol:String,amount:String) {
        
        let numberFormatter = NumberFormatter()
        
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        
        
        
        let formattedNumber = numberFormatter.string(from: NSNumber(value:Int64(Double((amount))!) )) //numberFormatter.string(from: NSNumber(value:Float64(amount)! ))
        
        if currency != nil
        {
            let filteredArray = formatterArray.filter(){$0.currencyCode == currency}
            
            
            if filteredArray.count > 0 {
                
                return ("\(filteredArray.first?.currencySymbol ?? "")" , " \(formattedNumber ?? "")")
                
            }
            else
            {
                return ( "\(currency ?? "")" , " \(formattedNumber ?? "")" )
            }
            
        }
        
        return ("","")
    }
    
    func getTodaysRate(by currencyCode:String) -> (rate:String,date:String) {
        
        if let filterCurrency =  self.currencyArray?.filter({ $0.currencyCode == currencyCode })
        {
            if filterCurrency.count > 0
            {
                let currencyRate = filterCurrency.first?.currencyRate
                
                let date = filterCurrency.first?.currencyDate
                
                return (currencyRate!,date!)
            }
            
            
        }
        
        
        return ("0","")
    }
    
    func getExchangeRate() -> Array<Currency>  {
        
        do {
            
            let fetchRequest: NSFetchRequest<Currency> = Currency.fetchRequest()
            
            let currency = try CoreDataController.getContext().fetch(fetchRequest)
            return currency
        }catch   {
            printLog(error)
        }
        
        return []
        
    }
    
    func getNewlyBookedOptionalDetails() -> Array<Dictionary<String,String>>
    {
        var sectionIndex:Int = -1
        
        var newlyBookedOptionalDetails:Array<Dictionary<String,String>> = Array<Dictionary<String,String>>()
        
        for _ in sectionOpenStatus
        {
            sectionIndex = sectionIndex + 1
            
            
            
            if sectionOpenStatus[sectionIndex].row.count > 1
            {
                var rowIndex:Int = -1
                
                for _ in sectionOpenStatus[sectionIndex].row
                {
                    rowIndex = rowIndex + 1
                    
                    if rowIndex > 0 && sectionOpenStatus[sectionIndex].row[rowIndex].isSelected == true
                    {
                        if let passengerDetails:Passenger = sectionOpenStatus[sectionIndex].row[rowIndex].passenger as? Passenger
                        {
                            var bookedOptionalDetails:Dictionary<String,String> = Dictionary<String,String>()
                            
                            if let optionalMaster:OptionalMaster = sectionOpenStatus[sectionIndex].optionalPrice as? OptionalMaster
                            {
                                bookedOptionalDetails[OptionalBookingConstant.OPTIONAL_CODE] = "\(optionalMaster.optionalCode ?? "")"
                                
                                
                                let totalOptioanlPrice:Double = self.getTotalOptionalPrice(passenger: passengerDetails, price: optionalMaster)
                                
                                bookedOptionalDetails[OptionalBookingConstant.AMOUNT] = "\(totalOptioanlPrice)"
                            }
                            
                            bookedOptionalDetails[OptionalBookingConstant.PAX_NUMBER] = "\(passengerDetails.passengerNumber ?? "")"
                            bookedOptionalDetails[OptionalBookingConstant.REMARKS] = Constant.DUMMY_VALUE
                            
                            newlyBookedOptionalDetails.append(bookedOptionalDetails)
                        }
                    }
                }
            }
        }
        
        return newlyBookedOptionalDetails
    }
    
    
    //MARK: UIPopoverPresentationController Delegate
    
    
    dynamic func presentationController(_ presentationController: UIPresentationController, willPresentWithAdaptiveStyle style: UIModalPresentationStyle, transitionCoordinator: UIViewControllerTransitionCoordinator?) {
        // add a semi-transparent view to parent view when presenting the popover
        let parentView = presentationController.presentingViewController.view
        
        let overlay = UIView(frame: (parentView?.bounds)!)
        overlay.backgroundColor = UIColor(white: 0.0, alpha: 0.4)
        parentView?.addSubview(overlay)
        
        let views: [String: UIView] = ["parentView": parentView!, "overlay": overlay]
        
        parentView?.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[overlay]|", options: [], metrics: nil, views: views))
        parentView?.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[overlay]|", options: [], metrics: nil, views: views))
        
        overlay.alpha = 0.0
        
        transitionCoordinator?.animate(alongsideTransition: { _ in
            overlay.alpha = 1.0
        }, completion: nil)
        
        self.overlay = overlay
    }
    
    func prepareForPopoverPresentation(_ popoverPresentationController: UIPopoverPresentationController) {
        
        popoverPresentationController.permittedArrowDirections = .any
        popoverPresentationController.sourceView = self.sourceView
        popoverPresentationController.sourceRect = CGRect(x:(self.sourceView?.bounds.midX)! , y: (self.sourceView?.bounds.midY)!, width: 0, height: 0)
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    func popoverPresentationControllerDidDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) {
        
        guard let overlay = overlay else {
            return
        }
        DispatchQueue.main.async() {
            UIView.animate(withDuration: 0.2, animations: {
                overlay.alpha = 0.0
            }, completion: { _ in
                overlay.removeFromSuperview()
            })
        }
        
    }
    
    func presentInfoPopover(description:String) {
        
        
        let popoverContentController = InfoPopUpViewController(nibName: "InfoPopUpViewController", bundle: nil)
        
        popoverContentController.presentingView = self
        // Set your popover size.
        popoverContentController.preferredContentSize = CGSize(width: UIScreen.main.bounds.width - 20, height: (UIScreen.main.bounds.height/3))
        
        popoverContentController.viewTilteString = ""
        
        popoverContentController.descriptionString = description
        popoverContentController.moduleName = "optionalinfo"
        
        // Set the presentation style to modal so that the above methods get called.
        popoverContentController.modalPresentationStyle = .popover
        
        // Set the popover presentation controller delegate so that the above methods get called.
        popoverContentController.popoverPresentationController!.delegate = self
        
        
        
        // Present the popover.
        self.present(popoverContentController, animated: true, completion: nil)
        
    }
    
    
    //MARK: - Payment
    
    @IBAction func onPayNowClick(_ sender: UIButton) {
        
        let validationMessage:String = self.isApplicableToBookPayNow()
        
        if validationMessage == "" {
        
       let optionalMailRequestData = self.generateOptionalSendMailRequest()
        
        if !optionalMailRequestData.isSingleRecordSelected
        {
            AppUtility.displayAlert(message: AlertMessage.OPTIONAL_EMAIL_SEND_VALIDATION)
            
        }else{
            
            LoadingIndicatorView.show()
        
           UserBookingController.sendOptionalEmailRequest(emailRequestData:optionalMailRequestData.dataDict, completion: { (status, message) in
                
                LoadingIndicatorView.hide()
                self.showAlertViewWithTitle(title: "", message: message)
                
                if status
                {
                    
                }
                
                
            })
            
        }
        }else
        {
            
            self.showAlertViewWithTitle(title: "", message: validationMessage)
            
        }
        
        
        
       /* let bookedOptionDetails = self.getNewlyBookedOptionalDetails()
        
        if bookedOptionDetails != nil && bookedOptionDetails.count > 0
        {
            
            let amountString:String = self.removeCharactersFromString(valueString: self.INDCurrencyLabel.text!, length: 1)
            
            if  amountString == ""
            {
                self.showAlertViewWithTitle(title: "", message: AlertMessage.AMOUNT_LESS_VALIDATION)
                
            }else
            {
                
                let amount:Double = Double(amountString) ?? 0.0
                
                
                if amount <= 0.0 || amount <= 0   {
                    
                    self.showAlertViewWithTitle(title: "", message: AlertMessage.AMOUNT_LESS_VALIDATION)
                    return
                }
                
               // self.startPayment(amount: amountString)
                
            }
            
        }*/
        
       
        
        
    }
    
    
    
    func removeCharactersFromString(valueString:String,length:Int) -> String
    {
        
        if !(valueString.isEmpty)
        {
            return valueString.toLengthOf(length: length)
        }
        
        return ""
        
        
    }
    
    
   /*
    //MARK: Helper methods
    func startPayment(amount:String) -> Void {
        if((amount) != nil){
            
//            if (Double(amount)! > 1000000.00) {
//                showAlertViewWithTitle(title: "Amount Exceeding the limit", message: "1000000")
//                return
//            }
            
            
            params.amount = amount
        }
        //PUMEnvironment.test for test environment and PUMEnvironment.production for live environment.
        params.environment = AuthenticationConstant.ENVIRONMENT;
        params.firstname = AuthenticationConstant.FIRST_NAME//txtFldName.text;
        params.key = AuthenticationConstant.KEY//"hZSUBFuI"//
        params.merchantid = AuthenticationConstant.merchantid// //"396132";  //Merchant merchantid
        params.logo_url = AuthenticationConstant.LOGO_URL; //Merchant logo_url
        params.productinfo = AuthenticationConstant.PRODUCT_INFO;
        params.email = AuthenticationConstant.EMAIL//txtFldEmail.text;  //user email
        params.phone = AuthenticationConstant.PHONE //user phone
        params.txnid = utils.getRandomString(2);  //set your correct transaction id here
        params.surl = AuthenticationConstant.PAYU_SUCCESS_URL
        params.furl = AuthenticationConstant.PAYU_FAILUER_URL
        
        //Below parameters are optional. It is to store any information you would like to save in PayU Database regarding trasnsaction. If you do not intend to store any additional info, set below params as empty strings.
        
        params.udf1 = "";
        params.udf2 = "";
        params.udf3 = "";
        params.udf4 = "";
        params.udf5 = "";
        params.udf6 = "";
        params.udf7 = "";
        params.udf8 = "";
        params.udf9 = "";
        params.udf10 = "";
        //We strictly recommend that you calculate hash on your server end. Just so that you can quickly see demo app working, we are providing a means to do it here. Once again, this should be avoided.
        if(params.environment == PUMEnvironment.production){
            generateHashForProdAndNavigateToSDK()
        }
        else{
            calculateHashFromServer()
        }
        // assign delegate for payment callback.
        params.delegate = self;
    }
    
    func generateHashForProdAndNavigateToSDK() -> Void {
        let txnid = params.txnid!
        //add your salt in place of 'salt' if you want to test on live environment.
        //We suggest to calculate hash from server and not to keep the salt in app as it is a severe security vulnerability.
        let hashSequence : NSString = "\(params.key!)|\(txnid)|\(params.amount!)|\(params.productinfo!)|\(params.firstname!)|\(params.email!)|||||||||||\(AuthenticationConstant.SALT)" as NSString
        let data :NSString = utils.createSHA512(hashSequence as String!) as NSString
        params.hashValue = data as String!;
        startPaymentFlow();
    }
    
    
    func transactinCanceledByUser() -> Void {
        self.dismiss(animated: true){
            self.showAlertViewWithTitle(title: "", message: AlertMessage.PAYMENT_CANCELLED)
        }
    }
    
    func startPaymentFlow() -> Void {
        let paymentVC : PUMMainVController = PUMMainVController()
        var paymentNavController : UINavigationController;
        paymentNavController = UINavigationController(rootViewController: paymentVC);
        self.present(paymentNavController, animated: true, completion: nil)
    }
    
    func transactionCompleted(withResponse response : NSDictionary,errorDescription error:NSError) -> Void {
        
        printLog(response)
        
        self.dismiss(animated: true){
           // self.showAlertViewWithTitle(title: "", message: AlertMessage.PAYMENT_SUCCESS)
        }
        
        
        
        LoadingIndicatorView.show()
        
        self.generatePaymentReceipt(response:response)
        
        
    }
    
    
    
    func transactinFailed(withResponse response : NSDictionary,errorDescription error:NSError) -> Void {
        self.dismiss(animated: true){
            self.showAlertViewWithTitle(title: "", message: AlertMessage.PAYMENT_FAILED)
        }
        
        self.resetReciptGenerationFXDetails()
    }
    
    func transactinExpired(withResponse msg: NSString) -> Void
    {
        self.dismiss(animated: true){
            self.showAlertViewWithTitle(title: "", message: AlertMessage.TRANSACTION_EXPIRED)
        }
        
    }
    
 
    
    // MARK:HASH CALCULATION
    
    //hash calculation strictly recommended to be done on your server end. This is just to show the hash sequence format and oe the api call for hash should be. Encryption is SHA-512.
    func prepareHashBody()->NSString{
        return "key=\(params.key!)&amount=\(params.amount!)&txnid=\(params.txnid!)&productinfo=\(params.productinfo!)&email=\(params.email!)&firstname=\(params.firstname!)" as NSString;
    }
    
    func calculateHashFromServer(){
        let config = URLSessionConfiguration.default // Session Configuration
        let session = URLSession(configuration: config) // Load configuration into Session
        let url = URL(string: AuthenticationConstant.PAYU_HASH_CALCULATION_URL)!
        var request = URLRequest(url: url)
        request.httpBody = prepareHashBody().data(using: String.Encoding.utf8.rawValue)
        request.httpMethod = "POST"
        
        let task = session.dataTask(with: request, completionHandler: {
            (data, response, error) in
            if error != nil {
                printLog(error!.localizedDescription)
            } else {
                do {
                    if let json = try JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any]{
                        //Implement your logic
                        printLog(json)
                        let status : NSNumber = json["status"] as! NSNumber
                        if(status.intValue == 0)
                        {
                            self.params.hashValue = json["result"] as! String!
                            OperationQueue.main.addOperation {
                                self.startPaymentFlow()
                            }
                        }
                        else{
                            OperationQueue.main.addOperation {
                                self.showAlertViewWithTitle(title: "Message", message: json["message"] as! String)
                            }
                        }
                    }
                } catch {
                    printLog("error in JSONSerialization")
                }
            }
        })
        task.resume()
    }
    
    */
    
    // MARK: - Payment Generation
    
    
    func showAlertViewWithTitle(title : String,message:String) -> Void {
        let alertController : UIAlertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: AlertMessage.TITLE_OK, style: UIAlertAction.Style.default) {
            UIAlertAction in
            NSLog("OK Pressed")
        }
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    
    // MARK: - Payment Generation
    
    func generatePaymentReceipt(response : Dictionary<String,Any>)  {
        
        LoadingIndicatorView.show("Loading")
        
        let paymentId:String = String(response[Constant.PAYU_RESPONSE_KEY_PAYMENTID] as? Int ?? 0)
        
        let amount = response[Constant.PAYU_RESPONSE_KEY_AMOUNT] as? String ?? ""
        
        
        UserBookingController.sendPaymentReceiptGenerationRequest(bfNumber:(tourDetails?.bfNumber)! , paymentId:paymentId , amount:amount , bookingFileName: (tourDetails?.bookingFileName)!,reciptGenerationFXDetails: self.reciptGenerationFXDetails,completion:{ (operationValidationMessage) in
            
            
            DispatchQueue.main.sync {
                
                LoadingIndicatorView.hide()
                
                self.sendNewlyBookedOptionalDetails()
                
                
                let paymentdetail:[PaymentDetails] = self.generatePaymentConfiramtionDetails(response: response, reciptNumber: operationValidationMessage.data, tour: self.tourDetails)
                
                
                if operationValidationMessage.status
                {
                    //self.showAlertViewWithTitle(title: "", message: AlertMessage.PAYMENT_RECIPT_GENERATED)
                    
                    self.displayPaymentConfiramtion(paymentDetails: paymentdetail, reciptStatus: true)
                    
                }
                else
                {
                    //self.showAlertViewWithTitle(title: "", message: AlertMessage.PAYMENT_RECIPT_GENERATED_FAIL)
                    
                    self.displayPaymentConfiramtion(paymentDetails: paymentdetail, reciptStatus: false)
                }
                
            }
            
            /*   if operationValidationMessage.status
             {
             
             DispatchQueue.main.sync {
             
             self.showAlertViewWithTitle(title: "", message: AlertMessage.PAYMENT_RECIPT_GENERATED)
             
             }
             
             }
             else
             {
             self.showAlertViewWithTitle(title: "", message: AlertMessage.PAYMENT_RECIPT_GENERATED_FAIL)
             }*/
            
        });
    }
    
    
    
    func displayPaymentConfiramtion(paymentDetails:[PaymentDetails],reciptStatus:Bool)
    {
        let paymentConfirmationVC:PaymentConfirmationViewController = PaymentConfirmationViewController(nibName: "PaymentConfirmationViewController", bundle: nil)
        
        paymentConfirmationVC.paymentonfirmationListArray = paymentDetails
        
        
        
        self.present(paymentConfirmationVC, animated: true, completion: {
            
            _ = self.navigationController?.popToViewController(self, animated: true)
        })   //pushViewController(paymentConfirmationVC, animated: true)
        
        ///self.present(paymentConfirmationVC, animated: true) {
        
        //
        
        // }
        
        
    }
    
    func generatePaymentConfiramtionDetails(response:Dictionary<String,Any>,reciptNumber:String,tour:Tour?) -> [PaymentDetails]
    {
        // var paymentDetailsArray:[PaymentDetails] = []
        
        let amount = "\u{20B9}" + (response[Constant.PAYU_RESPONSE_KEY_AMOUNT] as? String ?? "0")
        let transactionId = response[Constant.PAYU_RESPONSE_KEY_TRANSACTION_ID] as? String ?? ""
        let transactionDate = response[Constant.PAYU_RESPONSE_KEY_ADDED_ON] as? String ?? ""
        let productinfo = response[Constant.PAYU_RESPONSE_KEY_PROD_ON] as? String ?? ""
        
        var name = ""
        var number = ""
        var email = ""
        
        
        
        let passengerArray:Array<Passenger> = self.sortPassengerByPassengerNumber()
        
        if passengerArray.count > 0
        {
            
            if let passenger:Passenger =  passengerArray.first
            {
                name = "\(passenger.firstName ?? "" ) \(passenger.lastName ?? "" )"
                number = "\(passenger.mobileNumber ?? "" )"
                email = "\(passenger.emailId ?? "" )"
            }
        }
        
        
        
        // let name = //response[Constant.PAYU_RESPONSE_KEY_NAME] as? String ?? ""
        // let number = //response[Constant.PAYU_RESPONSE_KEY_PHONE_NUM] as? String ?? ""
        // let email = //response[Constant.PAYU_RESPONSE_KEY_EMAIL_ID] as? String ?? ""
        
        let bookingId = tour?.bfNumber ?? ""
        let travelDate =  AppUtility.convertDateToString(date: tour?.departureDate as! Date)
        
        
        let paymentDetailAmount:PaymentDetails = PaymentDetails.init(displayTitle: "Amount Paid : "
            , displaTitleValue: amount)
        
        let paymentDetailReciptNumber: PaymentDetails = PaymentDetails.init(displayTitle: "Recipt Number : ", displaTitleValue: reciptNumber)
        
        let paymentDetailReciptTransactionID: PaymentDetails = PaymentDetails.init(displayTitle: "Transaction ID : ", displaTitleValue: transactionId)
        
        
        let paymentDetailTransactionNumber: PaymentDetails = PaymentDetails.init(displayTitle: "Transaction Date : ", displaTitleValue: transactionDate)
        
        let paymentDetailProductDesc: PaymentDetails = PaymentDetails.init(displayTitle: "Product Description : ", displaTitleValue: productinfo)
        
        let paymentDetailBookingID: PaymentDetails = PaymentDetails.init(displayTitle: "Booking ID : ", displaTitleValue: bookingId)
        
        let paymentDetailTRVLDate: PaymentDetails = PaymentDetails.init(displayTitle: "Travel Date : ", displaTitleValue: travelDate)
        
        let paymentDetailName: PaymentDetails = PaymentDetails.init(displayTitle: "Name : ", displaTitleValue: name)
        
        let paymentDetailEmail: PaymentDetails = PaymentDetails.init(displayTitle: "Email ID : ", displaTitleValue: email)
        
        let paymentDetailMobile: PaymentDetails = PaymentDetails.init(displayTitle: "Mobile Number : ", displaTitleValue: number)
        
        let paymentDetails:[PaymentDetails] = [paymentDetailAmount,paymentDetailReciptNumber,paymentDetailTRVLDate,paymentDetailTransactionNumber,paymentDetailProductDesc,paymentDetailBookingID,paymentDetailTRVLDate,paymentDetailName,paymentDetailEmail,paymentDetailMobile]
        
        return paymentDetails
        
        
    }
    
    
    func resetReciptGenerationFXDetails() {
        
        self.reciptGenerationFXDetails = (isFX:false,fxCurrencyType:"",roeRate:"",fxAmount:"",isA2Attached:false)
    }
    
    func sendNewlyBookedOptionalDetails()
    {
        let bookedOptionDetails = self.getNewlyBookedOptionalDetails()
        
        if bookedOptionDetails != nil && bookedOptionDetails.count > 0
        {
            UserBookingController.sendBookedOptionalDetailsRequest(bfNumber: (tourDetails?.bfNumber)!, bookedOptionalDetails: bookedOptionDetails) { (status) in
                
                
                
                DispatchQueue.main.sync {
                    
                    LoadingIndicatorView.hide()
                    
                    self.refreshTourDetail(bfnNumber: (self.tourDetails?.bfNumber)!)
                    
                    
                }
                
                printLog(status)
            }
            
        }
    }
    
    func getUpdatedBookingListDetails(bfNumber:String)
    {
//            UserBookingController.sendGetBFNDetailRequest(bfNumber: bfNumber) { (status, tourDetails) in
//                
//                
//                
//        }
    }
    
    func getAttributedString(value:(currencySymbol:String,amount:String),label:UILabel) -> NSMutableAttributedString {
        
        let tourDateLabelAttr = [convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor): label.textColor, convertFromNSAttributedStringKey(NSAttributedString.Key.font):label.font] as [String : Any]
        
        let color:UIColor = AppUtility.hexStringToUIColor(hex: "F1585C")
        
        let fisrtLetterAttr = [convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor):color , convertFromNSAttributedStringKey(NSAttributedString.Key.font): label.font] as [String : Any]
        
        
        let tourPartOne = NSMutableAttributedString(string: value.currencySymbol, attributes: convertToOptionalNSAttributedStringKeyDictionary(fisrtLetterAttr))
        let tourPartTwo = NSMutableAttributedString(string: value.amount , attributes: convertToOptionalNSAttributedStringKeyDictionary(tourDateLabelAttr))
        let tourCombination = NSMutableAttributedString()
        tourCombination.append(tourPartOne)
        tourCombination.append(tourPartTwo)
        
        return  tourCombination
    }
    
    func refreshTourDetail(bfnNumber:String) {
        
        LoadingIndicatorView.show("Loading")
        
        UserBookingController.fetchTourDetails(bfNumber: bfnNumber) { (messageTourDetail, tourObject) in
            
            if messageTourDetail.status
            {
                
                
                DispatchQueue.main.sync {
                
                LoadingIndicatorView.hide()
                
                CoreDataController.getContext().refresh(self.tourDetails!, mergeChanges: true)
                
                    if self.currencyArray != nil && (self.currencyArray?.count)! > 0
                    {
                         self.currencyArray?.removeAll()
                    }
                    
                    
                   // self.currencyArray = self.getExchangeRate();
                    
                     self.fetchROEData()
                    
                    if self.sectionOpenStatus.count > 0
                    {
                        self.sectionOpenStatus.removeAll()
                    }
                    
                    self.generateOptionalDetailsData()
                    self.self.optionalPackageTableView.reloadData()
                
               
                
                
                }
                
                
                
            }
            
            
        }
        
        
        
    }
    
    func noDataAlert(message:String)
    {
        let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: AlertMessage.TITLE_OK, style: UIAlertAction.Style.default, handler: {
            alert -> Void in
            
            self.navigationController?.popViewController(animated: true)
        }))
        
        self.present(alert, animated: true, completion: nil)
    }

    
    func sortPassengerByPassengerNumber() -> Array<Passenger> {
        
        if let passengerArray:Array<Passenger> = self.tourDetails?.passengerRelation?.allObjects as? Array<Passenger>
        {
            if passengerArray.count > 0
            {
                
                let sortedPassengerArray = passengerArray.sorted(){ $0.passengerNumber! < $1.passengerNumber!}
                
                return sortedPassengerArray
                
            }
        }
        
        return []
        
    }
    
    func generateOptionalSendMailRequest() -> (dataDict:Dictionary<String,Any>,isSingleRecordSelected:Bool)
    {
        var sendMailDataDict:Dictionary<String,Any>  = Dictionary<String,Any>()
        
       // var sectionIndex:Int = 0
        
        var isSingleRecordSelected:Bool = false
        
         sendMailDataDict["messageType"] = "OPTIONAL"
        
         sendMailDataDict["TourName"] = self.tourDetails?.tourName ?? ""
        
        let tourDateInNSDate = tourDetails?.departureDate
        
        var tourDate:String = ""
        
        if tourDateInNSDate != nil
        {
            tourDate = AppUtility.convertDateToString(currentDateFormat: "yyyy-MM-dd", date:tourDateInNSDate! as Date, requiredDateFormat: "dd-MM-yyyy")
        }
        
        sendMailDataDict["TourDate"] = tourDate
        
        sendMailDataDict["BookingID"] = self.tourDetails?.bfNumber ?? ""
        sendMailDataDict["BranchName"] = self.tourDetails?.branch ?? ""
        
        sendMailDataDict["SalesUser"] = self.tourDetails?.createdBy ?? ""
        
        
        var passengerName:String = ""
        var contactNumber:String = ""
        
        let passengerArray:Array<Passenger> = self.sortPassengerByPassengerNumber()
        
        var passengerContactNumber = ""
        
        if passengerArray.count > 0
        {            
            if let passenger:Passenger =  passengerArray.first
            {
                passengerName = "\(passenger.firstName ?? "" ) \(passenger.lastName ?? "" )"
                
                 passengerContactNumber = passenger.mobileNumber ?? ""
                
            }
        }
        
        
        if !passengerContactNumber.isEmpty && !(passengerContactNumber == "")
        {
            contactNumber = passengerContactNumber
            
        }else{
            
            contactNumber = self.tourDetails?.mobileNumber ?? ""
        }
        
        sendMailDataDict["CustomerName"] = passengerName
        
        sendMailDataDict["ContactNumber"] = contactNumber
        
        sendMailDataDict["GroupDate"] = AppUtility.changeDateFormat(currentDateFormat: "dd-MM-yyyy", date: tourDate, requiredDateFormat: "dd-MM-yyyy")
        
         sendMailDataDict["NewDate"] = ""
        
        
        var optionalList:Array<Any> = []
        
        
        for singleSection in sectionOpenStatus
        {
            
            // To get selected optionalPassenger
            
          let filtereredArray =  singleSection.row.filter({ (isSelected: Bool, index: Int, passenger: Passenger?, bookingStatus: String) -> Bool in
                
            return isSelected == true && bookingStatus.uppercased() == Constant.OptionalTour.NOT_BOOKED
            
            })
            
            
          if  filtereredArray.count > 0
        
          {
            var optionalDataDict:Dictionary<String,Any>  = Dictionary<String,Any>()
            
            
            let optionalTourName:String = singleSection.optionalPrice.optionalDescription ?? ""
            let currency:String = singleSection.optionalPrice.optionalCurrency ?? ""
            
            
            optionalDataDict["optionalTourName"] = optionalTourName
             optionalDataDict["currency"] = currency
            
            var passengerList:Array<Any> = []
            
            //var rowIndex:Int = 0
            
            
            for singlePassenger in filtereredArray
            {
                if singlePassenger.isSelected
                {
                 
                    if let passenger = singlePassenger.passenger
                    {
                    
                    isSingleRecordSelected = true
                    
                    var passengerDataDict:Dictionary<String,Any>  = Dictionary<String,Any>()
                
                    
                    passengerDataDict["firstname"] = passenger.firstName ?? ""
                    passengerDataDict["lastname"] = passenger.lastName ?? ""
                    passengerDataDict["passengerType"] = passenger.paxType ?? ""
                
                    passengerDataDict["mailId"] = singlePassenger.passenger?.emailId ?? ""
                
                    
                    let totalOptioanlPrice:Double =  self.getTotalOptionalPrice(passenger: passenger, price: singleSection.optionalPrice)
                    
                
                    passengerDataDict["price"] = "\(totalOptioanlPrice)"
                
                    passengerList.append(passengerDataDict)
                        
                    }
                    
                }
                
            }
            
            optionalDataDict["passengerList"] = passengerList
            
            var optionaListDataDict:Dictionary<String,Any> = ["optionalData":optionalDataDict]
            
            optionalList.append(optionaListDataDict)
            
            }
            
        }
        
        sendMailDataDict["OptionalList"] = optionalList
        
        
        printLog(sendMailDataDict)
        
        return (dataDict:sendMailDataDict,isSingleRecordSelected:isSingleRecordSelected)
        
    }
    
    
    func isApplicableToBookPayNow() -> String {
        
        if tourDetails?.departureDate != nil
        {
        
            if let days:Int = self.getDaysBetweenTwoDates(firstDate: Date(), secondDate: tourDetails?.departureDate! as! Date)
        {
            if days < 15
            {
                return  AlertMessage.OPTIONAL_BOOKING_BEFORE_7_DAY
                
            }
            
        }
        }
        
        return ""
    }
    
    func getDaysBetweenTwoDates(firstDate:Date,secondDate:Date) -> Int?
    {
        let calendar = NSCalendar.current
        
        // Replace the hour (time) of both dates with 00:00
        let date1 = calendar.startOfDay(for: firstDate)
        let date2 = calendar.startOfDay(for: secondDate)
        
        let components = calendar.dateComponents([.day], from: date1, to: date2)
        
        return components.day
    }
    /*
    func fetchROEData()  {
        
        LoadingIndicatorView.show()
        
        UserBookingController.getExchangeRate { (status, currenyData) in
            if status && currenyData != nil  && currenyData?.count ?? 0 > 0
            {
                self.currencyArray = currenyData! //self.getExchangeRate();
                
               // self.showAlertForFewSecondsWithTitle(title: "", message: AlertMessage.Payment.ROE_DATA_UPDATED)
                
            }else
            {
                self.showAlertROEDataNotFoundTitle(title: AlertMessage.TITLE_Alert, message:"\(AlertMessage.Payment.ROE_DATA_NOT_FOUND)")
            }
            
            LoadingIndicatorView.hide()
        }
        
        
    }
    */
    
    func fetchROEData()  {
        
        LoadingIndicatorView.show()
        
        UserBookingController.getASTRASessionAndTockenID { (status, dataDict) in
            
            if status && dataDict != nil
            {
                
                if let fcyCurrencyDetails = self.getFirstFCurrencyValue(dataDict: dataDict!)
                {
                    UserBookingController.getCurrencyByCode(currencyCode: fcyCurrencyDetails.currencyCode, requestId: fcyCurrencyDetails.requestId, sessionId: fcyCurrencyDetails.tokenId, completion: { (status, currencyData) in
                        
                        LoadingIndicatorView.hide()
                        
                        printLog("getCurrencyByCode :\(status)")
                        
                        if status && currencyData != nil
                        {
                            if  self.currencyArray == nil
                            {
                                self.currencyArray = []
                                
                            }
                            
                            self.currencyArray?.append(currencyData!)
                            
                        }else{
                            
                            self.showAlertROEDataNotFoundTitle(title: AlertMessage.TITLE_Alert, message:"\(AlertMessage.Payment.ROE_DATA_NOT_FOUND)")
                        }
                        
                        
                    })
                    
                }else{
                    
                    LoadingIndicatorView.hide()
                    
                    self.showAlertROEDataNotFoundTitle(title: AlertMessage.TITLE_Alert, message:"\(AlertMessage.Payment.ROE_DATA_NOT_FOUND)")
                }
            }else
            {
                LoadingIndicatorView.hide()
                self.showAlertROEDataNotFoundTitle(title: AlertMessage.TITLE_Alert, message:"\(AlertMessage.Payment.ROE_DATA_NOT_FOUND)")
            }
            
        }
        
    }
        func getFirstFCurrencyValue(dataDict:Dictionary<String,Any>) -> (currencyCode:String,requestId:String,tokenId:String)? {
            
            if let requestId = dataDict["requestId"] as? String , !requestId.isEmpty {
                
                if let tokenId = dataDict["tokenId"] as? String , !tokenId.isEmpty {
                    
                    if let optionalCurrencyCodeArray:Array<String> = getUniqueFCCurrencyCodeFromOptionalMaster()
                    {
                        if (optionalCurrencyCodeArray.count) > 0
                        {
                            if let currency:String = optionalCurrencyCodeArray.first
                            {
                                return (currency,requestId,tokenId)
                            }
                        }
                    }
                }
                
            }
            return nil
    }
    
    func getUniqueFCCurrencyCodeFromOptionalMaster() ->Array<String>? {
        
        if let optionalPackageBookArray:Array<OptionalMaster> = self.tourDetails?.optionalMasterRelation?.allObjects as? Array<OptionalMaster>
        {
            
            let filteredOptionalCurrencyCodeArray:Array<OptionalMaster> = optionalPackageBookArray.filter({ (optionalMaster) -> Bool in
                optionalMaster.optionalCurrency != "INR"
            })
            
            let optionalCurrencyCode = filteredOptionalCurrencyCodeArray.compactMap { $0.optionalCurrency }
            let optionalCurrencyCodeSet:Set<String> = Set(optionalCurrencyCode)
            //array.sorted {$0.localizedStandardCompare($1) == .orderedAscending}
            
            return Array(optionalCurrencyCodeSet)
        }
        
        
        
        return nil
    }
        
    func showAlertROEDataNotFoundTitle(title : String,message:String) -> Void {
        let alertController : UIAlertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: AlertMessage.TITLE_CANCEL, style: UIAlertAction.Style.default) {
            UIAlertAction in
            
           self.navigationController?.popViewController(animated: true)
        }
        let cancleAction = UIAlertAction(title: AlertMessage.TITLE_OK, style: UIAlertAction.Style.default) {
            UIAlertAction in
            printLog("user click tru again")
            
            if (NetworkReachabilityManager.init()?.isReachable)! {
                self.fetchROEData()
            }else
            {
                self.showAlertROEDataNotFoundTitle(title: AlertMessage.TITLE_Alert, message: AlertMessage.Payment.ROE_DATA_NOT_FOUND)
            }
        }
        alertController.addAction(okAction)
        alertController.addAction(cancleAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    
    
    func showAlertForFewSecondsWithTitle(title : String,message:String) -> Void {
        let alertController : UIAlertController = UIAlertController(title: "", message: message, preferredStyle: UIAlertController.Style.alert)
        
        self.present(alertController, animated: true, completion: nil)
        
        
        let when = DispatchTime.now() + 0.9
        DispatchQueue.main.asyncAfter(deadline: when){
            // your code with delay
            alertController.dismiss(animated: true, completion: nil)
        }
        
    }
    
    
    
    
}





// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromNSAttributedStringKey(_ input: NSAttributedString.Key) -> String {
	return input.rawValue
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}
