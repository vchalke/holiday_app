//
//  SlideMenuViewController.m
//  genie
//
//  Created by ketan on 20/04/15.
//  Copyright (c) 2015 Mobicule. All rights reserved.
//

#import "SlideMenuViewController.h"
#import "SlideNavigationController.h"
#import "SearchHolidaysVC.h"
#import "TabMenuVC.h"
#import "UIViewController+MJPopupViewController.h"
#import "FindBranchVC.h"
#import "WebViewController.h"
#import "MyProfileVc.h"
#import "OrderSummaryVC.h"
#import "MyFavouriteVC.h"
#import "UITextField+categoryTextField.h"
#import "LandingPageViewController.h"
#import "NetCoreAnalyticsVC.h"
#import "Thomas_Cook_Holidays-Swift.h"
#import "NewLoginPopUpViewController.h"
//#import <NetCorePush/NetCorePush.h>


@interface SlideMenuViewController  ()<ManageHolidayPopUpVCDelegate>
{
    NSMutableArray      *sectionTitleArray;
    NSMutableDictionary *sectionContentDict;
    NSMutableArray      *arrayForBool,*arrayOfMenuImages;
    NSIndexPath *preIndexPath;
    KLCPopup *customePopUp;
    int keyBoardHeight;
    int txtFieldheight;
    UIView *popUpViewFrame;
    NSMutableDictionary *payloadList; 
 
}
@end

@implementation SlideMenuViewController
- (void)viewDidLoad {
    
    [super viewDidLoad];
    [[SlideNavigationController sharedInstance] setNavigationBarHidden:YES];
    
  
    [[NSBundle mainBundle]loadNibNamed:@"SlideMenuViewController" owner:self options:nil];
    [super addViewInBaseView:self.slideMenuView];
    
    _imgViewSectionFooter.image = [UIImage imageNamed:@"horizonatl_divider"];

    if (!sectionTitleArray)
    {
        sectionTitleArray = [NSMutableArray arrayWithObjects:@"Home", @"Holidays", @"Forex", @"Flights", @"Hotels", @"Visa",@"Insurance"/*,@"My Favourite"*/,@"Manage Your Holiday"/*@"Self Service"*/ /*@"My Account"*/,@"Customer Support", nil];
    }
    
    if (!arrayOfMenuImages)
    {
        arrayOfMenuImages = [NSMutableArray arrayWithObjects:@"home", @"holidays",@"forex_list",@"flight_list", @"hotel",@"visa_menuIcon",@"insurance",/*@"heart_footer@2x",*//*@"my_account"*/@"SelfServiceMenu", @"customer_support", nil];
    }
    if (!arrayForBool)
    {
        arrayForBool = [[NSMutableArray alloc]init];
        
        for (int i = 0 ; i < [sectionTitleArray count]; i++)
        {
            [arrayForBool addObject:[NSNumber numberWithBool:NO]];
        }
    }

    if (!sectionTitleArray)
    {
        sectionTitleArray = [NSMutableArray arrayWithObjects:@"Home", @"Holiday", @"Forex", @"Flight", @"Hotels", @"Visa",@"Insurance",/*@"My Favourite",*/@"Manage Your Holiday"/*@"Self Service"*/ /*@"My Account"*/,@"Customer Support", nil];
    }
    
    if (!arrayOfMenuImages)
    {
        arrayOfMenuImages = [NSMutableArray arrayWithObjects:@"home", @"holidays",@"forex_list",@"flight_list",
                             @"hotel",@"visa",@"insurance",/*@"heart_footer@2x",*/@"my_account",
                             @"customer_support", nil];
    }
   
    if (!sectionContentDict)
    {
        //home
        sectionContentDict  = [[NSMutableDictionary alloc] init];
        NSArray *array0     = [NSArray new];
        [sectionContentDict setValue:array0 forKey:[sectionTitleArray objectAtIndex:0]];
        
        //Holiday
        NSArray *array1     = [NSArray arrayWithObjects:@"International Holidays", @"Indian Holidays",nil];
        [sectionContentDict setValue:array1 forKey:[sectionTitleArray objectAtIndex:1]];
        
        //Forex NAtive
    /*  NSArray *array2     = [NSArray arrayWithObjects:@"Foreign Exchange",@"Buy Forex",@"Sell Forex",@"Money Transfer",@"Reload Forex Card",@"Currency Converter",@"Live Rates",@"Pending Transaction",@"Card Balance" ,nil];*/
        
        //Forex Web
         NSArray *array2     = [NSArray arrayWithObjects:@"Foreign Exchange",@"Buy Forex",@"Sell Forex",@"Money Transfer",@"Reload Forex Card",@"Currency Converter",@"Live Rates" ,nil];
        
        [sectionContentDict setValue:array2 forKey:[sectionTitleArray objectAtIndex:2]];
        
        //Flight
        NSArray *array3     = [NSArray new];
        [sectionContentDict setValue:array3 forKey:[sectionTitleArray objectAtIndex:3]];
        
        //Hotels
        NSArray *array4     = [NSArray new];
        [sectionContentDict setValue:array4 forKey:[sectionTitleArray objectAtIndex:4]];
        
        //Visa
        NSArray *array5     = [NSArray new];
        [sectionContentDict setValue:array5 forKey:[sectionTitleArray objectAtIndex:5]];
        
        //Insurance
        NSArray *array6     = [NSArray new];
        [sectionContentDict setValue:array6 forKey:[sectionTitleArray objectAtIndex:6]];
        
        //My Favourite
       /* NSArray *array7     = [NSArray new];
        [sectionContentDict setValue:array7 forKey:[sectionTitleArray objectAtIndex:7]];
        //NSArray *array8     = [NSArray arrayWithObjects:@"Profile",@"My Bookings", nil];*/
        
        //Self Service //Manage Holidays
        NSArray *array8     = [NSArray new]; //[NSArray arrayWithObjects:@"Profile",@"My Bookings", nil];
        // @"Visit Us",
        [sectionContentDict setValue:array8 forKey:[sectionTitleArray objectAtIndex:7]];
        
        //Customer Support
        NSArray *array9    = [NSArray arrayWithObjects:@"Call Us",@"Visit Us",nil];
        [sectionContentDict setValue:array9 forKey:[sectionTitleArray objectAtIndex:8]];
        //@"Email Us",@"Partnership enquiry",@"Escalation"
    }
    
    payloadList =  [NSMutableDictionary dictionary];
  
}

-(void)viewWillAppear:(BOOL)animated
{
    [super hideMenuBar];
    [super hideMenuButton];
    [self setLoginStatusValue];
    [SlideNavigationController sharedInstance].object=self;
    UITapGestureRecognizer *gestureRec = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDealsAndOffersClicked)];
    gestureRec.numberOfTapsRequired = 1;
      [self.labelDealsOffers addGestureRecognizer:gestureRec];
      self.labelDealsOffers.userInteractionEnabled = YES;

//    [self performSelector:@selector(menuDidOpened) withObject:nil afterDelay:0.3];
//    
//    [[NSNotificationCenter defaultCenter]addObserver:self
//                                            selector:@selector(menuDidOpened)
//                                                name:SlideNavigationControllerDidReveal
//                                              object:nil];
//    
//    [[NSNotificationCenter defaultCenter]addObserver:self
//                                            selector:@selector(menuDidClosed)
//                                                name:SlideNavigationControllerDidClose
//                                              object:nil];
    }


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TextField Delegate
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    txtFieldheight = textField.frame.origin.y;
    
    if (textField == self.txtFldEmailPopUpQueryFor)
    {
        [[SlideNavigationController sharedInstance]toggleLeftMenu];
        customePopUp = [KLCPopup popupWithContentView:self.viewPopUpQueryFor
                                             showType:KLCPopupShowTypeGrowIn
                                          dismissType:KLCPopupDismissTypeFadeOut
                                             maskType:KLCPopupMaskTypeDimmed
                             dismissOnBackgroundTouch:YES
                                dismissOnContentTouch:NO];
        
        [customePopUp show];
        
        return NO;

    }
    else if(textField == self.txtFldEmailPopUpQryRltdTo)
    {
        [[SlideNavigationController sharedInstance]toggleLeftMenu];
        customePopUp = [KLCPopup popupWithContentView:self.viewPopUpQueryRelatedTo
                                             showType:KLCPopupShowTypeGrowIn
                                          dismissType:KLCPopupDismissTypeFadeOut
                                             maskType:KLCPopupMaskTypeDimmed
                             dismissOnBackgroundTouch:YES
                                dismissOnContentTouch:NO];
        
        [customePopUp show];
        
        return NO;
    }
    
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(_txtFldFirstName==textField){
         [_txtFldLastName becomeFirstResponder];
    }else if(_txtFldLastName==textField){
        _viewVisaPopUp.frame=CGRectMake(_viewVisaPopUp.frame.origin.x,-50, _viewVisaPopUp.frame.size.width, _viewVisaPopUp.frame.size.height);
         [_txtFldPhoneNo becomeFirstResponder];
    }else if (_txtFldMailId==textField){
         _viewVisaPopUp.frame=CGRectMake(_viewVisaPopUp.frame.origin.x,_viewVisaPopUp.frame.origin.y, _viewVisaPopUp.frame.size.width, _viewVisaPopUp.frame.size.height);
        [_txtFldMailId becomeFirstResponder];
         //[textField resignFirstResponder];
    }
    else if (_txtFldMailId==textField){
         [_txtFldMailId becomeFirstResponder];
    }
    [textField resignFirstResponder];
    return YES;
}

-(void)resetAllTextFields
{
    self.txtFldEmailPopUpBFN.text = @"";
    self.txtFldEmailPopUpMailId.text = @"";
    self.txtFldEmailPopUpName.text = @"";
    self.txtFldEmailPopUpPhoneNo.text = @"";
    self.txtFldEmailPopUpQryRltdTo.text = @"";
    self.txtFldEmailPopUpQueryFor.text = @"";
    
    self.txtViewEmailPopUpComplaint.text = @"";
}

#pragma mark - Notification Observers

#pragma mark SlideMenu
-(void)menuDidOpened
{
    if (self.viewContainer.isHidden)
    {
        self.viewContainer.hidden = NO;
    }
    
    [[NSBundle mainBundle]loadNibNamed:@"SlideMenuViewController" owner:self options:nil];
    [super addViewInBaseView:self.slideMenuView];
    
    
    [UIView animateWithDuration:0.5f
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         
                         self.viewContainer.frame = CGRectMake(self.viewContainer.frame.size.width,0.0f,0.0f, self.viewContainer.frame.size.height);
                         
                     }
                     completion:NULL];

}

-(void)menuDidClosed
{
    self.viewContainer.hidden = YES;
}


#pragma mark - Slide Navigation Delegate

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return NO;
}

- (BOOL)slideNavigationControllerShouldDisplayRightMenu
{
    return NO;
}


#pragma mark - TableView Delegates and Data Source

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [sectionTitleArray count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([[arrayForBool objectAtIndex:section] boolValue])
    {
        return [[sectionContentDict valueForKey:[sectionTitleArray objectAtIndex:section]] intValue];
    }
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIImageView *imageUpDownArrow;
    
    UIView *headerView         = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    headerView.tag             = section;
    headerView.backgroundColor = [super colorWithHex:0x4C4C4C A:1];

    if (section == 8 || section == 9)
    {
        headerView.backgroundColor = [super colorWithHex:0x595959 A:1];
    }
    
    UIImageView *imageForSectionHeader = [[UIImageView alloc]initWithFrame:CGRectMake(17, 11, 22, 22)];
    UILabel     *lblForSectionHeader   = [[UILabel alloc]initWithFrame:CGRectMake(60, 12, self.view.frame.size.width - 55,22 )];
    
    UIImageView *imageViewBottom = [[UIImageView alloc]initWithFrame:CGRectMake(0, 43, self.view.frame.size.width, 1)];
    
    imageViewBottom.image = [UIImage imageNamed:@"horizonatl_divider"];
    
    lblForSectionHeader.textAlignment = NSTextAlignmentLeft;
    imageForSectionHeader.image       = [UIImage imageNamed:[arrayOfMenuImages objectAtIndex:section]];

    lblForSectionHeader.font          = [UIFont fontWithName:TITILLUM_REGULAR size:17.0];
    lblForSectionHeader.textColor     = [UIColor whiteColor];
    lblForSectionHeader.text          = [sectionTitleArray objectAtIndex:section];
    
    [headerView addSubview:lblForSectionHeader];
    [headerView addSubview:imageForSectionHeader];
    [headerView addSubview:imageViewBottom];
    
    BOOL manyCells = [[arrayForBool objectAtIndex:section]boolValue];
    
    if (manyCells)
    {
        headerView.backgroundColor = [super colorWithHex:0xC5C5C5 A:1];
        NSString *strImageName     = [NSString stringWithFormat:@"%@_highlighted.png",[arrayOfMenuImages objectAtIndex:section ]];
        
        imageForSectionHeader.image = [UIImage imageNamed:strImageName];
        lblForSectionHeader.textColor = [super colorWithHex:0x4C4C4C A:1];
    }
    
    UITapGestureRecognizer *headerTapped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sectionHeaderTapped:)];
    [headerView addGestureRecognizer:headerTapped];
    
    if (section == 1 || section == 8 || section == 9|| section == 2 )
    {
        CGRect rectForUpDownArrow = CGRectMake((self.tblViewMenu.frame.size.width * 0.68), 13, 15,15);
        imageUpDownArrow = [[UIImageView alloc]initWithImage:manyCells ?[UIImage imageNamed:@"minus"] : [UIImage imageNamed:@"plus_light_gray"]];
        imageUpDownArrow.contentMode = UIViewContentModeScaleAspectFit;
        imageUpDownArrow.autoresizingMask    = UIViewAutoresizingFlexibleLeftMargin;
        imageUpDownArrow.frame               = rectForUpDownArrow;
        [headerView addSubview:imageUpDownArrow];
        
    }
    
    return headerView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
   
    
    static NSString *cellIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    
    
    BOOL myCells = [[arrayForBool objectAtIndex:indexPath.section]boolValue];
    
    if (myCells)
    {
        NSArray *arrayOfcontent = [sectionContentDict valueForKey:[sectionTitleArray objectAtIndex:indexPath.section]];
        
        cell.contentView.backgroundColor = [super colorWithHex:0x727272 A:1];

        UILabel *lblCellText  = [[UILabel alloc]initWithFrame:CGRectMake(40, 15, 150, 15)];
        lblCellText.font      = [UIFont fontWithName:TITILLUM_REGULAR size:15];
        lblCellText.text      = [arrayOfcontent objectAtIndex:indexPath.row];
        lblCellText.textColor = [super colorWithHex:0xEFEFEF A:1];
        
        UIImageView *imageViewForArrow = [[UIImageView alloc]initWithFrame:CGRectMake(20, 17, 10, 10)];
        imageViewForArrow.image = [UIImage imageNamed:@"arrow"];
        
        [cell.contentView addSubview:imageViewForArrow];
        [cell.contentView addSubview:lblCellText];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    
    return cell;
}

-(void)onSelfServiceMenuClick
{
    
      [[SlideNavigationController sharedInstance] toggleLeftMenu];
    
    ManageHolidayPopUpVC* manageHolidayPopUpVC =  [[ManageHolidayPopUpVC alloc] initWithNibName:@"ManageHolidayPopUpVC" bundle:nil];
                                                   
    manageHolidayPopUpVC.htmlPath = @"Manage Holidays";
    manageHolidayPopUpVC.popuptitle = @"Manage Your Booking";
    manageHolidayPopUpVC.managedHolidayDelegate = self;
    [manageHolidayPopUpVC setModalPresentationStyle:UIModalPresentationOverCurrentContext];

    [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:manageHolidayPopUpVC animated:false completion:nil];
    
   //  UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
    
    
  //  [AppDelegate presentViewController:manageHolidayPopUpVC animated:false completion:nil];
    
  
    
  /*  let popupVC: PopUpWebViewViewController = PopUpWebViewViewController(nibName: "PopUpWebViewViewController", bundle: nil)
    popupVC.htmlPath = "bookingPolicy"
    popupVC.popuptitle = "Booking Policy"
    popupVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
    self.present(popupVC, animated: false, completion: nil)*/
}

-(void)onContinue
{
       [AppUtility setLoginController];
}

-(void)onCancle
{
    
}

//Forex with web view

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
     [AppUtility checkIsVersionUpgrade];
    
    payloadList =  [NSMutableDictionary dictionary];


    if (indexPath.section == 1)
    {
        if (indexPath.row == 0)
        {
            //[self sectionHeaderTapped:nil];// InterNational Holidays

            SearchHolidaysVC *searchVC=[[SearchHolidaysVC alloc]initWithNibName:@"BaseViewController" bundle:nil];
            searchVC.headerName = kInternationalHoliday;
            [[SlideNavigationController sharedInstance]pushViewController:searchVC animated:YES];
            [payloadList setObject:@"yes" forKey:@"s^HOLIDAYS"];

        }

        if (indexPath.row == 1)
        {
            //[self sectionHeaderTapped:nil]; // indian holidays

            SearchHolidaysVC *searchVC=[[SearchHolidaysVC alloc]initWithNibName:@"BaseViewController" bundle:nil];
            searchVC.headerName = kIndianHoliday;
            [[SlideNavigationController sharedInstance]pushViewController:searchVC animated:YES];
            [payloadList setObject:@"yes" forKey:@"s^HOLIDAYS"];

        }

        [self netCoreDataDisplay];
    }

    else if(indexPath.section == 2)
    {

        WebViewController *webViewVC = [[WebViewController alloc]initWithNibName:@"BaseViewController" bundle:nil];
        NSString *forexUrl;

        if (indexPath.row == 0)
        {
            /*  forexUrl = kUrlForForex;
             webViewVC.headerString = @"Foreign Exchange";
             NSURL *url = [NSURL URLWithString:forexUrl];
             webViewVC.url = url;
             [[SlideNavigationController sharedInstance] pushViewController:webViewVC animated:NO];*/

            ForexLandingPageViewController *ForexLandingVc = [[ForexLandingPageViewController alloc] initWithNibName:@"ForexBaseViewController" bundle:nil];

            [[SlideNavigationController sharedInstance] pushViewController:ForexLandingVc animated:NO];

        }
        else if (indexPath.row == 1)
        {
             forexUrl = kForexBuyURL;
             webViewVC.headerString = @"Buy Forex";
             NSURL *url = [NSURL URLWithString:forexUrl];
             webViewVC.url = url;
             [[SlideNavigationController sharedInstance] pushViewController:webViewVC animated:NO];

            payloadList =  [NSMutableDictionary dictionary];
            [payloadList setObject:@"Buy Forex" forKey:@"s^FOREX"];
            [self netCoreDataDisplay];

        }
        else if (indexPath.row == 2)
        {
            forexUrl = kForexSellURL;
             webViewVC.headerString = @"Sell Forex";
             NSURL *url = [NSURL URLWithString:forexUrl];
             webViewVC.url = url;
             [[SlideNavigationController sharedInstance] pushViewController:webViewVC animated:NO];

            payloadList =  [NSMutableDictionary dictionary];
            [payloadList setObject:@"Sell Forex" forKey:@"s^FOREX"];
            [self netCoreDataDisplay];

        }
        else if (indexPath.row == 3)
        {
            forexUrl = kForexMoneyTransferURL;
             webViewVC.headerString = @"Money Transfer";
             NSURL *url = [NSURL URLWithString:forexUrl];
             webViewVC.url = url;
             [[SlideNavigationController sharedInstance] pushViewController:webViewVC animated:NO];

            payloadList =  [NSMutableDictionary dictionary];
            [payloadList setObject:@"MoneyTransfer" forKey:@"s^FOREX"];
            [self netCoreDataDisplay];

        }
        else if (indexPath.row == 4)
        {
            forexUrl = kForexReloadForexURL;
             webViewVC.headerString = @"Reload Forex";
             NSURL *url = [NSURL URLWithString:forexUrl];
             webViewVC.url = url;
             [[SlideNavigationController sharedInstance] pushViewController:webViewVC animated:NO];

            payloadList =  [NSMutableDictionary dictionary];
            [payloadList setObject:@"Reload Forex Card" forKey:@"s^FOREX"];
            [self netCoreDataDisplay];


        }
        else if (indexPath.row == 5)
        {
            forexUrl = kForexCurrencyConverterURL;
             webViewVC.headerString = @"Currency Converter";
             NSURL *url = [NSURL URLWithString:forexUrl];
             webViewVC.url = url;
             [[SlideNavigationController sharedInstance] pushViewController:webViewVC animated:NO];

            payloadList =  [NSMutableDictionary dictionary];
            [payloadList setObject:@"Currency Converter" forKey:@"s^FOREX"];
            [self netCoreDataDisplay];

        }
        else if (indexPath.row == 6)
        {
            forexUrl = kForexLiveRatesyURL;
             webViewVC.headerString = @"Live Rates";
             NSURL *url = [NSURL URLWithString:forexUrl];
             webViewVC.url = url;
             [[SlideNavigationController sharedInstance] pushViewController:webViewVC animated:NO];
            payloadList =  [NSMutableDictionary dictionary];
            [payloadList setObject:@"Live Rates" forKey:@"s^FOREX"];
            [self netCoreDataDisplay];

        }
        else if (indexPath.row == 7)
        {
            //Pending Transaction

          /*  payloadList = [NSMutableDictionary dictionary];
            [payloadList setObject:@"Pending Transaction" forKey:@"s^FOREX"];
            [self netCoreDataDisplay];

            PendingTransactionViewController *pendingTransaction = [[PendingTransactionViewController alloc]initWithNibName:@"ForexBaseViewController" bundle:nil];
            pendingTransaction.previousController = @"SlideMenuViewController";
            [[SlideNavigationController sharedInstance] pushViewController:pendingTransaction animated:NO];*/

        }
    }
    //    else if(indexPath.section == 8)
    //    {
    //
    //        NSUserDefaults *LoginStatus = [NSUserDefaults standardUserDefaults];
    //
    //
    //
    //        if (![LoginStatus valueForKey:@"isCSSUserLoggedIn"]) {
    //
    //            [AppUtility setLoginController];
    //
    //        }
    //        else
    //        {
    //
    //            [AppUtility setTabbarController];
    //        }
    //
    //       /* if (indexPath.row == 0)
    //        {
    //            //[self sectionHeaderTapped:nil];
    //
    //            MyProfileVc *myProfileObj=[[MyProfileVc alloc]initWithNibName:@"BaseViewController" bundle:nil];
    //            myProfileObj.headerName = kProfile;
    //            [[SlideNavigationController sharedInstance]pushViewController:myProfileObj animated:YES];
    //             [payloadList setObject:@"yes" forKey:@"s^MY_ACC"];
    //        }
    //
    //        if (indexPath.row == 1)
    //        {
    //            //[self sectionHeaderTapped:nil];
    //
    //            NSUserDefaults *LoginStatus = [NSUserDefaults standardUserDefaults];
    //
    //
    //
    //            if (![LoginStatus valueForKey:@"isCSSUserLoggedIn"]) {
    //
    //                [AppUtility setLoginController];
    //
    //            }
    //            else
    //            {
    //
    //                [AppUtility setTabbarController];
    //            }
    //
    //
    //
    //
    //
    ////
    ////            OrderSummaryVC *orderVC=[[OrderSummaryVC alloc]initWithNibName:@"BaseViewController" bundle:nil];
    ////            orderVC.headerName = kOrderSummary;
    ////
    //
    //
    //            // [payloadList setObject:@"yes" forKey:@"s^My_BOOKINGS"];
    //        }*/
    //
    //        [payloadList setObject:@"yes" forKey:@"s^Self_Service"];
    //         [self netCoreDataDisplay];
    //    }


    else if (indexPath.section == 8)
    {
        if (indexPath.row == 0)
        {
            //Call us
            // --  NSString *phNo = @"18002000464";
            //    -- NSURL *   url = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"tel://%@", phNo]];
            // -- [[UIApplication sharedApplication] openURL:url];

            //   NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",phNo]];
            //  [[UIApplication shared]
            // if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
            //   [[UIApplication sharedApplication] openURL:phoneUrl];
            // } else
            /*  {
             UIAlertView * calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Call facility is not available!!!" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
             [calert show];
             }
             */

            [payloadList setObject:@"yes" forKey:@"s^CUST_SUPPORT"];

            CustomIOSAlertView *alertView = [[CustomIOSAlertView alloc] init];
            CoreUtility *coreObj=[CoreUtility sharedclassname];
            coreObj.statusWhichPopUpCalled=0;
            [alertView setContainerView:[self createDemoView]];


            // Modify the parameters
            [alertView setButtonTitles:[NSMutableArray arrayWithObjects:@"Close1", @"Close2", @"Close3", nil]];
            [alertView setDelegate:self];

            // You may use a Block, rather than a delegate.
            [alertView setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
                NSLog(@"Block: Button at position %d is clicked on alertView %d.", buttonIndex, (int)[alertView tag]);
                [alertView close];
            }];

            [alertView setUseMotionEffects:true];

            // And launch the dialog
            [alertView show];


        }
        else if(indexPath.row==1)
        {
            //Visit us
            FindBranchVC *findBranchVC=[[FindBranchVC alloc]initWithNibName:@"BaseViewController" bundle:nil];
            findBranchVC.headerName=@"Find Branch";
            [[SlideNavigationController sharedInstance]pushViewController:findBranchVC animated:YES];
            [payloadList setObject:@"yes" forKey:@"s^CUST_SUPPORT"];

        }
        /*else if (indexPath.row == 3)
         {
         UIScrollView *scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, 220, 500)];
         [scrollView addSubview:self.viewEmailUsPopUp];
         [scrollView setShowsVerticalScrollIndicator:NO];
         scrollView.layer.cornerRadius = 3.0f;
         [scrollView setContentSize:CGSizeMake(220,610)];
         [scrollView setScrollEnabled:YES];

         [self resetAllTextFields];

         self.imageViewQueryFor.hidden = NO;
         self.imageViewQueryRelatedTo.hidden = NO;

         [[SlideNavigationController sharedInstance]toggleLeftMenu];
         customePopUp = [KLCPopup popupWithContentView:scrollView
         showType:KLCPopupShowTypeGrowIn
         dismissType:KLCPopupDismissTypeFadeOut
         maskType:KLCPopupMaskTypeDimmed
         dismissOnBackgroundTouch:YES
         dismissOnContentTouch:NO];

         //[customePopUp show];
         }*/
        [self netCoreDataDisplay];
    }
}


//Forex with Native module
//-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    payloadList =  [NSMutableDictionary dictionary];
//
//
//     if (indexPath.section == 1)
//    {
//        if (indexPath.row == 0)
//        {
//            //[self sectionHeaderTapped:nil];// InterNational Holidays
//
//            SearchHolidaysVC *searchVC=[[SearchHolidaysVC alloc]initWithNibName:@"BaseViewController" bundle:nil];
//            searchVC.headerName = kInternationalHoliday;
//            [[SlideNavigationController sharedInstance]pushViewController:searchVC animated:YES];
//             [payloadList setObject:@"yes" forKey:@"s^HOLIDAYS"];
//
//        }
//
//        if (indexPath.row == 1)
//        {
//            //[self sectionHeaderTapped:nil]; // indian holidays
//
//            SearchHolidaysVC *searchVC=[[SearchHolidaysVC alloc]initWithNibName:@"BaseViewController" bundle:nil];
//            searchVC.headerName = kIndianHoliday;
//            [[SlideNavigationController sharedInstance]pushViewController:searchVC animated:YES];
//             [payloadList setObject:@"yes" forKey:@"s^HOLIDAYS"];
//
//        }
//
//      [self netCoreDataDisplay];
//    }
//
//     else if(indexPath.section == 2)
//     {
//
//        WebViewController *webViewVC = [[WebViewController alloc]initWithNibName:@"BaseViewController" bundle:nil];
//         NSString *forexUrl;
//
//         if (indexPath.row == 0)
//         {
//           /*  forexUrl = kUrlForForex;
//             webViewVC.headerString = @"Foreign Exchange";
//             NSURL *url = [NSURL URLWithString:forexUrl];
//             webViewVC.url = url;
//             [[SlideNavigationController sharedInstance] pushViewController:webViewVC animated:NO];*/
//
//             ForexLandingPageViewController *ForexLandingVc = [[ForexLandingPageViewController alloc] initWithNibName:@"ForexBaseViewController" bundle:nil];
//
//             [[SlideNavigationController sharedInstance] pushViewController:ForexLandingVc animated:NO];
//
//
//           //  ForexRightSlideViewController *forexRightVC = [[ForexRightSlideViewController alloc] initWithNibName:@"ForexRightSlideViewController" bundle:nil];
//
//           //  SWRevealViewController *revealController = [[SWRevealViewController alloc] init];
//             //revealController.delegate = self;
//
//           //  revealController.rightViewController = forexRightVC;
//           //  revealController.frontViewController = ForexLandingVc;
//
//            // revealController.rightViewRevealWidth = 300;
//
//            // UINavigationController *navVc = [[UINavigationController alloc]initWithRootViewController:revealController];
//
//             //[self presentViewController:navVc animated:NO completion:nil];
//
//             //[[SlideNavigationController sharedInstance] pushViewController:ForexLandingVc animated:NO];
//         }
//         else if (indexPath.row == 1)
//         {
//            /* forexUrl = kForexBuyURL;
//             webViewVC.headerString = @"Buy Forex";
//             NSURL *url = [NSURL URLWithString:forexUrl];
//             webViewVC.url = url;
//             [[SlideNavigationController sharedInstance] pushViewController:webViewVC animated:NO];*/
//
//             payloadList =  [NSMutableDictionary dictionary];
//             [payloadList setObject:@"Buy Forex" forKey:@"s^FOREX"];
//             [self netCoreDataDisplay];
//
//             BuyForexOptionsViewController *ForexLandingVc = [[BuyForexOptionsViewController alloc] initWithNibName:@"ForexBaseViewController" bundle:nil];
//             ForexLandingVc.previousController = @"SlideMenuViewController";
//             [[SlideNavigationController sharedInstance] pushViewController:ForexLandingVc animated:NO];
//
//         }
//         else if (indexPath.row == 2)
//         {
//             /*forexUrl = kForexSellURL;
//             webViewVC.headerString = @"Sell Forex";
//             NSURL *url = [NSURL URLWithString:forexUrl];
//             webViewVC.url = url;
//             [[SlideNavigationController sharedInstance] pushViewController:webViewVC animated:NO];*/
//
//             payloadList =  [NSMutableDictionary dictionary];
//             [payloadList setObject:@"Sell Forex" forKey:@"s^FOREX"];
//             [self netCoreDataDisplay];
//
//             SellForexProductViewController *ForexLandingVc = [[SellForexProductViewController alloc] initWithNibName:@"ForexBaseViewController" bundle:nil];
//             ForexLandingVc.previousController = @"SlideMenuViewController";
//             [[SlideNavigationController sharedInstance] pushViewController:ForexLandingVc animated:NO];
//
//
//         }
//         else if (indexPath.row == 3)
//         {
//           /*  forexUrl = kForexMoneyTransferURL;
//             webViewVC.headerString = @"Money Transfer";
//             NSURL *url = [NSURL URLWithString:forexUrl];
//             webViewVC.url = url;
//             [[SlideNavigationController sharedInstance] pushViewController:webViewVC animated:NO];*/
//
//             payloadList =  [NSMutableDictionary dictionary];
//             [payloadList setObject:@"MoneyTransfer" forKey:@"s^FOREX"];
//             [self netCoreDataDisplay];
//
//             MoneyTransferViewController *ForexLandingVc = [[MoneyTransferViewController alloc] initWithNibName:@"ForexBaseViewController" bundle:nil];
//
//             ForexLandingVc.moduleName = @"Money Transfer";
//             ForexLandingVc.urlString = kForexMoneyTransferURL;
//
//             ForexLandingVc.previousController = @"SlideMenuViewController";
//             [[SlideNavigationController sharedInstance] pushViewController:ForexLandingVc animated:NO];
//         }
//         else if (indexPath.row == 4)
//         {
//             /*forexUrl = kForexReloadForexURL;
//             webViewVC.headerString = @"Reload Forex";
//             NSURL *url = [NSURL URLWithString:forexUrl];
//             webViewVC.url = url;
//             [[SlideNavigationController sharedInstance] pushViewController:webViewVC animated:NO];*/
//
//             payloadList =  [NSMutableDictionary dictionary];
//             [payloadList setObject:@"Reload Forex Card" forKey:@"s^FOREX"];
//             [self netCoreDataDisplay];
//
//             ReloadForexCardViewController *ForexLandingVc = [[ReloadForexCardViewController alloc] initWithNibName:@"ForexBaseViewController" bundle:nil];
//             ForexLandingVc.previousController = @"SlideMenuViewController";
//             [[SlideNavigationController sharedInstance] pushViewController:ForexLandingVc animated:NO];
//
//
//         }
//         else if (indexPath.row == 5)
//         {
//              /*forexUrl = kForexCurrencyConverterURL;
//             webViewVC.headerString = @"Currency Converter";
//             NSURL *url = [NSURL URLWithString:forexUrl];
//             webViewVC.url = url;
//             [[SlideNavigationController sharedInstance] pushViewController:webViewVC animated:NO];*/
//
//             payloadList =  [NSMutableDictionary dictionary];
//             [payloadList setObject:@"Currency Converter" forKey:@"s^FOREX"];
//             [self netCoreDataDisplay];
//
//            CurrencyConverterViewController *ForexLandingVc =
//             [[CurrencyConverterViewController alloc] initWithNibName:@"ForexBaseViewController" bundle:nil];
//             ForexLandingVc.previousController = @"SlideMenuViewController";
//             [[SlideNavigationController sharedInstance] pushViewController:ForexLandingVc animated:NO];
//
//
//         }
//         else if (indexPath.row == 6)
//         {
//            /* forexUrl = kForexLiveRatesyURL;
//             webViewVC.headerString = @"Live Rates";
//             NSURL *url = [NSURL URLWithString:forexUrl];
//             webViewVC.url = url;
//             [[SlideNavigationController sharedInstance] pushViewController:webViewVC animated:NO];*/
//             payloadList =  [NSMutableDictionary dictionary];
//             [payloadList setObject:@"Live Rates" forKey:@"s^FOREX"];
//             [self netCoreDataDisplay];
//
//             LiveRatesViewController *ForexLandingVc = [[LiveRatesViewController alloc] initWithNibName:@"ForexBaseViewController" bundle:nil];
//             ForexLandingVc.previousController = @"SlideMenuViewController";
//             [[SlideNavigationController sharedInstance] pushViewController:ForexLandingVc animated:NO];
//
//         }
//         else if (indexPath.row == 7)
//         {
//             //Pending Transaction
//
//             payloadList = [NSMutableDictionary dictionary];
//             [payloadList setObject:@"Pending Transaction" forKey:@"s^FOREX"];
//             [self netCoreDataDisplay];
//
//             PendingTransactionViewController *pendingTransaction = [[PendingTransactionViewController alloc]initWithNibName:@"ForexBaseViewController" bundle:nil];
//             pendingTransaction.previousController = @"SlideMenuViewController";
//             [[SlideNavigationController sharedInstance] pushViewController:pendingTransaction animated:NO];
//
//         }     else if (indexPath.row == 8)
//         {
//             /*  forexUrl = kForexMoneyTransferURL;
//              webViewVC.headerString = @"Money Transfer";
//              NSURL *url = [NSURL URLWithString:forexUrl];
//              webViewVC.url = url;
//              [[SlideNavigationController sharedInstance] pushViewController:webViewVC animated:NO];*/
//
//             payloadList =  [NSMutableDictionary dictionary];
//             [payloadList setObject:@"Know your Card Balance" forKey:@"s^FOREX"];
//             [self netCoreDataDisplay];
//
//             MoneyTransferViewController *ForexLandingVc = [[MoneyTransferViewController alloc] initWithNibName:@"ForexBaseViewController" bundle:nil];
//
//             ForexLandingVc.moduleName = @"Card Balance";
//             ForexLandingVc.urlString = kForexKnowYourCardBalanceURL;
//
//             ForexLandingVc.previousController = @"SlideMenuViewController";
//             [[SlideNavigationController sharedInstance] pushViewController:ForexLandingVc animated:NO];
//         }
//    }
////    else if(indexPath.section == 8)
////    {
////
////        NSUserDefaults *LoginStatus = [NSUserDefaults standardUserDefaults];
////
////
////
////        if (![LoginStatus valueForKey:@"isCSSUserLoggedIn"]) {
////
////            [AppUtility setLoginController];
////
////        }
////        else
////        {
////
////            [AppUtility setTabbarController];
////        }
////
////       /* if (indexPath.row == 0)
////        {
////            //[self sectionHeaderTapped:nil];
////
////            MyProfileVc *myProfileObj=[[MyProfileVc alloc]initWithNibName:@"BaseViewController" bundle:nil];
////            myProfileObj.headerName = kProfile;
////            [[SlideNavigationController sharedInstance]pushViewController:myProfileObj animated:YES];
////             [payloadList setObject:@"yes" forKey:@"s^MY_ACC"];
////        }
////
////        if (indexPath.row == 1)
////        {
////            //[self sectionHeaderTapped:nil];
////
////            NSUserDefaults *LoginStatus = [NSUserDefaults standardUserDefaults];
////
////
////
////            if (![LoginStatus valueForKey:@"isCSSUserLoggedIn"]) {
////
////                [AppUtility setLoginController];
////
////            }
////            else
////            {
////
////                [AppUtility setTabbarController];
////            }
////
////
////
////
////
//////
//////            OrderSummaryVC *orderVC=[[OrderSummaryVC alloc]initWithNibName:@"BaseViewController" bundle:nil];
//////            orderVC.headerName = kOrderSummary;
//////
////
////
////            // [payloadList setObject:@"yes" forKey:@"s^My_BOOKINGS"];
////        }*/
////
////        [payloadList setObject:@"yes" forKey:@"s^Self_Service"];
////         [self netCoreDataDisplay];
////    }
//
//
//    else if (indexPath.section == 8)
//    {
//        if (indexPath.row == 0)
//        {
//             //Call us
//         // --  NSString *phNo = @"18002000464";
//       //    -- NSURL *   url = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"tel://%@", phNo]];
//          // -- [[UIApplication sharedApplication] openURL:url];
//
//         //   NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",phNo]];
//                    //  [[UIApplication shared]
//           // if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
//             //   [[UIApplication sharedApplication] openURL:phoneUrl];
//           // } else
//          /*  {
//                UIAlertView * calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Call facility is not available!!!" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
//                [calert show];
//            }
//            */
//
//              [payloadList setObject:@"yes" forKey:@"s^CUST_SUPPORT"];
//
//            CustomIOSAlertView *alertView = [[CustomIOSAlertView alloc] init];
//            CoreUtility *coreObj=[CoreUtility sharedclassname];
//            coreObj.statusWhichPopUpCalled=0;
//            [alertView setContainerView:[self createDemoView]];
//
//
//            // Modify the parameters
//            [alertView setButtonTitles:[NSMutableArray arrayWithObjects:@"Close1", @"Close2", @"Close3", nil]];
//            [alertView setDelegate:self];
//
//            // You may use a Block, rather than a delegate.
//            [alertView setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
//                NSLog(@"Block: Button at position %d is clicked on alertView %d.", buttonIndex, (int)[alertView tag]);
//                [alertView close];
//            }];
//
//            [alertView setUseMotionEffects:true];
//
//            // And launch the dialog
//            [alertView show];
//
//
//        }
//        else if(indexPath.row==1)
//        {
//             //Visit us
//            FindBranchVC *findBranchVC=[[FindBranchVC alloc]initWithNibName:@"BaseViewController" bundle:nil];
//            findBranchVC.headerName=@"Find Branch";
//            [[SlideNavigationController sharedInstance]pushViewController:findBranchVC animated:YES];
//              [payloadList setObject:@"yes" forKey:@"s^CUST_SUPPORT"];
//
//        }
//        /*else if (indexPath.row == 3)
//        {
//            UIScrollView *scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, 220, 500)];
//            [scrollView addSubview:self.viewEmailUsPopUp];
//            [scrollView setShowsVerticalScrollIndicator:NO];
//            scrollView.layer.cornerRadius = 3.0f;
//            [scrollView setContentSize:CGSizeMake(220,610)];
//            [scrollView setScrollEnabled:YES];
//
//            [self resetAllTextFields];
//
//            self.imageViewQueryFor.hidden = NO;
//            self.imageViewQueryRelatedTo.hidden = NO;
//
//            [[SlideNavigationController sharedInstance]toggleLeftMenu];
//            customePopUp = [KLCPopup popupWithContentView:scrollView
//                                                 showType:KLCPopupShowTypeGrowIn
//                                              dismissType:KLCPopupDismissTypeFadeOut
//                                                 maskType:KLCPopupMaskTypeDimmed
//                                 dismissOnBackgroundTouch:YES
//                                    dismissOnContentTouch:NO];
//
//            //[customePopUp show];
//        }*/
//         [self netCoreDataDisplay];
//    }
//}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
//    CGRect originalCellFrame = [self.tblViewMenu rectForRowAtIndexPath:indexPath];
//    
//    cell.frame = CGRectMake(0 - originalCellFrame.size.width,
//                                originalCellFrame.origin.y,
//                                originalCellFrame.size.width,
//                                originalCellFrame.size.height);
//        
//    [UIView animateWithDuration:0.75
//                          delay:0.25
//         usingSpringWithDamping:0.8
//          initialSpringVelocity:2.0
//                        options: UIViewAnimationOptionCurveLinear
//                     animations:^{
//                         
//                         cell.frame = originalCellFrame;
//                     }
//                     completion:^(BOOL finished){
//                             
//                         }];

}


#pragma mark - gesture tapped
- (void)sectionHeaderTapped:(UITapGestureRecognizer *)gestureRecognizer
{
    payloadList =  [NSMutableDictionary dictionary];
    
    NSLog(@"%ld",gestureRecognizer.view.tag);
    [self.txtFldFirstName becomeFirstResponder];
    self.txtFldMailId.text=@"";
    self.txtFldPhoneNo.text=@"";
    self.txtFldFirstName.text=@"";
    self.txtFldLastName.text=@"";
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:gestureRecognizer.view.tag];
    BOOL collapsed  = [[arrayForBool objectAtIndex:indexPath.section] boolValue];
    
    if (gestureRecognizer.view.tag == 0)
    {
        [FIRAnalytics logEventWithName:@"Home" parameters:@{kFIRParameterItemID : @"Home"}];
        
        [[SlideNavigationController sharedInstance] popToRootViewControllerAnimated:NO];
        
        [payloadList setObject:@"yes" forKey:@"s^HOME"];
        
    }else if (gestureRecognizer.view.tag == 7)
    {
        NSUserDefaults *LoginStatus = [NSUserDefaults standardUserDefaults];
        
       if (![[LoginStatus valueForKey:@"isCSSUserLoggedIn"] boolValue])
       {
           
           [self onSelfServiceMenuClick];
           // [AppUtility setLoginController];
            
        }
        else
        {
            
            [AppUtility setTabbarController];
        }
    }
        
    else if (gestureRecognizer.view.tag == 1  || gestureRecognizer.view.tag == 8 || gestureRecognizer.view.tag == 9 ||gestureRecognizer.view.tag == 2)
    {
               
        if (preIndexPath != indexPath)
        {
            [self collapseExpanded:preIndexPath];
        }

        if (indexPath.row == 0)
        {
            preIndexPath=indexPath;
            
            collapsed       = !collapsed;
            [arrayForBool replaceObjectAtIndex:indexPath.section withObject:[NSNumber numberWithBool:collapsed]];
            
            //reload specific section animated
            NSRange range   = NSMakeRange(indexPath.section, 1);
            NSIndexSet *sectionToReload = [NSIndexSet indexSetWithIndexesInRange:range];
            
            if (collapsed)
            {
                [self.tblViewMenu reloadSections:sectionToReload withRowAnimation:UITableViewRowAnimationRight];
            }
            else
            {
                [self.tblViewMenu reloadSections:sectionToReload withRowAnimation:UITableViewRowAnimationLeft];
            }
        }

    }
    else if (gestureRecognizer.view.tag == 5)
    {
       /* [FIRAnalytics logEventWithName:@"Visa" parameters:@{kFIRParameterItemID : @"Visa"}];
        [payloadList setObject:@"yes" forKey:@"s^VISA"];
        
        self.lblPopUpHeader.text = @"VISA";
        self.lblPopUpInfo.text   = @"NEED HELP ?\nOur agent would call you and assist you with your Visa application.";

        [[SlideNavigationController sharedInstance] toggleLeftMenu];
        customePopUp = [KLCPopup popupWithContentView:self.viewVisaPopUp showType:KLCPopupShowTypeGrowIn dismissType:KLCPopupDismissTypeFadeOut maskType:KLCPopupMaskTypeDimmed dismissOnBackgroundTouch:YES dismissOnContentTouch:NO];
        
        [customePopUp show];*/
        
        if ([super connected])
        {
            WebViewController *webViewVC = [[WebViewController alloc]initWithNibName:@"BaseViewController" bundle:nil];
            NSString *forexUrl = kUrlForVisa;
            NSURL *url = [NSURL URLWithString:forexUrl];
            webViewVC.url = url;
            webViewVC.headerString = @"VISA";
            [[SlideNavigationController sharedInstance] pushViewController:webViewVC animated:NO];
        }
        else{
            CustomIOSAlertView *alertView = [[CustomIOSAlertView alloc] init];
            CoreUtility *coreObj=[CoreUtility sharedclassname];
            coreObj.statusWhichPopUpCalled=2;
            // Add some custom content to the alert view
            [alertView setContainerView:[self createDemoView]];
            
            // Modify the parameters
            [alertView setButtonTitles:[NSMutableArray arrayWithObjects:@"Close1", @"Close2", @"Close3", nil]];
            [alertView setDelegate:self];
            
            // You may use a Block, rather than a delegate.
            [alertView setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
                NSLog(@"Block: Button at position %d is clicked on alertView %d.", buttonIndex, (int)[alertView tag]);
                [alertView close];
            }];
            
            [alertView setUseMotionEffects:true];
            
            // And launch the dialog
            [alertView show];
        }

    }
    else if (gestureRecognizer.view.tag == 6)
    {
       /* [FIRAnalytics logEventWithName:@"Insurance" parameters:@{kFIRParameterItemID : @"Insurance"}];
        [payloadList setObject:@"yes" forKey:@"s^INSURANCE"];
        
        self.lblPopUpHeader.text = @"INSURANCE";
        self.lblPopUpInfo.text   = @"NEED HELP ?\nOur agent would call you and help you insure your travel.";
        
         [[SlideNavigationController sharedInstance] toggleLeftMenu];
        customePopUp = [KLCPopup popupWithContentView:self.viewVisaPopUp  showType:KLCPopupShowTypeGrowIn dismissType:KLCPopupDismissTypeFadeOut maskType:KLCPopupMaskTypeDimmed  dismissOnBackgroundTouch:YES dismissOnContentTouch:NO];
        
        [customePopUp show];*/
        
        if ([super connected])
        {
            WebViewController *webViewVC = [[WebViewController alloc]initWithNibName:@"BaseViewController" bundle:nil];
            NSString *forexUrl = kUrlForInsurance;
            NSURL *url = [NSURL URLWithString:forexUrl];
            webViewVC.url = url;
            webViewVC.headerString = @"INSURANCE";
            [[SlideNavigationController sharedInstance] pushViewController:webViewVC animated:NO];
        }
        else{
            CustomIOSAlertView *alertView = [[CustomIOSAlertView alloc] init];
            CoreUtility *coreObj=[CoreUtility sharedclassname];
            coreObj.statusWhichPopUpCalled=2;
            // Add some custom content to the alert view
            [alertView setContainerView:[self createDemoView]];
            
            // Modify the parameters
            [alertView setButtonTitles:[NSMutableArray arrayWithObjects:@"Close1", @"Close2", @"Close3", nil]];
            [alertView setDelegate:self];
            
            // You may use a Block, rather than a delegate.
            [alertView setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
                NSLog(@"Block: Button at position %d is clicked on alertView %d.", buttonIndex, (int)[alertView tag]);
                [alertView close];
            }];
            
            [alertView setUseMotionEffects:true];
            
            // And launch the dialog
            [alertView show];
        }

    }
    else if (gestureRecognizer.view.tag == 3)//flights
    {
     
        [FIRAnalytics logEventWithName:@"Flights" parameters:@{kFIRParameterItemID : @"Flights"}];
        [payloadList setObject:@"yes" forKey:@"s^FLIGHTS"];
        
        if ([super connected]) {
            WebViewController *webViewVC = [[WebViewController alloc]initWithNibName:@"BaseViewController" bundle:nil];
            NSString *forexUrl = kUrlForFlights;
            NSURL *url = [NSURL URLWithString:forexUrl];
            webViewVC.url = url;
            webViewVC.headerString = @"Flight";
            [[SlideNavigationController sharedInstance] pushViewController:webViewVC animated:NO];
            
        }
        else
        {
            CustomIOSAlertView *alertView = [[CustomIOSAlertView alloc] init];
            CoreUtility *coreObj=[CoreUtility sharedclassname];
            coreObj.statusWhichPopUpCalled=1;
            // Add some custom content to the alert view
            [alertView setContainerView:[self createDemoView]];
            
            // Modify the parameters
            [alertView setButtonTitles:[NSMutableArray arrayWithObjects:@"Close1", @"Close2", @"Close3", nil]];
            [alertView setDelegate:self];
            
            // You may use a Block, rather than a delegate.
            [alertView setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
                NSLog(@"Block: Button at position %d is clicked on alertView %d.", buttonIndex, (int)[alertView tag]);
                [alertView close];
            }];
            
            [alertView setUseMotionEffects:true];
            
            // And launch the dialog
            [alertView show];
            
            

            
        }
       
    }
    else if (gestureRecognizer.view.tag == 4)//hotels
    {
        [FIRAnalytics logEventWithName:@"Hotels" parameters:@{kFIRParameterItemID : @"Hotels"}];
        [payloadList setObject:@"yes" forKey:@"s^HOTELS"];
        
        if ([super connected]) {
        WebViewController *webViewVC = [[WebViewController alloc]initWithNibName:@"BaseViewController" bundle:nil];
        NSString *forexUrl = kUrlForHotels;
        NSURL *url = [NSURL URLWithString:forexUrl];
        webViewVC.url = url;
        webViewVC.headerString = @"Hotels";
        [[SlideNavigationController sharedInstance] pushViewController:webViewVC animated:NO];
        }
        else{
            CustomIOSAlertView *alertView = [[CustomIOSAlertView alloc] init];
            CoreUtility *coreObj=[CoreUtility sharedclassname];
            coreObj.statusWhichPopUpCalled=2;
            // Add some custom content to the alert view
            [alertView setContainerView:[self createDemoView]];
            
            // Modify the parameters
            [alertView setButtonTitles:[NSMutableArray arrayWithObjects:@"Close1", @"Close2", @"Close3", nil]];
            [alertView setDelegate:self];
            
            // You may use a Block, rather than a delegate.
            [alertView setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
                NSLog(@"Block: Button at position %d is clicked on alertView %d.", buttonIndex, (int)[alertView tag]);
                [alertView close];
            }];
            
            [alertView setUseMotionEffects:true];
            
            // And launch the dialog
            [alertView show];
        }
    }
    else if (gestureRecognizer.view.tag == 2)//forex
    {
        [FIRAnalytics logEventWithName:@"Forex" parameters:@{kFIRParameterItemID : @"Forex"}];
         [payloadList setObject:@"yes" forKey:@"s^FOREX"];
        
         if ([super connected]) {
        WebViewController *webViewVC = [[WebViewController alloc]initWithNibName:@"BaseViewController" bundle:nil];
        NSString *forexUrl = kUrlForForex;
        NSURL *url = [NSURL URLWithString:forexUrl];
        webViewVC.url = url;
        webViewVC.headerString = @"Forex";
        [[SlideNavigationController sharedInstance] pushViewController:webViewVC animated:NO];
         }else{
             CustomIOSAlertView *alertView = [[CustomIOSAlertView alloc] init];
             CoreUtility *coreObj=[CoreUtility sharedclassname];
             coreObj.statusWhichPopUpCalled=3;
             // Add some custom content to the alert view
             [alertView setContainerView:[self createDemoView]];
             
             // Modify the parameters
             [alertView setButtonTitles:[NSMutableArray arrayWithObjects:@"Close1", @"Close2", @"Close3", nil]];
             [alertView setDelegate:self];
             
             // You may use a Block, rather than a delegate.
             [alertView setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
                 NSLog(@"Block: Button at position %d is clicked on alertView %d.", buttonIndex, (int)[alertView tag]);
                 [alertView close];
             }];
             
             [alertView setUseMotionEffects:true];
             
             // And launch the dialog
             [alertView show];

         }
        
    }
   /* if(gestureRecognizer.view.tag==7)
    {
        
//        [FIRAnalytics logEventWithName:@"Favourits" parameters:@{kFIRParameterItemID :@"Favourits"}];
//        
//        
//        MyFavouriteVC *favouriteVC=[[MyFavouriteVC alloc] initWithNibName:@"BaseViewController" bundle:nil];
//        favouriteVC.headerName=@"My Favourite";
//        [payloadList setObject:@"yes" forKey:@"s^MY_FAV"];
//        [[SlideNavigationController sharedInstance]pushViewController:favouriteVC animated:YES];
        
    }*/

    if (gestureRecognizer.view.tag != 1  && gestureRecognizer.view.tag != 8 && gestureRecognizer.view.tag != 9)
    {
        
    [self netCoreDataDisplay];
        
    }
    
    
}

-(void)collapseExpanded:(NSIndexPath*)index{
    
    [arrayForBool replaceObjectAtIndex:index.section withObject:[NSNumber numberWithBool:NO]];
    NSRange range   = NSMakeRange(index.section, 1);
    NSIndexSet *sectionToReload = [NSIndexSet indexSetWithIndexesInRange:range];
    [self.tblViewMenu reloadSections:sectionToReload withRowAnimation:UITableViewRowAnimationLeft];
    
}
-(void)onDealsAndOffersClicked
{
    WebViewController *webViewVC = [[WebViewController alloc]initWithNibName:@"BaseViewController" bundle:nil];
    NSString *forexUrl = kpgURLForDeals;
    NSURL *url = [NSURL URLWithString:forexUrl];
    webViewVC.url = url;
    webViewVC.headerString = @"Deals & Offers";
    
    [payloadList setObject:@"yes" forKey:@"s^DEALS_OFFERS"];
    [[SlideNavigationController sharedInstance] pushViewController:webViewVC animated:NO];
    
    [self netCoreDataDisplay];
}

#pragma mark - UIButton Action Methods

- (IBAction)btnSubmitOnVisaInsuranceClicked:(id)sender {
    
//    NSMutableString *strRequest = [[NSMutableString alloc]initWithFormat:@"https://thomascookindia.custhelp.com/cgi-bin/thomascookindia.cfg/php/custom/dom_int_visa_opp.php?"];
//    
//    [strRequest appendString:[NSString stringWithFormat:@"first_name=%@&last_name=%@&email=%@&mobile=%@",self.txtFldFirstName.text,self.txtFldLastName.text,self.txtFldMailId.text,self.txtFldPhoneNo.text]];
//    
//    NSLog(@"%@",strRequest);
    
    NSRange whiteSpaceRangeInLastName = [self.txtFldLastName.text rangeOfCharacterFromSet:[NSCharacterSet whitespaceCharacterSet]];
    
    NSString *email=self.txtFldMailId.text;
    
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    if ([self.txtFldFirstName.text isEqualToString:@""])
    {
        [super showAlertViewWithTitle:@"Alert" withMessage:@"Please Enter First Name"];
    }
    else if ([self.txtFldLastName.text isEqualToString:@""])
    {
       
        [super showAlertViewWithTitle:@"Alert" withMessage:@"Please Enter Last Name"];
    }
    else if (whiteSpaceRangeInLastName.location != NSNotFound)
    {
        
        [super showAlertViewWithTitle:@"Alert" withMessage:@"last name should not contain space"];
        
    }

    else if ([self.txtFldPhoneNo.text isEqualToString:@""])
    {
        [super showAlertViewWithTitle:@"Alert" withMessage:@"Please Enter Phone Number"];
    }
    else if ([self.txtFldPhoneNo.text length]!=10)
    {
        [super showAlertViewWithTitle:@"Alert" withMessage:@"Please Enter 10 digit Number"];
    }
    else if( ![emailTest evaluateWithObject:email]==YES)
    {
        [super showAlertViewWithTitle:@"Alert" withMessage:@"Please Enter valid Email ID"];
        
    }
    else if ([self.txtFldMailId.text isEqualToString:@""])
    {
            [super showAlertViewWithTitle:@"Alert" withMessage:@"Please Enter Email ID"];
        
    }else
    {
        /*
         "type":"lead",
         "entity":"insurance",
         "action":"submit",
         "platform":"android",
         "data":{
         "mobile":"9766356347",
         "firstName":"sunny",
         "lastName":"vijay",
         "email":"sunny.vijay@mobicule.com"
         }
         */
        
        NSString *insuranceString = [self.lblPopUpHeader.text lowercaseString];
        NSMutableDictionary *dictOfData = [[NSMutableDictionary alloc]init];
        [dictOfData setValue:self.txtFldPhoneNo.text forKey:@"mobile"];
        [dictOfData setValue:_txtFldFirstName.text forKey:@"firstName"];
        [dictOfData setValue:self.txtFldLastName.text forKey:@"lastName"];
        [dictOfData setValue:self.txtFldMailId.text forKey:@"email"];
        
        
        if ([super connected])
        {
            
          /*  activityLoadingView = [LoadingView loadingViewInView:customePopUp
                                                      withString:@""
                                               andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                NetworkHelper *helper = [NetworkHelper sharedHelper];
                NSString *strResponse = [helper getDataFromServerForType:@"lead" entity:insuranceString action:@"submit" andUserJson:dictOfData];
                NSString *strStatus = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"status"];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    if ([strStatus isEqualToString:kStatusSuccess])
                    {
                        // NSString *responseString = [dataDict valueForKey:@"responseString"];
                        [customePopUp dismiss:YES];
                        [super showAlertViewWithTitle:@"Alert" withMessage:@"Thank you. We have received your details and will get back to you shortly."];
                    }
                    else
                    {
                        NSString *messsage = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"message"];
                        if (messsage)
                        {
                            if ([messsage isEqualToString:kVersionUpgradeMessage])
                            {
                                NSDictionary *downloadDict = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"data"];
                                NSString *downloadURL = [downloadDict valueForKey:@"download_url"];
                                [super showAlertViewForVersionUpdateWithUrl:downloadURL];
                                
                            }else
                            {
                                [super showAlertViewWithTitle:@"Alert" withMessage:messsage];
                            }
                        }else
                        {
                            [super showAlertViewWithTitle:@"Alert" withMessage:kMessageSomeErrorTryAfterSometime];
                        }
                        
                    }
                    
                    [activityLoadingView removeView];
                    activityLoadingView = nil;
                    
                    
                });
            });*/
            
            
            activityLoadingView = [LoadingView loadingViewInView:[UIApplication sharedApplication].keyWindow
                                                      withString:@""
                                               andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
            
            @try
            {
                
                //Visa
              //  https://thomascookindia.custhelp.com/cgi-bin/thomascookindia.cfg/php/custom/dom_int_insurance_opp_android_app_lead.php?first_name=nar&last_name=kumar&email=nar@yahoo.com&mobile=7878787878
                
                
                //insurance
                //https://thomascookindia.custhelp.com/cgi-bin/thomascookindia.cfg/php/custom/dom_int_visa_opp_android_app_lead.php?first_name=nar&last_name=kumar&email=nar@yahoo.com&mobile=7878787878
                
                NSString *urlString;
                
                if([self.lblPopUpHeader.text isEqualToString:@"VISA"])
                {
                    urlString  = @"https://thomascookindia.custhelp.com/cgi-bin/thomascookindia.cfg/php/custom/dom_int_visa_opp_ios_app_lead.php";
                }
                else
                {
                    urlString  = @"https://thomascookindia.custhelp.com/cgi-bin/thomascookindia.cfg/php/custom/dom_int_insurance_opp_ios_app_lead.php";
                }
                
                
                NSString *queryParam = [NSString stringWithFormat:@"?first_name=%@&last_name=%@&email=%@&mobile=%@",_txtFldFirstName.text,_txtFldLastName.text,self.txtFldMailId.text,self.txtFldPhoneNo.text];
                
                
                NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
                [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",urlString,queryParam]]];
                [request setHTTPMethod:@"GET"];
                
                NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
                [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                {
                    dispatch_async(dispatch_get_main_queue(), ^{

                    [activityLoadingView removeFromSuperview];
                    
                    NSString *requestReply = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
                    
                    NSArray *arrayForRespose = [requestReply componentsSeparatedByString:@"|"];
                    
                    if (arrayForRespose.count >= 2)
                    {
                        NSString *messegeCode  = [arrayForRespose objectAtIndex:0];
                        NSString *messege = [arrayForRespose objectAtIndex:1];
                        
                        [customePopUp dismiss:YES];
                        [super showAlertViewWithTitle:@"Alert" withMessage:messege];
                        
                        //28-02-2018
                        if ([[insuranceString lowercaseString] isEqualToString:@"visa"])
                        {
                            [self netCoreVisa];
                        }
                        else
                        {
                            [self netCoreInsurance];
                        }


                    }
                    else
                    {
                         [super showAlertViewWithTitle:@"Alert" withMessage:@"Some Error Occured"];
                    }
                    
                    
                    NSLog(@"Request reply: %@", requestReply);
                    });
                    
                }] resume];

                
                
            }
            @catch (NSException *exception)
            {
                NSLog(@"%@", exception.reason);
                [activityLoadingView removeFromSuperview];
            }
            @finally
            {
                NSLog(@"exception finally called");
            }
            

            
        }
        else
        {
            [super showAlertViewWithTitle:@"Alert" withMessage:kMessageNoInternet];
        }
            
    }
    
    
}

- (IBAction)btnSubmitOnEmilPopUpClicked:(id)sender
{
    
}

- (IBAction)btnOnQueryForPopUpClicked:(id)sender {
    
    self.imageViewQueryFor.hidden = YES;
    
    UIButton *currentButton = (UIButton *)sender;
    
    self.txtFldEmailPopUpQueryFor.text = currentButton.currentTitle;
    
    [customePopUp dismiss:YES];
}

- (IBAction)btnOnQueryRelatedPopUpClicked:(id)sender {
    
    self.imageViewQueryRelatedTo.hidden = YES;
    
    UIButton *currentButton = (UIButton *)sender;
    
    self.txtFldEmailPopUpQryRltdTo.text = currentButton.currentTitle;
    
    [customePopUp dismiss:YES];
}

- (IBAction)onSignInButtonClicked:(id)sender
{
    if ([_btnSignInOut.titleLabel.text isEqualToString:@"SignOut"])
    {
        [FIRAnalytics logEventWithName:@"Sign_Out" parameters:@{kFIRParameterItemID:@"Sign Out"}];
        _lblWelcomeText.text=@"You are not signed in";
        _lblUserId.text=@"Sign in to get discount and offers";
         [_btnSignInOut setTitle:@"SignIn" forState:UIControlStateNormal];
        
        [[NetCoreInstallation sharedInstance] netCorePushLogout:^(NSInteger statusCode) {
            
            NSLog(@"User Logged out from netcore!!");
        }];
       
        NSUserDefaults *LoginStatus=[NSUserDefaults standardUserDefaults];
        [LoginStatus setObject:kLoginFailed forKey:kLoginStatus];
        
      CoreUtility *coreObj=[CoreUtility sharedclassname];
        
        if (coreObj != nil && ![coreObj isEqual:[NSNull new]] && coreObj.strPhoneNumber != nil && ![coreObj.strPhoneNumber isEqual:[NSNull new]] && coreObj.strPhoneNumber.count > 0)
        {
            [coreObj.strPhoneNumber removeAllObjects];
            
        }
        
        
        
        [LoginStatus setBool:false forKey:@"isRegisterForPushNotification"];
        
        [LoginStatus setBool:false forKey:@"isCSSUserLoggedIn"];
        
        [LoginStatus setObject:nil forKey:@"mobicleNumber"];
        
        [LoginStatus setObject:@"" forKey:@"userDefaultTC-LoginMobileNumber"];
        
        [LoginStatus setObject:@"" forKey:@"Holidays_TextField_MobicleNumber"];
        
        //NSUserDefaults *userIdForGooglePlusSignIn=[NSUserDefaults standardUserDefaults];
        [LoginStatus removeObjectForKey:kLoginEmailId];
        
        [LoginStatus removeObjectForKey:kPhoneNo];
        
       [[NSUserDefaults standardUserDefaults] synchronize];
        
        [FIRAnalytics logEventWithName:@"Home" parameters:@{kFIRParameterItemID : @"Home"}];
        
        [[SlideNavigationController sharedInstance] popToRootViewControllerAnimated:NO];
        
        [payloadList setObject:@"yes" forKey:@"s^HOME"];
        
        
    }else{
  /* LoginViewPopUp *loginView = [[LoginViewPopUp alloc] initWithNibName:@"LoginViewPopUp" bundle:nil];
       // LoginViewPopUp.sharedInstance.
    [LoginViewPopUp sharedInstance].object=loginView;
    loginView.view.frame = CGRectMake(0, 0, 300, self.slideMenuView.frame.size.height - 100);
    
    [[SlideNavigationController sharedInstance] toggleLeftMenu];
     [self presentPopupViewController:loginView animationType:MJPopupViewAnimationFade];*/
        
       
        
        NewLoginPopUpViewController *loginView = [[NewLoginPopUpViewController alloc] initWithNibName:@"NewLoginPopUpViewController" bundle:nil];
       
       // [LoginViewPopUp sharedInstance].object=loginView;
       // loginView.view.frame = CGRectMake(0, 0, 300, self.slideMenuView.frame.size.height - 100);
        
        [[SlideNavigationController sharedInstance] toggleLeftMenu];
       //  [[SlideNavigationController sharedInstance]pushViewController:loginView animated:YES];
        
         UINavigationController * loginNavigation = [[UINavigationController alloc] initWithRootViewController:loginView];
        
        [[SlideNavigationController sharedInstance]presentViewController:loginNavigation animated:true completion:^{
            
        }];
        
       // [self presentPopupViewController:loginView animationType:MJPopupViewAnimationFade];
   
    }
    
}

#pragma mark - Pop VIew For Call Us
- (void)customIOS7dialogButtonTouchUpInside: (CustomIOSAlertView *)alertView clickedButtonAtIndex: (NSInteger)buttonIndex
{
    NSLog(@"Delegate: Button at position %d is clicked on alertView %d.", (int)buttonIndex, (int)[alertView tag]);
    [alertView close];
}

- (UIView *)createDemoView
{
    UIView *demoView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 290, 200)];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 30, 30)];
    [imageView setImage:[UIImage imageNamed:@"customer_support.png"]];
    UILabel *lblCallus=[[UILabel alloc]initWithFrame:CGRectMake(50, 10, 200, 40)];
    //Old Contact Number
    //lblCallus.text=@"1800 2099 100";
    
    //New Contact Number
    lblCallus.text=@"8652908370"; //@"18002000464,7039003560"; //new mobile number 4th Nov 2019
   
    [demoView addSubview:lblCallus];
    [demoView addSubview:imageView];
    
    
    return demoView;
}




#pragma mark - set Login Status
-(void)setLoginStatusValue{
  
    NSUserDefaults *LoginStatus=[NSUserDefaults standardUserDefaults];
    NSString *statusForLogin= [LoginStatus valueForKey:kLoginStatus];
    
    NSUserDefaults *userIdForGooglePlusSignIn=[NSUserDefaults standardUserDefaults];
    NSString *userId=[userIdForGooglePlusSignIn valueForKey:kLoginEmailId];
    
    if ([statusForLogin isEqualToString:kLoginSuccess])
    {
        _lblWelcomeText.text=@"Welcome..!";
        _lblUserId.text=userId;
        [_btnSignInOut setTitle:@"SignOut" forState:UIControlStateNormal];
        
    }

    
}
#pragma mark - Popup view for Net disconnected
-(UIView *)popUpViewForInternetDiscoonected{
    UIView *popUpView = [UIView new];
    popUpView.frame = CGRectMake(0, 0, 200,200 );
    popUpView.backgroundColor = [UIColor whiteColor];
    
    UIImageView *imagelogo=[[UIImageView alloc] initWithFrame:CGRectMake(55,50,80,60)];
    imagelogo.image=[UIImage imageNamed:@"login_logo"];
    UILabel *lblInternetHeader=[[UILabel alloc]initWithFrame:CGRectMake(15, 15, 180, 40)];
    lblInternetHeader.font=[UIFont fontWithName:@"Titillium-bold" size:17];
    lblInternetHeader.text=@"Internet not available";
    
    UILabel *lblInternetMsg=[[UILabel alloc]initWithFrame:CGRectMake(10, 105, 190, 50)];
    lblInternetMsg.text=@"Please turn on your internet";
    lblInternetMsg.font=[UIFont fontWithName:@"Titillium-semibold" size:14];
    lblInternetMsg.textColor=[UIColor grayColor];
    
    UIButton *btnInterNetOk=[[UIButton alloc]initWithFrame:CGRectMake(70, 150, 50, 40)];
    [btnInterNetOk addTarget:self action: @selector(actionForNetok) forControlEvents:UIControlEventTouchUpInside];
    [btnInterNetOk setBackgroundColor:[UIColor orangeColor]];
    [btnInterNetOk setTitle:@"OK" forState:UIControlStateNormal];
    
    
    popUpView.layer.cornerRadius = 1.0;
    [popUpView addSubview:lblInternetHeader];
    [popUpView addSubview:imagelogo];
    [popUpView addSubview:lblInternetMsg];
    [popUpView addSubview:btnInterNetOk];
    
    return popUpView;
}
-(void)actionForNetok{
    [customePopUp dismiss:YES];
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([string isEqualToString:@""])
    {
        return YES;
    }
    
    if (textField == self.txtFldPhoneNo)
    {
        if([self.txtFldPhoneNo.text length]>=10)
        {
            return NO;
        }
        else
        {
            return YES;
        }
        
    }
    return YES;
    
}

#pragma mark netCore logEvent

//APP_DRAWER

-(void)netCoreDataDisplay
{
    if (![payloadList objectForKey:@"s^DEALS_OFFERS"])
    {
        [payloadList setObject:@"no" forKey:@"s^DEALS_OFFERS"];
    }
    if (![payloadList objectForKey:@"s^HOME"])
    {
        [payloadList setObject:@"no" forKey:@"s^HOME"];
    }
    if (![payloadList objectForKey:@"s^HOLIDAYS"])
    {
        [payloadList setObject:@"no" forKey:@"s^HOLIDAYS"];
    }
    if (![payloadList objectForKey:@"s^FOREX"])
    {
        [payloadList setObject:@"no" forKey:@"s^FOREX"];
    }
    if (![payloadList objectForKey:@"s^FLIGHTS"])
    {
        [payloadList setObject:@"no" forKey:@"s^FLIGHTS"];
    }
    if (![payloadList objectForKey:@"s^HOTELS"])
    {
        [payloadList setObject:@"no" forKey:@"s^HOTELS"];
    }
    if (![payloadList objectForKey:@"s^VISA"])
    {
        [payloadList setObject:@"no" forKey:@"s^VISA"];
    }
    if (![payloadList objectForKey:@"s^INSURANCE"])
    {
        [payloadList setObject:@"no" forKey:@"s^INSURANCE"];
    }
    if (![payloadList objectForKey:@"s^MY_FAV"])
    {
       // [payloadList setObject:@"no" forKey:@"s^MY_FAV"];
         [payloadList setObject:[NSNull new] forKey:@"s^MY_FAV"]; // 09-04-2018
    }
    if (![payloadList objectForKey:@"s^MY_ACC"])
    {
       // [payloadList setObject:@"no" forKey:@"s^MY_ACC"];
         [payloadList setObject:[NSNull new] forKey:@"s^MY_ACC"]; // 09-04-2018
    }
    if (![payloadList objectForKey:@"s^CUST_SUPPORT"])
    {
        [payloadList setObject:@"no" forKey:@"s^CUST_SUPPORT"];
    }
    if (![payloadList objectForKey:@"s^PROFILE"])
    {
       // [payloadList setObject:@"no" forKey:@"s^PROFILE"];
         [payloadList setObject:[NSNull new] forKey:@"s^PROFILE"];
    }
   /* if (![payloadList objectForKey:@"s^My_BOOKINGS"]) //28-02-2018
    {
        [payloadList setObject:@"no" forKey:@"s^My_BOOKINGS"];
    }*/
    if (![payloadList objectForKey:@"s^My_BOOKING"])//28-02-2018
    {
       // [payloadList setObject:@"no" forKey:@"s^My_BOOKING"];
          [payloadList setObject:[NSNull new] forKey:@"s^My_BOOKING"]; // 09-04-2018
    }
    if (![payloadList objectForKey:@"s^SOURCE"])        //further new parameters added : 28-02-2018
    {
        [payloadList setObject:@"App" forKey:@"s^SOURCE"];
    }
    if (![payloadList objectForKey:@"s^CALL_US"])
    {
        [payloadList setObject:@"no" forKey:@"s^CALL_US"];
    }
    if (![payloadList objectForKey:@"s^VISIT_US"])
    {
        [payloadList setObject:@"no" forKey:@"s^VISIT_US"];
    }
    
    if (![payloadList objectForKey:@"s^Self_Service"]) // 09-04-2018
    {
        [payloadList setObject:@"no" forKey:@"s^Self_Service"];
       
    }
    
    
    //[NetCoreAnalyticsVC getNetCorePushNotificationDetailDictionary:payloadList withPayloadCount:110];//28-02-2018
    
    [NetCoreAnalyticsVC getNetCorePushNotificationDetailDictionary:payloadList withPayloadCount:119]; //28-02-2018
    
}

//APP_FORM_VISA
-(void)netCoreVisa
{
    NSMutableDictionary *payloadListVisa = [[NSMutableDictionary alloc] init];
    
    [payloadListVisa setObject:_txtFldFirstName.text forKey:@"s^FIRST_NAME"];
    [payloadListVisa setObject:_txtFldLastName.text forKey:@"s^LAST_NAME"];
    [payloadListVisa setObject:_txtFldMailId.text forKey:@"s^EMAIL"];
    [payloadListVisa setObject:@"App" forKey:@"s^SOURCE"];
    
    [NetCoreAnalyticsVC getNetCorePushNotificationDetailDictionary:payloadListVisa withPayloadCount:120];
    [NetCoreAnalyticsVC getNetCorePushNotificationDetailDictionary:payloadListVisa withPayloadCount:102];
  
    [[NetCoreInstallation sharedInstance] netCoreProfilePush:_txtFldPhoneNo.text Payload:payloadListVisa Block:nil];
    
    
}

//APP_INSURANCE
-(void)netCoreInsurance
{
    NSMutableDictionary *payloadListInsurance = [[NSMutableDictionary alloc] init];
    [payloadListInsurance setObject:_txtFldFirstName.text forKey:@"s^FIRST_NAME"];
    [payloadListInsurance setObject:_txtFldLastName.text forKey:@"s^LAST_NAME"];
    [payloadListInsurance setObject:_txtFldMailId.text forKey:@"s^EMAIL"];
    [payloadListInsurance setObject:@"App" forKey:@"s^SOURCE"];
    [NetCoreAnalyticsVC getNetCorePushNotificationDetailDictionary:payloadListInsurance withPayloadCount:121];
    [NetCoreAnalyticsVC getNetCorePushNotificationDetailDictionary:payloadListInsurance withPayloadCount:102];
    
    [[NetCoreInstallation sharedInstance] netCoreProfilePush:_txtFldPhoneNo.text Payload:payloadListInsurance Block:nil];
    
}


@end
