//
//  DefaultHeaderRequestBuilder.h
//  holidays
//
//  Created by Vaibhav Indalkar on 17/09/15.
//  Copyright (c) 2015 Mobicule. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DefaultHeaderRequestBuilder : NSObject
{

    NSString *Type,*Entity,*Action;

    NSString *RequestJson;
    NSDictionary *dictUserJson;

    NSMutableDictionary *jsonDictionary;

}


-(id)initWithType:(NSString *)type withEntity:(NSString *)entity withAction:(NSString *)action withUserJson:(NSDictionary *)userJsonDict;
-(void)addRequestHeader;
-(void)addAuthorizationDetails;
-(void)addRequestData;
-(void)putWithKey:(NSString *)key withValue:(NSObject *)object;
-(void)setRequestData:(NSString *)jsonValue;
-(NSString *)build;
@end
