//
//  LandingPageViewController.h
//  holidayApp
//
//  Created by vaibhav on 06/08/15.
//  Copyright (c) 2015 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "PSSlideShow.h"
#import "TabMenuVC.h"

typedef void(^isCompleted)(BOOL);

@interface LandingPageViewController : BaseViewController<PSSlideShowDelegate>
{
    LoadingView  *activityLoadingView;
    LoadingView  *activityLoadingView1;
    LoadingView  *activityLoadingViewRT;
    LoadingView *imageLoadingView;
}
@property (strong, nonatomic) IBOutlet UIView *viewForLandingPage;

@property (weak, nonatomic) IBOutlet UIButton *btnInternationalHoliday;

@property (weak, nonatomic) IBOutlet UIButton *btnIndianHoliday;
@property (weak, nonatomic) IBOutlet UIButton *btnFlight;
@property (weak, nonatomic) IBOutlet UIButton *btnForex;
@property (strong, nonatomic) IBOutlet UILabel *notificationCountLabel;

- (IBAction)clickedInternationalHolidays:(id)sender;
- (IBAction)clickedIndiaHolidays:(id)sender;
- (IBAction)clickedFlightsSearch:(id)sender;
- (IBAction)clickedForex:(id)sender;
- (IBAction)btnSearchClicked:(id)sender;
- (IBAction)onNotificationBtnClick:(id)sender;
- (IBAction)onOffersDealsButtonClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *notificationButton;
@property (strong ,nonatomic) NSDictionary *notificationDict;
 

@end
