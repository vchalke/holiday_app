//
//  FilterSCrView.m
//  holidays
//
//  Created by Kush_Team on 24/06/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "FilterSCrView.h"
#import "DeptCityTableViewCell.h"
#import "PrizePerPersonTableViewCell.h"
#import "MonthTravelTableViewCell.h"
#import "FilterDurationTableViewCell.h"
#import "FilterPackageTableViewCell.h"
#import "FilterThemeTableViewCell.h"
#import "Holiday.h"
#import "TabMenuVC.h"
#import "CoreUtility.h"
#import "PackageListVC.h"
#import "NetCoreAnalyticsVC.h"
#import "WebUrlConstants.h"


@interface FilterSCrView ()
{
    NSMutableArray *arrayDepartureList;
    NSArray *arrayMonthOfTravel;
    NSArray *arrayDuration;
    NSMutableArray *arrayFlightFrom;
    NSMutableArray *arrayForResetData;
    NSMutableArray *departureCity;
    int minDuration,maxDuration,monthIndex;
    NSMutableArray *arrayFlightFromList;
    NSMutableDictionary *payloadList;
    
}
@property (strong,nonatomic)NSMutableArray *arrayOfHolidayData;
//@property (strong,nonatomic)NSDictionary *dictOfFilters;
@end

@implementation FilterSCrView

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    self.dictOfFilters = [[NSDictionary alloc]initWithDictionary:_filterDict];
//    [self configureLabelSlider];
//     [self updateSliderLabels];
     arrayForResetData=[[NSMutableArray alloc]init];
     arrayForResetData=[self.arrayOfHolidayData mutableCopy];
     payloadList =  [NSMutableDictionary dictionary];
    
    self.mainNewFilterDict =  [[NSMutableDictionary alloc]init];
    [self.tableView_Filter registerNib:[UINib nibWithNibName:@"DeptCityTableViewCell" bundle:nil] forCellReuseIdentifier:@"DeptCityTableViewCell"];
    [self.tableView_Filter registerNib:[UINib nibWithNibName:@"PrizePerPersonTableViewCell" bundle:nil] forCellReuseIdentifier:@"PrizePerPersonTableViewCell"];
    [self.tableView_Filter registerNib:[UINib nibWithNibName:@"MonthTravelTableViewCell" bundle:nil] forCellReuseIdentifier:@"MonthTravelTableViewCell"];
    [self.tableView_Filter registerNib:[UINib nibWithNibName:@"FilterDurationTableViewCell" bundle:nil] forCellReuseIdentifier:@"FilterDurationTableViewCell"];
    [self.tableView_Filter registerNib:[UINib nibWithNibName:@"FilterPackageTableViewCell" bundle:nil] forCellReuseIdentifier:@"FilterPackageTableViewCell"];
    [self.tableView_Filter registerNib:[UINib nibWithNibName:@"FilterThemeTableViewCell" bundle:nil] forCellReuseIdentifier:@"FilterThemeTableViewCell"];
    
    NSDictionary *filetrDict = [[NSUserDefaults standardUserDefaults] objectForKey:filterOptionsDict];
    if (filetrDict != nil){
        _mainNewFilterDict = [filetrDict mutableCopy];
    }
    
    
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:NO];
    //  CoreUtility *coreObj=[CoreUtility sharedclassname];
    //    PackageListVC *filterObj=coreObj.obj;
    //    filterObj.title=@"PACKAGE LISTING";
    
    [[SlideNavigationController sharedInstance] setEnableSwipeGesture:NO];
    
    self.arrayOfHolidayData = [[NSMutableArray alloc]initWithArray:_holidayArrayInNewFilter];
    
    departureCity = [[NSMutableArray alloc]init];
    
//    [departureCity insertObject:@"SELECT" atIndex:0];
    
    
    //[departureCity addObjectsFromArray: [self.filterDict valueForKey:@"departureFromList"]];
    
    arrayDepartureList = [[NSMutableArray alloc]init];
    
    [departureCity addObjectsFromArray:[self getDepartureCityFromArray]];
    
    [arrayDepartureList addObjectsFromArray:departureCity];
    
        
    arrayDuration = [[NSArray alloc] initWithObjects:@"less than 3",@"From 3 to 7",@"more than 7", nil];
    
    
    arrayMonthOfTravel = [self nextYearMonths];
    
    arrayFlightFrom = [[NSMutableArray alloc]init];
    [arrayFlightFrom insertObject:@"SELECT" atIndex:0];
   /// [arrayFlightFrom addObjectsFromArray:[self.filterDict valueForKey:@"departureFromList"]];
    [arrayFlightFrom addObjectsFromArray:[self getDepartureCityFromArray]];
    
}
- (void)viewDidAppear:(BOOL)animated{
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 6;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row == 0){
        DeptCityTableViewCell* deptCell = (DeptCityTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"DeptCityTableViewCell"];
//        deptCell.arrayOfHolidayData = self.arrayOfHolidayData;
        deptCell.deptCityFilterDict = self.mainNewFilterDict;
        deptCell.arrayDepartureList = arrayDepartureList;
        [deptCell setupCityTableView];
        return deptCell;
    }else if (indexPath.row == 1){
        PrizePerPersonTableViewCell* prizeCell = (PrizePerPersonTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"PrizePerPersonTableViewCell"];
        prizeCell.filterPrizeDict = self.mainNewFilterDict;
        prizeCell.holidayArrayFilter = self.holidayArrayInNewFilter;
        [prizeCell setupTableViewCell];
        return prizeCell;
    }else if (indexPath.row == 2){
        MonthTravelTableViewCell* travelCell = (MonthTravelTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"MonthTravelTableViewCell"];
         travelCell.filterTravelMonthDict = self.mainNewFilterDict;
        [travelCell setupTableViewCell];
        return travelCell;
    }else if (indexPath.row == 3){
        FilterDurationTableViewCell* filterCell = (FilterDurationTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"FilterDurationTableViewCell"];
        filterCell.filterDurationDict = self.mainNewFilterDict;
        [filterCell setupTableViewCell];
        return filterCell;
    }else if (indexPath.row == 4){
        FilterPackageTableViewCell* packCell = (FilterPackageTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"FilterPackageTableViewCell"];
        packCell.filterPackageDict = self.mainNewFilterDict;
        [packCell setupTableViewCell];
        return packCell;
    }
    FilterThemeTableViewCell* themeCell = (FilterThemeTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"FilterThemeTableViewCell"];
     themeCell.themeFilterDict = self.mainNewFilterDict;
    [themeCell setupTableViewCell];
    return themeCell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0){
        return  100;
    }else if (indexPath.row == 1){
        return  150;
    }else if (indexPath.row == 2){
        return  250;
    }else if (indexPath.row == 3){
        return  180;
    }else if (indexPath.row == 4){
        return  170;
    }
    return  220;
}

-(void)saveAllValeusInFilterUserDefaults{
    /*
    NSMutableDictionary *editFilterDict = [[NSMutableDictionary alloc]init];
    [editFilterDict setObject:@"Mumbai" forKey:filterDepartureCity];
    [editFilterDict setObject:@"100" forKey:filterMinBudget];
    [editFilterDict setObject:@"100000" forKey:filterMaxBudget];
    [editFilterDict setObject:@""  forKey:filterTravelMonth];
    [editFilterDict setObject:@""  forKey:filterDuration];
    [editFilterDict setObject:@"NO" forKey:filterByGroup];
    [editFilterDict setObject:@"NO" forKey:filterByStay];
    [editFilterDict setObject:@"NO" forKey:filterByCustomized];
    [editFilterDict setObject:@"NO" forKey:filterByAirInclusive];
    [editFilterDict setObject:@""  forKey:filterTheme];
    [[NSUserDefaults standardUserDefaults]setObject:editFilterDict forKey:filterOptionsDict];
    */
    NSLog(@"SELECTED MAIN_NEW_FILTERDICT %@",self.mainNewFilterDict);
    [[NSUserDefaults standardUserDefaults] setObject:self.mainNewFilterDict forKey:filterOptionsDict];
}
-(NSArray *)getDepartureCityFromArray
{
    NSMutableSet *departureCityList = [[NSMutableSet alloc] init];
    
    for (int index = 0; index < self.arrayOfHolidayData.count; index ++)
    {
        Holiday *holidayObject = self.arrayOfHolidayData[index];
        
        NSArray *hubArray = holidayObject.arrayOfHubList;
        
        if (hubArray)
        {
            if (hubArray.count != 0)
            {
                [departureCityList addObjectsFromArray:hubArray];

            }
        }
    }
    
    NSArray *finalDestinationList = [departureCityList allObjects];
    
    return finalDestinationList;
}

-(NSArray *)nextYearMonths
{
    NSDateFormatter  *dateFormatter   = [[NSDateFormatter alloc] init];
    NSDate           *today           = [NSDate date];
    NSCalendar       *currentCalendar = [NSCalendar currentCalendar];
    
    NSDateComponents *monthComponents = [currentCalendar components:NSCalendarUnitMonth fromDate:today];
    int currentMonth = (int)[monthComponents month];
    
    NSDateComponents *yearComponents  = [currentCalendar components:NSCalendarUnitYear  fromDate:today];
    int currentYear  = (int)[yearComponents year];
    int nextYear     = currentYear + 1;
    
    int months  = 1;
    int year;
    
    NSMutableArray *monthsArray = [[NSMutableArray alloc]init];
    
    //[monthsArray addObject:[NSString stringWithFormat:@"%@ %i",[[dateFormatter monthSymbols] objectAtIndex: currentMonth],currentYear]];
    
    for(int m = currentMonth; months <= 12; m++) {
        
        int nextMonth;
        
        if (m == 12)
        {
            nextMonth = 12;
        }
        else
        {
            nextMonth = (m % 12);
        }
        
        if(nextMonth < currentMonth){
            year = nextYear;
        } else {
            year = currentYear;
        }
        
        if (nextMonth >= 0 )
        {
            NSLog(@"Filter pacjkage = %@ %i",[[dateFormatter monthSymbols] objectAtIndex: nextMonth-1],year);
            [monthsArray addObject:[NSString stringWithFormat:@"%@ %i",[[dateFormatter monthSymbols] objectAtIndex: nextMonth-1],year]];
        }
        
        months++;
    }
    
    
    return monthsArray;
}

-(void)selectApplyFiltered{
    [FIRAnalytics logEventWithName:@"International_Holidays_Filters" parameters:@{kFIRParameterItemID:@"International Holidays Button Click on Filter"}];
    
//    [self applyFilter];
}


- (IBAction)btn_backPress:(id)sender {
    [self backWithFilres];
}

- (IBAction)btn_resetPress:(id)sender {
    [self.mainNewFilterDict removeAllObjects];
//    self.mainNewFilterDict = [[NSMutableDictionary alloc]init];
    [self.tableView_Filter reloadData];
    [[NSUserDefaults standardUserDefaults] setObject:self.mainNewFilterDict forKey:filterOptionsDict];
}

- (IBAction)btn_applyFilterPress:(id)sender {
    [self backWithFilres];
}
-(void)backWithFilres{
    [self saveAllValeusInFilterUserDefaults];
    [self.filterDelgate applySelectedNewFilterInSRP];
    [self.navigationController popViewControllerAnimated:YES];
}
@end
