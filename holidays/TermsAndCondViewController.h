//
//  TermsAndCondViewController.h
//  holidays
//
//  Created by ketan on 15/12/15.
//  Copyright © 2015 Mobicule. All rights reserved.
//

#import "BaseViewController.h"

@interface TermsAndCondViewController : BaseViewController
@property (strong, nonatomic) IBOutlet UIView *termsConditionView;
@property (weak, nonatomic) IBOutlet UIWebView *termsWebView;
@property (strong ,nonatomic) NSString *requestUrlString;

@end
