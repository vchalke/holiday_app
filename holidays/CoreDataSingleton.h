//
//  CoreDataSingleton.h
//  holidays
//
//  Created by Kush_Tech on 12/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreMotion/CoreMotion.h>
#import <CoreData/CoreData.h>
#import "AppDelegate.h"
#import "WebUrlConstants.h"
#import "RecentHolidayObject.h"
#import "WishListHolidayObject.h"
#import "CompareHolidayObject.h"
NS_ASSUME_NONNULL_BEGIN

@interface CoreDataSingleton : NSObject{
    AppDelegate *app_delegate;
}
+ (id)sharedInstance;
- (id)init;
-(NSManagedObjectContext*)getManagedObjectContext;
-(NSFetchRequest*)getFetchRequesrForEntityName:(NSString*)entityName;
-(NSArray*)getArrayOfObjectForEntityName:(NSString*)entityName;
-(void)saveRecentObjectInCoreDataForEntity:(NSString*)entityName withObject:(RecentHolidayObject*)recentObj;
-(void)saveWishListObjectInCoreDataForEntity:(NSString*)entityName withObject:(WishListHolidayObject*)wishListObj;
-(void)saveCompareListObjectInCoreDataForEntity:(NSString*)entityName withObject:(CompareHolidayObject*)wishListObj;
-(BOOL)checkIsPackagePresentInEntityWithName:(NSString*)entityName withPackageID:(NSString*)packageString;
-(NSArray*)getdeletedObjectFromPackageListArray:(NSArray*)wishListsArray withPackID:(NSString*)packageString forEntityName:(NSString*)entityName;
- (void)deleteEntitiesWithName:(NSString*)entityName;
@end

NS_ASSUME_NONNULL_END
