//
//  ExpandableCell.h
//  holidays
//
//  Created by Pushpendra Singh on 21/08/15.
//  Copyright (c) 2015 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HolidayExpandableCell : UITableViewCell<UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *imgColor;
@property (weak, nonatomic) IBOutlet UIImageView *imgIcon;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgIndicator;
@property (strong, nonatomic) IBOutlet UIWebView *webView;

@property (weak, nonatomic) IBOutlet UILabel *lblChildTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgChildIndicator;
@property (strong, nonatomic) IBOutlet UIWebView *topWebView;
@property (strong, nonatomic) IBOutlet UIView *VizaView;
@property (strong, nonatomic) IBOutlet UIButton *vizaButton;
@property (strong, nonatomic) IBOutlet UIButton *passportButton;
@property (strong, nonatomic) IBOutlet UIButton *insuranceButton;
@property (strong,nonatomic) IBOutlet UIImageView *imgCheck;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintVisaView;

@end
