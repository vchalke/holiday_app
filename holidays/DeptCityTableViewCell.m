//
//  DeptCityTableViewCell.m
//  holidays
//
//  Created by Kush_Team on 24/06/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "DeptCityTableViewCell.h"
#import "Holiday.h"
#import "WebUrlConstants.h"

@implementation DeptCityTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    _txt_deptCity.delegate = self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setupCityTableView{
    selectDeptCity = ([self.deptCityFilterDict objectForKey:filterDepartureCity]) ? [NSString stringWithFormat:@"%@",[self.deptCityFilterDict objectForKey:filterDepartureCity]] : @"";
    self.txt_deptCity.text = selectDeptCity;
    
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    [self openDepartureCities];
    return false;
}
-(void)openDepartureCities{
    UIAlertController *departureActnSht = [UIAlertController alertControllerWithTitle:@"Departure City"
                                                                              message:@"Select your choice"
                                                                       preferredStyle:UIAlertControllerStyleActionSheet];
    
    for (int j =0 ; j<_arrayDepartureList.count; j++)
    {
        NSString *titleString = _arrayDepartureList[j];
        UIAlertAction *action = [UIAlertAction actionWithTitle:titleString style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
            self.txt_deptCity.text = _arrayDepartureList[j];
            [self.deptCityFilterDict setObject:self.txt_deptCity.text forKey:filterDepartureCity];
        }];
        
        [departureActnSht addAction:action];
    }
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
        
//        [self.deptCityDelgate clickCity:@""];
    }];
    NSLog(@"ArrayDepartureList %@",_arrayDepartureList);
    [departureActnSht addAction:cancelAction];
    [self.window.rootViewController presentViewController:departureActnSht animated:YES completion:nil];
    
}
/*
-(NSArray *)getDepartureCityFromArray
{
    NSMutableSet *departureCityList = [[NSMutableSet alloc] init];
    
    for (int index = 0; index < self.arrayOfHolidayData.count; index ++)
    {
        Holiday *holidayObject = self.arrayOfHolidayData[index];
        
        NSArray *hubArray = holidayObject.arrayOfHubList;
        
        if (hubArray)
        {
            if (hubArray.count != 0)
            {
                [departureCityList addObjectsFromArray:hubArray];

            }
        }
    }
    
    NSArray *finalDestinationList = [departureCityList allObjects];
    
    return finalDestinationList;
}
*/
@end
