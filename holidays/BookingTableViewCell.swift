//
//  BookingTableViewCell.swift
//  sotc-consumer-application
//
//  Created by Saurav Kumar on 25/07/17.
//  Copyright © 2017 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit

class BookingTableViewCell: UITableViewCell {


    @IBOutlet var containnerView: UIView!
    @IBOutlet var tourTitleLabel: UILabel!
    @IBOutlet var paymentButton: UIButton!
    @IBOutlet var tourDateLabel: UILabel!
    @IBOutlet var detailView: UIView!
    @IBOutlet var tourImageViewBG: UIImageView!
    @IBOutlet var bfnNumberLabel: UILabel!
    @IBOutlet var routeView: UIView!
    
    @IBOutlet var routeCollectionView: UICollectionView!
    
    var citiesCoveredArray:Array<CityCovered>?
    
    
    
    override func awakeFromNib() {
     
       
        super.awakeFromNib()
        // Initialization code
        
        
        self.routeCollectionView.register(UINib(nibName: "TourRouteCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "TourRouteCollectionViewCell")
        
 
        let layout = UICollectionViewFlowLayout()
        
        layout.scrollDirection = .horizontal //.horizontal
 
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumLineSpacing = 0.0
        layout.minimumInteritemSpacing = 0.0
        routeCollectionView.setCollectionViewLayout(layout, animated: true)
        
        routeCollectionView.dataSource = self
        routeCollectionView.delegate = self
        routeCollectionView.reloadData()

    }

    override func prepareForReuse(){
        self.tourImageViewBG.image = #imageLiteral(resourceName: "defaultImage")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
   
    public func downloadImage(filePath:String,url:String)  {
        
        
        var isFilePresent = TourDocumentController.isFileExistInDocumentDirectory(fileNamePath: filePath)

        if(isFilePresent.chkFile)
        {
           self.tourImageViewBG.image = UIImage(contentsOfFile: filePath)
            
        }else{
        
            
            NetworkCommunication.downloadDocuments(urlString: url, destinationFilePath: filePath, completion: { (downlodedfilePath, status) in
                
                printLog(status,filePath);
                
                 //self.tourImageViewBG.image = UIImage(contentsOfFile: filePath)
                
                if(status)
                {
                   // self.viewDownloadedFile(fileurl: URL(fileURLWithPath:filePath))
                    
                    self.tourImageViewBG.image = UIImage(contentsOfFile: filePath)
                    
                }
                else{
                        self.tourImageViewBG.image = #imageLiteral(resourceName: "defaultImage")
                    }
                self.setNeedsLayout()
                
            })
        }
        
        
        
    }
  
    
   
}

extension BookingTableViewCell: UICollectionViewDataSource {
    // implementations here ...
    
    //MARK: UICollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if let count:Int = citiesCoveredArray?.count ?? 0
        {
            return  count ;
        }
       
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TourRouteCollectionViewCell", for: indexPath as IndexPath) as! TourRouteCollectionViewCell
        
        if let city:CityCovered =  citiesCoveredArray?[indexPath.row]
        {
            var routelText = ""
            
             if let numberOfNights = city.noOfNights
            {
                routelText = numberOfNights + "N"
            }
            
            if let cityName = city.cityName
            {
                if indexPath.row + 1 == citiesCoveredArray?.count ?? 0
                {
                    routelText = routelText + " " + cityName
                    
                }else
                {
                    routelText = routelText + " " + cityName + " -"
                }
                
            }
                
             cell.routeTitleLabel.text =  routelText //"\(city.noOfNights ?? "" )N \(city.cityName ?? "") - "
        }
        
        //" 1 America"
        cell.routeTitleLabel.textColor = UIColor.white
        cell.routeTitleLabel.font = UIFont(name: cell.routeTitleLabel.font.fontName, size: 15.0)
        
        
        return cell      //return your cell
        
        
        
    }
}

extension BookingTableViewCell: UICollectionViewDelegateFlowLayout {
    
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
     {
        if let city:CityCovered =  citiesCoveredArray?[indexPath.row]
        {
            if let numerOfNights:String = city.noOfNights
            {
               var  cityCoverd = numerOfNights + "N"
                
                  if let cityName:String = city.cityName
                  {
                    cityCoverd = cityCoverd + cityCoverd
                    
                    if let font =  UIFont(name:"Roboto-Light", size:17)
                    {
                    
                    let width = "\(numerOfNights)N \(cityName) - ".width(withConstraintedHeight: 30, font:font);
                        
                        return CGSize(width: width , height: 30)
                    }
                    
                    
                    
                }
                
            }            
            
        }
        
        return  CGSize(width: 50.0, height:30)
    }
    
    
    
   
    
    
}

extension String {
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: convertToOptionalNSAttributedStringKeyDictionary([convertFromNSAttributedStringKey(NSAttributedString.Key.font): font]), context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstraintedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: convertToOptionalNSAttributedStringKeyDictionary([convertFromNSAttributedStringKey(NSAttributedString.Key.font): font]), context: nil)
        
        return ceil(boundingBox.width)
    }
}


// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromNSAttributedStringKey(_ input: NSAttributedString.Key) -> String {
	return input.rawValue
}
