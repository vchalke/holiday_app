//
//  ImageViewCell.h
//  holidays
//
//  Created by Designer on 06/01/16.
//  Copyright © 2016 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIView *selectorView;

@end
