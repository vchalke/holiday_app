//
//  CompareScreenVC.h
//  holidays
//
//  Created by Kush_Tech on 23/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//
//getCommonCitiesFromPackage
#import <UIKit/UIKit.h>
#import "PDPScreenVC.h"
#import "HolidayPackageDetail.h"
#import "CoreDataSingleton.h"
NS_ASSUME_NONNULL_BEGIN

@interface CompareScreenVC : NewMasterVC

@property (strong ,nonatomic) HolidayPackageDetail *holidayPackageDetailInCompare;


// First Compare View
@property (weak, nonatomic) IBOutlet UIImageView *tour_ImgeViewOne;
@property (weak, nonatomic) IBOutlet UIButton *btn_cancelPackageOne;
@property (weak, nonatomic) IBOutlet UIButton *btn_addAnotherPkgOne;
@property (weak, nonatomic) IBOutlet UILabel *lbl_packageNameOne;
@property (weak, nonatomic) IBOutlet UILabel *lbl_toursTypesOne;
@property (weak, nonatomic) IBOutlet UIButton *btn_tourTypeOne;
@property (weak, nonatomic) IBOutlet UILabel *lbl_departureCityOne;
@property (weak, nonatomic) IBOutlet UIButton *btn_departureCityOne;
@property (weak, nonatomic) IBOutlet UILabel *lbl_departureDateOne;
@property (weak, nonatomic) IBOutlet UIButton *btn_calenderOne;
@property (weak, nonatomic) IBOutlet UILabel *lbl_tourPrizeOne;
@property (weak, nonatomic) IBOutlet UITextView *txtview_CountriesCoverOne;
@property (weak, nonatomic) IBOutlet UILabel *lbl_holidayTypeOne;

@property (weak, nonatomic) IBOutlet UITextView *txtview_AccomOne;
@property (weak, nonatomic) IBOutlet UITextView *txtview_SightOne;
@property (weak, nonatomic) IBOutlet UITextView *txtview_MealOne;
@property (weak, nonatomic) IBOutlet UITextView *txtview_InclusionOne;


// Second Compare View
@property (weak, nonatomic) IBOutlet UIImageView *tour_ImgeViewTwo;
@property (weak, nonatomic) IBOutlet UIButton *btn_cancelPackageTwo;
@property (weak, nonatomic) IBOutlet UIButton *btn_addAnotherPkgTwo;
@property (weak, nonatomic) IBOutlet UILabel *lbl_packageNameTwo;
@property (weak, nonatomic) IBOutlet UILabel *lbl_toursTypesTwo;
@property (weak, nonatomic) IBOutlet UIButton *btn_tourTypeTwo;
@property (weak, nonatomic) IBOutlet UILabel *lbl_departureCityTwo;
@property (weak, nonatomic) IBOutlet UIButton *btn_tdepartureCityTwo;
@property (weak, nonatomic) IBOutlet UILabel *lbl_departureDateTwo;
@property (weak, nonatomic) IBOutlet UIButton *btn_calenderTwo;
@property (weak, nonatomic) IBOutlet UILabel *lbl_tourPrizeTwo;
@property (weak, nonatomic) IBOutlet UITextView *txtview_CountriesCoverTwo;
@property (weak, nonatomic) IBOutlet UILabel *lbl_holidayTypeTwo;

@property (weak, nonatomic) IBOutlet UITextView *txtview_AccomTwo;
@property (weak, nonatomic) IBOutlet UITextView *txtview_SightTwo;
@property (weak, nonatomic) IBOutlet UITextView *txtview_MealTwo;
@property (weak, nonatomic) IBOutlet UITextView *txtview_InclusionTwo;

@end

NS_ASSUME_NONNULL_END
