//
//  categoriesCell.h
//  SOTCAPPDEsign
//
//  Created by Kush Thakkar on 03/03/20.
//  Copyright © 2020 Kush Thakkar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WhatsNewObject.h"
NS_ASSUME_NONNULL_BEGIN

@interface categoriesCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbl_Titles;
@property (weak, nonatomic) IBOutlet UIImageView *imges_view;
-(void)showCategory:(WhatsNewObject*)object;
@end

NS_ASSUME_NONNULL_END
