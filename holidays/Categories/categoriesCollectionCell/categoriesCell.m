//
//  categoriesCell.m
//  SOTCAPPDEsign
//
//  Created by Kush Thakkar on 03/03/20.
//  Copyright © 2020 Kush Thakkar. All rights reserved.
//

#import "categoriesCell.h"
#import "UIImageView+WebCache.h"

@implementation categoriesCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(void)showCategory:(WhatsNewObject*)object{
    self.lbl_Titles.text = object.bannerName;
    NSURL* urlImage=[NSURL URLWithString:object.bannerSmallImage];
    if (urlImage == nil){
        urlImage=[NSURL URLWithString:object.mainBannerImgUrl];
    }
    if(urlImage)
    {
        UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        indicator.center = self.imges_view.center;
        [self.imges_view addSubview:indicator];
        [indicator startAnimating];
        [self.imges_view sd_setImageWithURL:urlImage placeholderImage:[UIImage imageNamed:@"defaultBanner.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
            [indicator stopAnimating];
        }];
    }
}
@end
