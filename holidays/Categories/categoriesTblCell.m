//
//  categoriesTblCell.m
//  SOTCAPPDEsign
//
//  Created by Kush Thakkar on 03/03/20.
//  Copyright © 2020 Kush Thakkar. All rights reserved.
//

#import "categoriesTblCell.h"
#import "categoriesCell.h"
@implementation categoriesTblCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setupCollectionViewTitle:(NSString*)title subTitle:(NSString*)subTitle
{
    [self.collectionView registerNib:[UINib nibWithNibName:@"categoriesCell" bundle:nil] forCellWithReuseIdentifier:@"categoriesCell"];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
//    double  consts = self.collectionView.frame.size.height * 4 / self.frame.size.height;
    titleArray = [NSArray arrayWithObjects:@"Family",@"Romantic",@"Youth",@"Cruise",@"Kid Special",@"Wild Life", nil];
    imgesArray = [NSArray arrayWithObjects:@"psdFamily",@"psdRomantic",@"psdYouth",@"psdCruise",@"psdKidSpecial",@"psdWildlife", nil];
    categoryArray = [NSArray arrayWithObjects:@"Family",@"Couple",@"Youth Special",@"Cruise",@"Kidsfriendly",@"Wildlife", nil];
    searchTextArray = [NSArray arrayWithObjects:@"All Family Holidays",@"All Couple Holidays",@"All Youth Special Holidays",@"All Cruise Holidays",@"All Kidsfriendly Holidays",@"All Wildlife Holidays", nil];
    [self.collectionView reloadData];
    self.lbl_title.text = title;
    
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
//    return [titleArray count];
    return [self.categoryArray count];
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    categoriesCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"categoriesCell" forIndexPath:indexPath];
//    cell.lbl_Titles.text = [titleArray objectAtIndex:indexPath.row];
//    cell.imges_view.image = [UIImage imageNamed:[imgesArray objectAtIndex:indexPath.row]];
    [cell showCategory:[self.categoryArray objectAtIndex:indexPath.row]];
    return cell;
}
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(2.0, 2.0, 2.0, 2.0);
}
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat width = collectionView.frame.size.width / 3 - 10;
//    return CGSizeMake(width, 90);
    return CGSizeMake(width, collectionView.frame.size.height*0.48);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
//    [self.categoryDelegate selectedCategory:[categoryArray objectAtIndex:indexPath.row] andSearchText:[searchTextArray objectAtIndex:indexPath.row]];
    WhatsNewObject *obj = [self.categoryArray objectAtIndex:indexPath.row];
    [self.categoryDelegate selectedCategory:obj.bannerName andSearchText:[searchTextArray objectAtIndex:indexPath.row] withObejct:obj];
}

@end

