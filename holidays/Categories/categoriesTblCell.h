//
//  categoriesTblCell.h
//  SOTCAPPDEsign
//
//  Created by Kush Thakkar on 03/03/20.
//  Copyright © 2020 Kush Thakkar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WhatsNewObject.h"
NS_ASSUME_NONNULL_BEGIN
@protocol categoriesTblCellDelegaete <NSObject>
@optional
- (void)selectedCategory:(NSString*)categoryString andSearchText:(NSString*)searchStr withObejct:(WhatsNewObject*)objcet;
@end
@interface categoriesTblCell : UITableViewCell<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>{
    NSArray *titleArray;
    NSArray *imgesArray;
    NSArray *categoryArray;
    NSArray *searchTextArray;
}
@property (weak, nonatomic) IBOutlet UILabel *lbl_title;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (nonatomic, weak) id <categoriesTblCellDelegaete> categoryDelegate;
@property (weak, nonatomic)NSArray *categoryArray;
-(void)setupCollectionViewTitle:(NSString*)title subTitle:(NSString*)subTitle;

@end

NS_ASSUME_NONNULL_END
