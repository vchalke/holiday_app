//
//  NetworkManager.m
//  mobicule-sync-core
//
//  Created by Kishan on 16/07/15.
//  Copyright (c) 2015 Mobicule. All rights reserved.
//

#import "NetworkManager.h"


@implementation NetworkManager

- (NSString *)sendPostRequest:(NSString*)request {
    
    if(_isLogEnabled) {
        if (DEBUG_ENABLED) {
            debugMethodStart(@" ");
        }
    }
    
    HTTPCommunicationService *commService = [[HTTPCommunicationService alloc]init];
    
    NSString* response = [commService sendRequest:request withUrl:_URL withAuthorization:_authorization withShowLogs:_isLogEnabled];
    
    if(_isLogEnabled) {
        if (DEBUG_ENABLED) {
            debugMethodEnd(@" ");
        }
    }
    return response;
}

- (NSString *)sendPostRequest1:(NSString*)request withCompletionHandler:(void (^)(NSString * reponse))handler{
    
    if(_isLogEnabled) {
        if (DEBUG_ENABLED) {
            debugMethodStart(@" ");
        }
    }
    
    HTTPCommunicationService *commService = [[HTTPCommunicationService alloc]init];
    
    __block NSString *response = nil;
    
    [commService sendRequest1:request withUrl:_URL withAuthorization:_authorization withShowLogs:_isLogEnabled withCompletionHandler:^(NSString *resultString) {
        if (resultString != nil) {
            response = resultString;
            
            
            handler(response);
        }
        
    }];
    
    if(_isLogEnabled) {
        if (DEBUG_ENABLED) {
            debugMethodEnd(@" ");
        }
    }
    return response;
}


- (NSString *)sendGetRequest:(NSString*)request {
    
    if(_isLogEnabled) {
        if (DEBUG_ENABLED) {
            debugMethodStart(@" ");
        }
    }
    
    HTTPCommunicationService *commService = [[HTTPCommunicationService alloc]init];
    
    NSString* response = [commService sendRequest:request withUrl:_URL withAuthorization:_authorization withShowLogs:_isLogEnabled];
    
    if(_isLogEnabled) {
        if (DEBUG_ENABLED) {
            debugMethodEnd(@" ");
        }
    }
    return response;
}

- (void)setIsLogEnabled:(BOOL)isLogEnabled {
    
    
    _isLogEnabled = isLogEnabled;
    
}

- (void)setAuthorization:(NSString*)authorization {
 
    _authorization = authorization;
}

- (void) setServerUrl:(NSString*)URL {
    
    _URL = [URL stringByReplacingOccurrencesOfString:@" " withString:@""];
    
}

- (BOOL)validateConfiguration {
    
    NSURL * validateURL = [NSURL URLWithString:_URL];
    
    if (_URL == nil || [_URL isEqualToString:@""]) {
        
        if(_isLogEnabled) {
            if (DEBUG_ENABLED) {
                debug(@"Url is empty please set Url ");
            }
        }
        
        return NO;
    }
    
    else if  (validateURL && validateURL.scheme && validateURL.host) {
        return YES;
    }
    
    if(_isLogEnabled) {
        if (DEBUG_ENABLED) {
            debug(@"Invalid Url Type Please Configure Correct Url ");
        }
    }
    return NO;
}
@end
