//
//  OffersCollectionObject.m
//  holidays
//
//  Created by Kush_Team on 11/06/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "OffersCollectionObject.h"

@implementation OffersCollectionObject
-(instancetype)initWithCollectionObject:(NSDictionary *)arrayObjDict{
   if ([super init]){
       self.isOfferVisible = [arrayObjDict valueForKey:@"isOfferVisible"];
       self.packageid = [arrayObjDict valueForKey:@"packageid"];
      NSDictionary *tcilHolidayOffersPK = [arrayObjDict valueForKey:@"tcilHolidayOffersPK"];
       self.holidayOfferId = [[tcilHolidayOffersPK valueForKey:@"holidayOfferId"] integerValue];
       self.offerId = [[tcilHolidayOffersPK valueForKey:@"offerId"] integerValue];
       NSDictionary *tcilMstHolidayOffers = [arrayObjDict valueForKey:@"tcilMstHolidayOffers"];
             self.holidayOfferName = [tcilMstHolidayOffers valueForKey:@"holidayOfferName"];
   }
    return self;
}
/*
    {
      "isOfferVisible": "N",
      "packageid": "PKG000654",
      "tcilHolidayOffersPK": {
        "holidayOfferId": 9,
        "offerId": 40
      },
      "tcilMstHolidayOffers": {
        "createDate": "2018-12-13T13:38:18+05:30",
        "createdBy": "kumblet",
        "holidayOfferId": 9,
        "holidayOfferName": "Grand India Holiday Sale - Flat Rs. 8,000/- off per txn (Nov - On selected Departures Use Code GIHS8)",
        "ip": "14.141.26.106",
        "isActive": "Y",
        "offerFromDate": "2018-12-13T00:00:00+05:30",
        "offerToDate": "2019-12-18T00:00:00+05:30"
      }
    }
*/
@end
