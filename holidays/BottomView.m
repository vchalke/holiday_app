//
//  BottomView.m
//  Holiday
//
//  Created by Kush Thakkar on 15/02/20.
//  Copyright © 2020 Kush Thakkar. All rights reserved.
//

#import "BottomView.h"
#import "BottomCollectionViewCell.h"
#import "ColourClass.h"

@implementation BottomView

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
     self = [super initWithCoder:aDecoder];
     if(self) {
        [self loadNib];
    }
     return self;

}

- (instancetype)initWithFrame:(CGRect)frame {
     self = [super initWithFrame:frame];
     if(self) {
        [self loadNib];
    }
     return self;
}

-(void)loadNib{
    
    UIView *view = [[[NSBundle bundleForClass:[self class]]loadNibNamed:@"BottomView" owner:self options:nil] firstObject];
    view.frame = self.bounds;
    NSLog(@"Custom View Width %0.2f",view.frame.size.width);
    [self addSubview:view];
    [self layoutIfNeeded];
    [self setNeedsLayout];
    imgArray = [NSArray arrayWithObjects:@"home",@"booking",@"myAccount",@"notification",@"threeDots", nil];
    selectImgArray = [NSArray arrayWithObjects:@"homeInBlue",@"bookingInBlue",@"myAccountInBlue",@"notificationInBlue",@"threeDotsInBlue", nil];
    imgObjArray = [NSArray arrayWithObjects:_img_home,_img_booking,_img_myAccount,_img_notification,_img_more, nil];
    lblObjArray = [NSArray arrayWithObjects:_lbl_home,_lbl_booking,_lbl_myAccount,_lbl_notification,_lbl_more, nil];

}
-(void)buttonSelected:(NSInteger)index{
    for (int i = 0;i<[selectImgArray count];i++){
        if (index == i){
//            UILabel *lable = [lblObjArray objectAtIndex:index];
//            lable.textColor = [ColourClass colorWithHexString:@"004E9B"];
            UIImageView *imgView = [imgObjArray objectAtIndex:index];
            imgView.image = [UIImage imageNamed:[selectImgArray objectAtIndex:index]];
        }else{
//           UILabel *lable = [lblObjArray objectAtIndex:index];
//            lable.textColor = [UIColor blackColor];//FFFFFF
            UIImageView *imgView = [imgObjArray objectAtIndex:index];
            imgView.image = [UIImage imageNamed:[selectImgArray objectAtIndex:index]];
        }
    }
}
- (IBAction)btn_HonePress:(id)sender {
    [self.bottomDelegate homeButtonClick];
}
- (IBAction)btn_bookingPress:(id)sender {
    [self.bottomDelegate bookingButtonClick];
}
- (IBAction)btn_profilePress:(id)sender {
    [self.bottomDelegate profileButtonClick];
}
- (IBAction)btn_NotificationPress:(id)sender {
    [self.bottomDelegate notificationButtonClick];
}
- (IBAction)btn_MorePress:(id)sender {
    [self.bottomDelegate moreButtonClick];
}



@end
