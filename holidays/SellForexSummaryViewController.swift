//
//  SellForexSummaryViewController.swift
//  holidays
//
//  Created by Mac on 03/08/18.
//  Copyright © 2018 Mobicule. All rights reserved.
//

import UIKit

class SellForexSummaryViewController: ForexBaseViewController {

    @IBOutlet weak var productStackviewWidthConstant: NSLayoutConstraint!
    @IBOutlet weak var totalAmountOfProductThree: UILabel!
    @IBOutlet weak var totalAmountOfProductTwo: UILabel!
    @IBOutlet weak var totalAmountOfProductOne: UILabel!
    @IBOutlet weak var travellerForexINRAmountLabel: UILabel!
    @IBOutlet weak var calculationLabelofProductThree: UILabel!
    @IBOutlet var sellForexSummaryView: UIView!
    @IBOutlet weak var productTypeTwoLabel: UILabel!
    @IBOutlet weak var productTypeOneLabel: UILabel!
    @IBOutlet weak var productTypeThreeLabel: UILabel!
    @IBOutlet weak var calculationLabelofProductTwo: UILabel!
    @IBOutlet weak var calculationLabelofProductOne: UILabel!
    @IBOutlet weak var totalPayableAmountAllInclusiveLabel: UILabel!
    @IBOutlet weak var labelSGSTAmount: UILabel!
    @IBOutlet weak var heightConstraintOfSGSTView: NSLayoutConstraint!
    
    @IBOutlet weak var heightConstraintOfCGSTView: NSLayoutConstraint!
    @IBOutlet weak var labelCGST: UILabel!
    @IBOutlet weak var labelCGSTAmount: UILabel!
    var sellForexBO : SellForexBO =  SellForexBO.init()
    let stackviewProductHeight : CGFloat = 55
    override func viewDidLoad() {
        super.viewDidLoad()
        Bundle.main.loadNibNamed("SellForexSummaryViewController", owner: self, options: nil)
        
        super.addViewInBaseView(childView: self.sellForexSummaryView)
        super.setUpHeaderLabel(labelHeaderNameText: "Sell Forex")
        super.viewDidLoad()
        self.intializeView()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidLayoutSubviews()
    {
        super.setSubViewFrame(childView: self.sellForexSummaryView)
    }
    
    override func viewWillLayoutSubviews()
    {
        super.setSubViewFrame(childView: self.sellForexSummaryView)
    }
    func intializeView()
    {
        
        let productCount : Int = sellForexBO.productArray.count
        self.productStackviewWidthConstant.constant = CGFloat(productCount) * stackviewProductHeight
        
        for i in 0..<(3-productCount)
        {
            let view : UIView? = self.sellForexSummaryView.viewWithTag(i+1)
            if view != nil
            {
                view?.removeFromSuperview()
            }
        }
        var travellerINRAmount : Int = 0
        for i in 0..<sellForexBO.productArray.count
        {
            let product : ProductBO = sellForexBO.productArray[i]
            if i == 0
            {
                self.productTypeOneLabel.text = product.getProductName() as String
                self.calculationLabelofProductOne.text = "\(product.getFXAmount()) \(product.getCurrencyCode()) * \(product.getROE())"
                self.totalAmountOfProductOne.text = "\(product.getINRAmount()) INR"
            }
            else if i == 1
            {
                self.productTypeTwoLabel.text = product.getProductName() as String
                self.calculationLabelofProductTwo.text = "\(product.getFXAmount()) \(product.getCurrencyCode()) * \(product.getROE())"
                self.totalAmountOfProductTwo.text = "\(product.getINRAmount()) INR"
            }
            else if i == 2
            {
                self.productTypeThreeLabel.text = product.getProductName() as String
                self.calculationLabelofProductThree.text = "\(product.getFXAmount()) \(product.getCurrencyCode()) * \(product.getROE())"
                self.totalAmountOfProductThree.text = "\(product.getINRAmount()) INR"
            }
            travellerINRAmount = travellerINRAmount + product.inrAmount
        }
        
        self.travellerForexINRAmountLabel.text = "\(travellerINRAmount) INR"
        self.totalPayableAmountAllInclusiveLabel.text = "\(sellForexBO.totalAmountWithTax)"
        
        let forexTAXTuple = ForexAppUtility.getCGST_IGST_Total(quoteDict: sellForexBO.quoteResponseDict)
        
        if forexTAXTuple != nil
        {
            if let taxType:String = forexTAXTuple.taxType
            {
                if taxType == TaxType.CSGST_SGST.rawValue
                {
                    self.labelCGSTAmount.text = "(-) \(forexTAXTuple.totalCGST ?? 0.0) INR"
                    
                    self.labelSGSTAmount.text = "(-) \(forexTAXTuple.totalSGST ?? 0.0) INR"
                    
                    self.heightConstraintOfSGSTView.constant = 40
                    self.heightConstraintOfCGSTView.constant = 40
                    
                }
                else if taxType == TaxType.IGST.rawValue{
                    
                    self.labelCGSTAmount.text = "(-) \(forexTAXTuple.totlaIGST ?? 0.0) INR"
                    self.labelCGST.text = "IGST"
                    self.heightConstraintOfSGSTView.constant = 0
                    self.heightConstraintOfCGSTView.constant = 40
                }
            }
            
        }
    }
 
    
    
    @IBAction func homeButtonClicked(_ sender: Any) {
        SlideNavigationController.sharedInstance().popToRootViewController(animated: true)

    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
