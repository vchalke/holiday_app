//
//  ForexRightSlideViewController.h
//  holidays
//
//  Created by ketan on 10/01/18.
//  Copyright © 2018 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForexRightSlideViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tblViewMenu;

@end
