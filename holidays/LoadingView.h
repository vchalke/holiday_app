

#import <UIKit/UIKit.h>

@interface LoadingView : UIView
{
   
}
@property (atomic,retain)LoadingView *loadingView;
+ (id)loadingViewInView:(UIView *)aSuperview withString:(NSString*)label andIndicatorColor:(UIColor *)color;
- (void)removeView;

@end
