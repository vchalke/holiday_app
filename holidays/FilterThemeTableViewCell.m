//
//  FilterThemeTableViewCell.m
//  holidays
//
//  Created by Kush_Team on 24/06/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "FilterThemeTableViewCell.h"
#import "FilterLabelCollectionViewCell.h"
#import "WebUrlConstants.h"
@implementation FilterThemeTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setupTableViewCell{
    selectTheme = ([self.themeFilterDict objectForKey:filterTheme]) ? [NSString stringWithFormat:@"%@",[self.themeFilterDict objectForKey:filterTheme]] : @"";
    themesArray = [NSArray arrayWithObjects:@"Family",@"Adventure",@"Kid Friendly",@"Luxury",@"Cruise",@"Youth Special",@"Couple",@"Honeymoon", nil];
    [self.collectionView registerNib:[UINib nibWithNibName:@"FilterLabelCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"FilterLabelCollectionViewCell"];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    [self.collectionView reloadData];
    
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [themesArray count];//4;
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    FilterLabelCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"FilterLabelCollectionViewCell" forIndexPath:indexPath];
//    cell.btn_view.titleLabel.text = [themesArray objectAtIndex:indexPath.row];
    NSString *selectedStr = [themesArray objectAtIndex:indexPath.row];
    [cell.btn_view setTag:indexPath.row];
    [cell setButtonColour:[selectedStr isEqualToString:selectTheme]];
    [cell.btn_view setTitle:selectedStr forState:UIControlStateNormal];
    [cell.btn_view addTarget:self action:@selector(selectThemes:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(1.0, 1.0, 1.0, 1.0);
}
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
//    CGFloat width = collectionView.frame.size.width / 2 - 10;
//    CGFloat hight = collectionView.frame.size.height / 4 + 110;
    CGFloat width = collectionView.frame.size.width*0.3;
    CGFloat hight = collectionView.frame.size.height*0.28;
    return CGSizeMake(width, hight);
    
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
}
-(void)selectThemes:(UIButton*)sender{
    selectTheme = [themesArray objectAtIndex:sender.tag];
    [self.themeFilterDict setObject:selectTheme forKey:filterTheme];
    [self.collectionView reloadData];
}
@end
