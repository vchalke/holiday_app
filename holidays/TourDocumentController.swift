    //
//  TourDocumentController.swift
//  sotc-consumer-application
//
//  Created by Saurav Kumar on 17/09/17.
//  Copyright © 2017 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit

class TourDocumentController: NSObject {
    
    
    
    class func createFolderStructureForBFNumber(bfNumber:String) -> (bfNumberPath:String,paymentPath:String,ticketsPath:String,InsurancePath:String,FinalTourPath:String,visaPath:String,bookingListPath: String,itienearyTourPath:String){
        
        
        var filePaths: (bfNumberPath: String, paymentPath: String,visaPath: String, InsurancePath: String,FinalTourPath: String, ticketsPath: String,bookingListPath: String,itienearyTourPath:String)  = ("","","","","","","","")
        
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        
        
        
        let userMobileNumber:String = UserDefaults.standard.value(forKey:UserDefauldConstant.userMobileNumber) as! String
        
        
        let userMobileNumberFolder = documentsDirectory.appendingPathComponent("\(userMobileNumber)")
        
        
        do {
            try FileManager.default.createDirectory(atPath: userMobileNumberFolder.path, withIntermediateDirectories: false, attributes: nil)
            
        } catch let error as NSError {
            
            printLog("Error creating directory: \(error.localizedDescription)")
        }
        
        
         let bfNumberFolder = userMobileNumberFolder.appendingPathComponent("\(bfNumber)")
        
        
        do {
            try FileManager.default.createDirectory(atPath: bfNumberFolder.path, withIntermediateDirectories: false, attributes: nil)
            
        } catch let error as NSError {
            
            printLog("Error creating directory: \(error.localizedDescription)")
        }
        
        
        
        let paymentReceipts = bfNumberFolder.appendingPathComponent(Title.PAYMENT)
        let visa = bfNumberFolder.appendingPathComponent(Title.VISA)
        let tickets = bfNumberFolder.appendingPathComponent(Title.TICKETS)
        let insurance = bfNumberFolder.appendingPathComponent(Title.INSURANCE)
        let finalTourDetails = bfNumberFolder.appendingPathComponent(Title.FINAL_TOUR_DETAILS)
         let bookingList = bfNumberFolder.appendingPathComponent(Title.MY_BOOKING)
        let itienearyList = bfNumberFolder.appendingPathComponent(Title.ITINERARY)
        
        
        
        do {
            try FileManager.default.createDirectory(atPath: paymentReceipts.path, withIntermediateDirectories: false, attributes: nil)
            
        } catch let error as NSError {
            
            printLog("Error creating directory: \(error.localizedDescription)")
        }
        
        
        do {
            try FileManager.default.createDirectory(atPath: visa.path, withIntermediateDirectories: false, attributes: nil)
            
        } catch let error as NSError {
            
            printLog("Error creating directory: \(error.localizedDescription)")
        }
        
        
        do {
            try FileManager.default.createDirectory(atPath: tickets.path, withIntermediateDirectories: false, attributes: nil)
            
        } catch let error as NSError {
            
            printLog("Error creating directory: \(error.localizedDescription)")
        }
        
        
        do {
            try FileManager.default.createDirectory(atPath: insurance.path, withIntermediateDirectories: false, attributes: nil)
            
        } catch let error as NSError {
            
            printLog("Error creating directory: \(error.localizedDescription)")
        }
        
        
        do {
            try FileManager.default.createDirectory(atPath: finalTourDetails.path, withIntermediateDirectories: false, attributes: nil)
            
        } catch let error as NSError {
            
            printLog("Error creating directory: \(error.localizedDescription)")
        }
        
        do {
            try FileManager.default.createDirectory(atPath: bookingList.path, withIntermediateDirectories: false, attributes: nil)
            
        } catch let error as NSError {
            
            printLog("Error creating directory: \(error.localizedDescription)")
        }
        
        do {
            try FileManager.default.createDirectory(atPath: itienearyList.path, withIntermediateDirectories: false, attributes: nil)
            
        } catch let error as NSError {
            
            printLog("Error creating directory: \(error.localizedDescription)")
        }
        
        filePaths.bfNumberPath = bfNumberFolder.path
        filePaths.paymentPath = paymentReceipts.path
        
        filePaths.ticketsPath = tickets.path
        
        filePaths.InsurancePath = insurance.path
        
        filePaths.FinalTourPath = finalTourDetails.path
        
        filePaths.visaPath = visa.path
        
        filePaths.bookingListPath = bookingList.path
        
        filePaths.itienearyTourPath = itienearyList.path
        
       printLog(tickets.path)
        
        return filePaths
        
        
    }
    
    class func getDocumentDirectoryPathForMobileNumber(mobileNumber:String) -> URL
    {
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let userMobileNumber:String = UserDefaults.standard.value(forKey:UserDefauldConstant.userMobileNumber) as! String
        let userMobileNumberFolder = documentsDirectory.appendingPathComponent("\(userMobileNumber)")
        
        return userMobileNumberFolder
    }
    
  class  func deleteAllFileForCurrentUser()  {
    
 
    do {
        
        let userMobileNumber:String = UserDefaults.standard.value(forKey:UserDefauldConstant.userMobileNumber) as! String
        
        let fileManager = FileManager.default
        
        let folderURL = TourDocumentController.getDocumentDirectoryPathForMobileNumber(mobileNumber: userMobileNumber)
        
            if let enumerator = fileManager.enumerator(at: folderURL, includingPropertiesForKeys: nil) {
                
                while let fileURL = enumerator.nextObject() as? URL {
                    
                    try fileManager.removeItem(at: fileURL)
                }
                
                 try fileManager.removeItem(at: folderURL)
            }
           
    } catch  {
        
        printLog(error)
    }
    
    }
    
    class func encodeBase64(value:String) -> String
    {
        
      
        let data = (value).data(using: String.Encoding.utf8)
       if let base64 = data?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
       {
            return base64
        
        }
        
        return ""
        
      /*  let utf8str = value.data(using: String.Encoding.utf8)
        
        if let base64Encoded = utf8str?.base64EncodedStringWithOptions(NSData.Base64EncodingOptions(rawValue: 0))
        {
           
            
            
        }*/
        
    }
    
    
  class func decodeBase64ToString(base64Encoded:String) -> String {
//        if let base64Decoded = NSData(base64Encoded: base64Encoded, options:   NSData.Base64DecodingOptions(rawValue: 0))
//            .map({ NSString(data: $0 as Data, encoding: String.Encoding.utf8.rawValue) })
//        {
//            // Convert back to a string
//            let decodedString = String(describing: base64Decoded)
//            
//           return decodedString
//        }
    
    let decodedData = Data(base64Encoded: base64Encoded)!
    let decodedString = String(data: decodedData, encoding: .utf8)!
    
        return decodedString
    }
    
    class func isFileExistInDocumentDirectory(fileNamePath:String) -> (chkFile:Bool,destinatioFilePath:String)
    {
        
        let checkValidation = FileManager.default
        
        if (checkValidation.fileExists(atPath: fileNamePath))
        {
            return (true,fileNamePath)
        }
        else
        {
            return (false,fileNamePath)
        }
        
        
    }
    
    
    class func getDecodedFileName(url:String)->String
    {
        if let base64Encode:String = url.components(separatedBy: "/").last
        {
            if let decodedFilePath:String = TourDocumentController.decodeBase64ToString(base64Encoded: base64Encode)
            {
                if let fileName:String = decodedFilePath.components(separatedBy: "/").last
                {
                    return fileName
                }
            }
        }
        return ""
    }
    
    class func getDocumentFilePathByDecoding(url:String,bfnNumber:String,moduleFolder:String) -> String {
        
        
                if let fileName:String = TourDocumentController.getDecodedFileName(url:url)
                {
                    let filePaths = TourDocumentController.createFolderStructureForBFNumber(bfNumber: bfnNumber)
                    
                    var folderPath = ""
                    
                    switch moduleFolder {
                    case Title.PAYMENT:
                       folderPath = filePaths.paymentPath
                        
                    case Title.VISA:
                        folderPath = filePaths.visaPath
                        
                    case Title.TICKETS:
                        folderPath = filePaths.ticketsPath
                        
                    case Title.INSURANCE:
                        folderPath = filePaths.InsurancePath
                        
                    case Title.FINAL_TOUR_DETAILS:
                        folderPath = filePaths.FinalTourPath
                        
                    case Title.MY_BOOKING:
                        folderPath = filePaths.FinalTourPath
                     
                    case Title.ITINERARY:
                        folderPath = filePaths.itienearyTourPath
                        
                    default:
                        folderPath = filePaths.bookingListPath
                    }
                    
                    let filePath = folderPath + "/" + fileName
                    return filePath
                }
        
        
        return ""
    }
    
    class func getDocumentFilePath(url:String,bfnNumber:String,moduleFolder:String) -> String {
        
                            
                    let filePaths = TourDocumentController.createFolderStructureForBFNumber(bfNumber: bfnNumber)
                    
                    var folderPath = ""
                    
                    switch moduleFolder {
                    case Title.PAYMENT:
                        folderPath = filePaths.paymentPath
                        
                    case Title.VISA:
                        folderPath = filePaths.visaPath
                        
                    case Title.TICKETS:
                        folderPath = filePaths.ticketsPath
                        
                    case Title.INSURANCE:
                        folderPath = filePaths.InsurancePath
                        
                    case Title.FINAL_TOUR_DETAILS:
                        folderPath = filePaths.FinalTourPath
                        
                    case Title.MY_BOOKING:
                        folderPath = filePaths.bookingListPath
                        
                    case Title.ITINERARY:
                        folderPath = filePaths.itienearyTourPath
                        
                    default:
                        folderPath = filePaths.bfNumberPath
                    }
                    
                    let filePath = folderPath + "/" + url
                    return filePath
        
        
        
    }
    
    class func getTourFileDirectoryPath(bfnNumber:String) -> (bfNumberPath:String,paymentPath:String,ticketsPath:String,InsurancePath:String,FinalTourPath:String,visaPath:String,bookingListPath: String,itienearyTourPath:String) {
        
        
        let filePaths = TourDocumentController.createFolderStructureForBFNumber(bfNumber: bfnNumber)
        
 
        return filePaths
    }

    
    
    
}
