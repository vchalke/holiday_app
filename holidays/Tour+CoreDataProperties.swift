//
//  Tour+CoreDataProperties.swift
//  holidays
//
//  Created by Kush_Team on 24/08/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//
//

import Foundation
import CoreData


extension Tour {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Tour> {
        return NSFetchRequest<Tour>(entityName: "Tour")
    }

    @NSManaged public var address: String?
    @NSManaged public var arrivalDate: Date?
    @NSManaged public var bfNumber: String?
    @NSManaged public var bookingFileName: String?
    @NSManaged public var bookingFileStatus: String?
    @NSManaged public var branch: String?
    @NSManaged public var branchAddress: String?
    @NSManaged public var branchNumber: String?
    @NSManaged public var businessUnit: String?
    @NSManaged public var createdBy: String?
    @NSManaged public var departureDate: Date?
    @NSManaged public var emailID: String?
    @NSManaged public var emrDetails: String?
    @NSManaged public var heKitFlag: String?
    @NSManaged public var hub: String?
    @NSManaged public var kyc: String?
    @NSManaged public var market: String?
    @NSManaged public var mobileNumber: String?
    @NSManaged public var noOfAdults: String?
    @NSManaged public var noOfChild: String?
    @NSManaged public var noOfInfant: String?
    @NSManaged public var noOfNights: String?
    @NSManaged public var noOfTravelers: String?
    @NSManaged public var opportunityId: String?
    @NSManaged public var ownerId: String?
    @NSManaged public var packageId: String?
    @NSManaged public var packageName: String?
    @NSManaged public var paymentFlag: String?
    @NSManaged public var performInoiceDocURL: String?
    @NSManaged public var piNumber: String?
    @NSManaged public var prodITINCode: String?
    @NSManaged public var productBookingStatus: String?
    @NSManaged public var region: String?
    @NSManaged public var tourCode: String?
    @NSManaged public var tourName: String?
    @NSManaged public var tourZone: String?
    @NSManaged public var visaDocUrl: String?
    @NSManaged public var bookingDate: Date?
    @NSManaged public var bookedOptionalPackageRelation: NSSet?
    @NSManaged public var cityCoveredRelation: NSSet?
    @NSManaged public var deviationRelation: NSSet?
    @NSManaged public var insuranceRelation: NSSet?
    @NSManaged public var itineraryRelation: NSSet?
    @NSManaged public var optionalMasterRelation: NSSet?
    @NSManaged public var packageImagesRelation: NSSet?
    @NSManaged public var passengerDocumentRelation: NSSet?
    @NSManaged public var passengerRelation: NSSet?
    @NSManaged public var paymentRelation: NSSet?
    @NSManaged public var receiptRelation: NSSet?
    @NSManaged public var tourImagesRelation: NSSet?
    @NSManaged public var userProfileRelation: UserProfile?
    @NSManaged public var visaCountryRelation: NSSet?
    @NSManaged public var visaRelation: NSSet?

}

// MARK: Generated accessors for bookedOptionalPackageRelation
extension Tour {

    @objc(addBookedOptionalPackageRelationObject:)
    @NSManaged public func addToBookedOptionalPackageRelation(_ value: OptionalPackageBooked)

    @objc(removeBookedOptionalPackageRelationObject:)
    @NSManaged public func removeFromBookedOptionalPackageRelation(_ value: OptionalPackageBooked)

    @objc(addBookedOptionalPackageRelation:)
    @NSManaged public func addToBookedOptionalPackageRelation(_ values: NSSet)

    @objc(removeBookedOptionalPackageRelation:)
    @NSManaged public func removeFromBookedOptionalPackageRelation(_ values: NSSet)

}

// MARK: Generated accessors for cityCoveredRelation
extension Tour {

    @objc(addCityCoveredRelationObject:)
    @NSManaged public func addToCityCoveredRelation(_ value: CityCovered)

    @objc(removeCityCoveredRelationObject:)
    @NSManaged public func removeFromCityCoveredRelation(_ value: CityCovered)

    @objc(addCityCoveredRelation:)
    @NSManaged public func addToCityCoveredRelation(_ values: NSSet)

    @objc(removeCityCoveredRelation:)
    @NSManaged public func removeFromCityCoveredRelation(_ values: NSSet)

}

// MARK: Generated accessors for deviationRelation
extension Tour {

    @objc(addDeviationRelationObject:)
    @NSManaged public func addToDeviationRelation(_ value: Deviation)

    @objc(removeDeviationRelationObject:)
    @NSManaged public func removeFromDeviationRelation(_ value: Deviation)

    @objc(addDeviationRelation:)
    @NSManaged public func addToDeviationRelation(_ values: NSSet)

    @objc(removeDeviationRelation:)
    @NSManaged public func removeFromDeviationRelation(_ values: NSSet)

}

// MARK: Generated accessors for insuranceRelation
extension Tour {

    @objc(addInsuranceRelationObject:)
    @NSManaged public func addToInsuranceRelation(_ value: Insurance)

    @objc(removeInsuranceRelationObject:)
    @NSManaged public func removeFromInsuranceRelation(_ value: Insurance)

    @objc(addInsuranceRelation:)
    @NSManaged public func addToInsuranceRelation(_ values: NSSet)

    @objc(removeInsuranceRelation:)
    @NSManaged public func removeFromInsuranceRelation(_ values: NSSet)

}

// MARK: Generated accessors for itineraryRelation
extension Tour {

    @objc(addItineraryRelationObject:)
    @NSManaged public func addToItineraryRelation(_ value: ItineraryDetail)

    @objc(removeItineraryRelationObject:)
    @NSManaged public func removeFromItineraryRelation(_ value: ItineraryDetail)

    @objc(addItineraryRelation:)
    @NSManaged public func addToItineraryRelation(_ values: NSSet)

    @objc(removeItineraryRelation:)
    @NSManaged public func removeFromItineraryRelation(_ values: NSSet)

}

// MARK: Generated accessors for optionalMasterRelation
extension Tour {

    @objc(addOptionalMasterRelationObject:)
    @NSManaged public func addToOptionalMasterRelation(_ value: OptionalMaster)

    @objc(removeOptionalMasterRelationObject:)
    @NSManaged public func removeFromOptionalMasterRelation(_ value: OptionalMaster)

    @objc(addOptionalMasterRelation:)
    @NSManaged public func addToOptionalMasterRelation(_ values: NSSet)

    @objc(removeOptionalMasterRelation:)
    @NSManaged public func removeFromOptionalMasterRelation(_ values: NSSet)

}

// MARK: Generated accessors for packageImagesRelation
extension Tour {

    @objc(addPackageImagesRelationObject:)
    @NSManaged public func addToPackageImagesRelation(_ value: TourPackageImages)

    @objc(removePackageImagesRelationObject:)
    @NSManaged public func removeFromPackageImagesRelation(_ value: TourPackageImages)

    @objc(addPackageImagesRelation:)
    @NSManaged public func addToPackageImagesRelation(_ values: NSSet)

    @objc(removePackageImagesRelation:)
    @NSManaged public func removeFromPackageImagesRelation(_ values: NSSet)

}

// MARK: Generated accessors for passengerDocumentRelation
extension Tour {

    @objc(addPassengerDocumentRelationObject:)
    @NSManaged public func addToPassengerDocumentRelation(_ value: TourPassengerDouments)

    @objc(removePassengerDocumentRelationObject:)
    @NSManaged public func removeFromPassengerDocumentRelation(_ value: TourPassengerDouments)

    @objc(addPassengerDocumentRelation:)
    @NSManaged public func addToPassengerDocumentRelation(_ values: NSSet)

    @objc(removePassengerDocumentRelation:)
    @NSManaged public func removeFromPassengerDocumentRelation(_ values: NSSet)

}

// MARK: Generated accessors for passengerRelation
extension Tour {

    @objc(addPassengerRelationObject:)
    @NSManaged public func addToPassengerRelation(_ value: Passenger)

    @objc(removePassengerRelationObject:)
    @NSManaged public func removeFromPassengerRelation(_ value: Passenger)

    @objc(addPassengerRelation:)
    @NSManaged public func addToPassengerRelation(_ values: NSSet)

    @objc(removePassengerRelation:)
    @NSManaged public func removeFromPassengerRelation(_ values: NSSet)

}

// MARK: Generated accessors for paymentRelation
extension Tour {

    @objc(addPaymentRelationObject:)
    @NSManaged public func addToPaymentRelation(_ value: Payment)

    @objc(removePaymentRelationObject:)
    @NSManaged public func removeFromPaymentRelation(_ value: Payment)

    @objc(addPaymentRelation:)
    @NSManaged public func addToPaymentRelation(_ values: NSSet)

    @objc(removePaymentRelation:)
    @NSManaged public func removeFromPaymentRelation(_ values: NSSet)

}

// MARK: Generated accessors for receiptRelation
extension Tour {

    @objc(addReceiptRelationObject:)
    @NSManaged public func addToReceiptRelation(_ value: PaymentReceipt)

    @objc(removeReceiptRelationObject:)
    @NSManaged public func removeFromReceiptRelation(_ value: PaymentReceipt)

    @objc(addReceiptRelation:)
    @NSManaged public func addToReceiptRelation(_ values: NSSet)

    @objc(removeReceiptRelation:)
    @NSManaged public func removeFromReceiptRelation(_ values: NSSet)

}

// MARK: Generated accessors for tourImagesRelation
extension Tour {

    @objc(addTourImagesRelationObject:)
    @NSManaged public func addToTourImagesRelation(_ value: ItineraryImages)

    @objc(removeTourImagesRelationObject:)
    @NSManaged public func removeFromTourImagesRelation(_ value: ItineraryImages)

    @objc(addTourImagesRelation:)
    @NSManaged public func addToTourImagesRelation(_ values: NSSet)

    @objc(removeTourImagesRelation:)
    @NSManaged public func removeFromTourImagesRelation(_ values: NSSet)

}

// MARK: Generated accessors for visaCountryRelation
extension Tour {

    @objc(addVisaCountryRelationObject:)
    @NSManaged public func addToVisaCountryRelation(_ value: VisaCountryMaster)

    @objc(removeVisaCountryRelationObject:)
    @NSManaged public func removeFromVisaCountryRelation(_ value: VisaCountryMaster)

    @objc(addVisaCountryRelation:)
    @NSManaged public func addToVisaCountryRelation(_ values: NSSet)

    @objc(removeVisaCountryRelation:)
    @NSManaged public func removeFromVisaCountryRelation(_ values: NSSet)

}

// MARK: Generated accessors for visaRelation
extension Tour {

    @objc(addVisaRelationObject:)
    @NSManaged public func addToVisaRelation(_ value: Visa)

    @objc(removeVisaRelationObject:)
    @NSManaged public func removeFromVisaRelation(_ value: Visa)

    @objc(addVisaRelation:)
    @NSManaged public func addToVisaRelation(_ values: NSSet)

    @objc(removeVisaRelation:)
    @NSManaged public func removeFromVisaRelation(_ values: NSSet)

}
