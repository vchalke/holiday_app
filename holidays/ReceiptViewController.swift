//
//  ReceiptViewController.swift
//  holidays
//
//  Created by Komal Katkade on 11/30/17.
//  Copyright © 2017 Mobicule. All rights reserved.
//

import UIKit

class ReceiptViewController: ForexBaseViewController,UITableViewDataSource,UITableViewDelegate {
    @IBOutlet weak var heightOfReceiptTableView: NSLayoutConstraint!
    @IBOutlet var sellForexReceiptView: UIView!

    @IBOutlet var receiptView: UIView!
    @IBOutlet weak var receiptScrollView: UIScrollView!
    @IBOutlet weak var receiptTableView: UITableView!
    var noOfProduct = 5
    override func viewDidLoad() {
        super.viewDidLoad()
        Bundle.main.loadNibNamed("ReceiptViewController", owner: self, options: nil)
        
        super.addViewInBaseView(childView: self.sellForexReceiptView)
        super.viewDidLoad()
        self.receiptTableView.register(UINib(nibName: "ReceiptTableViewCell", bundle: nil), forCellReuseIdentifier:  "ReceiptCell")
        self.receiptTableView.isScrollEnabled = false
        
        self.receiptScrollView.addSubview(self.receiptView)
        heightOfReceiptTableView.constant = CGFloat(noOfProduct * 99)
        self.receiptScrollView.contentSize = CGSize.init(width: 0, height: heightOfReceiptTableView.constant + 500)
        self.receiptView.frame = CGRect.init(x: 0, y: 0, width: Int(self.receiptScrollView.frame.size.width), height:  Int(self.receiptScrollView.contentSize.height))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: tableview  delegate
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return noOfProduct
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell :  ReceiptTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ReceiptCell", for: indexPath) as! ReceiptTableViewCell
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 99
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
