//
//  NetworkReachable.h
//  holidays
//
//  Created by Kush_Tech on 07/05/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reachability.h"
NS_ASSUME_NONNULL_BEGIN

@interface NetworkReachable : NSObject
+ (id)sharedInstance;
- (id)init;
@property (nonatomic,strong)Reachability *reachable;
- (BOOL)checkIsConnected;
-(void)postNetworkNotification:(NSString*)status;
-(void)obseverNetworkNotification:(void(^) (NSString *isNetwork))updateHandler;
@end

NS_ASSUME_NONNULL_END
