//
//  ItinenaryDetailTableViewCell.h
//  holidays
//
//  Created by Kush_Tech on 05/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PackageDetailModel.h"

NS_ASSUME_NONNULL_BEGIN

@protocol ItinenaryDetailTableViewCellDelegaete <NSObject>
@optional
- (void)jumpToItinenary;
@end

@interface ItinenaryDetailTableViewCell : UITableViewCell<UITableViewDelegate,UITableViewDataSource>{
    NSMutableArray *dataArray;
    NSInteger selectedOption;
    NSInteger itinenaryCount;
}
@property (nonatomic, weak) id <ItinenaryDetailTableViewCellDelegaete> itinenaryDelegate;
@property (weak, nonatomic) IBOutlet UILabel *lbl_descrption;
@property (weak, nonatomic) IBOutlet UITableView *tableViews;
@property (weak, nonatomic) IBOutlet UIWebView *web_ItinenaryView;

-(void)loadDataFromPackageModel:(PackageDetailModel*)packageModel;
@end

NS_ASSUME_NONNULL_END
