//
//  FilterScreenVC.h
//  holidays
//
//  Created by Kush_Tech on 17/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewMasterVC.h"
#import "TTRangeSlider.h"
#import "TTRangeSliderDelegate.h"
#import "WebUrlConstants.h"
#import "FilteredOptionsVC.h"
#import <FirebaseAnalytics/FirebaseAnalytics.h>

NS_ASSUME_NONNULL_BEGIN
@protocol FilterScreenDelegate <NSObject>
-(void)applyFilterWithCityArray:(NSArray*)cityArr budgetStart:(NSInteger)startValue budgetEnd:(NSInteger)endValue packType:(NSArray*)packTypeArr duration:(NSArray*)durationArr theme:(NSArray*)themeArr;
-(void)applySelectedFilterDictionaryInSRP;
@end
@interface FilterScreenVC : NewMasterVC<UIScrollViewDelegate,TTRangeSliderDelegate,FilteredOptionsVCDelegate>
@property (nonatomic, weak) id <FilterScreenDelegate> filterDelgate;
@property (weak, nonatomic) IBOutlet UITextField *txt_departureCity;
@property (weak, nonatomic) IBOutlet UITextField *txt_duration;
@property (weak, nonatomic) IBOutlet UITextField *txt_Theme;

@property (weak, nonatomic) IBOutlet UIImageView *groupImgView;
@property (weak, nonatomic) IBOutlet UIImageView *onlyStayImgView;
@property (weak, nonatomic) IBOutlet UIImageView *customizedImgView;
@property (weak, nonatomic) IBOutlet UIImageView *airInclusiveImgView;

@property (weak, nonatomic) IBOutlet UILabel *lbl_groupView;
@property (weak, nonatomic) IBOutlet UILabel *lbl_onlyStayView;
@property (weak, nonatomic) IBOutlet UILabel *lbl_customizedView;
@property (weak, nonatomic) IBOutlet UILabel *lbl_airInclusiveView;

@property (weak, nonatomic) IBOutlet UIButton *btn_group;
@property (weak, nonatomic) IBOutlet UIButton *btn_onlyStay;
@property (weak, nonatomic) IBOutlet UIButton *btn_customized;
@property (weak, nonatomic) IBOutlet UIButton *btn_airInclusive;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollViews;
@property (weak, nonatomic) IBOutlet TTRangeSlider *range_slider;
@property (strong, nonatomic) NSArray *holidayArrayInFilter;
@property (strong, nonatomic) NSDictionary *saveFilterDict;

@property (strong, nonatomic) NSArray *deptCountryArray;
@property (strong, nonatomic) NSArray *deptDurationArray;
@property (strong, nonatomic) NSArray *themesArray;

@end

NS_ASSUME_NONNULL_END
