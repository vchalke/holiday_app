//
//  SignUpViewController.h
//  holidays
//
//  Created by Swapnil on 10/01/18.
//  Copyright © 2018 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginCommunicationManager.h"

@protocol SignUpViewControllerDelegate

-(void)userloggedInSignUpSuccessFullWithUserDict:(NSString *)userID;

@end

@class LoadingView;

@interface SignUpViewController : UIViewController
{
    LoadingView *activityIndicator;
}

@property (weak, nonatomic) IBOutlet UIView *loginView;
@property (weak, nonatomic) IBOutlet UITextField *textFieldUserId;
@property (weak, nonatomic) IBOutlet UITextField *textFieldPassword;
@property (weak, nonatomic) IBOutlet UITextField *textFieldReEnterPassword;

@property (weak, nonatomic) IBOutlet UIView *userIdView;
@property (weak, nonatomic) IBOutlet UIView *passwordView;
@property (weak, nonatomic) IBOutlet UIView *reEnterPasswordView;

@property (weak,nonatomic) id <SignUpViewControllerDelegate> delegate;

- (IBAction)closeButtonClicked:(UIButton *)sender;
- (IBAction)signUpButtonClicked:(UIButton *)sender;

@end
