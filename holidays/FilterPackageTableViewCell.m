//
//  FilterPackageTableViewCell.m
//  holidays
//
//  Created by Kush_Team on 24/06/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "FilterPackageTableViewCell.h"
#import "WebUrlConstants.h"

@implementation FilterPackageTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setupTableViewCell{
    isStay = ([self.filterPackageDict objectForKey:filterByStay]) ? [NSString stringWithFormat:@"%@",[self.filterPackageDict objectForKey:filterByStay]] : @"NO";
    isAirInclusive = ([self.filterPackageDict objectForKey:filterByAirInclusive]) ? [NSString stringWithFormat:@"%@",[self.filterPackageDict objectForKey:filterByAirInclusive]] : @"NO";
    isGrouped = ([self.filterPackageDict objectForKey:filterByGroup]) ? [NSString stringWithFormat:@"%@",[self.filterPackageDict objectForKey:filterByGroup]] : @"NO";
    isCustomized = ([self.filterPackageDict objectForKey:filterByCustomized]) ? [NSString stringWithFormat:@"%@",[self.filterPackageDict objectForKey:filterByCustomized]] : @"NO";
    [self setSwictes];
}
-(void)setSwictes{
    [self.switch_OnlyStay addTarget:self action:@selector(setStateChanegd:) forControlEvents:UIControlEventTouchUpInside];
    [self.switch_airInclusive addTarget:self action:@selector(setStateChanegd:) forControlEvents:UIControlEventTouchUpInside];
    [self.switch_groupTour addTarget:self action:@selector(setStateChanegd:) forControlEvents:UIControlEventTouchUpInside];
    [self.switch_customized addTarget:self action:@selector(setStateChanegd:) forControlEvents:UIControlEventTouchUpInside];

    self.switch_OnlyStay.on = [isStay isEqualToString:@"YES"];
    self.switch_airInclusive.on = [isAirInclusive isEqualToString:@"YES"];
    self.switch_groupTour.on = [isGrouped isEqualToString:@"YES"];
    self.switch_customized.on = [isCustomized isEqualToString:@"YES"];
}

- (void)setStateChanegd:(id)sender
{
    BOOL state = [sender isOn];
    switch ([sender tag]) {
        case 0:
            self.switch_OnlyStay.on = state;
            [self.filterPackageDict setObject:state ? @"YES" : @"NO" forKey:filterByStay];
            self.switch_airInclusive.on = NO;
            [self.filterPackageDict setObject:@"NO" forKey:filterByAirInclusive];
            break;
        case 1:
            self.switch_airInclusive.on = state;
            [self.filterPackageDict setObject:state ? @"YES" : @"NO" forKey:filterByAirInclusive];
            self.switch_OnlyStay.on = NO;
            [self.filterPackageDict setObject:@"NO" forKey:filterByStay];
            break;
        case 2:
            self.switch_groupTour.on = state;
            [self.filterPackageDict setObject:state ? @"YES" : @"NO" forKey:filterByGroup];
            self.switch_customized.on = NO;
            [self.filterPackageDict setObject:@"NO" forKey:filterByCustomized];
            break;
        case 3:
            self.switch_customized.on = state;
            [self.filterPackageDict setObject:state ? @"YES" : @"NO" forKey:filterByCustomized];
            self.switch_groupTour.on = NO;
            [self.filterPackageDict setObject:@"NO" forKey:filterByGroup];
            break;
            
        default:
            
            break;
    }
    
}

@end
