//
//  SellForexConfirmOrderViewController.swift
//  holidays
//
//  Created by Komal Katkade on 11/30/17.
//  Copyright © 2017 Mobicule. All rights reserved.
//

import UIKit

class SellForexConfirmOrderViewController: ForexBaseViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {
    @IBOutlet weak var buttonContinue: UIButton!
    
    @IBOutlet weak var labelCGSTAmount: UILabel!
    @IBOutlet weak var labelCGST: UILabel!
    @IBOutlet weak var labelSGSTAmount: UILabel!
    @IBOutlet weak var labelTaxCalculationAmount: UILabel!
    @IBOutlet var sellForexConfirmOrderView: UIView!
    @IBOutlet weak var heightConstraintOfSGSTView: NSLayoutConstraint!
    @IBOutlet weak var heightConstraintOfCGSTView: NSLayoutConstraint!
    @IBOutlet weak var heightConstraintOfTableView: NSLayoutConstraint!
    @IBOutlet weak var sellForexConfirmOrderTableview: UITableView!
    var productArray : [ProductBO] = []
    var responseDict: NSDictionary = [:] ;
    var sellForexBO : SellForexBO =  SellForexBO.init()
    
    override func viewDidLoad()
    {
        Bundle.main.loadNibNamed("SellForexConfirmOrderViewController", owner: self, options: nil)
        
        super.addViewInBaseView(childView: self.sellForexConfirmOrderView)
        super.setUpHeaderLabel(labelHeaderNameText: "Sell Forex")
        super.viewDidLoad()
        self.sellForexConfirmOrderTableview.register(UINib(nibName: "SellForexConfirmOrderCell", bundle: nil), forCellReuseIdentifier:  "sellForexConfirmOrderCell")
        self.sellForexConfirmOrderTableview.isScrollEnabled = false
        productArray = sellForexBO.productArray
        heightConstraintOfTableView.constant = CGFloat(productArray.count * 40)
        getQuoteForSell()
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ForexBuyPassangerViewController.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        buttonContinue.layer.cornerRadius = 10
    }
    override func viewDidLayoutSubviews()
    {
        super.setSubViewFrame(childView: self.sellForexConfirmOrderView)
        
    }
    override func viewWillLayoutSubviews()
    {
        super.setSubViewFrame(childView: self.sellForexConfirmOrderView)
        
    }
    func dismissKeyboard()
    {
        view.endEditing(true)
    }
    
    // MARK:  textfield  delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        self.view.endEditing(true)
        return true
    }
    
    // MARK: button action neha.b@focusite.com
    @IBAction func continueButtonClicked(_ sender: Any)
    {
        let loginStatus = UserDefaults.standard.string(forKey: kLoginStatus)
        print("loginStatus---> \(String(describing: loginStatus))")
        
        if loginStatus == kLoginSuccess
        {
            let userId = UserDefaults.standard.string(forKey: kLoginEmailId)
            
            let sellForxVC : SellerDetailsViewController = SellerDetailsViewController.init(nibName: "ForexBaseViewController", bundle: nil)
            sellForxVC.sellForexBO = sellForexBO
            sellForexBO.emailID = userId!
            self.navigationController?.pushViewController(sellForxVC, animated: true)
        }
        else
        {
            let sellForexloginVc : SellForexLoginViewController = SellForexLoginViewController(nibName: "ForexBaseViewController", bundle: nil)
            sellForexloginVc.sellForexBO = sellForexBO
            self.navigationController?.pushViewController(sellForexloginVc, animated: true)
        }
    }
    
    // MARK: tableview  delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return productArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell :  SellForexConfirmOrderCell = tableView.dequeueReusableCell(withIdentifier: "sellForexConfirmOrderCell", for: indexPath) as! SellForexConfirmOrderCell
        let product : ProductBO = productArray[indexPath.row]
        cell.labelProductNo.text = "\(indexPath.row + 1)"
        cell.labelProduct.text = product.productName as String
        cell.labelCurrency.text = "\(product.fxAmount) \(product.currencyCode)"
        cell.labelINR.text = "\(product.getINRAmount()) INR"
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    // MARK: Quote request
    
    func getQuoteForSell()
    {
        LoadingIndicatorView.show();
        
        let arrayForProductCollection : NSMutableArray = NSMutableArray()
        
        for productObject in productArray
        {
            
            let dictForProduct : NSMutableDictionary = NSMutableDictionary()
            
            dictForProduct.setValue(productObject.productName, forKey:"productName")
            dictForProduct.setValue(NSNumber.init(value: productObject.productID!), forKey:"productId")
            dictForProduct.setValue(NSNumber.init(value: productObject.currencyID!), forKey:"currencyId")
            dictForProduct.setValue(productObject.currencyCode, forKey:"currencyCode")
            dictForProduct.setValue(productObject.currency, forKey:"currencyName")
            dictForProduct.setValue(productObject.isNostroAcc, forKey:"nostroAvailable")
            dictForProduct.setValue("Sell", forKey:"moduleName")
            dictForProduct.setValue(NSNumber.init(value: productObject.roe).decimalValue, forKey:"roe")
            dictForProduct.setValue(NSNumber.init(value: productObject.fxAmount), forKey:"amount")
            dictForProduct.setValue(NSNumber.init(value: productObject.inrAmount), forKey:"equivalentInr")
            
            arrayForProductCollection .add(dictForProduct)
        }
        
        let dictForTraveller : NSMutableDictionary  = NSMutableDictionary()
        
        dictForTraveller .setValue(arrayForProductCollection, forKey: "tcilForexQuoteTravellerProductCollection")
        dictForTraveller .setValue(NSNumber.init(value: 1), forKey: "travellerNo")
        dictForTraveller .setValue(NSNumber.init(value: productArray.count), forKey: "travellerTotalProducts")
        
        
        let arrayForTraveller = NSArray.init(objects: dictForTraveller)
        
        let dictOfJson : NSMutableDictionary = NSMutableDictionary()
        
        dictOfJson .setValue(sellForexBO.mobileNumber, forKey: "bookerMobileNo")
        dictOfJson .setValue("test@test.com", forKey: "bookerEmailId")
        dictOfJson .setValue("", forKey: "dateOfTravel")
        dictOfJson .setValue(NSNumber.init(value: 2), forKey: "moduleId")
        dictOfJson .setValue(NSNumber.init(value: productArray.count), forKey: "totalNoOfProducts")
        dictOfJson .setValue(NSNumber.init(value: 1), forKey: "totalNoOfTraveller")
        dictOfJson .setValue(arrayForTraveller, forKey: "tcilForexQuoteTravellerCollection")
        dictOfJson .setValue(sellForexBO.gstStateCode, forKey: "gstState")
        dictOfJson .setValue(sellForexBO.branchCode, forKey: "gstBranch")
        dictOfJson .setValue(sellForexBO.gstBranchCode, forKey: "gstBranchCode")
        dictOfJson .setValue(sellForexBO.isUnionTerritoryState, forKey: "isStateUt")
        dictOfJson .setValue(sellForexBO.isUnionTerritoryBranch, forKey: "isBranchUt")
        
        let pathParameter  : NSString = "/tcForexRS/quote/sell/save"
        
        let jsonData: NSData = try!JSONSerialization.data(withJSONObject: dictOfJson, options: JSONSerialization.WritingOptions.prettyPrinted )as NSData
        
        print(NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue)! as String)
        
        ForexCommunicationManager.sharedInstance.execTaskWithAlamofire(pathParam: pathParameter, queryParam: "", requestType: "post", jsonDict: dictOfJson) { (status, response) in
            DispatchQueue.main.async {
                LoadingIndicatorView.hide()
                
                if(response != nil)
                {
                    
                    
                  
                    //  let _ : NSArray = response as! NSArray
                /*
                    let forexQueostTouple = ForexAppUtility.updateForexQuote(quotesResponse: response as! Dictionary<String,Any>)
                    
                    if forexQueostTouple.responseQuestsDict != nil
                    {
                        self.responseDict =  forexQueostTouple.responseQuestsDict as! NSDictionary //response as! NSDictionary
                        
                          self.sellForexBO.quoteResponseDict = self.responseDict
                        
                        self.labelTaxCalculationAmount.text  = "\(self.responseDict.object(forKey: "totalPrice") as! NSInteger) INR"
                        self.sellForexBO.totalAmountWithTax = "\( self.responseDict.object(forKey: "totalPrice") as! NSInteger) INR"
                        
                        if let taxType:String = forexQueostTouple.taxType
                        {
                            if taxType == TaxType.CSGST_SGST.rawValue
                            {
                                self.labelCGSTAmount.text = "\(forexQueostTouple.totalCGST ?? 0.0) INR"
                                
                                self.labelSGSTAmount.text = "\(forexQueostTouple.totalSGST ?? 0.0) INR"
                                
                                self.heightConstraintOfSGSTView.constant = 40
                                self.heightConstraintOfCGSTView.constant = 40
                                
                            }else if taxType == TaxType.IGST.rawValue{
                                
                                self.labelCGSTAmount.text = "\(forexQueostTouple.totlaIGST ?? 0.0) INR"
                                self.labelCGST.text = "IGST"
                                
                                self.heightConstraintOfSGSTView.constant = 0
                                self.heightConstraintOfCGSTView.constant = 40
                            }
                        }
                    }
                    */
                    
                    self.responseDict = response as! NSDictionary
                    self.sellForexBO.quoteResponseDict = self.responseDict
                    self.labelTaxCalculationAmount.text = "\( self.responseDict.object(forKey: "totalPrice") as! NSInteger) INR"
                    self.sellForexBO.totalAmountWithTax = "\( self.responseDict.object(forKey: "totalPrice") as! NSInteger) INR"
                    
                    let tcilForexQuoteTravellerCollection : NSArray = self.responseDict.object(forKey: "tcilForexQuoteTravellerCollection") as! NSArray
                    let tax : NSString = "\( self.responseDict.object(forKey: "totalServiceTax") as! NSInteger) INR" as NSString
                    
                    let forexTAXTuple = ForexAppUtility.getCGST_IGST_Total(quoteDict: self.responseDict)
                    
                    if forexTAXTuple != nil
                    {
                        if let taxType:String = forexTAXTuple.taxType
                        {
                            if taxType == TaxType.CSGST_SGST.rawValue
                            {
                                self.labelCGSTAmount.text = "\(forexTAXTuple.totalCGST ?? 0.0) INR"
                                
                                self.labelSGSTAmount.text = "\(forexTAXTuple.totalSGST ?? 0.0) INR"
                                
                                self.heightConstraintOfSGSTView.constant = 40
                                self.heightConstraintOfCGSTView.constant = 40
                                
                            }else if taxType == TaxType.IGST.rawValue{
                                
                                self.labelCGSTAmount.text = "\(forexTAXTuple.totlaIGST ?? 0.0) INR"
                                self.labelCGST.text = "IGST"
                                
                                self.heightConstraintOfSGSTView.constant = 0
                                self.heightConstraintOfCGSTView.constant = 40
                            }
                        }
                        
                    }
                    
                   
                    
                    
                 /*   if tcilForexQuoteTravellerCollection.count > 0
                    {
                        var totalCGST:Decimal = 0.0
                        
                        var totalSGST:Decimal = 0.0
                        
                        var taxType = ""
                        
                        var totlaIGST:Decimal = 0.0
                        
                        for index in 0..<tcilForexQuoteTravellerCollection.count
                        {
                            let travellerDict : NSDictionary =  tcilForexQuoteTravellerCollection[index] as! NSDictionary
                            let travellerServiceTaxDesc : String   = travellerDict.object(forKey: "travellerServiceTaxDesc") as! String
                            
                            if travellerServiceTaxDesc.contains("IGST")
                            {
                                var igst : String = travellerServiceTaxDesc.substring(from: travellerServiceTaxDesc.index(of: "=")!)
                                igst.removeFirst()
                                
                                let igstDouble:Double = Double(igst) ?? 0.0
                                    
                                let igstNSNumber:NSNumber = NSNumber(value: igstDouble)
                                    
                                totlaIGST = totlaIGST + igstNSNumber.decimalValue
                                
                                
                                 taxType = TaxType.IGST.rawValue
                                
                               
                            }
                            if travellerServiceTaxDesc.contains("CGST") || travellerServiceTaxDesc.contains("SGST")
                            {
                                let array : NSArray = travellerServiceTaxDesc.split(separator: ",") as NSArray
                                var cgst : String = array[0] as! String
                                cgst = cgst.substring(from: cgst.index(of: "=")!);
                                cgst.removeFirst()
                                
                                let cgstDouble:Double = Double(cgst) ?? 0.0
                                
                                let cgstNSNumber:NSNumber =  NSNumber(value: cgstDouble )
                                totalCGST = totalCGST + cgstNSNumber.decimalValue
                                
                                
                                
                              
                                var sgst : String = array[1] as! String
                                sgst = sgst.substring(from: sgst.index(of: "=")!);
                                sgst.removeFirst()
                                
                                let sgstDouble:Double = Double(sgst) ?? 0.0
                                
                                let sgstNSNumber:NSNumber =  NSNumber(value: sgstDouble )
                                totalSGST = totalSGST + sgstNSNumber.decimalValue
                                
                                 taxType = TaxType.CSGST_SGST.rawValue
                                
                                
                            }
                        }
                        
                       
                        
                        if taxType == TaxType.CSGST_SGST.rawValue
                        {
                            self.labelSGSTAmount.text = "\(totalSGST) INR"
                            self.heightConstraintOfSGSTView.constant = 40
                            self.heightConstraintOfCGSTView.constant = 40
                            
                              self.labelCGSTAmount.text = "\(totalCGST) INR"
                            
                        }else if taxType == TaxType.IGST.rawValue{
                            
                            self.labelCGSTAmount.text = "\(totlaIGST) INR"
                            self.labelCGST.text = "IGST"
                            self.heightConstraintOfSGSTView.constant = 0
                            self.heightConstraintOfCGSTView.constant = 40
                        }
                        
                    }*/
                }
                else
                {
                    self.showAlert(message: "Some error has occured")
                }
            }
            
        }
    }
    
    
    
}



/*
 // MARK: - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
 // Get the new view controller using segue.destinationViewController.
 // Pass the selected object to the new view controller.
 }
 */


