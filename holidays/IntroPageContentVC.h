//
//  IntroPageContentVC.h
//  holiday
//
//  Created by Pushpendra Singh on 04/08/15.
//  Copyright (c) 2015 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Thomas_Cook_Holidays-Swift.h"

@interface IntroPageContentVC : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *imgInfoContent;

@property (weak,nonatomic) NSString *imageName;
@property NSUInteger pageIndex;

@end
