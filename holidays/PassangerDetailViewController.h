//
//  PassangerDetailViewController.h
//  holidays
//
//  Created by ketan on 26/10/15.
//  Copyright © 2015 Mobicule. All rights reserved.
//

#import "BaseViewController.h"
#import "LoadingView.h"
#import "HolidayPackageDetail.h"
#import "Holiday.h"
#import "PassangerDetailCell.h"
#import "TravellerInformationModel.h"
#import "AdressPopUpVC.h"
#import "UIViewController+MJPopupViewController.h"
#import "RoomsDataModel.h"
#import "CustomIOSAlertView.h"

@interface PassangerDetailViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,AddressVCSubmit,CustomIOSAlertViewDelegate>
{
    LoadingView *activityLoadingView;
}
@property (strong, nonatomic) IBOutlet UIView *passangerDetailsView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewForRoomSelection;
@property (strong , nonatomic) NSArray *roomRecordArray;
@property (weak, nonatomic) IBOutlet UITableView *tableViewForPassangerDetail;
@property (strong , nonatomic) NSArray *travellerArrayForCalculation;
- (IBAction)onProceedButtonClicked:(id)sender;
@property (strong ,nonatomic) HolidayPackageDetail *packageDetail;
- (IBAction)onAdressButtonClicked:(id)sender;
@property (strong ,nonatomic) NSDictionary *DictJourneyDate;
@property (strong,nonatomic) NSString *hubName;
@property int accomType;
@property (nonatomic,strong) NSString *bookingAmountString;
@property (nonatomic,strong) NSString *totalAmountString;
@property (weak, nonatomic) IBOutlet UIButton *proceedButton;
- (IBAction)onTermsAndConditionClicked:(id)sender;
- (IBAction)onCallButtonClicked:(id)sender;
@property (nonatomic,strong) NSArray *currencyArray;
@property (nonatomic,strong) NSDictionary *gstTaxDetails;
@property (nonatomic,strong) NSString *stateCode;
@property (strong,nonatomic) NSDictionary *selectedStateDict;
@property (strong,nonatomic) NSDictionary *dictForCalculation;
@property (strong,nonatomic) NSString *amountType;


@end
