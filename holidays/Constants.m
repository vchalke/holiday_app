#import "Constants.h"

// Production URL old
//NSString * const serverUrl = @"http://220.226.201.140:8080/holidays-server-prod/InboundGateway";

//prod new url
//NSString * const serverUrl = @"http://103.233.79.46:8080/holidays-server-prod/InboundGateway";

// UAT url
//NSString * const serverUrl   = @"http://220.226.201.140:8080/thomascook-holidays-server/InboundGateway";

//live url
//NSString * const serverUrl  = @"http://103.233.79.46:8080/holidays-server-prod/InboundGateway";

//live url with domain name

//NSString * const serverUrl  = @"https://mb2c.thomascook.in/holidays-server-prod/InboundGateway";

//test for login

//NSString * const serverUrl  = @"http://220.226.201.140:8080/thomascook-holidays-server/InboundGateway";






NSString *const kInternationalHoliday    = @"International Holidays";
NSString *const kIndianHoliday           = @"India Holidays";
NSString *const kServerResponseKeyStatus = @"status";
NSString *const kServerResponseKeyData   = @"data";
NSString *const kStatusSuccess           = @"success";
NSString *const kStatusFailure           = @"failure";


NSString *const kBudgetKey         = @"budget";
NSString *const kNightToSpendKey   = @"duration";
NSString *const kDestinationKey    = @"destination";
NSString *const kHolidays          = @"holidayList";
NSString *const kFilters           = @"filters";
NSString *const kDurationList      = @"durationList";


NSString *const kFitlerMaxPrice    = @"maxPrice";
NSString *const kFilterMinPrice    = @"minPrice";

NSString *const kDataDidFilteredNotification = @"dataDidFiltered";
NSString *const kMessageNoInternet = @"Please check your internet connection.";
NSString *const kMessageNoInternetTryAfterSometime = @"No Network connection please try after sometime";
NSString *const kMessageSomeErrorTryAfterSometime = @"Some error occured. Please try after sometime.";

NSString *const kTravellerTypeChild = @"childTraveller";
NSString *const kTravellerTypeAdult = @"adultTraveller";
NSString *const kTravellerTypeInfant = @"infantTraveller";

NSString *const kTravellerRoomTypeSR = @"sr";
NSString *const kTravellerRoomTypeTS = @"ts";
NSString *const kTravellerRoomTypeDR = @"dr";
NSString *const kTravellerRoomTypeTR = @"tr";
NSString *const kTravellerRoomTypeCWB = @"cwb";
NSString *const kTravellerRoomTypeCNB = @"cnb";
NSString *const kTravellerRoomTypeInfant = @"inf";
NSString *const kCurrency_japanese_yen = @"jpy";
NSString *const kCurrency_thai_baht = @"thb";

NSString *const kuserDefaultUserId = @"userDefaultUserId";
NSString *const kuserDefaultPassword = @"userDefaultPassword";
NSString *const kuserDefaultRequestId = @"userDefaultRequestId";
NSString *const kuserDefaultTokenId = @"userDefaultTokenId";
NSString *const kuserDefaultTC_MobileNumber = @"userDefaultTC-LoginMobileNumber";
NSString *const kLoginUserDetails = @"userDetail";
NSString *const kUserFirstName = @"userFirstName";
NSString *const kUserImageString = @"userImgUrlString";
NSString *const kUserDeviceUniqueID = @"uniqueId";
NSString *const kCurrentTransactionId = @"currentTransactionId" ;

NSString *const kpackageTypeFITInternational = @"packageFITInternational";
NSString *const kpackageTypeGITInternational = @"packageGITInternational";
NSString *const kpackageTypeFITDomestic = @"packageFITDomestic";
NSString *const kpackageTypeGITDomestic = @"packageGITDomestic";
NSString *const kProfile    = @"My Profile";
NSString *const kOrderSummary=@"Order Summary";
NSString *const kMyFavourite=@"My Favourites";

//Login Session ANd Token ID
NSString *const kLoginRequestID = @"loginRequestID";
NSString *const kLoginTokenID = @"loginTokenID";



//Login Status Constant
NSString *const kLoginStatus =@"LoginStatus";
NSString *const kLoginEmailId =@"LoginEmailId";
NSString *const kLoginPasswords =@"LoginPassword";
NSString *const kPhoneNo = @"PhoneNo";
NSString *const kLoginSuccess=@"YES";
NSString *const kLoginFailed=@"NO";
NSString *const kVersionUpgradeMessage = @"forcefully upgradation required";
NSString *const kSignInWithOtp =@"signInWithOtp";
NSString *const kSignInWithPassword =@"signIn";
NSString *const kSendOtp =@"sendOtp";

NSString *const kEnterEmailAddress = @"EnterEmailAddress" ;
NSString *const kEnterPassword = @"EnterPassword" ;
NSString *const kEnterConfirmPassword = @"EnterConfirmPassword" ;
NSString *const kEnterMobileNumber = @"EnterMobileNumber" ;

//CertificateConstant
NSString *const kSSLCertificates = @"thomascookIn.cer,SSLServicesThomascookUAT.cer,SSLServicesthomascook.cer,LTsotcin.cer";


//FireBase logEventWithName
NSString *const kInternational_Holidays = @"International_Holidays_Details_Exclusion_PDP";
NSString *const kIndian_Holidays        = @"Indian_Holidays_Details_Exclusion_PDP";


//Astra URLS


//https://services-uatastra.thomascook.in/


/*
go live

Services : https://services.thomascook.in/

For pg page : www.thomascook.in

For preprod

Services : https://services.thomascook.in/

For pg page : pprod.thomascook.in
 
uat
 
Services : https://services-uatastra.thomascook.in/
 
For pg page : https://uatastra.thomascook.in/paymentGateway.html
 :
 // live payment URL o holiday :https://www.thomascook.in/paymentGateway.html?tid=KagBD6QHz7n%2FnMaydsNZfQ%3D%3D
 Menu Uat urls
 International Holidays
 In App
 India Holidays
 In App
 Forex
 In App
 Flights
 https://uatastra.thomascook.in/flights
 Hotels
 https://uatastra.thomascook.in/hotels
 Visa
 https://uatastra.thomascook.in/visa
 Insurance
 https://uatastra.thomascook.in/travel-insurance
 Menu Live urls
 International Holidays
 In App
 India Holidays
 In App
 Forex
 In App
 Flights
 https://www.thomascook.in/flights
 Hotels
 https://www.thomascook.in/hotels
 Visa
 https://www.thomascook.in/visa
 Insurance
 https://www.thomascook.in/travel-insurance
 
 
 
 Fx URLs:
 UAT: https://uatastra.thomascook.in/foreign-exchange
 LIVE: https://www.thomascook.in/foreign-exchange
 
 Offers URLs:
 UAT: https://uatastra.thomascook.in/offers-and-deals
 LIVE: https://www.thomascook.in/offers-and-deals
 

 Image :- 
 UAT :- https://resources-uatastra.thomascook.in
 
 PProd :- https://resources.thomascook.in
 */
//

//---UAT URLs
/*
//NSString * const serverUrl = @"http://220.226.201.140:8080/thomascook-holidays-ASTRA-server/InboundGateway"; // mobicule middelware
//NSString * const serverUrl = @"https://services-uatastra.thomascook.in/tcStaticPages/bannerapiservice/getbannerdata/iphone";

//NSString * const serverUrl = @"http://180.179.69.73:8080/thomascook-holidays-ASTRA-server/InboundGateway";
NSString * const serverUrl = @"http://52.172.189.249:8080/thomascook-holidays-ASTRA-server/InboundGateway"; // Added_ON_24_AUG_2020
//NSString * const serverUrl = @"https://tm.thomascook.in/thomascook-holidays-ASTRA-server/InboundGateway";// NEW DUE TO MIGRATION ON 15_July_2020 // Uncomment_ON_24_AUG_2020
NSString *const kbaseURLForAstra = @"https://services-uatastra.thomascook.in/";
// NSString *const kbaseURLForAstra = @"http://14.141.26.109/";// Added_ON_18Sept20
 NSString *const kpgURLForPayment = @"https://uatastra.thomascook.in/paymentGateway.html?tid=";
 
 NSString *const kpgURLForConfirmation = @"https://uatastra.thomascook.in";
 
 NSString *const kUrlForCreateLead = @"https://thomascookindia--tst1.custhelp.com/cgi-bin/thomascookindia.cfg/php/custom/mobile_iosapp_forex_lead.php?";

NSString *const KUrlHolidayCreateLead = @"https://thomascookindia--tst1.custhelp.com/cgi-bin/thomascookindia.cfg/php/custom/mobile_app_lead_holiday_ios_app_leads.php";

NSString *const kUrlForFlights  = @"https://uatastra.thomascook.in/flights";

NSString *const kUrlForHotels  = @"https://uatastra.thomascook.in/hotels";

NSString *const kUrlForVisa  = @"https://uatastra.thomascook.in/visa";

NSString *const kUrlForInsurance  = @"https://uatastra.thomascook.in/travel-insurance";

NSString *const kUrlForForex = @"https://uatastra.thomascook.in/foreign-exchange";

NSString *const kpgURLForDeals = @"https://uatastra.thomascook.in/offers-and-deals";

//NSString *const kUrlForImage  = @"https://resources.thomascook.in";
NSString *const kUrlForImage  = @"https://resources-uatastra.thomascook.in";

NSString *const kForexBuyURL = @"https://uatastra.thomascook.in//foreign-exchange/buy-forex-online?utm_source=HolidaysApp&utm_medium=referral&utm_campaign=HolidaysAppSept2019";

NSString *const kForexSellURL = @"https://uatastra.thomascook.in/foreign-exchange/sell-forex-online?utm_source=HolidaysApp&utm_medium=referral&utm_campaign=HolidaysAppSept2019";

NSString *const kForexMoneyTransferURL = @"https://uatastra.thomascook.in/foreign-exchange/online-money-transfer?utm_source=HolidaysApp&utm_medium=referral&utm_campaign=HolidaysAppSept2019";

NSString *const kForexReloadForexURL = @"https://uatastra.thomascook.in/foreign-exchange/reload-forex-card?utm_source=HolidaysApp&utm_medium=referral&utm_campaign=HolidaysAppSept2019";

NSString *const kForexCurrencyConverterURL = @"https://uatastra.thomascook.in/foreign-exchange/currency-converter?utm_source=HolidaysApp&utm_medium=referral&utm_campaign=HolidaysAppSept2019";

NSString *const kForexLiveRatesyURL = @"https://uatastra.thomascook.in/foreign-exchange/forex-rate-card?utm_source=HolidaysApp&utm_medium=referral&utm_campaign=HolidaysAppSept2019";

NSString *const kForexKnowYourCardBalanceURL = @"https://www.thomascooktravelcards.com/tccustomer/loginuser";

NSString *const kVersionUpgradeAppstoreURL = @"itms-apps://itunes.apple.com/us/app/apple-store/id1111576845?ls=1&mt=8";//@"itms://itunes.apple.com/us/app/thomas-cook-holiday-packages/id1111576845?ls=1&mt=8";
//itms://itunes.apple.com/us/app/apple-store/id375380948?mt=8


//https://itunes.apple.com/in/app/thomas-cook-holiday-packages/id1111576845?ls=1&mt=8

//NSString *const kgetbannerDataURL = @"tcStaticPages/bannerapiservice/getbannerdata/iphone";

//NSString *const kgetbannerServiceURL = @"tcHolidayRS/bannerapiservice/getbannerdata/all";

 NSString *const kTravelBlogURL = @"http://blog.thomascook.in/wp-json/wp/v2/posts";
// NSString *const kTravelBlogURL = @"https://blog.sotc.in/wp-json/wp/v2/posts"; // Travel Blog From SOTC
 
NSString *const kGetCustInfoURL = @"TcilMyAccount/profile/getCustInfo";
NSString *const getByPackageId = @"tcHolidayRS/Offers/getByPackageId";
///--- end of UAT
*/
//PreProd

//NSString *const kbaseURLForAstra = @"https://services.thomascook.in/";
//
//NSString *const kpgURLForPayment = @"https://www.thomascook.in/paymentGateway.html?tid=";
//
 //NSString *const kpgURLForConfirmation = @"https://uatastra.thomascook.in";

//NSString *const kUrlForCreateLead = @"https://thomascookindia.custhelp.com/cgi-bin/thomascookindia.cfg/php/custom/mobile_iosapp_forex_lead.php?";
//
//NSString *const kUrlForFlights  = @"https://www.thomascook.in/flights";
//
//NSString *const kUrlForHotels  = @"https://www.thomascook.in/hotels";
//
//NSString *const kUrlForVisa  = @"https://www.thomascook.in/visa";
//
//NSString *const kUrlForInsurance  = @"https://www.thomascook.in/travel-insurance";
//
//NSString *const kUrlForForex = @"https://www.thomascook.in/foreign-exchange";
//
//NSString *const kpgURLForDeals = @"https://www.thomascook.in/offers-and-deals";
//
//NSString *const kUrlForImage  = @"https://resources.thomascook.in";
//
//NSString *const kForexBuyURL = @"https://pprod.thomascook.in/foreign-exchange/buy-forex-online";
//
//NSString *const kForexSellURL = @"https://pprod.thomascook.in/foreign-exchange/sell-forex-online";
//
//NSString *const kForexMoneyTransferURL = @"https://pprod.thomascook.in/foreign-exchange/online-remittance";

//NSString *const kForexKnowYourCardBalanceURL = @"https://www.thomascooktravelcards.com/tccustomer/loginuser";
//
//NSString *const kForexReloadForexURL = @"https://pprod.thomascook.in/foreign-exchange/reload-borderless-prepaid-card";
//
//NSString *const kForexCurrencyConverterURL = @"https://pprod.thomascook.in/foreign-exchange/currency-converter";
//
//NSString *const kForexLiveRatesyURL = @"https://pprod.thomascook.in/foreign-exchange/buy-forex-online";


//---LIVE URLs


 NSString * const serverUrl  = @"http://103.233.79.46:8080/thomascook-holidays-ASTRA-server/InboundGateway"; // mobicule middelware
// NSString * const serverUrl  = @"https://103.233.79.46:8080/thomascook-holidays-ASTRA-server/InboundGateway"; // mobicule middelware add S at last 27July20
 NSString *const kbaseURLForAstra = @"https://services.thomascook.in/";

//NSString *const kpgURLForPayment = @"https://www.thomascook.in";//paymentGateway.html?tid

NSString *const kpgURLForPayment = @"https://www.thomascook.in/paymentGateway.html?tid=";

NSString *const kpgURLForConfirmation = @"https://www.thomascook.in";

NSString *const KUrlHolidayCreateLead = @"https://thomascookindia.custhelp.com/cgi-bin/thomascookindia.cfg/php/custom/mobile_app_lead_holiday_ios_app_leads.php";
 
NSString *const kUrlForCreateLead = @"https://thomascookindia.custhelp.com/cgi-bin/thomascookindia.cfg/php/custom/mobile_iosapp_forex_lead.php?";
    
//@"https://www.thomascook.in";

NSString *const kUrlForFlights  = @"https://www.thomascook.in/flights";

NSString *const kUrlForHotels  = @"https://www.thomascook.in/hotels";

NSString *const kUrlForVisa  = @"https://www.thomascook.in/visa";

NSString *const kUrlForInsurance  = @"https://www.thomascook.in/travel-insurance";

NSString *const kUrlForForex = @"https://www.thomascook.in/foreign-exchange";

NSString *const kpgURLForDeals = @"https://www.thomascook.in/offers-and-deals";

NSString *const kUrlForImage  = @"https://resources.thomascook.in";

NSString *const kForexBuyURL = @"https://www.thomascook.in/foreign-exchange/buy-forex-online?utm_source=HolidaysApp&utm_medium=referral&utm_campaign=HolidaysAppSept2019";

NSString *const kForexSellURL = @"https://www.thomascook.in/foreign-exchange/sell-forex-online?utm_source=HolidaysApp&utm_medium=referral&utm_campaign=HolidaysAppSept2019";

NSString *const kForexMoneyTransferURL = @"https://www.thomascook.in/foreign-exchange/online-money-transfer?utm_source=HolidaysApp&utm_medium=referral&utm_campaign=HolidaysAppSept2019";

NSString *const kForexKnowYourCardBalanceURL = @"https://www.thomascooktravelcards.com/tccustomer/loginuser";

NSString *const kForexReloadForexURL = @"https://www.thomascook.in/foreign-exchange/reload-forex-card?utm_source=HolidaysApp&utm_medium=referral&utm_campaign=HolidaysAppSept2019";

NSString *const kForexCurrencyConverterURL = @"https://www.thomascook.in/foreign-exchange/currency-converter?utm_source=HolidaysApp&utm_medium=referral&utm_campaign=HolidaysAppSept2019";

NSString *const kForexLiveRatesyURL = @"https://www.thomascook.in/foreign-exchange/forex-rate-card?utm_source=HolidaysApp&utm_medium=referral&utm_campaign=HolidaysAppSept2019";
  
NSString *const kVersionUpgradeAppstoreURL = @"itms-apps://itunes.apple.com/us/app/apple-store/id1111576845?ls=1&mt=8";

// NSString *const kgetbannerDataURL = @"tcStaticPages/bannerapiservice/getbannerdata/iphone";

// NSString *const kgetbannerServiceURL = @"tcHolidayRS/bannerapiservice/getbannerdata/all";

 NSString *const kTravelBlogURL = @"http:blog.thomascook.in/wp-json/wp/v2/posts";
//  NSString *const kTravelBlogURL = @"https://blog.sotc.in/wp-json/wp/v2/posts"; // Travel Blog From SOTC
 
NSString *const kGetCustInfoURL = @"TcilMyAccount/profile/getCustInfo";;

/// --- END OF LIVE URLS  ---
/*
 NSString *const kForexBuyURL = @"https://www.thomascook.in/foreign-exchange/buy-forex-online";
 
 NSString *const kForexSellURL = @"https://www.thomascook.in/foreign-exchange/sell-forex-online";
 
 NSString *const kForexMoneyTransferURL = @"https://www.thomascook.in/foreign-exchange/online-remittance";
 
 NSString *const kForexKnowYourCardBalanceURL = @"https://www.thomascooktravelcards.com/tccustomer/loginuser";
 
 NSString *const kForexReloadForexURL = @"https://www.thomascook.in/foreign-exchange/reload-borderless-prepaid-card";
 
 NSString *const kForexCurrencyConverterURL = @"https://www.thomascook.in/foreign-exchange/currency-converter";
 
 NSString *const kForexLiveRatesyURL = @"https://www.thomascook.in/foreign-exchange/buy-forex-online";
 
*/



NSString *const krequestTypeQueryParam = @"QueryParam";
NSString *const krequestTypePathParam = @"PathParam";
NSString *const krequestTypeJsonParam = @"JsonParam";


NSString *const kAstraUrlNewToken = @"tcCommonRS/extnrt/getNewRequestToken";  // for new token

NSString *const kAstraUrlSearchPackage = @"tcHolidayRS/autosuggest"; // 3 keyword search

NSString *const kAstraUrlPackageFilterSearch = @"tcHolidayRS/srp/packages"; // Find Holiday

NSString *const kAstraUrlPackageDetail = @"tcHolidayRS/packagedetails/pdp";

NSString *const kAstraUrlFareCalender = @"tcHolidayRS/pdp.compare/fareCalender";

NSString *const kAstraUrlPreConfirmationBooking = @"tcHolidayRS/preconfirmation";//2

NSString *const kAstraUrlGstStates = @"tcCommonRS/tcilGstService/getGstStateCodes";

NSString *const kAstraUrlPricing = @"tcHolidayRS/pricing";

NSString *const kAstraUrlPromocode = @"tcHolidayRS/quotation/promocode";//1


NSString *const kAstraUrlBooking =@"tcHolidayRS/holidayBooking";

NSString *const kAstraUrlLoginCheck = @"TcilMyAccount/login/user";

NSString *const kAstraUrlSendOtp = @"TcilMyAccount/login/sendOTP";

NSString *const kAstraUrlForgotPassword = @"TcilMyAccount/login/forgetPassword";

NSString *const kAstraUrlSavePass = @"TcilMyAccount/login/savePass";

NSString *const kAstraVerifyCredentials = @"TcilMyAccount/login/verifyCrediential";

NSString *const kAstraSocialLogin = @"TcilMyAccount/login/socialLogin";

NSString *const kAstraCreateLogin = @"TcilMyAccount/login/create";

NSString *const KGetCurrencyList = @"tcHolidayRS/roe/getCurrencyList";


NSString *const KGetByPackageId = @"tcHolidayRS/Offers/getByPackageId/PKG002102";

NSString *const kGetbannerDataURL = @"tcStaticPages/bannerapiservice/getbannerdata/iphone";

NSString *const kGetbannerServiceURL = @"tcHolidayRS/bannerapiservice/getbannerdata/all";

NSString *const kAstraPdpPromocodeURL = @"tcHolidayRS/holidayBooking/pdpPromocode";

NSString *const kAstraTcHolidayRSConfirmation = @"tcHolidayRS/confirmation"; // /i8GRCGvqRzBJ8VPpYm%2B48g%3D%3D

//HolidayLandingConstant bannerLevelName
NSString *const kserviceTopBanners = @"Top Banners";
NSString *const kserviceWhatsNew = @"Whats new";
NSString *const kserviceLastMinuteDeals = @"Last Minute Deals";
NSString *const kserviceSummerHolidays = @"Staycations 2020";
NSString *const kserviceAdventureHoliday = @"Introducing New Adventure Holidays for youth";
NSString *const kserviceCategoriesHoliday = @"Explore Holidays by Categories";
NSString *const kserviceUpcomingEvents = @"Upcoming Events";
NSString *const kserviceCherryBlossomSpecial = @"Cherry Blossom Special";
NSString *const kservicePopularDestinations = @"Popular Destinations";

//HolidayLandingConstant bannerLevelType

//HolidayLandingConstant Level Name
NSString *const levelNumberOne = @"Level1";
NSString *const levelNumberTwo = @"Level2";
NSString *const levelNumberThree = @"Level3";
NSString *const levelNumberFour = @"Level4";
NSString *const levelNumberFive = @"Level5";
NSString *const levelNumberSix = @"Level6";
NSString *const levelNumberSeven = @"Level7";
NSString *const levelNumberEight = @"Level8";
NSString *const levelNumberNine = @"Level9";
NSString *const levelNumberTen = @"Level10";


//NSString *const kserviceTopBanners = @"Level 1";
//NSString *const kserviceWhatsNew = @"Level 1";
//NSString *const kserviceLastMinuteDeals = @"Level 3";
//NSString *const kserviceSummerHolidays = @"Level 1";
//NSString *const kserviceAdventureHoliday = @"Level 1";
//NSString *const kserviceCategoriesHoliday = @"Level 1";
//NSString *const kserviceUpcomingEvents = @"Level 1";
//NSString *const kserviceCherryBlossomSpecial = @"Level 1";
//NSString *const kservicePopularDestinations = @"Level 1";
