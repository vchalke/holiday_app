//
//  PackageDetailModel.h
//  holidays
//
//  Created by Kush_Tech on 04/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PackageDetailModel : NSObject
-(instancetype)initWithPackageDetailDict:(NSDictionary *)dictionary;
@property(nonatomic,strong)NSMutableArray *hotelCollectionArray;
@property(nonatomic,strong)NSMutableArray *itineraryCollectionArray;
@property(nonatomic,strong)NSMutableArray *mealCollectionArray;
@property(nonatomic,strong)NSMutableArray *transfersCollectionArray;
@property(nonatomic,strong)NSMutableArray *flightsCollectionArray;
@property(nonatomic,strong)NSMutableArray *visaCollectionArray;
@property(nonatomic,strong)NSMutableArray *sightseenCollectionArray;
@property(nonatomic,strong)NSMutableArray *includeexcludeCollectionArray;
@end

NS_ASSUME_NONNULL_END
