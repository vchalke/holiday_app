//
//  PSPageMenu.h
//  holidays
//
//  Created by Pushpendra Singh on 12/08/15.
//  Copyright (c) 2015 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PSPageMenu;

#pragma mark - Delegate functions
@protocol PSPageMenuDelegate <NSObject>

@optional
- (void)willMoveToPage:(UIViewController *)controller index:(NSInteger)index;
- (void)didMoveToPage:(UIViewController *)controller index:(NSInteger)index;
@end

#pragma mark - Delegate functions
@protocol PSDelegate <NSObject>

@optional
- (void)identifyMenuWithindex:(NSInteger)index;

@end

@interface MenuItemView : UIView

@property (nonatomic) UILabel *titleLabel;
@property (nonatomic) UIView *menuItemSeparator;

- (void)setUpMenuItemView:(CGFloat)menuItemWidth menuScrollViewHeight:(CGFloat)menuScrollViewHeight indicatorHeight:(CGFloat)indicatorHeight separatorPercentageHeight:(CGFloat)separatorPercentageHeight separatorWidth:(CGFloat)separatorWidth separatorRoundEdges:(BOOL)separatorRoundEdges menuItemSeparatorColor:(UIColor *)menuItemSeparatorColor;

- (void)setTitleText:(NSString *)text;

@end

@interface PSPageMenu : UIViewController <UIScrollViewDelegate, UIGestureRecognizerDelegate>

@property (nonatomic, strong) UIScrollView *menuScrollView;
@property (nonatomic, strong) UIScrollView *controllerScrollView;

@property (nonatomic, readonly) NSArray *controllerArray;
@property (nonatomic, readonly) NSArray *menuItems;
@property (nonatomic, readonly) NSArray *menuItemWidths;

@property (nonatomic) NSInteger currentPageIndex;
@property (nonatomic) NSInteger lastPageIndex;

@property (nonatomic) CGFloat menuHeight;
@property (nonatomic) CGFloat menuMargin;
@property (nonatomic) CGFloat menuItemWidth;
@property (nonatomic) CGFloat selectionIndicatorHeight;
@property (nonatomic) NSInteger scrollAnimationDurationOnMenuItemTap;

@property (nonatomic) UIColor *selectionIndicatorColor;
@property (nonatomic) UIColor *selectedMenuItemLabelColor;
@property (nonatomic) UIColor *unselectedMenuItemLabelColor;
@property (nonatomic) UIColor *scrollMenuBackgroundColor;
@property (nonatomic) UIColor *viewBackgroundColor;
@property (nonatomic) UIColor *bottomMenuHairlineColor;
@property (nonatomic) UIColor *menuItemSeparatorColor;

@property (nonatomic) UIFont *menuItemFont;
@property (nonatomic) CGFloat menuItemSeparatorPercentageHeight;
@property (nonatomic) CGFloat menuItemSeparatorWidth;
@property (nonatomic) BOOL menuItemSeparatorRoundEdges;

@property (nonatomic) BOOL addBottomMenuHairline;
@property (nonatomic) BOOL menuItemWidthBasedOnTitleTextWidth;
@property (nonatomic) BOOL useMenuLikeSegmentedControl;
@property (nonatomic) BOOL centerMenuItems;
@property (nonatomic) BOOL enableHorizontalBounce;
@property (nonatomic) BOOL hideTopMenuBar;

@property (nonatomic, weak) id <PSPageMenuDelegate> delegate;
@property (nonatomic, weak) id <PSDelegate> identifyDelegate;

- (void)addPageAtIndex:(NSInteger)index;
- (void)moveToPage:(NSInteger)index;

- (instancetype)initWithViewControllers:(NSArray *)viewControllers frame:(CGRect)frame options:(NSDictionary *)options;

extern NSString * const PSPageMenuOptionSelectionIndicatorHeight;
extern NSString * const PSPageMenuOptionMenuItemSeparatorWidth;
extern NSString * const PSPageMenuOptionScrollMenuBackgroundColor;
extern NSString * const PSPageMenuOptionViewBackgroundColor;
extern NSString * const PSPageMenuOptionBottomMenuHairlineColor;
extern NSString * const PSPageMenuOptionSelectionIndicatorColor;
extern NSString * const PSPageMenuOptionMenuItemSeparatorColor;
extern NSString * const PSPageMenuOptionMenuMargin;
extern NSString * const PSPageMenuOptionMenuHeight;
extern NSString * const PSPageMenuOptionSelectedMenuItemLabelColor;
extern NSString * const PSPageMenuOptionUnselectedMenuItemLabelColor;
extern NSString * const PSPageMenuOptionUseMenuLikeSegmentedControl;
extern NSString * const PSPageMenuOptionMenuItemSeparatorRoundEdges;
extern NSString * const PSPageMenuOptionMenuItemFont;
extern NSString * const PSPageMenuOptionMenuItemSeparatorPercentageHeight;
extern NSString * const PSPageMenuOptionMenuItemWidth;
extern NSString * const PSPageMenuOptionEnableHorizontalBounce;
extern NSString * const PSPageMenuOptionAddBottomMenuHairline;
extern NSString * const PSPageMenuOptionMenuItemWidthBasedOnTitleTextWidth;
extern NSString * const PSPageMenuOptionScrollAnimationDurationOnMenuItemTap;
extern NSString * const PSPageMenuOptionCenterMenuItems;
extern NSString * const PSPageMenuOptionHideTopMenuBar;

@end