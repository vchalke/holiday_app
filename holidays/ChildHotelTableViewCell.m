//
//  ChildHotelTableViewCell.m
//  holidays
//
//  Created by Kush_Tech on 03/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "ChildHotelTableViewCell.h"

@implementation ChildHotelTableViewCell


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(void)loadDataInChildFromDetailModel:(HolidayAccomodationObject*)detailModel withHideAddess:(BOOL)flag{
    NSLog(@"%@",detailModel.state);
    NSLog(@"%@",detailModel.hotelName);
    NSLog(@"%ld",detailModel.noOfNights);
    self.lbl_Title.text = [NSString stringWithFormat:@"%@  | ",detailModel.cityName];;
    self.lbl_subTitle.text =  [NSString stringWithFormat:@"%@ / Similar",detailModel.hotelName];
    self.lbl_numOfDays.text =  [NSString stringWithFormat:@"%ld Nights",detailModel.noOfNights];
    NSInteger starRating = detailModel.starRating;
    self.star_ratingView.spacing = 2.0;
    self.star_ratingView.value = starRating;
    self.star_ratingView.userInteractionEnabled = NO;
    NSLog(@"%ld",starRating);
    self.cnst_htofAddress.constant = flag ? 0 : 45;
    self.view_addres.hidden = flag;
    if (!flag) {
        self.lbl_address.text = detailModel.address;
    }
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


@end
