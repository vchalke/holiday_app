//
//  UIColor+HexToColor.h
//  holidays
//
//  Created by vaibhav on 08/09/15.
//  Copyright (c) 2015 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (HexToColor)

+ (UIColor *)colorWithR:(CGFloat)red G:(CGFloat)green B:(CGFloat)blue A:(CGFloat)alpha;

+ (UIColor *)colorWithHex:(UInt32)hex A:(CGFloat)alpha;

+ (UIColor *)colorFromHexString:(NSString *)hexString;
@end
