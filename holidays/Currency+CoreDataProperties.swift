//
//  Currency+CoreDataProperties.swift
//  holidays
//
//  Created by Saurav on 28/11/17.
//  Copyright © 2017 Mobicule. All rights reserved.
//

import Foundation
import CoreData


extension Currency {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Currency> {
        return NSFetchRequest<Currency>(entityName: "Currency")
    }

    @NSManaged public var currencyCode: String?
    @NSManaged public var currencyDate: String?
    @NSManaged public var currencyName: String?
    @NSManaged public var currencyRate: String?
    @NSManaged public var exchangeRateId: Int64

}
