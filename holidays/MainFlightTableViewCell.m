//
//  MainFlightTableViewCell.m
//  holidays
//
//  Created by Kush_Tech on 04/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "MainFlightTableViewCell.h"
#import "FlightsTableViewCell.h"
#import "WebUrlConstants.h"
@implementation MainFlightTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)loadFlightDataFromPackageModel:(PackageDetailModel*)packageModel holidayPkgModel:(HolidayPackageDetail*)holiPkgModel  withAray:(NSArray*)flightModelArr{
    packageDetailModel = packageModel;
    self.flightsdataArray = flightModelArr;
    if([self.flightsdataArray count]>0){
        flightsShowArray = [[NSMutableArray alloc] init];
        NSSortDescriptor *sortDescriptor;
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"position"
                                                   ascending:YES];
        NSArray *sortedArray = [flightModelArr sortedArrayUsingDescriptors:@[sortDescriptor]];
        flightsShowArray= [sortedArray mutableCopy];
        [self.table_views registerNib:[UINib nibWithNibName:@"FlightsTableViewCell" bundle:nil] forCellReuseIdentifier:@"FlightsTableViewCell"];
        self.table_views.delegate = self;
        self.table_views.dataSource = self;
        self.webVioews.hidden = YES;
    }else{
        self.table_views.hidden = YES;
        NSString *blankStr = (holiPkgModel.flightDefaultMsg.length > 0)? holiPkgModel.flightDefaultMsg : @"Return Economy class airfare as per the Itinenary";
        NSString *htmlStringObj = [NSString stringWithFormat:@"<span style=\"font-family: Lato-Regular; font-size: 16\">%@</span>", blankStr];
        [self.webVioews loadHTMLString:htmlStringObj baseURL:nil];
    }
}
- (IBAction)btn_ViewMorePress:(id)sender {
    [self.mainFlightDelaget showFlightWithSectionTitle:sFlights withPackageModel:packageDetailModel];
}
#pragma mark - UITableView DataSource Methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    return 1;
//    return [self.flightsdataArray count ];
    return [flightsShowArray count ];
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    FlightsTableViewCell* cell = (FlightsTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"FlightsTableViewCell"];
//    FlightsColectObject *flighObject = [[FlightsColectObject alloc]initWithFlightObjectDict:[self.flightsdataArray objectAtIndex:indexPath.row]];
    FlightsColectObject *flighObject = [[FlightsColectObject alloc]initWithFlightObjectDict:[flightsShowArray objectAtIndex:indexPath.row]];
    [cell setFilghtsdataSelected:flighObject];
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 80;
}

@end
