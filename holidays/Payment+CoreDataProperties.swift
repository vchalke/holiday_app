//
//  Payment+CoreDataProperties.swift
//  holidays
//
//  Created by Saurav on 28/11/17.
//  Copyright © 2017 Mobicule. All rights reserved.
//

import Foundation
import CoreData


extension Payment {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Payment> {
        return NSFetchRequest<Payment>(entityName: "Payment")
    }

    @NSManaged public var balancePending: String?
    @NSManaged public var bfNumber: String?
    @NSManaged public var currency: String?
    @NSManaged public var currency_bfNumber: String?
    @NSManaged public var totalReceivable: String?
    @NSManaged public var totalReceived: String?
    @NSManaged public var tourRelation: Tour?

}
