//
//  DateStringSingleton.h
//  holidays
//
//  Created by Kush_Tech on 24/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface DateStringSingleton : NSObject

typedef enum {
    failureInSeverError,
    failureError,
    intenertError,
    emptyData
} ErrorType;

extern NSString * _Nonnull const ErrorType_toString[];

+ (id)shared;
- (id)init;

-(NSString *)getDateTimeStringFormat:(NSString*)stringFormat DateValue:(NSDate *)dateValue;
-(NSDate *)getDateFromStringFormat:(NSString*)stringFormat strDateValue:(NSString *)dateValue;
-(NSDate *)getTodaysStartDate;
-(NSDate *)getTodaysLastDate;
-(NSDate *)getTodaysCurrentDate;
-(NSInteger )getCurrentYear;
-(NSInteger )getCurrentMonth;
-(NSInteger )getCurrentDay;
-(NSInteger )getCurrentHour;
-(NSInteger )getCurrenMinute;
-(NSInteger )getCurrentSecond;
-(NSInteger )getTotalDaysInMonthAtIndex:(NSInteger)num;
-(NSDate*)getMonthDateAtIndex:(NSInteger)num ofYearBack:(int)numOfYear;
//-(NSDate*)getStartDayOfMonth:(NSInteger)monthIndex;
-(NSDate*)getDayNumber:(NSInteger)dayIndex OfMonth:(NSInteger)monthIndex;
-(NSInteger)getMonthNumberFromDate:(NSDate*)date;
-(NSString*)getMonthNameAtIndex:(NSInteger)num withStrFormat:(NSString*)strFormat ofYearBack:(int)numOfYear;
@end

NS_ASSUME_NONNULL_END
