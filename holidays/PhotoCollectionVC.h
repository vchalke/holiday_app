//
//  PhotoCollectionVC.h
//  holidays
//
//  Created by Kush_Team on 14/07/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Holiday.h"
#import "HolidayPackageDetail.h"
#import "NewMasterVC.h"
NS_ASSUME_NONNULL_BEGIN

@interface PhotoCollectionVC : NewMasterVC<UITableViewDelegate,UITableViewDataSource>
@property (strong ,nonatomic) HolidayPackageDetail *packageDetailInPhotoGallery;
@property (weak, nonatomic) IBOutlet UITableView *tableViews;

@end

NS_ASSUME_NONNULL_END
