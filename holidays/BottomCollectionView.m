//
//  BottomCollectionView.m
//  holidays
//
//  Created by Kush_Tech on 24/02/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "BottomCollectionView.h"
#import "BottomCollectionViewCell.h"
@implementation BottomCollectionView

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
     self = [super initWithCoder:aDecoder];
     if(self) {
        [self loadNib];
    }
     return self;

}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void)loadNib{
    
    UIView *view = [[[NSBundle bundleForClass:[self class]]loadNibNamed:@"BottomCollectionView" owner:self options:nil] firstObject];
    
    view.frame = self.bounds;

    [self addSubview:view];
    
    [self.bottom_collectionView registerNib:[UINib nibWithNibName:@"BottomCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"BottomCollectionViewCell"];

    imageArray = @[@"home",@"booking",@"myAccount",@"notification",@"threeDots"];

    titleArray = @[@"HOME",@"BOOKINGS",@"MY ACCOUNT",@"NOTIFICATION",@"INSURANCE"];

    self.bottom_collectionView.delegate = self;
    self.bottom_collectionView.dataSource = self;

//    [self layoutIfNeeded];
//    [self setNeedsLayout];

}
#pragma mark - collectionview datasource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return [imageArray count];
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    BottomCollectionViewCell *selectedCell = (BottomCollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"BottomCollectionViewCell" forIndexPath:indexPath];
    selectedCell.img_view.image = [UIImage imageNamed:imageArray[indexPath.row]];
    selectedCell.lbl_title.text = titleArray[indexPath.row];
    
    return selectedCell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {

    return CGSizeMake(self.bottom_collectionView.bounds.size.width*0.2,self.bottom_collectionView.bounds.size.height);
    
}

@end
