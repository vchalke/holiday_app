//
//  NotificationViewController.m
//  holidays
//
//  Created by Saurav Kumar on 23/11/15.
//  Copyright © 2015 Mobicule. All rights reserved.
//

#import "NotificationViewController.h"
#import "NotificationViewTableViewCell.h"
#import <CoreData/CoreData.h>
#import "AppDelegate.h"
#import "CoreUtility.h"
#import "Holiday.h"
#import "Constants.h"
#import "LoadingView.h"
#import "TabMenuVC.h"
#import "NetCoreAnalyticsVC.h"
#import "SDWebImageDownloader.h"
#import "SDImageCache.h"
#import "WebViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>
#import "Thomas_Cook_Holidays-Swift.h"
#import "LandingPageViewController.h"


#define kEntity @"holidays"
#define kType   @"webservice"
#define kAction  @"search"


#define KEYCHAIN_SERVICE @"Holiday_App_Keychain"
#define KEYCHAIN_ACCOUNT @"com.thomasCookHoliday.app"
#define DATE_FORMAT_24_HR @"dd-MMM-yyyy HH:mm:ss"
#define DATE_FORMAT_12_HR @"dd-MMM-yyyy hh:mm:ss a"


@interface NotificationViewController () <ManageHolidayPopUpVCDelegate>
{
    NSMutableArray * notificationArray;
    LoadingView *activityIndicator;
    LoadingView *activityLoadingView;
    NSMutableDictionary *payLoadForViewDetails;
    HolidayPackageDetail *packageDetail;
    UIImage *imagecell;
}


@property (strong, nonatomic) IBOutlet UITableViewCell * tableViewCellNotification;

@end

@implementation NotificationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
   // NSArray *notificationArray = [[NetCoreSharedManager sharedInstance] getNotifications];
    [[NSBundle mainBundle]loadNibNamed:@"NotificationViewController" owner:self options:nil];
    
    [super hideBackButton:YES];
    [self.tableView registerNib:[UINib nibWithNibName:@"NotificationViewTableViewCell" bundle:nil]
         forCellReuseIdentifier:@"notificationImageCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"CarNotificationouselTableViewCell" bundle:nil]
         forCellReuseIdentifier:@"CarNotificationouselTableViewCell"];
    [super addViewInBaseView:self.viewNotification];
    [super setHeaderTitle:@"Notifications"];
    payLoadForViewDetails = [[NSMutableDictionary alloc] init];
    
    NSString *requestId = [[NSUserDefaults standardUserDefaults] objectForKey:kuserDefaultRequestId];
    NSString *tokenId = [[NSUserDefaults standardUserDefaults] objectForKey:kuserDefaultRequestId];

    
    if (requestId == nil || tokenId == nil)
    {
        [self getTokenID];
    }
  
}


- (void)viewWillAppear:(BOOL)animated
{
    
    
    [self fetchNotificationData];
    self.navigationController.view.backgroundColor = [UIColor clearColor];
   // [self.tableView reloadData];
}


-(void) fetchNotificationData
{
    notificationArray = [[NSMutableArray alloc] init];
    
    /*AppDelegate * delegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    
    NSManagedObjectContext *context = [delegate managedObjectContext] ;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Notification" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSError * error;
    
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
     //NSArray *fetchedObjects = [[NetCoreSharedManager sharedInstance] getNotifications];
    if (fetchedObjects.count == 0 || fetchedObjects == nil)
    {
        [[SlideNavigationController sharedInstance] popViewControllerAnimated:NO];
        [super showAlertViewWithTitle:@"Alert" withMessage:@"0 Notifications are there !"];
        
        return;
    }
    
    for (NSManagedObject *info in fetchedObjects)
    {
        
        if(DEBUG_ENABLED)
        {
            debug(@"\n info: %@ ", info );
        }

        NSLog(@"id: %@", [info valueForKey:@"id"]);
        
    
//        {
//            "title" : "Go Andaman: Rs 20,000 off",
//            "redirect" : "Andaman",
//            "me" : {
//                "subtitle" : "",
//                "body" : "Book Online & Get Flat 20,000 off on Andaman Holiday. Code: TCAND20 . Offer for selected Departures. T&C",
//                "title" : "Go Andaman: Rs 20,000 off"
//            },
//            "notificationId" : "8",
//            "imageUrl" : "",
//            "createdOn" : "1487069160023",
//            "notificationtype" : "destination"
//        }
        
//         {
//         DeliverTime = 181101173040;
//         ID = "25805-764-0-0-180125162917";
//         MessageData =         {
//         aps =             {
//         alert =                 {
//         body = "iOS Developer";
//         title = 8;
//         };
//         category = smartechPush;
//         };
//         payload =             {
//         actionButton =                 (
//         );
//         carousel =                 (
//         );
//         deeplink = "thomasCook://com.thomasCookHoliday.app/package_list/searchType=CONTINENT&destination=Europe?__sta=vhg.uosvpxUYQIlzjokqisfjst%7CVHV&__stm_medium=apn&__stm_source=smartech&__stm_id=242";
//         mediaurl = "";
//         trid = "25805-764-0-0-180125162917";
//         };
//         };
//         Status = unread;
//         }
        
        NSString *messageString = [NSString stringWithFormat:@"%@", [info valueForKey:@"message"]];
        //NSString *titleString = [NSString stringWithFormat:@"%@", [info valueForKey:@"title"]];
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        
        [dict setValue:[info valueForKey:@"id"] forKey:@"id"];
        [dict setValue:[info valueForKey:@"createdOn"] forKey:@"createdOn"];
        [dict setValue:messageString forKey:@"message"];
        [dict setValue:[info valueForKey:@"notificationType"] forKey:@"notificationType"];
        [dict setValue:[info valueForKey:@"redirect"] forKey:@"redirect"];
        [dict setValue:[info valueForKey:@"status"] forKey:@"status"];
        [dict setValue:[info valueForKey:@"data"] forKey:@"data"];
        //[dict setValue:titleString forKey:@"title"];
        
        [notificationArray addObject:dict];
        
        if(DEBUG_ENABLED)
        {
            debug(@"notificationArray: %@", notificationArray);
        }
        
    }
    
    NSSortDescriptor * titleDescriptor;
    
    NSArray * sortDescriptors ;
    
    NSArray * sortedArray;
    
    titleDescriptor = [[ NSSortDescriptor alloc ] initWithKey:@"createdOn" ascending:NO];
    
    sortDescriptors = @[ titleDescriptor ];
    
    sortedArray = [[NSArray alloc] initWithArray:[ notificationArray sortedArrayUsingDescriptors:sortDescriptors ]];
    
    [notificationArray removeAllObjects];
    
    [notificationArray addObjectsFromArray:sortedArray];*/
    NSArray *arrofNotifications = [[NetCoreSharedManager sharedInstance] getNotifications];
    NSLog(@"arr of notification %@",arrofNotifications);
    if(arrofNotifications.count == 0){
         [self showAlertInNotificationView:@"Alert" withMessage:@"0 Notifications are there !"];
       
    }else{
        NSLog(@"notufication found");
         [notificationArray addObjectsFromArray:arrofNotifications];
    }
    
    

}
-(void)showAlertInNotificationView:(NSString *)title withMessage:(NSString *)message
{
    UIAlertController *alertView = [UIAlertController alertControllerWithTitle:title
                                                                       message:message
                                                                preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [alertView dismissViewControllerAnimated:YES completion:nil];
        
    }];
    
    [alertView addAction:okAction];
    
    [self presentViewController:alertView animated:YES completion:nil];
}
#pragma mark -tableView data Source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [notificationArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellMainNibID = @"notificationCell";
    NSString *mediaURl = [[NSString alloc]init];
    mediaURl = [[[notificationArray[indexPath.row] valueForKey:@"MessageData"] valueForKey:@"payload"]valueForKey:@"mediaurl"];
    NSArray *carousal = [[NSArray alloc] initWithArray:[[[notificationArray[indexPath.row] valueForKey:@"MessageData"] valueForKey:@"payload"]valueForKey:@"carousel"]];
    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureTap:)];
    
    if (mediaURl.length != 0 && carousal.count == 0){
        if ([[mediaURl pathExtension] containsString:@"jpg" ] || [[mediaURl pathExtension] containsString:@"jpeg"] || [[mediaURl pathExtension] containsString:@"gif"]){
            NSString *cellID = @"notificationImageCell";
            NotificationViewTableViewCell *cell;
            if (cell == nil){
                cell = (NotificationViewTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellID];
            }
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            NSDictionary *dict = [notificationArray objectAtIndex:indexPath.row];
            
            NSDictionary *MessageNotificationData = [dict objectForKey:@"MessageData"];
            NSDictionary *apsNotificationData = [MessageNotificationData objectForKey:@"aps"];
            NSDictionary *alertNotificationdata = [apsNotificationData objectForKey:@"alert"];
            NSString *title = [alertNotificationdata objectForKey:@"title"];
            NSString *message = [alertNotificationdata objectForKey:@"body"];
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyMMddHHmmss"];
            NSDate *date = [dateFormatter dateFromString:[dict objectForKey:@"DeliverTime"]];
            
            
            [dateFormatter setDateFormat:DATE_FORMAT_12_HR];
            NSString *dateString=[dateFormatter stringFromDate:date];
            [cell.notificationImage setHidden:NO];
            [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:mediaURl] options:SDWebImageDownloaderUseNSURLCache progress:nil completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
                
                if (image && finished) {
                    cell.notificationImage.image = image;
                }else{
                    cell.notificationImage.image = [UIImage imageNamed:@"defaultBanner.png"];
                }
            }];
            cell.notificationImage.userInteractionEnabled = NO;
            cell.PlayButton.userInteractionEnabled = NO;
            cell.notificationTitle.text = [NSString stringWithFormat:@"%@", title];
            cell.notificationMsg.text = [NSString stringWithFormat:@"%@",message];
            [cell.PlayButton setHidden:YES];
            cell.notificationDate.text = [NSString stringWithFormat:@"%@",dateString];
            return cell;
        }else if ([[mediaURl pathExtension] containsString:@"mp4"]){
            NSString *cellID = @"notificationImageCell";
            NotificationViewTableViewCell *cell;
            if (cell == nil){
                cell = (NotificationViewTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellID];
            }
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            NSDictionary *dict = [notificationArray objectAtIndex:indexPath.row];
            
            NSDictionary *MessageNotificationData = [dict objectForKey:@"MessageData"];
            NSDictionary *apsNotificationData = [MessageNotificationData objectForKey:@"aps"];
            NSDictionary *alertNotificationdata = [apsNotificationData objectForKey:@"alert"];
            NSString *title = [alertNotificationdata objectForKey:@"title"];
            NSString *message = [alertNotificationdata objectForKey:@"body"];
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyMMddHHmmss"];
            NSDate *date = [dateFormatter dateFromString:[dict objectForKey:@"DeliverTime"]];
            [cell.notificationImage addGestureRecognizer:gesture];
            cell.notificationImage.image = [UIImage imageNamed:@"defaultBanner.png"];
            //[self loadThumbNail:mediaURl withcomplrtionhandler:^(BOOL istaskfinishde){
            //   cell.notificationImage.image = imagecell;
            //    [self.tableView reloadData];
           // }];
            [dateFormatter setDateFormat:DATE_FORMAT_12_HR];
            NSString *dateString=[dateFormatter stringFromDate:date];
            [gesture setDelegate: self ];
            cell.notificationImage.userInteractionEnabled = YES;
            cell.notificationTitle.text = [NSString stringWithFormat:@"%@", title];
            cell.notificationMsg.text = [NSString stringWithFormat:@"%@",message];
             [cell.PlayButton setHidden:NO];
            [cell.notificationImage setHidden:NO];
            cell.PlayButton.userInteractionEnabled = NO;
            cell.notificationDate.text = [NSString stringWithFormat:@"%@",dateString];
            return cell;
            
        }else if ([[mediaURl pathExtension] containsString:@"mp3"]){
            NSString *cellID = @"notificationImageCell";
            NotificationViewTableViewCell *cell;
            if (cell == nil){
                cell = (NotificationViewTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellID];
            }
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            NSDictionary *dict = [notificationArray objectAtIndex:indexPath.row];
            
            NSDictionary *MessageNotificationData = [dict objectForKey:@"MessageData"];
            NSDictionary *apsNotificationData = [MessageNotificationData objectForKey:@"aps"];
            NSDictionary *alertNotificationdata = [apsNotificationData objectForKey:@"alert"];
            NSString *title = [alertNotificationdata objectForKey:@"title"];
            NSString *message = [alertNotificationdata objectForKey:@"body"];
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyMMddHHmmss"];
            NSDate *date = [dateFormatter dateFromString:[dict objectForKey:@"DeliverTime"]];
            [dateFormatter setDateFormat:DATE_FORMAT_12_HR];
            NSString *dateString=[dateFormatter stringFromDate:date];
            [cell.notificationImage setHidden:YES];
            cell.notificationImage.userInteractionEnabled = YES;
            cell.PlayButton.userInteractionEnabled = YES;
            [cell.PlayButton addGestureRecognizer:gesture];
            [cell.PlayButton setHidden:NO];
            cell.notificationTitle.text = [NSString stringWithFormat:@"%@", title];
            cell.notificationMsg.text = [NSString stringWithFormat:@"%@",message];
            cell.notificationDate.text = [NSString stringWithFormat:@"%@",dateString];
            return cell;
        }else{
            NSString *cellID = @"notificationImageCell";
            NotificationViewTableViewCell *cell;
            if (cell == nil){
                cell = (NotificationViewTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellID];
            }
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }
        
    }else if(carousal.count != 0){
        NSString *cellID = @"CarNotificationouselTableViewCell";
        CarNotificationouselTableViewCell *cell;
        if (cell == nil){
            cell = (CarNotificationouselTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellID];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.dataDict = notificationArray[indexPath.row];
        cell.delegate = self;
        cell.fetchData;
        return cell;
        
    }else{
        _tableViewCellNotification = [tableView dequeueReusableCellWithIdentifier:cellMainNibID];
        if (_tableViewCellNotification == nil)
        {
            NSArray *inb= [[NSBundle mainBundle] loadNibNamed:@"NotificationCell" owner:self options:nil];
            _tableViewCellNotification=[inb objectAtIndex:0];
            
        }
        _tableViewCellNotification.selectionStyle = UITableViewCellSelectionStyleNone;
        //  UIImageView *notificationImage = (UIImageView *) [_tableViewCellNotification viewWithTag:1];
        
        UILabel *notificationTitleLabel = (UILabel *) [_tableViewCellNotification viewWithTag:1];
        
        UILabel *notificationMessageLabel = (UILabel *) [_tableViewCellNotification viewWithTag:2];
        
        UILabel *dateLabel = (UILabel *) [_tableViewCellNotification viewWithTag:3];
        
        UIView *view = (UIView *) [_tableViewCellNotification viewWithTag:4];
        NSDictionary *dict = [notificationArray objectAtIndex:indexPath.row];
        view.layer.cornerRadius = 10;
        
        /*  notification dictionary from notification controller dict: {
         createdOn = 1487069548336;
         id = 8;
         message = "Book Online & Get Flat 20,000 off on Andaman Holiday. Code: TCAND20 . Offer for selected Departures. T&C";
         notificationType = destination;
         redirect = Andaman;
         status = unread;
         }*/
        
        NSDictionary *MessageNotificationData = [dict objectForKey:@"MessageData"];
        NSDictionary *apsNotificationData = [MessageNotificationData objectForKey:@"aps"];
        NSDictionary *alertNotificationdata = [apsNotificationData objectForKey:@"alert"];
        NSString *title = [alertNotificationdata objectForKey:@"title"];
        NSString *message = [alertNotificationdata objectForKey:@"body"];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyMMddHHmmss"];
        NSDate *date = [dateFormatter dateFromString:[dict objectForKey:@"DeliverTime"]];
        
        
        [dateFormatter setDateFormat:DATE_FORMAT_12_HR];
        NSString *dateString=[dateFormatter stringFromDate:date];
        
        
        notificationTitleLabel.text = [NSString stringWithFormat:@"%@", title];
        notificationMessageLabel.text = [NSString stringWithFormat:@"%@",message];
        
        dateLabel.text = [CoreUtility convertSystemMillisToDateString:[dict objectForKey:@"DeliverTime"]];
        dateLabel.text = [NSString stringWithFormat:@"%@",dateString];
        return _tableViewCellNotification;
        
    }
    

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary * dict = [notificationArray objectAtIndex:indexPath.row];
    NSString *DeepLink = [[NSString alloc]init];
    DeepLink = [[[notificationArray[indexPath.row] valueForKey:@"MessageData"] valueForKey:@"payload"]valueForKey:@"deeplink"];
    NSURL *urlForDeepLink = [NSURL URLWithString:DeepLink];
    
    if ([[urlForDeepLink path] containsString:@"package_details"])
    {
        NSString *stringWithoutHash = [[urlForDeepLink path] stringByReplacingOccurrencesOfString:@"/" withString:@""];
        NSString *packageId = [stringWithoutHash stringByReplacingOccurrencesOfString:@"package_details" withString:@""];
        
        [self fetchPackageDetailsWithPackageID:packageId];
        
    }
    else if ([[urlForDeepLink path] containsString:@"package_list"])
    {
        NSString *stringWithoutHash = [[urlForDeepLink path] stringByReplacingOccurrencesOfString:@"/" withString:@""];
        NSString *destination = [stringWithoutHash stringByReplacingOccurrencesOfString:@"package_list" withString:@""];
        [self searchDestinationWithDestinationName:destination];
        
        
    }
    else if ([[urlForDeepLink path] containsString:@"web"])
    {
        NSString *stringWithoutHash = [[urlForDeepLink path] stringByReplacingOccurrencesOfString:@"/" withString:@""];
        NSString *destination = [stringWithoutHash stringByReplacingOccurrencesOfString:@"web" withString:@""];
        NSURL *url = [NSURL URLWithString:destination];
        WebViewController *webViewVC = [[WebViewController alloc]initWithNibName:@"BaseViewController" bundle:nil];
        webViewVC.url = url;
        webViewVC.headerString = @"WEB";
        [[SlideNavigationController sharedInstance] pushViewController:webViewVC animated:YES];
        
    }else if ([[urlForDeepLink scheme] containsString:@"WWW"] || [[urlForDeepLink scheme] containsString:@"www"] || [[urlForDeepLink scheme] containsString:@"http"]){
        WebViewController *webVC = [[WebViewController alloc] initWithNibName:@"BaseViewController" bundle:nil];
        webVC.url = urlForDeepLink;
        webVC.headerString = @"WEB";
        [[SlideNavigationController sharedInstance] pushViewController:webVC animated:YES];
        
    }else if ([[urlForDeepLink path] containsString:@"css_payment"])
    {
        if ([[NSUserDefaults standardUserDefaults] valueForKey: @"isCSSUserLoggedIn"]) {
            NSString *stringWithoutHash = [[urlForDeepLink path] stringByReplacingOccurrencesOfString:@"/" withString:@":"];
            NSArray *bffarray = [stringWithoutHash componentsSeparatedByString:@":"];
            NSString *bffnumber = [bffarray objectAtIndex:bffarray.count - 1];
            NSLog(@"----bff:%@",bffnumber);
            [NotificationController moveToRespectiveViewControllerWithNotificationType:@"payments" bfNumber:bffnumber webViewRedirectionURL:@"" message:@""];
            
        }
        
    }else if ([[urlForDeepLink path] containsString:@"css_ticket"])
    {
        if ([[NSUserDefaults standardUserDefaults] valueForKey: @"isCSSUserLoggedIn"]) {
            NSString *stringWithoutHash = [[urlForDeepLink path] stringByReplacingOccurrencesOfString:@"/" withString:@":"];
            NSArray *bffarray = [stringWithoutHash componentsSeparatedByString:@":"];
            NSString *bffnumber = [bffarray objectAtIndex:bffarray.count - 1];
            NSLog(@"----bff:%@",bffnumber);
            [NotificationController moveToRespectiveViewControllerWithNotificationType:@"air tickets" bfNumber:bffnumber webViewRedirectionURL:@"" message:@""];
            
        }
        
    }else if ([[urlForDeepLink path] containsString:@"css_tour_document"])
    {
        if ([[NSUserDefaults standardUserDefaults] valueForKey: @"isCSSUserLoggedIn"]) {
            NSString *stringWithoutHash = [[urlForDeepLink path] stringByReplacingOccurrencesOfString:@"/" withString:@":"];
            NSArray *bffarray = [stringWithoutHash componentsSeparatedByString:@":"];
            NSString *bffnumber = [bffarray objectAtIndex:bffarray.count - 1];
            NSLog(@"----bff:%@",bffnumber);
            [NotificationController moveToRespectiveViewControllerWithNotificationType:@"tour details" bfNumber:bffnumber webViewRedirectionURL:@"" message:@""];
            
        }
        
    }else if ([[urlForDeepLink path] containsString:@"css_profile"])
    {//Profile
        if ([[NSUserDefaults standardUserDefaults] valueForKey: @"isCSSUserLoggedIn"]) {
            
            NSString *stringWithoutHash = [[urlForDeepLink path] stringByReplacingOccurrencesOfString:@"/" withString:@":"];
            NSArray *bffarray = [stringWithoutHash componentsSeparatedByString:@":"];
            NSString *bffnumber = [bffarray objectAtIndex:bffarray.count - 1];
            NSLog(@"----bff:%@",bffnumber);
            [NotificationController moveToRespectiveViewControllerWithNotificationType:@"profile" bfNumber:bffnumber webViewRedirectionURL:@"" message:@""];
            
            
            
        }
        
    }else if ([[urlForDeepLink path] containsString:@"css_deviation"])
    {
        if ([[NSUserDefaults standardUserDefaults] valueForKey: @"isCSSUserLoggedIn"]) {
            NSString *stringWithoutHash = [[urlForDeepLink path] stringByReplacingOccurrencesOfString:@"/" withString:@":"];
            NSArray *bffarray = [stringWithoutHash componentsSeparatedByString:@":"];
            NSString *bffnumber = [bffarray objectAtIndex:bffarray.count - 1];
            NSLog(@"----bff:%@",bffnumber);
            [NotificationController moveToRespectiveViewControllerWithNotificationType:@"deviation" bfNumber:bffnumber webViewRedirectionURL:@"" message:@""];
            
        }
        
    }else if ([[urlForDeepLink path] containsString:@"css_visa"])
    {
        if ([[NSUserDefaults standardUserDefaults] valueForKey: @"isCSSUserLoggedIn"]) {
            NSString *stringWithoutHash = [[urlForDeepLink path] stringByReplacingOccurrencesOfString:@"/" withString:@":"];
            NSArray *bffarray = [stringWithoutHash componentsSeparatedByString:@":"];
            NSString *bffnumber = [bffarray objectAtIndex:bffarray.count - 1];
            NSLog(@"----bff:%@",bffnumber);
            [NotificationController moveToRespectiveViewControllerWithNotificationType:@"visas" bfNumber:bffnumber webViewRedirectionURL:@"" message:@""];
            
        }
        
    }else if ([[urlForDeepLink path] containsString:@"css_feedback"])
    {
        if ([[NSUserDefaults standardUserDefaults] valueForKey: @"isCSSUserLoggedIn"]) {
            NSString *stringWithoutHash = [[urlForDeepLink path] stringByReplacingOccurrencesOfString:@"/" withString:@":"];
            NSArray *bffarray = [stringWithoutHash componentsSeparatedByString:@":"];
            NSString *bffnumber = [bffarray objectAtIndex:bffarray.count - 1];
            NSLog(@"----bff:%@",bffnumber);
            [NotificationController moveToRespectiveViewControllerWithNotificationType:@"feedback" bfNumber:bffnumber webViewRedirectionURL:@"" message:@""];
            
        }else{
           [self onSelfServiceMenuClick];
        }
        
    }
    
    //[self makeNotificationAsRead:dict];
    /*
    if ([[dict objectForKey:@"notificationType"] caseInsensitiveCompare:@"offer"] == NSOrderedSame) {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[dict objectForKey:@"redirect"]]];
        
    }
    else if ([[dict objectForKey:@"notificationType"] caseInsensitiveCompare:@"web"] == NSOrderedSame) {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[dict objectForKey:@"redirect"]]];
        
    }
    else if ([[dict objectForKey:@"notificationType"] caseInsensitiveCompare:@"package"] == NSOrderedSame) {
        
        [self fetchPackageDetails:[dict objectForKey:@"redirect"]];
    }
    else if ([[dict objectForKey:@"notificationType"] caseInsensitiveCompare:@"destination"] == NSOrderedSame)
    {
        [self searchDestinationWithDestinationName:dict];
    }
    else
    {
        
    }*/
    
}


-(void)fetchPackageDetailsWithPackageID:(NSString *)packageID
{
    if ([self connected])
    {
        @try
        {
            
            NetworkHelper *helper = [NetworkHelper sharedHelper];
            
            NSString *pathParam = packageID;
            
            NSDictionary *headerDict = [CoreUtility getHeaderDict];
            
            [helper getResponseWithRequestType:@"" withQueryParam:@"" withPathParam:pathParam withJsonParam:nil withHeaders:headerDict withUrl:kAstraUrlPackageDetail success:^(NSDictionary *responseDict)
             {
                 NSLog(@"Response Dict : %@",responseDict);
                 
                 
                 dispatch_async(dispatch_get_main_queue(), ^(void) {
                     [activityLoadingView removeFromSuperview];
                     if (responseDict)
                     {
                         if (responseDict.count>0)
                         {
                             NSArray *packageArray = (NSArray *)responseDict;
                             NSDictionary *dictForCompletePackage = [packageArray objectAtIndex:0];
                             
                             HolidayPackageDetail *packageDetailHoliday = [[HolidayPackageDetail alloc] initWithDataDict:dictForCompletePackage];
                             
                             //  [self fireAppIntHoViewDetailsEvent]; //
                             
                             TabMenuVC *tabMenuVC = [[TabMenuVC alloc]initWithNibName:@"BaseViewController" bundle:nil];
                             
                             //tabMenuVC.headerName = objHoliday.strPackageName;
                             
                             tabMenuVC.flag = @"Overview";
                             tabMenuVC.packageDetail = packageDetailHoliday;
                             tabMenuVC.completePackageDetail = [NSArray arrayWithObjects:dictForCompletePackage, nil];
                             [[SlideNavigationController sharedInstance] pushViewController:tabMenuVC animated:YES];
                             
                             // [self doMyOperationsWithResponse:packageArray];
                             
                         }
                         else
                         {
                             [self showAlertViewWithTitle:@"Alert" withMessage:@"some error occurred"];
                             
                         }
                     }
                     else
                     {
                         [self showAlertViewWithTitle:@"Alert" withMessage:@"some error occurred"];
                         
                     }
                 });
                 
             }
             
                                       failure:^(NSError *error)
             {
                 dispatch_async(dispatch_get_main_queue(), ^(void)
                                {
                                    [activityLoadingView removeFromSuperview];
                                    [self showAlertViewWithTitle:@"Alert" withMessage:@"some error occurred"];
                                    NSLog(@"Response Dict : %@",[error description]);
                                });
                 
                 
             }];
            
        }
        @catch (NSException *exception)
        {
            NSLog(@"%@", exception.reason);
            [activityLoadingView removeFromSuperview];
        }
        @finally
        {
            NSLog(@"exception finally called");
        }
    }
}

-(void)fetchPackageDetails:(NSString *)packageID
{
     activityIndicator = [LoadingView loadingViewInView:self.viewNotification.superview withString:@"" andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
    NSMutableDictionary *dictOfData = [[NSMutableDictionary alloc]init];
    [dictOfData setValue:packageID forKey:@"packageId"];
    if([super connected])
    {

   /* dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NetworkHelper *helper = [NetworkHelper sharedHelper];
        NSString *strResponse = [helper getDataFromServerForType:@"webservice" entity:@"package" action:@"details" andUserJson:dictOfData];
        NSString *strStatus = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"status"];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([strStatus isEqualToString:kStatusSuccess])
            {
                NSArray *dataArray = [[JsonSerealizer arrayOfJSonFromJsonData:strResponse] valueForKey:@"data"];
                NSDictionary *dataDict;
                if (dataArray.count != 0)
                {
                    dataDict = dataArray[0];
                    packageDetail = [[HolidayPackageDetail alloc]initWithDataDict:dataDict];
                    
                    
                    [activityIndicator removeView];
                    activityIndicator = nil;
                  
                    [self fireAppIntHoViewDetailsEvent];
                    
                    TabMenuVC *tabMenuVC = [[TabMenuVC alloc]initWithNibName:@"BaseViewController" bundle:nil];
                    //tabMenuVC.headerName = objHoliday.strPackageName;
                    tabMenuVC.flag=@"Overview";
                    tabMenuVC.packageDetail = packageDetail;
                    tabMenuVC.completePackageDetail = dataArray;
                    [[SlideNavigationController sharedInstance] pushViewController:tabMenuVC animated:YES];
                    
                    
                }
            }else
            {
                NSString *messsage = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"message"];
                if (messsage)
                {
                    if ([messsage isEqualToString:kVersionUpgradeMessage])
                    {
                        NSDictionary *downloadDict = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"data"];
                        NSString *downloadURL = [downloadDict valueForKey:@"download_url"];
                        [self showAlertViewForVersionUpdateWithUrl:downloadURL];
                        
                    }
                    else
                    {
                        [self showAlertViewWithTitle:@"Alert" withMessage:messsage];
                    }
                }else
                {
                    [self showAlertViewWithTitle:@"Alert" withMessage:kMessageSomeErrorTryAfterSometime];
                }
                
            }
              [activityIndicator removeView];
            activityIndicator = nil;
        });
    });*/
        
        @try
        {
            
            NetworkHelper *helper = [NetworkHelper sharedHelper];
            
            NSString *pathParam = packageID;
            
            NSDictionary *headerDict = [CoreUtility getHeaderDict];
            
            [helper getResponseWithRequestType:@"" withQueryParam:@"" withPathParam:pathParam withJsonParam:nil withHeaders:headerDict withUrl:kAstraUrlPackageDetail success:^(NSDictionary *responseDict)
             {
                 NSLog(@"Response Dict : %@",responseDict);
                 
                 
                 dispatch_async(dispatch_get_main_queue(), ^(void) {
                     [activityIndicator removeFromSuperview];
                     if (responseDict)
                     {
                         if (responseDict.count>0)
                         {
                             NSArray *packageArray = (NSArray *)responseDict;
                             NSDictionary *dictForCompletePackage = [packageArray objectAtIndex:0];
                             
                             HolidayPackageDetail *packageDetailHoliday = [[HolidayPackageDetail alloc] initWithDataDict:dictForCompletePackage];
                             
                             //  [self fireAppIntHoViewDetailsEvent]; //
                             
                             TabMenuVC *tabMenuVC = [[TabMenuVC alloc]initWithNibName:@"BaseViewController" bundle:nil];
                             
                             //tabMenuVC.headerName = objHoliday.strPackageName;
                             
                             tabMenuVC.flag = @"Overview";
                             tabMenuVC.packageDetail = packageDetailHoliday;
                             tabMenuVC.completePackageDetail = [NSArray arrayWithObjects:dictForCompletePackage, nil];
                            // tabMenuVC.payloadDict = payloadList;
                             [[SlideNavigationController sharedInstance] pushViewController:tabMenuVC animated:YES];
                             
                             // [self doMyOperationsWithResponse:packageArray];
                             
                         }
                         else
                         {
                             [super showAlertViewWithTitle:@"Alert" withMessage:@"Some error occurred"];
                         }
                             
                     }
                     else
                     {
                         [super showAlertViewWithTitle:@"Alert" withMessage:@"Some error occurred"];

                     }
                 });
                 
             }
                                       failure:^(NSError *error)
             {
                 dispatch_async(dispatch_get_main_queue(), ^(void)
                                {
                                    [activityIndicator removeFromSuperview];
                                    NSLog(@"Response Dict : %@",[error description]);
                                    [super showAlertViewWithTitle:@"Alert" withMessage:@"Some error occurred"];

                                });
                 
                 
                 
             }];
            
        }
        @catch (NSException *exception)
        {
            NSLog(@"%@", exception.reason);
            [activityIndicator removeFromSuperview];
        }
        @finally
        {
            NSLog(@"exception finally called");
        }
    }
    else
    {
         [self showAlertViewWithTitle:@"Alert" withMessage:kMessageNoInternet];
    }
}

-(void)searchDestinationWithDestinationName:(NSString *)destinationstr
{
    
  /*  NSString *strDestination=destination;
    
    NSDictionary *dictOfData = [NSDictionary dictionaryWithObjectsAndKeys:@"",kBudgetKey,@"",kNightToSpendKey,strDestination,kDestinationKey, nil];
    
        activityIndicator = [LoadingView loadingViewInView:self.viewNotification.superview withString:@"" andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
    
    if ([super connected])
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            NetworkHelper *helper = [NetworkHelper sharedHelper];
            
            NSString *strResponseLocal = [helper getDataFromServerForType:kType entity:kEntity action:kAction andUserJson:dictOfData];
            
            NSString *strStatus = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponseLocal]valueForKey:@"status"];
            dispatch_async(dispatch_get_main_queue(), ^{
                if ([strStatus isEqualToString:kStatusSuccess])
                {
                    NSArray *arrayOfData = [[[JsonSerealizer dictonaryOfJsonFromJsonData:strResponseLocal]valueForKey:kServerResponseKeyData]valueForKey:kHolidays];
                    NSDictionary *filterDict = [[[JsonSerealizer dictonaryOfJsonFromJsonData:strResponseLocal]valueForKey:kServerResponseKeyData]valueForKey:kFilters];
                    NSMutableArray *arrayOfHolidays = [[NSMutableArray alloc]init];
                    
                    for (NSDictionary *dictOfData in arrayOfData)
                    {
                        Holiday *nwHoliday  = [[Holiday alloc]initWithDataDict:dictOfData];
                        [arrayOfHolidays addObject:nwHoliday];
                    }
  
                        [activityIndicator removeView];

                    TabMenuVC *tabMenuVC=[[TabMenuVC alloc]initWithNibName:@"BaseViewController" bundle:nil];
                    tabMenuVC.headerName=[NSString stringWithFormat:@"%@ %@",strDestination, @"Holidays"];
                    tabMenuVC.searchedCity = destination;
                    tabMenuVC.holidayArray = arrayOfHolidays;
                    tabMenuVC.filterDict = filterDict;
                    tabMenuVC.totalPackages=[NSString stringWithFormat:@"%lu",(unsigned long)[arrayOfData count]];
                    
                    [[SlideNavigationController sharedInstance] pushViewController:tabMenuVC animated:YES];
                    
                }
                else
                {
                    NSString *messsage = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponseLocal]valueForKey:@"message"];
                    if (messsage)
                    {
                        if ([messsage isEqualToString:kVersionUpgradeMessage])
                        {
                            NSDictionary *downloadDict = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponseLocal] valueForKey:@"data"];
                            NSString *downloadURL = [downloadDict valueForKey:@"download_url"];
                            [super showAlertViewForVersionUpdateWithUrl:downloadURL];
                            
                        }else
                        {
                            [super showAlertViewWithTitle:@"Alert" withMessage:messsage];
                            [activityIndicator removeView];
                        }
                    }else
                    {
                        [super showAlertViewWithTitle:@"Alert" withMessage:kMessageSomeErrorTryAfterSometime];
                        [activityIndicator removeView];
                        
                        
                    }
                    
                }
                
            });
        });
        
    }
    else
    {
            [activityIndicator removeView];
        
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
        {
            UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Info"
                                                                               message:kMessageNoInternet
                                                                        preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                
                [alertView dismissViewControllerAnimated:YES completion:nil];
                
            }];
            
            [alertView addAction:actionOk];
            
            [self presentViewController:alertView animated:YES completion:nil];
            
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Info"
                                                           message:kMessageNoInternet
                                                          delegate:nil
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles:nil, nil];
            
            [alert show];
        }
    }*/
    
    NSString *destination  = destinationstr;
    
    
    // NSString *dataString = [destinationDict valueForKey:@"data"];
    
    
   // NSDictionary *dataDict = [NSJSONSerialization JSONObjectWithData:[dataString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:nil];
    
    NSString *titleString = destinationstr ;
    
     NetworkHelper *helper = [NetworkHelper sharedHelper];
    
   LoadingView  *activityLoadingView = [LoadingView loadingViewInView:self.view.superview.superview
                                              withString:@""
                                       andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];

    
    NSMutableString *queryParam = [[NSMutableString alloc] init] ;
    
    [queryParam appendFormat:@"%@", destination];
   
    
    @try
    {
        
        NSString *stringWithDecode  = [queryParam stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        
        
        NSDictionary *headerDict = [CoreUtility getHeaderDict];
        
        [helper getResponseWithRequestType:@"" withQueryParam:stringWithDecode withPathParam:@"" withJsonParam:nil withHeaders:headerDict withUrl:kAstraUrlPackageFilterSearch success:^(NSDictionary *responseDict)
         {
             dispatch_async(dispatch_get_main_queue(), ^(void)
                            {
                                [activityLoadingView removeFromSuperview];
                                NSLog(@"Response Dict : %@",responseDict);
                                
                                if (responseDict)
                                {
                                    if (responseDict.count>0)
                                    {
                                        NSMutableArray *arrayOfHolidays = [[NSMutableArray alloc] init];
                                        NSArray *packageArray = (NSArray *)responseDict;
                                        
                                        for (NSDictionary *dictOfData in packageArray)
                                        {
                                            Holiday *nwHoliday  = [[Holiday alloc]initWithDataDict:dictOfData];
                                            [arrayOfHolidays addObject:nwHoliday];
                                        }
                                        
                                        [activityIndicator removeView];
                                        
                                        TabMenuVC *tabMenuVC=[[TabMenuVC alloc]initWithNibName:@"BaseViewController" bundle:nil];
                                        NSArray *listItems = [[NSArray alloc]initWithArray:[destination componentsSeparatedByString:@"="]];
                                        if ([listItems count] != 0){
                                            NSString *headerlabel = listItems[[listItems count] - 1];
                                            tabMenuVC.headerName = headerlabel;
                                        }else{
                                            tabMenuVC.headerName = @"";
                                        }
                                        
                                        tabMenuVC.searchedCity = titleString;
                                        tabMenuVC.holidayArray = arrayOfHolidays;
                                        tabMenuVC.totalPackages=[NSString stringWithFormat:@"%lu",(unsigned long)[arrayOfHolidays count]];
                                        tabMenuVC.completePackageDetail = packageArray;
                                        [[SlideNavigationController sharedInstance] pushViewController:tabMenuVC animated:YES];
                                    }
                                    else
                                    {
                                        [super showAlertViewWithTitle:@"error" withMessage:@"Sorry, we could not find any package for the given search. Please try with a different search criteria."];
                                        NSLog(@" response Date Search packageDetailsMessage");
                                        
                                    }
                                }
                                else
                                {
                                      [activityLoadingView removeFromSuperview];
                                    [super showAlertViewWithTitle:@"Alert" withMessage:@"Some error occurred"];

                                }
                                
                            });
         }
                                   failure:^(NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^(void)
                            {
                                [activityLoadingView removeFromSuperview];
                                [super showAlertViewWithTitle:@"Alert" withMessage:[error description]];
                                NSLog(@"Response Dict : %@",[error description]);
                            });
             
         }];
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"%@", exception.reason);
        [activityLoadingView removeFromSuperview];
    }
    @finally
    {
        NSLog(@"exception finally called");
    }

    
    
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
   NSString *mediaURl = [[[notificationArray[indexPath.row] valueForKey:@"MessageData"] valueForKey:@"payload"]valueForKey:@"mediaurl"];
    NSArray *carousal = [[NSArray alloc] initWithArray:[[[notificationArray[indexPath.row] valueForKey:@"MessageData"] valueForKey:@"payload"]valueForKey:@"carousel"]];
    
    if (mediaURl.length == 0) {
        if (carousal.count == 0) {
            NSDictionary * textdict = [[[[notificationArray objectAtIndex:indexPath.row]valueForKey:@"MessageData"] valueForKey:@"aps"] valueForKey:@"alert"];
            
            NSString * text = [textdict valueForKey:@"body"];
            
            UIFont *font = [UIFont fontWithName:@"Arial" size:14.0f];
            
            CGSize size = [text sizeWithAttributes:@{NSFontAttributeName: font}];
            
            int numOfLines = (int)size.width / (int)tableView.contentSize.width + 1;
            
            CGFloat textHeight = numOfLines * size.height;
            
            
            return textHeight + 75;
        }else{
            return 475;
        }
       
    }else if([[mediaURl pathExtension] containsString:@"mp3" ] || [[mediaURl pathExtension] containsString:@"audio"] || [[mediaURl pathExtension] containsString:@"music"]){
        return 180;
    }else if (mediaURl.length != 0 && carousal.count !=0){
        return 475;
    }else{
        return 300;
    }
    
    

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) makeNotificationAsRead:(NSDictionary*)dict
{
    
    
    AppDelegate * delegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    
    NSManagedObjectContext *context = [delegate managedObjectContext] ;
   
    
    NSFetchRequest *request1 = [NSFetchRequest fetchRequestWithEntityName:@"Notification"];
    
    request1.predicate = [NSPredicate predicateWithFormat:
                          @"id = %@", [dict objectForKey:@"id"]];
    
    NSError * error;
    
    NSArray * activeIds1 = [context executeFetchRequest:request1 error:&error];
    
    
    if (activeIds1 == nil) {
        
        // handle error
    }
    else
    {
        NSManagedObject *object =(NSManagedObject*)[activeIds1 objectAtIndex:0];
        
        [object setValue:@"read" forKey:@"status"];
        
        if (![object.managedObjectContext save:&error]) {
            
            NSLog(@"Unable to update managed object context.");
            NSLog(@"%@, %@", error, error.localizedDescription);
        }
    }
    [self fetchNotificationData];
    
    [self.tableView reloadData];
    
}

- (BOOL)slideNavigationControllerShouldDisplayRightMenu
{
    return NO;
}

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
}

//APP_INT_HO_VIEW_DETAILS
-(void)fireAppIntHoViewDetailsEvent:(HolidayPackageDetail *)PackageDetail
{
    [payLoadForViewDetails setObject:PackageDetail.strPackageName forKey:@"s^DESTINATION"];
    [payLoadForViewDetails setObject:[NSString stringWithFormat:@"%d",PackageDetail.durationNoDays] forKey:@"s^DURATION"];
    [payLoadForViewDetails setObject:[NSString stringWithFormat:@"%d",PackageDetail.packagePrise] forKey:@"s^BUDGET"];
    [payLoadForViewDetails setObject:PackageDetail.strPackageId forKey:@"s^PACKAGE_ID"];
    [payLoadForViewDetails setObject:@"App" forKey:@"s^SOURCE"];
    [payLoadForViewDetails setObject:[NSNumber numberWithInt:PackageDetail.packagePrise] forKey:@"i^PACKAGE_PRICE"]; //28-02-2018
    
//  [payLoadForViewDetails setObject:[NSString stringWithFormat:@"%d",packageDetail.packagePrise] forKey:@"i^PACKAGE_PRICE"]; //28-02-2018
    
    
    
    if ([[PackageDetail.strPackageSubType lowercaseString]isEqualToString:@"git"])
    {
        [payLoadForViewDetails setObject:@"no" forKey:@"s^PERSONALIZED_TOUR"];
    }
    else
    {
        [payLoadForViewDetails setObject:@"yes" forKey:@"s^PERSONALIZED_TOUR"];
    }
    
    if(PackageDetail.strPackageType != nil && ![PackageDetail.strPackageType isEqualToString:@""])
    {
        [payLoadForViewDetails setObject:PackageDetail.strPackageType forKey:@"s^TYPE"]; //28-02-2018
    }
    
    //[NetCoreAnalyticsVC getNetCorePushNotificationDetailDictionary:payLoadForViewDetails withPayloadCount:105];
    
    [NetCoreAnalyticsVC getNetCorePushNotificationDetailDictionary:payLoadForViewDetails withPayloadCount:125]; //28-02-2018
    NSLog(@"Data submitted to the netcore:125");
    
}

-(void)getTokenID
{
    @try
    {
        
        if ([SSKeychain passwordForService:KEYCHAIN_SERVICE account:KEYCHAIN_ACCOUNT] == nil)
        {
            NSString *deviceId = [MobiculeUtilityManager getApplicationUUID];
            [SSKeychain setPassword:deviceId forService:KEYCHAIN_SERVICE account:KEYCHAIN_ACCOUNT];
        }
        
        
        NetworkHelper *networkHelper = [[NetworkHelper alloc] init];
        
        NSDictionary *headerDict = [[NSDictionary alloc] initWithObjectsAndKeys:[SSKeychain passwordForService:KEYCHAIN_SERVICE account:KEYCHAIN_ACCOUNT],@"uniqueId",@"mobicule",@"user", nil];
        
      LoadingView  *activityLoadingView = [LoadingView loadingViewInView:self.viewNotification.superview withString:@"" andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            [networkHelper  getResponseWithRequestType:@"" withQueryParam:@"" withPathParam:@"" withJsonParam:nil withHeaders:headerDict withUrl:kAstraUrlNewToken success:^(NSDictionary *responseDict)
             {
                 dispatch_async(dispatch_get_main_queue(), ^(void) {
                     
                    [activityLoadingView removeFromSuperview];
                     
                     NSLog(@"%@",responseDict);
                     
                     if(responseDict)
                     {
                         
                         if (responseDict.count > 0)
                         {
                             NSDictionary *requestId  = [responseDict valueForKey:@"requestId"];
                             NSDictionary *tokenId  = [responseDict valueForKey:@"tokenId"];
                             
                             if (requestId)
                             {
                                 [[NSUserDefaults standardUserDefaults] setObject:requestId forKey:kuserDefaultRequestId];
                                 
                             }
                             
                             if (tokenId)
                             {
                                 [[NSUserDefaults standardUserDefaults] setObject:tokenId forKey:kuserDefaultTokenId];
                                 
                             }
                         }
                         else
                         {
                             [super showAlertViewWithTitle:@"Alert" withMessage:@"some error occurred"];
                         }
                     }
                     else
                     {
                         [super showAlertViewWithTitle:@"Alert" withMessage:@"some error occurred"];
                     }
                 });
             }
                                               failure:^(NSError *error)
             {
                 dispatch_async(dispatch_get_main_queue(), ^(void)
                                {
                                    [activityLoadingView removeFromSuperview];
                                    [super showAlertViewWithTitle:@"Alert" withMessage:@"some error occurred"];
                                    NSLog(@"Response Dict : %@",[error description]);
                                });
             }];
            
        });
    }
    @catch (NSException *exception)
    {
        NSLog(@"%@", exception.reason);
        //[activityLoadingView removeFromSuperview];
    }
    @finally
    {
        NSLog(@"exception finally called");
    }
}

-(void)DownloadVideo:(NSString*)url withCompletionHandler:(isCompleted)CompletionBlock
{
    //download the file in a seperate thread.
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSLog(@"Downloading Started");
        NSString *urlToDownload = url;
        NSURL  *url = [NSURL URLWithString:urlToDownload];
        NSData *urlData = [NSData dataWithContentsOfURL:url];
        if ( urlData )
        {
            NSArray*paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString  *documentsDirectory = [paths objectAtIndex:0];
            
            NSString  *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory,url];
            
            //saving is done on main thread
            dispatch_async(dispatch_get_main_queue(), ^{
                [urlData writeToFile:filePath atomically:YES];
                NSLog(@"File Saved !");
            });
        }
        
    });
}

- (void)tapGestureTap:(UITapGestureRecognizer *)sender {
    CGPoint index = [sender locationInView:self.tableView];
    NSIndexPath *indexpath = [self.tableView indexPathForRowAtPoint:index];
    NSLog(@"%@", notificationArray[indexpath.row]);
    NSString *mediaURl = [[NSString alloc]init];
    mediaURl = [[[notificationArray[indexpath.row] valueForKey:@"MessageData"] valueForKey:@"payload"]valueForKey:@"mediaurl"];
    NSURL *URL = [[NSURL alloc] initWithString:mediaURl];
    AVPlayer *player = [AVPlayer playerWithURL:URL];
    AVPlayerViewController *playerViewController = [AVPlayerViewController new];
    playerViewController.player = player;
    [playerViewController.player play];//Used to Play On start
    [self presentViewController:playerViewController animated:YES completion:nil];
    NSLog(@"video play");
}

-(void)loadThumbNail:(NSString *)urlVideo withcomplrtionhandler:(isCompleted)CompletionBlock
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    
    NSURL *urlvid = [[NSURL alloc]initWithString:urlVideo];
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:urlvid options:nil];
    AVAssetImageGenerator *generate = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    generate.appliesPreferredTrackTransform=TRUE;
    NSError *err = NULL;
    //CMTime time = CMTimeMake(1, 12);
        CMTime time = [asset duration];
        time.value = 10;
    CGImageRef imgRef = [generate copyCGImageAtTime:time actualTime:NULL error:&err];
    NSLog(@"err==%@, imageRef==%@", err, imgRef);
        if (imgRef != NULL) {
           imagecell = [UIImage imageWithCGImage:imgRef];
            CompletionBlock(YES);
            CGImageRelease(imgRef);
        }else{
            imagecell = [UIImage imageNamed:@"defaultBanner.png"];
            CGImageRelease(imgRef);
            CompletionBlock(YES);
        }
        
    });
}
-(void)didPressButton:(NSURL *)imageURL{
    NSLog(@"imageurl %@",imageURL);
    NSURL* urlForDeepLink = imageURL;
    if ([[urlForDeepLink path] containsString:@"package_details"])
    {
        NSString *stringWithoutHash = [[urlForDeepLink path] stringByReplacingOccurrencesOfString:@"/" withString:@""];
        NSString *packageId = [stringWithoutHash stringByReplacingOccurrencesOfString:@"package_details" withString:@""];
        
        [self fetchPackageDetailsWithPackageID:packageId];
        
    }
    else if ([[urlForDeepLink path] containsString:@"package_list"])
    {
        NSString *stringWithoutHash = [[urlForDeepLink path] stringByReplacingOccurrencesOfString:@"/" withString:@""];
        NSString *destination = [stringWithoutHash stringByReplacingOccurrencesOfString:@"package_list" withString:@""];
        [self searchDestinationWithDestinationName:destination];
        
        
    }
    else if ([[urlForDeepLink path] containsString:@"web"])
    {
        NSString *stringWithoutHash = [[urlForDeepLink path] stringByReplacingOccurrencesOfString:@"/" withString:@""];
        NSString *destination = [stringWithoutHash stringByReplacingOccurrencesOfString:@"web" withString:@""];
        NSURL *url = [NSURL URLWithString:destination];
        WebViewController *webViewVC = [[WebViewController alloc]initWithNibName:@"BaseViewController" bundle:nil];
        webViewVC.url = url;
        [[SlideNavigationController sharedInstance] pushViewController:webViewVC animated:YES];
        
    }else if ([[urlForDeepLink scheme] containsString:@"WWW"] || [[urlForDeepLink scheme] containsString: @"www"] || [[urlForDeepLink scheme] containsString:@"http"]){
        WebViewController *webVC = [[WebViewController alloc] initWithNibName:@"BaseViewController" bundle:nil];
        webVC.url = urlForDeepLink;
        webVC.title = @"WEB";
        webVC.headerString = @"WEB";
        [[SlideNavigationController sharedInstance] pushViewController:webVC animated:YES];
        
    }else{
       // WebViewController *webVC = [[WebViewController alloc] initWithNibName:@"BaseViewController" bundle:nil];
       // webVC.url = urlForDeepLink;
       // [[SlideNavigationController sharedInstance] pushViewController:webVC animated:YES];
    }
}

#pragma Mark: onSelfServiceMenuClick Delegates
-(void)onSelfServiceMenuClick
{
    [AppUtility checkIsVersionUpgrade];
    
    ManageHolidayPopUpVC* manageHolidayPopUpVC =  [[ManageHolidayPopUpVC alloc] initWithNibName:@"ManageHolidayPopUpVC" bundle:nil];
    
    manageHolidayPopUpVC.htmlPath = @"Manage Holidays";
    manageHolidayPopUpVC.popuptitle = @"Manage Your Booking";
    manageHolidayPopUpVC.managedHolidayDelegate = self;
    [manageHolidayPopUpVC setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    
    [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:manageHolidayPopUpVC animated:false completion:nil];
    
    //  UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
    
    
    //  [AppDelegate presentViewController:manageHolidayPopUpVC animated:false completion:nil];
    
    
    
    /*  let popupVC: PopUpWebViewViewController = PopUpWebViewViewController(nibName: "PopUpWebViewViewController", bundle: nil)
     popupVC.htmlPath = "bookingPolicy"
     popupVC.popuptitle = "Booking Policy"
     popupVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
     self.present(popupVC, animated: false, completion: nil)*/
}

-(void)onContinue
{
    
    [AppUtility setLoginController];
}

-(void)onCancle
{
    
}

@end
