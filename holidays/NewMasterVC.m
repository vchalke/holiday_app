//
//  NewMasterVC.m
//  holidays
//
//  Created by Kush_Tech on 03/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "NewMasterVC.h"
#import "NetworkReachable.h"
#import "SSKeychain.h"
#import "MobiculeUtilityManager.h"
#import "Thomas_Cook_Holidays-Swift.h"
#define KEYCHAIN_SERVICE @"Holiday_App_Keychain"
#define KEYCHAIN_ACCOUNT @"com.thomasCookHoliday.app"

@interface NewMasterVC ()
{
    NSString *mainAppStoreVersion;
    NSString *mainCurrentAppVersion;
}
@end

@implementation NewMasterVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
       [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    self.isNetworkPresent = [[NetworkReachable sharedInstance]checkIsConnected];
    NSLog(self.isNetworkPresent ? @"Network Availability Yes" : @"Network Availability No");
    [[NetworkReachable sharedInstance]obseverNetworkNotification:^(NSString * _Nonnull isNetwork) {
        NSLog(@"Network Status Is %@",isNetwork);
        if ([isNetwork isEqualToString:@"1"]){
            self.isNetworkPresent = YES;
        }else{
            self.isNetworkPresent = NO;
        }
    }];
    
}
- (void)viewWillAppear:(BOOL)animated{
    
}
- (void)viewDidAppear:(BOOL)animated{
    
}
-(void)disableBackSwipeAllow:(BOOL)allow{
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = allow;
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)jumpToPreviousViewController{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)jumpToViewController:(UIViewController*)controller{
    [self.navigationController popToViewController:controller animated:YES];
}
-(void)jumpToRootViewController{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
-(NSLayoutConstraint*)setMultiplier:(NSLayoutConstraint *)changerLayout withMultiplier:(CGFloat)multiplier{
    [NSLayoutConstraint deactivateConstraints:[NSArray arrayWithObjects:changerLayout, nil]];
    
    NSLayoutConstraint *layoutConst = [NSLayoutConstraint constraintWithItem:changerLayout.firstItem attribute:changerLayout.firstAttribute relatedBy:changerLayout.relation toItem:changerLayout.secondItem attribute:changerLayout.secondAttribute multiplier:multiplier constant:changerLayout.constant];
    [layoutConst setPriority:changerLayout.priority];
    layoutConst.shouldBeArchived = changerLayout.shouldBeArchived;
    layoutConst.identifier = changerLayout.identifier;
    layoutConst.active = YES;
    
    [NSLayoutConstraint activateConstraints:[NSArray arrayWithObjects:layoutConst, nil]];
    
    return layoutConst;
}
- (NSAttributedString *)attributedStringForBulletTexts:(NSArray *)stringList
                                              withFont:(UIFont *)font
                                          bulletString:(NSString *)bullet
                                           indentation:(CGFloat)indentation
                                           lineSpacing:(CGFloat)lineSpacing
                                      paragraphSpacing:(CGFloat)paragraphSpacing
                                             textColor:(UIColor *)textColor
                                           bulletColor:(UIColor *)bulletColor {

    NSDictionary *textAttributes = @{NSFontAttributeName: font,
                                 NSForegroundColorAttributeName: textColor};
    NSDictionary *bulletAttributes = @{NSFontAttributeName: font, NSForegroundColorAttributeName: bulletColor};

    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.tabStops = @[[[NSTextTab alloc] initWithTextAlignment: NSTextAlignmentLeft location:indentation options:@{}]];
    paragraphStyle.defaultTabInterval = indentation;
    paragraphStyle.lineSpacing = lineSpacing;
    paragraphStyle.paragraphSpacing = paragraphSpacing;
    paragraphStyle.headIndent = indentation;

    NSMutableAttributedString *bulletList = [NSMutableAttributedString new];

    for (NSString *string in stringList) {
        NSString *formattedString = [NSString stringWithFormat:@"%@\t%@\n", bullet, string];
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:formattedString];
        if (string == stringList.lastObject) {
            paragraphStyle = [paragraphStyle mutableCopy];
            paragraphStyle.paragraphSpacing = 0;
        }
        [attributedString addAttributes:@{NSParagraphStyleAttributeName: paragraphStyle} range:NSMakeRange(0, attributedString.length)];
        [attributedString addAttributes:textAttributes range:NSMakeRange(0, attributedString.length)];

        NSRange rangeForBullet = [formattedString rangeOfString:bullet];
        [attributedString addAttributes:bulletAttributes range:rangeForBullet];
        [bulletList appendAttributedString:attributedString];
    }

    return bulletList;
}
-(NSAttributedString*)getAttributeStringFromHTML:(NSString*)htmlString{
 NSAttributedString *attrStr = [[NSAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:nil error:nil];
    return attrStr;
}
-(void)showAlertViewWithTitle:(NSString *)title withMessage:(NSString *)message
{
    UIAlertController *alertView = [UIAlertController alertControllerWithTitle:title
                                                                       message:message
                                                                preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [alertView dismissViewControllerAnimated:YES completion:nil];
        
    }];
    
    [alertView addAction:okAction];
    [self presentViewController:alertView animated:YES completion:nil];
}

-(void)showButtonsInAlertWithTitle:(NSString*)title msg:(NSString*)message style:(UIAlertControllerStyle)style buttArray:(NSArray*)buttArray completion:(void (^)( NSString *))completion{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:style];
    for (NSString* key in buttArray){
        [alert addAction:[UIAlertAction actionWithTitle:key style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            completion(key);
        }]];
    }
    [self presentViewController:alert animated:YES completion:nil];
}
-(void)showMultipleOptionsInAlertWithTitle:(NSString*)title msg:(NSString*)message style:(UIAlertControllerStyle)style buttArray:(NSArray*)buttArray completion:(void (^)( NSString *))completion{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:style];
    for (NSString* key in buttArray){
        [alert addAction:[UIAlertAction actionWithTitle:key style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            completion(key);
        }]];
    }
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        completion(@"Cancel");
    }];
    [alert addAction:cancelAction];
    [self presentViewController:alert animated:YES completion:nil];
}
- (void)setMaskTo:(UIView*)view byRoundingCorners:(UIRectCorner)corners withCornerRadius:(CGFloat)radiusValue
{
    UIBezierPath *rounded = [UIBezierPath bezierPathWithRoundedRect:view.bounds
                                                  byRoundingCorners:corners
                                                        cornerRadii:CGSizeMake(radiusValue, radiusValue)];
    CAShapeLayer *shape = [[CAShapeLayer alloc] init];
    [shape setPath:rounded.CGPath];
    view.layer.mask = shape;
}
- (void)keyboardWillShow:(NSNotification *)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;

    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.view.frame;
        f.origin.y = -keyboardSize.height;
        self.view.frame = f;
    }];
}

-(void)keyboardWillHide:(NSNotification *)notification
{
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.view.frame;
        f.origin.y = 0.0f;
        self.view.frame = f;
    }];
}
-(NSString*)getNumberFormatterString:(NSInteger)number{
    
    NSNumberFormatter* numberFormatter = [[NSNumberFormatter alloc] init];
     [numberFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_IN"]]; // For India Only
    [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [numberFormatter setGeneratesDecimalNumbers:FALSE];
    [numberFormatter setMaximumFractionDigits:0];
    NSString *numberString = [numberFormatter stringFromNumber: [NSNumber numberWithInteger: number]];
    return numberString;
}
-(UIImage *)coloredImage:(UIImage *)firstImage withColor:(UIColor *)color {
    UIGraphicsBeginImageContext(firstImage.size);

    CGContextRef context = UIGraphicsGetCurrentContext();
    [color setFill];

    CGContextTranslateCTM(context, 0, firstImage.size.height);
    CGContextScaleCTM(context, 1.0, -1.0);

    CGContextSetBlendMode(context, kCGBlendModeCopy);
    CGRect rect = CGRectMake(0, 0, firstImage.size.width, firstImage.size.height);
    CGContextDrawImage(context, rect, firstImage.CGImage);

    CGContextClipToMask(context, rect, firstImage.CGImage);
    CGContextAddRect(context, rect);
    CGContextDrawPath(context,kCGPathElementMoveToPoint);

    UIImage *coloredImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return coloredImg;
}
-(NSString*)convertJsonDictToString:(NSDictionary*)jsonDict{
    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:jsonDict options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
    NSLog(@"%@",myString);
    return myString;
}
-(NSDictionary*)convertStringToJsonDict:(NSString*)string{
    NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    NSLog(@"string: %@",json);
    return json;
}

-(UIToolbar*)getUIToolBarForKeyBoard{
    UIToolbar* keyboardToolbar = [[UIToolbar alloc] init];
    [keyboardToolbar sizeToFit];
    UIBarButtonItem *flexBarButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                      target:nil action:nil];
    UIBarButtonItem *doneBarButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                      target:self action:@selector(yourTextViewDoneButtonPressed:)];
    keyboardToolbar.items = @[flexBarButton, doneBarButton];
    return keyboardToolbar;
}
-(void)yourTextViewDoneButtonPressed:(UIBarButtonItem*)barButton{
    [self.masterDelgate setToolBarActionTextField:barButton];
}

-(void)showToastsWithTitle:(NSString *)title{
    FFToast *toast = [[FFToast alloc]initToastWithTitle:title message:@"" iconImage:nil];
//    toast.toastType = FFToastTypeSuccess;
    toast.duration = 2.5f;
    toast.toastCornerRadius = 3.0f;
    toast.toastBackgroundColor = [UIColor blackColor];
    toast.titleTextColor = [UIColor whiteColor];
    toast.toastPosition = FFToastPositionBottom;
    [toast show:^{
    }];
}
-(void)showToastsWithTitle:(NSString *)title withMessage:(NSString *)message{
    FFToast *toast = [[FFToast alloc]initToastWithTitle:title message:message iconImage:nil];
//    toast.toastType = FFToastTypeSuccess;
    toast.duration = 2.5f;
    toast.toastCornerRadius = 3.0f;
    toast.toastBackgroundColor = [UIColor blackColor];
    toast.titleTextColor = [UIColor whiteColor];
    toast.toastPosition = FFToastPositionBottom;
    [toast show:^{
    }];
}
-(void)callForHolidayEnquiry{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tel:18002099100"]]; //8652908370//1800 209 9100
}
- (void)changeMultiplier:(NSLayoutConstraint *)constraint to:(CGFloat)newMultiplier{
//    NSLayoutConstraint *newConstraint = [NSLayoutConstraint constraintWithItem:(*constraint).firstItem attribute:(*constraint).firstAttribute relatedBy:(*constraint).relation toItem:(*constraint).secondItem attribute:(*constraint).secondAttribute multiplier:newMultiplier constant:(*constraint).constant];
//    [self.view removeConstraint:(*constraint)];
//    [self.view addConstraint:newConstraint];
//    *constraint = newConstraint;
}
-(NSLayoutConstraint *)constraintWithIndientifer:(NSString *)identifer InView:(UIView *)view{
    NSLayoutConstraint * constraintToFind = nil;
    for (NSLayoutConstraint * constraint in view.constraints ) {
        if([constraint.identifier isEqualToString:identifer]){
            constraintToFind = constraint;
            break;
        }
    }
    return constraintToFind;
}
- (BOOL)validateEmailWithString:(NSString*)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}
- (BOOL)validatePhone:(NSString *)phoneNumber
{
//    if ([phoneNumber length]<10){
//        return NO;
//    }
//    NSString *phoneRegex = @"^((\\+)|(00))[0-9]{6,14}$";
//    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
//    return [phoneTest evaluateWithObject:phoneNumber];
    
    NSString *phoneRegex = @"[235689][0-9]{6}([0-9]{3})?";
    NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    BOOL matches = [test evaluateWithObject:phoneNumber];
    return matches;
}
-(NSString*)isCheckValidityInSignIn:(NSDictionary*)signDictionary{
    NSLog(@"signDictionary %@",signDictionary);
    
    for (NSString* key in signDictionary.allKeys)
    {
        NSString *value = [signDictionary objectForKey:key];
        if ([value isEqualToString:@""] || [value length] == 0){
            return @"All fields are mandatory";
        }
    }
    
    if (![self validateEmailWithString:[signDictionary valueForKey:kEnterEmailAddress]]){
        return @"Enter Valid Email Address";
    }
    if ([[signDictionary valueForKey:kEnterPassword] length]<7){
        return @"Password contain more than 7 character";
    }
    if (![[signDictionary valueForKey:kEnterPassword] isEqualToString:[signDictionary valueForKey:kEnterConfirmPassword]]){
        return @"Password and Confirm Password must be same";
    }
    if ([[signDictionary valueForKey:kEnterMobileNumber] length]==10){
        return @"Enter Valid Mobile Number";
    }
    return @"";
}
- (BOOL)checkInternet{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}

-(void)getTokenIDInMaster{
 
     LoadingView *activityLoadingViewRT = [LoadingView loadingViewInView:self.view withString:@"" andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
    {
        
        if ([AppUtility checkNetConnection]) {
        
        @try
        {
            
            if ([SSKeychain passwordForService:KEYCHAIN_SERVICE account:KEYCHAIN_ACCOUNT] == nil)
            {
                NSString *deviceId = [MobiculeUtilityManager getApplicationUUID];
                [SSKeychain setPassword:deviceId forService:KEYCHAIN_SERVICE account:KEYCHAIN_ACCOUNT];
            }
            
            
            NetworkHelper *networkHelper = [[NetworkHelper alloc] init];
            
            NSDictionary *headerDict = [[NSDictionary alloc] initWithObjectsAndKeys:[SSKeychain passwordForService:KEYCHAIN_SERVICE account:KEYCHAIN_ACCOUNT],@"uniqueId",@"mobicule",@"user", nil];
            
           
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                [networkHelper  getResponseWithRequestType:@"" withQueryParam:@"" withPathParam:@"" withJsonParam:nil withHeaders:headerDict withUrl:kAstraUrlNewToken success:^(NSDictionary *responseDict)
                 {
                     dispatch_async(dispatch_get_main_queue(), ^(void) {
                         [activityLoadingViewRT removeFromSuperview];
                         NSLog(@"%@",responseDict);
                         if(responseDict){
                             if (responseDict.count > 0){
                                 NSDictionary *requestId  = [responseDict valueForKey:@"requestId"];
                                 NSDictionary *tokenId  = [responseDict valueForKey:@"tokenId"];
                                 
                                 if (requestId){
                                     [[NSUserDefaults standardUserDefaults] setObject:requestId forKey:kuserDefaultRequestId];
                                 }
                                 if (tokenId){
                                     [[NSUserDefaults standardUserDefaults] setObject:tokenId forKey:kuserDefaultTokenId];
                                     
                                 }
                             }
                             else{
                                 [self showAlertViewWithTitle:@"Alert" withMessage:@"some error occurred"];
                             }
                         }
                         else{
                             [self showAlertViewWithTitle:@"Alert" withMessage:@"some error occurred"];
                         }
                     });
                 }
                                                   failure:^(NSError *error)
                 {
                     dispatch_async(dispatch_get_main_queue(), ^(void)
                                    {
                                        [activityLoadingViewRT removeFromSuperview];
                                        [self showAlertViewWithTitle:@"Alert" withMessage:@"some error occurred"];
                                        NSLog(@"Response Dict : %@",[error description]);
                                    });
                 }];
                
            });
        }
        @catch (NSException *exception)
        {
            NSLog(@"%@", exception.reason);
            [activityLoadingViewRT removeFromSuperview];
        }
        @finally
        {
            NSLog(@"exception finally called");
            //[activityLoadingViewRT removeFromSuperview];
        }
        }else{
            [activityLoadingViewRT removeFromSuperview];
        }
    }
    
    
}


#pragma mark - Checking App Store Update
-(void)checkUpdateAvaialble{
   
    [self checkForUpdateWithHandler:^(BOOL isUpdateAvailable) {
        dispatch_async(dispatch_get_main_queue(), ^{
        if (isUpdateAvailable) {
            [self showUpdateAlertView:mainAppStoreVersion];
        }else{
            NSLog(@"No Update Available");
        }
        });
    }];
}

- (void)checkForUpdateWithHandler:(void(^)(BOOL isUpdateAvailable))updateHandler {
    
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *appID = infoDictionary[@"CFBundleIdentifier"];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://itunes.apple.com/lookup?bundleId=%@", appID]];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *theTask = [session dataTaskWithRequest:[NSURLRequest requestWithURL:url]
                                               completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        NSDictionary *lookup;
        @try {
            lookup =  [self returnDictFromData:data];
        }
        @catch (NSException *e) {
            updateHandler(NO);
            return ;
        }
        mainAppStoreVersion = lookup[@"results"][0][@"version"];
        NSString *mainCurrentVersion  = infoDictionary[@"CFBundleShortVersionString"];
        mainCurrentAppVersion = mainCurrentVersion;
        BOOL isUpdateAvailable = [self checkAppVersion:[self->mainAppStoreVersion floatValue] current:[mainCurrentVersion floatValue]];
//        if (isUpdateAvailable) {
            updateHandler(isUpdateAvailable);
//        }
    }];
    [theTask resume];
}
-(BOOL)checkAppVersion :(float)appVersion current:(float)currenetVersion
{
    if(appVersion>currenetVersion)
        return true;
    else
        return false;
}
-(NSDictionary*)returnDictFromData:(NSData*)data{
    NSDictionary *lookup;
    NSException *exception = [NSException
                              exceptionWithName:@"NullDataException"
                              reason:@"No Data "
                              userInfo:nil];
    if (data==nil) {
        @throw exception;
    }else{
        lookup = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        if (lookup == nil){
            @throw exception;
        }
    }
    return lookup;
}
-(void)showUpdateAlertView:(NSString *)appVersion{
    
    [self fetchStuff:@"New Update" message:[NSString stringWithFormat:@"New Version %@ Available onAppstore",appVersion] style:UIAlertControllerStyleAlert buttNames:[NSArray arrayWithObjects:@"Update",@"Cancel", nil] completion:^(NSString *buttName) {
        if ([buttName isEqualToString:@"Update"]){
            [[UIApplication sharedApplication]openURL:[NSURL URLWithString:@"https://apps.apple.com/in/app/thomas-cook-holiday-packages/id1111576845"] options:@{} completionHandler:nil];
        }
    }];
}

-(void)fetchStuff:(NSString*)title message:(NSString*)message style:(UIAlertControllerStyle)style buttNames:(NSArray*)buttArray completion:(void (^)( NSString *))completion{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:style];
    for (NSString* key in buttArray){
        [alert addAction:[UIAlertAction actionWithTitle:key style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            completion(key);
        }]];
    }
    [self presentViewController:alert animated:YES completion:nil];
}
@end
