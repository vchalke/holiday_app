//
//  NetCoreAnalyticsVC.h
//  holidays
//
//  Created by Sarita on 25/07/17.
//  Copyright © 2017 Mobicule. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetCoreAnalyticsVC : NSObject

+(void)getNetCorePushNotificationDetailDictionary:(NSDictionary *)payLoadDictionary withPayloadCount:(int)payloadCount;

@end
