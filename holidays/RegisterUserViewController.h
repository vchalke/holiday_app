//
//  RegisterUserViewController.h
//  holidays
//
//  Created by ketan on 04/12/17.
//  Copyright © 2017 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol registerUserDelegate <NSObject>

-(void)dismissWithSuccessFullRegistration;

@end

@interface RegisterUserViewController : UIViewController

- (IBAction)onBackButtonClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *textFieldfirstname;
@property (weak, nonatomic) IBOutlet UITextField *textfieldTitle;
@property (weak, nonatomic) IBOutlet UITextField *textFieldLastName;
@property (weak, nonatomic) IBOutlet UITextField *textfieldMobileNumber;
@property (strong,nonatomic) NSDictionary *dictforEncryption;
- (IBAction)onSignUpButtonClicked:(id)sender;
@property (strong,nonatomic) NSString *userId ;
@property (weak,nonatomic) id <registerUserDelegate> delegate;

@property (strong,nonatomic) NSString *signUpVC;

@end
