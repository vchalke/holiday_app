//
//  PDPScreenVC.h
//  holidays
//
//  Created by Kush_Tech on 03/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewMasterVC.h"
#import "HolidayPackageDetail.h"
#import "CoreDataSingleton.h"
#import <CoreMotion/CoreMotion.h>
#import <CoreData/CoreData.h>
#import "Holiday.h"
NS_ASSUME_NONNULL_BEGIN

@interface PDPScreenVC : NewMasterVC<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *base_table_view;
@property (weak, nonatomic) IBOutlet UILabel *lbl_MainTitle;
@property (weak, nonatomic) IBOutlet UIImageView *img_ViewAtTop;
@property (weak, nonatomic) IBOutlet UIButton *btn_Back;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topViewConstarints;
@property (weak, nonatomic) IBOutlet UILabel *lbl_packagePrise;
@property (weak, nonatomic) IBOutlet UIView *baseCompareView;
@property (weak, nonatomic) IBOutlet UIView *compareView;
@property (weak, nonatomic) IBOutlet UIButton *btn_CompareHide;
@property (weak, nonatomic) IBOutlet UIButton *btn_CompareShow;
@property (weak, nonatomic) IBOutlet UIView *offersView;

// Compare View One
@property (weak, nonatomic) IBOutlet UIImageView *compareImgViewOne;
@property (weak, nonatomic) IBOutlet UILabel *lbl_titleOne;
@property (weak, nonatomic) IBOutlet UILabel *lbl_subtitleOne;
@property (weak, nonatomic) IBOutlet UIButton *btn_CompareCancelOne;
@property (weak, nonatomic) IBOutlet UIView *upperCompareViewOne;
@property (weak, nonatomic) IBOutlet UIButton *btn_addNewPackageOne;


// Compare View Two
@property (weak, nonatomic) IBOutlet UIImageView *compareImgViewTwo;
@property (weak, nonatomic) IBOutlet UILabel *lbl_titleTwo;
@property (weak, nonatomic) IBOutlet UILabel *lbl_subtitleTwo;
@property (weak, nonatomic) IBOutlet UIButton *btn_CompareCancelTwo;
@property (weak, nonatomic) IBOutlet UIView *upperCompareViewTwo;
@property (weak, nonatomic) IBOutlet UIButton *btn_addNewPackageTwo;




@property (strong,nonatomic) NSMutableDictionary *dictForCompletePackage;

@property (strong,nonatomic) NSString *headerName;
@property (strong ,nonatomic) HolidayPackageDetail *holidayPackageDetailInPdp;
@property (strong ,nonatomic) Holiday *holidayObjInPdp;
@property (strong,nonatomic) NSArray * completePackageDetail;
@property (strong,nonatomic) NSMutableDictionary *payloadDict;

@property (strong,nonatomic)NSArray *holidayArray;
@property (strong,nonatomic) NSString *searchedCity;
@property(strong,nonatomic)NSString *totalPackages;
@property(strong,nonatomic)NSString *packAgeImgString;
@property (strong,nonatomic) NSDictionary *filterDict;

@property(strong,nonatomic)UILabel *totalPackageLabel;
@property (nonatomic, assign) int pageNumber;
@property (nonatomic) BOOL isFromHomePage;
@property (strong ,nonatomic) NSDictionary *notificationDict;

@property (weak, nonatomic) IBOutlet UIView *base_viewOffer;
@property (weak, nonatomic) IBOutlet UILabel *lbl_offers;

@property (weak, nonatomic) IBOutlet UIView *bottomOfferView;
//#pragma mark - Adding Methoda used in next screen

-(NSArray*)getCommonCitiesFromPackage:(HolidayPackageDetail*)packageDetail packageTypes:(NSString*)packageType withAccomType:(int)accomType;

@end

NS_ASSUME_NONNULL_END
