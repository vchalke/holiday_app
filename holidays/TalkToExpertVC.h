//
//  TalkToExpertVC.h
//  holidays
//
//  Created by Ios_Team on 07/09/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TalkToExpertView.h"
#import "HolidayPackageDetail.h"
NS_ASSUME_NONNULL_BEGIN
@protocol TalkToExpertVCDelegate <NSObject>
@optional
- (void)clickedOptions:(NSInteger)indexNum;
@end
@interface TalkToExpertVC : UIViewController
@property (nonatomic, weak) id <TalkToExpertVCDelegate> delegate;
@property (weak, nonatomic) IBOutlet TalkToExpertView *talkToExpertView;
@property (strong,nonatomic)HolidayPackageDetail *talkToExprHPackage;
@end

NS_ASSUME_NONNULL_END
