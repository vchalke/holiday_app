//
//  ForexRequestCallPopUpViewController.swift
//  holidays
//
//  Created by Parshwanath on 16/01/18.
//  Copyright © 2018 Mobicule. All rights reserved.
//

import UIKit

class ForexRequestCallPopUpViewController: UIViewController,UITextFieldDelegate
{
    @IBOutlet weak var yourReqirementTextField: UITextField!
    @IBOutlet weak var mobileNumberTextField: UITextField!
    
    var yourRequirmentArray:[String] = ["Buy Forex","Sell Forex","Money Transfer"]
    var yourRequirementString : String = ""
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ReloadForexCardViewController.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        
        self.addDoneButtonOnKeyboard()
    }
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle       = UIBarStyle.default
        let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(ForexRequestCallPopUpViewController.doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.mobileNumberTextField.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction()
    {
        self.mobileNumberTextField.resignFirstResponder()
    }

    @IBAction func cancelButtonPress(_ sender: UIButton)
    {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onSubmitRequestCallClick(_ sender: UIButton)
    {
        if validateFields()
        {
            self.requestForCall(mobileNumber: self.mobileNumberTextField.text!, requirementType: yourRequirementString)
//            dismiss(animated: true, completion: nil)
        }
    }
    
    func showAlert(message:NSString) -> Void
    {
        let alert = UIAlertController(title: "Alert", message: message as String, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { UIAlertAction in
            alert.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func isValidMobileNo(testStr:String) -> Bool
    {
        printLog("validate mobileno: \(testStr)")
        let mobRegEx = "^[789]\\d{9}$"
        let mobTest = NSPredicate(format:"SELF MATCHES %@", mobRegEx)
        let result = mobTest.evaluate(with: testStr)
        
        return result
    }
    
    func validateFields() -> Bool
    {
        if (yourReqirementTextField.text?.isEmpty)!
        {
            showAlert(message: "Please select requirement type.")
            return false
        }
        
        if !isValidMobileNo(testStr: mobileNumberTextField.text!)
        {
            showAlert(message: "Please enter valid mobile number.")
            return false
        }
        
        return true
    }
    
    //MARK: Text Field Delegates
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        switch textField
        {
            case self.yourReqirementTextField:
                self.displayYourRequirmentActionSheet()
                return false
            case self.mobileNumberTextField:
                return true
            default:
                return true
        }
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        switch textField
        {
            case self.self.mobileNumberTextField:
                let currentCharacterCount = self.mobileNumberTextField.text?.count ?? 0
                if (range.length + range.location > currentCharacterCount)
                {
                    return false
                }
            
                let newLength = currentCharacterCount + string.count - range.length
                return newLength <= 10
            
            default:
                printLog("no case found for")
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        textField.resignFirstResponder()
    }
    
     //MARK: UI Action Sheets
    func displayYourRequirmentActionSheet()
    {
        let yourRequirmentActionSheet:UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        yourRequirmentActionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:nil))
        
        
        
        for yourRequirment in yourRequirmentArray /*DeviationConstants.PASSANGER_NAMES*/
        {
            
            yourRequirmentActionSheet.addAction(UIAlertAction(title: yourRequirment, style: .default, handler: { (action: UIAlertAction!)in
                
                self.yourReqirementTextField.text = yourRequirment
                self.yourRequirementString = self.yourReqirementTextField.text!
                
                
            }))
            
        }
        self.present(yourRequirmentActionSheet, animated: true, completion: nil)
    }
    
    func dismissKeyboard()
    {
        view.endEditing(true)
    }
    
    func requestForCall(mobileNumber : String, requirementType : String)
    {
        //        https://thomascookindia--tst1.custhelp.com/cgi-bin/thomascookindia.cfg/php/custom/mobile_iosapp_forex_lead.php?mobile=9811976670&requirement_type=Buy&sub_type=Send Money
        
        LoadingIndicatorView.show();
        
        let urlString = String(format: "\(kUrlForCreateLead)mobile=\(String(describing: mobileNumber))&requirement_type=\(String(describing: requirementType))")
        
        let serviceUrl = URL(string: urlString.replacingOccurrences(of: " ", with: "%20"))
        printLog("serviceUrl---> \(String(describing: serviceUrl))")
        
        URLSession.shared.dataTask(with: serviceUrl!, completionHandler:
        {
            (data, response, error) in
         
            DispatchQueue.main.async { () -> Void in
                LoadingIndicatorView.hide()
            }
            
            if(error != nil)
            {
                printLog("error")
            }
            else
            {
                if let data = data, let stringResponse = String(data: data, encoding: .utf8)
                {
                    printLog("Response---< \(stringResponse)")
                    
//                    MessageCode=0|Success|A new opportunity with Id: 4203292 is created and associated with Contact ID: 3917042<br>
                    
                    DispatchQueue.main.async
                    {
                        let alert = UIAlertController(title: "Thank you", message: "We have recieved your details and will get back to you shortly." as String, preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { UIAlertAction in
                            alert.dismiss(animated: true, completion: nil)
                            self.dismiss(animated: true, completion: nil)
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
        }).resume()
    }
}
