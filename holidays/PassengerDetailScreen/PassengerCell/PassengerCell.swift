//
//  PassengerCell.swift
//  sotc-consumer-application
//
//  Created by ketan on 11/09/18.
//  Copyright © 2018 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit

protocol passengerDelegate
{
    func expandShrinkCell(_ sender :UIButton) -> Void
}

class PassengerCell: UITableViewCell,UITextFieldDelegate,DatePickerViewControllerDelegate {
    
    

    var delegate:passengerDelegate?

    @IBOutlet weak var travellerNumberAndTypeLabel: UILabel!
    @IBOutlet weak var viewTitle: UIView!
    @IBOutlet weak var viewOuterContainer: UIView!
    @IBOutlet weak var viewMealType: UIView!
    @IBOutlet weak var viewAdharCard: UIView!
    @IBOutlet weak var viewName: UIView!
    @IBOutlet weak var viewDateOfBirth: UIView!
    @IBOutlet weak var constraintContentHeight: NSLayoutConstraint!
    @IBOutlet weak var buttonExpandSrink: UIButton!
    
    @IBOutlet weak var sideview2: UIView!
    @IBOutlet weak var sideView1: UIView!
    
    @IBOutlet weak var textFieldPassengerTitle: UITextField!
    @IBOutlet weak var textFieldPassengerFirstName: UITextField!
    @IBOutlet weak var textFieldPassengerLastName: UITextField!
    @IBOutlet weak var textFieldPassengerDate: UITextField!
    @IBOutlet weak var textFieldPassengerMonth: UITextField!
    @IBOutlet weak var textFieldPassengerYear: UITextField!
    @IBOutlet weak var textFieldPassengerAadharCard1: UITextField!
    @IBOutlet weak var textFieldPassengerAadharCard2: UITextField!
    @IBOutlet weak var textFieldPassengerAadharCard3: UITextField!
    @IBOutlet weak var textFieldPassengerMealType: UITextField!
    var passengarModel:PassengerModel?
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
    //viewOuterContainer.fadedShadow()
      viewOuterContainer.layer.borderWidth = 1
      viewOuterContainer.layer.borderColor = AppUtility.hexStringToUIColor(hex: "CDCDCD").cgColor
        viewOuterContainer.roundedCorners()
        
        viewTitle.fadedShadow()
        viewTitle.roundedCorners()
        
        viewMealType.fadedShadow()
        viewMealType.roundedCorners()

        viewAdharCard.fadedShadow()
        viewAdharCard.roundedCorners()

        viewName.fadedShadow()
        viewName.roundedCorners()

        viewDateOfBirth.fadedShadow()
        viewDateOfBirth.roundedCorners()
        
        
        viewMealType.fadedShadow()
        viewMealType.roundedCorners()
        textFieldPassengerMealType.setLeftPaddingPoints(5.0)
        
        textFieldPassengerTitle.delegate = self
        textFieldPassengerFirstName.delegate = self
        textFieldPassengerLastName.delegate = self
        textFieldPassengerAadharCard1.delegate = self
        textFieldPassengerAadharCard2.delegate = self
        textFieldPassengerAadharCard3.delegate = self
        textFieldPassengerDate.delegate = self
        textFieldPassengerMonth.delegate = self
        textFieldPassengerYear.delegate = self
        textFieldPassengerMealType.delegate = self
        
        textFieldPassengerFirstName.addDoneButtonOnKeyboard()
        textFieldPassengerLastName.addDoneButtonOnKeyboard()
        textFieldPassengerAadharCard1.addDoneButtonOnKeyboard()
        textFieldPassengerAadharCard2.addDoneButtonOnKeyboard()
        textFieldPassengerAadharCard3.addDoneButtonOnKeyboard()
        textFieldPassengerDate.addDoneButtonOnKeyboard()
        textFieldPassengerMonth.addDoneButtonOnKeyboard()
        textFieldPassengerYear.addDoneButtonOnKeyboard()
        textFieldPassengerMealType.addDoneButtonOnKeyboard()
        textFieldPassengerTitle.setLeftPaddingPoints(5)
        textFieldPassengerFirstName.setLeftPaddingPoints(5)
        textFieldPassengerLastName.setLeftPaddingPoints(5)
        
        let button = UIButton(type: .custom)
        //button.setImage(#imageLiteral(resourceName: "pricingDownArrow"), for: .normal)
        button.isUserInteractionEnabled = false
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
        button.frame = CGRect(x: CGFloat(textFieldPassengerTitle.frame.size.width - 25), y: CGFloat(5), width: CGFloat(20), height: CGFloat(20))
        button.isUserInteractionEnabled = false
        textFieldPassengerTitle.rightView = button
        textFieldPassengerTitle.rightViewMode = .always
        textFieldPassengerTitle.keyboardType = .default
    }
    
    func setData(_ passengerModelLocal:PassengerModel)
    {
        self.textFieldPassengerTitle.text = passengerModelLocal.travellerTitle
        self.textFieldPassengerFirstName.text = passengerModelLocal.travellerFirstName
        self.textFieldPassengerLastName.text = passengerModelLocal.travellerLastName
        self.textFieldPassengerAadharCard1.text = passengerModelLocal.travellerAadharCard1
        self.textFieldPassengerAadharCard2.text = passengerModelLocal.travellerAadharCard2
        self.textFieldPassengerAadharCard3.text = passengerModelLocal.travellerAadharCard3
        self.textFieldPassengerDate.text = passengerModelLocal.travellerBirthDate
        self.textFieldPassengerMonth.text = passengerModelLocal.travellerBirthMonth
        self.textFieldPassengerYear.text = passengerModelLocal.travellerBirthYear
        self.textFieldPassengerMealType.text = passengerModelLocal.travellerMealType
        self.travellerNumberAndTypeLabel.text = "Traveller \(passengerModelLocal.travellerSerialNumber ?? 1) (\(passengerModelLocal.travellerType ?? "" ))"
        
        if self.constraintContentHeight.constant == 0
        {
            //self.constraintContentHeight.constant = 400
            buttonExpandSrink.setImage(UIImage.init(named: "minus.png"), for: .normal)
        }
        else
        {
             buttonExpandSrink.setImage(UIImage.init(named: "plus.png"), for: .normal)
           // self.constraintContentHeight.constant = 0
        }
        
     /*   let button1 = UIButton(type: .custom)
        button1.setImage(#imageLiteral(resourceName: "pricingDownArrow"), for: .normal)
        button1.isUserInteractionEnabled = false
        button1.imageEdgeInsets = UIEdgeInsetsMake(0, -16, 0, 0)
        button1.frame = CGRect(x: CGFloat(textFieldPassengerMealType.frame.size.width - 25), y: CGFloat(5), width: CGFloat(20), height: CGFloat(20))
        button1.isUserInteractionEnabled = false
        textFieldPassengerMealType.rightView = button1
        textFieldPassengerMealType.rightViewMode = .always*/
        textFieldPassengerMealType.keyboardType = .default
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)

    }
    
    @IBAction func onExpandShrinkButtonClicked(_ sender: Any)
    {
        self.delegate?.expandShrinkCell(sender as! UIButton)
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        if textField == self.textFieldPassengerTitle
        {
            let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: "Please select", message: "Title", preferredStyle: .actionSheet)
            let passengerTitleArray:Array<String> = ["Mr.","Mrs.","Ms.","Master"]
            
            
            for passengerTitle in passengerTitleArray
            {
                
                let actionButton  = UIAlertAction(title: passengerTitle, style: .default) { _ in
                    self.textFieldPassengerTitle.text = passengerTitle
                    self.passengarModel?.travellerTitle = passengerTitle
                   // self.nextTextField(textField: textField)
                }
                actionSheetControllerIOS8.addAction(actionButton)
                
            }
            
            let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
                printLog("Cancel")
            }
            actionSheetControllerIOS8.addAction(cancelActionButton)
            let myvc  : PassengerDetailViewController = delegate as! PassengerDetailViewController
            myvc.present(actionSheetControllerIOS8, animated: true, completion: nil)
            
            return false
        }
        else if textField == self.textFieldPassengerMealType
        {
            let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: "Please select", message: "Meal Type", preferredStyle: .actionSheet)
            let passengerMealTypeArray:Array<String> = ["Veg","Non-veg","Jain"]
            
            
            for passengerTitle in passengerMealTypeArray
            {
                
                let actionButton  = UIAlertAction(title: passengerTitle, style: .default) { _ in
                    self.textFieldPassengerMealType.text = passengerTitle
                    self.passengarModel?.travellerMealType = passengerTitle
                }
                actionSheetControllerIOS8.addAction(actionButton)
                
            }
            
            let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
                printLog("Cancel")
            }
            actionSheetControllerIOS8.addAction(cancelActionButton)
            let myvc  : PassengerDetailViewController = delegate as! PassengerDetailViewController
            myvc.present(actionSheetControllerIOS8, animated: true, completion: nil)
            
            return false
        }else if textField == self.textFieldPassengerDate || textField == self.textFieldPassengerMonth || textField == self.textFieldPassengerYear {
            let date:DatePickerViewController = DatePickerViewController(nibName:"DatePickerViewController",bundle:nil)
            date.delegate = self
            date.datetype = "birthdate"
            printLog(passengarModel?.travellerType ?? "")
            date.travellertype = passengarModel?.travellerType ?? ""
           // date.modalPresentationStyle = .overCurrentContext
            //date.modalTransitionStyle = .crossDissolve
            let myvc  : PassengerDetailViewController = delegate as! PassengerDetailViewController
            myvc.modalPresentationStyle = .overCurrentContext
            myvc.modalTransitionStyle = .crossDissolve
            myvc.present(date, animated: true, completion: nil)
            return false
        }
        
         return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.contentView.endEditing(true)
       // self.nextTextField(textField: textField)
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        let totalString:String = "\(textField.text ?? "")\(string)"
        let textFieldString:String = textField.text ?? ""
        printLog("textfiedcount\(textFieldString.count)")
        if textField == self.textFieldPassengerFirstName
        {
            passengarModel?.travellerFirstName = totalString
        }
        else if textField == self.textFieldPassengerLastName
        {
            if range.location < 1 && range.length > 0 && textFieldString.count == 1{
                self.backTextField(textField: textField)
                textField.text = ""
                
                
            }
            
            passengarModel?.travellerLastName = totalString
        }
        else if textField == self.textFieldPassengerAadharCard1
        {
            
            if totalString.count > 4
            {
               //self.nextTextField(textField: textField)
                return false
            }else if range.location < 1 && range.length > 0 && textFieldString.count == 1 {
                self.backTextField(textField: textField)
                textField.text = ""
            }
            
            let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            
            if string == numberFiltered
            {
                 passengarModel?.travellerAadharCard1 = totalString
            }
            
            return string == numberFiltered

        }
        else if textField == self.textFieldPassengerAadharCard2
        {
            if totalString.count > 4
            {
               // self.nextTextField(textField: textField)
                return false
            }else if range.location < 1 && range.length > 0 && textFieldString.count == 1{
                self.backTextField(textField: textField)
                textField.text = ""
            }
            
            let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            
            if string == numberFiltered
            {
                passengarModel?.travellerAadharCard2 = totalString
            }
            
            return string == numberFiltered
            
        }
        else if textField == self.textFieldPassengerAadharCard3
        {
            if totalString.count > 4
            {
                //self.nextTextField(textField: textField)
                return false
            }
            else if range.location < 1 && range.length > 0 && textFieldString.count == 1{
                self.backTextField(textField: textField)
                textField.text = ""
            }
            
            let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            
            if string == numberFiltered
            {
                passengarModel?.travellerAadharCard3 = totalString
            }
            
            return string == numberFiltered
        }
        else if textField == self.textFieldPassengerDate
        {
            
            if totalString.count > 2
            {
               // self.nextTextField(textField: textField)
                return false
            }
           else if range.location < 1 && range.length > 0 && textFieldString.count == 1{
                self.backTextField(textField: textField)
                textField.text = ""
            }
            
            let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            
            if string == numberFiltered
            {
                passengarModel?.travellerBirthDate = totalString
            }
            
            return string == numberFiltered
            
        }
        else if textField == self.textFieldPassengerMonth
        {
            if totalString.count > 2
            {
               // self.nextTextField(textField: textField)
                return false
            }
            else if range.location < 1 && range.length > 0 && textFieldString.count == 1{
                self.backTextField(textField: textField)
                textField.text = ""
            }
            
            let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            
            if string == numberFiltered
            {
                passengarModel?.travellerBirthMonth = totalString
            }
            
            return string == numberFiltered
        }
        else if textField == self.textFieldPassengerYear
        {
            if totalString.count > 4
            {
               // self.nextTextField(textField: textField)
                return false
            }
            else if range.location < 1 && range.length > 0 && textFieldString.count == 1{
                self.backTextField(textField: textField)
                textField.text = ""
            }
            
            
            let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            
            if string == numberFiltered
            {
                passengarModel?.travellerBirthYear = totalString
            }
            
            return string == numberFiltered
            
        }
        
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        printLog("textfielddidbegin\(textField)")
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        printLog("textfildclear\(textField)")
        return true
    }
    func nextTextField(textField:UITextField){
       /* let nextTag:Int = textField.tag + 1
        let nextResponder = textField.superview?.superview?.viewWithTag(nextTag) as UIResponder!
        if nextResponder != nil {
            nextResponder?.becomeFirstResponder()
        }else{
            textField .resignFirstResponder()
        }*/
    }
    
    func backTextField(textField:UITextField){
      /*  let nextTag:Int = textField.tag - 1
        let nextResponder = textField.superview?.superview?.viewWithTag(nextTag) as UIResponder!
        if nextResponder != nil {
            nextResponder?.becomeFirstResponder()
        }else{
            textField .resignFirstResponder()
        }*/
    }
    
    func date(_ controller: DatePickerViewController, text: String) {
        printLog("date in passenger",text)
        
        let arrofbirthdate = text.split(separator: "-")
        printLog(arrofbirthdate)
        self.textFieldPassengerDate.text = "\(arrofbirthdate[0])"
        self.textFieldPassengerMonth.text = "\(arrofbirthdate[1])"
        self.textFieldPassengerYear.text = "\(arrofbirthdate[2])"
        passengarModel?.travellerBirthDate = self.textFieldPassengerDate.text
        passengarModel?.travellerBirthMonth = self.textFieldPassengerMonth.text
        passengarModel?.travellerBirthYear = self.textFieldPassengerYear.text
    }
}
