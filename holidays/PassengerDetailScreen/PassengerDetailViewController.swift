//
//  PassengerDetailViewController.swift
//  sotc-consumer-application
//
//  Created by ketan on 11/09/18.
//  Copyright © 2018 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit

class PassengerDetailViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,passengerDelegate,communicationDelegate {//PayUResponseDelegate 
   
    
    @IBOutlet weak var continueButton: UIButton!
    
    @IBOutlet weak var tableViewBottom: NSLayoutConstraint!
    var packageDetailModel:PackageDetailsModel = PackageDetailsModel.init()
    var pricingModel:PricingModel = PricingModel()
    var quotationModel : QuotationView =  QuotationView.init()
    var passengerArray:Array<PassengerModel> = Array()
    var currentRoomPassengerArray:Array<PassengerModel> = Array()
    var addressForCommModel:AddressForCommunication = AddressForCommunication.init()
    var selectedRoomButton : Int = 11
    var setForExpandedIndex: Set<NSIndexPath> = Set()
    
//    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var passengerDetailTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        printLog("payment type: \(quotationModel.selectedPaymentType)");
        continueButton.roundedCorners()
        let headerNib = UINib.init(nibName: "PassengerHeaderView", bundle: Bundle.main)
        passengerDetailTableView.register(headerNib, forHeaderFooterViewReuseIdentifier: "PassengerHeaderViewIdentifier")
        
        passengerDetailTableView .register(UINib.init(nibName: "PassengerCell", bundle: nil), forCellReuseIdentifier: "PassengerCellIdentifier")
        
         passengerDetailTableView .register(UINib.init(nibName: "AddressCommunicationTableCell", bundle: nil), forCellReuseIdentifier: "AddressCommunicationTableCellIdentifier")
        
        addressForCommModel.mobileNumber = pricingModel.pricingMobileNumber
        
        self.passengerArrayPreparation()
        
        currentRoomPassengerArray = passengerArray.filter({ $0.travellerRoomNumber == 1 })
        
//        headerView.fadedShadow()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        AppUtility.checkVersioning(completionBlock: { (isVersion) in
            
        })
        
    }
    override func viewWillDisappear(_ animated: Bool)
    {
      

    }
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        if self.responds(to: #selector(setter: edgesForExtendedLayout))
        {
            self.edgesForExtendedLayout = []
            
        }
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidLayoutSubviews() {
        
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let keyboardHeight = keyboardSize.height
            //printLog(keyboardHeight)
           // tableViewB    ottom.constant = keyboardHeight - 63
            
            self.passengerDetailTableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height, right: 0)
            
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        //tableViewBottom.constant = 0.0
        self.passengerDetailTableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    //MARK: Tableview delegate datasource
    
    func passengerArrayPreparation()
    {
        for roomModel in pricingModel.pricingArrayRooms
        {
            var generalIndex:Int = 0
            for  _ in 0..<roomModel.adultCount
            {
                generalIndex = generalIndex + 1
                let passengerModel:PassengerModel = PassengerModel()
                passengerModel.travellerRoomNumber = roomModel.roomNumber
                passengerModel.travellerSerialNumber = generalIndex
                passengerModel.travellerType = "ADULT"
                passengerArray.append(passengerModel)
            }
            
            for  _ in 0..<roomModel.childWithBedCount
            {
                generalIndex = generalIndex + 1
                let passengerModel:PassengerModel = PassengerModel()
                passengerModel.travellerRoomNumber = roomModel.roomNumber
                passengerModel.travellerSerialNumber = generalIndex
                passengerModel.travellerType = "CWB"
                passengerArray.append(passengerModel)
            }
            
            for  _ in 0..<roomModel.childWithoutBedCount
            {
                generalIndex = generalIndex + 1
                let passengerModel:PassengerModel = PassengerModel()
                passengerModel.travellerRoomNumber = roomModel.roomNumber
                passengerModel.travellerSerialNumber = generalIndex
                passengerModel.travellerType = "CNB"
                passengerArray.append(passengerModel)
            }
            
            for _ in 0..<roomModel.infantCount
            {
                generalIndex = generalIndex + 1
                let passengerModel:PassengerModel = PassengerModel()
                passengerModel.travellerRoomNumber = roomModel.roomNumber
                passengerModel.travellerSerialNumber = generalIndex
                passengerModel.travellerType = "INFANT"
                passengerArray.append(passengerModel)
            }
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.section == 0
        {
            let cell :PassengerCell = tableView.dequeueReusableCell(withIdentifier: "PassengerCellIdentifier", for: indexPath) as! PassengerCell
            cell.delegate = self
            
            
            let passengerModel = currentRoomPassengerArray[indexPath.row]
            
            if indexPath.row == (currentRoomPassengerArray.count - 1)
            {
                cell.sideView1.isHidden = true
                cell.sideview2.isHidden = true
            }
            else
            {
                cell.sideView1.isHidden = false
                cell.sideview2.isHidden = false
            }
            
            
            if setForExpandedIndex.contains(indexPath as NSIndexPath)
            {
                cell.constraintContentHeight.constant = 510
            }
            else
            {
                cell.constraintContentHeight.constant = 0
            }
            
            
            cell.passengarModel = passengerModel
            
            cell.setData(passengerModel)
            
            return cell
        }
        else
        {
            let cell :AddressCommunicationTableCell = tableView.dequeueReusableCell(withIdentifier: "AddressCommunicationTableCellIdentifier", for: indexPath) as! AddressCommunicationTableCell
            cell.delegate = self as communicationDelegate
            cell.addressForCommuModel = addressForCommModel
            cell.setData(addressForCommModel)
            return cell
        }
       
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        if section == 0
        {
            let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "PassengerHeaderViewIdentifier") as! PassengerHeaderView
            
            let containerView:UIView = headerView.viewWithTag(66) as! UIView
            
            let buttonView:UIView = containerView.viewWithTag(55) as! UIView
            buttonView.layer.cornerRadius = 20
            buttonView.layer.borderWidth = 1
            buttonView.layer.borderColor = UIColor.clear.cgColor
            let leftView:UIView = containerView.viewWithTag(101) as! UIView
            let rightView:UIView = containerView.viewWithTag(102) as! UIView
            let button1 : UIButton = buttonView.viewWithTag(11) as! UIButton
            button1.addTarget(self, action:#selector(self.onRoomButtonClicked(_:)), for: .touchUpInside)
            
            let button2 : UIButton = buttonView.viewWithTag(22) as! UIButton
            button2.addTarget(self, action:#selector(self.onRoomButtonClicked(_:)), for: .touchUpInside)
            
            let button3 : UIButton = buttonView.viewWithTag(33) as! UIButton
            button3.addTarget(self, action:#selector(self.onRoomButtonClicked(_:)), for: .touchUpInside)
            
            
            let button4 : UIButton = buttonView.viewWithTag(44) as! UIButton
            button4.addTarget(self, action:#selector(self.onRoomButtonClicked(_:)), for: .touchUpInside)
            
            
            if pricingModel.pricingArrayRooms.count == 1
            {
                button2.isHidden = true
                button3.isHidden = true
                button4.isHidden = true
            }
            else if pricingModel.pricingArrayRooms.count == 2
            {
                button3.isHidden = true
                button4.isHidden = true
            }
            else if pricingModel.pricingArrayRooms.count == 3
            {
    
                button4.isHidden = true
            }
            
          //  button1.layer.borderColor = AppUtility.hexStringToUIColor(hex: "CDCDCD").cgColor
            
            //button2.layer.borderColor = AppUtility.hexStringToUIColor(hex: "CDCDCD").cgColor
            
            //button3.layer.borderColor = AppUtility.hexStringToUIColor(hex: "CDCDCD").cgColor
            
            //button4.layer.borderColor = AppUtility.hexStringToUIColor(hex: "CDCDCD").cgColor
            
            containerView.layer.cornerRadius = 10
            containerView.layer.borderWidth = 1
            containerView.layer.borderColor = AppUtility.hexStringToUIColor(hex: "CDCDCD").cgColor
            
            setBorderColorWithStackView(buttonView,containerView: containerView)
            
            return headerView
        }
        return nil
       
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if section == 0
        {
           return 180
        }
        return 1
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 0
        {
           return currentRoomPassengerArray.count
        }
        return 1;
    }
    
/*    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        
        if (cell.responds(to: #selector(getter: UIView.tintColor))) {
            let cornerRadius: CGFloat = 5
            cell.backgroundColor = UIColor.clear
            let layer: CAShapeLayer  = CAShapeLayer()
            let pathRef: CGMutablePath  = CGMutablePath()
            let bounds: CGRect  = cell.bounds.insetBy(dx: 8, dy: 0)
            var addLine: Bool  = false
            if (indexPath.row == 0 && indexPath.row == tableView.numberOfRows(inSection: indexPath.section)-1)
            {
                pathRef.__addRoundedRect(transform: nil, rect: bounds, cornerWidth: cornerRadius, cornerHeight: cornerRadius)
            }
            else if (indexPath.row == 0)
            {
               pathRef.move(to: CGPoint(x:bounds.minX,y:bounds.maxY))
               pathRef.addArc(tangent1End: CGPoint(x:bounds.minX,y:bounds.minY), tangent2End: CGPoint(x:bounds.midX,y:bounds.minY), radius: cornerRadius)
                
               pathRef.addArc(tangent1End: CGPoint(x:bounds.maxX,y:bounds.minY), tangent2End: CGPoint(x:bounds.maxX,y:bounds.midY), radius: cornerRadius)
                pathRef.addLine(to: CGPoint(x:bounds.maxX,y:bounds.maxY))
                addLine = true;
            }
            else if (indexPath.row == tableView.numberOfRows(inSection: indexPath.section)-1)
            {
                
                pathRef.move(to: CGPoint(x:bounds.minX,y:bounds.minY))
                pathRef.addArc(tangent1End: CGPoint(x:bounds.minX,y:bounds.maxY), tangent2End: CGPoint(x:bounds.midX,y:bounds.maxY), radius: cornerRadius)
                
                pathRef.addArc(tangent1End: CGPoint(x:bounds.maxX,y:bounds.maxY), tangent2End: CGPoint(x:bounds.maxX,y:bounds.midY), radius: cornerRadius)
                pathRef.addLine(to: CGPoint(x:bounds.maxX,y:bounds.minY))
                
            }
            else
            {
                pathRef.addRect(bounds)
                addLine = true
            }
            layer.path = pathRef
            //CFRelease(pathRef)
            //set the border color
            layer.strokeColor = AppUtility.hexStringToUIColor(hex: "CDCDCD").cgColor;
            //set the border width
            layer.lineWidth = 1
            layer.fillColor = UIColor(white: 1, alpha: 1.0).cgColor
            
            
            if (addLine == true) {
                let lineLayer: CALayer = CALayer()
                let lineHeight: CGFloat  = (1 / UIScreen.main.scale)
                lineLayer.frame = CGRect(x:bounds.minX, y:bounds.size.height-lineHeight, width:bounds.size.width, height:lineHeight)
                lineLayer.backgroundColor = AppUtility.hexStringToUIColor(hex: "CDCDCD").cgColor
                layer.addSublayer(lineLayer)
            }
            
            let testView: UIView = UIView(frame:bounds)
            testView.layer.insertSublayer(layer, at: 0)
            testView.backgroundColor = UIColor.clear
            cell.backgroundView = testView
        }
    }*/
    
    func expandShrinkCell(_ sender: UIButton)
    {
        guard let cell = sender.superview?.superview?.superview as? PassengerCell else
        {
            return // or fatalError() or whatever
        }
        
        let indexPath = self.passengerDetailTableView.indexPath(for: cell)
        
        
        if setForExpandedIndex.contains(indexPath! as NSIndexPath)
        {
            setForExpandedIndex.remove(indexPath! as NSIndexPath)
        }
        else
        {
            setForExpandedIndex.insert(indexPath! as NSIndexPath)
        }
        
       /* if cell.constraintContentHeight.constant == 0
        {
            cell.constraintContentHeight.constant = 400
            sender.setImage(#imageLiteral(resourceName: "MinusSymbol"), for: .normal)
        }
        else
        {
            sender.setImage(#imageLiteral(resourceName: "plusSymbol"), for: .normal)
            cell.constraintContentHeight.constant = 0
        }*/
        
        self.view.layoutIfNeeded()
        
      //  self.passengerDetailTableView.reloadSections(IndexSet(integersIn: 0...0), with: UITableViewRowAnimation.top)

          passengerDetailTableView.reloadData();
    }
    
    func onRoomButtonClicked(_ sender: UIButton)
    {
        
        let buttonView:UIView = sender.superview as! UIView
       
        let button1 : UIButton = buttonView.viewWithTag(11) as! UIButton
        button1.layer.borderWidth = 1
        
        let button2 : UIButton = buttonView.viewWithTag(22) as! UIButton
        button2.layer.borderWidth = 1
        
        let button3 : UIButton = buttonView.viewWithTag(33) as! UIButton
        button3.layer.borderWidth = 1
        
        let button4 : UIButton = buttonView.viewWithTag(44) as! UIButton
        button4.layer.borderWidth = 1

        
        if sender.tag == 11
        {
            currentRoomPassengerArray = passengerArray.filter({ $0.travellerRoomNumber == 1 })
            
            
        }
        else if sender.tag == 22
        {
             currentRoomPassengerArray = passengerArray.filter({ $0.travellerRoomNumber == 2 })
            

        }
        else if sender.tag == 33
        {
             currentRoomPassengerArray = passengerArray.filter({ $0.travellerRoomNumber == 3 })
            

        }
        else if sender.tag == 44
        {
             currentRoomPassengerArray = passengerArray.filter({ $0.travellerRoomNumber == 4 })
            
        }
        
        selectedRoomButton = sender.tag
        
          passengerDetailTableView.reloadData();
    }
    
    func onClickofCity(_ sender: UITextField)
    {
        //UAT- https://resources-uatastra.sotc.in/commonRS/profile.autosuggest/gstStateCities?state=27
        //{27 = gst state code}
        //PRD- https://services.sotc.in/commonRS/profile.autosuggest/gstStateCities?state=27
        
        guard let stateId = addressForCommModel.gstStateCode, stateId != ""
            else {
                AppUtility.displayAlert(message: "Please select State")
                return
        }
        
        
        LoadingIndicatorView.show("Loading")
        
        PreBookingCommunicationManager.getPrebookingData(requestType:"GET", queryParam: "state=\(addressForCommModel.gstStateCode ?? "")", pathParam: nil, jsonParam: nil, headers: nil, requestURL: "commonRS/profile.autosuggest/gstStateCities?", returnInCaseOffailure: false)
        { (status,response) in
            
            LoadingIndicatorView.hide()
            
            printLog("status is \(status)")
            
            printLog("response is \(String(describing: response))")
            if status == true
            {
                if var responseData : Array<Dictionary<String,Any>> = response as? Array<Dictionary<String, Any>>
                {
                    if responseData.count > 0
                    {
                        let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: "Please select", message: "City", preferredStyle: .actionSheet)
                        responseData = responseData.sorted(by: { (dict1 : Dictionary<String,Any>, dict2 : Dictionary<String,Any>) -> Bool in
                            let city1 : String = dict1["cityName"] as? String ?? ""
                            let city2 : String = dict2["cityName"] as? String ?? ""
                            if city1 < city2
                            {
                                return true
                            }
                            return false
                        })
                        for stateDict in responseData
                        {
                            let stateName : String = stateDict["cityName"] as? String ?? ""
                         

                            //let stateCode = stateDict["gstStateCode"]
                            let actionButton  = UIAlertAction(title: (stateName ), style: .default) { _ in
                                sender.text = (stateName )
                                self.addressForCommModel.city = stateName
                                //self.addressForCommModel.gstCityCode = stateCode as? String
                            }
                            actionSheetControllerIOS8.addAction(actionButton)
                            
                        }
                        
                        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
                            printLog("Cancel")
                        }
                        actionSheetControllerIOS8.addAction(cancelActionButton)
                        
                        self.present(actionSheetControllerIOS8, animated: true, completion: nil)
                    }
                    
                }
            }
        }
    }
    
    func onClickofState(_ sender: UITextField)
    {
        //https://services.sotc.in/commonRS/tcilGstService/getGstStateCodes/4
        
        LoadingIndicatorView.show("Loading")
        
        PreBookingCommunicationManager.getPrebookingData(requestType:"GET", queryParam: nil, pathParam: nil, jsonParam: nil, headers: nil, requestURL: "commonRS/tcilGstService/getGstStateCodes/4", returnInCaseOffailure: false)
        { (status,response) in
            
            LoadingIndicatorView.hide()
            
            printLog("status is \(status)")
            
            printLog("response is \(String(describing: response))")
            if status == true
            {
                if let responseData : Array<Dictionary<String,Any>> = response as? Array<Dictionary<String, Any>>
                {
                    if responseData.count > 0
                    {
                        
                        let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: "Please select", message: "State", preferredStyle: .actionSheet)
                        
                        for stateDict in responseData
                        {
                            let stateName = stateDict["gstState"]
                            let stateCode = stateDict["gstStateCode"]
                            let actionButton  = UIAlertAction(title: (stateName as! String), style: .default) { _ in
                                sender.text = (stateName as! String)
                                self.addressForCommModel.city = " "
                                self.addressForCommModel.stateName = stateName as? String
                                self.addressForCommModel.gstStateCode = stateCode as? String
                            }
                            actionSheetControllerIOS8.addAction(actionButton)
                            
                        }
                        
                        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
                            printLog("Cancel")
                        }
                        actionSheetControllerIOS8.addAction(cancelActionButton)
                        
                          self.present(actionSheetControllerIOS8, animated: true, completion: nil)
                    }
                    
                }
            }
        }
    }
    
    @IBAction func onContinueButtonClicked(_ sender: Any)
    {
        
        
        //https://services.sotc.in/holidayRS/holidayBooking
        
        /* {"addressId":0,"bookingAmount":58735,"bookingType":"F","tcilHolidayBookingTravellerDetailCollection":[{"roomNo":1,"position":1,"travellerNo":1,"title":"Mr","firstName":"test","lastName":"test","gender":"M","dob":"01-07-2006","mealPreference":"VEG","paxType":0},{"roomNo":1,"position":2,"travellerNo":2,"title":"Mr","firstName":"test","lastName":"test","gender":"M","dob":"03-07-2006","mealPreference":"VEG","paxType":0}],"quotationId":"658467","title":"Mr","firstName":"test","lastName":"test","addressEmailId":"test@test.com","addressPhoneNo":"7777777777","addressStreet":"test","addressCity":"Mumbai","addressState":"Maharashtra","addressPincode":"407606","packageId":"PKG002815","preConformationPageUrl":"https://www.sotc.in/holidays/holidaysPreConfirmation?hubCode=BLR&pkgId=PKG002815&pkgClassId=0&quotationId=658467&upgradation=0&destination=PKG002815_asia_CONTINENT_1","bookedForEmail":null,"isOnBefalf":"false","bookingThrough":"Desktop","gstStateCode":"27","isUnionTerritory":"N","gstinNo":"","gstinName":""}
         */
        
        if self.checkValidity()
        {
            let passengerModel:PassengerModel = currentRoomPassengerArray.first!
            
            var bookingJsonDict:Dictionary<String,Any> = Dictionary<String,Any>()
            bookingJsonDict.updateValue("0", forKey: "addressId")
            let PaymentAmount = String(Int(quotationModel.selectedPaymentAmount ?? 0.0) ?? 0) ?? "0"
            bookingJsonDict.updateValue(PaymentAmount, forKey: "bookingAmount")
            
            //ADVANCE, FULL_PAYMENT ,USER_CHOICE chengea done on 29March 19
            
            if quotationModel.selectedPaymentType == "ADVANCE"{
                bookingJsonDict.updateValue("A", forKey: "bookingType")
            }else if quotationModel.selectedPaymentType == "FULL_PAYMENT" {
                bookingJsonDict.updateValue("F", forKey: "bookingType")
            }else if quotationModel.selectedPaymentType == "USER_CHOICE"{
                bookingJsonDict.updateValue("U", forKey: "bookingType")
            }else{
                bookingJsonDict.updateValue("F", forKey: "bookingType")
            }
            
            bookingJsonDict.updateValue(quotationModel.quoteID, forKey: "quotationId")
            bookingJsonDict.updateValue(passengerModel.travellerTitle ?? "", forKey: "title")
            bookingJsonDict.updateValue(passengerModel.travellerFirstName ?? "", forKey: "firstName")
            bookingJsonDict.updateValue(passengerModel.travellerLastName ?? "", forKey: "lastName")
            bookingJsonDict.updateValue(addressForCommModel.email ?? "", forKey: "addressEmailId")
            bookingJsonDict.updateValue(pricingModel.pricingMobileNumber, forKey: "addressPhoneNo")
            bookingJsonDict.updateValue(addressForCommModel.address ?? "", forKey: "addressStreet")
            bookingJsonDict.updateValue(addressForCommModel.city ?? "", forKey: "addressCity")
            bookingJsonDict.updateValue(addressForCommModel.stateName ?? "", forKey: "addressState")
            bookingJsonDict.updateValue(addressForCommModel.pincode ?? "", forKey: "addressPincode")
            bookingJsonDict.updateValue(packageDetailModel.packageId, forKey: "packageId")
            bookingJsonDict.updateValue("", forKey: "preConformationPageUrl")
            bookingJsonDict.updateValue(addressForCommModel.email ?? "", forKey: "bookedForEmail")
            bookingJsonDict.updateValue("false", forKey: "isOnBefalf")
            bookingJsonDict.updateValue("APP", forKey: "bookingThrough")
            let gstCode = Int(addressForCommModel.gstStateCode ?? "") ?? 0
            bookingJsonDict.updateValue(gstCode, forKey: "gstStateCode")
            bookingJsonDict.updateValue("N", forKey: "isUnionTerritory")
            bookingJsonDict.updateValue(addressForCommModel.gstinNumber ?? "", forKey: "gstinNo")
            bookingJsonDict.updateValue("", forKey: "gstinName")
            
            let object : PreBookingPayUParameters = PreBookingPayUParameters.init()
            object.customerName = (passengerModel.travellerFirstName ?? "") + (passengerModel.travellerLastName ?? "")
            object.emailID = addressForCommModel.email ?? ""
            object.phoneNo = "\(pricingModel.pricingMobileNumber)"
            object.amount = "\(self.quotationModel.selectedPaymentAmount)"
            
            var arrayForPassengersRoom:Array<Dictionary<String,Any>> = Array<Dictionary<String,Any>>()
            
            var travellerPosition:Int = 0
            
            for passengerModel in passengerArray
            {
                travellerPosition = travellerPosition + 1
                
                var dictForPassenger:Dictionary<String,Any> = Dictionary<String,Any>()
                dictForPassenger.updateValue("\(passengerModel.travellerRoomNumber ?? 0)", forKey: "roomNo")
                dictForPassenger.updateValue( "\(travellerPosition)", forKey: "position")
                dictForPassenger.updateValue("\(passengerModel.travellerSerialNumber ?? 0)", forKey: "travellerNo")
                dictForPassenger.updateValue(passengerModel.travellerTitle ?? "", forKey: "title")
                
               if (passengerModel.travellerTitle ?? "") == "Mr."
                {
                    dictForPassenger.updateValue("M", forKey: "gender")
                }
                else
                {
                    dictForPassenger.updateValue("F", forKey: "gender")
                }
                
                dictForPassenger.updateValue(passengerModel.travellerFirstName ?? "", forKey: "firstName")
                dictForPassenger.updateValue(passengerModel.travellerLastName ?? "", forKey: "lastName")
                dictForPassenger.updateValue("\(passengerModel.travellerBirthDate ?? "")-\(passengerModel.travellerBirthMonth ?? "")-\(passengerModel.travellerBirthYear ?? "")", forKey: "dob")
                
                
                if let mealType = passengerModel.travellerMealType
                {
                    if mealType == "Veg"
                    {
                         dictForPassenger.updateValue("VEG", forKey: "mealPreference")
                    }
                    else
                    {
                         dictForPassenger.updateValue("NONVEG", forKey: "mealPreference")
                    }
                }
                else
                {
                    dictForPassenger.updateValue("", forKey: "mealPreference")
                }
                    
                
                if passengerModel.travellerType == "ADULT"
                {
                    dictForPassenger.updateValue(NSNumber.init(value: 0), forKey: "paxType")
                }
                else if passengerModel.travellerType == "CNB" || passengerModel.travellerType == "CWB"
                {
                    dictForPassenger.updateValue(NSNumber.init(value: 1), forKey: "paxType")
                }
                else
                {
                    dictForPassenger.updateValue(NSNumber.init(value: 2), forKey: "paxType")
                }
                
                
                arrayForPassengersRoom.append(dictForPassenger)
            }
            
            
            bookingJsonDict.updateValue(arrayForPassengersRoom, forKey: "tcilHolidayBookingTravellerDetailCollection")
            
            
            LoadingIndicatorView.show("Loading")
            
            PreBookingCommunicationManager.getPrebookingData(requestType:"POST", queryParam: nil, pathParam: nil, jsonParam: bookingJsonDict, headers: nil, requestURL: "holidayRS/holidayBooking", returnInCaseOffailure: false)
            { (status,response) in
                
                LoadingIndicatorView.hide()
                
                printLog("status is \(status)")
                
                printLog("response is \(String(describing: response))")
                if status == true
                {
                    if let responseData : Dictionary<String,Any> = response as? Dictionary<String, Any>
                    {
                        if responseData.count > 0
                        {
                            //self.startPayment(passengerDetail: object)
                            if let transactionID = responseData["tid"] as? String
                            {
                                self.redirectToPaymentGateway(transactionID: transactionID)
                                
                                return
                            }
                            
                            if let message = responseData["message"] as? String
                            {
                                AppUtility.displayAlert(message: message)
                                return
                            }
                        }
                    }
                }
                 AppUtility.displayAlert(message: "some error occuered")
            }
        }
    }
    
    func setBorderColorWithStackView(_ buttonView:UIView, containerView:UIView)
    {
        let leftView:UIView = containerView.viewWithTag(101) as! UIView
        let rightView:UIView = containerView.viewWithTag(102) as! UIView
        
        let button1 : UIButton = buttonView.viewWithTag(11) as! UIButton
        button1.layer.borderWidth = 1.5
        
        let button2 : UIButton = buttonView.viewWithTag(22) as! UIButton
        button2.layer.borderWidth = 1.5
        
        let button3 : UIButton = buttonView.viewWithTag(33) as! UIButton
        button3.layer.borderWidth = 1.5
        
        let button4 : UIButton = buttonView.viewWithTag(44) as! UIButton
        button4.layer.borderWidth = 1.5
        
        
       if selectedRoomButton == 11
        {
            leftView.layer.cornerRadius = 10
            leftView.layer.borderWidth = 1.5
            leftView.layer.borderColor = AppUtility.hexStringToUIColor(hex: "CC0000").cgColor
            rightView.layer.borderColor = AppUtility.hexStringToUIColor(hex: "CDCDCD").cgColor
            button1.layer.borderColor = AppUtility.hexStringToUIColor(hex: "CC0000").cgColor
            button2.layer.borderColor = AppUtility.hexStringToUIColor(hex: "CDCDCD").cgColor
            button3.layer.borderColor = AppUtility.hexStringToUIColor(hex: "CDCDCD").cgColor
            button4.layer.borderColor = AppUtility.hexStringToUIColor(hex: "CDCDCD").cgColor
            
        }
        else if selectedRoomButton == 22
        {
            leftView.layer.borderColor = AppUtility.hexStringToUIColor(hex: "CDCDCD").cgColor
            rightView.layer.borderColor = AppUtility.hexStringToUIColor(hex: "CDCDCD").cgColor
            button1.layer.borderColor = AppUtility.hexStringToUIColor(hex: "CDCDCD").cgColor
            button2.layer.borderColor = AppUtility.hexStringToUIColor(hex: "CC0000").cgColor
            button3.layer.borderColor = AppUtility.hexStringToUIColor(hex: "CDCDCD").cgColor
            button4.layer.borderColor = AppUtility.hexStringToUIColor(hex: "CDCDCD").cgColor
            
        }
        else if selectedRoomButton == 33
        {
            leftView.layer.borderColor = AppUtility.hexStringToUIColor(hex: "CDCDCD").cgColor
            rightView.layer.borderColor = AppUtility.hexStringToUIColor(hex: "CDCDCD").cgColor
            button1.layer.borderColor = AppUtility.hexStringToUIColor(hex: "CDCDCD").cgColor
            button2.layer.borderColor = AppUtility.hexStringToUIColor(hex: "CDCDCD").cgColor
            button3.layer.borderColor = AppUtility.hexStringToUIColor(hex: "CC0000").cgColor
            button4.layer.borderColor = AppUtility.hexStringToUIColor(hex: "CDCDCD").cgColor
            
        }
        else if selectedRoomButton == 44
        {
            rightView.layer.cornerRadius = 10
            rightView.layer.borderWidth = 1.5
            rightView.layer.borderColor = AppUtility.hexStringToUIColor(hex: "CC0000").cgColor
            leftView.layer.borderColor = AppUtility.hexStringToUIColor(hex: "CDCDCD").cgColor
            button1.layer.borderColor = AppUtility.hexStringToUIColor(hex: "CDCDCD").cgColor
            button2.layer.borderColor = AppUtility.hexStringToUIColor(hex: "CDCDCD").cgColor
            button3.layer.borderColor = AppUtility.hexStringToUIColor(hex: "CDCDCD").cgColor
            button4.layer.borderColor = AppUtility.hexStringToUIColor(hex: "CC0000").cgColor
        }
    }
    
    
    func checkValidity() -> Bool
    {
       for modelTraveller in  passengerArray
       {
        if modelTraveller.travellerTitle == ""
        {
            AppUtility.displayAlert(message: "Enter title from room \(modelTraveller.travellerRoomNumber ?? 0) and passenger \(modelTraveller.travellerSerialNumber ?? 0)")
            return false
        }
        else if modelTraveller.travellerFirstName == ""
        {
            AppUtility.displayAlert(message: "Enter first name from room \(modelTraveller.travellerRoomNumber ?? 0) and passenger \(modelTraveller.travellerSerialNumber ?? 0)")
            return false
        }
        else if  modelTraveller.travellerLastName == ""
        {
            AppUtility.displayAlert(message: "Enter last name from room \(modelTraveller.travellerRoomNumber ?? 0) and passenger \(modelTraveller.travellerSerialNumber ?? 0)")
            return false
        }
//        else if modelTraveller.travellerAadharCard1 == ""
//        {
//            AppUtility.displayAlert(message: "Enter Aadhar card details from room \(modelTraveller.travellerRoomNumber ?? 0) and passenger \(modelTraveller.travellerSerialNumber ?? 0)")
//            return false
//        }
//        else if modelTraveller.travellerAadharCard2 == ""
//        {
//            AppUtility.displayAlert(message: "Enter Aadhar card details from room \(modelTraveller.travellerRoomNumber ?? 0) and passenger \(modelTraveller.travellerSerialNumber ?? 0)")
//            return false
//        }
//        else if modelTraveller.travellerAadharCard3 == ""
//        {
//            AppUtility.displayAlert(message: "Enter Aadhar card details from room \(modelTraveller.travellerRoomNumber ?? 0) and passenger \(modelTraveller.travellerSerialNumber ?? 0)")
//            return false
//        }
        else if modelTraveller.travellerBirthDate == ""
        {
            AppUtility.displayAlert(message: "Enter dob from room \(modelTraveller.travellerRoomNumber ?? 0) and passenger \(modelTraveller.travellerSerialNumber ?? 0)")
            return false
        }
        else if modelTraveller.travellerBirthMonth == ""
        {
            AppUtility.displayAlert(message: "Enter dob from room \(modelTraveller.travellerRoomNumber ?? 0) and passenger \(modelTraveller.travellerSerialNumber ?? 0)")
            return false
        }
        else if modelTraveller.travellerBirthYear == ""
        {
            AppUtility.displayAlert(message: "Enter dob from room \(modelTraveller.travellerRoomNumber ?? 0) and passenger \(modelTraveller.travellerSerialNumber ?? 0)")
            return false
        }
        else if modelTraveller.travellerMealType == ""
        {
            AppUtility.displayAlert(message: "Enter meal type from room \(modelTraveller.travellerRoomNumber ?? 0) and passenger \(modelTraveller.travellerSerialNumber ?? 0)")
                                                                                                                                                                                                                                                                                                                                return false
        }
//        else
//        {
//
//            let dateOfBirth:String = (modelTraveller.travellerBirthYear  ?? "0000") + "-" + (modelTraveller.travellerBirthMonth ?? "00") + "-" + (modelTraveller.travellerBirthDate ?? "00") //+ "00:00:00" + "+0000";
//
//            //▿ 2018-12-22 13:48:14 +0000
//
//            if let passengerDate:Date =  AppUtility.convertStringToDate(string: dateOfBirth, format: "yyyy-MM-dd") as? Date
//            {
//                let dateDifference = AppUtility.getDaysBetweenTwoDates(firstDate:passengerDate, secondDate: Date()) //Date().getYear() - Int(modelTraveller.travellerBirthYear ?? "00")! //
//
//                if modelTraveller.travellerType ?? "" == "ADULT" && dateDifference ?? 0 <= (12*365)
//                {
//                     AppUtility.displayAlert(message: "Select adult age more than 12 years old")
//                    return false
//                }
//                else if (modelTraveller.travellerType ?? "" == "CNB" || modelTraveller.travellerType ?? "" == "CWB") && (dateDifference ?? 0 < (2*365) || dateDifference ?? 0 > (12*365))
//                {
//                     AppUtility.displayAlert(message: "Select child age between 2 to 11 years old")
//                    return false
//                }
//                else if (modelTraveller.travellerType ?? "" == "INFANT" ) && ( dateDifference ?? 0 < 0 || (dateDifference ?? 0 > (2*365)))
//                {
//                     AppUtility.displayAlert(message: "Select infant age less than 2 years")
//                    return false
//                }
//            }
//
//        }
        }
        
        
        if addressForCommModel.address == ""
        {
            AppUtility.displayAlert(message: "Enter address")
            return false
        }
        else if addressForCommModel.city == ""
        {
            AppUtility.displayAlert(message: "Enter City name")
            return false
        }
        else if addressForCommModel.pincode == ""
        {
            AppUtility.displayAlert(message: "Enter pincode")
            return false
        }
        else if addressForCommModel.email == ""
        {
            AppUtility.displayAlert(message: "Enter E-mail")
            return false
        }
        else if !AppUtility.isValidEmail(testStr: addressForCommModel.email!)
        {
            AppUtility.displayAlert(message: "Enter valid E-mail")
            return false
        }
        else if addressForCommModel.mobileNumber == ""
        {
            AppUtility.displayAlert(message: "Enter mobile number")
            return false
        }
        else if !AppUtility.isValidMobileNumber(MobileNumber: addressForCommModel.mobileNumber!)
        {
            AppUtility.displayAlert(message: "Enter valid mobile number")
            return false
        }
        else if addressForCommModel.stateName == ""
        {
            AppUtility.displayAlert(message: "Enter state name")
            return false
        }
        
        return true
    }
    @IBAction func onBackClicked(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    //Mark:- Redirect to Payment Gateway
    
    func redirectToPaymentGateway(transactionID:String)
    {
//        let viewController:paymentPackageViewController = paymentPackageViewController(nibName: "paymentPackageViewController", bundle: nil)
        
        let redirectionURL = AuthenticationConstant.PG_URL + "productId=4&invoiceNo=" + transactionID
        if let url = URL(string: redirectionURL) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:],completionHandler:{(success) -> Void in
                    
                    // When download completes,control flow goes here.
                    if success {
                        self.navigationController?.popToRootViewController(animated: true)
                    } else {
                    
                    }
                    
                })
            } else {
                UIApplication.shared.openURL(url)
                self.navigationController?.popToRootViewController(animated: true)
            }
        }
       
//        viewController.fromModuleTitle = Title.TRAVELER_DETAILS
//        viewController.pageTitle = Title.PAYMENT
//
//        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    
    
    //MARK: - Payment Methods -
    /*
    func startPayment(passengerDetail : PreBookingPayUParameters)
    {
        if AppUtility.checkNetConnection()
        {
            
            LoadingIndicatorView.show()
                
            let paymentController:PaymentController = PaymentController.init(delegateViewController: self)
                paymentController.payUResponseDelegate = self
          
            paymentController.startPaymentForPrebookingFlow(amount: passengerDetail.amount, passengerDetail:passengerDetail)
                
        }
      
    }
    */
    func payUSucessReceived(strConvertedRespone:String)
    {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        printLog("payUSucessReceivedNotification")
        
        
        var merchantHash = String()
        // var strConvertedRespone = "\(notification.object!)"
        
        var _ : NSDictionary = try! JSONSerialization.jsonObject(with: strConvertedRespone.data(using: String.Encoding.utf8)!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
        
        if  let responseDict:Dictionary<String,Any> = try! JSONSerialization.jsonObject(with: strConvertedRespone.data(using: String.Encoding.utf8)!, options: JSONSerialization.ReadingOptions.mutableContainers) as? Dictionary<String,Any>
        {
            printLog(responseDict)
            
            if let status:String = responseDict["status"] as? String
            {
                if "success".caseInsensitiveCompare(status) == .orderedSame
                {
                    printLog("The transaction is successful")
                    AppUtility.displayAlertController(title: "Alert", message: AlertMessage.PAYMENT_SUCCESS)
                }
                else if "failure".caseInsensitiveCompare(status) == .orderedSame
                {
                    LoadingIndicatorView.hide()
                    _ = self.navigationController?.popToViewController(self, animated: true)
                    AppUtility.displayAlertController(title: "Alert", message: AlertMessage.PAYMENT_FAILED)
                }
                else
                {
                    LoadingIndicatorView.hide()
                    _ = self.navigationController?.popToViewController(self, animated: true)
                    AppUtility.displayAlertController(title: "Alert", message: AlertMessage.PAYMENT_FAILED)
                }
                
            }
            
            
        }else
        {
            
            LoadingIndicatorView.hide()
            _ = self.navigationController?.popToViewController(self, animated: true)
             AppUtility.displayAlertController(title: "Alert", message: AlertMessage.PAYMENT_FAILED)
        }
        
    }
    func payUFailedReceived()
    {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        AppUtility.displayAlertController(title: "Alert", message: "Payment Failed.")
        printLog("payUFailedReceivedNotification")
        LoadingIndicatorView.hide()
        _ = self.navigationController?.popToViewController(self, animated: true)
    }
}

extension UIButton{
    
    
    func roundedButton(){
        let maskPath1 = UIBezierPath(roundedRect: bounds,
                                     byRoundingCorners: [.topLeft , .topRight],
                                     cornerRadii: CGSize(width: 8, height: 8))
        let maskLayer1 = CAShapeLayer()
        maskLayer1.borderColor = UIColor.red.cgColor
        maskLayer1.borderWidth = 1.0
        maskLayer1.fillColor = UIColor.red.cgColor
        UIColor.blue.setStroke()
        maskLayer1.frame = bounds
        maskLayer1.path = maskPath1.cgPath
        self.layer.mask = maskLayer1
    }
}
