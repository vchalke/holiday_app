//
//  PassengerModel.swift
//  sotc-consumer-application
//
//  Created by ketan on 20/09/18.
//  Copyright © 2018 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit

class PassengerModel: NSObject
{
    var travellerRoomNumber:Int?
    var travellerType:String? = ""
    var travellerSerialNumber:Int?
    var travellerTitle:String? = ""
    var travellerFirstName:String? = ""
    var travellerLastName:String? = ""
    var travellerBirthDate:String? = ""
    var travellerBirthMonth:String? = ""
    var travellerBirthYear:String? = ""
    var travellerAadharCard1:String? = ""
    var travellerAadharCard2:String? = ""
    var travellerAadharCard3:String? = ""
    var travellerMealType:String? = ""
    var travellerPanCard:String? = ""

}
