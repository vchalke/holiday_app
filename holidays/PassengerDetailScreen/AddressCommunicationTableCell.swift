//
//  AddressCommunicationTableCell.swift
//  sotc-consumer-application
//
//  Created by ketan on 21/09/18.
//  Copyright © 2018 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit
protocol communicationDelegate
{
    func onClickofState(_ sender:UITextField)
    func onClickofCity(_ sender:UITextField)
    
}

class AddressCommunicationTableCell: UITableViewCell,UITextFieldDelegate,UITextViewDelegate {

    var addressForCommuModel:AddressForCommunication? = AddressForCommunication.init()
    var delegate:communicationDelegate?
    
    @IBOutlet weak var textFieldGSTINNumber: UITextField!
    @IBOutlet weak var textFieldState: UITextField!
    @IBOutlet weak var textFieldMobile: UITextField!
    @IBOutlet weak var textFieldEmail: UITextField!
    @IBOutlet weak var buttonExistingProfile: UIButton!
    @IBOutlet weak var textFieldPinCode: UITextField!
    @IBOutlet weak var textFieldCity: UITextField!
    @IBOutlet weak var textViewAddress: UITextView!
    override func awakeFromNib()
    {
        textFieldGSTINNumber.superview?.fadedShadow()
        textFieldGSTINNumber.roundedCorners()
        textFieldGSTINNumber.addDoneButtonOnKeyboard()
        
        textFieldState.superview?.fadedShadow()
        textFieldState.superview?.roundedCorners()
        textFieldState.addDoneButtonOnKeyboard()
        
        textFieldMobile.superview?.fadedShadow()
        textFieldMobile.superview?.roundedCorners()
        textFieldMobile.addDoneButtonOnKeyboard()
        
        textFieldEmail.superview?.fadedShadow()
        textFieldEmail.superview?.roundedCorners()
        textFieldEmail.addDoneButtonOnKeyboard()
        
        textFieldPinCode.superview?.fadedShadow()
        textFieldPinCode.superview?.roundedCorners()
        textFieldPinCode.addDoneButtonOnKeyboard()
        
        textFieldCity.superview?.fadedShadow()
        textFieldCity.superview?.roundedCorners()
        textFieldCity.addDoneButtonOnKeyboard()

        textViewAddress.fadedShadow()
        textViewAddress.roundedCorners()
        
        buttonExistingProfile.treatAsCheckBox()
        
        super.awakeFromNib()
    }
    
    func setData(_ communnicationModel:AddressForCommunication)
    {
        textFieldCity.text = communnicationModel.city
        textFieldEmail.text = communnicationModel.email
        textFieldState.text = communnicationModel.stateName
        textFieldPinCode.text = communnicationModel.pincode
        textFieldMobile.text = communnicationModel.mobileNumber
        textFieldGSTINNumber.text = communnicationModel.gstinNumber
        textViewAddress.text = communnicationModel.address
        
        if communnicationModel.isExistingUseAddress!
        {
            buttonExistingProfile.isSelected = true
        }
        else
        {
            buttonExistingProfile.isSelected = false
        }
        
        
        
        textFieldCity.delegate = self
        textFieldEmail.delegate = self
        textFieldState.delegate = self
        textFieldPinCode.delegate = self
        textFieldMobile.delegate = self
        textFieldGSTINNumber.delegate = self
        textViewAddress.delegate = self
        
        /*let button = UIButton(type: .custom)
        button.setImage(#imageLiteral(resourceName: "pricingDownArrow"), for: .normal)
        button.imageEdgeInsets = UIEdgeInsetsMake(0, -16, 0, 0)
        button.frame = CGRect(x: CGFloat(textFieldState.frame.size.width - 25), y: CGFloat(5), width: CGFloat(20), height: CGFloat(20))
        textFieldState.rightView = button
        textFieldState.rightViewMode = .always
        textFieldState.keyboardType = .default*/

    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
      if textField  == self.textFieldState
      {
        textFieldCity.text = " "
        self.delegate?.onClickofState(textField)
        
        return false
      }
        else if textField == self.textFieldCity
      {
        self.delegate?.onClickofCity(textField)
        
        return false
       }
        return true
    }
    
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        let totalString:String = "\(textField.text ?? "")\(string)"
        if textField == self.textFieldEmail
        {
            addressForCommuModel?.email = totalString
        }
        else if textField == self.textFieldPinCode
        {
            if totalString.count > 6
            {
                return false
            }
            
            let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            
            if string == numberFiltered
            {
                 addressForCommuModel?.pincode = totalString
            }
            
            return string == numberFiltered
            
           
        }
        else if textField == self.textFieldMobile
        {
            let textFieldText: NSString = (textField.text ?? "") as NSString
            
            var txtAfterUpdate = textFieldText.replacingCharacters(in: range, with: string)
            let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let newStr = compSepByCharInSet.joined(separator: "")
            if newStr.count == 0 && string != ""
            {
                return false
            }
            if ((txtAfterUpdate.count) <= 10)
            {
                
                if ((txtAfterUpdate.count) == 10)
                {
                    
                    self.textFieldMobile.text = txtAfterUpdate
                     addressForCommuModel?.mobileNumber = txtAfterUpdate
                    textField.resignFirstResponder()
                    return false
                }
                else
                {
                     addressForCommuModel?.mobileNumber = txtAfterUpdate
                    return true
                }
                
            }
            else
            {
                textField.resignFirstResponder()
                return false
            }
            /*
            if totalString.count > 10
            {
                return false
            }
            
            let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            
            if string == numberFiltered
            {
                 addressForCommuModel?.mobileNumber = "\(self.textFieldMobile)\(string)"
            }
            
            return string == numberFiltered*/
        }
        else if textField == self.textFieldGSTINNumber
        {
            addressForCommuModel?.gstinNumber = totalString
        }
       
        return true
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        let totalString:String = "\(textView.text ?? "")\(text)"
        if textView == self.textViewAddress
        {
            addressForCommuModel?.address = totalString
        }
        return true
    }
    
    @IBAction func onCheckmarkExisting(_ sender: Any)
    {
        let checkMarkButton = sender as! UIButton
        
        if checkMarkButton.isSelected
        {
            checkMarkButton.isSelected = false
            addressForCommuModel?.isExistingUseAddress = false
        }
        else
        {
            checkMarkButton.isSelected = true
            addressForCommuModel?.isExistingUseAddress = true
        }
        
    }
    
}
