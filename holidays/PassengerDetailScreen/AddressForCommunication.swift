//
//  AddressForCommunication.swift
//  sotc-consumer-application
//
//  Created by ketan on 22/09/18.
//  Copyright © 2018 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit

class AddressForCommunication: NSObject
{
    var isExistingUseAddress:Bool? = false
    var address:String? = ""
    var city:String? = ""
    var pincode:String? = ""
    var email:String? = ""
    var mobileNumber:String? = ""
    var stateName:String? = ""
    var gstinNumber:String? = ""
    var gstStateCode:String? = ""
    var gstCityCode:String? = ""
    override init() {
        let userDetails : Dictionary<String,Any>? = UserDefaults.standard.dictionary(forKey: Constant.userProfile.userDetails)
        if (userDetails != nil)
        {
            self.email = userDetails?["email"] as? String ?? ""
        }
       
    }
}
