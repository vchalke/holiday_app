//
//  UserDetailViewCell.swift
//  holidays
//
//  Created by Kush_Team on 15/06/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

import UIKit
protocol UserDetailViewCellDelegate {
    func openTextField(txtField:UITextField)
}
class UserDetailViewCell: UITableViewCell {
    var currentRoomPassengerModel: PassengerModel?
    var addressForCommModel:AddressForCommunication?
     var pricingModel:PricingModel = PricingModel()
       var packageDetailModel:PackageDetailsModel?
       var reloadDelegate : ReloadPricing? = nil
//    var statesData : [[String:Any]] = Array()
    var statesData = [[String:Any]]()
    @IBOutlet weak var txt_fields: UITextField!
     @IBOutlet weak var btn_ontext: UIButton!
    @IBOutlet weak var lnl_info: UILabel!
//    var stateSheetController: UIAlertController?
    var delegates : UserDetailViewCellDelegate? = nil
    var datePicker : UIDatePicker?
    override func awakeFromNib() {
        super.awakeFromNib()
         
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    

    
    
    func setTextField(tagg:Int, strArray:[String]){
        self.lnl_info.isHidden = true
        self.txt_fields.delegate = self
        self.txt_fields.tag = tagg
        self.txt_fields.placeholder = strArray[tagg]
       setRightImage(identifier: strArray[tagg])
    }
    func setRightImage(identifier:String){
        switch identifier {
        case "Date Of Birth":
            self.txt_fields.setLeftImgInTextField(leftImage:UIImage(named: "PricingCalender") ?? nil)
            break
        case "Meal Preference":
            self.txt_fields.setLeftImgInTextField(leftImage:UIImage(named: "dropdowncutout") ?? nil)
            break
        case "City Of Residence":
            self.txt_fields.setLeftImgInTextField(leftImage: nil)
            break
        case "State":
            self.txt_fields.setLeftImgInTextField(leftImage:UIImage(named: "dropdowncutout") ?? nil)
            break
        default:
            self.txt_fields.setLeftImgInTextField(leftImage: nil)
            break
        }
    }
    func showDepartureCityList(){
        var arrayDepartureCity :Array<Dictionary<String,Any>>
        
        var filteredArray = Array<Dictionary<String,Any>>()
        if packageDetailModel?.packageSubTypeName == "fit"{
            arrayDepartureCity = (packageDetailModel?.tcilHolidayPriceCollection)!
            
            filteredArray = AppUtility.filterArray(ArrayToBeFilter: arrayDepartureCity, withKey: "packageClassId", AndStringValue: "\(self.pricingModel.packageClassId)")
            arrayDepartureCity = self.uniqcity(tcilHolidayPriceCollection: arrayDepartureCity)
        }else{
            arrayDepartureCity = (packageDetailModel?.ltPricingCollection)!
            
            filteredArray = arrayDepartureCity
        }
        
        let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: "Please select", message: "Departure City", preferredStyle: .actionSheet)
        arrayDepartureCity = AppUtility.sortHubCity(hubArray: filteredArray)
        let reducedSubtitles = arrayDepartureCity.reduce([[String: Any]]()) { (result, current) in
            
            if packageDetailModel?.packageSubTypeName == "fit"{
                if result.contains(where: {
                    ($0["hubCityCode"] as! Dictionary<String, Any>)["cityName"] as! String == (current["hubCityCode"] as! Dictionary<String, Any>)["cityName"] as! String }) {
                    return result
                }
            }else{
                if result.contains(where: {
                    ($0["hubCode"] as! Dictionary<String, Any>)["cityName"] as! String == (current["hubCode"] as! Dictionary<String, Any>)["cityName"] as! String }) {
                    return result
                }
            }
            
            return result + [current]
        }


        
        
        
        for packageCityDict in reducedSubtitles
        {
            var packageCity:String
            var hubCodeDict:Dictionary<String,Any> = Dictionary.init()
            if packageDetailModel?.packageSubTypeName == "fit"
            {
                hubCodeDict = packageCityDict["hubCityCode"] as! Dictionary
                packageCity = hubCodeDict["cityName"] as! String
            }
            else
            {
                hubCodeDict = packageCityDict["hubCode"] as! Dictionary
                packageCity = hubCodeDict["cityName"] as! String
            }
            
            
            let actionButton  = UIAlertAction(title: packageCity, style: .default) { _ in
                self.txt_fields.text = packageCity
                self.pricingModel.pricingDepartureCity = packageCity
                if let hubcode : String = hubCodeDict[PricingConstants.CityCode] as? String
                {
                    self.pricingModel.pricinghubCode = hubcode
                    self.addressForCommModel?.city = packageCity
                    
                }
                self.pricingModel.pricingTourDate = ""
                self.reloadDelegate?.reloadTable()
            }
            
            actionSheetControllerIOS8.addAction(actionButton)
            
        }
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            printLog("Cancel")
        }
        actionSheetControllerIOS8.addAction(cancelActionButton)
        
        self.window?.rootViewController?.present(actionSheetControllerIOS8, animated: true, completion: nil)
        
    
    }
    
    func uniqcity(tcilHolidayPriceCollection:Array<Dictionary<String,Any>>) -> Array<Dictionary<String,Any>> {
        var tempSet  = Set<String>()
        for dict in tcilHolidayPriceCollection
        {
            let hubCodeDict : Dictionary<String,Any> = dict["hubCityCode"] as? Dictionary<String,Any> ?? Dictionary.init()
            let packageCity : String = hubCodeDict["cityName"] as? String ?? ""
            tempSet.insert(packageCity)
        }
        var tempArray : Array<Dictionary<String, Any>> = Array .init()
        for hubcity in Array(tempSet)
        {
            let filteredArray : Array<Dictionary<String,Any>> = tcilHolidayPriceCollection.filter({ (dict : Dictionary<String,Any>) -> Bool in
                let hubCodeDict : Dictionary<String,Any> = dict["hubCityCode"] as? Dictionary<String,Any> ?? Dictionary.init()
                let packageCity : String = hubCodeDict["cityName"] as? String ?? ""
                if packageCity == hubcity
                {
                    return true
                }
                return false
            })
            
            if filteredArray.count > 0
            {
                tempArray.append(filteredArray.first!)
            }
        }
        //
        
        return tempArray
    }
    func uniq<S : Sequence, T : Hashable>(source: S) -> [T] where S.Iterator.Element == T {
        var buffer = [T]()
        var added = Set<T>()
        for elem in source {
            if !added.contains(elem) {
                buffer.append(elem)
                added.insert(elem)
            }
        }
        return buffer
    }
    /*
    var statesData : Array<StatesModel>?{
        didSet{
           stateSheetController = UIAlertController(title: "Please select", message: "Departure State", preferredStyle: .actionSheet)
            guard let values = statesData else { return }
            for packagestates in values{
                let gstCode = packagestates.gstStateCode
                let stateName = packagestates.gstState
                let actionButton  = UIAlertAction(title: stateName, style: .default) { _ in
                    self.txt_fields.text = stateName
                    self.addressForCommModel?.gstStateCode = "\(String(describing: gstCode))"
                    self.addressForCommModel?.stateName = stateName
                }
                stateSheetController.addAction(actionButton)
                
            }
            let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
                printLog("Cancel")
            }
            stateSheetController!.addAction(cancelActionButton)
            
        }
    }
    func showStatesList(){

        self.window?.rootViewController?.present(stateSheetController!, animated: true, completion: nil)
    }
    */
     func showStatesList(){
             let stateSheetController: UIAlertController = UIAlertController(title: "Please select", message: "Departure State", preferredStyle: .actionSheet)
             
             for packagestates in statesData{
                if let gstCode = packagestates["gstStateCode"] , let stateName = packagestates["gstState"]{
                    let actionButton  = UIAlertAction(title: stateName as! String, style: .default) { _ in
                     self.txt_fields.text = stateName as! String
                     self.addressForCommModel?.gstStateCode = "\(String(describing: gstCode))"
                     self.addressForCommModel?.stateName = stateName as! String
                 }
                 stateSheetController.addAction(actionButton)
                }
             }
             let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
                 printLog("Cancel")
             }
             stateSheetController.addAction(cancelActionButton)
             
             self.window?.rootViewController?.present(stateSheetController, animated: true, completion: nil)
     }
    
    func showMealReferenceList(){
        let mealType = ["VEG","NON VEG","JAIN"]
            let stateSheetController: UIAlertController = UIAlertController(title: "Please select", message: "Departure State", preferredStyle: .actionSheet)
            for meal in mealType{
                let actionButton  = UIAlertAction(title: meal, style: .default) { _ in
                    self.txt_fields.text = meal
                    self.currentRoomPassengerModel?.travellerMealType = meal
                }
                stateSheetController.addAction(actionButton)
                
            }
            let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
                printLog("Cancel")
            }
            stateSheetController.addAction(cancelActionButton)
            
            self.window?.rootViewController?.present(stateSheetController, animated: true, completion: nil)
        
    }
    func showDatePicker(){
        let maximumDate = Calendar.current.date(byAdding: .year,value: -18,to: Date())
//        let calendar = Calendar(identifier: .gregorian)
//        let currentDate = Date()
//        var components = DateComponents()
//        components.calendar = calendar
//        components.year = -18 eur
//        let maximumDate = calendar.date(byAdding: components, to: currentDate)!
        self.datePicker = UIDatePicker()
        self.datePicker?.datePickerMode = .date
//        self.datePicker?.maximumDate = Date()
        self.datePicker?.maximumDate = maximumDate
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelPicker));
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        self.txt_fields.inputAccessoryView = toolbar
        self.txt_fields.inputView = datePicker
    }
    @objc func donePicker(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        if let selectedDate = self.datePicker?.date {
            self.txt_fields.text = formatter.string(from: selectedDate )
        }
        self.txt_fields.resignFirstResponder()
        self.datePicker?.endEditing(true)
    }
    
    @objc func cancelPicker(){
        self.txt_fields.resignFirstResponder()
        self.datePicker?.endEditing(true)
    }
}

extension UserDetailViewCell:UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        if (textField.tag == 0){

        }else if (textField.tag == 1){

        }else if (textField.tag == 2){
            
        }else if (textField.tag == 3){
            textField.resignFirstResponder()
        }else if (textField.tag == 4){

        }else if (textField.tag == 5){
            textField.resignFirstResponder()
        }else if (textField.tag == 6){
            textField.resignFirstResponder()
        }
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if (textField.tag == 0){
            currentRoomPassengerModel?.travellerFirstName = textField.text
        }else if (textField.tag == 1){
            currentRoomPassengerModel?.travellerLastName = textField.text
        }else if (textField.tag == 2){
           currentRoomPassengerModel?.travellerBirthDate = textField.text
        }else if (textField.tag == 3){
            currentRoomPassengerModel?.travellerMealType = textField.text
        }else if (textField.tag == 4){
//            self.lnl_info.isHidden = !(textField.text?.count==0)
//            self.lnl_info.text = (textField.text?.count==0) ? "Enter Valid PAN Card Number" : ""
//            currentRoomPassengerModel?.travellerPanCard = (textField.text?.count==0) ? "" : textField.text
            let enterString = textField.text
//            if(enterString?.count == 10 && AppUtility.containsSpecialCharacters(string:enterString ?? "")==false){
            if(AppUtility.validatePANCardNumber(enterString ?? "")==true){
                self.lnl_info.isHidden = true
                self.lnl_info.text = ""
                currentRoomPassengerModel?.travellerPanCard = textField.text
            }else{
                self.lnl_info.isHidden = false
                self.lnl_info.text = (packageDetailModel?.packageTypeId == 1) ? "Enter Valid PAN Card Number" : ""
                currentRoomPassengerModel?.travellerPanCard = ""
            }
        }else if (textField.tag == 5){
            addressForCommModel?.city = textField.text
        }else if (textField.tag == 6){
            addressForCommModel?.stateName = textField.text
        }else if (textField.tag == 7){
//            self.lnl_info.isHidden = !(!AppUtility.isValidEmail(testStr: textField.text ?? "") || textField.text?.count==0)
//            self.lnl_info.text = (textField.text?.count==0) ? "Enter Valid Email-ID" : ""
//            addressForCommModel?.email = (textField.text?.count==0) ? "" : textField.text
            let enterString = textField.text
            if(AppUtility.isValidEmail(testStr: textField.text ?? "") && enterString?.count != 0){
                self.lnl_info.isHidden = true
                self.lnl_info.text = ""
                addressForCommModel?.email = textField.text
            }else{
                self.lnl_info.isHidden = false
                self.lnl_info.text = "Enter Valid Email-ID"
                addressForCommModel?.email = ""
            }
            printLog(AppUtility.isValidEmail(testStr: addressForCommModel?.email ?? "asdf"))
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
       
            if (textField.tag == 0){
                
            }else if (textField.tag == 1){
                
            }else if (textField.tag == 2){
                self.showDatePicker()
            }else if (textField.tag == 3){
                textField.resignFirstResponder()
                self.showMealReferenceList()
            }else if (textField.tag == 4){
                
            }else if (textField.tag == 5){
//                textField.resignFirstResponder()
//                self.showDepartureCityList()
            }else if (textField.tag == 6){
                textField.resignFirstResponder()
                self.showStatesList()
            }
        }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
}
