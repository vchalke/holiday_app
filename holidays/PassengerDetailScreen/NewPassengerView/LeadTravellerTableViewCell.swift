//
//  LeadTravellerTableViewCell.swift
//  holidays
//
//  Created by Kush_Team on 15/06/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

import UIKit

class LeadTravellerTableViewCell: UITableViewCell {
    var currentRoomPassengerModel:PassengerModel?
    var addressForCommModel:AddressForCommunication?
    @IBOutlet weak var btn_mr: UIButton!
    @IBOutlet weak var btn_mrs: UIButton!
    @IBOutlet weak var btn_dr: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        btn_mr.addTarget(self,action:#selector(buttonClicked),
                         for:.touchUpInside)
        btn_mrs.addTarget(self,action:#selector(buttonClicked),
                         for:.touchUpInside)
        btn_dr.addTarget(self,action:#selector(buttonClicked),
                         for:.touchUpInside)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @objc func buttonClicked(sender:UIButton)
    {
        self.btn_mr.setImage(UIImage.init(named: (sender.tag == 0) ? "circularBoxSelectBlue" : "circularBoxNoSelectBlue"), for: .normal)
        self.btn_mrs.setImage(UIImage.init(named: (sender.tag == 1) ? "circularBoxSelectBlue" : "circularBoxNoSelectBlue"), for: .normal)
        self.btn_dr.setImage(UIImage.init(named: (sender.tag == 2) ? "circularBoxSelectBlue" : "circularBoxNoSelectBlue"), for: .normal)
        
        currentRoomPassengerModel?.travellerTitle = (sender.tag == 0) ? "Mr" : (sender.tag == 1) ? "Mrs" : "Dr"
        
    }
    
}
