//
//  PassengerDetailVC.swift
//  holidays
//
//  Created by Kush_Team on 15/06/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

import UIKit

class PassengerDetailVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UserDetailViewCellDelegate  {
    var optionalActivityArray : Array<OptionalModel> = Array.init()
    @IBOutlet weak var table_travellerDetail: UITableView!
    var packageDetailModel:PackageDetailsModel = PackageDetailsModel.init()
    var pricingModel:PricingModel = PricingModel()
    var quotationModel : QuotationView =  QuotationView.init()
    var passengerArray:Array<PassengerModel> = Array()
//    var statesArray : [[String:Any]] = Array()
    var statesArray = [[String:Any]]()
    var currentRoomPassengerArray:Array<PassengerModel> = Array()
    var currentRoomPassengerModel: PassengerModel = PassengerModel.init()
    var addressForCommModel:AddressForCommunication = AddressForCommunication.init()
    var selectedRoomButton : Int = 11
    var setForExpandedIndex: Set<NSIndexPath> = Set()
    let txtStrArray = ["First Name","Last Name","Date Of Birth","Meal Preference","PAN Card","City Of Residence","State","Email ID"]
    override func viewDidLoad() {
        super.viewDidLoad()
        addressForCommModel.mobileNumber = pricingModel.pricingMobileNumber
        addressForCommModel.email = ""
//        addressForCommModel.city = pricingModel.pricingDepartureCity
        printLog("payment type: \(quotationModel.selectedPaymentType)");
        self.table_travellerDetail.register(UINib.init(nibName: "LeadTravellerTableViewCell", bundle: Bundle.main), forCellReuseIdentifier:"LeadTravellerTableViewCell")
        let nib = UINib(nibName: "PriceDistributionHeader", bundle: nil)
        self.table_travellerDetail.register(nib, forHeaderFooterViewReuseIdentifier: "PriceDistributionHeader")
        self.table_travellerDetail.register(UINib(nibName: "UserDetailViewCell", bundle: Bundle.main), forCellReuseIdentifier: "UserDetailViewCell")
        self.table_travellerDetail.register(UINib(nibName: "PackageDescriptionViewCell", bundle: Bundle.main), forCellReuseIdentifier: "PackageDescriptionViewCell")
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        self.passengerArrayPreparation()
        currentRoomPassengerArray = passengerArray.filter({ $0.travellerRoomNumber == 1 })
        self.getStatesList()
        AppUtility.checkVersioning(completionBlock: { (isVersion) in
            
        })
    }

    override func viewWillDisappear(_ animated: Bool){
    
    }
    override func viewWillAppear(_ animated: Bool){
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        if self.responds(to: #selector(setter: edgesForExtendedLayout)){
            self.edgesForExtendedLayout = []
        }
        self.quotationModel.expandPriceAndRoomingDetail = true // false VJ_Change
        self.table_travellerDetail.reloadData()
    }
    override func didReceiveMemoryWarning(){
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidLayoutSubviews() {
        
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            self.table_travellerDetail.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height, right: 0)
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        //tableViewBottom.constant = 0.0
        self.table_travellerDetail.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }

    //MARK:-  Tableview Delegates -
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
//            return (self.quotationModel.expandPriceAndRoomingDetail) ?  txtStrArray.count+2:txtStrArray.count+1
            return txtStrArray.count+2
        }
        
        func numberOfSections(in tableView: UITableView) -> Int{
            return 1
        }
        func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
            return 100
        }
        func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
            if section == 0{
                return 70//UITableView.automaticDimension//70
            }
            return 0
    }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
//            if (indexPath.row == 0 && self.quotationModel.expandPriceAndRoomingDetail){
            if (indexPath.row == 0){
                let cell = tableView.dequeueReusableCell(withIdentifier: "PackageDescriptionViewCell") as! PackageDescriptionViewCell
                cell.setData(quotationView:  self.quotationModel, pricingModel: self.pricingModel, optionalArray:optionalActivityArray)
                cell.hideConatentView(flags: !self.quotationModel.expandPriceAndRoomingDetail)
                return cell;
//            }else if (indexPath.row == 1 && self.quotationModel.expandPriceAndRoomingDetail){
            }else if (indexPath.row == 1){
                let cell :LeadTravellerTableViewCell = tableView.dequeueReusableCell(withIdentifier: "LeadTravellerTableViewCell", for: indexPath) as! LeadTravellerTableViewCell
                cell.addressForCommModel = self.addressForCommModel
                self.currentRoomPassengerModel.travellerTitle = "Mr"
                cell.currentRoomPassengerModel = self.currentRoomPassengerModel
                return cell;
            }
            let cell :UserDetailViewCell = tableView.dequeueReusableCell(withIdentifier: "UserDetailViewCell", for: indexPath) as! UserDetailViewCell
            cell.delegates = self
            cell.statesData = statesArray
            cell.packageDetailModel = self.packageDetailModel
            cell.addressForCommModel = self.addressForCommModel
            cell.currentRoomPassengerModel = self.currentRoomPassengerModel
            cell.setTextField(tagg:indexPath.row-2, strArray: txtStrArray)
            return cell;
        }
  
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?{
        let cell : PriceDistributionHeader = tableView.dequeueReusableHeaderFooterView(withIdentifier: "PriceDistributionHeader") as! PriceDistributionHeader
        cell.containerView.shadowAtBottom()
        let numFormatter : NumberFormatter = NumberFormatter.init()
        numFormatter.numberStyle = NumberFormatter.Style.decimal
        numFormatter.locale = Locale(identifier: "en_IN")
        let isAfterPromoApplyAmount = pricingModel.applyPromocodeSuccess ? (Int(pricingModel.totalTourCost) ?? 15000) - Int(pricingModel.promocodeAmount) : Int(pricingModel.totalTourCost) ?? 0
        cell.tourCostLabel.attributedText = self.getRuppesString(value: isAfterPromoApplyAmount)
        cell.packageNameLabel.text = self.packageDetailModel.pkgName
        cell.addressLabel.text = pricingModel.pricingDepartureCity + ", " + AppUtility.convertDateToString(date: AppUtility.convertStringToDate(string: pricingModel.pricingTourDate, format: "dd-MM-yyyy"), dateFormat: "dd MMMM,yyyy")
        cell.arrowButton.addTarget(self, action: #selector(self.toggleArrowView(_:)), for: UIControl.Event.touchUpInside)
        let menuItemTapGestureRecognizer = UITapGestureRecognizer(target: self, action:#selector(self.onTapOfCellView(_:)))
        menuItemTapGestureRecognizer.numberOfTapsRequired = 1
        menuItemTapGestureRecognizer.numberOfTouchesRequired = 1
        cell.containerView.addGestureRecognizer(menuItemTapGestureRecognizer)
        cell.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
        cell.setImageAndButtonImg(flag: self.quotationModel.expandPriceAndRoomingDetail)
        return cell
    }
    func openTextField(txtField: UITextField) {
        
    }
    func getRuppesString(value:Int)->NSMutableAttributedString{
        let numFormatter : NumberFormatter = NumberFormatter.init()
        numFormatter.numberStyle = NumberFormatter.Style.decimal
        numFormatter.locale = Locale(identifier: "en_IN")
        let boldFontAttributes = [NSAttributedString.Key.foregroundColor: AppUtility.hexStringToUIColor(hex: "#0054A5"), NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 18)]//0037A3
        let normalFontAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14)]
        let partOne = NSMutableAttributedString(string: "₹ ", attributes: normalFontAttributes)
        let partTwo = NSMutableAttributedString(string: (numFormatter.string(from: NSNumber.init(value:value)) ?? "0"), attributes: boldFontAttributes)
        //            let partTwo = NSMutableAttributedString(string: (numFormatter.string(from: NSNumber.init(value:Int(pricingModel.totalTourCost) ?? 0)) ?? "0"), attributes: boldFontAttributes)
        let combinationTwo = NSMutableAttributedString()
        combinationTwo.append(partOne)
        combinationTwo.append(partTwo)
        return combinationTwo
    }
    @objc func onTapOfCellView(_ sender: UITapGestureRecognizer){
        self.quotationModel.expandPriceAndRoomingDetail = !self.quotationModel.expandPriceAndRoomingDetail
        self.table_travellerDetail.reloadData()
        self.table_travellerDetail.reloadSections(IndexSet.init(integer: 0), with: .automatic)
        DispatchQueue.main.async {
            let indexPath = IndexPath(item: 0, section: 0)
           self.table_travellerDetail.scrollToRow(at: indexPath, at: .top, animated: true)
        }
    }
    @objc func toggleArrowView(_ sender : UIButton){
        self.quotationModel.expandPriceAndRoomingDetail = !self.quotationModel.expandPriceAndRoomingDetail
        self.table_travellerDetail.reloadData()
        self.table_travellerDetail.reloadSections(IndexSet.init(integer: 0), with: .automatic)
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat{
        if (indexPath.row == 0 && self.quotationModel.expandPriceAndRoomingDetail){
            return UITableView.automaticDimension
        }else if (indexPath.row == 0 && !self.quotationModel.expandPriceAndRoomingDetail){
            return 0;
        }else if (indexPath.row == 1){
            return 150;
        }else if (indexPath.row > 1){
            return 70;
        }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
//                return (indexPath.row == 0) ? UITableView.automaticDimension : 70
        if (indexPath.row == 0 && self.quotationModel.expandPriceAndRoomingDetail){
            return UITableView.automaticDimension
        }else if (indexPath.row == 0 && !self.quotationModel.expandPriceAndRoomingDetail){
            return 0;
        }else if (indexPath.row == 1){
            return 150;
        }else if (indexPath.row > 1){
            return 70;
        }
        return UITableView.automaticDimension
    }
    func reloadTable(){
        self.table_travellerDetail.reloadData()
    }
    @IBAction func btn_backPress(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btn_callPress(_ sender: Any) {
        AppUtility.callToTCFromPhaseTwo()
    }
    @IBAction func btn_continuePress(_ sender: Any) {
                self.onContinueButtonClicked()
    }
    func jumpOnConfirmationBoking(dictionary : Dictionary<String,Any>){
        let bookingConfirmVC : BookingConfirmOne = BookingConfirmOne(nibName: "BookingConfirmOne", bundle: nil)
        bookingConfirmVC.packageDetailModel = self.packageDetailModel
        bookingConfirmVC.pricingModel = self.pricingModel
        bookingConfirmVC.bookingConfirmJsonDict = dictionary
        self.navigationController?.pushViewController(bookingConfirmVC, animated: true)
    }
    
    func passengerArrayPreparation(){
        for roomModel in pricingModel.pricingArrayRooms{
            var generalIndex:Int = 0
            for  _ in 0..<roomModel.adultCount{
                generalIndex = generalIndex + 1
                let passengerModel:PassengerModel = PassengerModel()
                passengerModel.travellerRoomNumber = roomModel.roomNumber
                passengerModel.travellerSerialNumber = generalIndex
                passengerModel.travellerType = "ADULT"
                if (roomModel.roomNumber == 1 && generalIndex == 1){
                    passengerModel.travellerPanCard = ""
                }
                passengerArray.append(passengerModel)
            }
            for  _ in 0..<roomModel.childWithBedCount{
                generalIndex = generalIndex + 1
                let passengerModel:PassengerModel = PassengerModel()
                passengerModel.travellerRoomNumber = roomModel.roomNumber
                passengerModel.travellerSerialNumber = generalIndex
                passengerModel.travellerType = "CWB"
                passengerArray.append(passengerModel)
            }
            for  _ in 0..<roomModel.childWithoutBedCount{
                generalIndex = generalIndex + 1
                let passengerModel:PassengerModel = PassengerModel()
                passengerModel.travellerRoomNumber = roomModel.roomNumber
                passengerModel.travellerSerialNumber = generalIndex
                passengerModel.travellerType = "CNB"
                passengerArray.append(passengerModel)
            }
            for _ in 0..<roomModel.infantCount{
                generalIndex = generalIndex + 1
                let passengerModel:PassengerModel = PassengerModel()
                passengerModel.travellerRoomNumber = roomModel.roomNumber
                passengerModel.travellerSerialNumber = generalIndex
                passengerModel.travellerType = "INFANT"
                passengerArray.append(passengerModel)
            }
        }
    }
    func setValidation()->String{
        printLog("travellerMealType \(currentRoomPassengerModel.travellerMealType ?? "NOMEAL")")
        if (currentRoomPassengerModel.travellerFirstName?.count==0){
          return "Enter Lead Traveller First Name"
        }
        if (currentRoomPassengerModel.travellerLastName?.count==0){
          return "Enter Lead Traveller Last Name"
        }
        if (currentRoomPassengerModel.travellerBirthDate?.count==0){
          return "Enter Lead Traveller Birth Date"
        }
        if (currentRoomPassengerModel.travellerMealType?.count==0){
          return "Enter Lead Traveller Meal Type"
        }
//        if ((currentRoomPassengerModel.travellerPanCard?.count != 10 || AppUtility.containsSpecialCharacters(string:currentRoomPassengerModel.travellerPanCard ?? "")==true) && packageDetailModel.packageTypeId == 1){
//          return "Enter Valid Lead Traveller Pan Card Detail"
//        }
        if (AppUtility.validatePANCardNumber(currentRoomPassengerModel.travellerPanCard ?? "")==false && packageDetailModel.packageTypeId == 1){
          return "Enter Valid Lead Traveller Pan Card Detail"
        }
        if (addressForCommModel.city?.count==0){
          return "Enter Lead Traveller City"
        }
        if (addressForCommModel.stateName?.count==0){
          return "Enter Lead Traveller State"
        }
        if (AppUtility.isValidEmail(testStr: addressForCommModel.email ?? "") == false){
          return "Enter Valid Lead Traveller Email"
        }
        return ""
    }
    func onContinueButtonClicked(){
        //https://services.sotc.in/holidayRS/holidayBooking
        /* {"addressId":0,"bookingAmount":58735,"bookingType":"F","tcilHolidayBookingTravellerDetailCollection":[{"roomNo":1,"position":1,"travellerNo":1,"title":"Mr","firstName":"test","lastName":"test","gender":"M","dob":"01-07-2006","mealPreference":"VEG","paxType":0},{"roomNo":1,"position":2,"travellerNo":2,"title":"Mr","firstName":"test","lastName":"test","gender":"M","dob":"03-07-2006","mealPreference":"VEG","paxType":0}],"quotationId":"658467","title":"Mr","firstName":"test","lastName":"test","addressEmailId":"test@test.com","addressPhoneNo":"7777777777","addressStreet":"test","addressCity":"Mumbai","addressState":"Maharashtra","addressPincode":"407606","packageId":"PKG002815","preConformationPageUrl":"https://www.sotc.in/holidays/holidaysPreConfirmation?hubCode=BLR&pkgId=PKG002815&pkgClassId=0&quotationId=658467&upgradation=0&destination=PKG002815_asia_CONTINENT_1","bookedForEmail":null,"isOnBefalf":"false","bookingThrough":"Desktop","gstStateCode":"27","isUnionTerritory":"N","gstinNo":"","gstinName":""}
         */
        let validStr = self.setValidation()
        if (validStr.count == 0){
            let passengerModel:PassengerModel = currentRoomPassengerArray.first!
            var bookingJsonDict:Dictionary<String,Any> = Dictionary<String,Any>()
            bookingJsonDict.updateValue("0", forKey: "addressId")
            let PaymentAmount = String(Int(quotationModel.selectedPaymentAmount ?? 0.0) ?? 0) ?? "0"
            bookingJsonDict.updateValue(PaymentAmount, forKey: "bookingAmount")
            
            //ADVANCE, FULL_PAYMENT ,USER_CHOICE chengea done on 29March 19
            
            if quotationModel.selectedPaymentType == "ADVANCE"{
                bookingJsonDict.updateValue("A", forKey: "bookingType")
            }else if quotationModel.selectedPaymentType == "FULL_PAYMENT" {
                bookingJsonDict.updateValue("F", forKey: "bookingType")
            }else if quotationModel.selectedPaymentType == "USER_CHOICE"{
                bookingJsonDict.updateValue("U", forKey: "bookingType")
            }else{
                bookingJsonDict.updateValue("F", forKey: "bookingType")
            }
            bookingJsonDict.updateValue(quotationModel.quoteID, forKey: "quotationId")
            
            //        bookingJsonDict.updateValue(passengerModel.travellerTitle ?? "", forKey: "title")
            //        bookingJsonDict.updateValue(passengerModel.travellerFirstName ?? "", forKey: "firstName")
            //        bookingJsonDict.updateValue(passengerModel.travellerLastName ?? "", forKey: "lastName")
            
            bookingJsonDict.updateValue(currentRoomPassengerModel.travellerTitle ?? "", forKey: "title")
            bookingJsonDict.updateValue(currentRoomPassengerModel.travellerFirstName ?? "", forKey: "firstName")
            bookingJsonDict.updateValue(currentRoomPassengerModel.travellerLastName ?? "", forKey: "lastName")
            let gstCode = Int(addressForCommModel.gstStateCode ?? "") ?? 0
            bookingJsonDict.updateValue(gstCode, forKey: "gstStateCode")
            bookingJsonDict.updateValue(addressForCommModel.email ?? "", forKey: "addressEmailId")
            bookingJsonDict.updateValue(addressForCommModel.address ?? "", forKey: "addressStreet")
            bookingJsonDict.updateValue(addressForCommModel.city ?? "", forKey: "addressCity")
            bookingJsonDict.updateValue(addressForCommModel.stateName ?? "", forKey: "addressState")
            bookingJsonDict.updateValue(addressForCommModel.pincode ?? "", forKey: "addressPincode")
            bookingJsonDict.updateValue(addressForCommModel.email ?? "", forKey: "bookedForEmail")
            bookingJsonDict.updateValue(addressForCommModel.gstinNumber ?? "", forKey: "gstinNo")
            /*
             bookingJsonDict.updateValue("Mr", forKey: "title")
             bookingJsonDict.updateValue("Vijay", forKey: "firstName")
             bookingJsonDict.updateValue("Chalke", forKey: "lastName")
             let gstCode = Int(addressForCommModel.gstStateCode ?? "") ?? 0
             bookingJsonDict.updateValue(gstCode, forKey: "gstStateCode")
             bookingJsonDict.updateValue("vchalke@gmail.com", forKey: "addressEmailId")
             bookingJsonDict.updateValue("Vikhroli", forKey: "addressStreet")
             bookingJsonDict.updateValue("Mumbai", forKey: "addressCity")
             bookingJsonDict.updateValue("Maharashtra", forKey: "addressState")
             bookingJsonDict.updateValue("400079", forKey: "addressPincode")
             bookingJsonDict.updateValue("vchalke@gmail.com", forKey: "bookedForEmail")
             bookingJsonDict.updateValue("", forKey: "gstinNo")
             */
            bookingJsonDict.updateValue(pricingModel.pricingMobileNumber, forKey: "addressPhoneNo")
            bookingJsonDict.updateValue(packageDetailModel.packageId, forKey: "packageId")
            bookingJsonDict.updateValue("", forKey: "preConformationPageUrl")
            bookingJsonDict.updateValue("false", forKey: "isOnBefalf")
            bookingJsonDict.updateValue("APP", forKey: "bookingThrough")
            bookingJsonDict.updateValue("N", forKey: "isUnionTerritory")
            bookingJsonDict.updateValue("", forKey: "gstinName")
            let object : PreBookingPayUParameters = PreBookingPayUParameters.init()
            object.customerName = (passengerModel.travellerFirstName ?? "") + (passengerModel.travellerLastName ?? "")
            object.emailID = addressForCommModel.email ?? ""
            object.phoneNo = "\(pricingModel.pricingMobileNumber)"
            object.amount = "\(self.quotationModel.selectedPaymentAmount)"
            var arrayForPassengersRoom:Array<Dictionary<String,Any>> = Array<Dictionary<String,Any>>()
            var travellerPosition:Int = 0
            // When All Passenger dEtails Are there
            /*
             for passengerModel in passengerArray
             {
             travellerPosition = travellerPosition + 1
             
             var dictForPassenger:Dictionary<String,Any> = Dictionary<String,Any>()
             dictForPassenger.updateValue("\(passengerModel.travellerRoomNumber ?? 0)", forKey: "roomNo")
             dictForPassenger.updateValue( "\(travellerPosition)", forKey: "position")
             dictForPassenger.updateValue("\(passengerModel.travellerSerialNumber ?? 0)", forKey: "travellerNo")
             dictForPassenger.updateValue(passengerModel.travellerTitle ?? "", forKey: "title")
             
             if (passengerModel.travellerTitle ?? "") == "Mr."
             {
             dictForPassenger.updateValue("M", forKey: "gender")
             }
             else
             {
             dictForPassenger.updateValue("F", forKey: "gender")
             }
             
             dictForPassenger.updateValue(passengerModel.travellerFirstName ?? "", forKey: "firstName")
             dictForPassenger.updateValue(passengerModel.travellerLastName ?? "", forKey: "lastName")
             dictForPassenger.updateValue("\(passengerModel.travellerBirthDate ?? "")-\(passengerModel.travellerBirthMonth ?? "")-\(passengerModel.travellerBirthYear ?? "")", forKey: "dob")
             
             
             if let mealType = passengerModel.travellerMealType
             {
             if mealType == "Veg"{
             dictForPassenger.updateValue("VEG", forKey: "mealPreference")
             }else{
             dictForPassenger.updateValue("NONVEG", forKey: "mealPreference")
             }
             }else{
             dictForPassenger.updateValue("", forKey: "mealPreference")
             }
             
             if passengerModel.travellerType == "ADULT"{
             dictForPassenger.updateValue(NSNumber.init(value: 0), forKey: "paxType")
             }else if passengerModel.travellerType == "CNB" || passengerModel.travellerType == "CWB"{
             dictForPassenger.updateValue(NSNumber.init(value: 1), forKey: "paxType")
             }else{
             dictForPassenger.updateValue(NSNumber.init(value: 2), forKey: "paxType")
             }
             arrayForPassengersRoom.append(dictForPassenger)
             }
             */
            // Allow Only One passenger Details and Set TBA for other One // On 21_July_20
            for passengerModel in passengerArray{
                travellerPosition = travellerPosition + 1
                var dictForPassenger:Dictionary<String,Any> = Dictionary<String,Any>()
                let travellerNumber = passengerModel.travellerSerialNumber ?? 0
                if (travellerNumber == 1){
                    dictForPassenger.updateValue("\(travellerNumber)", forKey: "travellerNo")
                    dictForPassenger.updateValue("\(passengerModel.travellerRoomNumber ?? 0)", forKey: "roomNo")
                    dictForPassenger.updateValue( "\(travellerPosition)", forKey: "position")
                    
                    dictForPassenger.updateValue(currentRoomPassengerModel.travellerFirstName ?? "", forKey: "firstName")
                    dictForPassenger.updateValue(currentRoomPassengerModel.travellerLastName ?? "", forKey: "lastName")
                    
//                    let travellerFirstName = currentRoomPassengerModel.travellerFirstName ?? "z"
//                    let truncateFirstName = "\(travellerFirstName.last!)"
//                    if (truncateFirstName.uppercased() == "A" || truncateFirstName.uppercased() == "I"){
//                        dictForPassenger.updateValue("Ms.", forKey: "title")
//                        dictForPassenger.updateValue("F", forKey: "gender")
//                    }else{
//                        dictForPassenger.updateValue("Mr.", forKey: "title")
//                        dictForPassenger.updateValue("M", forKey: "gender")
//                    }
                    dictForPassenger.updateValue("\(currentRoomPassengerModel.travellerPanCard ?? "")", forKey: "panNumber")
                    dictForPassenger.updateValue("\(currentRoomPassengerModel.travellerTitle ?? "")", forKey: "title")
                    dictForPassenger.updateValue("\(currentRoomPassengerModel.travellerBirthDate ?? "")", forKey: "dob")
                    if let mealType = currentRoomPassengerModel.travellerMealType
                    {
                        dictForPassenger.updateValue(mealType == "VEG" ? "NON VEG" : mealType == "NON VEG" ? "NON VEG" : "JAIN" , forKey: "mealPreference")
                    }else{
                        dictForPassenger.updateValue("", forKey: "mealPreference")
                    }
                    if passengerModel.travellerType == "ADULT"{
                        dictForPassenger.updateValue(NSNumber.init(value: 0), forKey: "paxType")
                    }else if passengerModel.travellerType == "CNB" || passengerModel.travellerType == "CWB"{
                        dictForPassenger.updateValue(NSNumber.init(value: 1), forKey: "paxType")
                    }else{
                        dictForPassenger.updateValue(NSNumber.init(value: 2), forKey: "paxType")
                    }
                }else{
                    /*
                     dictForPassenger.updateValue("\(travellerNumber)", forKey: "travellerNo")
                     dictForPassenger.updateValue("\(passengerModel.travellerRoomNumber ?? 0)", forKey: "roomNo")
                     dictForPassenger.updateValue( "\(travellerPosition)", forKey: "position")
                     dictForPassenger.updateValue(passengerModel.travellerTitle ?? "", forKey: "title")
                     dictForPassenger.updateValue((passengerModel.travellerTitle ?? "") == "Mr." ? "M" : "F", forKey: "gender")
                     dictForPassenger.updateValue(passengerModel.travellerFirstName ?? "", forKey: "firstName")
                     dictForPassenger.updateValue(passengerModel.travellerLastName ?? "", forKey: "lastName")
                     dictForPassenger.updateValue("\(passengerModel.travellerBirthDate ?? "")-\(passengerModel.travellerBirthMonth ?? "")-\(passengerModel.travellerBirthYear ?? "")", forKey: "dob")
                     if let mealType = passengerModel.travellerMealType
                     {
                     dictForPassenger.updateValue(mealType == "Veg" ? "VEG" : "NONVEG" , forKey: "mealPreference")
                     }else{
                     dictForPassenger.updateValue("", forKey: "mealPreference")
                     }
                     if passengerModel.travellerType == "ADULT"{
                     dictForPassenger.updateValue(NSNumber.init(value: 0), forKey: "paxType")
                     }else if passengerModel.travellerType == "CNB" || passengerModel.travellerType == "CWB"{
                     dictForPassenger.updateValue(NSNumber.init(value: 1), forKey: "paxType")
                     }else{
                     dictForPassenger.updateValue(NSNumber.init(value: 2), forKey: "paxType")
                     }
                     */
                    dictForPassenger.updateValue("\(travellerNumber)", forKey: "travellerNo")
                    dictForPassenger.updateValue("\(passengerModel.travellerRoomNumber ?? 0)", forKey: "roomNo")
                    dictForPassenger.updateValue( "\(travellerPosition)", forKey: "position")
                    dictForPassenger.updateValue("TBA", forKey: "firstName")
                    dictForPassenger.updateValue("TBA", forKey: "lastName")
                    dictForPassenger.updateValue("VEG", forKey: "mealPreference")
                    dictForPassenger.updateValue(NSNumber.init(value: 0), forKey: "paxType")
                    if passengerModel.travellerType == "ADULT"{
                        dictForPassenger.updateValue("Mr", forKey: "title")
                        dictForPassenger.updateValue("M", forKey: "gender")
                        dictForPassenger.updateValue("01-01-1979", forKey: "dob")
                        dictForPassenger.updateValue(NSNumber.init(value: 0), forKey: "paxType")
                    }else if passengerModel.travellerType == "CNB" || passengerModel.travellerType == "CWB"{
                        dictForPassenger.updateValue("Miss", forKey: "title")
                        dictForPassenger.updateValue("F", forKey: "gender")
                        dictForPassenger.updateValue("01-01-2010", forKey: "dob")
                        dictForPassenger.updateValue(NSNumber.init(value: 1), forKey: "paxType")
                    }else{
                        dictForPassenger.updateValue("Miss", forKey: "title")
                        dictForPassenger.updateValue("F", forKey: "gender")
                        dictForPassenger.updateValue("01-01-2020", forKey: "dob")
                        dictForPassenger.updateValue(NSNumber.init(value: 2), forKey: "paxType")
                    }
                }
                arrayForPassengersRoom.append(dictForPassenger)
            }
            bookingJsonDict.updateValue(arrayForPassengersRoom, forKey: "tcilHolidayBookingTravellerDetailCollection")
            print("bookingJsonDict \(bookingJsonDict)")
            
            LoadingIndicatorView.show("Loading")
            //             "holidayRS/holidayBooking" tcHolidayRS/holidayBooking
            PreBookingCommunicationManager.getPrebookingData(requestType:"POST", queryParam: nil, pathParam: nil, jsonParam: bookingJsonDict, headers: nil, requestURL:kAstraUrlBooking , returnInCaseOffailure: false){ (status,response) in
                LoadingIndicatorView.hide()
                printLog("status is \(status)")
                printLog("response is \(String(describing: response))")
                if status == true{
                    if let responseData : Dictionary<String,Any> = response as? Dictionary<String, Any>{
                        if responseData.count > 0{
                            //self.startPayment(passengerDetail: object)
                            if let transactionID = responseData["tid"] as? String{
                                print("transactionID \(transactionID)")
                                print("Jump To Transaction View")
                                let urlString = "\(kpgURLForPayment)"+"\(transactionID)"
                                print("urlString \(urlString)")
                                let url = NSURL.init(string: urlString)
                                print("url \(String(describing: url))")
                                if let isUrl = url{
                                    if #available(iOS 10.0, *) {
                                        UIApplication.shared.open(isUrl as URL, options: [:],completionHandler:{(success) -> Void in
                                            if success {
                                                print("Successful Redirection")
                                                UserDefaults.standard.set(transactionID, forKey: kCurrentTransactionId)
                                                //                                            self.jumpOnConfirmationBoking(dictionary: responseData)
                                            } else {
                                                print("Not Successful Redirection")
                                            }
                                        })
                                    } else {
                                        UIApplication.shared.openURL(isUrl as URL)
                                        self.jumpOnConfirmationBoking(dictionary: responseData)
                                    }
                                }
                                // self.redirectToPaymentGateway(transactionID: transactionID)
                                return
                            }
                            if let message = responseData["message"] as? String{
                                AppUtility.displayAlert(message: message)
                                return
                            }
                        }
                    }
                }
                AppUtility.displayAlert(message: "some error occuered")
            }
        }else{
            self.alertViewControllerSingleton(title: "Alert", message: validStr, type: .alert, buttonArryName: ["Ok"]) { (strin) in
                printLog(strin)
            }
        }
    }
    
    func getStatesList(){
        LoadingIndicatorView.show("Loading")
        PreBookingCommunicationManager.getPrebookingData(requestType:"GET", queryParam: "", pathParam:"/4", jsonParam: nil, headers: nil, requestURL:kAstraUrlGstStates , returnInCaseOffailure: false)
        {
            (status,response) in
            
            printLog("status is \(status)")
            printLog("response is \(String(describing: response))")
            if let responseDict = response {
                printLog("status is \(responseDict)")
                self.statesArray = responseDict as! [[String:Any]]
                printLog("self.statesArray is \(self.statesArray)")
                DispatchQueue.main.async {
                    self.table_travellerDetail.reloadData()
                }
            }
            else
            {
                AppUtility.displayAlert(title: "Alert", message: "Some error has occurred")
                
            }
        }
    }
}
//Details OF Passenger
/*
 "{
 ""addressId"":0,
 ""bookingAmount"":40320,
 ""bookingType"":""F"",
 ""tcilHolidayBookingTravellerDetailCollection"":[{""roomNo"":1,""position"":1,""travellerNo"":1,""title"":""Mr"",""firstName"":""sdg"",""lastName"":""sdh"",""gender"":""M"",""dob"":""07-07-2005"",""mealPreference"":""JAIN"",""paxType"":0,""aadhaarNumber"":""""},{""roomNo"":1,""position"":2,""travellerNo"":2,""title"":""Mr"",""firstName"":""sdhsdh"",""lastName"":""sdhsdh"",""gender"":""M"",""dob"":""14-07-2005"",""mealPreference"":""JAIN"",""paxType"":0,""aadhaarNumber"":""""}],
 ""quotationId"":""139751"",
 ""title"":""Mr"",
 ""firstName"":""sdhsdh"",
 ""lastName"":""sdhsdh"",
 ""addressEmailId"":""aa@hh.ll"",
 ""addressPhoneNo"":""9964434636"",
 ""addressStreet"":""dshsdhsdh"",
 ""addressCity"":""Mumbai"",
 ""addressState"":""Maharashtra"",
 ""addressPincode"":""400235"",
 ""packageId"":""PKG001424"",
 ""preConformationPageUrl"":""https://www.sotc.in/holidays/holidaysPreConfirmation?hubCode=IDR&pkgId=PKG001424&pkgClassId=2&quotationId=139751&upgradation=0&destination=PKG001424_indore_CITY_0"",
 ""bookedForEmail"":null, : onbehalf email id,applicable if booking is onbehalf - Non Mandatory
 ""isOnBefalf"":""false"",
 ""bookingThrough"":""Desktop"",
 ""gstStateCode"":""27"",
 ""isUnionTerritory"":""N"",
 ""gstinNo"":""27asdfg1234dggd""
 }"
 */
class StatesModel{
    
    var gstStateCode:String? = ""
    var gstState:String? = ""
    var isUnionTerritory:String? = ""
}
