//
//  ForexAppUtility.swift
//  holidays
//
//  Created by Parshwanath on 27/03/18.
//  Copyright © 2018 Mobicule. All rights reserved.
//

import UIKit

class ForexAppUtility: NSObject
{
   class func convertQuoteResponse(quoteDict:NSDictionary) -> NSDictionary
    {
        let reqDict: NSMutableDictionary = quoteDict.mutableCopy() as! NSMutableDictionary
        
        let tcilForexQuoteTravellerCollectionArray : NSArray = quoteDict.object(forKey: "tcilForexQuoteTravellerCollection") as! NSArray
        let mutabletcilForexQuoteTravellerCollectionArray : NSMutableArray = tcilForexQuoteTravellerCollectionArray.mutableCopy() as! NSMutableArray
        
        
        for traveller in 0..<mutabletcilForexQuoteTravellerCollectionArray.count
        {
            let tcilForexQuoteTravellerCollectio : NSDictionary = mutabletcilForexQuoteTravellerCollectionArray.object(at: traveller) as! NSDictionary
            
            let tcilForexQuoteTravellerCollection : NSMutableDictionary = tcilForexQuoteTravellerCollectio.mutableCopy() as! NSMutableDictionary
            
            let tcilForexQuoteTravellerProductCollectio : NSArray = tcilForexQuoteTravellerCollection.object(forKey: "tcilForexQuoteTravellerProductCollection") as! NSArray
            let tcilForexQuoteTravellerProductCollection : NSMutableArray = tcilForexQuoteTravellerProductCollectio.mutableCopy() as! NSMutableArray
            for i in 0..<tcilForexQuoteTravellerProductCollection.count
            {
                let productDict : NSDictionary = tcilForexQuoteTravellerProductCollection[i] as! NSDictionary
                let mutableProductDict : NSMutableDictionary = productDict.mutableCopy() as! NSMutableDictionary
                
                let roe : Float = productDict.object(forKey: "roe") as! Float
                mutableProductDict.setValue(NSNumber.init(value: roe).decimalValue, forKey: "roe")
                tcilForexQuoteTravellerProductCollection[i] = mutableProductDict;
                
            }
            
            tcilForexQuoteTravellerCollection.setValue(tcilForexQuoteTravellerProductCollection, forKey: "tcilForexQuoteTravellerProductCollection")
            mutabletcilForexQuoteTravellerCollectionArray[traveller] = tcilForexQuoteTravellerCollection
            
            
        }
        reqDict.setValue(mutabletcilForexQuoteTravellerCollectionArray, forKey: "tcilForexQuoteTravellerCollection")
        return reqDict
    }
    
    
    class func getCGST_IGST_Total(quoteDict:NSDictionary) -> (totalCGST:Decimal?,totalSGST:Decimal?,totlaIGST:Decimal?,taxType:String?)
    {
        let tcilForexQuoteTravellerCollection : NSArray = quoteDict.object(forKey: "tcilForexQuoteTravellerCollection") as! NSArray
        
        if tcilForexQuoteTravellerCollection.count > 0
        {
            var totalCGST:Decimal = 0.0
            
            var totalSGST:Decimal = 0.0
            
            var taxType = ""
            
            var totlaIGST:Decimal = 0.0
            
            for index in 0..<tcilForexQuoteTravellerCollection.count
            {
                let travellerDict : NSDictionary =  tcilForexQuoteTravellerCollection[index] as! NSDictionary
                let travellerServiceTaxDesc : String   = travellerDict.object(forKey: "travellerServiceTaxDesc") as! String
                
                if travellerServiceTaxDesc.contains("IGST")
                {
                    var igst : String = travellerServiceTaxDesc.substring(from: travellerServiceTaxDesc.firstIndex(of: "=")!)
                    igst.removeFirst()
                    
                    let igstDouble:Double = Double(igst) ?? 0.0
                    
                    let igstNSNumber:NSNumber = NSNumber(value: igstDouble)
                    
                    totlaIGST = totlaIGST + igstNSNumber.decimalValue
                    
                    
                    taxType = TaxType.IGST.rawValue
                    
                    
                }
                if travellerServiceTaxDesc.contains("CGST") || travellerServiceTaxDesc.contains("SGST")
                {
                    let array : NSArray = travellerServiceTaxDesc.split(separator: ",") as NSArray
                    var cgst : String = array[0] as! String
                    cgst = cgst.substring(from: cgst.firstIndex(of: "=")!);
                    cgst.removeFirst()
                    
                    let cgstDouble:Double = Double(cgst) ?? 0.0
                    
                    let cgstNSNumber:NSNumber =  NSNumber(value: cgstDouble )
                    totalCGST = totalCGST + cgstNSNumber.decimalValue
                    
                    
                    
                    
                    var sgst : String = array[1] as! String
                    sgst = sgst.substring(from: sgst.firstIndex(of: "=")!);
                    sgst.removeFirst()
                    
                    let sgstDouble:Double = Double(sgst) ?? 0.0
                    
                    let sgstNSNumber:NSNumber =  NSNumber(value: sgstDouble )
                    totalSGST = totalSGST + sgstNSNumber.decimalValue
                    
                    taxType = TaxType.CSGST_SGST.rawValue
                    
                    
                }
                
            }
            
           
            let numberFormatter:NumberFormatter = NumberFormatter.init()
            numberFormatter.maximumFractionDigits = 2
            let totalCGSTString:String = numberFormatter.string(for: totalCGST) ?? "0.0"
              let totalSGSTString:String = numberFormatter.string(for: totalSGST) ?? "0.0"
              let totlaIGSTString:String = numberFormatter.string(for: totlaIGST) ?? "0.0"
            
            totalCGST = Decimal.init(string: totalCGSTString)!
            totalSGST = Decimal.init(string: totalSGSTString)!
            totlaIGST = Decimal.init(string: totlaIGSTString)!
            
            return(totalCGST:totalCGST,totalSGST:totalSGST,totlaIGST:totlaIGST,taxType:taxType)
        }
        
        return(nil,nil,nil,nil)
    }
    
    
    class func updateForexQuote(quotesResponse:Dictionary<String,Any>) -> (responseQuestsDict:Dictionary<String,Any>?,totalCGST:Decimal?,totalSGST:Decimal?,totlaIGST:Decimal?,taxType:String?) {
       
        
        
       
        
        if var quotesResponseDict:Dictionary<String,Any> = quotesResponse //as? Dictionary<String,Any>
        {
            var updatedForexQuoteDict:Dictionary<String,Any>?
            
            var totalCGST:Decimal = 0.0
            
            var totalSGST:Decimal = 0.0
            
            var taxType = ""
            
            var totlaIGST:Decimal = 0.0
            
            var totlaSericeTax:Decimal = 0.0
         
            
            if var tcilFQTravColleArray:Array<Any> = quotesResponseDict["tcilForexQuoteTravellerCollection"] as? Array<Any>
            {
                
                
                
                for (tcilFQuTravCollIndex, singleTcilFQTravColl) in tcilFQTravColleArray.enumerated() {
                    
                    if var tcilFQTraveProduCollDict:Dictionary<String,Any>  =  singleTcilFQTravColl as? Dictionary<String,Any>
                    {
                        
                     if let travellerServiceTaxDesc:String = tcilFQTraveProduCollDict["travellerServiceTaxDesc"] as? String
                       {
                        
                        if travellerServiceTaxDesc.contains("IGST")
                        {
                            var igst : String = travellerServiceTaxDesc.substring(from: travellerServiceTaxDesc.firstIndex(of: "=")!)
                            igst.removeFirst()
                             let igstDouble:Double = Double(igst) ?? 0.0
                            
                            let igstNSNumber:NSNumber = NSNumber(value: igstDouble)
                           totlaIGST = totlaIGST + igstNSNumber.decimalValue
                              taxType = TaxType.IGST.rawValue
                            
                        }
                        if travellerServiceTaxDesc.contains("CGST") || travellerServiceTaxDesc.contains("SGST")
                        {
                            
                            let array : NSArray = travellerServiceTaxDesc.split(separator: ",") as NSArray
                            var cgst : String = array[0] as! String
                            cgst = cgst.substring(from: cgst.firstIndex(of: "=")!);
                            cgst.removeFirst()
                            
                            let cgstDouble:Double = Double(cgst) ?? 0.0
                          
                            let cgstNSNumber:NSNumber =  NSNumber(value: cgstDouble )
                            totalCGST = totalCGST + cgstNSNumber.decimalValue
                           
                            var sgst : String = array[1] as! String
                            sgst = sgst.substring(from: sgst.firstIndex(of: "=")!);
                            sgst.removeFirst()
                            
                            let sgstDouble:Double = Double(sgst) ?? 0.0
                            
                            let sgstNSNumber:NSNumber =  NSNumber(value: sgstDouble )
                            totalSGST = totalSGST + sgstNSNumber.decimalValue
                            
                            taxType = TaxType.CSGST_SGST.rawValue
                            
                        }
                    }
                        
                        
                        
                        if var tcilFQTraveProdCollArray:Array<Any> =  tcilFQTraveProduCollDict["tcilForexQuoteTravellerProductCollection"] as? Array<Any>
                        {
                             for (tcilFQTraveProdCollIndex, singleTcilFQTraveColl) in tcilFQTraveProdCollArray.enumerated() {
                                
                                if var singleRoeProduct:Dictionary<String,Any> = singleTcilFQTraveColl as? Dictionary<String,Any>
                                {
                                    if  singleRoeProduct["roe"] != nil
                                    {
                                        let roeInDecimal:NSNumber = singleRoeProduct["roe"] as! NSNumber
                                        singleRoeProduct["roe"] = roeInDecimal.decimalValue
                                    }
                                    
                                    tcilFQTraveProdCollArray[tcilFQTraveProdCollIndex] = singleRoeProduct
                                }
                                
                                
                            }
                            
                            tcilFQTraveProduCollDict["tcilForexQuoteTravellerProductCollection"] = tcilFQTraveProdCollArray
                        }
                        
                        tcilFQTravColleArray[tcilFQuTravCollIndex] = tcilFQTraveProduCollDict
                    }
                    
                }
                
                
                quotesResponseDict["tcilForexQuoteTravellerCollection"] = tcilFQTravColleArray
                
            }
            
            return(quotesResponseDict,totalCGST,totalSGST,totlaIGST,taxType)
            
        }
        
        return (nil,nil,nil,nil,nil)
       
    }
    
    
    class func savePrefredBranch(cityCode:String,cityName:String,branchCode:Int?,branchName:String,address:String,latitude:Double,longitude:Double,isSave:Bool)
    {
        if isSave
        {
        
        let savePrefredBranch:Dictionary<String,Any> = [cityCodeSavePreferedBranch:cityCode, cityNameSavePreferedBranch:cityName,branchCodeSavePreferedBranch:branchCode,branchNameSavePreferedBranch:branchName,addressSavePreferedBranch:address,latitudeSavePreferedBranch:latitude,longitudeSavePreferedBranch:longitude];
        
        UserDefaults.standard.set(savePrefredBranch,forKey: userDefaultSavePreferedBranch)
            
        }else{
            
             UserDefaults.standard.set(nil,forKey: userDefaultSavePreferedBranch)
        }
        
        printLog(savePrefredBranch);
    }
    
    class func getPreferdBranchDetails() -> Dictionary<String,Any>?  {
        
      if let savedPrefredBranch:Dictionary<String,Any> = UserDefaults.standard.value(forKey: userDefaultSavePreferedBranch) as? Dictionary<String,Any>
        {
            return savedPrefredBranch
        }
        
        return nil
    }
    
    class func checkVersionUpgrade(controller:UIViewController)
    {
        let pathParameter  : NSString = "tcCommonRS/extnrt/getVersion"
        ForexCommunicationManager.sharedInstance.execTask(pathParam:pathParameter , queryParam: "", requestType: "get", jsonDict:NSDictionary())
        {
            (status, response) in
            DispatchQueue.main.async { () -> Void in
                if(response != nil)
                {
                    let jsonArray : NSArray = response as! NSArray
                   
                    
                    
                }
                else
                {
                    
                }
            }
        }
        
    }
     
}
