//
//  ReloadCardPaymentViewController.swift
//  holidays
//
//  Created by Parshwanath on 07/01/18.
//  Copyright © 2018 Mobicule. All rights reserved.
//

import UIKit

class ReloadCardPaymentViewController: ForexBaseViewController,UITableViewDelegate,UITableViewDataSource
{
    let headerHeight = 51;
    
    let rowHeight = 37;
    
    let kHeaderSectionTag: Int = 6900;
    
    var responseDict: NSDictionary = [:] ;
    var selectedSection = -1
    
    @IBOutlet weak var labelTermsAndCond: UILabel!
    
    @IBOutlet weak var labelPaymentTerms: UILabel!
    
    @IBOutlet weak var heightConstraintOfDeliveryDetail: NSLayoutConstraint!
    @IBOutlet weak var additionalExpensesView: UIView!
    @IBOutlet weak var lavelDeliveryChargesAmount: UILabel!
    @IBOutlet weak var additionalExpensesAmountLabel: UILabel!
    @IBOutlet weak var heightConstraintDeliverycharges: NSLayoutConstraint!
    @IBOutlet weak var heightConstraintOfAdditionalExpenses: NSLayoutConstraint!
    @IBOutlet weak var labelTotalAmount: UILabel!
    
    @IBOutlet weak var labelTaxCalculationAmount: UILabel!
    @IBOutlet weak var labelCGST: UILabel!
    @IBOutlet weak var labelCGSTAmount: UILabel!
    @IBOutlet weak var labelSGSTAmount: UILabel!
    @IBOutlet weak var taxCalculationView: UIView!
    @IBOutlet weak var heightConstraintOfHomeDeliveryView: NSLayoutConstraint!
    var expandedSectionHeaderNumber: Int = -1
    
    var travellerArray: [TravellerBO] = []
    var buyForexBo : BuyForexBO = BuyForexBO.init()
    
    var atBranchResponseDict: NSDictionary = [:] ;
    var atHomeDeliveryResponseDict: NSDictionary = [:] ;
    
    @IBOutlet weak var heightConstraintOfSGSTView: NSLayoutConstraint!
    @IBOutlet weak var heightConstraintOfCGStView: NSLayoutConstraint!
    @IBOutlet weak var labelAdvancePaymentAmt: UILabel!
    @IBOutlet weak var labelFullPaymentAmt: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var fullPaymentRadioButton: UIButton!
    @IBOutlet weak var buyForexScrollView: TPKeyboardAvoidingScrollView!
    @IBOutlet var containerBuyForexPaymentView: UIView!
    @IBOutlet weak var advancepaymentRadioButton: UIButton!
    @IBOutlet var buyForexPaymentView: UIView!
    @IBOutlet weak var atBranchRadioButton: UIButton!
    @IBOutlet weak var homeDeliveryRadioButton: UIButton!
    @IBOutlet weak var tickMarkRadioButton: UIButton!
    @IBOutlet weak var heightConstraintForTableView: NSLayoutConstraint!
    
    var paymentType : String = ""
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        Bundle.main.loadNibNamed("ReloadCardPaymentViewController", owner: self, options: nil)
        
        super.addViewInBaseView(childView: self.containerBuyForexPaymentView)
        
        super.setUpHeaderLabel(labelHeaderNameText: "Reload Forex")
        
        travellerArray = (buyForexBo.buyForexPassengerInfo?.travellerArray)!
        
        heightConstraintForTableView.constant = CGFloat(travellerArray.count * 50)
        heightConstraintOfCGStView.constant = 0
        heightConstraintOfSGSTView.constant = 0
        heightConstraintOfHomeDeliveryView.constant = 0 //61
        self.buyForexScrollView.contentSize = CGSize.init(width: 0, height:Int(heightConstraintForTableView.constant ) + 550  + Int(heightConstraintOfCGStView.constant + heightConstraintOfSGSTView.constant + heightConstraintOfHomeDeliveryView.constant));
        self.buyForexPaymentView.frame = CGRect.init(x: 0, y: 0, width: Int(self.buyForexScrollView.frame.size.width), height: Int(buyForexScrollView.contentSize.height + 400))
        self.buyForexScrollView.addSubview(self.buyForexPaymentView)
        
        
        let normalText = "There is no processing fees for "
        let boldText  = "Advance Payment"
        
        let attributedString = NSMutableAttributedString(string:normalText)
        
        let attrs = [convertFromNSAttributedStringKey(NSAttributedString.Key.font) : UIFont.boldSystemFont(ofSize: 15)]
        let boldString = NSMutableAttributedString(string:boldText, attributes:convertToOptionalNSAttributedStringKeyDictionary(attrs))
        
        attributedString.append(boldString)
        
        labelPaymentTerms.attributedText = attributedString
        
        self.paymentType = "F"
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ReloadCardPaymentViewController.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        
        
        addGestureRecognizerToTaxCalculationView()
        addGestureRecognizerToAdditionalExpensesView()
        
        heightConstraintOfAdditionalExpenses.constant = 0
        heightConstraintOfDeliveryDetail.constant = 0
        heightConstraintDeliverycharges.constant = 0
        self.setupRadioButtons()
        self.getQuoteForReload()
        setTableHeight()
        
        let text = (labelTermsAndCond.text)!
        //        let underlineAttriString = NSMutableAttributedString(string: text)
        let underlineAttriString = NSMutableAttributedString(attributedString: labelTermsAndCond.attributedText!)
        
        let range1 = (text as NSString).range(of: "Terms and conditions")
        underlineAttriString.setAttributes(convertToOptionalNSAttributedStringKeyDictionary([convertFromNSAttributedStringKey(NSAttributedString.Key.font) : UIFont(name: "HelveticaNeue-Light", size: CGFloat(13.0))!
            , convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor) : UIColor(red: 0 / 255.0, green: 149 / 255.0, blue: 218 / 255.0, alpha: 1.0)]), range: range1)
        underlineAttriString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: range1)
        
        let range2 = (text as NSString).range(of: "Booking policy")
        underlineAttriString.setAttributes(convertToOptionalNSAttributedStringKeyDictionary([convertFromNSAttributedStringKey(NSAttributedString.Key.font) : UIFont(name: "HelveticaNeue-Light", size: CGFloat(13.0))!
            , convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor) : UIColor(red: 0 / 255.0, green: 149 / 255.0, blue: 218 / 255.0, alpha: 1.0)]), range: range2)
        underlineAttriString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: range2)
        labelTermsAndCond.attributedText = underlineAttriString
        
        let tapAddExpe : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ReloadCardPaymentViewController.tapLabel))
        tapAddExpe.delegate = self as? UIGestureRecognizerDelegate
        tapAddExpe.numberOfTapsRequired = 1
        tapAddExpe.numberOfTouchesRequired = 1
        labelTermsAndCond.addGestureRecognizer(tapAddExpe)
        
    }
    
    func addGestureRecognizerToAdditionalExpensesView()
    {
        let tapAddExpe : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ReloadCardPaymentViewController.additionalExpensesViewTapped))
        tapAddExpe.delegate = self as? UIGestureRecognizerDelegate
        tapAddExpe.numberOfTapsRequired = 1
        tapAddExpe.numberOfTouchesRequired = 1
        additionalExpensesView.addGestureRecognizer(tapAddExpe)
    }
    
    func addGestureRecognizerToTaxCalculationView()
    {
        let tapR : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ReloadCardPaymentViewController.taxCalculationViewTapped))
        tapR.delegate = self as? UIGestureRecognizerDelegate
        tapR.numberOfTapsRequired = 1
        tapR.numberOfTouchesRequired = 1
        taxCalculationView.addGestureRecognizer(tapR)
    }
    
    func convertStringToDictionary(text: String) -> [String:AnyObject]? {
        if let data = text.data(using: String.Encoding.utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject]
            } catch let error as NSError {
                print(error)
            }
        }
        return nil
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillLayoutSubviews()
    {
        self.buyForexPaymentView.frame = CGRect.init(x: 0, y: 0, width: self.buyForexScrollView.frame.size.width, height: self.buyForexScrollView.frame.size.height)
        super.setSubViewFrame(childView: self.containerBuyForexPaymentView)
        setTableHeight()
    }
    
    @objc func dismissKeyboard()
    {
        view.endEditing(true)
    }
    func setupRadioButtons()
    {
        fullPaymentRadioButton.setImage(UIImage.init(named: "ForexRadioButtonOn"), for: UIControl.State.selected)
        fullPaymentRadioButton.setImage(UIImage.init(named: "ForexRadioButtonOff"), for: UIControl.State.normal)
        fullPaymentRadioButton.isSelected = true
        
        advancepaymentRadioButton.setImage(UIImage.init(named: "ForexRadioButtonOn"), for: UIControl.State.selected)
        advancepaymentRadioButton.setImage(UIImage.init(named: "ForexRadioButtonOff"), for: UIControl.State.normal)
        advancepaymentRadioButton.isSelected = false
        
        homeDeliveryRadioButton.setImage(UIImage.init(named: "ForexRadioButtonOn"), for: UIControl.State.selected)
        homeDeliveryRadioButton.setImage(UIImage.init(named: "ForexRadioButtonOff"), for: UIControl.State.normal)
        homeDeliveryRadioButton.isSelected = false
        
        atBranchRadioButton.setImage(UIImage.init(named: "ForexRadioButtonOn"), for: UIControl.State.selected)
        atBranchRadioButton.setImage(UIImage.init(named: "ForexRadioButtonOff"), for: UIControl.State.normal)
        atBranchRadioButton.isSelected = true
        
        
        tickMarkRadioButton.setImage(UIImage.init(named: "ForexCheckBoxOn"), for: UIControl.State.selected)
        tickMarkRadioButton.setImage(UIImage.init(named: "ForexCheckBoxOff"), for: UIControl.State.normal)
        tickMarkRadioButton.isSelected = true
        
        
    }
    
    func setTableHeight()
    {
        //heightConstraintForTableView.constant = CGFloat(heightForTableView);
        if(advancepaymentRadioButton.isSelected)
        {
            heightConstraintOfHomeDeliveryView.constant = 0
            
            let heightFT:CGFloat = (heightConstraintOfCGStView.constant + heightConstraintOfSGSTView.constant + heightConstraintOfHomeDeliveryView.constant + heightConstraintDeliverycharges.constant + heightConstraintOfAdditionalExpenses.constant + heightConstraintOfDeliveryDetail.constant)
            let height:Int = Int(heightConstraintForTableView.constant ) + 550 + Int(heightFT)
            
            self.buyForexScrollView.contentSize = CGSize.init(width: 0, height:height);
        }
        else
        {
            heightConstraintOfHomeDeliveryView.constant = 0 //61
            let heightFT:CGFloat = (heightConstraintOfCGStView.constant + heightConstraintOfSGSTView.constant + heightConstraintOfHomeDeliveryView.constant + heightConstraintDeliverycharges.constant + heightConstraintOfAdditionalExpenses.constant + heightConstraintOfDeliveryDetail.constant)
            let height = Int(heightConstraintForTableView.constant ) + 550 + Int(heightFT)
            self.buyForexScrollView.contentSize = CGSize.init(width: 0, height:height);
        }
        self.buyForexPaymentView.frame = CGRect.init(x: 0, y: 0, width: Int(self.buyForexScrollView.frame.size.width), height: Int(buyForexScrollView.contentSize.height + 400))
        self.tableView.layoutIfNeeded()
        self.buyForexPaymentView.layoutIfNeeded()
    }
    
    func getTotalProductCount() -> NSInteger {
        var total : NSInteger = 0
        for i in 0..<travellerArray.count
        {
            total = total + travellerArray[i].getProductCount()
        }
        return total
    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    func getHomeDeliveryCharges()
    {
        LoadingIndicatorView.show()
        
        let pathParameter  : NSString = "/tcForexRS/quote/buy/delivery/\(self.responseDict.object(forKey: "quoteId")!)" as NSString
        
        ForexCommunicationManager.sharedInstance.execTask(pathParam:pathParameter , queryParam: "", requestType: "post", jsonDict:NSDictionary.init())
        {
            (status, response) in
             DispatchQueue.main.async {
            if(response != nil)
            {
                //let jsonResponse : NSDictionary = response as! NSDictionary
                
               
                    
                    LoadingIndicatorView.hide()
                    
                   /* self.responseDict = ForexAppUtility.updateForexQuote(quotesResponse: response as! Dictionary<String,Any>)! as NSDictionary //response as! NSDictionary
                    self.atHomeDeliveryResponseDict = response as! NSDictionary
                    
                    self.homeDeliveryRadioButton.isSelected =  true
                    self.atBranchRadioButton.isSelected = false
                    self.heightConstraintOfAdditionalExpenses.constant = 50
                    self.heightConstraintOfDeliveryDetail.constant = 50
                    self.labelTaxCalculationAmount.text =  "0 INR"
                    let totalPrice : NSInteger = self.responseDict.object(forKey: "totalPrice") as! NSInteger
                    
                    let deliveryCharges : NSInteger = self.responseDict.object(forKey: "deliveryCharges") as! NSInteger
                    self.labelFullPaymentAmt.text = "\(totalPrice + deliveryCharges) INR"
                    self.labelTotalAmount.text = "\(totalPrice + deliveryCharges) INR"
                    self.taxCalculationView.isUserInteractionEnabled = false
                    self.setTableHeight()*/
                    
                }
                
            
            else
            {
                self.showAlert(message: "Some error has occurred")
            }
            }
        }
        
    }
    
    func getQuoteForReload()
    {
        LoadingIndicatorView.show();
        
        let arrayForTravellerCollection : NSMutableArray = NSMutableArray()
        for travellerIndex in 0..<travellerArray.count
        {
            let dictForTraveller : NSMutableDictionary = NSMutableDictionary()
            let arrayForProductCollection : NSMutableArray = NSMutableArray()
            
            
            dictForTraveller.setValue(travellerIndex+1, forKey:"travellerNo")
            dictForTraveller.setValue(travellerArray[travellerIndex].getProductCount(), forKey:"travellerTotalProducts")
            for productIndex in 0..<travellerArray[travellerIndex].getProductCount()
            {
                
                
                let dictForProduct : NSMutableDictionary = NSMutableDictionary()
                let product : ProductBO =  travellerArray[travellerIndex].getProduct(atIndex:productIndex)
                dictForProduct.setValue(product.productName, forKey:"productName")
                dictForProduct.setValue(NSNumber.init(value: product.productID!), forKey:"productId")
                dictForProduct.setValue(NSNumber.init(value: product.currencyID!), forKey:"currencyId")
                //dictForProduct.setValue(NSNumber.init(value: product.currencyID!), forKey:"currencySelected")
                dictForProduct.setValue(product.currencyCode, forKey:"currencyCode")
                dictForProduct.setValue(product.currency, forKey:"currencyName")
                dictForProduct.setValue(product.isNostroAcc, forKey:"nostroAvailable")
                dictForProduct.setValue(RELOAD_MODULE_NAME, forKey:"moduleName")
                dictForProduct.setValue(NSNumber.init(value: product.roe).decimalValue, forKey:"roe")
                dictForProduct.setValue(product.fxAmount, forKey:"amount")
                dictForProduct.setValue(0, forKey:"roeEdit")
                //(NSNumber.init(value: product.fxAmount), forKey:"amount")
                
                dictForProduct.setValue(product.inrAmount, forKey:"equivalentInr")
                
                arrayForProductCollection .add(dictForProduct)
            }
            dictForTraveller.setValue(arrayForProductCollection, forKey:"tcilForexQuoteTravellerProductCollection")
            arrayForTravellerCollection.add(dictForTraveller)
        }
        
        
        
        let dictOfJson : NSMutableDictionary = NSMutableDictionary()
        dictOfJson .setValue(buyForexBo.buyForexOptionViewDetails?.contactDetail, forKey: "bookerMobileNo")
        dictOfJson .setValue("test@test.com", forKey: "bookerEmailId")
        dictOfJson .setValue("\(buyForexBo.buyForexOptionViewDetails!.cardNumber)", forKey: "reloadCardNo")
        dictOfJson .setValue(false, forKey: "onBehalf")
        dictOfJson .setValue("B2CApp", forKey: "device")
        
        let dot : NSDate = (buyForexBo.buyForexOptionViewDetails?.travelDate)!
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        
        let calendar = Calendar.current
        let year = calendar.component(.year, from: dot as Date)
        let month = calendar.component(.month, from: dot as Date)
        let day = calendar.component(.day, from: dot as Date)
        dictOfJson.setValue("\(day)-\(month)-\(year)", forKey: "dateOfTravel")
        
      //  dictOfJson .setValue("", forKey: "dateOfTravel")
        dictOfJson .setValue(NSNumber.init(value: RELOAD_MODULE_ID), forKey: "moduleId")
        dictOfJson .setValue(travellerArray.count, forKey: "totalNoOfTraveller")
        dictOfJson .setValue(getTotalProductCount(), forKey: "totalNoOfProducts")
        dictOfJson .setValue(arrayForTravellerCollection, forKey: "tcilForexQuoteTravellerCollection")
       
        dictOfJson.setValue("N", forKey:"isRoeEdit")
        let gstStateCode = buyForexBo.buyForexPassengerInfo?.gstStateCode
        let gstBranch = buyForexBo.buyForexPassengerInfo?.branchCode
        let gstBranchCode = buyForexBo.buyForexPassengerInfo?.gstBranchCode
        let isUnionTerritoryState = buyForexBo.buyForexPassengerInfo?.isUnionTerritoryState
        let isUnionTerritoryBranch = buyForexBo.buyForexPassengerInfo?.isUnionTerritoryBranch
        
        dictOfJson .setValue(gstStateCode, forKey: "gstState")
        dictOfJson .setValue(gstBranch, forKey: "gstBranch")
        dictOfJson .setValue(gstBranchCode, forKey: "gstBranchCode")
        dictOfJson .setValue(isUnionTerritoryState, forKey: "isStateUt")
        dictOfJson .setValue(isUnionTerritoryBranch, forKey: "isBranchUt")
        let jsonData: NSData = try!JSONSerialization.data(withJSONObject: dictOfJson, options: JSONSerialization.WritingOptions.prettyPrinted )as NSData
        
        print(NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue)! as String)
        
        let pathParameter  : NSString = "/tcForexRS/quote/reload/save"
        ForexCommunicationManager.sharedInstance.execTaskWithAlamofire(pathParam: pathParameter, queryParam: "", requestType: "post", jsonDict: dictOfJson) { (status, response) in
            
            DispatchQueue.main.async {
                
                LoadingIndicatorView.hide()
            
            
            if(response != nil)
            {
                DispatchQueue.main.async {
                    
                    LoadingIndicatorView.hide()
                    
                   /* let forexQueostTouple = ForexAppUtility.updateForexQuote(quotesResponse: response as! Dictionary<String,Any>)
                    
                    if forexQueostTouple.responseQuestsDict != nil
                    {
                        self.responseDict =  forexQueostTouple.responseQuestsDict! as NSDictionary //response as! NSDictionary
                        
                        
                        self.labelFullPaymentAmt.text = "\(self.responseDict.object(forKey: "totalPrice") as! NSInteger) INR"
                        self.labelTotalAmount.text = "\( self.responseDict.object(forKey: "totalPrice") as! NSInteger) INR"
                        self.labelAdvancePaymentAmt.text = "\( self.responseDict.object(forKey: "advanceAmount") as! NSInteger) INR"
                        let tax : NSString = "\( self.responseDict.object(forKey: "totalServiceTax") as! Float) INR" as NSString
                        
                        self.labelTaxCalculationAmount.text = tax as String
                        
                        if let taxType:String = forexQueostTouple.taxType
                        {
                            if taxType == TaxType.CSGST_SGST.rawValue
                            {
                                self.labelCGSTAmount.text = "\(forexQueostTouple.totalCGST ?? 0.0) INR"
                                
                                self.labelSGSTAmount.text = "\(forexQueostTouple.totalSGST ?? 0.0) INR"
                                
                                self.heightConstraintOfSGSTView.constant = 0
                                self.heightConstraintOfCGStView.constant = 0
                                
                            }else if taxType == TaxType.IGST.rawValue{
                                
                                
                                self.labelCGSTAmount.text = "\(forexQueostTouple.totlaIGST ?? 0.0) INR"
                                self.labelCGST.text = "IGST"
                                
                                self.heightConstraintOfSGSTView.constant = 0
                                self.heightConstraintOfCGStView.constant = 0
                            }
                            
                            self.setTableHeight()
                            
                        }
                        
                        
                    }*/
                    
                   self.responseDict = response as! NSDictionary
                    self.atBranchResponseDict = response as! NSDictionary
                    
                    self.labelFullPaymentAmt.text = "\(self.responseDict.object(forKey: "totalPrice") as! NSInteger) INR"
                    self.labelTotalAmount.text = "\( self.responseDict.object(forKey: "totalPrice") as! NSInteger) INR"
                    self.labelAdvancePaymentAmt.text = "\( self.responseDict.object(forKey: "advanceAmount") as! NSInteger) INR"
                    let tax : NSString = "\( self.responseDict.object(forKey: "totalServiceTax") as! Float) INR" as NSString
                    
                    
                    let forexTAXTuple = ForexAppUtility.getCGST_IGST_Total(quoteDict: self.responseDict)
                    
                    if forexTAXTuple != nil
                    {
                        if let taxType:String = forexTAXTuple.taxType
                        {
                            if taxType == TaxType.CSGST_SGST.rawValue
                            {
                                self.labelTaxCalculationAmount.text = tax as String
                                self.labelCGSTAmount.text = "\(forexTAXTuple.totalCGST ?? 0.0) INR"
                                self.labelSGSTAmount.text = "\(forexTAXTuple.totalSGST ?? 0.0) INR"
                                
                                self.heightConstraintOfSGSTView.constant = 40
                                self.heightConstraintOfCGStView.constant = 40
                                
                              
                                
                            }else if taxType == TaxType.IGST.rawValue{
                                
                                self.labelTaxCalculationAmount.text = tax as String
                                self.labelCGSTAmount.text = "\(forexTAXTuple.totlaIGST ?? 0.0) INR"
                                self.labelCGST.text = "IGST"
                                
                                self.heightConstraintOfSGSTView.constant = 0
                                self.heightConstraintOfCGStView.constant = 40
                            }
                        }
                        
                    }
                    
                    self.setTableHeight()
                    
                    
                /*    let tcilForexQuoteTravellerCollection : NSArray = self.responseDict.object(forKey: "tcilForexQuoteTravellerCollection") as! NSArray
                    if tcilForexQuoteTravellerCollection.count > 0
                    {
                        let travellerDict : NSDictionary =  tcilForexQuoteTravellerCollection[0] as! NSDictionary
                        let travellerServiceTaxDesc : String   = travellerDict.object(forKey: "travellerServiceTaxDesc") as! String
                        if travellerServiceTaxDesc.contains("IGST")
                        {
                            self.labelCGSTAmount.text = tax as String
                            self.labelCGST.text = "IGST"
                            self.labelTaxCalculationAmount.text = tax as String
                            self.heightConstraintOfSGSTView.constant = 0
                            self.heightConstraintOfCGStView.constant = 0
                        }
                        if travellerServiceTaxDesc.contains("CGST") || travellerServiceTaxDesc.contains("SGST")
                        {
                            self.labelTaxCalculationAmount.text = tax as String
                            let taxamount = tax.floatValue
                            
                            self.labelCGSTAmount.text = "\(taxamount/2) INR"
                            self.labelSGSTAmount.text = "\(taxamount/2) INR"
                            self.heightConstraintOfSGSTView.constant = 0
                            self.heightConstraintOfCGStView.constant = 0
                        }
                        self.setTableHeight()
                    }*/
                    
                    /* pathParameter = "/tcForexRS/quote/delivery/\(self.responseDict.object(forKey: "quoteId") as! NSInteger)" as NSString
                     ForexCommunicationManager.sharedInstance.execTask(pathParam:pathParameter , queryParam: "", requestType: "get", jsonDict:NSDictionary())
                     {
                     (status, response) in
                     if(response != nil)
                     {
                     }
                     // self.arrayForCustomerState = NSMutableArray.init(array: jsonArray)  as! [NSDictionary]
                     }*/
                }
            }
            else
            {
                self.showAlert(message: "Some error has occurred")
            }
            }
        }
        //      ForexCommunicationManager.sharedInstance.execTask(pathParam:pathParameter , queryParam: "", requestType: "post", jsonDict:dictOfJson)
        //        {
        //            (status, response) in
        //            if(response != nil)
        //            {
        //                let _ : NSArray = response as! NSArray
        //
        //                // self.arrayForCustomerState = NSMutableArray.init(array: jsonArray)  as! [NSDictionary]
        //            }
        //        }
        
        /*
         {
         "bookerMobileNo": "8978978799",
         "bookerEmailId": "test@test.com",
         "dateOfTravel": "",
         "moduleId": 1,
         "totalNoOfProducts": 1,
         "totalNoOfTraveller": 1,
         "tcilForexQuoteTravellerCollection": [
         {
         "travellerNo": 1,
         "travellerTotalProducts": 1,
         "tcilForexQuoteTravellerProductCollection": [
         {
         "productId": 1,
         "productName": "Cash",
         "currencyId": 20,
         "currencyCode": "USD",
         "currencyName": "US Dollar",
         "nostroAvailable": "N",
         "moduleName": "Sell",
         "roe": 85.7,
         "amount": 1000,
         "equivalentInr": "",
         }
         ]
         }
         ],
         "gstState": "27",
         "gstBranch": "27",
         "gstBranchCode": 1,
         "isStateUt": "N",
         "isBranchUt": "N"
         }
         */
    }
    
    func setUpUI()
    {
        self.responseDict = self.atBranchResponseDict
        
        self.labelFullPaymentAmt.text = "\(self.responseDict.object(forKey: "totalPrice") as! NSInteger) INR"
        self.labelTotalAmount.text = "\( self.responseDict.object(forKey: "totalPrice") as! NSInteger) INR"
        self.labelAdvancePaymentAmt.text = "\( self.responseDict.object(forKey: "advanceAmount") as! NSInteger) INR"
        let tax : NSString = "\( self.responseDict.object(forKey: "totalServiceTax") as! Float) INR" as NSString
        let tcilForexQuoteTravellerCollection : NSArray = self.responseDict.object(forKey: "tcilForexQuoteTravellerCollection") as! NSArray
        if tcilForexQuoteTravellerCollection.count > 0
        {
            let travellerDict : NSDictionary =  tcilForexQuoteTravellerCollection[0] as! NSDictionary
            let travellerServiceTaxDesc : String   = travellerDict.object(forKey: "travellerServiceTaxDesc") as! String
            if travellerServiceTaxDesc.contains("IGST")
            {
                self.labelCGSTAmount.text = tax as String
                self.labelCGST.text = "IGST"
                self.labelTaxCalculationAmount.text = tax as String
            }
            if travellerServiceTaxDesc.contains("CGST") || travellerServiceTaxDesc.contains("SGST")
            {
                self.labelTaxCalculationAmount.text = tax as String
                let taxamount = tax.floatValue
                
                self.labelCGSTAmount.text = "\(taxamount/2)"
                self.labelSGSTAmount.text = "\(taxamount/2)"
                
            }
            
        }
        
    }
    
    func validateFields() -> Bool
    {
        if !tickMarkRadioButton.isSelected
        {
            showAlert(message: "Please accept the Booking Policy and Terms & Condition")
            return false
        }
        return true
    }
    
    //MARK: on tap
    @objc func tapLabel (sender: UITapGestureRecognizer)
    {
        let text = (labelTermsAndCond.text)!
        let termsRange = (text as NSString).range(of: "Terms and conditions")
        let policyRange = (text as NSString).range(of: "Booking policy")
        
        if sender.didTapAttributedTextInLabel(label: labelTermsAndCond, inRange: policyRange)
        {
            let popupVC: PopUpWebViewViewController = PopUpWebViewViewController(nibName: "PopUpWebViewViewController", bundle: nil)
            popupVC.htmlPath = "bookingPolicy"
            popupVC.popuptitle = "Booking Policy"
            popupVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            self.present(popupVC, animated: false, completion: nil)
        }
        else if sender.didTapAttributedTextInLabel(label: labelTermsAndCond, inRange: termsRange)
        {
            //            let popupVC: PopUpWebViewViewController = PopUpWebViewViewController(nibName: "PopUpWebViewViewController", bundle: nil)
            //            popupVC.htmlPath = "holidays/bookingPolicy"
            //            popupVC.popuptitle = "Booking Policy"
            //            popupVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            //            self.present(popupVC, animated: false, completion: nil)
            
            let popupVC1: TermsAndConditionPopUpViewController = TermsAndConditionPopUpViewController(nibName: "TermsAndConditionPopUpViewController", bundle: nil)
            popupVC1.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            self.present(popupVC1, animated: false, completion: nil)
        } else {
            print("Tapped none")
        }
    }
    
    @objc func additionalExpensesViewTapped (sender: UITapGestureRecognizer)
    {
        
        if(heightConstraintDeliverycharges.constant == 0)
        {
            heightConstraintDeliverycharges.constant = 0//50
            
        }
        else
        {
            heightConstraintDeliverycharges.constant = 0
            
            
        }
        setTableHeight()
    }
    
    @objc func taxCalculationViewTapped (sender: UITapGestureRecognizer)
    {
        if labelCGST.text == "CGST"
        {
            if(heightConstraintOfCGStView.constant == 0)
            {
                heightConstraintOfSGSTView.constant = 50
                heightConstraintOfCGStView.constant = 50
            }
            else
            {
                heightConstraintOfSGSTView.constant = 0
                heightConstraintOfCGStView.constant = 0
                
            }
            
        }
        else
        {
            if(heightConstraintOfCGStView.constant == 0)
            {
                heightConstraintOfSGSTView.constant = 0
                heightConstraintOfCGStView.constant = 50
            }
            else
            {
                heightConstraintOfSGSTView.constant = 0
                heightConstraintOfCGStView.constant = 0
                
            }
            
        }
        setTableHeight()
    }
    
    
    // MARK: TableView delegates
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return travellerArray.count
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return travellerArray[section].getProductCount();
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        var cell : ForexTravellerCostTableViewCell? = tableView.dequeueReusableCell(withIdentifier: "identifier") as? ForexTravellerCostTableViewCell
        
        if cell == nil
        {
            cell =  Bundle.main.loadNibNamed("ForexTravellerCostTableViewCell", owner: self, options: nil)?[0] as? ForexTravellerCostTableViewCell;
        }
//        cell?.labelFxAmount.text = travellerArray[indexPath.section].getProduct(atIndex: indexPath.row).getFXAmount() as String
//        cell?.labelINRAmount.text = travellerArray[indexPath.section].getProduct(atIndex: indexPath.row).getINRAmount() as String
        cell?.labelFxAmount.text = "\(travellerArray[indexPath.section].getProduct(atIndex: indexPath.row).getFXAmount()) \(travellerArray[indexPath.section].getProduct(atIndex: indexPath.row).currencyCode)"
        cell?.labelINRAmount.text = "\(travellerArray[indexPath.section].getProduct(atIndex: indexPath.row).getINRAmount()) INR"
        cell?.labelProductName.text = travellerArray[indexPath.section].getProduct(atIndex: indexPath.row).getProductName() as String
        
        
        if indexPath.section == selectedSection && indexPath.row == 0
        {
            cell?.constraintHeight.constant = 40;
        }
        else
        {
            cell?.constraintHeight.constant = 0;
        }
        return cell!;
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        
        let headerView = (Bundle.main.loadNibNamed("HeaderViewPaymentPage", owner: self, options: nil)?[0] as? UIView)
        
        headerView?.tag = section
        let headerTapGesture = UITapGestureRecognizer()
        headerTapGesture.addTarget(self, action: #selector(self.sectionHeaderWasTouched(_:)))
        headerView?.addGestureRecognizer(headerTapGesture)
        
        
        return headerView;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        
        if indexPath.section == selectedSection && indexPath.row == 0
        {
            
            return 70;
            
            
        }
        if indexPath.section == selectedSection
        {
            
            return 40;
            
            
        }
        
        return 0;
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        
        return CGFloat(headerHeight);
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        return 0;
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int)
    {
        //recast your view as a UITableViewHeaderFooterView
        let header: UIView = view
        let travellerlabel : UILabel =  header.viewWithTag(222) as! UILabel
        let totalPayableAmountlabel : UILabel =  header.viewWithTag(333) as! UILabel
        travellerlabel.text = "Traveller \(section+1)"
        totalPayableAmountlabel.text = "\(travellerArray[section].getTravellerAmount()) INR"
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        setTableHeight()
    }
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        setTableHeight()
    }
    @objc func sectionHeaderWasTouched(_ sender: UITapGestureRecognizer)
    {
        let headerView : UIView = sender.view!
        if selectedSection == -1
        {             // if any traveller is deleted or minimized
            selectedSection = headerView.tag
            heightConstraintForTableView.constant = CGFloat(travellerArray.count * 50 + travellerArray[selectedSection].getProductCount() * 40 + 30 )
            self.buyForexPaymentView.layoutIfNeeded()
            tableView.reloadData()
            self.buyForexPaymentView.setNeedsLayout()
        }
        else if (selectedSection == headerView.tag)   // close traveller view on tap of opened traveller view
        {
            heightConstraintForTableView.constant = CGFloat(travellerArray.count * 50)
            self.buyForexPaymentView.layoutIfNeeded()
            selectedSection = -1
            tableView.reloadData()
        }
        else
        {
            selectedSection =  headerView.tag
            heightConstraintForTableView.constant = CGFloat(travellerArray.count * 50 + travellerArray[selectedSection].getProductCount() * 40 + 30 )
            self.buyForexPaymentView.layoutIfNeeded()
            self.tableView.reloadData()
            
        }
        
        /*  let headerView = sender.view!
         
         let section    = headerView.tag
         
         if (self.expandedSectionHeaderNumber == -1)
         {
         self.expandedSectionHeaderNumber = section
         
         tableViewExpandSection(section)
         }
         else
         {
         if (self.expandedSectionHeaderNumber == section)
         {
         tableViewCollapeSection(section)
         }
         else
         {
         tableViewCollapeSection(self.expandedSectionHeaderNumber)
         tableViewExpandSection(section)
         }
         }*/
    }
    
    func tableViewCollapeSection(_ section: Int)
    {
        self.expandedSectionHeaderNumber = -1;
        
        if (travellerArray.count == 0)
        {
            return;
        }
        else
        {
            UIView.animate(withDuration: 0.4, animations:
                {
                    //imageView.transform = CGAffineTransform(rotationAngle: (0.0 * CGFloat(Double.pi)) / 180.0)
            })
            var indexesPath = [IndexPath]()
            for i in 0 ..< travellerArray.count
            {
                let index = IndexPath(row: i, section: section)
                indexesPath.append(index)
            }
            
            //  heightForTableView = travellerArray.count * headerHeight
            
            
            self.tableView!.beginUpdates()
            self.tableView!.deleteRows(at: indexesPath, with: UITableView.RowAnimation.fade)
            self.tableView!.endUpdates()
            self.setTableHeight()
        }
    }
    
    func tableViewExpandSection(_ section: Int)
    {
        if (self.travellerArray.count == 0) {
            self.expandedSectionHeaderNumber = -1;
            return;
        }
        else
        {
            UIView.animate(withDuration: 0.4, animations: {
                //  imageView.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) / 180.0)
            })
            var indexesPath = [IndexPath]()
            for i in 0 ..< self.travellerArray.count {
                let index = IndexPath(row: i, section: section)
                indexesPath.append(index)
            }
            
            if section == 0
            {
                //  heightForTableView = travellerArray.count * headerHeight + (travellerArray.count * rowHeight) + rowHeight;
            }
            else
            {
                // heightForTableView = travellerArray.count * headerHeight + (self.travellerArray.count * rowHeight);
            }
            
            self.expandedSectionHeaderNumber = section
            self.tableView!.beginUpdates()
            self.tableView!.insertRows(at: indexesPath, with: UITableView.RowAnimation.fade)
            self.tableView!.endUpdates()
            self.setTableHeight()
        }
    }
    
    
    // MARK: Button action
    
    @IBAction func fullPaymentRadioButtonClicked(_ sender: Any)
    {
        let normalText = "There is no processing fees for "
        let boldText  = "Advance Payment"
        
        let attributedString = NSMutableAttributedString(string:normalText)
        
        let attrs = [convertFromNSAttributedStringKey(NSAttributedString.Key.font) : UIFont.boldSystemFont(ofSize: 15)]
        let boldString = NSMutableAttributedString(string:boldText, attributes:convertToOptionalNSAttributedStringKeyDictionary(attrs))
        
        attributedString.append(boldString)
        
        labelPaymentTerms.attributedText = attributedString
        
        fullPaymentRadioButton.isSelected = true
        advancepaymentRadioButton.isSelected = false
        labelTotalAmount.text = labelFullPaymentAmt.text
        atBranchRadioButton.isSelected = true
        homeDeliveryRadioButton.isSelected = false
        setTableHeight()
        
        self.paymentType = "F"
    }
    
    @IBAction func onContinueButtonClicked(_ sender: Any)
    {
        if(validateFields())
        {
            let sgst : NSInteger;
            let cgst : NSInteger ;
            let igst : NSInteger ;
            let labelName : NSString = labelCGST.text! as NSString
            if labelName.isEqual(to: "IGST")
            {
                sgst = 0
                cgst = 0
                igst = NSInteger((labelCGSTAmount.text! as NSString).intValue)
            }
            else
            {
                sgst = NSInteger((labelSGSTAmount.text! as NSString).intValue)
                cgst = NSInteger((labelCGSTAmount.text! as NSString).intValue)
                igst = 0
            }
            
            let totalAmt : NSInteger  = NSInteger((labelTotalAmount.text! as NSString).intValue)
            
            let loginStatus = UserDefaults.standard.string(forKey: kLoginStatus)
            print("loginStatus---> \(String(describing: loginStatus))")
            
            if loginStatus == kLoginSuccess
            {
                let userId = UserDefaults.standard.string(forKey: kLoginEmailId)
                
                let buyForxVC : ReloadCardPassengerViewController = ReloadCardPassengerViewController.init(nibName: "ForexBaseViewController", bundle: nil)
                let buyForexPaymentBO : BuyForexPayementBO = BuyForexPayementBO.init(totalTaxAmount:totalAmt, SGST: sgst, CGST: cgst, isAtBranchChecked: atBranchRadioButton.isSelected, isHomeDeliveryChecked: homeDeliveryRadioButton.isSelected, isAdvancePaymentChecked: advancepaymentRadioButton.isSelected, isFullPaymentChecked: fullPaymentRadioButton.isSelected, IGST: igst, totalPayableAmount: totalAmt, tcilForexQuote: responseDict, paymentType: self.paymentType as NSString)
                buyForexBo.addBuyForexPayment(buyForexPayment: buyForexPaymentBO)
                buyForxVC.buyForexBo = buyForexBo
                buyForexBo.emailID = userId!
                self.navigationController?.pushViewController(buyForxVC, animated: true)
            }
            else
            {
                let buyForxVC : ReloadCardLoginViewController = ReloadCardLoginViewController.init(nibName: "ForexBaseViewController", bundle: nil)
                
                let buyForexPaymentBO : BuyForexPayementBO = BuyForexPayementBO.init(totalTaxAmount:totalAmt, SGST: sgst, CGST: cgst, isAtBranchChecked: atBranchRadioButton.isSelected, isHomeDeliveryChecked: homeDeliveryRadioButton.isSelected, isAdvancePaymentChecked: advancepaymentRadioButton.isSelected, isFullPaymentChecked: fullPaymentRadioButton.isSelected, IGST: igst, totalPayableAmount: totalAmt, tcilForexQuote: responseDict, paymentType: self.paymentType as NSString)
                buyForexBo.addBuyForexPayment(buyForexPayment: buyForexPaymentBO)
                buyForxVC.buyForexBo = buyForexBo
                self.navigationController?.pushViewController(buyForxVC, animated: true)
            }
        }
    }
    
    @IBAction func advancePaymentRadioButtonClicked(_ sender: Any)
    {
        
        labelPaymentTerms.text = "Make full payment & avail Delivery at home facility. Delivery charges applicable'"
        
        self.setUpUI()
        fullPaymentRadioButton.isSelected = false
        advancepaymentRadioButton.isSelected = true
        atBranchRadioButton.isSelected = true
        homeDeliveryRadioButton.isSelected = false
        labelTotalAmount.text = labelAdvancePaymentAmt.text
        heightConstraintOfAdditionalExpenses.constant = 0
        heightConstraintDeliverycharges.constant = 0
        heightConstraintOfDeliveryDetail.constant = 0
        taxCalculationView.isUserInteractionEnabled = true
        addGestureRecognizerToTaxCalculationView()
        setTableHeight()
        
        let popupVC1: PopUpPleaseNoteViewController = PopUpPleaseNoteViewController(nibName: "PopUpPleaseNoteViewController", bundle: nil)
        popupVC1.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(popupVC1, animated: false, completion: nil)
        
        self.paymentType = "A"
    }
    
    @IBAction func homeDeliveryRadioButtonClicked(_ sender: Any)
    {
        getHomeDeliveryCharges()
    }
    
    @IBAction func documentsRequiredClicked(_ sender: Any)
    {
        let popupVC1: PopUpDocumentsRequiredViewController = PopUpDocumentsRequiredViewController(nibName: "PopUpDocumentsRequiredViewController", bundle: nil)
        popupVC1.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(popupVC1, animated: false, completion: nil)
    }
    
    @IBAction func termsAndConditionsButtonClicked(_ sender: Any)
    {
        tickMarkRadioButton.isSelected =  !tickMarkRadioButton.isSelected
    }
    
    @IBAction func atBranchRadioButtonClicked(_ sender: Any)
    {
        setUpUI()
        homeDeliveryRadioButton.isSelected =  false
        atBranchRadioButton.isSelected = true
        heightConstraintOfAdditionalExpenses.constant = 0
        heightConstraintOfDeliveryDetail.constant = 0
        heightConstraintDeliverycharges.constant = 0
        taxCalculationView.isUserInteractionEnabled = true
        
        let totalPrice : NSInteger = self.responseDict.object(forKey: "totalPrice") as! NSInteger
        self.labelFullPaymentAmt.text = "\(totalPrice) INR"
        self.labelTotalAmount.text = "\(totalPrice) INR"
        
    }
}


// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromNSAttributedStringKey(_ input: NSAttributedString.Key) -> String {
	return input.rawValue
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}
