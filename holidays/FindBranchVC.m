//
//  FindBranchVC.m
//  holidays
//
//  Created by ROHIT on 12/12/15.
//  Copyright © 2015 Mobicule. All rights reserved.
//

#import "FindBranchVC.h"
#import "MobileNumberPopUpViewController.h"
#define kCityKey @"city"
#define kType @"webservice"
#define kAction @"getBranchDetails"
#define kEntity @"branch"
#define ksmsType @"gupshup"
#define ksmsAction @"sendSms"
#define ksmsEntity @"smsIntegration"
#define kmobileNoKey @"phoneNumber"
#define kaddressKey @"branchAddress"
#define kmapEntity @"nearby"
#define kmapAction @"branch"
#define kmaplatKey @"lat"
#define kmaplongKey @"long"
#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

@interface FindBranchVC ()

@end

@implementation FindBranchVC
{
    
    NSArray * arrForCity;
    NSMutableArray *filterArray;
    NSMutableArray *mapAddressArray;
    NSMutableArray *mapCLlocationsArray;
    NSMutableArray *mapCLlocationsArrayForAnnotation;
    NSMutableArray *floatLatLongArray;
    NSMutableArray *addressArrayForOtherLocation;
    
    NSInteger indexForNearestLocation;
    NSInteger indexForAnotationTitle,indexLoop;
    
    
    CGFloat lattitude;
    CGFloat longitude;
    NSString *strLatitude;
    NSString *strLongitude;
    NSString *mobileNumberForSMS;
    NSString *addressForNearestLoc;
    CLLocation *currentLocation,*nearestLocation,*nearestAnotationLocation;
    
    BOOL isNearestLocation;
    BOOL isCallForFindNearestLocation;
    BOOL isFlagForNearestAnnotation;
    MKPointAnnotation *    pointAnnotation;
    NSString *nearestLocationKey,*nearestLocationAddress;
    CGRect frameForView;
    UITableView * tableViewForSelectCity;
    UITableView * tableViewForSelectBranch;
}

- (void)viewDidLoad {
    //    [super viewDidLoad];
    //
    //   arrForCity=[[NSArray alloc]init];
    //    indexLoop=0;
    //   filterArray=[[NSMutableArray alloc]init];
    //   mapAddressArray=[[NSMutableArray alloc]init];
    //      _buttonpopViewOfSMS.userInteractionEnabled=YES;
    //    isFlagForNearestAnnotation=NO;
    //    [_btnNearestBranch setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    //     tableViewForSelectCity=[[UITableView alloc]init];
    //    tableViewForSelectCity.delegate=self;
    //    tableViewForSelectCity.dataSource=self;
    //     tableViewForSelectBranch=[[UITableView alloc]init];
    //    tableViewForSelectBranch.delegate=self;
    //    tableViewForSelectBranch.dataSource=self;
    //
    //    arrForCity=[NSArray arrayWithObjects:@" AGRA",@" AHMEDABAD", @"AHMEDNAGAR",@"AIZAWL",@"AJMER",@"ALIGARH",@"ALLAHABAD",@"AMBALA",@"AMRITSAR",@"ANAND",@"AURANGABAD",@"BANGALORE",@"BARDEZ",@"BARDOLI",@"BATALA",@"BERHAMPUR",@"BHARUCH",@"BHATINDA",@"BHAVNAGAR",@"BHILAI",@"BHILWARA",@"BHIMAVARAM",@"BHOPAL",@"BHUJ",@"BILASPUR",@"CALICUT",@"CHANDIGARH",@"CHENNAI",@"COIMBATORE",@"DEHRA DUN",@"DELHI",@"DHANBAD",@"DHARAMSALA",@"DIMAPUR",@"DURGAPUR",@"ERODE",@"FARIDABAD",@"GANDHIDHAM",@"GANDHINAGAR",@"GHAZIABAD",@"GOA",@"GUNTUR",@"GURGAON",@"GUWAHATI",@"HALDWANI",@"HISSAR",@"HUBLI",@"HYDERABAD",@"INDORE",@"JABALPUR",@"JAIPUR",@"JAISALMER",@"JALANDHAR",@"JALGAON",@"JAMMU",@"JAMNAGAR",@"JAMSHEDPUR",@"JEYPORE",@"JODHPUR",@"KANNUR",@"KANPUR",@"KANYAKUMARI",@"KARNAL",@"KOCHI",@"KOLHAPUR",@"KOLKATA",@"KOLLAM",@"KOTA",@"KOTTAYAM",@"KOVALAM",nil];
    //
    //    NSLog(@"CITY ARRAY COUNT %ld",[arrForCity count]);
    //
    //
    //    self.txtMobileNumber.delegate=self;
    //    [[NSBundle mainBundle]loadNibNamed:@"FindBranchVC" owner:self options:nil];
    //    [super addViewInBaseView:self.findBranchView];
    //    super.HeaderLabel.text=_headerName;
    //    _txtViewForAddress.layer.borderColor=[[UIColor grayColor]CGColor];
    //    _txtViewForAddress.layer.borderWidth=1.0;
    //     frameForView=self.view.frame;
    
    [[NSBundle mainBundle]loadNibNamed:@"FindBranchVC" owner:self options:nil];
    [super addViewInBaseView:self.findBranchView];
    super.HeaderLabel.text=_headerName;
    
    
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidLoad];
    [self getLatitudeAndLongitude];
    
    arrForCity=[[NSArray alloc]init];
    
    indexLoop=0;
    filterArray=[[NSMutableArray alloc]init];
    mapAddressArray=[[NSMutableArray alloc]init];
    mapCLlocationsArray=[[NSMutableArray alloc]init];
    mapCLlocationsArrayForAnnotation=[[NSMutableArray alloc]init];
    addressArrayForOtherLocation=[[NSMutableArray alloc]init];
    pointAnnotation=[[MKPointAnnotation alloc]init];
    _mapView.delegate=self;
    isCallForFindNearestLocation=YES;
    _buttonpopViewOfSMS.userInteractionEnabled=YES;
    isFlagForNearestAnnotation=NO;
    [_btnNearestBranch setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    tableViewForSelectCity=[[UITableView alloc]init];
    tableViewForSelectCity.delegate=self;
    tableViewForSelectCity.dataSource=self;
    tableViewForSelectBranch=[[UITableView alloc]init];
    tableViewForSelectBranch.delegate=self;
    tableViewForSelectBranch.dataSource=self;
    
    [self getCitiesList];
    
    // arrForCity=[NSArray arrayWithObjects:@" AGRA",@" AHMEDABAD", @"AHMEDNAGAR",@"AIZAWL",@"AJMER",@"ALIGARH",@"ALLAHABAD",@"AMBALA",@"AMRITSAR",@"ANAND",@"AURANGABAD",@"BANGALORE",@"BARDEZ",@"BARDOLI",@"BATALA",@"BERHAMPUR",@"BHARUCH",@"BHATINDA",@"BHAVNAGAR",@"BHILAI",@"BHILWARA",@"BHIMAVARAM",@"BHOPAL",@"BHUJ",@"BILASPUR",@"CALICUT",@"CHANDIGARH",@"CHENNAI",@"COIMBATORE",@"DEHRA DUN",@"DELHI",@"DHANBAD",@"DHARAMSALA",@"DIMAPUR",@"DURGAPUR",@"ERODE",@"FARIDABAD",@"GANDHIDHAM",@"GANDHINAGAR",@"GHAZIABAD",@"GOA",@"GUNTUR",@"GURGAON",@"GUWAHATI",@"HALDWANI",@"HISSAR",@"HUBLI",@"HYDERABAD",@"INDORE",@"JABALPUR",@"JAIPUR",@"JAISALMER",@"JALANDHAR",@"JALGAON",@"JAMMU",@"JAMNAGAR",@"JAMSHEDPUR",@"JEYPORE",@"JODHPUR",@"KANNUR",@"KANPUR",@"KANYAKUMARI",@"KARNAL",@"KOCHI",@"KOLHAPUR",@"KOLKATA",@"KOLLAM",@"KOTA",@"KOTTAYAM",@"KOVALAM",@"KOZHIKODE",@"LEH",@"LUCKNOW",@"LUDHIANA",@"MADURAI",@"MALLESWARAM",@"MANGALORE",@"MARGAO",@"MATHURA",@"MEERUT",@"MEHSANA",@"MOGA",@"MUMBAI",@"MYSORE",@"NADIAD",@"NAGPUR",@"NASIK",@"NAVI MUMBAI",@"NAVSARI",@"NAWANSHAHAR",@"NEW DELHI",@"NOIDA",@"PANAJI",@"PANCHKULA",@"PANIPAT",@"PATIALA",@"PATNA",@"PHAGWARA",@"PONDICHERRY",@"PORBANDAR",@"PUNE",@"PUSHKAR",@"RAIPUR",@"RAJAHMUNDRY",@"RAJKOT",@"RANCHI",@"RATNAGIRI",@"ROORKEE",@"ROURKELA",@"SALEM",@"SILIGURI",@"SRIGANGANAGAR",@"SURAT",@"THANE",@"THANJAVUR",@"THIRUVANANTHAPURAM",@"THRISSUR",@"TIRUCHIRAPPALI",@"TIRUNELVELI",@"TIRUPATI",@"TRIVANDRUM",@"TUMKUR",@"UDAIPUR",@"UDUPI",@"VADODARA",@"VAPI",@"VARANASI",@"VIJAYAWADA",@"VISHAKHAPATANAM",@"WARANGAL",@"ZIRAKPUR",nil];
    // NSLog(@"CITY ARRAY COUNT %ld",[arrForCity count]);
    
    
    self.txtMobileNumber.delegate=self;
    _txtViewForAddress.layer.borderColor=[[UIColor grayColor]CGColor];
    _txtViewForAddress.layer.borderWidth=1.0;
    frameForView=self.view.frame;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)keyboardWasShown:(NSNotification *)notification
{
    
    // Get the size of the keyboard.
    //  CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    //Given size may not account for screen rotation
    
    
    //your other code here..........
}
#pragma ------------------------------------------------
#pragma IBactions Methods
#pragma------------------------------------------------

- (IBAction)actionForbtnSelectBranch:(id)sender {
    
    _btnHighlightedView.frame=CGRectMake(_btnSelectABranch.frame.origin.x, _btnHighlightedView.frame.origin.y, _btnHighlightedView.frame.size.width, _btnHighlightedView.frame.size.height);
    [_btnSelectABranch setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_btnNearestBranch setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    
    for (UIView *view in [self.view subviews]) {
        if ([view isKindOfClass:[FindBranchVC class]]) {
            [view removeFromSuperview];
        }
    }
    [self.findBranchView addSubview:_selectBranchView];
    
    [super addViewInBaseView:self.findBranchView];
    
    
    
    
    //    _btnSelectABranch.enabled=YES;
    //    _btnNearestBranch.enabled=NO;
    
    
}


- (IBAction)actionForbtnNearestBranch:(id)sender {
    
    _btnHighlightedView.frame=CGRectMake(_btnNearestBranch.frame.origin.x, _btnHighlightedView.frame.origin.y, _btnHighlightedView.frame.size.width, _btnHighlightedView.frame.size.height);
    [_btnSelectABranch setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [_btnNearestBranch setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    if (isCallForFindNearestLocation==YES)
    {
        
        NSDictionary *dictOfData=[[NSDictionary alloc]init];
        dictOfData = [NSDictionary dictionaryWithObjectsAndKeys:strLatitude,kmaplatKey,strLongitude,kmaplongKey,nil];
        isCallForFindNearestLocation=NO;
        
        activityIndicator = [LoadingView loadingViewInView:self.nearestBranchView.superview  withString:@"" andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
        
        if ([super connected])
        {
            //        /AIzaSyB9Kma8VC41PEBXYtDGJL6CC0T6FgcWVFI
            //http://maps.google.com/maps/api/geocode/json?latlng=19.392674,72.861643&sensor=false
            NSString  *urlString  = @"http://maps.google.com/maps/api/geocode/json";
            
            NSString *queryParam = [NSString stringWithFormat:@"?API=%@&latlng=%@,%@&sensor=true",@"AIzaSyB9Kma8VC41PEBXYtDGJL6CC0T6FgcWVFI",strLatitude,strLongitude];
            
            
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
            
            NSString *urlStringTotal = [NSString stringWithFormat:@"%@%@",urlString,queryParam];
            
            NSString * encodedString = [urlStringTotal stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
            
            NSURL *url = [NSURL URLWithString:encodedString];
            
            [request setURL:url];
            [request setHTTPMethod:@"GET"];
            
            NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
            [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
              {
                  dispatch_async(dispatch_get_main_queue(), ^{
                      
                      [activityIndicator removeFromSuperview];
                      
                      NSString *responseString = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
                      
                      NSLog(@"Response :- %@",responseString);
                      
                      NSArray *responseFromGoogle = [NSJSONSerialization JSONObjectWithData:[responseString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:nil];
                      
                      NSArray *responseArray = [responseFromGoogle valueForKey:@"results"];
                      
                      if (responseArray != nil)
                      {
                          if (responseArray.count != 0)
                          {
                              NSDictionary *responseDict = responseArray[0];
                              
                              NSString *formatedAddress = [responseDict valueForKey:@"formatted_address"];
                              
                              NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%@ CONTAINS[cd] cityName",formatedAddress];
                              NSArray *arrayWithResult = [arrForCity filteredArrayUsingPredicate:predicate];
                              if (arrayWithResult !=nil)
                              {
                                  if (arrayWithResult.count != 0)
                                  {
                                      
                                      NSString  *cityCode = [[arrayWithResult firstObject] valueForKey:@"cityCode"];
                                      
                                      activityIndicator = [LoadingView loadingViewInView:self.nearestBranchView.superview  withString:@"" andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
                                      
                                      
                                      NSString *pathParameter = @"";
                                      
                                      NetworkHelper *helper = [NetworkHelper sharedHelper];
                                      
                                      NSDictionary *headerDict = [CoreUtility getHeaderDict];
                                      
                                      [helper getResponseWithRequestType:@"GET" withQueryParam:@"" withPathParam:pathParameter withJsonParam:nil withHeaders:headerDict withUrl:[NSString stringWithFormat:@"/tcForexRS/generic/branchdetails/%@",cityCode] success:^(NSDictionary *responseDict)
                                       {
                                           NSLog(@"Response Dict : %@",responseDict);
                                           
                                           dispatch_async(dispatch_get_main_queue(), ^(void) {
                                               [activityIndicator removeFromSuperview];
                                               if (responseDict)
                                               {
                                                   if (responseDict.count>0)
                                                   {
                                                       
                                                       // filterArray = (NSMutableArray *)responseDict;
                                                       
                                                       mapAddressArray = (NSMutableArray *)responseDict;
                                                       
                                                       
                                                       NSMutableDictionary *tempDict =[[NSMutableDictionary alloc]init];
                                                       for (tempDict in mapAddressArray) {
                                                           
                                                           NSString *tempLat= [NSString stringWithFormat:@"%@",[tempDict objectForKey:@"latitude"]];
                                                           NSString *tempLong =[NSString stringWithFormat:@"%@",[tempDict objectForKey:@"longitude"]];
                                                           double strFloatLat=[[NSString stringWithString:tempLat]floatValue];
                                                           double strFloatLong=[[NSString stringWithString:tempLong]floatValue];
                                                           CLLocation *createdLocation = [[CLLocation alloc]initWithLatitude:strFloatLat
                                                                                                                   longitude:strFloatLong];
                                                           [mapCLlocationsArray addObject:createdLocation];
                                                           [mapCLlocationsArrayForAnnotation addObject:createdLocation];
                                                           
                                                       }
                                                       
                                                       addressArrayForOtherLocation = [mapAddressArray mutableCopy];
                                                       
                                                       nearestLocation=[self closestLocationToLocation:currentLocation];
                                                       
                                                       NSString *nearestLatitude=  [[NSNumber numberWithFloat:nearestLocation.coordinate.latitude] stringValue];
                                                       NSString *nearestLongitude=[[NSNumber numberWithFloat:nearestLocation.coordinate.longitude] stringValue];
                                                       nearestLatitude = [NSString stringWithFormat:@"%.4f",nearestLocation.coordinate.latitude];
                                                       nearestLongitude = [NSString stringWithFormat:@"%.4f",nearestLocation.coordinate.longitude];
                                                       
                                                       ///FINDING NEAREST LOCATION
                                                       NSString * firstVlue, * secondValue,*tempTitle,*tempSubTitle;
                                                       for(int i=0;i<mapCLlocationsArray.count;i++){
                                                           CLLocation *location=[mapCLlocationsArray objectAtIndex:i];
                                                           double fourPrecisionlatitude=location.coordinate.latitude;
                                                           double fourPrecisionlongitude=location.coordinate.longitude;
                                                           firstVlue = [NSString stringWithFormat:@"%.4f",fourPrecisionlatitude];
                                                           secondValue = [NSString stringWithFormat:@"%.4f",fourPrecisionlongitude];
                                                           
                                                           if ([firstVlue isEqualToString:nearestLatitude]&&[secondValue isEqualToString:nearestLongitude]) {
                                                               isNearestLocation=YES;
                                                               
                                                               _lblNearestAddress.text=[[mapAddressArray objectAtIndex:i ] objectForKey:@"address1"];
                                                               tempTitle=[[mapAddressArray objectAtIndex:i ] objectForKey:@"address1"];
                                                               _lblNearestPhone.text=[[mapAddressArray objectAtIndex:i] objectForKey:@"mobile"];
                                                               _lblNearestBranchName.text=[[mapAddressArray objectAtIndex:i]  objectForKey:@"branchName"];
                                                               
                                                               tempSubTitle=[[mapAddressArray objectAtIndex:i]  objectForKey:@"branchName"];
                                                               nearestLocationAddress=[NSString stringWithFormat:@"%@,%@",_lblNearestAddress.text,_lblNearestBranchName];
                                                               
                                                               nearestAnotationLocation=[mapCLlocationsArray objectAtIndex:i];
                                                               indexForNearestLocation=i;
                                                               
                                                           }
                                                           NSLog(@"first :: %f and second :: %f",[firstVlue floatValue],[secondValue floatValue]);
                                                       }
                                                       
                                                       [mapCLlocationsArrayForAnnotation removeObjectAtIndex:indexForNearestLocation];
                                                       [addressArrayForOtherLocation removeObjectAtIndex:indexForNearestLocation];
                                                       ///Add Annotation on map View
                                                       [self createAddressString];
                                                       //                isFlagForNearestAnnotation=YES;
                                                       //                 [self createAddressString];
                                                       //
                                                       
                                                       
                                                       MKPointAnnotation*    annotation = [[MKPointAnnotation alloc] init];
                                                       CLLocationCoordinate2D myCoordinate;
                                                       myCoordinate.latitude=nearestAnotationLocation.coordinate.latitude;
                                                       myCoordinate.longitude=nearestAnotationLocation.coordinate.longitude;
                                                       
                                                       
                                                       MKCoordinateRegion region=MKCoordinateRegionMakeWithDistance(myCoordinate, 250,250);
                                                       [self.mapView setRegion:region];
                                                       
                                                       annotation.coordinate=myCoordinate;
                                                       
                                                       [annotation setTitle:tempTitle];
                                                       [annotation setSubtitle:tempSubTitle];
                                                       nearestLocationAddress=tempTitle;
                                                       
                                                       // pointAnnotation.title=appendedAddressString;
                                                       [self.mapView addAnnotation: annotation];
                                                       
                                                       for (UIView *view in [self.view subviews]) {
                                                           if ([view isKindOfClass:[FindBranchVC class]]) {
                                                               [view removeFromSuperview];
                                                           }
                                                       }
                                                       
                                                       
                                                       [self.findBranchView addSubview:_nearestBranchView];
                                                       
                                                       
                                                       [super addViewInBaseView:self.findBranchView];
                                                       
                                                       
                                                       
                                                       // [tableViewForSelectBranch reloadData];
                                                       
                                                   }
                                               }
                                           });
                                           
                                       }
                                                                 failure:^(NSError *error)
                                       {
                                           dispatch_async(dispatch_get_main_queue(), ^(void)
                                                          {
                                                              [activityIndicator removeFromSuperview];
                                                              NSLog(@"Response Dict : %@",[error description]);
                                                          });
                                           
                                       }];
                                      
                                      
                                  }
                              }
                          }
                      }
                  });
                  
              }] resume];
            
            
            
            /*dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
             
             NetworkHelper *helper = [NetworkHelper sharedHelper];
             
             NSString *strResponse = [helper getDataFromServerForType:kType entity:kmapEntity action:kmapAction andUserJson:dictOfData];
             
             NSString *strStatus = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"status"];
             NSDictionary *dictOfData=[[NSDictionary alloc]init];
             if ([strStatus isEqualToString:kStatusSuccess])
             {
             dictOfData= [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:kServerResponseKeyData];
             
             mapAddressArray=[dictOfData objectForKey:@"branchList"];
             
             
             NSMutableDictionary *tempDict =[[NSMutableDictionary alloc]init];
             for (tempDict in mapAddressArray) {
             
             NSString *tempLat=[tempDict objectForKey:@"latitude"];
             NSString *tempLong=[tempDict objectForKey:@"longitude"];
             double strFloatLat=[[NSString stringWithString:tempLat]floatValue];
             double strFloatLong=[[NSString stringWithString:tempLong]floatValue];
             CLLocation *createdLocation = [[CLLocation alloc]initWithLatitude:strFloatLat
             longitude:strFloatLong];
             [mapCLlocationsArray addObject:createdLocation];
             [mapCLlocationsArrayForAnnotation addObject:createdLocation];
             
             }
             
             addressArrayForOtherLocation = [mapAddressArray mutableCopy];
             
             nearestLocation=[self closestLocationToLocation:currentLocation];
             
             NSString *nearestLatitude=  [[NSNumber numberWithFloat:nearestLocation.coordinate.latitude] stringValue];
             NSString *nearestLongitude=[[NSNumber numberWithFloat:nearestLocation.coordinate.longitude] stringValue];
             nearestLatitude = [NSString stringWithFormat:@"%.4f",nearestLocation.coordinate.latitude];
             nearestLongitude = [NSString stringWithFormat:@"%.4f",nearestLocation.coordinate.longitude];
             
             
             
             //                let x = 1.23556789
             //                let y = Double(round(1000*x)/1000)
             //                print(y)
             
             ///FINDING NEAREST LOCATION
             NSString * firstVlue, * secondValue,*tempTitle,*tempSubTitle;
             for(int i=0;i<mapCLlocationsArray.count;i++){
             CLLocation *location=[mapCLlocationsArray objectAtIndex:i];
             double fourPrecisionlatitude=location.coordinate.latitude;
             double fourPrecisionlongitude=location.coordinate.longitude;
             firstVlue = [NSString stringWithFormat:@"%.4f",fourPrecisionlatitude];
             secondValue = [NSString stringWithFormat:@"%.4f",fourPrecisionlongitude];
             
             if ([firstVlue isEqualToString:nearestLatitude]&&[secondValue isEqualToString:nearestLongitude]) {
             isNearestLocation=YES;
             _lblNearestAddress.text=[[mapAddressArray objectAtIndex:i ] objectForKey:@"address"];
             tempTitle=[[mapAddressArray objectAtIndex:i ] objectForKey:@"address"];
             _lblNearestPhone.text=[[mapAddressArray objectAtIndex:i] objectForKey:@"phone"];
             _lblNearestBranchName.text=[[mapAddressArray objectAtIndex:i]  objectForKey:@"branchName"];
             
             tempSubTitle=[[mapAddressArray objectAtIndex:i]  objectForKey:@"branchName"];
             nearestLocationAddress=[NSString stringWithFormat:@"%@,%@",_lblNearestAddress.text,_lblNearestBranchName];
             
             nearestAnotationLocation=[mapCLlocationsArray objectAtIndex:i];
             indexForNearestLocation=i;
             
             }
             NSLog(@"first :: %f and second :: %f",[firstVlue floatValue],[secondValue floatValue]);
             }
             
             [mapCLlocationsArrayForAnnotation removeObjectAtIndex:indexForNearestLocation];
             [addressArrayForOtherLocation removeObjectAtIndex:indexForNearestLocation];
             ///Add Annotation on map View
             [self createAddressString];
             //                isFlagForNearestAnnotation=YES;
             //                 [self createAddressString];
             //
             
             
             MKPointAnnotation*    annotation = [[MKPointAnnotation alloc] init];
             CLLocationCoordinate2D myCoordinate;
             myCoordinate.latitude=nearestAnotationLocation.coordinate.latitude;
             myCoordinate.longitude=nearestAnotationLocation.coordinate.longitude;
             
             
             MKCoordinateRegion region=MKCoordinateRegionMakeWithDistance(myCoordinate, 250,250);
             [self.mapView setRegion:region];
             
             annotation.coordinate=myCoordinate;
             
             [annotation setTitle:tempTitle];
             [annotation setSubtitle:tempSubTitle];
             nearestLocationAddress=tempTitle;
             
             // pointAnnotation.title=appendedAddressString;
             [self.mapView addAnnotation: annotation];
             
             for (UIView *view in [self.view subviews]) {
             if ([view isKindOfClass:[FindBranchVC class]]) {
             [view removeFromSuperview];
             }
             }
             
             
             [self.findBranchView addSubview:_nearestBranchView];
             
             
             [super addViewInBaseView:self.findBranchView];
             
             
             
             
             dispatch_async(dispatch_get_main_queue(), ^{
             
             
             });
             
             }
             else
             {
             NSString *messsage = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"message"];
             dispatch_async(dispatch_get_main_queue(), ^{
             
             if ([activityIndicator isDescendantOfView:self.findBranchView.superview])
             {
             [activityIndicator removeView];
             }
             
             if (messsage)
             
             {
             
             if ([messsage isEqualToString:kVersionUpgradeMessage])
             
             {
             
             NSDictionary *downloadDict = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"data"];
             NSString *downloadURL = [downloadDict valueForKey:@"download_url"];
             [super showAlertViewForVersionUpdateWithUrl:downloadURL];
             
             
             }
             
             }
             if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
             {
             UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Info"
             message:@"Sorry we don’t have TCIL Branch in this city"
             preferredStyle:UIAlertControllerStyleAlert];
             
             UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
             
             [alertView dismissViewControllerAnimated:YES completion:nil];
             
             }];
             
             [alertView addAction:actionOk];
             
             for (UIView *view in [self.view subviews]) {
             if ([view isKindOfClass:[FindBranchVC class]]) {
             [view removeFromSuperview];
             }
             }
             
             
             [self.findBranchView addSubview:_nearestBranchView];
             
             
             [super addViewInBaseView:self.findBranchView];
             
             
             [self presentViewController:alertView animated:YES completion:nil];
             }
             else
             {
             UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Info" message:@"   " delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             
             [alert show];
             }
             
             
             });
             }
             
             });*/
        }
        else
        {
            if([activityIndicator isDescendantOfView:self.findBranchView.superview])
            {
                [activityIndicator removeView];
            }
            
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
            {
                UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Info"
                                                                                   message:kMessageNoInternet
                                                                            preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    
                    [alertView dismissViewControllerAnimated:YES completion:nil];
                    
                }];
                
                [alertView addAction:actionOk];
                
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Info"
                                                               message:kMessageNoInternet
                                                              delegate:nil
                                                     cancelButtonTitle:@"OK"
                                                     otherButtonTitles:nil, nil];
                
                [alert show];
            }
        }
        
    }//end if for isApicall
    else {
        
        for (UIView *view in [self.view subviews]) {
            if ([view isKindOfClass:[FindBranchVC class]]) {
                [view removeFromSuperview];
            }
        }
        
        
        [self.findBranchView addSubview:_nearestBranchView];
        
        
        [super addViewInBaseView:self.findBranchView];
    }
    
}

- (IBAction)actionForSelectCityPopUp:(id)sender {
    
    _txtSelectCity.text=@"-Select City-";
    _txtSelectBranch.text=@"-Select Branch-";
    _txtViewForAddress.text=@" ";
    if (filterArray.count>0) {
        filterArray=nil;
    }
    [tableViewForSelectCity reloadData];
    [tableViewForSelectBranch reloadData];
    UIView *viewPopUpNightCount =   [self popUpViewForSelectCity];
    
    customePopUp = [KLCPopup popupWithContentView:viewPopUpNightCount
                                         showType:KLCPopupShowTypeGrowIn
                                      dismissType:KLCPopupDismissTypeFadeOut
                                         maskType:KLCPopupMaskTypeDimmed
                         dismissOnBackgroundTouch:YES
                            dismissOnContentTouch:NO];
    [customePopUp show];
    
}

- (IBAction)actionForSelectBranchPopUp:(id)sender {
    
    if ([_txtSelectCity.text isEqualToString:@"-Select City-"])
    {
        [super showAlertViewWithTitle:@"Alert" withMessage:@"Please select city first"];
    }
    else if (filterArray != nil || filterArray.count != 0)
    {
        UIView *viewPopUpNightCount =   [self popUpViewForSelectBranch];
        [tableViewForSelectCity reloadData];
        [tableViewForSelectBranch reloadData];
        
        customePopUp = [KLCPopup popupWithContentView:viewPopUpNightCount
                                             showType:KLCPopupShowTypeGrowIn
                                          dismissType:KLCPopupDismissTypeFadeOut
                                             maskType:KLCPopupMaskTypeDimmed
                             dismissOnBackgroundTouch:YES
                                dismissOnContentTouch:NO];
        
        
        [customePopUp show];
    }
    else
    {
        //[super showAlertViewWithTitle:@"Alert" withMessage:@"No branch found for this city"];
        [super showAlertViewWithTitle:@"Alert" withMessage:@"Sorry we don’t have TCIL Branch in this city"];
    }
    
    
    
}

- (IBAction)actionForSMSSend:(id)sender {
    
    
    //for iphone 4
    //    if([[UIScreen mainScreen]bounds].size.height<481)
    //    {
    //
    //    }
    _txtMobileNumber.text=@"";
    self.viewForSMSPopUp.hidden=NO;
    self.viewForSMSPopUp.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    
    if ([_txtSelectBranch.text isEqualToString:@"-Select Branch-"]) {
        _viewForSMSPopUp.hidden=YES;
        _viewForPopUpMessage.hidden=NO;
        _viewForPopUpMessage.alpha=1.0f;
        [UIView animateWithDuration:0.5 delay:1.0 options:0 animations:^{
            _viewForPopUpMessage.alpha=0.0f;
        } completion:^(BOOL finished) {
            _viewForPopUpMessage.hidden=YES;
        }];
        
        
    }else{
        if (_lblNearestBranchName.text.length>0 || _txtViewForAddress.text.length>0 ) {
            
            self.viewForSMSPopUp.hidden=NO;
            _buttonpopViewOfSMS.userInteractionEnabled=YES;
            
            UIView *viewPopUpNightCount =   [self viewForSMSPopUp];
            
            if(viewPopUpNightCount==nil){
                
                _viewForPopUpMessage.hidden=NO;
                _viewForPopUpMessage.alpha=1.0f;
                [UIView animateWithDuration:0.5 delay:1.0 options:0 animations:^{
                    _viewForPopUpMessage.alpha=0.0f;
                } completion:^(BOOL finished) {
                    _viewForPopUpMessage.hidden=YES;
                }];
                
                
            }else{
                //viewPopUpNightCount
                _txtMobileNumber.delegate=self;
                
                customePopUp = [KLCPopup popupWithContentView:viewPopUpNightCount
                                                     showType:KLCPopupShowTypeGrowIn
                                                  dismissType:KLCPopupDismissTypeFadeOut
                                                     maskType:KLCPopupMaskTypeDimmed
                                     dismissOnBackgroundTouch:YES
                                        dismissOnContentTouch:NO];
                
                
                [customePopUp show];
                
            }
            
        }else{
            self.viewForSMSPopUp.hidden=YES;
            [customePopUp dismiss:YES];
            _viewForPopUpMessage.hidden=NO;
            _viewForPopUpMessage.alpha=1.0f;
            [UIView animateWithDuration:0.5 delay:1.0 options:0 animations:^{
                _viewForPopUpMessage.alpha=0.0f;
            } completion:^(BOOL finished) {
                _viewForPopUpMessage.hidden=YES;
            }];
            
        }
    }
    
}

- (IBAction)actionForSENDSMS:(id)sender {
    self.viewForSMSPopUp.hidden=YES;
    
    //[self.viewForSMSPopUp removeFromSuperview];
    
    
    [self.view endEditing:YES];
    NSString *strMobileNumber;
    NSString *branchAddress;
    if (_txtViewForAddress.text.length>0) {
        strMobileNumber=_txtMobileNumber.text;
        branchAddress=_txtViewForAddress.text;
    }else{
        strMobileNumber=_txtMobileNumber.text;
        branchAddress=addressForNearestLoc;
        
    }
    if(strMobileNumber.length==0)
    {
        self.viewForSMSPopUp.hidden=NO;
        [super showAlertViewWithTitle:@"Alert" withMessage:@"Please Enter Mobile Number"];
        
    }
    
    if(strMobileNumber.length!=10)
    {
        self.viewForSMSPopUp.hidden=NO;
        
        [super showAlertViewWithTitle:@"Alert" withMessage:@"Please Enter 10 digit Number"];
        
    }
    
    
    if([self validatePhoneNumber]){
        
        NSDictionary *dictOfData = [NSDictionary dictionaryWithObjectsAndKeys:strMobileNumber,kmobileNoKey,branchAddress,kaddressKey,nil];
        
        activityIndicator = [LoadingView loadingViewInView:self.findBranchView.superview  withString:@"" andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
        
        if ([super connected])
        {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                NetworkHelper *helper = [NetworkHelper sharedHelper];
                
                NSString *strResponse = [helper getDataFromServerForType:ksmsType entity:ksmsEntity action:ksmsAction andUserJson:dictOfData];
                
                NSString *strStatus = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"status"];
                NSDictionary *dictOfData=[[NSDictionary alloc]init];
                dictOfData= [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:kServerResponseKeyData];
                dispatch_async(dispatch_get_main_queue(), ^{
                    if ([strStatus isEqualToString:kStatusSuccess])
                    {
                        
                        
                        if ([activityIndicator isDescendantOfView:self.findBranchView.superview])
                        {
                            [activityIndicator removeView];
                        }
                        
                        [super showAlertViewWithTitle:@"Success" withMessage:@"message sent"];
                        
                        [customePopUp dismiss:YES];
                        
                        // [self.view dismissPresentingPopup];
                    }
                    else
                    {
                        NSString *messsage = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"message"];
                        
                        
                        if ([activityIndicator isDescendantOfView:self.findBranchView.superview])
                        {
                            [activityIndicator removeView];
                        }
                        if (messsage)
                            
                        {
                            
                            if ([messsage isEqualToString:kVersionUpgradeMessage])
                                
                            {
                                
                                NSDictionary *downloadDict = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"data"];
                                NSString *downloadURL = [downloadDict valueForKey:@"download_url"];
                                [super showAlertViewForVersionUpdateWithUrl:downloadURL];
                                
                                
                            }
                            
                        }
                        
                        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
                        {
                            UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Info"
                                                                                               message:@"Sorry we don’t have TCIL Branch in this city"
                                                                                        preferredStyle:UIAlertControllerStyleAlert];
                            
                            UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                                
                                [alertView dismissViewControllerAnimated:YES completion:nil];
                                
                            }];
                            
                            [alertView addAction:actionOk];
                            
                            [self presentViewController:alertView animated:YES completion:nil];
                        }
                        else
                        {
                            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Info" message:@"Sorry we don’t have TCIL Branch in this city" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                            
                            [alert show];
                        }
                    }
                });
            });
            
        }
        else
        {
            if([activityIndicator isDescendantOfView:self.findBranchView.superview])
            {
                [activityIndicator removeView];
            }
            
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
            {
                UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Info"
                                                                                   message:kMessageNoInternet
                                                                            preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    
                    [alertView dismissViewControllerAnimated:YES completion:nil];
                    [self.view dismissPresentingPopup];
                    
                }];
                
                [alertView addAction:actionOk];
                
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Info"
                                                               message:kMessageNoInternet
                                                              delegate:nil
                                                     cancelButtonTitle:@"OK"
                                                     otherButtonTitles:nil, nil];
                
                [alert show];
            }
        }
    }else{
        self.viewForSMSPopUp.hidden=NO;
        [super showAlertViewWithTitle:@"Alert" withMessage:@"Please Enter Valid Phone number"];
        
    }
    
}

- (IBAction)nearestSendAddress:(id)sender {
    
    _txtMobileNumber.text=@"";
    self.viewForSMSPopUp.hidden=NO;
    self.viewForSMSPopUp.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    // self.viewForSMSPopUp=[[UIView alloc]init];
    
    if (_lblNearestBranchName.text.length>0) {
        
        self.viewForSMSPopUp.hidden=NO;
        _buttonpopViewOfSMS.userInteractionEnabled=YES;
        UIView *viewPopUpNightCount =   [self viewForSMSPopUp];
        
        customePopUp = [KLCPopup popupWithContentView:viewPopUpNightCount
                                             showType:KLCPopupShowTypeGrowIn
                                          dismissType:KLCPopupDismissTypeFadeOut
                                             maskType:KLCPopupMaskTypeDimmed
                             dismissOnBackgroundTouch:YES
                                dismissOnContentTouch:NO];
        [customePopUp show];
        addressForNearestLoc=[NSString stringWithFormat:@"%@,%@",_lblNearestAddress.text,_lblNearestBranchName.text];
        
        
    }
    else{
        self.viewForSMSPopUp.hidden=YES;
        [customePopUp dismiss:YES];
    }
    
}






#pragma mark - Pop Up View Creation
-(UIView *)popUpViewForSelectCity{
    UIView *popUpView = [UIView new];
    popUpView.frame = CGRectMake(0, 0, 250, 350);
    popUpView.backgroundColor = [UIColor whiteColor];
    popUpView.layer.cornerRadius = 1.0;
    
    
    
    tableViewForSelectCity.frame=CGRectMake(popUpView.frame.origin.x, popUpView.frame.origin.y+36
                                            , popUpView.frame.size.width, popUpView.frame.size.height);
    
    tableViewForSelectCity.tag=3001;
    if (tableViewForSelectCity.tag==3001) {
        //        tableViewForSelectCity.delegate=self;
        //        tableViewForSelectCity.dataSource=self;
    }
    tableViewForSelectCity.userInteractionEnabled=YES;
    
    
    UIView *viewHeader = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 250, 45)];
    viewHeader.backgroundColor = [UIColor colorFromHexString:@"#0066FF"];
    
    UILabel *lblHeader = [[UILabel alloc]initWithFrame:CGRectMake(0, 14, 130, 21)];
    lblHeader.font = [UIFont fontWithName:TITILLUM_SEMIBOLD size:17];
    lblHeader.text = @"Select City";
    lblHeader.textColor = [UIColor whiteColor];
    lblHeader.textAlignment = NSTextAlignmentCenter;
    tableViewForSelectCity.delegate=self;
    tableViewForSelectCity.dataSource=self;
    [viewHeader addSubview:lblHeader];
    lblHeader.center = viewHeader.center;
    
    [popUpView addSubview:viewHeader];
    [popUpView addSubview:tableViewForSelectCity];
    
    
    
    [tableViewForSelectCity reloadData];
    
    return popUpView;
}

-(UIView *)popUpViewForSelectBranch{
    UIView *popUpView = [UIView new];
    popUpView.frame = CGRectMake(0, 0, 300, 300);
    popUpView.backgroundColor = [UIColor whiteColor];
    popUpView.layer.cornerRadius = 1.0;
    
    tableViewForSelectBranch.frame=CGRectMake(popUpView.frame.origin.x, popUpView.frame.origin.y+36
                                              , popUpView.frame.size.width, popUpView.frame.size.height);
    tableViewForSelectBranch.tag=4001;
    
    
    
    UIView *viewHeader = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 300, 45)];
    viewHeader.backgroundColor = [UIColor colorFromHexString:@"#0066FF"];
    
    UILabel *lblHeader = [[UILabel alloc]initWithFrame:CGRectMake(0, 14, 130, 21)];
    lblHeader.font = [UIFont fontWithName:TITILLUM_SEMIBOLD size:17];
    lblHeader.text = @"Select Branch";
    lblHeader.textColor = [UIColor whiteColor];
    lblHeader.textAlignment = NSTextAlignmentCenter;
    
    [viewHeader addSubview:lblHeader];
    
    lblHeader.center = viewHeader.center;
    
    [popUpView addSubview:viewHeader];
    [popUpView addSubview:tableViewForSelectBranch];
    
    
    return popUpView;
}

-(UIView *)popUpViewForSendSMS{
    UIView *popUpView = [UIView new];
    popUpView.frame = CGRectMake(0, 0, 300, 300);
    popUpView.backgroundColor = [UIColor whiteColor];
    popUpView.layer.cornerRadius = 1.0;
    
    UITextField *mobileNumberText=[[UITextField alloc]init];
    mobileNumberText.frame=CGRectMake(12, popUpView.center.y, 280, 30);
    mobileNumberText.layer.borderColor=[[UIColor lightGrayColor]CGColor];
    [mobileNumberText setKeyboardType:UIKeyboardTypeNumberPad];
    
    [popUpView addSubview:mobileNumberText];
    
    return popUpView;
    
    
}
#pragma mark - TableView Delegate and DataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (tableView.tag==3001) {
        return arrForCity.count;
    }else if(tableView.tag==4001){
        if (filterArray.count>0) {
            return filterArray.count;
            
        }
    }
    return 1;
    
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    if (tableView.tag==3001) {
        
        cell.textLabel.adjustsFontSizeToFitWidth = YES;
        //         UILabel *lblForAddress=[[UILabel alloc]init];
        //         lblForAddress.frame=CGRectMake(cell.frame.origin.x,cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height);
        //        lblForAddress.tag=indexPath.row;
        //         lblForAddress.text=[arrForCity objectAtIndex:indexPath.row];
        //         [cell addSubview:lblForAddress];
        
        
        NSDictionary *dictForCity = [arrForCity objectAtIndex:indexPath.row];
        NSString *cityName = [dictForCity valueForKey:@"cityName"];
        
        cell.textLabel.text = cityName;
        
    }else if(tableView.tag==4001){
        //cell.textLabel.adjustsFontSizeToFitWidth = YES;
        if (filterArray.count>0)
        {
            cell.textLabel.text=[[ filterArray objectAtIndex:indexPath.row] valueForKey:@"branchName"];
        }
        //cell.textLabel.text=@"";
    }
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag==3001)
    {
        NSDictionary *dictForCity = [arrForCity objectAtIndex:indexPath.row];
        NSString *cityName = [dictForCity valueForKey:@"cityName"];
        NSString *cityCode = [dictForCity valueForKey:@"cityCode"];
        
        _txtSelectCity.text = cityName;
        [customePopUp dismiss:YES];
        // [tableView reloadData];
        
        //        NSString *strCity=_txtSelectCity.text;
        //        NSDictionary *dictOfData = [NSDictionary dictionaryWithObjectsAndKeys:strCity,kCityKey,nil];
        
        //        activityIndicator = [LoadingView loadingViewInView:self.findBranchView.superview  withString:@"" andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
        
        if ([super connected])
        {
            
            
            [self getBranchDetailsUsingCityCode:cityCode];
            
            //           dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            //             NetworkHelper *helper = [NetworkHelper sharedHelper];
            
            //                NSString *strResponse = [helper getDataFromServerForType:kType entity:kEntity action:kAction andUserJson:dictOfData];
            //
            //                NSString *strStatus = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"status"];
            //
            //                NSDictionary *dictOfData=[[NSDictionary alloc]init];
            //                if ([strStatus isEqualToString:kStatusSuccess])
            //                {
            //                    dictOfData= [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:kServerResponseKeyData];
            //
            //                    filterArray=[dictOfData objectForKey:@"branchList"] ;
            //
            //                    NSLog(@"Dictionary Response %@",filterArray);
            //
            //                    dispatch_async(dispatch_get_main_queue(), ^{
            //
            //                        if ([activityIndicator isDescendantOfView:self.findBranchView.superview])
            //                        {
            //                            [activityIndicator removeView];
            //                        }
            //
            //
            //                    });
            //
            //                }
            //                else
            //                {
            //
            //                    if (filterArray.count>0) {
            //                        filterArray=nil;
            //                    }
            //                    [tableViewForSelectBranch reloadData];
            //                    NSString *messsage = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"message"];
            //
            //
            //                    dispatch_async(dispatch_get_main_queue(), ^{
            //
            //                        if ([activityIndicator isDescendantOfView:self.findBranchView.superview])
            //                        {
            //                            [activityIndicator removeView];
            //                        }
            //
            //                        if (messsage)
            //                        {
            //
            //                            if ([messsage isEqualToString:kVersionUpgradeMessage])
            //
            //                            {
            //
            //                                NSDictionary *downloadDict = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"data"];
            //                                NSString *downloadURL = [downloadDict valueForKey:@"download_url"];
            //                                [super showAlertViewForVersionUpdateWithUrl:downloadURL];
            //
            //
            //                            }
            //
            //                        }
            //                        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
            //                        {
            //                            UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Info"
            //                                                                                               message:@"Sorry we don’t have TCIL Branch in this city"
            //                                                                                        preferredStyle:UIAlertControllerStyleAlert];
            //
            //                            UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            //
            //                                [alertView dismissViewControllerAnimated:YES completion:nil];
            //
            //                            }];
            //
            //                            [alertView addAction:actionOk];
            //
            //                           // [self presentViewController:alertView animated:YES completion:nil];
            //                        }
            //                        else
            //                        {
            //                                //                            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Info" message:@"No City branch available." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            //                               //
            //                                // [alert show];
            //                        }
            //                    });
            //                }
            //
            //            });
            
        }
        else
        {
            if([activityIndicator isDescendantOfView:self.findBranchView.superview])
            {
                [activityIndicator removeView];
            }
            
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
            {
                UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Info"
                                                                                   message:kMessageNoInternet
                                                                            preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    
                    [alertView dismissViewControllerAnimated:YES completion:nil];
                    
                }];
                
                [alertView addAction:actionOk];
                
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Info"
                                                               message:kMessageNoInternet
                                                              delegate:nil
                                                     cancelButtonTitle:@"OK"
                                                     otherButtonTitles:nil, nil];
                
                [alert show];
            }
        }
        
        
        
    }
    // [tableView reloadData];
    
    if (filterArray.count>0) {
        if (tableView.tag==4001)
        {
            
            _txtViewForAddress.text=[NSString stringWithFormat:@"%@ %@ %@ %@ %@",[[filterArray objectAtIndex:indexPath.row] valueForKey:@"address1"],[[filterArray objectAtIndex:indexPath.row] valueForKey:@"address2"],[[filterArray objectAtIndex:indexPath.row] valueForKey:@"branchName"],[[filterArray objectAtIndex:indexPath.row] valueForKey:@"state"],[[filterArray objectAtIndex:indexPath.row] valueForKey:@"zipCode"]];
            
            _txtSelectBranch.text=[[filterArray objectAtIndex:indexPath.row]valueForKey:@"branchName"];
            [customePopUp dismiss:YES];
            [tableView reloadData];
        }
    }
    else{
        filterArray=nil;
        [tableViewForSelectBranch reloadData];
        [tableView reloadData];
        _txtViewForAddress.text=@"";
        _txtSelectBranch.text=@"-Select Branch-";
        [customePopUp dismiss:YES];
        
    }
    
}

#pragma mark -MapView and MapViewDelegate Methods


-(void)getLatitudeAndLongitude
{
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate=self;
    locationManager.desiredAccuracy=kCLLocationAccuracyBest;
    // locationManager.distanceFilter=kCLDistanceFilterNone;
    
    if(IS_OS_8_OR_LATER)
    {
        [locationManager requestAlwaysAuthorization];
    }
    //[locationManager startMonitoringSignificantLocationChanges];
    
    [locationManager startUpdatingLocation];
}
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    
    
    
    CLLocation *newLocation = [locations lastObject];
    
    NSLog(@"didUpdateToLocation: %@", newLocation);
    if (newLocation != nil) {
        lattitude = newLocation.coordinate.latitude;
        longitude =  newLocation.coordinate.longitude;
        strLatitude=[[NSNumber numberWithFloat:lattitude] stringValue];
        strLongitude=[[NSNumber numberWithFloat:longitude]stringValue];
        currentLocation=newLocation;
        
        // Stop Location Manager
        [locationManager stopUpdatingLocation];
        
    }
    
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(currentLocation.coordinate, 10000, 10000);
    [self.mapView setRegion:[self.mapView regionThatFits:region] animated:YES];
}
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    [locationManager startUpdatingLocation];
}


- (CLLocation*)closestLocationToLocation:(CLLocation*)currLocation
{
    CLLocation *closestLocation;
    CLLocationDistance closestDistance = DBL_MAX;
    
    for (CLLocation* location in mapCLlocationsArray) {
        CLLocationDistance distance = [currLocation distanceFromLocation:location];
        if (distance < closestDistance) {
            closestLocation = location;
            closestDistance = distance;
        }
    }
    return  closestLocation;
    
}

-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation{
    
    MKAnnotationView *pinView = nil;
    if(annotation != mapView.userLocation)
    {
        static NSString *defaultPinID = @"com.invasivecode.pin";
        pinView = (MKAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:defaultPinID];
        if ( pinView == nil )
            pinView = [[MKAnnotationView alloc]
                       initWithAnnotation:annotation reuseIdentifier:defaultPinID];
        
        pinView.canShowCallout = YES;
        //        if (isFlagForNearestAnnotation==NO) {
        pinView.image = [UIImage imageNamed:@"location_normal"];
        
        isNearestLocation=NO;
        isFlagForNearestAnnotation=NO;
        //    }
        //   else{
        if ([[annotation title] isEqualToString:nearestLocationAddress]) {
            
            pinView.image=[UIImage imageNamed:@"location_near"];
            
            
            
        }
    }
    //as suggested by Squatch
    //  }
    // else {
    [mapView.userLocation setTitle:@"I am here"];
    // }
    
    if ([activityIndicator isDescendantOfView:self.findBranchView.superview])
    {
        [activityIndicator removeView];
    }
    
    return pinView;
}


-(void)createAddressString
{
    
    
    for (CLLocation *location in mapCLlocationsArrayForAnnotation) {
        
        MKPointAnnotation*    annotation = [[MKPointAnnotation alloc] init];
        CLLocationCoordinate2D myCoordinate;
        myCoordinate.latitude=location.coordinate.latitude;
        myCoordinate.longitude=location.coordinate.longitude;
        
        
        MKCoordinateRegion region=MKCoordinateRegionMakeWithDistance(myCoordinate, 250,250);
        [self.mapView setRegion:region];
        
        NSString *tempAddresAnnotation=[NSString stringWithFormat:@"%@,%@",[[mapAddressArray objectAtIndex:indexLoop]objectForKey:@"address1" ],[[mapAddressArray objectAtIndex:indexLoop] objectForKey:@"branchName"]];
        
        if ([tempAddresAnnotation isEqualToString:nearestLocationAddress]) {
        }else{
            annotation.coordinate=myCoordinate;
            NSString *titleOfAnnotation=[[addressArrayForOtherLocation objectAtIndex:indexLoop]objectForKey:@"address" ];
            NSString *subTitle=[[addressArrayForOtherLocation objectAtIndex:indexLoop]objectForKey:@"branchName" ];
            [annotation setTitle: titleOfAnnotation];
            [annotation setSubtitle:subTitle];
            
            [self.mapView addAnnotation: annotation];
        }
        indexLoop=indexLoop+1;
        
        
    }
}

#pragma mark - Mobile Number Validation Method
-(BOOL)validatePhoneNumber
{
    NSString *string = self.txtMobileNumber.text;
    NSString *expression = @"^([+][9][1]|[9][1]|[0]){0,1}([7-9]{1})([0-9]{9})$";
    NSError *error = NULL;
    
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
    
    NSTextCheckingResult *match = [regex firstMatchInString:string options:0 range:NSMakeRange(0, [string length])];
    
    if (match){
        NSLog(@"yes");
        return YES;
    }else{
        NSLog(@"no");
        return NO;
    }
    
}
#pragma mark -UitextField Delegate method

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    
    return YES;
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    //
    _buttonpopViewOfSMS.userInteractionEnabled=YES;
    self.viewForSMSPopUp.hidden=NO;
    [self.view endEditing:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    
    return YES;
}


- (void)keyboardDidShow:(NSNotification *)notification
{
    // Assign new frame to your view
    [self.view setFrame:CGRectMake(self.view.frame.origin.x,-50,self.view.frame.size.width,self.view.frame.size.height)]; //here taken -20 for example i.e. your view will be scrolled to -20. change its value according to your requirement.
    
}

-(void)keyboardDidHide:(NSNotification *)notification
{
    // [self.view setFrame:CGRectMake(0,0,320,460)];
    [self.view setFrame:CGRectMake(self.view.frame.origin.x,0,self.view.frame.size.width,self.view.frame.size.height)];
}

#pragma mark - get cities -
-(void)getCitiesList
{
    LoadingView  *activityLoadingView = [LoadingView loadingViewInView:self.view.superview.superview
                                                            withString:@""
                                                     andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
    @try
    {
        NSString *pathParameter = @"";
        
        NetworkHelper *helper = [NetworkHelper sharedHelper];
        
        NSDictionary *headerDict = [CoreUtility getHeaderDict];
        
        [helper getResponseWithRequestType:@"GET" withQueryParam:@"" withPathParam:pathParameter withJsonParam:nil withHeaders:headerDict withUrl:@"tcForexRS/generic/gstCity" success:^(NSDictionary *responseDict)
         {
             NSLog(@"Response Dict : %@",responseDict);
             
             dispatch_async(dispatch_get_main_queue(), ^(void) {
                 [activityLoadingView removeFromSuperview];
                 if (responseDict)
                 {
                     if (responseDict.count>0)
                     {
                         arrForCity = (NSArray *)responseDict;
                         [tableViewForSelectCity reloadData];
                     }
                 }
             });
             
         }
                                   failure:^(NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^(void)
                            {
                                [activityLoadingView removeFromSuperview];
                                NSLog(@"Response Dict : %@",[error description]);
                            });
             
             
             
         }];
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"%@", exception.reason);
        [activityLoadingView removeFromSuperview];
    }
    @finally
    {
        NSLog(@"exception finally called");
    }
    
    
}

-(void)getBranchDetailsUsingCityCode:(NSString *)cityCode
{
    LoadingView  *activityLoadingView = [LoadingView loadingViewInView:self.view.superview.superview
                                                            withString:@""
                                                     andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
    @try
    {
        NSString *pathParameter = @"";
        
        NetworkHelper *helper = [NetworkHelper sharedHelper];
        
        NSDictionary *headerDict = [CoreUtility getHeaderDict];
        
        [helper getResponseWithRequestType:@"GET" withQueryParam:@"" withPathParam:pathParameter withJsonParam:nil withHeaders:headerDict withUrl:[NSString stringWithFormat:@"/tcForexRS/generic/branchdetails/%@",cityCode] success:^(NSDictionary *responseDict)
         {
             NSLog(@"Response Dict : %@",responseDict);
             
             dispatch_async(dispatch_get_main_queue(), ^(void) {
                 [activityLoadingView removeFromSuperview];
                 if (responseDict)
                 {
                     if (responseDict.count>0)
                     {
                         
                         filterArray = (NSMutableArray *)responseDict;
                         
                         
                         
                         // [tableViewForSelectBranch reloadData];
                         
                     }
                 }
             });
             
         }
                                   failure:^(NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^(void)
                            {
                                [activityLoadingView removeFromSuperview];
                                NSLog(@"Response Dict : %@",[error description]);
                            });
             
             
             
         }];
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"%@", exception.reason);
        [activityLoadingView removeFromSuperview];
    }
    @finally
    {
        NSLog(@"exception finally called");
    }
    
    
}



@end
