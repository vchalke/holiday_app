//
//  MonthTravelTableViewCell.h
//  holidays
//
//  Created by Kush_Team on 24/06/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MonthTravelTableViewCell : UITableViewCell<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
{
    NSArray *monthArrayOne;
    NSArray *monthArrayTwo;
    NSMutableArray *mutArrayOne;
    NSMutableArray *mutArrayTwo;
    NSDate *selectDate;
}
@property (weak, nonatomic) IBOutlet UICollectionView *collection_viewOne;
@property (weak, nonatomic) IBOutlet UICollectionView *collection_viewTwo;

@property (weak, nonatomic) IBOutlet UILabel *lbl_first;
@property (weak, nonatomic) IBOutlet UILabel *lbl_second;
-(void)setupTableViewCell;

@property (strong,nonatomic) NSMutableDictionary *filterTravelMonthDict;
@end

NS_ASSUME_NONNULL_END
