//
//  PackageListVC.m
//  holidays
//
//  Created by Pushpendra Singh on 12/08/15.
//  Copyright (c) 2015 Mobicule. All rights reserved.
//

#import "PackageListVC.h"
#import "PackageListCell.h"
#import "TabMenuVC.h"
#import "PSPageMenu.h"
#import "CoreUtility.h"
//#import <NetCorePush/NetCorePush.h>
#import "NetCoreAnalyticsVC.h"
#import "Thomas_Cook_Holidays-Swift.h"

@interface PackageListVC ()<PSDelegate>
{
    LoadingView *activityLoadingView;
    NSMutableArray *arrayForResetData;
    NSMutableDictionary *payloadList;
    NSMutableDictionary *payloadListSortBy;
    NSMutableDictionary *payLoadForViewDetails;
    HolidayPackageDetail *packageDetail ;
}
@end

@implementation PackageListVC
{
    
    NSArray *siteArray;
    int flag;
    PackageListCell *cell;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    
 // [[NSBundle mainBundle]loadNibNamed:@"PackageListVC" owner:self options:nil];
     [AppUtility checkIsVersionUpgrade];
    
    _packageList.dataSource = self;
    _packageList.delegate   = self;
    flag=1;
    arrayForResetData=[[NSMutableArray alloc]init];
    
     arrayForResetData=[_arrayOfHolidays mutableCopy];
    CoreUtility *coreObj=[CoreUtility sharedclassname];
    coreObj.arrForResetPackageData=[[NSMutableArray alloc]init];
    coreObj.arrForResetPackageData=[_arrayOfHolidays mutableCopy];
    
    siteArray=@[@"London",@"Paris",@"France",@"New York",@"Hong Kong",@"Nepal"];
    
    payloadList =  [NSMutableDictionary dictionary];
    payloadListSortBy =  [NSMutableDictionary dictionary];
    payLoadForViewDetails = [[NSMutableDictionary alloc]init];
    
    [CoreUtility reloadRequestID];
}

-(void)viewWillAppear:(BOOL)animated
{
    
    CoreUtility *coreObj=[CoreUtility sharedclassname];
    coreObj.pageNumber=1;
    
//    CoreUtility *coreObj=[[CoreUtility alloc]init];
//    coreObj.pageNumber=1;
   // CoreUtility *coreObj=[CoreUtility sharedclassname];
//    PackageListVC *filterObj=coreObj.obj;
//    filterObj.title=@"SORT BY";


    [self animateTableView:self.packageList];
    
    PackageListVC *objFortitle=[[PackageListVC alloc]init];
    objFortitle.title=@"SORT BY";

    if (coreObj.statusForResetPackageDetails==1)
    {
        _arrayOfHolidays=[arrayForResetData mutableCopy];
        if (_arrayOfHolidays.count>0)
        {
   
        [_packageList reloadData];
        coreObj.statusForResetPackageDetails=0;
        
        NSString *count=[NSString stringWithFormat:@"%lu",(unsigned long)[_arrayOfHolidays count]];
        NSDictionary *dictOfFilteredData=[[NSDictionary alloc]initWithObjectsAndKeys:_arrayOfHolidays,@"data",count,@"totalPackages",nil];
        [[NSNotificationCenter defaultCenter]postNotificationName:kDataDidFilteredNotification
                                                           object:self
                                                         userInfo:dictOfFilteredData];

        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated{
    
    _viewPopup.frame=CGRectMake(22, 0, 180, 235);
    _viewPopup.hidden=YES;
    _viewPopup.alpha = 0.0f;
    
}


#pragma mark- Tableview datasource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [_arrayOfHolidays count];
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellIdentifier= @"Cell";
    
       cell=(PackageListCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell==nil) {
        
        NSArray *nib=[[NSBundle mainBundle] loadNibNamed:@"PackageListCell" owner:self options:nil];
        
        cell=[nib objectAtIndex:0];
        
        Holiday *objHoliday = [_arrayOfHolidays objectAtIndex:indexPath.row];
        [cell getSiteArrayDetails:_arrayOfHolidays];
        
        siteArray = objHoliday.timeLineList;
        NSSortDescriptor *brandDescriptor = [[NSSortDescriptor alloc] initWithKey:@"position" ascending:YES];
        NSArray *sortDescriptors = [NSArray arrayWithObject:brandDescriptor];
        NSArray *sortedArray = [siteArray sortedArrayUsingDescriptors:sortDescriptors];

        for (int i=0; i<[sortedArray count]; i++) {
            
            
            UIView *siteView=[[UIView alloc]initWithFrame:CGRectMake(i*100, 0, 100, 49)];
            UILabel *lblSiteName=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 100, 20)];
            UIImageView *imgSiteDot=[[UIImageView alloc]initWithFrame:CGRectMake(45, 20, 10,10)];
            UILabel *lblTime=[[UILabel alloc]initWithFrame:CGRectMake(0, 30, 100, 20)];
            
            lblSiteName.font=[UIFont fontWithName:TITILLUM_REGULAR size:13.0];
            lblTime.font=[UIFont fontWithName:TITILLUM_THIN size:12.0];
            
            [lblSiteName setTextAlignment:NSTextAlignmentCenter];
            [lblTime setTextAlignment:NSTextAlignmentCenter];
            
            NSDictionary *siteNameDict = sortedArray[i];
            
            NSDictionary *cityDict = [siteNameDict valueForKey:@"cityCode"];
            
            NSString *cityName = [cityDict valueForKey:@"cityName"];
            
            lblSiteName.text = cityName;
            lblTime.text=[NSString stringWithFormat:@"%@ Nights",[siteNameDict valueForKey:@"noOfNights"]];
            
            
            NSDictionary *iconDict = [siteNameDict valueForKey:@"iconId"];

            
            NSString *iconName = [iconDict valueForKey:@"iconName"];
            
            
            if ([iconName isEqualToString:@"Flight"])
            {
                imgSiteDot=[[UIImageView alloc]initWithFrame:CGRectMake(45, 20, 20 ,20)];
                imgSiteDot.image=[UIImage imageNamed:@"flight_package"];
                lblTime.text = @"";
            }
            else
            {
                imgSiteDot.image=[UIImage imageNamed:@"siteDot"];
            }
            
           // imgSiteDot.image=[UIImage imageNamed:@"siteDot.png"];
            
            [siteView addSubview:lblSiteName];
            [siteView addSubview:imgSiteDot];
            [siteView addSubview:lblTime];
            
            [cell.scrollSite addSubview:siteView];
            
            [cell.scrollSite setContentSize:CGSizeMake([sortedArray count]*100, 49)];
            
            [cell.btnViewDetails addTarget:self action:@selector(clickViewDetail:) forControlEvents:UIControlEventTouchUpInside];
            [cell.btnSideArrowLeft addTarget:cell action:@selector(actionForLeftbuttonClicked:) forControlEvents:UIControlEventTouchUpInside];
            cell.btnSideArrowRight.tag=indexPath.row;
            [cell.btnSideArrowRight addTarget:cell action:@selector(actionForRightbuttonClicked:) forControlEvents:UIControlEventTouchUpInside];

            
        }
        
        
        
//        NSArray *priceArray = objHoliday.packagePriceArray;
//        
//        if(priceArray)
//        {
//            if (priceArray.count>0)
//            {
//                NSDictionary *packagePriceDict = [priceArray firstObject];
//                //cell.lblTripCost.text  =  [packagePriceDict valueForKey:@"startingPriceAmount"];
//            }
//        }
        
        NSString *strCostPP = [NSString stringWithFormat:@"Rs. %d",objHoliday.packagePrise];
        
        cell.lblPackageName.text = objHoliday.strPackageName;
        cell.lblTripCost.text    = strCostPP;

        
        if (objHoliday.mealsFlag == YES)
        {
            cell.constraintWidthFood.constant = 35;
            cell.lblInclusion.alpha = 1;
        }
        if (objHoliday.airFlag == YES)
        {
            cell.constraintWidthFlight.constant = 35;
            cell.lblInclusion.alpha = 1;
        }
        if (objHoliday.tourMngerFlag == YES)
        {
            cell.constraintWidthVisa.constant = 35;
            cell.lblInclusion.alpha = 1;
        }
        if (objHoliday.accomFlag == YES)
        {
            cell.constraintWidthhotel.constant = 35;
            cell.lblInclusion.alpha = 1;
        }
        if (objHoliday.sightSeeingFlag == YES)
        {
            cell.constraintWidthSight.constant = 35;
            cell.lblInclusion.alpha = 1;
        }
        
        cell.lblNumberOfDays.text = [NSString stringWithFormat:@"%d",objHoliday.durationNoDays];
        cell.lblNumberOfNights.text = [NSString stringWithFormat:@"%d",objHoliday.durationNoDays-1];
        
        if([[objHoliday.strPackageSubType lowercaseString] isEqualToString:@"git"])
        {
            cell.groupTourLabel.text = @"Group Tour";
            cell.ImagegroupTour.image = [UIImage imageNamed:@"group_icon"];
        }
        else
        {
           cell.groupTourLabel.text = @"Personalised Tour";
            cell.ImagegroupTour.image = [UIImage imageNamed:@"personal_tourSymbol"];
            
            
        }
        
        [cell layoutIfNeeded];
        
        [self animateTableViewCell:cell];
       
    }
    
   
    return cell;
}


//-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    // 1
//    CGRect originalCellFrame = [self.packageList rectForRowAtIndexPath:indexPath];
//    
//    // 2
//    cell.frame = CGRectMake(0 - originalCellFrame.size.width,
//                            originalCellFrame.origin.y,
//                            originalCellFrame.size.width,
//                            originalCellFrame.size.height);
//    // 3
//    [UIView animateWithDuration:0.75
//                          delay:0.25
//         usingSpringWithDamping:0.8
//          initialSpringVelocity:2.0
//                        options: UIViewAnimationOptionCurveLinear
//     // 4
//                     animations:^{
//                         cell.frame = originalCellFrame;
//                     }
//                     completion:^(BOOL finished){
//                     }];
//    
//}



/*
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
//    // setup initial state (e.g. before animation)
//    cell.layer.shadowColor = [[UIColor blackColor] CGColor];
//    cell.layer.shadowOffset = CGSizeMake(10, 10);
//    cell.alpha = 0;
//    cell.layer.transform = CATransform3DMakeScale(0.5, 0.5, 0.5);
//    cell.layer.anchorPoint = CGPointMake(0, 0.5);
//    
//    // define final state (e.g. after animation) & commit animation
//    [UIView beginAnimations:@"scaleTableViewCellAnimationID" context:NULL];
//    [UIView setAnimationDuration:0.7];
//    cell.layer.shadowOffset = CGSizeMake(0, 0);
//    cell.alpha = 1;
//    cell.layer.transform = CATransform3DIdentity;
//    [UIView commitAnimations];
    
    
    
    CATransform3D rotation;
    rotation = CATransform3DMakeRotation( (90.0*M_PI)/180, 0.0, 0.7, 0.4);
    rotation.m34 = 1.0/ -400;
    
    
    //2. Define the initial state (Before the animation)
    cell.layer.shadowColor = [[UIColor blackColor]CGColor];
    cell.layer.shadowOffset = CGSizeMake(10, 10);
    cell.alpha = 0;
    
    cell.layer.transform = rotation;
    cell.layer.anchorPoint = CGPointMake(0, 0.5);
    
    
    //3. Define the final state (After the animation) and commit the animation
    [UIView beginAnimations:@"rotation" context:NULL];
    [UIView setAnimationDuration:0.8];
    cell.layer.transform = CATransform3DIdentity;
    cell.alpha = 1;
    cell.layer.shadowOffset = CGSizeMake(0, 0);
    [UIView commitAnimations];
    
}
*/

#pragma mark- View Detail Action

-(void)clickViewDetail:(UIButton*)sender
{
  
    
    CGPoint center= sender.center;
    CGPoint rootViewPoint = [sender.superview convertPoint:center toView:self.packageList];
    NSIndexPath *indexPath = [self.packageList indexPathForRowAtPoint:rootViewPoint];
    NSLog(@"%ld",(long)indexPath.row);
    Holiday *objHoliday = [_arrayOfHolidays objectAtIndex:indexPath.row];

    activityLoadingView = [LoadingView loadingViewInView:self.view.superview.superview
                                              withString:@""
                                       andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
    
    
 //   [self performSelectorInBackground:@selector(fetchPackageDetails:) withObject:objHoliday];
    
    [self fetchPackageDetails:objHoliday];
    
 /*   NSDictionary *dictForCompletePackage = [_completePackageDetail objectAtIndex:indexPath.row];
   
    HolidayPackageDetail *packageDetailHoliday = [[HolidayPackageDetail alloc] initWithDataDict:dictForCompletePackage];
    
  //  [self fireAppIntHoViewDetailsEvent]; //
    
    TabMenuVC *tabMenuVC = [[TabMenuVC alloc]initWithNibName:@"BaseViewController" bundle:nil];
    
    tabMenuVC.headerName = objHoliday.strPackageName;
    
    tabMenuVC.flag = @"Overview";
    tabMenuVC.packageDetail = packageDetailHoliday;
    tabMenuVC.completePackageDetail = [NSArray arrayWithObjects:dictForCompletePackage, nil];
    tabMenuVC.payloadDict = payloadList;
    [[SlideNavigationController sharedInstance] pushViewController:tabMenuVC animated:YES];*/

    
    
    
    if ([objHoliday.strPackageType caseInsensitiveCompare:@"International"]==NSOrderedSame)
    {
        [FIRAnalytics logEventWithName:@"International_Holidays_View_Details" parameters:@{kFIRParameterItemName:[NSString stringWithFormat:@"PackageName :%@, PackageType :%@",objHoliday.strPackageName,objHoliday.strPackageType]}];
    }
    else
    {
       [FIRAnalytics logEventWithName:@"Indian_Holidays_View_Details" parameters:@{kFIRParameterItemName:[NSString stringWithFormat:@"PackageName :%@, PackageType :%@",objHoliday.strPackageName,objHoliday.strPackageType]}];
    }
    
    [payloadList setObject:@"yes" forKey:@"s^VIEW_DETAILS"];
    
}

-(void)fetchPackageDetails:(Holiday *)objHoliday
{
  /*  NSMutableDictionary *dictOfData = [[NSMutableDictionary alloc]init];
    [dictOfData setValue:objHoliday.strPackageId forKey:@"packageId"];
    
    if([CoreUtility connected])
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            NetworkHelper *helper = [NetworkHelper sharedHelper];
            NSString *strResponse = [helper getDataFromServerForType:@"webservice" entity:@"package" action:@"details" andUserJson:dictOfData];
            NSString *strStatus = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"status"];
            
             dispatch_async(dispatch_get_main_queue(), ^{
            if ([strStatus isEqualToString:kStatusSuccess])
            {
                NSArray *dataArray = [[JsonSerealizer arrayOfJSonFromJsonData:strResponse] valueForKey:@"data"];
                NSDictionary *dataDict;
                if (dataArray.count != 0)
                {
                    dataDict = dataArray[0];
                    NSArray *itineraryListArray = [dataDict objectForKey:@"itineraryList"];
                    
                    if ([itineraryListArray count]>0)
                    {
                        NSString *destination;
                        for (int i=0; i <[itineraryListArray count] ; i++)
                        {
                            NSDictionary *itineraryDict = [itineraryListArray objectAtIndex:i];
                            destination = [itineraryDict objectForKey:@"destination"];
                            
                            [payloadList setObject:destination forKey:@"s^DESTINATION"];
                        }
                    }
                    
                    packageDetail = [[HolidayPackageDetail alloc]initWithDataDict:dataDict];
                    
                    if ([objHoliday.strPackageType caseInsensitiveCompare:@"International"]==NSOrderedSame)
                    {
                        [FIRAnalytics logEventWithName:@"International_Holidays_View_Details" parameters:@{kFIRParameterItemName:[NSString stringWithFormat:@"PackageName :%@, PackageType :%@",objHoliday.strPackageName,objHoliday.strPackageType]}];
                       
                    }
                    else
                    {
                    [FIRAnalytics logEventWithName:@"Indian_Holidays_View_Details" parameters:@{kFIRParameterItemName:[NSString stringWithFormat:@"PackageName :%@, PackageType :%@",objHoliday.strPackageName,objHoliday.strPackageType]}];
                    }
                    
                        if ([activityLoadingView isDescendantOfView:self.view.superview.superview])
                        {
                            [activityLoadingView removeView];
                            activityLoadingView = nil;
                        }
                    
                       [self fireAppIntHoViewDetailsEvent];
                    
                        TabMenuVC *tabMenuVC = [[TabMenuVC alloc]initWithNibName:@"BaseViewController" bundle:nil];
                        tabMenuVC.headerName = objHoliday.strPackageName;
                        tabMenuVC.flag=@"Overview";
                        tabMenuVC.packageDetail = packageDetail;
                        tabMenuVC.completePackageDetail = dataArray;
                        tabMenuVC.payloadDict = payloadList;
                        [[SlideNavigationController sharedInstance] pushViewController:tabMenuVC animated:YES];
                   

                 }
            }else
            {
                NSString *messsage = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"message"];
                if (messsage)
                {
                    if ([messsage isEqualToString:kVersionUpgradeMessage])
                    {
                        NSDictionary *downloadDict = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"data"];
                        NSString *downloadURL = [downloadDict valueForKey:@"download_url"];
                        [self showAlertViewForVersionUpdateWithUrl:downloadURL];

                    }
                    else
                    {
                        [self showAlertViewWithTitle:@"Alert" withMessage:messsage];
                    }
                }else
                {
                    [self showAlertViewWithTitle:@"Alert" withMessage:kMessageSomeErrorTryAfterSometime];
                }
                
            }
            if ([activityLoadingView isDescendantOfView:self.view.superview.superview])
            {
                [activityLoadingView removeView];
                activityLoadingView = nil;
            }
           });
        });
    }else
    {
        [self showAlertViewWithTitle:@"Alert" withMessage:kMessageNoInternet];
    }*/
    
    @try
    {
        
    NetworkHelper *helper = [NetworkHelper sharedHelper];
    
    NSString *pathParam = objHoliday.strPackageId;
    
    NSDictionary *headerDict = [CoreUtility getHeaderDict];
    
    [helper getResponseWithRequestType:@"" withQueryParam:@"" withPathParam:pathParam withJsonParam:nil withHeaders:headerDict withUrl:kAstraUrlPackageDetail success:^(NSDictionary *responseDict)
     {
         NSLog(@"Response Dict : %@",responseDict);
         
         
         dispatch_async(dispatch_get_main_queue(), ^(void) {
        [activityLoadingView removeFromSuperview];
         if (responseDict)
         {
             if (responseDict.count>0)
             {
                 NSArray *packageArray = (NSArray *)responseDict;
                 NSDictionary *dictForCompletePackage = [packageArray objectAtIndex:0];
                 
                 HolidayPackageDetail *packageDetailHoliday = [[HolidayPackageDetail alloc] initWithDataDict:dictForCompletePackage];
                 
                 
                 
                 TabMenuVC *tabMenuVC = [[TabMenuVC alloc]initWithNibName:@"BaseViewController" bundle:nil];
                 
                 tabMenuVC.headerName = objHoliday.strPackageName;
                 
                 tabMenuVC.flag = @"Overview";
                 tabMenuVC.packageDetail = packageDetailHoliday;
                 [self fireAppIntHoViewDetailsEvent:packageDetailHoliday];
                 tabMenuVC.completePackageDetail = [NSArray arrayWithObjects:dictForCompletePackage, nil];
                 tabMenuVC.payloadDict = payloadList;
                 [[SlideNavigationController sharedInstance] pushViewController:tabMenuVC animated:YES];

                 
                // [self doMyOperationsWithResponse:packageArray];
                 
             }
         }
          });
         
     }
                               failure:^(NSError *error)
     {
         dispatch_async(dispatch_get_main_queue(), ^(void)
                        {
                            [activityLoadingView removeFromSuperview];
                            NSLog(@"Response Dict : %@",[error description]);
                        });

         
         
     }];
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"%@", exception.reason);
        [activityLoadingView removeFromSuperview];
    }
    @finally
    {
        NSLog(@"exception finally called");
    }
}

-(void)showAlertViewWithTitle:(NSString *)title withMessage:(NSString *)message
{
    UIAlertController *alertView = [UIAlertController alertControllerWithTitle:title
                                                                       message:message
                                                                preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [alertView dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertView addAction:okAction];
    
    [[[[SlideNavigationController sharedInstance] viewControllers ] lastObject] presentViewController:alertView animated:YES completion:nil];
}

-(void)showAlertViewForVersionUpdateWithUrl:(NSString *)urlString
{
    UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Alert"
                                                                       message:@"forcefully upgradation required"
                                                                preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        
        [alertView dismissViewControllerAnimated:YES completion:nil];
        NSURL *url = [NSURL URLWithString:urlString];
        if (url)
        {
            [[UIApplication sharedApplication] openURL:url];
        }
        
    }];
    [alertView addAction:okAction];
    
    [[[[SlideNavigationController sharedInstance] viewControllers ] lastObject] presentViewController:alertView animated:YES completion:nil];
}


#pragma mark- PSDelegate method

-(void)identifyMenuWithindex:(NSInteger)index{
    
    NSLog(@"Menu clicked index : %ld",(long)index);
    
    if (index==0)
    {
        flag++;
    }
    else
    {
        flag=0;
        _viewPopup.hidden=YES;
    }
    
    if (flag>=2)
    {
         NSLog(@"Menu clicked flag : %d",flag);
        
        if (_viewPopup.hidden)
        {
            _viewPopup.hidden=NO;
            
            [self showPopUp];
        }
        else
        {
            [self dismissPopUp];
        }
    }
    
}

#pragma mark - Slide Navigation Delegate

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
}

- (BOOL)slideNavigationControllerShouldDisplayRightMenu
{
    return NO;
}

#pragma mark - Button action methods

- (IBAction)btnOnSortingPopUpClicked:(id)sender
{
    payloadListSortBy =  [NSMutableDictionary dictionary];

    UIButton *btnClicked = (UIButton *)sender;
    
    _arrayOfHolidays = arrayForResetData;
    
      NSArray *arrayOfTempHolidays = [NSArray arrayWithArray:_arrayOfHolidays];
    
    
    if (btnClicked.tag == 1)
    {
        _arrayOfHolidays = [NSArray new];
        NSSortDescriptor *desc = [[NSSortDescriptor alloc]initWithKey:@"packagePrise" ascending:YES];
        
        _arrayOfHolidays = [arrayOfTempHolidays sortedArrayUsingDescriptors:@[desc]];
        
         [payloadListSortBy setObject:@"Low To High" forKey:@"s^SORT_OPTION"];
    }
    else if (btnClicked.tag == 2)
    {
        _arrayOfHolidays = [NSArray new];
        NSSortDescriptor *desc = [[NSSortDescriptor alloc]initWithKey:@"packagePrise" ascending:NO];
        
        _arrayOfHolidays = [arrayOfTempHolidays sortedArrayUsingDescriptors:@[desc]];
         [payloadListSortBy setObject:@"High To Low" forKey:@"s^SORT_OPTION"];
    }
    else if (btnClicked.tag == 3)
    {
        _arrayOfHolidays = [NSArray new];
        NSSortDescriptor *desc = [[NSSortDescriptor alloc]initWithKey:@"durationNoDays" ascending:YES];
        
        _arrayOfHolidays = [arrayOfTempHolidays sortedArrayUsingDescriptors:@[desc]];
         [payloadListSortBy setObject:@"Short To Long" forKey:@"s^SORT_OPTION"];
    }
    else if (btnClicked.tag == 4)
    {
        _arrayOfHolidays = [NSArray new];
        NSSortDescriptor *desc = [[NSSortDescriptor alloc]initWithKey:@"durationNoDays" ascending:NO];
        
        _arrayOfHolidays = [arrayOfTempHolidays sortedArrayUsingDescriptors:@[desc]];
         [payloadListSortBy setObject:@"Long To Short" forKey:@"s^SORT_OPTION"];
    }
    else if (btnClicked.tag == 5)
    {
        
        NSPredicate *pred = [NSPredicate predicateWithBlock:^BOOL(Holiday* evaluatedObject, NSDictionary *bindings) {
            
            if([[evaluatedObject.stringPopularFlag lowercaseString] isEqualToString:@"y"])
            {
                return true;
            }
                             
            return false;
        }];
        
        NSArray *results = [arrayOfTempHolidays filteredArrayUsingPredicate:pred];
        if(results.count != 0)
        {
          _arrayOfHolidays = results;
        }
        else
        {
            UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Alert" message:@"No Package available"
                                                                        preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [alertView dismissViewControllerAnimated:YES completion:nil];
                
            }];
            
            [alertView addAction:okAction];
            
            [self presentViewController:alertView animated:YES completion:nil];

        }
            
        
         [payloadListSortBy setObject:@"Popular" forKey:@"s^SORT_OPTION"];

    }
    else if (btnClicked.tag == 6)
    {
        NSPredicate *pred = [NSPredicate predicateWithBlock:^BOOL(Holiday* evaluatedObject, NSDictionary *bindings) {
            
            if(![evaluatedObject.Offers isEqualToString:@""])
            {
                return true;
            }
            
            return false;
        }];
        
        NSArray *results = [arrayOfTempHolidays filteredArrayUsingPredicate:pred];
        
        if(results.count != 0)
        {
           _arrayOfHolidays = results;
        }
        else
        {
            UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Alert" message:@"No Package available"
                                                                        preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [alertView dismissViewControllerAnimated:YES completion:nil];
                
            }];
            
            [alertView addAction:okAction];
            
            [self presentViewController:alertView animated:YES completion:nil];
            
        }


         [payloadListSortBy setObject:@"Package With Deals" forKey:@"s^SORT_OPTION"];
    }
    else if (btnClicked.tag == 7)
    {
        NSPredicate *pred = [NSPredicate predicateWithBlock:^BOOL(Holiday* evaluatedObject, NSDictionary *bindings) {
            
            if(evaluatedObject.pkgStatusId  == 1)
            {
                return true;
            }
            
            return false;
        }];
        
        NSArray *results = [arrayOfTempHolidays filteredArrayUsingPredicate:pred];
        
        if(results.count != 0)
        {
          _arrayOfHolidays = results;
        }
        else
        {
            UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Alert" message:@"No Package available"
                                                                        preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [alertView dismissViewControllerAnimated:YES completion:nil];
                
            }];
            
            [alertView addAction:okAction];
            
            [self presentViewController:alertView animated:YES completion:nil];
            
        }


         [payloadListSortBy setObject:@"Bookable Online" forKey:@"s^SORT_OPTION"];
    }
    
    
    [self netCoreAppPackageSortBy];
    [self animateTableView:self.packageList];
    
    [self dismissPopUp];

}

//-(void)actionForRightbuttonClicked:(NSInteger)index{
//    
//    if (  cell.scrollSite.contentOffset.x < (40 * [siteArray count]-1) -250 ) {
//        
//        [UIView animateWithDuration:0.8 animations:^{
//            cell.scrollSite.contentOffset = CGPointMake(cell.scrollSite.contentOffset.x+50, 0);
//        }];
//        
//    }

    
//}


#pragma mark - Helper method

#pragma mark Pop Up Dismissal

-(void)showPopUp
{
    [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        
        [_viewPopup setAlpha:1.0f];
        //_viewPopup.frame=CGRectMake(22, 0, 180, 165);
        
    } completion:^(BOOL finished){
        
    }];
}
-(void)dismissPopUp
{
    [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        
        [_viewPopup setAlpha:0.0f];
        
    } completion:^(BOOL finished){
        _viewPopup.hidden=YES;
    }];
}

#pragma mark TableView Animation

-(void)animateTableView:(UITableView *)tableView
{
    if (_arrayOfHolidays.count > 0)
    {
        CATransition *transition = [CATransition animation];
        transition.type = kCATransitionMoveIn;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
        transition.fillMode = kCAFillModeForwards;
        transition.duration = 1;
        transition.subtype = kCATransitionFade;
        
        [[tableView layer] addAnimation:transition forKey:@"UITableViewReloadDataAnimationKey"];
        
        [tableView reloadData];
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        [self.packageList scrollToRowAtIndexPath:indexPath
                                atScrollPosition:UITableViewScrollPositionTop
                                        animated:YES];

    }
}

-(void)animateTableViewCell:(UITableViewCell *)cell
{
    if (_arrayOfHolidays.count > 0)
    {

    CATransition *transition = [CATransition animation];
    transition.type = kCATransitionMoveIn;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    transition.fillMode = kCAFillModeForwards;
    transition.duration = 1;
    transition.subtype = kCATransitionFade;
    
     [[cell layer] addAnimation:transition forKey:@"cellAnimationKey"];
    }
    
}

-(void)netCoreAppPackageSortBy
{
    [payloadListSortBy setObject:@"" forKey:@"s^DESTINATION"];
    
    if (![payloadListSortBy objectForKey:@"s^SORT_OPTION"])
    {
        [payloadListSortBy setObject:@"no" forKey:@"s^SORT_OPTION"];
    }
    
    [payloadListSortBy setObject:@"" forKey:@"s^BUDGET"];
    [payloadListSortBy setObject:@"App" forKey:@"s^SOURCE"];
    
    [NetCoreAnalyticsVC getNetCorePushNotificationDetailDictionary:payloadListSortBy withPayloadCount:113];
}


 
 //APP_INT_HO_VIEW_DETAILS
 
-(void)fireAppIntHoViewDetailsEvent:(HolidayPackageDetail *) PackageDetail
 {
     
     if((PackageDetail.strPackageName == nil) || (PackageDetail.strPackageName == NULL)){
         [payLoadForViewDetails setObject:@"" forKey:@"s^DESTINATION"];
     }else{
         
         [payLoadForViewDetails setObject:PackageDetail.strPackageName forKey:@"s^DESTINATION"];
     }
     [payLoadForViewDetails setObject:PackageDetail.strPackageName forKey:@"s^DESTINATION"];
     [payLoadForViewDetails setObject:[NSString stringWithFormat:@"%d",PackageDetail.durationNoDays] forKey:@"s^DURATION"];
     [payLoadForViewDetails setObject:[NSString stringWithFormat:@"%d",PackageDetail.packagePrise] forKey:@"s^BUDGET"];
     [payLoadForViewDetails setObject:PackageDetail.strPackageId forKey:@"s^PACKAGE_ID"];
     [payLoadForViewDetails setObject:@"App" forKey:@"s^SOURCE"];
   
   //[payLoadForViewDetails setObject:[NSString stringWithFormat:@"%d",packageDetail.packagePrise] forKey:@"i^PACKAGE_PRICE"];//28-02-2018
     
     [payLoadForViewDetails setObject:[NSNumber numberWithInt:PackageDetail.packagePrise] forKey:@"i^PACKAGE_PRICE"];//28-02-2018
     
     if ([[PackageDetail.strPackageSubType lowercaseString]isEqualToString:@"git"])
     {
         [payLoadForViewDetails setObject:@"no" forKey:@"s^PERSONALIZED_TOUR"];
     }
     else
     {
       [payLoadForViewDetails setObject:@"yes" forKey:@"s^PERSONALIZED_TOUR"];
     }
     
     if(PackageDetail.strPackageType != nil && ![PackageDetail.strPackageType isEqualToString:@""])
     {
         [payLoadForViewDetails setObject:PackageDetail.strPackageType forKey:@"s^TYPE"];
     }
     
     [NetCoreAnalyticsVC getNetCorePushNotificationDetailDictionary:payLoadForViewDetails withPayloadCount:125];
     NSLog(@"Data submitted to the netcore for 105");
     
     //[NetCoreAnalyticsVC getNetCorePushNotificationDetailDictionary:payLoadForViewDetails withPayloadCount:105];

 
 }
 





@end
