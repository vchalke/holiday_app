//
//  LoginViewController.m
//  holidays
//
//  Created by Sarita on 02/03/17.
//  Copyright © 2017 Mobicule. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController ()

@end

static LoginViewController *singletonInstance;

@implementation LoginViewController

+ (LoginViewController *)sharedInstance
{
    if (!singletonInstance)
        NSLog(@"SlideNavigationController has not been initialized. Either place one in your storyboard or initialize one in code");
    
    return singletonInstance;
}


-(void)setUp
{
    [GIDSignInButton class];
    
    GIDSignIn *signIn = [GIDSignIn sharedInstance];
    signIn.shouldFetchBasicProfile = YES;
    signIn.delegate = self;
    signIn.uiDelegate = self;
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        [self setUp];
        
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        [self setUp];
    }
    return self;
}


-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // [self adoptUserSettings];
    [self reportAuthStatus];
    [self updateButtons];
    // [self.tableView reloadData];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - GIDSignInDelegate

- (void)signIn:(GIDSignIn *)signIn
didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    if (error) {
        //_signInAuthStatus.text = [NSString stringWithFormat:@"Status: Authentication error: %@", error];
        return;
    }
    [self reportAuthStatus];
    [self updateButtons];
}

- (void)signIn:(GIDSignIn *)signIn
didDisconnectWithUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    if (error) {
        // _signInAuthStatus.text = [NSString stringWithFormat:@"Status: Failed to disconnect: %@", error];
    } else {
        // _signInAuthStatus.text = [NSString stringWithFormat:@"Status: Disconnected"];
    }
    [self reportAuthStatus];
    [self updateButtons];
}

- (void)presentSignInViewController:(UIViewController *)viewController {
    [[self navigationController] pushViewController:viewController animated:YES];
}


- (void)reportAuthStatus {
    GIDGoogleUser *googleUser = [[GIDSignIn sharedInstance] currentUser];
    if (googleUser.authentication)
    {
        
    } else
    {
        // To authenticate, use Google+ sign-in button.
        
    }
    
    [self refreshUserInfo];
}

// Update the interface elements containing user data to reflect the
// currently signed in user.
- (void)refreshUserInfo {
    if ([GIDSignIn sharedInstance].currentUser.authentication == nil) {
        
        // self.userEmailAddress.text = kPlaceholderEmailAddress;
        return;
    }
    self.userEmailAddress.text = [GIDSignIn sharedInstance].currentUser.profile.email;
    
    
}

- (void)updateButtons {
    BOOL authenticated = ([GIDSignIn sharedInstance].currentUser.authentication != nil);
    
    self.signInButton.enabled = !authenticated;
    
    
    if (authenticated) {
        self.signInButton.alpha = 0.5;
    } else {
        self.signInButton.alpha = 1.0;
        
    }
}


@end
