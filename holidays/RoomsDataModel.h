//
//  RoomsDataModel.h
//  holidays
//
//  Created by ketan on 15/10/15.
//  Copyright © 2015 Mobicule. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RoomsDataModel : NSObject
@property int adultCount;
@property (nonatomic ,strong) NSMutableArray *arrayChildrensData;
@property int infantCount;
@property int CNBCount ;
@property int CWBCount ;
@property int TRCount ;
@property int SRCount ;
@property int DRCount ;
@property int CNBAge;
@property int CWBAge;

@end
