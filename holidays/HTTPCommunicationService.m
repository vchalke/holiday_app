//
//  SyncCommunicationService.m
//  Mobicule-Sync-Core
//
//  Created by Kishan on 29/06/15.
//  Copyright (c) 2015 Mobicule. All rights reserved.
//

#import "HTTPCommunicationService.h"



@implementation HTTPCommunicationService


- (NSString *)sendRequest:(NSString*)syncRequest withUrl:(NSString*)url withAuthorization:(NSString*)authorization withShowLogs:(BOOL)isLogEnable  {
    
    if(isLogEnable) {
        if (DEBUG_ENABLED) {
            debugMethodStart(@" ");
        }
    }
    _isLogEnabled = isLogEnable;
    
    NSMutableURLRequest *request = [self getRequestObject:syncRequest withUrl:url withAuthorization:authorization];
    
    NSError *error = nil;
    NSURLResponse *response;
    
    NSData *result = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    NSString * resultString = [[NSString alloc] initWithData:result encoding:NSASCIIStringEncoding];
    
    NSInteger statusCode = [(NSHTTPURLResponse *)response statusCode];
    
    if(isLogEnable) {
        if (DEBUG_ENABLED) {
            debugMethodEnd(@"Response :%@",resultString);
        }
    }
    
    if(error == NULL  && statusCode == 200) {
        return resultString;
    }
    else if(statusCode == 404) {
        
        return  SERVER_NOT_ACCESS ;
    }
    else if(statusCode == 0) {
        
        return INTERNET_NOT_ACCESS ;
    }
    else if([[error localizedDescription] isEqualToString:@"The request timed out."])
    {
        return TIMED_OUT_ERROR;
    }
    else
    {
        return NETWORK_FAILED;
    }
    
}

////---------For Asynchronous Request--------------------

- (NSString *)sendRequest1:(NSString*)syncRequest withUrl:(NSString*)url withAuthorization:(NSString*)authorization withShowLogs:(BOOL)isLogEnable withCompletionHandler:(void (^)(NSString *str))handler  {
    
    if(isLogEnable)
    {
        if (DEBUG_ENABLED)
        {
            debugMethodStart(@" ");
        }
    }
    
    _isLogEnabled = isLogEnable;
    NSMutableData *dataOfAnynch=[[NSMutableData alloc]init];
    CoreUtility *coreobj=[CoreUtility sharedclassname];
    coreobj.dataForSearch=[[NSMutableData alloc]init];
    NSMutableURLRequest *request = [self getRequestObject:syncRequest withUrl:url withAuthorization:authorization];
    
    //NSError *error = nil;
    //NSURLResponse *response;
    
   // NSData *result = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    __block NSString * resultString = nil;
    
    //https://services.sotc.in/holidayRS/autosuggest?searchAutoSuggest=ind

    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse * response, NSData *data, NSError *error)
     {
         if (error)
         {
             NSLog(@"error:%@", error.localizedDescription);
         }
         
         if (response != nil)
         {
             //[coreobj.dataForSearch appendData:data];
             [dataOfAnynch appendData:data];
             resultString = [[NSString alloc] initWithData:dataOfAnynch encoding:NSASCIIStringEncoding];
             handler(resultString);
         }
        
    }];
   
    if(isLogEnable) {
        if (DEBUG_ENABLED) {
            debugMethodEnd(@"Response :%@",resultString);
        }
    }
    
        return resultString;
//
//    if(error == NULL  && statusCode == 200) {
//        return resultString;
//    }
//    else if(statusCode == 404) {
//        
//        return  SERVER_NOT_ACCESS ;
//    }
//    else if(statusCode == 0) {
//        
//        return INTERNET_NOT_ACCESS ;
//    }
//    else if([[error localizedDescription] isEqualToString:@"The request timed out."]) {
//        return TIMED_OUT_ERROR;
//    }
//    else {
//        return NETWORK_FAILED;
//    }
//    

    return nil;
}


-(NSMutableURLRequest *)getRequestObject:(NSString*)syncRequest withUrl:(NSString *)url withAuthorization:(NSString*)authorization {
    
    if(_isLogEnabled) {
        if (DEBUG_ENABLED) {
            debugMethodStart(@" ");
        }
    }
    
    NSURL *syncURL = [NSURL URLWithString:url];
    // get post request
    if (syncRequest != nil) {
        
        NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:syncURL cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:180.0];
        
        NSData* syncRequestData = [NSData dataWithBytes:[syncRequest UTF8String] length:[syncRequest length]];
        
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        if (authorization != nil) {
            [request setValue:authorization forHTTPHeaderField:@"Authorization"];
        }
        
        [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[syncRequestData length]] forHTTPHeaderField:@"Content-Length"];
        
        [request setHTTPBody: syncRequestData];
        
        if(_isLogEnabled) {
            if (DEBUG_ENABLED) {
                debugMethodEnd(@"request :%@",request);
            }
        }
        return request;

    }
    
    // get post request
    else {
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL: syncURL cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:60];
        
        [request setHTTPMethod: @"GET"];
        if (authorization != nil) {
            [request setValue:authorization forHTTPHeaderField:@"Authorization"];
        }
        
        if(_isLogEnabled) {
            if (DEBUG_ENABLED) {
                debugMethodEnd(@"request :%@",request);
            }
        }
        return request;
    }
    
}
@end
