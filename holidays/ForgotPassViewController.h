//
//  ForgotPassViewController.h
//  holidays
//
//  Created by Swapnil on 13/01/18.
//  Copyright © 2018 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LoadingView;

@interface ForgotPassViewController : UIViewController
{
    LoadingView *activityIndicator;
}

@property (weak, nonatomic) IBOutlet UITextField *textFieldEmail;
- (IBAction)submitButtonClicked:(UIButton *)sender;
- (IBAction)closeButtonClicked:(UIButton *)sender;

@end
