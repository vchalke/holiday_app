//
//  PassengerPopUpViewController.swift
//  sotc-consumer-application
//
//  Created by Saurav Kumar on 26/09/17.
//  Copyright © 2017 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit

@objc protocol PassengerPopUpProtocol {
    
    @objc optional func onOkButtonClick(vieWcontroller:UIViewController)
    
}
 
class PassengerPopUpViewController: UIViewController ,UITableViewDataSource,UITableViewDelegate{
    
    @IBOutlet var passengerTableView: UITableView!
    
    @IBOutlet weak var OkBtnheighConstraint: NSLayoutConstraint!
    var presentingView:UIViewController? = nil
    
    var tourDetails:Tour?
    var tourStatus : TourStatus?
    
    var selectedPassengerNumber:String?
    
    var passengerProtocol : PassengerPopUpProtocol?
    
    var passangerDetailsArray:Array<Passenger>?
    var deviationDetailsArray:Array<Deviation>?
    
    var sectionDetails : [(isOpen: Bool,isSelectAll: Bool, section: Int ,row : [(isSelected: Bool, index: Int ,passenger:Passenger?)])] = []
    
    var moduleName:String = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.passengerTableView.register(UINib(nibName:  "MenuViewTableViewCell", bundle: nil), forCellReuseIdentifier: "MenuTableCell")
        
        self.passengerTableView.register(UITableViewCell.self, forCellReuseIdentifier: "passengerTableView")
        self.passengerTableView.register(UITableViewHeaderFooterView.self, forHeaderFooterViewReuseIdentifier: "passengerTableHeaderView")
        
        self.passengerTableView.tableFooterView = UIView()
        
        self.isShowOkButton()
        
        if sectionDetails.count <= 0
        {
            self.sectionDetails = self.getPassengerSectionDetails()
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - TableView Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        
            switch moduleName {
                
            case "BookingDetatils":
                return 1
                
            case Constant.Deviation.DEVIATION_DROP_DOWN:
                return sectionDetails.count
                
            default:
                 return 0
            }
            
      
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        switch moduleName {
            
        case "BookingDetatils":
            return (passangerDetailsArray?.count)!
            
        case Constant.Deviation.DEVIATION_DROP_DOWN:
                  return sectionDetails[section].row.count
            
        default:
            return 0
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
            switch moduleName {
                
            case "BookingDetatils":
                return 80
                
            case Constant.Deviation.DEVIATION_DROP_DOWN:
            return 40
                
            default:
                return 0
            }
            
        
        
        
    }
    
  /*  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch moduleName {
            
        case AlertMessage.Deviation.DEVIATION_DROP_DOWN:
            return 40
            
        default:
            return 0
        }
    }*/
    
/*    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        
         switch moduleName {
            
         case AlertMessage.Deviation.DEVIATION_DROP_DOWN:
            guard let cell = tableView.dequeueReusableHeaderFooterView(withIdentifier: "passengerTableHeaderView") else {
                fatalError("The dequeued cell is not an instance of passengerTableView.")
            }
            
            var myCustomView: UIImageView =  UIImageView(frame: CGRect(x: cell.contentView.bounds.width - 50 , y: cell.contentView.bounds.height / 4 , width: 25, height: 25))
            // var myImage: UIImage = UIImage(cgImage: #imageLiteral(resourceName: "tick") as! CGImage)
            myCustomView.image = #imageLiteral(resourceName: "tick")
            
            var label:UILabel = UILabel(frame: CGRect(x: 8, y: cell.contentView.bounds.height / 4 , width: cell.contentView.bounds.width - 50, height: 25))
            
            label.text = "Hi"
            
            cell.backgroundColor =  UIColor.black
            
             cell.contentView.addSubview(label)
            
            cell.contentView.addSubview(myCustomView)
            
            return cell
            
        default:
            return UIView()
        }
        
       
    }*/
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            switch moduleName {
                
            case "BookingDetatils":
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableCell", for: indexPath) as? MenuViewTableViewCell
                    else {
                        
                        fatalError("The dequeued cell is not an instance of MenuViewTableViewCell.")
                }
                
                return self.getCellForPassengerDeviationBookingDetatils(tableView, cellForRowAt: indexPath,cell: cell)
                
            case Constant.Deviation.DEVIATION_DROP_DOWN:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "passengerTableView", for: indexPath) as? UITableViewCell
                    else {
                        
                        fatalError("The dequeued cell is not an instance of MealTableViewCell.")
                }
               return  self.getCellForPassengerDetailsForDeviationDropDown(tableView, cellForRowAt: indexPath, cell: cell)
                
            default:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "passengerTableView", for: indexPath) as? UITableViewCell
                    else {
                        
                        fatalError("The dequeued cell is not an instance of MealTableViewCell.")
                }
                return cell
            }
        
    }
    
    
    func getCellForPassengerDeviationBookingDetatils(_ tableView: UITableView, cellForRowAt indexPath: IndexPath,cell:MenuViewTableViewCell) -> UITableViewCell {
        
        
        
        var userName:NSMutableAttributedString? = nil
        
        if let passenger = passangerDetailsArray?[indexPath.row] {
            
            let groupAttr = [convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor):UIColor.lightGray , convertFromNSAttributedStringKey(NSAttributedString.Key.font): cell.menuItemTitleLabel?.font! ?? UIFont.systemFontSize] as [String : Any]
            
            let userNameAttr = [convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor):  UIColor.black , convertFromNSAttributedStringKey(NSAttributedString.Key.font): cell.menuItemTitleLabel?.font! ?? UIFont.systemFontSize] as [String : Any]
            
            
            userName = NSMutableAttributedString(string: "\(passenger.firstName?.capitalized ?? "") \(passenger.middleName?.capitalized ?? "") \(passenger.lastName?.capitalized ?? "")", attributes: convertToOptionalNSAttributedStringKeyDictionary(userNameAttr))
            
            let group = NSMutableAttributedString(string: " (As per group)", attributes: convertToOptionalNSAttributedStringKeyDictionary(groupAttr))
            
            let deviationObject =  deviationDetailsArray?.filter({ (deviationObject) -> Bool in
                
                deviationObject.passengerNumber == passenger.passengerNumber
                
            })
            
            
            switch (deviationObject?.count)! {
                
            case  0:
                
                let userNameText = NSMutableAttributedString()
                userNameText.append(userName!)
                userNameText.append(group)
                cell.menuItemTitleLabel.attributedText = userNameText
                
                cell.deviationButton.isHidden = true
                
            default:
                
                let userNameText = NSMutableAttributedString()
                userNameText.append(userName!)
                cell.menuItemTitleLabel.attributedText = userNameText
                
                cell.deviationButton.isHidden = false
                
            }
            
            
            switch passenger.title!.lowercased() {
            case "mr":
                cell.menuItemImageView.image = #imageLiteral(resourceName: "maleTourDetail") //UIImage(named:"adultOptional.png")
                
            case  "master","inf":
                cell.menuItemImageView.image = #imageLiteral(resourceName: "childTourDetail")
            case  "mrs":
                cell.menuItemImageView.image = #imageLiteral(resourceName: "femaleTourDetail")
                
            default:
                cell.menuItemImageView.image = #imageLiteral(resourceName: "maleTourDetail")//UIImage(named:"adultOptional.png")
            }
            
            
        }
        
        
        
        cell.menuDetailIcon.isHidden = true
        
        cell.deviationButton.addTarget(self, action: #selector(onDeviationNowButtonClick(button:)), for: .touchUpInside)
        
        
        return cell
        
    }
    
    
    func getCellForPassengerDetailsForDeviationDropDown(_ tableView: UITableView, cellForRowAt indexPath: IndexPath,cell:UITableViewCell) -> UITableViewCell
    {
        
        
        cell.textLabel?.font = UIFont(name: "Roboto-Light", size: 12)
        
      //  cell.imageView?.frame  = CGRect(x: 0, y: 0, width: 20, height: 20)
        
        cell.textLabel?.textColor = AppUtility.hexStringToUIColor(hex: "#333333")

        cell.backgroundColor = AppUtility.hexStringToUIColor(hex: "#ffffff")
   
        
        switch indexPath.section {
            
        case 0:
            
            let passengerDetails =  sectionDetails[0].row[indexPath.row]
            
            // cell.accessoryType = .checkmark
            
            let customAccessoryView:UIView = UIView(frame:CGRect(x: 0, y: 0, width: 20, height: 20) )
            
            customAccessoryView.isUserInteractionEnabled = false
            
            let selectAllButton = UIButton(type: .custom)
            selectAllButton.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
         
            
            if passengerDetails.isSelected
            {
               // cell.tintColor = UIColor.blue
                
               
                selectAllButton.setImage(#imageLiteral(resourceName: "checkbox_tick"), for: .normal)
 
                customAccessoryView.addSubview(selectAllButton)
                
             
                
            }else{
                
            
                selectAllButton.setImage(#imageLiteral(resourceName: "checkbox_empty"), for: .normal)
               
                customAccessoryView.addSubview(selectAllButton)
            }
            
               cell.accessoryView = customAccessoryView
            
            if indexPath.row == 0
            {
                cell.textLabel?.text = "Select All"
                
                cell.backgroundColor = AppUtility.hexStringToUIColor(hex: "#f4f4f4")
               
            }else
            {
                
                cell.textLabel?.text = "\(passengerDetails.passenger?.firstName ?? "") ".capitalized + "\(passengerDetails.passenger?.middleName ??  "") ".capitalized + "\(passengerDetails.passenger?.lastName ??  "")".capitalized
                
            }
            
            break
            
        case 1:
            
            let passengerDetails =  sectionDetails[1].row[indexPath.row]
            
             cell.accessoryType = .none
            
            if indexPath.row == 0
            {
                cell.textLabel?.text = "Booked Deviation/Ticket generated Passenger"
                cell.backgroundColor = AppUtility.hexStringToUIColor(hex: "#f4f4f4")
                
            }else
            {
               // cell.accessoryType = .checkmark
                 cell.imageView?.image = nil // #imageLiteral(resourceName: "checkbox_empty")
                cell.textLabel?.text = "\(passengerDetails.passenger?.firstName ?? "") ".capitalized + "\(passengerDetails.passenger?.middleName ??  "") ".capitalized + "\(passengerDetails.passenger?.lastName ??  "")".capitalized
            }

            break
            
            default:
            cell.accessoryType = .none
        }
        
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        tableView.deselectRow(at: indexPath, animated: true)
       // self.removeOverLay()
        
        switch moduleName {
            
        case "BookingDetatils":
            self.dismiss(animated: true, completion: {
                
                self.removeOverLay()
                
                let userProfileVC:UserProfileViewController = UserProfileViewController(nibName: "UserProfileViewController", bundle: nil)
                
                userProfileVC.passangerDetailsArray = self.passangerDetailsArray
                userProfileVC.tourStatus = self.tourStatus
                userProfileVC.selectedPassenger = self.passangerDetailsArray?[indexPath.row]
                
                self.presentingView?.navigationController?.pushViewController(userProfileVC, animated: true)
            })
            break
        case Constant.Deviation.DEVIATION_DROP_DOWN:
            
            
           if indexPath.section == 0
           {
                if indexPath.row == 0
                {
                    if sectionDetails[0].row[0].isSelected == true
                    {
                        self.setDeSelectAll(indexPath: indexPath)
                        
                    }else{
                    
                         self.setSelectAll(indexPath: indexPath)
                    }
                    
                }else{
                    
                
                sectionDetails[0].row[indexPath.row].isSelected = !sectionDetails[0].row[indexPath.row].isSelected
                    
                  if self.isSelectAll(indexPath: indexPath)
                  {
                    sectionDetails[0].row[0].isSelected = true
                    
                  }else{
                    
                     sectionDetails[0].row[0].isSelected = false
                    
                    }
                
                    
                }
                
                let sectionIndex = IndexSet(integer: indexPath.section)
                
                self.passengerTableView.reloadSections(sectionIndex, with: .none)
           }
            break
            
        default:
            printLog("default click")
        }
        
        
        
    }
    
  /*  func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        switch moduleName {
            
        case "BookingDetatils":
            return 0
            
        case AlertMessage.Deviation.DEVIATION_DROP_DOWN:
            if section == sectionDetails.count - 1
            {
                        return 30
            }
            return 0
            
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        
        switch moduleName {
            
        case "BookingDetatils":
            return UIView()
            
        case AlertMessage.Deviation.DEVIATION_DROP_DOWN:
            if section == sectionDetails.count - 1
            {
                let customView = UIView(frame: CGRect(x: 0, y: 0, width: self.passengerTableView.bounds.width, height: 30))
                
                customView.backgroundColor = UIColor.white
                let button = UIButton(frame: CGRect(x:self.passengerTableView.bounds.width - 50 , y: 0, width:25, height: 25))
               // button.center =  CGPoint(x: customView.frame.size.width  / 2, y: customView.frame.size.height / 2)
                button.backgroundColor = UIColor.red
                
                button.setTitle("Ok", for: .normal)
                button.addTarget(self, action: #selector(okButtonClick), for: .touchUpInside)
                customView.addSubview(button)
                
                return customView
            }
            return UIView()
            
        default:
            return UIView()
        }
        
        
        
        
    }*/
    
    @IBAction func okButtonClick(_ sender: Any) {
        
        self.passengerProtocol?.onOkButtonClick!(vieWcontroller: self)
    }
    
    func isSelectAll(indexPath:IndexPath) -> Bool {
        
       if sectionDetails[indexPath.section].row.count > 1
       {
        
        for (index, data) in sectionDetails[indexPath.section].row.enumerated() {
            
            if   index > 0 && !data.isSelected
            {
                return false
            }
            
        }
        
       }else{
        
        return false
       
        }

         return true
    }
    
    func setSelectAll(indexPath:IndexPath) {
        
        if sectionDetails[indexPath.section].row.count > 1
        {
            for (index, data) in sectionDetails[indexPath.section].row.enumerated() {
                
                sectionDetails[indexPath.section].row[index].isSelected = true
            
        }
    }
    }
    
        func setDeSelectAll(indexPath:IndexPath) {
            
            if sectionDetails[indexPath.section].row.count > 1
            {
                for (index, data) in sectionDetails[indexPath.section].row.enumerated() {
                    
                    sectionDetails[indexPath.section].row[index].isSelected = false
                    
                }
            }
    }
    
    //MARK:Deviation Button Click
    
    @objc func onDeviationNowButtonClick(button:UIButton)  {
        
        let buttonPosition = button.convert(CGPoint(), to: passengerTableView)
        
        if  let indexPath = self.passengerTableView.indexPathForRow(at: buttonPosition)
        {
            if let passenger = passangerDetailsArray?[(indexPath.row)]
            {
                
                let deviationceDetailsArray = self.getDeviationDetails(by : passenger)
                
                if(deviationceDetailsArray != nil && deviationceDetailsArray.count > 0)
                {
                    self.dismiss(animated: true, completion: {
                        
                        
                        self.removeOverLay()
                        
                        let viewController : RequestedDeviationViewController = RequestedDeviationViewController(nibName: "RequestedDeviationViewController", bundle: nil)
                        viewController.passengerName = "\(passenger.firstName!)" + " "+"\(passenger.lastName!)"
                        
                        
                        viewController.deviationceDetailsArray = deviationceDetailsArray
                        
                        self.presentingView?.navigationController?.pushViewController(viewController, animated: true)
                        
                    })
                    
                    
                    
                    
                }
            }
        }
        
    }
    
    func getDeviationDetails(by passenger:Passenger) -> Array<Deviation> {
        
        if passenger.passengerNumber != nil
        {
            let filteredPassengerArray = deviationDetailsArray?.filter() { $0.passengerNumber == passenger.passengerNumber }
            
            return filteredPassengerArray!;
            
        }
        
        return [];
    }
    
    func removeOverLay() {
        
        guard let overlay = (self.presentingView as! TourDetailViewController).overlay else {
            return
        }
        
        DispatchQueue.main.async() {
            UIView.animate(withDuration: 0.2, animations: {
                overlay.alpha = 0.0
            }, completion: { _ in
                overlay.removeFromSuperview()
            })
        }
        
    }
    
    
    
    func getPassengerSectionDetails() ->[(isOpen: Bool,isSelectAll: Bool, section: Int ,row : [(isSelected: Bool, index: Int ,passenger:Passenger?)])]
    {
        switch moduleName {
            
        case "BookingDetatils":
            return []
        case Constant.Deviation.DEVIATION_DROP_DOWN:
            return self.getPassengerDetailsForDeviationDropDown()
            
        default:
            return []
        }
    }
    
    func getPassengerDetailsForDeviationDropDown() ->[(isOpen: Bool,isSelectAll: Bool, section: Int ,row : [(isSelected: Bool, index: Int ,passenger:Passenger?)])]  {
        
        var deviationDropDownPassengerDetails :[(isOpen: Bool,isSelectAll: Bool, section: Int ,row : [(isSelected: Bool, index: Int ,passenger:Passenger?)])] = []
        
        var  rowSection0 : [(isSelected: Bool, index: Int, passenger: Passenger?)] = []
        var  rowSection1 : [(isSelected: Bool, index: Int, passenger: Passenger?)] = []
        
        
        var rowSection0Index:Int = 0
        var rowSection1Index:Int = 0
        
        let passenger:Passenger? = nil
        
        rowSection1.append((isSelected: false, index: rowSection1Index, passenger:passenger ))
        rowSection0.append((isSelected: false, index: rowSection0Index, passenger:passenger))
        
        for passenger in passangerDetailsArray!
        {
            if self.isTicketPresent(passenger: passenger) || self.isDeviationCreated(passenger: passenger)
            {
              rowSection1Index = rowSection1Index + 1
                
            rowSection1.append((isSelected: false, index: rowSection1Index, passenger: passenger))
            
            }else{
            
                rowSection0Index = rowSection0Index + 1
                
                if self.selectedPassengerNumber != nil && self.selectedPassengerNumber == passenger.passengerNumber
                {
                     rowSection0.append((isSelected: true, index: rowSection0Index, passenger: passenger))
                }else
                {
                    rowSection0.append((isSelected: false, index: rowSection0Index, passenger: passenger))
                }
            }
            
            
            
        }
        
         if rowSection0Index > 0
         {
             deviationDropDownPassengerDetails.append((isOpen: true, isSelectAll: false, section: 0, row:rowSection0 ))
            
        }
        
        if rowSection1Index > 0
         {
            deviationDropDownPassengerDetails.append((isOpen: true, isSelectAll: false, section: 1, row:rowSection1 ))
        }
        
        
    return deviationDropDownPassengerDetails
        
    }
    
    
    
    func isDeviationCreated(passenger:Passenger) -> Bool
    {
    
        if self.getDeviationDetails(by: passenger).count > 0
        {
            return true
        }
    
        return false
    
    }
    
    
    
    func isTicketPresent(passenger:Passenger) -> Bool
    {
        if let passengerDocuments:Array<TourPassengerDouments> = tourDetails?.passengerDocumentRelation?.allObjects as? Array<TourPassengerDouments>
        {
            if  passengerDocuments.count > 0
            {
                
                let filteredTicket = passengerDocuments.filter(){ /*$0.passengerNumber == passenger.passengerNumber &&*/ ($0.documentType?.uppercased() ?? "").contains("VOUCHER")}
                
                if filteredTicket.count > 0
                {
                    return true
                }
                
            }
        }
        
        
        return false
    }
    
    func isShowOkButton()  {
        
        
        switch moduleName {
            
        case "BookingDetatils":
            self.OkBtnheighConstraint.constant = 0
            break
        case Constant.Deviation.DEVIATION_DROP_DOWN:
            self.OkBtnheighConstraint.constant = 30
            break
            
        default:
            self.OkBtnheighConstraint.constant = 0
        }
        
        
        
    }

}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromNSAttributedStringKey(_ input: NSAttributedString.Key) -> String {
	return input.rawValue
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}
