//
//  BannerCollectionViewCell.m
//  holidays
//
//  Created by Kush_Tech on 24/02/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "BannerCollectionViewCell.h"
#import "UIImageView+WebCache.h"
#import "NetCoreAnalyticsVC.h"
#import "Constants.h"
@implementation BannerCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    dispatch_async(dispatch_get_main_queue(), ^{
            
//            self.banner_baseView.layer.cornerRadius = 5.0;
//            self.banner_imgView.layer.cornerRadius = 5.0;
    //      self.banner_imgView.layer.masksToBounds = YES;
//            self.banner_baseView.clipsToBounds = YES;
//            self.banner_imgView.clipsToBounds = YES;
        });
}
-(void)showBannerDetails:(BannerImgObject*)bannerObj{
    
//    NSLog(@"%@",bannerObj.bannerLargeImage);
//    NSLog(@"%@",bannerObj.bannerMediumImage);
//    NSLog(@"%@",bannerObj.bannerSmallImage);
//
//
//    NSString *largeImg = bannerObj.bannerLargeImage;
//    NSString *mediumImg = bannerObj.bannerMediumImage;
//    NSString *smallImg = bannerObj.bannerSmallImage;
//    NSString *mainImgUrl;
//    if ([largeImg isEqualToString:@"NA"]){
//        mainImgUrl = largeImg;
//    }else if ([mediumImg isEqualToString:@"NA"]){
//        mainImgUrl = mediumImg;
//    }else if ([smallImg isEqualToString:@"NA"]){
//        mainImgUrl = smallImg;
//    }
    NSLog(@"%@",bannerObj.mainBannerImgUrl);
    NSURL* urlImage=[NSURL URLWithString:bannerObj.mainBannerImgUrl];
    if(urlImage)
    {
        UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        indicator.center = self.banner_imgView.center;
        [self.banner_imgView addSubview:indicator];
        [indicator startAnimating];
        
        [self.banner_imgView sd_setImageWithURL:urlImage placeholderImage:[UIImage imageNamed:@"defaultBanner.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
            [indicator stopAnimating];
        }];
    }
    
}

-(void)showWhatsNewDetails:(WhatsNewObject*)bannerObj withData:(BOOL)flag{
    
    if (!flag){
        self.lbl_Title.text = bannerObj.bannerName;
    }
    
    NSURL* urlImage=[NSURL URLWithString:bannerObj.bannerSmallImage];
    if (urlImage == nil){
        urlImage=[NSURL URLWithString:bannerObj.mainBannerImgUrl];
    }
    if(urlImage)
    {
        UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        indicator.center = self.banner_imgView.center;
        [self.banner_imgView addSubview:indicator];
        [indicator startAnimating];
        
        //        self.lbl_SubTitle.text = bannerObj.bannerPageName;
        [self.banner_imgView sd_setImageWithURL:urlImage placeholderImage:[UIImage imageNamed:@"defaultBanner.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
            [indicator stopAnimating];
        }];
        
    }
    
}
@end
