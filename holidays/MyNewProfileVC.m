//
//  MyNewProfileVC.m
//  holidays
//
//  Created by Kush_Team on 27/05/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "MyNewProfileVC.h"
#import "ProfileTblViewCell.h"
#import "ProfileHeaderViewCell.h"

@interface MyNewProfileVC ()<BottomViewDelegaete,BottomStacksViewDelegaete,UITableViewDelegate,UITableViewDataSource>
{
    NSArray *imgArray;
    NSArray *titleArray;
     NSArray *subTitleArray;
}
@end

@implementation MyNewProfileVC

- (void)viewDidLoad {
//    self.botton_view.bottomDelegate = self;
    self.botton_view.bottomStackDelegate = self;
    [self.botton_view buttonSelected:2];
//    imgArray = [NSArray arrayWithObjects:@"MyAccAddress",@"MyAccEmail",@"MyAccContact",@"MyAccPancard",@"MyAccPassport", nil];
//    titleArray = [NSArray arrayWithObjects:@"Address",@"Email Address",@"Contact Number",@"PanCard",@"Passport", nil];
    imgArray = [NSArray arrayWithObjects:@"MyAccAddress",@"MyAccEmail",@"MyAccContact", nil];
    titleArray = [NSArray arrayWithObjects:@"Address",@"Email Address",@"Contact Number", nil];
    NSString *emailID = [[NSUserDefaults standardUserDefaults] valueForKey:kLoginEmailId];
    NSString *phone = [[NSUserDefaults standardUserDefaults] valueForKey:kuserDefaultTC_MobileNumber];
    emailID = (emailID) ? emailID : @"";
    phone = (phone) ? phone : @"";
    subTitleArray = [NSArray arrayWithObjects:@"..",emailID, phone, nil];
    [self.table_Views registerNib:[UINib nibWithNibName:@"ProfileHeaderViewCell" bundle:nil] forCellReuseIdentifier:@"ProfileHeaderViewCell"];
    [self.table_Views registerNib:[UINib nibWithNibName:@"ProfileTblViewCell" bundle:nil] forCellReuseIdentifier:@"ProfileTblViewCell"];
}
- (void)viewWillAppear:(BOOL)animated{
    
}- (void)viewDidAppear:(BOOL)animated{
    
}
#pragma mark - Table view data source and delegated
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [titleArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ProfileTblViewCell* infoCell = (ProfileTblViewCell*)[tableView dequeueReusableCellWithIdentifier:@"ProfileTblViewCell"];
    infoCell.img_Icons.image = [UIImage imageNamed:[imgArray objectAtIndex:indexPath.row]];
    infoCell.lbl_firstOne.text = [titleArray objectAtIndex:indexPath.row];
    infoCell.lbl_secondOne.text = [subTitleArray objectAtIndex:indexPath.row];
    infoCell.view_firstOne.hidden = YES;
    infoCell.const_firtsViewWidth.constant = 0;
    infoCell.const_secondViewWidth.constant = infoCell.frame.size.width*0.5;
    [infoCell setConstraints:30.0];
    return infoCell;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    ProfileTblViewCell* newHeaderView = (ProfileTblViewCell*)[tableView dequeueReusableCellWithIdentifier:@"ProfileTblViewCell"];
//    ProfileHeaderViewCell *newHeaderView = (ProfileHeaderViewCell*)[tableView dequeueReusableCellWithIdentifier:@"ProfileHeaderViewCell"];
//    newHeaderView.lbl_emailId.hidden = YES;
//    newHeaderView.btn_logout.hidden = YES;
    newHeaderView.img_Icons.image = [UIImage imageNamed:@"MoreEmptyUser"];
    newHeaderView.lbl_title.font = [UIFont fontWithName:@"Helvetica-Bold" size:15];
//    NSString *emailID = [[NSUserDefaults standardUserDefaults] valueForKey:kLoginEmailId];
//    newHeaderView.lbl_title.text = emailID;
    NSString *fname = [[NSUserDefaults standardUserDefaults] valueForKey:kUserFirstName];
    newHeaderView.lbl_title.text = ([fname length]>0) ? fname : @"Username";
    newHeaderView.const_firtsViewWidth.constant = newHeaderView.frame.size.width*0.5;
    newHeaderView.const_secondViewWidth.constant = 0;
    newHeaderView.view_secondOne.hidden = YES;
    NSLog(@"Prfile Imahe %@",[[NSUserDefaults standardUserDefaults]objectForKey:kUserImageString]);
    [newHeaderView setProfileImageFromStore:[[NSUserDefaults standardUserDefaults]objectForKey:kUserImageString]];
    return newHeaderView;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return  68;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 100;
}

#pragma mark - Bottom View Delegate

-(void)homeButtonClick{
    [self jumpToControllerfromViewNum:3 toViewNum:0];
}
- (void)bookingButtonClick{
    [self jumpToControllerfromViewNum:3 toViewNum:1];
}
-(void)profileButtonClick{
 
}
-(void)notificationButtonClick{
    [self jumpToControllerfromViewNum:3 toViewNum:4];
}
-(void)moreButtonClick{
    [self jumpToControllerfromViewNum:3 toViewNum:4];
}
@end
