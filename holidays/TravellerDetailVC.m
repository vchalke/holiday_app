//
//  TravellerDetailVC.m
//  holidays
//
//  Created by Kush_Tech on 16/04/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "TravellerDetailVC.h"

@interface TravellerDetailVC ()

@end

@implementation TravellerDetailVC

- (void)viewDidLoad {
//    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)btn_backFromTravelDetail:(id)sender {
    [self jumpToPreviousViewController];
}

@end
