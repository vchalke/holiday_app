//
//  Notifications+CoreDataProperties.swift
//  holidays
//
//  Created by Saurav on 28/11/17.
//  Copyright © 2017 Mobicule. All rights reserved.
//

import Foundation
import CoreData


extension Notifications {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Notifications> {
        return NSFetchRequest<Notifications>(entityName: "Notifications")
    }

    @NSManaged public var bfNumber: String?
    @NSManaged public var createdOn: NSDate?
    @NSManaged public var imageURL: String?
    @NSManaged public var message: String?
    @NSManaged public var notificationStatus: String?
    @NSManaged public var redirect: String?
    @NSManaged public var subTitle: String?
    @NSManaged public var title: String?
    @NSManaged public var type: String?
    @NSManaged public var userMobileNumber: String?
    @NSManaged public var userRelation: UserProfile?

}
