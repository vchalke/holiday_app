//
//  SightSeeingTableViewCell.h
//  holidays
//
//  Created by Kush_Team on 24/07/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SightSeenObject.h"
NS_ASSUME_NONNULL_BEGIN

@interface SightSeeingTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbl_Title;
@property (weak, nonatomic) IBOutlet UILabel *lbl_subTitle;
-(void)loadDSightSeeingFromDetailModel:(SightSeenObject*)sightModel;
-(void)loadDSightSeeingFromDetailDict:(NSDictionary*)sightDict withArray:(NSString*)key;
@end

NS_ASSUME_NONNULL_END
