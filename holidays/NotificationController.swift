//
//  NotificationController.swift
//  sotc-consumer-application
//
//  Created by Parshwanath on 06/11/17.
//  Copyright © 2017 Mobicule Technologies Private Limited. All rights reserved.
//

import Foundation
import CoreData
import UserNotifications
import UIKit



class NotificationController:NSObject
{
    
    class func sendNotificationSubscriberRequest(completion : @escaping(_ status:Bool,_ message:String) -> Void) {
     
        
        
        
        let mobileNumber:String = UserDefaults.standard.object(forKey: UserDefauldConstant.userMobileNumber) as? String ?? ""
        
        let modelName = UIDevice.current.model
        
        let osVersion = UIDevice.current.systemVersion
        
        let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        
        let deviceToken =  UserDefaults.standard.object(forKey: UserDefauldConstant.pushNotificationDeviceToken)
        
        let uuID =  KeychainService.loadUUID()
//        let uuID =  AppUtility.setUUID()
       /* let dataDetails:Dictionary<String,Any> = [APIConstants.Platform:APIConstants.PlatformType,APIConstants.DeviceId:deviceToken ?? "",APIConstants.DeviceModel:modelName,APIConstants.OSVersion:osVersion,APIConstants.MacAddress:"\(mobileNumber)-\(uuID ?? "")",APIConstants.ImeiNumber:""]*/
        
        let dataDetails:Dictionary<String,Any> = [APIConstants.Platform:APIConstants.PlatformType,APIConstants.DeviceId:deviceToken ?? "",APIConstants.DeviceModel:modelName,APIConstants.OSVersion:osVersion,APIConstants.MacAddress:"\(uuID ?? "")",APIConstants.Notification_MobileNumber:"\(mobileNumber)",APIConstants.ImeiNumber:""]
        
        
        let data:Array<Dictionary<String,Any>> = [dataDetails]
        
        let  queryParameterMap:Dictionary<String,Any> = [:]
        
        let userDetails:Dictionary<String,Any> = [APIConstants.CheckVersionKey:APIConstants.CheckVersionType,
                                                  APIConstants.Client:APIConstants.PlatformType,
                                                  APIConstants.AppVersion:version!,
                                                  APIConstants.EntityId:"1"]
        
        let requestDict:Dictionary<String,Any> = [ APIConstants.EntityType:EntityType.Transaction.rawValue,
                                                   APIConstants.EntityAction:EntityAction.Add.rawValue,
                                                   APIConstants.Entity:"notificationSubscriber",APIConstants.User:userDetails,APIConstants.Data:data,APIConstants.QueryParameterMap:queryParameterMap]
        
        let headers: HTTPHeaders = [
            APIConstants.ContentType: ContentType.ApplicationJson.rawValue,
            APIConstants.Authorization: AuthenticationConstant.Mobicule_Authorization
            
        ]
        
        request(AuthenticationConstant.MOBICULE_URL, method: .post, parameters: requestDict, encoding: JSONEncoding.default,headers: headers).responseJSON { response in
            
            printLog("Request: \(String(describing: response.request))")   // original url request
            printLog("Request Data: \(String(describing: requestDict))")   // original url request
            printLog("Response: \(String(describing: response.response))") // http url response
            printLog("Result: \(response.result)")                         // response serialization result
            if response.response?.statusCode
                == 200
            {
                
                if let json = response.result.value {
                    printLog("JSON: \(json)") // serialized json response
                    
                    
                    if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                        
                        printLog("Data: \(utf8Text)") // original server data as UTF8 string
                        
                        if let json = response.result.value {
                            
                            if let responseDict:Dictionary<String,Any> = json as? Dictionary<String,Any>
                            {
                                
                                if let status:Any = (responseDict[APIConstants.Status])
                                {
                                    if  "\(status)".lowercased() == APIConstants.Success
                                    {
                                        
                                        completion(true,"")
                                        
                                    }
                                    else{
                                        completion(false,"Failed to register for notification")
                                    }
                                }
                                else{
                                    completion(false,"Failed to register for notification")
                                }
                            }
                            else{
                                completion(false,"Failed to register for notification")
                            }
                        }
                        else{
                            completion(false,"Failed to register for notification")
                        }
                        
                        
                    }
                    else{
                        completion(false,"Failed to register for notification")
                    }
                }else{
                    
                    completion(false,"Failed to register for notification")
                    
                }
                
            }
            else
            {
                completion(false,NetworkValidationMessages.InternetNotConnected.message)
            }
        }
        
 
        
    }
    
    
    class func getNotificationsDetailsFromDB(by mobileNumber:String) -> Array<Notifications>?
    {
        return NotificationDBManager.getNotificationsDetails(by: mobileNumber)
    }
    
    class func moveToRespectiveViewController(notificationType:String , bfNumber:String , webViewRedirectionURL:String , message:String)  {
        
        
       var tourStatus:TourStatus = TourStatus.Past
       
        
        do {
            
          /*  let fetchRequest: NSFetchRequest<Tour> = Tour.fetchRequest()
            let predicate = NSPredicate(format: "bfNumber == '\(bfNumber)'")
            
            fetchRequest.predicate = predicate
            let tourList:Array<Tour> = try CoreDataController.getContext().fetch(fetchRequest)*/
            
            LoadingIndicatorView.show()
            
            UserBookingController.fetchTourDetails(bfNumber: bfNumber) { (messageTourDetail, tourList) in
                
                if messageTourDetail.status                {
                    
                    
                    //DispatchQueue.main.sync {
                        
                        LoadingIndicatorView.hide()
                        
                        if  let tourDetail:Tour = tourList {
                            
                            
                            
                            
                           // let tourDetail:Tour = tourList.first!
                            
                            
                            if (tourDetail.departureDate?.addingTimeInterval(23.9 * 60 * 60).timeIntervalSince1970)! < Date().timeIntervalSince1970 {
                                
                                tourStatus = TourStatus.Past
                                
                            }
                            else if tourDetail.productBookingStatus?.lowercased() == "canceled" || tourDetail.productBookingStatus?.lowercased() == "cancelled"
                            {
                                tourStatus = TourStatus.Cancelled
                                
                            }else
                            {
                                tourStatus = TourStatus.UpComing
                            }
                            
                            switch notificationType.lowercased() {
                                
                            case NotificationType.AppointmentLetter.rawValue.lowercased(),NotificationType.Visas.rawValue.lowercased(),NotificationType.VisaDocuments.rawValue.lowercased() :
                                
                                
                                let visaDetailsDoucumets:Array<Visa>? = (tourDetail.visaRelation?.allObjects as?  Array<Visa>)!
                                
                                let visaCountryArray:Array<VisaCountryMaster>? = (tourDetail.visaCountryRelation?.allObjects as?  Array<VisaCountryMaster>)!
                                
                                if (visaDetailsDoucumets != nil && (visaDetailsDoucumets?.count)! > 0) || (visaCountryArray != nil && (visaCountryArray?.count)! > 0)
                                    
                                {
                                    
                                    let viewController:VisaViewController = VisaViewController(nibName: "VisaViewController", bundle: nil)
                                    
                                    viewController.tourDetails = tourDetail
                                    viewController.isfromNotificationHomeVc = NotificationController.isFromNotificationHomeVC()
                                    
                                    NotificationController.setViewController(viewController: viewController, message:message ,title: tourDetail.bfNumber ?? "")
                                    
                                }else{
                                    
                                    AppUtility.displayAlertController(title: "No data", message: "No data found !")
                                    
                                }
                                
                                
                            case NotificationType.Tickets.rawValue.lowercased() :
                                
                                
                                if tourDetail.paymentFlag?.lowercased() == "yes"
                                    
                                {
                                    let viewController:TicketsViewController = TicketsViewController(nibName: "TicketsViewController", bundle: nil)
                                    
                                    viewController.isfromNotificationHomeVc = NotificationController.isFromNotificationHomeVC()
                                    
                                    if let passengerData: Array<Passenger> = (tourDetail.passengerRelation?.allObjects as? Array<Passenger>)?.sorted(by: { (Passenger1, Passenger2) -> Bool in
                                        
                                        Passenger1.passengerNumber! < Passenger2.passengerNumber!
                                        
                                    }) {
                                        
                                        
                                        viewController.passengerDetailsArray = passengerData
                                        
                                        if let tourPassengerDoucumets = tourDetail.passengerDocumentRelation?.allObjects as?  Array<TourPassengerDouments>
                                            
                                        {
                                            if tourPassengerDoucumets.count>0 {
                                                let filteredDocumnetsArray = tourPassengerDoucumets.filter() { $0.documentType?.uppercased() == "TICKET" }
                                                
                                                viewController.passengerTicketDoucumets = filteredDocumnetsArray
                                                
                                                NotificationController.setViewController(viewController: viewController ,message:message ,title: tourDetail.bfNumber ?? "")
                                            }else{
                                                AppUtility.displayAlert(message: AlertMessage.Tickets.TICKET_YET_TO_ISSUED)
                                            }
                                            
                                        }else {
                                            
                                            AppUtility.displayAlert(message: AlertMessage.Tickets.TICKET_YET_TO_ISSUED)
                                        }
                                        
                                    }
                                    else
                                    {
                                        AppUtility.displayAlertController(title: "No data", message: "No data found !")
                                    }
                                    
                                }else
                                {
                                    AppUtility.displayAlert(message: AlertMessage.Tickets.COMPLETE_PAYMENT)
                                }
                                
                            case NotificationType.FinalTourDetail.rawValue.lowercased(),NotificationType.TourConfirmationVoucher.rawValue.lowercased() :
                                
                                if tourDetail.paymentFlag?.lowercased() == "yes"
                                {
                                    
                                    let viewController:FinalTourDetailsViewController = FinalTourDetailsViewController(nibName: "FinalTourDetailsViewController", bundle: nil)
                                    
                                    viewController.isfromNotificationHomeVc = NotificationController.isFromNotificationHomeVC()
                                    
                                    
                                    viewController.tourDetail = tourDetail
                                    
                                    NotificationController.setViewController(viewController: viewController ,message:message,title: tourDetail.bfNumber ?? "")
                                    
                                }else
                                {
                                    AppUtility.displayAlert(message:AlertMessage.FinalTourDetails.COMPLETE_PAYMENT)
                                }
                                
                            case NotificationType.Profile.rawValue.lowercased(),NotificationType.ProfileLocale.rawValue.lowercased():
                                
                                let viewController:UserProfileViewController = UserProfileViewController(nibName: "UserProfileViewController", bundle: nil)
                                
                                viewController.isfromNotificationHomeVc = NotificationController.isFromNotificationHomeVC()
                                
                                if let passengerData:Array<Passenger> = tourDetail.passengerRelation?.allObjects as? Array<Passenger>
                                {
                                    
                                    if passengerData.count > 0
                                    {
                                        viewController.passangerDetailsArray = tourDetail.passengerRelation?.allObjects as? Array<Passenger>
                                        
                                        viewController.tourStatus = tourStatus
                                        
                                        NotificationController.setViewController(viewController: viewController ,message:message,title: tourDetail.bfNumber ?? "")
                                    }
                                    else {
                                        
                                        AppUtility.displayAlertController(title: "No data", message: "No data found !")
                                    }
                                    
                                }
                                else
                                    
                                {
                                    AppUtility.displayAlertController(title: "No data", message: "No data found !")
                                }
                                
                            case NotificationType.Deviation.rawValue.lowercased(),NotificationType.DeviationLocale.rawValue.lowercased():
                                
                                let viewController:DeviationPassengerListViewController = DeviationPassengerListViewController(nibName: "DeviationPassengerListViewController", bundle: nil)
                                
                                viewController.tourDetails = tourDetail
                                viewController.tourStatus = tourStatus
                                
                                viewController.isfromNotificationHomeVc = NotificationController.isFromNotificationHomeVC()
                                
                                
                                if let passengerData:Array<Passenger> = tourDetail.passengerRelation?.allObjects as? Array<Passenger>
                                {
                                    
                                    if passengerData.count > 0
                                    {
                                        viewController.passangerDetailsArray = tourDetail.passengerRelation?.allObjects as? Array<Passenger>
                                        
                                        NotificationController.setViewController(viewController: viewController ,message:message,title: tourDetail.bfNumber ?? "")
                                    }
                                    else {
                                        
                                        AppUtility.displayAlertController(title: "No data", message: "No data found !")
                                    }
                                    
                                }
                                else
                                    
                                {
                                    AppUtility.displayAlertController(title: "No data", message: "No data found !")
                                }
                                
                                
                            case NotificationType.Feedback.rawValue.lowercased():
                                
                                self.sendTC_CSSFeedBackRequest(tourDetail: tourDetail)
                               /* let viewController:FeedbackWebViewController = FeedbackWebViewController(nibName: "FeedbackWebViewController", bundle: nil)
                                
                                viewController.redirectionURL = webViewRedirectionURL
                                if webViewRedirectionURL.count>0{
                                    viewController.moduleTitle = Title.FEEDBACK
                                    
                                    viewController.isfromNotificationHomeVc = NotificationController.isFromNotificationHomeVC()
                                    
                                    
                                    NotificationController.setViewController(viewController: viewController ,message:message ,title: tourDetail.bfNumber ?? "")
                                }else{
                                    AppUtility.displayAlertController(title: "No data", message: "No data found !")
                                }*/
                                
                                
                                
                            case NotificationType.Payments.rawValue.lowercased():
                                
                                
                                let viewController = PaymentViewController(nibName: "PaymentViewController", bundle: nil)
                                
                                viewController.tourDetail = tourDetail //tourList.first
                                viewController.tourStatus = tourStatus
                                viewController.isfromNotificationHomeVc = NotificationController.isFromNotificationHomeVC()
                                
                                if let paymentData:Array<Payment> = tourDetail.paymentRelation?.allObjects as? Array<Payment>
                                {
                                    
                                    if paymentData.count > 0
                                    {
                                        
                                        
                                        NotificationController.setViewController(viewController: viewController ,message:message ,title: tourDetail.bfNumber ?? "")
                                    }
                                    else {
                                        
                                        AppUtility.displayAlertController(title: "No data", message: "No data found !")
                                    }
                                    
                                }
                                else
                                    
                                {
                                    AppUtility.displayAlertController(title: "No data", message: "No data found !")
                                }
                                
                                
                            default:
                                
                                printLog("Invalid NotificationType")
                            }
                            
                        }
                            
                            
                        else
                        {
                            printLog("bfn is not present in database")
                            
                        }
                       
                   // }
                    
                }else
                {
                    
                    LoadingIndicatorView.hide()
                }
                
                
            }
            
            
           
            
            
        } catch  {
            printLog(error)
        }
        
        
        
        
        
    }

    //Mark: sendfeedback url
    
    class func sendTC_CSSFeedBackRequest(tourDetail:Tour)
    {
        let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        
        
        let surveyName = ""
        let oppId = tourDetail.opportunityId ?? ""
        let tourCode = tourDetail.tourCode ?? ""
        let bfn:String = tourDetail.bfNumber ?? ""
        
        
        let  departureDate = AppUtility.convertDateToString(currentDateFormat: "yyyy-MM-dd", date:tourDetail.departureDate! as Date? ?? Date(), requiredDateFormat: "dd-MM-yyyy")
        
        let  arrivalDate = AppUtility.convertDateToString(currentDateFormat: "yyyy-MM-dd", date:tourDetail.arrivalDate as! Date? ?? Date(), requiredDateFormat: "dd-MM-yyyy")
        
        let dataDetails:Dictionary<String,Any> = [surveyName:"","oppId":oppId,
                                                  "tourCode":tourCode,
                                                  "departureDate":departureDate,
                                                  "arrivalDate":arrivalDate,
                                                  "bfn":bfn]
        
        let data:Array<Dictionary<String,Any>> = [dataDetails]
        
        
        
        let  queryParameterMap:Dictionary<String,Any> = [:]
        
        let userDetails:Dictionary<String,Any> = [APIConstants.CheckVersionKey:"false",
                                                  APIConstants.Client:JsonConstant.IOS_CLIENT,
                                                  APIConstants.AppVersion:version!,
                                                  APIConstants.EntityId:"1"]
        
        let requestDict:Dictionary<String,Any> = [ APIConstants.EntityType:"transaction",
                                                   APIConstants.EntityAction:"get",
                                                   APIConstants.Entity:"feedbackUrl","identifier":"",APIConstants.User:userDetails,"data":data,"queryParameterMap":queryParameterMap]
        
        
        LoadingIndicatorView.show("Loading")
        
        
        UserBookingController.sendFeedbackRequestRequest(requestData:requestDict, completion: { (status, message) in
            
            LoadingIndicatorView.hide()
            
            if status
            {
                
                
                    
                
                    
                    
                    let viewController:FeedbackWebViewController = FeedbackWebViewController(nibName: "FeedbackWebViewController", bundle: nil)
                    
                    viewController.redirectionURL = message
                    
                    viewController.moduleTitle = Title.FEEDBACK
                    
                viewController.isfromNotificationHomeVc = NotificationController.isFromNotificationHomeVC()
                
                
                NotificationController.setViewController(viewController: viewController ,message:message ,title: tourDetail.bfNumber ?? "")
                    
                
                
            }
            else{
                
                AppUtility.displayAlertController(title: "No data", message: "No data found !")
            }
            
            
        })
        
        
    }
    
    
    
    
    class func updateNotificationBadgeCount()  {
        
//        if let appDelegate:AppDelegate = (UIApplication.shared.delegate as? AppDelegate)
//        {
//            if let rootTabbarController:UITabBarController = (appDelegate.window?.rootViewController as? UITabBarController)
//            {
//                if (rootTabbarController.viewControllers?.count)! > 1  {
//                    
//                    
//                    
//                    if let navigationController:UINavigationController = (rootTabbarController.viewControllers?[1] as? UINavigationController)
//                    {
//                        
//                        if NotificationDBManager.getUnreadNotificationCount() > 0  {
//                            
//                            navigationController.tabBarItem.badgeValue = "\(NotificationDBManager.getUnreadNotificationCount())"
//                        }
//                        else
//                        {
//                            
//                            navigationController.tabBarItem.badgeValue = nil
//                        }
//                        
//                    }
//                    
//                }
//                
//            }
//        }
        
        
      //  UIApplication.shared.applicationIconBadgeNumber = NotificationDBManager.getUnreadNotificationCount()
        
    }
    
    class func fireLocaleNotification(notificationUserInfo:Dictionary<String,Any>, fireDateTime:Date)
    {
        
        /*
        if let notificationUserInfo:Dictionary<String,String> = notificationUserInfo as? Dictionary<String,String>
        {
            
       
            
            if #available(iOS 10.0, *) {
                let content = UNMutableNotificationContent()
                content.categoryIdentifier = "awesomeNotification"
                content.title = notificationUserInfo[NotificationConstant.BFN]!
                content.body = notificationUserInfo[NotificationConstant.SUBTITLE]!
                content.userInfo = ["data":AppUtility.stringRepresenation(fromJSonDataObject: notificationUserInfo)]
                
                
                let trigger = UNTimeIntervalNotificationTrigger(timeInterval: fireDateTime.addingTimeInterval(2).timeIntervalSinceNow, repeats: false)
                let request = UNNotificationRequest(identifier: "FiveSecond \(fireDateTime.addingTimeInterval(2).timeIntervalSinceNow)", content: content, trigger: trigger)
                let center = UNUserNotificationCenter.current()
                
                //center.delegate = self
                center.add(request) { (error) in
                    
                    if  let mobileNumber:String = UserDefaults.standard.object(forKey: UserDefauldConstant.userMobileNumber) as? String
                    {
                        if !mobileNumber.isEmpty
                        {
                            
                            UserBookingDBManager.saveLocaleNotificationDetailsInDB(notificationData: notificationUserInfo,mobileNumber:mobileNumber,fireDateTime: fireDateTime)
                        }
                    }
                    
                    printLog("fire locale")
                }
            }
            else
            {
                
                let notification = UILocalNotification()
                notification.alertBody = notificationUserInfo[NotificationConstant.SUBTITLE]
                notification.alertTitle = notificationUserInfo[NotificationConstant.BFN]
                notification.fireDate = NSDate(timeIntervalSinceNow:fireDateTime.addingTimeInterval(2).timeIntervalSinceNow) as Date
                //notification.repeatInterval = NSCalendar.Unit.minute
                //UIApplication.shared.cancelAllLocalNotifications()
                notification.userInfo = ["data":AppUtility.stringRepresenation(fromJSonDataObject: notificationUserInfo)]
                UIApplication.shared.scheduledLocalNotifications = [notification]
                
                if  let mobileNumber:String = UserDefaults.standard.object(forKey: UserDefauldConstant.userMobileNumber) as? String
                {
                    if !mobileNumber.isEmpty
                    {
                        UserBookingDBManager.saveLocaleNotificationDetailsInDB(notificationData: notificationUserInfo,mobileNumber:mobileNumber,fireDateTime: fireDateTime)
                    }
                }
                
            }
        }
        
        */
    }
    
    class func removeAllNotification()
    {
        NotificationDBManager.DeleteAllNotifications()
    }
    
    
   @objc class func updateAplicationIconBadgeNumber() {
        
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().getDeliveredNotifications { (delivered) in
                printLog("Pannel Count: \(delivered.count)")
//                VJ_Commented
//                UIApplication.shared.applicationIconBadgeNumber = delivered.count
            }
        } else {
            //  Unable to update Fallback on earlier versions
        }
    }
    
    class func setViewController(viewController: UIViewController ,message:String ,title:String) {
        
        if let appDelegate:AppDelegate = (UIApplication.shared.delegate as? AppDelegate)
        {
            //(appDelegate.window?.rootViewController?.childViewControllers.last as? UITabBarController)?.viewControllers
            
            let childControllerArray:[UIViewController]? = (appDelegate.window?.rootViewController?.children)
            
            if childControllerArray?.count ?? 0 > 0
            {
                if let rootTabbarController:UITabBarController = childControllerArray?.last as? UITabBarController
                    {
                        if (rootTabbarController.viewControllers?.count)! > 0  {
                            
                            
                            if let navigationController:UINavigationController = (rootTabbarController.viewControllers?[rootTabbarController.selectedIndex] as? UINavigationController)
                            {
                                
                                if   navigationController.viewControllers.count > 0 {
                                    
                                    
                                    if let _:UserProfileViewController =  navigationController.viewControllers.last as? UserProfileViewController  {
                                        
                                        
                                        AppUtility.displayAlertController(title: "Notification - \(title)", message: message)
                                        
                                    }
                                    else
                                    {
                                        rootTabbarController.tabBar.isHidden = true
                                        navigationController.navigationBar.isHidden = false
                                        navigationController.pushViewController(viewController, animated: true)
                                    }
                                    
                                }
                                else
                                {
                                    rootTabbarController.tabBar.isHidden = true
                                    navigationController.navigationBar.isHidden = false
                                    navigationController.pushViewController(viewController, animated: true)
                                }
                                
                            }
                            
                        }
                }else{
                    if !UserDefaults.standard.bool(forKey: UserDefauldConstant.isUserLoggedIn) {
                        
                        // self.setLoginController()
                        let loginViewController = AppUtility.getLoginController()
                        
                        SlideNavigationController.sharedInstance().pushViewController(loginViewController, animated: true)
                        
                    }
                    else
                    {
                        //self.setTabbarController()
                        
                        viewController.navigationController?.isNavigationBarHidden = false
                        
                        let homeViewController =  AppUtility.getHomeController(redirectionController: viewController)
                        
                        
                        SlideNavigationController.sharedInstance().pushViewController(homeViewController, animated: true)
                        
                        
                        
                        
                        
                    }
                }
                
                
            }
            
        }
       /* if let appDelegate:AppDelegate = (UIApplication.shared.delegate as? AppDelegate)
        {
            if let rootTabbarController:UITabBarController = (appDelegate.window?.rootViewController as? UITabBarController)
            {
                if (rootTabbarController.viewControllers?.count)! > 0  {
                    
                    if let navigationController:UINavigationController = (rootTabbarController.viewControllers?[rootTabbarController.selectedIndex] as? UINavigationController)
                    {
                        
                        if   navigationController.viewControllers.count > 0 {
                            
                            
                            if let _:UserProfileViewController =  navigationController.viewControllers.last as? UserProfileViewController  {
                                
                                
                                AppUtility.displayAlertController(title: "Notification - \(title)", message: message)
                                
                            }
                            else
                            {
                                rootTabbarController.tabBar.isHidden = true
                                navigationController.pushViewController(viewController, animated: true)
                            }
                            
                        }
                        else
                        {
                            rootTabbarController.tabBar.isHidden = true
                            navigationController.pushViewController(viewController, animated: true)
                        }
                        
                    }
                    
                }
            }
        }*/
    }
    
    
    class func failToRegisterForCSSRemoteNotificationsWithError() {
        
          UserDefaults.standard.set(false, forKey: UserDefauldConstant.isRegisterForPushNotification)
    }
    
    class func resetROEDataOnController(){
        
        if let appDelegate:AppDelegate = (UIApplication.shared.delegate as? AppDelegate)
        {
            //(appDelegate.window?.rootViewController?.childViewControllers.last as? UITabBarController)?.viewControllers
            
            let childControllerArray:[UIViewController]? = (appDelegate.window?.rootViewController?.children)
            
           if childControllerArray?.count ?? 0 > 0
           {
            if let tabbarController:UITabBarController = childControllerArray?.last as? UITabBarController
            {
                if (tabbarController.viewControllers?.count)! > 0  {
                    
                    if let navigationController:UINavigationController = (tabbarController.viewControllers?[tabbarController.selectedIndex] as? UINavigationController)
                    {
                        
                        
                        if   navigationController.viewControllers.count > 0 {
                            
                            
                            if let paymentVc:PaymentViewController =  navigationController.viewControllers.last as? PaymentViewController  {
                                
                                if (paymentVc.currencyArray?.count ?? 0) > 0
                                {
                                    paymentVc.currencyArray?.removeAll();
                                }
                                
                                //paymentVc.fetchROEData(Type: "PayNow", Currency: )
                                
                            }else if let optionalPackageVC:OptionalPackageViewController =  navigationController.viewControllers.last as? OptionalPackageViewController  {
                                
                                if (optionalPackageVC.currencyArray?.count ?? 0) > 0
                                {
                                    optionalPackageVC.currencyArray?.removeAll();
                                }
                                
                                //optionalPackageVC.fetchROEData()
                            }
                            
                            
                        }
                        else
                        {
                            
                        }
                        
                    }
                    
                }
                
                
            }
            
            }
            
           /* if let tabbarController:UITabBarController = (appDelegate.window?.rootViewController as? UITabBarController)
            {
                if (tabbarController.viewControllers?.count)! > 0  {
                    
                    if let navigationController:UINavigationController = (tabbarController.viewControllers?[tabbarController.selectedIndex] as? UINavigationController)
                    {
                        
                        
                        if   navigationController.viewControllers.count > 0 {
                            
                            
                            if let paymentVc:PaymentViewController =  navigationController.viewControllers.last as? PaymentViewController  {
                                
                                if (paymentVc.currencyArray?.count ?? 0) > 0
                                {
                                    paymentVc.currencyArray?.removeAll();
                                }
                                
                                paymentVc.fetchROEData()
                                
                            }else if let optionalPackageVC:OptionalPackageViewController =  navigationController.viewControllers.last as? OptionalPackageViewController  {
                                
                                if (optionalPackageVC.currencyArray?.count ?? 0) > 0
                                {
                                    optionalPackageVC.currencyArray?.removeAll();
                                }
                                
                                optionalPackageVC.fetchROEData()
                            }
                            
                            
                        }
                        else
                        {
                            
                        }
                        
                    }
                    
                }
                
                
            }*/
        }
        
        
        
    }
    
   @objc class func setUUID() {
        
        let uuID =    KeychainService.loadUUID()
        
        if uuID != nil || uuID != ""{
            
            KeychainService.saveUUID(token:  "\(UIDevice.current.identifierForVendor?.uuidString ?? "")" as NSString)
            
            printLog(KeychainService.loadUUID() ?? "")
            
        }
    }
    
  class func registerNotificationForCSS(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceTokenString:String) {
        
       // let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        
        
        printLog("deviceTokenString : \(deviceTokenString)")
        
        let userDefaultDeviceToken = UserDefaults.standard.object(forKey: UserDefauldConstant.pushNotificationDeviceToken)
        
        
        if userDefaultDeviceToken == nil
        {
            UserDefaults.standard.set(false, forKey: UserDefauldConstant.isSubscribeForPushNotification) // make server flag false
            
            if deviceTokenString == ""
            {
                
                UserDefaults.standard.set(false, forKey: UserDefauldConstant.isRegisterForPushNotification)
                
            }else{
                
                UserDefaults.standard.set("\(deviceTokenString)", forKey: UserDefauldConstant.pushNotificationDeviceToken)
                UserDefaults.standard.set(true, forKey: UserDefauldConstant.isRegisterForPushNotification) // make tocken recived flag true
                // UserDefaults.standard.set(false, forKey: UserDefauldConstant.isSubscribeForPushNotification) // make server flag false
                
            }
            
        }else{
            
            let userDefaultDeviceTokenString = userDefaultDeviceToken as! String
            
            if !(userDefaultDeviceTokenString.caseInsensitiveCompare(deviceTokenString) == .orderedSame)
            {
                
                //set server hit false
                UserDefaults.standard.set(false, forKey: UserDefauldConstant.isSubscribeForPushNotification) // make server flag false
                UserDefaults.standard.set("\(deviceTokenString)", forKey: UserDefauldConstant.pushNotificationDeviceToken)
                UserDefaults.standard.set(true, forKey: UserDefauldConstant.isRegisterForPushNotification) // make tocken recived flag true
                
            }
            
        }
        
        //AppUtility.checkAndSubscibeNotificationOnServer()
        
        
        
    }
    
    
    class func isFromNotificationHomeVC() -> Bool {
        
        if let appDelegate:AppDelegate = (UIApplication.shared.delegate as? AppDelegate)
        {
            //(appDelegate.window?.rootViewController?.childViewControllers.last as? UITabBarController)?.viewControllers
            
            let childControllerArray:[UIViewController]? = (appDelegate.window?.rootViewController?.children)
            
            if childControllerArray?.count ?? 0 > 0
            {
                if let rootTabbarController:UITabBarController = childControllerArray?.last as? UITabBarController
                {
                     return false
                }else{
                    if UserDefaults.standard.bool(forKey: UserDefauldConstant.isUserLoggedIn) {
                        
                        return true
                    }
                }
                
            }
        }
        return false
        
    }
}

