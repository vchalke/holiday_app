//
//  UserProfile+CoreDataProperties.swift
//  holidays
//
//  Created by Saurav on 28/11/17.
//  Copyright © 2017 Mobicule. All rights reserved.
//

import Foundation
import CoreData


extension UserProfile {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<UserProfile> {
        return NSFetchRequest<UserProfile>(entityName: "UserProfile")
    }

    @NSManaged public var address: String?
    @NSManaged public var emailID: String?
    @NSManaged public var mobileNumber: String?
    @NSManaged public var notificationRelation: NSSet?
    @NSManaged public var tourRelation: NSSet?

}

// MARK: Generated accessors for notificationRelation
extension UserProfile {

    @objc(addNotificationRelationObject:)
    @NSManaged public func addToNotificationRelation(_ value: Notifications)

    @objc(removeNotificationRelationObject:)
    @NSManaged public func removeFromNotificationRelation(_ value: Notifications)

    @objc(addNotificationRelation:)
    @NSManaged public func addToNotificationRelation(_ values: NSSet)

    @objc(removeNotificationRelation:)
    @NSManaged public func removeFromNotificationRelation(_ values: NSSet)

}

// MARK: Generated accessors for tourRelation
extension UserProfile {

    @objc(addTourRelationObject:)
    @NSManaged public func addToTourRelation(_ value: Tour)

    @objc(removeTourRelationObject:)
    @NSManaged public func removeFromTourRelation(_ value: Tour)

    @objc(addTourRelation:)
    @NSManaged public func addToTourRelation(_ values: NSSet)

    @objc(removeTourRelation:)
    @NSManaged public func removeFromTourRelation(_ values: NSSet)

}
