//
//  SRPScreenVC.m
//  Thomoscook_Holiday
//
//  Created by Kush_Tech on 19/02/20.
//  Copyright © 2020 Kush_Tech. All rights reserved.
//

#import "SRPScreenVC.h"
#import "SRPCollectionCell.h"
#import "TabMenuVC.h"
#import "PSPageMenu.h"
#import "CoreUtility.h"
//#import <NetCorePush/NetCorePush.h>
#import "NetCoreAnalyticsVC.h"
#import "Thomas_Cook_Holidays-Swift.h"
#import "ColourClass.h"
#import "PDPScreenVC.h"
#import "SVProgressHUD.h"
#import "WishListScreenVC.h"
#import "FilterScreenVC.h"
#import "HeaderTwoLineTableViewCell.h"
#import "RequestForCallPopUp.h"
#import "UIViewController+MJPopupViewController.h"
#import "RequestACallVC.h"
#import "RequestViewVC.h"
#import "FilterSCrView.h"
#import "OffersDetailsVC.h"
#define fiveNights @"5 Nights"
#define sixNights @"6 Nights"
#define eightNights @"8 Nights"
#define tenNights @"10 Nights"
@interface SRPScreenVC ()<PSDelegate,SRPCollectionCellDelegate,FilterScreenDelegate,FilterSCrViewDelegate,TalkToExpertViewDelegate>
{
    LoadingView *activityLoadingView;
    NSMutableArray *arrayForResetData;
    NSString *selectedNightFilter;
    NSMutableDictionary *payloadList, *payLoadViewPageDetail;
    NSMutableDictionary *payloadListSortBy;
//    NSMutableDictionary *payLoadForViewDetails;
//    NSMutableDictionary *payLoadForViewDetailsDict;
    HolidayPackageDetail *packageDetail ;
}
@end

@interface SRPScreenVC ()
{
    NSArray *buttonArray;
//    int flag;
    NSInteger selectedIndexPath;
    NSInteger selectedSortOption;
    TalkToExpertView *expertView;
    NSArray *tableTitle;
     BOOL isShowSortView;
}
@end

@implementation SRPScreenVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.collection_view registerNib:[UINib nibWithNibName:@"SRPCollectionCell" bundle:nil] forCellWithReuseIdentifier:@"SRPCollectionCell"];
    self.lbl_title.text = [NSString stringWithFormat:@"%@",self.headerName];
    [self setMaskTo:self.talk_toExpView byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerTopLeft withCornerRadius:self.talk_toExpView.frame.size.height/2];
    buttonArray = [NSArray arrayWithObjects:self.btn_FiveNights,self.btn_SixNights,self.btn_EightNights,self.btn_TenNights, nil];
    NSArray *tagArray =  @[@5, @6, @8, @10];
    [self setButtonTitleAndAction:buttonArray withDayNum:tagArray];
    selectedNightFilter = @"1 Nights";
    self.filteredHolidayArray = [self filterByNumberOfDays:1];
    
    payloadList =  [NSMutableDictionary dictionary];
    payLoadViewPageDetail =  [NSMutableDictionary dictionary];
//    payLoadForViewDetailsDict = [[NSMutableDictionary alloc]init];
    
//    UITapGestureRecognizer *singleFingerTap =
//      [[UITapGestureRecognizer alloc] initWithTarget:self
//                                              action:@selector(handleSingleTap:)];
//    [self.talkToexpertView addGestureRecognizer:singleFingerTap];
//    tableTitle = [NSArray arrayWithObjects:@"Price: Low to High",@"Price: High to Low",@"Duration: Short to Long",@"Duration: Long to Short",@"Popular",@"Package with Deals",@"Bookable Online", nil];
    tableTitle = [NSArray arrayWithObjects:@"Price: Low to High",@"Price: High to Low",@"Duration: Short to Long",@"Duration: Long to Short",@"Popular",@"Package with Deals", nil];
    [self.sort_TableView registerNib:[UINib nibWithNibName:@"HeaderTwoLineTableViewCell" bundle:nil] forCellReuseIdentifier:@"HeaderTwoLineTableViewCell"];
    self.talkToexpertView.expertDelgate = self;
    [self.collection_view reloadData];
}

- (void)viewWillAppear:(BOOL)animated {
    selectedIndexPath = 1000;
    selectedSortOption = 1000;
    self.sort_View.hidden = YES;
    [self showCountOfWishListData];
    
}
- (void)viewDidAppear:(BOOL)animated {
    self.lbl_Like.layer.masksToBounds = YES;
    self.lbl_Like.layer.cornerRadius = self.lbl_Like.frame.size.height/2;
}
#pragma mark - Change Button Colour Methods
-(void)setButtonTitleAndAction:(NSArray*)buttonArray withDayNum:(NSArray*)tagArray{
    for(int i = 0; i<buttonArray.count; i++){
        UIButton *buttn = [buttonArray objectAtIndex:i];
        NSInteger tagId = [[tagArray objectAtIndex:i] integerValue];
        buttn.tag = tagId;
        buttn.titleLabel.text = [NSString stringWithFormat:@"%d Nights",(int)[tagArray objectAtIndex:i]];
        [buttn addTarget:self action:@selector(nightsButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    }
}

-(void)changeButtonColour:(NSInteger)clickIndex{
    for(int i = 0; i<buttonArray.count; i++){
        UIButton *buttn = [buttonArray objectAtIndex:i];
        [buttn setTitleColor:(buttn.tag == clickIndex)? [UIColor whiteColor]:[UIColor blackColor] forState:UIControlStateNormal];
        [buttn setBackgroundColor:(buttn.tag == clickIndex)? [ColourClass colorWithHexString:@"0053A9"]:[UIColor clearColor]];
        [buttn setBorderColor:(buttn.tag == clickIndex)? [ColourClass colorWithHexString:@"0053A9"]:[UIColor blackColor]];
    }
}
-(void) nightsButtonClicked:(UIButton*)sender
{
    NSDictionary *filetrDict = [[NSUserDefaults standardUserDefaults] objectForKey:filterOptionsDict];
    if (filetrDict != nil){
       [[NSUserDefaults standardUserDefaults]removeObjectForKey:filterOptionsDict];
    }
    self.filteredHolidayArray = [self filterByNumberOfDays:sender.tag];
    selectedIndexPath = 1000;
    if ([self.filteredHolidayArray count] == 0){
        [self showEmptyArrayPopUp];
    }
    [self.collection_view reloadData];
    
}
-(void)showEmptyArrayPopUp{
    [self showAlertWithTitle:@"Empty !" message:@"No Packages Available For Selected Duration" style:UIAlertControllerStyleAlert buttNames:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString *str) {
    }];
}

-(void)showAlertWithTitle:(NSString*)title message:(NSString*)message style:(UIAlertControllerStyle)style buttNames:(NSArray*)buttArray completion:(void (^)( NSString *))completion{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:style];
    for (NSString* key in buttArray){
        [alert addAction:[UIAlertAction actionWithTitle:key style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            completion(key);
        }]];
    }
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - Show Expert View

//- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
//{
//    [self hideTakToExpertView:YES];
//}
-(void)hideTakToExpertView:(BOOL)flag{
    [UIView transitionWithView:self.talkToexpertView duration:0.8 options:UIViewAnimationOptionTransitionFlipFromRight animations:^(void){
           self.talkToexpertView.hidden = flag;
           [self.top_optionsView setUserInteractionEnabled:flag];
       } completion:nil];
}
#pragma mark - Collection View Delegates And DataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.filteredHolidayArray count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    SRPCollectionCell *cell= (SRPCollectionCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"SRPCollectionCell" forIndexPath:indexPath];
    
    Holiday *objHoliday = [self.filteredHolidayArray objectAtIndex:indexPath.row];
    cell.srpDelgate = self;
    cell.selectedIndexNumber = selectedIndexPath;
    cell.indexNumber = indexPath.row;
    [cell showHolidayPackage:objHoliday withCurrentIndex:indexPath.row withTag:selectedIndexPath];
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout
        sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGSize sizeOfCollection = CGSizeMake(self.view.frame.size.width, 500);
    if (selectedIndexPath == indexPath.row){
            [UIView animateWithDuration:0.5f delay:0.1f options:UIViewAnimationOptionCurveEaseOut animations:^{
                [self.view layoutIfNeeded];
                [self.view layoutSubviews];
            } completion:^(BOOL finished) {
                
            }];
        return sizeOfCollection;
    }
    return CGSizeMake(self.view.frame.size.width, 360);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    Holiday *objHoliday = [self.filteredHolidayArray objectAtIndex:indexPath.row];
    
//    activityLoadingView = [LoadingView loadingViewInView:self.view.superview.superview
//           withString:@""
//    andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
//
//    [self fetchPackageDetails:objHoliday];
//
//    if ([objHoliday.strPackageType caseInsensitiveCompare:@"International"]==NSOrderedSame){
//        [FIRAnalytics logEventWithName:@"International_Holidays_View_Details" parameters:@{kFIRParameterItemName:[NSString stringWithFormat:@"PackageName :%@, PackageType :%@",objHoliday.strPackageName,objHoliday.strPackageType]}];
//    }else{
//       [FIRAnalytics logEventWithName:@"Indian_Holidays_View_Details" parameters:@{kFIRParameterItemName:[NSString stringWithFormat:@"PackageName :%@, PackageType :%@",objHoliday.strPackageName,objHoliday.strPackageType]}];
//    }
//    [payloadList setObject:@"yes" forKey:@"s^VIEW_DETAILS"];
    
    [self fetchPackageDetailsInSRPSCreen:objHoliday];
}
- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath{
}

#pragma mark - Other API Calling Methods

-(void)fetchPackageDetailsInSRPSCreen:(Holiday *)objHoliday{
    LoadingView *activityLoadingView = [LoadingView loadingViewInView:self.view.superview.superview
           withString:@""
    andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE_WHITE]];
    [self fetchPackageDetailsWithHoliday:objHoliday completionHandler:^(NSDictionary * _Nullable resPonseDict, NSMutableDictionary * _Nullable payLoadDict, NSError * _Nullable error) {
        dispatch_async(dispatch_get_main_queue(), ^(void){
            [activityLoadingView removeFromSuperview];
            if (error != nil){
                [self showAlertViewWithTitle:@"Alert" withMessage:[error localizedDescription]];
            }else if ([resPonseDict count] ==0){
                [self showAlertViewWithTitle:@"Alert" withMessage:ErrorType_toString[emptyData]];
            }else if ([resPonseDict count] > 0){
                NSArray *packageArray = (NSArray *)resPonseDict;
                NSDictionary *dictForCompletePackage = [packageArray objectAtIndex:0];
                HolidayPackageDetail *packageDetailHoliday = [[HolidayPackageDetail alloc] initWithDataDict:dictForCompletePackage];
                PDPScreenVC *controller = [[PDPScreenVC alloc] initWithNibName:@"PDPScreenVC" bundle:nil];
                controller.headerName = objHoliday.strPackageName;
                controller.holidayPackageDetailInPdp = packageDetailHoliday;
                controller.holidayObjInPdp = objHoliday;
                controller.completePackageDetail = [NSArray arrayWithObjects:dictForCompletePackage, nil];
                controller.payloadDict = payLoadDict;
                [self.navigationController pushViewController:controller animated:YES];
            }else{
                [self showAlertViewWithTitle:@"Alert" withMessage:ErrorType_toString[emptyData]];
            }
        });
    }];
    if ([objHoliday.strPackageType caseInsensitiveCompare:@"International"]==NSOrderedSame){
        [FIRAnalytics logEventWithName:@"International_Holidays_View_Details" parameters:@{kFIRParameterItemName:[NSString stringWithFormat:@"PackageName :%@, PackageType :%@",objHoliday.strPackageName,objHoliday.strPackageType]}];
    }else{
       [FIRAnalytics logEventWithName:@"Indian_Holidays_View_Details" parameters:@{kFIRParameterItemName:[NSString stringWithFormat:@"PackageName :%@, PackageType :%@",objHoliday.strPackageName,objHoliday.strPackageType]}];
    }
    [payloadList setObject:@"yes" forKey:@"s^VIEW_DETAILS"];
}
#pragma mark - TableView Delegates And DataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [tableTitle count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
     static NSString *cellIdentifier = @"HeaderTwoLineTableViewCell";
       HeaderTwoLineTableViewCell *cell = (HeaderTwoLineTableViewCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.lbl_titleOne.text = [tableTitle objectAtIndex:indexPath.row];
    cell.accessoryType = (selectedSortOption == indexPath.row) ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 35;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    selectedSortOption = indexPath.row;
    isShowSortView = !isShowSortView;
//    [self hideSortedTableview:isShowSortView];

    if (indexPath.row == 0){
        self.filteredHolidayArray = [self filterByPriceAndDaysLowToHigh:YES filterDay:NO isFilterFfomOther:NO withIndex:indexPath.row];
    }else if (indexPath.row == 1){
        self.filteredHolidayArray = [self filterByPriceAndDaysLowToHigh:NO filterDay:NO isFilterFfomOther:NO withIndex:indexPath.row];
    }else if (indexPath.row == 2){
        self.filteredHolidayArray = [self filterByPriceAndDaysLowToHigh:YES filterDay:YES isFilterFfomOther:NO withIndex:indexPath.row];
    }else if (indexPath.row == 3){
        self.filteredHolidayArray = [self filterByPriceAndDaysLowToHigh:NO filterDay:YES isFilterFfomOther:NO withIndex:indexPath.row];
    }else if (indexPath.row == 4){
        self.filteredHolidayArray = [self filterByPriceAndDaysLowToHigh:NO filterDay:YES isFilterFfomOther:YES withIndex:indexPath.row];
    }else if (indexPath.row == 5){
        self.filteredHolidayArray = [self filterByPriceAndDaysLowToHigh:NO filterDay:YES isFilterFfomOther:YES withIndex:indexPath.row];
    }else if (indexPath.row == 6){
        self.filteredHolidayArray = [self filterByPriceAndDaysLowToHigh:NO filterDay:YES isFilterFfomOther:YES withIndex:indexPath.row];
    }
    [self hideSortedTableview:isShowSortView];
}
#pragma mark - TalkToExpertView Delegates
-(void)optionClickwithIndex:(NSInteger)indexNum{
    [self hideTakToExpertView:YES];
    if (indexNum==0){
//        [self showToastsWithTitle:@"Send Query" withMessage:@"Coming Soon ..."];
        [self requestACall];
    }else if (indexNum==1){
        [self callForHolidayEnquiry];
    }else{
        NSLog(@"No any option clcik");
    }
}
-(void)requestACall{
   
    if ([self.srpHolidayPackage.strPackageType caseInsensitiveCompare:@"international"]==NSOrderedSame)
    {
        [FIRAnalytics logEventWithName:@"International_Holidays_Email_PDP" parameters:@{kFIRParameterItemID:self.srpHolidayPackage.strPackageName}];
        [FIRAnalytics logEventWithName:@"International_Holidays_Email_PDP" parameters:@{kFIRParameterItemID:self.srpHolidayPackage.strPackageType}];
    }
    else{
        [FIRAnalytics logEventWithName:@"Indian_Holidays_Email_PDP" parameters:@{kFIRParameterItemID:self.srpHolidayPackage.strPackageName}];
        [FIRAnalytics logEventWithName:@"Indian_Holidays_Email_PDP" parameters:@{kFIRParameterItemID:self.srpHolidayPackage.strPackageType}];
    }
//    RequestForCallPopUp *ReqViewController = [[RequestForCallPopUp alloc] initWithNibName:@"RequestForCallPopUp" bundle:nil];
    
//   RequestACallVC *requestVC = [[RequestACallVC alloc] initWithNibName:@"RequestACallVC" bundle:nil];
//    requestVC.requestCallHolidayPackage = self.srpHolidayPackage;
    
    RequestViewVC *requestVC = [[RequestViewVC alloc] initWithNibName:@"RequestViewVC" bundle:nil];
    requestVC.requestHolidayPackage = self.srpHolidayPackage;
    
   [self.navigationController pushViewController:requestVC animated:YES];
    
    [payloadList setObject:@"yes" forKey:@"s^EMAIL_FORM"];
    [self netCoreViewDetailPage];
}
#pragma mark - Other API Caslling Methods
-(void)fetchPackageDetailsWithHoliday:(Holiday *)objHoliday completionHandler:(void (^)(NSDictionary * _Nullable resPonseDict,NSMutableDictionary * _Nullable payLoadDict, NSError * _Nullable error))handlers{
    @try
    {
    NetworkHelper *helper = [NetworkHelper sharedHelper];
    NSString *pathParam = objHoliday.strPackageId;
    NSDictionary *headerDict = [CoreUtility getHeaderDict];
    [helper getResponseWithRequestType:@"" withQueryParam:@"" withPathParam:pathParam withJsonParam:nil withHeaders:headerDict withUrl:kAstraUrlPackageDetail success:^(NSDictionary *responseDict)
     {
         dispatch_async(dispatch_get_main_queue(), ^(void) {
         if (responseDict)
         {
             if (responseDict.count>0)
             {
                 NSArray *packageArray = (NSArray *)responseDict;
                 NSDictionary *dictForCompletePackage = [packageArray objectAtIndex:0];
                 HolidayPackageDetail *packageDetailHoliday = [[HolidayPackageDetail alloc] initWithDataDict:dictForCompletePackage];
                 [self fireAppIntHoViewDetailsEvent:packageDetailHoliday];
                 handlers(responseDict, payloadList, nil);
                 
             }
         }
          });
     }
    failure:^(NSError *error)
     {
        handlers(nil, payloadList, error);
     }];
    }
    @catch (NSException *exception){
        NSLog(@"exception catch called");
    }
    @finally{
        NSLog(@"exception finally called");
    }
}
-(void)fetchPackageDetails:(Holiday *)objHoliday
{
    @try
    {
    NetworkHelper *helper = [NetworkHelper sharedHelper];
    
    NSString *pathParam = objHoliday.strPackageId;
    
    NSDictionary *headerDict = [CoreUtility getHeaderDict];
    
    [helper getResponseWithRequestType:@"" withQueryParam:@"" withPathParam:pathParam withJsonParam:nil withHeaders:headerDict withUrl:kAstraUrlPackageDetail success:^(NSDictionary *responseDict)
     {
         dispatch_async(dispatch_get_main_queue(), ^(void) {
        [activityLoadingView removeFromSuperview];
         if (responseDict)
         {
             if (responseDict.count>0)
             {
                 NSArray *packageArray = (NSArray *)responseDict;
                 NSDictionary *dictForCompletePackage = [packageArray objectAtIndex:0];
                 NSLog(@"packageDetail  allKeys %@",[[dictForCompletePackage valueForKey:@"packageDetail"] allKeys]);
                 HolidayPackageDetail *packageDetailHoliday = [[HolidayPackageDetail alloc] initWithDataDict:dictForCompletePackage];
                 [self fireAppIntHoViewDetailsEvent:packageDetailHoliday];
                  NSLog(@"arrayOfHubList %@",packageDetailHoliday.arrayOfHubList);
                 PDPScreenVC *controller = [[PDPScreenVC alloc] initWithNibName:@"PDPScreenVC" bundle:nil];
                 controller.headerName = objHoliday.strPackageName;
                 controller.holidayPackageDetailInPdp = packageDetailHoliday;
                 controller.holidayObjInPdp = objHoliday;
                 controller.completePackageDetail = [NSArray arrayWithObjects:dictForCompletePackage, nil];
                 controller.payloadDict = payloadList;
                 [self.navigationController pushViewController:controller animated:YES];
                 
             }
         }
          });
         
     }
                               failure:^(NSError *error)
     {
         dispatch_async(dispatch_get_main_queue(), ^(void)
                        {
                            [activityLoadingView removeFromSuperview];
                            NSLog(@"Response Dict : %@",[error description]);
                        });
     }];
    }
    @catch (NSException *exception){
        [activityLoadingView removeFromSuperview];
    }
    @finally{
        NSLog(@"exception finally called");
    }
}

-(void)fireAppIntHoViewDetailsEvent:(HolidayPackageDetail *) PackageDetail
{
    NSMutableDictionary *payLoadForViewDetailsDict = [[NSMutableDictionary alloc]init];
    if((PackageDetail.strPackageName == nil) || (PackageDetail.strPackageName == NULL)){
        [payLoadForViewDetailsDict setObject:@"" forKey:@"s^DESTINATION"];
    }else{
        
        [payLoadForViewDetailsDict setObject:PackageDetail.strPackageName forKey:@"s^DESTINATION"];
    }
    [payLoadForViewDetailsDict setObject:PackageDetail.strPackageName forKey:@"s^DESTINATION"];
    [payLoadForViewDetailsDict setObject:[NSString stringWithFormat:@"%d",PackageDetail.durationNoDays] forKey:@"s^DURATION"];
    [payLoadForViewDetailsDict setObject:[NSString stringWithFormat:@"%d",PackageDetail.packagePrise] forKey:@"s^BUDGET"];
    [payLoadForViewDetailsDict setObject:PackageDetail.strPackageId forKey:@"s^PACKAGE_ID"];
    [payLoadForViewDetailsDict setObject:@"App" forKey:@"s^SOURCE"];
  
  //[payLoadForViewDetails setObject:[NSString stringWithFormat:@"%d",packageDetail.packagePrise] forKey:@"i^PACKAGE_PRICE"];//28-02-2018
    
    [payLoadForViewDetailsDict setObject:[NSNumber numberWithInt:PackageDetail.packagePrise] forKey:@"i^PACKAGE_PRICE"];//28-02-2018
    
    if ([[PackageDetail.strPackageSubType lowercaseString]isEqualToString:@"git"])
    {
        [payLoadForViewDetailsDict setObject:@"no" forKey:@"s^PERSONALIZED_TOUR"];
    }
    else
    {
      [payLoadForViewDetailsDict setObject:@"yes" forKey:@"s^PERSONALIZED_TOUR"];
    }
    
    if(PackageDetail.strPackageType != nil && ![PackageDetail.strPackageType isEqualToString:@""])
    {
        [payLoadForViewDetailsDict setObject:PackageDetail.strPackageType forKey:@"s^TYPE"];
    }
    
    [NetCoreAnalyticsVC getNetCorePushNotificationDetailDictionary:payLoadForViewDetailsDict withPayloadCount:125];
    //[NetCoreAnalyticsVC getNetCorePushNotificationDetailDictionary:payLoadForViewDetails withPayloadCount:105];
}
#pragma mark - Filtere Screen Delegates

-(void)applySelectedFilterDictionaryInSRP{
    
    NSDictionary *filetrDict = [[NSUserDefaults standardUserDefaults] objectForKey:filterOptionsDict];
    if (filetrDict != nil){
        NSArray* filteredArray = [self.holidayArrayInSrp filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id  _Nullable evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
            Holiday *obj = (Holiday*)evaluatedObject;
            return (obj.packagePrise>=[[filetrDict objectForKey:filterMinBudget]intValue] && obj.packagePrise<=[[filetrDict objectForKey:filterMaxBudget]intValue]);
        }]];
        NSArray *departureCityarray = [filetrDict objectForKey:filterDepartureCity];
        if ([departureCityarray count]>0){
            filteredArray = [filteredArray filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id  _Nullable evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
                Holiday *obj = (Holiday*)evaluatedObject;
                return [obj.arrayOfHubList filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(NSString *evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
                    return [departureCityarray containsObject:evaluatedObject];
                }]];
            }]];
        }
        NSArray *durationarray = [self replaceDurationFilterArrayByNumber:[filetrDict objectForKey:filterDuration]];;
        if ([durationarray count]>0){
            filteredArray = [filteredArray filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id  _Nullable evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
                Holiday *obj = (Holiday*)evaluatedObject;
                if ([durationarray containsObject:@"7"]){
                    return obj.durationNoDays<7;
                }else if ([durationarray containsObject:@"8"]){
                    return (obj.durationNoDays>=8 && obj.durationNoDays<=12);
                }
                return obj.durationNoDays>12;
            }]];
        }
        NSArray *filterThemearray = [filetrDict objectForKey:filterTheme];
        if ([filterThemearray count]>0){
            filteredArray = [filteredArray filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id  _Nullable evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
                Holiday *obj = (Holiday*)evaluatedObject;
                return [obj.arrayOfThemeList filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(NSString *evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
                    return [filterThemearray containsObject:evaluatedObject];
                }]];
            }]];
        }
        if ([[filetrDict objectForKey:filterByGroup] integerValue]==1){
            filteredArray = [filteredArray filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id  _Nullable evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
                Holiday *obj = (Holiday*)evaluatedObject;
                return [obj.strPackageSubType isEqualToString:@"GIT"];
            }]];
        }
        self.filteredHolidayArray = filteredArray;
        [self changeButtonColour:1];
    }else{
        self.filteredHolidayArray = [self filterByNumberOfDays:5];
    }
    [self.collection_view reloadData];
    
}
-(NSArray*)replaceDurationFilterArrayByNumber:(NSArray*)strArray{
    NSMutableArray *array = [[NSMutableArray alloc] init];
    if ([strArray containsObject:@"Less than 7 nights"])
        [array addObject:@"7"];
    if ([strArray containsObject:@"8 to 12 nights"])
        [array addObject:@"8"];
    if ([strArray containsObject:@"More than 12 nights"])
        [array addObject:@"12"];
    return [array copy];
}

#pragma mark - New Filter ScrVC Delegates

-(void)applySelectedNewFilterInSRP{
    NSLog(@"Before Filteration %ld",[self.holidayArrayInSrp count]);
    NSDictionary *filetrDict = [[NSUserDefaults standardUserDefaults] objectForKey:filterOptionsDict];
    if (filetrDict != nil && [filetrDict count]!=0){
        //        int startPrize = ([filetrDict objectForKey:filterMinBudget]) ? [[filetrDict objectForKey:filterMinBudget]intValue] : 0;
        //        int endPrize = ([filetrDict objectForKey:filterMaxBudget]) ? [[filetrDict objectForKey:filterMaxBudget]intValue] : 10000000;
        NSArray* filteredArray = [self.holidayArrayInSrp filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id  _Nullable evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
            Holiday *obj = (Holiday*)evaluatedObject;
            if ([filetrDict objectForKey:filterMinBudget]){
                int startPrize = ([[filetrDict objectForKey:filterMinBudget] intValue]);
                return obj.packagePrise>=startPrize;
            }
            return YES;
        }]];
        filteredArray = [filteredArray filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id  _Nullable evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
            Holiday *obj = (Holiday*)evaluatedObject;
            if ([filetrDict objectForKey:filterMaxBudget]){
                int endPrize = ([[filetrDict objectForKey:filterMaxBudget] intValue]);
                return obj.packagePrise<=endPrize;
            }
            return YES;
        }]];
        NSString *deptCityStr = [filetrDict objectForKey:filterDepartureCity];
        if (deptCityStr){
            filteredArray = [filteredArray filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id  _Nullable evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
                Holiday *obj = (Holiday*)evaluatedObject;
//                return [obj.arrayOfHubList filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(NSString *evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
//                    return [[deptCityStr uppercaseString] isEqualToString:[evaluatedObject uppercaseString]];
//                }]];
                return [obj.arrayOfCityList containsObject:[NSString stringWithFormat:@"%@",deptCityStr]];
            }]];
        }
        int duration = [[filetrDict objectForKey:filterDuration]intValue];
        if (duration > 0){
            filteredArray = [filteredArray filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id  _Nullable evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
                Holiday *obj = (Holiday*)evaluatedObject;
                if (duration == 7){
                    return obj.durationNoDays<7;
                }else if (duration == 8){
                    return (obj.durationNoDays>=8 && obj.durationNoDays<=12);
                }
                return obj.durationNoDays>12;
            }]];
        }
        NSString *filterThemeStr = [filetrDict objectForKey:filterTheme];
        if (filterThemeStr){
            filteredArray = [filteredArray filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id  _Nullable evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
                Holiday *obj = (Holiday*)evaluatedObject;
                //                return [obj.arrayOfThemeList filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(NSString *evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
                //                    return [filterThemeStr isEqualToString:evaluatedObject];
                //                }]];
                
                return [obj.arrayOfThemeList containsObject:[NSString stringWithFormat:@"%@",filterThemeStr]];
            }]];
        }
        if ([[filetrDict objectForKey:filterByGroup] isEqualToString:@"YES"]){
            filteredArray = [filteredArray filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id  _Nullable evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
                Holiday *obj = (Holiday*)evaluatedObject;
                return [obj.strPackageSubType isEqualToString:@"GIT"];
            }]];
        }
        if ([[filetrDict objectForKey:filterByAirInclusive] isEqualToString:@"YES"]){
            filteredArray = [filteredArray filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id  _Nullable evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
                Holiday *obj = (Holiday*)evaluatedObject;
                return obj.airFlag;
            }]];
        }
        if ([[filetrDict objectForKey:filterByStay] isEqualToString:@"YES"]){
            filteredArray = [filteredArray filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id  _Nullable evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
                Holiday *obj = (Holiday*)evaluatedObject;
                return obj.accomFlag;
            }]];
        }
        if ([[filetrDict objectForKey:filterByCustomized] isEqualToString:@"YES"]){
            filteredArray = [filteredArray filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id  _Nullable evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
                Holiday *obj = (Holiday*)evaluatedObject;
                return ([[obj.strPackageSubType uppercaseString] isEqualToString:@"FIT FIXED"] || [[obj.strPackageSubType uppercaseString] isEqualToString:@"FIT"]);
            }]];
        }
        if ([filetrDict objectForKey:filterTravelMonth]){
            
            NSDate *filterMonthDate = [filetrDict objectForKey:filterTravelMonth];
                NSDateFormatter *strDateFormat = [[NSDateFormatter alloc] init];
                [strDateFormat setDateFormat:@"MMMM yyyy"];
                NSString *strDates = [strDateFormat stringFromDate:filterMonthDate];
            NSPredicate *monthTravelPred = [NSPredicate predicateWithBlock:^BOOL(Holiday* evaluatedObject, NSDictionary *bindings){
                NSArray *datesArray = evaluatedObject.tcilHolidayTravelmonthCollection;
                if([[evaluatedObject.strPackageSubType lowercaseString] isEqualToString:@"git"]||[[evaluatedObject.strPackageSubType lowercaseString] isEqualToString:@"fit fixed"]){
                    return true;
                }
                if (datesArray){
                    if (datesArray.count >0){
                        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
                        [formatter setDateFormat:@"MMMM yyyy"];
                        NSString *strDateFromUser = strDates;
//                        NSString *strDateFromUser = filterMonths;
                        NSDate *givenDate = [formatter dateFromString:strDateFromUser];
                        
                        NSDateFormatter *dateFormatForMonth = [[NSDateFormatter alloc]init];
                        [dateFormatForMonth setDateFormat:@"MMM"];
                        NSString *givenMonthString = [dateFormatForMonth stringFromDate:givenDate];
                        
                        
                        NSDateFormatter *dateFormatForYear = [[NSDateFormatter alloc]init];
                        [dateFormatForYear setDateFormat:@"yyyy"];
                        NSString *givenYearString = [dateFormatForYear stringFromDate:givenDate];
                        
                        NSPredicate *predicateForDateAvail =  [NSPredicate predicateWithFormat:@"monthCode CONTAINS[cd] %@ AND monthYear == %d", givenMonthString, [givenYearString intValue]];
                        
                        NSArray *filteredArrayForDates = [datesArray filteredArrayUsingPredicate:predicateForDateAvail];
                        
                        if (filteredArrayForDates)
                        {
                            if (filteredArrayForDates.count >0)
                            {
                                return true;
                            }
                        }
                        
                    }
                }
                return false;
            }];
            filteredArray = [filteredArray filteredArrayUsingPredicate:monthTravelPred];
            
        }
        self.filteredHolidayArray = filteredArray;
        [self changeButtonColour:1];
    }else{
        selectedNightFilter = @"1 Nights";
        self.filteredHolidayArray = [self filterByNumberOfDays:1];
    }
    NSLog(@"After Filteration %ld",[self.filteredHolidayArray count]);
    [self.collection_view reloadData];
    
}
-(int)replaceDurationWithNumber:(NSString*)string{
    if ([string isEqualToString:@"Less than 7 nights"]){
        return 7;
    }else if ([string isEqualToString:@"8 to 12 nights"]){
        return 8;
    }else if ([string isEqualToString:@"More than 12 nights"]){
        return 12;
    }
    return 0;
}
#pragma mark - Other Filtered Methods In UIViewController

-(NSArray*)filterByNumberOfDays:(NSInteger)numOfDays{
    NSString *previousNightStr = selectedNightFilter;
    NSString *currentNightStr = [NSString stringWithFormat:@"%ld Nights",numOfDays];
    BOOL flag = [previousNightStr isEqualToString:currentNightStr];
    [self changeButtonColour:flag ? 1 : numOfDays];
    selectedNightFilter = flag ? @"" : currentNightStr;
    NSArray* filteredArray = [self.holidayArrayInSrp filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id  _Nullable evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
        Holiday *obj = (Holiday*)evaluatedObject;
        if (numOfDays == 6){
           return ((obj.durationNoDays-1) == 6);
        }else if (numOfDays == 8){
           return ((obj.durationNoDays-1)>=6 && (obj.durationNoDays-1)<=(int)numOfDays);
        }else if (numOfDays == 10){
           return (obj.durationNoDays-1)>=(int)numOfDays;
        }
        return (obj.durationNoDays-1)<=(int)numOfDays;
    }]];
    return flag ? self.holidayArrayInSrp : filteredArray;
}
-(NSArray*)filterByPriceAndDaysLowToHigh:(BOOL)flags filterDay:(BOOL)isDayFilter isFilterFfomOther:(BOOL)isOtherFlag withIndex:(NSInteger)sortIndex{
    [self changeButtonColour:1];
    if (isOtherFlag){
    NSArray* filteredArray = [self.holidayArrayInSrp filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id  _Nullable evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
        Holiday *obj = (Holiday*)evaluatedObject;
        if (sortIndex == 4){
            return [obj.stringPopularFlag isEqualToString:@"Y"];
        }else if (sortIndex == 5){
//            return [obj.Offers isEqualToString:@"Y"];
            return (obj.Offers.length > 0);
        }
        return [obj.stringBookingOnline isEqualToString:@"Y"];
    }]];
    return filteredArray;
    }
    NSArray* filteredArray = [self.holidayArrayInSrp sortedArrayUsingComparator:^NSComparisonResult(Holiday* obj1, Holiday* obj2) {
        
        if (isDayFilter){
            if (obj1.durationNoDays > obj2.durationNoDays) {
                return (flags) ? (NSComparisonResult)NSOrderedDescending : (NSComparisonResult)NSOrderedAscending;
            }
            if (obj1.durationNoDays < obj2.durationNoDays) {
                return (flags) ? (NSComparisonResult)NSOrderedAscending : (NSComparisonResult)NSOrderedDescending;
            }
        }else{
        if (obj1.packagePrise > obj2.packagePrise) {
            return (flags) ? (NSComparisonResult)NSOrderedDescending : (NSComparisonResult)NSOrderedAscending;
        }
        if (obj1.packagePrise < obj2.packagePrise) {
            return (flags) ? (NSComparisonResult)NSOrderedAscending : (NSComparisonResult)NSOrderedDescending;
        }
        }
        return (NSComparisonResult)NSOrderedSame;
    }];
    return filteredArray;
}

#pragma mark - SRPCollection Delegate

-(void)setSelectedIndex:(UICollectionViewCell *)cell withTag:(NSInteger)tagNum{
    NSIndexPath *indexPath = [self.collection_view indexPathForCell:cell];
//    selectedIndexPath =indexPath.row;
    selectedIndexPath = (tagNum == 1000) ? 1000 : indexPath.row;
    [self.collection_view reloadData];
}
-(void)downButtonClick:(SRPCollectionCell *)cell{
    NSInteger addValue = ([cell.siteArray count]/4);
    cell.const_lblDestination.constant = 12 + (addValue*12);
}
-(void)likeDislikePressInCell:(UICollectionViewCell *)cell withFlag:(BOOL)flag{
    [self showToastsWithTitle:(flag) ? @" Added In Wishlist " : @" Removed From Wishlist "];
    [self.collection_view reloadData];
    [self showCountOfWishListData];
}
-(void)showCountOfWishListData{
    NSArray *array = [[CoreDataSingleton sharedInstance] getArrayOfObjectForEntityName:WishListHoliday];
    [self.btn_Like setImage:[UIImage imageNamed:([array count]>0) ? @"heartSelect" : @"heart"] forState:UIControlStateNormal];
    self.lbl_Like.hidden = ([array count]==0);
    self.lbl_Like.text = [NSString stringWithFormat:@"%ld",[array count]];
}
- (void)offersButtonClick:(UICollectionViewCell *)cell withHoliday:(Holiday *)hObject{
    OffersDetailsVC *viewVC = [[OffersDetailsVC alloc] init];
    viewVC.holidayInOffers = hObject;
    viewVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:viewVC animated:YES completion:nil];
}
#pragma mark - Button Actions

- (IBAction)btnaction_Header:(id)sender {
    // Removing Filter Info From UserDefaults 
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:filterOptionsDict];
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)btn_talkToExpertPress:(id)sender
{
    [self hideTakToExpertView:NO];
}
- (IBAction)btnaction_Like:(id)sender{
    WishListScreenVC *controller = [[WishListScreenVC alloc] initWithNibName:@"WishListScreenVC" bundle:nil];
    [self.navigationController pushViewController:controller animated:YES];
}
- (IBAction)btnaction_SortArrow:(id)sender{
    isShowSortView = !isShowSortView;
    selectedSortOption = (!isShowSortView) ? 1000 : selectedSortOption;
    if (!isShowSortView){
        self.filteredHolidayArray = [self filterByNumberOfDays:5];
    }
    [self hideSortedTableview:isShowSortView];
}
-(void)hideSortedTableview:(BOOL)flag{
    
    [UIView transitionWithView:self.sort_View duration:0.8 options:UIViewAnimationOptionTransitionCurlDown animations:^(void){
        self.sort_View.hidden = (flag) ? NO : YES;
        self.cnst_heightOfSortView.constant = (flag) ? 230 : 0;
    } completion:nil];
    [self.sort_TableView reloadData];
    selectedIndexPath = 1000;
    [self.collection_view reloadData];
}
- (IBAction)btnaction_Filter:(id)sender{
//    FilterScreenVC *controller = [[FilterScreenVC alloc] initWithNibName:@"FilterScreenVC" bundle:nil];
//    controller.holidayArrayInFilter = self.holidayArrayInSrp;
//    controller.filterDelgate = self;
    FilterSCrView *controller = [[FilterSCrView alloc] initWithNibName:@"FilterSCrView" bundle:nil];
    controller.holidayArrayInNewFilter = [self.holidayArrayInSrp mutableCopy];
    controller.filterDelgate = self;
    [self.navigationController pushViewController:controller animated:YES];
}
-(void)netCoreViewDetailPage
{
    if (![payLoadViewPageDetail objectForKey:@"s^OVERVIEW"])
    {
        [payLoadViewPageDetail setObject:@"no" forKey:@"s^OVERVIEW"];
    }
    if (![payLoadViewPageDetail objectForKey:@"s^DETAILS"])
    {
        [payLoadViewPageDetail setObject:@"no" forKey:@"s^DETAILS"];
    }
    
    if (![payLoadViewPageDetail objectForKey:@"s^ITINERARY"])
    {
         [payLoadViewPageDetail setObject:@"no" forKey:@"s^ITINERARY"];
    }
    
    //28-02-2018
    /*if(![payLoadViewPageDetail objectForKey:@"s^BOOK_NOW"])
    {
        [payLoadViewPageDetail setObject:@"no" forKey:@"s^BOOK_NOW"];
    }*/
    
    if(![payLoadViewPageDetail objectForKey:@"s^EMAIL_FORM"])
    {
        [payLoadViewPageDetail setObject:@"no" forKey:@"s^EMAIL_FORM"];
    }
    
    if(![payLoadViewPageDetail objectForKey:@"s^CALL"])
    {
        [payLoadViewPageDetail setObject:@"no" forKey:@"s^CALL"];
    }
    
    [payLoadViewPageDetail setObject:self.srpHolidayPackage.strPackageId forKey:@"s^PACKAGE_ID"];
    [payLoadViewPageDetail setObject:@"App" forKey:@"s^SOURCE"];
    [payLoadViewPageDetail setObject:[NSNumber numberWithInt:self.srpHolidayPackage.packagePrise] forKey:@"i^PACKAGE_PRICE"];
    
    if ([self.completePackageDetail count]>0)
    {
        NSDictionary *packageDict = [[NSDictionary alloc] init];
        packageDict = [self.completePackageDetail objectAtIndex:0];
        
        NSString *packageType = [packageDict objectForKey:@"pkgType"];
        NSArray *inclusionArray = [packageDict objectForKey:@"inclusionList"];
        NSArray *excludeArray = [packageDict objectForKey:@"excludeList"];
        NSString *thingsNote = [packageDict objectForKey:@"thingsNote"];
        
        
         [payLoadViewPageDetail setObject:@"no" forKey:@"s^INCLUSION"];
        
        if (inclusionArray)
        {
            if (inclusionArray.count !=0)
            {
                [payLoadViewPageDetail setObject:@"yes" forKey:@"s^INCLUSION"];
            }
        }
        
        [payLoadViewPageDetail setObject:@"no" forKey:@"s^EXCLUSION"];
        
        if (excludeArray)
        {
            if (excludeArray.count !=0)
            {
                [payLoadViewPageDetail setObject:@"yes" forKey:@"s^EXCLUSION"];
            }
        }

        if (thingsNote != nil && ![thingsNote isEqualToString:@""])
        {
            [payLoadViewPageDetail setObject:thingsNote forKey:@"s^THINGS_TO_NOTE"];//28-02-2018
        }
        
        [payLoadViewPageDetail setObject:@"" forKey:@"s^THINGS"];//28-02-2018
        
        
        if (packageType != nil && ![packageType isEqualToString:@""])
        {
            [payLoadViewPageDetail setObject:packageType forKey:@"s^TYPE"];//28-02-2018
        }
        
    }
    
//     [NetCoreAnalyticsVC getNetCorePushNotificationDetailDictionary:payLoadViewPageDetail withPayloadCount:106];
    
    [NetCoreAnalyticsVC getNetCorePushNotificationDetailDictionary:payLoadViewPageDetail withPayloadCount:126]; //28-02-2018
}
@end
