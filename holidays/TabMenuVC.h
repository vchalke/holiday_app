//
//  TabMenuVC.h
//  holidays
//
//  Created by Pushpendra Singh on 12/08/15.
//  Copyright (c) 2015 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "Holiday.h"
#import "HolidayPackageDetail.h"
#import "LoadingView.h"
#import "MobileNumberPopUpViewController.h"
#import "CustomIOSAlertView.h"


@interface TabMenuVC : BaseViewController<MobileNumberVCDelegate,dismissView,CustomIOSAlertViewDelegate>
{
    LoadingView *activityLoadingView;
}

@property (strong, nonatomic) IBOutlet UIView *TabMenuView;
@property (strong,nonatomic) NSString *headerName;
@property (weak,nonatomic) NSString *flag;
@property (strong,nonatomic)NSArray *holidayArray;
@property (strong,nonatomic) NSString *searchedCity;
@property(strong,nonatomic)NSString *totalPackages;
@property (strong,nonatomic) NSDictionary *filterDict;
@property (strong ,nonatomic) HolidayPackageDetail *packageDetail;
@property(strong,nonatomic)UILabel *totalPackageLabel;
@property (nonatomic, assign) int pageNumber;
@property (strong,nonatomic) NSArray * completePackageDetail;
- (IBAction)onBookNowButtonClicked:(id)sender;
- (IBAction)onAddToWishListButtonClicked:(id)sender;
- (IBAction)onEmailButtonClicked:(id)sender;
- (IBAction)onCallUsButtonClicked:(id)sender;
- (IBAction)onShareButtonClicked:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *buttonCall;
@property (weak, nonatomic) IBOutlet UIButton *buttonMail;
@property (weak, nonatomic) IBOutlet UIButton *buttonLike;
@property (weak, nonatomic) IBOutlet UIButton *buttonShare;
@property (weak, nonatomic) IBOutlet UIButton *buttonBookNow;
@property (weak, nonatomic) IBOutlet UIScrollView *tabScrollView;

@property (weak, nonatomic) IBOutlet UIView *containerView;

-(void)setLabel:(NSString*)PackageCount;
@property (strong,nonatomic) NSMutableDictionary *payloadDict;



@end
