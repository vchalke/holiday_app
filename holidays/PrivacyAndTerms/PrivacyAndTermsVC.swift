//
//  PrivacyAndTermsVC.swift
//  holidays
//
//  Created by Kush_Team on 18/06/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

import UIKit

class PrivacyAndTermsVC: UIViewController {

    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var tableTxtViews: UITextView!
    @objc var fileStr  = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lbl_title.text = fileStr
        if (fileStr == "Privacy Policy"){
             self.tableTxtViews.text = load(file: "PrivacyPolicy")
        }else{
             self.tableTxtViews.text = load(file: "PrivacyPolicy")
        }
    }


    func load(file name:String) -> String {

        if let path = Bundle.main.path(forResource: name, ofType: "") {

            if let contents = try? String(contentsOfFile: path) {

                return contents

            } else {

                print("Error! - This file doesn't contain any text.")
            }

        } else {

            print("Error! - This file doesn't exist.")
        }

        return ""
    }
    
    @IBAction func btn_backPress(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
}
