//
//  CalculateDictObject.h
//  holidays
//
//  Created by Kush_Tech on 09/04/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CalculateDictObject : NSObject
-(instancetype)initWithCalculateObjectDict:(NSDictionary *)dictionary;

@property(nonatomic)NSInteger adultDiscount;
@property(nonatomic)NSInteger adultPrice;

@property(nonatomic)NSInteger advancePayableAmount;
@property(nonatomic,strong)NSArray *currencyBreakup;
@property(nonatomic)NSInteger amountOne;
@property(nonatomic)float currencyRateOne;
@property(nonatomic,strong)NSString *currencycodeOne;
@property(nonatomic)NSInteger amountTwo;
@property(nonatomic)float currencyRateTwo;
@property(nonatomic,strong)NSString *currencycodeTwo;

@property(nonatomic,strong)NSDictionary *discountResponseBean;
@property(nonatomic)NSInteger discAmont;
@property(nonatomic,strong)NSString *discCode;


@property(nonatomic,strong)NSArray *advanceDiscountGSTDescription;
@property(nonatomic,strong)NSArray *fullDiscountGSTDescription;
@property(nonatomic,strong)NSArray *gSTDescription;

@property(nonatomic)NSInteger totalPrice;
@property(nonatomic)NSInteger totalServicetax;
@property(nonatomic)NSInteger totalServicetaxAdvanceDiscount;
@property(nonatomic)NSInteger totalServicetaxFullDiscount;
@property(nonatomic)NSInteger totaltourCost;

@end

NS_ASSUME_NONNULL_END
