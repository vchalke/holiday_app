//
//  ViewMoreOtherVC.m
//  holidays
//
//  Created by Ios_Team on 04/09/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "ViewMoreOtherVC.h"
#import "HolidayAccomodationObject.h"
#import "ChildHotelTableViewCell.h"
#import "SightSeeingTableViewCell.h"
#import "SightSeenObject.h"

@interface ViewMoreOtherVC ()
{
    NSMutableArray *hotelObjectsArray;
    NSMutableArray *sightObjectsArray;
}
@end

@implementation ViewMoreOtherVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    [self.base_table_view registerNib:[UINib nibWithNibName:@"ItinenaryDetailTableViewCell" bundle:nil] forCellReuseIdentifier:@"ItinenaryDetailTableViewCell"];
    self.lbl_Title.text = self.sectionTitLe;
    if ([[self.sectionTitLe uppercaseString]isEqualToString:@"HOTELS"]){
        [self setHotelsData];
    }else{
        [self setSightseeingData];
    }
    
}
-(void)setHotelsData{
    [self.tbl_OtherViews registerNib:[UINib nibWithNibName:@"ChildHotelTableViewCell" bundle:nil] forCellReuseIdentifier:@"ChildHotelTableViewCell"];
    NSMutableString *mutString = [[NSMutableString alloc]init];
    hotelObjectsArray = [[NSMutableArray alloc]init];
    NSSortDescriptor *brandDescriptor = [[NSSortDescriptor alloc] initWithKey:@"position" ascending:YES];
        NSArray *sortDescriptors = [NSArray arrayWithObject:brandDescriptor];
    NSArray *filteredArray = [self.packageModel.hotelCollectionArray sortedArrayUsingDescriptors:sortDescriptors];
    for (id object in filteredArray) {
        HolidayAccomodationObject *hotelObj = [[HolidayAccomodationObject alloc]initWithAccomodationDict:object];
        if (hotelObj.packageClassId == self.holidayPkgModelInViewMore.mainPkgSubClassId){
            [hotelObjectsArray addObject:hotelObj];
        }
    }
    if ([hotelObjectsArray count]>0){
        [self.tbl_OtherViews reloadData];
        self.webViews.hidden = YES;
    }else {
        if (self.holidayPkgModelInViewMore.hotelDefaultMsg.length > 2){
            [mutString appendString:[NSString stringWithFormat:@"%@\n",self.holidayPkgModelInViewMore.hotelDefaultMsg]];
        }else{
            for (NSDictionary *object in self.holidayPkgModelInViewMore.hotelCollection) {
                NSInteger pkgClasId = [[object objectForKey:@"packageClassId"] integerValue];
                if (pkgClasId == self.holidayPkgModelInViewMore.mainPkgSubClassId){
                    [mutString appendString:[NSString stringWithFormat:@"%@\n",object[@"typeDefaultMsg"]]];
                }
            }
            for (NSDictionary *object in self.holidayPkgModelInViewMore.accombdationCollection) {
                NSInteger pkgClasId = [[object objectForKey:@"packageClassId"] integerValue];
                if (pkgClasId == self.holidayPkgModelInViewMore.mainPkgSubClassId){
                    [mutString appendString:[NSString stringWithFormat:@"%@\n",object[@"typeDefaultMsg"]]];
                }
            }
        }
        [self showStringInWebView:mutString];
    }
}
-(void)setSightseeingData{
    [self.tbl_OtherViews registerNib:[UINib nibWithNibName:@"SightSeeingTableViewCell" bundle:nil] forCellReuseIdentifier:@"SightSeeingTableViewCell"];
    NSMutableString *mutString = [[NSMutableString alloc]init];
    sightObjectsArray = [[NSMutableArray alloc]init];
    NSArray *filteredArray = self.packageModel.sightseenCollectionArray;
    for (id object in filteredArray) {
        SightSeenObject *sightObj = [[SightSeenObject alloc]initWithSightSeenObjectDict:object];
        if (sightObj.packageClassId == self.holidayPkgModelInViewMore.mainPkgSubClassId){
            [sightObjectsArray addObject:sightObj];
        }
    }
    if ([sightObjectsArray count]>0){
        [self.tbl_OtherViews reloadData];
        self.webViews.hidden = YES;
    }else {
        if (self.holidayPkgModelInViewMore.isSightseeingDefaultMsg.length > 2){
            [mutString appendString:[NSString stringWithFormat:@"%@\n",self.holidayPkgModelInViewMore.isSightseeingDefaultMsg]];
        }else{
            for (NSDictionary *object in self.holidayPkgModelInViewMore.sightseenCollection) {
                NSInteger pkgClasId = [[object objectForKey:@"packageClassId"] integerValue];
                if (pkgClasId == self.holidayPkgModelInViewMore.mainPkgSubClassId){
                    [mutString appendString:[NSString stringWithFormat:@"%@\n",object[@"typeDefaultMsg"]]];
                }
            }
        }
        [self showStringInWebView:mutString];
    }
}
-(void)showStringInWebView:(NSString*)strings{
    NSString *htmlStringObj = [NSString stringWithFormat:@"<span style=\"font-family: Lato-Regular; font-size: 15\">%@</span>", strings];
    [self.webViews loadHTMLString:[htmlStringObj stringByReplacingOccurrencesOfString:@"\n" withString:@"<br/>"] baseURL:nil];
    
}
- (IBAction)btn_backPress:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableView DataSource Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//      return hotelObjectsArray.count;
    if ([[self.sectionTitLe uppercaseString]isEqualToString:@"HOTELS"]){
        return hotelObjectsArray.count;
    }else{
        return sightObjectsArray.count;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([[self.sectionTitLe uppercaseString]isEqualToString:@"HOTELS"]){
        ChildHotelTableViewCell* cell = (ChildHotelTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"ChildHotelTableViewCell"];
        [cell loadDataInChildFromDetailModel:[hotelObjectsArray objectAtIndex:indexPath.row] withHideAddess:NO];
        return cell;
    }
    SightSeeingTableViewCell* cell = (SightSeeingTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"SightSeeingTableViewCell"];
    [cell loadDSightSeeingFromDetailModel:[sightObjectsArray objectAtIndex:indexPath.row]];
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    if ([[self.sectionTitLe uppercaseString]isEqualToString:@"HOTELS"]){
        return 130;
    }
    return 70;
    
}

@end
