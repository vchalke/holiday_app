//
//  BranchDetailViewController.swift
//  holidays
//
//  Created by Komal Katkade on 11/30/17.
//  Copyright © 2017 Mobicule. All rights reserved.
//

import UIKit

class BranchDetailViewController: ForexBaseViewController,UITextFieldDelegate,GetAddressViaSmsPopUpViewControllerDelegate
{
    @IBOutlet weak var saveAsPreferredBrancheRadioButton: UIButton!
    @IBOutlet weak var getAddrsViaSMSRadioButton: UIButton!
    @IBOutlet weak var textfieldAddress: UITextView!
    @IBOutlet weak var textfieldBranch: UITextField!
    @IBOutlet weak var textfieldCity: UITextField!
    @IBOutlet var branchDetailView: UIView!
    @IBOutlet weak var labelTotalEncashmentAmount: UILabel!
    var arrayForCity : [NSDictionary] = [];
    var arrayForBranch : [NSDictionary] = [];
    var branchAddress : NSString = ""
    var branchId : Int?
    var sellForexBO : SellForexBO =  SellForexBO.init()
    var cityCode:String = ""
    
    var lattitude : Double  =  0.00;
    var longitude : Double  =  0.00;
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        Bundle.main.loadNibNamed("BranchDetailViewController", owner: self, options: nil)
        
        super.addViewInBaseView(childView: self.branchDetailView)
        super.setUpHeaderLabel(labelHeaderNameText: "Sell Forex")
        super.viewDidLoad()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ForexBuyPassangerViewController.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        textfieldCity.delegate = self
        textfieldBranch.delegate = self
        fetchCustomerBranch()
    
        labelTotalEncashmentAmount.text = sellForexBO.totalAmountWithTax
        
        saveAsPreferredBrancheRadioButton.isSelected = false
        getAddrsViaSMSRadioButton.isSelected = false
        
        
        self.bindPreferdBranch()
        self.addDoneButtonOnKeyboard()
    }
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle       = UIBarStyle.default
        let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(BranchDetailViewController.doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.textfieldCity.inputAccessoryView = doneToolbar
        self.textfieldBranch.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction()
    {
        self.textfieldCity.resignFirstResponder()
        self.textfieldBranch.resignFirstResponder()
    }
    
    override func viewDidLayoutSubviews()
    {
        super.setSubViewFrame(childView: self.branchDetailView)
    }
    
    override func viewWillLayoutSubviews()
    {
        super.setSubViewFrame(childView: self.branchDetailView)
    }
    
    func dismissKeyboard()
    {
        view.endEditing(true)
    }
    
    func validateFields() -> Bool
    {
        if (textfieldCity.text?.count)! < 3
        {
            showAlert(message: "Please Select Valid City")
            return false
        }
        if (textfieldBranch.text?.count)! < 3
        {
            showAlert(message: "Please Select Valid Branch")
            return false
        }
        return true
    }
    
    // MARK: textfield delegates
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == textfieldCity
        {
            self.textfieldAddress.text = ""
            self.textfieldBranch.text = ""
            self.branchAddress = ""
            self.branchId = nil
            self.arrayForBranch = []
            self.lattitude = 0.00
            self.longitude = 0.00
            
        }else if textField == textfieldBranch
        {
            self.textfieldAddress.text = ""
             self.textfieldBranch.text = ""
            self.branchId = nil
            self.lattitude = 0.00
            self.longitude = 0.00
            
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField == textfieldCity
        {
            let totalString  = "\(textField.text ?? "")\(string)"
            
            if totalString.count > 2
            {
                if !string.isEmpty
                {
                    textField.resignFirstResponder()
                    let filteredArray = arrayForCity.filter
                    {
                        guard let dictionary = $0 as? [String: Any],
                            let name  = dictionary["cityName"] as? String else
                        {
                            return false
                        }
                        return name.localizedCaseInsensitiveContains(totalString)
                    }
                    
                    if filteredArray.isEmpty
                    {
                        showAlert(message: "Please Enter Valid City")
                    }
                    else
                    {
                        let alertController : UIAlertController  = UIAlertController.init(title: "Customer City", message: "Select Customer City", preferredStyle:.actionSheet)
                        
                        for dict in filteredArray
                        {
                            let productname = dict.object(forKey: "cityName")
                            let cityCode = dict.object(forKey: "cityCode")
                            self.cityCode = (cityCode as! NSString) as String
                            
                            let  alertAction: UIAlertAction = UIAlertAction.init(title: productname as? String, style: .default, handler:
                            {
                                
                                (alert: UIAlertAction!) -> Void in
                                
                                textField.text = productname as? String
                                self.fetchBranchDetails(withCityCode: cityCode as! NSString)
                                
                            })
                            
                            alertController.addAction(alertAction)
                        }
                        
                        let  alertCancel: UIAlertAction = UIAlertAction.init(title: "Cancel" , style: .cancel, handler:
                        {
                            
                            (alert: UIAlertAction!) -> Void in
                            //textField.text = productname as? String
                        })
                        
                        alertController.addAction(alertCancel)
                        self.present(alertController, animated:true, completion: nil)
                    }
                }
            }
            
        }
        else if textField == textfieldBranch
        {
            let totalString  = "\(textField.text ?? "")\(string)"
            
            if totalString.count > 2
            {
                if !string.isEmpty
                {
                    textField.resignFirstResponder()
                    let filteredArray = arrayForBranch.filter
                    {
                        guard let dictionary = $0 as? [String: Any],
                            let name  = dictionary["branchName"] as? String else
                        {
                            return false
                        }
                        return name.localizedCaseInsensitiveContains(totalString)
                    }
                    
                    if filteredArray.isEmpty
                    {
                        showAlert(message: "Please Enter Valid Branch")
                    }
                    else
                    {
                        let alertController : UIAlertController  = UIAlertController.init(title: "Customer Branch", message: "Select Customer Branch", preferredStyle:.actionSheet)
                        
                        for dict in filteredArray
                        {
                            
                            //                        "address1":"hgfhgf",
                            //                        "address2":"hgfhgfh",
                            //                        "alternateNo":"9879789789789",
                            //                        "branchCode":"14",
                            //                        "branchId":11,
                            //                        "branchName":"Borivali Manek Nagar",
                            //                        "city":"Mumbai",
                            //                        "landline":7567567567567,
                            //                        "latitude":19.217907,
                            //                        "longitude":72.847084,
                            //                        "mobile":9978987978,
                            //                        "state":"Maharashtra"
                            
                            
                            let productname = dict.object(forKey: "branchName")
                            let branchAddress = "\(dict.object(forKey: "address1") ?? "") \(dict.object(forKey: "address2") ?? "") \(dict.object(forKey: "branchName") ?? "") \(dict.object(forKey: "city") ?? "")"
                            
                            let branchLattitude = dict.object(forKey: "latitude")
                            let branchLongitude = dict.object(forKey: "longitude")
                            
                            let  alertAction: UIAlertAction = UIAlertAction.init(title: productname as? String, style: .default, handler:
                            {
                                (alert: UIAlertAction!) -> Void in
                                textField.text = productname as? String
                                self.textfieldAddress.text = branchAddress
                                self.branchAddress = branchAddress as NSString
                                self.branchId = dict.object(forKey: "branchId") as? Int
                                
                                self.lattitude = branchLattitude as! Double
                                self.longitude = branchLongitude as! Double
                            })
                            alertController.addAction(alertAction)
                        }
                        
                        let  alertCancel: UIAlertAction = UIAlertAction.init(title: "Cancel" , style: .cancel, handler:
                        {
                            (alert: UIAlertAction!) -> Void in
                            //textField.text = productname as? String
                        })
                        alertController.addAction(alertCancel)
                        self.present(alertController, animated:true, completion: nil)
                    }
                }
            }
        }
        
        return true;
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        self.view.endEditing(true)
        return true
    }
    
    // MARK:booking request
    func createBookingRequest(isReconfirm:Int)
    {
        LoadingIndicatorView.show()
        
        let reqJSON : NSMutableDictionary = NSMutableDictionary()
        let tcilForexBooking : NSMutableDictionary = NSMutableDictionary()
        tcilForexBooking.setValue(0, forKey: "addressId")
       
        tcilForexBooking.setValue("ONLINE", forKey: "bookingType")
      
        tcilForexBooking.setValue(sellForexBO.quoteResponseDict.object(forKey: "quoteId")!, forKey: "quoteId")
        
        let advanceAmount:Int = sellForexBO.quoteResponseDict.object(forKey: "advanceAmount")! as! Int
        
        if (!sellForexBO.isSellAtBranchSelected)
        {
            tcilForexBooking.setValue("A", forKey: "paymentType")
            tcilForexBooking.setValue("BLOCK_RATE", forKey: "deliveryMode")
             tcilForexBooking.setValue(advanceAmount, forKey: "bookingAmount")
        }
        else
        {
            tcilForexBooking.setValue("F", forKey: "paymentType")
            tcilForexBooking.setValue("SELL_BRANCH", forKey: "deliveryMode")
            tcilForexBooking.setValue(self.branchId , forKey: "tcBranchId")
             tcilForexBooking.setValue((labelTotalEncashmentAmount.text! as NSString).intValue, forKey: "bookingAmount")
        }
        
        tcilForexBooking.setValue(sellForexBO.title, forKey: "deliveryTitle")
        tcilForexBooking.setValue(sellForexBO.firstName, forKey: "deliveryFirstName")
        tcilForexBooking.setValue(sellForexBO.lastName, forKey: "deliveryLastName")
        tcilForexBooking.setValue(sellForexBO.emailID, forKey: "deliveryEmail")
        tcilForexBooking.setValue("App", forKey: "bookingThrough")
        tcilForexBooking.setValue(sellForexBO.mobileNumber, forKey: "deliveryMobile")
        tcilForexBooking.setValue(textfieldAddress.text, forKey: "deliveryAddress")
        tcilForexBooking.setValue(textfieldCity.text, forKey: "deliveryCity")
        tcilForexBooking.setValue(textfieldBranch.text, forKey: "deliveryState")
        tcilForexBooking.setValue("", forKey: "deliveryPincode")
        tcilForexBooking.setValue("", forKey: "preconfirmPageUrl")
        tcilForexBooking.setValue(sellForexBO.emailID, forKey: "bookedFor")
        tcilForexBooking.setValue(sellForexBO.emailID, forKey: "bookedForEmail")
        tcilForexBooking.setValue("F", forKey: "isOnBehalf")
        tcilForexBooking.setValue("N", forKey: "isRoeEdit")
        
        reqJSON.setValue(tcilForexBooking, forKey: "tcilForexBooking")
        
        //quote
        
        /* let tcilForexQuoteTravellerCollection : NSMutableArray =   (buyForexBo.buyForexPayment!.tcilForexQuote.object(forKey: "tcilForexQuoteTravellerCollection") as! NSArray).mutableCopy() as! NSMutableArray
         buyForexBo.buyForexPayment?.tcilForexQuote.removeObject(forKey: "isError")
         for i in 0..<tcilForexQuoteTravellerCollection.count
         {
         let dict : NSMutableDictionary = (tcilForexQuoteTravellerCollection[i] as! NSDictionary).mutableCopy() as! NSMutableDictionary
         dict.removeObject(forKey: "gstAfterConvenienceFee")
         dict.removeObject(forKey: "gstAfterDelivery")
         dict.setValue(buyForexBo.buyForexPayment?.tcilForexQuote.object(forKey: "quoteId")!, forKey: "quoteId")
         dict.setValue(buyForexBo.buyForexPassengerInfo?.travellerArray[i].travellerTitle, forKey: "travellerTitle")
         dict.setValue(buyForexBo.buyForexPassengerInfo?.travellerArray[i].travellerFirstName, forKey: "travellerFirstName")
         dict.setValue("\(buyForexBo.buyForexPassengerInfo!.travellerArray[i].travellerDateOfTravel)", forKey: "travellerDot")
         dict.setValue(buyForexBo.buyForexPassengerInfo?.travellerArray[i].travellerLastName, forKey: "travellerLastName")
         dict.setValue(buyForexBo.buyForexOptionViewDetails.purposeOfTravel, forKey: "travelType")
         dict.setValue("", forKey: "travellerGender")
         
         tcilForexQuoteTravellerCollection.replaceObject(at: i, with: dict)
         }
         buyForexBo.buyForexPayment?.tcilForexQuote.setValue(tcilForexQuoteTravellerCollection, forKey: "tcilForexQuoteTravellerCollection") */
   
        reqJSON.setValue(ForexAppUtility.convertQuoteResponse(quoteDict: sellForexBO.quoteResponseDict), forKey: "tcilForexQuote")
        reqJSON.setValue(isReconfirm, forKey: "isReconfirm")
        
        let jsonData : NSData = try!JSONSerialization.data(withJSONObject: reqJSON, options: JSONSerialization.WritingOptions.prettyPrinted )as NSData
        print("Actual response")
           print(NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue)! as String)
        let pathParameter  : NSString = "/tcForexRS/booking/sell"
        ForexCommunicationManager.sharedInstance.execTaskWithAlamofire(pathParam: pathParameter, queryParam: "", requestType: "post", jsonDict: reqJSON) { (status, response) in
            
            DispatchQueue.main.async {
                LoadingIndicatorView.hide()
           
            
            if(response != nil)
            {
                //  let _ : NSArray = response as! NSArray
                let responseDict : NSDictionary = response as! NSDictionary
                print("responseDict---> \(responseDict)")
                
                let transactionId = responseDict["isError"] as! Bool
                let message = responseDict["message"] as! String
                
                if transactionId == false
                {
//                    let url = URL(string: "https://www.thomascook.in/paymentGateway.html?tid=".appending(message))
                    var url : URL?;
                    if (!self.sellForexBO.isSellAtBranchSelected)
                    {
                         url = URL(string: "\(kpgURLForPayment)".appending(message))
                        if #available(iOS 10.0, *)
                        {
                            UIApplication.shared.open(url!, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
                        }
                        else
                        {
                            UIApplication.shared.openURL(url!)
                        }
                    }
                    else
                    {
                        //url = URL(string: "\(kpgURLForConfirmation)/Forex/sell/confirmation?tid=".appending(message)) commented by komal 3 aug 2018
                        let sellForxSummaryVC : SellForexSummaryViewController = SellForexSummaryViewController.init(nibName: "ForexBaseViewController", bundle: nil)
                        sellForxSummaryVC.sellForexBO = self.sellForexBO
                        self.navigationController?.pushViewController(sellForxSummaryVC, animated: true)
                    }
                    
                   
                    
                   
                    
                    
                }
                else if message.lowercased().contains(BookingFareIncrease) || message.lowercased().contains(BookingFareDecrease)
                {
                    
                    self.showAlertWithCancel(message: message as NSString)
                    
                }else{
                    
                    self.showAlert(message: message as NSString)
                }
            }
            else
            {
                self.showAlert(message: "Some error has occurred")
            }
                 }
        }
        
        
        
        
        
        
        /* {
         "tcilForexBooking": {
         "addressId": 6843,
         "bookingAmount": 60108,
         "bookingType": "ONLINE",
         "paymentType": "F",
         "quoteId": "1445",
         "deliveryMode": "SELL_BRANCH",
         "deliveryTitle": "Ms",
         "deliveryFirstName": "Shruti",
         "deliveryLastName": "Tele",
         "deliveryEmail": "shruti.tele@in.thomascook.com",
         "deliveryMobile": "9879789789",
         "deliveryAddress": "yry yrtyr\nhjkhj\njkhjkh\nhjkhhk",
         "deliveryCity": "Mumbai",
         "deliveryState": "Maharashtra",
         "deliveryPincode": "454664",
         "preconfirmPageUrl": "https://uatastra.thomascook.in/Forex/sell/forex-booking-review?quoteId=1445&type=3",
         "bookedFor": "shruti.tele@in.thomascook.com",
         "bookedForEmail": "shruti.tele@in.thomascook.com",
         "isOnBehalf": "T",
         "bookingThrough": "Desktop",
         "isRoeEdit": "N"
         },
         "tcilForexQuote": {
         "advanceAmount": 6011,
         "bookerEmailId": "bh@dfg.hfgh",
         "bookerMobileNo": "9789979797",
         "convenienceFee": 0,
         "createDate": "2017-11-24T17:25:09+05:30",
         "custId": 53,
         "dateOfTravel": "29-11-2017",
         "deliveryCharges": 0,
         "gstAfterConvenienceFee": 0,
         "gstAfterDelivery": 0,
         "gstBranch": "23",
         "gstBranchCode": "BHO",
         "gstState": "27",
         "isBranchUt": "N",
         "isRoeEdit": "N",
         "isStateUt": "N",
         "moduleId": 1,
         "moduleName": "Sell",
         "pageType": "PCP",
         "quoteId": 1445,
         "quoteStatus": "Y",
         "splitDiffAmount": 0,
         "tcilForexQuoteRmInfoCollection": [],
         "tcilForexQuoteTravellerCollection": [
         {
         "createDate": "2017-11-24T17:25:09+05:30",
         "tcilForexQuoteTravellerProductCollection": [
         {
         "amount": 1000,
         "createDate": "2017-11-24T17:25:09+05:30",
         "currencyCode": "USD",
         "currencyId": 1,
         "currencyName": "US Dollar",
         "equivalentInr": 60000,
         "moduleName": "Sell",
         "nostroAvailable": "Y",
         "productId": 1,
         "productName": "Cash",
         "productOrder": 0,
         "roe": 60,
         "travellerProductId": 2086
         }
         ],
         "travelType": "Personal",
         "travellerAmount": 60000,
         "travellerDot": "29-11-2017",
         "travellerFirstName": "Shruti",
         "travellerGender": "F",
         "travellerId": 1552,
         "travellerLastName": "Tele",
         "travellerNo": 1,
         "travellerOrder": 0,
         "travellerServiceTax": 108,
         "travellerServiceTaxDesc": "IGST@18%=108",
         "travellerTitle": "Ms",
         "travellerTotalProducts": 1,
         "quoteId": 1445
         }
         ],
         "totalNoOfProducts": 1,
         "totalNoOfTraveller": 1,
         "totalPrice": 60108,
         "totalPriceAfterConvenienceFee": 0,
         "totalPriceAfterDeliveryCharge": 0,
         "totalQuoteAmount": 60000,
         "totalServiceTax": 108
         },
         "isReconfirm": 0
         }
         */
    }
      
    
    // MARK: Button action
    @IBAction func onMapOrAddressButtonClicked(_ sender: Any)
    {
        if !(textfieldAddress.text?.isEmpty)!
        {
            let mapVC : ForexMapViewController = ForexMapViewController.init(nibName: "ForexMapViewController", bundle: nil)
            mapVC.destinationName = self.branchAddress;
            mapVC.lattitude = self.lattitude
            mapVC.longitude = self.longitude
            mapVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            self.present(mapVC, animated: true, completion: nil)
        }
        else
        {
            showAlert(message: "Please select branch")
        }
    }
    @IBAction func continueButtonClicked(_ sender: Any)
    {
        if validateFields()
        {
            createBookingRequest(isReconfirm: 0)
          //  let receiptViewController : ReceiptViewController = ReceiptViewController(nibName: "ForexBaseViewController", bundle: nil)
         //   self.present(receiptViewController, animated: true, completion: nil)
        }
    }
    @IBAction func saveAsPreferredBrancheRadioButtonClicked(_ sender: Any)
    {
        if ((textfieldCity.text?.trimmingCharacters(in: CharacterSet(charactersIn: " ")))?.isEmpty)!
        {
            showAlert(message: "Select city")
            
        }else if ((textfieldBranch.text?.trimmingCharacters(in: CharacterSet(charactersIn: " ")))?.isEmpty)!
        {
            showAlert(message: "Select branch")
            
        }
        else
        {
            saveAsPreferredBrancheRadioButton.isSelected = !saveAsPreferredBrancheRadioButton.isSelected
            
            ForexAppUtility.savePrefredBranch(cityCode: self.cityCode, cityName: self.textfieldCity.text ?? "", branchCode: self.branchId, branchName: self.textfieldBranch.text ?? "", address: self.branchAddress as String, latitude: self.lattitude, longitude: self.longitude, isSave: saveAsPreferredBrancheRadioButton.isSelected)
        }
        
    }
    
    @IBAction func getAddrsViaSMSRadioButtonClicked(_ sender: Any)
    {
        if !(textfieldAddress.text?.isEmpty)!
        {
//            getAddrsViaSMSRadioButton.isSelected = !getAddrsViaSMSRadioButton.isSelected
//            
//            if getAddrsViaSMSRadioButton.isSelected == true
//            {
//                let branchAddress = self.textfieldAddress.text
//                let mobileNumber = sellForexBO.mobileNumber
//                
//                self.getBranchAddressViaSms(mobileNumber: mobileNumber,branchAddress: branchAddress!)
//                {
//                    (String,status) in
//                    
//                    print("String---> \(String)")
//                    print("status---> \(status)")
//                    
//                    if status
//                    {
//                        
//                    }
//                    else
//                    {
//                        
//                    }
//                }
//            }
            
            getAddrsViaSMSRadioButton.isSelected = !getAddrsViaSMSRadioButton.isSelected
            
            if getAddrsViaSMSRadioButton.isSelected == true
            {
                let popupVC: GetAddressViaSmsPopUpViewController = GetAddressViaSmsPopUpViewController(nibName: "GetAddressViaSmsPopUpViewController", bundle: nil)
                popupVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                popupVC.delegate = self
                popupVC.branchAddress = self.textfieldAddress.text
                self.present(popupVC, animated: false, completion: nil)
            }
        }
        else
        {
            getAddrsViaSMSRadioButton.isSelected = false
            showAlert(message: "Please select branch.")
        }
    }
    
    func unCheckGetAddressViaSmsButton()
    {
        getAddrsViaSMSRadioButton.isSelected = false
    }
    
    func getBranchAddressViaSms(mobileNumber:String ,branchAddress: String, completion: @escaping (_ responseDataString:String , _ status:Bool) -> Void)
    {
        let urlString = String(format: AuthenticationConstant.SendBranchAddressTOSERVERURL,mobileNumber,branchAddress)
        let serviceUrl = URL(string: urlString.replacingOccurrences(of: " ", with: "%20"))
        
        var request = URLRequest(url: serviceUrl!)
        request.httpMethod = HTTPMethod.get.rawValue
        request.setValue(ContentType.ApplicationText.rawValue, forHTTPHeaderField: APIConstants.ContentType)
        
        let session = URLSession.shared
        session.dataTask(with: request)
        {
            (data, response, error) in
            
            if let error = error
            {
                printLog(error)
                completion(error.localizedDescription,false)
            }
            else
            {
                if let response = response
                {
                    printLog(response)
                }
                
                if let data1 = data
                {
                    let responseString:String = String(data: data1, encoding: String.Encoding.utf8)!
                    completion(responseString,true)
                }
            }
            }.resume()
    }
    
    @IBAction func continueButtonClickled(_ sender: Any) {
    }
    
    // MARK: Fetch Customer Branch
    func fetchCustomerBranch()
    {
        LoadingIndicatorView.show()
        let pathParameter  : NSString = "/tcForexRS/generic/gstCity"
        ForexCommunicationManager.sharedInstance.execTask(pathParam:pathParameter , queryParam: "", requestType: "get", jsonDict:NSDictionary())
        {
            (status, response) in
            
            DispatchQueue.main.async { () -> Void in
                LoadingIndicatorView.hide()
           
            
            if(response != nil)
            {
                let jsonArray : NSArray = response as! NSArray
                self.arrayForCity = NSMutableArray.init(array: jsonArray) as! [NSDictionary];
            }
            else
            {
                self.showAlert(message: "Some error has occurred")
            }
             }
        }
        
    }
    
    func fetchBranchDetails(withCityCode cityCode:NSString)
    {
        LoadingIndicatorView.show();
        //http://172.16.177.159:6602/tcForexRS/generic/branchdetails/BOM
        
        let pathParameter  : NSString = "/tcForexRS/generic/branchdetails/\(cityCode)" as NSString
        ForexCommunicationManager.sharedInstance.execTask(pathParam:pathParameter , queryParam: "", requestType: "get", jsonDict:NSDictionary())
        {
            (status, response) in
            DispatchQueue.main.async { () -> Void in
                LoadingIndicatorView.hide()
           
            if(response != nil)
            {
                let jsonArray : NSArray = response as! NSArray
                self.arrayForBranch = NSMutableArray.init(array: jsonArray) as! [NSDictionary];
            }
            
            }
            
        }
        
    }
    
    @IBAction func documentsRequiredForProcessingClicked(_ sender: UIButton)
    {
        let popupVC1: PopUpDocumentsRequiredViewController = PopUpDocumentsRequiredViewController(nibName: "PopUpDocumentsRequiredViewController", bundle: nil)
        popupVC1.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(popupVC1, animated: false, completion: nil)
    }
    
    func bindPreferdBranch() {
        if let savedBranchDetasils:Dictionary<String,Any> = ForexAppUtility.getPreferdBranchDetails()
        {
            saveAsPreferredBrancheRadioButton.isSelected = true
            
            if  let cityCode:String = savedBranchDetasils[cityCodeSavePreferedBranch] as? String
           {
                self.cityCode = cityCode
            }
            
            if let cityName:String = savedBranchDetasils[cityNameSavePreferedBranch] as? String
             {
                 self.textfieldCity.text = cityName
            }
             if let branchName:String = savedBranchDetasils[branchNameSavePreferedBranch] as? String
             {
                 self.textfieldBranch.text = branchName
            }
            if let branchCode:Int = savedBranchDetasils[branchCodeSavePreferedBranch] as? Int
            {
                  self.branchId = branchCode
            }
             if let address:String = savedBranchDetasils[addressSavePreferedBranch] as? String
             {
                 textfieldAddress.text =  address
                self.branchAddress  = address as NSString
            }
              if let latitude:Double = savedBranchDetasils[latitudeSavePreferedBranch] as? Double
              {
                 self.lattitude = latitude
            }
            if let longitude:Double = savedBranchDetasils[longitudeSavePreferedBranch] as? Double
            {
                 self.longitude = longitude
            }
            
            self.fetchBranchDetails(withCityCode: cityCode as NSString)
           
            
        }
        
       
        
    }
    
    func showAlertWithCancel(message:NSString) -> Void
    {
        let alert = UIAlertController(title: "Alert", message: message as String, preferredStyle: UIAlertController.Style.alert)
        
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: { UIAlertAction in
            alert.dismiss(animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { UIAlertAction in
            
            self.createBookingRequest(isReconfirm: 1)
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
