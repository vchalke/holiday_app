//
//  RoomsDataModel.m
//  holidays
//
//  Created by ketan on 15/10/15.
//  Copyright © 2015 Mobicule. All rights reserved.
//

#import "RoomsDataModel.h"

@implementation RoomsDataModel
@synthesize infantCount,adultCount,arrayChildrensData;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.SRCount = 0;
        self.DRCount = 0;
        self.TRCount = 0;
        self.CWBCount = 0;
        self.CNBCount = 0;
        self.adultCount = 2;
        if (self.arrayChildrensData == nil)
        {
            arrayChildrensData = [[NSMutableArray alloc]init];
        }
    }
    return self;
}
@end
