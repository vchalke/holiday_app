//
//  WishListHolidayObject.m
//  holidays
//
//  Created by Kush_Tech on 21/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "WishListHolidayObject.h"

@implementation WishListHolidayObject
-(instancetype)initWithWishListObject:(NSDictionary *)dictionary{
    if ([super init])
    {
        NSLog(@"%@",dictionary);
        NSLog(@"%@",[dictionary valueForKey:@"packageID"]);
        self.packageData = [dictionary valueForKey:@"packageData"];
        self.packageID = [dictionary valueForKey:@"packageID"];
        self.packageName = [dictionary valueForKey:@"packageName"];
        self.packageImgUrl = [dictionary valueForKey:@"packageImgUrl"];
        self.packagePrize = [[dictionary valueForKey:@"packagePrize"] integerValue];
    }
    
    return self;
}
@end
