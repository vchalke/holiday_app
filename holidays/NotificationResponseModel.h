//
//  NotificationResponseModel.h
//  holidays
//
//  Created by Kush_Team on 16/07/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NotificationResponseModel : NSObject
-(instancetype)initWithNotificationResponseModel:(NSDictionary *)dictionary;
@property(nonatomic,strong)NSString *DeliverTime;
@property(nonatomic,strong)NSString *ID;
@property(nonatomic,strong)NSString *Identity;
@property(nonatomic,strong)NSDictionary *MessageData;
@property(nonatomic,strong)NSDictionary *aps;
@property(nonatomic,strong)NSDictionary *alert;
@property(nonatomic,strong)NSString *body;
@property(nonatomic,strong)NSString *subtitle;
@property(nonatomic,strong)NSString *title;
@property(nonatomic,strong)NSDictionary *customPayload;
@property(nonatomic,strong)NSDictionary *payload;
@property(nonatomic,strong)NSArray *actionButton;
@property(nonatomic,strong)NSArray *carousel;
@property(nonatomic,strong)NSString *deeplink;
@property(nonatomic,strong)NSString *expiry;
@property(nonatomic,strong)NSString *mediaurl;
@property(nonatomic,strong)NSString *trid;
@property(nonatomic,strong)NSString *Status;
@end

NS_ASSUME_NONNULL_END



