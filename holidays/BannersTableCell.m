//
//  BannersTableCell.m
//  holidays
//
//  Created by Ios_Team on 29/10/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "BannersTableCell.h"
#import "BannersCollectionCell.h"
@implementation BannersTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setupCollectionView{
    self.collectionViews.delegate = self;
    self.collectionViews.dataSource = self;
    [self.collectionViews registerNib:[UINib nibWithNibName:@"BannersCollectionCell" bundle:nil] forCellWithReuseIdentifier:@"BannersCollectionCell"];
}

#pragma mark - CollectionView Delegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 8;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    BannersCollectionCell *cell = (BannersCollectionCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"BannersCollectionCell" forIndexPath:indexPath];
    cell.lbl_name.text = @"Europe";
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return CGSizeMake(self.collectionViews.frame.size.width*0.3, self.collectionViews.frame.size.height*0.95);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{

}
@end
