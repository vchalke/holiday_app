//
//  PixelSingleton.m
//  holidays
//
//  Created by Kush_Team on 24/08/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "PixelSingleton.h"

@implementation PixelSingleton

+(id)sharedInstance{
    static PixelSingleton *shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[self alloc] init];
        
    });
    return shared;
}

- (id)init {
    if (self = [super init]) {
        
    }
    return self;
}


-(CGFloat)sizeInPixel:(CGFloat)pixel withBase:(CGFloat)basePixel{
    
    CGFloat phoneHeight = [[UIScreen mainScreen] bounds].size.height;
    CGFloat phoneHeightInpixel = phoneHeight * pixel;
    CGFloat onlyPixel = phoneHeightInpixel / basePixel;
//    return onlyPixel;
    CGFloat pixelToPoint = onlyPixel * 0.75;
    return pixelToPoint;
}

@end

// Formula For Pixel Conversion
/*
 Pixel to point conversion
 1 px = 0.75 points
 100 px = 75 points
 
 Point to Pixel conversion
 75 points = 100 px
 
 One Way
 First Calculate all in pixel then multiply it by 0.75 to convert it into point
 (((IPhone_Height_px * Given_px) / Respective_px)) * 0.75
 */
