//
//  TravelBlogCollectionViewCell.m
//  holidays
//
//  Created by Kush_Tech on 24/02/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "TravelBlogCollectionViewCell.h"
#import "UIImageView+WebCache.h"
#import "Thomas_Cook_Holidays-Swift.h"
@implementation TravelBlogCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(void)loadTravelBlogObjectInCell:(TravelBlogObject*)travelObj{
//    self.lbl_Info.text = travelObj.title;
}
-(void)loadTravelBlogObject:(TravelBlogObj*)travelObj{
//    NSAttributedString * titleString = [[NSAttributedString alloc]initWithString:travelObj.title attributes:nil];
//    self.lbl_Info.text = titleString.string;
    NSAttributedString * titleString = [[NSAttributedString alloc] initWithData:[travelObj.title dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    self.lbl_Info.text = titleString.string;
  
    NSURL* urlImage=[NSURL URLWithString:travelObj.imgUrl];
    if(urlImage)
    {
        UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        indicator.center = self.img_views.center;
        [self.img_views addSubview:indicator];
        [indicator startAnimating];

        [self.img_views sd_setImageWithURL:urlImage placeholderImage:[UIImage imageNamed:@"defaultBanner.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
            [indicator stopAnimating];
        }];
    }
}
@end
