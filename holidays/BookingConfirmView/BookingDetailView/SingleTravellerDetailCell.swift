//
//  SingleTravellerDetailCell.swift
//  holidays
//
//  Created by Kush_Team on 21/07/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

import UIKit

class SingleTravellerDetailCell: UITableViewCell {

    @IBOutlet weak var lblRoomName: UILabel!
    @IBOutlet weak var lblFirstPersonName: UILabel!
    @IBOutlet weak var lblSecondPersonName: UILabel!
    
    var travellerDetail : [String]?{
        didSet{
            if let travellerDetail = travellerDetail{
                lblRoomName.text = travellerDetail[0]
                lblFirstPersonName.text = travellerDetail[1]
                lblSecondPersonName.text = travellerDetail[2]
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
