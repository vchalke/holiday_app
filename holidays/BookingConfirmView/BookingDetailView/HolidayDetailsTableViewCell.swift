//
//  HolidayDetailsTableViewCell.swift
//  holidays
//
//  Created by Kush_Team on 18/06/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

import UIKit

class HolidayDetailsTableViewCell: UITableViewCell {
    var packageDetailModel:PackageDetailsModel = PackageDetailsModel.init()
    var pricingModel:PricingModel = PricingModel()
    var bookingConfirmJsonDict:Dictionary<String,Any> = Dictionary<String,Any>()
    
    @IBOutlet weak var lbl_PackageName: UILabel!
    @IBOutlet weak var lbl_PackageDuration: UILabel!
    @IBOutlet weak var lbl_StartFrom: UILabel!
    @IBOutlet weak var lbl_PackagetravelDate: UILabel!
    @IBOutlet weak var lbl_numOfTraveller: UILabel!
    @IBOutlet weak var btn_optionalActivity: UIButton!
    @IBOutlet weak var btn_PremiumUpdate: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setHeaderInfo(){
        self.lbl_PackageName.text = packageDetailModel.pkgName
        self.lbl_PackagetravelDate.text = pricingModel.pricingTourDate
        self.lbl_numOfTraveller.text = "\(pricingModel.pricingArrayRooms) Rooms"
        let room = "\(pricingModel.pricingArrayRooms) Rooms"
        let traveller = "\(pricingModel.getAdultCount() + pricingModel.getchildCount() + pricingModel.getInfantCount()) Travellers"
        self.lbl_numOfTraveller.text = "\(room) | \(traveller)"
        self.btn_optionalActivity.setTitle("3 Optional Activities", for: .normal)
        self.btn_PremiumUpdate.setTitle("Upgradation to Premium Holiday", for: .normal)
    }
}
