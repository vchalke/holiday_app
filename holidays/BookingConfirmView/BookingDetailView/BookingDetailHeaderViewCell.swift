//
//  BookingDetailHeaderViewCell.swift
//  holidays
//
//  Created by Kush_Team on 18/06/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

import UIKit
protocol BookingDetailHeaderViewCellDelegawete {
    func delegatePrintPress()
    func delegateSharePress()
}
class BookingDetailHeaderViewCell: UITableViewCell {

    var packageDetailModel:PackageDetailsModel = PackageDetailsModel.init()
    var bookingConfirmJsonDict:Dictionary<String,Any> = Dictionary<String,Any>()
    @IBOutlet weak var lbl_bookRef: UILabel!
    @IBOutlet weak var lbl_packageName: UILabel!
    @IBOutlet weak var lbl_packageDate: UILabel!
    var delegates : BookingDetailHeaderViewCellDelegawete!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setHeaderInfo(){
        if let bookingRefNo = bookingConfirmJsonDict["bookingRefNo"]{
            self.lbl_bookRef.text = "Booking ID. \(bookingRefNo)"
        }
        let stringDate = AppUtility.getDatesInFormat(inDate: Date(), formatter: "EE, dd MMM yyyy")
        self.lbl_packageDate.text = "Booking On : \(stringDate)"
        self.lbl_packageName.text = packageDetailModel.pkgName
    }
    @IBAction func btn_pressPrint(_ sender: Any) {
        self.delegates.delegatePrintPress()
    }
    @IBAction func btn_pressShare(_ sender: Any) {
        self.delegates.delegateSharePress()
    }
    
}
