//
//  TravellerDetailsTableViewCell.swift
//  holidays
//
//  Created by Kush_Team on 18/06/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

import UIKit

class TravellerDetailsTableViewCell: UITableViewCell,UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var cnstbaseViewHeight: NSLayoutConstraint!
    @IBOutlet weak var cnstTableViewContainerHeight: NSLayoutConstraint!
    var pricingModel:PricingModel = PricingModel()
    var travellerArray = [[String]]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func initTableView(){
        tableView.register(UINib(nibName: "SingleTravellerDetailCell", bundle: nil), forCellReuseIdentifier: "SingleTravellerDetailCell")
        tableView.dataSource = self
        tableView.delegate = self
        self.cnstTableViewContainerHeight.constant = 220
        self.cnstbaseViewHeight.constant = 300
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return travellerArray.count
        return pricingModel.pricingArrayRooms.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SingleTravellerDetailCell", for: indexPath) as! SingleTravellerDetailCell
        cell.travellerDetail = travellerArray[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110 // 110
    }
}
