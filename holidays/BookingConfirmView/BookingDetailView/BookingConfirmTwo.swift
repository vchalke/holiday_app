//
//  BookingConfirmTwo.swift
//  holidays
//
//  Created by Kush_Team on 18/06/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

import UIKit

class BookingConfirmTwo: UIViewController,UITableViewDelegate,UITableViewDataSource,BookingDetailHeaderViewCellDelegawete {
    
    var packageDetailModel:PackageDetailsModel = PackageDetailsModel.init()
    var pricingModel:PricingModel = PricingModel()
    var bookingConfirmJsonDict:Dictionary<String,Any> = Dictionary<String,Any>()
    var travellerArray = [["Room1","Mr. Tejas Juware","Mrs. xyz Juware"],["Room2","Mr. Tejas1 Juware","Mrs. xyz1 Juware"]]
    @IBOutlet weak var tableViews: UITableView!
    @IBOutlet weak var titleLbl: UILabel!
    var titleString = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.titleLbl.text = titleString
        // Do any additional setup after loading the view.
        self.initTableView()
        
    }
    func initTableView(){
        self.tableViews.register(UINib(nibName: "UserDetailViewCell", bundle: Bundle.main), forCellReuseIdentifier: "UserDetailViewCell")
        self.tableViews.register(UINib(nibName: "BookingDetailHeaderViewCell", bundle: Bundle.main), forCellReuseIdentifier: "BookingDetailHeaderViewCell")
        self.tableViews.register(UINib(nibName: "HolidayDetailsTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "HolidayDetailsTableViewCell")
        self.tableViews.register(UINib(nibName: "PaymentDetailsTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "PaymentDetailsTableViewCell")
        self.tableViews.register(UINib(nibName: "TravellerDetailsTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "TravellerDetailsTableViewCell")
    }

    @IBAction func btn_backPress(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        return 3 //
    }
    
    func numberOfSections(in tableView: UITableView) -> Int{
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        if (indexPath.row==0){
            let cell :HolidayDetailsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "HolidayDetailsTableViewCell", for: indexPath) as! HolidayDetailsTableViewCell
            cell.packageDetailModel = self.packageDetailModel
            cell.bookingConfirmJsonDict = self.bookingConfirmJsonDict
            cell.pricingModel = self.pricingModel
            cell.setHeaderInfo()
            return cell
        }else if (indexPath.row==1){
            let cell :PaymentDetailsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PaymentDetailsTableViewCell", for: indexPath) as! PaymentDetailsTableViewCell
            return cell
        }else if (indexPath.row==2){
            let cell :TravellerDetailsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "TravellerDetailsTableViewCell", for: indexPath) as! TravellerDetailsTableViewCell
            cell.initTableView()
            cell.travellerArray = self.travellerArray
            return cell
        }
        
        let cell :UserDetailViewCell = tableView.dequeueReusableCell(withIdentifier: "UserDetailViewCell", for: indexPath) as! UserDetailViewCell
        return cell;
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 200
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableCell(withIdentifier: "BookingDetailHeaderViewCell" ) as! BookingDetailHeaderViewCell
        headerView.packageDetailModel = self.packageDetailModel
            headerView.bookingConfirmJsonDict = self.bookingConfirmJsonDict
        headerView.delegates = self
        headerView.setHeaderInfo()
        return headerView
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (indexPath.row==2){
            return 350
        }
            return UITableView.automaticDimension //400
    }
    
//    BookingDetailHeaderViewCell
    
    func delegatePrintPress() {
    }
    
    func delegateSharePress() {
        let appNaqme = "Thomas Cook Holidays"
        let pkgName = "\(self.packageDetailModel.pkgName)"
        var pkgBookingID = ""
        if let bookingRefNo = bookingConfirmJsonDict["bookingRefNo"]{
            pkgBookingID = "Reference No. \(bookingRefNo)"
        }
        let pkgBookingDate = AppUtility.getDatesInFormat(inDate: Date(), formatter: "EE, dd MMM yyyy")
        
        let activityViewController : UIActivityViewController = UIActivityViewController(
            activityItems: [appNaqme,pkgName,pkgBookingID,pkgBookingDate], applicationActivities: nil)
        
        // This lines is for the popover you need to show in iPad
//        activityViewController.popoverPresentationController?.sourceView = (sender as! UIButton)
        
        // This line remove the arrow of the popover to show in iPad
        activityViewController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.unknown
        activityViewController.popoverPresentationController?.sourceRect = CGRect(x: 150, y: 150, width: 0, height: 0)
        
        // Anything you want to exclude
        activityViewController.excludedActivityTypes = [
            UIActivity.ActivityType.postToWeibo,
            UIActivity.ActivityType.print,
            UIActivity.ActivityType.assignToContact,
            UIActivity.ActivityType.saveToCameraRoll,
            UIActivity.ActivityType.addToReadingList,
            UIActivity.ActivityType.postToFlickr,
            UIActivity.ActivityType.postToVimeo,
            UIActivity.ActivityType.postToTencentWeibo
        ]
        
        self.present(activityViewController, animated: true, completion: nil)
    }
}
