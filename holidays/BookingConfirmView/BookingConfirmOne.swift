//
//  BookingConfirmOne.swift
//  holidays
//
//  Created by Kush_Team on 18/06/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

//Ok sure
//https://services-uatastra.thomascook.in/tcHolidayRS/confirmation/i8GRCGvqRzBJ8VPpYm%2B48g%3D%3D
//After closing browser or minimising browser - we will have to call this service so that we know if booking is done or not

import UIKit

class BookingConfirmOne: UIViewController {
    var packageDetailModel:PackageDetailsModel = PackageDetailsModel.init()
    var pricingModel:PricingModel = PricingModel()
    var getBookingModel : BookingConfirmationModel = BookingConfirmationModel()
    var bookingConfirmJsonDict:Dictionary<String,Any> = Dictionary<String,Any>()
    @IBOutlet weak var mainBaseTopView: UIView!
    @IBOutlet weak var View_BookingDetails: UIView!
    @IBOutlet weak var lbl_referenceNumber: UILabel!
    @IBOutlet weak var lbl_bookingConfirm: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.isHidden = true
        self.callAPIofBookingConfimation()
    }
    override func viewWillAppear(_ animated: Bool) {
        
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    func callAPIofBookingConfimation() {
        let transactionId = UserDefaults.standard.value(forKey: kCurrentTransactionId) as? String
        if (transactionId?.count ?? 0 > 0){
            
//            let transactionId =  "qEmpP8Z3EOmciHIjWez44g%3D%3D"
            PreBookingCommunicationManager.getBookingsConfirmations(requestType: "GET", queryParam: transactionId, pathParam: nil, jsonParam: nil, headers: nil, requestURL: kAstraTcHolidayRSConfirmation, returnInCaseOffailure: false) { (status, responseData) in
                LoadingIndicatorView.hide()
                if status{
                    if let bookingDetailsDict:Dictionary<String,Any> = responseData as? Dictionary<String,Any>{
                        print("bookingDetailsDict \(bookingDetailsDict)")
                        DispatchQueue.main.async {
                            self.mainBaseTopView.isHidden = false
                            self.View_BookingDetails.isHidden = false
                            self.lbl_bookingConfirm.text = "Booking Confirmed"
                            let referenceId = bookingDetailsDict["bookingReferenceId"] as? String ?? ""
                            self.lbl_referenceNumber.text = "Reference No. \(referenceId)"
                        }
                    }
                }else{
                    self.alertViewControllerSingleton(title: "Failure", message: "Some Error Occured", type: .alert, buttonArryName: ["Ok"]) { (strin) in
                        printLog(strin)
                        self.jumpToLanding()
                    }
                }
            }
            
        }else{
            self.alertViewControllerSingleton(title: "Failure", message: "Some Error Occured", type: .alert, buttonArryName: ["Ok"]) { (strin) in
                printLog(strin)
                self.jumpToLanding()
            }
        }
        
        
    }
    @IBAction func btn_backPress(_ sender: Any){
        self.jumpToLanding()
    }
    @IBAction func btn_TravellerDetail(_ sender: Any){
        let bookingdetailVC : BookingConfirmTwo = BookingConfirmTwo(nibName: "BookingConfirmTwo", bundle: nil)
        bookingdetailVC.titleString = "Other Travller Details"
        self.navigationController?.pushViewController(bookingdetailVC, animated: true)
    }
    @IBAction func btn_BookingPress(_ sender: Any){
        let bookingdetailVC : BookingConfirmTwo = BookingConfirmTwo(nibName: "BookingConfirmTwo", bundle: nil)
        bookingdetailVC.titleString = "Booking Details"
        bookingdetailVC.packageDetailModel = self.packageDetailModel
        bookingdetailVC.pricingModel = self.pricingModel
        bookingdetailVC.bookingConfirmJsonDict = bookingConfirmJsonDict
        self.navigationController?.pushViewController(bookingdetailVC, animated: true)
    }
    @IBAction func btn_Phonepress(_ sender: Any){
        AppUtility.callToTCFromPhaseTwo()
    }
    @IBAction func btn_BackToHome(_ sender: Any){
        self.jumpToLanding()
    }

    func jumpToLanding(){
        let landingVC : LandingScreenVC = LandingScreenVC(nibName: "LandingScreenVC", bundle: nil)
               self.navigationController?.pushViewController(landingVC, animated: true)
    }
}



class BookingConfirmationModel: NSObject
{
    var convenienceFee : Int
    var totalPrice : Int
    var totalCnbJunior : Int
    var lastName : String
    var promoCode : String
    var totalTr : Int
    var addressPhoneNo : String
    var totalCwb : Int
    var convenienceFeeServiceTax : Int
    var bookedBy : String
    var totalSr : Int
    var firstName : String
    var totalCnbSenior : Int
    var totalQr : Int
    var custId : String
    var addressCity : String
    var bookingThrough : String
    var addressEmailId : String
    var packageId : String
    var bookingDate : String
    var quotationId : String
    var receiptNo : String
    var upgradedTo : Int
    var isGstApplicable : String
    var addressStreet : String
    var gstinNo : String
    var bookingAmount : Int
    var bookingTempAmount : Int
    var tcilHolidayBookingTravellerDetailCollection : Array<Dictionary<String,Any>>
    var addressPincode : String
    var isHsa : String
    var bookingReferenceId : String
    var paymentGateway : String
    var serviceTax : Int
    var createDate : String
    var totalConvenienceFee : Int
    var gstinName : String
    var isOnBehalfToDB : String
    var tcilHoldayBookingPkgPriceCollection : Array<Dictionary<String,Any>>
    var promoCodeAmount : Int
    var bookingAmountBeforeDiscount : Int
    var serviceTaxBeforeDiscount : Int
    var bookedFor : String
    var title : String
    var bookingStatus : Int
    var totalDr : Int
    var bookingType : String
    
//    var profileGSTInfo : Dictionary<String,Any>
    
    override init() {
        
        self.convenienceFee = 0
        self.totalPrice = 0
        self.totalCnbJunior = 0
        self.lastName = ""
        self.promoCode = ""
        self.totalTr = 0
        self.addressPhoneNo = ""
        self.totalCwb = 0
        self.convenienceFeeServiceTax = 0
        self.bookedBy = ""
        self.totalSr = 0
        self.firstName = ""
        self.totalCnbSenior = 0
        self.totalQr = 0
        self.custId = ""
        self.addressCity = ""
        self.bookingThrough = ""
        self.addressEmailId = ""
        self.packageId = ""
        self.bookingDate = ""
        self.quotationId = ""
        self.receiptNo = ""
        self.upgradedTo = 0
        self.isGstApplicable = ""
        self.addressStreet = ""
        self.gstinNo = ""
        self.bookingAmount = 0
        self.bookingTempAmount = 0
        self.tcilHolidayBookingTravellerDetailCollection = Array.init()
        self.addressPincode = ""
        self.isHsa = ""
        self.bookingReferenceId = ""
        self.paymentGateway = ""
        self.serviceTax = 0
        self.createDate = ""
        self.totalConvenienceFee = 0
        self.gstinName = ""
        self.isOnBehalfToDB = ""
        self.tcilHoldayBookingPkgPriceCollection = Array.init()
        self.promoCodeAmount = 0
        self.bookingAmountBeforeDiscount = 0
        self.serviceTaxBeforeDiscount = 0
        self.bookedFor = ""
        self.title = ""
        self.bookingStatus = 0
        self.totalDr = 0
        self.bookingType = ""
        
        //        self.gender = ""
        //        self.addresses = Array.init()
        //        self.profileGSTInfo = Dictionary.init()
        
    }
    
    func initWithDictionary(confirmationdict:Dictionary<String,Any>)
    {
        //        let packageDetailDict : Dictionary<String,Any> = dict["packageDetail"] as! Dictionary<String, Any>
        //        self.arrayPackageImagePathList = packageDetailDict["tcilHolidayPhotoVideoCollection"] as? Array<Any> ?? Array.init()
        //        self.tcilHolidaySightseeingCollection = packageDetailDict["tcilHolidaySightseeingCollection"] as? Array<Dictionary<String, Any>> ?? Array.init()
        
        self.convenienceFee = confirmationdict["convenienceFee"] as? Int ?? 0
        self.totalPrice = confirmationdict["totalPrice"] as? Int ?? 0
        self.totalCnbJunior = confirmationdict["totalCnbJunior"] as? Int ?? 0
        self.lastName = confirmationdict["lastName"] as? String ?? ""
        self.promoCode = confirmationdict["promoCode"] as? String ?? ""
        self.totalTr = confirmationdict["convenienceFee"] as? Int ?? 0
        self.addressPhoneNo = confirmationdict["addressPhoneNo"] as? String ?? ""
        self.totalCwb = confirmationdict["convenienceFee"] as? Int ?? 0
        self.convenienceFeeServiceTax = confirmationdict["convenienceFee"] as? Int ?? 0
        self.bookedBy = confirmationdict["bookedBy"] as? String ?? ""
        self.totalSr = confirmationdict["totalSr"] as? Int ?? 0
        self.firstName = confirmationdict["firstName"] as? String ?? ""
        self.totalCnbSenior = confirmationdict["totalCnbSenior"] as? Int ?? 0
        self.totalQr = confirmationdict["totalQr"] as? Int ?? 0
        self.custId = confirmationdict["custId"] as? String ?? ""
        self.addressCity = confirmationdict["addressCity"] as? String ?? ""
        self.bookingThrough = confirmationdict["bookingThrough"] as? String ?? ""
        self.addressEmailId = confirmationdict["addressEmailId"] as? String ?? ""
        self.packageId = confirmationdict["packageId"] as? String ?? ""
        self.bookingDate = confirmationdict["bookingDate"] as? String ?? ""
        self.quotationId = confirmationdict["quotationId"] as? String ?? ""
        self.receiptNo = confirmationdict["receiptNo"] as? String ?? ""
        self.upgradedTo = confirmationdict["upgradedTo"] as? Int ?? 0
        self.isGstApplicable = confirmationdict["isGstApplicable"] as? String ?? ""
        self.addressStreet = confirmationdict["addressStreet"] as? String ?? ""
        self.gstinNo = confirmationdict["gstinNo"] as? String ?? ""
        self.bookingAmount = confirmationdict["bookingAmount"] as? Int ?? 0
        self.bookingTempAmount = confirmationdict["bookingTempAmount"] as? Int ?? 0
        self.tcilHolidayBookingTravellerDetailCollection = confirmationdict["tcilHolidayBookingTravellerDetailCollection"] as? Array<Dictionary<String, Any>> ?? Array.init()
        self.addressPincode = confirmationdict["addressPincode"] as? String ?? ""
        self.isHsa = confirmationdict["isHsa"] as? String ?? ""
        self.bookingReferenceId = confirmationdict["bookingReferenceId"] as? String ?? ""
        self.paymentGateway = confirmationdict["paymentGateway"] as? String ?? ""
        self.serviceTax = confirmationdict["serviceTax"] as? Int ?? 0
        self.createDate = confirmationdict["createDate"] as? String ?? ""
        self.totalConvenienceFee = confirmationdict["totalConvenienceFee"] as? Int ?? 0
        self.gstinName = confirmationdict["gstinName"] as? String ?? ""
        self.isOnBehalfToDB = confirmationdict["isOnBehalfToDB"] as? String ?? ""
        self.tcilHoldayBookingPkgPriceCollection = confirmationdict["tcilHoldayBookingPkgPriceCollection"] as? Array<Dictionary<String, Any>> ?? Array.init()
        self.promoCodeAmount = confirmationdict["promoCodeAmount"] as? Int ?? 0
        self.bookingAmountBeforeDiscount = confirmationdict["bookingAmountBeforeDiscount"] as? Int ?? 0
        self.serviceTaxBeforeDiscount = confirmationdict["serviceTaxBeforeDiscount"] as? Int ?? 0
        self.bookedFor = confirmationdict["bookedFor"] as? String ?? ""
        self.title = confirmationdict["title"] as? String ?? ""
        self.bookingStatus = confirmationdict["bookingStatus"] as? Int ?? 0
        self.totalDr = confirmationdict["totalDr"] as? Int ?? 0
        self.bookingType = confirmationdict["bookingType"] as? String ?? ""
        
    }
}



/*
 {
   "convenienceFee" : 0,
   "totalPrice" : 120015,
   "totalCnbJunior" : 0,
   "lastName" : "Karve",
   "promoCode" : "TESTPROM",
   "totalTr" : 0,
   "addressPhoneNo" : "9820198918",
   "totalCwb" : 0,
   "convenienceFeeServiceTax" : 0,
   "bookedBy" : "339",
   "totalSr" : 0,
   "firstName" : "Atari",
   "totalCnbSenior" : 0,
   "totalQr" : 0,
   "custId" : "339",
   "addressCity" : "Mumbai",
   "bookingThrough" : "APP",
   "addressEmailId" : "vchalke@gmail.com",
   "addressState" : "Maharashtra",
   "packageId" : "PKG000526",
   "bookingDate" : "2020-07-27T00:00:00+05:30",
   "quotationId" : "WaKckUuYB1lPDw2yA%2B9zvQ%3D%3D",
   "receiptNo" : "EX1P0qlz9wnU3gZjUK4HmA%3D%3D",
   "upgradedTo" : 0,
   "isGstApplicable" : "Y",
   "addressStreet" : "",
   "gstinNo" : "",
   "bookingAmount" : 120015,
   "bookingTempAmount" : 120015,
   "tcilHolidayBookingTravellerDetailCollection" : [
     {
       "lastName" : "Karve",
       "firstName" : "Atari",
       "roomNo" : 1,
       "dob" : "27-07-1992",
       "isDone" : false,
       "mealPreference" : "NONVEG",
       "travellerNo" : 1,
       "title" : "",
       "travellerDetailId" : 43912,
       "paxType" : 0,
       "gender" : "F",
       "hotelRoomType" : 1
     },
     {
       "gender" : "M",
       "isDone" : false,
       "firstName" : "TBA",
       "mealPreference" : "VEG",
       "dob" : "01-01-1979",
       "paxType" : 0,
       "title" : "Mr",
       "travellerDetailId" : 43913,
       "travellerNo" : 2,
       "hotelRoomType" : 0,
       "lastName" : "TBA"
     }
   ],
   "addressPincode" : "",
   "isHsa" : "N",
   "bookingReferenceId" : "HD00023884",
   "paymentGateway" : "NIDFC",
   "serviceTax" : 5715,
   "createDate" : "2020-07-27T10:59:03.62+05:30",
   "totalConvenienceFee" : 0,
   "gstinName" : "",
   "isOnBehalfToDB" : "F",
   "tcilHoldayBookingPkgPriceCollection" : [
     {
       "amount" : 84300,
       "amountId" : 26931,
       "currencyCode" : "INR",
       "currencyRate" : 1
     },
     {
       "amount" : 600,
       "amountId" : 26930,
       "currencyCode" : "EUR",
       "currencyRate" : 50
     }
   ],
   "promoCodeAmount" : 12000,
   "bookingAmountBeforeDiscount" : 114300,
   "serviceTaxBeforeDiscount" : 5715,
   "bookedFor" : "339",
   "title" : "",
   "bookingStatus" : 1,
   "totalDr" : 1,
   "bookingType" : "F"
 }
 */

