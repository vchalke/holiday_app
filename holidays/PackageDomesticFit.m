//
//  PackageDomesticFit.m
//  holidays
//
//  Created by ketan on 23/10/15.
//  Copyright © 2015 Mobicule. All rights reserved.
//

#import "PackageDomesticFit.h"
#import "RoomsDataModel.h"
#import "TravellerInformationModel.h"

@implementation PackageDomesticFit
- (instancetype)initWithTravellerArray:(NSArray *)travellerArray
{
    self = [super init];
    if (self) {
        self.travellerArray = travellerArray;
    }
    return self;
}


-(id)calaculatePackageInternationalFIT
{
    NSMutableArray *dynamicTravellerinfoArray = [[NSMutableArray alloc]init];
    for (int index = 0; index < self.travellerArray.count ; index ++)
    {
        RoomsDataModel *roomsModelInsatance = self.travellerArray[index];
        roomsModelInsatance.DRCount = 0;
        roomsModelInsatance.SRCount = 0;
        roomsModelInsatance.TRCount = 0;
        roomsModelInsatance.CWBCount = 0;
        roomsModelInsatance.CNBCount = 0;
        int adultCount = roomsModelInsatance.adultCount;
        int infantCount = roomsModelInsatance.infantCount;
        NSArray *childArray = roomsModelInsatance.arrayChildrensData;
        int childWithCNBCount = 0;
        int childWithCWBCount = 0;
        NSMutableArray *childrenAgeArray = [[NSMutableArray alloc]init];
        NSMutableArray *childDictArray = [[NSMutableArray alloc]init];
        for (int j = 0; j<childArray.count; j++)
        {
            NSDictionary *childDict = childArray[j];
            UIButton *button = [childDict valueForKey:@"button"];
            NSDictionary *dictForChild ;
            UILabel *label = [childDict valueForKey:@"textField"];
            int age = [label.text intValue];
            if ([button isSelected]) {
                dictForChild = [[NSMutableDictionary alloc] initWithObjectsAndKeys:label.text,@"CWB", nil];
                childWithCWBCount = childWithCWBCount + 1;
            }
            else
            {
                dictForChild = [[NSMutableDictionary alloc] initWithObjectsAndKeys:label.text,@"CNB", nil];
                childWithCNBCount = childWithCNBCount + 1;
            }
            
            [childDictArray addObject:dictForChild];
            [childrenAgeArray addObject:[NSNumber numberWithInt:age]];
        }
        id travellerInfo = [self calculatePersonInfoWithTravellerArray:dynamicTravellerinfoArray withAdultCount:adultCount withChildWithCNBCount:childWithCNBCount withChildWithCWBcount:childWithCWBCount withRoomNo:index+1 withChildrenArray:childrenAgeArray withRoomDataModelInstance:roomsModelInsatance withChildDictArray:childDictArray];
        if ([travellerInfo isKindOfClass:[NSString class]])
        {
            return travellerInfo;
        }else
        {
            for (int j = 0; j<infantCount; j++)
            {
                TravellerInformationModel *travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeInfant withRoomNo:index+1 withType:kTravellerTypeInfant];
                [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
            }
        }
        
    }
    
    return dynamicTravellerinfoArray;
}
-(id)calculatePersonInfoWithTravellerArray:(NSMutableArray *)dynamicTravellerinfoArray withAdultCount:(int) adultCount withChildWithCNBCount:(int)childCNBCount withChildWithCWBcount:(int)childCWBCount withRoomNo:(int)roomNo withChildrenArray:(NSArray *)childrenAgeArray withRoomDataModelInstance:(RoomsDataModel *)roomDataModelInstance withChildDictArray:(NSArray *)childDictArray
{
    TravellerInformationModel *travellerInfoModelInstance;
    int totalChildCount = childCNBCount+childCWBCount;
    
    switch (totalChildCount) {
        case 0:
            switch (adultCount) {
                case 1:
                    // 1*SR
                    roomDataModelInstance.SRCount = 1;
                    travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeSR withRoomNo:roomNo withType:kTravellerTypeAdult];
                    [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                    break;
                    
                case 2:
                    // 2*DR
                    roomDataModelInstance.DRCount = 1;
                    travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeAdult];
                    [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                    
                    travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeAdult];
                    [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                    break;
                    
                case 3:
                    // 3*TR
                    roomDataModelInstance.TRCount = 1;
                    travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeTR withRoomNo:roomNo withType:kTravellerTypeAdult];
                    [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                    
                    travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeTR withRoomNo:roomNo withType:kTravellerTypeAdult];
                    [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                    
                    travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeTR withRoomNo:roomNo withType:kTravellerTypeAdult];
                    [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                    
                    break;
                    
                default:
                    break;
            }
            break;
        case 1:
            if (totalChildCount == childCWBCount)
            {
                switch (adultCount) {
                    case 1:
                        //2*DR
                        roomDataModelInstance.DRCount = 1;
                        travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeChild];
                        [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                        
                        travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeAdult];
                        [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                        
                        break;
                        
                    case 2:
                        // 2*DR + 1*CWB
                        roomDataModelInstance.DRCount = 1;
                        roomDataModelInstance.CWBCount = 1;
                        travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeCWB withRoomNo:roomNo withType:kTravellerTypeChild];
                        [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                        
                        travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeAdult];
                        [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                        
                        travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeAdult];
                        [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                        
                        break;
                        
                    case 3:
                        //
                        return @"This combination is not be allowed";
                        break;
                        
                    default:
                        break;
                }
            }else if (totalChildCount == childCNBCount)
            {
                NSNumber *age = childrenAgeArray[0];
                
                int ageCNBchild = 0;
                NSArray *CNBArray = [childDictArray valueForKey:@"CNB"];
                if (CNBArray.count != 0)
                {
                    NSString *ageCNBchildString = CNBArray[0];
                    if ([ageCNBchildString isKindOfClass:[NSNull class]])
                    {
                        ageCNBchildString = CNBArray[1];
                    }

                    ageCNBchild = [ageCNBchildString intValue];
                }
                
                if ([age intValue]>=2 && [age intValue] <= 5)
                {
                    switch (adultCount) {
                        case 1:
                            //2*DR
                            roomDataModelInstance.DRCount = 1;
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeChild];
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeAdult];
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            break;
                        case 2:
                            //2*DR + 1*CNB
                            roomDataModelInstance.DRCount = 1;
                            roomDataModelInstance.CNBCount = 1;
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeCNB withRoomNo:roomNo withType:kTravellerTypeChild];
                            travellerInfoModelInstance.travellerAge = ageCNBchild;
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeAdult];
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeAdult];
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            break;
                        case 3:
                            // 3*TR + 1*CNB
                            roomDataModelInstance.TRCount = 1;
                            roomDataModelInstance.CNBCount = 1;
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeCNB withRoomNo:roomNo withType:kTravellerTypeChild];
                            travellerInfoModelInstance.travellerAge = ageCNBchild;
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeTR withRoomNo:roomNo withType:kTravellerTypeAdult];
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeTR withRoomNo:roomNo withType:kTravellerTypeAdult];
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeTR withRoomNo:roomNo withType:kTravellerTypeAdult];
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            break;
                            
                        default:
                            break;
                    }
                }else if ([age intValue] >=6 && [age intValue] <= 12)
                {
                    switch (adultCount) {
                        case 1:
                            //2*DR
                            roomDataModelInstance.DRCount = 1;
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeChild];
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeAdult];
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            break;
                        case 2:
                            //2*DR + 1*CNB
                            roomDataModelInstance.DRCount = 1;
                            roomDataModelInstance.CNBCount = 1;
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeCNB withRoomNo:roomNo withType:kTravellerTypeChild];
                            travellerInfoModelInstance.travellerAge = ageCNBchild;
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeAdult];
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeAdult];
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            break;
                        case 3:
                            // 3*TR + 1*CNB
                            roomDataModelInstance.TRCount = 1;
                            roomDataModelInstance.CNBCount = 1;
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeCNB withRoomNo:roomNo withType:kTravellerTypeChild];
                            travellerInfoModelInstance.travellerAge = ageCNBchild;
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeTR withRoomNo:roomNo withType:kTravellerTypeAdult];
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeTR withRoomNo:roomNo withType:kTravellerTypeAdult];
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeTR withRoomNo:roomNo withType:kTravellerTypeAdult];
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            break;
                            
                        default:
                            break;
                    }
                }
            }
            break;
        case 2:
            if (totalChildCount == childCWBCount) {
                switch (adultCount) {
                    case 1:
                        //2*DR + 1*CWB
                        roomDataModelInstance.DRCount = 1;
                        roomDataModelInstance.CWBCount = 1;
                        travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeCWB withRoomNo:roomNo withType:kTravellerTypeChild];
                        [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                        
                        travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeChild];
                        [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                        
                        travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeAdult];
                        [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                        
                        break;
                        
                    case 2:
                        return @"This combination is not be allowed";
                        
                        break;
                        
                    default:
                        break;
                }
            }else if (totalChildCount == childCNBCount)
            {
                int age1 = [childrenAgeArray[0] intValue];
                int age2 = [childrenAgeArray[1] intValue];
                
                int ageCNBchild1 = 0;
                int ageCNBchild2 = 0;
                NSArray *CNBArray = [childDictArray valueForKey:@"CNB"];
                if (CNBArray.count != 0)
                {
                    NSString *ageCNBchildString1 = CNBArray[0];
                    ageCNBchild1 = [ageCNBchildString1 intValue];
                    
                    NSString *ageCNBchildString2 = CNBArray[1];
                    ageCNBchild2 = [ageCNBchildString2 intValue];
                }
                
                if (((age1 >=2 && age1 <= 4) && (age2 >= 2 && age2 <= 4))|| ((age1 > 4 && age1 <= 11 ) && (age2 > 4 && age2 <= 11)))
                {
                    switch (adultCount) {
                        case 1:
                           // 2*DR + 1*CNB
                            roomDataModelInstance.DRCount = 1;
                            roomDataModelInstance.CNBCount = 1;
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeCNB withRoomNo:roomNo withType:kTravellerTypeChild];
                            if (ageCNBchild1 >= 2 && ageCNBchild1 <= 4)
                            {
                                travellerInfoModelInstance.travellerAge = ageCNBchild1;
                            }else
                            {
                                travellerInfoModelInstance.travellerAge = ageCNBchild2;
                            }
                            
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeChild];
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];

                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeAdult];
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            break;
                        case 2:
                            //2*DR + 1*CWB + 1*CNB
                            roomDataModelInstance.DRCount = 1;
                            roomDataModelInstance.CWBCount = 1;
                            roomDataModelInstance.CNBCount = 1;
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeCNB withRoomNo:roomNo withType:kTravellerTypeChild];
                            if (ageCNBchild1 >= 2 && ageCNBchild1 <= 4)
                            {
                                travellerInfoModelInstance.travellerAge = ageCNBchild1;
                            }else
                            {
                                travellerInfoModelInstance.travellerAge = ageCNBchild2;
                            }
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeCWB withRoomNo:roomNo withType:kTravellerTypeChild];
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeAdult];
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeAdult];
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            break;
                        case 3:
                            return @"This combination is not be allowed";
                            break;
                        default:
                            break;
                    }
                }
                
            }
            else if (childCWBCount == 1 && childCNBCount == 1)
            {
                int ageCNBchild = 0;
                NSArray *CNBArray = [childDictArray valueForKey:@"CNB"];
                if (CNBArray.count != 0)
                {
                    NSString *ageCNBchildString = CNBArray[0];
                    if ([ageCNBchildString isKindOfClass:[NSNull class]])
                    {
                        ageCNBchildString = CNBArray[1];
                    }

                    ageCNBchild = [ageCNBchildString intValue];
                }
                
                switch (adultCount) {
                    case 1:
                        //DR + CNB
                        roomDataModelInstance.DRCount = 1;
                        roomDataModelInstance.CNBCount = 1;
                        
                        travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeCNB withRoomNo:roomNo withType:kTravellerTypeChild];
                        travellerInfoModelInstance.travellerAge = ageCNBchild;
                        [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                        
                        travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeChild];
                        [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                        
                        travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeAdult];
                        [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                        
                        break;
                    case 2:
                        // 2*DR + 1*CWB + 1*CNB
                        roomDataModelInstance.DRCount = 1;
                        roomDataModelInstance.CWBCount = 1;
                        roomDataModelInstance.CNBCount = 1;
                        travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeCWB withRoomNo:roomNo withType:kTravellerTypeChild];
                        [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                        
                        travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeCNB withRoomNo:roomNo withType:kTravellerTypeChild];
                        travellerInfoModelInstance.travellerAge = ageCNBchild;
                        [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                        
                        travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeAdult];
                        [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                        
                        travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeAdult];
                        [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                        break;
                        
                    default:
                        break;
                }
            }
            break;
            
        default:
            break;
    }
    return self;
}

@end
