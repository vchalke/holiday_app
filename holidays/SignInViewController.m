//
//  SignInViewController.m
//
//  Copyright 2012 Google Inc.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

#import "SignInViewController.h"

#import <GoogleSignIn/GoogleSignIn.h>

//#import "AuthInspectorViewController.h"
//#import "DataPickerState.h"
//#import "DataPickerViewController.h"

typedef void(^AlertViewActionBlock)(void);

@interface SignInViewController () <GIDSignInDelegate, GIDSignInUIDelegate>

@property (nonatomic, copy) void (^confirmActionBlock)(void);
@property (nonatomic, copy) void (^cancelActionBlock)(void);

@end



@implementation SignInViewController {
    
    NSDictionary *_drilldownCellState;
}

#pragma mark - View lifecycle

- (void)setUp
{
    [GIDSignInButton class];
    
    GIDSignIn *signIn = [GIDSignIn sharedInstance];
    signIn.shouldFetchBasicProfile = YES;
    signIn.delegate = self;
    signIn.uiDelegate = self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self setUp];
        
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setUp];
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    

    [self.navigationController setNavigationBarHidden:NO];
    
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
}

#pragma mark - GIDSignInDelegate

- (void)signIn:(GIDSignIn *)signIn
didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error
{
    
        NSLog(@"didSignInForUser %@",error);
    if (error) {
        [self.navigationController popViewControllerAnimated:YES];
        [self.googleViewDelegate didFinishWithGoogleLogin:@"error"];
        
        
        return;
    }
    [self reportAuthStatus];
    
}

- (void)signIn:(GIDSignIn *)signIn
didDisconnectWithUser:(GIDGoogleUser *)user
     withError:(NSError *)error
{
    NSLog(@"didDisconnectWithUser %@",error);
}

- (void)presentSignInViewController:(UIViewController *)viewController {
    
    
    [[self navigationController] pushViewController:viewController animated:YES];
}

#pragma mark - Helper methods


- (void)reportAuthStatus {
    
    
    GIDGoogleUser * googleUser = [[GIDSignIn sharedInstance] currentUser];
    
    if (googleUser.authentication) {
        GIDGoogleUser * user = [GIDSignIn sharedInstance].currentUser;
        NSString * userEmail = user.profile.email;
        NSString *userId = user.userID;
         NSString *givenName = user.profile.givenName;
         NSString *imageURLString;
         if ([GIDSignIn sharedInstance].currentUser.profile.hasImage)
         {
             NSUInteger dimension = round(50 * [[UIScreen mainScreen] scale]);
             NSURL *imageURL = [user.profile imageURLWithDimension:dimension];
             imageURLString=[imageURL absoluteString];
         }
        NSDictionary *userDict =@{
            @"first_name":givenName,
            @"email":userEmail,
            @"login_with":@"3",
            @"g_id":userId,
            @"gender":@"",
            @"bmonth":@"",
            @"bdate":@"",
            @"byear":@"",
            @"mobile_no":@"",
            @"user_profile_pic":imageURLString,
            @"platform": @"1"
        };
        if (userEmail != nil) {
//         VJ_Commented
            /*
            UIAlertController * alert=   [UIAlertController
                                          alertControllerWithTitle:@"Google Sign In"
                                          message:[NSString stringWithFormat:@" User ID : %@",userEmail]
                                          preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:@"OK"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                     
                                     
                                        [self.googleViewDelegate didFinishWithGoogleLogin:userEmail];
                                     
                                  
                                     
                                 }];
          
            
            [alert addAction:ok];
        
            
            [self presentViewController:alert animated:YES completion:nil];
            */
//         VJ_Added
            [self showButtonsInAlertWithTitle:@"Google Sign In" msg:[NSString stringWithFormat:@" User ID : %@",userEmail] style:UIAlertControllerStyleAlert buttArray:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString * _Nonnull strValue) {
                if ([strValue isEqualToString:@"Ok"]){
                    [self.navigationController popViewControllerAnimated:YES];
                    [self.googleViewDelegate didFinishWithGoogleLogin:userEmail];
//                       [self.googleViewDelegate didFinishWithGoogleLoginWithuserData:userDict];
                }
            }];
            
        }
        else
        {
            [self.navigationController popViewControllerAnimated:YES];
            [self.googleViewDelegate didFinishWithGoogleLogin:@"error"];
        }
        
    }
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
        [self.googleViewDelegate didFinishWithGoogleLogin:@"error"];
    }
    
    
}



- (void)signInWillDispatch:(GIDSignIn *)signIn error:(NSError *)error
{
    //  [myActivityIndicator stopAnimating];
}

// Present a view that prompts the user to sign in with Google
- (void)signIn:(GIDSignIn *)signIn presentViewController:(UIViewController *)viewController
{
    //  [self presentViewController:viewController animated:YES completion:nil];
    [self presentViewController:viewController animated:YES completion:nil];
    
}

// Dismiss the "Sign in with Google" view
- (void)signIn:(GIDSignIn *)signIn
dismissViewController:(UIViewController *)viewController
{
    //[self dismissViewControllerAnimated:YES completion:nil];
    [self dismissViewControllerAnimated:YES completion:nil];
    
    
}

@end
