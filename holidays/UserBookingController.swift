//
//  UserBookingController.swift
//  sotc-consumer-application
//
//  Created by Saurav Kumar on 22/08/17.
//  Copyright © 2017 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit
import CoreData


class UserBookingController: NSObject {
    
    
    //MARK: - SOCTC XML Request
    
    class  func sendGetBFNListRequest(mobileNumber:String , completion: @escaping (_ operationValidationMessage:OperationValidationMessage) -> Void)   {
        
        
        let requestString  = XMLParserRequestBuilder.getBFNListSoapRequestString(mobileNumber: mobileNumber)
        
        
        let loginNetworkManager:UserBookingNetworkManager = UserBookingNetworkManager()
        
        
       // loginNetworkManager.sendSoapRequest(soapRequestString: requestString ,soapAction: "getBFNList") { (operationValidationMessage) in
         loginNetworkManager.sendSoapRequestMiddleware(soapRequestString: requestString ,soapAction: "getBFNList", requestKey: "BOOKING_FILE_LIST_RESPONSE") { (operationValidationMessage) in
            
            var operationMessage =  operationValidationMessage
            
            
            if !operationMessage.status
                
            {
                
                completion(operationMessage)
            }
            else
            {
                
                do {
                    
//                    let bfnListSoapResponse = try AEXMLDocument(xml: operationValidationMessage.data.replacingOccurrences(of: "&", with: "AND"))
                    let bfnListSoapResponse = try AEXMLDocument(xml: operationValidationMessage.data)
                    
                    
                    printLog(bfnListSoapResponse["BOOKING_FILE_LIST_RESPONSE"]["RESP_CODE"].string )
                    
                    printLog(bfnListSoapResponse["BOOKING_FILE_LIST_RESPONSE"]["RESP_DESC"].string )
                    
                    if !(bfnListSoapResponse["BOOKING_FILE_LIST_RESPONSE"]["RESP_CODE"].string.isEmpty) && (bfnListSoapResponse["BOOKING_FILE_LIST_RESPONSE"]["RESP_CODE"].string == "0") && (bfnListSoapResponse["BOOKING_FILE_LIST_RESPONSE"]["RESP_DESC"].string.lowercased() == "SUCCESS".lowercased() )
                    {
                        
                      //  DispatchQueue.main.sync {
                        
                        UserBookingDBManager.saveBFNList(bfnListResponse: bfnListSoapResponse)
                        
                        operationMessage.status = true
                        
                        completion(operationMessage)
                            return
                      //  }
                       
                        
                        
                    }
                    else
                    {
                        
                        operationMessage.status = false
                        
                        completion(operationMessage)
                         return
                    }
                    
                    
                } catch   {
                    
                    
                    printLog(error)
                    operationMessage.message = error.localizedDescription
                    operationMessage.status = false
                    
                    completion(operationValidationMessage)
                     return
                }
                
            }
            
        }
        
        
    }
    
    class  func sendGetBFNDetailRequest(bfNumber:String , completion: @escaping (_ operationValidationMessage:OperationValidationMessage) -> Void)   {
        
        let requestString  = XMLParserRequestBuilder.getBFNDetailSoapRequestString(bfNumber: bfNumber)
        
        
        let loginNetworkManager:UserBookingNetworkManager = UserBookingNetworkManager()
        
        
       // loginNetworkManager.sendSoapRequest(soapRequestString: requestString , soapAction: "getBookingDetails") { (operationValidationMessage) in
           loginNetworkManager.sendSoapRequestMiddleware(soapRequestString: requestString , soapAction: "getBookingDetails", requestKey:"BOOKING_DETAILS_RESPONSE") { (operationValidationMessage) in
            
            var operationMessage =  operationValidationMessage
            
            
            if !operationMessage.status
                
            {
                completion(operationMessage)
                return
            }
            else
            {
                
                do {
                    
                    let bfnListDetailsSoapResponse = try AEXMLDocument(xml: operationValidationMessage.data.replacingOccurrences(of: "&", with: "&amp;", options: .literal))
                    
                    
                    
                    printLog(bfnListDetailsSoapResponse["BOOKING_DETAILS_RESPONSE"]["RESP_CODE"].string )
                    
                    printLog(bfnListDetailsSoapResponse["BOOKING_DETAILS_RESPONSE"]["RESP_DESC"].string )
                    
                    if !(bfnListDetailsSoapResponse["BOOKING_DETAILS_RESPONSE"]["RESP_CODE"].string.isEmpty) && (bfnListDetailsSoapResponse["BOOKING_DETAILS_RESPONSE"]["RESP_CODE"].string == "0") && (bfnListDetailsSoapResponse["BOOKING_DETAILS_RESPONSE"]["RESP_DESC"].string.lowercased() == "SUCCESS".lowercased() )
                    {
                         //DispatchQueue.main.sync {
                        
                        
                        let messageDB:String = UserBookingDBManager.saveBFNListDetails(bfnListDetailsResponse: bfnListDetailsSoapResponse,bfNumber: bfNumber)
                        
                        printLog(messageDB)
                        
                        operationMessage.status = true
                        
                        operationMessage.message = messageDB
                        
                        completion(operationMessage)
                            return
                       
                      //  }
                        
                    }
                    else
                    {
                        operationMessage.status = false
                        
                        completion(operationMessage)
                         return
                    }
                    
                    
                } catch   {
                    
                    
                    printLog(error)
                    
                    operationMessage.status = false
                    
                    operationMessage.message = error.localizedDescription
                    
                    completion(operationMessage)
                     return
                    
                }
            }
            
            
        }
        
        
    }
    
    
    class  func sendGetBFNOptionalRequest(bfNumber:String , completion: @escaping (_ operationValidationMessage:OperationValidationMessage) -> Void)   {
        
        let requestString  = XMLParserRequestBuilder.getBFNOptionalPackageSoapRequestString(bfNumber: bfNumber)
        
        
        let tourNetworkManager:UserBookingNetworkManager = UserBookingNetworkManager()
        
        
       // tourNetworkManager.sendSoapRequest(soapRequestString: requestString , soapAction: "getBFNOptional") { (operationValidationMessage) in
         tourNetworkManager.sendSoapRequestMiddleware(soapRequestString: requestString , soapAction: "getBFNOptional", requestKey: "BOOKING_OPTIONAL_RESPONSE") { (operationValidationMessage) in
            
             // DispatchQueue.main.sync {
            var operationMessage = operationValidationMessage
            
            if !operationMessage.status
                
            {
                
                completion(operationMessage)
                return
            }
            else
            {
                
                do {
                    
                    let bfnOptionalDetailsSoapResponse = try AEXMLDocument(xml: operationValidationMessage.data.replacingOccurrences(of: "&", with: "&amp;", options: .literal))
                    
                    
                    
                    printLog(bfnOptionalDetailsSoapResponse["BOOKING_OPTIONAL_RESPONSE"]["RESP_CODE"].string )
                    
                    printLog(bfnOptionalDetailsSoapResponse["BOOKING_OPTIONAL_RESPONSE"]["RESP_DESC"].string )
                    
                    if !(bfnOptionalDetailsSoapResponse["BOOKING_OPTIONAL_RESPONSE"]["RESP_CODE"].string.isEmpty) && (bfnOptionalDetailsSoapResponse["BOOKING_OPTIONAL_RESPONSE"]["RESP_CODE"].string == "0") && (bfnOptionalDetailsSoapResponse["BOOKING_OPTIONAL_RESPONSE"]["RESP_DESC"].string.lowercased() == "SUCCESS".lowercased() )
                    {
                        
                       
                        
                        UserBookingDBManager.saveBFNOptionalDetails(bfnOptionalDetailsResponse: bfnOptionalDetailsSoapResponse, bfNumber: bfNumber)
                        
                        operationMessage.status = true
                        
                        completion(operationMessage)
                        return
                        
                        
                        
                    }
                    else
                    {
                        operationMessage.status = false
                        completion(operationMessage)
                        return
                    }
                    
                    
                } catch   {
                    
                    
                    
                    
                    operationMessage.message = error.localizedDescription
                    operationMessage.status = false
                    
                    
                    printLog(error)
                    
                    completion(operationMessage)
                    return
                }
            }
            
            
      //  }
        
        
    }
    }
    
    class  func sendNotificationRequest(bfNumber:String , completion: @escaping (_ operationValidationMessage:OperationValidationMessage) -> Void)   {
        
        let requestString  = XMLParserRequestBuilder.getNotificationRequestString(bfNumber: bfNumber)
        
        
        let loginNetworkManager:UserBookingNetworkManager = UserBookingNetworkManager()
        
        
     //   loginNetworkManager.sendSoapRequest(soapRequestString: requestString ,soapAction: "setNotificationFlag") { (operationValidationMessage) in
            loginNetworkManager.sendSoapRequestMiddleware(soapRequestString: requestString ,soapAction: "setNotificationFlag", requestKey: "SET_NOTIFICATION_RESPONSE") { (operationValidationMessage) in
            
            
            var operationMessage = operationValidationMessage
            
            if !operationMessage.status
                
            {
                
                completion(operationMessage)
                return
            }
            else
            {
                
                do {
                    
                    let bfnListSoapResponse = try AEXMLDocument(xml: operationValidationMessage.data)
                    
                    
                    
                    printLog(bfnListSoapResponse["SET_NOTIFICATION_RESPONSE"]["RESP_CODE"].string )
                    
                    printLog(bfnListSoapResponse["SET_NOTIFICATION_RESPONSE"]["RESP_DESC"].string )
                    
                    if !(bfnListSoapResponse["SET_NOTIFICATION_RESPONSE"]["RESP_CODE"].string.isEmpty) && (bfnListSoapResponse["SET_NOTIFICATION_RESPONSE"]["RESP_CODE"].string == "0") && (bfnListSoapResponse["SET_NOTIFICATION_RESPONSE"]["RESP_DESC"].string.lowercased() == "SUCCESS".lowercased() )
                    {
                        
                        operationMessage.status = true
                        
                        
                        completion(operationMessage)
                        return
                        
                        
                    }
                    else
                    {
                        operationMessage.status = false
                        completion(operationMessage)
                        return
                    }
                    
                    
                } catch   {
                    
                    operationMessage.status = false
                    operationMessage.message = error.localizedDescription
                    
                    printLog(error)
                    
                    completion(operationMessage)
                    return
                    
                }
                
            }
            
        }
        
        
    }
    
    
    class  func sendPaymentReceiptGenerationRequest(bfNumber:String,paymentId:String,amount:String,bookingFileName:String,reciptGenerationFXDetails:(isFX:Bool,fxCurrencyType:String,roeRate:String,fxAmount:String,isA2Attached:Bool), completion: @escaping (_ operationValidationMessage:OperationValidationMessage) -> Void)   {
        
        let requestString  = XMLParserRequestBuilder.getPaymentReceiptGenerationSoapRequestString(bfNumber: bfNumber, paymentId: paymentId, amount: amount, bookingFileName: bookingFileName,reciptGenerationFXDetails: reciptGenerationFXDetails)
        
        
        let tourNetworkManager:UserBookingNetworkManager = UserBookingNetworkManager()
        
        
       // tourNetworkManager.sendSoapRequest(soapRequestString: requestString , soapAction: "getLtPortalReciept") { (operationValidationMessage) in
        tourNetworkManager.sendSoapRequestMiddleware(soapRequestString: requestString ,soapAction: "getLtPortalReciept", requestKey: "LT_PORTAL_RECEIPT_RESP") { (operationValidationMessage) in
            
            
            var operationMessage = operationValidationMessage
            
            if !operationMessage.status
                
            {
                
                completion(operationMessage)
                return
            }
            else
            {
                
                do {
                    
                    let bfnOptionalDetailsSoapResponse = try AEXMLDocument(xml: operationValidationMessage.data.replacingOccurrences(of: "&", with: "&amp;", options: .literal))
                    
                    
                    
                    printLog(bfnOptionalDetailsSoapResponse["LT_PORTAL_RECEIPT_RESP"]["RESP_CODE"].string )
                    
                    printLog(bfnOptionalDetailsSoapResponse["LT_PORTAL_RECEIPT_RESP"]["RESP_DESC"].string )
                    
                    if !(bfnOptionalDetailsSoapResponse["LT_PORTAL_RECEIPT_RESP"]["RESP_CODE"].string.isEmpty) && (bfnOptionalDetailsSoapResponse["LT_PORTAL_RECEIPT_RESP"]["RESP_CODE"].string == "0") && (bfnOptionalDetailsSoapResponse["LT_PORTAL_RECEIPT_RESP"]["RESP_DESC"].string.lowercased() == "SUCCESS".lowercased() )
                    {
                        
                        operationMessage.status = true
                        operationMessage.data = bfnOptionalDetailsSoapResponse["LT_PORTAL_RECEIPT_RESP"]["SUCCESS"]["RECEIPT_NO"].string
                        
                        completion(operationMessage)
                        return
                    }
                    else
                    {
                        operationMessage.status = false
                        
                        completion(operationMessage)
                        return
                    }
                    
                    
                } catch   {
                    
                    
                    printLog(error)
                    
                    
                    operationMessage.status = false
                    
                    operationMessage.message = error.localizedDescription
                    
                    
                    completion(operationMessage)
                    return
                    
                }
            }
            
            
        }
        
        
    }
    
    class func sendBookedOptionalDetailsRequest(bfNumber:String,bookedOptionalDetails: Array<Dictionary<String, String>>,completion: @escaping (_ operationValidationMessage:OperationValidationMessage) -> Void){
        
        let requestString  = XMLParserRequestBuilder.getBookingOptionalRequestString(bfNumber: bfNumber, bookedOptionalDetails: bookedOptionalDetails)
        
        let loginNetworkManager:UserBookingNetworkManager = UserBookingNetworkManager()
        
        //loginNetworkManager.sendSoapRequest(soapRequestString: requestString ,soapAction: "bookOptional") { (operationValidationMessage) in
         loginNetworkManager.sendSoapRequestMiddleware(soapRequestString: requestString ,soapAction: "bookOptional", requestKey: "BOOK_OPTIONAL_RESPONSE") { (operationValidationMessage) in
            
            
            var operationMessage = operationValidationMessage
            
            if !operationValidationMessage.status
                
            {
                
                completion(operationValidationMessage)
                return
            }
            else
            {
                
                do {
                    
                    let bfnListSoapResponse = try AEXMLDocument(xml: operationValidationMessage.data)
                    
                    
                    
                    printLog(bfnListSoapResponse["BOOK_OPTIONAL_RESPONSE"]["RESP_CODE"].string )
                    
                    printLog(bfnListSoapResponse["BOOK_OPTIONAL_RESPONSE"]["RESP_DESC"].string )
                    
                    if !(bfnListSoapResponse["BOOK_OPTIONAL_RESPONSE"]["RESP_CODE"].string.isEmpty) && (bfnListSoapResponse["BOOK_OPTIONAL_RESPONSE"]["RESP_CODE"].string == "0") && (bfnListSoapResponse["BOOK_OPTIONAL_RESPONSE"]["RESP_DESC"].string.lowercased() == "SUCCESS".lowercased() )
                    {
                        
                        operationMessage.status = true
                        
                        completion(operationMessage)
                        return
                    }
                    else
                    {
                        operationMessage.status = false
                        
                        completion(operationMessage)
                        return
                    }
                    
                    
                } catch   {
                    
                    
                    printLog(error)
                    
                    operationMessage.status = false
                    completion(operationMessage)
                    return
                }
                
            }
            
        }
        
    }
    
    
    
        
    
    //Mark:- TC-CCS get session and request token
    
    class func getASTRASessionAndTockenID(completion:@escaping(_ status:Bool,_ sessionAndTockenDictData:Dictionary<String,Any>?)->Void) {
        
        let UUID:String = AppUtility.setUUID()
        
        let headers: HTTPHeaders = [
            "content-type": "application/json",
            APIConstants.UniqueId:UUID,
            APIConstants.User:APIConstants.USER_VALUE
        ]
        
        let urlString:String = kbaseURLForAstra + "tcCommonRS/extnrt/getNewRequestToken"
        
         request(urlString, method: .get, parameters: nil, encoding: JSONEncoding.default,headers:headers).responseJSON { response in
            
            printLog("Request: \(String(describing: response.request))")   // original url request
            printLog("Response: \(String(describing: response.response))") // http url response
            printLog("Result: \(response.result)")
            
            if let json = response.result.value {
                printLog("JSON: \(json)") // serialized json response
                
                if let responseData:Dictionary<String,Any> = json as? Dictionary<String,Any>
                {
                    if let requestId = responseData["requestId"] as? String , !requestId.isEmpty {
                        
                        if let tokenId = responseData["tokenId"] as? String , !tokenId.isEmpty {
                            UserDefaults.standard.set(requestId, forKey: kLoginRequestID)
                            UserDefaults.standard.set(tokenId, forKey: kLoginTokenID)
                            completion(true,responseData)
                            return
                        }
                        
                    }
                }
                
                completion(false,nil)
                return
            }
            
            completion(false,nil)
            return
            
        }
    }
    
    
    ////
    
    class func getCurrencyByCode(currencyCode:String,requestId:String,sessionId:String,completion:@escaping(_ status:Bool,_ currencyData:CurrencyData?)->Void) {
        
        
        
        let urlString:String = kbaseURLForAstra + "tcHolidayRS/roe/getCurrencyByCode/"  + currencyCode
        
        let headers: HTTPHeaders = [
            "content-type": "application/json",
            "Accept": "application/json",
            APIConstants.RequestID:requestId,
            APIConstants.SessionId:sessionId
        ]
        printLog("url\(urlString)")
        
        request(urlString, method: .get, parameters: nil, encoding: JSONEncoding.default,headers:headers).responseJSON { response in
            
            printLog("Request: \(String(describing: response.request))")   // original url request
            printLog("Response: \(String(describing: response.response))") // http url response
            printLog("Result: \(response.result)")
            
            if let json = response.result.value {
                printLog("JSON: \(json)") // serialized json response
                
                if let responseData:Dictionary<String,Any> = json as? Dictionary<String,Any>
                {
                    if var currencyData:CurrencyData = UserBookingDBManager.parseCurrencyByCode(curencyByCodeResponseData: responseData)
                    {
                        currencyData.currencyCode = currencyCode
                        completion(true,currencyData)
                        return
                    }
                }
            }
            
            completion(false,nil)
            return
            
        }
    }
    
    
    
    //MARK :- TC-CCS MiddleWare
    
    class func sendPayNowRequest(requestData:Dictionary<String,Any>,completion : @escaping(_ status:Bool,_ message:String) -> Void) {
        
        
        let requestId  : String =  UserDefaults.standard.value(forKey: "userDefaultRequestId")as! String
        
        let sessionId : String =  UserDefaults.standard.value(forKey: "userDefaultTokenId") as! String
        
        let headers: HTTPHeaders = [
            "content-type": "application/json",
            "Accept":"application/json",
            "requestId":requestId ,
            "sessionId":sessionId
        ]
        
        
        request(AuthenticationConstant.TC_CSS_PAY_NOW_URL, method: .post, parameters: requestData, encoding: JSONEncoding.default,headers: headers).responseJSON { response in
            
            printLog("Request: \(String(describing: response.request))")   // original url request
            printLog("Request Data: \(String(describing: requestData))")   // original url request
            printLog("Response: \(String(describing: response.response))") // http url response
            printLog("Result: \(response.result)")                         // response serialization result
            if response.response?.statusCode
                == 200
            {
                
                if let json = response.result.value {
                    printLog("JSON: \(json)") // serialized json response
                    
                    
                    if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                        
                        printLog("Data: \(utf8Text)") // original server data as UTF8 string
                        
                        if let json = response.result.value {
                            
                            if let responseDict:Dictionary<String,Any> = json as? Dictionary<String,Any>
                            {
                                if let resultValue:Bool = responseDict["success"] as? Bool , resultValue
                                {
                                    
                                    if let paymentLink:String = responseDict["paymentLink"] as? String
                                    {
                                        completion(true,paymentLink)
                                        return
                                    }else{
                                        
                                        completion(false,AlertMessage.PAYMENT_FAILED)
                                         return
                                    }
                                    
                                }else{
                                    completion(false,AlertMessage.PAYMENT_FAILED)
                                    return
                                }
                                
                            }
                            else{
                                completion(false,AlertMessage.PAYMENT_FAILED)
                                 return
                            }
                        }
                        else{
                            completion(false,AlertMessage.PAYMENT_FAILED)
                             return
                        }
                    }
                    else{
                        completion(false,AlertMessage.PAYMENT_FAILED)
                         return
                    }
                                        
                }
                else{
                   completion(false,AlertMessage.PAYMENT_FAILED)
                    return
                }
            }else
            {
                completion(false,NetworkValidationMessages.InternetNotConnected.message)
                 return
                
            }
            
        }
        
    }
    
    
    
    //MARK: - MOBICULE JSON Request
    
    class func getUserBookedImagesAndCityCovered(bookList:Array<Tour>,completion : @escaping(_ status:Bool) -> Void) {
        
        
        var prodITINCodeArray:Array<String> = []
        
        for tour in bookList
        {
            printLog(tour.bfNumber!)
            printLog(tour.departureDate!)
            
            prodITINCodeArray.append(tour.prodITINCode!)
            
            
        }
        
        let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        
        let data:Array<String> = []
        
        //var itienearyCode:Array<String> = ["2017EUR103S1"]
        
        let  queryParameterMap:Dictionary<String,Any> = [APIConstants.LtItineraryCode:prodITINCodeArray]
        
        let userDetails:Dictionary<String,Any> = [APIConstants.CheckVersionKey:"false",
                                                  APIConstants.Client:JsonConstant.IOS_CLIENT,
                                                  APIConstants.AppVersion:version!]
        
        let requestDict:Dictionary<String,Any> = [ APIConstants.EntityType:"transaction",
                                                   APIConstants.EntityAction:"get",
                                                   APIConstants.Entity:"packageDetails",
                                                   "identifier":"packageDetails",
                                                   APIConstants.User:userDetails,"data":data,"queryParameterMap":queryParameterMap]
        
        
        let requestString = requestDict.jsonRepresentation()
        
        printLog("requestString:\(requestString)")
        
        
        let digest:String = NetworkUtility().generateDigestValue(requestString, withPackageName: "com.mobicule")
        printLog("digest:\(digest)")
        
        let headers: HTTPHeaders = [
            "content-type": "application/json",
            "Authorization": AuthenticationConstant.Mobicule_Authorization,
            "digest":digest
        ]
        
        request(AuthenticationConstant.MOBICULE_URL, method: .post, parameters: requestDict, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            
            printLog("Request: \(String(describing: response.request))")   // original url request
            printLog("Response: \(String(describing: response.response))") // http url response
            printLog("Result: \(response.result)")                         // response serialization result
            
            if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                printLog("Data: \(utf8Text)") // original server data as UTF8 string
            }
            
            
            if let json = response.result.value {
                printLog("JSON: \(json)") // serialized json response
                
                if let responseData:Dictionary<String,Any> = json as? Dictionary<String,Any>
                {
                    printLog(responseData["data"])
                    
                    completion(UserBookingDBManager.saveUserBookedImageAndCityCovered(bookingTourList:bookList, packageDetailsResponseData: responseData))
                    
                    return
                    
                }
                
            }
            completion(false)
            return
        }
    }
    
    
    class func getItienearyImagesAndCityCovered(bfNumber:String,packageIdArray:Array<String>,completion : @escaping(_ status:Bool) -> Void) {
        
        
        let data:Array<String> = []
        
        let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        
        //  var itienearyCode:Array<String> = ["PKG000014"]
        
        let  queryParameterMap:Dictionary<String,Any> = ["packageId":packageIdArray]
        
        let userDetails:Dictionary<String,Any> = [APIConstants.CheckVersionKey:"false",
                                                  APIConstants.Client:JsonConstant.IOS_CLIENT,
                                                  APIConstants.AppVersion:"12.0"]
        
        let requestDict:Dictionary<String,Any> = [ APIConstants.EntityType:"transaction",
                                                   APIConstants.EntityAction:"get",
                                                   APIConstants.Entity:"itinerary",
                                                   "identifier":"itinerary",
                                                   APIConstants.User:userDetails,"data":data,"queryParameterMap":queryParameterMap]
        
        let requestString = requestDict.jsonRepresentation()
        
        
        
        
        let digest:String = NetworkUtility().generateDigestValue(requestString, withPackageName: "com.mobicule")
        printLog("digest:\(digest)")
        
        
        let headers: HTTPHeaders = [
            "content-type": "application/json",
            "Authorization": AuthenticationConstant.Mobicule_Authorization,
            "digest":digest
        ]
        
        request(AuthenticationConstant.MOBICULE_URL, method: .post, parameters: requestDict, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            
            printLog("Request: \(String(describing: response.request))")   // original url request
            printLog("Response: \(String(describing: response.response))") // http url response
            printLog("Result: \(response.result)")                         // response serialization result
            
            if let json = response.result.value {
                printLog("JSON: \(json)") // serialized json response
                
                if let responseData:Dictionary<String,Any> = json as? Dictionary<String,Any>
                {
                    printLog(responseData["data"])
                    
                   // DispatchQueue.main.sync {
                    
                    completion(UserBookingDBManager.saveUserItienearyImageAndItienearyDetails(bfNumber: bfNumber, itienearyDetailsResponseData: responseData))
                    return
                    
                  //  }
                    
                    
                }
                
                if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                    printLog("Data: \(utf8Text)") // original server data as UTF8 string
                }
            }else{
                
                completion(false)
                return
                
            }
            
        }
    }
    
    // VJ_Added_Start Below Three To Four Functions
    
    class func getcheckDiscountService(destinationName:String,completion : @escaping(_ status:Bool,_ responseData:Dictionary<String,Any>) -> Void) {
        
        
        let data:Array<String> = []
        
        let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        
        
        
        let  queryParameterMap:Dictionary<String,Any> = ["destinationName":destinationName.lowercased()]
        
        let userDetails:Dictionary<String,Any> = [APIConstants.CheckVersionKey:"false",
                                                  APIConstants.Client:JsonConstant.IOS_CLIENT,
                                                  APIConstants.AppVersion:version!]
        
        let requestDict:Dictionary<String,Any> = [ APIConstants.EntityType:"transaction",
                                                   APIConstants.EntityAction:"get",
                                                   APIConstants.Entity:"discountAmount",
                                                   "identifier":"getDiscountValue",
                                                   APIConstants.User:userDetails,"data":data,"queryParameterMap":queryParameterMap]
        
        let requestString = requestDict.jsonRepresentation()
        
        
        
        
        let digest:String = NetworkUtility().generateDigestValue(requestString, withPackageName: "com.mobicule")
        printLog("digest:\(digest)")
        
        let headers: HTTPHeaders = [
            "content-type": "application/json",
            "Authorization": AuthenticationConstant.Mobicule_Authorization,
            "digest":digest
        ]
        
        
        
        printLog("\(requestString)")
        request(AuthenticationConstant.MOBICULE_URL, method: .post, parameters: requestDict, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            
            printLog("Request: \(String(describing: response.request))")   // original url request
            printLog("Response: \(String(describing: response.response))") // http url response
            printLog("Result: \(response.result)")                         // response serialization result
            
            let responseDatas:Dictionary<String,Any> = [:]
            if let json = response.result.value {
                printLog("JSON: \(json)") // serialized json response
                
                if let responseData:Dictionary<String,Any> = json as? Dictionary<String,Any>
                {
                    printLog(responseData["data"] ?? " ")
                    completion(true,responseData)
                    
                }
                
                if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                    printLog("Data: \(utf8Text)") // original server data as UTF8 string
                }
            }
            else
            {
                completion(false,responseDatas)
            }
        }
    }
    
    //
    
    
    class func getcheckDiscountAplied(bfnumber:String,completion : @escaping(_ status:Bool,_ message:String) -> Void) {
        
        
        
        
        let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        
        
        
        let  queryParameterMap:Dictionary<String,Any> = [:]
        
        let  data:Dictionary<String,Any> = ["bookingId":bfnumber]
//        let  data:Dictionary<String,Any> = ["bookingId":"212"] // TEST For Three DIGIT on 25Aug20
        let dataArr:[Dictionary<String,Any>] = [data];
        
        let userDetails:Dictionary<String,Any> = [APIConstants.CheckVersionKey:"false",
                                                  APIConstants.Client:JsonConstant.IOS_CLIENT,
                                                  APIConstants.AppVersion:version!]
        
        let requestDict:Dictionary<String,Any> = [ APIConstants.EntityType:"transaction",
                                                   APIConstants.EntityAction:"add",
                                                   APIConstants.Entity:"checkDiscountService",
                                                   
                                                   "data":dataArr,"queryParameterMap":queryParameterMap]
        
        let requestString = requestDict.jsonRepresentation()
        
        
        
        
        let digest:String = NetworkUtility().generateDigestValue(requestString, withPackageName: "com.mobicule")
        printLog("digest:\(digest)")
        
        let headers: HTTPHeaders = [
            "content-type": "application/json",
            "Authorization": AuthenticationConstant.Mobicule_Authorization,
            "digest":digest
        ]
        
        
        
        printLog("\(requestString)")
        request(AuthenticationConstant.MOBICULE_URL, method: .post, parameters: requestDict, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            
            printLog("Request: \(String(describing: response.request))")   // original url request
            printLog("Response: \(String(describing: response.response))") // http url response
            printLog("Result: \(response.result)")                         // response serialization result
            
            if let json = response.result.value {
                printLog("JSON: \(json)") // serialized json response
                
                if let responseData:Dictionary<String,Any> = json as? Dictionary<String,Any>
                {
                    printLog(responseData["data"] ?? " ")
                    let message:String = responseData["message"] as? String ?? " "
                    
                    completion(true,message)
                    
                }
                
                if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                    printLog("Data: \(utf8Text)") // original server data as UTF8 string
                }
            }
            else
            {
                
            }
        }
    }
    
    
    class func getSubmittedDiscount(bfnumber:String,completion : @escaping(_ status:Bool) -> Void) {
        
        
        //let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        
        
        let  queryParameterMap:Dictionary<String,Any> = [:]
        
        let  data:Dictionary<String,Any> = ["bookingId":bfnumber, "flag":"T"]
        
        let dataArr:[Dictionary<String,Any>] = [data];
        
        
        
        let requestDict:Dictionary<String,Any> = [ APIConstants.EntityType:"transaction",
                                                   APIConstants.EntityAction:"add",
                                                   APIConstants.Entity:"checkDiscountService",
                                                   "data":dataArr,"queryParameterMap":queryParameterMap]
        
        let requestString = requestDict.jsonRepresentation()
        
        
        
        
        let digest:String = NetworkUtility().generateDigestValue(requestString, withPackageName: "com.mobicule")
        printLog("digest:\(digest)")
        
        let headers: HTTPHeaders = [
            "content-type": "application/json",
            "Authorization": AuthenticationConstant.Mobicule_Authorization,
             "digest":digest
        ]
        
        
        
        printLog("\(requestString)")
        request(AuthenticationConstant.MOBICULE_URL, method: .post, parameters: requestDict, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            
            printLog("Request: \(String(describing: response.request))")   // original url request
            printLog("Response: \(String(describing: response.response))") // http url response
            printLog("Result: \(response.result)")                         // response serialization result
            
            if let json = response.result.value {
                printLog("JSON: \(json)") // serialized json response
                
                if let responseData:Dictionary<String,Any> = json as? Dictionary<String,Any>
                {
                    printLog(responseData["data"] ?? " ")
                    completion(true)
                    
                }
                
                
            }
            else
            {
                completion(false)
            }
        }
    }
    
    // VJ_Added_End
    class func sendEmailRequest(emailRequestData:Dictionary<String,Any>,completion : @escaping(_ status:Bool,_ message:String) -> Void) {
        
        let userDetails:Dictionary<String,Any> = [APIConstants.CheckVersionKey:"false",
                                                  APIConstants.Client:JsonConstant.IOS_CLIENT,
                                                  APIConstants.AppVersion:"12.0"]
        
        let requestDict:Dictionary<String,Any> =    [ APIConstants.EntityType:"transaction",
                                                      APIConstants.EntityAction:"get",
                                                      APIConstants.Entity:"sendMail",
                                                      APIConstants.User:userDetails,
                                                      APIConstants.Data:[emailRequestData]]
        
        
        
        
        printLog(requestDict)
        
        
        let reqeustBoday:String = requestDict.jsonRepresentation()
        printLog(reqeustBoday)
        
        let digest:String = NetworkUtility().generateDigestValue(reqeustBoday,withPackageName:"com.mobicule")
        printLog("digest\(digest)")
        
        let headers: HTTPHeaders = [
            "content-type": "application/json",
            "Accept": "application/json",
            "digest": digest,
            "Authorization": AuthenticationConstant.Mobicule_Authorization,
            //APIConstants.UniqueId:UUID,
            //APIConstants.User:APIConstants.USER_VALUE
        ]
        
        
        
        
        
        if (NetworkReachabilityManager.init()?.isReachable)!
        {
            
            
            let manager = SessionManager.default
            manager.session.configuration.timeoutIntervalForRequest = 180
            
            
            request(AuthenticationConstant.MOBICULE_URL, method: .post, parameters: requestDict, encoding: JSONEncoding.default,headers: headers).responseJSON { response in
                
                printLog("Request: \(String(describing: response.request))")   // original url request
                printLog("Response: \(String(describing: response.response))") // http url response
                printLog("Result: \(response.result)")                         // response serialization result
                
                if let json = response.result.value {
                    printLog("JSON: \(json)") // serialized json response
                    
                    if let responseData:Dictionary<String,Any> = json as? Dictionary<String,Any>
                    {
                        
                        printLog(responseData["message"] ?? "")
                        
                        let message = responseData["message"] as? String ?? ""
                        
                        if let status:String = responseData["status"] as? String ?? ""
                        {
                            if  status != "" && status.lowercased() == "success"
                            {
                                completion(true,message)
                                return
                            }
                            
                        }
                        
                        completion(false,message)
                        return
                        
                    }
                    
                    if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                        printLog("Data: \(utf8Text)") // original server data as UTF8 string
                    }
                    
                    
                }
                
                completion(false,"Unable to send your request please try again")
                return
                
            }
            
            
        }else{
            
            completion(false,AlertMessage.CHECK_NET_CONNECTION)
            return
        }
        
       
    }
    
    class func sendOptionalEmailRequest(emailRequestData:Dictionary<String,Any>,completion : @escaping(_ status:Bool,_ message:String) -> Void) {
        
        let userDetails:Dictionary<String,Any> = [APIConstants.CheckVersionKey:"false",
                                                  APIConstants.Client:JsonConstant.IOS_CLIENT,
                                                  APIConstants.AppVersion:"12.0"]
        
        let requestDict:Dictionary<String,Any> =    [ APIConstants.EntityType:"transaction",
                                                      APIConstants.EntityAction:"get",
                                                      APIConstants.Entity:"sendMail",
                                                      APIConstants.User:userDetails,
                                                      APIConstants.Data:[emailRequestData]]
        
        
        
        
        printLog(requestDict)
        
        
        let reqeustBoday:String = requestDict.jsonRepresentation()
        printLog(reqeustBoday)
        
        let digest:String = NetworkUtility().generateDigestValue(reqeustBoday,withPackageName:"com.mobicule")
        printLog("digest\(digest)")
        
        let headers: HTTPHeaders = [
            "content-type": "application/json",
            "Accept": "application/json",
            "digest": digest,
            "Authorization": AuthenticationConstant.Mobicule_Authorization,
            //APIConstants.UniqueId:UUID,
            //APIConstants.User:APIConstants.USER_VALUE
        ]
        
        if (NetworkReachabilityManager.init()?.isReachable)!
        {
        
        let manager = SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 180
        
        
        request(AuthenticationConstant.MOBICULE_URL, method: .post, parameters: requestDict, encoding: JSONEncoding.default,headers: headers).responseJSON { response in
            
            printLog("Request: \(String(describing: response.request))")   // original url request
            printLog("Response: \(String(describing: response.response))") // http url response
            printLog("Result: \(response.result)")                         // response serialization result
            
            if let json = response.result.value {
                printLog("JSON: \(json)") // serialized json response
                
                if let responseData:Dictionary<String,Any> = json as? Dictionary<String,Any>
                {
                    
                    printLog(responseData["message"] ?? "")
                    
                    let message = responseData["message"] as? String ?? ""
                    
                    if let status:String = responseData["status"] as? String ?? ""
                    {
                        if  status != "" && status.lowercased() == "success"
                        {
                            completion(true,message)
                            return
                        }
                        
                    }
                    
                    completion(false,message)
                    return
                    
                }
                
                if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                    printLog("Data: \(utf8Text)") // original server data as UTF8 string
                }
                
                
            }
            
            completion(false,"Unable to send your request please try again")
            return
            
        }
        }else{
            
            completion(false,AlertMessage.CHECK_NET_CONNECTION)
            return
        }
        
    }
    
    class func getExchangeRate(completion : @escaping(_ status:Bool,_ currencyArray:Array<CurrencyData>?) -> Void) {
        
        let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        
        let DataDict:Dictionary<String,Any> = ["pageSize":100,
                                               "pageNumber":0,
                                               "lastSyncDate":"0"]
        
        let data:Array<Any> = [DataDict]
        
        //  var itienearyCode:Array<String> = ["PKG000014"]
        
        let  queryParameterMap:Dictionary<String,Any> = Dictionary<String, String>()
        
        let userDetails:Dictionary<String,Any> = [APIConstants.CheckVersionKey:"true",
                                                  APIConstants.Client:JsonConstant.IOS_CLIENT,
                                                  "password":"",
                                                  "login":"",
                                                  APIConstants.AppVersion:"12.0"]
        
        let requestDict:Dictionary<String,Any> = [ APIConstants.EntityType:"sync",
                                                   APIConstants.EntityAction:"add",
                                                   APIConstants.Entity:"exchangeRate",
                                                   APIConstants.User:userDetails,"data":data,"queryParameterMap":queryParameterMap]
        
        
        let requestString = requestDict.jsonRepresentation()
        
        
        
        
        let digest:String = NetworkUtility().generateDigestValue(requestString, withPackageName: "com.mobicule")
        printLog("digest:\(digest)")
        
        
        let headers: HTTPHeaders = [
            "content-type": "application/json",
            "Authorization": AuthenticationConstant.Mobicule_Authorization,
            "digest":digest
            
        ]
        
        request(AuthenticationConstant.MOBICULE_URL, method: .post, parameters: requestDict, encoding: JSONEncoding.default,headers:headers).responseJSON { response in
            
            printLog("Request: \(String(describing: response.request))")   // original url request
            printLog("Response: \(String(describing: response.response))") // http url response
            printLog("Result: \(response.result)")                         // response serialization result
            
            if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                printLog("Data: \(utf8Text)") // original server data as UTF8 string
            }
            
            if let json = response.result.value {
                printLog("JSON: \(json)") // serialized json response
                
                if let responseData:Dictionary<String,Any> = json as? Dictionary<String,Any>
                {
                    printLog(responseData["data"] ?? "No Data")
                    
                    //  DispatchQueue.main.sync {
                    
                    //completion(UserBookingDBManager.saveCurrentFxRatesList(itienearyDetailsResponseData: responseData))
                    
                    
                    let currencyData:Array<CurrencyData> = UserBookingDBManager.parseCurrentFxRatesList(currencyDetailsResponseData: responseData)
                    
                    completion(true,currencyData)
                    return
                    
                    // }
                    
                    
                    
                    
                }else{
                    
                    completion(false,[])
                    return
                }
                
                
            }else{
                
                completion(false,[])
                return
                
            }
            
        }
    }
    
    /*class func getExchangeRate(completion : @escaping(_ status:Bool) -> Void) {
        
        let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        
        var DataDict:Dictionary<String,Any> = ["pageSize":100,
                                               "pageNumber":0,
                                               "lastSyncDate":"0"]
        
        var data:Array<Any> = [DataDict]
        
        //  var itienearyCode:Array<String> = ["PKG000014"]
        
        let  queryParameterMap:Dictionary<String,Any> = Dictionary<String, String>()
        
        var userDetails:Dictionary<String,Any> = [APIConstants.CheckVersionKey:"true",
                                                  APIConstants.Client:JsonConstant.IOS_CLIENT,
                                                  "password":"",
                                                  "login":"",
                                                  APIConstants.AppVersion:version!]
        
        var requestDict:Dictionary<String,Any> = [ APIConstants.EntityType:"sync",
                                                   APIConstants.EntityAction:"add",
                                                   APIConstants.Entity:"exchangeRate",
                                                   APIConstants.User:userDetails,"data":data,"queryParameterMap":queryParameterMap]
        
        
        let headers: HTTPHeaders = [
            "content-type": "application/json",
            "Authorization": AuthenticationConstant.Mobicule_Authorization
            
        ]
        
        request(AuthenticationConstant.MOBICULE_URL, method: .post, parameters: requestDict, encoding: JSONEncoding.default,headers:headers).responseJSON { response in
            
            printLog("Request: \(String(describing: response.request))")   // original url request
            printLog("Response: \(String(describing: response.response))") // http url response
            printLog("Result: \(response.result)")                         // response serialization result
            
            if let json = response.result.value {
                printLog("JSON: \(json)") // serialized json response
                
                if let responseData:Dictionary<String,Any> = json as? Dictionary<String,Any>
                {
                    printLog(responseData["data"] ?? "No Data")
                    
                    
                    
                    //completion(UserBookingDBManager.saveCurrentFxRatesList(itienearyDetailsResponseData: responseData))
                    
                    UserBookingDBManager.saveCurrentFxRatesList(currencyDetailsResponseData: responseData)
                    
                    completion(true)
                    return
                }
                
                if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                    printLog("Data: \(utf8Text)") // original server data as UTF8 string
                }
            }else{
                
                completion(false)
                return
                
            }
            
        }
    }*/
    
    
    class func sendFeedbackRequestRequest(requestData:Dictionary<String,Any>,completion : @escaping(_ status:Bool,_ message:String) -> Void) {
        
        let requestString = requestData.jsonRepresentation()
        
        let digest:String = NetworkUtility().generateDigestValue(requestString, withPackageName: "com.mobicule")
        printLog("digest:\(digest)")
        
        let headers: HTTPHeaders = [
            "content-type": "application/json",
            "Authorization": AuthenticationConstant.Mobicule_Authorization,
            "digest":digest
            
        ]
        
        
        
        request(AuthenticationConstant.MOBICULE_URL, method: .post, parameters: requestData, encoding: JSONEncoding.default,headers: headers).responseJSON { response in
            
            printLog("Request: \(String(describing: response.request))")   // original url request
            printLog("Request Data: \(String(describing: requestData))")   // original url request
            printLog("Response: \(String(describing: response.response))") // http url response
            printLog("Result: \(response.result)")                         // response serialization result
            if response.response?.statusCode
                == 200
            {
                
                if let json = response.result.value {
                    printLog("JSON: \(json)") // serialized json response
                    
                    
                    if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                        
                        printLog("Data: \(utf8Text)") // original server data as UTF8 string
                        
                        if let json = response.result.value {
                            
                            if let responseDict:Dictionary<String,Any> = json as? Dictionary<String,Any>
                            {
                                
                                if let dataArray:Array<Any> = responseDict["data"] as? Array<Any>
                                {
                                    if dataArray.count > 0
                                    {
                                        if  let dataDict:Dictionary<String,Any> = dataArray.first as? Dictionary<String,Any>
                                        {
                                            
                                            if let redirectString:String = (dataDict["feedBackUrl"] as? String)
                                            {
                                                
                                                completion(true,redirectString)
                                                return
                                            }
                                            else{
                                                completion(false,AlertMessage.NO_DATA)
                                                return
                                            }
                                        }else
                                        {
                                            
                                            completion(false,AlertMessage.NO_DATA)
                                            return
                                        }
                                    }else{
                                        completion(false,AlertMessage.NO_DATA)
                                        return
                                    }
                                    
                                }else{
                                    completion(false,AlertMessage.NO_DATA)
                                    return
                                }
                                }
                                else{
                                completion(false,AlertMessage.NO_DATA)
                                return
                                }
                            }
                            else{
                            completion(false,AlertMessage.NO_DATA)
                            return
                            }
                        }
                        else{
                        completion(false,AlertMessage.NO_DATA)
                        return                        }
                    
                        
                    }
                    else{
                    completion(false,AlertMessage.NO_DATA)
                    return
                    }
            }else
            {
                completion(false,NetworkValidationMessages.InternetNotConnected.message)
                return
            }
                
            }
        
        }
    
    
    /* class  func sendGetCurrentFxRatesRequest(completion: @escaping (_ Success:Bool) -> Void)   {
     
     let requestString  = XMLParserRequestBuilder.getCurrentFxRatesSoapRequestString()
     
     
     let tourNetworkManager:UserBookingNetworkManager = UserBookingNetworkManager()
     
     
     tourNetworkManager.sendSoapgetCurrentFxRatesRequest(soapRequestString: requestString , soapAction: "getCurrentFxRates") { (String) in
     
     
     printLog(String)
     
     if String.lowercased() == "error"
     
     {
     
     completion(false)
     }
     else
     {
     
     do {
     
     let resultString = String.replacingOccurrences(of: "\n", with: "")
     
     let resultString2 = resultString.replacingOccurrences(of: "value=\"CASH\"", with: "")
     
     let resultString3 = resultString2.replacingOccurrences(of: "value=\"TC\"", with: "")
     
     let resultString4 = resultString3.replacingOccurrences(of: "value=\"BPC\"", with: "")
     
     let bfnOptionalDetailsSoapResponse = try AEXMLDocument(xml: resultString4)
     
     
     
     printLog(bfnOptionalDetailsSoapResponse["ForexRateResponse"]["ForexRate"].string )
     
     
     if !(bfnOptionalDetailsSoapResponse["ForexRateResponse"]["ForexRate"].string.isEmpty)
     {
     
     
     UserBookingDBManager.saveCurrentFxRatesList(bfnListResponse: bfnOptionalDetailsSoapResponse)
     
     
     completion(true)
     
     
     }
     else
     {
     completion(false)
     }
     
     
     } catch   {
     
     
     printLog(error)
     }
     }
     
     
     }
     
     
     }*/
    
    //MARK: - Helper Methods
    
    class func isPaymentDone(by currency:String,tourDetails:Tour) -> Bool
    {
        return UserBookingDBManager.isPaymentDone(by: currency, tourDetails: tourDetails)
    }
    
    class func isPaymentDonePaymentFlag(by tourDetails:Tour) -> Bool
    {
        return UserBookingDBManager.isPaymentDonePaymentFlag(by: tourDetails)
    }
    
    
    //MARK: - UserBooking DataBase Methods
    
    
    class func fetchTourDetails(tourObject:Tour,completion : @escaping(_ operationValidationMessage:OperationValidationMessage,_ tourObject:Tour?) -> Void)  {
        
        UserBookingController.sendGetBFNDetailRequest(bfNumber: (tourObject.bfNumber)!) { (messageTourDetail) in
            
            // To get optional details from server
            UserBookingController.sendGetBFNOptionalRequest(bfNumber: (tourObject.bfNumber)!) { (Bool) in
                
                // To get Itieneary details from server
                
                if let packageId:String = tourObject.packageId
                {
                    let packageIdArray:Array<String> = [packageId]
                    
                    UserBookingController.getItienearyImagesAndCityCovered(bfNumber:(tourObject.bfNumber)!,packageIdArray: packageIdArray, completion: { (status) in
                        
                        
                        // Fetch and Save FOREX Currency Data
                      //  UserBookingController.getExchangeRate(completion: { (Bool) in })
                        
                        
                        let tour = UserBookingDBManager.fetchTourDetail(bfNumber: tourObject.bfNumber!)
                        
                        if tour != nil
                        {
                            
                            completion(messageTourDetail,tour)
                            return
                        }
                        else
                            
                        {
                            completion(messageTourDetail,tour)
                            return
                            
                        }
                        
                        
                    })
                }else{
                    
                    
                    // Fetch and Save FOREX Currency Data
                  //  UserBookingController.getExchangeRate(completion: { (Bool) in })
                    
                    
                    let tour = UserBookingDBManager.fetchTourDetail(bfNumber: tourObject.bfNumber!)
                    
                    if tour != nil
                    {
                        
                        completion(messageTourDetail,tour)
                        return
                    }
                    else
                        
                    {
                        completion(messageTourDetail,tour)
                        return
                        
                    }
                    
                }
            }
            
        }
        
        
    }
    
    
    class func fetchTourDetails(bfNumber:String,completion : @escaping(_ operationValidationMessage:OperationValidationMessage,_ tourObject:Tour?) -> Void)  {
        
        UserBookingController.sendGetBFNDetailRequest(bfNumber: bfNumber) { (operationValidationMessage) in
            
            
            let tour = UserBookingDBManager.fetchTourDetail(bfNumber: bfNumber)
            
            var operationMessage = operationValidationMessage
            
            if tour != nil
            {
                operationMessage.status = true
                completion(operationMessage,tour)
                return
            }
            else
                
            {
                operationMessage.status = false
                completion(operationMessage,tour)
                return
                
            }
            
        }
        
        
    }
    
    
    class func getAppVersion(completion:@escaping(_ status:Bool,_ sessionAndTockenDictData:String?)->Void) {
        
      let UUID:String = MobiculeUtilityManager.getApplicationUUID()
        
        let headers: HTTPHeaders = [
            "content-type": "application/json",
            "Accept": "application/json",
            APIConstants.UniqueId:UUID,
            APIConstants.User:APIConstants.USER_VALUE
        ]
        
        let urlString:String = kbaseURLForAstra + "tcCommonRS/extnrt/getVersion"
        
        request(urlString, method: .get, parameters: nil, encoding: JSONEncoding.default,headers:headers).responseJSON { response in
            
            printLog("Request: \(String(describing: response.request))")   // original url request
            printLog("Response: \(String(describing: response.response))") // http url response
            printLog("Result: \(response.result)")
            
            if let json = response.result.value {
                printLog("JSON: \(json)") // serialized json response
                
                if let responseData:Dictionary<String,Any> = json as? Dictionary<String,Any>
                {
                    if let versionNumber = responseData["version"] as? String , !versionNumber.isEmpty {
                        
                        printLog(versionNumber)
                        completion(true,versionNumber)
                        return
                    }
                }
                
                completion(false,nil)
                return
            }
            
            completion(false,nil)
            return
            
        }
    }
    
    // VJ_Added_Start on 14/08/20
    
    class  func sendAdhocRequest(bfNumber:String,bookedOptionalDetails: Array<Dictionary<String, String>>, completion: @escaping (_ operationValidationMessage:OperationValidationMessage) -> Void){
        
        let requestString  = XMLParserRequestBuilder.getAdhocSoapRequestString(bfNumber: bfNumber, bookedOptionalDetails: bookedOptionalDetails)
        
        
        let loginNetworkManager:UserBookingNetworkManager = UserBookingNetworkManager()
        
        
        loginNetworkManager.sendSoapRequestMiddleware(soapRequestString: requestString ,soapAction: "bookAdhocDiscount", requestKey: "BOOK_ADHOC_DISCOUNT_RESPONSE") { (operationValidationMessage) in
            
            var operationMessage =  operationValidationMessage
            
            
            if !operationMessage.status
                
            {
                
                completion(operationMessage)
            }
            else
            {
                
                do {
                    
                    let bfnListSoapResponse = try AEXMLDocument(xml: operationValidationMessage.data)
                    
                    
                    
                    printLog(bfnListSoapResponse["BOOK_ADHOC_RESPONSE"]["RESP_CODE"].string )
                    
                    printLog(bfnListSoapResponse["BOOK_ADHOC_RESPONSE"]["RESP_DESC"].string )
                    
                    completion(operationValidationMessage)
                    
                    
                } catch   {
                    
                    
                    printLog(error)
                    operationMessage.message = error.localizedDescription
                    operationMessage.status = false
                    
                    completion(operationValidationMessage)
                    return
                }
                
            }
            
        }
        
        
    }
    // VJ_Added_End
}


extension Dictionary {
    func jsonRepresentation() -> String {
        if let jsonData : Data = try? JSONSerialization.data(withJSONObject: self, options: [])
        {
            let decoded = String(data: jsonData, encoding: .utf8)!
            return decoded
        }
        return ""
    }
}
