//
//  HolidayAccomodationObject.m
//  holidays
//
//  Created by Kush_Tech on 06/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "HolidayAccomodationObject.h"
#import "AccomodationObject.h"
@implementation HolidayAccomodationObject

-(instancetype)initWithAccomodationDict:(NSDictionary *)dictionary{
    if ([super init])
    {
        
        self.accomodationHotelId = [dictionary valueForKey:@"accomodationHotelId"];
        self.address = [self.accomodationHotelId valueForKey:@"address"];
        self.accomodationOtherId = [dictionary valueForKey:@"accomodationOtherId"];
        NSString *boolString = [dictionary valueForKey:@"isActive"];
        NSDictionary *cityCode = [self.accomodationHotelId valueForKey:@"cityCode"];
        self.cityName = [cityCode valueForKey:@"cityName"];
        self.isActive = [[boolString uppercaseString] isEqualToString:@"Y"] ? YES : NO;
        self.noOfNights = [[dictionary valueForKey:@"noOfNights"] integerValue];
        self.packageClassId = [[dictionary valueForKey:@"packageClassId"] integerValue];
        self.position = [[dictionary valueForKey:@"position"] integerValue];
        AccomodationObject *accomObject = [[AccomodationObject alloc]initWithAccomodationObjectDict:self.accomodationHotelId];
        self.state = accomObject.state;
        self.country = accomObject.country;
        self.state = accomObject.state;
        self.hotelName = accomObject.hotelName;
        self.starRating = accomObject.starRating;
        
        NSLog(@"self.hotelName %@",self.hotelName);
        NSLog(@"self.state %@",self.state);
        NSLog(@"self.country %@",self.country);
        NSLog(@"self.starRating %ld",self.starRating);
        
        
    }
    
    return self;
}
@end
