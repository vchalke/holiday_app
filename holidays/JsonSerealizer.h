//
//  JsonSerealizer.h
//  holidays
//
//  Created by vaibhav on 16/09/15.
//  Copyright (c) 2015 Mobicule. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JsonSerealizer : NSJSONSerialization

+(NSString *)stringRepresenationFromJSonDataObject:(id)dataObject;
+(NSDictionary *)dictonaryOfJsonFromJsonData:(id)dataObject;
+(NSArray *)arrayOfJSonFromJsonData:(id)dataObject;

@end
