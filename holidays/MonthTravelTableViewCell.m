//
//  MonthTravelTableViewCell.m
//  holidays
//
//  Created by Kush_Team on 24/06/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "MonthTravelTableViewCell.h"
#import "FilterLabelCollectionViewCell.h"
#import "DateStringSingleton.h"
#import "WebUrlConstants.h""
@implementation MonthTravelTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setupTableViewCell{
    selectDate = ([self.filterTravelMonthDict objectForKey:filterTravelMonth]) ? [self.filterTravelMonthDict objectForKey:filterTravelMonth] : [[DateStringSingleton shared]getMonthDateAtIndex:2 ofYearBack:-1];
    monthArrayOne = [NSArray arrayWithObjects:@"July",@"August",@"September",@"October",@"November",@"December", nil];
    monthArrayTwo = [NSArray arrayWithObjects:@"January",@"February",@"March",@"April",@"May",@"June", nil];
    mutArrayOne = [[NSMutableArray alloc] init];
    mutArrayTwo = [[NSMutableArray alloc] init];
    NSInteger currentMonth = [[[DateStringSingleton shared]getDateTimeStringFormat:@"MM" DateValue:[NSDate date]] integerValue];
//    NSInteger currentMonth = 1;
    for (int i=1;i<=12;i++){
        if (currentMonth == 1 || currentMonth == 12){
            NSDate *monthdateOne = [[DateStringSingleton shared]getMonthDateAtIndex:i ofYearBack:0];
            [mutArrayOne addObject:monthdateOne];
            NSDate *monthdateTwo = [[DateStringSingleton shared]getMonthDateAtIndex:i ofYearBack:1];
            [mutArrayTwo addObject:monthdateTwo];
        }else{
            if (i>=currentMonth){
                NSDate *monthdate = [[DateStringSingleton shared]getMonthDateAtIndex:i ofYearBack:0];
                [mutArrayOne addObject:monthdate];
            }else{
                NSDate *monthdate = [[DateStringSingleton shared]getMonthDateAtIndex:i ofYearBack:1];
                [mutArrayTwo addObject:monthdate];
            }
        }
        
    }
    [self.collection_viewOne registerNib:[UINib nibWithNibName:@"FilterLabelCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"FilterLabelCollectionViewCell"];
    [self.collection_viewTwo registerNib:[UINib nibWithNibName:@"FilterLabelCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"FilterLabelCollectionViewCell"];
    self.collection_viewOne.delegate = self;
    self.collection_viewOne.dataSource = self;
    self.collection_viewTwo.delegate = self;
    self.collection_viewTwo.dataSource = self;
    [self.collection_viewOne reloadData];
    [self.collection_viewTwo reloadData];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (collectionView == self.collection_viewOne){
        return [mutArrayOne count];
    }else if (collectionView == self.collection_viewTwo){
        return [mutArrayTwo count];
    }else{
       return 5;
    }
    return 5;
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    FilterLabelCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"FilterLabelCollectionViewCell" forIndexPath:indexPath];
    if (collectionView == self.collection_viewOne){
        NSString *dateString = [[DateStringSingleton shared]getDateTimeStringFormat:@"MMM yyyy" DateValue:[mutArrayOne objectAtIndex:indexPath.row]];
        [cell.btn_view setTag:indexPath.row];
            [cell setButtonColour:[selectDate isEqualToDate:[mutArrayOne objectAtIndex:indexPath.row]]];
        [cell.btn_view setTitle:dateString forState:UIControlStateNormal];
//        [cell]0046A0
    }else{
        NSString *dateString = [[DateStringSingleton shared]getDateTimeStringFormat:@"MMM yyyy" DateValue:[mutArrayTwo objectAtIndex:indexPath.row]];
        [cell.btn_view setTag:(100 + indexPath.row)];
        [cell setButtonColour:[selectDate isEqualToDate:[mutArrayTwo objectAtIndex:indexPath.row]]];
        [cell.btn_view setTitle:dateString forState:UIControlStateNormal];
    }
    [cell.btn_view addTarget:self action:@selector(selectDate:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(1.0, 1.0, 1.0, 1.0);
}
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
//    CGFloat width = collectionView.frame.size.width / 2 - 10;
//    CGFloat hight = collectionView.frame.size.height / 4 + 110;
    CGFloat width = collectionView.frame.size.width*0.28;
    CGFloat hight = collectionView.frame.size.height*0.75;
    return CGSizeMake(width, hight);
    
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
}
    

-(void)selectDate:(UIButton*)sender{
    if (sender.tag >= 100){
        selectDate = [mutArrayTwo objectAtIndex:sender.tag-100];
    }else{
        selectDate = [mutArrayOne objectAtIndex:sender.tag];
    }
//    NSDateFormatter *strDateFormat = [[NSDateFormatter alloc] init];
//    [strDateFormat setDateFormat:@"MMMM yyyy"];
//    NSString *strDates = [strDateFormat stringFromDate:selectDate];
//    [self.filterTravelMonthDict setObject:strDates forKey:filterTravelMonth];
    [self.filterTravelMonthDict setObject:selectDate forKey:filterTravelMonth];
    [self.collection_viewOne reloadData];
    [self.collection_viewTwo reloadData];
}
@end
