//
//  LoginNetworkManager.swift
//  sotc-consumer-application
//
//  Created by Saurav Kumar on 09/08/17.
//  Copyright © 2017 Mobicule Technologies Private Limited. All rights reserved.
//

import Foundation



class LoginNetworkManager: NSObject ,XMLParserDelegate,URLSessionDelegate{
    
    lazy var xmlResponse:String = ""
    
    
    func sendOTPRequest(mobileNumber:String , completion: @escaping (_ responseDataString:String , _ status:Bool) -> Void)  {
        
        
        let urlString = String(format: "\(AuthenticationConstant.SendOTPURL)\(mobileNumber)")
        
        let serviceUrl = URL(string: urlString)
        
        
        var request = URLRequest(url: serviceUrl!)
        
        request.httpMethod = HTTPMethod.get.rawValue
        
        request.setValue(ContentType.ApplicationText.rawValue, forHTTPHeaderField: APIConstants.ContentType)
        
       // let session = URLSession.shared
         let session = URLSession(configuration: .default, delegate: self, delegateQueue: nil)
        session.dataTask(with: request) { (data, response, error) in
            
            if let error = error{
                
                printLog(error)
                completion(error.localizedDescription,false)
                
            }
            else
            {
                
                if let response = response {
                    
                    printLog(response)
                }
                
                if let data1 = data {
                    
                    let responseString:String = String(data: data1, encoding: String.Encoding.utf8)!
                    
                    completion(responseString,true)
                    
                }
            }
            
            }.resume()
        
        
        
        
    }
    func sendOTPtoServer(mobileNumber:String ,otp: String, completion: @escaping (_ responseDataString:String , _ status:Bool) -> Void)  {
        
        
       // let urlString = String(format: "\(AuthenticationConstant.SendOTPTOSERVERURL)\(mobileNumber)")
        let urlString = String(format: AuthenticationConstant.SendOTPTOSERVERURL,mobileNumber
            ,otp)
        
        let serviceUrl = URL(string: urlString.replacingOccurrences(of: " ", with: "%20"))
        
        
        var request = URLRequest(url: serviceUrl!)
        
        request.httpMethod = HTTPMethod.get.rawValue
        
        request.setValue(ContentType.ApplicationText.rawValue, forHTTPHeaderField: APIConstants.ContentType)
        
      //  let session = URLSession.shared
         let session = URLSession(configuration: .default, delegate: self, delegateQueue: nil)
        session.dataTask(with: request) { (data, response, error) in
            
            if let error = error{
                
                printLog(error)
                completion(error.localizedDescription,false)
                
            }
            else
            {
                
                if let response = response {
                    
                    printLog(response)
                }
                
                if let data1 = data {
                    
                    let responseString:String = String(data: data1, encoding: String.Encoding.utf8)!
                    
                    completion(responseString,true)
                    
                }
            }
            
            }.resume()
        
        
        
        
    }
    func sendOTPValidateRequest(mobileNumber:String , otpString:String, completion: @escaping (_ networkStatus:(statusCode: Int, status: Bool , message:String)) -> Void)  {
        
        
        let urlString = String(format: AuthenticationConstant.ValidateOTPURL,mobileNumber
            ,otpString)
   
        
        let serviceUrl = URL(string: urlString)
       
        
        var request = URLRequest(url: serviceUrl!)
        
        request.httpMethod = HTTPMethod.get.rawValue
        
        request.setValue(ContentType.ApplicationText.rawValue, forHTTPHeaderField: APIConstants.ContentType)
        
       // let session = URLSession.shared
        
         let session = URLSession(configuration: .default, delegate: self, delegateQueue: nil)
        
        session.dataTask(with: request) { (data, response, error) in
            
            
            if let error = error{
                
   
                
                printLog(error)
                completion(NetworkValidationMessages.InternetNotConnected)
                
            }
            else
            {
                if response != nil {
                    
                    
                    let statusCode = (response as! HTTPURLResponse).statusCode
                    
                    if statusCode == 200
                        
                    {
                        
                        if let data1 = data
                        {
                            
                            let responseString:String = String(data: data1, encoding: String.Encoding.utf8)!
                            
                            printLog(responseString)
                            //completion(responseString,true)
                            
                            if(responseString.lowercased() == "true")
                            {
                                completion(NetworkValidationMessages.ValidOTP)
                            }else
                            {
                                
                                completion(NetworkValidationMessages.NotValidOTP)
                            }
                            
                        }else{
                            
                            completion(NetworkValidationMessages.InternetNotConnected)
                        }
                        
                    }else{
                        
                        completion(NetworkValidationMessages.InternetNotConnected)
                    }
                    
                }else{
                    
                    completion(NetworkValidationMessages.InternetNotConnected)
                }
            }
            }.resume()
        
    }
    
    
    
    
    func sendCheckMobileNumberSoapRequest(soapRequestString:String, completion: @escaping (_ responseDataString:String,_ status:Bool) -> Void) {
        
        let urlString = String(format: AuthenticationConstant.CheckMobileURL)
        
        let serviceUrl = URL(string: urlString)
        
        //        let loginParams = String(format: LOGIN_PARAMETERS1, "test", "Hi World")
        
        
        
        var request = URLRequest(url: serviceUrl!)
        
        request.httpMethod = HTTPMethod.post.rawValue
        
        request.setValue(ContentType.ApplicationText.rawValue, forHTTPHeaderField: APIConstants.ContentType)
        request.setValue("\(urlString)\(APIConstants.CheckMobileNumberSOAPAction)", forHTTPHeaderField: APIConstants.SOAPAction)
        
        
        request.httpBody = soapRequestString .data(using: .utf8)
        
       // let session = URLSession.shared
        
        let session = URLSession(configuration: .default, delegate: self, delegateQueue: nil)
        session.dataTask(with: request) { (data, response, error) in
            
            
            if response != nil {
                
                
                let statusCode = (response as! HTTPURLResponse).statusCode
                
                if statusCode == 200
                {
                    
                    if let data1 = data {
                        
                        // let xmlString1:String = String(data: data1, encoding: String.Encoding.utf8)!
                        
                        let parser:XMLParser = XMLParser(data: data1)
                        
                        parser.delegate = self
                        parser.parse()
                        
                        completion(self.xmlResponse,true)
                        
                        
                    }
                    else{
                        completion("error",false)
                    }
                }
                else
                {
                    completion("error",true)
                }
            }
            else
            {
                completion("error",false)
            }
            
            
            
            }.resume()
    }
    
    
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        
        if (challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust) {
            if let serverTrust = challenge.protectionSpace.serverTrust {
                var secresult = SecTrustResultType.invalid
                let status = SecTrustEvaluate(serverTrust, &secresult)
                
                if (errSecSuccess == status) {
                    if let serverCertificate = SecTrustGetCertificateAtIndex(serverTrust, 0) {
                        let serverCertificateData = SecCertificateCopyData(serverCertificate)
                        let data = CFDataGetBytePtr(serverCertificateData);
                        let size = CFDataGetLength(serverCertificateData);
                        let cert1 = NSData(bytes: data, length: size)
                        
                        for singleCertificate in AuthenticationConstant.SSLCertificates {
                            
                            let file_der = Bundle.main.path(forResource: singleCertificate, ofType: "cer")
                            
                            if let file = file_der {
                                if let cert2 = NSData(contentsOfFile: file) {
                                    if cert1.isEqual(to: cert2 as Data) {
                                        completionHandler(URLSession.AuthChallengeDisposition.useCredential, URLCredential(trust:serverTrust))
                                        return
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
        // Pinning failed
        completionHandler(URLSession.AuthChallengeDisposition.cancelAuthenticationChallenge, nil)
        
    }
    
//MARK: check mobile number with middleware
    
    func sendCheckMobileNumberSoapRequestwithMiddleware(soapRequestString:String, completion: @escaping (_ responseDataString:String,_ status:Bool) -> Void) {
        
        let urlString = String(format: AuthenticationConstant.CheckMobileURL)
        
        let serviceUrl = URL(string: urlString)
        
        //        let loginParams = String(format: LOGIN_PARAMETERS1, "test", "Hi World")
        
        
        
        var request = URLRequest(url: serviceUrl!)
        
        request.httpMethod = HTTPMethod.post.rawValue
        
        request.setValue(ContentType.ApplicationText.rawValue, forHTTPHeaderField: APIConstants.ContentType)
        request.setValue("\(urlString)\(APIConstants.CheckMobileNumberSOAPAction)", forHTTPHeaderField: APIConstants.SOAPAction)
        
        
        request.httpBody = soapRequestString .data(using: .utf8)
        
        
        getDataViaMiddleware(bookListRequest: soapRequestString,requestFor: "CHECK_MOBILE_RESPONSE"){ (Status, data, xmlStr) in
            
            
            if(Status){
                printLog("Status--->\(Status)")
                printLog("Status--->\(data)")
                
                let parser:XMLParser = XMLParser(data: data)
                
                parser.delegate = self
                parser.parse()
                
                
                printLog(self.xmlResponse)
                
                
                
                printLog(xmlStr)
                
                completion(xmlStr,true)
            }else{
                
                completion("error",false)
                
            }
            
        }
        
        
        
        
        /* let session = URLSession.shared
         session.dataTask(with: request) { (data, response, error) in
         
         
         if response != nil {
         
         
         let statusCode = (response as! HTTPURLResponse).statusCode
         
         if statusCode == 200
         {
         
         if let data1 = data {
         
         // let xmlString1:String = String(data: data1, encoding: String.Encoding.utf8)!
         
         let parser:XMLParser = XMLParser(data: data1)
         
         parser.delegate = self
         parser.parse()
         
         completion(self.xmlResponse,true)
         
         
         }
         else{
         completion("error",false)
         }
         }
         else
         {
         completion("error",true)
         }
         }
         else
         {
         completion("error",false)
         }
         
         
         
         }.resume()*/
    }
    
    
//MARK:-middleware request:
    
    func getDataViaMiddleware(bookListRequest:String,requestFor:String,completion : @escaping(_ status:Bool, _ data:Data, _ xmlStr:String) -> Void) {
        
        
        let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        
        
        //"BOOKING_FILE_LIST_RESPONSE"
        let data:Array<Dictionary<String,Any>> = [["request":bookListRequest,"searchKey":requestFor]]
        
        //var itienearyCode:Array<String> = ["2017EUR103S1"]
        
        let  queryParameterMap:Dictionary<String,Any> = ["soapaction":""]
        
        let userDetails:Dictionary<String,Any> = [APIConstants.CheckVersionKey:"false",
                                                  APIConstants.Client:JsonConstant.IOS_CLIENT,
                                                  APIConstants.AppVersion:version!]
        
        let requestDict:Dictionary<String,Any> = [ APIConstants.EntityType:"transaction",
                                                   APIConstants.EntityAction:"get",
                                                   APIConstants.Entity:"middleWareWebservice",
                                                   "identifier":"contentMiddleWareWebservice",
                                                   APIConstants.User:userDetails,"data":data,"queryParameterMap":queryParameterMap]
        
        
        
        
        let requestString = requestDict.jsonRepresentation()
        
        
        printLog("requestDict:\(requestDict)")
        
        let digest:String = NetworkUtility().generateDigestValue(requestString, withPackageName: "com.mobicule")
        printLog("digest:\(digest)")
        
        let headers: HTTPHeaders = [
            "content-type": "application/json",
            "Authorization": AuthenticationConstant.Mobicule_Authorization,
            "digest":digest
        ]
        //AuthenticationConstant.MOBICULE_URL
        //http://172.98.192.15:8081/Mobicule-Platform_MiddleWare_SOTC/api/
        request(AuthenticationConstant.MOBICULE_URL, method: .post, parameters: requestDict, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            
            
            printLog("Request: \(String(describing: response.request))")
            printLog("Request httpbody: \(String(describing: response.request?.httpBody))")   // original url request
            printLog("Response: \(String(describing: response.response))") // http url response
            printLog("Result: \(response.result)")                         // response serialization result
            var xmldata:Data?
            if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                printLog("Data: \(utf8Text)") // original server data as UTF8 string
            }
            
            
            if let json = response.result.value {
                printLog("JSON: \(json)") // serialized json response
                
                if let responseData:Dictionary<String,Any> = json as? Dictionary<String,Any>
                {
                    // printLog(responseData["data"])
                    
                    
                    let dict:Dictionary<String,Any> = responseData["data"] as! Dictionary<String, Any>
                    
                    xmldata = (dict["newResponse"] as! String).data(using: .utf8)
                    let xmlString = dict["newResponse"] as! String
                    
                    printLog(xmlString)
                    
                    
                    completion(true,xmldata!,xmlString)
                    
                    return
                    
                }
                
            }
            completion(false,xmldata ?? Data()," ")
        }
    }
    
    
    func sendOTPViaMiddleware(mobileNumber:String,completion : @escaping(_ String:String, _ status:Bool) -> Void) {
        
        
        let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        
        
        //"BOOKING_FILE_LIST_RESPONSE"
        let data:Array<Dictionary<String,Any>> = [["mobileNo":mobileNumber,
                                                   "otp":"",
                                                   "validateOtp":"false"]]
        
        //var itienearyCode:Array<String> = ["2017EUR103S1"]
        
        let  queryParameterMap:Dictionary<String,Any> = ["":""]
        
        let userDetails:Dictionary<String,Any> = [APIConstants.CheckVersionKey:"false",
                                                  APIConstants.Client:JsonConstant.IOS_CLIENT,
                                                  APIConstants.AppVersion:version!]
        
        let requestDict:Dictionary<String,Any> = [ APIConstants.EntityType:"transaction",
                                                   APIConstants.EntityAction:"get",
                                                   APIConstants.Entity:"sendOtpUrl",
                                                   "identifier":"",
                                                   APIConstants.User:userDetails,"data":data,"queryParameterMap":queryParameterMap]
        
        
        
        
        let requestString = requestDict.jsonRepresentation()
        
        
        printLog("requestDict:\(requestDict)")
        
        let digest:String = NetworkUtility().generateDigestValue(requestString, withPackageName: "com.mobicule")
        printLog("digest:\(digest)")
        
        let headers: HTTPHeaders = [
            "content-type": "application/json",
            "Authorization": AuthenticationConstant.Mobicule_Authorization,
            "digest":digest
        ]
        
        //AuthenticationConstant.MOBICULE_URL
        //http://172.98.192.15:8081/Mobicule-Platform_MiddleWare_SOTC/api/
        request(AuthenticationConstant.MOBICULE_URL, method: .post, parameters: requestDict, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            
            printLog("Request: \(String(describing: response.request))")   // original url request
            printLog("Response: \(String(describing: response.response))") // http url response
            printLog("Result: \(response.result)")                         // response serialization result
            
            if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                printLog("Data: \(utf8Text)") // original server data as UTF8 string
            }
            
            
            if let json = response.result.value {
                printLog("JSON: \(json)") // serialized json response
                
                
                if let responseData:Dictionary<String,Any> = json as? Dictionary<String,Any>
                {
                    // printLog(responseData["data"])
                    let status:String = responseData["status"] as! String
                    if (status.uppercased() == "SUCCESS")
                    {
                        
                        
                        let dict:Dictionary<String,Any> = responseData["data"] as! Dictionary<String, Any>
                        
                        let xmlString = dict["Otp"] as! String
                        
                        let otp:String = NetworkUtility().getOTP(xmlString) ?? " "
                        
                        printLog(otp)
                        
                        
                        completion(otp,true)
                        
                    }else{
                        completion(" ",false)
                    }
                    
                }else{
                    completion(" ",false)
                }
                
            }
            completion(" ",false)
        }
    }
    
    
    func validateOTPViaMiddleware(mobileNumber:String,otp:String,completion : @escaping(_ networkStatus:(statusCode: Int, status: Bool , message:String)) -> Void) {
        
        
        let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        
        let encryotedOTP:String = NetworkUtility().setOTP(otp) ?? " "
        
        let encryptedMobileNo:String = NetworkUtility().setOTP(mobileNumber) ?? " "
        
        //"BOOKING_FILE_LIST_RESPONSE"
        let data:Array<Dictionary<String,Any>> = [["mobileNo":encryptedMobileNo,
                                                   "otp":encryotedOTP,
                                                   "validateOtp":"false"]]
        
        //var itienearyCode:Array<String> = ["2017EUR103S1"]
        
        let  queryParameterMap:Dictionary<String,Any> = ["":""]
        
        let userDetails:Dictionary<String,Any> = [APIConstants.CheckVersionKey:"false",
                                                  APIConstants.Client:JsonConstant.IOS_CLIENT,
                                                  APIConstants.AppVersion:version!]
        
        let requestDict:Dictionary<String,Any> = [ APIConstants.EntityType:"transaction",
                                                   APIConstants.EntityAction:"get",
                                                   APIConstants.Entity:"sendOtpUrl",
                                                   "identifier":"",
                                                   APIConstants.User:userDetails,"data":data,"queryParameterMap":queryParameterMap]
        
        
        
        
        let requestString = requestDict.jsonRepresentation()
        
        
        printLog("requestDict:\(requestDict)")
        
        let digest:String = NetworkUtility().generateDigestValue(requestString, withPackageName: "com.mobicule")
        printLog("digest:\(digest)")
        
        let headers: HTTPHeaders = [
            "content-type": "application/json",
            "Authorization": AuthenticationConstant.Mobicule_Authorization,
            "digest":digest
        ]
        
        //AuthenticationConstant.MOBICULE_URL
        //http://172.98.192.15:8081/Mobicule-Platform_MiddleWare_SOTC/api/
        request(AuthenticationConstant.MOBICULE_URL, method: .post, parameters: requestDict, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            
            printLog("Request: \(String(describing: response.request))")   // original url request
            printLog("Response: \(String(describing: response.response))") // http url response
            printLog("Result: \(response.result)")                         // response serialization result
            
            if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                printLog("Data: \(utf8Text)") // original server data as UTF8 string
            }
            
            
            if let json = response.result.value {
                printLog("JSON: \(json)") // serialized json response
                
                if let responseData:Dictionary<String,Any> = json as? Dictionary<String,Any>
                {
                    // printLog(responseData["data"])
                    
                    
                    
                    
                    
                    printLog("valid otp response-->\(response)")
                    
                    let status:String = responseData["status"] as! String
                    if (status.uppercased() == "SUCCESS")
                    {
                        
                        let dict:Dictionary<String,Any> = responseData["data"] as! Dictionary<String, Any>
                        let responseString:String = responseData["message"] as! String
                        
                        printLog(responseString)
                        //completion(responseString,true)
                        
                        if(responseString.lowercased() == "otp generated successfully")
                        {
                            completion(NetworkValidationMessages.ValidOTP)
                        }else
                        {
                            
                            completion(NetworkValidationMessages.NotValidOTP)
                        }
                        
                    }else{
                        
                        completion(NetworkValidationMessages.NotValidOTP)
                    }
                    
                    
                    
                }else{
                    
                    completion(NetworkValidationMessages.InternetNotConnected)
                }
                
                
                
                /* let xmlString = dict["Otp"] as! String
                 
                 let otp:String = NetworkUtility().getOTP(xmlString) ?? " "
                 
                 printLog(otp)
                 
                 
                 completion(otp,true)
                 
                 return*/
                
            }
            
            
            
        }
        //completion(" ",false)
        
    }
    
    public func parserDidStartDocument(_ parser: XMLParser)
    {
        printLog("************Started")
    }
    
    
    public func parserDidEndDocument(_ parser: XMLParser)
    {
        printLog("***********End*************")
        
        // delegate?.sendSoapResponseData(responseData: xmlResponse)
        
        // printLog(xmlResponse)
        
    }
    
    
    public func parser(_ parser: XMLParser, foundNotationDeclarationWithName name: String, publicID: String?, systemID: String?)
    {
        
    }
    
    public func parser(_ parser: XMLParser, foundUnparsedEntityDeclarationWithName name: String, publicID: String?, systemID: String?, notationName: String?)
    {
        
    }
    
    
    public func parser(_ parser: XMLParser, foundAttributeDeclarationWithName attributeName: String, forElement elementName: String, type: String?, defaultValue: String?)
    {
        
    }
    
    
    public func parser(_ parser: XMLParser, foundElementDeclarationWithName elementName: String, model: String)
    {
        
    }
    
    
    public func parser(_ parser: XMLParser, foundInternalEntityDeclarationWithName name: String, value: String?)
    {
        
    }
    
    
    public func parser(_ parser: XMLParser, foundExternalEntityDeclarationWithName name: String, publicID: String?, systemID: String?)
    {
        
    }
    
    public func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:])
    {
        printLog(elementName)
    }
    
    
    public func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?)
    {
        printLog(elementName)
    }
    
    
    public func parser(_ parser: XMLParser, didStartMappingPrefix prefix: String, toURI namespaceURI: String)
    {
        printLog(prefix)
    }
    
    
    public func parser(_ parser: XMLParser, didEndMappingPrefix prefix: String)
    {
        printLog(prefix)
    }
    
    
    public func parser(_ parser: XMLParser, foundCharacters string: String)
    {
        
        xmlResponse.append(string)
    }
    
    
    public func parser(_ parser: XMLParser, foundIgnorableWhitespace whitespaceString: String)
    {
        
    }
    
    
    public func parser(_ parser: XMLParser, foundProcessingInstructionWithTarget target: String, data: String?)
    {
        
    }
    
    
    public func parser(_ parser: XMLParser, foundComment comment: String)
    {
        
    }
    
    
    public func parser(_ parser: XMLParser, foundCDATA CDATABlock: Data)
    {
        
    }
    
    //
    //    public func parser(_ parser: XMLParser, resolveExternalEntityName name: String, systemID: String?) -> Data?
    //    {
    //
    //    }
    
    
    public func parser(_ parser: XMLParser, parseErrorOccurred parseError: Error)
    {
        printLog(parseError.localizedDescription)
    }
    
    public func parser(_ parser: XMLParser, validationErrorOccurred validationError: Error)
    {
        printLog(validationError.localizedDescription)
    }
    
}


