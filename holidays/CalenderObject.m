//
//  CalenderObject.m
//  holidays
//
//  Created by Kush_Tech on 26/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "CalenderObject.h"

@implementation CalenderObject
-(instancetype)initWithCalenderObjectDict:(NSDictionary *)dictionary{
    if ([super init])
    {
        self.calendarEndDate = [dictionary valueForKey:@"calendarEndDate"];
        self.calendarStartDate = [dictionary valueForKey:@"calendarStartDate"];
        self.mode = [dictionary valueForKey:@"mode"];
        self.pkgId = [dictionary valueForKey:@"pkgId"];
        self.pkgIdentificationNo = [dictionary valueForKey:@"pkgIdentificationNo"];
        self.pkgSubType = [[dictionary valueForKey:@"pkgSubType"] intValue];
        NSString *boolString = [dictionary valueForKey:@"isDateAvialable"];
        self.isDateAvialable = [[boolString uppercaseString] isEqualToString:@"YES"] ? YES : NO;
        if (self.pkgSubType == 1){
        
        }else if (self.pkgSubType == 2){
            
        }else if (self.pkgSubType == 3){
            self.dbResponseBean = [dictionary valueForKey:@"ltResponseBean"];
        }else if (self.pkgSubType == 4){
            self.dbResponseBean = [dictionary valueForKey:@"dbResponseBean"];
        }else{
            
        }
        
         self.bookable = [self.dbResponseBean valueForKey:@"bookable"];;
         self.onRequest = [self.dbResponseBean valueForKey:@"onRequest"];;
    }
    return self;
}

-(instancetype)initWithBookableAndRequestObjectDict:(NSDictionary *)dictionary withPackgeSubType:(int)subType{
    if ([super init])
    {
        if (subType == 1){
        
        }else if (subType == 2){
            
        }else if (subType == 3){
            self.date = [dictionary valueForKey:@"DATE"];
            self.strikeOutPrice = [[dictionary valueForKey:@"DR_STRIKEOUT"] integerValue];
             self.price = [[dictionary valueForKey:@"DR_PRICE"] integerValue];
        }else if (subType == 4){
            self.date = [dictionary valueForKey:@"date"];
            self.strikeOutPrice = [[dictionary valueForKey:@"strikeOutPrice"] integerValue];
             self.price = [[dictionary valueForKey:@"price"] integerValue];
        }else{
            
        }
    }
    return self;
}
@end
