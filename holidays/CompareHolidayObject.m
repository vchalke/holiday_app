//
//  CompareHolidayObject.m
//  holidays
//
//  Created by Kush_Tech on 22/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "CompareHolidayObject.h"

@implementation CompareHolidayObject
-(instancetype)initWithCompareHolidayObject:(NSDictionary *)dictionary{
    if ([super init])
    {
        NSLog(@"%@",dictionary);
        NSLog(@"%@",[dictionary valueForKey:@"packageID"]);
        self.packageData = [dictionary valueForKey:@"packageData"];
        self.packageID = [dictionary valueForKey:@"packageID"];
        self.packageName = [dictionary valueForKey:@"packageName"];
        self.packageImgUrl = [dictionary valueForKey:@"packageImgUrl"];
        self.packagePrize = [[dictionary valueForKey:@"packagePrize"] integerValue];
    }
    
    return self;
}
@end
