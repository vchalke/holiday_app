//
//  RecentHolidayObject.m
//  holidays
//
//  Created by Kush_Tech on 12/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "RecentHolidayObject.h"

@implementation RecentHolidayObject
-(instancetype)initWithRecentObject:(NSDictionary *)dictionary{
    if ([super init])
    {
        NSLog(@"%@",dictionary);
        NSLog(@"%@",[dictionary valueForKey:@"packageID"]);
        self.packageID = [dictionary valueForKey:@"packageID"];
        self.packageName = [dictionary valueForKey:@"packageName"];
        self.packageImgUrl = [dictionary valueForKey:@"packageImgUrl"];
        self.packagePrize = [[dictionary valueForKey:@"packagePrize"] integerValue];
    }
    
    return self;
}
//-(instancetype)initWithObjectCore:(RecentHolidayObject *)obj{
//    if ([super init])
//    {
//        self.packageID = obj.packageID;
//        self.packageName = obj.packageName;
//        self.packagePrize = obj.packagePrize;
//    }
//    
//    return self;
//}
@end
