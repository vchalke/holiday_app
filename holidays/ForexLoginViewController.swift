//
//  ForexLoginViewController.swift
//  holidays
//
//  Created by ketan on 27/11/17.
//  Copyright © 2017 Mobicule. All rights reserved.
//

import UIKit
import Foundation

class ForexLoginViewController: ForexBaseViewController,GoogleSignInViewDelegate,loginCommunicationDelegate,UITextFieldDelegate,SignUpViewControllerDelegate
{
    @IBOutlet weak var textFieldPassword: UITextField!
    @IBOutlet weak var buttonGuest: UIButton!
    @IBOutlet weak var buttonExistingUser: UIButton!
    @IBOutlet var forexLoginView: UIView!
    @IBOutlet weak var constraintPasswordHeight: NSLayoutConstraint!
    var buyForexBo : BuyForexBO = BuyForexBO.init()
    
    @IBOutlet weak var forgotPasswordButton: UIButton!
    @IBOutlet weak var textFieldEmailID: UITextField!
    @IBOutlet weak var totalPayableAmount: UILabel!
    
    //mark loging communication delegate
    
    func guestLoggedInSuccessFully(withUserID userID: String!)
    {
        
        let buyForxVC : BuyForexPassangerDetailsViewController = BuyForexPassangerDetailsViewController.init(nibName: "ForexBaseViewController", bundle: nil)
       // buyForexBo.emailID = textFieldEmailID.text!
         buyForexBo.emailID = userID
        buyForxVC.buyForexBo = buyForexBo
        self.navigationController?.pushViewController(buyForxVC, animated: true)
    }
    
    func userloggedInSuccessFull(withUserDict userID: [AnyHashable : Any]!)
    {
        let buyForxVC : BuyForexPassangerDetailsViewController = BuyForexPassangerDetailsViewController.init(nibName: "ForexBaseViewController", bundle: nil)
        buyForxVC.buyForexBo = buyForexBo
        self.navigationController?.pushViewController(buyForxVC, animated: true)
    }
    
    func loggedInFail(withRason reasonMessege: String!)
    {
        
    }
    
    func userloggedInSignUpSuccessFull(withUserDict userID: String!)
    {
        let buyForxVC : BuyForexPassangerDetailsViewController = BuyForexPassangerDetailsViewController.init(nibName: "ForexBaseViewController", bundle: nil)
        buyForexBo.emailID = userID
        buyForxVC.buyForexBo = buyForexBo
        self.navigationController?.pushViewController(buyForxVC, animated: true)
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        Bundle.main.loadNibNamed("ForexLoginViewController", owner: self, options: nil)
        
        super.addViewInBaseView(childView: self.forexLoginView)
        super.setUpHeaderLabel(labelHeaderNameText: "Buy Forex")
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ForexBuyPassangerViewController.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        let totalAmt : NSInteger = buyForexBo.buyForexPayment!.totalPayableAmount as NSInteger
        totalPayableAmount.text = "\(totalAmt) INR"
        textFieldEmailID.delegate = self
        textFieldPassword.delegate = self
        KeyboardAvoiding.setAvoidingView(self.forexLoginView, withTriggerView: self.textFieldEmailID)
        KeyboardAvoiding.setAvoidingView(self.forexLoginView, withTriggerView: self.textFieldPassword)
        
        forgotPasswordButton.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = true
    }
   
    func dismissKeyboard()
    {
        view.endEditing(true)
    }
    
    func signInWithGooglePlus()
    {
        let signInVC : SignInViewController = SignInViewController.init(nibName: "SignInViewController", bundle: nil)
        signInVC.googleViewDelegate = self
//        self.present(signInVC, animated: true, completion: nil)
        self.navigationController?.pushViewController(signInVC, animated: true)
    }
    
    override func viewWillLayoutSubviews()
    {
        super.setSubViewFrame(childView: self.forexLoginView)
        
    }
    
    @IBAction func loginWithGPlus(_ sender: Any)
    {
        self.signInWithGooglePlus()
    }
    
    func didFinish(withGoogleLogin userEmailID: String!)
    {
        printLog("userEmailID---> \(userEmailID)")
        if userEmailID != "error"
        {
            UserDefaults.standard.setValue(userEmailID, forKey: kuserDefaultUserId)
            let LoginStatus: UserDefaults? = UserDefaults.standard
            LoginStatus?.set(kLoginSuccess, forKey: kLoginStatus)
            let userIdForGooglePlusSignIn: UserDefaults? = UserDefaults.standard
            userIdForGooglePlusSignIn?.set(userEmailID, forKey: kLoginEmailId)
        
            let buyForxVC : BuyForexPassangerDetailsViewController = BuyForexPassangerDetailsViewController.init(nibName: "ForexBaseViewController", bundle: nil)
            buyForexBo.emailID = userEmailID
            buyForxVC.buyForexBo = buyForexBo
            self.navigationController?.pushViewController(buyForxVC, animated: true)
        }
        
    }
    
    func didFinish(withGoogleLoginWithuserData userEmailID :[AnyHashable : Any]!)
    {
        printLog("userEmailID---> \(userEmailID)")
    }
    
    func validateFields() -> Bool
    {
        if (textFieldEmailID.text?.isEmpty)!
        {
            showAlert(message: "Enter E-mail ID")
            return false
        }
        if !isValidEmail(testStr: textFieldEmailID.text!)
        {
            showAlert(message: "Enter valid E-mail ID")
            return false
        }
        if (textFieldPassword.text?.isEmpty)!
        {
            showAlert(message: "Enter Password")
            return false
        }
        return true
    }
    
    // MARK: textfield methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    // MARK: Button Actions
    
    @IBAction func forgotPasswordButtonClicked(_ sender: UIButton)
    {
        let forgotPassVC: ForgotPassViewController = ForgotPassViewController(nibName: "ForgotPassViewController", bundle: nil)
        forgotPassVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(forgotPassVC, animated: false, completion: nil)
    }
    
    @IBAction func onGuestButtonClicked(_ sender: Any)
    {
        constraintPasswordHeight.constant = 0;
        
        forgotPasswordButton.isHidden = true
    }
    
    @IBAction func onExistingUserButtonClicked(_ sender: Any)
    {
        constraintPasswordHeight.constant = 37;
        
        forgotPasswordButton.isHidden = false
    }
    
    @IBAction func proceedAsguestHelpBtnClicked(_ sender: Any)
    {
        let popupVC: PopUpViewController = PopUpViewController(nibName: "PopUpViewController", bundle: nil)
        popupVC.msg = "Login as a member or register yourself to get special offers."
        popupVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(popupVC, animated: false, completion: nil)

    }
    @IBAction func existingMemberHelpBtnClicked(_ sender: Any)
    {
        let popupVC: PopUpViewController = PopUpViewController(nibName: "PopUpViewController", bundle: nil)
        popupVC.msg = "Get special offers as a registered member. Save time by using saved address & travellers details."
        popupVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(popupVC, animated: false, completion: nil)

    }
    @IBAction func onContinueButtonClicked(_ sender: Any)
    {
        let loginCommunication : LoginCommunicationManager = LoginCommunicationManager.init()
        loginCommunication.delegate = self;
        loginCommunication.loginViewController = self;
        
        if buttonGuest.isSelected
        {
            if isValidEmail(testStr: textFieldEmailID.text!)
            {
                loginCommunication.loginCheck(withType: "guestLogin", withUserID: textFieldEmailID.text, withPassword: "")
            }
            else
            {
                showAlert(message: "Enter valid email ID")
            }
        }
        else
        {
            if validateFields()
            {
                loginCommunication.loginCheck(withType: "signIn", withUserID: textFieldEmailID.text, withPassword: textFieldPassword.text)
            }
        }
    }
    
    @IBAction func newUserRegistrationButtonClicked(_ sender: UIButton)
    {
        let signUpVC: SignUpViewController = SignUpViewController(nibName: "SignUpViewController", bundle: nil)
        signUpVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        signUpVC.delegate = self;
        self.present(signUpVC, animated: false, completion: nil)
    }
    
    @IBAction func loginWithFacebookClicked(_ sender: UIButton)
    {
        let loginCommunication : LoginCommunicationManager = LoginCommunicationManager.init()
        loginCommunication.delegate = self;
        loginCommunication.loginViewController = self;
        loginCommunication.setUpFacebookLogin()
    }
}
