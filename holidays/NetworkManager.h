//
//  NetworkManager.h
//  mobicule-sync-core
//
//  Created by Kishan on 16/07/15.
//  Copyright (c) 2015 Mobicule. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INetworkManager.h"
#import "HTTPCommunicationService.h"
#import "MobiculeLogger.h"

@interface NetworkManager : NSObject<INetworkManager>
{
    BOOL _isLogEnabled;
    NSString* _authorization;
    NSString* _URL;
}

@end
