//
//  SightSeenObject.h
//  holidays
//
//  Created by Kush_Tech on 09/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SightSeenObject : NSObject
-(instancetype)initWithSightSeenObjectDict:(NSDictionary *)dictionary;
@property(nonatomic)NSInteger packageClassId;
@property(nonatomic,strong)NSString *descriptionName;
@property(nonatomic,strong)NSString *cityName;
@property(nonatomic,strong)NSString *name;
@end

NS_ASSUME_NONNULL_END
