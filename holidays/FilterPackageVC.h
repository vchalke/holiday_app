//
//  FilterPackageVC.h
//  holidays
//
//  Created by Pushpendra Singh on 14/08/15.
//  Copyright (c) 2015 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPKeyboardAvoidingScrollView.h"
#import "NMRangeSlider.h"
#import "PSPageMenu.h"
#import <FirebaseAnalytics/FirebaseAnalytics.h>

@interface FilterPackageVC : UIViewController<UIActionSheetDelegate,PSPageMenuDelegate>

@property(strong,nonatomic)NSMutableArray *arrayOfHolidays;
@property (nonatomic) PSPageMenu *pageMenu;
@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollViewFilter;

@property (strong, nonatomic) IBOutlet UIView *viewFilter;
@property (weak, nonatomic) IBOutlet UITextField *txtDepartureCity;
@property (weak, nonatomic) IBOutlet UITextField *txtMonthOfTravel;
@property (weak, nonatomic) IBOutlet UITextField *txtDuration;
@property (weak, nonatomic) IBOutlet UITextField *txtFlightFrom;

#pragma mark - RadioButtons
@property (weak, nonatomic) IBOutlet UIButton *btnPackageGroup;
@property (weak, nonatomic) IBOutlet UIButton *btnPackagePersonalised;
@property (weak, nonatomic) IBOutlet UIButton *btnInclusionOnlyStay;
@property (weak, nonatomic) IBOutlet UIButton *btnFlightInclusive;
@property (weak, nonatomic) IBOutlet UIButton *btnSeasonSummer;
@property (weak, nonatomic) IBOutlet UIButton *btnSeasonWinter;



- (IBAction)onRadioButtonClicked:(id)sender;

#pragma mark - Range Slider

@property (weak, nonatomic) IBOutlet NMRangeSlider *labelSlider;
@property (weak, nonatomic) IBOutlet UILabel *lowerLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imgLowerLabel;
@property (weak, nonatomic) IBOutlet UILabel *upperLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imgUpperLabel;
@property (strong,nonatomic) NSMutableDictionary *filterDict;
@property (weak, nonatomic) IBOutlet UILabel *lblMinimumPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblMaximumPrice;
- (IBAction)labelSliderChanged:(NMRangeSlider*)sender;
@property (weak, nonatomic) IBOutlet NMRangeSlider *setValuesSlider;


- (IBAction)onDepartureCityClicked:(id)sender;
- (IBAction)onMonthTravelClicked:(id)sender;
- (IBAction)onDurationClicked:(id)sender;
- (IBAction)onFlightFromClicked:(id)sender;

- (IBAction)btnApplyFilterClicked:(id)sender;
- (IBAction)onResetButtonClicked:(id)sender;

@end
