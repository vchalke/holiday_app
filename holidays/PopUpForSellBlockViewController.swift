//
//  PopUpForSellBlockViewController.swift
//  holidays
//
//  Created by Swapnil on 13/01/18.
//  Copyright © 2018 Mobicule. All rights reserved.
//

import UIKit

class PopUpForSellBlockViewController: UIViewController
{
    @IBOutlet weak var labelData: UILabel!
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        let arrayOfLines = ["By selecting this option, you are blocking the rate of exchange for your Sell (Encash) Forex transaction for the next 48 hours.","The blocking amount that you are paying at this moment will be refunded to the source of payment in 4-5 working days post sucessful completion of the trasaction at the branch.","For a transaction to be successful the encashment should happen within the next 48 working hours and you must surrender at least 80% of Forex booked in this transaction.","In case the transaction is not completed by you within the next 40 working hours, this blocking amount will be retained by Thomas Cook India Ltd."]
        
        for value in arrayOfLines
        {
            self.labelData.text = labelData.text!  + " • " + value + "\n"
        }
    }
    
    @IBAction func closeButtonClicked(_ sender: UIButton)
    {
        self.dismiss(animated: false, completion: nil)
    }
}
