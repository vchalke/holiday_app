//
//  PackageInternationFIT.m
//  holidays
//
//  Created by ketan on 23/10/15.
//  Copyright © 2015 Mobicule. All rights reserved.
//

#import "PackageInternationFIT.h"
#import "RoomsDataModel.h"
#import "TravellerInformationModel.h"

@implementation PackageInternationFIT

- (instancetype)initWithTravellerArray:(NSArray *)travellerArray
{
    self = [super init];
    if (self) {
        self.travellerArray = travellerArray;
    }
    return self;
}

-(id)calaculatePackageInternationalFIT
{
    NSMutableArray *dynamicTravellerinfoArray = [[NSMutableArray alloc]init];
    for (int index = 0; index < self.travellerArray.count ; index ++)
    {
        RoomsDataModel *roomsModelInsatance = self.travellerArray[index];
        int adultCount = roomsModelInsatance.adultCount;
        int infantCount = roomsModelInsatance.infantCount;
        NSArray *childArray = roomsModelInsatance.arrayChildrensData;
        int childWithCNBCount = 0;
        int childWithCWBCount = 0;
        NSMutableArray *childDictArray = [[NSMutableArray alloc]init];
        
        for (int j = 0; j<childArray.count; j++)
        {
            NSDictionary *childDict = childArray[j];
            UIButton *button = [childDict valueForKey:@"button"];
            UILabel *labelAge = [childDict valueForKey:@"textField"];
            NSDictionary *dictForChild ;
            if ([button isSelected]) {
                dictForChild = [[NSMutableDictionary alloc] initWithObjectsAndKeys:labelAge.text,@"CWB", nil];
                childWithCWBCount = childWithCWBCount + 1;
            }else
            {
                 dictForChild = [[NSMutableDictionary alloc] initWithObjectsAndKeys:labelAge.text,@"CNB", nil];
                childWithCNBCount = childWithCNBCount + 1;
            }
            [childDictArray addObject:dictForChild];
        }
        id travellerInfo = [self calculatePersonInfoWithTravellerArray:dynamicTravellerinfoArray withAdultCount:adultCount withChildWithCNBCount:childWithCNBCount withChildWithCWBcount:childWithCWBCount withRoomNo:index+1 withRoomDataModelInstance:roomsModelInsatance withChildDictArray:childDictArray];
        if ([travellerInfo isKindOfClass:[NSString class]])
        {
            return travellerInfo;
        }
        else
        {
            for (int j = 0; j<infantCount; j++)
            {
                TravellerInformationModel *travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeInfant withRoomNo:index+1 withType:kTravellerTypeInfant];
                [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
            }
        }
        
    }
    
    return dynamicTravellerinfoArray;
}

-(id)calculatePersonInfoWithTravellerArray:(NSMutableArray *)dynamicTravellerinfoArray withAdultCount:(int) adultCount withChildWithCNBCount:(int)childCNBCount withChildWithCWBcount:(int)childCWBCount withRoomNo:(int)roomNo withRoomDataModelInstance:(RoomsDataModel *)roomDataModelInstance withChildDictArray:(NSArray *)childDictArray
{
    TravellerInformationModel *travellerInfoModelInstance;
    int totalChildCount = childCNBCount+childCWBCount;
    
     // Africa | USA
    if ([[self.travellerRegion lowercaseString] isEqualToString:@"africa"]||[[self.travellerRegion lowercaseString] isEqualToString:@"usa"]) {
        switch (totalChildCount) {
            case 0:
                switch (adultCount) {
                    case 1:
                        // 1*SR
                        roomDataModelInstance.SRCount = 1;
                        travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeSR withRoomNo:roomNo withType:kTravellerTypeAdult];
                        [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                        break;
                    case 2:
                        // 2*DR
                        roomDataModelInstance.DRCount = 1;
                        travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeAdult];
                        [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                        
                        travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeAdult];
                        [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                        
                        break;
                    case 3:
                        // 3*TR
                        roomDataModelInstance.TRCount = 1;
                        travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeTR withRoomNo:roomNo withType:kTravellerTypeAdult];
                        [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                        
                        travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeTR withRoomNo:roomNo withType:kTravellerTypeAdult];
                        [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                        
                        travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeTR withRoomNo:roomNo withType:kTravellerTypeAdult];
                        [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                        
                        break;
                    case 4:
                        // 2*DR + 2*DR
                        roomDataModelInstance.DRCount = 2;
                        travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeAdult];
                        [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                        
                        travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeAdult];
                        [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                        
                        travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeAdult];
                        [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                        
                        travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeAdult];
                        [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                        
            
                        break;
                        
                    default:
                        break;
                }
                break;
            case 1:
                if (totalChildCount == childCWBCount)
                {
                    switch (adultCount) {
                        case 1:
                            // 2*DR
                            roomDataModelInstance.DRCount = 1;
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeChild];
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeAdult];
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            break;
                        case 2:
                            // 2*DR + 1*CWB
                            roomDataModelInstance.DRCount = 1;
                            roomDataModelInstance.CWBCount = 1;
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeCWB withRoomNo:roomNo withType:kTravellerTypeChild];
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeAdult];
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeAdult];
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            break;
                            
                        default:
                            break;
                    }
                }else if (totalChildCount == childCNBCount)
                {
                    int ageCNBchild = 0;
                    NSArray *CNBArray = [childDictArray valueForKey:@"CNB"];
                    if (CNBArray.count != 0)
                    {
                        NSString *ageCNBchildString = CNBArray[0];
                        if ([ageCNBchildString isKindOfClass:[NSNull class]])
                        {
                            ageCNBchildString = CNBArray[1];
                        }

                        ageCNBchild = [ageCNBchildString intValue];
                    }
                    
                    switch (adultCount) {
                        case 1:
                            // 2*DR
                            roomDataModelInstance.DRCount = 1;
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeChild];
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeAdult];
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            break;
                        case 2:
                            //2*DR + 1*CNB
                            roomDataModelInstance.DRCount = 1;
                            roomDataModelInstance.CNBCount = 1;
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeCNB withRoomNo:roomNo withType:kTravellerTypeChild];
                            travellerInfoModelInstance.travellerAge = ageCNBchild;
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeAdult];
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeAdult];
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            break;
                        case 3:
                            //3*TR + 1*CNB
                            roomDataModelInstance.TRCount = 1;
                            roomDataModelInstance.CNBCount = 1;
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeCNB withRoomNo:roomNo withType:kTravellerTypeChild];
                            travellerInfoModelInstance.travellerAge = ageCNBchild;
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeTR withRoomNo:roomNo withType:kTravellerTypeAdult];
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeTR withRoomNo:roomNo withType:kTravellerTypeAdult];
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeTR withRoomNo:roomNo withType:kTravellerTypeAdult];
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            break;
                        default:
                            break;
                    }
                }
                break;
            case 2:
                if (totalChildCount == childCWBCount)
                {
                    switch (adultCount) {
                        case 1:
                            //2*DR + 1*CWB
                            roomDataModelInstance.DRCount = 1;
                            roomDataModelInstance.CWBCount = 1;
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeCWB withRoomNo:roomNo withType:kTravellerTypeChild];
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeChild];
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeAdult];
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            break;
                            
                        default:
                            break;
                    }
                }else if (totalChildCount == childCNBCount)
                {
                    int ageCNBchild1 = 0;
                    int ageCNBchild2 = 0;
                    NSArray *CNBArray = [childDictArray valueForKey:@"CNB"];
                    if (CNBArray.count != 0)
                    {
                        NSString *ageCNBchildString1 = CNBArray[0];
                        ageCNBchild1 = [ageCNBchildString1 intValue];
                        
                        NSString *ageCNBchildString2 = CNBArray[1];
                        ageCNBchild2 = [ageCNBchildString2 intValue];
                    }
                    
                    switch (adultCount) {
                        case 1:
                            //2*DR + 1*CNB
                            roomDataModelInstance.DRCount = 1;
                            roomDataModelInstance.CNBCount  = 1;
                            
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeChild];
                            travellerInfoModelInstance.travellerAge = ageCNBchild1;
                            //                            if (ageCNBchild1 >= 5)
                            //                            {
                            //                                travellerInfoModelInstance.travellerAge = ageCNBchild1;
                            //                            }
                            //                            else
                            //                            {
                            //                                travellerInfoModelInstance.travellerAge = ageCNBchild2;
                            //                            }

                            
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeCNB withRoomNo:roomNo withType:kTravellerTypeChild];
                            travellerInfoModelInstance.travellerAge = ageCNBchild1;
                            
//                            if (ageCNBchild1 >= 2 && ageCNBchild1 <= 4)
//                            {
//                                travellerInfoModelInstance.travellerAge = ageCNBchild1;
//                            }
//                            else
//                            {
//                                travellerInfoModelInstance.travellerAge = ageCNBchild2;
//                            }
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeAdult];
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            break;
                        case 2:
                            //2*DR + 1*CNB + 1*CNB
                            roomDataModelInstance.DRCount = 1;
                            roomDataModelInstance.CNBCount = 2;
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeCNB withRoomNo:roomNo withType:kTravellerTypeChild];
                              travellerInfoModelInstance.travellerAge = ageCNBchild1;
                             [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeCNB withRoomNo:roomNo withType:kTravellerTypeChild];
                             travellerInfoModelInstance.travellerAge = ageCNBchild2;
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeAdult];
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeAdult];
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            break;
                        default:
                            break;
                    }
                }else if (childCNBCount == 1 && childCWBCount == 1)
                {
                    int ageCNBchild = 0;
          
                    NSArray *CNBArray = [childDictArray valueForKey:@"CNB"];
                    if (CNBArray.count != 0)
                    {
                        NSString *ageCNBchildString = CNBArray[0];
                        if ([ageCNBchildString isKindOfClass:[NSNull class]])
                        {
                            ageCNBchildString = CNBArray[1];
                        }

                        ageCNBchild = [ageCNBchildString intValue];
                    }
                    
                    switch (adultCount) {
                            
                        case 1:
                            //DR + CNB
                            roomDataModelInstance.DRCount = 1;
                            roomDataModelInstance.CNBCount = 1;
                            
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeCNB withRoomNo:roomNo withType:kTravellerTypeChild];
                            travellerInfoModelInstance.travellerAge = ageCNBchild;
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeChild];
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeAdult];
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            break;
                            
                        case 2:
                            //2*DR + 1*CWB + 1*CNB
                            roomDataModelInstance.DRCount = 1;
                            roomDataModelInstance.CWBCount = 1;
                            roomDataModelInstance.CNBCount = 1;
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeCNB withRoomNo:roomNo withType:kTravellerTypeChild];
                            travellerInfoModelInstance.travellerAge = ageCNBchild;
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeCWB withRoomNo:roomNo withType:kTravellerTypeChild];
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeAdult];
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeAdult];
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            break;
                            
                        default:
                            break;
                    }
                }
                break;
            default:
                break;
        }
    }
    else // Aus | NZ | Eu | Asia | middle east
    {
        switch (totalChildCount) {
            case 0:
                switch (adultCount) {
                    case 1:
                        // 1*SR
                        roomDataModelInstance.SRCount = 1;
                        travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeSR withRoomNo:roomNo withType:kTravellerTypeAdult];
                        [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                        break;
                    case 2:
                        // 2*DR
                        roomDataModelInstance.DRCount = 1;
                        travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeAdult];
                        [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                        
                        travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeAdult];
                        [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                        
                        break;
                    case 3:
                        // 3*TR
                        roomDataModelInstance.TRCount = 1;
                        travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeTR withRoomNo:roomNo withType:kTravellerTypeAdult];
                        [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                        
                        travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeTR withRoomNo:roomNo withType:kTravellerTypeAdult];
                        [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                        
                        travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeTR withRoomNo:roomNo withType:kTravellerTypeAdult];
                        [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                        
                        break;
                    case 4:
                        // 2*DR + 2*DR
                        roomDataModelInstance.DRCount = 2;
                        travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeAdult];
                        [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                        
                        travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeAdult];
                        [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                        
                        travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeAdult];
                        [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                        
                        travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeAdult];
                        [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                        
                        break;
                        
                    default:
                        break;
                }
                break;
            case 1:
                if (totalChildCount == childCWBCount)
                {
                    switch (adultCount) {
                        case 1:
                            // 2*DR
                            roomDataModelInstance.DRCount = 1;
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeChild];
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeAdult];
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            break;
                        case 2:
                            // 2*DR + 1*CWB
                            roomDataModelInstance.DRCount = 1;
                            roomDataModelInstance.CWBCount = 1;
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeCWB withRoomNo:roomNo withType:kTravellerTypeChild];
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeAdult];
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeAdult];
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            break;
                            
                        default:
                            break;
                    }
                }else if (totalChildCount == childCNBCount)
                {
                    int ageCNBchild = 0;
                    
                    NSArray *CNBArray = [childDictArray valueForKey:@"CNB"];
                    if (CNBArray.count != 0)
                    {
                        NSString *ageCNBchildString = CNBArray[0];
                        if ([ageCNBchildString isKindOfClass:[NSNull class]])
                        {
                            ageCNBchildString = CNBArray[1];
                        }

                        ageCNBchild = [ageCNBchildString intValue];
                    }
                    
                    switch (adultCount) {
                        case 1:
                            // 2*DR
                            roomDataModelInstance.DRCount = 1;
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeChild];
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeAdult];
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            break;
                        case 2:
                            //2*DR + 1*CNB
                            roomDataModelInstance.DRCount = 1;
                            roomDataModelInstance.CNBCount = 1;
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeCNB withRoomNo:roomNo withType:kTravellerTypeChild];
                            travellerInfoModelInstance.travellerAge = ageCNBchild;
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeAdult];
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeAdult];
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            break;
                        case 3:
                            //3*TR + 1*CNB
                            roomDataModelInstance.TRCount = 1;
                            roomDataModelInstance.CNBCount = 1;
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeCNB withRoomNo:roomNo withType:kTravellerTypeChild];
                            travellerInfoModelInstance.travellerAge = ageCNBchild;
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeTR withRoomNo:roomNo withType:kTravellerTypeAdult];
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeTR withRoomNo:roomNo withType:kTravellerTypeAdult];
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeTR withRoomNo:roomNo withType:kTravellerTypeAdult];
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            break;
                        default:
                            break;
                    }
                }
                break;
            case 2:
                if (totalChildCount == childCWBCount)
                {
                    switch (adultCount) {
                        case 1:
                            //2*DR + 1*CWB
                            roomDataModelInstance.DRCount = 1;
                            roomDataModelInstance.CWBCount = 1;
                            
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeCWB withRoomNo:roomNo withType:kTravellerTypeChild];
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeChild];
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeAdult];
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            break;
                            
                        default:
                            break;
                    }
                }else if (totalChildCount == childCNBCount)
                {
                    int ageCNBchild1 = 0;
                    int ageCNBchild2 = 0;
                    NSArray *CNBArray = [childDictArray valueForKey:@"CNB"];
                    if (CNBArray.count != 0)
                    {
                        NSString *ageCNBchild1String = CNBArray[0];
                        ageCNBchild1 = [ageCNBchild1String intValue];
                        
                        NSString *ageCNBchild2String = CNBArray[1];
                        ageCNBchild2 = [ageCNBchild2String intValue];
                    }

                    
                    switch (adultCount) {
                        case 1:
                            //2*DR + 1*CNB
                            roomDataModelInstance.CNBCount = 1;
                            roomDataModelInstance.DRCount  = 1;
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeCNB withRoomNo:roomNo withType:kTravellerTypeChild];
                            travellerInfoModelInstance.travellerAge = ageCNBchild2;
//                            if (ageCNBchild1 >= 2 && ageCNBchild1 <= 4)
//                            {
//                                travellerInfoModelInstance.travellerAge = ageCNBchild1;
//                            }else
//                            {
//                                travellerInfoModelInstance.travellerAge = ageCNBchild2;
//                            }
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeChild];
                            travellerInfoModelInstance.travellerAge = ageCNBchild1;
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeAdult];
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            break;
                        case 2:
                            //2*DR + 1*CWB + 1*CNB
                            roomDataModelInstance.DRCount = 1;
                            roomDataModelInstance.CWBCount = 1;
                            roomDataModelInstance.CNBCount = 1;
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeCWB withRoomNo:roomNo withType:kTravellerTypeChild];
                            travellerInfoModelInstance.travellerAge = ageCNBchild1;
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeCNB withRoomNo:roomNo withType:kTravellerTypeChild];
                            travellerInfoModelInstance.travellerAge = ageCNBchild2;
//                            if (ageCNBchild1 >= 2 && ageCNBchild1 <= 4)
//                            {
//                                travellerInfoModelInstance.travellerAge = ageCNBchild1;
//                            }else
//                            {
//                                travellerInfoModelInstance.travellerAge = ageCNBchild2;
//                            }
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeAdult];
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeAdult];
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            break;
                        default:
                            break;
                    }
                }else if (childCNBCount == 1 && childCWBCount == 1)
                {
                    int ageCNBchild = 0;
                    
                    NSArray *CNBArray = [childDictArray valueForKey:@"CNB"];
                    if (CNBArray.count != 0)
                    {
                        NSString *ageCNBchildString = CNBArray[0];
                        if ([ageCNBchildString isKindOfClass:[NSNull class]])
                        {
                            ageCNBchildString = CNBArray[1];
                        }
                        
                        ageCNBchild = [ageCNBchildString intValue];
                    }
                    
                    switch (adultCount) {
                        case 1:
                            //DR + CNB
                            roomDataModelInstance.DRCount = 1;
                            roomDataModelInstance.CNBCount = 1;
                            
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeCNB withRoomNo:roomNo withType:kTravellerTypeChild];
                            travellerInfoModelInstance.travellerAge = ageCNBchild;
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeChild];
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeAdult];
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            break;

                        case 2:
                            //2*DR + 1*CWB + 1*CNB
                            roomDataModelInstance.DRCount = 1;
                            roomDataModelInstance.CWBCount = 1;
                            roomDataModelInstance.CNBCount = 1;
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeCNB withRoomNo:roomNo withType:kTravellerTypeChild];
                             travellerInfoModelInstance.travellerAge = ageCNBchild;
                           
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeCWB withRoomNo:roomNo withType:kTravellerTypeChild];
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeAdult];
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            travellerInfoModelInstance = [[TravellerInformationModel alloc]initWithRoomingType:kTravellerRoomTypeDR withRoomNo:roomNo withType:kTravellerTypeAdult];
                            [dynamicTravellerinfoArray addObject:travellerInfoModelInstance];
                            
                            break;
                            
                        default:
                            break;
                    }
                }
                break;
            default:
                break;
        }
        
    }
    
    return dynamicTravellerinfoArray;
}

@end
