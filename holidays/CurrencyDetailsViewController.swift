//
//  CurrencyDetailsViewController.swift
//  holidays
//
//  Created by Swapnil on 22/12/17.
//  Copyright © 2017 Mobicule. All rights reserved.
//

import UIKit
import QuartzCore

class CurrencyDetailsViewController: ForexBaseViewController,UITableViewDelegate,UITableViewDataSource,LineChartDelegate
{
    //MARK: - Outlets
    @IBOutlet var currDetailsView: UIView!
    @IBOutlet weak var currDetailsTableView: UITableView!
    @IBOutlet weak var changeRateSegControl: CustomSegmentControl!
   
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var lineGraphView: LineChart!
    var currencyCode : String = ""
    var currencyName : String = ""
    var productId : Int = 0
    
    var xLabelsArray: [String] = []
    var yLabelsArray: [CGFloat] = []

    var currencyRateDict : Dictionary<String,Any>?
    var currencyInfoArray: [Any] = Array()
    var currencyDetailsDict : Dictionary<String,Any>?
    
    var buyRatesDataArray:[Any]!
    var sellRatesDataArray:[Any]!
    var dictArray:[Any]!
    var productDataArray:[Any]!

    //MARK: - Controller Methods
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.buyRatesDataArray = []
        self.sellRatesDataArray = []
        self.dictArray = []
        self.productDataArray = []
        
        Bundle.main.loadNibNamed(CURR_DETAIL_VC, owner: self, options: nil)
        super.addViewInBaseView(childView: self.currDetailsView)
        super.setUpHeaderLabel(labelHeaderNameText: currencyName)
        
        lineGraphView.delegate = self
        
        self.currDetailsTableView.register(UINib(nibName:"CurrencyDetailTableViewCell", bundle: nil), forCellReuseIdentifier: "CurrencyDetailTableViewCell")
        
        self.showCurrentDate()
        self.getAllCurrencyData()
        self.setupDefaultLineGraph()
        
        self.getCurrencyDetails(moduleType: "1", productType: 1, currencyCode: currencyCode)
    }
    
    override func viewWillLayoutSubviews()
    {
        super.setSubViewFrame(childView: self.currDetailsView)
    }
    
    func showCurrentDate()
    {
        let currentDate = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM yyy"
        self.dateLabel.text = formatter.string(from: currentDate)
    }
    
    //MARK: - SegmentedControl
    @IBAction func changeRatesAction(_ sender: CustomSegmentControl)
    {
        switch changeRateSegControl.selectedSegementIndex
        {
            case 0:
            
                self.getCurrencyDetails(moduleType: "1", productType: 1, currencyCode: currencyCode)
            
            case 1:
            
                self.getCurrencyDetails(moduleType: "2", productType: 1, currencyCode: currencyCode)
            
        default:
            break;
        }
    }
    
    //MARK: - SetupLineGraphView
    func setupDefaultLineGraph()
    {
        let data: [CGFloat] = [0, 0, 0, 0]
        let xLabels: [String] = ["A", "B", "C", "D"]
        
        lineGraphView.animation.enabled = true
        lineGraphView.area = false
        lineGraphView.x.labels.visible = true
        lineGraphView.x.grid.count = 1
        lineGraphView.y.grid.count = 10
        lineGraphView.x.labels.values = xLabels
        lineGraphView.y.labels.visible = true
        lineGraphView.addLine(data)
    }

    func setupLineGraph(xLabels: [String], yLabels: [CGFloat])
    {
        lineGraphView.clearAll()
        
        lineGraphView.animation.enabled = true
        lineGraphView.area = false
        lineGraphView.x.labels.visible = true
        lineGraphView.x.grid.count = 1
        lineGraphView.y.grid.count = 10
        lineGraphView.x.labels.values = xLabels
        lineGraphView.y.labels.visible = true
        lineGraphView.addLine(yLabels)
    }
    
    //MARK: - LineChartView
    //Line chart delegate method.
    func didSelectDataPoint(_ x: CGFloat, yValues: Array<CGFloat>)
    {
        //                label.text = "x: \(x)     y: \(yValues)"
    }
    
    //Redraw chart on device rotation.
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation)
    {
        if let chart = lineGraphView
        {
            chart.setNeedsDisplay()
        }
    }
    
    // MARK: TableView delegates
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.productDataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CurrencyDetailTableViewCell", for: indexPath) as? CurrencyDetailTableViewCell else
        {
            fatalError("The dequeued cell is not an instance of LiveRatesTableViewCell.")
        }
        
        let dict = self.productDataArray[indexPath.row] as! [String:Any]
        
        let productName = dict["productName"] as! String
        let buyRate = dict["buyRate"] ?? 0.0
        let sellRate = dict["sellRate"] ?? 0.0
        
        cell.productLabel.text = productName
        cell.buyRateLabel.text = String(describing: buyRate)
        cell.sellRateLabel.text = String(describing: sellRate)
        
//        if cell.isSelected
//        {
//            cell.buyRateLabel.textColor = UIColor.red
//        }
//        else
//        {
//            cell.buyRateLabel.textColor = UIColor.black
//        }

  
        if cell.sellRateLabel.text == "0.0"
        {
            cell.sellRateLabel.text = "NA"
        }
        
        if cell.buyRateLabel.text == "0.0"
        {
            cell.buyRateLabel.text = "NA"
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let headerView = (Bundle.main.loadNibNamed("HeaderViewCurrencyDetail", owner: self, options: nil)?[0] as? UIView)
        return headerView;
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 45
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
//        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CurrencyDetailTableViewCell", for: indexPath) as? CurrencyDetailTableViewCell else
//        {
//            fatalError("The dequeued cell is not an instance of LiveRatesTableViewCell.")
//        }
//        cell.buyRateLabel.textColor = UIColor.red
//        currDetailsTableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.none)
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        let currInfoBuy = self.productDataArray[indexPath.row] as! [String:Any]
        let productId = currInfoBuy["productId"] ?? 0
        
        switch changeRateSegControl.selectedSegementIndex
        {
            case 0:
                self.getCurrencyDetails(moduleType: "1", productType: productId as! Int, currencyCode: currencyCode)
            
            case 1:
                self.getCurrencyDetails(moduleType: "2", productType: productId as! Int, currencyCode: currencyCode)
            
            default:
                break;
        }
    }
    
//    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath)
//    {
//        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CurrencyDetailTableViewCell", for: indexPath) as? CurrencyDetailTableViewCell else
//        {
//            fatalError("The dequeued cell is not an instance of LiveRatesTableViewCell.")
//        }
//        cell.buyRateLabel.textColor = UIColor.black
//        currDetailsTableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.none)
//    }
    
    // MARK: FetchDataFromServer
    func getAllCurrencyData()
    {
        LoadingIndicatorView.show();
        
        let pathParameter  : NSString = "/tcForexRS/generic/rateCards"
        
        ForexCommunicationManager.sharedInstance.execTask(pathParam:pathParameter , queryParam: "", requestType: "get", jsonDict:NSDictionary.init())
        {
            (status, response) in
            
            DispatchQueue.main.async { () -> Void in
                LoadingIndicatorView.hide()
            
            
            if(response != nil)
            {
                let responseDict:Dictionary<String,Any> = (response as?  Dictionary<String,Any>)!
                self.currencyRateDict  = responseDict
                self.currencyInfoArray = (self.currencyRateDict?["listModuleProductRoeMappingBean"] as? Array<Any>)!

                let array = self.currencyInfoArray.filter
                {
                    guard let dictionary = $0 as? [String: Any],
                        let name = dictionary["currencyCode"] as? String else
                    {
                        return false
                    }
                    return name.contains(self.currencyCode)
                }
                print("array---> \(array)")
                
                for element in array
                {
                    var dict = element as! [String:Any]
                    let productName = dict["productName"] as! String
                    let moduleTypeId = dict["moduleTypeId"] as! Int
                    let productId = dict["productId"] as! Int
                    let roe = dict["roe"] as! Float
                    
                    var productDict:[String:Any] = [:]
                    productDict["productName"] = productName
                    productDict["productId"] = productId
                    if moduleTypeId == 1
                    {
                        productDict["buyRate"] = roe
                        self.buyRatesDataArray.append(productDict)
                    }
                }
                
                for element in array
                {
                    var dict = element as! [String:Any]
                    let productName = dict["productName"] as! String
                    let moduleTypeId = dict["moduleTypeId"] as! Int
                    let productId = dict["productId"] as! Int
                    let roe = dict["roe"] as! Float
                    
                    var productDict:[String:Any] = [:]
                    productDict["productName"] = productName
                    productDict["productId"] = productId
                    if moduleTypeId == 2
                    {
                        productDict["sellRate"] = roe
                        self.sellRatesDataArray.append(productDict)
                    }
                }
                
                for element in self.buyRatesDataArray
                {
                    var dict = element as! [String:Any]
                    let productName = dict["productName"] as! String
                   
                    for element in self.sellRatesDataArray
                    {
                        var duplicateDict = element as! [String:Any]
                        let duplicateProductName = duplicateDict["productName"] as! String
                        if productName == duplicateProductName
                        {
                            dict["sellRate"] = duplicateDict["sellRate"]
                        }
                        self.dictArray.append(dict)
                    }
                }
                
                self.productDataArray = (self.dictArray as! [[String:Any]]).sorted(by: { (dictOne, dictTwo) -> Bool in
                    let d1 = dictOne["productId"]! as! Int
                    let d2 = dictTwo["productId"]! as! Int
                    
                    return d1 < d2
                })
                
                DispatchQueue.main.async
                {
                    self.currDetailsTableView.reloadData()
                }
            }
            else
            {
                self.showAlert(message: "Some error has occurred")
            }
            }
        }
    }
    
    func getCurrencyDetails(moduleType: String, productType: Int, currencyCode: String)
    {
        let appendString = "\(moduleType)/\(productType)/\(currencyCode)"
        let pathParameter  : NSString = "/tcForexRS/generic/rateCards/".appending(appendString) as NSString
        
        self.xLabelsArray.removeAll()
        self.yLabelsArray.removeAll()
        
        execTaskWithAlamofire(pathParam: pathParameter, queryParam: "", requestType: "get", jsonDict: NSDictionary.init())
        {
            (status, response) in
            
            DispatchQueue.main.async
                {
                    LoadingIndicatorView.hide()
            }
            
            
            if(response != nil)
            {
                let responseDict:Dictionary<String,Any> = (response as?  Dictionary<String,Any>)!
                self.currencyDetailsDict  = responseDict
                if let beanDict:Dictionary<String,Any> =  responseDict["bean"] as? Dictionary<String,Any>
                {
                    if let entryArray:Array<Any> =  beanDict["entry"] as? Array<Any>
                    {
                        if let dayArray:Array<String> =  responseDict["days"] as? Array<String>
                        {
                            for rate in dayArray
                            {
                                self.xLabelsArray.append(rate)
                                for rateDay in entryArray
                                {
                                    if let rateDataDict = rateDay as? [String: Any]
                                    {
                                        if let dayname = rateDataDict["key"] as? String
                                        {
                                            if (rate == dayname)
                                            {
                                                if let rate = rateDataDict["value"] as? [String: Any],
                                                    let currentRate = rate["roe"] as? Double
                                                {
                                                    self.yLabelsArray.append(CGFloat(currentRate.truncate(places: 2)))
                                                }
                                               
                                            }
                                            
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                    let xLabelsArrayRev = Array(self.xLabelsArray.reversed())
                    print("xLabelsArrayRev---> \(xLabelsArrayRev)")
                    let yabelsArrayRev = Array(self.yLabelsArray.reversed())
                    print("yabelsArrayRev---> \(yabelsArrayRev.count)")
                    
                    if self.xLabelsArray.count == 0 || self.yLabelsArray.count == 0
                    {
                        self.showAlert(message: "Data not available")
                    }
                    else if self.xLabelsArray.count == yabelsArrayRev.count
                    {
                        self.setupLineGraph(xLabels: xLabelsArrayRev , yLabels: yabelsArrayRev)
                        
                    }else{
                        
                        //self.setupDefaultLineGraph()
                        
                    }
                }

            }
        }
    }
    
    
    func execTaskWithAlamofire(pathParam:NSString, queryParam:NSString ,requestType:NSString, jsonDict:NSDictionary, taskCallback: @escaping (Bool,
        AnyObject?) -> ())
    {
        
        if AppUtility.checkNetConnection()
        {
        
        LoadingIndicatorView.show();
        let session = URLSession(configuration: URLSessionConfiguration.default)
        
        var urlComponents = URLComponents(string: kbaseURLForAstra)!//kbaseURLForAstra "https://services.thomascook.in"
        
        if pathParam != ""
        {
            urlComponents.path = pathParam as String
        }
        
        if queryParam != ""
        {
            urlComponents.query = queryParam as String
        }
        
        let url = urlComponents.url;
        
        let tokenId  : NSString = UserDefaults.standard.value(forKey: "userDefaultTokenId") as! NSString;
        let requestId : NSString = UserDefaults.standard.value(forKey: "userDefaultRequestId") as! NSString;
        
        
        let headerFielda:HTTPHeaders = ["Accept":"application/json","Content-Type":"application/json","requestId":requestId as String,"sessionId":tokenId as String]
        /* request(url!, method: .get, parameters: jsonDict  as? Parameters, encoding: JSONEncoding.default, headers: headerFielda).responseJSON */
        request(url!, method: .get, encoding: JSONEncoding.default, headers: headerFielda).responseJSON{ response in
            
            DispatchQueue.main.async
                {
              LoadingIndicatorView.hide()
            }
            
            printLog("Request: \(String(describing: response.request))")   // original url request
            printLog("Response: \(String(describing: response.response))") // http url response
            printLog("Result: \(response.result)")                         // response serialization result
            
            if response.result.isSuccess
            {
                if let json = response.result.value
                {
                    //                printLog("JSON: \(json)") // serialized json response
                    
                    if let responseData:Dictionary<String,Any> = json as? Dictionary<String,Any>
                    {
                        //  printLog(responseData["data"])
                        
                        DispatchQueue.main.async
                        { () -> Void in
                                LoadingIndicatorView.hide()
                        }
                        
                        taskCallback(true, responseData as AnyObject)
                    }
                    
                    if let data = response.data, let utf8Text = String(data: data, encoding: .utf8)
                    {
                        printLog("Data: \(utf8Text)") // original server data as UTF8 string
                    }
                }
                else
                {
                    taskCallback(false,nil)
                    
                }
            }
            else
            {
                LoadingIndicatorView.hide()
                
                self.showAlert(message: "Some error occured")
                self.setupDefaultLineGraph()
                
                 taskCallback(false,nil)
            }
        }
        }
    }
}
