//
//  PDPCalculatePromoVC.m
//  holidays
//
//  Created by Kush_Tech on 26/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "PDPCalculatePromoVC.h"
#import "TravellerInformationModel.h"
#import "NetCoreAnalyticsVC.h"
#import "NewLoginPopUpViewController.h"
#import "RoomsDataModel.h"
#import "DateStringSingleton.h"
#import "CalculateDictObject.h"
#import "TravellerDetailVC.h"

@interface PDPCalculatePromoVC ()
{
    NSString *packageType;
    NSString *amountType;
    int accomType;
    NSArray *packageTypeArray;
    NSMutableArray *roomRecordArray;
    NSMutableArray *ItineraryCodeArray;
    NSArray *travellerArrayForCalculation;
    LoadingView *activityLoadingView;
    NSString *selectedLtItineraryCode;
    NSString *bookingAmountString;
    NSString *bookingAmountForBookingSubmit;
    NSDictionary *calculationDict;
    
    NSUserDefaults *LoginStatus;
    NSString *statusForLogin;
    
    NSInteger selectPaymentMethod;
}
@end

@implementation PDPCalculatePromoVC

- (void)viewDidLoad {
    //    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    roomRecordArray = [[NSMutableArray alloc]init];
    RoomsDataModel *roomModel = [[RoomsDataModel alloc]init];
    roomModel.adultCount = [[self.numOfTravellers stringByReplacingOccurrencesOfString:@" Adults" withString:@""]intValue];
    roomModel.infantCount = 0;
    roomModel.arrayChildrensData = 0;
    [roomRecordArray addObject:roomModel];
    
    self.btn_ContinuePress.tag = 0;
    [self.btn_ContinuePress addTarget:self action:@selector(onButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.btn_selectButtOne addTarget:self action:@selector(onSelectPayClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.btn_selectButtTwo addTarget:self action:@selector(onSelectPayClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    LoginStatus=[NSUserDefaults standardUserDefaults];
    statusForLogin= [LoginStatus valueForKey:kLoginStatus];
    amountType = @"A";
    //    amountType = @"F";
    
    
    NSDate *date = [[DateStringSingleton shared]getDateFromStringFormat:@"dd-MM-yyyy" strDateValue:_departureDate];
    NSString *newdate = [[DateStringSingleton shared]getDateTimeStringFormat:@"dd MMM yyyy" DateValue:date];
    self.lbl_CityDate.text = [NSString stringWithFormat:@"%@, %@",_departureCity, newdate];
    if ([self.holidayPkgDetailInPromoVC.arrayAccomTypeList count]>0){
        self.lbl_tourType.text = self.holidayPkgDetailInPromoVC.arrayAccomTypeList.firstObject;
    }else{
        self.lbl_tourType.text = @"Standard";
    }
    self.lbl_numOfRooms.text = self.numOfRooms;
    self.lbl_numOfTraveller.text = self.numOfTravellers;
    
    
    NSLog(@"%@",self.holidayPkgDetailInPromoVC.strPackageSubType);
    NSString* identifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString]; // IOS 6+
    NSLog(@"output is : %@", identifier);
    [self calculatePriceAstra];
}

-(void)viewWillAppear:(BOOL)animated
{
    [CoreUtility reloadRequestID];
    if([[self.holidayPkgDetailInPromoVC.strPackageSubType lowercaseString] isEqualToString:@"fit fixed"]|| [[self.holidayPkgDetailInPromoVC.strPackageSubType lowercaseString] isEqualToString:@"git"])
    {
        if([[self.holidayPkgDetailInPromoVC.strPackageType lowercaseString] isEqualToString:@"international"])
        {
            packageType = kpackageTypeGITInternational;
            
        }
        else
        {
            packageType = kpackageTypeGITDomestic;
        }
    }else
    {
        if([[self.holidayPkgDetailInPromoVC.strPackageType lowercaseString] isEqualToString:@"international"])
        {
            packageType = kpackageTypeFITInternational;
        }
        else
        {
            packageType = kpackageTypeFITDomestic;
        }
    }
    if(self.holidayPkgDetailInPromoVC.stringSelectedAccomType == nil || [self.holidayPkgDetailInPromoVC.stringSelectedAccomType isEqualToString:@""])
    {
        if (packageTypeArray.count != 0)
        {
            NSString *packageTypeLocal = (NSString *)packageTypeArray[0];
            [self setAccomType:@"standard"];
        }
        
    }else
    {
        if ([self.holidayPkgDetailInPromoVC.stringSelectedAccomType isEqualToString:@"0"])
        {
            accomType = 0;
        }
        else if ([self.holidayPkgDetailInPromoVC.stringSelectedAccomType isEqualToString:@"1"])
        {
            accomType = 1;
        }
        else
        {
            accomType = 2;
        }
    }
    if (![statusForLogin isEqualToString:kLoginSuccess]){
        self.btn_ContinuePress.tag = 0;
    }else{
        self.btn_ContinuePress.tag = 1;
//        [self calculatePriceAstra];
        [self netCoreInternationalHolidayCalculate];
    }
}
-(void)setAccomType:(NSString *)accomTypeString
{
    if ([[accomTypeString lowercaseString] isEqualToString:@"standard"])
    {
        self.holidayPkgDetailInPromoVC.stringSelectedAccomType = @"0";
    }
    else if ([[accomTypeString lowercaseString] isEqualToString:@"delux"]||[[accomTypeString lowercaseString] isEqualToString:@"deluxe"])
    {
        self.holidayPkgDetailInPromoVC.stringSelectedAccomType = @"1";
    }
    else
    {
        self.holidayPkgDetailInPromoVC.stringSelectedAccomType = @"2";
    }
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - Reuse of Existing Methods in holiday

-(void)onButtonClicked:(UIButton *) sender {
    
    UIButton *button = (UIButton *)sender;
    if(button.tag == 1)
    {
        //        [self calculatePriceAstra];
        //        //28-02-2018
        //        [self netCoreInternationalHolidayCalculate];
        [self jumpToTravellerView];
    }else
    {
        [self netCoreInternationalHolidayCalculateContinue];
        
        
        
        if (![statusForLogin isEqualToString:kLoginSuccess]){
            NewLoginPopUpViewController *loginView = [[NewLoginPopUpViewController alloc] initWithNibName:@"NewLoginPopUpViewController" bundle:nil];
            
            UINavigationController * loginNavigation = [[UINavigationController alloc] initWithRootViewController:loginView];
            
            [self presentViewController:loginNavigation animated:true completion:^{
                
            }];
        }else{
            [self bookingTypeSubmission];
            
        }
    }
    
    [UIView animateWithDuration:0.4 animations:^{
        
        [self.view layoutIfNeeded];
    }];
    
}
-(void)onSelectPayClicked:(UIButton *) sender {
    UIButton *button = (UIButton *)sender;
    if (button.tag == 0){
        [self.btn_selectButtOne setImage:[UIImage imageNamed:@"circularBoxSelectGray"] forState:UIControlStateNormal];
        [self.btn_selectButtOne setImage:[UIImage imageNamed:@"circularBoxNoSelectBlue"] forState:UIControlStateNormal];
    }else{
        [self.btn_selectButtOne setImage:[UIImage imageNamed:@"circularBoxNoSelect"] forState:UIControlStateNormal];
        [self.btn_selectButtOne setImage:[UIImage imageNamed:@"circularBoxNoSelectBlue"] forState:UIControlStateNormal];
    }
}
-(void)netCoreInternationalHolidayCalculate
{
    int noOfChild = 0;
    int noOfInf = 0;
    int noOfAdult = 0;
    for (int i = 0; i< travellerArrayForCalculation.count; i++)
    {
        TravellerInformationModel *travellerInfoModelInstance = travellerArrayForCalculation[i];
        NSString *travellerRoomingType = travellerInfoModelInstance.travellerType;
        
        if ([travellerRoomingType isEqualToString:kTravellerTypeChild])
        {
            noOfChild = noOfChild + 1;
        }
        else if ([travellerRoomingType isEqualToString:kTravellerTypeAdult])
        {
            noOfAdult = noOfAdult + 1;
        }
        else if ([travellerRoomingType isEqualToString:kTravellerTypeInfant])
        {
            noOfInf = noOfInf + 1;
        }
    }
    
    NSMutableDictionary *payloadList =  [NSMutableDictionary dictionary];
    [payloadList setObject:packageType forKey:@"s^PACKAGE_TYPE"];
    [payloadList setObject:[NSNumber numberWithInteger:roomRecordArray.count] forKey:@"i^ROOM"];
    [payloadList setObject:[NSNumber numberWithInteger:noOfAdult] forKey:@"i^ADULTS"];
    [payloadList setObject:[NSNumber numberWithInteger:noOfChild] forKey:@"i^CHILDREN"];
    [payloadList setObject:[NSNumber numberWithInteger:noOfInf] forKey:@"i^INFANTS"];
    [payloadList setObject:[NSString stringWithFormat:@"%@",self.departureCity] forKey:@"s^DEPARTURE_CITY"];
    [payloadList setObject:[NSString stringWithFormat:@"%@", self.holidayPkgDetailInPromoVC.strPackageId] forKey:@"s^PACKAGE_ID"];
    [payloadList setObject:@"App" forKey:@"s^SOURCE"];
    [payloadList setObject:self.holidayPkgDetailInPromoVC.strPackageType forKey:@"s^TYPE"];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    NSDate *oldFormatDate =  [dateFormatter dateFromString:self.departureDate];
    
    
    NSDateFormatter *dateFormatterNew = [[NSDateFormatter alloc] init];
    [dateFormatterNew setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *newDateString = [dateFormatterNew stringFromDate:oldFormatDate];
    // NSDate *newDate = [dateFormatterNew dateFromString:newDateString];
    
    [payloadList setObject:newDateString forKey:@"d^DATE"]; //YYYY-MM-DD HH:MM:SS
    [payloadList setObject:@"" forKey:@"s^STATE"];
    
    
    [NetCoreAnalyticsVC getNetCorePushNotificationDetailDictionary:payloadList withPayloadCount:128];
}



-(void)netCoreInternationalHolidayCalculateContinue
{
    int noOfChild = 0;
    int noOfInf = 0;
    int noOfAdult = 0;
    
    for (int i = 0; i< travellerArrayForCalculation.count; i++)
    {
        TravellerInformationModel *travellerInfoModelInstance = travellerArrayForCalculation[i];
        NSString *travellerRoomingType = travellerInfoModelInstance.travellerType;
        
        if ([travellerRoomingType isEqualToString:kTravellerTypeChild])
        {
            noOfChild = noOfChild + 1;
        }
        else if ([travellerRoomingType isEqualToString:kTravellerTypeAdult])
        {
            noOfAdult = noOfAdult + 1;
        }
        else if ([travellerRoomingType isEqualToString:kTravellerTypeInfant])
        {
            noOfInf = noOfInf + 1;
        }
    }
    
    
    NSString *paymentOption = @"";
    if ([bookingAmountForBookingSubmit isEqualToString:bookingAmountString])
    {
        paymentOption = @"Booking Amount";
    }
    else
    {
        paymentOption = @"Total Amount";
    }
    
    
    NSMutableDictionary *payloadList =  [NSMutableDictionary dictionary];
    [payloadList setObject:packageType forKey:@"s^PACKAGE_TYPE"];
    [payloadList setObject:[NSNumber numberWithInteger:roomRecordArray.count] forKey:@"i^ROOM"];
    [payloadList setObject:[NSNumber numberWithInteger:noOfAdult] forKey:@"i^ADULTS"];
    [payloadList setObject:[NSNumber numberWithInteger:noOfChild] forKey:@"i^CHILDREN"];
    [payloadList setObject:[NSNumber numberWithInteger:noOfInf] forKey:@"i^INFANTS"];
    [payloadList setObject:[NSString stringWithFormat:@"%@",self.departureCity] forKey:@"s^DEPARTURE_CITY"];
    [payloadList setObject:[NSString stringWithFormat:@"%@", self.holidayPkgDetailInPromoVC.strPackageId] forKey:@"s^PACKAGE_ID"];
    [payloadList setObject:paymentOption forKey:@"s^PAYMENT_OPTION"];
    // [payloadList setObject:bookingAmountForBookingSubmit forKey:@"s^PAYMENT_AMOUNT"];//28-02-2018
    [payloadList setObject:[NSNumber numberWithInteger:[bookingAmountForBookingSubmit integerValue]] forKey:@"i^PAYMENT_AMOUNT"];//changed s^PAYMENT_AMOUNT to i^PAYMENT_AMOUNT : 28-02-2018
    [payloadList setObject:@"App" forKey:@"s^SOURCE"];
    
    if(self.holidayPkgDetailInPromoVC.strPackageType != nil && ![self.holidayPkgDetailInPromoVC.strPackageType isEqualToString:@""])
    {
        [payloadList setObject:self.holidayPkgDetailInPromoVC.strPackageType forKey:@"s^TYPE"]; //uncommented on  28-02-2018
    }
    
    // [NetCoreAnalyticsVC getNetCorePushNotificationDetailDictionary:payloadList withPayloadCount:109]; //28-02-2018
    [NetCoreAnalyticsVC getNetCorePushNotificationDetailDictionary:payloadList withPayloadCount:129]; //28-02-2018
    NSLog(@"Data submitted to the netcore:129");
}


-(void)bookingTypeSubmission{
    NSDictionary *headerDict = [CoreUtility getHeaderDict];
    
    NetworkHelper *helper = [NetworkHelper sharedHelper];
    
    NSString *userid = [[NSUserDefaults standardUserDefaults] valueForKey:kuserDefaultUserId];
    
    NSMutableDictionary *jsonDict = [[NSMutableDictionary alloc] init];
    [jsonDict setValue:[calculationDict valueForKey:@"quotationId"] forKey:@"quotationId"];
    [jsonDict setValue:amountType forKey:@"bookingAmountType"];
    [jsonDict setValue:bookingAmountString forKey:@"bookingAmount"];
    [jsonDict setValue:@"" forKey:@"promoCode"];
    [jsonDict setValue:userid forKey:@"userId"];
    [jsonDict setValue:@"Desktop" forKey:@"appUsed"];
    
    NSString *promocode = @"tcHolidayRS/quotation/promocode";
    [helper getResponseWithRequestType:@"POST" withQueryParam:@"" withPathParam:@"" withJsonParam:jsonDict withHeaders:headerDict withUrl:promocode success:^(NSDictionary *responseDict)
     {
        NSLog(@"Response Dict : %@",responseDict);
        
        dispatch_async(dispatch_get_main_queue(), ^(void)
                       {
            [activityLoadingView removeFromSuperview];
            
            if (responseDict){
                if (responseDict.count>0)
                {
                    NSLog(@"responseDict-->%@",responseDict);
                    if ([[[responseDict valueForKey:@"message"] uppercaseString] isEqualToString:@"SUCCESS"]){
                        
                        [self preconfirmationBookingAstra];
                    }else{
                        [self showAlertViewWithTitle:@"Alert" withMessage:@"some error occurred"];
                        
                    }
                }
                else{
                    [self showAlertViewWithTitle:@"Alert" withMessage:@"some error occurred"];
                    
                }
            }
            else{
                [self showAlertViewWithTitle:@"Alert" withMessage:@"some error occurred"];
                
            }
        });
    }
                               failure:^(NSError *error)
     {dispatch_async(dispatch_get_main_queue(), ^(void)
                     {
         [activityLoadingView removeFromSuperview];
         [super showAlertViewWithTitle:@"Alert" withMessage:@"some error occurred"];
         NSLog(@"Response Dict : %@",error);
     });
    }];
}

-(void)preconfirmationBookingAstra
{
    
    @try
    {
        
        activityLoadingView = [LoadingView loadingViewInView:self.view.superview.superview
                                                  withString:@""
                                           andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
        
        NSDictionary *headerDict = [CoreUtility getHeaderDict];
        
        NetworkHelper *helper = [NetworkHelper sharedHelper];
        
        NSString *queryParam = [NSString stringWithFormat:@"quotationId=%@",[calculationDict valueForKey:@"quotationId"]];
        
        [helper getResponseWithRequestType:@"GET" withQueryParam:queryParam withPathParam:@"" withJsonParam:nil withHeaders:headerDict withUrl:kAstraUrlPreConfirmationBooking success:^(NSDictionary *responseDict)
         {
            NSLog(@"Response Dict : %@",responseDict);
            
            dispatch_async(dispatch_get_main_queue(), ^(void)
                           {
                [activityLoadingView removeFromSuperview];
                if (responseDict)
                {
                    if (responseDict.count>0)
                    {
                        NSLog(@"responseDict %@",responseDict);
                        /*
                         PassangerDetailViewController *passangerVC = [[PassangerDetailViewController alloc]initWithNibName:@"BaseViewController" bundle:nil];
                         passangerVC.DictJourneyDate = dictForDate ;
                         passangerVC.gstTaxDetails = gstTaxDetails;
                         passangerVC.stateCode = selectedStateCode;
                         passangerVC.travellerArrayForCalculation = travellerArrayForCalculation;
                         passangerVC.hubName = departFrom;
                         passangerVC.bookingAmountString = bookingAmountForBookingSubmit;
                         passangerVC.totalAmountString = totalPackageAmountString;
                         passangerVC.accomType = accomType;
                         passangerVC.roomRecordArray = roomRecordArray;
                         passangerVC.packageDetail = self.packageDetail;
                         passangerVC.currencyArray = arrayForALLCurrency;
                         passangerVC.selectedStateDict = selectedStateDict;
                         passangerVC.dictForCalculation = calculationDict;
                         passangerVC.amountType = amountType;
                         [[SlideNavigationController sharedInstance] pushViewController:passangerVC animated:YES];
                         */
                        
                    }else{
                        [activityLoadingView removeFromSuperview];
                        [super showAlertViewWithTitle:@"Alert" withMessage:@"some error occurred"];
                    }
                }else{
                    [activityLoadingView removeFromSuperview];
                    [super showAlertViewWithTitle:@"Alert" withMessage:@"some error occurred"];
                    
                }
            });
        }
                                   failure:^(NSError *error)
         {
            dispatch_async(dispatch_get_main_queue(), ^(void)
                           {
                [activityLoadingView removeFromSuperview];
                [super showAlertViewWithTitle:@"Alert" withMessage:@"some error occurred"];
                NSLog(@"Response Dict : %@",error);
            });
            
        }];
    }
    @catch (NSException *exception)
    {
        NSLog(@"%@", exception.reason);
        [activityLoadingView removeFromSuperview];
    }
    @finally
    {
        NSLog(@"exception finally called");
    }
    
}

-(void)calculatePriceAstra
{
    /*
     "{
     ""mode"":""TCIL"",
     ""ltMarket"":""-1"", ""continent"":""Asia"",""isHsa"":""N"",
     ""room"":[{""roomNo"":1,""noAdult"":2,""noCwb"":0,""noCnbJ"":0,""noCnbS"":0,""inf"":0,""pax"":2}],
     ""optionalsActivities"":[], : Non mandatory
     ""pkgId"":""PKG001424"",
     ""pkgSubType"":2,
     ""pkgSubClass"":""2"",
     ""hub"":""IDR"",
     ""hubCity"":""Indore"",
     ""departureDate"":""23-10-2017"",
     ""ltProdCode"":"""",
     ""userMobileNo"":""9964434636"",
     ""userEmailId"":""aa@hh.ll"",
     ""enquirySource"":""Direct"", : GA - Non mandatory
     ""enquiryMedium"":""Direct"", : GA - Non mandatory
     ""enquiryCampaign"":""not set"", : GA - Non mandatory
     ""LP"":""/"", : GA - Non mandatory
     ""bookURL"":""/holidays/jal-mahotsav?pkgId=PKG001424&packageClas"", : GA - Non mandatory
     ""ltItineraryCode"":""-1"",
     ""regionId"":2,
     ""crmEnquiryId"":"""", Non mandatory
     ""crmStatus"":""N"",
     ""custState"":""27"",
     ""custStateName"":""Maharashtra"",
     ""isUnionTerritory"":""N"",
     ""isGstApplicable"":true
     }"
     */
    @try
    {
        
        activityLoadingView = [LoadingView loadingViewInView:self.view.superview.superview
                                                  withString:@""
                                           andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
        
        
        NSMutableArray *arrayForRooming = [[NSMutableArray alloc] init];
        for (int i = 0; i<roomRecordArray.count; i++)
        {
            
            int numberOfAdult = 0;
            int numberOfCwb = 0;
            int numberOfCnbJ = 0;
            int numberOfCnbS = 0;
            int numberOfInfants = 0;
            int totalPax = 0;
            
            
            RoomsDataModel *roomsModelInsatance = roomRecordArray[i];
            numberOfAdult = roomsModelInsatance.adultCount;
            numberOfInfants = roomsModelInsatance.infantCount;
            NSArray *childArray = roomsModelInsatance.arrayChildrensData;
            //      int childWithCNBCount = 0;
            //    NSMutableArray *childDictArray = [[NSMutableArray alloc]init];
            //  int childWithCWBCount = 0;
            
            for (int j = 0; j<childArray.count; j++)
            {
                NSDictionary *childDict = childArray[j];
                UIButton *button = [childDict valueForKey:@"button"];
                UILabel *labelAge = [childDict valueForKey:@"textField"];
                
                //  NSDictionary *dictForChild ;
                if ([button isSelected]) {
                    
                    //childWithCWBCount = childWithCWBCount + 1;
                    //dictForChild = [[NSMutableDictionary alloc] initWithObjectsAndKeys:labelAge.text,@"CWB", nil];
                    numberOfCwb ++;
                    
                }
                else
                {
                    //childWithCNBCount = childWithCNBCount + 1;
                    //dictForChild = [[NSMutableDictionary alloc] initWithObjectsAndKeys:labelAge.text,@"CNB", nil];
                    
                    if ([packageType isEqualToString:kpackageTypeGITInternational]||[packageType isEqualToString:kpackageTypeFITInternational])
                    {
                        numberOfCnbS ++;
                    }
                    else
                    {
                        if ([labelAge.text intValue] < 5)
                        {
                            numberOfCnbJ ++;
                        }
                        else
                        {
                            numberOfCnbS ++;
                        }
                        
                    }
                }
                // [childDictArray addObject:dictForChild];
            }
            
            
            totalPax = numberOfCnbS + numberOfCnbJ + numberOfAdult + numberOfInfants + numberOfCwb;
            
            
            
            
            //        NSPredicate *pred = [NSPredicate predicateWithBlock:^BOOL(TravellerInformationModel* evaluatedObject, NSDictionary *bindings) {
            //
            //
            //            if (evaluatedObject.travellerRoomNumber == i+1 )
            //            {
            //                return true;
            //            }
            //            else
            //            {
            //                return false;
            //            }
            //
            //        }];
            
            /*  NSArray *results = [travellerArrayForCalculation filteredArrayUsingPredicate:pred];
             
             
             for (int j = 0; j<results.count; j++)
             {
             totalPax ++;
             TravellerInformationModel * modelPassanger = results[j];
             
             if ([modelPassanger.travellerType isEqualToString:kTravellerTypeAdult])
             {
             numberOfAdult++;
             }
             
             if ([modelPassanger.travellerType isEqualToString:kTravellerTypeInfant])
             {
             numberOfInfants++;
             }
             
             if ([modelPassanger.travellerType isEqualToString:kTravellerTypeChild])
             {
             
             if ([modelPassanger.travellerRoomingType isEqualToString:kTravellerRoomTypeCNB])
             {
             if ([packageType isEqualToString:kpackageTypeGITInternational]||[packageType isEqualToString:kpackageTypeFITInternational])
             {
             numberOfCnbS ++;
             }
             else
             {
             if (modelPassanger.travellerAge < 5)
             {
             numberOfCnbJ ++;
             }
             else
             {
             numberOfCnbS ++;
             }
             
             }
             
             }
             else
             {
             numberOfCwb ++;
             }
             }
             }*/
            
            NSMutableDictionary *roomDict = [[NSMutableDictionary alloc] init];
            [roomDict setObject:[NSNumber numberWithInt:i+1] forKey:@"roomNo"];
            [roomDict setObject:[NSNumber numberWithInt:numberOfAdult] forKey:@"noAdult"];
            [roomDict setObject:[NSNumber numberWithInt:numberOfCwb] forKey:@"noCwb"];
            [roomDict setObject:[NSNumber numberWithInt:numberOfCnbJ] forKey:@"noCnbJ"];
            [roomDict setObject:[NSNumber numberWithInt:numberOfCnbS] forKey:@"noCnbS"];
            [roomDict setObject:[NSNumber numberWithInt:numberOfInfants] forKey:@"inf"];
            [roomDict setObject:[NSNumber numberWithInt:totalPax] forKey:@"pax"];
            
            [arrayForRooming addObject:roomDict];
            
        }
        
        
        CoreUtility *coreobj=[CoreUtility sharedclassname];
        NSString *phoneNumber = @"";
        
        if (coreobj.strPhoneNumber.count != 0)
        {
            phoneNumber = coreobj.strPhoneNumber[0];
        }
        
        NSString *emailIDString = [[NSUserDefaults standardUserDefaults] objectForKey:kLoginEmailId];
        
        
        NSMutableDictionary *jsonDict = [[NSMutableDictionary alloc] init];
        
        [jsonDict setObject:self.holidayPkgDetailInPromoVC.packageMode forKey:@"mode"];
        
        // [jsonDict setObject:packageDetail.ltMarket forKey:@"ltMarket"];
        [jsonDict setObject:@"" forKey:@"ltMarket"];
        
        [jsonDict setObject:self.holidayPkgDetailInPromoVC.packageContinent forKey:@"continent"];
        [jsonDict setObject:self.holidayPkgDetailInPromoVC.isHSA forKey:@"isHsa"];
        [jsonDict setObject:[NSArray new] forKey:@"optionalsActivities"];
        [jsonDict setObject:arrayForRooming forKey:@"room"];
        [jsonDict setObject:self.holidayPkgDetailInPromoVC.strPackageId forKey:@"pkgId"];
        [jsonDict setObject:[NSNumber numberWithInteger:[self.holidayPkgDetailInPromoVC.strPackageSubTypeID integerValue]] forKey:@"pkgSubType"];
        [jsonDict setObject:[NSString stringWithFormat:@"%d",accomType] forKey:@"pkgSubClass"]; //fit git
        [jsonDict setObject:[self.selectHubDict valueForKey:@"cityCode"] forKey:@"hub"];
        [jsonDict setObject:[self.selectHubDict valueForKey:@"cityName"] forKey:@"hubCity"];
        [jsonDict setObject:[self.dictForDate valueForKey:@"DATE"] forKey:@"departureDate"];
        [jsonDict setObject:[self.dictForDate valueForKey:@"LT_PROD_CODE"] forKey:@"ltProdCode"];
        [jsonDict setObject:phoneNumber forKey:@"userMobileNo"];
        [jsonDict setObject:@"" forKey:@"userEmailId"];
        
        if (emailIDString)
        {
            [jsonDict setObject:emailIDString forKey:@"userEmailId"];
        }
        
        [jsonDict setObject:@"Direct" forKey:@"enquirySource"];//
        [jsonDict setObject:@"Direct" forKey:@"enquiryMedium"];//
        [jsonDict setObject:@"" forKey:@"enquiryCampaign"];//
        [jsonDict setObject:@"" forKey:@"LP"];//
        [jsonDict setObject:@"" forKey:@"bookURL"];//
        //[jsonDict setObject:packageDetail.ltItineraryCode forKey:@"ltItineraryCode"];
        [jsonDict setObject:[self.dictForDate valueForKey:@"PROD_ITIN_CODE"] forKey:@"ltItineraryCode"];
        
        NSArray *farecaLtItineraryCodeArray = [[NSArray alloc] initWithObjects:[self.dictForDate valueForKey:@"PROD_ITIN_CODE"], nil];
        
        [jsonDict setObject:farecaLtItineraryCodeArray forKey:@"farecaLtItineraryCode"];
        
        
        [jsonDict setObject:@"" forKey:@"regionId"]; //
        
        [jsonDict setObject:self.holidayPkgDetailInPromoVC.productID forKey:@"productId"];
        
        
        if ([self.holidayPkgDetailInPromoVC.strPackageType isEqualToString:@"domestic"])
        {
            [jsonDict setObject:@"2" forKey:@"regionId"];
        }
        else
        {
            if ([[self.holidayPkgDetailInPromoVC.packageContinent uppercaseString] isEqualToString:@"AMERICA"]||[[self.holidayPkgDetailInPromoVC.packageContinent uppercaseString] isEqualToString:@"SOUTH AMERICA"]||[[self.holidayPkgDetailInPromoVC.packageContinent uppercaseString] isEqualToString:@"NORTH AMERICA"] ||[[self.holidayPkgDetailInPromoVC.packageContinent uppercaseString] isEqualToString:@"AFRICA"])
            {
                [jsonDict setObject:@"1" forKey:@"regionId"];
            }
            else
            {
                [jsonDict setObject:@"0" forKey:@"regionId"];
            }
        }
        
        
        [jsonDict setObject:@"" forKey:@"crmEnquiryId"]; //
        [jsonDict setObject:@"" forKey:@"crmStatus"];//?? - empty
        
        //        VJ_Commented
        //        [jsonDict setObject:[selectedStateDict valueForKey:@"gstStateCode"] forKey:@"custState"];
        //        [jsonDict setObject:[selectedStateDict valueForKey:@"gstState"] forKey:@"custStateName"];
        //        [jsonDict setObject:[selectedStateDict valueForKey:@"isUnionTerritory"] forKey:@"isUnionTerritory"];
        
        //        VJ_Added
        [jsonDict setObject:@"" forKey:@"custState"];
        [jsonDict setObject:@"" forKey:@"custStateName"];
        [jsonDict setObject:@"" forKey:@"isUnionTerritory"];
        
        
        [jsonDict setObject:@"true" forKey:@"isGstApplicable"]; //? ?
        
        NSDictionary *headerDict = [CoreUtility getHeaderDict];
        
        NetworkHelper *helper = [NetworkHelper sharedHelper];
        
        [helper getResponseWithRequestType:@"POST" withQueryParam:@"" withPathParam:@"" withJsonParam:jsonDict withHeaders:headerDict withUrl:kAstraUrlPricing success:^(NSDictionary *responseDict)
         {
            NSLog(@"Response Dict : %@",responseDict);
            
            dispatch_async(dispatch_get_main_queue(), ^(void)
                           {
                [activityLoadingView removeFromSuperview];
                
                if (responseDict)
                {
                    if (responseDict.count>0)
                    {
                        NSDictionary *pricingDict = responseDict;
                        
                        calculationDict = pricingDict;
                        
                        if (calculationDict!=nil && calculationDict.count!=0)
                        {
                            
                            NSString *messege = [calculationDict valueForKey:@"message"];
                            
                            if (messege == nil )
                            {
                                
                                double tourCost = [[calculationDict valueForKey:@"tourCost"] doubleValue];
                                
                                if (tourCost != 0)
                                {
                                    if([[self.holidayPkgDetailInPromoVC.strPackageSubType lowercaseString] isEqualToString:@"fit"])
                                    {
                                        
                                        dispatch_async(dispatch_get_main_queue(), ^(void) {
                                            [self calculationForPackageForFIT];
                                            
                                        });
                                    }
                                    else
                                    {
                                        dispatch_async(dispatch_get_main_queue(), ^(void) {
                                            [self calculationForPackageForNONFIT];
                                            
                                        });
                                        
                                    }
                                    
                                    
                                    if (self.holidayPkgDetailInPromoVC.pkgStatusId != 1 || [[self.holidayPkgDetailInPromoVC.stringIsOnReq lowercaseString] isEqualToString:@"y"])
                                    {
                                        
                                        
                                    }
                                    else
                                    {
                                        
                                        
                                        dispatch_async(dispatch_get_main_queue(), ^(void) {
                                            [self.view layoutIfNeeded];
                                            
                                        });
                                        
                                    }
                                    
                                    [UIView animateWithDuration:0.4 animations:^{
                                        
                                        [self.view layoutIfNeeded];
                                    }];
                                }
                                else
                                {
                                    [super showAlertViewWithTitle:@"Alert" withMessage:@"Price not available for this package"];
                                    
                                }
                                
                            }
                            else
                            {
                                [super showAlertViewWithTitle:@"Alert" withMessage:messege];
                            }
                            
                            
                            
                        }
                        
                    }
                    else
                    {
                        [super showAlertViewWithTitle:@"Alert" withMessage:@"some error occurred"];
                        
                    }
                }
                else
                {
                    [super showAlertViewWithTitle:@"Alert" withMessage:@"some error occurred"];
                    
                }
            });
            
        }
                                   failure:^(NSError *error)
         {dispatch_async(dispatch_get_main_queue(), ^(void)
                         {
             [activityLoadingView removeFromSuperview];
             [super showAlertViewWithTitle:@"Alert" withMessage:@"some error occurred"];
             NSLog(@"Response Dict : %@",error);
         });
            
        }];
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"%@", exception.reason);
        [activityLoadingView removeFromSuperview];
    }
    @finally
    {
        NSLog(@"exception finally called");
    }
    
}

-(void)calculationForPackageForFIT{
    CalculateDictObject *calObject = [[CalculateDictObject alloc]initWithCalculateObjectDict:calculationDict];
    self.lbl_titletotalCalculateCostINR.text = [NSString stringWithFormat:@"TotalTour Cost %@ calculated @ %@ %0.2f",calObject.currencycodeOne,calObject.currencycodeTwo,calObject.currencyRateOne];
    self.lbl_tourCostINR.text = [NSString stringWithFormat:@"%@ %ld +  %ld",calObject.currencycodeOne,(long)calObject.amountOne,(long)calObject.amountTwo];
}
-(void)calculationForPackageForNONFIT{
    NSLog(@"%@",calculationDict);
    CalculateDictObject *calObject = [[CalculateDictObject alloc]initWithCalculateObjectDict:calculationDict];
    self.lbl_titletotalCalculateCostINR.text = [NSString stringWithFormat:@"TotalTour Cost %@ calculated @ %@ %0.2f",calObject.currencycodeOne,calObject.currencycodeTwo,calObject.currencyRateOne];
    self.lbl_tourCostINR.text = [NSString stringWithFormat:@"%@ %ld +  %@",calObject.currencycodeOne,(long)calObject.amountOne,[self getNumberFormatterString:(long)calObject.amountTwo]];
    self.lbl_totalTourCost.text = [NSString stringWithFormat:@"%@",[self getNumberFormatterString:(long)calObject.totaltourCost]];
    self.lbl_payFullPayment.text = [NSString stringWithFormat:@"%@",[self getNumberFormatterString:(long)calObject.totaltourCost]];
}
-(void)jumpToTravellerView{
    TravellerDetailVC *controller = [[TravellerDetailVC alloc] initWithNibName:@"TravellerDetailVC" bundle:nil];
    [self.navigationController pushViewController:controller animated:YES];
}
#pragma mark - Reuse of Existing Methods in holiday
- (IBAction)btn_BackPress:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
