//
//  PopUpForSellBranchViewController.swift
//  holidays
//
//  Created by Swapnil on 13/01/18.
//  Copyright © 2018 Mobicule. All rights reserved.
//

import UIKit

class PopUpForSellBranchViewController: UIViewController
{
    @IBOutlet weak var labelData: UILabel!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        let arrayOfLines = ["Sell at Branch option means no blocking of rates online and you have to pay prevailing rate of exchange at the time of encashment.","After confirming, our forex expert will call you and guide about next steps.","We do not accept coins for encashment."]
        
        for value in arrayOfLines
        {
            self.labelData.text = labelData.text!  + " • " + value + "\n"
        }
    }
    
    @IBAction func closeButtonClicked(_ sender: UIButton)
    {
        self.dismiss(animated: false, completion: nil)
    }
}
