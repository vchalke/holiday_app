//
//  ItineraryVC.m
//  holidays
//
//  Created by Pushpendra Singh on 19/08/15.
//  Copyright (c) 2015 Mobicule. All rights reserved.
//

#import "ItineraryVC.h"
#import "BookNowViewController.h"
#import "UIImageView+WebCache.h"

@interface ItineraryVC ()

@end

@implementation ItineraryVC{
    
    UIView *tapview;
    UILabel *preLabel;
    UITapGestureRecognizer *tmpTap;
    NSString *strDescription;
}


- (void)viewDidLoad {
    
    [super viewDidLoad];
    
   
    // Do any additional setup after loading the view from its nib.
    tapview=[[UIView alloc]init];
    tapview.layer.borderColor=[UIColor whiteColor].CGColor;
    tapview.layer.borderWidth=1.0;
    NSLog(@"%@",self.holidayPackageDetail.arrayItinerary);
    
    NSLog(@"%@",self.holidayPackageDetail);
    
    //itineraryDay
    
   NSSortDescriptor *brandDescriptor = [[NSSortDescriptor alloc] initWithKey:@"itineraryDay" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:brandDescriptor];
    NSArray *sortedArray = [_holidayPackageDetail.arrayItinerary sortedArrayUsingDescriptors:sortDescriptors];

    _holidayPackageDetail.arrayItinerary = sortedArray;
    
    for (int i=0; i<self.holidayPackageDetail.arrayItinerary.count; i++) {
        
        UILabel *lblNumber=[[UILabel alloc]initWithFrame:CGRectMake(i*40, 0, 30, 30)];
        lblNumber.text=[NSString stringWithFormat:@"%d",i+1];
        [_scrollNumber addSubview:lblNumber];
        
        lblNumber.textAlignment=NSTextAlignmentCenter;
        lblNumber.textColor=[UIColor lightGrayColor];
        [lblNumber setUserInteractionEnabled:YES];
        
        UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapNumber:)];
        tap.numberOfTapsRequired=1;
        if (i==0) {
            tmpTap=tap;
        }
        
        [lblNumber addGestureRecognizer:tap];
        
    }
    self.iternaryMapView.hidden = YES;
    [_scrollNumber setContentSize:CGSizeMake(40* self.holidayPackageDetail.arrayItinerary.count, 30)];
    
    if (self.holidayPackageDetail.arrayItinerary.count !=0 )
    {
         [self tapNumber:tmpTap];
    }
    
    if([_holidayPackageDetail.strPackageType caseInsensitiveCompare:@"International"]==NSOrderedSame)
    {
        
      [FIRAnalytics logEventWithName:@"International_Holiday_Itinerary_PDP" parameters:@{kFIRParameterItemName:[NSString  stringWithFormat:@"PackageName: %@, PackageType :%@",_holidayPackageDetail.strPackageName,_holidayPackageDetail.strPackageType]}];
    
    }
    else
    {
      [FIRAnalytics logEventWithName:@"Indian_Holiday_Itinerary_PDP" parameters:@{kFIRParameterItemName:[NSString  stringWithFormat:@"PackageName: %@, PackageType :%@",_holidayPackageDetail.strPackageName,_holidayPackageDetail.strPackageType]}];
    }

}
-(void)plotMap:(NSArray *)citiesArray
{
    NSMutableArray *locationArray = [[NSMutableArray alloc]init];
    
    for (int i = 0; i < citiesArray.count; i++)
    {
      NSDictionary *dict = [self getLocationFromAddressString:citiesArray[i]];
        if (dict != nil)
        {
           [locationArray addObject:dict];
        }
    }
    
    CLLocationCoordinate2D coordinateArray[locationArray.count];
    
    for (int j = 0; j<locationArray.count ; j++)
    {
        NSDictionary *dict = locationArray[j];
        NSString *lat = [dict valueForKey:@"latitude"];
        NSString *longi = [dict valueForKey:@"longitude"];
        coordinateArray[j] = CLLocationCoordinate2DMake([lat floatValue], [longi floatValue]);
    }
    
    
    
    self.routeLine = [MKPolyline polylineWithCoordinates:coordinateArray count:locationArray.count];
    
    [self.iternaryMapView setVisibleMapRect:[self.routeLine boundingMapRect]]; //If you want the route to be visible
    
    [self.iternaryMapView addOverlay:self.routeLine];

}

-(MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id<MKOverlay>)overlay
{
    if(overlay == self.routeLine)
    {
        if(nil == self.routeLineView)
        {
            self.routeLineView = [[MKPolylineView alloc] initWithPolyline:self.routeLine] ;
            self.routeLineView.fillColor = [UIColor redColor];
            self.routeLineView.strokeColor = [UIColor redColor];
            self.routeLineView.lineWidth = 5;
            
        }
        
        return self.routeLineView;
    }
    
    return nil;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark- tap NUmber

-(void)tapNumber:(UITapGestureRecognizer*)tap{
    
    self.iternaryMapView.hidden = YES;
    NSLog(@"Tap label");
    
    id lab= [tap view];
    
    UILabel *label=(UILabel *)lab;
    label.textColor=[UIColor whiteColor];
    
    if (preLabel)
    {
       preLabel.textColor=[UIColor lightGrayColor];
    }
    
    preLabel=label;
    
    tapview.frame=label.frame;
    tapview.layer.cornerRadius=tapview.bounds.size.height/2;
    tapview.clipsToBounds=YES;
    [_scrollNumber addSubview:tapview];
    
    NSDictionary *daysDict = [self.holidayPackageDetail.arrayItinerary objectAtIndex:[label.text intValue]-1];
    
   // NSString* encodedText = [[daysDict valueForKey:@"image"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    //NSString* encodedText = [[daysDict valueForKey:@"image"]
    
    NSString* encodedText = [[daysDict valueForKey:@"image"] stringByReplacingOccurrencesOfString:@" " withString:@"_"];

    NSString *stringUrl = [NSString stringWithFormat:@"%@/images/holidays/%@/itinerary/%@",kUrlForImage,self.holidayPackageDetail.strPackageId,encodedText]; // not gettingImagePath
    
    NSURL *url = [NSURL URLWithString:stringUrl];
    if (url != nil)
    {
       //self.iternaryImageView.image  = [UIImage imageWithData:[NSData dataWithContentsOfURL:url]];
        [self.iternaryImageView sd_setImageWithURL:url
                       placeholderImage:[UIImage imageNamed:@"defaultBanner"] options:/* DISABLES CODE */ (0) == 0 ? SDWebImageRefreshCached : 0];
    }
    
    NSDictionary *cityCodeDict =  [daysDict valueForKey:@"cityCode"];
    
    self.txtNameCity.text = [cityCodeDict valueForKey:@"cityName"];
    
    strDescription = [daysDict valueForKey:@"itineraryDescription"];

    [self.iternaryWebView loadHTMLString:strDescription baseURL:nil];
   
   // [self.iternaryWebView reload];
}


- (IBAction)onMapViewButtonClicked:(id)sender
{
    if ([self.holidayPackageDetail.strPackageType caseInsensitiveCompare:@"International"]==NSOrderedSame)
    {
        [FIRAnalytics logEventWithName:@"International_Holidays_MAP" parameters:@{kFIRParameterItemName:[NSString stringWithFormat:@"PackageName :%@, PackageType :%@",self.holidayPackageDetail.strPackageName,self.holidayPackageDetail.strPackageType]}];
    }
    else
    {
         [FIRAnalytics logEventWithName:@"Indian_Holidays_MAP" parameters:@{kFIRParameterItemName:[NSString stringWithFormat:@"PackageName :%@, PackageType :%@",self.holidayPackageDetail.strPackageName,self.holidayPackageDetail.strPackageType]}];
    }
    
    self.iternaryMapView.hidden = NO;
    
    NSMutableArray *cityArray = [[NSMutableArray alloc]init];
    
    for (int i = 0; i<self.holidayPackageDetail.arrayItinerary.count; i++)
    {
        NSDictionary *daysDict = self.holidayPackageDetail.arrayItinerary[i];
        NSString *cityName = [daysDict valueForKey:@"destination"];
        [cityArray addObject:cityName];
    }
    
    [self plotMap:cityArray];
}

-(NSDictionary *) getLocationFromAddressString: (NSString*) addressStr
{
    double latitude = 0, longitude = 0;
    NSString *esc_addr =  [addressStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *req = [NSString stringWithFormat:@"http://maps.google.com/maps/api/geocode/json?sensor=false&address=%@", esc_addr];
    NSString *result = [NSString stringWithContentsOfURL:[NSURL URLWithString:req] encoding:NSUTF8StringEncoding error:NULL];
    if (result) {
        NSScanner *scanner = [NSScanner scannerWithString:result];
        if ([scanner scanUpToString:@"\"lat\" :" intoString:nil] && [scanner scanString:@"\"lat\" :" intoString:nil]) {
            [scanner scanDouble:&latitude];
            if ([scanner scanUpToString:@"\"lng\" :" intoString:nil] && [scanner scanString:@"\"lng\" :" intoString:nil]) {
                [scanner scanDouble:&longitude];
            }
        }
    }
    CLLocationCoordinate2D center;
    center.latitude=latitude;
    center.longitude = longitude;
    NSDictionary *dict = [[NSDictionary alloc]initWithObjectsAndKeys:[NSString stringWithFormat:@"%f", center.latitude],@"latitude",[NSString stringWithFormat:@"%f", center.longitude],@"longitude",nil] ;
    NSLog(@"View Controller get Location Logitute : %f",center.latitude);
    NSLog(@"View Controller get Location Latitute : %f",center.longitude);
    return dict;
    
}


- (IBAction)onSideArrowClicked:(id)sender {
    
    NSLog(@"%d",_scrollNumber.contentOffset.x < 40);
    if (_scrollNumber.contentOffset.x < (40 * self.holidayPackageDetail.arrayItinerary.count-1) -_scrollNumber.frame.size.width ) {
        
        [UIView animateWithDuration:0.8 animations:^{
            _scrollNumber.contentOffset = CGPointMake(_scrollNumber.contentOffset.x+50, 0);
        }];
        
    }
}

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    return YES;
}


@end
