//
//  ViewController.h
//  holidays
//
//  Created by ketan on 28/12/15.
//  Copyright © 2015 Mobicule. All rights reserved.
//

#import "BaseViewController.h"


@interface WebViewController : BaseViewController<UIWebViewDelegate>
{
    LoadingView *activityLoadingView;
}
@property (strong, nonatomic) IBOutlet UIView *viewForWebView;
@property (strong,nonatomic) NSURL *url;
@property (strong,nonatomic) NSString *headerString;
@property (weak, nonatomic) IBOutlet UIWebView *webViewForWebController;


@end
