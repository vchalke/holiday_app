//
//  AdressPopUpVC.h
//  holidays
//
//  Created by ketan on 06/11/15.
//  Copyright © 2015 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPKeyboardAvoidingScrollView.h"
@protocol AddressVCSubmit;

@interface AdressPopUpVC : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *adressScrollView;

@property (weak, nonatomic) IBOutlet UITextField *txtFieldAddress1;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldAddress2;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldCity;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldState;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldZip;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldContactNo;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldEmailID;
- (IBAction)onContinueButtonClicked:(id)sender;
@property (assign, nonatomic) id <AddressVCSubmit>delegate;
@property (strong, nonatomic) IBOutlet UIView *viewForElements;
@property (weak, nonatomic) IBOutlet UITextField *textFieldGSTInNumber;
@property (nonatomic,strong) NSString *stateCode;

@end

@protocol AddressVCSubmit<NSObject>
@optional
- (void)continueButtonClicked:(AdressPopUpVC*)secondDetailViewController;
@end
