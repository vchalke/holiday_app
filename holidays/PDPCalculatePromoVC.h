//
//  PDPCalculatePromoVC.h
//  holidays
//
//  Created by Kush_Tech on 26/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HolidayPackageDetail.h"
#import "NewMasterVC.h"
NS_ASSUME_NONNULL_BEGIN

@interface PDPCalculatePromoVC : NewMasterVC
@property (strong ,nonatomic) HolidayPackageDetail *holidayPkgDetailInPromoVC;
@property (strong ,nonatomic) NSDictionary *dictForDate;
@property (strong ,nonatomic) NSDictionary *selectHubDict;
@property (strong ,nonatomic) NSString *departureDate;
@property (strong ,nonatomic) NSString *departureCity;
@property (strong ,nonatomic) NSString *numOfTravellers;
@property (strong ,nonatomic) NSString *numOfRooms;
@property (weak, nonatomic) IBOutlet UIButton *btn_ContinuePress;


@property (weak, nonatomic) IBOutlet UILabel *lbl_CityDate;
@property (weak, nonatomic) IBOutlet UILabel *lbl_tourType;
@property (weak, nonatomic) IBOutlet UILabel *lbl_numOfRooms;
@property (weak, nonatomic) IBOutlet UILabel *lbl_numOfTraveller;
@property (weak, nonatomic) IBOutlet UILabel *lbl_tourCostINR;
@property (weak, nonatomic) IBOutlet UILabel *lbl_discount;
@property (weak, nonatomic) IBOutlet UILabel *lbl_promoCodeDiscount;
@property (weak, nonatomic) IBOutlet UILabel *lbl_gstCount;
@property (weak, nonatomic) IBOutlet UILabel *lbl_totalTourCost;
@property (weak, nonatomic) IBOutlet UILabel *lbl_titletotalCalculateCostINR;
@property (weak, nonatomic) IBOutlet UILabel *lbl_titlegst;

@property (weak, nonatomic) IBOutlet UILabel *lbl_payFullPayment;

@property (weak, nonatomic) IBOutlet UIButton *btn_selectButtOne;
@property (weak, nonatomic) IBOutlet UIButton *btn_selectButtTwo;
@end

NS_ASSUME_NONNULL_END
