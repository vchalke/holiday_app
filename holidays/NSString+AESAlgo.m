//
//  NSString+AES.m
//  AESEncryptionDemo
//

//

#import "NSString+AESAlgo.h"

@implementation NSString (AESAlgo)
- (NSString *)AES128EncryptWithKey:(NSString *)key
{
    NSData *plainData = [self dataUsingEncoding:NSUTF8StringEncoding];
    NSData *encryptedData = [plainData AES128EncryptWithKey:key];
    
    NSString *encryptedString = [encryptedData base64EncodedStringWithOptions:NSDataBase64Encoding76CharacterLineLength];
    
    return encryptedString;
}

- (NSString *)AES128DecryptWithKey:(NSString *)key
{
    NSData *data   = [[NSData alloc]initWithBase64EncodedString:self options:0];
    
    NSData *plainData = [data AES128DecryptWithKey:key];
    
    NSString *plainString = [[NSString alloc] initWithData:plainData encoding:NSUTF8StringEncoding];
    
    return plainString;
}


@end
