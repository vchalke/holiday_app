//
//  IncludeExcludeObject.m
//  holidays
//
//  Created by Kush_Tech on 04/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "IncludeExcludeObject.h"

@implementation IncludeExcludeObject

-(instancetype)initWithIncludeExcludeDict:(NSDictionary *)dictionary{
    if ([super init])
    {
        self.excludes = [dictionary valueForKey:@"excludes"];
        self.holidayIncludeExcludeId = [[dictionary valueForKey:@"holidayIncludeExcludeId"] integerValue];
        self.includes = [dictionary valueForKey:@"includes"];
        self.inclusionsSrp = [dictionary valueForKey:@"inclusionsSrp"];
        NSString *boolString = [dictionary valueForKey:@"isActive"];
        self.isActive = [[boolString uppercaseString] isEqualToString:@"Y"] ? YES : NO;
        self.packageClassId = [[dictionary valueForKey:@"packageClassId"] integerValue];
    }
    
    return self;
}
@end
