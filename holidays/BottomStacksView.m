//
//  BottomStacksView.m
//  holidays
//
//  Created by Kush_Team on 01/06/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "BottomStacksView.h"
#import "ColourClass.h"

@implementation BottomStacksView
- (instancetype)initWithCoder:(NSCoder *)aDecoder {
     self = [super initWithCoder:aDecoder];
     if(self) {
        [self loadNib];
    }
     return self;

}

- (instancetype)initWithFrame:(CGRect)frame {
     self = [super initWithFrame:frame];
     if(self) {
        [self loadNib];
    }
     return self;
}

-(void)loadNib{
    
    UIView *view = [[[NSBundle bundleForClass:[self class]]loadNibNamed:@"BottomStacksView" owner:self options:nil] firstObject];
    view.frame = self.bounds;
    NSLog(@"Custom View Width %0.2f",view.frame.size.width);
    [self addSubview:view];
    [self layoutIfNeeded];
    [self setNeedsLayout];
    imgArray = [NSArray arrayWithObjects:@"home",@"booking",@"myAccount",@"notification",@"threeDots", nil];
    selectImgArray = [NSArray arrayWithObjects:@"homeInBlue",@"bookingInBlue",@"myAccountInBlue",@"notificationInBlue",@"threeDotsInBlue", nil];
    imgObjArray = [NSArray arrayWithObjects:_img_home,_img_booking,_img_myAccount,_img_notification,_img_more, nil];
    lblObjArray = [NSArray arrayWithObjects:_lbl_home,_lbl_booking,_lbl_myAccount,_lbl_notification,_lbl_more, nil];

}
-(void)buttonSelected:(NSInteger)index{
    for (int i = 0;i<[selectImgArray count];i++){
        if (index == i){
            UILabel *lable = [lblObjArray objectAtIndex:index];
            lable.textColor = [ColourClass colorWithHexString:@"004E9B"];
            UIImageView *imgView = [imgObjArray objectAtIndex:index];
            imgView.image = [UIImage imageNamed:[selectImgArray objectAtIndex:index]];
        }else{
           UILabel *lable = [lblObjArray objectAtIndex:index];
            lable.textColor = [UIColor blackColor];//FFFFFF
            UIImageView *imgView = [imgObjArray objectAtIndex:index];
            imgView.image = [UIImage imageNamed:[selectImgArray objectAtIndex:index]];
        }
    }
}
- (IBAction)btn_HonePress:(id)sender {
    [self.bottomStackDelegate homeButtonClick];
}
- (IBAction)btn_bookingPress:(id)sender {
    [self.bottomStackDelegate bookingButtonClick];
}
- (IBAction)btn_profilePress:(id)sender {
    [self.bottomStackDelegate profileButtonClick];
}
- (IBAction)btn_NotificationPress:(id)sender {
    [self.bottomStackDelegate notificationButtonClick];
}
- (IBAction)btn_MorePress:(id)sender {
    [self.bottomStackDelegate moreButtonClick];
}
@end
