//
//  TransferObject.m
//  holidays
//
//  Created by Kush_Tech on 04/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "TransferObject.h"

@implementation TransferObject

-(instancetype)initWithTransferDict:(NSDictionary *)dictionary{
    if ([super init])
    {
      self.transferdescription = [dictionary valueForKey:@"description"];
//        self.isActive = [dictionary valueForKey:@"isActive"];
        NSString *boolString = [dictionary valueForKey:@"isActive"];
        self.isActive = [[boolString uppercaseString] isEqualToString:@"Y"] ? YES : NO;
        self.packageClassId = [[dictionary valueForKey:@"packageClassId"] integerValue];
        self.transfersId = [[dictionary valueForKey:@"transfersId"] integerValue];
        
    }
    return self;
    
}
@end
