//
//  MobiculeUtilityManager.h
//  MobiculeDeviceUtility
//
//  Created by Kishan Gupta on 03/11/15.
//  Copyright (c) 2015 mobicule. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "MobiculeLogger.h"


@interface MobiculeUtilityManager : NSObject <CLLocationManagerDelegate>

/**
 *  This is singleton method which use to get instance of MobiculeUtilityManager.
 */
+ (MobiculeUtilityManager*)getInstance;

/**
 *  This is use get device OS Version .
 *
 *  @return Response is string type associated with particular device OS Version.
 */
+ (NSString*)getOSVersion ;

/**
 *  This is use get device model .
 *
 *  @return Response is string type associated with particular device model.
 */
+ (NSString*)getDeviceModel ;

/**
 *  This is use get application version .
 *
 *  @return Response is string type associated with particular application version.
 */
+ (NSString*)getApplicationVersion ;

/**
 *  This is use get application name .
 *
 *  @return Response is string type associated with particular application name .
 */
+ (NSString*)getApplicationName ;

/**
 *  This is use get application bundle identifier .
 *
 *  @return Response is string type associated with particular application bundle identifier .
 */
+ (NSString*)getApplicationBundleIdentifier;

/**
 *  This is use get application UUID.
 *
 *  @return Response is string type associated with particular application UUID.
 */
+ (NSString*)getApplicationUUID;

/**
 *  This is use get current LatLong of device.
 *
 *  @return NSDictionary of currentLatLong for particular device.
 */
- (NSDictionary*)getCurrentLatLongOfdevice ;

/**
 *  This is use to get battery level.
 *
 *  @return float value of battery level.
 */
+ (float)getBatteryLevel ;

/**
 *  This is use to get total internal memory.
 *
 *  @return Total internal memory of device.
 */
+ (NSString*)getTotalInternalMemorySize ;

/**
 *  This is use to get available internal memory.
 *
 *  @return available internal memory of device.
 */
+ (NSString*)getAvailableInternalMemorySize ;

/**
 *  This is use to get used internal memory.
 *
 *  @return used internal memory of device.
 */
+ (NSString*)getUsedInternalMemorySize;

/**
 *  This is use to get total RAM size.
 *
 *  @return total RAM size of device.
 */
+ (NSString*)getTotalRamSize;

/**
 *  This is use to get aviailable RAM size.
 *
 *  @return aviailable RAM size of device.
 */
+ (NSString*)getAvialableRamSize;

/**
 *  This is use to get used RAM size.
 *
 *  @return used RAM size of device.
 */
+ (NSString*)getUsedRamSize ;

/**
 *  This is use to create zip file.
 *
 *  @param destinationPath This is path where wants to create zip file.
 *
 *  @param sourcePath This is path of source file in array type.
 *
 *  @return Response is Yes or No.
 */
+ (BOOL)createZipFileAtDestinationPath:(NSString *)destinationPath withFilesFromInputPaths:(NSArray*)sourcePath;

/**
 *  This is use to create zip file.
 *
 *  @param destinationPath This is path where wants to create zip file.
 *
 *  @param sourcePath This is path of directory in string type.
 *
 *  @return Response is Yes or No.
 */
+ (BOOL)createZipFileAtDestinationPath:(NSString *)destinationPath withDirectoryFromInputPath:(NSString*)sourcePath ;

/**
 *  This is use to Unzip the file.
 *
 *  @param destinationPath This is path where wants to create unzip file.
 *
 *  @param sourcePath This is path of source file in string type.
 *
 *  @return Response is Yes or No.
 */
+ (BOOL)createUnZipFileAtDestinationPath:(NSString *)destinationPath withFileFromInputPath:(NSString *)sourcePath;


//sskeychain

/**
 Returns a string containing the password for a given account and service, or `nil` if the Keychain doesn't have a
 password for the given parameters.
 
 @param serviceName The service for which to return the corresponding password.
 
 @param account The account for which to return the corresponding password.
 
 @return Returns a string containing the password for a given account and service, or `nil` if the Keychain doesn't
 have a password for the given parameters.
 */
+ (NSString *)passwordForService:(NSString *)serviceName account:(NSString *)account error:(NSError **)error;

/**
 Returns a nsdata containing the password for a given account and service, or `nil` if the Keychain doesn't have a
 password for the given parameters.
 
 @param serviceName The service for which to return the corresponding password.
 
 @param account The account for which to return the corresponding password.
 
 @return Returns a nsdata containing the password for a given account and service, or `nil` if the Keychain doesn't
 have a password for the given parameters.
 */
+ (NSData *)passwordDataForService:(NSString *)serviceName account:(NSString *)account error:(NSError **)error;


/**
 Deletes a password from the Keychain.
 
 @param serviceName The service for which to delete the corresponding password.
 
 @param account The account for which to delete the corresponding password.
 
 @return Returns `YES` on success, or `NO` on failure.
 */
+ (BOOL)deletePasswordForService:(NSString *)serviceName account:(NSString *)account error:(NSError **)error;


/**
 Sets a password in the Keychain.
 
 @param password The password to store in the Keychain.
 
 @param serviceName The service for which to set the corresponding password.
 
 @param account The account for which to set the corresponding password.
 
 @return Returns `YES` on success, or `NO` on failure.
 */
+ (BOOL)setPassword:(NSString *)password forService:(NSString *)serviceName account:(NSString *)account error:(NSError **)error;

/**
 Sets a password in the Keychain.
 
 @param password The password to store in the Keychain.
 
 @param serviceName The service for which to set the corresponding password.
 
 @param account The account for which to set the corresponding password.
 
 @return Returns `YES` on success, or `NO` on failure.
 */
+ (BOOL)setPasswordData:(NSData *)password forService:(NSString *)serviceName account:(NSString *)account error:(NSError **)error;

/**
 Returns an array containing the Keychain's accounts, or `nil` if the Keychain has no accounts.
 
 See the `NSString` constants declared in SSKeychain.h for a list of keys that can be used when accessing the
 dictionaries returned by this method.
 
 @return An array of dictionaries containing the Keychain's accounts, or `nil` if the Keychain doesn't have any
 accounts. The order of the objects in the array isn't defined.
 */
+ (NSArray *)allAccounts:(NSError *__autoreleasing *)error;


/**
 Returns an array containing the Keychain's accounts for a given service, or `nil` if the Keychain doesn't have any
 accounts for the given service.
 
 See the `NSString` constants declared in SSKeychain.h for a list of keys that can be used when accessing the
 dictionaries returned by this method.
 
 @param serviceName The service for which to return the corresponding accounts.
 
 @return An array of dictionaries containing the Keychain's accounts for a given `serviceName`, or `nil` if the Keychain
 doesn't have any accounts for the given `serviceName`. The order of the objects in the array isn't defined.
 */
+ (NSArray *)accountsForService:(NSString *)serviceName error:(NSError *__autoreleasing *)error;
@end
