//
//  RegisterUserViewController.m
//  holidays
//
//  Created by ketan on 04/12/17.
//  Copyright © 2017 Mobicule. All rights reserved.
//

#import "RegisterUserViewController.h"
#import "LoadingView.h"

@interface RegisterUserViewController ()<UITextFieldDelegate>
{
    LoadingView *activityLoadingView;
}
@end

@implementation RegisterUserViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (IBAction)onBackButtonClicked:(id)sender
{
    if([self.signUpVC isEqualToString:@"signUpVC"])
    {
        [self dismissViewControllerAnimated:NO completion:nil];
    }
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
        //[[SlideNavigationController sharedInstance] popViewControllerAnimated:YES];
    }
}
- (IBAction)onSignUpButtonClicked:(id)sender
{
    
    if ([_textfieldTitle.text isEqualToString:@""])
    {
        [self showAlertViewWithTitle:@"Alert" withMessage:@"Please enter title."];
    }
    else if ([_textfieldMobileNumber.text isEqualToString:@""])
    {
          [self showAlertViewWithTitle:@"Alert" withMessage:@"Please enter Mobile Number."];
    }
    else if ([_textFieldLastName.text isEqualToString:@""])
    {
          [self showAlertViewWithTitle:@"Alert" withMessage:@"Please enter last name."];
    }
    else if ([_textFieldfirstname.text isEqualToString:@""])
    {
  [self showAlertViewWithTitle:@"Alert" withMessage:@"Please enter first name."];
    }
    else
    {
        [self createNewUser];
    }
    
  
}

-(void)createNewUser
{
    
    activityLoadingView = [LoadingView loadingViewInView:self.view.superview.superview
                                              withString:@""
                                       andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
    
    @try
    {
        
        NSString *text1 = [_dictforEncryption valueForKey:@"text1"];
        NSString *text2 = [_dictforEncryption valueForKey:@"text2"];
        NSString *text3 = [_dictforEncryption valueForKey:@"text3"];

        
        NetworkHelper *helper = [NetworkHelper sharedHelper];
        
        NSMutableDictionary *dictForRequest = [[NSMutableDictionary alloc] init];
        [dictForRequest setObject:_textFieldfirstname.text forKey:@"firstName"];
        [dictForRequest setObject:_textFieldLastName.text forKey:@"lastName"];
        [dictForRequest setObject:_textfieldMobileNumber.text forKey:@"mobileNo"];
        [dictForRequest setObject:_textfieldTitle.text forKey:@"title"];
        [dictForRequest setObject:_userId forKey:@"userId"];
        [dictForRequest setObject:text1 forKey:@"text1"];
        [dictForRequest setObject:text2 forKey:@"text2"];
        [dictForRequest setObject:text3 forKey:@"text3"];

        
        NSDictionary *headerDict = [CoreUtility getHeaderDict];
        
        [helper getResponseWithRequestType:@"POST" withQueryParam:@"" withPathParam:@"" withJsonParam:dictForRequest withHeaders:headerDict withUrl:kAstraCreateLogin success:^(NSDictionary *responseDict)
         {
              NSLog(@"Response Dict : %@",responseDict);
            
             dispatch_async(dispatch_get_main_queue(), ^(void)
             {
                 [activityLoadingView removeFromSuperview];
                 if (responseDict)
                 {
                     if (responseDict.count>0)
                     {
                         
                         if ([[[responseDict valueForKey:@"message"] lowercaseString] isEqualToString:@"false"])
                         {
                             NSString *reasonOfMessage = [responseDict valueForKey:@"reasonOfMessage"];
                             
                             UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Alert"
                                                                                                message:reasonOfMessage
                                                                                         preferredStyle: UIAlertControllerStyleAlert];
                             
                             UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                                 
                                 [alertView dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
                             [alertView addAction:actionOk];
                             [self presentViewController:alertView animated:YES completion:nil];
                             
                         }
                         else
                         {
                             
                          /*
                         {"accountType":"TC","familyTreeList":[],"invalidLoginCount":0,"message":"true","reasonOfMessage":"Account created","userAddressList":[],"userDetail":{"accountType":"TC","age":0,"custId":"1309","email":"ketanbswami@gmail.com","fname":"ketan","lName":"swami","mobileNo":"9858585458","role":"B2C Customer","roleId":2,"userId":"ketanbswami@gmail.com","userTypeId":"Customer"}}
                         */
                         
                        CoreUtility * coreObj=[CoreUtility sharedclassname];
                        [coreObj.strPhoneNumber addObject:_textfieldMobileNumber.text];
                             
                             
                             UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Success"
                                                                                                message:@"User Registered Successfully"
                                                                                         preferredStyle: UIAlertControllerStyleAlert];
                             
                             
                             
                             UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                                        {
                                                            if([self.signUpVC isEqualToString:@"signUpVC"])
                                                            {
                                                                [self dismissViewControllerAnimated:NO completion:nil];
                                                                [_delegate dismissWithSuccessFullRegistration];
                                                            }
                                                            else
                                                            {
                                                            
                                                                [[SlideNavigationController sharedInstance] popViewControllerAnimated:YES];
                                                                [_delegate dismissWithSuccessFullRegistration];
                                                            }
                                                            
                                                           // [self setUserDetailsAndClosePopUp];
                                                        }];
                             
                             [alertView addAction:actionOk];
                             
                             [self presentViewController:alertView animated:YES completion:nil];

                         
                         }
                         
                     }
                 }
             });
             
         }
                                   failure:^(NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^(void)
                            {
                                [activityLoadingView removeFromSuperview];
                                NSLog(@"Response Dict : %@",[error description]);
                            });
             
             
             
         }];
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"%@", exception.reason);
        [activityLoadingView removeFromSuperview];
    }
    @finally
    {
        NSLog(@"exception finally called");
    }


}

-(void)showAlertViewWithTitle:(NSString *)title withMessage:(NSString *)message
{
    UIAlertController *alertView = [UIAlertController alertControllerWithTitle:title
                                                                       message:message
                                                                preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [alertView dismissViewControllerAnimated:YES completion:nil];
        
    }];
    
    [alertView addAction:okAction];
    
   
    [self presentViewController:alertView animated:YES completion:nil];
    
    //[[[[SlideNavigationController sharedInstance] viewControllers ] lastObject] presentViewController:alertView animated:YES completion:nil];
}

#pragma mark - text field delegate -

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField == _textfieldTitle)
    {
        
        //{ "Mr", "Mrs", "Ms", "Dr" };

        NSArray *titleArray = [[NSArray alloc] initWithObjects:@"Mr",@"Mrs",@"Ms",@"Dr", nil];
        
        UIAlertController *durationActionSheet = [UIAlertController alertControllerWithTitle:@"Title" message:@"Select Title" preferredStyle:UIAlertControllerStyleActionSheet];
        
        for (NSString *title in titleArray)
        {
            UIAlertAction *actionMr = [UIAlertAction actionWithTitle:title style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                       {
                                           textField.text = title;
                                       }];
            
            [durationActionSheet addAction:actionMr];
        }
        
        
        
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
                                   {
                                      
                                   }];
        
        [durationActionSheet addAction:cancel];
        
        
        [self presentViewController:durationActionSheet animated:YES completion:nil];
        return NO;
        
    }
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == _textfieldMobileNumber)
    {
        NSString *currentString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        int length = (int)[currentString length];
        
            if ([string rangeOfCharacterFromSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]].location != NSNotFound)
            {
                
                UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Alert"
                                                                                   message:@"This field accepts only numeric values"
                                                                            preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    
                    [alertView dismissViewControllerAnimated:YES completion:nil];
                    
                }];
                [alertView addAction:okAction];
                
                return FALSE;
            }
            
            if (length >10 )
            {
                return !([currentString length] > 10);
                return FALSE;
            }
            
            
    }
    
    return YES;
}

@end
