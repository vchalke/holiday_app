//
//  Holiday.h
//  holidays
//
//  Created by vaibhav on 19/09/15.
//  Copyright (c) 2015 Mobicule. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Holiday : NSObject

@property(nonatomic,strong)NSArray *arrayOfDestination;
@property(nonatomic,strong)NSArray *arrayOfHubList;
@property(nonatomic,strong)NSMutableArray *arrayOfCityList;
@property(nonatomic,strong)NSString *strOfferPrisePP;
@property(nonatomic,strong)NSString *strCurrenyCode;
@property(nonatomic,strong)NSString *strPackageDesc;
@property(nonatomic,strong)NSString *strPackageImgPath;
@property(nonatomic,strong)NSString *strpackageOfferDesc;
@property(nonatomic,strong)NSString *strPackageName;
@property(nonatomic,strong)NSString *strPackageValidFrom;
@property(nonatomic,strong)NSString *strPackageValidTo;
@property (nonatomic) BOOL mealsFlag;
@property (nonatomic) BOOL airFlag;
@property (nonatomic) BOOL tourMngerFlag;
@property (nonatomic) BOOL accomFlag;
@property (nonatomic) BOOL sightSeeingFlag;
@property (nonatomic) int durationNoDays;
@property (nonatomic,strong) NSArray *tcilHolidayOffersCollection;
@property (nonatomic , strong) NSString *strPackageSubType;
@property (nonatomic,strong) NSArray *timeLineList;
@property (nonatomic,strong) NSString *packageImagePath;
@property (nonatomic,strong) NSString *strPackageId;
@property (nonatomic,strong) NSString *strSeasonType;
@property (nonatomic) int packagePrise;
@property (nonatomic,strong) NSString *strPackageType;
@property (nonatomic,strong) NSString *stringBookingOnline;
@property (nonatomic,strong) NSString *stringPopularFlag;
@property (nonatomic,strong) NSArray *packagePriceArray;
@property (nonatomic) int minimumPackagePrise;
@property (nonatomic) int pkgStatusId;
@property (nonatomic) int startingPriceStandard;
@property (nonatomic) int startingPriceDelux;
@property (nonatomic) int startingPricePremium;
@property (nonatomic) int strikeoutPriceStandard;
@property (nonatomic) int strikeoutPriceDelux;
@property (nonatomic) int strikeoutPricePremium;
@property (nonatomic,strong) NSString *Offers;
@property (nonatomic,strong) NSArray *tcilHolidayTravelmonthCollection;

@property (nonatomic ,strong)NSMutableArray *mealCollection;
@property (nonatomic ,strong)NSMutableArray *flightCollection;
@property (nonatomic ,strong)NSMutableArray *visaCollection;
@property (nonatomic ,strong)NSMutableArray *hotelCollection;
@property (nonatomic ,strong)NSMutableArray *transferCollection;
@property (nonatomic ,strong)NSMutableArray *sightseenCollection;
@property (nonatomic ,strong)NSMutableArray *accombdationCollection;
@property (nonatomic ,strong)NSMutableArray *themeCollectionArray;
-(instancetype)initWithDataDict:(NSDictionary *)dataDict;

//Vijay Added
@property (nonatomic) BOOL transferFlag;
@property(nonatomic,strong)NSDictionary *objDetailDict;
@property(nonatomic,strong)NSArray *arrayOfThemeList;
@property (nonatomic,strong) NSString *flightDefaultMsg;
@end
