//
//  SRPCollectionCell.m
//  Thomoscook_Holiday
//
//  Created by Kush_Tech on 19/02/20.
//  Copyright © 2020 Kush_Tech. All rights reserved.
//

#import "SRPCollectionCell.h"
#import "UIImageView+WebCache.h"
//#import <NetCorePush/NetCorePush.h>
#import "NetCoreAnalyticsVC.h"
#import "OffersCollectionObject.h"
@implementation SRPCollectionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    allViewArray = [NSArray arrayWithObjects:self.flightView,self.hotelView,self.sightSeeingView,self.visaView,self.transferView,self.mealsView, nil];
    allImgViewArray = [NSArray arrayWithObjects:self.flightImgView,self.hotelImgView,self.sightSeeingImgView,self.visaImgView,self.transferImgView,self.mealsImgView, nil];
    allButtonArray = [NSArray arrayWithObjects:self.flightButton,self.hotelButton,self.sightSeeingButton,self.visaButton,self.transferButton,self.mealsButton, nil];
    allImgNamesArray = [NSArray arrayWithObjects:@"flights",@"hotel",@"sightSeeing",@"visa",@"transfer",@"meal" ,nil];
    
//    grayflights
}
//flightInSRP
-(void)showHolidayPackage:(Holiday *)holidayObj withCurrentIndex:(NSInteger)cellIindex withTag:(NSInteger)tagNum{
    
    self.hert_View.layer.cornerRadius = self.hert_View.frame.size.height*0.5;
    
    self.holidayObjectInSrpCell = holidayObj;
    
    [self loadAllViewsFromArrayForIndex:cellIindex];
//    [self loadAllViewsFromArrayForIndex:tagNum];
     self.lbl_PackageName.text = holidayObj.strPackageName;
    NSString *packageTypeString = (holidayObj.startingPriceStandard > 0) ? standardPackage : (holidayObj.startingPriceDelux > 0) ? deluxePackage : (holidayObj.startingPricePremium > 0) ? premiumPackage : standardPackage;
    int startPrice, strikeOutPrice;
    if ([packageTypeString isEqualToString:standardPackage]){
        startPrice = holidayObj.startingPriceStandard;
        strikeOutPrice = holidayObj.strikeoutPriceStandard;
    }else if ([packageTypeString isEqualToString:deluxePackage]){
        startPrice = holidayObj.startingPriceDelux;
        strikeOutPrice = holidayObj.strikeoutPriceDelux;
    }else{
        startPrice = holidayObj.startingPricePremium;
        strikeOutPrice = holidayObj.strikeoutPricePremium;
    }
    self.lbl_minprize.text = [self getNumberFormatterString:startPrice];
    self.lbl_maxprize.text = (strikeOutPrice>0) ? [self getNumberFormatterString:strikeOutPrice] : @"";
    self.lbl_maxprize.hidden = !(strikeOutPrice>0);
//    self.const_lblStrikeOutPrize.constant = (strikeOutPrice>0) ? 15 : 0;
    
    
//    int days = holidayObj.durationNoDays;
//    self.lbl_dayCount.text = [NSString stringWithFormat:@"%d Nights & %d Days",days-1,days];
self.lbl_dayCount.text = [NSString stringWithFormat:@"%i Nights & %i Days",holidayObj.durationNoDays-1,holidayObj.durationNoDays];
    self.cnst_webViewHeight.constant = (self.selectedIndexNumber == cellIindex) ? 140 : 0;
    NSLog(@"Content View cellIindex %ld",cellIindex);
    NSLog(@"Content View tagNum %ld",tagNum);
    NSLog(@"Content View selectedTag %ld",selectedTag);
    NSLog(@"Content View selectedIndexNumber %ld",self.selectedIndexNumber);
    NSLog(@"Content View Width %0.2f",self.contentView.frame.size.width);
//    self.siteArray = holidayObj.timeLineList;
    /*
    NSSortDescriptor *brandDescriptor = [[NSSortDescriptor alloc] initWithKey:@"position" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:brandDescriptor];
    NSArray *sortedArray = [holidayObj.timeLineList sortedArrayUsingDescriptors:sortDescriptors];
    NSMutableString *mutString = [[NSMutableString alloc]init];
    [mutString appendString:@"\u2708"]; //"\u2708"
    if ([sortedArray count]>0){
        for (int i=0; i<[sortedArray count]; i++) {
            NSDictionary *siteNameDict = sortedArray[i];
            NSLog(@"%@",siteNameDict);
            NSDictionary *cityDict = [siteNameDict valueForKey:@"cityCode"];
            NSLog(@"%@",cityDict);
            NSDictionary *iconDict = [siteNameDict valueForKey:@"iconId"];
            NSLog(@"%@",iconDict);
            NSString *iconName = [iconDict valueForKey:@"iconName"];
            NSLog(@"%@",iconName);
            NSString *cityName = [cityDict valueForKey:@"cityName"];
            NSLog(@"%@",cityName);
            NSString *timeStr =[NSString stringWithFormat:@"%@",[siteNameDict valueForKey:@"noOfNights"]];
            NSLog(@"%@",timeStr);
            NSString *dashString = @" ➝ ";
            [mutString appendString:[NSString stringWithFormat:@"%@ (%@N)%@",cityName,timeStr,(i==sortedArray.count-1) ? @"" : dashString]];
        }
        self.lbl_destination.text = mutString;
    }
    */
    [self setDestinationsArry:holidayObj.timeLineList];
    CGFloat widthConstant = self.contentView.frame.size.width/6.2;
    CGFloat constant = 0;
//    if (holidayObj.airFlag)
//[[iconString mutableCopy] appendAttributedString:[[NSAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@",mutString]]];
//    [[iconString mutableCopy] appendAttributedString:[mutString copy]];
//    self.lbl_destination.attributedText = iconString;
    
//    [self setImageIcon:[UIImage imageNamed:@"grayflights"] WithText:[mutString copy]];
    self.flightView.hidden = !holidayObj.airFlag;
    self.hotelView.hidden = !holidayObj.accomFlag;
    self.sightSeeingView.hidden = !holidayObj.sightSeeingFlag;
    self.visaView.hidden = !holidayObj.tourMngerFlag;
    self.transferView.hidden = !holidayObj.transferFlag;
    self.mealsView.hidden = !holidayObj.mealsFlag;
    constant += (!holidayObj.airFlag)? 0 : widthConstant ;
    constant += (!holidayObj.accomFlag)? 0 : widthConstant ;
    constant += (!holidayObj.sightSeeingFlag)? 0 : widthConstant ;
    constant += (!holidayObj.tourMngerFlag)? 0 : widthConstant ;
    constant += (!holidayObj.transferFlag)? 0 : widthConstant ;
    constant += (!holidayObj.mealsFlag)? 0 : widthConstant ;
    self.const_stckView.constant = constant;
    NSString *packageImagePath = holidayObj.packageImagePath;
    packageImagePath = [packageImagePath stringByReplacingOccurrencesOfString:@" " withString:@""];
//    NSString *packageImagePath = [holidayObj.packageImagePath stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
//    NSLog(@"%@",packageImagePath);
    self.wishListImgString = packageImagePath;
    [self setImageSatring:packageImagePath];
    /*
    NSURL* urlImage=[NSURL URLWithString:packageImagePath];
    if(urlImage)
    {
        UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        indicator.center = self.tour_imgView.center;// it will display in center of image view
        [self.tour_imgView addSubview:indicator];
        [indicator startAnimating];
        NSLog(@"UrlOfPackageImage: %@", urlImage);
        [self.tour_imgView sd_setImageWithURL:urlImage placeholderImage:[UIImage imageNamed:@"defaultBanner.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
            [indicator stopAnimating];
        }];
        
    }
    */
    self.flightCollection = holidayObj.flightCollection;
    self.hotelCollection = holidayObj.hotelCollection;
    self.sightseenCollection = holidayObj.sightseenCollection;
    self.visaCollection = holidayObj.visaCollection;
    self.accombdationCollection = holidayObj.accombdationCollection;
    self.mealCollection = holidayObj.mealCollection;
    self.transferCollection = holidayObj.transferCollection;
    self.heart_imgView.image = [UIImage imageNamed:([[CoreDataSingleton sharedInstance] checkIsPackagePresentInEntityWithName:WishListHoliday withPackageID:holidayObj.strPackageId]) ? @"heartBlue" : @"heartWhite"];
    [self.btn_like setTag:([[CoreDataSingleton sharedInstance] checkIsPackagePresentInEntityWithName:WishListHoliday withPackageID:holidayObj.strPackageId]) ? 1 : 0];

    
    if (holidayObj.Offers.length >0){
        [self.btn_Offers setHidden:NO];
    }else{
        [self.btn_Offers setHidden:YES];
    }
    
    
}
- (IBAction)btn_ClickOffers:(id)sender {
    [self.srpDelgate offersButtonClick:self withHoliday:self.holidayObjectInSrpCell];
}

-(void)setImageSatring:(NSString*)imgPath{
    NSURL* urlImage=[NSURL URLWithString:imgPath];
        if(urlImage)
        {
            UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            indicator.center = self.tour_imgView.center;// it will display in center of image view
            [self.tour_imgView addSubview:indicator];
            [indicator startAnimating];
    //        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0ul);
    //        dispatch_async(queue, ^{
    //
    //        });
            NSLog(@"UrlOfPackageImage: %@", urlImage);
            [self.tour_imgView sd_setImageWithURL:urlImage placeholderImage:[UIImage imageNamed:@"defaultBanner.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
             {
                [indicator stopAnimating];
            }];
            
        }
}
-(void)setDestinationsArry:(NSArray*)destArray{
    NSSortDescriptor *brandDescriptor = [[NSSortDescriptor alloc] initWithKey:@"position" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:brandDescriptor];
    NSArray *sortedArray = [destArray sortedArrayUsingDescriptors:sortDescriptors];
    NSMutableString *mutString = [[NSMutableString alloc]init];
//    [mutString appendString:@"\u2708 "]; //"\u2708" //"\U0000fffc"
    if ([sortedArray count]>0){
        for (int i=0; i<[sortedArray count]; i++) {
            NSDictionary *siteNameDict = sortedArray[i];
            NSLog(@"%@",siteNameDict);
            NSDictionary *cityDict = [siteNameDict valueForKey:@"cityCode"];
            NSLog(@"%@",cityDict);
            NSDictionary *iconDict = [siteNameDict valueForKey:@"iconId"];
            NSLog(@"%@",iconDict);
            NSString *iconName = [iconDict valueForKey:@"iconName"];
            NSLog(@"%@",iconName);
            NSString *cityName = [cityDict valueForKey:@"cityName"];
            NSLog(@"%@",cityName);
            NSString *timeStr =[NSString stringWithFormat:@"%@",[siteNameDict valueForKey:@"noOfNights"]];
            NSLog(@"%@",timeStr);
            NSString *dashString = @" ➝ ";
            [mutString appendString:[NSString stringWithFormat:@"%@ (%@N)%@",cityName,timeStr,(i==sortedArray.count-1) ? @"" : dashString]];
        }
//         self.lbl_destination.text = mutString;
//        self.txt_destination.text = mutString;
//        self.txt_destination.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"flightInSRP.png"]];
//        self.txt_destination.rightView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"flightInSRP.png"]];
//        UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(96, 40, 130, 20)];
//        [textField setLeftViewMode:UITextFieldViewModeAlways];
//        textField.leftView= [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"searchIccon.png"]];
        
//        NSTextAttachment *imageAttachment = [[NSTextAttachment alloc] init];
//        imageAttachment.image = [UIImage imageNamed:@"flightInSRP"];
//        imageAttachment.bounds = CGRectMake(0, 0, imageAttachment.image.size.width, imageAttachment.image.size.height);
//        NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:imageAttachment];
//        NSMutableAttributedString *completeText = [[NSMutableAttributedString alloc] initWithString:@" "];
//        [completeText appendAttributedString:attachmentString];
//        NSAttributedString *textAfterIcon = [[NSAttributedString alloc] initWithString:[mutString copy]];
//        [completeText appendAttributedString:textAfterIcon];
//        self.lbl_destination.textAlignment = NSTextAlignmentCenter;
//        self.lbl_destination.attributedText = completeText;
        
        [self setImageIcon:[UIImage imageNamed:@"flightInSRP"] WithText:mutString];;
        
//        CGSize sizeOfText = [self.lbl_destination.text sizeWithFont:self.lbl_destination.font
//                                constrainedToSize:self.lbl_destination.frame.size
//                                    lineBreakMode:NSLineBreakByCharWrapping];
//        labelNumOFLines = sizeOfText.height / 15;
        
        labelNumOFLines = [[NSNumber numberWithUnsignedInteger:[sortedArray count]/3] intValue];
        int newLineCount = (labelNumOFLines>=1) ? labelNumOFLines+1 : 1;
        NSLog(@"labelNumOFLines %i",newLineCount);
        self.btn_dropDown.hidden = !(newLineCount>1);
        [self setHeightOfLabel:NO];
        
    }
}
-(void)setImageIcon:(UIImage*)image WithText:(NSString*)strText{

//    NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
//    attachment.image = image;
//    [attachment setBounds:CGRectMake(self.lbl_destination.frame.origin.x, self.lbl_destination.frame.origin.y, self.lbl_destination.frame.size.height, self.lbl_destination.frame.size.height)];
//    NSMutableAttributedString *attachmentString = [[NSMutableAttributedString alloc] initWithAttributedString:[NSAttributedString attributedStringWithAttachment:attachment]];
//    NSMutableAttributedString *myString= [[NSMutableAttributedString alloc] initWithString:strText];
//    [attachmentString appendAttributedString:myString];
//    self.lbl_destination.attributedText = attachmentString;
    
    NSTextAttachment *imageAttachment = [[NSTextAttachment alloc] init];
    imageAttachment.image = image;
//    imageAttachment.bounds = CGRectMake(-5.0, 0, imageAttachment.image.size.width, imageAttachment.image.size.height);
    imageAttachment.bounds = CGRectMake(0, 0, 13, 13);
    NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:imageAttachment];
    NSMutableAttributedString *completeText = [[NSMutableAttributedString alloc] initWithString:@" "];
    [completeText appendAttributedString:attachmentString];
    NSAttributedString *textAfterIcon = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@" ➝ %@ ➝ ",strText]];
    [completeText appendAttributedString:textAfterIcon];
    [completeText appendAttributedString:attachmentString];
    self.lbl_destination.textAlignment = NSTextAlignmentLeft;
    self.lbl_destination.attributedText = completeText;
}
-(void)loadAllViewsFromArrayForIndex:(NSInteger)indexx{
    for(int i=0 ;i<[allViewArray count]; i++){
        UIView *selectedView = [allViewArray objectAtIndex:i];
        selectedView.tag = i;
//        UITapGestureRecognizer *tapGesture1 = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapGesture:)];
//        [selectedView addGestureRecognizer:tapGesture1];
        UIButton *btnView = [allButtonArray objectAtIndex:i];
        [btnView addTarget:self
                    action:@selector(tapButtom:)
        forControlEvents:UIControlEventTouchUpInside];
        btnView.tag = i;
        
    }
    if(self.selectedIndexNumber == indexx){
        [self loadSelectedImagesFromArray:clickIndexOfOptions withCurrentCellIndex:indexx];
    }else{
        [self loadSelectedImagesFromArray:10 withCurrentCellIndex:indexx];
    }
}
/*
- (void)tapGesture:(UITapGestureRecognizer*)sender{
    NSLog(@"%ld",(long)sender.view.tag);
    NSLog(@"Index Number %ld",(long)self.indexNumber);
    clickIndexOfOptions = sender.view.tag;
    NSLog(@"clickIndexOfOptions Number %ld",(long)clickIndexOfOptions);
    [self.srpDelgate setSelectedIndex:self withTag:(long)clickIndexOfOptions];
    [self.web_views loadHTMLString:@"" baseURL:nil];
    switch (sender.view.tag) {
        case 0:
            [self showInfoForKey:@"typeDefaultMsg" FromArray:self.flightCollection forSelectTheme:sFlights];
            break;
        case 1:
            [self showInfoForKey:@"typeDefaultMsg" FromArray:self.accombdationCollection forSelectTheme:sHotels];
            break;
        case 2:
            [self showInfoForKey:@"typeDefaultMsg" FromArray:self.sightseenCollection forSelectTheme:sSightSeeing];
            break;
        case 3:
            [self showInfoForKey:@"typeDefaultMsg" FromArray:self.visaCollection forSelectTheme:sVisaTransferInsurance];
            break;
        case 4:
            [self showInfoForKey:@"typeDefaultMsg" FromArray:self.transferCollection forSelectTheme:sTransfer];
            break;
        case 5:
            [self showInfoForKey:@"typeDefaultMsg" FromArray:self.mealCollection forSelectTheme:sMeals];
            break;
        default:
            break;
    }
 }
 */
- (void)tapButtom:(UIButton*)sender{
    NSLog(@"%ld",(long)sender.tag);
    NSLog(@"Index Number %ld",(long)self.indexNumber);
    if (clickIndexOfOptions != sender.tag ){
        [self.srpDelgate setSelectedIndex:self withTag:(long)clickIndexOfOptions];
        [self.web_views loadHTMLString:@"" baseURL:nil];
        [self selecetdThemeOption:sender.tag];
    }else{
        [self.srpDelgate setSelectedIndex:self withTag:1000];
        [self.web_views loadHTMLString:@"" baseURL:nil];
        [self selecetdThemeOption:1000];
    }
    clickIndexOfOptions = sender.tag;
    NSLog(@"clickIndexOfOptions Number %ld",(long)clickIndexOfOptions);
//    [self.srpDelgate setSelectedIndex:self withTag:(long)clickIndexOfOptions];
//    [self.web_views loadHTMLString:@"" baseURL:nil];
//    [self selecetdThemeOption:sender.tag];
}
-(void)selecetdThemeOption:(NSInteger)tagInt{
    selectedTag = tagInt;
    switch (tagInt) {
        case 0:
            [self showInfoForKey:@"typeDefaultMsg" FromArray:self.flightCollection forSelectTheme:sFlights];
            break;
        case 1:
            [self showInfoForKey:@"typeDefaultMsg" FromArray:self.accombdationCollection forSelectTheme:sHotels];
            break;
        case 2:
            [self showInfoForKey:@"typeDefaultMsg" FromArray:self.sightseenCollection forSelectTheme:sSightSeeing];
            break;
        case 3:
            [self showInfoForKey:@"typeDefaultMsg" FromArray:self.visaCollection forSelectTheme:sVisaTransferInsurance];
            break;
        case 4:
            [self showInfoForKey:@"typeDefaultMsg" FromArray:self.transferCollection forSelectTheme:sTransfer];
            break;
        case 5:
            [self showInfoForKey:@"typeDefaultMsg" FromArray:self.mealCollection forSelectTheme:sMeals];
            break;
        default:
            break;
    }
}
-(void)showInfoForKey:(NSString*)string FromArray:(NSMutableArray*)collectionArray forSelectTheme:(NSString*)themestring{
    NSLog(@"For %@ CollectionArray %@",themestring,collectionArray);
//    for (id object in collectionArray){
//        NSString *htmlString = [NSString stringWithFormat:@"<font face='HelveticaNeue' size='2'>%@", [object valueForKey:string]];
//        NSLog(@"%@",[object valueForKey:string]);
//        [self.web_views loadHTMLString:htmlString baseURL:nil];
//    }
    if ([collectionArray objectAtIndex:0] != nil){
        NSDictionary *firstObj = [collectionArray objectAtIndex:0];
        NSString *htmlString = [NSString stringWithFormat:@"<font face='HelveticaNeue' size='2'>%@", [firstObj valueForKey:string]];
        [self.web_views loadHTMLString:htmlString baseURL:nil];
    }else if ([themestring isEqualToString:sFlights]){
        NSString *htmlString = [NSString stringWithFormat:@"<font face='HelveticaNeue' size='2'>%@",self.holidayObjectInSrpCell.flightDefaultMsg];
        [self.web_views loadHTMLString:htmlString baseURL:nil];
    }else if ([themestring isEqualToString:sTransfer]){
        NSString *htmlString = [NSString stringWithFormat:@"<font face='HelveticaNeue' size='2'> Return Airport Transfers based on Private Basis"];
        [self.web_views loadHTMLString:htmlString baseURL:nil];
    }else{
        NSLog(@"No Any Data To Display");
    }
}
-(void)loadSelectedImagesFromArray:(NSInteger)index withCurrentCellIndex:(NSInteger)cellIndex{
    for(int i=0 ;i<[allImgViewArray count]; i++){
        UIImageView *selectedImgView = [allImgViewArray objectAtIndex:i];
        selectedImgView.image = [self coloredImage:[UIImage imageNamed:[allImgNamesArray objectAtIndex:i]] withColor:(index == i && cellIndex == self.selectedIndexNumber) ? [UIColor colorWithRed:0 green:83/255.0 blue:169/255.0 alpha:1.0]: [UIColor grayColor]];
        UIButton *selectedButton = [allButtonArray objectAtIndex:i];
        [selectedButton setTitleColor:(index == i && cellIndex == self.selectedIndexNumber) ? [UIColor colorWithRed:0 green:83/255.0 blue:169/255.0 alpha:1.0]: [UIColor grayColor] forState:UIControlStateNormal];
    }
}
- (IBAction)btn_downClick:(id)sender {
    NSLog(@"%@",_lbl_destination.text);
//    [self.srpDelgate downButtonClick:self];
    [self toggleButton:self.btn_dropDown];
}

- (void) toggleButton:(UIButton *)sender {

    self.btn_dropDown.tag = (sender.tag == 0) ? 1 : 0 ;
    [self setHeightOfLabel:(self.btn_dropDown.tag == 1)];
}
- (void)setHeightOfLabel:(BOOL)isFlag{
    self.const_lblDestination.constant = isFlag ? 20 * (labelNumOFLines + 1) : 20;
    [self.btn_dropDown setImage:isFlag ? [UIImage imageNamed:@"dropUpInCircle"] : [UIImage imageNamed:@"dropDownInCircle"] forState:UIControlStateNormal];
    self.const_txtDestination.constant = isFlag ? 25 * (labelNumOFLines + 1) : 20;
}
- (UICollectionViewLayoutAttributes *)preferredLayoutAttributesFittingAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes
{
    [super preferredLayoutAttributesFittingAttributes:layoutAttributes];
    UICollectionViewLayoutAttributes *attributes = [layoutAttributes copy];
    attributes.size = CGSizeMake(self.baseInfoView.frame.size.width, 400);
    return attributes;
}
- (IBAction)btn_likePress:(id)sender {
    UIButton* myButton = (UIButton*)sender;
    if (myButton.tag == 1){
        [myButton setTag:0];
        [self dislikePress];
        [self.srpDelgate likeDislikePressInCell:self withFlag:NO];
    }else{
        [myButton setTag:1];
        [self likePress];
        [self.srpDelgate likeDislikePressInCell:self withFlag:YES];
    }
}
-(void)likePress{
    NSLog(@"packageDetail %@",self.holidayObjectInSrpCell.objDetailDict);
    NSString * jsonString = [self convertJsonDictToString:self.holidayObjectInSrpCell.objDetailDict];
    NSDictionary *dict =  @{@"packageData" : jsonString,@"packageID" : self.holidayObjectInSrpCell.strPackageId, @"packageName" : self.holidayObjectInSrpCell.strPackageName, @"packagePrize" : [NSNumber numberWithInt:self.holidayObjectInSrpCell.packagePrise],@"packageImgUrl" : self.wishListImgString};
    NSLog(@"%@",dict);
    WishListHolidayObject *wishObject = [[WishListHolidayObject alloc]initWithWishListObject:dict];
    [[CoreDataSingleton sharedInstance]saveWishListObjectInCoreDataForEntity:WishListHoliday withObject:wishObject];
    
}
-(void)dislikePress{
    NSArray *array = [[CoreDataSingleton sharedInstance] getArrayOfObjectForEntityName:WishListHoliday];
    NSArray *packIdArray = [[CoreDataSingleton sharedInstance]getdeletedObjectFromPackageListArray:array withPackID:self.holidayObjectInSrpCell.strPackageId forEntityName:WishListHoliday];
    if ([packIdArray count]>0){
        [[[CoreDataSingleton sharedInstance]getManagedObjectContext] deleteObject:[packIdArray firstObject]];
        NSError * error = nil;
        if (![[[CoreDataSingleton sharedInstance]getManagedObjectContext] save:&error]){
            NSLog(@"Error ! %@", error);
        }
    }
}
@end
