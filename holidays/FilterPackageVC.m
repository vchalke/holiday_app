//
//  FilterPackageVC.m
//  holidays
//
//  Created by Pushpendra Singh on 14/08/15.
//  Copyright (c) 2015 Mobicule. All rights reserved.
//

#import "FilterPackageVC.h"
#import "Holiday.h"
#import "TabMenuVC.h"
#import "CoreUtility.h"
#import "PackageListVC.h"
//#import <NetCorePush/NetCorePush.h>
#import "NetCoreAnalyticsVC.h"


@interface FilterPackageVC ()
{
    NSMutableArray *arrayDepartureList;
    NSArray *arrayMonthOfTravel;
    NSArray *arrayDuration;
    NSMutableArray *arrayFlightFrom;
    NSMutableArray *arrayForResetData;
    NSMutableArray *departureCity;
    int minDuration,maxDuration,monthIndex;
    NSMutableArray *arrayFlightFromList;
    NSMutableDictionary *payloadList;
    
}

@property (strong,nonatomic)NSMutableArray *arrayOfHolidayData;

@property (strong,nonatomic)NSDictionary *dictOfFilters;
@end

@implementation FilterPackageVC {
    
    BOOL isAppeared;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.dictOfFilters = [[NSDictionary alloc]initWithDictionary:_filterDict];
    
   [self configureLabelSlider];
    [self updateSliderLabels];
    arrayForResetData=[[NSMutableArray alloc]init];
    arrayForResetData=[self.arrayOfHolidayData mutableCopy];
    payloadList =  [NSMutableDictionary dictionary];

     
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:NO];
    //  CoreUtility *coreObj=[CoreUtility sharedclassname];
    //    PackageListVC *filterObj=coreObj.obj;
    //    filterObj.title=@"PACKAGE LISTING";
    
    [[SlideNavigationController sharedInstance] setEnableSwipeGesture:NO];
    
    self.arrayOfHolidayData = [[NSMutableArray alloc]initWithArray:_arrayOfHolidays];
    
    departureCity = [[NSMutableArray alloc]init];
    
    [departureCity insertObject:@"SELECT" atIndex:0];
    
    
    //[departureCity addObjectsFromArray: [self.filterDict valueForKey:@"departureFromList"]];
    
    arrayDepartureList = [[NSMutableArray alloc]init];
    
    [departureCity addObjectsFromArray:[self getDepartureCityFromArray]];
    
    [arrayDepartureList addObjectsFromArray:departureCity];
    
        
    arrayDuration = [[NSArray alloc] initWithObjects:@"less than 3",@"From 3 to 7",@"more than 7", nil];
    
    
    arrayMonthOfTravel = [self nextYearMonths];
    
    arrayFlightFrom = [[NSMutableArray alloc]init];
    [arrayFlightFrom insertObject:@"SELECT" atIndex:0];
   /// [arrayFlightFrom addObjectsFromArray:[self.filterDict valueForKey:@"departureFromList"]];
    [arrayFlightFrom addObjectsFromArray:[self getDepartureCityFromArray]];
    
}

-(NSArray *)getDepartureCityFromArray
{
    NSMutableSet *departureCityList = [[NSMutableSet alloc] init];
    
    for (int index = 0; index < self.arrayOfHolidayData.count; index ++)
    {
        Holiday *holidayObject = self.arrayOfHolidayData[index];
        
        NSArray *hubArray = holidayObject.arrayOfHubList;
        
        if (hubArray)
        {
            if (hubArray.count != 0)
            {
                [departureCityList addObjectsFromArray:hubArray];

            }
        }
    }
    
    NSArray *finalDestinationList = [departureCityList allObjects];
    
    return finalDestinationList;
}

-(void)viewDidAppear:(BOOL)animated
{
    
    if (!isAppeared)
    {
        isAppeared=YES;
        _viewFilter.frame=CGRectMake(0, 0, self.view.frame.size.width, 658);
        [_scrollViewFilter addSubview:_viewFilter];
        [_scrollViewFilter setContentSize:CGSizeMake(_viewFilter.frame.size.width, _viewFilter.frame.size.height+40)];
        [self setLeftPadding:_txtDepartureCity];
        [self setLeftPadding:_txtDuration];
        [self setLeftPadding:_txtFlightFrom];
        [self setLeftPadding:_txtMonthOfTravel];
        
    }
    
}


-(void)viewDidDisappear:(BOOL)animated
{
    [[SlideNavigationController sharedInstance] setEnableSwipeGesture:YES];
    
    UIScrollView *superScrollView = (UIScrollView *)self.view.superview;
    superScrollView.scrollEnabled = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setLeftPadding:(UITextField*)textField{
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,10, 48)];
    textField.leftView = paddingView;
    textField.leftViewMode = UITextFieldViewModeAlways;
    
    [textField setBackground:[UIImage imageNamed:@"bgTextFieldH.png"]];
    
}

#pragma mark - Label  Slider

- (void) configureLabelSlider
{
    double  maxValue = 0.00;
    double  minValue = 0.00;
    
    for (int index = 0; index < _arrayOfHolidays.count; index++)
    {
        Holiday *objectHoliday = _arrayOfHolidays[index];
        
        double packagePrice = objectHoliday.packagePrise;
        
        if (packagePrice > maxValue)
        {
            maxValue = packagePrice;
        }
        
        if ( minValue == 0.00)
        {
            minValue = packagePrice;
        }
        else if (packagePrice < minValue)
        {
            minValue = packagePrice;
        }
        
        
    }
    
    minValue = 1;
    
    
  //  float maxValue = [[self.dictOfFilters valueForKey:kFitlerMaxPrice]floatValue];
   // float minValue = [[self.dictOfFilters valueForKey:kFilterMinPrice]floatValue];
    
    self.labelSlider.minimumValue =  minValue;
    self.labelSlider.maximumValue = maxValue;
    
    self.labelSlider.lowerValue = minValue;
    self.labelSlider.upperValue = maxValue;
    
    self.labelSlider.minimumRange = 10;
    
  //  self.lowerLabel.text = [NSString stringWithFormat:@"%d",minValue];
  //  self.lblMinimumPrice.text = [NSString stringWithFormat:@"%d", minValue];
    
    
      self.lowerLabel.text = [NSString stringWithFormat:@"%d",(int)minValue];
      self.lblMinimumPrice.text = [NSString stringWithFormat:@"%d",(int)minValue];

    
    
  //  self.upperLabel.text = [NSString stringWithFormat:@"%d",maxValue];
   // self.lblMaximumPrice.text = [NSString stringWithFormat:@"%d",maxValue];
    
      self.upperLabel.text = [NSString stringWithFormat:@"%d",(int)maxValue];
     self.lblMaximumPrice.text =[NSString stringWithFormat:@"%d",(int)maxValue];
    
}

- (void) updateSliderLabels
{
    // You get get the center point of the slider handles and use this to arrange other subviews
    
    UIScrollView *superScrollView = (UIScrollView *)self.view.superview;
    superScrollView.scrollEnabled = NO;
    
    int minValue = [[self.dictOfFilters valueForKey:kFilterMinPrice]intValue];
    
    CGPoint lowerCenter;
    lowerCenter.x = (self.labelSlider.lowerCenter.x + self.labelSlider.frame.origin.x);
    lowerCenter.y = (self.labelSlider.center.y - 18.0f);
    self.lowerLabel.center = lowerCenter;
    self.imgLowerLabel.center=lowerCenter;
    self.lowerLabel.text = [NSString stringWithFormat:@"%d", (int)self.labelSlider.lowerValue + minValue];
    self.lblMinimumPrice.text = [NSString stringWithFormat:@"%d", (int)self.labelSlider.lowerValue + minValue];
    
    CGPoint upperCenter;
    upperCenter.x = (self.labelSlider.upperCenter.x + self.labelSlider.frame.origin.x);
    upperCenter.y = (self.labelSlider.center.y - 18.0f);
    self.upperLabel.center = upperCenter;
    self.imgUpperLabel.center=upperCenter;
    self.upperLabel.text = [NSString stringWithFormat:@"%d", (int)self.labelSlider.upperValue];
    self.lblMaximumPrice.text = [NSString stringWithFormat:@"%d", (int)self.labelSlider.upperValue];
}

// Handle control value changed events just like a normal slider
- (IBAction)labelSliderChanged:(NMRangeSlider*)sender
{
    [self updateSliderLabels];
}


# pragma mark - Button click methods

- (IBAction)onDepartureCityClicked:(id)sender
{
    UIAlertController *departureActnSht = [UIAlertController alertControllerWithTitle:@"Departure City"
                                                                              message:@"Select your choice"
                                                                       preferredStyle:UIAlertControllerStyleActionSheet];
    
    for (int j =0 ; j<arrayDepartureList.count; j++)
    {
        NSString *titleString = arrayDepartureList[j];
        UIAlertAction *action = [UIAlertAction actionWithTitle:titleString style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
            self.txtDepartureCity.text = arrayDepartureList[j];
            self.txtFlightFrom.text = arrayDepartureList[j];
        }];
        
        [departureActnSht addAction:action];
    }
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
        
        [self dismissViewControllerAnimated:departureActnSht completion:nil];
    }];
    
    [departureActnSht addAction:cancelAction];
    [self.view.window.rootViewController presentViewController:departureActnSht animated:YES completion:nil];
    
}

- (IBAction)onMonthTravelClicked:(id)sender
{
    UIAlertController *monthsActionSheet = [UIAlertController alertControllerWithTitle:@"Months"
                                                                               message:@"Select your choice"
                                                                        preferredStyle:UIAlertControllerStyleActionSheet];
    
    for (int localMonthIndex = 0 ; localMonthIndex < arrayMonthOfTravel.count; localMonthIndex++)
    {
        NSString *titleString = arrayMonthOfTravel[localMonthIndex];
        
        UIAlertAction *action = [UIAlertAction actionWithTitle:titleString style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
        {
            
            self.txtMonthOfTravel.text = arrayMonthOfTravel[localMonthIndex];
            
            NSString *strMonth = arrayMonthOfTravel[localMonthIndex];
            
            NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
            [formatter setDateFormat:@"MMMM yyyy"];
            
            NSDate *dateCurrent = [formatter dateFromString:strMonth];
            [formatter setDateFormat:@"MM"];
            
            monthIndex = [[formatter stringFromDate:dateCurrent]intValue];
            
        }];
        
        [monthsActionSheet addAction:action];
    }
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
        
        [self dismissViewControllerAnimated:monthsActionSheet completion:nil];
    }];
    
    [monthsActionSheet addAction:cancelAction];
    [self.view.window.rootViewController presentViewController:monthsActionSheet animated:YES completion:nil];
    
}

- (IBAction)onDurationClicked:(id)sender
{
    UIAlertController *durationActionSheet = [UIAlertController alertControllerWithTitle:@"Duration"
                                                                                 message:@"Select your choice"
                                                                          preferredStyle:UIAlertControllerStyleActionSheet];
    
    for (int j =0 ; j < arrayDuration.count; j++)
    {
        NSString *titleString = arrayDuration[j];
        UIAlertAction *action = [UIAlertAction actionWithTitle:titleString style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
            self.txtDuration.text = arrayDuration[j];
            
            if([self.txtDuration.text containsString:@"less than 3"])
            {
                minDuration = 1;
                maxDuration = 2;
            }
            else if ([self.txtDuration.text containsString:@"From 3 to 7"])
            {
                minDuration = 3;
                maxDuration = 7;
            }
            else if ([self.txtDuration.text containsString:@"more than 7"])
            {
                minDuration = 8;
                maxDuration = 98;
            }
            else
            {
                minDuration = 0;
                maxDuration = 0;
            }
        }];
        
        [durationActionSheet addAction:action];
    }
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
        
        [self dismissViewControllerAnimated:durationActionSheet completion:nil];
    }];
    
    [durationActionSheet addAction:cancelAction];
    
    [self.view.window.rootViewController presentViewController:durationActionSheet animated:YES completion:nil];
    
}

- (IBAction)onFlightFromClicked:(id)sender
{
    UIAlertController *flightList = [UIAlertController alertControllerWithTitle:@"Flight List"
                                                                        message:@"Select your choice"
                                                                 preferredStyle:UIAlertControllerStyleActionSheet];
    
    for (int j =0 ; j < arrayFlightFrom.count; j++)
    {
        NSString *titleString = arrayFlightFrom[j];
        
        UIAlertAction *action = [UIAlertAction actionWithTitle:titleString style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
        {
            self.txtDepartureCity.text = arrayDepartureList[j];
            self.txtFlightFrom.text = arrayDepartureList[j];
            
        }];
        
        [flightList addAction:action];
    }
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
        
        [self dismissViewControllerAnimated:flightList completion:nil];
    }];
    
    [flightList addAction:cancelAction];
    [self.view.window.rootViewController presentViewController:flightList animated:YES completion:nil];
    
}

-(void)applyFilter
{
    
    payloadList = [[NSMutableDictionary alloc] init];
    
    NSString *strHolidaySeasonType,*strInclusionType,*strPackageTypeLocal;
    
    
    if ((self.btnSeasonWinter.isSelected && self.btnSeasonSummer.isSelected) ||
        (!self.btnSeasonSummer.isSelected && !self.btnSeasonWinter.isSelected))
    {
        
        strHolidaySeasonType = @"both";
    }
    else
    {
        if (self.btnSeasonWinter.isSelected)
        {
            [payloadList setObject:@"yes" forKey:@"s^SEASON_MONSOON"];
            strHolidaySeasonType = @"winter";
        }
        
        if (self.btnSeasonSummer.isSelected)
        {
            
            [payloadList setObject:@"yes" forKey:@"s^SEASON_SUMMER"];
            strHolidaySeasonType = @"summer";
            
        }
    }
    
    if (self.btnPackagePersonalised.isSelected && self.btnPackageGroup.isSelected)
    {
        
        strPackageTypeLocal = @"both";
    }
    else
    {
        if (self.btnPackagePersonalised.isSelected)
        {
            [payloadList setObject:@"yes" forKey:@"s^PACKAG_TYPE_PERSONAL"];
           
            strPackageTypeLocal = @"fit";
        }
        
        if (self.btnPackageGroup.isSelected)
        {
            [payloadList setObject:@"yes" forKey:@"s^PACKAG_TYPE_GROUP"];
            strPackageTypeLocal = @"git";
        }
    }
    
    if (self.btnInclusionOnlyStay.isSelected && self.btnFlightInclusive.isSelected)
    {
        
        strInclusionType = @"both";
    }
    else
    {
        if (self.btnInclusionOnlyStay.isSelected){
            
             [payloadList setObject:@"yes"  forKey:@"s^INCLUSION_STAY"];
            strInclusionType = @"onlyStay";
        }
        
        if (self.btnFlightInclusive.isSelected){
            
            [payloadList setObject:@"yes"  forKey:@"s^INCLUSION_FLIGHT_INCLUSIVE"];
            strInclusionType = @"flightInclusive";
        }
    }
    
    
    /////  filter departure city
    
    NSMutableArray *arrayOfFilteredHolidays = [[NSMutableArray alloc]init];
    
    if ([self.txtDepartureCity.text isEqualToString:@"  Departure City"])
    {
        arrayOfFilteredHolidays = [[NSMutableArray alloc]initWithArray:self.arrayOfHolidayData];
    }
    else if ([self.txtDepartureCity.text isEqualToString:@"SELECT"])
    {
        arrayOfFilteredHolidays = [[NSMutableArray alloc]initWithArray:self.arrayOfHolidayData];
    }
    else
    {
        NSPredicate *pred = [NSPredicate predicateWithBlock:^BOOL(Holiday* evaluatedObject, NSDictionary *bindings) {
            
            
           // NSMutableArray *cityArray = [evaluatedObject.arrayOfHubList valueForKey:@"departCityName"];
            
            NSArray *cityArray = evaluatedObject.arrayOfHubList ;
           
            if([cityArray containsObject:self.txtDepartureCity.text])
            {
                return true;
            }
            
            return false;
        }];
        
        NSArray *results = [self.arrayOfHolidayData filteredArrayUsingPredicate:pred];
        if (results.count != 0)
        {
            arrayOfFilteredHolidays =  [[NSMutableArray alloc]initWithArray:results];
        }else
        {
            arrayOfFilteredHolidays =  [[NSMutableArray alloc]init];
        }
        
    }
    
    //filter for minimum and maximum package price
    
    if (arrayOfFilteredHolidays.count !=  0)
    {
        int minPrice = [self.lblMinimumPrice.text intValue];
        int maxPrice = [self.lblMaximumPrice.text intValue];
        
        NSPredicate *predpackagePrice = [NSPredicate predicateWithBlock:^BOOL(Holiday* evaluatedObject, NSDictionary *bindings) {
            
            if( minPrice <= evaluatedObject.packagePrise && maxPrice >= evaluatedObject.packagePrise)
            {
                return true;
            }
            return false;
        }];
        
        NSArray *results = [arrayOfFilteredHolidays filteredArrayUsingPredicate:predpackagePrice];
        if (results.count != 0)
        {
            arrayOfFilteredHolidays =  [[NSMutableArray alloc]initWithArray:results];
        }else
        {
            arrayOfFilteredHolidays =  [[NSMutableArray alloc]init];
        }
    }
    
    // filter for month travel
    
    if (arrayOfFilteredHolidays.count != 0 && ![self.txtMonthOfTravel.text containsString:@"Month of Travel"])
    {
        NSPredicate *monthTravelPred = [NSPredicate predicateWithBlock:^BOOL(Holiday* evaluatedObject, NSDictionary *bindings)
        {

            NSArray *datesArray = evaluatedObject.tcilHolidayTravelmonthCollection;
            
        
            
            if([[evaluatedObject.strPackageSubType lowercaseString] isEqualToString:@"git"]||[[evaluatedObject.strPackageSubType lowercaseString] isEqualToString:@"fit fixed"])
            {
                return true;
            }
        
            
            /*
             "monthCode":"DEC",
             "monthYear":2017,
             "travelmonthId":3540
             */
            
            
            if (datesArray)
            {
                if (datesArray.count >0)
                {
                    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
                    [formatter setDateFormat:@"MMMM yyyy"];
                    NSString *strDateFromUser = self.txtMonthOfTravel.text;
                    
                    NSDate *givenDate = [formatter dateFromString:strDateFromUser];
                    
                    NSDateFormatter *dateFormatForMonth = [[NSDateFormatter alloc]init];
                    [dateFormatForMonth setDateFormat:@"MMM"];
                    NSString *givenMonthString = [dateFormatForMonth stringFromDate:givenDate];
                    
                    
                    NSDateFormatter *dateFormatForYear = [[NSDateFormatter alloc]init];
                    [dateFormatForYear setDateFormat:@"yyyy"];
                    NSString *givenYearString = [dateFormatForYear stringFromDate:givenDate];
                    
                    NSPredicate *predicateForDateAvail =  [NSPredicate predicateWithFormat:@"monthCode CONTAINS[cd] %@ AND monthYear == %d", givenMonthString, [givenYearString intValue]];
                    
                    NSArray *filteredArrayForDates = [datesArray filteredArrayUsingPredicate:predicateForDateAvail];
                    
                    if (filteredArrayForDates)
                    {
                        if (filteredArrayForDates.count >0)
                        {
                            return true;
                        }
                    }
                    
                }
            }
            
//            NSRange stringRange = {0,10};
//        
//            NSString *strValidFrom = [evaluatedObject.strPackageValidFrom substringWithRange:stringRange];
//
//            NSString *strValidTo  = [evaluatedObject.strPackageValidTo substringWithRange:stringRange];
            
            
//            NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
//            [formatter setDateFormat:@"yyyy-MM-dd"];
//            [formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
//            [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
//            
//            
//            NSDate *dateValidFrom = [formatter dateFromString:strValidFrom];
//            NSDate *dateValidTo   = [formatter dateFromString:strValidTo];
//            
//            [formatter setDateFormat:@"MMMM yyyy"];
//            
//            strValidFrom = [formatter stringFromDate:dateValidFrom];
//            strValidTo = [formatter stringFromDate:dateValidTo];
//            
//            dateValidFrom = [formatter dateFromString:strValidFrom];
//            dateValidTo   = [formatter dateFromString:strValidTo];
//            
//            NSString *strDateFromUser = self.txtMonthOfTravel.text;
//            
//            NSDate *dateFromUser = [formatter dateFromString:strDateFromUser];
//            
//            if (dateFromUser!=nil)
//            {
//                BOOL isDatevalid ;
//                isDatevalid= [self isDate:dateFromUser inRangeFirstDate:dateValidFrom lastDate:dateValidTo];
//                if (isDatevalid)
//                {
//                    return true;
//                }
//            }else
//            {
//                return false;
//            }
            return false;
        }];
        
        NSArray *results = [arrayOfFilteredHolidays filteredArrayUsingPredicate:monthTravelPred];
        if (results.count != 0)
        {
            arrayOfFilteredHolidays =  [[NSMutableArray alloc]initWithArray:results];
        }else
        {
            arrayOfFilteredHolidays =  [[NSMutableArray alloc]init];
        }
    }
    
    //filter package type
    
    if ([arrayOfFilteredHolidays count]!= 0 && ![strPackageTypeLocal isEqualToString:@"both"] && strPackageTypeLocal != nil)
    {
        NSPredicate *pred = [NSPredicate predicateWithBlock:^BOOL(Holiday* evaluatedObject, NSDictionary *bindings) {
            
            
            if ([strPackageTypeLocal isEqualToString:@"fit"])
            {
                if([[evaluatedObject.strPackageSubType lowercaseString] isEqualToString:strPackageTypeLocal] || [[evaluatedObject.strPackageSubType lowercaseString] isEqualToString:@"fit fixed"])
                {
                    return true;
                }
                return false;
                
            }
            else
            {
                if([[evaluatedObject.strPackageSubType lowercaseString] isEqualToString:strPackageTypeLocal])
                {
                    return true;
                }
                return false;
            }
            
        }];
        
        NSArray *results = [arrayOfFilteredHolidays filteredArrayUsingPredicate:pred];
        if (results.count != 0)
        {
            arrayOfFilteredHolidays =  [[NSMutableArray alloc]initWithArray:results];
        }else
        {
            arrayOfFilteredHolidays =  [[NSMutableArray alloc]init];
        }
        
    }
    
    
    // filter season check
    if ([arrayOfFilteredHolidays count] != 0 && strHolidaySeasonType!=nil && ![strHolidaySeasonType isEqualToString:@"both"])
    {
        NSPredicate *pred = [NSPredicate predicateWithBlock:^BOOL(Holiday* evaluatedObject, NSDictionary *bindings) {
            
            NSString *strSeasonFromObject = [evaluatedObject.strSeasonType lowercaseString];
            
            if ([strHolidaySeasonType isEqualToString:strSeasonFromObject])
            {
                return true;
            }
            return false;
        }];
        
        NSArray *results = [arrayOfFilteredHolidays filteredArrayUsingPredicate:pred];
        if (results.count != 0)
        {
            arrayOfFilteredHolidays =  [[NSMutableArray alloc]initWithArray:results];
        }
        else
        {
            arrayOfFilteredHolidays =  [[NSMutableArray alloc]init];
        }
    }
    
    
    // flight inclusion check
    
    if (strInclusionType != nil && arrayOfFilteredHolidays.count != 0)
    {
        if ([strInclusionType isEqualToString:@"onlyStay"] ||[strInclusionType isEqualToString:@"both"]) {
            
            NSPredicate *predForPackageType = [NSPredicate predicateWithBlock:^BOOL(Holiday* evaluatedObject, NSDictionary *bindings) {
                
                if (evaluatedObject.accomFlag)
                {
                    return true;
                }
                return false;
            }];
            
            NSArray *results = [arrayOfFilteredHolidays filteredArrayUsingPredicate:predForPackageType];
            if (results.count != 0)
            {
                arrayOfFilteredHolidays =  [[NSMutableArray alloc]initWithArray:results];
            }else
            {
                arrayOfFilteredHolidays =  [[NSMutableArray alloc]init];
            }
            
        }
        
        if ([strInclusionType isEqualToString:@"flightInclusive"]||[strInclusionType isEqualToString:@"both"]){
            
            
            NSPredicate *pred = [NSPredicate predicateWithBlock:^BOOL(Holiday* evaluatedObject, NSDictionary *bindings) {
                
                if (evaluatedObject.airFlag) {
                    return true;
                }
                return false;
            }];
            
            NSArray *results = [arrayOfFilteredHolidays filteredArrayUsingPredicate:pred];
            if (results.count != 0)
            {
                arrayOfFilteredHolidays =  [[NSMutableArray alloc]initWithArray:results];
            }
            else
            {
                arrayOfFilteredHolidays =  [[NSMutableArray alloc]init];
            }
            
            if (![self.txtFlightFrom.text isEqualToString:@"  Flight From"])
            {
                NSPredicate *pred = [NSPredicate predicateWithBlock:^BOOL(Holiday* evaluatedObject, NSDictionary *bindings) {
                    
                    //NSArray *cityArray = [evaluatedObject.arrayOfHubList valueForKey:@"departCityName"];
                    
                    NSArray *cityArray = evaluatedObject.arrayOfHubList;

                    
                    if([cityArray containsObject:self.txtFlightFrom.text])
                    {
                        return true;
                    }
                    return false;
                }];
                
                NSArray *results = [arrayOfFilteredHolidays filteredArrayUsingPredicate:pred];
                if (results.count != 0)
                {
                    arrayOfFilteredHolidays =  [[NSMutableArray alloc]initWithArray:results];
                }else
                {
                    arrayOfFilteredHolidays = [[NSMutableArray alloc]init];
                }
                
                
                //                for (int holidayIndex = 0 ; holidayIndex < [arrayOfFilteredHolidays count]; holidayIndex++) {
                //
                //                    BOOL isHolidayFound = FALSE;
                //
                //                    Holiday *object = [arrayOfFilteredHolidays objectAtIndex:holidayIndex];
                //
                //                    NSArray *arrayOfHubList = object.arrayOfHubList;
                //
                //                    for (int hubIndex = 0; hubIndex < [arrayOfHubList count]; hubIndex ++) {
                //
                //
                //                        NSString *strHubName = [[[arrayOfHubList objectAtIndex:hubIndex]valueForKey:@"departCityName"]lowercaseString];
                //
                //                        if ([strHubName isEqualToString:[self.txtFlightFrom.text lowercaseString]]) {
                //
                //                            isHolidayFound = YES;
                //                        }
                //                    }
                //
                //                    if (!isHolidayFound) {
                //
                //                        [arrayOfFilteredHolidays removeObjectAtIndex:holidayIndex];
                //                    }
                //                }
            }
        }
    }
    
    
    
    //
    
    
    
    //Final filter logic
    if ([arrayOfFilteredHolidays count]!= 0)
    {
        NSString *count=[NSString stringWithFormat:@"%lu",(unsigned long)[arrayOfFilteredHolidays count]];
        NSDictionary *dictOfFilteredData=[[NSDictionary alloc] initWithObjectsAndKeys:arrayOfFilteredHolidays,@"data",count,@"totalPackages",nil];
        [[NSNotificationCenter defaultCenter]postNotificationName:kDataDidFilteredNotification
                                                           object:self
                                                         userInfo:dictOfFilteredData];
        
        if([strPackageTypeLocal caseInsensitiveCompare:@"International"]==NSOrderedSame)
        {
            [FIRAnalytics logEventWithName:@"International_Holidays_Filters_Apply" parameters:@{kFIRParameterItemName:[NSString stringWithFormat:@"DepartureCity :%@, FlightFrom :%@, ModeOfTravel :%@, Duration :%@",self.txtDepartureCity.text,self.txtFlightFrom.text,self.txtMonthOfTravel.text,self.txtDuration.text]}];
        }
        else
        {
           [FIRAnalytics logEventWithName:@"India_Holidays_Filters_Apply" parameters:@{kFIRParameterItemName:[NSString stringWithFormat:@"DepartureCity :%@, FlightFrom :%@, ModeOfTravel :%@, Duration :%@",self.txtDepartureCity.text,self.txtFlightFrom.text,self.txtMonthOfTravel.text,self.txtDuration.text]}];
        }
        
    }
    else{
        
        UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Info"
                                                                           message:@"No Packages found"
                                                                    preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK"
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction *action){
                                                             
                                                             [alertView dismissViewControllerAnimated:YES
                                                                                           completion:nil];
                                                             
                                                         }];
        
        [alertView addAction:okAction];
        
        if([strPackageTypeLocal caseInsensitiveCompare:@"International"]==NSOrderedSame)
        {
            [FIRAnalytics logEventWithName:@"International_Holidays_Filters_Apply_Failure" parameters:@{kFIRParameterItemName:self.txtDepartureCity.text,kFIRParameterItemName:self.txtFlightFrom.text,kFIRParameterItemName:self.txtMonthOfTravel.text,kFIRParameterItemName:self.txtDuration.text}];
        }
        else{
             [FIRAnalytics logEventWithName:@"Indian_Holidays_Filters_Apply_Failure" parameters:@{kFIRParameterItemName:self.txtDepartureCity.text,kFIRParameterItemName:self.txtFlightFrom.text,kFIRParameterItemName:self.txtMonthOfTravel.text,kFIRParameterItemName:self.txtDuration.text}];
        }
        
        [self.view.window.rootViewController presentViewController:alertView animated:YES completion:nil];
    }
    
    [self netCoreAppPackageFilter];
    
}

- (IBAction)btnApplyFilterClicked:(id)sender {
    
    
    [FIRAnalytics logEventWithName:@"International_Holidays_Filters" parameters:@{kFIRParameterItemID:@"International Holidays Button Click on Filter"}];
    
    
    [self applyFilter];
    /*
     NSString *strHolidaySeasonType,*strInclusionType,*strPackageTypeLocal;
     
     
     if ((self.btnSeasonMansoon.isSelected && self.btnSeasonSummer.isSelected) ||
     (!self.btnSeasonSummer.isSelected && !self.btnSeasonMansoon.isSelected))
     {
     
     strHolidaySeasonType = @"both";
     }
     else
     {
     if (self.btnSeasonMansoon.isSelected)
     {
     
     strHolidaySeasonType = @"mansoon";
     
     }
     
     if (self.btnSeasonSummer.isSelected) {
     
     strHolidaySeasonType = @"summer";
     
     }
     }
     
     if (self.btnPackagePersonalised.isSelected && self.btnPackageGroup.isSelected)
     {
     
     strPackageTypeLocal = @"both";
     }
     else
     {
     if (self.btnPackagePersonalised.isSelected)
     {
     
     strPackageTypeLocal = @"fit";
     }
     
     if (self.btnPackageGroup.isSelected)
     {
     
     strPackageTypeLocal = @"git";
     }
     }
     
     if (self.btnInclusionOnlyStay.isSelected && self.btnFlightInclusive.isSelected)
     {
     
     strInclusionType = @"both";
     }
     else
     {
     if (self.btnInclusionOnlyStay.isSelected){
     
     strInclusionType = @"onlyStay";
     }
     
     if (self.btnFlightInclusive.isSelected){
     
     strInclusionType = @"flightInclusive";
     }
     }
     
     NSMutableArray *arrayOfFilteredHolidays = [[NSMutableArray alloc]init];
     NSMutableArray *packageIDArray=[[NSMutableArray alloc]init];
     //NSMutableArray *packageStringName=[[NSMutableArray alloc]init];
     
     
     // departure City Check
     
     ////--------------IF DEPARTURE TEXT IS departure city-----------------------
     NSString *strDepartCityName=@"  departure city";
     if (self.arrayOfHolidayData.count>0) {
     if ([[self.txtDepartureCity.text lowercaseString]isEqualToString:strDepartCityName]) {
     
     Holiday *objectForHoliday=[self.arrayOfHolidayData objectAtIndex:0];
     NSString *strTempPkgID=[objectForHoliday strPackageId];
     [packageIDArray addObject:strTempPkgID];
     
     for (int i=1; i<self.arrayOfHolidayData.count;i++) {
     Holiday *objectForHoliday=[self.arrayOfHolidayData objectAtIndex:i];
     NSString *strTempPkgID=[objectForHoliday strPackageId];
     for (int j=0; j<packageIDArray.count; j++) {
     if ([strTempPkgID isEqualToString:[packageIDArray objectAtIndex:j]]) {
     
     }else{
     
     [arrayOfFilteredHolidays addObject:objectForHoliday];
     }
     
     }
     
     }
     
     }
     
     }
     
     ///---
     
     for (int holidayObjectIndex = 0; holidayObjectIndex < [self.arrayOfHolidayData count]; holidayObjectIndex ++)
     {
     Holiday *objectHoliday = [self.arrayOfHolidayData objectAtIndex:holidayObjectIndex];
     
     NSArray *arrayOfHubList = objectHoliday.arrayOfHubList;
     
     for (int hubIndex = 0; hubIndex < [arrayOfHubList count]; hubIndex ++)
     {
     
     NSString *strHubName = [[[arrayOfHubList objectAtIndex:hubIndex]valueForKey:@"departCityName"]lowercaseString];
     
     
     // lonavala package is same in the both city(mumbai and pune)
     
     if ([strHubName isEqualToString:[self.txtDepartureCity.text lowercaseString]])
     {
     
     [arrayOfFilteredHolidays addObject:objectHoliday];
     
     }
     
     }
     
     }
     
     
     // price per person Check
     if ([arrayOfFilteredHolidays count] != 0) {
     
     int minPrice = [self.lblMinimumPrice.text intValue];
     int maxPrice = [self.lblMaximumPrice.text intValue];
     
     for (int holidayObject = 0 ; holidayObject < [arrayOfFilteredHolidays count]; holidayObject++) {
     
     Holiday *objectHoliday = [arrayOfFilteredHolidays objectAtIndex:holidayObject];
     
     int pricePerPerson = objectHoliday.packagePrise;
     
     if(pricePerPerson>=minPrice && pricePerPerson<=maxPrice)
     {
     
     
     }
     else{
     
     [arrayOfFilteredHolidays removeObjectAtIndex:holidayObject];
     }
     
     //            if (!pricePerPerson >= minPrice && !pricePerPerson <= maxPrice) {
     //
     //                [arrayOfFilteredHolidays removeObjectAtIndex:holidayObject];
     //            }
     }
     
     // month of travel check
     //int countArrayOfFilteredHolidays=[arrayOfFilteredHolidays count];
     if ([arrayOfFilteredHolidays count] != 0) {
     
     for (int holidayIndex = 0 ; holidayIndex < [arrayOfFilteredHolidays count]; holidayIndex++) {
     
     Holiday *object = [arrayOfFilteredHolidays objectAtIndex:holidayIndex];
     
     NSString *strValidFrom = object.strPackageValidFrom;
     NSString *strValidTo   = object.strPackageValidTo;
     
     NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
     [formatter setDateFormat:@"dd-MM-yyyy"];
     [formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
     [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
     
     
     NSDate *dateValidFrom = [formatter dateFromString:strValidFrom];
     NSDate *dateValidTo   = [formatter dateFromString:strValidTo];
     
     [formatter setDateFormat:@"MMMM yyyy"];
     
     strValidFrom = [formatter stringFromDate:dateValidFrom];
     strValidTo = [formatter stringFromDate:dateValidTo];
     
     
     dateValidFrom = [formatter dateFromString:strValidFrom];
     dateValidTo   = [formatter dateFromString:strValidTo];
     
     NSString *strDateFromUser = self.txtMonthOfTravel.text;
     
     NSDate *dateFromUser = [formatter dateFromString:strDateFromUser];
     //
     //                if (![dateFromUser isEqualToDate:dateValidTo]) {
     //                    [arrayOfFilteredHolidays removeObjectAtIndex:holidayIndex];
     //                }
     
     if (dateFromUser==nil) {
     
     }
     else{
     BOOL isDatevalid ;
     isDatevalid= [self isDate:dateFromUser inRangeFirstDate:dateValidFrom lastDate:dateValidTo];
     NSLog(@"boola %hhd",isDatevalid);
     
     
     if (!isDatevalid) {
     
     [arrayOfFilteredHolidays removeObjectAtIndex:holidayIndex];
     holidayIndex--;
     }
     }
     }
     
     // Duration of days check
     if ([arrayOfFilteredHolidays count] != 0) {
     
     for (int holidayIndex = 0 ; holidayIndex < [arrayOfFilteredHolidays count]; holidayIndex++) {
     
     Holiday *object = [arrayOfFilteredHolidays objectAtIndex:holidayIndex];
     
     int numberOfDays = object.durationNoDays;
     
     if (minDuration==0 && maxDuration==0) {
     
     }
     else{
     
     if (numberOfDays < minDuration || numberOfDays > maxDuration) {
     
     [arrayOfFilteredHolidays removeObjectAtIndex:holidayIndex];
     }
     }
     
     }
     
     //pakcage Type check
     
     
     if ([arrayOfFilteredHolidays count]!= 0) {
     
     //                    for (int  i = 1; i <= arrayOfFilteredHolidays.count ; i++)
     //                    {
     NSPredicate *pred = [NSPredicate predicateWithBlock:^BOOL(Holiday* evaluatedObject, NSDictionary *bindings) {
     
     if([[evaluatedObject.strPackageSubType lowercaseString] isEqualToString:strPackageTypeLocal])
     {
     return true;
     }
     return false;
     }];
     
     NSArray *results = [arrayOfFilteredHolidays filteredArrayUsingPredicate:pred];
     if (results.count != 0)
     {
     arrayOfFilteredHolidays =  [[NSMutableArray alloc]initWithArray:results];
     }
     //}
     
     
     // season check
     if ([arrayOfFilteredHolidays count] != 0 && strHolidaySeasonType!=nil && ![strHolidaySeasonType isEqualToString:@"both"])
     {
     NSPredicate *pred = [NSPredicate predicateWithBlock:^BOOL(Holiday* evaluatedObject, NSDictionary *bindings) {
     
     NSString *strSeasonFromObject = [evaluatedObject.strSeasonType lowercaseString];
     
     if ([strHolidaySeasonType isEqualToString:strSeasonFromObject]) {
     return true;
     }
     return false;
     }];
     
     NSArray *results = [arrayOfFilteredHolidays filteredArrayUsingPredicate:pred];
     if (results.count != 0)
     {
     arrayOfFilteredHolidays =  [[NSMutableArray alloc]initWithArray:results];
     }else
     {
     arrayOfFilteredHolidays =  [[NSMutableArray alloc]init];
     }
     }
     
     
     // flight inclusion check
     
     if ([arrayOfFilteredHolidays count] != 0) {
     
     if ([strInclusionType isEqualToString:@"onlyStay"]) {
     
     NSPredicate *pred = [NSPredicate predicateWithBlock:^BOOL(Holiday* evaluatedObject, NSDictionary *bindings) {
     
     if (evaluatedObject.accomFlag) {
     return true;
     }
     return false;
     }];
     
     NSArray *results = [arrayOfFilteredHolidays filteredArrayUsingPredicate:pred];
     if (results.count != 0)
     {
     arrayOfFilteredHolidays =  [[NSMutableArray alloc]initWithArray:results];
     }else
     {
     arrayOfFilteredHolidays =  [[NSMutableArray alloc]init];
     }
     
     }
     else if ([strInclusionType isEqualToString:@"flightInclusive"]){
     
     //                                for (int holidayIndex = 0 ; holidayIndex < [arrayOfFilteredHolidays count]; holidayIndex++) {
     //
     //                                    Holiday *object = [arrayOfFilteredHolidays objectAtIndex:holidayIndex];
     //
     //                                    if (!object.airFlag) {
     //
     //                                        [arrayOfFilteredHolidays removeObjectAtIndex:holidayIndex];
     //                                    }
     //                                }
     
     
     NSPredicate *pred = [NSPredicate predicateWithBlock:^BOOL(Holiday* evaluatedObject, NSDictionary *bindings) {
     
     if (evaluatedObject.airFlag) {
     return true;
     }
     return false;
     }];
     
     NSArray *results = [arrayOfFilteredHolidays filteredArrayUsingPredicate:pred];
     if (results.count != 0)
     {
     arrayOfFilteredHolidays =  [[NSMutableArray alloc]initWithArray:results];
     }else
     {
     arrayOfFilteredHolidays =  [[NSMutableArray alloc]init];
     }
     
     
     for (int holidayIndex = 0 ; holidayIndex < [arrayOfFilteredHolidays count]; holidayIndex++) {
     
     BOOL isHolidayFound = FALSE;
     
     Holiday *object = [arrayOfFilteredHolidays objectAtIndex:holidayIndex];
     
     NSArray *arrayOfHubList = object.arrayOfHubList;
     
     for (int hubIndex = 0; hubIndex < [arrayOfHubList count]; hubIndex ++) {
     
     
     NSString *strHubName = [[[arrayOfHubList objectAtIndex:hubIndex]valueForKey:@"departCityName"]lowercaseString];
     
     if ([strHubName isEqualToString:[self.txtFlightFrom.text lowercaseString]]) {
     
     isHolidayFound = YES;
     }
     }
     
     if (!isHolidayFound) {
     
     [arrayOfFilteredHolidays removeObjectAtIndex:holidayIndex];
     }
     }
     }
     }
     }
     }
     }
     }
     
     
     if ([arrayOfFilteredHolidays count]!= 0) {
     NSString *count=[NSString stringWithFormat:@"%lu",(unsigned long)[arrayOfFilteredHolidays count]];
     NSDictionary *dictOfFilteredData=[[NSDictionary alloc] initWithObjectsAndKeys:arrayOfFilteredHolidays,@"data",count,@"totalPackages",nil];
     [[NSNotificationCenter defaultCenter]postNotificationName:kDataDidFilteredNotification
     object:self
     userInfo:dictOfFilteredData];
     
     }
     else{
     
     UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Info"
     message:@"No Packages found"
     preferredStyle:UIAlertControllerStyleAlert];
     
     UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK"
     style:UIAlertActionStyleDefault
     handler:^(UIAlertAction *action){
     
     [alertView dismissViewControllerAnimated:YES
     completion:nil];
     
     }];
     
     [alertView addAction:okAction];
     
     [self.view.window.rootViewController presentViewController:alertView animated:YES completion:nil];
     }*/
}

- (IBAction)onResetButtonClicked:(id)sender
{
    self.txtDepartureCity.text = @"  Departure City";
    self.txtDuration.text = @"  Duration";
    self.txtFlightFrom.text = @"  Flight From";
    self.txtMonthOfTravel.text = @"  Month of Travel";
    
    [self.btnFlightInclusive setSelected:NO];
    [self.btnInclusionOnlyStay setSelected:NO];
    [self.btnPackageGroup setSelected:NO];
    [self.btnPackagePersonalised setSelected:NO];
    [self.btnSeasonWinter setSelected:NO];
    [self.btnSeasonSummer setSelected:NO];
    
    [self configureLabelSlider];
    [self updateSliderLabels];


    //--RESET DATA FOR HOLIDAY ARRAY---
    //    self.arrayOfHolidayData=[arrayForResetData mutableCopy];
    CoreUtility *coreObj=[CoreUtility sharedclassname];
    
    if (coreObj.arrForResetPackageData.count>0) {
        NSString *count=[NSString stringWithFormat:@"%lu",(unsigned long)[coreObj.arrForResetPackageData count]];
        NSDictionary *dictOfFilteredData=[[NSDictionary alloc] initWithObjectsAndKeys:coreObj.arrForResetPackageData ,@"data",count,@"totalPackages",nil];
        [[NSNotificationCenter defaultCenter]postNotificationName:kDataDidFilteredNotification
                                                           object:self
                                                         userInfo:dictOfFilteredData];
    }else{
        
    }
    
}

- (IBAction)onRadioButtonClicked:(id)sender {
    
    UIButton *btnRadio = (UIButton *)sender;
    
    if ([btnRadio isSelected]){
        [btnRadio setSelected: NO];
    }
    else{
        [btnRadio setSelected:YES];
    }
}

#pragma mark - Helper methods

-(NSArray *)nextYearMonths
{
    NSDateFormatter  *dateFormatter   = [[NSDateFormatter alloc] init];
    NSDate           *today           = [NSDate date];
    NSCalendar       *currentCalendar = [NSCalendar currentCalendar];
    
    NSDateComponents *monthComponents = [currentCalendar components:NSCalendarUnitMonth fromDate:today];
    int currentMonth = (int)[monthComponents month];
    
    NSDateComponents *yearComponents  = [currentCalendar components:NSCalendarUnitYear  fromDate:today];
    int currentYear  = (int)[yearComponents year];
    int nextYear     = currentYear + 1;
    
    int months  = 1;
    int year;
    
    NSMutableArray *monthsArray = [[NSMutableArray alloc]init];
    
    //[monthsArray addObject:[NSString stringWithFormat:@"%@ %i",[[dateFormatter monthSymbols] objectAtIndex: currentMonth],currentYear]];
    
    for(int m = currentMonth; months <= 12; m++) {
        
        int nextMonth;
        
        if (m == 12)
        {
            nextMonth = 12;
        }
        else
        {
            nextMonth = (m % 12);
        }
        
        if(nextMonth < currentMonth){
            year = nextYear;
        } else {
            year = currentYear;
        }
        
        if (nextMonth >= 0 )
        {
            NSLog(@"Filter pacjkage = %@ %i",[[dateFormatter monthSymbols] objectAtIndex: nextMonth-1],year);
            [monthsArray addObject:[NSString stringWithFormat:@"%@ %i",[[dateFormatter monthSymbols] objectAtIndex: nextMonth-1],year]];
        }
        
        months++;
    }
    
    
    return monthsArray;
}

- (BOOL)isDate:(NSDate *)date inRangeFirstDate:(NSDate *)firstDate lastDate:(NSDate *)lastDate {
    
    if ([date compare:lastDate]==NSOrderedSame) {
        return[date compare:firstDate] == NSOrderedDescending;
    }
    else
        
        return[date compare:firstDate] == NSOrderedDescending &&
        [date compare:lastDate]  == NSOrderedAscending ;
}

#pragma mark NetCore LogEvents

//APP_Package_FilterPackage

-(void)netCoreAppPackageFilter
{
//  Holiday* evaluatedObject;  //28-02-2018
    
    Holiday *evaluatedObject = [self.arrayOfHolidayData objectAtIndex:0];//28-02-2018
    
    [payloadList setObject:self.txtDepartureCity.text forKey:@"s^DESTINATION"];
    
    [payloadList setObject:[NSString stringWithFormat:@"%@", _txtDepartureCity.text] forKey:@"s^DEPARTURE_CITY"];
    [payloadList setObject:[NSNumber numberWithInt:evaluatedObject.packagePrise] forKey:@"i^PACKAGE_PRICE"];
    [payloadList setObject:[NSString stringWithFormat:@"%@", _txtMonthOfTravel.text] forKey:@"s^MONTH_OF_TRAVEL"];
    [payloadList setObject:[NSString stringWithFormat:@"%@",_txtDuration.text] forKey:@"s^DURATION"];
    
    
    if([[evaluatedObject.strPackageSubType lowercaseString] isEqualToString:@"git"])
    {
       //group
         [payloadList setObject:@"yes" forKey:@"s^PACKAG_TYPE_GROUP"];
         [payloadList setObject:@"no" forKey:@"s^PACKAG_TYPE_PERSONAL"];
    }
    else
    {
        //personalised
        [payloadList setObject:@"no" forKey:@"s^PACKAG_TYPE_GROUP"];
        [payloadList setObject:@"yes" forKey:@"s^PACKAG_TYPE_PERSONAL"];
    }

    
    
//    if (![payloadList objectForKey:@"s^PACKAG_TYPE_GROUP"])
//    {
//        [payloadList setObject:@"no" forKey:@"s^PACKAG_TYPE_GROUP"];
//    }
//    
//    if (![payloadList objectForKey:@"s^PACKAG_TYPE_PERSONAL"])
//    {
//        [payloadList setObject:@"no" forKey:@"s^PACKAG_TYPE_PERSONAL"];
//    }
    
    if (![payloadList objectForKey:@"s^INCLUSION_STAY"])
    {
         [payloadList setObject:@"no"  forKey:@"s^INCLUSION_STAY"];
    }
    
    if (![payloadList objectForKey:@"s^INCLUSION_FLIGHT_INCLUSIVE"])
    {
         [payloadList setObject:@"no"  forKey:@"s^INCLUSION_FLIGHT_INCLUSIVE"];
    }
    
    if (![payloadList objectForKey:@"s^INCLUSION_FLIGHT_FROM"])
    {
        [payloadList setObject:[NSString stringWithFormat:@"%@",self.txtFlightFrom.text] forKey:@"s^INCLUSION_FLIGHT_FROM"];
    }
    
   /* if (![payloadList objectForKey:@"s^SEASON_SUMMER"])//commented 28-02-2018 as s^SEASON_SUMMER not passed as parameter as per sheet
      {
         [payloadList setObject:@"no" forKey:@"s^SEASON_SUMMER"];
      }
    */
    
    if (![payloadList objectForKey:@"s^SEASON_MONSOON"])
    {
         [payloadList setObject:@"no" forKey:@"s^SEASON_MONSOON"];
    }
    
    if (![payloadList objectForKey:@"s^SOURCE"])//28-02-2018
    {
        [payloadList setObject:@"App" forKey:@"s^SOURCE"];
    }
    
    
//    Holiday *objectForHoliday = [self.arrayOfHolidayData objectAtIndex:0]; //28-02-2018
    
    if(evaluatedObject.strPackageType != nil && ![evaluatedObject.strPackageType isEqualToString:@""])
    {
        [payloadList setObject:evaluatedObject.strPackageType forKey:@"s^TYPE"]; //28-02-2018
    }

   // [NetCoreAnalyticsVC getNetCorePushNotificationDetailDictionary:payloadList withPayloadCount:114]; //28-02-2018
    
     [NetCoreAnalyticsVC getNetCorePushNotificationDetailDictionary:payloadList withPayloadCount:124];//28-02-2018
}

@end
