//
//  OtherDetailTableViewCell.m
//  holidays
//
//  Created by Kush_Tech on 03/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "OtherDetailTableViewCell.h"
#import "PackageDetailModel.h"
#import "ItinenaryObject.h"
#import "TransferObject.h"
#import "IncludeExcludeObject.h"
#import "WebUrlConstants.h"
#import "SightSeenObject.h"
#import "MealsObject.h"
#import "VisaPassInsurance.h"
@implementation OtherDetailTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    inclusionString = [[NSMutableString alloc]init];
    exclusionString = [[NSMutableString alloc]init];
    sightSeeingString = [[NSMutableString alloc]init];
    visaString = [[NSMutableString alloc]init];
    mealsString = [[NSMutableString alloc]init];
    allOtherString = [[NSMutableString alloc]init];
    
    allOtherStringArray = [[NSMutableArray alloc]init];
    inclusionStringArray = [[NSMutableArray alloc]init];
    exclusionStringArray = [[NSMutableArray alloc]init];
    
}

-(void)loadDataFromPackageModel:(PackageDetailModel*)packageModel forHolidayPackageDetail:(HolidayPackageDetail*)holiPackageDetailsModel forSection:(NSString*)sectionString{
    packageDetailModel = packageModel;
    NSMutableString *newWebMutableString = [[NSMutableString alloc]init];
    NSMutableArray *newWebStrringArr = [[NSMutableArray alloc]init];
    if ([sectionString isEqualToString:@"Itinenary"]) {
        for (id object in packageModel.itineraryCollectionArray) {
            ItinenaryObject *itinery = [[ItinenaryObject alloc]initWithItinenaryDict:object];
            NSString *discription = itinery.itineraryDescription;
//            NSAttributedString *attrStr = [[NSAttributedString alloc] initWithData:[discription dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:nil error:nil];
//            [allOtherString appendString:[NSString stringWithFormat:@"%@\n",attrStr.string]];
            [newWebMutableString appendString:[NSString stringWithFormat:@"%@\n",discription]];
            [newWebStrringArr addObject:discription];
        }
    } else if ([sectionString isEqualToString:@"Inclusion"]) {
        
        for (id object in packageModel.includeexcludeCollectionArray) {
            IncludeExcludeObject *itinery = [[IncludeExcludeObject alloc]initWithIncludeExcludeDict:object];
            NSString *discription = itinery.includes;
//            NSAttributedString *attrStr = [[NSAttributedString alloc] initWithData:[discription dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:nil error:nil];
//            [mutableString appendString:[NSString stringWithFormat:@"%@\n",attrStr.string]];
            NSLog(@"packageClassId %ld, mainPkgSubClassId %ld", itinery.packageClassId, holiPackageDetailsModel.mainPkgSubClassId);
            if (itinery.packageClassId == holiPackageDetailsModel.mainPkgSubClassId){
              [newWebMutableString appendString:[NSString stringWithFormat:@"%@\n",discription]];
                [newWebStrringArr addObject:discription];
            }
        }
        inclusionString = allOtherString;
        inclusionStringArray = newWebStrringArr;
    } else if ([sectionString isEqualToString:@"Exclusion"]) {
        for (id object in packageModel.includeexcludeCollectionArray) {
            IncludeExcludeObject *itinery = [[IncludeExcludeObject alloc]initWithIncludeExcludeDict:object];
            NSString *discription = itinery.excludes;
//            NSAttributedString *attrStr = [[NSAttributedString alloc] initWithData:[discription dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:nil error:nil];
//            [mutableString appendString:[NSString stringWithFormat:@"%@\n",attrStr.string]];
            NSLog(@"packageClassId %ld, mainPkgSubClassId %ld", itinery.packageClassId, holiPackageDetailsModel.mainPkgSubClassId);
            if (itinery.packageClassId == holiPackageDetailsModel.mainPkgSubClassId){
            [newWebMutableString appendString:[NSString stringWithFormat:@"%@\n",discription]];
            [newWebStrringArr addObject:discription];
            }
        }
        exclusionString = allOtherString;
        exclusionStringArray = newWebStrringArr;
    } else if ([[sectionString uppercaseString] isEqualToString:sVisaTransferInsurance]) {
        
        for (id object in packageModel.visaCollectionArray) {
            VisaPassInsurance *visaPass = [[VisaPassInsurance alloc]initWithVisaPassInsuranceModel:object];
            NSString *discription = visaPass.visaPasInsSrp;
//            NSAttributedString *attrStr = [[NSAttributedString alloc] initWithData:[discription dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:nil error:nil];
//            [mutableString appendString:[NSString stringWithFormat:@"%@\n",attrStr.string]];
            if (visaPass.packageClassId == holiPackageDetailsModel.mainPkgSubClassId){
                [newWebMutableString appendString:[NSString stringWithFormat:@"%@\n",discription]];
                [newWebStrringArr addObject:discription];
            }
        }
        visaString = allOtherString;
    }else if ([[sectionString uppercaseString] isEqualToString:sSightSeeing]) {
        if ([packageModel.sightseenCollectionArray count]>0){
        for (id object in packageModel.sightseenCollectionArray) {
            SightSeenObject *sightObj = [[SightSeenObject alloc]initWithSightSeenObjectDict:object];
            if (sightObj.packageClassId == holiPackageDetailsModel.mainPkgSubClassId){
                [newWebMutableString appendString:[NSString stringWithFormat:@"%@\n",sightObj.descriptionName]];
                [newWebStrringArr addObject:sightObj.descriptionName];
            }
        }
        }else{
            for (NSDictionary *object in holiPackageDetailsModel.sightseenCollection) {
                [newWebMutableString appendString:[NSString stringWithFormat:@"%@\n",object[@"typeDefaultMsg"]]];
                [newWebStrringArr addObject:object[@"typeDefaultMsg"]];
            }
            
        }
        
        sightSeeingString = allOtherString;
    }else if ([[sectionString uppercaseString] isEqualToString:sMeals]) {
        for (id object in packageModel.mealCollectionArray) {
            MealsObject *mealObj = [[MealsObject alloc]initWithMealsObjectDict:object];
            //            [mutableString appendString:[NSString stringWithFormat:@"%@\n",sightObj.name]];
            if (mealObj.packageClassId == holiPackageDetailsModel.mainPkgSubClassId){
                [newWebMutableString appendString:[NSString stringWithFormat:@"%@\n",mealObj.mealDescription]];
                [newWebStrringArr addObject:mealObj.mealDescription];
            }
            
        }
        mealsString = allOtherString;
    }else if ([[sectionString uppercaseString] isEqualToString:sTransfer]) {
        for (id object in packageModel.transfersCollectionArray) {
            TransferObject *transfer = [[TransferObject alloc]initWithTransferDict:object];
            //            [mutableString appendString:[NSString stringWithFormat:@"%@\n",sightObj.name]];
            if (transfer.packageClassId == holiPackageDetailsModel.mainPkgSubClassId){
                [newWebMutableString appendString:[NSString stringWithFormat:@"%@\n",transfer.transferdescription]];
                [newWebStrringArr addObject:transfer.transferdescription];
            }
            
        }
        mealsString = allOtherString;
    }
    
//    self.lbl_description.text = mutableString;
    allOtherString = newWebMutableString;
    [self showOnlyStringInWebView:[allOtherString copy]];
//    allOtherStringArray = newWebStrringArr;
//    [self showArrayOfStringInWebView:newWebStrringArr];
}

- (IBAction)btn_viewMorePress:(id)sender {
//    [self.otherInfoDelegate sendInfoOfSection:self.sectionTitle withDiscription:self.lbl_description.text inclusion:inclusionString exclusion:exclusionString withPackageModel:packageDetailModel];
    
    NSDictionary *dicts = @{sInclusion:inclusionString,
                            sExclusion:exclusionString,
                            sVisaTransferInsurance:visaString,
                            sSightSeeing:sightSeeingString,
                            sMeals:mealsString,
       };
    [self.otherInfoDelegate sendInfoArrayInDict:dicts withsectionTitle:self.sectionTitle withPackageModel:packageDetailModel];
    
//    [self.otherInfoDelegate sendInfoArrayOfSection:self.sectionTitle withDiscriptionArray:allOtherStringArray inclusionArray:inclusionStringArray exclusionArray:exclusionStringArray withPackageModel:packageDetailModel];
}
-(void)showOnlyStringInWebView:(NSString*)strings{
    NSString *htmlStringObj = [NSString stringWithFormat:@"<span style=\"font-family: Lato-Regular; font-size: 16\">%@</span>", strings];
        [self.webViewss loadHTMLString:htmlStringObj baseURL:nil];
}
-(void)showArrayOfStringInWebView:(NSArray*)strArray{
    for (NSString *htmlString in strArray) {
//        NSString *htmlStringObj = [NSString stringWithFormat:@"<font face='HelveticaNeue' size='3'>%@", htmlString];
//        NSLog(@"%@",htmlStringObj);
//         NSString *htmlStringObj = [NSString stringWithFormat:@"%@", [strArray objectAtIndex:0]];
//        NSString *htmlStringObj = [NSString stringWithFormat:@"<span style=\"font-family: Roboto-Regular; font-size: 17\">%@</span>", [strArray objectAtIndex:0]];
        NSString *htmlStringObj = [NSString stringWithFormat:@"<span style=\"font-family: Lato-Regular; font-size: 16\">%@</span>", htmlString];
        [self.webViewss loadHTMLString:htmlStringObj baseURL:nil];
    }
}
//"\u{2022}"
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end


// bulletString:@""
//UIFont *helvFont = [UIFont fontWithName:@"HelveticaNeue" size:15.0];
//self.lbl_description.attributedText = [self attributedStringForBulletTexts:itinenaryStrArray withFont:helvFont bulletString:@"\u2022" indentation:20.0 lineSpacing:2.0 paragraphSpacing:12.0 textColor:[UIColor blackColor] bulletColor:[UIColor blackColor]];
