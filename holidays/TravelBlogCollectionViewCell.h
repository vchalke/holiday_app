//
//  TravelBlogCollectionViewCell.h
//  holidays
//
//  Created by Kush_Tech on 24/02/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TravelBlogObject.h"
#import "TravelBlogObj.h"
NS_ASSUME_NONNULL_BEGIN

@interface TravelBlogCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbl_Info;
@property (weak, nonatomic) IBOutlet UIImageView *img_views;
@property (weak, nonatomic) IBOutlet UIWebView *web_views;

-(void)loadTravelBlogObjectInCell:(TravelBlogObject*)recentObj;
-(void)loadTravelBlogObject:(TravelBlogObj*)recentObj;
@end

NS_ASSUME_NONNULL_END
