//
//  ProductBO.swift
//  holidays
//
//  Created by Komal Katkade on 11/27/17.
//  Copyright © 2017 Mobicule. All rights reserved.
//

import UIKit

class ProductBO {
    var currency:NSString = "";
    var productName:NSString = "";
    var productID:NSInteger?;
    var fxAmount:NSInteger = 0;
    var amountInDollars:Float = 0;
    var inrAmount:NSInteger = 0;
    var isNewProduct :  Bool = true
    var isUpdateButtonVisible = false
    var currencyCode:NSString = ""
    var roe : Float = 0
    var isNostroAcc : NSString? = ""
    var currencyID:NSInteger?;
    
    init()
    {
        currency = ""
        productName = ""
        productID = nil;
        fxAmount = 0
        inrAmount = 0
        isNewProduct = true
        isUpdateButtonVisible = false
        isNostroAcc = ""
        currencyID = nil
        
    }
    init(productName:NSString,currency:NSString,fxAmount:NSInteger,inrAmount:NSInteger)
    {
        self.currency = currency;
        self.productName = productName;
        self.fxAmount = fxAmount;
        self.inrAmount = inrAmount;
        self.isNewProduct = true
        isUpdateButtonVisible = false
        
    }
    
    func isEmpty() -> Bool {
        
        if productName.isEqual(to: "")
        {
            return true
        }
        if currency.isEqual(to: "")
        {
            return true
        }
        if fxAmount == 0
        {
            return true
        }
        if inrAmount == 0
        {
            return true
        }

        return false
    }
    
    func isEmptyAllEmpty() -> Bool {
        
        if currency.isEqual(to: "")
        {
            return true
        }
        if fxAmount == 0
        {
            return true
        }
        if inrAmount == 0
        {
            return true
        }
        
        return false
    }
    
    func markAsOldProduct() -> Void
    {
        self.isNewProduct = false
    }
    func markAsOpen() -> Void
    {
        self.isNewProduct = true
    }
    func getProductName() -> NSString
    {
        return self.productName
    }
    func getCurrency() -> NSString
    {
        return self.currency
    }
    func getCurrencyCode() -> NSString
    {
        return self.currencyCode
    }
    func getFXAmount() -> NSString
    {
        if self.fxAmount == 0
        {
            return ""
        }
        return NSString.init(format: "%i",  self.fxAmount)
    }
    func getINRAmount() -> NSString
    {
        if self.inrAmount == 0
        {
            return ""
        }
        return NSString.init(format: "%i",  self.inrAmount)
    }
    func getROE() -> Float
    {
        return self.roe
    }
    func setProductName(productName : NSString) -> Void
    {
        self.productName = productName
    }
    
    func setProductId(productID : NSInteger) -> Void
    {
        self.productID = productID
    }
    
    func getProductId() -> NSInteger
    {
        return self.productID!
    }

    
    func setCurrencyCode(currencyCode : NSString) -> Void
    {
        self.currencyCode = currencyCode
    }
    func setCurrency(currency : NSString) -> Void
    {
       self.currency = currency
    }
    func setFXAmount(fxAmount : NSInteger) -> Void
    {
       self.fxAmount = fxAmount
    }
    func setINRAmount(inrAmount : NSInteger) -> Void {
        self.inrAmount = inrAmount
        
    }
    
    func setroe(roe : Float) -> Void
    {
        self.roe = roe
    }

    func setIsNostroAcc(astroAvail : NSString) -> ()
    {
        self.isNostroAcc = astroAvail
    }
    
    func getIsNostroAcc() ->  NSString
    {
        return self.isNostroAcc!
    }
    
    func setCurrencyId(currencyId:NSInteger) -> ()
    {
        self.currencyID = currencyId
    }
    
    func getCurrencyId() -> NSInteger
    {
        return self.currencyID!;
    }
}
