//
//  TalkToExpertView.h
//  holidays
//
//  Created by Kush_Tech on 11/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ExpertViewCell.h"
NS_ASSUME_NONNULL_BEGIN
@protocol TalkToExpertViewDelegate <NSObject>
@optional
- (void)optionClickwithIndex:(NSInteger)indexNum;
@end
@interface TalkToExpertView : UIView<UITableViewDelegate,UITableViewDataSource>{
    NSArray *imgArray;
    NSArray *titleArray;
     BOOL isLeftSide;
}
@property (weak, nonatomic) IBOutlet UITableView *table_views;
@property (weak, nonatomic) IBOutlet UIView *upper_views;
@property (weak, nonatomic) IBOutlet UIView *bottom_views;
@property (nonatomic, weak) id <TalkToExpertViewDelegate> expertDelgate;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cnst_distfromBotom;
-(void)setDistanceFromBottom:(CGFloat)distance withLeftSide:(BOOL)isLeft;
@end

NS_ASSUME_NONNULL_END
