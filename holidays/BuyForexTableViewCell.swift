//
//  BuyForexTableViewCell.swift
//  holidays
//
//  Created by Komal Katkade on 11/24/17.
//  Copyright © 2017 Mobicule. All rights reserved.
//

import UIKit
protocol  BuyForexTableViewCellDelegate: class
{
    func textFieldProductNameBeginEditing(textField: UITextField)
    func textFieldCurrencyBeginEditing(textField: UITextField)
    func didPressEditButton(_ tag: Int)
    func didPressProductNameHelpButton(_ tag: Int)
    func didPressINRHelpButton(_ tag: Int)
    func didPressDeleteProductButton(_ tag: Int)
    func didPressUpdateButton(tag: Int)
   
}

class BuyForexTableViewCell: UITableViewCell,UITextFieldDelegate
{
    
    @IBOutlet weak var conversionLabel: UILabel!
    @IBOutlet weak var heightConstraintOfconversionView: NSLayoutConstraint!
    @IBOutlet weak var travellerView: UIView!
    
    @IBOutlet weak var minimizedProductName: UILabel!
    @IBOutlet weak var labelFxAmount: UILabel!
    @IBOutlet weak var heightConstraintOfTravellerView: NSLayoutConstraint!
    @IBOutlet weak var labelINRAmt: UILabel!
    @IBOutlet weak var minimizeView: UIView!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var labelProductNoInLargeView: UILabel!
    @IBOutlet weak var heightConstraintOfMinimizeView: NSLayoutConstraint!
    
    @IBOutlet weak var selectproductHeightConstraints: NSLayoutConstraint!
    @IBOutlet weak var heightConstraintOfDeleteButtonView: NSLayoutConstraint!
    @IBOutlet weak var updateButton: UIButton!
    @IBOutlet weak var textFieldFXINRAmt: UITextField!
    @IBOutlet weak var textFieldFXAmt: UITextField!
    @IBOutlet weak var textFieldProduct: UITextField!
    @IBOutlet weak var textFieldCurrency: UITextField!

    @IBOutlet weak var labelProductNoInEditView: UILabel!
    @IBOutlet weak var labelProductNoInMinimiezedView: UILabel!
    @IBOutlet weak var updateButtonView: UIView!
    weak var cellDelegate: BuyForexTableViewCellDelegate?
    
    
    weak var baseViewVC:ForexBaseViewController?
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        self.labelProductNoInMinimiezedView.layer.cornerRadius = 15
        textFieldCurrency.delegate = self
        textFieldProduct.delegate = self
//        textFieldFXAmt.delegate = self
//        textFieldFXINRAmt.delegate = self
        
        self.addDoneButtonOnKeyboard()
    }
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle       = UIBarStyle.default
        let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(BuyForexTableViewCell.doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.textFieldFXAmt.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction()
    {
        self.textFieldFXAmt.resignFirstResponder()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField == textFieldFXAmt
        {
            let textFieldText: NSString = (textField.text ?? "") as NSString
            
            let txtAfterUpdate = textFieldText.replacingCharacters(in: range, with: string)
            
            if txtAfterUpdate.count > 6
            {
                return false
            }
        }
        
        if textField == textFieldFXINRAmt
        {
            let textFieldText: NSString = (textField.text ?? "") as NSString
            
            let txtAfterUpdate = textFieldText.replacingCharacters(in: range, with: string)
            
            if txtAfterUpdate.count > 7
            {
                return false
            }
        }
        return true
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func productNameHelpBtnClicked(_ sender: Any)
    {
         cellDelegate?.didPressProductNameHelpButton(editButton.tag)
    }
    
    @IBAction func inrAmountHelpBtnClicked(_ sender: Any)
    {
        cellDelegate?.didPressINRHelpButton(editButton.tag)
    }
    @IBAction func updateButtonClicked(_ sender: Any)
    {
//        if let validationResult:String = self.validateCellData()
//        {
//            baseViewVC?.showAlert(message: validationResult as NSString)
//            
//        }
//        else
//        {
//        
        cellDelegate?.didPressUpdateButton(tag: editButton.tag)
        //didPressUpdateButton(tag: editButton.tag, productName: textFieldProduct.text! as NSString, currency:  NSInteger.init(textFieldCurrency.text!), fxAmount: textFieldFXAmt.text, inrAmount: NSInteger.init(textFieldFXINRAmt.text!) )
  //  }
    }
    
    @IBAction func deleteProductButtonClicked(_ sender: Any)
    {
        cellDelegate?.didPressDeleteProductButton(editButton.tag)
    }
    
    @IBAction func editButtonClicked(_ sender: Any)
    {
        cellDelegate?.didPressEditButton(editButton.tag)
    
        
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        if textField == textFieldProduct
        {
            cellDelegate?.textFieldProductNameBeginEditing(textField: textField)
            
            return false
        }
        else if textField == textFieldCurrency
        {
            cellDelegate?.textFieldCurrencyBeginEditing(textField: textField)
            
            cellDelegate?.textFieldCurrencyBeginEditing(textField: textField)
            return false
        }
        return true
    }
    
    
    func validateCellData() -> String?
    {
        if !self.textFieldProduct.hasText
        {
            return "Please select product"
        }else if !self.textFieldCurrency.hasText
        {
             return "Please select currency"
        }
        else if !self.textFieldFXAmt.hasText || (self.textFieldFXAmt.text?.isEmpty)! || (self.textFieldFXAmt.text ?? "") == ""
        {
            return "Please enter FX amount"
        }else if !self.textFieldFXINRAmt.hasText || (self.textFieldFXINRAmt.text?.isEmpty)! || (self.textFieldFXINRAmt.text ?? "") == ""
        {
            return "Please enter FX amount"
        }
        return nil
    }
    
   
}
