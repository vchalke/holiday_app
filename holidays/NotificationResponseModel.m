//
//  NotificationResponseModel.m
//  holidays
//
//  Created by Kush_Team on 16/07/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "NotificationResponseModel.h"
#define KEYCHAIN_SERVICE @"Holiday_App_Keychain"
#define KEYCHAIN_ACCOUNT @"com.thomasCookHoliday.app"
#define DATE_FORMAT_24_HR @"dd-MMM-yyyy HH:mm:ss"
#define DATE_FORMAT_12_HR @"dd-MMM-yyyy hh:mm:ss a"
@implementation NotificationResponseModel

-(instancetype)initWithNotificationResponseModel:(NSDictionary *)dictionary{
    if ([super init])
    {
        
        NSDictionary *MessageNotificationData = [dictionary objectForKey:@"MessageData"];
        NSDictionary *apsNotificationData = [MessageNotificationData objectForKey:@"aps"];
        NSDictionary *alertNotificationdata = [apsNotificationData objectForKey:@"alert"];
        NSString *title = [alertNotificationdata objectForKey:@"title"];
        NSString *message = [alertNotificationdata objectForKey:@"body"];
        NSString *subtitles = [alertNotificationdata objectForKey:@"subtitle"];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyMMddHHmmss"];
        NSDate *date = [dateFormatter dateFromString:[dictionary objectForKey:@"DeliverTime"]];
        [dateFormatter setDateFormat:DATE_FORMAT_12_HR];
        NSString *dateString=[dateFormatter stringFromDate:date];
        
        
        self.DeliverTime = dateString;
        self.ID = [dictionary valueForKey:@"ID"];
        self.Identity = [dictionary valueForKey:@"Identity"];
        self.Status = [dictionary valueForKey:@"Status"];
        
        self.MessageData = MessageNotificationData;
        self.aps = apsNotificationData;
        self.alert = alertNotificationdata;
        self.body = message;
        self.title = title;
        self.subtitle = subtitles;
        
        self.customPayload = [MessageNotificationData valueForKey:@"customPayload"];
        self.payload = [MessageNotificationData valueForKey:@"payload"];
        
        self.actionButton = [self.payload valueForKey:@"actionButton"];
        self.carousel = [self.payload valueForKey:@"carousel"];
        self.actionButton = [self.payload valueForKey:@"actionButton"] ;
        
        self.deeplink = [self.payload valueForKey:@"deeplink"];
        self.expiry = [self.payload valueForKey:@"expiry"];
        self.mediaurl = [self.payload valueForKey:@"mediaurl"];
        self.trid = [self.payload valueForKey:@"trid"];
        
    }
    return self;
}

@end

/*
{
    DeliverTime = 200716113647;
    ID = "39986-1401-5733119-0-200716111510";
    Identity = "";
    MessageData =         {
        aps =             {
            alert =                 {
                body = "Unlock the best deal this season! Get 5 Offer's at the price of 1. Mauritius starting @Rs.22,970/- T&C Apply";
                subtitle = "";
                title = "Mauritius - A Tropical Island!";
            };
            category = SmartechSimpleNotification;
            "mutable-content" = 1;
        };
        customPayload =             {
        };
        payload =             {
            actionButton =                 (
            );
            carousel =                 (
            );
            deeplink = "https://www.thomascook.in/holidays/short-haul-live-it-up-tour-packages/mauritius-getaway?pkgId=PKG004278&utm_source=Apush&utm_medium=APN&utm_campaign=Intl_16thJuly&__sta_linkid=8812&__sta=HVJYJTBTTQ%7CIFHI&__stm_medium=apn&__stm_source=smartech&__stm_id=1401";
            expiry = 1597297415;
            mediaurl = "";
            trid = "39986-1401-5733119-0-200716111510";
        };
    };
    Status = read;
}
 */
