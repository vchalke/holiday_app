//
//  ExpertViewCell.h
//  holidays
//
//  Created by Kush_Tech on 11/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ExpertViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbl_titleRight;
@property (weak, nonatomic) IBOutlet UIImageView *imgeViewRight;
@property (weak, nonatomic) IBOutlet UILabel *lbl_titleLeft;
@property (weak, nonatomic) IBOutlet UIImageView *imgeViewLeft;
@end

NS_ASSUME_NONNULL_END
