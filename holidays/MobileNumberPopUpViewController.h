//
//  MobileNumberPopUpViewController.h
//  holidays
//
//  Created by ketan on 07/01/16.
//  Copyright © 2016 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HolidayPackageDetail.h"
@protocol MobileNumberVCDelegate;
@interface MobileNumberPopUpViewController : UIViewController<UITextFieldDelegate>
- (IBAction)onGoButtonClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtViewMobileNumber;
@property (strong ,nonatomic) id <MobileNumberVCDelegate> delegate;
@property (strong ,nonatomic) HolidayPackageDetail *packageDetail;

@end

@protocol MobileNumberVCDelegate<NSObject>
@optional
- (void)goButtonClickedWithMobileNumber:(NSString *)mobileNumber;
@end
