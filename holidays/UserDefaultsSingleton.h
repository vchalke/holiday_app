//
//  UserDefaultsSingleton.h
//  holidays
//
//  Created by Kush_Tech on 20/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserDefaultsSingleton : NSObject
+ (id)sharedInstance;
- (id)init;
-(void)saveInUserDefaultsStringValue:(NSString*)value ForKey:(NSString*)key;
-(NSString*)getUserDefaultStringForKey:(NSString*)key;
-(NSArray*)getUserDefaultArrayForKey:(NSString*)key;
@end

NS_ASSUME_NONNULL_END
