//
//  NavigationController.swift
//  sotc-consumer-application
//
//  Created by Saurav Kumar on 18/07/17.
//  Copyright © 2017 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit

 
extension UINavigationController
{
    
    
    
    func setEmergencyNavigationRightButton(showEmergencyButton:Bool , viewController : UIViewController) -> Void {
        
        
        let emergencyCallButton :UIBarButtonItem  = UIBarButtonItem(image: #imageLiteral(resourceName: "emergencyNavigationHeader"), style: UIBarButtonItem.Style.plain, target: self, action: Selector?(#selector(UINavigationController.emergencyButtonClick)))
        emergencyCallButton.tintColor = UIColor.white
        
       // viewController.navigationItem.rightBarButtonItem = emergencyCallButton
        
    }
    
    
    func setNagationRightButton(showRightButton:Bool , viewController : UIViewController) -> Void {
        
        
        let emergencyCallButton :UIBarButtonItem  = UIBarButtonItem(image: #imageLiteral(resourceName: "emergencyTest"), style: UIBarButtonItem.Style.plain, target: self, action: Selector?(#selector(UINavigationController.emergencyButtonClick)))
        emergencyCallButton.tintColor = UIColor.white
        
 
        
        let infoCallButton :UIBarButtonItem  = UIBarButtonItem(image: #imageLiteral(resourceName: "infoTest"), style: UIBarButtonItem.Style.plain, target: viewController, action: Selector?(#selector(UINavigationController.presentInfoPopover)))
        infoCallButton.tintColor = UIColor.white
        
        
        
        let weatherCallButton :UIBarButtonItem  = UIBarButtonItem(image: #imageLiteral(resourceName: "weatherNavigationHeader"), style: UIBarButtonItem.Style.plain, target: self, action: Selector?(#selector(UINavigationController.weatherClick)))
        weatherCallButton.tintColor = UIColor.white
        
       // viewController.navigationItem.rightBarButtonItems = [/*weatherCallButton,infoCallButton,*/emergencyCallButton]
              
    }
    
    func setNagationBackButton(showRightButton:Bool , viewController : UIViewController) -> Void {
        
        
        let backCallButton :UIBarButtonItem  = UIBarButtonItem(image: #imageLiteral(resourceName: "backNavigation"), style: UIBarButtonItem.Style.plain, target: viewController, action: Selector?(#selector(UINavigationController.backButtonClick)))
        backCallButton.tintColor = UIColor.white
        
       
 
        viewController.navigationItem.backBarButtonItem =  backCallButton
        
    }
    
//    func setNagationLeftBackButton(showRightButton:Bool , viewController : UIViewController) -> Void {
//        
//        
//        let backCallButton :UIBarButtonItem  = UIBarButtonItem(image: #imageLiteral(resourceName: "backNavigation"), style: UIBarButtonItemStyle.plain, target: viewController, action: Selector?(#selector(UINavigationController.backButtonClick)))
//        backCallButton.tintColor = UIColor.white
//        
//        
//        viewController.navigationItem.leftBarButtonItem =  backCallButton
//        
//    }
    
    @objc func backButtonClick() -> Void {
        
        
        
    }
    
   @objc func presentInfoPopover()
   {

   }

    @objc func emergencyButtonClick() -> Void {
        
     AppUtility.CallToEmergencyNumber(phoneNumber: "18002093344")
        
    }
   
    @objc func weatherClick()
    {
        
        let weatherVC:WeatherViewController = WeatherViewController(nibName: "WeatherViewController", bundle: nil)
        
     
        UIApplication.shared.keyWindow?.rootViewController?.present(weatherVC, animated: true, completion: nil)
    }

    func setRedNavigationBar() -> Void {
        
        self.navigationBar.barTintColor =  Constant.AppThemeColor
        self.navigationBar.isTranslucent = false
         self.navigationBar.shadowImage = UIImage()

    }
    
    func setNavigationBarTranslucent() -> Void {
        
        self.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationBar.shadowImage = UIImage()
        self.navigationBar.isTranslucent = true
        
    }
    
    
    func setnavigatiobBarTitle(title:String , viewController:UIViewController) -> Void {
        
        
        if let _:UserBookingHomeViewController = viewController as? UserBookingHomeViewController
        {
            
        }else{
            
            viewController.navigationController?.navigationBar.isHidden = false
        }
        
        viewController.navigationController?.navigationBar.tintColor = UIColor.white
        viewController.navigationController?.navigationBar.topItem?.title = " "
        
        let titleLabel:UILabel = UILabel(frame: CGRect(x: 0, y: 40, width: 320, height: 40))
        titleLabel .font = UIFont(name:"Roboto-Medium", size:19)
        titleLabel.textAlignment = NSTextAlignment.left
        titleLabel.textColor = UIColor.white
        titleLabel.text = title
        viewController.navigationItem.titleView = titleLabel
        
    }

    @objc func visaDocumentButtonClick()
    {
        
    }
    
    func setVisaRightButton(showRightButton:Bool , viewController : UIViewController , showVisaDocButton:Bool ) -> Void {
        
        
        let visaDocButton :UIBarButtonItem  = UIBarButtonItem(image: #imageLiteral(resourceName: "fileCheck"), style: UIBarButtonItem.Style.plain, target: viewController, action: Selector?(#selector(UINavigationController.visaDocumentButtonClick)))
        visaDocButton.tintColor = UIColor.white
        
        let emergencyCallButton :UIBarButtonItem  = UIBarButtonItem(image: #imageLiteral(resourceName: "emergencyNavigationHeader"), style: UIBarButtonItem.Style.plain, target: self, action: Selector?(#selector(UINavigationController.emergencyButtonClick)))
        emergencyCallButton.tintColor = UIColor.white
        
        if showVisaDocButton
        {
            viewController.navigationItem.rightBarButtonItems = [/*emergencyCallButton,*/visaDocButton]
        }else{
        
          //  viewController.navigationItem.rightBarButtonItems = [emergencyCallButton]
        }
        
        
        
    }
    
    func setItineraryRightButton(showRightButton:Bool , viewController : UIViewController) -> Void {
        
        
        let shareDocButton :UIBarButtonItem  = UIBarButtonItem(image: #imageLiteral(resourceName: "share"), style: UIBarButtonItem.Style.plain, target: viewController, action: Selector?(#selector(UINavigationController.shareItineraryDocumentButtonClick)))
        shareDocButton.tintColor = UIColor.white
        
        let downloadItineraryButton :UIBarButtonItem  = UIBarButtonItem(image: #imageLiteral(resourceName: "download"), style: UIBarButtonItem.Style.plain, target: viewController, action: Selector?(#selector(UINavigationController.downloadItineraryButtonClick)))
        downloadItineraryButton.tintColor = UIColor.white
        
        viewController.navigationItem.rightBarButtonItems = [/*downloadItineraryButton,*/shareDocButton]
        
    }
    
    @objc func shareItineraryDocumentButtonClick()
    {
        
    }
    
    @objc func downloadItineraryButtonClick()
    {
        
    }
    
    
    
}


