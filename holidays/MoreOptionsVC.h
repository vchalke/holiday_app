//
//  MoreOptionsVC.h
//  holidays
//
//  Created by Kush_Team on 26/05/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BottomView.h"
#import "NewMasterVC.h"
#import "BottomStacksView.h"
#import "Firebase.h"
#import "LandingScreenVC.h"
NS_ASSUME_NONNULL_BEGIN
@interface MoreOptionsVC : LandingScreenVC
//@property (weak, nonatomic) IBOutlet BottomView *botton_view;
@property (weak, nonatomic) IBOutlet BottomStacksView *botton_view;
//@property (weak, nonatomic) IBOutlet UIView *botton_view;
@property (weak, nonatomic) IBOutlet UITableView *table_views;

@end

NS_ASSUME_NONNULL_END
