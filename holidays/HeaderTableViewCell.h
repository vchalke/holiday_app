//
//  HeaderTableViewCell.h
//  holidays
//
//  Created by Kush_Tech on 03/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewMasterCellTableViewCell.h"
#import "Holiday.h"
NS_ASSUME_NONNULL_BEGIN

@protocol HeaderTableViewCellDelegaete <NSObject>
@optional
- (void)sharePress;
- (void)showViewGallery;
- (void)likePress:(BOOL)flags;
- (void)comparePress:(BOOL)flags;
- (void)setImageName:(NSString*)imgString;

@end

@interface HeaderTableViewCell : NewMasterCellTableViewCell
@property (nonatomic, weak) id <HeaderTableViewCellDelegaete> headerDelegate;
@property (weak, nonatomic) IBOutlet UIImageView *tour_imgView;
@property (weak, nonatomic) IBOutlet UIView *like_view;
@property (weak, nonatomic) IBOutlet UIView *share_view;
@property (weak, nonatomic) IBOutlet UIView *compare_view;

@property (weak, nonatomic) IBOutlet UIButton *btn_Compare;
@property (weak, nonatomic) IBOutlet UIButton *btn_likes;
@property (weak, nonatomic) IBOutlet UIImageView *compare_ImgView;
@property (weak, nonatomic) IBOutlet UIImageView *share_ImgView;
@property (weak, nonatomic) IBOutlet UIImageView *heart_ImgView;
@property (strong, nonatomic) HolidayPackageDetail *packageDetailInCell;
@property (nonatomic ,strong)Holiday *holidayObjInHeaderTableView;
@property (strong, nonatomic) NSDictionary *compPackageDetailDict;
@property (strong, nonatomic) NSString *imgUrlString;
-(void)loadImageFromHolidayPackageDetail:(HolidayPackageDetail*)packAgeDetails withHolidayObj:(Holiday*)holidayObj;
@end

NS_ASSUME_NONNULL_END
