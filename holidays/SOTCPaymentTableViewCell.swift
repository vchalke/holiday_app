//
//  SOTCPaymentTableViewCell.swift
//  holidays
//
//  Created by Komal Katkade on 1/27/18.
//  Copyright © 2018 Mobicule. All rights reserved.
//

import UIKit

class SOTCPaymentTableViewCell: UITableViewCell {
    @IBOutlet weak var labelUnpaidAmount: UILabel!

    @IBOutlet weak var progressViewCustom: CustomProgressView!
    @IBOutlet weak var labelTotalAmount: UILabel!
    @IBOutlet weak var progressView: UIView!
    @IBOutlet weak var outerviewOfProgressView: UIView!
    @IBOutlet weak var labelPaidAmount: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func populateData(paymentObject: Payment,formatterArray:Array<NumberFormatter>)
    {
        
        let filteredArray = formatterArray.filter(){$0.currencyCode == paymentObject.currency}
        
        
        if filteredArray.count > 0 {
            
            self.labelPaidAmount.text = "\(filteredArray.first?.currencySymbol ?? "") \(paymentObject.totalReceived!)"
            self.labelUnpaidAmount.text = "\(filteredArray.first?.currencySymbol ?? "") \(paymentObject.balancePending!)"
            self.labelTotalAmount.text = "\(filteredArray.first?.currencySymbol ?? "") \(paymentObject.totalReceivable!)"
            
        }
        else
        {
            self.labelPaidAmount.text = "\(paymentObject.currency ?? "") \(paymentObject.totalReceived!)"
            self.labelUnpaidAmount.text = "\(paymentObject.currency ?? "") \(paymentObject.balancePending!)"
            self.labelTotalAmount.text = "\(paymentObject.currency ?? "") \(paymentObject.totalReceivable!)"
        }
        
        
    /*    var paidPercentage:Double = 0.0
        
        if let paidAmountFloat:Float = Float(paymentObject.totalReceived ?? "0.0")
        {
            if let totalAmountFloat:Float = Float(paymentObject.totalReceivable ?? "0.0")
            {
                paidPercentage =  Double((paidAmountFloat / totalAmountFloat))
            }
        }
        
       /* let paidAmountFloat:CGFloat = CGFloat(Double((paymentObject.totalReceived)!)!)
        let totalAmountFloat:CGFloat = CGFloat(Double((paymentObject.totalReceivable)!)!)
        var paidPercentage:CGFloat?
        if totalAmountFloat > 0.0
        {
            paidPercentage =  (paidAmountFloat / totalAmountFloat)
        }
        else
        {
            paidPercentage = 0.0
        }
       // paidPercentage =  CGFloat.init(exactly: Float.init(String(format: "%.1f", paidPercentage!))! as NSNumber)
        
       // paidPercentage =  CGFloat.init(exactly: Float.init(String(format: "%.1f", paidPercentage!))! as NSNumber) -- komal code*/
        
        
        
        DispatchQueue.main.async {
            
            let doubleStr:String = String(format: "%.1f", paidPercentage)
            
            var finalPaidPercentage:Double = 0.0
            
            if doubleStr == "0.0" && paidPercentage > 0.000000000001
            {
                finalPaidPercentage = 0.1
                
            }
            else
            {
                finalPaidPercentage = Double(doubleStr) ?? 0.0//doubleStr.floatValue
            }
            
           // progressBar.progress = 0.1
            
            let progressBar = CustomProgressView(progressViewStyle: .bar)
            progressBar.height = 30
            progressBar.frame = self.outerviewOfProgressView.frame
            self.outerviewOfProgressView.addSubview(progressBar)
            self.outerviewOfProgressView.layer.borderColor = UIColor.black.cgColor
            self.outerviewOfProgressView.layer.borderWidth = 1
            progressBar.layer.borderWidth = 1
            progressBar.layer.borderColor = UIColor.black.cgColor
            progressBar.progressTintColor = AppUtility.hexStringToUIColor(hex: "#D8EFF5")
            progressBar.frame.origin = CGPoint.init(x: 0, y: 0)
            progressBar.sizeThatFits(CGSize.init(width: 150, height:  30))

            
            DispatchQueue.main.async {
                
                progressBar.setProgress(Float(0.1), animated: true)
            }

            
           // progressBar.setProgress(finalPaidPercentage, animated: true)
        }
        */
        
        
         let paidAmountFloat:CGFloat = CGFloat(Double((paymentObject.totalReceived)!)!)
         let totalAmountFloat:CGFloat = CGFloat(Double((paymentObject.totalReceivable)!)!)
         var paidPercentage:CGFloat?
        
         if totalAmountFloat > 0.0
         {
         paidPercentage =  (paidAmountFloat / totalAmountFloat)
         }
         else
         {
         paidPercentage = 0.0
         }
         
         let doublePercentage = Double((String(format: "%.1f", paidPercentage!)))!
         
         //paidPercentage = CGFloat.init(doublePercentage) //CGFloat.init(exactly: ! as NSNumber)
        
        
        paidPercentage = CGFloat.init(Float.init(String(format: "%.1f", paidPercentage!))! as NSNumber)
         // paidPercentage =  CGFloat.init(exactly: Float.init(String(format: "%.1f", paidPercentage!))! as NSNumber) //-- komal code
         
         
         let progressBar = CustomProgressView(progressViewStyle: .bar)
         progressBar.height = 30
         progressBar.frame = self.outerviewOfProgressView.frame
         self.outerviewOfProgressView.addSubview(progressBar)
         self.outerviewOfProgressView.layer.borderColor = UIColor.black.cgColor
         self.outerviewOfProgressView.layer.borderWidth = 1
         progressBar.layer.borderWidth = 1
         progressBar.layer.borderColor = UIColor.black.cgColor
         progressBar.progressTintColor = AppUtility.hexStringToUIColor(hex: "#D8EFF5")
         progressBar.frame.origin = CGPoint.init(x: 0, y: 0)
       //  progressBar.sizeThatFits(CGSize.init(width: 150, height:  30))
         
         DispatchQueue.main.async
            {
         
         progressBar.setProgress(Float(0.2), animated: true)
         }
         
        
        
    }
    
}
