//
//  NewMasterVC.h
//  holidays
//
//  Created by Kush_Tech on 03/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageView+WebCache.h"
#import "WebUrlConstants.h"
#import "Reachability.h"
#import "FFToast.h"
#import "FlightsColectObject.h"
#import "PixelSingleton.h"
NS_ASSUME_NONNULL_BEGIN

@protocol NewMasterVCDelegate <NSObject>
@optional
- (void)setToolBarActionTextField:(UIBarButtonItem*)barButtonItem;
@end

@interface NewMasterVC : UIViewController<UITextFieldDelegate>
@property (nonatomic, weak) id <NewMasterVCDelegate> masterDelgate;
-(void)jumpToPreviousViewController;
-(void)disableBackSwipeAllow:(BOOL)allow;
-(NSLayoutConstraint*)setMultiplier:(NSLayoutConstraint *)changerLayout withMultiplier:(CGFloat)multiplier;
- (NSAttributedString *)attributedStringForBulletTexts:(NSArray *)stringList
                                              withFont:(UIFont *)font
                                          bulletString:(NSString *)bullet
                                           indentation:(CGFloat)indentation
                                           lineSpacing:(CGFloat)lineSpacing
                                      paragraphSpacing:(CGFloat)paragraphSpacing
                                             textColor:(UIColor *)textColor
                                           bulletColor:(UIColor *)bulletColor;
-(NSAttributedString*)getAttributeStringFromHTML:(NSString*)htmlString;
-(void)showAlertViewWithTitle:(NSString *)title withMessage:(NSString *)message;
-(void)showButtonsInAlertWithTitle:(NSString*)title msg:(NSString*)message style:(UIAlertControllerStyle)style buttArray:(NSArray*)buttArray completion:(void (^)( NSString *))completion;
-(void)showMultipleOptionsInAlertWithTitle:(NSString*)title msg:(NSString*)message style:(UIAlertControllerStyle)style buttArray:(NSArray*)buttArray completion:(void (^)( NSString *))completion;
- (void)setMaskTo:(UIView*)view byRoundingCorners:(UIRectCorner)corners withCornerRadius:(CGFloat)radiusValue;
-(NSString*)getNumberFormatterString:(NSInteger)number;
-(UIImage *)coloredImage:(UIImage *)firstImage withColor:(UIColor *)color;
-(NSString*)convertJsonDictToString:(NSDictionary*)jsonDict;
-(NSDictionary*)convertStringToJsonDict:(NSString*)string;
-(void)checkUpdateAvaialble;
-(UIToolbar*)getUIToolBarForKeyBoard;
-(void)showToastsWithTitle:(NSString *)title;
-(void)showToastsWithTitle:(NSString *)title withMessage:(NSString *)message;
@property (nonatomic) BOOL isNetworkPresent;
-(void)callForHolidayEnquiry;
- (void)changeMultiplier:(NSLayoutConstraint *)constraint to:(CGFloat)newMultiplier;
-(NSLayoutConstraint *)constraintWithIndientifer:(NSString *)identifer InView:(UIView *)view;
- (BOOL)validateEmailWithString:(NSString*)checkString;
- (BOOL)validatePhone:(NSString *)phoneNumber;
-(NSString*)isCheckValidityInSignIn:(NSDictionary*)signDictionary;
- (BOOL)checkInternet;
-(void)getTokenIDInMaster;

@end

NS_ASSUME_NONNULL_END
