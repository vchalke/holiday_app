//
//  WishListScreenVC.h
//  holidays
//
//  Created by Kush_Tech on 17/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewMasterVC.h"
#import "Holiday.h"
#import "CoreDataSingleton.h"
#import "SRPCollectionCell.h"
#import "SRPScreenVC.h"
#import "PDPScreenVC.h"
NS_ASSUME_NONNULL_BEGIN

@interface WishListScreenVC : SRPScreenVC
@property (weak, nonatomic) IBOutlet UICollectionView *collectionViews;

@end

NS_ASSUME_NONNULL_END
