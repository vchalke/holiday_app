//
//  NotificationDBManager.swift
//  sotc-consumer-application
//
//  Created by Parshwanath on 06/11/17.
//  Copyright © 2017 Mobicule Technologies Private Limited. All rights reserved.
//

import Foundation
import CoreData
class NotificationDBManager:NSObject
{
    class func getNotificationsDetails(by mobileNumber:String) -> Array<Notifications>?
    {
        do
        {
            
            let predicate = NSPredicate(format: "userMobileNumber == '\(mobileNumber)'")
            
            let fetchRequest: NSFetchRequest<Notifications> = Notifications.fetchRequest()
            
            fetchRequest.predicate = predicate
            
            let notificationsListFiltered:Array<Notifications>? = try CoreDataController.getContext().fetch(fetchRequest)
            
            
            if notificationsListFiltered != nil {
                
                let currentDateFilter = notificationsListFiltered?.filter({ (NotificationsObject) -> Bool in
                    
                    return Int64((NotificationsObject.createdOn?.timeIntervalSince1970)!) <= Int64(Date().timeIntervalSince1970)
                })
                
                if currentDateFilter != nil {
                    
                    let soretedNotifications = currentDateFilter?.sorted(by: { (Notifications1:Notifications, Notifications2:Notifications) -> Bool in
                        
                        Notifications1.createdOn?.compare(Notifications2.createdOn! as Date) == ComparisonResult.orderedDescending
                        
                    })
                         return soretedNotifications
                    
                }
            }
            
           return []
         
            
        }
        catch
        {
            printLog(error)
            
            return []
        }
        
    }
    
    class func markNotificationsAsRead(by timeStamp:String)
    {
        do
        {
            let fetchRequest: NSFetchRequest<Notifications> = Notifications.fetchRequest()
            
            let date:NSDate? = Date(timeIntervalSince1970: (TimeInterval(timeStamp)! / 1000.0)) as NSDate
            
            if date != nil
            {
                 
                let notificationsListFiltered:Array<Notifications>? = try CoreDataController.getContext().fetch(fetchRequest)
                
                if (notificationsListFiltered?.count)! > 0 {
                    
                    
                    let currentDateFilter = notificationsListFiltered?.filter({ (NotificationsObject) -> Bool in
                        
                        return Int64((NotificationsObject.createdOn?.timeIntervalSince1970)!) == Int64((date?.timeIntervalSince1970)!)
                    })
                    
                    if currentDateFilter != nil && (currentDateFilter?.count)! > 0
                    {
                    
                    if let notificationObject:Notifications = currentDateFilter?.first {
                        
                        
                        notificationObject.notificationStatus = "read"
                        
                        _ = try CoreDataController.getContext().save()
                    }
                }
                
                   
                NotificationController.updateNotificationBadgeCount()
                
             
            }
         
        }
        }
        catch
        {
            printLog(error)
        }
        
    }
    
    class func markNotificationsAsReadForNotification(by notification:Notifications)
    {
        do {
            
            notification.notificationStatus = "read"
            
            _ = try CoreDataController.getContext().save()
            
            NotificationController.updateNotificationBadgeCount()
            
        } catch  {
            
            printLog(error)
        }
        
        
        
    }
    class func getUnreadNotificationCount() -> Int
    {
        do
        { 
            let predicate = NSPredicate(format: "notificationStatus == 'unread'")
            
            let fetchRequest: NSFetchRequest<Notifications> = Notifications.fetchRequest()
            
            fetchRequest.predicate = predicate
            
            let notificationsListFiltered:Array<Notifications>? = try CoreDataController.getContext().fetch(fetchRequest)
            
            if (notificationsListFiltered?.count)! > 0 {
                
                
                if notificationsListFiltered != nil {
                    
                    let currentDateFilter = notificationsListFiltered?.filter({ (NotificationsObject) -> Bool in
                        
                        return Int64((NotificationsObject.createdOn?.timeIntervalSince1970)!) <= Int64(Date().timeIntervalSince1970)
                        
                        
                    })
                    
                    if currentDateFilter !=  nil {
                        
                         return currentDateFilter?.count ?? 0
                    }
                    
                    
                   
                }
            
            }
           
                return 0
            
            
        }
        catch
        {
            printLog(error)
            
            return 0
        }
        
    }
    
    //MARK: Delete Methods
    /*
    class func DeleteAllNotifications()  {
        
        do {
            
            let userMobileNumber:String = UserDefaults.standard.value(forKey:UserDefauldConstant.userMobileNumber) as! String
            
            let predicate = NSPredicate(format: "mobileNumber == \(userMobileNumber)")
            
            
            let tableFetchRequest: NSFetchRequest<UserProfile> = UserProfile.fetchRequest()
            tableFetchRequest.predicate = predicate
            
            let tableList:Array<UserProfile>? = try CoreDataController.getContext().fetch(tableFetchRequest)
            
            if tableList != nil && (tableList?.count)! > 0 {
                
                
                for item in (tableList?.first?.notificationRelation)! {
                    
                    CoreDataController.getContext().delete(item as! NSManagedObject)
                    
                }
                
                try CoreDataController.getContext().save()
                
                
            }
            
        } catch  {
            printLog(error)
        }
    }
    */
// New Handling 07/09/20
    class func DeleteAllNotifications()  {
        
        do {
            
            let userMobileNumber:String = UserDefaults.standard.value(forKey:UserDefauldConstant.userMobileNumber) as? String ?? ""
            if (userMobileNumber.count > 0){
                let predicate = NSPredicate(format: "mobileNumber == \(userMobileNumber)")
                
                
                let tableFetchRequest: NSFetchRequest<UserProfile> = UserProfile.fetchRequest()
                tableFetchRequest.predicate = predicate
                
                let tableList:Array<UserProfile>? = try CoreDataController.getContext().fetch(tableFetchRequest)
                
                if tableList != nil && (tableList?.count)! > 0 {
                    
                    
                    for item in (tableList?.first?.notificationRelation)! {
                        
                        CoreDataController.getContext().delete(item as! NSManagedObject)
                        
                    }
                    
                    try CoreDataController.getContext().save()
                    
                    
                }
            }
        } catch  {
            printLog(error)
        }
    }
    
    
}
