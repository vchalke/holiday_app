//
//  AdvertiseCollectionViewCell.m
//  holidays
//
//  Created by Kush_Tech on 24/02/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "AdvertiseCollectionViewCell.h"
#import "UIImageView+WebCache.h"

@implementation AdvertiseCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(void)setData:(BankOffersObject*)bankOffer{
    
    self.lbl_bankInfo.text = bankOffer.holidayoffername;
    NSURL* urlImage=[NSURL URLWithString:bankOffer.imagepath];
    if(urlImage)
    {
        UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        indicator.center = self.img_bank.center;
        [self.img_bank addSubview:indicator];
        [indicator startAnimating];
        [self.img_bank sd_setImageWithURL:urlImage placeholderImage:[UIImage imageNamed:@"defaultBanner.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
            [indicator stopAnimating];
        }];
    }
    
}

@end
