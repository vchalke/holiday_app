//
//  AdvertiseCollectionViewCell.h
//  holidays
//
//  Created by Kush_Tech on 24/02/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BankOffersObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface AdvertiseCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *img_bank;
@property (weak, nonatomic) IBOutlet UILabel *lbl_bankInfo;
-(void)setData:(BankOffersObject*)bankOffer;
@end

NS_ASSUME_NONNULL_END
