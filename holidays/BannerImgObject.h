//
//  BannerImgObject.h
//  holidays
//
//  Created by Kush_Tech on 24/02/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface BannerImgObject : NSObject
@property(nonatomic,strong)NSString *bannerLargeImage;
@property(nonatomic,strong)NSString *bannerMediumImage;
@property(nonatomic,strong)NSString *bannerSmallImage;
@property(nonatomic,strong)NSString *mainBannerImgUrl;
@property(nonatomic,strong)NSString *bannerName;
@property(nonatomic,strong)NSString *redirectUrl;
-(instancetype)initWithImgArray:(NSDictionary *)arrayObj;
@end

NS_ASSUME_NONNULL_END
