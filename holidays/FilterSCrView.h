//
//  FilterSCrView.h
//  holidays
//
//  Created by Kush_Team on 24/06/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@protocol FilterSCrViewDelegate <NSObject>
-(void)applySelectedNewFilterInSRP;
@end
@interface FilterSCrView : UIViewController
@property (nonatomic, weak) id <FilterSCrViewDelegate> filterDelgate;
@property (weak, nonatomic) IBOutlet UITableView *tableView_Filter;
@property(strong,nonatomic)NSMutableArray *holidayArrayInNewFilter;
@property (strong,nonatomic) NSMutableDictionary *mainNewFilterDict;
@end

NS_ASSUME_NONNULL_END
