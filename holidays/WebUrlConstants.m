//
//  WebUrlConstants.m
//  holidays
//
//  Created by Kush_Tech on 03/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "WebUrlConstants.h"

NSString *const kForexWebUrl = @"https://www.thomascook.in/foreign-exchange/";
NSString *const kVisaWebUrl    = @"https://www.thomascook.in/visa";
NSString *const kFlightsWebUrl = @"https://www.thomascook.in/flights";
NSString *const kInsuranceWebUrl = @"https://www.thomascook.in/travel-insurance";
NSString *const kCruiseWebUrl = @"https://www.thomascook.in/holidays/cruise-tour-packages";
NSString *const kHotelsWebUrl = @"https://www.thomascook.in/hotels";
NSString *const kGiftCardsWebUrl = @"https://www.thomascook.in/gift-cards";

// Coredata Classes Names

NSString *const standardPackage = @"STANDARD";
NSString *const deluxePackage = @"DELUXE";
NSString *const premiumPackage = @"PREMIUM";


NSString *const sTourName = @"TOUR NAME";
NSString *const sFlights = @"FLIGHTS";
NSString *const sTransfer = @"TRANSFER";
NSString *const sHotels = @"HOTELS";
NSString *const sSightSeeing = @"SIGHTSEEING";
NSString *const sMeals = @"MEALS";
NSString *const sItininery = @"ITINENARY";
NSString *const sInclusion = @"INCLUSION";
NSString *const sExclusion  = @"EXCLUSION";
NSString *const sVisaTransferInsurance  = @"VISA,PASSPORT & INSURANCE";


// Coredata Classes Names

NSString *const RecentViewHoliday = @"RecentViewHoliday";
NSString *const WishListHoliday = @"WishListHoliday";
NSString *const CompareListHoliday = @"CompareListHoliday";



// Userdefaults Value


NSString *const filterOptionsDict = @"filterOptionsDict";
NSString *const filterDepartureCity = @"filterDepartureCity";
NSString *const filterTheme = @"filterTheme";
NSString *const filterPackageType = @"filterPackageType";
NSString *const filterMinBudget = @"filterMinBudget";
NSString *const filterMaxBudget = @"filterMaxBudget";
NSString *const filterDuration = @"filterDuration";
NSString *const filterTravelMonth = @"filterTravelMonth";
NSString *const filterByGroup = @"filterByGroup";
NSString *const filterByStay = @"filterByStay";
NSString *const filterByCustomized = @"filterByCustomized";
NSString *const filterByAirInclusive = @"filterByAirInclusive";


// Apple Sign In Userdefaults Value


NSString *const kAppleSignInDetailsOfDevice = @"currentDeviceAppleSignInDetails" ;
