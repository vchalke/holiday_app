//
//  NewNotificationVC.h
//  holidays
//
//  Created by Kush_Team on 01/06/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "SSKeychain.h"
#import "MobiculeUtilityManager.h"
#import "CarNotificationouselTableViewCell.h"
#import "BottomView.h"
#import "BottomStacksView.h"
#import "NewMasterVC.h"
#import "LandingScreenVC.h"
NS_ASSUME_NONNULL_BEGIN

@interface NewNotificationVC : LandingScreenVC<SlideNavigationControllerDelegate,MyTableCellProtocoll>
//@property (weak, nonatomic) IBOutlet BottomView *botton_views;
@property (weak, nonatomic) IBOutlet BottomStacksView *botton_views;
@property (strong, nonatomic) IBOutlet UIView *viewNotification;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic) NSInteger count;
-(void)fetchPackageDetails:(NSString *)packageID;
-(void)searchDestinationWithDestinationName:(NSDictionary *)destination;
@end

NS_ASSUME_NONNULL_END
