//
//  HolidayLandingScreenVC.m
//  holidays
//
//  Created by Kush_Tech on 12/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//DEFAULT_COLOUR_CODE_WHITE

#import "HolidayLandingScreenVC.h"
#import "HeaderTwoLineTableViewCell.h"
#import "Holiday.h"
#import "SRPScreenVC.h"
#import "gridViewTableViewCell.h"
#import "IntroAdventureTableViewCell.h"
#import "categoriesTblCell.h"
#import "UpcomingeventtblCell.h"
#import "AdvertiseTTableViewCell.h"
#import "PopularDestinationCell.h"
#import "SpecialHolidayTableViewCell.h"
#import "HolidayLandingSearchVC.h"
#import "TopWebVC.h"

@interface HolidayLandingScreenVC ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,categoriesTblCellDelegaete,UpcomingeventtblCellDelegate,gridViewTableViewCellDelegate,SpecialHolidayTableViewCellDelegaete,IntroAdventureTableViewCellDelegaete,PopularDestinationCellDelegaete>

@end

@implementation HolidayLandingScreenVC

- (void)viewDidLoad {
//    [super viewDidLoad];
    self.navigationController.navigationBar.hidden = YES;
    [self.table_views registerNib:[UINib nibWithNibName:@"HeaderTwoLineTableViewCell" bundle:nil] forCellReuseIdentifier:@"HeaderTwoLineTableViewCell"];
    [self.table_views registerNib:[UINib nibWithNibName:@"gridViewTableViewCell" bundle:nil] forCellReuseIdentifier:@"gridViewTableViewCell"];
    [self.table_views registerNib:[UINib nibWithNibName:@"IntroAdventureTableViewCell" bundle:nil] forCellReuseIdentifier:@"IntroAdventureTableViewCell"];
    [self.table_views registerNib:[UINib nibWithNibName:@"categoriesTblCell" bundle:nil] forCellReuseIdentifier:@"categoriesTblCell"];
    [self.table_views registerNib:[UINib nibWithNibName:@"UpcomingeventtblCell" bundle:nil] forCellReuseIdentifier:@"UpcomingeventtblCell"];
    [self.table_views registerNib:[UINib nibWithNibName:@"AdvertiseTTableViewCell" bundle:nil] forCellReuseIdentifier:@"AdvertiseTTableViewCell"];
    [self.table_views registerNib:[UINib nibWithNibName:@"SpecialHolidayTableViewCell" bundle:nil] forCellReuseIdentifier:@"SpecialHolidayTableViewCell"];
     [self.table_views registerNib:[UINib nibWithNibName:@"PopularDestinationCell" bundle:nil] forCellReuseIdentifier:@"PopularDestinationCell"];
}

- (void)viewWillAppear:(BOOL)animated{
    
}
- (void)viewDidAppear:(BOOL)animated{
}

#pragma mark - TableView Delegate and DataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 7;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0){
        gridViewTableViewCell *cell = (gridViewTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"gridViewTableViewCell"];
        cell.dataArray = self.summerArray;
        cell.delegate = self;
        if ([self.summerArray count]>0){
            WhatsNewObject *object = [self.summerArray objectAtIndex:0];
            [cell setupCollectionViewTitle:object ? object.bannerLevelName : @"" subTitle:@""];
        }
        [cell setButtonDataUsingArray:self.holidayOneArray];
        return cell;
    }else if (indexPath.row == 1){
        IntroAdventureTableViewCell *cell = (IntroAdventureTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"IntroAdventureTableViewCell"];
        cell.dataArray = self.adventureArray;
        cell.delegate = self;
        if ([self.adventureArray count]>0){
            WhatsNewObject *object = [self.adventureArray objectAtIndex:0];
            [cell setupCollectionViewTitle:object ? object.bannerLevelName : @""  subTitle:@""];
        }
        return cell;
        
    }else if (indexPath.row == 2){
        AdvertiseTTableViewCell *cell = (AdvertiseTTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"AdvertiseTTableViewCell"];
        cell.dataArray = self.bankOffersArray;
        [cell setupCollectionViewTitle:@""  subTitle:@""];
        return cell;
    }else if (indexPath.row == 3){
        categoriesTblCell *cell = (categoriesTblCell*)[tableView dequeueReusableCellWithIdentifier:@"categoriesTblCell"];
        cell.categoryArray = self.categoryArray;
        if ([self.categoryArray count]>0){
            WhatsNewObject *object = [self.categoryArray objectAtIndex:0];
            [cell setupCollectionViewTitle:object ? object.bannerLevelName : @""  subTitle:@""];
        }
        cell.categoryDelegate =self;
        return cell;
    }else if (indexPath.row == 4){
        UpcomingeventtblCell *cell = (UpcomingeventtblCell*)[tableView dequeueReusableCellWithIdentifier:@"UpcomingeventtblCell"];
        cell.delegate = self;
        cell.upcomingDataArray = self.upcomingArray;
        if ([self.upcomingArray count]>0){
            WhatsNewObject *object = [self.upcomingArray objectAtIndex:0];
            [cell setupCollectionViewTitle:object ? object.bannerLevelName : @""  subTitle:@""];
        }
        return cell;
    }else if (indexPath.row == 5){
        SpecialHolidayTableViewCell *cell = (SpecialHolidayTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"SpecialHolidayTableViewCell"];
        cell.cherryBlossomArray = self.cheeryBlossomArray;
        cell.delegate = self;
        if ([self.cheeryBlossomArray count]>0){
            WhatsNewObject *object = [self.cheeryBlossomArray objectAtIndex:0];
            [cell setupCollectionViewTitle:object ? object.bannerLevelName : @""  subTitle:@""];
        }
        [cell setButtonDataUsingArray:self.holidayOneArray];
        return cell;
    } else if (indexPath.row == 6){
        PopularDestinationCell *cell = (PopularDestinationCell*)[tableView dequeueReusableCellWithIdentifier:@"PopularDestinationCell"];
        cell.popularDestArray = self.popularDestinationArray;
        if ([self.popularDestinationArray count]>0){
            WhatsNewObject *object = [self.popularDestinationArray objectAtIndex:0];
            [cell setupCollectionViewTitle:object ? object.bannerLevelName : @""  subTitle:@""];
        }
        cell.delegate = self;
        return cell;
    }
    gridViewTableViewCell *cell = (gridViewTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"gridViewTableViewCell"];
    cell.dataArray = self.popularDestinationArray;
    cell.delegate = self;
    [cell setupCollectionViewTitle:@""  subTitle:@""];
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"Top Banner Array Count %lu",(unsigned long)[self.summerArray count]);
    int rowCount = ([self.summerArray count]%2 == 0) ? (int)[self.summerArray count]/2 : (int)[self.summerArray count]/2+1 ;
    if (indexPath.row == 0){
        return rowCount*330;//660;
    }else if (indexPath.row == 1){
        return 350;
    }else if (indexPath.row == 2 && [self.bankOffersArray count] > 0){
        return 100;
    }else if (indexPath.row == 3){
        return 300;
    }else if (indexPath.row == 4){
       return 400;
    }else if (indexPath.row == 5){
        return 360;
    }else if (indexPath.row == 6){
        return 280;
    }
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0;
}

-(void)jumpToHolidaySeacrhVC{
    HolidayLandingSearchVC *controller = [[HolidayLandingSearchVC alloc] initWithNibName:@"HolidayLandingSearchVC" bundle:nil];
    controller.boolForSearch_PackgScreen=true;
    controller.header_Name = kIndianHoliday;
    [self.navigationController pushViewController:controller animated:YES];
}
-(void)jumpToTheWebView:(NSString *)webUrlString withTitle:(NSString *)webViewTitle withObject:(WhatsNewObject *)newObj{
    if (newObj){
        if ([[newObj.bannerType uppercaseString] isEqualToString:@"PACKAGE"]){
            Holiday *objHoliday=[[Holiday alloc]init];
            objHoliday.strPackageId=newObj.packageID;
            objHoliday.strPackageName=newObj.bannerName;
            [self jumpToPdpFromLandingScreen:objHoliday];
        }else if ([[newObj.bannerType uppercaseString] isEqualToString:@"DESTINATION"]){
            [self jumpToDestinationWiseDictObjcet:newObj];
        }else if ([[newObj.bannerType uppercaseString] isEqualToString:@"WEB"] && ![[webUrlString uppercaseString] isEqualToString:@"NA"]){
            [self jumpToWebViewFromHolidayLandingwithTitle:newObj.bannerName withUrl:newObj.redirectUrl];
        }
    }else if (![[webUrlString uppercaseString] isEqualToString:@"NA"]){
        
    }else{
        
    }
}
-(void)jumpToWebViewFromHolidayLandingwithTitle:(NSString*)webtitle withUrl:(NSString*)weburl{
    TopWebVC *controler = [[TopWebVC alloc] initWithNibName:@"TopWebVC" bundle:nil];
    controler.web_url_string = weburl;
    controler.web_title = webtitle;
    [self.navigationController pushViewController:controler animated:YES];
}
- (void)jumpToTheWebViewFromSummer:(NSString *)webUrlString withTitle:(NSString *)webViewTitle withObject:(WhatsNewObject *)newObject{
     [self jumpToTheWebView:webUrlString withTitle:webViewTitle withObject:newObject];
}
- (void)jumpToTheWebViewFromUpcoming:(NSString *)webUrlString withTitle:(NSString *)webViewTitle withObject:(nonnull WhatsNewObject *)newObject{
    [self jumpToTheWebView:webUrlString withTitle:webViewTitle withObject:newObject];
}
- (void)jumpToTheWebViewFromAdventure:(NSString *)webUrlString withTitle:(NSString *)webViewTitle withObject:(WhatsNewObject *)newObject{
    [self jumpToTheWebView:webUrlString withTitle:webViewTitle withObject:newObject];
}
- (void)jumpToTheWebViewFromCherryBlossom:(NSString *)webUrlString withTitle:(NSString *)webViewTitle withObject:(WhatsNewObject *)newObject{
    [self jumpToTheWebView:webUrlString withTitle:webViewTitle withObject:newObject];
}
- (void)jumpToTheWebViewFromPopularDestination:(NSString *)webUrlString withTitle:(NSString *)webViewTitle withObject:(WhatsNewObject *)newObject{
    [self jumpToTheWebView:webUrlString withTitle:webViewTitle withObject:newObject];
}
#pragma mark - Delegates Of gridViewTableViewCell
-(void)jumpToSummerHolidayWithTag:(NSInteger)tagValue{
    NSDictionary *dicts = @{@"searchString":@"Thailand",
                            @"searchType":@"COUNTRY",
                            @"cityName":@"",
                            @"continentName":@"Asia",
                            @"countryCode":@"TH",
                            @"countryName":@"Thailand",
                            @"searchLatitude":@(0),
                            @"searchLongitude":@(0),
                            @"stateCode":@"",
                            @"stateName":@"",
    };
    [self searchDestinationSelectDict:dicts withText:@"All Thailand Holidays"];
}
#pragma mark - Delegates Of SpecialHolidayTableViewCell
-(void)jumpToParticularHoliday:(NSInteger)tagValue{
    // jump to japan // jump to India Holiday on 03Aug20
    NSDictionary *dicts = @{@"searchString":@"India",
                            @"searchType":@"COUNTRY",
                            @"cityName":@"",
                            @"continentName":@"Asia",
                            @"countryCode":@"IN",
                            @"countryName":@"India",
                            @"searchLatitude":@(0),
                            @"searchLongitude":@(0),
                            @"stateCode":@"",
                            @"stateName":@"",
    };
    [self searchDestinationSelectDict:dicts withText:@"All India Holidays"];
}
#pragma mark - Button Actions
-(void)selectedCategory:(NSString *)categoryString andSearchText:(nonnull NSString *)searchStr withObejct:(nonnull WhatsNewObject *)objcet{
    /*
    NSDictionary *dicts = @{@"searchString":categoryString,
                            @"searchType":@"THEME",
                            @"cityName":@"",
                            @"continentName":@"",
                            @"countryCode":@"",
                            @"countryName":@"",
                            @"searchLatitude":@(0),
                            @"searchLongitude":@(0),
                            @"stateCode":@"",
                            @"stateName":@"",
    };
    [self searchDestinationSelectDict:dicts withText:searchStr];
    */
    [self jumpToDestinationWiseDictObjcet:objcet];
}

#pragma mark - Button Actions
- (IBAction)btn_backPress:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)btn_searchPress:(id)sender {
    [self jumpToHolidaySeacrhVC];
}
#pragma mark - TextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if ([textField canResignFirstResponder]) {
        [textField resignFirstResponder];
    }
    return YES;
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return YES;
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField{

}

#pragma mark - Calling searchDestination API

-(void)searchDestinationSelectDict:(NSDictionary*)selecteDestinationDict withText:(NSString*)searchText{
    
    NSString *destinationStr;
    NSString *stringBudgetPerson;
    NSString *stringNoOfNights;
    NSString *monthIndex;
    
    LoadingView *activityIndicatorInLSVC = [LoadingView loadingViewInView:self.view.superview.superview
                                              withString:@""
                                       andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE_WHITE]];
  
    NSMutableString *queryParam = [[NSMutableString alloc] init] ;

    NSString *searchType  = [[selecteDestinationDict valueForKey:@"searchType"] uppercaseString];
    destinationStr = [NSString stringWithFormat:@"%@",[selecteDestinationDict objectForKey:@"countryName"]];
    if (searchText.length !=0 )
    {
    [queryParam appendFormat:@"searchType=%@",searchType];
    
    
    if ([[searchType uppercaseString] isEqualToString:@"COUNTRY"])
    {
        /*
         searchType=COUNTRY
         destination=IN
         */
       NSString *countryCode = [[selecteDestinationDict valueForKey:@"countryCode"] uppercaseString];
        [queryParam appendFormat:@"&destination=%@",countryCode];
    }
    else if ([[searchType uppercaseString] isEqualToString:@"STATE"])
    {
        /*
         searchType=STATE
         destination=MH
         countryCode=IN
         */
        
        NSString *stateCode = [[selecteDestinationDict valueForKey:@"stateCode"] uppercaseString];
        NSString *countryCode = [[selecteDestinationDict valueForKey:@"countryCode"] uppercaseString];
        
        [queryParam appendFormat:@"&destination=%@",stateCode];
        [queryParam appendFormat:@"&countryCode=%@",countryCode];
        
    }
    else if ([[searchType uppercaseString] isEqualToString:@"CITY"])
    {
        /*
         searchType=CITY
         destination=BOM
         countryCode=IN
         stateCode=MH
         */
        
        NSString *cityCode = [[selecteDestinationDict valueForKey:@"locationCode"] uppercaseString];
        NSString *stateCode = [[selecteDestinationDict valueForKey:@"stateCode"] uppercaseString];
        NSString *countryCode = [[selecteDestinationDict valueForKey:@"countryCode"] uppercaseString];
        
        
        [queryParam appendFormat:@"&destination=%@",cityCode];
        [queryParam appendFormat:@"&countryCode=%@",countryCode];
        [queryParam appendFormat:@"&stateCode=%@",stateCode];

    }
    else if ([[searchType uppercaseString] isEqualToString:@"CONTINENT"])
    {
        /*
         searchType=CONTINENT
         destination=asia
         */
//         [queryParam appendFormat:@"&destination=%@",destinationStr];
        [queryParam appendFormat:@"&destination=%@",searchText]; // search as per Service API New One
    }
    else if ([[searchType uppercaseString] isEqualToString:@"THEME"])  // Done by komal 10 Aug 2018
    {
        /*
         "searchString":"Beach",
         "searchType":"THEME",
         */
        NSString *searchString  = [[selecteDestinationDict valueForKey:@"searchString"] uppercaseString];
        [queryParam appendFormat:@"&theme=%@",searchString];
        // To show All Holiday Acc to New PSD We comment This Code On 31_July_20
        /*
         if ([headerName isEqualToString:kIndianHoliday])
         {
             [queryParam appendFormat:@"&pkgType=0"];
         }
         else
         {
             [queryParam appendFormat:@"&pkgType=1"];
         }
         */
    }
    else
    {
         [queryParam appendFormat:@"&destination=%@",destinationStr];
    }
    }
    NetworkHelper *helper = [NetworkHelper sharedHelper];
    
    
    //stringBudgetPerson
    
    NSString *budget1 = @"";
    NSString *budget2 = @"";
    
    if ([stringBudgetPerson isEqualToString:@"250000"])
    {
        budget1 = @"250000";
    }
    else
    {
        NSArray *budgetArray = [stringBudgetPerson componentsSeparatedByString:@"-"];
        
        if (budgetArray)
        {
            if(budgetArray.count == 2)
            {
                budget1 = [budgetArray firstObject];
                budget2 = budgetArray[1];
            }
        }
    }
    
    
    if ([budget1 isEqualToString:@""] && [budget2 isEqualToString:@""])
    {
       // queryParam = [NSMutableString stringWithFormat:@"searchType=%@&destination=%@",searchType,destinationStr];
    }
    else if ([budget1 isEqualToString:@""])
    {
        [queryParam appendFormat:@"&budget2=%@",budget2];
    }
    else if ([budget2 isEqualToString:@""])
    {
         [queryParam appendFormat:@"&budget1=%@",budget1];
    }
    else
    {
         [queryParam appendFormat:@"&budget1=%@&budget2=%@",budget1,budget2];
    }

    
    
    if (monthIndex != 0)
    {
        [queryParam appendString:[NSString stringWithFormat:@"&monthOfTravel=%@",monthIndex]];
    }
    
    @try
    {
        if ([queryParam characterAtIndex:0] == '&')
        {
            queryParam = [[queryParam substringFromIndex:1] mutableCopy];
        }
        NSString *stringWithDecode  = [queryParam stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];

        
        NSDictionary *headerDict = [CoreUtility getHeaderDict];
        
        [helper getResponseWithRequestType:@"" withQueryParam:stringWithDecode withPathParam:@"" withJsonParam:nil withHeaders:headerDict withUrl:kAstraUrlPackageFilterSearch success:^(NSDictionary *responseDict)
         {
             dispatch_async(dispatch_get_main_queue(), ^(void)
                            {
                                [activityIndicatorInLSVC removeFromSuperview];
                                NSLog(@"Response Dict : %@",responseDict);
                                
                                if (responseDict)
                                {
                                    if (responseDict.count>0)
                                    {
                                        NSMutableArray *arrayOfHolidays = [[NSMutableArray alloc] init];
                                        NSArray *packageArray = (NSArray *)responseDict;
                                        
                                        for (NSDictionary *dictOfData in packageArray)
                                        {
                                            NSDictionary *packageDatailDict =[dictOfData valueForKey:@"packageDetail"];
                                            
                                            long productId = [[packageDatailDict valueForKey:@"productId"] longValue];
                                            // Unbloacked on 01/09/20
//                                            if (productId != 11) {
                                                Holiday *nwHoliday  = [[Holiday alloc]initWithDataDict:dictOfData];
                                                if (![nwHoliday isEqual:nil]) {
                                                    [arrayOfHolidays addObject:nwHoliday];
                                                }
//                                            }else{
//                                                NSLog(@"Product blocked");
//                                            }
                                        }
                                        NSString *header = @"";
                                        if (destinationStr == nil) {
                                            header = @"Holidays";
                                        }
                                        else
                                        {
                                            header = [NSString stringWithFormat:@"%@ %@",destinationStr, @"Holidays"];
                                        }
                                        SRPScreenVC *srpScreenVC=[[SRPScreenVC alloc]initWithNibName:@"SRPScreenVC" bundle:nil];
                                        srpScreenVC.headerName=header;
                                        srpScreenVC.searchedCity = destinationStr;
                                        srpScreenVC.holidayArrayInSrp = arrayOfHolidays;
                                        srpScreenVC.totalPackages=[NSString stringWithFormat:@"%lu",(unsigned long)[arrayOfHolidays count]];
                                        srpScreenVC.completePackageDetail = packageArray;
                                        [self.navigationController pushViewController:srpScreenVC animated:YES];
                                       
                                    }
                                    else
                                    {
                                        [super showAlertViewWithTitle:@"error" withMessage:@"Sorry, we could not find any package for the given search. Please try with a different search criteria."];
                                        NSLog(@" response Date Search packageDetailsMessage "  );
                                        
                                    }
                                    
                                }
                                
                            });
         }
                                   failure:^(NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^(void)
                            {
                                [activityIndicatorInLSVC removeFromSuperview];
                                [super showAlertViewWithTitle:@"Alert" withMessage:[error description]];
                                NSLog(@"Response Dict : %@",[error description]);
                            });
             
         }];
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"%@", exception.reason);
        [activityIndicatorInLSVC removeFromSuperview];
    }
    @finally
    {
        NSLog(@"exception finally called");
    }
    
    
    //    NSString *strDuration     = self.txtNights.text;
    //    NSString *strBudget      = self.txtBudget.text;
    
    
    
    if (stringBudgetPerson == nil)
    {
        stringBudgetPerson = @"";
    }
    
    if (stringNoOfNights == nil)
    {
        stringNoOfNights = @"";
    }
    
    
}
-(void)searchDestinationWithDict:(NSDictionary*)selecteDestinationDict withText:(NSString*)searchText completionHandler:(void (^)(NSString * _Nullable header,NSString * _Nullable searchedCity, NSArray * _Nullable arrayOfHolidays, NSError * _Nullable error))handlers{
    
    NSString *destinationStr;
    NSString *stringBudgetPerson;
    NSString *stringNoOfNights;
    NSString *monthIndex;
    LoadingView *activityIndicatorInLSVC = [LoadingView loadingViewInView:self.view.superview.superview
                                              withString:@""
                                       andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE_WHITE]];
  
    NSMutableString *queryParam = [[NSMutableString alloc] init] ;

    NSString *searchType  = [[selecteDestinationDict valueForKey:@"searchType"] uppercaseString];
    
    if (searchText.length !=0 )
    {
    [queryParam appendFormat:@"searchType=%@",searchType];
    
    
    if ([[searchType uppercaseString] isEqualToString:@"COUNTRY"])
    {
        /*
         searchType=COUNTRY
         destination=IN
         */
       NSString *countryCode = [[selecteDestinationDict valueForKey:@"countryCode"] uppercaseString];
        [queryParam appendFormat:@"&destination=%@",countryCode];
    }
    else if ([[searchType uppercaseString] isEqualToString:@"STATE"])
    {
        /*
         searchType=STATE
         destination=MH
         countryCode=IN
         */
        
        NSString *stateCode = [[selecteDestinationDict valueForKey:@"stateCode"] uppercaseString];
        NSString *countryCode = [[selecteDestinationDict valueForKey:@"countryCode"] uppercaseString];
        
        [queryParam appendFormat:@"&destination=%@",stateCode];
        [queryParam appendFormat:@"&countryCode=%@",countryCode];
        
    }
    else if ([[searchType uppercaseString] isEqualToString:@"CITY"])
    {
        /*
         searchType=CITY
         destination=BOM
         countryCode=IN
         stateCode=MH
         */
        
        NSString *cityCode = [[selecteDestinationDict valueForKey:@"locationCode"] uppercaseString];
        NSString *stateCode = [[selecteDestinationDict valueForKey:@"stateCode"] uppercaseString];
        NSString *countryCode = [[selecteDestinationDict valueForKey:@"countryCode"] uppercaseString];
        
        
        [queryParam appendFormat:@"&destination=%@",cityCode];
        [queryParam appendFormat:@"&countryCode=%@",countryCode];
        [queryParam appendFormat:@"&stateCode=%@",stateCode];

    }
    else if ([[searchType uppercaseString] isEqualToString:@"CONTINENT"])
    {
        /*
         searchType=CONTINENT
         destination=asia
         */
         [queryParam appendFormat:@"&destination=%@",destinationStr];
    }
    else if ([[searchType uppercaseString] isEqualToString:@"THEME"])  // Done by komal 10 Aug 2018
    {
        /*
         "searchString":"Beach",
         "searchType":"THEME",
         */
        NSString *searchString  = [[selecteDestinationDict valueForKey:@"searchString"] uppercaseString];
        [queryParam appendFormat:@"&theme=%@",searchString];
        // To show All Holiday Acc to New PSD We comment This Code On 31_July_20
        /*
         if ([headerName isEqualToString:kIndianHoliday])
         {
             [queryParam appendFormat:@"&pkgType=0"];
         }
         else
         {
             [queryParam appendFormat:@"&pkgType=1"];
         }
         */
    }
    else
    {
         [queryParam appendFormat:@"&destination=%@",destinationStr];
    }
    }
    NetworkHelper *helper = [NetworkHelper sharedHelper];
    
    
    //stringBudgetPerson
    
    NSString *budget1 = @"";
    NSString *budget2 = @"";
    
    if ([stringBudgetPerson isEqualToString:@"250000"])
    {
        budget1 = @"250000";
    }
    else
    {
        NSArray *budgetArray = [stringBudgetPerson componentsSeparatedByString:@"-"];
        
        if (budgetArray)
        {
            if(budgetArray.count == 2)
            {
                budget1 = [budgetArray firstObject];
                budget2 = budgetArray[1];
            }
        }
    }
    
    
    if ([budget1 isEqualToString:@""] && [budget2 isEqualToString:@""])
    {
       // queryParam = [NSMutableString stringWithFormat:@"searchType=%@&destination=%@",searchType,destinationStr];
    }
    else if ([budget1 isEqualToString:@""])
    {
        [queryParam appendFormat:@"&budget2=%@",budget2];
    }
    else if ([budget2 isEqualToString:@""])
    {
         [queryParam appendFormat:@"&budget1=%@",budget1];
    }
    else
    {
         [queryParam appendFormat:@"&budget1=%@&budget2=%@",budget1,budget2];
    }

    
    
    if (monthIndex != 0)
    {
        [queryParam appendString:[NSString stringWithFormat:@"&monthOfTravel=%@",monthIndex]];
    }
    
    @try
    {
        if ([queryParam characterAtIndex:0] == '&')
        {
            queryParam = [[queryParam substringFromIndex:1] mutableCopy];
        }
        NSString *stringWithDecode  = [queryParam stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];

        
        NSDictionary *headerDict = [CoreUtility getHeaderDict];
        
        [helper getResponseWithRequestType:@"" withQueryParam:stringWithDecode withPathParam:@"" withJsonParam:nil withHeaders:headerDict withUrl:kAstraUrlPackageFilterSearch success:^(NSDictionary *responseDict)
         {
             dispatch_async(dispatch_get_main_queue(), ^(void)
                            {
                                [activityIndicatorInLSVC removeFromSuperview];
                                NSLog(@"Response Dict : %@",responseDict);
                                
                                if (responseDict)
                                {
                                    if (responseDict.count>0)
                                    {
                                        NSMutableArray *arrayOfHolidays = [[NSMutableArray alloc] init];
                                        NSArray *packageArray = (NSArray *)responseDict;
                                        
                                        for (NSDictionary *dictOfData in packageArray)
                                        {
                                            NSDictionary *packageDatailDict =[dictOfData valueForKey:@"packageDetail"];
                                            
                                            long productId = [[packageDatailDict valueForKey:@"productId"] longValue];
                                            // Unbloacked on 01/09/20
//                                            if (productId != 11) {
                                                Holiday *nwHoliday  = [[Holiday alloc]initWithDataDict:dictOfData];
                                                if (![nwHoliday isEqual:nil]) {
                                                    [arrayOfHolidays addObject:nwHoliday];
                                                }
//                                            }else{
//                                                NSLog(@"Product blocked");
//                                            }
                                        }
                                        NSString *header = @"";
                                        if (destinationStr == nil) {
                                            header = @"Holidays";
                                        }
                                        else
                                        {
                                            header = [NSString stringWithFormat:@"%@ %@",destinationStr, @"Holidays"];
                                        }
                                        SRPScreenVC *srpScreenVC=[[SRPScreenVC alloc]initWithNibName:@"SRPScreenVC" bundle:nil];
                                        srpScreenVC.headerName=header;
                                        srpScreenVC.searchedCity = destinationStr;
                                        srpScreenVC.holidayArrayInSrp = arrayOfHolidays;
                                        srpScreenVC.totalPackages=[NSString stringWithFormat:@"%lu",(unsigned long)[arrayOfHolidays count]];
                                        srpScreenVC.completePackageDetail = packageArray;
                                        [self.navigationController pushViewController:srpScreenVC animated:YES];
                                       
                                    }
                                    else
                                    {
                                        [super showAlertViewWithTitle:@"error" withMessage:@"Sorry, we could not find any package for the given search. Please try with a different search criteria."];
                                        NSLog(@" response Date Search packageDetailsMessage "  );
                                        
                                    }
                                    
                                }
                                
                            });
         }
                                   failure:^(NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^(void)
                            {
                                [activityIndicatorInLSVC removeFromSuperview];
                                [super showAlertViewWithTitle:@"Alert" withMessage:[error description]];
                                NSLog(@"Response Dict : %@",[error description]);
                            });
             
         }];
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"%@", exception.reason);
        [activityIndicatorInLSVC removeFromSuperview];
    }
    @finally
    {
        NSLog(@"exception finally called");
    }
    
    
    //    NSString *strDuration     = self.txtNights.text;
    //    NSString *strBudget      = self.txtBudget.text;
    
    
    
    if (stringBudgetPerson == nil)
    {
        stringBudgetPerson = @"";
    }
    
    if (stringNoOfNights == nil)
    {
        stringNoOfNights = @"";
    }
    
    
}



//completionHandler:(void (^)(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error))completionHandler
@end

