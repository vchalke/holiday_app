//
//  PDPCalculatePrizeVC.h
//  Thomoscook_Holiday
//
//  Created by Kush_Tech on 19/02/20.
//  Copyright © 2020 Kush_Tech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HolidayPackageDetail.h"
#import "NewMasterVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface PDPCalculatePrizeVC : NewMasterVC
//@property (weak, nonatomic) IBOutlet UIView *departureCity_View;
//@property (weak, nonatomic) IBOutlet UIView *numOfTraveller_View;
@property (strong ,nonatomic) HolidayPackageDetail *holidayPkgDetailInPdpCalculate;
@property (weak, nonatomic) IBOutlet UIButton *btn_policyTerms;
@property (weak, nonatomic) IBOutlet UIButton *btn_PrivactPolicy;
@property (weak, nonatomic) IBOutlet UILabel *lbl_numofRoom;
@property (weak, nonatomic) IBOutlet UIButton *btn_termsCondition;
@property (weak, nonatomic) IBOutlet UITextField *txt_DepartureCity;
@property (weak, nonatomic) IBOutlet UITextField *txt_numOfTraveller;
@property (weak, nonatomic) IBOutlet UITextField *txt_DepartureDate;
@property (weak, nonatomic) IBOutlet UITextField *txt_MobileDate;
@property (weak, nonatomic) IBOutlet UIButton *btn_Check;

@end

NS_ASSUME_NONNULL_END
