//
//  BankOffersObject.m
//  holidays
//
//  Created by Kush_Team on 13/06/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "BankOffersObject.h"

@implementation BankOffersObject

-(instancetype)initWithBankDataArray:(NSDictionary *)arrayObject{
   if ([super init]){
       self.holidayoffername = [arrayObject valueForKey:@"holidayoffername"];
       self.holidayoffertodate = [arrayObject valueForKey:@"holidayoffertodate"];
       self.holidayoffferfromdate = [arrayObject valueForKey:@"link"];
       self.holidayofferid = [[arrayObject valueForKey:@"holidayoffferfromdate"]integerValue];
       self.imagepath = [arrayObject valueForKey:@"imagepath"];
       self.isActive = [arrayObject valueForKey:@"isActive"];
   }
    return self;
}
@end
