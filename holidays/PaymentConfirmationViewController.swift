//
//  PaymentConfirmationViewController.swift
//  sotc-consumer-application
//
//  Created by Parshwanath on 22/01/18.
//  Copyright © 2018 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit


struct PaymentDetails {
    
    var displayTitle:String?
    var displaTitleValue:String?
    var subtitle:String?
    
    init() {
        
    }
    
    init(displayTitle:String,displaTitleValue:String) {
        
        self.displayTitle = displayTitle
        self.displaTitleValue = displaTitleValue
    }
    
    
}

class PaymentConfirmationViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    
     @IBOutlet weak var paymentConfirmationDetailsTableView: UITableView!
    
    var paymentonfirmationListArray:Array<PaymentDetails>?
    
    var reciptStatus:Bool?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.paymentConfirmationDetailsTableView.register(UITableViewCell.self, forCellReuseIdentifier: "paymentConfirmation")
        
        self.paymentConfirmationDetailsTableView.tableFooterView = UIView(frame: .zero)
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.setRedNavigationBar()
        self.navigationController?.setnavigatiobBarTitle(title: Title.PYMENT_CONFIRMATION, viewController: self)
       // self.navigationController?.setEmergencyNavigationRightButton(showEmergencyButton: true, viewController: self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return paymentonfirmationListArray!.count
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 0
        {
           return 50
            
        }else
        {
            return 35
            
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "paymentConfirmation", for: indexPath) as? UITableViewCell
            else {
                
                // fatalError("The dequeued cell is not an instance of MealTableViewCell.")
        }
        
        let tourDateLabelAttr = [convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor): UIColor.black, convertFromNSAttributedStringKey(NSAttributedString.Key.font): cell.textLabel?.font] as [String : Any]
        
        let fisrtLetterAttr = [convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor): UIColor.lightGray, convertFromNSAttributedStringKey(NSAttributedString.Key.font): cell.textLabel?.font] as [String : Any]
        
        
       
        
        cell.textLabel? .font = UIFont(name:"Roboto-Light", size:15)
        
        if let document = paymentonfirmationListArray?[indexPath.row] {
            
            let tourPartOne = NSMutableAttributedString(string: document.displayTitle ?? "", attributes: convertToOptionalNSAttributedStringKeyDictionary(fisrtLetterAttr))
            let tourPartTwo = NSMutableAttributedString(string: document.displaTitleValue ?? "", attributes: convertToOptionalNSAttributedStringKeyDictionary(tourDateLabelAttr))
            let tourCombination = NSMutableAttributedString()
            tourCombination.append(tourPartOne)
            tourCombination.append(tourPartTwo)
            cell.textLabel?.attributedText = tourCombination
            
           // cell.textLabel?.text = document.displayTitle
            
        }
        
      if indexPath.row == 0
      {
        cell.backgroundColor = AppUtility.hexStringToUIColor(hex: "E7E7E7")
        
        }else
      {
        cell.backgroundColor = UIColor.white
        
        }
         cell.textLabel?.textAlignment = .center
        
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
  /*      self.dismiss(animated: true, completion: {
            
            var document:DocumentDetails = self.paymentonfirmationListArray![indexPath.row]
            
            self.delegate?.selectedDocument(documnetUrl:document.documnetUrl!,downloadUrl:document.downloadUrl! )
            
            /// self.downloadDocumnet(filePath: document.documnetUrl!,url: document.downloadUrl!)
            
        })*/
        
    }
    
  /*  func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
         let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.paymentConfirmationDetailsTableView.frame.width - 20, height: 25))
        
        if self.reciptStatus ?? false
        {
            label.text = "Recipt generated successfully"
            label.textColor = UIColor.green
        }else{
        
        label.text =  "Recipt generation failed"
        label.textColor = UIColor.red
            
        }
        
        
        let view:UIView = UIView(frame: CGRect(x: 0, y: 0, width: self.paymentConfirmationDetailsTableView.frame.width, height: 30))
        
       
        
        view.addSubview(label)
        return view
        
    }
    
   
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
      //  return 30
    }*/
    
    @IBAction func onBackBtnClick(_ sender: UIButton) {
        self.dismiss(animated: true) {
            
        }
    }
    

}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromNSAttributedStringKey(_ input: NSAttributedString.Key) -> String {
	return input.rawValue
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}
