//
//  TalkToExpertVC.m
//  holidays
//
//  Created by Ios_Team on 07/09/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "TalkToExpertVC.h"
#import "RequestViewVC.h"
#import <FirebaseAnalytics/FirebaseAnalytics.h>

@interface TalkToExpertVC ()<TalkToExpertViewDelegate>

@end

@implementation TalkToExpertVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.talkToExpertView.expertDelgate = self;
    [self.talkToExpertView setDistanceFromBottom:60.0 withLeftSide:YES];
    
    
}

#pragma mark - TalkToExpertView Delegates
-(void)optionClickwithIndex:(NSInteger)indexNum{
    [self dismissViewControllerAnimated:YES completion:nil];
    if (indexNum==0 || indexNum==1){
        [self.delegate clickedOptions:indexNum];
    }else{
        NSLog(@"No any option clcik");
    }
}
@end
