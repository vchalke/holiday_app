//
//  MapTabViewController.m
//  holidays
//
//  Created by ketan on 29/09/15.
//  Copyright © 2015 Mobicule. All rights reserved.
//

#import "MapTabViewController.h"
#import "Holiday.h"
#import "TabMenuVC.h"
#import "PackageListVC.h"
#import "NetCoreAnalyticsVC.h"
#import "UIImageView+WebCache.h"
#define BANNER_IMAGE @"defaultBanner.png"

@interface MapTabViewController ()
{
    CLLocationCoordinate2D myCoordinate;
    LoadingView *loadingViewObject;
    NSMutableDictionary *payLoadForViewDetails;
    
    NSMutableArray *arrayOfImagesForCarousel;
    HolidayPackageDetail *packageDetail;
}
@property (nonatomic, strong) NSArray *items;
@end

@implementation MapTabViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.items = _arrayOfHolidays;
    
    arrayOfImagesForCarousel = [[NSMutableArray  alloc]init];
    
   // [self downloadImagesWithCompletion:^(BOOL completion){
        
    //    [_carousel reloadData];
   // }];
    
//    for (int imageIndex = 0; imageIndex < [self.items count]; imageIndex ++) {
//        
//        [arrayOfImagesForCarousel addObject:UIImagePNGRepresentation([UIImage imageNamed:BANNER_IMAGE])];
//    }
    
    payLoadForViewDetails = [[NSMutableDictionary alloc] init];
}

-(void)viewWillAppear:(BOOL)animated
{
    CoreUtility *coreObj=[CoreUtility sharedclassname];
    coreObj.pageNumber=2;
    
    
   
//    PackageListVC *filterObj=coreObj.obj;
//    filterObj.title=@"PACKAGE LISTING";
    [super viewWillAppear:animated];
    self.items = _arrayOfHolidays;
    [_carousel reloadData];
    _carousel.type = iCarouselTypeCoverFlow2;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    //[self getLocationFromAddressString:_searchedCity];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)dealloc
{
    _carousel.delegate = nil;
    _carousel.dataSource = nil;
}

-(void)downloadImagesWithCompletion:(isCompleted)completion {
    
    if ([super connected]) {
        
    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        for (int objectIndex= 0; objectIndex < [self.items count];objectIndex ++) {
            
            Holiday *objHoliday = [self.items objectAtIndex:objectIndex];
            NSString *imageUrlString  = objHoliday.packageImagePath;
            
            NSString* imageName = [imageUrlString stringByReplacingOccurrencesOfString:@" " withString:@"_"];
            
            NSURL *url = [NSURL URLWithString:imageName];
            NSData * imageData = [NSData dataWithContentsOfURL:url];
            
            if (imageData != nil)
            {
            
                [arrayOfImagesForCarousel insertObject:imageData atIndex:objectIndex];
            }
            else
            {
                
                [arrayOfImagesForCarousel insertObject:UIImagePNGRepresentation([UIImage imageNamed:BANNER_IMAGE])
                                               atIndex:objectIndex];
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                completion(YES);
            });
        }
    });
        
    }
    else
    {
        [self showAlertViewWithTitle:@"Alert" withMessage:kMessageNoInternet];

    }
    
}

#pragma mark -iCarousal delegate

- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    return [_items count];
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    Holiday *objHoliday = [_arrayOfHolidays objectAtIndex:index];
    view = [[[NSBundle mainBundle] loadNibNamed:@"carouslCell" owner:self options:nil] objectAtIndex:0];
    view.frame = CGRectMake(0, 0, carousel.frame.size.width-50, carousel.frame.size.height-20);
    UIImageView *imageView = (UIImageView *)[view viewWithTag:1];
    UILabel *titleLabel = (UILabel *)[view viewWithTag:2];
    UILabel *lblDays = (UILabel *)[view viewWithTag:3];
    UILabel *lblNights = (UILabel *)[view viewWithTag:4];
    UILabel *lblCost = (UILabel *)[view viewWithTag:5];
    
//    [UIImage imageWithData:[NSData dataWithContentsOfURL:url]];
//    NSString *imageUrlString  = objHoliday.packageImagePath;
//    NSURL *url = [NSURL URLWithString:imageUrlString];
//    [UIImage loadImageFromUrl:url callback:^(UIImage *imageToSet){
//    
//        imageView.image = imageToSet;
//    }];
    
    NSString *imageUrlString = objHoliday.packageImagePath;
    
    
    NSString* encodedText = [imageUrlString stringByReplacingOccurrencesOfString:@" " withString:@"_"];

    NSURL *urlImage = [NSURL URLWithString:encodedText];
    
    if(urlImage)
    {
        
        UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        indicator.center = imageView.center;// it will display in center of image view
        [imageView addSubview:indicator];
        [indicator startAnimating];
        
        
        [imageView sd_setImageWithURL:urlImage placeholderImage:[UIImage imageNamed:BANNER_IMAGE] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
            
            [indicator stopAnimating];
            
        }];
        
        
        
        //[cell.imageView sd_setImageWithURL:urlImage];
        // [cell.imageView setImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:urlImage]]];
        
    }

    //imageView.image = [UIImage imageWithData:[arrayOfImagesForCarousel objectAtIndex:index]];
    
    NSString *strCostPP = [NSString stringWithFormat:@"Rs. %d",objHoliday.packagePrise];
    titleLabel.text = objHoliday.strPackageName;
    lblCost.text    = strCostPP;
    lblDays.text = [NSString stringWithFormat:@"%d",objHoliday.durationNoDays];
    lblNights.text = [NSString stringWithFormat:@"%d",objHoliday.durationNoDays-1];
    
    UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapNumber:)];
    tap.numberOfTapsRequired=1;
    view.tag = index;
    [view addGestureRecognizer:tap];
    view.userInteractionEnabled = YES;
    return view;
}
-(void)tapNumber:(UITapGestureRecognizer*)tap
{
    NSLog(@" index %ld",[[tap view] tag]);
    
    Holiday *objHoliday = [_arrayOfHolidays objectAtIndex:[[tap view] tag]];
    
    activityLoadingView = [LoadingView loadingViewInView:self.view withString:@"" andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
    
    //[self performSelectorInBackground:@selector(fetchPackageDetails:) withObject:objHoliday];
    
    [self fetchPackageDetails:objHoliday];
    
}

#pragma mark -CLLocation delegate

-(CLLocationCoordinate2D) getLocationFromAddressString: (NSString*) addressStr {
    
    
    
    CLGeocoder *geocoder = [[CLGeocoder alloc]init];
    
    [geocoder geocodeAddressString:addressStr
                 completionHandler:^(NSArray* placemarks, NSError* error){
                     for (CLPlacemark* aPlacemark in placemarks)
                     {
                         MKPointAnnotation*  annotation = [[MKPointAnnotation alloc] init];
                         myCoordinate.latitude =  aPlacemark.location.coordinate.latitude;
                         myCoordinate.longitude = aPlacemark.location.coordinate.longitude;
                         annotation.coordinate = myCoordinate;
                         [self.mapView addAnnotation:annotation];
                         [self performSelector:@selector(zoomInToMyLocationWithLocation:)
                                    withObject:nil
                                    afterDelay:0];

                     }
                 }];
    
    
    __block CLLocationCoordinate2D center;
//    
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//    
//        double latitude = 0, longitude = 0;
//        NSString *esc_addr =  [addressStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//        NSString *req = [NSString stringWithFormat:@"http://maps.google.com/maps/api/geocode/json?sensor=false&address=%@", esc_addr];
//        NSString *result = [NSString stringWithContentsOfURL:[NSURL URLWithString:req] encoding:NSUTF8StringEncoding error:NULL];
//        if (result) {
//            NSScanner *scanner = [NSScanner scannerWithString:result];
//            if ([scanner scanUpToString:@"\"lat\" :" intoString:nil] && [scanner scanString:@"\"lat\" :" intoString:nil]) {
//                [scanner scanDouble:&latitude];
//                if ([scanner scanUpToString:@"\"lng\" :" intoString:nil] && [scanner scanString:@"\"lng\" :" intoString:nil]) {
//                    [scanner scanDouble:&longitude];
//                }
//            }
//        }
//        
//        center.latitude=latitude;
//        center.longitude = longitude;
//        NSLog(@"View Controller get Location Logitute : %f",center.latitude);
//        NSLog(@"View Controller get Location Latitute : %f",center.longitude);
//        
//        dispatch_async(dispatch_get_main_queue(), ^{
//        
//            MKPointAnnotation*    annotation = [[MKPointAnnotation alloc] init];
//            myCoordinate.latitude=latitude;
//            myCoordinate.longitude=longitude;
//            annotation.coordinate = myCoordinate;
//            [self.mapView addAnnotation:annotation];
//            [self performSelector:@selector(zoomInToMyLocationWithLocation:)
//                       withObject:nil
//                       afterDelay:0];
//        
//        });
//            
//    });
    
    return center;
    
}


-(void)zoomInToMyLocationWithLocation:(CLLocationCoordinate2D )location
{
    MKCoordinateRegion region = { {0.0, 0.0 }, { 0.0, 0.0 } };
    region.center.latitude = myCoordinate.latitude;
    region.center.longitude = myCoordinate.longitude;
    region.span.longitudeDelta = 1.0f;
    region.span.latitudeDelta = 1.0f;
    [_mapView setRegion:region animated:YES];
}

-(void)fetchPackageDetails:(Holiday *)objHoliday
{
    NSMutableDictionary *dictOfData = [[NSMutableDictionary alloc]init];
    [dictOfData setValue:objHoliday.strPackageId forKey:@"packageId"];
    
    if ([super connected])
    {
        @try
        {

        NetworkHelper *helper = [NetworkHelper sharedHelper];
        
        NSString *pathParam = objHoliday.strPackageId;
        
        NSDictionary *headerDict = [CoreUtility getHeaderDict];
        
        [helper getResponseWithRequestType:@"" withQueryParam:@"" withPathParam:pathParam withJsonParam:nil withHeaders:headerDict withUrl:kAstraUrlPackageDetail success:^(NSDictionary *responseDict)
         {
             NSLog(@"Response Dict : %@",responseDict);
             
             
             dispatch_async(dispatch_get_main_queue(), ^(void) {
                 [activityLoadingView removeFromSuperview];
                 if (responseDict)
                 {
                     if (responseDict.count>0)
                     {
                         NSArray *packageArray = (NSArray *)responseDict;
                         NSDictionary *dictForCompletePackage = [packageArray objectAtIndex:0];
                         
                         HolidayPackageDetail *packageDetailHoliday = [[HolidayPackageDetail alloc] initWithDataDict:dictForCompletePackage];
                         
                         
                         
                         TabMenuVC *tabMenuVC = [[TabMenuVC alloc]initWithNibName:@"BaseViewController" bundle:nil];
                         
                         tabMenuVC.headerName = objHoliday.strPackageName;
                         
                         tabMenuVC.flag = @"Overview";
                         tabMenuVC.packageDetail = packageDetailHoliday;
                         [self fireAppIntHoViewDetailsEvent:packageDetailHoliday];
                         tabMenuVC.completePackageDetail = [NSArray arrayWithObjects:dictForCompletePackage, nil];
                         [[SlideNavigationController sharedInstance] pushViewController:tabMenuVC animated:YES];
                         
                         // [self doMyOperationsWithResponse:packageArray];
                         
                     }
                     else
                     {
                         [super showAlertViewWithTitle:@"Alert" withMessage:@"some error occurred"];
                         
                     }
                 }
                 else
                 {
                     [super showAlertViewWithTitle:@"Alert" withMessage:@"some error occurred"];
                     
                 }
             });
             
         }
         
                                   failure:^(NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^(void)
                            {
                                [activityLoadingView removeFromSuperview];
                                [super showAlertViewWithTitle:@"Alert" withMessage:@"some error occurred"];
                                NSLog(@"Response Dict : %@",[error description]);
                            });

             
         }];

        }
        @catch (NSException *exception)
        {
            NSLog(@"%@", exception.reason);
            [activityLoadingView removeFromSuperview];
        }
        @finally
        {
            NSLog(@"exception finally called");
        }
    
   /* dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NetworkHelper *helper = [NetworkHelper sharedHelper];
        NSString *strResponse = [helper getDataFromServerForType:@"webservice" entity:@"package" action:@"details" andUserJson:dictOfData];
        NSString *strStatus = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"status"];
        
        if ([strStatus isEqualToString:kStatusSuccess])
        {
            NSArray *dataArray = [[JsonSerealizer arrayOfJSonFromJsonData:strResponse] valueForKey:@"data"];
            NSDictionary *dataDict;
            if (dataArray.count != 0)
            {
                dataDict = dataArray[0];
               packageDetail = [[HolidayPackageDetail alloc]initWithDataDict:dataDict];
                
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                   
                    TabMenuVC *tabMenuVC = [[TabMenuVC alloc]initWithNibName:@"BaseViewController" bundle:nil];
                    tabMenuVC.headerName = objHoliday.strPackageName;
                    tabMenuVC.flag=@"Overview";
                    tabMenuVC.completePackageDetail = dataArray;
                    tabMenuVC.packageDetail = packageDetail;
                    [[SlideNavigationController sharedInstance] pushViewController:tabMenuVC animated:YES];
                });
                
            }
        }else
        {
            NSString *messsage = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"message"];
            if (messsage)
            {
                if ([messsage isEqualToString:kVersionUpgradeMessage])
                {
                    NSDictionary *downloadDict = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"data"];
                    NSString *downloadURL = [downloadDict valueForKey:@"download_url"];
                    [super showAlertViewForVersionUpdateWithUrl:downloadURL];

                }
                else
                {
                    [self showAlertViewWithTitle:@"Alert" withMessage:messsage];
                }
            }else
            {
                [self showAlertViewWithTitle:@"Alert" withMessage:kMessageSomeErrorTryAfterSometime];
            }
            
        }
        if ([activityLoadingView isDescendantOfView:self.view])
        {
            [activityLoadingView removeView];
            activityLoadingView = nil;
        }
        
    });*/
        
    }
    else
    {
        [self showAlertViewWithTitle:@"Alert" withMessage:kMessageNoInternet];
    }
}


#pragma mark - MapView Delegates
-(void)mapViewDidFinishLoadingMap:(MKMapView *)mapView
{
    
}

-(void)mapViewDidFinishRenderingMap:(MKMapView *)mapView fullyRendered:(BOOL)fullyRendered
{
    if (fullyRendered) {
        
        [self getLocationFromAddressString:_searchedCity];
    }
}

//APP_INT_HO_VIEW_DETAILS
-(void)fireAppIntHoViewDetailsEvent:(HolidayPackageDetail *)PacakageDetail
{
    
    [payLoadForViewDetails setObject:PacakageDetail.strPackageName forKey:@"s^DESTINATION"];
    [payLoadForViewDetails setObject:[NSString stringWithFormat:@"%d",PacakageDetail.durationNoDays] forKey:@"s^DURATION"];
    [payLoadForViewDetails setObject:[NSString stringWithFormat:@"%d",PacakageDetail.packagePrise] forKey:@"s^BUDGET"];
    [payLoadForViewDetails setObject:PacakageDetail.strPackageId forKey:@"s^PACKAGE_ID"];
    [payLoadForViewDetails setObject:@"App" forKey:@"s^SOURCE"];
    
//  [payLoadForViewDetails setObject:[NSString stringWithFormat:@"%d",packageDetail.packagePrise] forKey:@"i^PACKAGE_PRICE"]; //28-02-2018
    
    [payLoadForViewDetails setObject:[NSNumber numberWithInt:PacakageDetail.packagePrise] forKey:@"i^PACKAGE_PRICE"];//28-02-2018
    
    if ([[PacakageDetail.strPackageSubType lowercaseString]isEqualToString:@"git"])
    {
        [payLoadForViewDetails setObject:@"no" forKey:@"s^PERSONALIZED_TOUR"];
    }
    else
    {
        [payLoadForViewDetails setObject:@"yes" forKey:@"s^PERSONALIZED_TOUR"];
    }
    
    if(PacakageDetail.strPackageType != nil && ![PacakageDetail.strPackageType isEqualToString:@""])
    {
        [payLoadForViewDetails setObject:PacakageDetail.strPackageType forKey:@"s^TYPE"];
    }
    
    //[NetCoreAnalyticsVC getNetCorePushNotificationDetailDictionary:payLoadForViewDetails withPayloadCount:105];//28-02-2018
    
    [NetCoreAnalyticsVC getNetCorePushNotificationDetailDictionary:payLoadForViewDetails withPayloadCount:125];
    NSLog(@"Data submitted to the netcore: 125");
    
    
}

@end
