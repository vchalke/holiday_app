//
//  customView.m
//  Holiday
//
//  Created by Kush Thakkar on 15/02/20.
//  Copyright © 2020 Kush Thakkar. All rights reserved.
//

#import "customView.h"
#import "TopCollectionViewCell.h"
#import "WebUrlConstants.h"
#import "ColourClass.h"

@implementation customView

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
     self = [super initWithCoder:aDecoder];
     if(self) {
        [self loadNib];
    }
     return self;

}

//- (instancetype)initWithFrame:(CGRect)frame {
//     self = [super initWithFrame:frame];
//     if(self) {
//        [self loadNib];
//    }
//     return self;
//}



-(void)loadNib{
    
    UIView *view = [[[NSBundle bundleForClass:[self class]]loadNibNamed:@"customView" owner:self options:nil] firstObject];
    
    view.frame = self.bounds;

    [self addSubview:view];
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"TopCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"TopCollectionViewCell"];

    imageArray = @[@"holidaysBlue",@"forexBlue",@"visaBlue",@"flightsBlue",@"insuranceBlue",@"cruiseBlue",@"hotelsBlue",@"giftcardBlue"];

    titleArray = @[@"Holidays",@"Forex",@"Visa",@"Flights",@"Insurance",@"Cruise",@"Hotels",@"Gift Cards"];

    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;

//    [self layoutIfNeeded];
//    [self setNeedsLayout];

}


-(UIImage *)coloredImage:(UIImage *)firstImage withColor:(UIColor *)color {
    UIGraphicsBeginImageContext(firstImage.size);

    CGContextRef context = UIGraphicsGetCurrentContext();
    [color setFill];

    CGContextTranslateCTM(context, 0, firstImage.size.height);
    CGContextScaleCTM(context, 1.0, -1.0);

    CGContextSetBlendMode(context, kCGBlendModeCopy);
    CGRect rect = CGRectMake(0, 0, firstImage.size.width, firstImage.size.height);
    CGContextDrawImage(context, rect, firstImage.CGImage);

    CGContextClipToMask(context, rect, firstImage.CGImage);
    CGContextAddRect(context, rect);
    CGContextDrawPath(context,kCGPathElementMoveToPoint);

    UIImage *coloredImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return coloredImg;
}

#pragma mark - collectionview datasource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return [imageArray count];
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    TopCollectionViewCell *selectedCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TopCollectionViewCell" forIndexPath:indexPath];
    
    if(!selectedCell){
        
        selectedCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TopCollectionViewCell" forIndexPath:indexPath];
    }
    
    selectedCell.imageViewOne.image = [UIImage imageNamed:imageArray[indexPath.row]];
//    selectedCell.imageViewOne.image = [self coloredImage:[UIImage imageNamed:imageArray[indexPath.row]] withColor:[ColourClass colorWithHexString:@"#0054A5"]];
//    selectedCell.titleLabel.text = titleArray[indexPath.row];
    selectedCell.titleLabel.text = [titleArray[indexPath.row] uppercaseString];
    
    return selectedCell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {

    return CGSizeMake(self.collectionView.bounds.size.width*0.22,self.collectionView.bounds.size.height*0.92);
    
}

- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout *)collectionViewLayout
        minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    /*
    switch (indexPath.row) {
        case 0:
            [self.custDelegate searchButtonClick];
            break;
        case 1:
            [self.custDelegate jumpToWebView:kForexWebUrl withTitle:@"Forex"];
            break;
            
        case 2:
            [self.custDelegate jumpToWebView:kVisaWebUrl withTitle:@"Visa"];
            break;
            
        case 3:
            [self.custDelegate jumpToWebView:kFlightsWebUrl withTitle:@"Flights"];
            break;
            
        case 4:
            [self.custDelegate jumpToWebView:kForexWebUrl withTitle:@"Insurance"];
            break;
            
        case 5:
            [self.custDelegate jumpToWebView:kForexWebUrl withTitle:@"Cruise"];
            break;
            
        case 6:
            [self.custDelegate jumpToWebView:kForexWebUrl withTitle:@"Hotels"];
            break;
            
        case 7:
            [self.custDelegate jumpToWebView:kForexWebUrl withTitle:@"Gift Cards"];
            break;
        default:
            break;
    }
    */
    NSArray *webTitleArray = @[@"Holiday",@"Forex",@"Visa",@"Flights",@"Insurance",@"Cruise",@"Hotels",@"Gift Cards"];
    NSArray *webUrlArray = @[kForexWebUrl,kForexWebUrl,kVisaWebUrl,kFlightsWebUrl,kInsuranceWebUrl,kCruiseWebUrl,kHotelsWebUrl,kGiftCardsWebUrl];
    if (indexPath.row > 0){
        [self.custDelegate jumpToWebView:[webUrlArray objectAtIndex:indexPath.row] withTitle:[webTitleArray objectAtIndex:indexPath.row]];
    }else{
        [self.custDelegate searchButtonClick];
    }
}

@end
