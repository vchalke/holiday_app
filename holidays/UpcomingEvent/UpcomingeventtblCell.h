//
//  UpcomingeventtblCell.h
//  SOTCAPPDEsign
//
//  Created by Kush Thakkar on 03/03/20.
//  Copyright © 2020 Kush Thakkar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WhatsNewObject.h"
NS_ASSUME_NONNULL_BEGIN
@protocol UpcomingeventtblCellDelegate <NSObject>
@optional
-(void)jumpToTheWebViewFromUpcoming:(NSString *)webUrlString withTitle:(NSString *)webViewTitle withObject:(WhatsNewObject *)newObject;
@end
@interface UpcomingeventtblCell : UITableViewCell<UICollectionViewDelegate,UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UICollectionView *collection_view;
@property (nonatomic, weak) id <UpcomingeventtblCellDelegate> delegate;
@property (weak, nonatomic)NSArray *upcomingDataArray;
-(void)setupCollectionViewTitle:(NSString*)title subTitle:(NSString*)subTitle;
@property (weak, nonatomic) IBOutlet UILabel *lbl_titles;

@end

NS_ASSUME_NONNULL_END
