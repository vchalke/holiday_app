//
//  upcomingEventCell.m
//  SOTCAPPDEsign
//
//  Created by Kush Thakkar on 03/03/20.
//  Copyright © 2020 Kush Thakkar. All rights reserved.
//

#import "upcomingEventCell.h"
#import "UIImageView+WebCache.h"

@implementation upcomingEventCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(void)hideOtherLabels:(BOOL)flags{
    if (flags){
        self.lbl_destination.text = @"";
        self.lbl_duration.text = @"";
        self.lbl_prize.text = @"";
    }
    self.const_bottViewHeight.constant= flags ? 1 : 40;
    self.const_fromLeft.constant= 0;
    self.const_fromTop.constant= 0;
    self.const_fromRight.constant= 0;
    self.const_fromBottom.constant= 0;
}
-(void)showPopularDestination:(WhatsNewObject*)newObject{
    self.lbl_destination.text = newObject.bannerName;
//    self.lbl_prize.text = [NSString stringWithFormat:@"Starting @ %@",newObject.bannerStartingPrice];
//    self.lbl_duration.text = newObject.bannerDuration;
    self.lbl_prize.text = @"";
    self.lbl_duration.text = @"";
    NSURL* urlImage=[NSURL URLWithString:newObject.bannerSmallImage];
    if (urlImage == nil){
        urlImage=[NSURL URLWithString:newObject.mainBannerImgUrl];
    }
    if(urlImage)
    {
        UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        indicator.center = self.img_views.center;
        [self.img_views addSubview:indicator];
        [indicator startAnimating];
        [self.img_views sd_setImageWithURL:urlImage placeholderImage:[UIImage imageNamed:@"defaultBanner.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
            [indicator stopAnimating];
        }];
    }
}
-(void)showUpcomingHolidays:(WhatsNewObject*)newObject{
    self.lbl_destination.text = newObject.bannerName;
    self.lbl_prize.text = [NSString stringWithFormat:@"Starting @ %@",newObject.bannerStrikeoutPrice];
    self.lbl_duration.text = newObject.bannerDuration;
    NSURL* urlImage=[NSURL URLWithString:newObject.bannerSmallImage];
    if (urlImage == nil){
        urlImage=[NSURL URLWithString:newObject.mainBannerImgUrl];
    }
    if(urlImage)
    {
        UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        indicator.center = self.img_views.center;
        [self.img_views addSubview:indicator];
        [indicator startAnimating];
        [self.img_views sd_setImageWithURL:urlImage placeholderImage:[UIImage imageNamed:@"defaultBanner.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
            [indicator stopAnimating];
        }];
    }
}
@end
