//
//  upcomingEventCell.h
//  SOTCAPPDEsign
//
//  Created by Kush Thakkar on 03/03/20.
//  Copyright © 2020 Kush Thakkar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WhatsNewObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface upcomingEventCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbl_destination;
@property (weak, nonatomic) IBOutlet UILabel *lbl_duration;
@property (weak, nonatomic) IBOutlet UILabel *lbl_prize;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_bottViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_fromLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_fromTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_fromRight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_fromBottom;
@property (weak, nonatomic) IBOutlet UIImageView *img_views;
-(void)hideOtherLabels:(BOOL)flags;
-(void)showPopularDestination:(WhatsNewObject*)newObject;
-(void)showUpcomingHolidays:(WhatsNewObject*)newObject;
@end

NS_ASSUME_NONNULL_END
