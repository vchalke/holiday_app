//
//  UpcomingeventtblCell.m
//  SOTCAPPDEsign
//
//  Created by Kush Thakkar on 03/03/20.
//  Copyright © 2020 Kush Thakkar. All rights reserved.
//

#import "UpcomingeventtblCell.h"
#import "gridViewCollectionCell.h"
#import "upcomingEventCell.h"
@implementation UpcomingeventtblCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setupCollectionViewTitle:(NSString*)title subTitle:(NSString*)subTitle
{
    [self.collection_view registerNib:[UINib nibWithNibName:@"upcomingEventCell" bundle:nil] forCellWithReuseIdentifier:@"upcomingEventCell"];
    self.collection_view.delegate = self;
    self.collection_view.dataSource = self;
    self.lbl_titles.text = title;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [self.upcomingDataArray count] ; //5
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    upcomingEventCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"upcomingEventCell" forIndexPath:indexPath];
//    NSArray *imgArray = [NSArray arrayWithObjects:@"natureOne",@"natureTwo",@"natureThree",@"natureFour", nil];
//    cell.img_views.image = [UIImage imageNamed:[imgArray objectAtIndex:indexPath.row]];
        [cell showUpcomingHolidays:[self.upcomingDataArray objectAtIndex:indexPath.row]];
    return cell;
}
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(5.0, 15.0, 5.0, 15.0);
}
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(self.collection_view.frame.size.width*0.6, self.collection_view.frame.size.height*0.9);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    WhatsNewObject *bannerObj = [self.upcomingDataArray objectAtIndex:indexPath.row];
    [self.delegate jumpToTheWebViewFromUpcoming:bannerObj.redirectUrl withTitle:bannerObj.bannerName withObject:bannerObj];
}
@end
