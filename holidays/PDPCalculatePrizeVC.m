//
//  PDPCalculatePrizeVC.m
//  Thomoscook_Holiday
//
//  Created by Kush_Tech on 19/02/20.
//  Copyright © 2020 Kush_Tech. All rights reserved.
//

#import "PDPCalculatePrizeVC.h"
#import "MKDropdownMenu.h"
#import "TopWebVC.h"
#import "PaymentTermsObject.h"
#import "CalenderScreenVC.h"
#import "RoomsDataModel.h"
#import "PackageDomesticFit.h"
#import "PackageInternationFIT.h"
#import "PackageInternationalGIT.h"
#import "Constants.h"
#import "CalendarViewController.h"
#import "DateStringSingleton.h"
#import "PDPCalculatePromoVC.h"
#import "TravellerInformationModel.h"
#import "NetCoreAnalyticsVC.h"
#import "NewLoginPopUpViewController.h"
#define kPaymentTerms @"Payment Terms"
#define kCancellatipnPolicy @"Cancellation Policy"
#define kTermsAndCond @"Terms & Conditions"

@interface PDPCalculatePrizeVC ()<UITextFieldDelegate,NewMasterVCDelegate,CalenderScreenVCDelegate>
{
    NSArray *stateArray;
    NSArray *adultArray;
    MKDropdownMenu *dropMenuForCity;
    MKDropdownMenu *dropMenuForTravellers;
    BOOL isCheck;
    
    NSArray *packageTypeArray;
    NSMutableArray *roomRecordArray;
    NSMutableArray *ItineraryCodeArray;
    NSArray *travellerArrayForCalculation;
    LoadingView *activityLoadingView;
    NSString *packageType;
    int accomType;
    NSString *departFrom;
    NSDictionary *selectedHubDict;
    NSString *selectedLtItineraryCode;
    NSString *bookingAmountString;
    NSString *bookingAmountForBookingSubmit;
    NSDictionary *dictForDate;
     NSDictionary *calculationDict;
}
@property (nonatomic, retain) MKDropdownMenu *deleg;
@end

@implementation PDPCalculatePrizeVC

- (void)viewDidLoad {
//    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self checkButtonClicked:nil];
    [self.btn_Check addTarget:self action:@selector(checkButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.btn_policyTerms addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.btn_PrivactPolicy addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.btn_termsCondition addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    stateArray = [NSArray arrayWithObjects:@"Delhi",@"Punjab",@"Maharashtra",@"Gujarat",@"TamilNadu",@"Kerlaa", nil];
    adultArray = [NSArray arrayWithObjects:@"2 Adults",@"4 Adults",@"6 Adults",@"8 Adults",@"10 Adults",@"12 Adults", nil];
    
    self.txt_MobileDate.inputAccessoryView = [self getUIToolBarForKeyBoard];
    self.masterDelgate = self;
    
    /*
    dropMenuForCity = [[MKDropdownMenu alloc] initWithFrame:CGRectMake(0, 0, self.departureCity_View.frame.size.width, self.departureCity_View.frame.size.height-5)];
    dropMenuForCity.dataSource = self;
    dropMenuForCity.delegate = self;
    [self.departureCity_View addSubview:dropMenuForCity];
    
    dropMenuForTravellers = [[MKDropdownMenu alloc] initWithFrame:CGRectMake(0, 0, self.numOfTraveller_View.frame.size.width, self.numOfTraveller_View.frame.size.height-5)];
    dropMenuForTravellers.dataSource = self;
    dropMenuForTravellers.delegate = self;
    [self.numOfTraveller_View addSubview:dropMenuForTravellers];
    */
    
    roomRecordArray = [[NSMutableArray alloc]init];
    RoomsDataModel *roomModel = [[RoomsDataModel alloc]init];
    [roomRecordArray addObject:roomModel];
    [self setRoomDataModelWithIndex:1];
    packageTypeArray = self.holidayPkgDetailInPdpCalculate.arrayAccomTypeList;
    NSLog(@"newPackageDetailDict %@",self.holidayPkgDetailInPdpCalculate.packageDetailDictObj);
    NSLog(@"arrayItinerary %@",self.holidayPkgDetailInPdpCalculate.arrayItinerary);
    NSLog(@"arrayHubList %@",self.holidayPkgDetailInPdpCalculate.arrayHubList);
    NSLog(@"arrayOfHubList %@",self.holidayPkgDetailInPdpCalculate.arrayOfHubList);
    NSLog(@"ltItineraryCode %@",self.holidayPkgDetailInPdpCalculate.ltItineraryCode);
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.txt_DepartureDate resignFirstResponder];
    [CoreUtility reloadRequestID];
    if([[self.holidayPkgDetailInPdpCalculate.strPackageSubType lowercaseString] isEqualToString:@"fit fixed"]|| [[self.holidayPkgDetailInPdpCalculate.strPackageSubType lowercaseString] isEqualToString:@"git"])
    {
        if([[self.holidayPkgDetailInPdpCalculate.strPackageType lowercaseString] isEqualToString:@"international"])
        {
            packageType = kpackageTypeGITInternational;
            
        }
        else
        {
            packageType = kpackageTypeGITDomestic;
        }
    }else
    {
        if([[self.holidayPkgDetailInPdpCalculate.strPackageType lowercaseString] isEqualToString:@"international"])
        {
            packageType = kpackageTypeFITInternational;
        }
        else
        {
            packageType = kpackageTypeFITDomestic;
        }
    }
    if(self.holidayPkgDetailInPdpCalculate.stringSelectedAccomType == nil || [self.holidayPkgDetailInPdpCalculate.stringSelectedAccomType isEqualToString:@""])
    {
        if (packageTypeArray.count != 0)
        {
            NSString *packageTypeLocal = (NSString *)packageTypeArray[0];
            [self setAccomType:@"standard"];
        }
        
    }else
    {
        if ([self.holidayPkgDetailInPdpCalculate.stringSelectedAccomType isEqualToString:@"0"])
        {
            accomType = 0;
        }
        else if ([self.holidayPkgDetailInPdpCalculate.stringSelectedAccomType isEqualToString:@"1"])
        {
            accomType = 1;
        }
        else
        {
            accomType = 2;
        }
    }
     
    
}
-(void)setAccomType:(NSString *)accomTypeString
{
    if ([[accomTypeString lowercaseString] isEqualToString:@"standard"])
    {
        self.holidayPkgDetailInPdpCalculate.stringSelectedAccomType = @"0";
    }
    else if ([[accomTypeString lowercaseString] isEqualToString:@"delux"]||[[accomTypeString lowercaseString] isEqualToString:@"deluxe"])
    {
        self.holidayPkgDetailInPdpCalculate.stringSelectedAccomType = @"1";
    }
    else
    {
        self.holidayPkgDetailInPdpCalculate.stringSelectedAccomType = @"2";
    }
}
-(void) buttonClicked:(UIButton*)sender
{
    switch (sender.tag) {
        case 0:
            [self jumpToWebViewFromPDP:@"" withTitle:kPaymentTerms];
            break;
        case 1:
            [self jumpToWebViewFromPDP:@"" withTitle:kCancellatipnPolicy];
            break;
        case 2:
            [self jumpToWebViewFromPDP:@"" withTitle:kTermsAndCond];
            break;
        default:
            break;
    }
}
-(void) checkButtonClicked:(UIButton*)sender{
    isCheck = !isCheck;
    [self.btn_Check setImage:[UIImage imageNamed:(isCheck) ? @"tickIcon" : @""] forState:UIControlStateNormal];
}
-(void)setRoomDataModelWithIndex:(NSInteger)index{
//    RoomsDataModel *roomModel = roomRecordArray[index-1];
    RoomsDataModel *roomModel = [roomRecordArray firstObject];
    roomModel.adultCount = [[self.txt_numOfTraveller.text stringByReplacingOccurrencesOfString:@" Adults" withString:@""]intValue];
    roomModel.infantCount = 0;
    roomModel.arrayChildrensData = 0;
    [roomRecordArray replaceObjectAtIndex:0 withObject:roomModel];
}
/*
#pragma mark - MKDropdownMenu Delegaet

- (NSInteger)numberOfComponentsInDropdownMenu:(MKDropdownMenu *)dropdownMenu{
    return 1;
}
- (NSInteger)dropdownMenu:(MKDropdownMenu *)dropdownMenu numberOfRowsInComponent:(NSInteger)component{
    if (dropdownMenu == dropMenuForCity){
      return stateArray.count;
    }else if (dropdownMenu == dropMenuForTravellers){
      return adultArray.count;
    }
    return 0;
}

- (NSString *)dropdownMenu:(MKDropdownMenu *)dropdownMenu titleForComponent:(NSInteger)component{
    if (dropdownMenu == dropMenuForCity){
      return [stateArray objectAtIndex:0];
    }else if (dropdownMenu == dropMenuForTravellers){
      return [adultArray objectAtIndex:0];
    }
    return @"";
    
}
- (NSString *)dropdownMenu:(MKDropdownMenu *)dropdownMenu titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if (dropdownMenu == dropMenuForCity){
      return [stateArray objectAtIndex:row];
    }else if (dropdownMenu == dropMenuForTravellers){
      return [adultArray objectAtIndex:row];
    }
    return @"";
    
}
-(void)dropdownMenu:(MKDropdownMenu *)dropdownMenu didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if (dropdownMenu == dropMenuForCity){
      NSLog(@"%@",[stateArray objectAtIndex:row]);
        [dropMenuForCity closeAllComponentsAnimated:YES];
    }else if (dropdownMenu == dropMenuForTravellers){
      NSLog(@"%@",[adultArray objectAtIndex:row]);
        [dropMenuForTravellers closeAllComponentsAnimated:YES];
    }
}
*/


#pragma mark - Calender Screen Delegaet
-(void)setSelectedDate:(NSDate *)date withVCIndex:(int)index withDataDict:(nonnull NSDictionary *)dataDict{
    self.txt_DepartureDate.text = ([[NSDate date] compare:date] == NSOrderedAscending) ? [[DateStringSingleton shared]getDateTimeStringFormat:@"dd-MM-yyyy" DateValue:date]:@"";
    NSLog(@"dataDict %@",dataDict);
    dictForDate = dataDict;
}
#pragma mark - Checking Validations
-(NSString*)getEmptyWarning{
    if ([self.txt_DepartureCity.text length]==0){
        return @"Select City";
    }
    if ([self.txt_numOfTraveller.text length]==0){
        return @"Select Number Of Travellers";
    }
    if ([self.txt_DepartureDate.text length]==0){
        return @"Select Date";
    }
    if ([self.txt_MobileDate.text length]!=10){
        return @"Enter Valid Mobile Number";
    }
    if (!isCheck){
        return @"Click On Accept Policy";
    }
    return @"";
}
#pragma mark - Checking Validations


-(void)textFieldDepartureCityPress{
    
    NSArray *hubList = [NSArray new];
    if([packageType isEqualToString:kpackageTypeFITDomestic]||[packageType isEqualToString:kpackageTypeFITInternational]){
        hubList = self.holidayPkgDetailInPdpCalculate.arraytcilHolidayPriceCollection;
        
    }
    else{
        hubList = self.holidayPkgDetailInPdpCalculate.arrayLtPricingCollection;
    }
    UIAlertController *durationActionSheet = [UIAlertController alertControllerWithTitle:@"Please Select" message:@"Select your choice" preferredStyle:UIAlertControllerStyleActionSheet];
    NSArray *filteredarray;
    filteredarray = hubList;
    filteredarray = [hubList filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(packageClassId == %@)", [NSString stringWithFormat:@"%d",accomType]]];
    
    NSArray* uniqueValues;
    if([packageType isEqualToString:kpackageTypeFITDomestic]||[packageType isEqualToString:kpackageTypeFITInternational]){
        uniqueValues = [filteredarray valueForKeyPath:[NSString stringWithFormat:@"@distinctUnionOfObjects.hubCityCode.cityName"]];
    }
    else{
        uniqueValues = [filteredarray valueForKeyPath:[NSString stringWithFormat:@"@distinctUnionOfObjects.hubCode.cityName"]];
    }
    NSMutableArray * uniqueDataArray = [[NSMutableArray alloc] init];
    for (NSString * city in uniqueValues)
    {
        NSExpression *lhs;
        if([packageType isEqualToString:kpackageTypeFITDomestic]||[packageType isEqualToString:kpackageTypeFITInternational]){
            
            lhs = [NSExpression expressionForKeyPath:@"hubCityCode.cityName"];
        }
        else{
            lhs = [NSExpression expressionForKeyPath:@"hubCode.cityName"];
            
        }
        NSExpression *rhs = [NSExpression expressionForConstantValue:city];
        NSPredicate * finalPredicate = [NSComparisonPredicate
                                        predicateWithLeftExpression:lhs
                                        rightExpression:rhs
                                        modifier:NSDirectPredicateModifier
                                        type:NSContainsPredicateOperatorType
                                        options:NSCaseInsensitivePredicateOption];
        NSArray * filteredChannelArray   = [[filteredarray filteredArrayUsingPredicate:finalPredicate] mutableCopy];
        if (filteredChannelArray.count > 0) {
            [uniqueDataArray addObject:[filteredChannelArray objectAtIndex:0]];
        }
    }
    NSSortDescriptor *nameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"hubCode.cityName"  ascending:YES];
    NSArray *sortDescriptors = @[nameDescriptor];
    NSArray * ordered = [uniqueDataArray sortedArrayUsingDescriptors:sortDescriptors];
    
    for (int j =0 ; j < ordered.count; j++)
    {
        NSDictionary *cityDict = ordered[j];
        NSDictionary *hubCodeDict;
        if([packageType isEqualToString:kpackageTypeFITDomestic]||[packageType isEqualToString:kpackageTypeFITInternational]) {
            hubCodeDict = [cityDict valueForKey:@"hubCityCode"];
        }
        else{
            hubCodeDict = [cityDict valueForKey:@"hubCode"];
        }
        NSString *titleString = [hubCodeDict valueForKey:@"cityName"];
        NSString *departFromLocal = [hubCodeDict valueForKey:@"cityCode"];
        NSString *ltCodeString = [cityDict valueForKey:@"ltItineraryCode"];
        UIAlertAction *action = [UIAlertAction actionWithTitle:titleString style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                 {
            departFrom = departFromLocal;
            self.txt_DepartureCity.text = titleString;
            selectedHubDict = hubCodeDict;
            selectedLtItineraryCode = ltCodeString;
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"hubCode.cityName contains[c] %@ ",titleString];
            NSArray *filteredData = [filteredarray filteredArrayUsingPredicate:predicate];
            ItineraryCodeArray = [[NSMutableArray alloc]init];
            NSLog(@"%@",filteredData);
            for(int i =0; i<filteredData.count;i++){
                NSDictionary *cityDict = filteredData[i];
                selectedLtItineraryCode = [cityDict valueForKey:@"ltItineraryCode"];
                [ItineraryCodeArray addObject:selectedLtItineraryCode];
            }
        }];
        [durationActionSheet addAction:action];
    }
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    [durationActionSheet addAction:cancelAction];
    [self.view.window.rootViewController presentViewController:durationActionSheet animated:YES completion:nil];
    
}
#pragma mark - TextField Delegaet
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField != self.txt_MobileDate){
        [textField resignFirstResponder];
        if (textField == self.txt_DepartureCity){
            [self textFieldDepartureCityPress];
        }else if (textField == self.txt_numOfTraveller){
            [self showMultipleOptionsInAlertWithTitle:@"Please Select" msg:@"Number Of Adults" style:UIAlertControllerStyleActionSheet buttArray:[NSArray arrayWithObjects:@"1 Adult",@"2 Adults",@"3 Adults",@"4 Adults",@"5 Adults",nil] completion:^(NSString * string) {
                if (![string isEqualToString:@"Cancel"]){
                self.txt_numOfTraveller.text = string;
                }
            }];
        }else if (textField == self.txt_DepartureDate){
            if ([self.txt_DepartureCity.text length]==0){
                [self showAlertViewWithTitle:@"Alert !" withMessage:@"Select Departure City"];
            }else{
                [self checkValidityOfCombinations];
                [self showCalenderViewAspsdInPdp];
            }
        }
    }
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if(textField == self.txt_MobileDate)
    {
        if ([textField.text length]>9){
            return YES;
        }
    }
    return YES;
}
#pragma mark - Button Actions

- (IBAction)btn_CalculateTourCost:(id)sender {
    NSString *warningString = [self getEmptyWarning];
    if ([warningString length]==0){
        [self jumpYourQuoteVC];
    }else{
        [self showButtonsInAlertWithTitle:@"Alert" msg:warningString style:UIAlertControllerStyleAlert buttArray:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString * string) {
            
        }];
    }
}
- (IBAction)btn_BackPress:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)btn_AddRoomPress:(id)sender{
    [self showMultipleOptionsInAlertWithTitle:@"Please Select" msg:@" Number Of Rooms" style:UIAlertControllerStyleActionSheet buttArray:[NSArray arrayWithObjects:@"Room 1",@"Room 2",@"Room 3",@"Room 4",@"Room 5",nil] completion:^(NSString * string) {
        if (![string isEqualToString:@"Cancel"]){
        self.lbl_numofRoom.text = string;
        }
    }];
}
#pragma mark - Jump To Other Views

-(void)setToolBarActionTextField:(UIBarButtonItem *)barButtonItem{
    [self.txt_MobileDate resignFirstResponder];
}

-(void)jumpToWebViewFromPDP:(NSString *)webUrlString withTitle:(NSString *)webViewTitle{
    TopWebVC *controler = [[TopWebVC alloc] initWithNibName:@"TopWebVC" bundle:nil];
    controler.web_url_string = webUrlString;
    controler.web_title = webViewTitle;
    controler.isFromPdpCalculate = true;
    NSMutableArray *passARray = [[NSMutableArray alloc]init];
    for (id object in self.holidayPkgDetailInPdpCalculate.arrayTermsAndConditions) {
        PaymentTermsObject *payobject = [[PaymentTermsObject alloc]initWithPaymentTermsObjectDict:object];
        if ([webViewTitle isEqualToString:kPaymentTerms])
            [passARray addObject:payobject.paymentTerms];
        else if ([webViewTitle isEqualToString:kCancellatipnPolicy])
            [passARray addObject:payobject.cancellationPolicy];
        else if ([webViewTitle isEqualToString:kTermsAndCond])
            [passARray addObject:payobject.descriptionArray];
    }
    controler.stringArray = passARray;
    [self.navigationController pushViewController:controler animated:YES];
}

-(void)jumpCalenderVC:(NSDictionary*)responseDict{
    CalenderScreenVC *controler = [[CalenderScreenVC alloc] initWithNibName:@"CalenderScreenVC" bundle:nil];
    controler.calenderDelgate = self;
    controler.dataDict = responseDict;
    if ([[self.holidayPkgDetailInPdpCalculate.packageMRP lowercaseString] isEqualToString:@"true"])
    {
        controler.isMRP = YES;
    }
    else
    {
        controler.isMRP = NO;
    }
    controler.holidayPackageDetailInCalender = self.holidayPkgDetailInPdpCalculate;
    controler.arrayTravellerCalculation = travellerArrayForCalculation;
    controler.roomRecordArray = roomRecordArray;
    controler.hubName = departFrom;
    controler.packageId = self.holidayPkgDetailInPdpCalculate.strPackageId;
    controler.accomType = accomType;
    controler.previosVCIndex = 100;
    [self.navigationController pushViewController:controler animated:YES];
}
-(void)jumpYourQuoteVC{
    PDPCalculatePromoVC *controller = [[PDPCalculatePromoVC alloc] initWithNibName:@"PDPCalculatePromoVC" bundle:nil];
    controller.holidayPkgDetailInPromoVC = self.holidayPkgDetailInPdpCalculate;
    controller.dictForDate = dictForDate;
    controller.departureDate = self.txt_DepartureDate.text;
    controller.departureCity = self.txt_DepartureCity.text;
    controller.numOfTravellers = self.txt_numOfTraveller.text;
    controller.selectHubDict = selectedHubDict;
    controller.numOfRooms = self.lbl_numofRoom.text;
    [self.navigationController pushViewController:controller animated:YES];
}
#pragma mark - Reuse of Existing Methods in holiday

#pragma mark - Showing Calender

-(void)showCalenderViewAspsdInPdp{

    if([CoreUtility connected])
    {

    @try
    {
        int totalChildCount = 0;
        int totalAdultCount = 0;

        for (int i = 0; i<roomRecordArray.count; i++)
        {
            RoomsDataModel *dataModel = roomRecordArray[i];
            totalAdultCount = totalAdultCount + dataModel.adultCount;
            totalChildCount = totalChildCount + (int)dataModel.arrayChildrensData.count;
        }

        NSMutableDictionary *dictOfData = [[NSMutableDictionary alloc]init];
        [dictOfData setValue:self.holidayPkgDetailInPdpCalculate.strPackageId forKey:@"packageId"];
        [dictOfData setValue:packageType forKey:@"accomType"];
        [dictOfData setValue:[NSString stringWithFormat:@"%d",totalAdultCount+totalChildCount] forKey:@"noOfPax"];
        [dictOfData setValue:departFrom forKey:@"departFrom"];

            activityLoadingView = [LoadingView loadingViewInView:self.view.superview.superview
                                                      withString:@""
                                               andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];


            NetworkHelper *helper = [NetworkHelper sharedHelper];



            NSMutableArray *arrayForItinarayCode = [[NSMutableArray alloc] init];



        if (![packageType isEqualToString:kpackageTypeFITInternational] && ![packageType isEqualToString:kpackageTypeFITDomestic])
        {
          //  [arrayForItinarayCode addObject:selectedLtItineraryCode];
            [arrayForItinarayCode addObjectsFromArray:ItineraryCodeArray];
        }

            NSMutableDictionary *jsonDict = [[NSMutableDictionary alloc] init];
            [jsonDict setObject:arrayForItinarayCode forKey:@"ltItineraryCode"];
            //[jsonDict setObject:packageDetail.ltMarket forKey:@"market"];
            [jsonDict setObject:@"-1" forKey:@"market"];

            [jsonDict setObject:departFrom forKey:@"hubCode"];
            [jsonDict setObject:[NSNumber numberWithInteger:[self.holidayPkgDetailInPdpCalculate.strPackageSubTypeID integerValue]] forKey:@"pkgSubTypeId"];
            [jsonDict setObject:self.self.holidayPkgDetailInPdpCalculate.stringSelectedAccomType forKey:@"pkgClassId"];
            [jsonDict setObject:self.holidayPkgDetailInPdpCalculate.strPackageId forKey:@"pkgId"];
            [jsonDict setObject:@"TCIL" forKey:@"mode"];
            [jsonDict setObject:@"N" forKey:@"isHsa"];


            NSDictionary *headerDict = [CoreUtility getHeaderDict];

            [helper getResponseWithRequestType:@"POST" withQueryParam:@"" withPathParam:@"" withJsonParam:jsonDict withHeaders:headerDict withUrl:kAstraUrlFareCalender success:^(NSDictionary *responseDict)
             {
                 NSLog(@"Response Dict : %@",responseDict);


                 dispatch_async(dispatch_get_main_queue(), ^(void)
                                {
                                    [activityLoadingView removeFromSuperview];
                                    if (responseDict)
                                    {
                                        if (responseDict.count>0){
                                            [self jumpCalenderVC:responseDict];

                                        }
                                        else{
                                            [super showAlertViewWithTitle:@"Alert" withMessage:@"Dates not available for this package."];
                                        }
                                    }else{
                                        [super showAlertViewWithTitle:@"Alert" withMessage:@"Dates not available for this package."];

                                    }
                                });

             }
                                       failure:^(NSError *error)
             {dispatch_async(dispatch_get_main_queue(), ^(void)
                             {
                                 [activityLoadingView removeFromSuperview];
                                 [super showAlertViewWithTitle:@"Alert" withMessage:@"Dates not available for this package."];
                                 NSLog(@"Response Dict : %@",error);
                             });

             }];
    }
    @catch (NSException *exception)
    {
        NSLog(@"%@", exception.reason);
        [activityLoadingView removeFromSuperview];
    }
    @finally
    {
        NSLog(@"exception finally called");

    }
    }
    else{
        [super showAlertViewWithTitle:@"Alert" withMessage:kMessageNoInternet];
    }

}

-(BOOL)checkValidityOfCombinations
{
    NSLog(@"%@",roomRecordArray)  ;
    id travellerInfo;

    if([[self.holidayPkgDetailInPdpCalculate.strPackageSubType lowercaseString] isEqualToString:@"fit fixed"]|| [[self.holidayPkgDetailInPdpCalculate.strPackageSubType lowercaseString] isEqualToString:@"git"]){
        if([[self.holidayPkgDetailInPdpCalculate.strPackageType lowercaseString] isEqualToString:@"international"]){
            PackageInternationalGIT *packageInternational = [[PackageInternationalGIT alloc]initWithTravellerArray:roomRecordArray];
            packageInternational.travellerRegion = self.holidayPkgDetailInPdpCalculate.stringRegion;
            travellerInfo = [packageInternational calaculatePackageInternationalGIT];
        }else{
            PackageDomesticFit *packageInternational = [[PackageDomesticFit alloc]initWithTravellerArray:roomRecordArray];
            travellerInfo = [packageInternational calaculatePackageInternationalFIT];
        }
    }else{
        if([[self.holidayPkgDetailInPdpCalculate.strPackageType lowercaseString] isEqualToString:@"international"]){
            PackageInternationFIT *packageInternational = [[PackageInternationFIT alloc]initWithTravellerArray:roomRecordArray];
            packageInternational.travellerRegion = self.holidayPkgDetailInPdpCalculate.stringRegion;
            travellerInfo = [packageInternational calaculatePackageInternationalFIT];
        } else{
            PackageDomesticFit *packageInternational = [[PackageDomesticFit alloc]initWithTravellerArray:roomRecordArray];
            travellerInfo = [packageInternational calaculatePackageInternationalFIT];
        }
    }

    NSLog(@"%@",roomRecordArray)  ;
    if ([travellerInfo isKindOfClass:[NSString class]]||travellerInfo == nil ){
        return NO;
    }
    else if([travellerInfo isKindOfClass:[NSArray class]]){
        travellerArrayForCalculation = (NSArray*)travellerInfo;
        if (travellerArrayForCalculation.count == 0){
            return NO;
        }
        return YES;
    }
    return NO;
}


@end


//@{NSFontAttributeName: [UIFont systemFontOfSize:18 weight:UIFontWeightLight],NSForegroundColorAttributeName: [UIColor whiteColor]}
