//
//  BuyForexBO.swift
//  holidays
//
//  Created by Komal Katkade on 12/8/17.
//  Copyright © 2017 Mobicule. All rights reserved.
//

import Foundation
class BuyForexBO
{
    var buyForexOptionViewDetails : BuyForexOptionsViewBO? = nil
    var buyForexPassengerInfo : BuyForexPassengerInfoBO? = nil
    var buyForexPayment : BuyForexPayementBO? = nil
    var buyForexPassengerDetails : BuyForexPassengerDetailBO? = nil
    var emailID : String = ""
   
    init()
    {
       
    }
    
    func addBuyForexOptionViewDetails(buyForexOptionViewDetails:BuyForexOptionsViewBO)
    {
        self.buyForexOptionViewDetails = buyForexOptionViewDetails
    }
    func addBuyForexPassengerInfo(buyForexPassengerInfo:BuyForexPassengerInfoBO)
    {
        self.buyForexPassengerInfo = buyForexPassengerInfo
    }
    func addBuyForexPayment(buyForexPayment:BuyForexPayementBO)
    {
        self.buyForexPayment = buyForexPayment
    }
    func addBuyForexPassengerDetails(buyForexPassengerDetails:BuyForexPassengerDetailBO)
    {
        self.buyForexPassengerDetails = buyForexPassengerDetails
    }

}
