//
//  CustomProgressView.swift
//  holidays
//
//  Created by Komal Katkade on 1/27/18.
//  Copyright © 2018 Mobicule. All rights reserved.
//

import UIKit

class CustomProgressView: UIProgressView {
    
    var height:CGFloat = 1.0
    // Do not change this default value,
    // this will create a bug where your progressview wont work for the first x amount of pixel.
    // x being the value you put here.
    
    override func sizeThatFits(_ size: CGSize) -> CGSize {
        let size:CGSize = CGSize.init(width: size.width, height: height)
        
        return size
    }
    
}
