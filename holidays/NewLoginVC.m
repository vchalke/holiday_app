//
//  NewLoginVC.m
//  holidays
//
//  Created by Kush_Team on 27/05/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "NewLoginVC.h"
#import "NewSignUpVC.h"
#import "UIViewController+MJPopupViewController.h"
#import "NetCoreAnalyticsVC.h"
#import "LandingScreenVC.h"
#import "NetCoreAnalyticsVC.h"
#import "SSKeychain.h"
#import "MobiculeUtilityManager.h"
#import "Thomas_Cook_Holidays-Swift.h"
#import "WebUrlConstants.h"
#define KEYCHAIN_SERVICE @"Holiday_App_Keychain"
#define KEYCHAIN_ACCOUNT @"com.thomasCookHoliday.app"

#define kEntity @"holidays"
#define kType   @"webservice"
#define kAction  @"search"
NSString* const setCurrentIdentifier = @"setCurrentIdentifier";
//@import CodeIn
@import FirebaseAuth;
@interface NewLoginVC ()<UITextFieldDelegate,GoogleSignInViewDelegate,GIDSignInDelegate, GIDSignInUIDelegate>
{
    BOOL isPassClick;
    BOOL isOTPClick;
    BOOL isSecurePassText;
    LoadingView *activityLoadingView;
}
@property (weak, nonatomic) IBOutlet KWVerificationCodeView *kwVerifyCode;
@property (weak, nonatomic) IBOutlet KWTextFieldView *kwVerifyCodeTextViews;
@end
//@property (weak, nonatomic) IBOutlet UIButton *btn_passWord;
//@property (weak, nonatomic) IBOutlet UIButton *btn_otp;
//@property (weak, nonatomic) IBOutlet UIButton *btn_showPassword;
//@property (weak, nonatomic) IBOutlet UIButton *btn_continue;
//@property (weak, nonatomic) IBOutlet UIButton *btn_shareFacebook;
//@property (weak, nonatomic) IBOutlet UIButton *btn_shareGooglr;
//@property (weak, nonatomic) IBOutlet UIButton *btn_logins;
//@property (weak, nonatomic) IBOutlet UIButton *btn_resend_otp;
//@property (weak, nonatomic) IBOutlet UIButton *btn_signUp;
@implementation NewLoginVC

- (void)viewDidLoad {
    isSecurePassText = YES;
    [self showHideShowButtonImage:isSecurePassText];
    [self.navigationController.navigationBar setHidden:YES];
    [self disableBackSwipeAllow:NO];
    [self hideMainViewsAtIndex:3];
    [self setButtonSelectors];
    [self setToolBarForOTPTextField];
    
}
-(void)setToolBarForOTPTextField{
    UIToolbar* keyboardToolbar = [[UIToolbar alloc] init];
    [keyboardToolbar sizeToFit];
    UIBarButtonItem *flexBarButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                      target:nil action:nil];
    UIBarButtonItem *doneBarButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                      target:self action:@selector(yourTextViewDoneButton)];
    keyboardToolbar.items = @[flexBarButton, doneBarButton];
    self.txt_otp.inputAccessoryView = keyboardToolbar;
}
-(void)yourTextViewDoneButton{
    [self.txt_otp resignFirstResponder];
}
- (void)viewWillAppear:(BOOL)animated{
    dispatch_async(dispatch_get_main_queue(), ^(void)
               {
        [self getTokenIDLoginVC];
               });
}
- (void)viewDidAppear:(BOOL)animated{
    UIBezierPath *maskPath = [UIBezierPath
        bezierPathWithRoundedRect:self.main_baseBottomViews.bounds
        byRoundingCorners:(UIRectCornerTopRight | UIRectCornerBottomRight)
        cornerRadii:CGSizeMake(20, 20)
    ];
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = self.main_baseBottomViews.bounds;
    maskLayer.path = maskPath.CGPath;
    self.main_baseBottomViews.layer.mask = maskLayer;
    
    if (@available(iOS 13.0, *)) {
        [self observeAppleSignInState];
        [self setupUI];
    }else{
        self.sign_AppLeView.hidden = YES;
    }
    
}
#pragma mark - Apple Signing
- (void)observeAppleSignInState {
    if (@available(iOS 13.0, *)) {
        NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
        [center addObserver:self selector:@selector(handleSignInWithAppleStateChanged:) name:ASAuthorizationAppleIDProviderCredentialRevokedNotification object:nil];
    }
}

- (void)handleSignInWithAppleStateChanged:(id)noti {
    NSLog(@"%s", __FUNCTION__);
    NSLog(@"%@", noti);
}


- (void)setupUI {
    
    if (@available(iOS 13.0, *)) {
        // Sign In With Apple Button
//        ASAuthorizationAppleIDButton *appleIDButton = [ASAuthorizationAppleIDButton new];
        //    appleIDButton.frame =  CGRectMake(.0, .0, CGRectGetWidth(self.view.frame) - 40.0, 100.0);
//        CGPoint origin = CGPointMake(20.0, CGRectGetMidY(self.view.frame));
//        CGRect frame = appleIDButton.frame;
//        frame.origin = origin;
//        appleIDButton.frame = frame;
        ASAuthorizationAppleIDButton *appleIDButton = [ASAuthorizationAppleIDButton buttonWithType:ASAuthorizationAppleIDButtonTypeSignIn style:ASAuthorizationAppleIDButtonStyleWhiteOutline];
        appleIDButton.borderWidth = 1.1;
        appleIDButton.layer.cornerRadius = 5;
        appleIDButton.frame =  CGRectMake(.0, .0, self.sign_AppLeView.frame.size.width * 0.7, self.sign_AppLeView.frame.size.height * 0.66);
        appleIDButton.center = CGPointMake(self.sign_AppLeView.frame.size.width  / 2,
        self.sign_AppLeView.frame.size.height / 2);
        appleIDButton.cornerRadius = CGRectGetHeight(appleIDButton.frame) * 0.25;
        [self.sign_AppLeView addSubview:appleIDButton];
        [appleIDButton addTarget:self action:@selector(handleAuthrization:) forControlEvents:UIControlEventTouchUpInside];
    }
}


#pragma mark - Actions

- (void)handleAuthrization:(UIButton *)sender {
    if (@available(iOS 13.0, *)) {
        // A mechanism for generating requests to authenticate users based on their Apple ID.
        ASAuthorizationAppleIDProvider *appleIDProvider = [ASAuthorizationAppleIDProvider new];
        
        // Creates a new Apple ID authorization request.
        ASAuthorizationAppleIDRequest *request = appleIDProvider.createRequest;
        // The contact information to be requested from the user during authentication.
        request.requestedScopes = @[ASAuthorizationScopeFullName, ASAuthorizationScopeEmail];
        
        // A controller that manages authorization requests created by a provider.
        ASAuthorizationController *controller = [[ASAuthorizationController alloc] initWithAuthorizationRequests:@[request]];
        
        // A delegate that the authorization controller informs about the success or failure of an authorization attempt.
        controller.delegate = self;
        
        // A delegate that provides a display context in which the system can present an authorization interface to the user.
        controller.presentationContextProvider = self;
        
        // starts the authorization flows named during controller initialization.
        [controller performRequests];
    }
}

#pragma mark - Other Functions Called In DidLoad
-(void)setButtonSelectors{
    [self.btn_passWord addTarget:self action: @selector(passOtpClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.btn_otp addTarget:self action: @selector(passOtpClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.btn_showPassword addTarget:self action: @selector(securedButtClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.btn_continue addTarget:self action: @selector(continueClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.btn_shareFacebook addTarget:self action: @selector(shareFacebookClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.btn_shareGooglr addTarget:self action: @selector(shareGoogleClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.btn_logins addTarget:self action: @selector(loginClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.btn_resend_otp addTarget:self action: @selector(resendOtpClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.btn_signUp addTarget:self action: @selector(signUpClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.btn_forgotPass addTarget:self action: @selector(forgotPassClicked:) forControlEvents:UIControlEventTouchUpInside];
}
-(void)hideMainViewsAtIndex:(int)number{
    self.main_baseViewOne.hidden = (number == 1);
    self.main_baseViewTwo.hidden = (number == 2);
    self.main_baseViewThree.hidden = (number == 3);
}
-(void)hideSubViewsInThirdViewAtIndex:(int)number{
    self.main_baseViewThree_pass.hidden = (number == 1);
    self.main_baseViewThree_Otp.hidden = (number == 2);
}

#pragma mark - Button Action Using Selector

-(void)passOtpClicked:(id)sender{
    UIButton* butt = (UIButton*)sender;
    if (butt.tag == 1){
        isPassClick = !isPassClick;
        isOTPClick = NO;
    }else if (butt.tag == 2){
        isOTPClick = !isOTPClick;
        isPassClick = NO;
    }
    [self hideMainViewsAtIndex:3];
    [self setPassAndOtpButtonImage];
}
-(void)setPassAndOtpButtonImage{
    self.txt_password.text = @"";
    [self.btn_passWord setImage:[UIImage imageNamed:(isPassClick) ? @"circularBoxSelectGray" : @"circularBoxNoSelectGrayOne"] forState:UIControlStateNormal];
    [self.btn_otp setImage:[UIImage imageNamed:(isOTPClick) ? @"circularBoxSelectGray" : @"circularBoxNoSelectGrayOne"] forState:UIControlStateNormal];
}
-(void)continueClicked:(id)sender{
    [self.view endEditing:YES];
    if (![self validateEmailWithString:self.txt_email.text]){
       [self showToastsWithTitle:@"Plz Enter Valid Email Addres"];
        [self.txt_email resignFirstResponder];
        return;
    }
    if (isPassClick){
        [self hideSubViewsInThirdViewAtIndex:2];
        [self hideMainViewsAtIndex:2];
    }else if (isOTPClick){
        [self hideSubViewsInThirdViewAtIndex:1];
        [self hideMainViewsAtIndex:2];
        self.txt_password.text = @"25252525";
        [self loginCheckWithType:kSignInWithOtp withEmail:self.txt_email.text withPass:@""];
    }else{
        [self showToastsWithTitle:@"Select Any One option before Login"];
    }
    
}
-(void)securedButtClicked:(id)sender{
    isSecurePassText = !isSecurePassText;
    [self.txt_password setSecureTextEntry:isSecurePassText];
    [self showHideShowButtonImage:isSecurePassText];
}
-(void)showHideShowButtonImage:(BOOL)flag{
    [self.btn_showPassword setImage:[UIImage imageNamed:(isSecurePassText) ? @"LoginShowHidePassOne" : @"LoginHidePassOne"] forState:UIControlStateNormal];
}
-(void)shareFacebookClicked:(id)sender{
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
        login.loginBehavior = FBSDKLoginBehaviorWeb;
        [login logOut];
//    @[@"email",@"id",@"name",@"first_name",@"last_name",@"picture.type(large)"]
//    @"public_profile",@"email"
//    @"id,name,link,first_name, last_name, picture.type(large), birthday, bio ,location ,friends ,hometown , friendlists"
        [login
         logInWithReadPermissions: @[@"email",@"public_profile"]
         fromViewController:self
         handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
             if (error)
             {
                 
                 [self showButtonsInAlertWithTitle:@"Alert" msg:@"Unable to retreive emailid from facebook.please enter your email id here..!" style:UIAlertControllerStyleAlert buttArray:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString * _Nonnull strValue) {
                     if ([strValue isEqualToString:@"Ok"]){
                     }
                 }];
                 NSLog(@"Process error");
             }
             else if (result.isCancelled)
             {
                 NSLog(@"Cancelled");
                 [self showButtonsInAlertWithTitle:@"Alert" msg:@"Unable to retreive emailid from facebook.please enter your email id" style:UIAlertControllerStyleAlert buttArray:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString * _Nonnull strValue) {
                     if ([strValue isEqualToString:@"Ok"]){
                     }
                 }];
             }
             else {
                 NSLog(@"getBasicInfoWithResult %@",result);
                 [self getBasicInfoWithResult:result];
                 
             }
             
        }];
}
-(void)shareGoogleClicked:(id)sender{
    
    SignInViewController * signVC =   [[SignInViewController alloc] initWithNibName:@"SignInViewController" bundle:nil];
    signVC.googleViewDelegate =  self;
    [self.navigationController pushViewController:signVC animated:true];
    
//    [GIDSignIn sharedInstance].delegate = self;
//    [GIDSignIn sharedInstance].shouldFetchBasicProfile = YES;
//    [[GIDSignIn sharedInstance]signIn];
}
-(void)loginClicked:(id)sender{
    if (isPassClick){
    if ([self.txt_email.text length]==0 || [self.txt_password.text length]==0 ){
        [self showToastsWithTitle:@"All Field Are Mandatory"];
        return;
    }
    if (![self validateEmailWithString:self.txt_email.text]){
        [self showToastsWithTitle:@"Enter Valid Email ID"];
        return;
    }
    }
    [self loginCheckWithType:(isPassClick) ? kSignInWithPassword : (isOTPClick) ? kSignInWithPassword : @"" withEmail:self.txt_email.text withPass:self.txt_password.text];
}
-(void)resendOtpClicked:(id)sender{
    
}
-(void)signUpClicked:(id)sender{
    NewSignUpVC *signUp=[[NewSignUpVC alloc]initWithNibName:@"NewSignUpVC" bundle:nil];
    [self.navigationController pushViewController:signUp animated:YES];
}
-(void)forgotPassClicked:(id)sender{
    if (![self validateEmailWithString:self.txt_email.text]){
        [self showToastsWithTitle:@"Please Enter valid mail address"];
        return;
    }
    [self resetPasswordAstraAPI];
}
#pragma mark - TextField Delegaet

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if(textField == self.txt_otp)
    {
        if ([textField.text length]>3){
            return YES;
        }
    }
    return YES;
}


-(void)getTokenIDLoginVC{
        if([CoreUtility connected])
        {
            activityLoadingView = [LoadingView loadingViewInView:self.view withString:@"" andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
        @try{
            
            if ([SSKeychain passwordForService:KEYCHAIN_SERVICE account:KEYCHAIN_ACCOUNT] == nil){
                NSString *deviceId = [MobiculeUtilityManager getApplicationUUID];
                [SSKeychain setPassword:deviceId forService:KEYCHAIN_SERVICE account:KEYCHAIN_ACCOUNT];
            }
            NetworkHelper *networkHelper = [[NetworkHelper alloc] init];
            
            NSDictionary *headerDict = [[NSDictionary alloc] initWithObjectsAndKeys:[SSKeychain passwordForService:KEYCHAIN_SERVICE account:KEYCHAIN_ACCOUNT],@"uniqueId",@"mobicule",@"user", nil];
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                [networkHelper  getResponseWithRequestType:@"" withQueryParam:@"" withPathParam:@"" withJsonParam:nil withHeaders:headerDict withUrl:kAstraUrlNewToken success:^(NSDictionary *responseDict)
                 {
                     dispatch_async(dispatch_get_main_queue(), ^(void) {
                         [activityLoadingView removeFromSuperview];
                         NSLog(@"%@",responseDict);
                         if(responseDict){
                             if (responseDict.count > 0){
                                 NSDictionary *requestId  = [responseDict valueForKey:@"requestId"];
                                 NSDictionary *tokenId  = [responseDict valueForKey:@"tokenId"];
                                 
                                 if (requestId) {
                                     [[NSUserDefaults standardUserDefaults] setObject:requestId forKey:kuserDefaultRequestId];
                                 }
                                 if (tokenId){
                                     [[NSUserDefaults standardUserDefaults] setObject:tokenId forKey:kuserDefaultTokenId];
                                 }
                             }
                             else{
                                 [self showAlertViewWithTitle:@"Alert" withMessage:@"some error occurred"];
                             }
                         }
                         else{
                             [self showAlertViewWithTitle:@"Alert" withMessage:@"some error occurred"];
                         }
                     });
                 }
                                                   failure:^(NSError *error)
                 {
                     dispatch_async(dispatch_get_main_queue(), ^(void)
                                    {
                         [activityLoadingView removeFromSuperview];
                                        [self showAlertViewWithTitle:@"Alert" withMessage:@"some error occurred"];
                                        NSLog(@"Response Dict : %@",[error description]);
                                    });
                 }];
                
            });
        }
        @catch (NSException *exception){
            NSLog(@"%@", exception.reason);
            [activityLoadingView removeFromSuperview];
        }
        @finally{
            NSLog(@"exception finally called");
            [activityLoadingView removeFromSuperview];
        }
        }else{
            [self showButtonsInAlertWithTitle:@"Alert" msg:kMessageNoInternet style:UIAlertControllerStyleAlert buttArray:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString * _Nonnull strValue) {
                
            }];
        }
}

#pragma mark - Login Flow API

-(void)loginCheckWithType:(NSString *)signInType withEmail:(NSString*)emailString withPass:(NSString*)password{
    //abc@test.com/
    self.emailField = emailString;
    self.passField = password;
    activityLoadingView = [LoadingView loadingViewInView:self.view.superview.superview
                                              withString:@""
                                       andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
    if (![signInType isEqualToString:@""]){
    @try
    {
        NetworkHelper *helper = [NetworkHelper sharedHelper];
        NSString *pathParam = self.emailField;
        NSDictionary *headerDict = [CoreUtility getHeaderDict];
        [helper getResponseWithRequestType:@"" withQueryParam:@"" withPathParam:pathParam withJsonParam:nil withHeaders:headerDict withUrl:kAstraUrlLoginCheck success:^(NSDictionary *responseDict)
         {
             NSLog(@"Response Dict : %@",responseDict);
             dispatch_async(dispatch_get_main_queue(), ^(void)
             {
                 [activityLoadingView removeFromSuperview];
                 if (responseDict) {
                     if (responseDict.count>0) {
                         if ([signInType isEqualToString:@"signIn"] || [signInType isEqualToString:@"signUp"]) {
                             [self savePasswordWithType:signInType withEmail:self.emailField];
                             
                         }else if ([signInType isEqualToString:kSignInWithOtp]){
                             [self sendOtprequestWithUserName:self.emailField andWithLoginType:signInType];
                         }else if ([signInType isEqualToString:@"guestLogin"]){
                              [self netCoreDataForLoginWithEmailID:self.emailField]; //09-04-2018
                             [self showButtonsInAlertWithTitle:@"Alert" msg:@"User Signed in Successfully" style:UIAlertControllerStyleAlert buttArray:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString * _Nonnull strValue) {
                                 if ([strValue isEqualToString:@"Ok"]){
                                     [self setUserDetailsAndClosePopUp];
                                     LandingScreenVC *landing=[[LandingScreenVC alloc]initWithNibName:@"LandingScreenVC" bundle:nil];
                                     [self.navigationController pushViewController:landing animated:YES];
                                 }
                             }];
                         }
                         else if([signInType isEqualToString:@"ForgotPassword"]){
                              NSString *messege = [responseDict valueForKey:@"message"];
                             if ([[messege lowercaseString] isEqualToString:@"false"]) {
                                 [self showButtonsInAlertWithTitle:@"Alert" msg:@"Email Id is not registered with us." style:UIAlertControllerStyleAlert buttArray:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString * _Nonnull strValue) {
                                     if ([strValue isEqualToString:@"Ok"]){
                                        [self setUserDetailsAndClosePopUp];
                                     }
                                 }];
                             }
                             else{
                                 [self resetPasswordAstraAPI];
                             }
                         }
                     }
                 }
             });
         }
                                   failure:^(NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^(void)
                            {
                                [activityLoadingView removeFromSuperview];
                                NSLog(@"Response Dict : %@",[error description]);
                            });
         }];
    }
    @catch (NSException *exception){
        NSLog(@"%@", exception.reason);
        [activityLoadingView removeFromSuperview];
    }
    @finally{
        NSLog(@"exception finally called");
         [activityLoadingView removeFromSuperview];
    }
    }else{
        [self showToastsWithTitle:@"Enter Proper Email and Password"];
    }
}

-(void)getTokenWithcompletion:(void (^)( NSDictionary *_Nullable dictionary))completion{
        if([CoreUtility connected])
        {
        @try{
            
            if ([SSKeychain passwordForService:KEYCHAIN_SERVICE account:KEYCHAIN_ACCOUNT] == nil){
                NSString *deviceId = [MobiculeUtilityManager getApplicationUUID];
                [SSKeychain setPassword:deviceId forService:KEYCHAIN_SERVICE account:KEYCHAIN_ACCOUNT];
            }
            NetworkHelper *networkHelper = [[NetworkHelper alloc] init];
            
            NSDictionary *headerDict = [[NSDictionary alloc] initWithObjectsAndKeys:[SSKeychain passwordForService:KEYCHAIN_SERVICE account:KEYCHAIN_ACCOUNT],@"uniqueId",@"mobicule",@"user", nil];
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                [networkHelper  getResponseWithRequestType:@"" withQueryParam:@"" withPathParam:@"" withJsonParam:nil withHeaders:headerDict withUrl:kAstraUrlNewToken success:^(NSDictionary *responseDict)
                 {
                     dispatch_async(dispatch_get_main_queue(), ^(void) {
                         NSLog(@"%@",responseDict);
                         if(responseDict){
                             if (responseDict.count > 0){
                                 if ([responseDict valueForKey:@"requestId"]) {
                                     [[NSUserDefaults standardUserDefaults] setObject:[responseDict valueForKey:@"requestId"] forKey:kuserDefaultRequestId];
                                 }
                                 if ([responseDict valueForKey:@"tokenId"]){
                                     [[NSUserDefaults standardUserDefaults] setObject:[responseDict valueForKey:@"tokenId"] forKey:kuserDefaultTokenId];
                                 }
                                 completion(responseDict);
                             }
                             else{
                                 completion(nil);
                             }
                         }
                         else{
                             completion(nil);
                         }
                     });
                 }
                                                   failure:^(NSError *error)
                 {
                     dispatch_async(dispatch_get_main_queue(), ^(void)
                                    {
                                        NSLog(@"Response Dict : %@",[error description]);
                         completion(nil);
                                    });
                 }];
                
            });
        }
        @catch (NSException *exception){
            NSLog(@"%@", exception.reason);
//            completion(nil);
        }
        @finally{
            NSLog(@"exception finally called");
//            completion(nil);
        }
        }else{
//            completion(nil);
        }
}
-(void)savePasswordWithType:(NSString *)signInType withEmail:(NSString*)emails{
    
    activityLoadingView = [LoadingView loadingViewInView:self.view.superview.superview
                                              withString:@""
                                       andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
//    NSDictionary *requestId  = [responseDict valueForKey:@"requestId"];
//                                    NSDictionary *tokenId  = [responseDict valueForKey:@"tokenId"];
//    [self getTokenWithcompletion:^(NSDictionary * _Nullable dictionary) {
//        if (dictionary){
            @try
            {
                NetworkHelper *helper = [NetworkHelper sharedHelper];
                
               
//                if ([dictionary valueForKey:@"requestId"]) {
//                    [[NSUserDefaults standardUserDefaults] setObject:[dictionary valueForKey:@"requestId"] forKey:kLoginRequestID];
//                }
//                if ([dictionary valueForKey:@"tokenId"]){
//                    [[NSUserDefaults standardUserDefaults] setObject:[dictionary valueForKey:@"tokenId"] forKey:kLoginTokenID];
//                }
                
                NSDictionary *headerDict = [CoreUtility getHeaderDict];
                
//                 NSDictionary *headerDict = @{ @"requestId" : [dictionary valueForKey:@"requestId"], @"sessionId" : [dictionary valueForKey:@"tokenId"]};
                
                [helper getResponseWithRequestType:@"" withQueryParam:@"" withPathParam:@"" withJsonParam:nil withHeaders:headerDict withUrl:kAstraUrlSavePass success:^(NSDictionary *responseDict)
                 {
                     NSLog(@"Response Dict : %@",responseDict);
                     dispatch_async(dispatch_get_main_queue(), ^(void) {
                         [activityLoadingView removeFromSuperview];
                         
                         if (responseDict){
                             if (responseDict.count>0){
                                 NSString *keyString = [responseDict valueForKey:@"text"];
                                [self fetchEncryptionPassWithKey:keyString withloginType:signInType withEmail:emails];
                             }
                         }
                     });
                 }
                                           failure:^(NSError *error)
                 {
                     dispatch_async(dispatch_get_main_queue(), ^(void)
                                    {
                                        [activityLoadingView removeFromSuperview];
                                        NSLog(@"Response Dict : %@",[error description]);
                                    });
                 }];
            }
            @catch (NSException *exception)
            {
                NSLog(@"%@", exception.reason);
                [activityLoadingView removeFromSuperview];
            }
            @finally
            {
                NSLog(@"exception finally called");
                 [activityLoadingView removeFromSuperview];
            }
            
//        }else{
//            [self showButtonsInAlertWithTitle:@"Failure" msg:@"Some error occured" style:UIAlertControllerStyleAlert buttArray:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString * _Nonnull strValue) {
//                if ([strValue isEqualToString:@"Ok"]){
//                }
//            }];
//        }
//    }];
    

}
-(void)sendOtprequestWithUserName:(NSString *)userName andWithLoginType:(NSString *)loginType{
    
    activityLoadingView = [LoadingView loadingViewInView:self.view.superview.superview
                                              withString:@""
                                       andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
    
    @try
    {
        NetworkHelper *helper = [NetworkHelper sharedHelper];
        NSString *pathParam = self.emailField;
        NSDictionary *headerDict = [CoreUtility getHeaderDict];
        [helper getResponseWithRequestType:@"" withQueryParam:@"" withPathParam:pathParam withJsonParam:nil withHeaders:headerDict withUrl:kAstraUrlSendOtp success:^(NSDictionary *responseDict){
             NSLog(@"Response Dict : %@",responseDict);
             
             
             dispatch_async(dispatch_get_main_queue(), ^(void)
                            {
                                [activityLoadingView removeFromSuperview];
                                if (responseDict != nil && ![responseDict isEqual:[NSNull new]]){
                                  NSString * isValidUser = [responseDict objectForKey:@"text"];
                                    if   (isValidUser != nil && ![isValidUser isEqual:[NSNull new]] && ![isValidUser isEqualToString:@""] && [isValidUser caseInsensitiveCompare:@"true"] == NSOrderedSame){
                                        [self showButtonsInAlertWithTitle:@"Alert" msg:@"otp is sent to your registered email" style:UIAlertControllerStyleAlert buttArray:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString * _Nonnull strValue) {
                                            if ([strValue isEqualToString:@"Ok"]){
                                                [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
                                            }
                                        }];
                                        return;
                                    }
                                }
                 [self showButtonsInAlertWithTitle:@"Failure" msg:@"Some error occured" style:UIAlertControllerStyleAlert buttArray:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString * _Nonnull strValue) {
                     if ([strValue isEqualToString:@"Ok"]){
                         //[self setUserDetailsAndClosePopUp];
                     }
                 }];
                                return;
                            });
         }
            failure:^(NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^(void)
                            {
                                [activityLoadingView removeFromSuperview];
                                NSLog(@"Response Dict : %@",[error description]);
                            });
         }];
    }@catch (NSException *exception){
        NSLog(@"%@", exception.reason);
        [activityLoadingView removeFromSuperview];
    }
    @finally{
        NSLog(@"exception finally called");
        [activityLoadingView removeFromSuperview];
    }
    
}
-(void)resetPasswordAstraAPI
{
    //2. https://services-uatastra.thomascook.in/TcilMyAccount/login/forgetPassword/test@test.com
    activityLoadingView = [LoadingView loadingViewInView:self.view.superview.superview
                                              withString:@""
                                       andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
    
    @try
    {
        NSString *pathParameter = self.txt_email.text;
        NetworkHelper *helper = [NetworkHelper sharedHelper];
        NSDictionary *headerDict = [CoreUtility getHeaderDict];
        [helper getResponseWithRequestType:@"" withQueryParam:@"" withPathParam:pathParameter withJsonParam:nil withHeaders:headerDict withUrl:kAstraUrlForgotPassword success:^(NSDictionary *responseDict){
             NSLog(@"Response Dict : %@",responseDict);
             
             dispatch_async(dispatch_get_main_queue(), ^(void) {
                 [activityLoadingView removeFromSuperview];
                 if (responseDict){
                     if (responseDict.count>0){
                        // NSString *keyString = [responseDict valueForKey:@"text"];
                        // Success Response : {"accountType":"Password Set","custId":0,"message":"true"}
                         
                         NSString *messege = [responseDict valueForKey:@"message"];
                         if ([[messege lowercaseString] isEqualToString:@"true"]){
                             [self showButtonsInAlertWithTitle:@"Alert" msg:@"Your request for new password has been accepted.The new password would be emailed to the registered email address only.Please do write us at support@thomascook.in in case you any require any assistance." style:UIAlertControllerStyleAlert buttArray:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString * _Nonnull strValue) {
                                 if ([strValue isEqualToString:@"Ok"]){
                                     [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
                                 }
                             }];
                         }
                         else{
                             [self showButtonsInAlertWithTitle:@"Failure" msg:@"Some error occured" style:UIAlertControllerStyleAlert buttArray:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString * _Nonnull strValue) {
                                 if ([strValue isEqualToString:@"Ok"]){
                                     //[self setUserDetailsAndClosePopUp];
                                 }
                             }];

                         }
                     }
                 }
             });
         }
                                   failure:^(NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^(void)
                            {
                                [activityLoadingView removeFromSuperview];
                                NSLog(@"Response Dict : %@",[error description]);
                            });
         }];
    }
    @catch (NSException *exception){
        NSLog(@"%@", exception.reason);
        [activityLoadingView removeFromSuperview];
    }
    @finally{
        NSLog(@"exception finally called");
         [activityLoadingView removeFromSuperview];
    }
}

-(void)fetchEncryptionPassWithKey:(NSString *)key withloginType:(NSString *)loginType withEmail:(NSString*)emailID{
    activityLoadingView = [LoadingView loadingViewInView:self.view withString:@"" andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
    //            "key": "495kcG744o"
    NSLog(@"%@",self.txt_otp.text);
    NSString *otpText;
    if (@available(iOS 11.0, *)) {
        otpText = [NSString stringWithFormat:@"%@", self.kwVerifyCode.getVerificationCode];
        NSLog(@"otpText %@",otpText);
    }
//    NSString *paswords = (isPassClick) ? self.passField : (isOTPClick) ? self.txt_otp.text : @"";
    NSString *paswords = (isPassClick) ? self.passField : (isOTPClick) ? otpText : @"";
//    NSDictionary *dict = [[NSDictionary alloc]initWithObjectsAndKeys:key, @"key",self.passField,@"password",nil];
    NSDictionary *dict = [[NSDictionary alloc]initWithObjectsAndKeys:key, @"key",paswords,@"password",nil];
    NetworkHelper *helper = [NetworkHelper sharedHelper];
    if([CoreUtility connected])
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            NSString *strResponse = [helper getDataFromServerForType:@"sync" entity:@"encrypt" action:@"generate" andUserJson:dict];
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                [activityLoadingView removeFromSuperview];
                if (![strResponse isEqualToString:@"No Internet"]){
                    NSString *strStatus = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"Status"];
                    NSString *strMessage = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"message"];
                    
                    if ([[strStatus lowercaseString] isEqualToString:kStatusSuccess]){
                        
                        NSDictionary *dataDict = [[[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"data"] firstObject];
                        NSString *otpText;
                        if (@available(iOS 11.0, *)) {
                            otpText = [NSString stringWithFormat:@"%@", self.kwVerifyCode.getVerificationCode];
                            NSLog(@"otpText %@",otpText);
                        }
                        if ([loginType isEqualToString:@"signIn"]){
                            NSString *text1 = [dataDict valueForKey:@"text1"];
                            NSString *text2 = [dataDict valueForKey:@"text2"];
                            NSString *text3 = [dataDict valueForKey:@"text3"];
//                            NSString *ifOtp = (isOTPClick) ? self.txt_otp.text : @"";
                            NSString *ifOtp = (isOTPClick) ? otpText : @"";
                            NSString *newLoginType = (isPassClick) ? loginType : (isOTPClick) ? kSignInWithOtp : @"";
                            [self verifyCredentialsWithText1:text1 withText2:text2 withText3:text3 withloginType:newLoginType withOtp:ifOtp withEmail:emailID];
                            
                        }else{
                            // Jump To register View Controller
                        }
                    }
                    else{
                        [self showButtonsInAlertWithTitle:@"Alert" msg:strMessage style:UIAlertControllerStyleAlert buttArray:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString * _Nonnull strValue) {
                            
                        }];
                    }
                }
            });
        });
    }
    else{
        [activityLoadingView removeFromSuperview];
        [self showButtonsInAlertWithTitle:@"Alert" msg:kMessageNoInternet style:UIAlertControllerStyleAlert buttArray:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString * _Nonnull strValue) {
            
        }];
    }

}
-(void)verifyCredentialsWithText1:(NSString *)text1 withText2:(NSString *)text2 withText3:(NSString *)text3 withloginType:(NSString *)loginType withOtp:(NSString*)otp withEmail:(NSString*)emailId {
    
    LoadingView *activityLoadingView = [LoadingView loadingViewInView:self.view.superview.superview
                                                           withString:@""
                                                    andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
    
//    [self getTokenWithcompletion:^(NSDictionary * _Nullable dictionary) {
//        if (dictionary){
            
            @try
            {
                NetworkHelper *helper = [NetworkHelper sharedHelper];
                NSMutableDictionary *dictForRequest = [[NSMutableDictionary alloc] init];
                [dictForRequest setObject:emailId forKey:@"userId"];
                [dictForRequest setObject:text1 forKey:@"text1"];
                [dictForRequest setObject:text2 forKey:@"text2"];
                [dictForRequest setObject:text3 forKey:@"text3"];
                
                if ([loginType caseInsensitiveCompare:kSignInWithOtp] == NSOrderedSame){
                    [dictForRequest setObject:otp forKey:@"otp"];
                }
                NSDictionary *headerDict = [CoreUtility getHeaderDict];
                
                if ([headerDict valueForKey:@"requestId"]) {
                    [[NSUserDefaults standardUserDefaults] setObject:[headerDict valueForKey:@"requestId"] forKey:kLoginRequestID];
                }
                if ([headerDict valueForKey:@"sessionId"]){
                    [[NSUserDefaults standardUserDefaults] setObject:[headerDict valueForKey:@"sessionId"] forKey:kLoginTokenID];
                }
                
//                NSDictionary *headerDict = @{ @"requestId" : [dictionary valueForKey:@"requestId"], @"sessionId" : [dictionary valueForKey:@"tokenId"]};
                
                [helper getResponseWithRequestType:@"POST" withQueryParam:@"" withPathParam:@"" withJsonParam:dictForRequest withHeaders:headerDict withUrl:kAstraVerifyCredentials success:^(NSDictionary *responseDict){
                    NSLog(@"Response Dict : %@",responseDict);
                    dispatch_async(dispatch_get_main_queue(), ^(void) {
                        [activityLoadingView removeFromSuperview];
                        if (responseDict){
                            if (responseDict.count>0){
                                if ([[[responseDict valueForKey:@"message"] lowercaseString] isEqualToString:@"false"]){
                                    NSString *reasonOfMessage = [responseDict valueForKey:@"reasonOfMessage"];
                                    
                                    [self showButtonsInAlertWithTitle:@"Alert" msg:reasonOfMessage style:UIAlertControllerStyleAlert buttArray:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString * _Nonnull strValue) {
                                        if ([strValue isEqualToString:@"Ok"]){
                                            [self setUserDetailsAndClosePopUp];
                                        }
                                    }];
                                    
                                }
                                else{
                                    [self netCoreDataForLoginWithEmailID:emailId]; //09-04-2018
                                    
                                    NSDictionary * userDetailsDict = [responseDict valueForKey:@"userDetail"]; //09-04-2018 --
                                    
                                    if( userDetailsDict && ![userDetailsDict isEqual:[NSNull new]]) //09-04-2018 --
                                    {
                                        NSString * mobileNoString =[userDetailsDict valueForKey:@"mobileNo"];
                                        
                                        if (nil != mobileNoString && ![mobileNoString isEqual:[NSNull new]] && ![mobileNoString isEqualToString:@""])
                                        {
                                            [self setUserDetailsAndClosePopUp];
                                            [[NSUserDefaults standardUserDefaults]setValue:mobileNoString forKey:kuserDefaultTC_MobileNumber]; //09-04-2018 --
                                            [[NSUserDefaults standardUserDefaults]setValue:userDetailsDict forKey:kLoginUserDetails];
                                            [[NSUserDefaults standardUserDefaults]setValue:[userDetailsDict valueForKey:@"fname"] forKey:kUserFirstName];
//                                            [[NSUserDefaults standardUserDefaults]setValue:mobileNoString forKey:@"mobicleNumber"];
                                            [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"isCSSUserLoggedIn"];
                                            [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"isRegisterForCSSPushNotification"];
                                            [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"isCSSSubscribeForPushNotification"];
                                            self.firstNameField = [userDetailsDict valueForKey:@"fname"];
                                            [[NSUserDefaults standardUserDefaults] synchronize];
                                            
                                            //                                       [[NetCoreSharedManager sharedInstance] setUpIdentity:mobileNoString]; //24-04-2018 --
                                        }
                                    }
                                    
                                    [self showButtonsInAlertWithTitle:@"Alert" msg:@"Thank you, You have been successfully logged In" style:UIAlertControllerStyleAlert buttArray:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString * _Nonnull strValue) {
                                        if ([strValue isEqualToString:@"Ok"]){
                                            [self setUserDetailsAndClosePopUp];
                                            LandingScreenVC *landing=[[LandingScreenVC alloc]initWithNibName:@"LandingScreenVC" bundle:nil];
                                            [self.navigationController pushViewController:landing animated:YES];
                                        }
                                    }];
                                    
                                    
                                }
                            }
                        }
                    });
                    
                }
                                           failure:^(NSError *error)
                 {
                    dispatch_async(dispatch_get_main_queue(), ^(void)
                                   {
                        [activityLoadingView removeFromSuperview];
                        NSLog(@"Response Dict : %@",[error description]);
                    });
                }];
            }
            @catch (NSException *exception){
                NSLog(@"%@", exception.reason);
                [activityLoadingView removeFromSuperview];
            }@finally{
                NSLog(@"exception finally called");
                [activityLoadingView removeFromSuperview];
            }
            
            
//        }else{
//            [self showButtonsInAlertWithTitle:@"Failure" msg:@"Some error occured" style:UIAlertControllerStyleAlert buttArray:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString * _Nonnull strValue) {
//                if ([strValue isEqualToString:@"Ok"]){
//                }
//            }];
//        }
//
//
//    }];
    
    
}
#pragma mark - After Calling API save Value in Userdefaults

-(void)setUserDetailsAndClosePopUp{
    [[NSUserDefaults standardUserDefaults]setValue:self.emailField forKey:kuserDefaultUserId];
    [[NSUserDefaults standardUserDefaults]setValue:self.passField forKey:kuserDefaultPassword];
    NSUserDefaults *LoginStatus=[NSUserDefaults standardUserDefaults];
    [LoginStatus setObject:kLoginSuccess  forKey:kLoginStatus];
    NSUserDefaults *userIdForGooglePlusSignIn=[NSUserDefaults standardUserDefaults];
    
    [userIdForGooglePlusSignIn setObject:self.emailField forKey:kLoginEmailId];
    [userIdForGooglePlusSignIn setObject:self.passField forKey:kLoginPasswords];
    [userIdForGooglePlusSignIn setObject:self.imgUrlField forKey:kUserImageString];
    [userIdForGooglePlusSignIn setObject:self.firstNameField forKey:kUserFirstName];
  //  [[NetCoreSharedManager sharedInstance] setUpIdentity:self.textFieldUserId.text];
    
    //[self fetchMobileNumber];
    
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];

}
-(void)netCoreDataForLoginWithEmailID:(NSString *)emailID
{
    
    NSMutableDictionary *payloadList =  [NSMutableDictionary dictionary];
    [payloadList setObject:emailID forKey:@"EMAIL"];
    [NetCoreAnalyticsVC getNetCorePushNotificationDetailDictionary:payloadList withPayloadCount:102];
}

#pragma mark --------------- Facebook -----------------------
-(void)getBasicInfoWithResult:(FBSDKLoginManagerLoginResult *)result
{
    // loginOBJ.view.frame = CGRectMake(0, 0, 300, 455);
    
    if ([result.grantedPermissions containsObject:@"email"]){
        NSLog(@"result.grantedPermissions %@",result.grantedPermissions);
        if ([FBSDKAccessToken currentAccessToken]){
            NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
            [parameters setValue:@"id,name,email,first_name,last_name,picture.type(large)" forKey:@"fields"];

//            [parameters setValue:@"email" forKey:@"fields"];//public_profile
//            [parameters setValue:@"id" forKey:@"fields"];//id
//            [parameters setValue:@"name" forKey:@"fields"];//name
//            [parameters setValue:@"first_name" forKey:@"fields"];//first_name
//            [parameters setValue:@"last_name" forKey:@"fields"];//last_name
//            [parameters setValue:@"picture.type(large)" forKey:@"fields"];//public_profile
//            [parameters setValue:@"public_profile" forKey:@"fields"];
//            @{@"fields": @"id,name,link,first_name, last_name, picture.type(large), email, birthday, bio ,location ,friends ,hometown , friendlists"}
            
            [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:parameters]
             startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error){
                 if (!error){
                     NSLog(@"fetched user:%@", result);
                     NSString *email = [result valueForKey:@"email"];
                     NSString *pictureURL = [[NSString alloc] initWithFormat: @"http://graph.facebook.com/%@/picture?type=large", ([result objectForKey:@"id"]) ? [result valueForKey:@"id"] : @""];
//                     NSString *pictureURL = [NSString stringWithFormat:@"%@",[result objectForKey:@"picture"]];
                     NSString *firstName = ([result objectForKey:@"first_name"]) ? [result objectForKey:@"first_name"] : @"";
                     NSString *lastName = ([result objectForKey:@"last_name"]) ? [result objectForKey:@"last_name"] : @"";
                     NSLog(@"pictureURL user:%@", pictureURL);
                     NSLog(@"firstName user:%@", firstName);
                     NSLog(@"lastName user:%@", lastName);
                     NSLog(@"email is %@", [result objectForKey:@"email"]);
                     if (email){
                         NSUserDefaults *userIdForGooglePlusSignIn=[NSUserDefaults standardUserDefaults];
                         [userIdForGooglePlusSignIn setObject:email forKey:kLoginEmailId];
                         self.txt_email.text = [NSString stringWithFormat:@"%@", email];
                         self.emailField = [NSString stringWithFormat:@"%@", email];
                         self.imgUrlField = [NSString stringWithFormat:@"%@", pictureURL];
                         [self socialLoginWithFirstName:firstName withLastName:lastName withImageURl:pictureURL withType:@"FB" withUserID:email withID:@"fb1021596341229809"];
                     }else{
                         [self showButtonsInAlertWithTitle:@"Alert" msg:@"Unable to retreive emailid from facebook please enter your email id" style:UIAlertControllerStyleAlert buttArray:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString * _Nonnull strValue) {
                             if ([strValue isEqualToString:@"Ok"]){
                             }
                         }];
                     }
                 }
                 else{
                     NSLog(@"Unable to Login");
                     [self showButtonsInAlertWithTitle:@"Alert" msg:@"Unable to Login" style:UIAlertControllerStyleAlert buttArray:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString * _Nonnull strValue) {
                         if ([strValue isEqualToString:@"Ok"]){
                         }
                     }];
                 }
                 
             }];
        }
    }
}
#pragma mark ------------ Social Login ------------
-(void)socialLoginWithFirstName:(NSString *)firstName withLastName:(NSString *)lastName withImageURl:(NSString *)imageURL withType:(NSString *)type withUserID:(NSString *)userID withID:(NSString *)Id
{
    activityLoadingView = [LoadingView loadingViewInView:self.view.superview.superview
                                              withString:@""
                                       andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
    @try
    {
        NetworkHelper *helper = [NetworkHelper sharedHelper];
        NSMutableDictionary *dictForRequest = [[NSMutableDictionary alloc] init];
        [dictForRequest setObject:firstName forKey:@"firstName"];
        [dictForRequest setObject:Id forKey:@"id"];
        [dictForRequest setObject:imageURL forKey:@"imgUrl"];
        [dictForRequest setObject:lastName forKey:@"lastName"];
        [dictForRequest setObject:type forKey:@"type"];
        [dictForRequest setObject:userID forKey:@"userId"];
        
        NSDictionary *headerDict = [CoreUtility getHeaderDict];
        if ([headerDict valueForKey:@"requestId"]) {
            [[NSUserDefaults standardUserDefaults] setObject:[headerDict valueForKey:@"requestId"] forKey:kLoginRequestID];
        }
        if ([headerDict valueForKey:@"sessionId"]){
            [[NSUserDefaults standardUserDefaults] setObject:[headerDict valueForKey:@"sessionId"] forKey:kLoginTokenID];
        }
        [self showToastsWithTitle:@"Verifying Social Login"];
        [helper getResponseWithRequestType:@"POST" withQueryParam:@"" withPathParam:@"" withJsonParam:dictForRequest withHeaders:headerDict withUrl:kAstraSocialLogin success:^(NSDictionary *responseDict)
         {
             NSLog(@"Response Dict : %@",responseDict);
             dispatch_async(dispatch_get_main_queue(), ^(void) {
                 [activityLoadingView removeFromSuperview];
                 if (responseDict){
                     if (responseDict.count>0){
                         NSDictionary * userDetailsDict = [responseDict valueForKey:@"userDetail"]; //09-04-2018 --
                         if( userDetailsDict && ![userDetailsDict isEqual:[NSNull new]]) //09-04-2018 --
                         {
                             NSString * mobileNoString =[userDetailsDict valueForKey:@"mobileNo"];
                             
                             if (nil != mobileNoString && ![mobileNoString isEqual:[NSNull new]] && ![mobileNoString  isEqualToString:@""]){
                                 
                                 [[NSUserDefaults standardUserDefaults]setValue:mobileNoString forKey:kuserDefaultTC_MobileNumber]; //09-04-2018 --
                                 
                                 
                                 [[NetCoreSharedManager sharedInstance] setUpIdentity:mobileNoString];//24-04-2018 --
                             }
                             [[NSUserDefaults standardUserDefaults]setValue:userDetailsDict forKey:kLoginUserDetails];
                             [[NSUserDefaults standardUserDefaults]setValue:[userDetailsDict valueForKey:@"fname"] forKey:kUserFirstName];
                             [[NSUserDefaults standardUserDefaults] synchronize];
                         }
                         [self netCoreDataForLoginWithEmailID:self.txt_email.text];
                         self.emailField = userID;
                         self.imgUrlField = imageURL;
                         self.firstNameField = firstName;
                         [self showButtonsInAlertWithTitle:@"Success" msg:@"User Signed in Successfully" style:UIAlertControllerStyleAlert buttArray:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString * _Nonnull strValue) {
                             if ([strValue isEqualToString:@"Ok"]){
                                 [self setUserDetailsAndClosePopUp];
                                 LandingScreenVC *landing=[[LandingScreenVC alloc]initWithNibName:@"LandingScreenVC" bundle:nil];
                                 [self.navigationController pushViewController:landing animated:YES];
                             }
                         }];
                     }
                 }else{
                     [self showButtonsInAlertWithTitle:@"Failure" msg:@"Some error occured" style:UIAlertControllerStyleAlert buttArray:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString * _Nonnull strValue) {
                         if ([strValue isEqualToString:@"Ok"]){
                             [self setUserDetailsAndClosePopUp];
                         }
                     }];
                 }
             });
         }
                                   failure:^(NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^(void)
                            {
                                [activityLoadingView removeFromSuperview];
                 [self showButtonsInAlertWithTitle:@"Failure" msg:[error description] style:UIAlertControllerStyleAlert buttArray:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString * _Nonnull strValue) {
                     if ([strValue isEqualToString:@"Ok"]){
                         [self setUserDetailsAndClosePopUp];
                     }
                 }];
                            });
         }];
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"%@", exception.reason);
        [activityLoadingView removeFromSuperview];
    }
    @finally
    {
        NSLog(@"exception finally called");
        [activityLoadingView removeFromSuperview];
    }
}
#pragma mark ------------- Google login delegates --------
-(void)didFinishWithGoogleLogin:(NSString *)userEmailID{
    NSLog(@"Signed in user");
    self.UserEmailID = userEmailID;
    [self checkedForGoogleLogin];
    NSLog(@"");
}
-(void)checkedForGoogleLogin{
    if ([self.UserEmailID isEqual:[NSNull class]] || [self.UserEmailID length] == 0 || !self.UserEmailID.length){
        [self showButtonsInAlertWithTitle:@"Alert" msg:@"Unable to retreive emailid from Google plus.please enter your email id here..!" style:UIAlertControllerStyleAlert buttArray:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString * _Nonnull strValue) {
            if ([strValue isEqualToString:@"Ok"]){
                [self setUserDetailsAndClosePopUp];
            }
        }];
    }else if ([self.UserEmailID isEqualToString:@"error"]){
        
        [self showButtonsInAlertWithTitle:@"Alert" msg:@"Unable to retreive emailid from Google plus.please enter your email id here..!" style:UIAlertControllerStyleAlert buttArray:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString * _Nonnull strValue) {
            if ([strValue isEqualToString:@"Ok"]){
                [self setUserDetailsAndClosePopUp];
            }
        }];
        
        NSLog(@"Process error");
    }else{
        NSUserDefaults *userIdForGooglePlusSignIn=[NSUserDefaults standardUserDefaults];
        
        [userIdForGooglePlusSignIn setObject:self.UserEmailID forKey:kLoginEmailId];
        
        self.txt_email.text = self.UserEmailID;
        GIDGoogleUser * googleUser = [[GIDSignIn sharedInstance] currentUser];
        
        NSString *emailId = googleUser.profile.email;
        
        NSString *name = googleUser.profile.name;
        CGSize thumbSize=CGSizeMake(500, 500);
        NSString *imgString = nil;
        if (googleUser.profile.hasImage){
            NSUInteger dimension = round(thumbSize.width * [[UIScreen mainScreen] scale]);
            NSURL *imageURL = [googleUser.profile imageURLWithDimension:dimension];
            NSLog(@"image url=%@",imageURL);
            imgString = imageURL.absoluteString;
        }
        
        [self socialLoginWithFirstName:name withLastName:name withImageURl:(imgString) ? imgString : @"" withType:@"GP" withUserID:emailId withID:@"327645202876-3pmqts5pkaaqqvhh1gjh52k7sbuv468a.apps.googleusercontent.com"];
    }
    [[SlideNavigationController sharedInstance] popViewControllerAnimated:YES];
}

- (void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user withError:(NSError *)error {
    // Perform any operations on signed in user here.
    
    
    NSLog(@"Signed in user");
    
    if (error){
        [self showButtonsInAlertWithTitle:@"Alert" msg:@"Unable to retreive emailid from Google plus.please enter your email id here..!" style:UIAlertControllerStyleAlert buttArray:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString * _Nonnull strValue) {
            if ([strValue isEqualToString:@"Ok"]){
                [self setUserDetailsAndClosePopUp];
            }
        }];
    }else{
        GIDAuthentication *authentication = user.authentication;
        FIRAuthCredential *credential =
        [FIRGoogleAuthProvider credentialWithIDToken:authentication.idToken
                                         accessToken:authentication.accessToken];
        [[FIRAuth auth] signInWithCredential:credential
                                  completion:^(FIRUser *user, NSError *error) {
                                      NSLog(@"Authentication Done");
                                  }];
        NSLog(@"email : %@",[GIDSignIn sharedInstance].currentUser.profile.email);
        if(DEBUG_ENABLED){
            debug(@"email : %@",[GIDSignIn sharedInstance].currentUser.profile.email);
        }
        if ([GIDSignIn sharedInstance].currentUser.profile.email){
//            NSUserDefaults *userIdForGooglePlusSignIn=[NSUserDefaults standardUserDefaults];
//            [userIdForGooglePlusSignIn setObject:[GIDSignIn sharedInstance].currentUser.profile.email forKey:kLoginEmailId];
//            [userIdForGooglePlusSignIn setObject:[GIDSignIn sharedInstance].currentUser.profile.name forKey:kUserFirstName];
            self.emailField = [GIDSignIn sharedInstance].currentUser.profile.email;
            self.firstNameField = [GIDSignIn sharedInstance].currentUser.profile.name;
            CGSize thumbSize=CGSizeMake(500, 500);
            if ([GIDSignIn sharedInstance].currentUser.profile.hasImage){
                NSUInteger dimension = round(thumbSize.width * [[UIScreen mainScreen] scale]);
                NSURL *imageURL = [[GIDSignIn sharedInstance].currentUser.profile imageURLWithDimension:dimension];
                NSLog(@"image url=%@",imageURL);
                self.imgUrlField = imageURL.absoluteString;
            }
            
            [self fetchMobileNumber];
            [self showButtonsInAlertWithTitle:@"Success" msg:@"User Signed in Successfully" style:UIAlertControllerStyleAlert buttArray:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString * _Nonnull strValue) {
                if ([strValue isEqualToString:@"Ok"]){
                    [self dismissPopupViewControllerWithanimationType];
                    [self setUserDetailsAndClosePopUp];
                    LandingScreenVC *landing=[[LandingScreenVC alloc]initWithNibName:@"LandingScreenVC" bundle:nil];
                    [self.navigationController pushViewController:landing animated:YES];
                }
            }];
            self.txt_email.text=[GIDSignIn sharedInstance].currentUser.profile.email;
        }
        NSString *userId = user.userID;                  // For client-side use only!
        NSString *idToken = user.authentication.idToken; // Safe to send to the server
        NSString *name = user.profile.name;
        NSString *email = user.profile.email;
        NSLog(@"Customer details: %@ %@ %@ %@", userId, idToken, name, email);
        
    }
}
-(void)dismissPopupViewControllerWithanimationType{
    LandingScreenVC *landing=[[LandingScreenVC alloc]initWithNibName:@"LandingScreenVC" bundle:nil];
    [self.navigationController pushViewController:landing animated:YES];
}
-(void)fetchMobileNumber{
    NSUserDefaults *userIdForGooglePlusSignIn=[NSUserDefaults standardUserDefaults];
    NSString *userId=[userIdForGooglePlusSignIn valueForKey:kLoginEmailId];
    NSDictionary *dictOfData = [NSDictionary dictionaryWithObjectsAndKeys:userId,@"userName",nil];
    activityLoadingView = [LoadingView loadingViewInView:self.view withString:@"" andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
    
    if ([self connected]){
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NetworkHelper *helper = [NetworkHelper sharedHelper];
            NSString *strResponse = [helper getDataFromServerForType:@"webservice" entity:@"myAccountInfo" action:@"search" andUserJson:dictOfData];
            NSString *strStatus = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"status"];
            NSDictionary *dictOfData=[[NSDictionary alloc]init];
            if ([strStatus isEqualToString:kStatusSuccess]){
                dictOfData= [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:kServerResponseKeyData];
                NSString *phoneNo = [dictOfData valueForKeyPath:@"profileDetails.phoneNo"];
                [userIdForGooglePlusSignIn setObject:phoneNo forKey:kPhoneNo];
                if (phoneNo){
                    CoreUtility *coreobj=[CoreUtility sharedclassname];
                    [coreobj.strPhoneNumber addObject:phoneNo];
                    if( nil != phoneNo && ![phoneNo isEqual:[NSNull new]] && ![phoneNo isEqualToString:@""])
                    {
                        [[NSUserDefaults standardUserDefaults]setValue:phoneNo  forKey:kuserDefaultTC_MobileNumber]; //09-04-2018 --
                        [[NSUserDefaults standardUserDefaults]synchronize];
                        
                        [[NetCoreSharedManager sharedInstance] setUpIdentity:phoneNo];
                    }
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [activityLoadingView removeView];
                });
                
            }
            else
            {
                
            }
        });
    }
    else
    {
        [activityLoadingView removeView];
    }
}
- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}

//Adding Apple Sign In
//If user login using apple sign in then it login successfully for first time
//but if user logout and try again using apple sign in then apple setting not provide email id again it only provide user id (in string format)
//So at device side we implement one logic
//When user login first time in app we saved all apple sign in details in user defaults
//if user logout and try again apple sign in we fetch user details from stored data and do login using stored email id
#pragma mark - Delegate Of Apple Signing

- (void)authorizationController:(ASAuthorizationController *)controller didCompleteWithAuthorization:(ASAuthorization *)authorization  API_AVAILABLE(ios(13.0)){
    
    NSLog(@"%s", __FUNCTION__);
    NSLog(@"%@", controller);
    NSLog(@"%@", authorization);
    
    NSLog(@"authorization.credential：%@", authorization.credential);
    if ([authorization.credential isKindOfClass:[ASAuthorizationAppleIDCredential class]]) {
        // ASAuthorizationAppleIDCredential
        ASAuthorizationAppleIDCredential *appleIDCredential = authorization.credential;
        NSString *user = appleIDCredential.user;
        [[NSUserDefaults standardUserDefaults] setValue:user forKey:setCurrentIdentifier];
        NSString *familyName = appleIDCredential.fullName.familyName;
        NSLog(@"familyName：%@", familyName);
        NSString *givenName = appleIDCredential.fullName.givenName;
        NSLog(@"givenName：%@", givenName);
        NSString *email = appleIDCredential.email;
        NSLog(@"email：%@", email);
        [self showToastsWithTitle:@"First Possibility" withMessage:[NSString stringWithFormat:@"%@",authorization.credential]];
        if(email == nil || email == [NSNull class] || [[email uppercaseString] isEqualToString:@"(NULL)"]){
            NSDictionary *existDetsil = [[NSUserDefaults standardUserDefaults]objectForKey:kAppleSignInDetailsOfDevice];
            if(existDetsil != nil){
                NSString *firstName = ([existDetsil objectForKey:@"firstName"] == nil) ? @"" : [existDetsil objectForKey:@"firstName"];
                NSString *lastName = ([existDetsil objectForKey:@"lastName"] == nil) ? @"" : [existDetsil objectForKey:@"lastName"];
                NSString *email = ([existDetsil objectForKey:@"email"] == nil) ? @"" : [existDetsil objectForKey:@"email"];
                if ([email length]>0){
                   [self socialLoginWithFirstName:firstName withLastName:lastName withImageURl:@"" withType:@"AP" withUserID:email withID:@"TEST"];
                }else{
                    [self showButtonsInAlertWithTitle:@"Failure" msg:@"Not Able To Sign-In Using Current Apple Id" style:UIAlertControllerStyleAlert buttArray:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString * _Nonnull strValue) {

                    }];
                }
            }else{
                [self showButtonsInAlertWithTitle:@"Failure" msg:@"Not Able To Sign-In Using Apple Id" style:UIAlertControllerStyleAlert buttArray:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString * _Nonnull strValue) {

                }];
            }
        }else{
            NSMutableDictionary *signDict = [[NSMutableDictionary alloc] init];
            [signDict setObject:(givenName == nil) ? @"" : givenName forKey:@"firstName"];
            [signDict setObject:(familyName == nil) ? @"" : familyName  forKey:@"lastName"];
            [signDict setObject:(email == nil) ? @"" : email forKey:@"email"];
            [signDict setObject:(user == nil) ? @"" : user forKey:@"uniqueId"];
            [[NSUserDefaults standardUserDefaults]setObject:signDict forKey:kAppleSignInDetailsOfDevice];
            [[NSUserDefaults standardUserDefaults]synchronize];
            [[NSUserDefaults standardUserDefaults]objectForKey:kAppleSignInDetailsOfDevice];
            [self socialLoginWithFirstName:givenName withLastName:familyName withImageURl:@"" withType:@"AP" withUserID:email withID:@"TEST"];
        }
//        [self showButtonsInAlertWithTitle:@"First Possibility" msg:[NSString stringWithFormat:@"%@",authorization.credential] style:UIAlertControllerStyleAlert buttArray:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString * _Nonnull strValue) {
//
//        }];
    } else if ([authorization.credential isKindOfClass:[ASPasswordCredential class]]) {
        ASPasswordCredential *passwordCredential = authorization.credential;
        NSString *user = passwordCredential.user;
        NSString *password = passwordCredential.password;
        NSLog(@"user：%@", user);
        NSLog(@"password：%@", password);
        [self showToastsWithTitle:@"Second Possibility" withMessage:[NSString stringWithFormat:@"%@",authorization.credential]];
        [self socialLoginWithFirstName:user withLastName:user withImageURl:@"" withType:@"AP" withUserID:user withID:@"TEST"];
//        [self showButtonsInAlertWithTitle:@"Second Possibility" msg:[NSString stringWithFormat:@"%@",authorization.credential] style:UIAlertControllerStyleAlert buttArray:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString * _Nonnull strValue) {
//
//        }];
    } else {
        NSLog(@"Not Able To Sign In With Apple");
        
        [self showToastsWithTitle:@"Third Possibility" withMessage:@"Due To Some Reason Not Able To Sign In With Apple"];
        [self showButtonsInAlertWithTitle:@"Failure" msg:@"Due To Some Reason Not Able To Sign In With Apple" style:UIAlertControllerStyleAlert buttArray:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString * _Nonnull strValue) {

        }];
    }
    
//    [self showButtonsInAlertWithTitle:@"Fourth Possibility" msg:@"Not Going In Loop" style:UIAlertControllerStyleAlert buttArray:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString * _Nonnull strValue) {
//
//    }];
//    [self showToastsWithTitle:@"Fourth Possibility" withMessage:@"Not Going In Loop"];
}
 

- (void)authorizationController:(ASAuthorizationController *)controller didCompleteWithError:(NSError *)error  API_AVAILABLE(ios(13.0)){
    
    NSLog(@"%s", __FUNCTION__);
    NSLog(@"error ：%@", error);
    NSString *errorMsg = nil;
    switch (error.code) {
        case ASAuthorizationErrorCanceled:
            errorMsg = @"ASAuthorizationErrorCanceled";
            break;
        case ASAuthorizationErrorFailed:
            errorMsg = @"ASAuthorizationErrorFailed";
            break;
        case ASAuthorizationErrorInvalidResponse:
            errorMsg = @"ASAuthorizationErrorInvalidResponse";
            break;
        case ASAuthorizationErrorNotHandled:
            errorMsg = @"ASAuthorizationErrorNotHandled";
            break;
        case ASAuthorizationErrorUnknown:
            errorMsg = @"ASAuthorizationErrorUnknown";
            break;
    }
    if (errorMsg || error.localizedDescription) {
//        NSLog(@"localizedDescription Copy：%@", error.localizedDescription);
//        [self showButtonsInAlertWithTitle:@"Failure" msg:@"Some Error Occured" style:UIAlertControllerStyleAlert buttArray:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString * _Nonnull strValue) {
//
//        }];
//        return;
        [self showToastsWithTitle:@"ASAuthorizationErrorUnknown Found"];
    }
    
//    [self showButtonsInAlertWithTitle:@"Failure" msg:@"Some Error Occured" style:UIAlertControllerStyleAlert buttArray:[NSArray arrayWithObjects:@"Ok", nil] completion:^(NSString * _Nonnull strValue) {
//
//    }];
   
}
 
//! Tells the delegate from which window it should present content to the user.
- (ASPresentationAnchor)presentationAnchorForAuthorizationController:(ASAuthorizationController *)controller  API_AVAILABLE(ios(13.0)){
    
    NSLog(@"window：%s", __FUNCTION__);
    return self.view.window;
}

- (void)dealloc {
    
    if (@available(iOS 13.0, *)) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:ASAuthorizationAppleIDProviderCredentialRevokedNotification object:nil];
    }
}

@end
