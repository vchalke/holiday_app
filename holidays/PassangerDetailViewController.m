//
//  PassangerDetailViewController.m
//  holidays
//
//  Created by ketan on 26/10/15.
//  Copyright © 2015 Mobicule. All rights reserved.
//

#import "PassangerDetailViewController.h"
#import "TermsAndCondViewController.h"
#import "WebViewController.h"
#import "LoginViewPopUp.h"
#import "UIViewController+MJPopupViewController.h"
#import "NetCoreAnalyticsVC.h"
//#import <NetCorePush/NetCorePush.h>

@interface PassangerDetailViewController ()
{
    NSMutableArray *arrayForBool;
    NSMutableArray *cellArray;
    UILabel *preLabel;
    UIView *previousTapview;
    NSMutableArray *arrayRoomWise;
    int currentRoomSelected;
    UIView *dateView;
    UIDatePicker *datePicker1;
    UITextField *textFieldDateOfBirth;
    AdressPopUpVC *addressDetails;
    NSMutableArray *tapGestureArray;
    int currentSection;
    int previousIndexPath;
    int adultCountForIndexing;
    int childCountforIndexing;
    int infantCountForIndexing;
    NSMutableDictionary *profileDetail;
}
@end

@implementation PassangerDetailViewController
@synthesize roomRecordArray,DictJourneyDate,dictForCalculation;

- (void)viewDidLoad
{
    [super viewDidLoad];
    [CoreUtility reloadRequestID];

    [super setHeaderTitle:@"Traveller Details"];
    // Do any additional setup after loading the view from its nib.
    [[NSBundle mainBundle]loadNibNamed:@"PassangerDetailViewController" owner:self options:nil];
    [super addViewInBaseView:self.passangerDetailsView];
    currentRoomSelected = 1;
    arrayRoomWise = [[NSMutableArray alloc]init];
    for (int  i = 1; i <= roomRecordArray.count ; i++)
    {
        NSPredicate *pred = [NSPredicate predicateWithBlock:^BOOL(TravellerInformationModel* evaluatedObject, NSDictionary *bindings) {
            
            if(evaluatedObject.travellerRoomNumber == i)
            {
                evaluatedObject.cell = nil;
                return true;
            }
            return false;
        }];
        
        NSArray *results = [_travellerArrayForCalculation filteredArrayUsingPredicate:pred];
        if (results.count != 0)
        {
            
            [arrayRoomWise addObject:results];
        }
    }
    
    if (!arrayForBool)
    {
        arrayForBool = [[NSMutableArray alloc]init];
        for (int i = 0 ; i < [arrayRoomWise[0] count]; i++)
        {
            if(i == 0)
            {
                [arrayForBool addObject:[NSNumber numberWithBool:YES]];
            }
            else
            {
                [arrayForBool addObject:[NSNumber numberWithBool:NO]];
            }
            
        }
    }
    
    tapGestureArray = [[NSMutableArray alloc]init];
    [self plotMenuButtons];
    if (roomRecordArray.count == 1)
    {
        if (addressDetails == nil)
        {
            [self.proceedButton setTitle:[NSString stringWithFormat:@"Address of communication"] forState:UIControlStateNormal];
        }else
        {
            [self.proceedButton setTitle:[NSString stringWithFormat:@"Payment"] forState:UIControlStateNormal];
        }
    }
    profileDetail = [[NSMutableDictionary alloc] init];
    [self.tableViewForPassangerDetail reloadData];
    
    
}

-(void)setArrayForCountWithArray:(NSArray *)array
{
    infantCountForIndexing = 0;
    adultCountForIndexing = 0;
    childCountforIndexing = 0;
    
    for (int i = 0; i< array.count; i++)
    {
        TravellerInformationModel *model = [array objectAtIndex:i];
        NSString *passangerType = model.travellerType;
        if ([passangerType isEqualToString:kTravellerTypeAdult])
        {
            adultCountForIndexing += 1;
        }
        else if ([passangerType isEqualToString:kTravellerTypeChild])
        {
            childCountforIndexing += 1;
        }
        else if ([passangerType isEqualToString:kTravellerTypeInfant])
        {
            infantCountForIndexing += 1;
        }
    }
}

-(void)plotMenuButtons
{
    [[_scrollViewForRoomSelection subviews]
     makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    for (int i=0; i< roomRecordArray.count ; i++)
    {
        UILabel *lblNumber=[[UILabel alloc]initWithFrame:CGRectMake(i*40, 0, 30, 30)];
        lblNumber.text= [NSString stringWithFormat:@"%d",i+1];
        lblNumber.tag = 1;
        lblNumber.textAlignment=NSTextAlignmentCenter;
        lblNumber.textColor=[UIColor lightGrayColor];
        [lblNumber setUserInteractionEnabled:YES];
        UITapGestureRecognizer *tap= [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapNumber:)];
        tap.numberOfTapsRequired=1;
        [lblNumber addGestureRecognizer:tap];
        [_scrollViewForRoomSelection addSubview:lblNumber];
        if (i==0)
        {
            [self tapNumber:tap];
        }
        [tapGestureArray addObject:tap];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)tapNumber:(UITapGestureRecognizer*)tap{
    id lab= [tap view];
    UILabel *label=(UILabel *)lab;
    
    
    if ([label.text intValue] != 1)
    {
        BOOL isValid = [self validationOfTravellersInfoWithRoomNo:[label.text intValue]-2];
        if (isValid)
        {
            label.textColor=[UIColor whiteColor];
            UIView *tapview = [[UIView alloc]init];
            tapview.frame = label.frame;
            tapview.layer.cornerRadius=tapview.bounds.size.height/2;
            tapview.backgroundColor = [UIColor colorFromHexString:@"#0197D4"];
            tapview.clipsToBounds=YES;
            
            
            if (preLabel)
            {
                previousTapview.backgroundColor = [UIColor whiteColor];
                preLabel.textColor=[UIColor lightGrayColor];
                
            }
            else
            {
                tapview.backgroundColor = [UIColor colorFromHexString:@"#0197D4"];
            }
            previousTapview = tapview;
            preLabel = label;
            currentRoomSelected = [label.text intValue];
            //tapview.backgroundColor = [UIColor colorFromHexString:@"#0197D4"];
            
            [_scrollViewForRoomSelection addSubview:tapview];
            [_scrollViewForRoomSelection bringSubviewToFront:label];
            
            arrayForBool = [[NSMutableArray alloc]init];
            
            previousIndexPath = 0;
            
            for (int i = 0 ; i < [arrayRoomWise[currentRoomSelected-1] count]; i++)
            {
                if(i == 0)
                {
                    [arrayForBool addObject:[NSNumber numberWithBool:YES]];
                }else
                {
                    [arrayForBool addObject:[NSNumber numberWithBool:NO]];
                }
                
            }
            
            [self.tableViewForPassangerDetail reloadData];
            
            if (roomRecordArray.count ==  [label.text intValue])
            {
                if (addressDetails == nil) {
                    [self.proceedButton setTitle:[NSString stringWithFormat:@"Address of communication"] forState:UIControlStateNormal];
                }else
                {
                    [self.proceedButton setTitle:[NSString stringWithFormat:@"Payment"] forState:UIControlStateNormal];
                }
                
            }else
            {
                [self.proceedButton setTitle:[NSString stringWithFormat:@"Continue to Room %d",[label.text intValue]+1] forState:UIControlStateNormal];
            }
            
            
        }
//        else
//        {
//            NSLog(@"Error");
//            [super showAlertViewWithTitle:@"Alert" withMessage:[NSString stringWithFormat:@"Please enter all information of All travellers from room number %d",[label.text intValue]-1]];
//            
//            //            if (![[self.proceedButton titleLabel].text isEqualToString:@"Payment"])
//            //            {
//            //
//            //            }
//            
//        }
    }
    else
    {
        label.textColor=[UIColor whiteColor];
        UIView *tapview = [[UIView alloc]init];
        tapview.frame = label.frame;
        tapview.layer.cornerRadius=tapview.bounds.size.height/2;
        tapview.backgroundColor = [UIColor colorFromHexString:@"#0197D4"];
        tapview.clipsToBounds=YES;
        
        
        if (preLabel)
        {
            previousTapview.backgroundColor = [UIColor whiteColor];
            preLabel.textColor=[UIColor lightGrayColor];
            
        }
        else
        {
            tapview.backgroundColor = [UIColor colorFromHexString:@"#0197D4"];
        }
        previousTapview = tapview;
        preLabel = label;
        currentRoomSelected = [label.text intValue];
        //tapview.backgroundColor = [UIColor colorFromHexString:@"#0197D4"];
        
        [_scrollViewForRoomSelection addSubview:tapview];
        [_scrollViewForRoomSelection bringSubviewToFront:label];
        
        arrayForBool = [[NSMutableArray alloc]init];
        
        for (int i = 0 ; i < [arrayRoomWise[currentRoomSelected-1] count]; i++)
        {
            if(i == 0)
            {
                [arrayForBool addObject:[NSNumber numberWithBool:YES]];
            }else
            {
                [arrayForBool addObject:[NSNumber numberWithBool:NO]];
            }
            
        }
        
        [self.tableViewForPassangerDetail reloadData];
        if (roomRecordArray.count ==  [label.text intValue])
        {
            [self.proceedButton setTitle:[NSString stringWithFormat:@"Payment"] forState:UIControlStateNormal];
        }else
        {
            [self.proceedButton setTitle:[NSString stringWithFormat:@"Continue to Room %d",[label.text intValue]+1] forState:UIControlStateNormal];
        }
    }
    
    
    NSLog(@"Tap label");
}

-(BOOL)validationOfTravellersInfoWithRoomNo:(int)roomNo
{
    NSArray *arrayOfTraveller =  arrayRoomWise[roomNo];
    for (int i = 0; i<arrayOfTraveller.count; i++)
    {
        TravellerInformationModel *model = [arrayOfTraveller objectAtIndex:i];
        PassangerDetailCell *cell = model.cell;
        
        if (cell == nil)
        {
            return NO;
        }
        else if([cell.textFieldDOB.text isEqualToString:@""])
        {
          [super showAlertViewWithTitle:@"Alert" withMessage:[NSString stringWithFormat:@"Please Enter Date Of Birth of Traveller From Room Number %d",roomNo+1]];
            return NO;
        }
        else if ([cell.textFieldfirstName.text isEqualToString:@""])
        {
             [super showAlertViewWithTitle:@"Alert" withMessage:[NSString stringWithFormat:@"Please Enter First Name of Traveller From Room Number %d",roomNo+1]];
            return NO;
        }
        else if ([cell.textFieldLastName.text isEqualToString:@""])
        {
             [super showAlertViewWithTitle:@"Alert" withMessage:[NSString stringWithFormat:@"Please Enter Last Name of Traveller From Room Number %d",roomNo+1]];
            return NO;
        }
//        else if (![self validateName:cell.textFieldfirstName.text])
//        {
//             [super showAlertViewWithTitle:@"Alert" withMessage:[NSString stringWithFormat:@"Please Enter Valid First Name of Traveller From Room Number %d",roomNo+1]];
//            return NO;
//        }
//        else if (![self validateName:cell.textFieldLastName.text])
//        {
//             [super showAlertViewWithTitle:@"Alert" withMessage:[NSString stringWithFormat:@"Please Enter Valid Last Name of Traveller From Room Number %d",roomNo+1]];
//            return NO;
//        }
        else if (![self validateAAdharNumberWithNumber:cell.textFieldAadharNumber.text])
        {
            [super showAlertViewWithTitle:@"Alert" withMessage:[NSString stringWithFormat:@"Please Enter Valid Aadhaar Number of Traveller From Room Number %d",roomNo+1]];
            return NO;
        }
    }
    return YES;
}

#pragma mark - TableView Delegates and Data Source


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *arrayOfTraveller =  arrayRoomWise[currentRoomSelected-1];
    TravellerInformationModel *model = [arrayOfTraveller objectAtIndex:indexPath.section];
    PassangerDetailCell *cell = model.cell;
    
  /*  if ([cell.textFieldTitle.text isEqualToString:@"Dr"])
    {
        return 278;
    }
    else
    {
        return 278-68;
    }*/
    if ([cell.textFieldTitle.text isEqualToString:@"Dr"])
    {
        return 368;
    }
    else
    {
        return 368-68;
    }
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSArray *arrayOfTraveller =  arrayRoomWise[currentRoomSelected-1];
    return arrayOfTraveller.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if ([[arrayForBool objectAtIndex:section] boolValue])
    {
        return 1;
    }
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIImageView *imageUpDownArrow;
    UIView *headerView         = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    headerView.tag             = section;
    UIImageView *bgImage = [[UIImageView  alloc]initWithFrame:headerView.frame];
    bgImage.image = [UIImage imageNamed:@"RoomTitlePanel"];
    UITapGestureRecognizer *headerTapped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sectionHeaderTapped:)];
    UILabel *labelForTitile = [[UILabel alloc]initWithFrame:CGRectMake(20, 0, 300, 50)];
    NSArray *arrayOfTraveller =  arrayRoomWise[currentRoomSelected-1];
    TravellerInformationModel *model = [arrayOfTraveller objectAtIndex:section];
    NSString *travellerType;
    if([model.travellerType isEqualToString:kTravellerTypeAdult])
    {
        travellerType = @"Adult";
    }
    else if([model.travellerType isEqualToString:kTravellerTypeChild])
    {
        travellerType = @"Child";
    }
    else if([model.travellerType isEqualToString:kTravellerTypeInfant])
    {
        travellerType = @"Infant";
    }
    
    labelForTitile.text = [NSString stringWithFormat:@"Traveller %ld (%@)",(long)section+1,travellerType];
    
    BOOL manyCells = [[arrayForBool objectAtIndex:section]boolValue];
    CGRect rectForUpDownArrow = CGRectMake((self.tableViewForPassangerDetail.frame.size.width * 0.9), 13, 15,15);
    imageUpDownArrow = [[UIImageView alloc]initWithImage:manyCells ?[UIImage imageNamed:@"minus"] : [UIImage imageNamed:@"plus"]];
    imageUpDownArrow.contentMode = UIViewContentModeScaleAspectFit;
    // imageUpDownArrow.autoresizingMask    = UIViewAutoresizingFlexibleLeftMargin;
    imageUpDownArrow.frame               = rectForUpDownArrow;
    [headerView addSubview:bgImage];
    [headerView addSubview:labelForTitile];
    [headerView addGestureRecognizer:headerTapped];
    [headerView addSubview:imageUpDownArrow];
    return headerView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cell";
    NSArray *arrayOfTraveller =  arrayRoomWise[currentRoomSelected-1];
    TravellerInformationModel *model = [arrayOfTraveller objectAtIndex:indexPath.section];
    PassangerDetailCell *cell = model.cell;
    
    if (cell == nil)
    {
        cell = (PassangerDetailCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PassangerDetailCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
        model.cell = cell;
    }
    cell.textFieldDOB.delegate = self;
    cell.textFieldTitle.delegate = self;
    cell.textFieldMealType.delegate = self;
    if (![cell.textFieldTitle.text isEqualToString:@"Dr"])
    {
        cell.constraintFemale.constant = 0;
        cell.ConstraintMale.constant = 0;
        cell.constraintGender.constant = 0;
        cell.buttonFemale.alpha = 0;
        cell.buttonMale.alpha = 0;
        cell.deviderLabel.alpha = 0;
        [cell.contentView layoutIfNeeded];
    }else
    {
        cell.constraintFemale.constant = 38;
        cell.ConstraintMale.constant = 39;
        cell.constraintGender.constant = 31;
        cell.buttonFemale.alpha = 1;
        cell.buttonMale.alpha = 1;
        cell.deviderLabel.alpha = 1;
        [cell.contentView layoutIfNeeded];
    }
    
    if ([model.travellerType isEqualToString:kTravellerTypeInfant])
    {
        cell.textFieldTitle.text = @"Master";
    }
    if ([model.travellerType isEqualToString:kTravellerTypeChild])
    {
        if ([cell.textFieldTitle.text isEqualToString:@"Mr"] || [cell.textFieldTitle.text isEqualToString:@""])
        {
            cell.textFieldTitle.text = @"Master";
        }
    }
    
    return cell;
}

- (void)sectionHeaderTapped:(UITapGestureRecognizer *)gestureRecognizer
{
    
    if([self validationForParticularTravellerInfo])
    {
        BOOL collapsedForPrevious  = [[arrayForBool objectAtIndex:previousIndexPath] boolValue];
        collapsedForPrevious = !collapsedForPrevious;
        [arrayForBool replaceObjectAtIndex:previousIndexPath withObject:[NSNumber numberWithBool:collapsedForPrevious]];
        
        //reload specific section animated
        NSRange rangeForPrevious   = NSMakeRange(previousIndexPath, 1);
        NSIndexSet *sectionToReloadForPrevious = [NSIndexSet indexSetWithIndexesInRange:rangeForPrevious];
        
        
        if (collapsedForPrevious)
        {
            [self.tableViewForPassangerDetail reloadSections:sectionToReloadForPrevious withRowAnimation:UITableViewRowAnimationFade];
        }
        else
        {
            [self.tableViewForPassangerDetail reloadSections:sectionToReloadForPrevious withRowAnimation:UITableViewRowAnimationFade];
        }
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:gestureRecognizer.view.tag];
        BOOL collapsed  = [[arrayForBool objectAtIndex:indexPath.section] boolValue];
        collapsed = !collapsed;
        [arrayForBool replaceObjectAtIndex:indexPath.section withObject:[NSNumber numberWithBool:collapsed]];
        
        //reload specific section animated
        NSRange range   = NSMakeRange(indexPath.section, 1);
        NSIndexSet *sectionToReload = [NSIndexSet indexSetWithIndexesInRange:range];
        
        
        if (collapsed)
        {
            [self.tableViewForPassangerDetail reloadSections:sectionToReload withRowAnimation:UITableViewRowAnimationFade];
        }
        else
        {
            [self.tableViewForPassangerDetail reloadSections:sectionToReload withRowAnimation:UITableViewRowAnimationFade];
        }
        
        previousIndexPath = (int)indexPath.section;
    }
}

#pragma mark -textFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSIndexPath *indexPath = [self GetIndexPathFromSender:textField];
    currentSection = (int)indexPath.section;
    NSArray *arrayOfTraveller =  arrayRoomWise[currentRoomSelected-1];
    TravellerInformationModel *model = [arrayOfTraveller objectAtIndex:indexPath.section];
    PassangerDetailCell *cell = model.cell;
    if (textField == cell.textFieldAadharNumber)
    {
        if(![self validateAAdharNumberWithNumber:cell.textFieldAadharNumber.text])
        {
            [super showAlertViewWithTitle:@"Alert" withMessage:@"Please Enter Valid Aadhaar number of passenger "];
        }

    }

    
    [textField resignFirstResponder];
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSIndexPath *indexPath = [self GetIndexPathFromSender:textField];
    currentSection = (int)indexPath.section;
    NSArray *arrayOfTraveller =  arrayRoomWise[currentRoomSelected-1];
    TravellerInformationModel *model = [arrayOfTraveller objectAtIndex:indexPath.section];
    PassangerDetailCell *cell = model.cell;
    
    if (textField == cell.textFieldAadharNumber)
    {
        NSString *currentString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        int length = (int)[currentString length];
        
        if ([currentString isEqualToString:@""])
        {
            return YES;
        }
        else if (length > 12)
        {
            return NO;
        }
        else
        {
            NSCharacterSet *numbersOnly = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
            NSCharacterSet *characterSetFromTextField = [NSCharacterSet characterSetWithCharactersInString:currentString];
            
            BOOL stringIsValid = [numbersOnly isSupersetOfSet:characterSetFromTextField];
            
            return stringIsValid;

        }
            
            
        return YES;
    }
    
    
    return YES;
    
    
}


-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    NSIndexPath *indexPath = [self GetIndexPathFromSender:textField];
    currentSection = (int)indexPath.section;
    NSArray *arrayOfTraveller =  arrayRoomWise[currentRoomSelected-1];
    TravellerInformationModel *model = [arrayOfTraveller objectAtIndex:indexPath.section];
    PassangerDetailCell *cell = model.cell;
    
    if (cell.textFieldDOB == textField)
    {
        [self setDateWithTextField:textField];
        return NO;
    }
    else if (cell.textFieldTitle == textField)
    {
        UIAlertController *durationActionSheet = [UIAlertController alertControllerWithTitle:@"Title" message:@"Select your choice" preferredStyle:UIAlertControllerStyleActionSheet];
        NSArray *arrayTitle = [[NSArray alloc]initWithObjects:@"Mr",@"Miss",@"Mrs",@"Dr",nil];
        if ([model.travellerType isEqualToString:kTravellerTypeChild]||[model.travellerType isEqualToString:kTravellerTypeInfant])
        {
            arrayTitle = [[NSArray alloc]initWithObjects:@"Master",@"Miss",nil];
        }
        
        
        for (int j =0 ; j < arrayTitle.count; j++)
        {
            NSString *titleString = arrayTitle[j];
            UIAlertAction *action = [UIAlertAction actionWithTitle:titleString style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                cell.textFieldTitle.text = titleString;
                NSIndexPath *indexPath = [self GetIndexPathFromSender:cell];
                NSLog(@"%ld",(long)indexPath.section);
                //                NSRange range   = NSMakeRange(indexPath.section, 1);
                //                NSIndexSet *sectionToReload = [NSIndexSet indexSetWithIndexesInRange:range];
                //                [self.tableViewForPassangerDetail reloadSections:sectionToReload withRowAnimation:UITableViewRowAnimationFade];
                [self.tableViewForPassangerDetail reloadData];
            }];
            
            [durationActionSheet addAction:action];
        }
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
            
            [durationActionSheet dismissViewControllerAnimated:durationActionSheet completion:nil];
        }];
        
        [durationActionSheet addAction:cancelAction];
        
        [self presentViewController:durationActionSheet animated:YES completion:nil];
        return NO;
    }else if (cell.textFieldMealType == textField)
    {
        UIAlertController *durationActionSheet = [UIAlertController alertControllerWithTitle:@"Meal Type" message:@"Select your choice" preferredStyle:UIAlertControllerStyleActionSheet];
        NSArray *arrayTitle = [[NSArray alloc]initWithObjects:@"Veg",@"Non Veg",@"Jain",nil];
        
        for (int j =0 ; j < arrayTitle.count; j++)
        {
            NSString *titleString = arrayTitle[j];
            UIAlertAction *action = [UIAlertAction actionWithTitle:titleString style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                cell.textFieldMealType.text = titleString;
            }];
            
            [durationActionSheet addAction:action];
        }
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
            
            [durationActionSheet dismissViewControllerAnimated:durationActionSheet completion:nil];
        }];
        
        [durationActionSheet addAction:cancelAction];
        
        [self presentViewController:durationActionSheet animated:YES completion:nil];
        
        return NO;
    }
    
    return YES;
}

-(NSIndexPath*)GetIndexPathFromSender:(id)sender
{
    if(!sender)
    {
        return nil;
    }
    
    if([sender isKindOfClass:[UITableViewCell class]])
    {
        UITableViewCell *cell = sender;
        return [_tableViewForPassangerDetail indexPathForCell:cell];
    }
    
    return [self GetIndexPathFromSender:((UIView*)[sender superview])];
}

-(void)setDateWithTextField:(UITextField *)textField
{
    [self.view endEditing:YES];
    textFieldDateOfBirth = textField;
    [self cancelAction:nil];
    datePicker1.date = [NSDate date];
    if (dateView)
    {
        
        [UIView animateWithDuration:0.4
         
                         animations:^{
                             
                             dateView.frame = CGRectMake(0, self.view.frame.size.height - 200, self.view.frame.size.width, 250);
                             
                         }];
    }else
    {
        dateView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 250)];
        
        
        
        UIToolbar *pickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
        
        pickerToolbar.barStyle=UIBarStyleBlackOpaque;
        
        [pickerToolbar setBackgroundImage:[UIImage imageNamed:@"header_bg.png"]
         
                       forToolbarPosition:0
         
                               barMetrics:0];
        [pickerToolbar sizeToFit];
        
        
        
        UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Done"  style:UIBarButtonItemStyleDone target:self action:@selector(doneButton:)];
        [barButtonItem setTintColor:[UIColor whiteColor]];
        
        UIBarButtonItem *cancelBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleDone target:self action:@selector(cancelAction:)];
        
        [cancelBarButtonItem setTintColor:[UIColor whiteColor]];
        
        UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        
        [pickerToolbar setItems:[NSArray arrayWithObjects:barButtonItem,flexibleSpace,flexibleSpace,cancelBarButtonItem, nil]];
        
        [dateView addSubview:pickerToolbar];
        
        datePicker1 = [[UIDatePicker alloc] initWithFrame:CGRectMake(0.0, 44.0, self.view.frame.size.width, 210)];
        
        datePicker1.tag = 50;
        datePicker1.maximumDate = [NSDate date];
        datePicker1.date = [NSDate date];
        
        datePicker1.datePickerMode = UIDatePickerModeDate;
        
        [datePicker1 addTarget:self action:@selector(changeDate:) forControlEvents:UIControlEventValueChanged];
        
        datePicker1.backgroundColor = [UIColor whiteColor];
        
        [dateView addSubview:datePicker1];
        [textField setInputView:datePicker1];
        [self.passangerDetailsView addSubview:dateView];
        
        
        
        [UIView animateWithDuration:0.4
         
                         animations:^{
                             
                             dateView.frame = CGRectMake(0, self.view.frame.size.height - 200, self.view.frame.size.width, 250);
                             
                         }];
    }
}

- (void)changeDate:(UIDatePicker *)sender

{
    
    NSDate *date = sender.date;
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    NSLog(@"date formatter%@",[dateFormat stringFromDate:date]);
    
}

-(void)dateChosen:(id)sender
{
    [self cancelAction:sender];
}

- (void)doneButton:(id)sender
{
    [self cancelAction:sender];
    
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [gregorianCalendar components:NSCalendarUnitDay
                                                        fromDate:datePicker1.date
                                                          toDate:[NSDate date]
                                                         options:NSCalendarWrapComponents];
    //    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian] ;
    //    NSDateComponents *components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:datePicker1.date toDate:[NSDate date] options:0];
    
    NSIndexPath *indexPath = [self GetIndexPathFromSender:datePicker1];
    NSLog(@"%ld",(long)indexPath.section);
    
    NSArray *arrayOfTraveller =  arrayRoomWise[currentRoomSelected-1];
    TravellerInformationModel *model = [arrayOfTraveller objectAtIndex:currentSection];
    
    if ([model.travellerType isEqualToString:kTravellerTypeAdult])
    {
        if (components.day <= (11*365) )
        {
            [super showAlertViewWithTitle:@"Alert" withMessage:@"Select adult age more than 11 years old"];
            return;
        }
    }
    else if ([model.travellerType isEqualToString:kTravellerTypeChild])
    {
        if (components.day <= (2*365) || components.day > (11*365))
        {
            [super showAlertViewWithTitle:@"Alert" withMessage:@"Select child age between 2 to 11 years old"];
            return;
        }
    }
    else if ([model.travellerType isEqualToString:kTravellerTypeInfant])
    {
        if (components.day > (2*365))
        {
            [super showAlertViewWithTitle:@"Alert" withMessage:@"Select infant age less than 2 years"];
            return;
        }
    }
    
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSString *selectedDate = [dateFormat stringFromDate:datePicker1.date];
    textFieldDateOfBirth.text=selectedDate;
    
    
}
-(void)cancelAction:(id)sender {
    if (dateView)
        
    {
        [UIView animateWithDuration:0.6
                         animations:^{
                             dateView.frame = CGRectMake(0, self.view.frame.size.height, 320, 200);
                         }
                         completion:^(BOOL finished)
         {
             //[dateView removeFromSuperview];
         }];
    }
}

- (IBAction)onProceedButtonClicked:(id)sender
{
    //latest
    
    if ([[self.proceedButton titleLabel].text isEqualToString:@"Payment"])
    {
        if ([_packageDetail.strPackageName caseInsensitiveCompare:@"international"]==NSOrderedSame)
        {
            [FIRAnalytics logEventWithName:@"International_Holidays_Payment_Traveller_Details" parameters:@{kFIRParameterItemName:[NSString stringWithFormat:@"PackageName :%@, PackageType :%@, PackageID :%@",_packageDetail.strPackageName,_packageDetail.strPackageType,_packageDetail.strPackageId]}];
        }
        else
        {
          [FIRAnalytics logEventWithName:@"Indian_Holidays_Payment_Traveller_Details" parameters:@{kFIRParameterItemName:[NSString stringWithFormat:@"PackageName :%@, PackageType :%@, PackageID :%@",_packageDetail.strPackageName,_packageDetail.strPackageType,_packageDetail.strPackageId]}];
        }
      
        
        
        BOOL isValid = [self validationOfTravellersInfo];
        
        if (!isValid)
        {
            [super showAlertViewWithTitle:@"Alert" withMessage:@"You have not filled enough details"];
            
        }
        else if (addressDetails == nil)
        {
            [self onAdressButtonClicked:nil];
        }
        else
        {
            
            NSUserDefaults *LoginStatus=[NSUserDefaults standardUserDefaults];
            NSString *statusForLogin= [LoginStatus valueForKey:kLoginStatus];
            
            if (![statusForLogin isEqualToString:kLoginSuccess])
            {
                /*LoginViewPopUp *loginView = [[LoginViewPopUp alloc] initWithNibName:@"LoginViewPopUp" bundle:nil];
                // LoginViewPopUp.sharedInstance.
                [LoginViewPopUp sharedInstance].object=loginView;
                loginView.view.frame = CGRectMake(0, 0, 300, self.passangerDetailsView.frame.size.height - 100);
                loginView.isForGuestUser = TRUE;
                [self presentPopupViewController:loginView animationType:MJPopupViewAnimationFade];*/
                
                NewLoginPopUpViewController * loginView = [[NewLoginPopUpViewController alloc] initWithNibName:@"NewLoginPopUpViewController" bundle:nil];
                //loginView.currentSelectedeScreen = @"Guest User";
                
                 UINavigationController * loginNavigation = [[UINavigationController alloc] initWithRootViewController:loginView];
                
                [self presentViewController:loginNavigation animated:true completion:^{
                    
                }];
                
            }
            else
            {
                activityLoadingView = [LoadingView loadingViewInView:self.view.superview.superview
                                                          withString:@""
                                                   andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];
               // [self performSelectorInBackground:@selector(submitBooking) withObject:nil];
                [self submitBooking];
            }
            //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://uat2.thomascook.in/tcmobile/tcm/tcm/pmt/tcPg.do?refID=GwCK/4wvOT8nTN/Fi/v8Jw=="]];
            
        }
    }
    else if([self.proceedButton.titleLabel.text isEqualToString:@"Address of communication"])
    {
        if([_packageDetail.strPackageId caseInsensitiveCompare:@"international"]==NSOrderedSame)
        {
            [FIRAnalytics logEventWithName:@"International_Holidays_Address_of_Communication_Traveller_Details" parameters:@{kFIRParameterItemName:[NSString stringWithFormat:@"PackageName :%@, PackageType :%@, PackageID :%@",_packageDetail.strPackageName,_packageDetail.strPackageType,_packageDetail.strPackageId]}];
            
        }
        else
        {
            [FIRAnalytics logEventWithName:@"Indian_Holidays_Address_of_Communication_Traveller_Details" parameters:@{kFIRParameterItemName:[NSString stringWithFormat:@"PackageName :%@, PackageType :%@, PackageID :%@",_packageDetail.strPackageName,_packageDetail.strPackageType,_packageDetail.strPackageId]}];
            
        }
        
        AdressPopUpVC *detailViewController = [[AdressPopUpVC alloc] initWithNibName:@"AdressPopUpVC" bundle:nil];
        detailViewController.view.frame = CGRectMake(0, 0, 300, self.passangerDetailsView.frame.size.height - 100);
        detailViewController.stateCode = self.stateCode;
        detailViewController.delegate = self;
        
        if(addressDetails)
        {
            detailViewController.txtFieldAddress1.text =  addressDetails.txtFieldAddress1.text;
            detailViewController.txtFieldAddress2.text = addressDetails.txtFieldAddress2.text;
            detailViewController.txtFieldCity.text = addressDetails.txtFieldCity.text;
            detailViewController.txtFieldContactNo.text = addressDetails.txtFieldContactNo.text;
            detailViewController.txtFieldEmailID.text = addressDetails.txtFieldEmailID.text;
            detailViewController.txtFieldState.text = addressDetails.txtFieldState.text;
            detailViewController.txtFieldZip.text = addressDetails.txtFieldZip.text;
            detailViewController.textFieldGSTInNumber.text = addressDetails.textFieldGSTInNumber.text;
        }
        [self presentPopupViewController:detailViewController animationType:MJPopupViewAnimationFade];
        
       /* AdressPopUpVC *detailViewController = [[AdressPopUpVC alloc] initWithNibName:@"AdressPopUpVC" bundle:nil];
        detailViewController.view.frame = CGRectMake(0, 0, 300, self.passangerDetailsView.frame.size.height - 100);
        detailViewController.delegate = self;
        [self presentPopupViewController:detailViewController animationType:MJPopupViewAnimationFade];*/
    }
    else
    {
        UITapGestureRecognizer *tap = tapGestureArray[currentRoomSelected];
        [self tapNumber:tap];
    }
    
    //}
    
    
}

-(void)submitBooking
{
    
   // activityLoadingView = [LoadingView loadingViewInView:self.view.superview.superview withString:@""  andIndicatorColor:[UIColor colorFromHexString:DEFAULT_COLOUR_CODE]];

  //  LoadingView *loadingView = [LoadingView loadingViewInView:_passangerDetailsView.superview withString:@"" andIndicatorColor:[UIColor ]]
    /*
     {
     "type": "webservice",
     "entity": "booking",
     "action": "submit",
     "platform": "android",
     "data": {
     "bookingDetails": {
     "packageId": "PKG120106",
     "userId": "30249",
     "gitProductCode": "TCEXP011014",
     "date": "09-10-2014",
     "subBusiness": "1",
     "noOfAdult": "2",
     "noOfChild": "1",
     "noOfInfant": "0",
     "noOfrooms": "1",
     "bookingAmount": "8000",
     "offerCode": "NEPALC1",
     "totalPackageAmt": "360816",
     "addonPackages": "PKG120104,PKG120113",
     "offerAmount": "6500",
     "accomType": "0",
     "priceSlab": "500"
     "custStateCode":"27",              //new keys added 16-06-2017
     "gstinNo":"GSTINNO0027",     
     "gstTaxAmountDesc":"CGST=1570.0#SGST=1570.0#IGST=0.0#UTGST=0.0",
     "gstTaxPerDesc":"CGST=9.0#SGST=9.0#IGST=0.0#UTGST=0.0#TAXPER=100.0"
     
     },
     "leadPaxDetails": {
     "leadPaxTitle": "Miss",
     "leadPaxFirstName": "EBIZ",
     "leadPaxLastName": "TEAM",
     "leadPaxAddress1": "Fort",
     "leadPaxAddress2": "Bandra",
     "leadPaxCity": "Bombay",
     "leadPaxState": "Maharashtra",
     "leadPaxZip": "888888",
     "leadPaxPhone": "1234567890",
     "leadPaxEmail": "sush@gmail.com"
     },
     "travellerDetails": [
     {
     "travellerNo": "1",
     "title": "Mrs",
     "firstName": "test",
     "lastName": "test",
     "gender": "male",
     "dob": "09-06-1983",
     "roomin": "1",
     "hub": "DEL",
     "remarks": "remarks",
     "roomNo": "1",
     "paxType": "1",
     "nationality": "Indians",
     "diet": "VEG",
     "disCode": "discount code",
     "disAmt": "dis amt",
     "adhocDis": "adhocDis"
     "aadharNo":"MH123456789"
     }
     ],
     "tourCostList": [
     {
     "currencyCode": "INR",
     "amount": ""
     }
     ]
     }
     }
     
     */
    
    
  /*
    
    NSUserDefaults *userIdForGooglePlusSignIn=[NSUserDefaults standardUserDefaults];
    NSString *userId=[userIdForGooglePlusSignIn valueForKey:kLoginEmailId];
    
    int noOfInf = 0;
    int noOfAdults = 0;
    int noOfChilds = 0;
    int noOfSR = 0;
    int noOfDR = 0;
    int noOFTR = 0;
    int noOfCWB = 0;
    int noOfCNB = 0;
    
    
    for (int j = 0; j<self.roomRecordArray.count; j++)
    {
        RoomsDataModel *roomModel = roomRecordArray[j];
        noOfAdults = roomModel.adultCount + noOfAdults;
        noOfInf = roomModel.infantCount + noOfInf;
        noOfChilds = (int)roomModel.arrayChildrensData.count + noOfChilds;
        noOfSR = roomModel.SRCount + noOfSR;
        noOfDR = roomModel.DRCount + noOfDR;
        noOFTR = roomModel.TRCount + noOFTR;
        noOfCNB = roomModel.CNBCount + noOfCNB;
        noOfCWB = roomModel.CWBCount + noOfCWB;
    }
    
    
    
    
    PassangerDetailCell *leadPaxDetailCell;
    if (arrayRoomWise.count != 0)
    {
        NSArray *arrayOfTraveller =  arrayRoomWise[0];
        if (arrayOfTraveller.count != 0)
        {
            TravellerInformationModel *model = [arrayOfTraveller objectAtIndex:0];
            leadPaxDetailCell = model.cell;
        }
    }
    
    NSMutableDictionary *bookingDetailsDict =  [[NSMutableDictionary alloc]init];
    [bookingDetailsDict setValue:_packageDetail.strPackageId forKey:@"packageId"];
    [bookingDetailsDict setValue:userId forKey:@"userId"];
    
    [bookingDetailsDict setValue:self.packageDetail.ltProdCode forKey:@"gitProductCode"];
    [bookingDetailsDict setValue:[DictJourneyDate valueForKey:@"date"] forKey:@"date"];
    [bookingDetailsDict setValue:@"INR" forKey:@"currencyCode"];
    [bookingDetailsDict setValue:@"0" forKey:@"subBusiness"];
    [bookingDetailsDict setValue:[NSString stringWithFormat:@"%d",noOfAdults] forKey:@"noOfAdult"];
    [bookingDetailsDict setValue:[NSString stringWithFormat:@"%d",noOfChilds] forKey:@"noOfChild"];
    [bookingDetailsDict setValue:[NSString stringWithFormat:@"%d",noOfInf] forKey:@"noOfInfant"];
    [bookingDetailsDict setValue:[NSString stringWithFormat:@"%lu",(unsigned long)roomRecordArray.count] forKey:@"noOfrooms"];
    [bookingDetailsDict setValue:self.bookingAmountString forKey:@"bookingAmount"];
    [bookingDetailsDict setValue:@"" forKey:@"offerCode"];
    [bookingDetailsDict setValue:self.totalAmountString forKey:@"totalPackageAmt"];
    [bookingDetailsDict setValue:@"" forKey:@"addonPackages"];
    [bookingDetailsDict setValue:@"0" forKey:@"offerAmount"];
    [bookingDetailsDict setValue:[NSString stringWithFormat:@"%d",self.accomType] forKey:@"accomType"];
    [bookingDetailsDict setValue:@"" forKey:@"priceSlab"];
    
    
    [bookingDetailsDict setValue:self.stateCode forKey:@"custStateCode"];
    [bookingDetailsDict setValue:addressDetails.textFieldGSTInNumber.text forKey:@"gstinNo"];
    [bookingDetailsDict setValue:[self.gstTaxDetails valueForKey:@"gstTaxAmountDesc"] forKey:@"gstTaxAmountDesc"];
    [bookingDetailsDict setValue:[self.gstTaxDetails valueForKey:@"gstTaxPerDesc"] forKey:@"gstTaxPerDesc"];

    

    
    NSMutableDictionary *leadPaxDetailDict = [[NSMutableDictionary alloc]init];
    
    if (leadPaxDetailCell)
    {
        [leadPaxDetailDict setValue:leadPaxDetailCell.textFieldTitle.text forKey:@"leadPaxTitle"];
        [leadPaxDetailDict setValue:leadPaxDetailCell.textFieldfirstName.text forKey:@"leadPaxFirstName"];
        [leadPaxDetailDict setValue:leadPaxDetailCell.textFieldLastName.text forKey:@"leadPaxLastName"];
        [leadPaxDetailDict setValue:addressDetails.txtFieldAddress1.text forKey:@"leadPaxAddress1"];
        [leadPaxDetailDict setValue:addressDetails.txtFieldAddress2.text forKey:@"leadPaxAddress2"];
        [leadPaxDetailDict setValue:addressDetails.txtFieldCity.text forKey:@"leadPaxCity"];
        [leadPaxDetailDict setValue:addressDetails.txtFieldState.text forKey:@"leadPaxState"];
        [leadPaxDetailDict setValue:addressDetails.txtFieldZip.text forKey:@"leadPaxZip"];
        [leadPaxDetailDict setValue:addressDetails.txtFieldContactNo.text forKey:@"leadPaxPhone"];
        [leadPaxDetailDict setValue:addressDetails.txtFieldEmailID.text forKey:@"leadPaxEmail"];
        
        [profileDetail setObject:leadPaxDetailCell.textFieldfirstName.text forKey:@"FIRST_NAME"];
        [profileDetail setObject:leadPaxDetailCell.textFieldLastName.text forKey:@"LAST_NAME"];
        [profileDetail setObject:addressDetails.txtFieldEmailID.text forKey:@"EMAIL"];
        [profileDetail setObject:addressDetails.txtFieldCity.text forKey:@"CITY"];
        
        [self netCoreBookingForm];
        
    }
    
    
    NSMutableArray *travellerDetailsArray = [[NSMutableArray alloc]init];
    
    if (arrayRoomWise.count != 0)
    {
        for (int i= 0; i<arrayRoomWise.count; i++)
        {
            NSArray *arrayOfTraveller =  arrayRoomWise[i];
            if (arrayOfTraveller.count != 0)
            {
                for (int j = 0; j<arrayOfTraveller.count; j++)
                {
                    TravellerInformationModel *model = [arrayOfTraveller objectAtIndex:j];
                    PassangerDetailCell *passangerCell = model.cell;
                    NSMutableDictionary *travellerDict = [[NSMutableDictionary alloc]init];
                    [travellerDict setValue:[NSString stringWithFormat:@"%lu",(unsigned long)travellerDetailsArray.count+1] forKey:@"travellerNo"];
                    [travellerDict setValue:passangerCell.textFieldTitle.text forKey:@"title"];
                    [travellerDict setValue:passangerCell.textFieldfirstName.text forKey:@"firstName"];
                    [travellerDict setValue:passangerCell.textFieldLastName.text forKey:@"lastName"];
                    
                    //gstNew
                    
                    //aadharNo
                    [travellerDict setValue:passangerCell.textFieldAadharNumber.text forKey:@"aadharNo"];
                    
                    
                    
                    if ([[passangerCell.textFieldTitle.text lowercaseString] isEqualToString:@"dr"])
                    {
                        UIButton *buttonGenderMale = passangerCell.buttonMale;
                        
                        if ([buttonGenderMale isSelected])
                        {
                            [travellerDict setValue:@"male" forKey:@"gender"];
                        }
                        else
                        {
                            [travellerDict setValue:@"female" forKey:@"gender"];
                        }
                    }else if ([[passangerCell.textFieldTitle.text lowercaseString] isEqualToString:@"miss"]||[[passangerCell.textFieldTitle.text lowercaseString] isEqualToString:@"mrs"])
                    {
                        [travellerDict setValue:@"female" forKey:@"gender"];
                    }
                    else
                    {
                        [travellerDict setValue:@"male" forKey:@"gender"];
                    }
                    
                    NSString *dateString = passangerCell.textFieldDOB.text;
                    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
                    NSDate *date = [dateFormatter dateFromString:dateString];
                    
                    // Convert date object into desired format
                    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
                    NSString *newDateString = [dateFormatter stringFromDate:date];
                    
                    [travellerDict setValue:newDateString forKey:@"dob"];
                    if ([model.travellerRoomingType isEqualToString:kTravellerRoomTypeSR])
                    {
                        [travellerDict setValue:@"0" forKey:@"roomin"];
                    }else if ([model.travellerRoomingType isEqualToString:kTravellerRoomTypeDR])
                    {
                        [travellerDict setValue:@"1" forKey:@"roomin"];
                    }else if ([model.travellerRoomingType isEqualToString:kTravellerRoomTypeTR])
                    {
                        [travellerDict setValue:@"2" forKey:@"roomin"];
                    }else if ([model.travellerRoomingType isEqualToString:kTravellerRoomTypeCWB])
                    {
                        [travellerDict setValue:@"4" forKey:@"roomin"];
                    }else if ([model.travellerRoomingType isEqualToString:kTravellerRoomTypeCNB])
                    {
                        [travellerDict setValue:@"5" forKey:@"roomin"];
                    }
                    else if ([model.travellerRoomingType isEqualToString:kTravellerRoomTypeInfant])
                    {
                        [travellerDict setValue:@"6" forKey:@"roomin"];
                    }
                    //                        else if ([model.travellerRoomingType isEqualToString:kTravellerRoomTypeTS])
                    //                        {
                    //
                    //                        }
                    
                    if ([model.travellerType isEqualToString:kTravellerTypeAdult])
                    {
                        [travellerDict setValue:@"0" forKey:@"paxType"];
                    }
                    else if ([model.travellerType isEqualToString:kTravellerTypeChild])
                    {
                        [travellerDict setValue:@"1" forKey:@"paxType"];
                    }
                    else if ([model.travellerType isEqualToString:kTravellerTypeInfant])
                    {
                        [travellerDict setValue:@"2" forKey:@"paxType"];
                    }
                    
                    [travellerDict setValue:self.hubName forKey:@"hub"];
                    [travellerDict setValue:@"" forKey:@"remarks"];
                    [travellerDict setValue:[NSString stringWithFormat:@"%d",i+1] forKey:@"roomNo"];
                    [travellerDict setValue:passangerCell.textFieldNationality.text forKey:@"nationality"];
                    [travellerDict setValue:passangerCell.textFieldMealType.text forKey:@"diet"];
                    [travellerDict setValue:@"" forKey:@"disCode"];
                    [travellerDict setValue:@"0" forKey:@"disAmt"];
                    [travellerDict setValue:@"0" forKey:@"adhocDis"];
                    [travellerDict setValue:@"Indian" forKey:@"nationality"];
                    [travellerDetailsArray addObject:travellerDict];
                }
            }
        }
    }
    
    NSMutableArray *tourCostListArray =[[NSMutableArray alloc]init];
    for (int i = 0; i < _currencyArray.count; i++)
    {
        NSDictionary *nonINRDict = _currencyArray[i];
        NSString *currencyString = [nonINRDict valueForKey:@"currency"];
        NSString *totalValue = [nonINRDict valueForKey:@"total"];
        if(totalValue == nil)
        {
            totalValue = self.packageDetail.stringAmountINRFIT;
        }
        NSDictionary *currencyDict = [[NSDictionary alloc]initWithObjectsAndKeys:currencyString,@"currencyCode",totalValue,@"amount",nil];
        [tourCostListArray addObject:currencyDict];
        
    }
    
    NSMutableDictionary *dictOfData = [[NSMutableDictionary alloc]init];
    [dictOfData setObject:bookingDetailsDict forKey:@"bookingDetails"];
    [dictOfData setObject:leadPaxDetailDict forKey:@"leadPaxDetails"];
    [dictOfData setObject:travellerDetailsArray forKey:@"travellerDetails"];
    [dictOfData setObject:tourCostListArray forKey:@"tourCostList"];
    
    
    NetworkHelper *helper = [NetworkHelper sharedHelper];
    NSString *strResponse = [helper getDataFromServerForType:@"webservice" entity:@"booking" action:@"submit" andUserJson:dictOfData];
    NSString *strStatus = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"status"];
    
    if ([self connected])
    {
    
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            
            if ([strStatus isEqualToString:kStatusSuccess])
            {
                NSDictionary *dataDict = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"data"];
                NSString *pageUrlString = [dataDict valueForKey:@"pgUrl"];
                pageUrlString = [self parseUrl:pageUrlString];
                NSURL *url = [NSURL URLWithString:pageUrlString];
                if (url)
                {
                    [[UIApplication sharedApplication] openURL:url];
                    [[SlideNavigationController sharedInstance] popToRootViewControllerAnimated:YES];
                }
            }else
            {
                //NSString *messsage = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"message"];
                
                NSString *messsage = @"Please enter all details.";
                
                if (messsage)
                {
                    if ([messsage isEqualToString:kVersionUpgradeMessage])
                    {
                        NSDictionary *downloadDict = [[JsonSerealizer dictonaryOfJsonFromJsonData:strResponse]valueForKey:@"data"];
                        NSString *downloadURL = [downloadDict valueForKey:@"download_url"];
                        [super showAlertViewForVersionUpdateWithUrl:downloadURL];
                        
                    }else
                    {
                        [super showAlertViewWithTitle:@"Alert" withMessage:messsage];
                    }
                }else
                {
                    [super showAlertViewWithTitle:@"Alert" withMessage:kMessageSomeErrorTryAfterSometime];
                }
                
            }
            
            [activityLoadingView removeFromSuperview];
        });
    
    }
    else
    {
        [super showAlertViewWithTitle:@"Alert" withMessage:kMessageNoInternet];
    }*/
    
    /*
     "{
     ""addressId"":0,
     ""bookingAmount"":40320,
     ""bookingType"":""F"",
     ""tcilHolidayBookingTravellerDetailCollection"":[{""roomNo"":1,""position"":1,""travellerNo"":1,""title"":""Mr"",""firstName"":""sdg"",""lastName"":""sdh"",""gender"":""M"",""dob"":""07-07-2005"",""mealPreference"":""JAIN"",""paxType"":0,""aadhaarNumber"":""""},{""roomNo"":1,""position"":2,""travellerNo"":2,""title"":""Mr"",""firstName"":""sdhsdh"",""lastName"":""sdhsdh"",""gender"":""M"",""dob"":""14-07-2005"",""mealPreference"":""JAIN"",""paxType"":0,""aadhaarNumber"":""""}],
     ""quotationId"":""139751"",
     ""title"":""Mr"",
     ""firstName"":""sdhsdh"",
     ""lastName"":""sdhsdh"",
     ""addressEmailId"":""aa@hh.ll"",
     ""addressPhoneNo"":""9964434636"",
     ""addressStreet"":""dshsdhsdh"",
     ""addressCity"":""Mumbai"",
     ""addressState"":""Maharashtra"",
     ""addressPincode"":""400235"",
     ""packageId"":""PKG001424"",
     ""preConformationPageUrl"":""https://www.sotc.in/holidays/holidaysPreConfirmation?hubCode=IDR&pkgId=PKG001424&pkgClassId=2&quotationId=139751&upgradation=0&destination=PKG001424_indore_CITY_0"",
     ""bookedForEmail"":null, : onbehalf email id,applicable if booking is onbehalf - Non Mandatory
     ""isOnBefalf"":""false"",
     ""bookingThrough"":""Desktop"",
     ""gstStateCode"":""27"",
     ""isUnionTerritory"":""N"",
     ""gstinNo"":""27asdfg1234dggd""
     }"
     */
    @try
    {
    PassangerDetailCell *leadPaxDetailCell;
    if (arrayRoomWise.count != 0)
    {
        NSArray *arrayOfTraveller =  arrayRoomWise[0];
        if (arrayOfTraveller.count != 0)
        {
            TravellerInformationModel *model = [arrayOfTraveller objectAtIndex:0];
            leadPaxDetailCell = model.cell;
        }
    }
    
    
    NSMutableArray *travellerDetailsArray = [[NSMutableArray alloc]init];
    
    if (arrayRoomWise.count != 0)
    {
        for (int i= 0; i<arrayRoomWise.count; i++)
        {
            NSArray *arrayOfTraveller =  arrayRoomWise[i];
            if (arrayOfTraveller.count != 0)
            {
                for (int j = 0; j<arrayOfTraveller.count; j++)
                {
                    TravellerInformationModel *model = [arrayOfTraveller objectAtIndex:j];
                    PassangerDetailCell *passangerCell = model.cell;
                    NSMutableDictionary *travellerDict = [[NSMutableDictionary alloc]init];
                    
                    [travellerDict setValue:[NSString stringWithFormat:@"%lu",(unsigned long)travellerDetailsArray.count+1] forKey:@"position"];
                    [travellerDict setValue:[NSString stringWithFormat:@"%d",i+1] forKey:@"roomNo"];
                    [travellerDict setValue:[NSString stringWithFormat:@"%lu",(unsigned long)travellerDetailsArray.count+1] forKey:@"travellerNo"];

                    
                    [travellerDict setValue:passangerCell.textFieldTitle.text forKey:@"title"];
                    [travellerDict setValue:passangerCell.textFieldfirstName.text forKey:@"firstName"];
                    [travellerDict setValue:passangerCell.textFieldLastName.text forKey:@"lastName"];
                    
                    [travellerDict setValue:passangerCell.textFieldAadharNumber.text forKey:@"aadhaarNumber"];
                    
                    
                    
                    if ([[passangerCell.textFieldTitle.text lowercaseString] isEqualToString:@"dr"])
                    {
                        UIButton *buttonGenderMale = passangerCell.buttonMale;
                        
                        if ([buttonGenderMale isSelected])
                        {
                            [travellerDict setValue:@"M" forKey:@"gender"];
                        }
                        else
                        {
                            [travellerDict setValue:@"F" forKey:@"gender"];
                        }
                    }
                    else if ([[passangerCell.textFieldTitle.text lowercaseString] isEqualToString:@"miss"]||[[passangerCell.textFieldTitle.text lowercaseString] isEqualToString:@"mrs"])
                    {
                        [travellerDict setValue:@"F" forKey:@"gender"];
                    }
                    else
                    {
                        [travellerDict setValue:@"M" forKey:@"gender"];
                    }
                    
                    
                    NSString *dateString = passangerCell.textFieldDOB.text;
                    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
                    NSDate *date = [dateFormatter dateFromString:dateString];
                    
                    // Convert date object into desired format
                    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
                    NSString *newDateString = [dateFormatter stringFromDate:date];
                    
                    [travellerDict setValue:newDateString forKey:@"dob"];
                    
                    
                    if ([model.travellerType isEqualToString:kTravellerTypeAdult])
                    {
                        [travellerDict setValue:@"0" forKey:@"paxType"];
                    }
                    else if ([model.travellerType isEqualToString:kTravellerTypeChild])
                    {
                        [travellerDict setValue:@"1" forKey:@"paxType"];
                    }
                    else if ([model.travellerType isEqualToString:kTravellerTypeInfant])
                    {
                        [travellerDict setValue:@"2" forKey:@"paxType"];
                    }
                    
                    [travellerDict setValue:passangerCell.textFieldMealType.text forKey:@"mealPreference"];
                    
                    [travellerDetailsArray addObject:travellerDict];
                }
            }
        }
    }
         NSMutableDictionary *jsonDict = [[NSMutableDictionary alloc] init];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.title != [c] %@", @"Master"];
        NSArray *arrayWithResult = [travellerDetailsArray filteredArrayUsingPredicate:predicate];
    
        if (arrayWithResult != nil && arrayWithResult.count > 0)
        {
            NSDictionary* passengerDict = [arrayWithResult[0] copy];
            if (passengerDict != nil && ![passengerDict isEqual:[NSNull new]])
            {
            
                NSString* title = [passengerDict objectForKey:@"title"];
                  NSString* firstName = [passengerDict objectForKey:@"firstName"];
                  NSString* lastName = [passengerDict objectForKey:@"lastName"];
                
                
               [jsonDict setObject:title forKey:@"title"];
                [jsonDict setObject:firstName forKey:@"firstName"];
                [jsonDict setObject:lastName forKey:@"lastName"];
            }
            
           
        }
        
   
    [jsonDict setObject:@"0" forKey:@"addressId"]; //?
    [jsonDict setObject:_bookingAmountString forKey:@"bookingAmount"];
    [jsonDict setObject:_amountType forKey:@"bookingType"];//?
    [jsonDict setObject:travellerDetailsArray forKey:@"tcilHolidayBookingTravellerDetailCollection"];
    [jsonDict setObject:[dictForCalculation valueForKey:@"quotationId"] forKey:@"quotationId"];
  /* [jsonDict setObject:leadPaxDetailCell.textFieldTitle.text forKey:@"title"];
    [jsonDict setObject:leadPaxDetailCell.textFieldfirstName.text forKey:@"firstName"];
    [jsonDict setObject:leadPaxDetailCell.textFieldLastName.text forKey:@"lastName"]; /// as error come on production 18 jan 2018*/
    [jsonDict setObject:addressDetails.txtFieldEmailID.text forKey:@"addressEmailId"];
    [jsonDict setObject:addressDetails.txtFieldContactNo.text forKey:@"addressPhoneNo"];
    [jsonDict setObject:addressDetails.txtFieldAddress1.text forKey:@"addressStreet"];
    [jsonDict setObject:addressDetails.txtFieldCity.text forKey:@"addressCity"];
    [jsonDict setObject:addressDetails.txtFieldState.text forKey:@"addressState"];
    [jsonDict setObject:addressDetails.txtFieldZip.text forKey:@"addressPincode"];
    [jsonDict setObject:_packageDetail.strPackageId forKey:@"packageId"];
    [jsonDict setObject:@"" forKey:@"preConformationPageUrl"];//?
    [jsonDict setObject:@"" forKey:@"bookedForEmail"]; // userEmail ID ??
    [jsonDict setObject:@"false" forKey:@"isOnBefalf"]; // ?
    [jsonDict setObject:@"APP" forKey:@"bookingThrough"];
    [jsonDict setObject:[_selectedStateDict valueForKey:@"gstStateCode"] forKey:@"gstStateCode"];
    [jsonDict setObject:[_selectedStateDict valueForKey:@"isUnionTerritory"] forKey:@"isUnionTerritory"];
    [jsonDict setObject:addressDetails.textFieldGSTInNumber.text forKey:@"gstinNo"];

    NSDictionary *headerDict = [CoreUtility getHeaderDict];
    
    NetworkHelper *helper1 = [NetworkHelper sharedHelper];
    
    [helper1 getResponseWithRequestType:@"POST" withQueryParam:@"" withPathParam:@"" withJsonParam:jsonDict withHeaders:headerDict withUrl:kAstraUrlBooking success:^(NSDictionary *responseDict)
     {
         NSLog(@"Response Dict : %@",responseDict);
         
         
         dispatch_async(dispatch_get_main_queue(), ^(void)
                        {
                            [activityLoadingView removeFromSuperview];
                            
                            if (responseDict)
                            {
                                if (responseDict.count>0)
                                {
                                    //https://uatastra.thomascook.in/paymentGateway.html?tid=alwoMcmH0AX2kzGjw%2FAACA%3D%3D
                                    
                                   // NSString *msg = [responseDict valueForKey:@"message"]; // updated on 18/04/2018
                                    
                                     NSString *msg = [responseDict valueForKey:@"tid"]; /// as said by ajay updated on 18/04/2018
                                    
                                    NSString *urlString = [NSString stringWithFormat:@"%@%@",kpgURLForPayment,msg];
                                    
                                    NSURL *url = [NSURL URLWithString:urlString];
                                    
                                    if (url)
                                    {
                                        [[UIApplication sharedApplication] openURL:url];
                                        [[SlideNavigationController sharedInstance] popToRootViewControllerAnimated:YES];
                                    }
                                }
                                else
                                {
                                    [super showAlertViewWithTitle:@"Alert" withMessage:@"some error occurred"];

                                }
                                    
                            }
                            else
                            {
                                [super showAlertViewWithTitle:@"Alert" withMessage:@"some error occurred"];
                            }
                        });
         
     }
                               failure:^(NSError *error)
     {
         dispatch_async(dispatch_get_main_queue(), ^(void)
                        {
                            [activityLoadingView removeFromSuperview];
                            [super showAlertViewWithTitle:@"Alert" withMessage:@"some error occurred"];
                            NSLog(@"Response Dict : %@",[error description]);
                        });
         
     }];

    }
    @catch (NSException *exception)
    {
        NSLog(@"%@", exception.reason);
        [activityLoadingView removeFromSuperview];
    }
    @finally
    {
        NSLog(@"exception finally called");
    }
    
    
    
}


- (NSString *)parseUrl:(NSString *)url {
    if (url.length == 0 || [url isEqualToString:@"http://"]) {
        return url; // put your desired URL here
    } else if ([url hasPrefix:@"http://"] || [url hasPrefix:@"https://"]) {
        return url;
    } else {
        return [NSString stringWithFormat:@"http://%@", url];
    }
}

-(BOOL)validateName:(NSString *)nameString
{
    NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"];
    for (int i = 0; i < [nameString length]; i++)
    {
        unichar c = [nameString characterAtIndex:i];
        if (![myCharSet characterIsMember:c])
        {
            return NO;
        }
    }
    return YES;
}

-(BOOL)validationOfTravellersInfo
{
    for (int i= 0; i<_travellerArrayForCalculation.count; i++)
    {
        TravellerInformationModel *model = [_travellerArrayForCalculation objectAtIndex:i];
        PassangerDetailCell *cell = model.cell;
        if (cell.textFieldfirstName.text == nil || [cell.textFieldfirstName.text isEqualToString:@""])
        {
            NSString *msg = @"Please enter first name of passenger ";
            
            NSString *strInt = [NSString stringWithFormat:@"%d", (i+1)];
            
            msg = [msg stringByAppendingString:strInt];
            
            [super showAlertViewWithTitle:@"Alert" withMessage:msg];
            
            return NO;
        }
        else if (cell.textFieldLastName.text == nil || [cell.textFieldLastName.text isEqualToString:@""])
        {
            NSString *msg = @"Please enter last name of passenger ";
            
            NSString *strInt = [NSString stringWithFormat:@"%d", (i+1)];
            
            msg = [msg stringByAppendingString:strInt];
            
            [super showAlertViewWithTitle:@"Alert" withMessage:msg];
            
            return NO;
        }
        else if(cell.textFieldDOB.text == nil || [cell.textFieldDOB.text isEqualToString:@""])
        {
            NSString *msg = @"Please select date of birth of passenger ";
            
            NSString *strInt = [NSString stringWithFormat:@"%d", (i+1)];
            
            msg = [msg stringByAppendingString:strInt];
            
            [super showAlertViewWithTitle:@"Alert" withMessage:msg];
            
            return NO;
        }
//        else if (![self validateName:cell.textFieldfirstName.text])
//        {
//            NSString *msg = @"Please enter valid first name of passenger ";
//            
//            NSString *strInt = [NSString stringWithFormat:@"%d", (i+1)];
//            
//            msg = [msg stringByAppendingString:strInt];
//            
//             [super showAlertViewWithTitle:@"Alert" withMessage:msg];
//            
//            return NO;
//        }
//        else if (![self validateName:cell.textFieldLastName.text])
//        {
//            NSString *msg = @"Please enter valid last name of passenger ";
//            
//            NSString *strInt = [NSString stringWithFormat:@"%d", (i+1)];
//            
//            msg = [msg stringByAppendingString:strInt];
//            
//            [super showAlertViewWithTitle:@"Alert" withMessage:msg];
//            
//            return NO;
//
//        }
        else if(![self validateAAdharNumberWithNumber:cell.textFieldAadharNumber.text])
        {
            NSString *msg = @"Please Enter Valid Aadhaar Number of Passenger ";
            
            NSString *strInt = [NSString stringWithFormat:@"%d", (i+1)];
            
            msg = [msg stringByAppendingString:strInt];
            
            [super showAlertViewWithTitle:@"Alert" withMessage:msg];
            
            return NO;
        }
    }
    return YES;
}
-(BOOL)validateAAdharNumberWithNumber:(NSString *)aadharNumber
{
    
    if(![aadharNumber isEqualToString:@""])
    {
        NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
        if ([aadharNumber rangeOfCharacterFromSet:notDigits].location == NSNotFound && aadharNumber.length == 12)
        {
            //numbers are there
            return YES;
        }
        
        return NO;
    }
    
    return YES;
}

-(BOOL) validationForParticularTravellerInfo
{
    TravellerInformationModel *model = [_travellerArrayForCalculation objectAtIndex:previousIndexPath];
    PassangerDetailCell *cell = model.cell;
    if ([cell.textFieldfirstName.text isEqualToString:@""])
    {
        [super showAlertViewWithTitle:@"Alert" withMessage:@"Please enter first name"];
        return NO;
    }
    else if ([cell.textFieldLastName.text isEqualToString:@""])
    {
        [super showAlertViewWithTitle:@"Alert" withMessage:@"Please enter last name"];
        return NO;
    }
    else if([cell.textFieldDOB.text isEqualToString:@""])
    {
        [super showAlertViewWithTitle:@"Alert" withMessage:@"Please select date of birth"];
        return NO;
    }
    
    return YES;
}

- (IBAction)onAdressButtonClicked:(id)sender
{
   
    AdressPopUpVC *detailViewController = [[AdressPopUpVC alloc] initWithNibName:@"AdressPopUpVC" bundle:nil];
    detailViewController.view.frame = CGRectMake(0, 0, 300, self.passangerDetailsView.frame.size.height - 100);
    detailViewController.stateCode = self.stateCode;
    detailViewController.delegate = self;
    
    if(addressDetails)
    {
        detailViewController.txtFieldAddress1.text =  addressDetails.txtFieldAddress1.text;
        detailViewController.txtFieldAddress2.text = addressDetails.txtFieldAddress2.text;
        detailViewController.txtFieldCity.text = addressDetails.txtFieldCity.text;
        detailViewController.txtFieldContactNo.text = addressDetails.txtFieldContactNo.text;
        detailViewController.txtFieldEmailID.text = addressDetails.txtFieldEmailID.text;
        detailViewController.txtFieldState.text = addressDetails.txtFieldState.text;
        detailViewController.txtFieldZip.text = addressDetails.txtFieldZip.text;
        detailViewController.textFieldGSTInNumber.text = addressDetails.textFieldGSTInNumber.text;
    }
    [self presentPopupViewController:detailViewController animationType:MJPopupViewAnimationFade];
}

- (void)continueButtonClicked:(AdressPopUpVC*)secondDetailViewController
{
    addressDetails = secondDetailViewController;
    [self.proceedButton setTitle:[NSString stringWithFormat:@"Payment"] forState:UIControlStateNormal];
}

- (IBAction)onTermsAndConditionClicked:(id)sender
{
    TermsAndCondViewController *termsConditionVC = [[TermsAndCondViewController alloc]initWithNibName:@"BaseViewController" bundle:nil];
    
    NSArray *termsConditionArray = _packageDetail.arrayTermsAndConditions;
    
    NSDictionary *termsAndConditionDict ;
    
    for (int i = 0; i<termsConditionArray.count; i++)
    {
        NSDictionary *termsDict = [ termsConditionArray objectAtIndex:i];
        
        NSInteger pkgClassID = [[termsDict  valueForKey:@"packageClassId"] integerValue];
        
        if (_accomType == pkgClassID)
        {
            termsAndConditionDict = termsDict;
        }
        
    }
    
    NSMutableString *htmlString =  [[NSMutableString alloc] init];
    
    
    if (termsAndConditionDict)
    {
        NSString *cancellationPolicy = [termsAndConditionDict valueForKey:@"cancellationPolicy"];
        NSString *paymentTerms = [termsAndConditionDict valueForKey:@"paymentTerms"];
        
        [htmlString appendFormat:@"<p class=\"p1\">&nbsp;</p> <p class=\"p1\"><span class=\"s1\">Cancellation Policy</span></p> <p class=\"p1\">&nbsp;</p>"];
        [htmlString appendString:cancellationPolicy];
        
        [htmlString appendFormat:@"<p class=\"p1\">&nbsp;</p> <p class=\"p1\"><span class=\"s1\">Payment Terms</span></p> <p class=\"p1\">&nbsp;</p>"];
        [htmlString appendString:paymentTerms];
        
        NSArray *tcilHolidayTermsConditionsCollection = [termsAndConditionDict valueForKey:@"tcilHolidayTermsConditionsCollection"];
        if (tcilHolidayTermsConditionsCollection)
        {
            if (tcilHolidayTermsConditionsCollection.count != 0)
            {
                
                for (int i = 0; i<tcilHolidayTermsConditionsCollection.count; i++)
                {
                    NSDictionary *conditionDict = [tcilHolidayTermsConditionsCollection objectAtIndex:i];
                    
                        [htmlString appendFormat:@"<p class=\"p1\">&nbsp;</p> <p class=\"p1\"><span class=\"s1\">Description</span></p> <p class=\"p1\">&nbsp;</p>"];
                        
                        NSString *descriptionString  = [conditionDict valueForKey:@"description"];
                        
                        [htmlString appendString:descriptionString];
                    
                }
            }
        }
    }

    termsConditionVC.requestUrlString = htmlString;
    
    [[SlideNavigationController sharedInstance] pushViewController:termsConditionVC animated:NO];
}

- (IBAction)onCallButtonClicked:(id)sender
{
    if([_packageDetail.strPackageType caseInsensitiveCompare:@"International"]==NSOrderedSame)
    {
        [FIRAnalytics logEventWithName:@"International_Holidays_Call_Traveller_Details" parameters:@{kFIRParameterItemName:[NSString stringWithFormat:@"PackageName :%@, PackageType :%@, PackageID :%@",_packageDetail.strPackageName,_packageDetail.strPackageType,_packageDetail.strPackageId]}];
        
    }
    else
    {
        [FIRAnalytics logEventWithName:@"Indian_Holidays_Call_Traveller_Details" parameters:@{kFIRParameterItemName:[NSString stringWithFormat:@"PackageName :%@, PackageType :%@, PackageID :%@",_packageDetail.strPackageName,_packageDetail.strPackageType,_packageDetail.strPackageId]}];
    }
    
    CustomIOSAlertView *alertView = [[CustomIOSAlertView alloc] init];
    CoreUtility *coreObj=[CoreUtility sharedclassname];
    coreObj.statusWhichPopUpCalled=0;
    [alertView setContainerView:[self createDemoView]];
    
    
    // Modify the parameters
    [alertView setButtonTitles:[NSMutableArray arrayWithObjects:@"Close1", @"Close2", @"Close3", nil]];
    [alertView setDelegate:self];
    
    // You may use a Block, rather than a delegate.
    [alertView setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
        NSLog(@"Block: Button at position %d is clicked on alertView %d.", buttonIndex, (int)[alertView tag]);
        [alertView close];
    }];
    
    [alertView setUseMotionEffects:true];
    
    // And launch the dialog
    [alertView show];
}

#pragma mark - Pop VIew For Call Us

- (void)customIOS7dialogButtonTouchUpInside: (CustomIOSAlertView *)alertView clickedButtonAtIndex: (NSInteger)buttonIndex
{
    NSLog(@"Delegate: Button at position %d is clicked on alertView %d.", (int)buttonIndex, (int)[alertView tag]);
    [alertView close];
}

- (UIView *)createDemoView
{
    UIView *demoView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 290, 200)];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 30, 30)];
    [imageView setImage:[UIImage imageNamed:@"customer_support.png"]];
    UILabel *lblCallus=[[UILabel alloc]initWithFrame:CGRectMake(50, 10, 200, 40)];
    //Old Contact Number
    //lblCallus.text=@"1800 2099 100";
    
    //New Contact Number
    lblCallus.text= @"8652908370"; //@"18002000464,7039003560"; //new mobile number 4th Nov 2019
    
    [demoView addSubview:lblCallus];
    [demoView addSubview:imageView];
    
    
    return demoView;
}

/*
 NSMutableDictionary *profileDetail = @{ @"FIRST_NAME" : "<first name>", @"LAST_NAME" : "<last name>", @"EMAIL" : "<email id>", @"CITY" : "<city>", @"FORM_TYPE" : "<view details /booking>"};
 
 NetCoreAppTracking.sharedInstance().sendEvent(withCustomPayload: 102, payload: profileDetail, block: nil);
 */

#pragma mark netCore LogEvents

-(void)netCoreBookingForm
{
    [profileDetail setObject:@"viewdetails/booking" forKey:@"FORM_TYPE"];
    
    [NetCoreAnalyticsVC getNetCorePushNotificationDetailDictionary:profileDetail withPayloadCount:102];
    
    
    CoreUtility *coreobj=[CoreUtility sharedclassname];

    if (coreobj.strPhoneNumber.count != 0)
    {
        NSString *phoneNumber = coreobj.strPhoneNumber[0];
        if (phoneNumber != nil && ![phoneNumber isEqualToString:@""])
        {
           // [[NetCoreInstallation sharedInstance] netCoreProfilePush:phoneNumber Payload:profileDetail Block:nil];
        }
        else
        {
           // [[NetCoreInstallation sharedInstance] netCoreProfilePush:@"" Payload:profileDetail Block:nil];
        }
    }
    else
    {
        //[[NetCoreInstallation sharedInstance] netCoreProfilePush:@"" Payload:profileDetail Block:nil];
    }

}
@end
