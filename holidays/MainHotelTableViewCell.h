//
//  MainHotelTableViewCell.h
//  holidays
//
//  Created by Kush_Tech on 04/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HolidayPackageDetail.h"
#import "HolidayAccomodationObject.h"
#import "PackageDetailModel.h"
NS_ASSUME_NONNULL_BEGIN
@protocol MainHotelTableViewCellDelegaete <NSObject>
@optional
- (void)showHotelWithSectionTitle:(NSString*)sectionTitle withPackageModel:(PackageDetailModel*)packageModel;
@end
@interface MainHotelTableViewCell : UITableViewCell <UITableViewDataSource,UITableViewDelegate>
{
    
    NSMutableArray *hotelObjectsArray;
    NSMutableArray *sightObjectsArray;
    NSMutableDictionary *sightObjectsDict;
    PackageDetailModel *packageDetailModel;
}
@property (nonatomic, weak) id <MainHotelTableViewCellDelegaete> mainHotelDelegate;
@property (weak, nonatomic) IBOutlet UITableView *main_tableView;
@property (strong ,nonatomic) HolidayPackageDetail *packageDetail;
@property(nonatomic)NSInteger packageNum;
@property (nonatomic) BOOL isShowViewMore;
@property (nonatomic, strong) NSString *sectionTitle;
@property (weak, nonatomic) IBOutlet UIButton *btn_ViewMore;
@property (weak, nonatomic) IBOutlet UIWebView *webViews;
-(void)loadHotelDataFromPackageModel:(PackageDetailModel*)packageModel forHolidayPackageDetail:(HolidayPackageDetail*)holiPackageDetailsModel;
-(void)loadSightSeeingDataFromPackageModel:(PackageDetailModel*)packageModel forHolidayPackageDetail:(HolidayPackageDetail*)holiPackageDetailsModel;
@end

NS_ASSUME_NONNULL_END
