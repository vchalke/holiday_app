//
//  gridViewCollectionCell.h
//  SOTCAPPDEsign
//
//  Created by Kush Thakkar on 03/03/20.
//  Copyright © 2020 Kush Thakkar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WhatsNewObject.h"
NS_ASSUME_NONNULL_BEGIN

@interface gridViewCollectionCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIView *view_belowImgView;
@property (weak, nonatomic) IBOutlet UIImageView *img_MainView;
@property (weak, nonatomic) IBOutlet UILabel *lbl_title;
@property (weak, nonatomic) IBOutlet UILabel *lbl_days;
@property (weak, nonatomic) IBOutlet UILabel *lbl_prize;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_fromLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_fromTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *const_fromRight;
-(void)setCollectionView:(WhatsNewObject*)object;
-(void)showCherryBlossom:(WhatsNewObject*)object;
@end

NS_ASSUME_NONNULL_END
