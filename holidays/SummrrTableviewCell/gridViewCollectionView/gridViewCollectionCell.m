//
//  gridViewCollectionCell.m
//  SOTCAPPDEsign
//
//  Created by Kush Thakkar on 03/03/20.
//  Copyright © 2020 Kush Thakkar. All rights reserved.
//

#import "gridViewCollectionCell.h"
#import "UIImageView+WebCache.h"
@implementation gridViewCollectionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(void)setCollectionView:(WhatsNewObject*)object{
//    self.lbl_title.text = object.destinationName;
     self.lbl_title.text = object.bannerName;
//    NSString *str=object.bannerDuration;
//    NSArray *items = [str componentsSeparatedByString:@"|"];
//    NSString *str1=[items objectAtIndex:0];
    self.const_fromLeft.constant = 4.0;
    self.const_fromRight.constant = 4.0;
     self.const_fromTop.constant = 4.0;
    self.lbl_days.text = object.bannerDuration;
    self.lbl_prize.text = [NSString stringWithFormat:@"Starting @ %@",object.bannerStrikeoutPrice];
    NSLog(@"%@",object.mainBannerImgUrl);
    NSURL* urlImage=[NSURL URLWithString:object.bannerSmallImage];
    if (urlImage == nil){
        urlImage=[NSURL URLWithString:object.mainBannerImgUrl];
    }
    if(urlImage)
    {
        UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        indicator.center = self.img_MainView.center;
        [self.img_MainView addSubview:indicator];
        [indicator startAnimating];
        [self.img_MainView sd_setImageWithURL:urlImage placeholderImage:[UIImage imageNamed:@"defaultBanner.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
            [indicator stopAnimating];
        }];
    }
}
-(void)showCherryBlossom:(WhatsNewObject*)object{
    self.lbl_title.text = object.bannerName;
        self.lbl_days.text = object.bannerDuration;
        self.lbl_prize.text = [NSString stringWithFormat:@"Starting @ %@",object.bannerStrikeoutPrice];
    NSURL* urlImage=[NSURL URLWithString:object.bannerSmallImage];
    if (urlImage == nil){
        urlImage=[NSURL URLWithString:object.mainBannerImgUrl];
    }
    if(urlImage)
    {
        UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        indicator.center = self.img_MainView.center;
        [self.img_MainView addSubview:indicator];
        [indicator startAnimating];
        [self.img_MainView sd_setImageWithURL:urlImage placeholderImage:[UIImage imageNamed:@"defaultBanner.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
            [indicator stopAnimating];
        }];
    }
}
@end
