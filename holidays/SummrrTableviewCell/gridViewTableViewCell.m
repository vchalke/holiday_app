//
//  gridViewTableViewCell.m
//  SOTCAPPDEsign
//
//  Created by Kush Thakkar on 03/03/20.
//  Copyright © 2020 Kush Thakkar. All rights reserved.
//

#import "gridViewTableViewCell.h"
#import "gridViewCollectionCell.h"
#import "DateStringSingleton.h"
#import "WhatsNewObject.h"
@implementation gridViewTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)btn_pressSummerHoliday:(UIButton *)sender {
//    [self.delegate jumpToSummerHolidayWithTag:sender.tag];
    [self.delegate jumpToTheWebViewFromSummer:self.buttonObject.redirectUrl withTitle:self.buttonObject.bannerName withObject:self.buttonObject];
}

-(void)setupCollectionViewTitle:(NSString*)title subTitle:(NSString*)subTitle
{
    [self.collectionView registerNib:[UINib nibWithNibName:@"gridViewCollectionCell" bundle:nil] forCellWithReuseIdentifier:@"gridViewCollectionCell"];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.lbl_title.text = title;
    self.lbl_subtitle.text = subTitle;
    
//    [self.btn_viewAllHoliday setTitle:[NSString stringWithFormat:@"View All Summer Holidays %@",[[DateStringSingleton shared]getDateTimeStringFormat:@"yyyy" DateValue:[NSDate date]]] forState:UIControlStateNormal];
    
//    [self.btn_viewAllHoliday setTitle:[NSString stringWithFormat:@"View All %@",title] forState:UIControlStateNormal];
//    [self.btn_viewAllHoliday setTitle:[NSString stringWithFormat:@"%@",title] forState:UIControlStateNormal];
//    self.btn_viewAllHoliday.tag = ([title isEqualToString:@"Summer Holidays 2020"]) ? 1 : 2 ;
}
-(void)setButtonDataUsingArray:(NSArray*)dataArray{
    for (id object in dataArray){
        WhatsNewObject *wObject = object;
        if (wObject.bannerOrder==1 && wObject.orderOnThePage==1){
            [self.btn_viewAllHoliday setTitle:[NSString stringWithFormat:@"%@",wObject.bannerName] forState:UIControlStateNormal];
            self.buttonObject = wObject;
        }
    }
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [self.dataArray count];//4;
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    gridViewCollectionCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"gridViewCollectionCell" forIndexPath:indexPath];
        WhatsNewObject *bannerObj = [[WhatsNewObject alloc] initWithImgArray:[self.dataArray objectAtIndex:indexPath.row]];
        [cell setCollectionView:bannerObj];
//    NSArray *imgArray = [NSArray arrayWithObjects:@"natureOne",@"natureTwo",@"natureThree",@"natureFour", nil];
//    cell.view_belowImgView.hidden = YES;
//    cell.img_MainView.image = [UIImage imageNamed:[imgArray objectAtIndex:indexPath.row]];
    return cell;
}
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(1.0, 1.0, 1.0, 1.0);
}
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
//    CGFloat width = collectionView.frame.size.width / 2 - 10;
//    CGFloat hight = collectionView.frame.size.height / 4 + 110;
    CGFloat width = collectionView.frame.size.width*0.48;
    CGFloat hight = collectionView.frame.size.height*0.48;
    return CGSizeMake(width, hight);
    
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    WhatsNewObject *bannerObj = [[WhatsNewObject alloc] initWithImgArray:[self.dataArray objectAtIndex:indexPath.row]];
    [self.delegate jumpToTheWebViewFromSummer:bannerObj.redirectUrl withTitle:bannerObj.bannerName withObject:bannerObj];
}

@end
