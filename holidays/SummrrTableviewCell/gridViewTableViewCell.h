//
//  gridViewTableViewCell.h
//  SOTCAPPDEsign
//
//  Created by Kush Thakkar on 03/03/20.
//  Copyright © 2020 Kush Thakkar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WhatsNewObject.h"
NS_ASSUME_NONNULL_BEGIN
@protocol gridViewTableViewCellDelegate <NSObject>
@optional
-(void)jumpToTheWebViewFromSummer:(NSString *)webUrlString withTitle:(NSString *)webViewTitle withObject:(WhatsNewObject *)newObject;
-(void)jumpToSummerHolidayWithTag:(NSInteger)tagValue;
@end

@interface gridViewTableViewCell : UITableViewCell<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
-(void)setupCollectionViewTitle:(NSString*)title subTitle:(NSString*)subTitle;
-(void)setButtonDataUsingArray:(NSArray*)dataArray;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cnsheightCollectionView;
@property (nonatomic, weak) id <gridViewTableViewCellDelegate> delegate;
@property (weak, nonatomic)NSArray *dataArray;
@property (weak, nonatomic)WhatsNewObject *buttonObject;
@property (weak, nonatomic) IBOutlet UILabel *lbl_title;
@property (weak, nonatomic) IBOutlet UILabel *lbl_subtitle;
@property (weak, nonatomic) IBOutlet UIButton *btn_viewAllHoliday;

@end

NS_ASSUME_NONNULL_END
