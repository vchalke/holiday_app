//
//  PaymentViewController.swift
//  sotc-consumer-application
//
//  Created by Saurav Kumar on 09/08/17.
//  Copyright © 2017 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit
import CoreData



struct PaymentCurrency {
    
    static let Australlian = ("AUD", "e3f1d1")
    static let Canadian = ("CAD", "fbd3cb")
    static let Swiss = ("CHF", "caecf8")
    static let Euro = ("EUR", "f8dbea")
    static let Sterling = ("GBP", "e4deed")
    static let Indian = ("INR", "cdede5")
    static let Japanese = ( "JPY", "d1e0f1")
    static let NewZeaLand = ("NZD", "d1d3d6")
    static let Singapore = ("SGD", "fceac6")
    static let Thai = ("THB", "e5e8eb")
    static let US = ("USO", "faccd0")
    static let SouthAfrican = ("ZAR", "f4f5f7")
}


class PaymentViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource, ARPieChartDelegate, ARPieChartDataSource,UIDocumentInteractionControllerDelegate,UITextFieldDelegate  {
    
    //MARK: - OUTLETS
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var switchView: UIView!
    @IBOutlet var changeCurrencySwitch: UISwitch!
    @IBOutlet var paymentView: UIView!
    
    @IBOutlet var currentROEDate: UILabel!
    @IBOutlet var totalAmountLabel: UILabel!
    @IBOutlet var paidAmountLabel: UILabel!
    
    @IBOutlet var currentAmountpercentageLabel: UILabel!
    @IBOutlet var paymentPieChart: ARPieChart!
    @IBOutlet var paymentReceiptTableView: UITableView!
    @IBOutlet var paymentCollectionView: UICollectionView!
    
    @IBOutlet var paymentViewWithTableView: UIView!
    @IBOutlet var proformaButtonView: UIView!
    @IBOutlet var paymentReceiptView: UIView!
    @IBOutlet var pendingPaymentView: UIView!
    @IBOutlet var graphView: UIView!
    @IBOutlet weak var paymentTableview: UITableView!
    
    
    
    //MARK: - CLASS VARIABLES
   // var params : PUMRequestParams = PUMRequestParams.shared()
  //  var utils : Utils = Utils()
    
    var isfromNotificationHomeVc:Bool = false
    var tourStatus:TourStatus?
    //var isNotFromDidLoad = true
    
    var dataItems: NSMutableArray = []
    
    var tourDetail:Tour?
    var bfNumber:String?
    var currencyArray:Array<CurrencyData>?
    var CurrencyCode:Dictionary<String,Any>?
    
    var paymentList:[Payment]?
    var paymentReceiptList:[PaymentReceipt]?
    
    var paymentButton:UIButton?
    var roeButton:UIButton?
    
    @IBOutlet weak var paymentTableViewHeightConstraints: NSLayoutConstraint!
    var curentlPaymentCellIndexPath:IndexPath = IndexPath(row: -1, section: -1)
    var formatterArray:Array<NumberFormatter> = []
    var alertController:UIAlertController?
    var localeList = NSLocale.availableLocaleIdentifiers
    var editPaymentAmountalertController:UIAlertController?
    var reciptGenerationFXDetails:(isFX:Bool,fxCurrencyType:String,roeRate:String,fxAmount:String,isA2Attached:Bool) = (isFX:false,fxCurrencyType:"",roeRate:"",fxAmount:"",isA2Attached:false)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        
        LoadingIndicatorView.show("Loading")
        
         UserBookingDBManager.deleteCurencyData()
        
        //self.updatePaymentData()
        self.updatePaymentDataInPaymentTableView()
        
      //  self.configurePayUNotification()
        
        DispatchQueue.main.after(1.0, execute: {
            
            self.fetchNSLocaleFormatter()
            
           // self.isNotFromDidLoad = false
           // self.fetchROEData()
            
            self.paymentTableview.reloadData()
            self.paymentReceiptTableView.reloadData()
            self.paymentCollectionView.reloadData()
            
            LoadingIndicatorView.hide()
            
        })
        
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
         self.navigationController?.navigationBar.isHidden = false
        
        self.navigationController?.setRedNavigationBar()
        self.navigationController?.setnavigatiobBarTitle(title: Title.PAYMENT, viewController: self)
        //self.navigationController?.setNagationBackButton(showRightButton: true, viewController: self)
        self.navigationController?.setEmergencyNavigationRightButton(showEmergencyButton: true, viewController: self)
        
        self.resetCurentlPaymentCellIndexPath()
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        paymentPieChart.reloadData()
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        
     if self.navigationController?.viewControllers.firstIndex(of: self) == nil && self.isfromNotificationHomeVc
        {
            SlideNavigationController.sharedInstance().popViewController(animated: true)
        }
     
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func getSortedPaymentreciptByDate() -> [PaymentReceipt]
    {
        
        if let paymentReceiptArray = Array((tourDetail?.receiptRelation)!) as? [PaymentReceipt]
        {
            if paymentReceiptArray.count > 0
            {
                let soretedPaymentRecipt = paymentReceiptArray.sorted(by: { (paymentrecipt1:PaymentReceipt, paymentrecipt2:PaymentReceipt) -> Bool in
                    
                  //  paymentrecipt1.receiptDate?.compare(paymentrecipt2.receiptDate! as Date) == ComparisonResult.orderedDescending
                    
                    if paymentrecipt1.receiptDate?.compare(paymentrecipt2.receiptDate! as Date) != ComparisonResult.orderedSame
                    {
                      return  paymentrecipt1.receiptDate?.compare(paymentrecipt2.receiptDate! as Date) == ComparisonResult.orderedDescending
                    }else
                    {
                        return paymentrecipt1.receiptNumber?.localizedCompare(paymentrecipt2.receiptNumber!) == .orderedDescending
                        
                    }
                    
                })
                
                return soretedPaymentRecipt
                
            }
        }
        return []
        
    }
    
    //MARK:Amount Calulation
      func updatePaymentDataInPaymentTableView()  {
           self.paymentTableview.register(UINib(nibName: "SOTCPaymentTableViewCell", bundle: nil), forCellReuseIdentifier:  "SOTCPaymentTableViewCellID")
        self.paymentReceiptTableView.register(UINib(nibName:  "PaymentReceiptTableViewCell", bundle: nil), forCellReuseIdentifier: "PaymentReceipt")
        
        self.paymentCollectionView.register(UINib(nibName: "PaymentPendingCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PaymentPending")
        
        self.paymentReceiptTableView.tableFooterView = UIView(frame: .zero)
        
        self.paymentTableview.tableFooterView = UIView(frame: .zero)
        
        paymentPieChart.delegate = self
        paymentPieChart.dataSource = self
        paymentPieChart.showDescriptionText = true
        paymentPieChart.outerRadius = CGFloat(paymentPieChart.frame.height * 0.4)
        paymentPieChart.innerRadius = CGFloat(paymentPieChart.frame.height * 0.2)
        
        paymentCollectionView.dataSource = self
        paymentCollectionView.delegate = self
        
        
        if paymentList != nil && (paymentList?.count)! > 0
        {
            paymentList?.removeAll()
        }
        
        if paymentReceiptList != nil && (paymentReceiptList?.count)! > 0
        {
            paymentReceiptList?.removeAll()
        }
        
        
        paymentList = Array((tourDetail?.paymentRelation)!) as? [Payment]
        
        // paymentReceiptList = Array((tourDetail?.receiptRelation)!) as? [PaymentReceipt]
        
        paymentReceiptList = self.getSortedPaymentreciptByDate()
        
        if (paymentList?.count)! > 0
        {
            
            
            if paymentList?.count == 1 {
                
                
                self.switchView.isHidden = true
            }
            
            
            let paymentListINR = paymentList?.filter({ (Payment) -> Bool in
                
                Payment.currency?.uppercased() == "INR"
            })
            
            if paymentListINR != nil && (paymentListINR?.count)! > 0 {
                
                
                let paymentObjectIndex = (paymentList?.firstIndex(of: (paymentListINR?.first)!))
                
                
                paymentList?.remove(at: paymentObjectIndex!)
                
                paymentList?.insert((paymentListINR?.first)!, at: 0)
                
                
                self.updateGraph(paymentObject: (paymentListINR?.first)!)
                
                
                var collectionViewWidth:CGFloat = 0.0
                
                if paymentList?.count == 2 {
                    
                    collectionViewWidth = (UIScreen.main.bounds.width / 2)
                }
                else
                {
                    collectionViewWidth = 150.0
                }
                
                let cellSize = CGSize(width:collectionViewWidth , height:100)
                
                let layout = UICollectionViewFlowLayout()
                layout.scrollDirection = .horizontal //.horizontal
                layout.itemSize = cellSize
                layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
                layout.minimumLineSpacing = 0.0
                layout.minimumInteritemSpacing = 10.0
                
                self.paymentCollectionView.reloadData()
                
                paymentCollectionView.setCollectionViewLayout(layout, animated: true)
            }
            else
            {
                self.updateGraph(paymentObject: (paymentList?.first)!)
                
                
                var collectionViewWidth:CGFloat = 0.0
                
                if paymentList?.count == 2 {
                    
                    collectionViewWidth = (UIScreen.main.bounds.width / 2)
                }
                else
                {
                    collectionViewWidth = 150.0
                }
                
                let cellSize = CGSize(width:collectionViewWidth , height:100)
                
                let layout = UICollectionViewFlowLayout()
                layout.scrollDirection = .horizontal //.horizontal
                layout.itemSize = cellSize
                layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
                layout.minimumLineSpacing = 0.0
                layout.minimumInteritemSpacing = 10.0
                
                paymentCollectionView.reloadData()
                
                paymentCollectionView.setCollectionViewLayout(layout, animated: true)
                
                
            }
            
        }
        else
        {
            graphView.isHidden = true
            pendingPaymentView.isHidden = true
            proformaButtonView.isHidden = true
            paymentReceiptView.isHidden = true
            switchView.isHidden = true
            
        }
        
      
       // self.paymentView.frame = CGRect(x: 0, y: 0, width: (self.scrollView.frame.maxX), height:CGFloat(500 + ((paymentReceiptList?.count)! * 112)))
        
        
        //self.scrollView.contentSize = CGSize(width: (self.scrollView.frame.maxX), height: CGFloat(500 + ((paymentReceiptList?.count)! * 112)))
        
       // self.scrollView.addSubview(paymentView)
        
        var height:Int = (300 + ((paymentReceiptList?.count ?? 0) * 112))
        height += ((paymentList?.count)! * 50)
        let heigthFT:CGFloat = CGFloat(height)
        self.paymentViewWithTableView.frame = CGRect(x: 0, y: 0, width: self.scrollView.frame.size.width, height: heigthFT)
        
          self.paymentTableViewHeightConstraints.constant = CGFloat(((paymentList?.count ?? 0) * 50))
        
        self.paymentView.frame = CGRect(x: 0, y: 0, width: (self.scrollView.frame.maxX), height:CGFloat(300 + ((paymentReceiptList?.count ?? 0) * 112)))
        
        self.scrollView.contentSize = CGSize(width: (self.scrollView.frame.maxX), height: heigthFT)
        
        self.scrollView.addSubview(paymentViewWithTableView)
        
    }
    
    func updatePaymentData()  {
        
        self.paymentReceiptTableView.register(UINib(nibName:  "PaymentReceiptTableViewCell", bundle: nil), forCellReuseIdentifier: "PaymentReceipt")
        
        self.paymentCollectionView.register(UINib(nibName: "PaymentPendingCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PaymentPending")
        
        self.paymentReceiptTableView.tableFooterView = UIView(frame: .zero)
        
        
        paymentPieChart.delegate = self
        paymentPieChart.dataSource = self
        paymentPieChart.showDescriptionText = true
        paymentPieChart.outerRadius = CGFloat(paymentPieChart.frame.height * 0.4)
        paymentPieChart.innerRadius = CGFloat(paymentPieChart.frame.height * 0.2)
        
        paymentCollectionView.dataSource = self
        paymentCollectionView.delegate = self
        
        
        if paymentList != nil && (paymentList?.count)! > 0
        {
            paymentList?.removeAll()
        }
        
        if paymentReceiptList != nil && (paymentReceiptList?.count)! > 0
        {
            paymentReceiptList?.removeAll()
        }
        
        
        paymentList = Array((tourDetail?.paymentRelation)!) as? [Payment]
        
        // paymentReceiptList = Array((tourDetail?.receiptRelation)!) as? [PaymentReceipt]
        
        paymentReceiptList = self.getSortedPaymentreciptByDate()
        
        if (paymentList?.count)! > 0
        {
            
            
            if paymentList?.count == 1 {
                
                
                self.switchView.isHidden = true
            }
            
            
            let paymentListINR = paymentList?.filter({ (Payment) -> Bool in
                
                Payment.currency?.uppercased() == "INR"
            })
            
            if paymentListINR != nil && (paymentListINR?.count)! > 0 {
                
                
                let paymentObjectIndex = (paymentList?.firstIndex(of: (paymentListINR?.first)!))
                
                
                paymentList?.remove(at: paymentObjectIndex!)
                
                paymentList?.insert((paymentListINR?.first)!, at: 0)
                
                
                self.updateGraph(paymentObject: (paymentListINR?.first)!)
                
                
                var collectionViewWidth:CGFloat = 0.0
                
                if paymentList?.count == 2 {
                    
                    collectionViewWidth = (UIScreen.main.bounds.width / 2)
                }
                else
                {
                    collectionViewWidth = 150.0
                }
                
                let cellSize = CGSize(width:collectionViewWidth , height:100)
                
                let layout = UICollectionViewFlowLayout()
                layout.scrollDirection = .horizontal //.horizontal
                layout.itemSize = cellSize
                layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
                layout.minimumLineSpacing = 0.0
                layout.minimumInteritemSpacing = 10.0
                
                
                self.paymentCollectionView.reloadData()
                
                paymentCollectionView.setCollectionViewLayout(layout, animated: true)
            }
            else
            {
                self.updateGraph(paymentObject: (paymentList?.first)!)
                
                
                var collectionViewWidth:CGFloat = 0.0
                
                if paymentList?.count == 2 {
                    
                    collectionViewWidth = (UIScreen.main.bounds.width / 2)
                }
                else
                {
                    collectionViewWidth = 150.0
                }
                
                let cellSize = CGSize(width:collectionViewWidth , height:100)
                
                let layout = UICollectionViewFlowLayout()
                layout.scrollDirection = .horizontal //.horizontal
                layout.itemSize = cellSize
                layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
                layout.minimumLineSpacing = 0.0
                layout.minimumInteritemSpacing = 10.0
                
                 paymentCollectionView.reloadData()
                
                paymentCollectionView.setCollectionViewLayout(layout, animated: true)
                
               
            }
            
        }
        else
        {
            graphView.isHidden = true
            pendingPaymentView.isHidden = true
            proformaButtonView.isHidden = true
            paymentReceiptView.isHidden = true
            switchView.isHidden = true
            
        }
        
        
        self.paymentView.frame = CGRect(x: 0, y: 0, width: (self.scrollView.frame.maxX), height:CGFloat(500 + ((paymentReceiptList?.count)! * 112)))
        
        
        self.scrollView.contentSize = CGSize(width: (self.scrollView.frame.maxX), height: CGFloat(500 + ((paymentReceiptList?.count)! * 112)))
        
        self.scrollView.addSubview(paymentView)
        
        
    }
    
    func updateGraph(paymentObject:Payment)
    {
        dataItems.removeAllObjects()
        
        let paidAmountFloat:CGFloat = CGFloat(Double((paymentObject.totalReceived)!)!)
        let totalAmountFloat:CGFloat = CGFloat(Double((paymentObject.totalReceivable)!)!)
        let paidPercentage:CGFloat?
        
        if totalAmountFloat > 0.0
        {
            paidPercentage =  (paidAmountFloat / totalAmountFloat) * 100
            
            let paidAmount = randomItem()
            paidAmount.value = paidPercentage!
            paidAmount.color = AppUtility.hexStringToUIColor(hex: "8cd3da")
            
            let totalAmount = randomItem()
            totalAmount.color = AppUtility.hexStringToUIColor(hex: "4EA8B0")
            totalAmount.value = 100 - paidPercentage!
            
            dataItems.add(paidAmount)
            dataItems.add(totalAmount)
            
            
            if (Int32(paidPercentage!) >= 1)
            {
                if (Int32(paidPercentage!) > 100) {
                    
                    self.currentAmountpercentageLabel.text = "100 %"
                }
                else
                {
                    self.currentAmountpercentageLabel.text = "\(Int32(paidPercentage!) )%"
                }
                
                
                
            }
            else
            {
                self.currentAmountpercentageLabel.text = String(format: "%.2f", paidPercentage!) + "%" //"\(Double(paidPercentage!).rounded(FloatingPointRoundingRule.awayFromZero) )%"
            }
            
        }
        
        
        let part1Attr = [convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor): self.paidAmountLabel.textColor, convertFromNSAttributedStringKey(NSAttributedString.Key.font): self.paidAmountLabel.font] as [String : Any]
        let part2Attr = [convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor): AppUtility.hexStringToUIColor(hex: "333333") , convertFromNSAttributedStringKey(NSAttributedString.Key.font): self.paidAmountLabel.font] as [String : Any]
        
        
        let paidAmount1 = NSMutableAttributedString(string: "\(paymentObject.totalReceived?.numberFormatterDecimal() ?? "")", attributes: convertToOptionalNSAttributedStringKeyDictionary(part1Attr))
        let totalAmount1 = NSMutableAttributedString(string: "\(paymentObject.totalReceivable?.numberFormatterDecimal() ?? "")", attributes: convertToOptionalNSAttributedStringKeyDictionary(part1Attr))
        
        let inrCurrency = NSMutableAttributedString(string: " \(paymentObject.currency?.uppercased() ?? "")", attributes: convertToOptionalNSAttributedStringKeyDictionary(part2Attr))
        
        
        
        let combinationPaid = NSMutableAttributedString()
        let combinationTotal = NSMutableAttributedString()
        
        combinationPaid.append(paidAmount1)
        combinationPaid.append(inrCurrency)
        
        combinationTotal.append(totalAmount1)
        combinationTotal.append(inrCurrency)
        
        
        paidAmountLabel.attributedText = combinationPaid
        
        totalAmountLabel.attributedText = combinationTotal
        
        
    }
    
    func fetchNSLocaleFormatter()  {
        
        
        for locale in NSLocale.availableLocaleIdentifiers {
            
            let formatter = NumberFormatter()
            formatter.locale = NSLocale(localeIdentifier: locale) as Locale?
            self.formatterArray.append(formatter)
            
        }
    }
    
    /*
    func fetchROEData()  {
        
        LoadingIndicatorView.show()
        
        UserBookingController.getExchangeRate { (status, currenyData) in
            if status && currenyData != nil && currenyData?.count ?? 0 > 0
            {
                self.currencyArray = currenyData! //self.getExchangeRate();
                
//                if self.isNotFromDidLoad
//                {
                    //self.showAlertForFewSecondsWithTitle(title: "", message: AlertMessage.Payment.ROE_DATA_UPDATED)
               // }
                
            }else
            {
                self.showAlertROEDataNotFoundTitle(title: AlertMessage.TITLE_Alert, message:"\(AlertMessage.Payment.ROE_DATA_NOT_FOUND)")
            }
            
            LoadingIndicatorView.hide()
        }
        
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let currentDate = dateFormatter.string(from: Date())
        
        self.currentROEDate.text = "Rate of Exchanges as per date \(currentDate)"
        
        CoreUtility.reloadRequestID()
    }
    */
    
    
    
    
    func fetchROEData(Type:String,pendingPayment:Payment) {
        // self.onPayNowButton()
        
       LoadingIndicatorView.show()

        UserBookingController.getASTRASessionAndTockenID { (status, dataDict) in

        if status && dataDict != nil
        {

        if let fcyCurrencyDetails = self.getFirstFCurrencyValue(dataDict: dataDict!)
        {
            UserBookingController.getCurrencyByCode(currencyCode: pendingPayment.currency ?? "", requestId: fcyCurrencyDetails.requestId, sessionId: fcyCurrencyDetails.tokenId, completion: { (status, currencyData) in

        LoadingIndicatorView.hide()

        printLog("getCurrencyByCode :\(status)")

        if status && currencyData != nil
        {
        if  self.currencyArray == nil
        {
        self.currencyArray = []

        }

        self.currencyArray?.append(currencyData!)
            
            if Type.caseInsensitiveCompare("PayNow") == .orderedSame
            {
                self.onPayNowButton()
            }else if Type == "ROE" {
                self.onROEButton(pendingPayment: pendingPayment)
            }
        }else{

            self.showAlertROEDataNotFoundTitle(title: AlertMessage.TITLE_Alert, message:"\(AlertMessage.Payment.ROE_DATA_NOT_FOUND)",pendingPayment:pendingPayment)
        }


        })

        }else{

        LoadingIndicatorView.hide()

            self.showAlertROEDataNotFoundTitle(title: AlertMessage.TITLE_Alert, message:"\(AlertMessage.Payment.ROE_DATA_NOT_FOUND)",pendingPayment:pendingPayment)
        }
        }else
        {
        LoadingIndicatorView.hide()
            self.showAlertROEDataNotFoundTitle(title: AlertMessage.TITLE_Alert, message:"\(AlertMessage.Payment.ROE_DATA_NOT_FOUND)",pendingPayment:pendingPayment)
        }

        }


        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let currentDate = dateFormatter.string(from: Date())

        self.currentROEDate.text = "Rate of Exchanges as per date \(currentDate)"
    }
    
    
    func getFirstFCurrencyValue(dataDict:Dictionary<String,Any>) -> (currencyCode:String,requestId:String,tokenId:String)? {
        
        if let requestId = dataDict["requestId"] as? String , !requestId.isEmpty {
            
            if let tokenId = dataDict["tokenId"] as? String , !tokenId.isEmpty {
                
                if (self.paymentList?.count)! > 0
                {
                    let paymentListOther = self.paymentList?.filter({ (Payment) -> Bool in
                        
                        Payment.currency?.uppercased() != "INR"
                    })
                    
                    if paymentListOther != nil && (paymentListOther?.count)! > 0 {
                        
                        
                        if let payment:Payment = paymentListOther?.first
                        {
                            if let currency:String = payment.currency
                            {
                                return (currency,requestId,tokenId)
                            }
                        }
                        
                    }
                    
                }
            }
            
        }
        return nil
    }
    
    
    //MARK:TableView Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        switch tableView {
        case self.paymentTableview:
             return paymentList?.count ?? 0
        case self.paymentReceiptTableView:
            return paymentReceiptList?.count ?? 0
        default:
            return 0
        }
        
     /*  if paymentList != nil
       {
        return (paymentList?.count)!
        }
        return 0*/
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        switch tableView {
            
        case self.paymentTableview:
        
            let cell :  SOTCPaymentTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SOTCPaymentTableViewCellID", for: indexPath) as! SOTCPaymentTableViewCell
            let paymentObject : Payment = paymentList![indexPath.row]
            
            // cell.populateData(paymentObject: paymentObject,formatterArray:formatterArray)
            
            
            let filteredArray = formatterArray.filter(){$0.currencyCode == paymentObject.currency}
            
            
            if filteredArray.count > 0 {
                
                cell.labelPaidAmount.text = "\(filteredArray.first?.currencySymbol ?? "") \(paymentObject.totalReceived!)"
                cell.labelUnpaidAmount.text = "\(filteredArray.first?.currencySymbol ?? "") \(paymentObject.balancePending!)"
                cell.labelTotalAmount.text = "\(filteredArray.first?.currencySymbol ?? "") \(paymentObject.totalReceivable!)"
                
            }
            else
            {
                cell.labelPaidAmount.text = "\(paymentObject.currency ?? "") \(paymentObject.totalReceived!)"
                cell.labelUnpaidAmount.text = "\(paymentObject.currency ?? "") \(paymentObject.balancePending!)"
                cell.labelTotalAmount.text = "\(paymentObject.currency ?? "") \(paymentObject.totalReceivable!)"
            }
            
            
            
            var paidPercentage:Double = 0.0
            
            if let paidAmountFloat:Float = Float(paymentObject.totalReceived ?? "0.0")
            {
                if let totalAmountFloat:Float = Float(paymentObject.totalReceivable ?? "0.0")
                {
                    paidPercentage =  Double((paidAmountFloat / totalAmountFloat))
                }
            }
            
            let doubleStr:String = String(format: "%.1f", paidPercentage)
            
            var finalPaidPercentage:Double = 0.0
            
            if doubleStr == "0.0" && paidPercentage > 0.000000000001
            {
                finalPaidPercentage = 0.1
                
            }
            else if doubleStr == "1.0" && paidPercentage < 1.000000000000
            {
                finalPaidPercentage = 0.9
            }else
            {
                finalPaidPercentage = Double(doubleStr) ?? 0.0//doubleStr.floatValue
            }
            
            cell.progressViewCustom.height = 30
            cell.outerviewOfProgressView.layer.borderColor = UIColor.black.cgColor
            cell.outerviewOfProgressView.layer.borderWidth = 1
            
            let transform: CGAffineTransform = CGAffineTransform(scaleX: 1.0, y: 11.0);
            
            cell.progressViewCustom.transform = transform;
            
            cell.progressViewCustom.setProgress(Float(finalPaidPercentage), animated: false)
            
            return cell
            
            
        case self.paymentReceiptTableView:
            
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentReceipt", for: indexPath) as? PaymentReceiptTableViewCell
                    else {
                        
                        fatalError("The dequeued cell is not an instance of MealTableViewCell.")
                }
                
                let receipt = paymentReceiptList?[indexPath.row]
                
                
                
                let tourDateLabelAttr = [convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor): UIColor.black, convertFromNSAttributedStringKey(NSAttributedString.Key.font): cell.receiptAmount.font] as [String : Any]
                let fisrtLetterAttr = [convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor): cell.receiptButton.titleLabel?.textColor! ?? UIColor.red, convertFromNSAttributedStringKey(NSAttributedString.Key.font): cell.receiptAmount.font] as [String : Any]
                
                
                var rupee = NSMutableAttributedString(string: "", attributes: convertToOptionalNSAttributedStringKeyDictionary(fisrtLetterAttr))
                
                
                
                
                if receipt?.receiptCurrency != nil
                {
                    let filteredArray = formatterArray.filter(){$0.currencyCode == receipt?.receiptCurrency}
                    
                    
                    if filteredArray.count > 0 {
                        
                        rupee = NSMutableAttributedString(string: "\(filteredArray.first?.currencySymbol ?? "")", attributes: convertToOptionalNSAttributedStringKeyDictionary(fisrtLetterAttr))
                    }
                    else
                    {
                        
                        rupee = NSMutableAttributedString(string: "\(receipt?.receiptCurrency ?? "")", attributes: convertToOptionalNSAttributedStringKeyDictionary(fisrtLetterAttr))
                        
                        
                    }
                    
                    
                }
                
                
                let amount = NSMutableAttributedString(string: "\(receipt?.receiptAmount?.numberFormatterDecimal() ?? "")", attributes: convertToOptionalNSAttributedStringKeyDictionary(tourDateLabelAttr))
                let combination = NSMutableAttributedString()
                combination.append(rupee)
                combination.append(amount)
                cell.receiptAmount.attributedText = combination
                
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd-MM-yyyy"
                
                var receiptDate:String = ""
                if receipt?.receiptDate != nil{
                    
                    receiptDate = dateFormatter.string(from: (receipt?.receiptDate! as! NSDate) as Date)
                }
                
                
                cell.receiptDate.text = "\(receiptDate)"
                cell.receiptType.text = "(Paid by \(receipt?.receiptType ?? ""))"
                
                cell.receiptButton.layer.borderColor = (cell.receiptButton.titleLabel?.textColor)?.cgColor
                cell.receiptButton.layer.borderWidth = 1.0
                cell.receiptButton.layer.cornerRadius  =  5.0
                
                //cell.receiptButton.addTarget(self, action: #selector(receiptBtnClick), for: .touchUpInside)
                
                return cell
                
            
        default:
            return UITableViewCell.init()
            
            
        }
        
     
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch tableView {
        case self.paymentTableview:
           return 50
        case self.paymentReceiptTableView:
            return 122
        default:
            return 0
        }
      
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
         tableView.deselectRow(at: indexPath, animated: true)
        
        switch tableView {
        case self.paymentReceiptTableView:
            
            let receipt = paymentReceiptList?[indexPath.row]
            
            
            let filePaths = TourDocumentController.createFolderStructureForBFNumber(bfNumber: (receipt?.bfNumber)!)
            
            let filePath = filePaths.paymentPath + "/" + (receipt?.receiptNumber)! + ".pdf"
            
            
            self.downloadDocument(filePath: filePath, url: (receipt?.receiptURL)!,alertMessage: AlertMessage.Payment.NO_RECIPT_DETAILS_FOUND)
            break;
            
        default:
            break
        }
        
        
        
        
    }
    
  /*  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if paymentReceiptList?.count != nil
        {
            return (paymentReceiptList?.count)!
        }
        
        return 0
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 112
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentReceipt", for: indexPath) as? PaymentReceiptTableViewCell
            else {
                
                fatalError("The dequeued cell is not an instance of MealTableViewCell.")
        }
        
        let receipt = paymentReceiptList?[indexPath.row]
        
        
        
        let tourDateLabelAttr = [NSForegroundColorAttributeName: UIColor.black, NSFontAttributeName: cell.receiptAmount.font] as [String : Any]
        let fisrtLetterAttr = [NSForegroundColorAttributeName: cell.receiptButton.titleLabel?.textColor! ?? UIColor.red, NSFontAttributeName: cell.receiptAmount.font] as [String : Any]
        
        
        var rupee = NSMutableAttributedString(string: "", attributes: fisrtLetterAttr)
        
        
        
        
        if receipt?.receiptCurrency != nil
        {
            let filteredArray = formatterArray.filter(){$0.currencyCode == receipt?.receiptCurrency}
            
            
            if filteredArray.count > 0 {
                
                rupee = NSMutableAttributedString(string: "\(filteredArray.first?.currencySymbol ?? "")", attributes: fisrtLetterAttr)
            }
            else
            {
                
                rupee = NSMutableAttributedString(string: "\(receipt?.receiptCurrency ?? "")", attributes: fisrtLetterAttr)
                
                
            }
            
            
        }
        
        
        let amount = NSMutableAttributedString(string: "\(receipt?.receiptAmount?.numberFormatterDecimal() ?? "")", attributes: tourDateLabelAttr)
        let combination = NSMutableAttributedString()
        combination.append(rupee)
        combination.append(amount)
        cell.receiptAmount.attributedText = combination
        
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        
        var receiptDate:String = ""
        if receipt?.receiptDate != nil{
        
         receiptDate = dateFormatter.string(from: (receipt?.receiptDate! as! NSDate) as Date)
        }
        
        
        cell.receiptDate.text = "\(receiptDate)"
        cell.receiptType.text = "(Paid by \(receipt?.receiptType ?? ""))"
        
        cell.receiptButton.layer.borderColor = (cell.receiptButton.titleLabel?.textColor)?.cgColor
        cell.receiptButton.layer.borderWidth = 1.0
        cell.receiptButton.layer.cornerRadius  =  5.0
        
        //cell.receiptButton.addTarget(self, action: #selector(receiptBtnClick), for: .touchUpInside)
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        let receipt = paymentReceiptList?[indexPath.row]
        
        
        let filePaths = TourDocumentController.createFolderStructureForBFNumber(bfNumber: (receipt?.bfNumber)!)
        
        let filePath = filePaths.paymentPath + "/" + (receipt?.receiptNumber)! + ".pdf"
        
        
        self.downloadDocument(filePath: filePath, url: (receipt?.receiptURL)!)
        
        
    }*/
    
    
    //MARK: UICollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return (paymentList?.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PaymentPending", for: indexPath as IndexPath) as! PaymentPendingCollectionViewCell
        
        
        let pendingPayment:Payment = paymentList![indexPath.row]
        
        //        let numberFormatter = NumberFormatter()
        //        numberFormatter.groupingSeparator = "^"
        //        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        //
        //        let formattedNumber = numberFormatter.string(from: NSNumber(value:Int64(Double((pendingPayment.balancePending)!)!) ))
        
        
        var isPaymentDone = false
        
        if pendingPayment.currency != nil
        {
            isPaymentDone = UserBookingController.isPaymentDone(by: pendingPayment.currency!, tourDetails: self.tourDetail!)
            
            let filteredArray = formatterArray.filter(){$0.currencyCode == pendingPayment.currency}
            
            
            if filteredArray.count > 0 {
                
                cell.pendingAmountLabel.text = "\(filteredArray.first?.currencySymbol ?? "")" + " \(pendingPayment.balancePending?.numberFormatterDecimal() ?? "")"
                
            }
            else
            {
                cell.pendingAmountLabel.text = "\(pendingPayment.currency ?? "")" + " \(pendingPayment.balancePending?.numberFormatterDecimal() ?? "")"
            }
            
            cell.roeButton.isHidden = false
            
            switch pendingPayment.currency!.uppercased() {
                
                
            case PaymentCurrency.Australlian.0:
                
                cell.containnerBackgroundView.backgroundColor = AppUtility.hexStringToUIColor(hex: PaymentCurrency.Australlian.1)
                
            case PaymentCurrency.Canadian.0:
                
                cell.containnerBackgroundView.backgroundColor = AppUtility.hexStringToUIColor(hex: PaymentCurrency.Canadian.1)
                
                
            case PaymentCurrency.Swiss.0:
                
                cell.containnerBackgroundView.backgroundColor = AppUtility.hexStringToUIColor(hex: PaymentCurrency.Swiss.1)
                
            case PaymentCurrency.Euro.0:
                
                cell.containnerBackgroundView.backgroundColor = AppUtility.hexStringToUIColor(hex: PaymentCurrency.Australlian.1)
                
            case PaymentCurrency.Euro.0:
                
                cell.containnerBackgroundView.backgroundColor = AppUtility.hexStringToUIColor(hex: PaymentCurrency.Sterling.1)
                
            case PaymentCurrency.Indian.0:
                cell.roeButton.isHidden = true
                cell.containnerBackgroundView.backgroundColor = AppUtility.hexStringToUIColor(hex: PaymentCurrency.Indian.1)
                
            case PaymentCurrency.Japanese.0:
                
                cell.containnerBackgroundView.backgroundColor = AppUtility.hexStringToUIColor(hex: PaymentCurrency.Japanese.1)
                
            case PaymentCurrency.Singapore.0:
                
                cell.containnerBackgroundView.backgroundColor = AppUtility.hexStringToUIColor(hex: PaymentCurrency.Singapore.1)
                
            case PaymentCurrency.Thai.0:
                
                cell.containnerBackgroundView.backgroundColor = AppUtility.hexStringToUIColor(hex: PaymentCurrency.Thai.1)
                
            case PaymentCurrency.US.0:
                
                cell.containnerBackgroundView.backgroundColor = AppUtility.hexStringToUIColor(hex: PaymentCurrency.US.1)
                
            case PaymentCurrency.SouthAfrican.0:
                
                cell.containnerBackgroundView.backgroundColor = AppUtility.hexStringToUIColor(hex: PaymentCurrency.SouthAfrican.1)
                
            case PaymentCurrency.NewZeaLand.0:
                
                cell.containnerBackgroundView.backgroundColor = AppUtility.hexStringToUIColor(hex: PaymentCurrency.NewZeaLand.1)
                
                
            default:
                
                cell.containnerBackgroundView.backgroundColor = AppUtility.hexStringToUIColor(hex: PaymentCurrency.Australlian.1)
                
                
            }
        }
        
        
        if  self.tourStatus == TourStatus.Past || isPaymentDone  ||  self.tourStatus == TourStatus.Cancelled     {
            
            cell.payButton.isHidden = true
            
            cell.payButton.isUserInteractionEnabled = false
            
            if cell.roeButton.isHidden
            {
                cell.buttonStackView.isHidden = true
            }
            else
            {
                cell.buttonStackView.isHidden = false
            }
            
        }else
        {
            cell.payButton.isHidden = false
            cell.payButton.isUserInteractionEnabled = true
            cell.buttonStackView.isHidden = false
        }
        
        cell.roeButton.layer.borderWidth = 1.0
        
        cell.roeButton.layer.borderColor = AppUtility.hexStringToUIColor(hex: "333333").cgColor
        
        cell.payButton.addTarget(self, action: #selector(self.onPayNowButtonClick(button:)), for:UIControl.Event.touchUpInside)
        cell.roeButton.addTarget(self, action: #selector(self.onROEButtonClick(button:)), for:UIControl.Event.touchUpInside)
        
        
        return cell
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // When user selects the cell
        
    }
    
    
    
    //    MARK: Pie chart data item
    
    
    
    func randomColor() -> UIColor {
        let randomR: CGFloat = CGFloat(Float(arc4random()) / Float(UINT32_MAX))
        let randomG: CGFloat = CGFloat(Float(arc4random()) / Float(UINT32_MAX))
        let randomB: CGFloat = CGFloat(Float(arc4random()) / Float(UINT32_MAX))
        return UIColor(red: randomR, green: randomG, blue: randomB, alpha: 1)
    }
    
    func randomInteger(_ lower: Int, upper: Int) -> Int {
        return Int(arc4random_uniform(UInt32(upper - lower + 1))) + lower
    }
    
    func randomItem() -> PieChartItem {
        let value = CGFloat(randomInteger(1, upper: 10))
        let color = randomColor()
        let description = "\(value)"
        return PieChartItem(value: value, color: color, description: description)
    }
    
    /**
     *  MARK: ARPieChartDelegate
     */
    func pieChart(_ pieChart: ARPieChart, itemSelectedAtIndex index: Int) {
        let _: PieChartItem = dataItems[index] as! PieChartItem
        
        //  self.currentAmountpercentageLabel.text = "\(Int32(itemSelected.value) )%"
        
    }
    
    func pieChart(_ pieChart: ARPieChart, itemDeselectedAtIndex index: Int) {
        
        let _: PieChartItem = dataItems[index] as! PieChartItem
        
        // self.currentAmountpercentageLabel.text = "\(Int32(itemSelected.value) )%"
        
    }
    
    
    /**
     *   MARK: ARPieChartDataSource
     */
    func numberOfSlicesInPieChart(_ pieChart: ARPieChart) -> Int {
        return dataItems.count
    }
    
    func pieChart(_ pieChart: ARPieChart, valueForSliceAtIndex index: Int) -> CGFloat {
        let item: PieChartItem = dataItems[index] as! PieChartItem
        return item.value
    }
    
    func pieChart(_ pieChart: ARPieChart, colorForSliceAtIndex index: Int) -> UIColor {
        let item: PieChartItem = dataItems[index] as! PieChartItem
        return item.color
    }
    
    func pieChart(_ pieChart: ARPieChart, descriptionForSliceAtIndex index: Int) -> String {
        
        return ""
    }
    
    
    //MARK:Amount payUMoney Button Click
    
    @objc func onPayNowButtonClick(button:UIButton)  {
        paymentButton = button
        
        
        let buttonPosition = paymentButton?.convert(CGPoint(), to: paymentCollectionView)
        let indexPath = self.paymentCollectionView.indexPathForItem(at: buttonPosition!)
        
        let pendingPayment:Payment = paymentList![indexPath!.row]
        
        //let curencyRateDetails = self.getTodaysRate(by: pendingPayment.currency!)
        
        //let filteredArray = formatterArray.filter(){$0.currencyCode == pendingPayment.currency}
        
        print("currency array:\(pendingPayment)")
        
        self.fetchROEData(Type: "PAYNOW",pendingPayment:pendingPayment)
    }

    func onPayNowButton(){
        self.resetCurentlPaymentCellIndexPath()
        
        let buttonPosition = paymentButton?.convert(CGPoint(), to: paymentCollectionView)
        let indexPath = self.paymentCollectionView.indexPathForItem(at: buttonPosition!)
        
        let pendingPayment:Payment = paymentList![indexPath!.row]
        
        self.curentlPaymentCellIndexPath = indexPath!
        
        if pendingPayment.currency?.lowercased() == "INR".lowercased() {
            
            if AppUtility.checkNetConnection()
            {
                showEditPaymentAlertController(payment:pendingPayment)
            }
            
        }
        else
        {
            if self.checkIsRoeDataPresent()
            {
                if tourDetail?.kyc?.lowercased() == "YES".lowercased() {
                    
                    showEditPaymentAlertController(payment:pendingPayment)
                }
                else
                {
                    showOtherCurrenyKYCCheck(payment:pendingPayment)
                }
            }else{
                
                self.showAlertROEDataNotFoundTitle(title: AlertMessage.TITLE_Alert, message: AlertMessage.Payment.ROE_DATA_NOT_FOUND,pendingPayment:pendingPayment)
            }
        }
    }
    @objc func onROEButtonClick(button:UIButton)  {
       
        roeButton = button
        
        let buttonPosition = roeButton?.convert(CGPoint(), to: paymentCollectionView)
        let indexPath = self.paymentCollectionView.indexPathForItem(at: buttonPosition!)
        
        let pendingPayment:Payment = paymentList![indexPath!.row]
        
        //let curencyRateDetails = self.getTodaysRate(by: pendingPayment.currency!)
        
        //let filteredArray = formatterArray.filter(){$0.currencyCode == pendingPayment.currency}
        
        print("currency array:\(pendingPayment)")
      
        self.fetchROEData(Type: "ROE",pendingPayment:pendingPayment)
    }
    
    func onROEButton(pendingPayment:Payment){
        if !self.checkIsRoeDataPresent()
        {
            
            self.showAlertROEDataNotFoundTitle(title: AlertMessage.TITLE_Alert, message:"\(AlertMessage.Payment.ROE_DATA_NOT_FOUND)",pendingPayment: pendingPayment)
            
        }else{
            
            let buttonPosition = roeButton?.convert(CGPoint(), to: paymentCollectionView)
            let indexPath = self.paymentCollectionView.indexPathForItem(at: buttonPosition!)
            
            let pendingPayment:Payment = paymentList![indexPath!.row]
            
            let curencyRateDetails = self.getTodaysRate(by: pendingPayment.currency!)
            
            let filteredArray = formatterArray.filter(){$0.currencyCode == pendingPayment.currency}
            
            var currenySymbol = ""
            
            if filteredArray.count > 0 {
                
                currenySymbol = "\(filteredArray.first?.currencySymbol ?? "")"
                
            }
            
            
            self.showAlertViewWithTitle(title: "", message: "Rate of exchange  is ₹\(curencyRateDetails.rate) as per date of \(curencyRateDetails.date)")
        }
    }
    
    
    @objc func checkBoxAction(_ sender: UIButton)
    {
        if sender.isSelected
        {
            sender.isSelected = false
            
            let btnImage    =  #imageLiteral(resourceName: "checkbox_empty")
            sender.setImage(btnImage, for: UIControl.State())
            
            let filteredAlert =  alertController?.actions.filter({ (UIAlertAction) -> Bool in
                
                UIAlertAction.title?.lowercased() == "ok"
                
            })
            
            
            filteredAlert?.first?.isEnabled = false
            
        }else {
            sender.isSelected = true
            let btnImage    =  #imageLiteral(resourceName: "checkbox_tick")
            sender.setImage(btnImage, for: UIControl.State())
            
            let filteredAlert =  alertController?.actions.filter({ (UIAlertAction) -> Bool in
                
                UIAlertAction.title?.lowercased() == "ok"
                
            })
            
            
            filteredAlert?.first?.isEnabled = true
        }
    }
    
    
    
    func showEditPaymentAlertController(payment:Payment)  {
        
        editPaymentAmountalertController = UIAlertController(title: "Add Money", message: "", preferredStyle: UIAlertController.Style.alert)
        
        let saveAction = UIAlertAction(title: "Pay", style: UIAlertAction.Style.default, handler: {
            alert -> Void in
            
            let inrTextField:UITextField?
            let firstTextField1:UITextField?
            
            if payment.currency?.uppercased() == "INR"
            {
                firstTextField1 = self.editPaymentAmountalertController?.textFields![0]
                inrTextField = self.editPaymentAmountalertController?.textFields![0]
                
            }else
            {
                firstTextField1 = self.editPaymentAmountalertController?.textFields![0]
                inrTextField = self.editPaymentAmountalertController?.textFields![1]
                
            }
            
            
            if !((firstTextField1?.text?.isEmpty)!)
            {
                
                let amount:Double = Double(firstTextField1!.text!)!
                
                let balancingAmount:Double = Double(payment.balancePending?.roundedNumberFormatterDouble() ?? 0.0)
                
                
                if amount <= 0.0 && amount <= 0  {
                    
                    self.showAlertViewWithPaymentValidationTitle(title: "", message: AlertMessage.AMOUNT_LESS_VALIDATION,payment: payment)
                    
                    return
                    
                }else if (amount > balancingAmount)
                {
                    self.showAlertViewWithPaymentValidationTitle(title: "", message: AlertMessage.AMOUNT_LESS_VALIDATION + "\(payment.balancePending?.numberFormatterDecimal() ?? "0")" ,payment:payment)
                    
                    return
                }
                
                if self.reciptGenerationFXDetails.isFX
                {
                    self.reciptGenerationFXDetails.fxAmount = String(amount)
                }
                
                self.startPayment(amount: (inrTextField?.text!)!)
                
                
            }
            else
            {
                self.showAlertViewWithPaymentValidationTitle(title: "", message: "Please enter amount",payment: payment)
                
                return
            }
            
        })
        
        // alertController.textFields?.first?.text = filteredPayment?.first?.balancePending
        // alertController.textFields?.first?.keyboardType = UIKeyboardType.numberPad
        
        let cancelAction = UIAlertAction(title: AlertMessage.TITLE_CANCEL, style: UIAlertAction.Style.default, handler: {
            (action : UIAlertAction!) -> Void in
            
            self.resetCurentlPaymentCellIndexPath()
        })
        
        
        self.editPaymentAmountalertController?.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "0"
            
            
            
            let balanceString:String = "\( payment.balancePending ?? "0.0")"
            
            
            let payAmount:String = String(describing:Int64(balanceString.roundedNumberFormatterDouble()))
           
            
            
            textField.text = payAmount
            
            //filteredPayment?.first?.balancePending
            textField.keyboardType = UIKeyboardType.decimalPad
            textField.tag = 1001
            // textField.borderStyle = UITextBorderStyle.roundedRect
            textField.delegate = self
            
        }
        
        
        var currenySymbol = ""
        var convertedCurrency:String = "0"
        
        
        let balanceString:String = "\( payment.balancePending ?? "0.0")"
        
        
        
        let blance:Double = balanceString.roundedNumberFormatterDouble()
        
        if let paymentCurrency = payment.currency
        {
            
            let filteredArray = formatterArray.filter(){$0.currencyCode == payment.currency}
            
            
            if filteredArray.count > 0 {
                
                currenySymbol = "\(filteredArray.first?.currencySymbol ?? "")"
            }
            
            if paymentCurrency.uppercased() != "INR"
            {
                let curencyRateDetails = self.getTodaysRate(by: payment.currency!)
                
                let rate:Double = Double(curencyRateDetails.rate)!
                
                convertedCurrency =  String(Int64((rate * blance).rounded()))
                
                
                
                self.editPaymentAmountalertController?.addTextField { (textField : UITextField!) -> Void in
                    textField.placeholder = "0"
                    textField.text = convertedCurrency//filteredPayment?.first?.balancePending
                    //textField.keyboardType = UIKeyboardType.decimalPad
                    textField.tag = 1002
                    textField.isUserInteractionEnabled = false
                    //textField.borderStyle = UITextBorderStyle.none
                    
                    
                }
                
                self.reciptGenerationFXDetails = (isFX:true,fxCurrencyType:payment.currency!,roeRate:String(rate),fxAmount:"",isA2Attached:false)
                
            }
            
        }
        
        
        
        
        
        // alertController.textFields?.first?.text = filteredPayment?.first?.balancePending
        // alertController.textFields?.first?.keyboardType = UIKeyboardType.numberPad
        
        
        
        
        
        
        for textField in (self.editPaymentAmountalertController?.textFields!)!
        {
            
            let view:UIView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
            // label.text = "test1"
            let label:UILabel = UILabel(frame: view.frame)
            
            if textField.tag == 1001
            {
                label.text = currenySymbol
                
            }else if textField.tag == 1002
            {
                label.text = "₹"
            }
            textField.leftViewMode = .always
            //label.backgroundColor = UIColor.red
            textField.leftView = label
            
            /* if let container = textField.superview?.superview?.subviews[0]
             {
             if let containerClass:UIVisualEffectView = container as? UIVisualEffectView
             {
             containerClass.backgroundColor = UIColor.red
             
             container.removeFromSuperview()
             }
             
             } */
        }
        
        
        
        self.editPaymentAmountalertController?.addAction(saveAction)
        self.editPaymentAmountalertController?.addAction(cancelAction)
        
        self.present((self.editPaymentAmountalertController)!, animated: true, completion: nil)
        
    }
    func showOtherCurrenyKYCCheck(payment:Payment)  {
        
        
        //simple alert dialog
        alertController = UIAlertController(title: "", message: "   I hereby confirm that I shall be submitting  my KYC document to Thomas Cook", preferredStyle: UIAlertController.Style.alert);
        // Add Action
        
        let cancelAction = UIAlertAction(title: AlertMessage.TITLE_CANCEL, style: UIAlertAction.Style.default, handler: {
            (action : UIAlertAction!) -> Void in
            
        })
        
        let continueAction = UIAlertAction(title: AlertMessage.TITLE_OK, style:UIAlertAction.Style.default) {
            UIAlertAction in
            
            self.showEditPaymentAlertController(payment:payment)
            
        }
        alertController?.view.backgroundColor = UIColor.white
        alertController?.view.layer.cornerRadius = 10.0
        
        continueAction.isEnabled = false
        
        alertController?.addAction(continueAction);
        alertController?.addAction(cancelAction);
        //show it
        let btnImage    = #imageLiteral(resourceName: "checkbox_empty")
        let imageButton : UIButton = UIButton(frame: CGRect(x: 15, y:10, width: 30, height: 30))
        imageButton.setImage(btnImage, for: UIControl.State())
        imageButton.contentMode = UIView.ContentMode.center
        imageButton.addTarget(self, action: #selector(self.checkBoxAction(_:)), for: .touchUpInside)
        
        alertController?.view.addSubview(imageButton)
        
        self.present(alertController!, animated: false, completion: { () -> Void in
            
            
        })
        
        
    }
    //MARK: UINavigationController Delegate
    
    func backButtonClick() -> Void {
        
        
        self.navigationController?.popViewController(animated: true)
        
        self.navigationController?.setNavigationBarTranslucent()
        
    }
    
    
    func showAlertForFewSecondsWithTitle(title : String,message:String) -> Void {
        let alertController : UIAlertController = UIAlertController(title: "", message: message, preferredStyle: UIAlertController.Style.alert)
        
        self.present(alertController, animated: true, completion: nil)
        
        
        let when = DispatchTime.now() + 0.9
        DispatchQueue.main.asyncAfter(deadline: when){
            // your code with delay
            alertController.dismiss(animated: true, completion: nil)
        }
        
    }
    
    func showAlertViewWithTitle(title : String,message:String) -> Void {
        let alertController : UIAlertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: AlertMessage.TITLE_OK, style: UIAlertAction.Style.default) {
            UIAlertAction in
            NSLog("OK Pressed")
        }
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func showAlertViewWithPaymentValidationTitle(title : String,message:String,payment:Payment) -> Void {
        let alertController : UIAlertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: AlertMessage.TITLE_OK, style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.showEditPaymentAlertController(payment: payment)
        }
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    func showAlertROEDataNotFoundTitle(title : String,message:String,pendingPayment:Payment) -> Void {
        let alertController : UIAlertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: AlertMessage.TITLE_CANCEL, style: UIAlertAction.Style.default) {
            UIAlertAction in
            printLog("write sync logic")
        }
        let cancleAction = UIAlertAction(title: AlertMessage.TITLE_OK, style: UIAlertAction.Style.default) {
            UIAlertAction in
            printLog("user click try again")
            
            if (NetworkReachabilityManager.init()?.isReachable)! {
                self.fetchROEData(Type: "ROE",pendingPayment:pendingPayment)
            }else
            {
                self.showAlertROEDataNotFoundTitle(title: AlertMessage.TITLE_Alert, message: AlertMessage.Payment.ROE_DATA_NOT_FOUND,pendingPayment: pendingPayment)
            }
            
            
            
            
        }
        alertController.addAction(okAction)
        alertController.addAction(cancleAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    // MARK: - Switch Change Event
    
    @IBAction func changeCurrencySwitchValueChangeEvent(_ sender: Any) {
        
        if changeCurrencySwitch.isOn {
            
            if (paymentList?.count)! > 0
            {
                
                let paymentListOther = paymentList?.filter({ (Payment) -> Bool in
                    
                    Payment.currency?.uppercased() != "INR"
                })
                
                if paymentListOther != nil && (paymentListOther?.count)! > 0 {
                    
                    self.updateGraph(paymentObject: (paymentListOther?.first)!)
                    
                    self.paymentPieChart.reloadData()
                }
                
            }
            
        }
        else
        {
            
            
            if (paymentList?.count)! > 0
            {
                
                let paymentListINR = paymentList?.filter({ (Payment) -> Bool in
                    
                    Payment.currency?.uppercased() == "INR"
                })
                
                if paymentListINR != nil && (paymentListINR?.count)! > 0 {
                    
                    self.updateGraph(paymentObject: (paymentListINR?.first)!)
                    
                    self.paymentPieChart.reloadData()
                    
                }
                
                
            }
            
            
            
        }
        
    }
    
    
    //MARK: - Payment

    func startPayment(amount:String){
        
        let bfnNumber: String = self.tourDetail?.bfNumber ?? ""
        
         let productDescription: String = self.tourDetail?.packageName ?? ""
        
         let dateOfTravel: String  = AppUtility.convertDateToString(currentDateFormat: "yyyy-MM-dd", date:self.tourDetail?.departureDate! as! Date , requiredDateFormat: "dd-MM-yyyy")
        
         let noOfTraveller: String =  "\(self.getPassengerCount())" //self.tourDetail?. ?? ""
        
        
        
         let noOfNights: String = self.tourDetail?.noOfNights ?? ""
         let userId: String =  "KadaliV"
         let ip: String = "0:0:0:0:0:0:1"
        
        let paymentDataDictionary:Dictionary<String,Any> = ["ltFileNo":bfnNumber,"productDescription":productDescription,"dateOfTravel":dateOfTravel,"noOfTraveller":noOfTraveller,"noOfNights":noOfNights,"amount":amount,"userId":userId,"ip":ip]
        
        LoadingIndicatorView.show()
        
     UserBookingController.sendPayNowRequest(requestData: paymentDataDictionary) { (resultStatus, resultParameters) in
        
        printLog(resultParameters)
        if resultStatus
        {
        
        let viewController:FeedbackWebViewController = FeedbackWebViewController(nibName: "FeedbackWebViewController", bundle: nil)
        
        viewController.redirectionURL = resultParameters
        viewController.moduleTitle = Title.PAYMENT
        
        self.navigationController?.pushViewController(viewController, animated: true)
            
        }else
        {
            DispatchQueue.main.async {
                
                LoadingIndicatorView.hide()
                
                AppUtility.displayAlert(message: resultParameters)
            }
        }        
    }
        
    }
    
    // MARK: - Payment Generation
    
    func generatePaymentReceipt(response : Dictionary<String,Any>)  {
        
        LoadingIndicatorView.show("Loading")
        
        let paymentId:String = String(response[Constant.PAYU_RESPONSE_KEY_PAYMENTID] as? Int ?? 0)
        
        let amount = response[Constant.PAYU_RESPONSE_KEY_AMOUNT] as? String ?? ""
        
        
        UserBookingController.sendPaymentReceiptGenerationRequest(bfNumber:(tourDetail?.bfNumber)! , paymentId:paymentId , amount:amount , bookingFileName: (tourDetail?.bookingFileName)!,reciptGenerationFXDetails: self.reciptGenerationFXDetails, completion:{ (operationValidationMessage) in
            
            
            DispatchQueue.main.sync {
                
                LoadingIndicatorView.hide()
                
                self.refreshTourDetail(bfnNumber: (self.tourDetail?.bfNumber)!)
                
                let paymentdetail:[PaymentDetails] = self.generatePaymentConfiramtionDetails(response: response, reciptNumber: operationValidationMessage.data, tour: self.tourDetail)
                
             
                
          
            
            if operationValidationMessage.status
            {
                
               
                    
                    //self.showAlertViewWithTitle(title: "", message: AlertMessage.PAYMENT_RECIPT_GENERATED)
                    
                      self.displayPaymentConfiramtion(paymentDetails: paymentdetail, reciptStatus: true)
                    
               
                
            }
            else
            {
                //self.showAlertViewWithTitle(title: "", message: AlertMessage.PAYMENT_RECIPT_GENERATED_FAIL)
                
                self.displayPaymentConfiramtion(paymentDetails: paymentdetail, reciptStatus: false)
            }
                
            }
            
        });
    }
    
    func displayPaymentConfiramtion(paymentDetails:[PaymentDetails],reciptStatus:Bool)
    {
        let paymentConfirmationVC:PaymentConfirmationViewController = PaymentConfirmationViewController(nibName: "PaymentConfirmationViewController", bundle: nil)
        
        paymentConfirmationVC.paymentonfirmationListArray = paymentDetails
        
       
        
        self.present(paymentConfirmationVC, animated: true, completion: {
            
              _ = self.navigationController?.popToViewController(self, animated: true)
        })   //pushViewController(paymentConfirmationVC, animated: true)
        
        ///self.present(paymentConfirmationVC, animated: true) {
            
            //
            
       // }
        
        
    }
    
    func generatePaymentConfiramtionDetails(response:Dictionary<String,Any>,reciptNumber:String,tour:Tour?) -> [PaymentDetails]
    {
       // var paymentDetailsArray:[PaymentDetails] = []
        
        let amount = "\u{20B9}" + (response[Constant.PAYU_RESPONSE_KEY_AMOUNT] as? String ?? "0")
        let transactionId = response[Constant.PAYU_RESPONSE_KEY_TRANSACTION_ID] as? String ?? ""
        let transactionDate = response[Constant.PAYU_RESPONSE_KEY_ADDED_ON] as? String ?? ""
        let productinfo = response[Constant.PAYU_RESPONSE_KEY_PROD_ON] as? String ?? ""
        
        var name = ""
        var number = ""
        var email = ""
        
        
        
        let passengerArray:Array<Passenger> = self.sortPassengerByPassengerNumber()
        
        if passengerArray.count > 0
        {
            
            if let passenger:Passenger =  passengerArray.first
            {
                name = "\(passenger.firstName ?? "" ) \(passenger.lastName ?? "" )"
                number = "\(passenger.mobileNumber ?? "" )"
                 email = "\(passenger.emailId ?? "" )"
            }
        }
        
      
        
       // let name = //response[Constant.PAYU_RESPONSE_KEY_NAME] as? String ?? ""
       // let number = //response[Constant.PAYU_RESPONSE_KEY_PHONE_NUM] as? String ?? ""
       // let email = //response[Constant.PAYU_RESPONSE_KEY_EMAIL_ID] as? String ?? ""
        
        let bookingId = tour?.bfNumber ?? ""
        let travelDate =  AppUtility.convertDateToString(date: tour?.departureDate as! Date)
        
        
        let paymentDetailAmount:PaymentDetails = PaymentDetails.init(displayTitle: "Amount Paid : "
            , displaTitleValue: amount)
        
        let paymentDetailReciptNumber: PaymentDetails = PaymentDetails.init(displayTitle: "Recipt Number : ", displaTitleValue: reciptNumber)
        
        let paymentDetailReciptTransactionID: PaymentDetails = PaymentDetails.init(displayTitle: "Transaction ID : ", displaTitleValue: transactionId)
        
        
        let paymentDetailTransactionNumber: PaymentDetails = PaymentDetails.init(displayTitle: "Transaction Date : ", displaTitleValue: transactionDate)
        
        let paymentDetailProductDesc: PaymentDetails = PaymentDetails.init(displayTitle: "Product Description : ", displaTitleValue: productinfo)
        
        let paymentDetailBookingID: PaymentDetails = PaymentDetails.init(displayTitle: "Booking ID : ", displaTitleValue: bookingId)
        
        let paymentDetailTRVLDate: PaymentDetails = PaymentDetails.init(displayTitle: "Travel Date : ", displaTitleValue: travelDate)
        
        let paymentDetailName: PaymentDetails = PaymentDetails.init(displayTitle: "Name : ", displaTitleValue: name)
        
        let paymentDetailEmail: PaymentDetails = PaymentDetails.init(displayTitle: "Email ID : ", displaTitleValue: email)
        
        let paymentDetailMobile: PaymentDetails = PaymentDetails.init(displayTitle: "Mobile Number : ", displaTitleValue: number)
        
        let paymentDetails:[PaymentDetails] = [paymentDetailAmount,paymentDetailReciptNumber,paymentDetailTRVLDate,paymentDetailTransactionNumber,paymentDetailProductDesc,paymentDetailBookingID,paymentDetailTRVLDate,paymentDetailName,paymentDetailEmail,paymentDetailMobile]
        
        return paymentDetails
        
        
    }
    
    //MARK:- Documnet Download
    
    func receiptBtnClick(button:UIButton)
    {
        
        
    }
    
    
    func downloadDocument(filePath:String ,url:String, alertMessage:String) {
        
        //  let passengerDocumnet:TourPassengerDouments =  documentArray.first!
        
        LoadingIndicatorView.show("Loading")
        
        NetworkCommunication.downloadDocuments(urlString: url, destinationFilePath: filePath, completion: { (downlodedfilePath, status) in
            
            printLog(status,filePath);
            
            if(status)
            {
                self.viewDownloadedFile(fileurl: URL(fileURLWithPath:filePath))
                
            }
            else{
                
                let isFilePresent = TourDocumentController.isFileExistInDocumentDirectory(fileNamePath: filePath)
                
                if(isFilePresent.chkFile)
                {
                    self.viewDownloadedFile(fileurl: URL(fileURLWithPath:filePath))
                    
                }else{
                    
                  //  DispatchQueue.main.sync {
                        
                        LoadingIndicatorView.hide()
                        
                        AppUtility.displayAlert(message:alertMessage)
                   // }
                    
                }
                
            }
            
            
            
            
        })
    }
    
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController
    {
        return self
    }
    
    func viewDownloadedFile(fileurl : URL) -> Void
    {
        
        let concurrentQueue = DispatchQueue(label: "queuename", attributes: .concurrent)
        concurrentQueue.sync {
            
            //DispatchQueue.main.sync {
            
            let documentController : UIDocumentInteractionController = UIDocumentInteractionController.init(url: fileurl)
            
            //self.documentController.uti =
            
            documentController.name  = Title.VIEW_PDF
            
            documentController.delegate = self
            
            documentController.presentPreview(animated: true)
            
            LoadingIndicatorView.hide()
            
            // }
            
        }
        
    }
    
    func displayAlert()
    {
        let alert = UIAlertController(title: "Alert", message: "No Receipt details found", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: AlertMessage.TITLE_OK, style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    //MARK : - Text field Delegates
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        // textField.resignFirstResponder()
        
        
        
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        
        
        guard let text = textField.text else { return true }
        
        
        
        
        if textField.tag == 1001
        {
            if string.contains(".")
            {
                return false
            }
            
            var mutableText:String = "0"
            
            
            
            if (textField.text?.isEmpty)! && string.isEmpty
            {
                return true;
                
            }else if (textField.text?.isEmpty)! && !string.isEmpty
            {
                mutableText = string
                
            }else if !(textField.text?.isEmpty)! && string.isEmpty
            {
                //mutableText = textField.text!
                
                let nsString = textField.text as NSString?
                let newString = nsString?.replacingCharacters(in: range, with: string)
                
               // mutableText = mutableText.substring(to: mutableText.index(before: mutableText.endIndex))
                
                mutableText = newString!
                
            }else{
                
                //mutableText = textField.text!
                
                //mutableText = mutableText + string
                
                let nsString = textField.text as NSString?
                let newString = nsString?.replacingCharacters(in: range, with: string)
                
                // mutableText = mutableText.substring(to: mutableText.index(before: mutableText.endIndex))
                
                mutableText = newString!
            }
            
            printLog("mutableString \(mutableText)")
            
            if self.curentlPaymentCellIndexPath.row > -1
            {
                let pendingPayment:Payment = paymentList![curentlPaymentCellIndexPath.row]
                
                let balancingAmount:Float = Float(pendingPayment.balancePending?.roundedNumberFormatterDouble() ?? 0.0)
                
                
                if let paymentCurrency = pendingPayment.currency
                {
                    if paymentCurrency.uppercased() != "INR" && (self.editPaymentAmountalertController?.textFields?.count)! > 1
                    {
                        
                        let INRTextField =  self.editPaymentAmountalertController?.textFields![1]
                        
                        if mutableText.isEmpty
                        {
                            INRTextField?.text = "0"
                            
                            return true
                        }
                        
                        let amount:Float = Float(mutableText)!
                        
                        
                        
                        if (amount > balancingAmount)
                        {
                            return false
                        }
                        
                        var convertedCurrency:String = "0"
                        
                        // if let paymentCurrency = pendingPayment.currency
                        // {
                        //if paymentCurrency.uppercased() != "INR"
                        // {
                        let curencyRateDetails = self.getTodaysRate(by: paymentCurrency)
                        
                        let rate:Float = Float(curencyRateDetails.rate)!
                        
                        
                        convertedCurrency =  String(Int64((rate * amount).rounded()))
                        
                        
                        
                        INRTextField?.text = convertedCurrency
                        
                        // self.editPaymentAmountalertController?.textFields[0].
                        
                        // }
                        
                        // }
                        
                        
                        
                        printLog("String - \(string)")
                        
                        printLog("range - \(range)")
                        
                        printLog("textField.text - \(textField.text)")
                        
                        return true
                    }else{
                        
                        if mutableText.isEmpty
                        {
                            return true
                        }
                        
                        let amount:Float = Float(mutableText)!
                        
                        if (amount > balancingAmount)
                        {
                            return false
                        }
                    }
                }
                
            }
        }
        
        return true
    }
    
    
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        if textField.tag == 1001
        {
            let amountString:String = textField.text!
            
        }
    }
    
    @IBAction func proformaInvoiceClick(_ sender: UIButton) {
        
        //let proformaInvoiceUrl:String = DocumentsURLConstant.PERFORMA_INVOICE_URL
        
       if let proformaInvoiceUrl:String = self.tourDetail?.performInoiceDocURL as? String
       {
        
        
        let documentFolderPath = TourDocumentController.getTourFileDirectoryPath(bfnNumber: (tourDetail?.bfNumber)!)
        
        let dodocumentFilePath:String = documentFolderPath.bfNumberPath + "/proformaInvoice.pdf"
        
        let downloadUrl:String = proformaInvoiceUrl + (tourDetail?.bfNumber)!
        
        self.downloadDocument(filePath: dodocumentFilePath, url: downloadUrl, alertMessage: AlertMessage.NO_DOCUMENT)
        
        
        }else
       {
            AppUtility.displayAlert(message: AlertMessage.NO_DOCUMENT)
        
        }
        
        
    }
    
    func getExchangeRate() -> Array<Currency>  {
        
        do {
            
            let fetchRequest: NSFetchRequest<Currency> = Currency.fetchRequest()
            
            let currency = try CoreDataController.getContext().fetch(fetchRequest)
            return currency
        }catch   {
            printLog(error)
        }
        
        return []
        
    }
    
    func getTodaysRate(by currencyCode:String) -> (rate:String,date:String) {
        
        if let filterCurrency =  self.currencyArray?.filter({ $0.currencyCode == currencyCode })
        {
            if filterCurrency.count > 0
            {
                let currencyRate = filterCurrency.first?.currencyRate
                
                let date = filterCurrency.first?.currencyDate
                
                return (currencyRate!,date!)
            }
            
            
        }
        
        
        return ("","")
    }
    
    
    
    func resetCurentlPaymentCellIndexPath() {
        
        self.curentlPaymentCellIndexPath  = IndexPath(row: -1, section: -1)
        self.editPaymentAmountalertController = nil
        self.reciptGenerationFXDetails = (isFX:false,fxCurrencyType:"",roeRate:"",fxAmount:"",isA2Attached:false)
        
    }
    
    
    func refreshTourDetail(bfnNumber:String) {
        
        LoadingIndicatorView.show("Loading")
        
        UserBookingController.fetchTourDetails(bfNumber: bfnNumber) { (messageTourDetail, tourObject) in
            
            if messageTourDetail.status
            {
                
                
                DispatchQueue.main.sync {
                    
                    LoadingIndicatorView.hide()
                    
                    CoreDataController.getContext().refresh(self.tourDetail!, mergeChanges: true)
                    
                   // self.updatePaymentData()
                    self.updatePaymentDataInPaymentTableView()
                    self.paymentReceiptTableView.reloadData()
                    self.paymentTableview.reloadData()
                    
                    self.paymentCollectionView.reloadData()
                }
                
                
                
            }
            
            
        }
        
        
        
    }
   
    func getPassengerCount() -> Int {
        if let passengerArray:Array<Passenger> = self.tourDetail?.passengerRelation?.allObjects as? Array<Passenger>
        {
            return passengerArray.count
        }
        
        return 0
    }
    
    
    func sortPassengerByPassengerNumber() -> Array<Passenger> {
        
        if let passengerArray:Array<Passenger> = self.tourDetail?.passengerRelation?.allObjects as? Array<Passenger>
        {
            if passengerArray.count > 0
            {
                
                let sortedPassengerArray = passengerArray.sorted(){ $0.passengerNumber! < $1.passengerNumber!}
                
                return sortedPassengerArray
                
            }
        }
        
        return []
        
    }
    
    func checkIsRoeDataPresent() -> Bool {
        
        if self.currencyArray != nil
        {
            if self.currencyArray!.count > 0
            {
                return true;
            }
        }
        return false
    }
}


/**
 *  MARK: Pie chart data item
 */
open class PieChartItem {
    
    /// Data value
    open var value: CGFloat = 0.0
    
    /// Color displayed on chart
    open var color: UIColor = UIColor.black
    
    /// Description text
    open var description: String?
    
    public init(value: CGFloat, color: UIColor, description: String?) {
        self.value = value
        self.color = color
        self.description = description
    }
}

/* payment response
 {
 addedon = "2017-10-25 19:56:41";
 additionalCharges = "";
 "additional_param" = "";
 address1 = "";
 address2 = "";
 amount = "1.0";
 "amount_split" = "{\"PAYU\":\"1.0\"}";
 "bank_ref_num" = 469231;
 bankcode = MAST;
 baseUrl = "<null>";
 calledStatus = 0;
 cardToken = "";
 "card_merchant_param" = "<null>";
 "card_type" = "";
 cardhash = "This field is no longer supported in postback params.";
 cardnum = 512345XXXXXX2346;
 city = "";
 country = "";
 createdOn = 1508941607000;
 discount = "0.00";
 email = "p@g.com";
 encryptedPaymentId = "<null>";
 error = E000;
 "error_Message" = "No Error";
 fetchAPI = "<null>";
 field1 = 469231;
 field2 = 901179;
 field3 = 2052;
 field4 = MC;
 field5 = 538352841602;
 field6 = 00;
 field7 = 0;
 field8 = 3DS;
 field9 = " Verification of Secure Hash Failed: E700 -- Approved -- Transaction Successful -- Unable to be determined--E000";
 firstname = parth;
 furl = "<null>";
 hash = fcd50740bc11309f86bbd2eb744ec5055f57c477091e341b12a673a84f2380546a39dfa5b512b9e01e0457ced7f3efc03e1e92395501d3678fe54b553d4f8eaa;
 id = "<null>";
 key = 40747T;
 lastname = "";
 meCode = "{\"vpc_Merchant\":\"TESTIBIBOWEB\"}";
 mihpayid = 403993715516781500;
 mode = CC;
 "name_on_card" = payu;
 "net_amount_debit" = 1;
 "offer_availed" = "";
 "offer_failure_reason" = "";
 "offer_key" = "";
 "offer_type" = "";
 "paisa_mecode" = "";
 paymentId = 1111431989;
 payuMoneyId = 1111431989;
 "pg_TYPE" = AXISPG;
 "pg_ref_no" = "";
 phone = "";
 postBackParamId = 1009895;
 postUrl = "https://www.payumoney.com/mobileapp/payumoney/success.php";
 productinfo = "Product Info";
 state = "";
 status = success;
 surl = "<null>";
 txnid = 0nf777;
 udf1 = "";
 udf10 = "";
 udf2 = "";
 udf3 = "";
 udf4 = "";
 udf5 = "";
 udf6 = "";
 udf7 = "";
 udf8 = "";
 udf9 = "";
 unmappedstatus = captured;
 version = "";
 zipcode = "";
 }
 */



// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromNSAttributedStringKey(_ input: NSAttributedString.Key) -> String {
	return input.rawValue
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}
