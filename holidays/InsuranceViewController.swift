//
//  InsuranceViewController.swift
//  sotc-consumer-application
//
//  Created by Saurav Kumar on 09/08/17.
//  Copyright © 2017 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit

class InsuranceViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UIDocumentInteractionControllerDelegate {

    @IBOutlet var insuranceTableView: UITableView!
    
    var passangerDetailsArray:Array<Passenger>?
    var insuraceDetailsArray:Array<Insurance>?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.insuranceTableView.register(UINib(nibName:  "InsuranceTableViewCell", bundle: nil), forCellReuseIdentifier: "insurance")
        
         self.insuranceTableView.tableFooterView = UIView(frame: .zero)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.setRedNavigationBar()
        self.navigationController?.setnavigatiobBarTitle(title: Title.INSURANCE, viewController: self)
        self.navigationController?.setEmergencyNavigationRightButton(showEmergencyButton: true, viewController: self)
        self.navigationController?.setNagationBackButton(showRightButton: true, viewController: self)
        
        
        
    }
    

    
    func backButtonClick() -> Void {
        
        
        self.navigationController?.popViewController(animated: true)
        
        self.navigationController?.setNavigationBarTranslucent()
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return insuraceDetailsArray!.count
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 131
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "insurance", for: indexPath) as? InsuranceTableViewCell
            else {
                
                fatalError("The dequeued cell is not an instance of MealTableViewCell.")
        }
        
         if let insurance = insuraceDetailsArray?[indexPath.row] {
            
            cell.uiLabelNumber?.text = insurance.policyNumber!
        
            let passengerName = self.getPassengerPolicyDetails(by:insurance)
           
            cell.uiLabelPassengerName?.text = passengerName
            
        }
       
     
        
        //cell.textLabel?.text = "Vikas Patil"
//        cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
        
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
         if let insurance = insuraceDetailsArray?[indexPath.row] {
            
            var documnetdetails : DocumentDetails = DocumentDetails()
            
            documnetdetails.displayTitle = "Ticket \(index)"
            documnetdetails.downloadUrl = insurance.policyURL
            let documnetPath = TourDocumentController.getTourFileDirectoryPath(bfnNumber: insurance.bfNumber!)
            
            if insurance.passengerNumber! != ""
            {
            
                if insurance.policyNumber! != ""                 {
                
                     let fileDocumnetPath = documnetPath.InsurancePath + "/" + insurance.passengerNumber! + "/" + insurance.policyNumber! + ".pdf"
                    
                      self.downloadDocument(filePath: fileDocumnetPath , url: insurance.policyURL!)
                
                }
                
            }
            
        }
        
    }

    func getPassengerPolicyDetails(by insurance:Insurance) ->String  {
        
        if insurance.passengerNumber != nil
        {
            let filteredPassengerArray = passangerDetailsArray?.filter() { $0.passengerNumber == insurance.passengerNumber }
            
            if(filteredPassengerArray != nil && filteredPassengerArray!.count > 0)
            {
                let passenger:Passenger = filteredPassengerArray![0]
                
                return "\((passenger.firstName ?? "").capitalized) \((passenger.middleName ?? "").capitalized) \((passenger.lastName ?? "").capitalized)"
            }
        }
        
        return "";
    }
    
    func downloadDocument(filePath:String ,url:String) {
        
        //  let passengerDocumnet:TourPassengerDouments =  documentArray.first!
        
        LoadingIndicatorView.show("Loading")
        do {
        
        NetworkCommunication.downloadDocuments(urlString: url, destinationFilePath: filePath, completion: { (downlodedfilePath, status) in
            
            printLog(status,filePath);
            
            if(status)
            {
                self.viewDownloadedFile(fileurl: URL(fileURLWithPath:filePath))
                
            }
            else{
                
                let isFilePresent = TourDocumentController.isFileExistInDocumentDirectory(fileNamePath: filePath)
                
                if(isFilePresent.chkFile)
                {
                    self.viewDownloadedFile(fileurl: URL(fileURLWithPath:filePath))
                    
                }else{
                    
                   // DispatchQueue.main.sync {
                        
                        LoadingIndicatorView.hide()
                        
                        self.displayAlert()
                  //  }
                    
                }
                
            }
            
            
            
            
        })
            
        }catch let error {
            printLog(error.localizedDescription)
        }
}
    
    
    func displayAlert()
    {
        let alert = UIAlertController(title: "", message: AlertMessage.Insurance.INSURANCE_YET_TO_ISSUED /*"No Insurance Policy found"*/, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: AlertMessage.TITLE_OK, style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: UIDocumentInteractionController Delegate
    
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController
    {
        return self
    }
    
    func viewDownloadedFile(fileurl : URL) -> Void
    {
        
        let concurrentQueue = DispatchQueue(label: "queuename", attributes: .concurrent)
        concurrentQueue.sync {
            
            //DispatchQueue.main.sync {
            
            let documentController : UIDocumentInteractionController = UIDocumentInteractionController.init(url: fileurl)
            
            //self.documentController.uti =
            documentController.name  = Title.VIEW_PDF
            
            documentController.delegate = self
            
            documentController.presentPreview(animated: true)
            
            LoadingIndicatorView.hide()
            
            // }
            
        }
        
    }

    
}
