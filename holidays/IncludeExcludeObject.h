//
//  IncludeExcludeObject.h
//  holidays
//
//  Created by Kush_Tech on 04/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface IncludeExcludeObject : NSObject
-(instancetype)initWithIncludeExcludeDict:(NSDictionary *)dictionary;

@property(nonatomic,strong)NSString *excludes;
@property (nonatomic) NSInteger holidayIncludeExcludeId;
@property(nonatomic,strong)NSString *includes;
@property(nonatomic,strong)NSString *inclusionsSrp;
@property (nonatomic) BOOL isActive;
@property (nonatomic) NSInteger packageClassId;
@end

NS_ASSUME_NONNULL_END
