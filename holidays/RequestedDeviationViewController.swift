//
//  RequestedDeviationViewController.swift
//  sotc-consumer-application
//
//  Created by Payal on 12/09/17.
//  Copyright © 2017 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit

class RequestedDeviationViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate {

    @IBOutlet weak var passengerNameLabel: UILabel!
    
    @IBOutlet weak var addDeviaionBtn: UIButton!
    var deviationceDetailsArray:Array<Deviation>?
    
    @IBOutlet weak var createDeviationBtnHeightConstraints: NSLayoutConstraint!
    
    var touDetails:Tour?
    
    var section:[(isOpen: Bool, deviation: Deviation)] = []
    var passengerName : String?
    var canCreateDeviation:Bool = false
    
    var selectedPassenger:Passenger?
   
    
    @IBOutlet var requestedDeviationTableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.requestedDeviationTableView.register(UINib(nibName: "RequestedDeviationViewCell", bundle: nil), forCellReuseIdentifier: "requestedDeviationCell")
        self.requestedDeviationTableView.register(UINib(nibName: "RequestedDeviationHeaderCell", bundle: nil), forHeaderFooterViewReuseIdentifier: "requestedDeviationHeaderCell")
        
        self.requestedDeviationTableView.register(UITableViewHeaderFooterView.self, forHeaderFooterViewReuseIdentifier: "requestedDeviationFooterCell")
        
        self.passengerNameLabel.text = passengerName?.capitalized
        
        self.checkAndHideCreateDeviationBtn()
        
        self.getDisplayDeviationDetails()
       
        
    }

    override func viewWillAppear(_ animated: Bool) {
        
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.setRedNavigationBar()
        self.navigationController?.setnavigatiobBarTitle(title: Title.DEVIATION, viewController: self)
        self.navigationController?.setEmergencyNavigationRightButton(showEmergencyButton: true, viewController: self)
        
        self.automaticallyAdjustsScrollViewInsets =  false
        requestedDeviationTableView.tableFooterView = UIView(frame: CGRect.zero)
        
    }
    
    
    //MARK: UINavigationController Delegate
    
    func leftButtonClick() -> Void    {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //tableview delegate methods
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if section[indexPath.section].isOpen
        {
            return 73;
        }
        
        return 0
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return section.count
    }
  
     func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 95
    }
    

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        
       
        let headerCell = tableView.dequeueReusableHeaderFooterView(withIdentifier: "requestedDeviationHeaderCell") as? RequestedDeviationHeaderCell
        
         let deviation = self.section[section].deviation
        
        String(describing: deviation.startDate!)
     
        let date =  AppUtility.convertDateToString(currentDateFormat: "yyyy-MM-dd", date: deviation.startDate! as Date, requiredDateFormat: "dd-MM-yyyy") 
        
            headerCell?.customInit(title: "Group Date", subtitle:date  , deviationStatus: deviation.status!, section: section)
      
            let heraderView1Tap = UITapGestureRecognizer(target: self, action: #selector(self.uiTapGestureRecognizerClick(_:)))
            heraderView1Tap.delegate = self
            headerCell?.addGestureRecognizer(heraderView1Tap)
            headerCell?.tag = section
        
        if(self.section[section].isOpen)
        {
            
            UIView.animate(withDuration: 8.0,
                           delay: 1.1,
                           options: UIView.AnimationOptions.curveEaseIn,
                           animations: { () -> Void in
                          headerCell?.arrowImage.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(M_PI)) / 180.0)
                            self.loadViewIfNeeded()
            }, completion: { (finished) -> Void in
                // ....
            })
            
          /*  UIView.animate(withDuration: 5.0, animations: {
                headerCell?.arrowImage.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(M_PI)) / 180.0)
            
           
            })*/
            
        }
        
     
        return headerCell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if(self.section[indexPath.section].isOpen)
        {
        
        guard let childCell = tableView.dequeueReusableCell(withIdentifier: "requestedDeviationCell", for: indexPath) as? RequestedDeviationViewCell
            else {
                
                fatalError("The dequeued cell is not an instance of MealTableViewCell.")
        }
        let deviation = self.section[indexPath.section].deviation
                
            
       childCell.newDateLabel.text =  AppUtility.convertDateToString(currentDateFormat: "yyyy-MM-dd", date: deviation.newDate! as Date, requiredDateFormat: "dd-MM-yyyy")
            
        return childCell
            
        }
        
        return UITableViewCell(frame: UIView().frame)
    }
    
  /*    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
     
     return 1
        
     }
     
     func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
          let customView = UIView(frame: CGRect(x: 0, y: 0, width: self.passengerTableView.bounds.width, height: 1))
     
     customView.backgroundColor = UIColor.lightGray
     
         customView.addSubview(button)
     
     
     return UIView()
     }*/
    
    
    
    func getDisplayDeviationDetails()   {
     
        for deviation in deviationceDetailsArray!
        {
            section.append((isOpen: false, deviation: deviation))
        }
        
        
        
    }

    func toggelSection(header: RequestedDeviationHeaderCell, section: Int) {
        self.section[section].isOpen = !self.section[section].isOpen
        
        requestedDeviationTableView.beginUpdates()
        
        requestedDeviationTableView.reloadSections([section], with: .automatic)
        
        requestedDeviationTableView.endUpdates()
    }
    
    @objc func uiTapGestureRecognizerClick(_ sender: UITapGestureRecognizer) {
        
        
        let tappedView = sender.view!
        
        self.section[tappedView.tag].isOpen = !self.section[tappedView.tag].isOpen
        
        
        
        requestedDeviationTableView.beginUpdates()
        
        //requestedDeviationTableView.reloadRows(at: <#T##[IndexPath]#>, with: <#T##UITableViewRowAnimation#>)
        
        requestedDeviationTableView.reloadSections([tappedView.tag], with: .fade)
        
        requestedDeviationTableView.endUpdates()
        
       // self.requestedDeviationTableView.reloadData()
        
    }
    
    @IBAction func addDeviationBtnClick(_ sender: Any) {
        
        
        let viewController : DeviationViewController = DeviationViewController(nibName: "DeviationViewController", bundle: nil)
        
        viewController.tourDetails = self.touDetails
        
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func checkAndHideCreateDeviationBtn()
    {
        if !canCreateDeviation || !self.isDeviationBooked()
        {
            addDeviaionBtn.isHidden = true
            
            addDeviaionBtn.isUserInteractionEnabled = false
            
            self.toggelCreateDeviationBtn(isHide: true)
        }
    }
    
    func isDeviationBooked()->Bool
    {
         //let filteredDeviationArray = deviationceDetailsArray?.filter(){$0.status?.uppercased() == Constant.BOOKED}
        
        //if filteredDeviationArray != nil && (filteredDeviationArray?.count)! > 0
            if deviationceDetailsArray != nil && (deviationceDetailsArray?.count)! > 0
        {
            return false
        }
        
        return true
    }
    
    func toggelCreateDeviationBtn(isHide:Bool)  {
        
        if isHide == true{
            
        self.createDeviationBtnHeightConstraints.constant = 0
            
        }else
        {
            self.createDeviationBtnHeightConstraints.constant = 31
        }
    
        
    }
}
