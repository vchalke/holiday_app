//
//  TravelBlogObject.m
//  holidays
//
//  Created by Kush_Tech on 16/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "TravelBlogObject.h"

@implementation TravelBlogObject
-(instancetype)initWithTravelBlogsArray:(NSDictionary *)object{
    if ([super init])
    {
    
        self.link = [object valueForKey:@"link"];
        self.title = [object valueForKey:@"title"];
        self.titleString = [self.title valueForKey:@"rendered"];
        self.content = [object valueForKey:@"content"];
        NSLog(@"%@",self.content);
        
    }
    return self;
}
@end
