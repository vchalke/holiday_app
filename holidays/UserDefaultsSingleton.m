//
//  UserDefaultsSingleton.m
//  holidays
//
//  Created by Kush_Tech on 20/03/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "UserDefaultsSingleton.h"

@implementation UserDefaultsSingleton
+(id)sharedInstance {
    //+ (Contact *)sharedInstance {
    static UserDefaultsSingleton *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
        
    });
    return sharedInstance;
}

- (id)init {
    if (self = [super init]) {
        
    }
    return self;
}
-(void)saveInUserDefaultsStringValue:(NSString*)value ForKey:(NSString*)key{
    [[NSUserDefaults standardUserDefaults]setObject:value forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
-(NSString*)getUserDefaultStringForKey:(NSString*)key{
    return [[NSUserDefaults standardUserDefaults]valueForKey:key];
}
-(NSArray*)getUserDefaultArrayForKey:(NSString*)key{
    return [[NSUserDefaults standardUserDefaults]valueForKey:key];
}
@end
