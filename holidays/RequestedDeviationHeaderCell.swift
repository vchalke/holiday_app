//
//  RequestedDeviationHeaderCell.swift
//  sotc-consumer-application
//
//  Created by Payal on 13/09/17.
//  Copyright © 2017 Mobicule Technologies Private Limited. All rights reserved.
//

import UIKit




class RequestedDeviationHeaderCell: UITableViewHeaderFooterView {
    
 
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var subtitleLabel: UILabel!
    @IBOutlet var deviationStatusLabel: UILabel!
 
    @IBOutlet weak var arrowImage: UIImageView!
    
//    override init(reuseIdentifier: String?) {
//        super.init(reuseIdentifier: reuseIdentifier)
//    }
//
//    required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//    }
//    
    
    
    func customInit(title:String,subtitle:String,deviationStatus:String,section:Int) {
        
        self.titleLabel.text = title
        self.subtitleLabel.text = subtitle
        
        if deviationStatus.caseInsensitiveCompare(Constant.BOOKED) == ComparisonResult.orderedSame
        {
            self.deviationStatusLabel.textColor = AppUtility.hexStringToUIColor(hex: "#EB3338")
        }
        
        self.deviationStatusLabel.text = deviationStatus
       
       
    }
}
