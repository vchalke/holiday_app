//
//  IntroPageVC.m
//  holiday
//
//  Created by Pushpendra Singh on 04/08/15.
//  Copyright (c) 2015 Mobicule. All rights reserved.
//

#import "IntroPageVC.h"
#import "IntroPageContentVC.h"
#import "LandingPageViewController.h"
#import "SlideMenuViewController.h"

#import "LandingScreenVC.h"
@interface IntroPageVC ()

@end

@implementation IntroPageVC{
    
    NSArray *pageImages;
    BOOL pageControlIsBeingUsed;
}

- (void)viewDidLoad {
    [super viewDidLoad];
//    
//    UIPageControl *pageControl = [UIPageControl appearance];
//    pageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
//    pageControl.currentPageIndicatorTintColor = [UIColor blackColor];
//    pageControl.backgroundColor = [UIColor clearColor];
//    
//    _btnLetsGo.hidden=YES;
//    
//    [_btnLetsGo addTarget:self action:@selector(btnLetsGo:) forControlEvents:UIControlEventTouchUpInside];
//    
    pageImages = @[@"page1.jpg", @"page2.jpg", @"page3.jpg"];
//
//    _pageViewController=[[UIPageViewController alloc]initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
//    
//    self.pageViewController.dataSource = self;
//    
//    IntroPageContentVC *startingViewController = [self viewControllerAtIndex:0];
//    NSArray *viewControllers = @[startingViewController];
//    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
//    
//    // Change the size of page view controller
//    [self addChildViewController:_pageViewController];
//    [self.view addSubview:_pageViewController.view];
//    
//    [self.pageViewController didMoveToParentViewController:self];

     [NSTimer scheduledTimerWithTimeInterval:1.00 target:self selector:@selector(onTimer) userInfo:nil repeats:YES];
    
    _btnLetsGo.layer.borderColor = [UIColor whiteColor].CGColor;
    
}
-(void)viewDidLayoutSubviews
{
//    self.pageViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height+40);
//    
//    
//       NSLog(@"page view controller frame :- %@",NSStringFromCGRect(self.pageViewController.view.frame));
    
    for (int i = 0; i<pageImages.count; i++)
    {
        int x = i * self.view.frame.size.width;
        UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(x, 0, self.view.frame.size.width, self.view.frame.size.height)];
        imageView.image = [UIImage imageNamed:pageImages[i]];
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        [self.scrollViewIntroPage addSubview:imageView];
        
        
    }
    self.pageControl.currentPage = 0;
    self.pageControl.numberOfPages = pageImages.count;

    [self.scrollViewIntroPage setContentSize:CGSizeMake(pageImages.count*self.view.frame.size.width, self.view.frame.size.height)];
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
//    if (!pageControlIsBeingUsed)
//    {
//        // Switch the indicator when more than 50% of the previous/next page is visible
//        CGFloat pageWidth = _scrollViewIntroPage.frame.size.width;
//        int page = floor((_scrollViewIntroPage.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
//        self.pageControl.currentPage = page;
//    }

}






-(void)btnLetsGo:(UIButton*)sender
{
    
    NSLog(@"Let's Go!");
    
    
   
    

}

//- (IntroPageContentVC *)viewControllerAtIndex:(NSUInteger)index
//{
//    if (([pageImages count] == 0)) {
//        return nil;
//    }
//    
//    // Create a new view controller and pass suitable data.
//    IntroPageContentVC *pageContentViewController = [[IntroPageContentVC alloc] initWithNibName:@"IntroPageContentVC" bundle:nil];
//    pageContentViewController.imageName = pageImages[index];
//    //pageContentViewController.titleText = self.pageTitles[index];
//    pageContentViewController.pageIndex = index;
//    
//    return pageContentViewController;
//}
//
//
//
//
//#pragma mark - Page View Controller Data Source
//
//- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
//{
//    NSUInteger index = ((IntroPageContentVC*) viewController).pageIndex;
//     _btnLetsGo.hidden=YES;
//    if ((index == 0) || (index == NSNotFound)) {
//        return nil;
//    }
//    
//    index--;
//    return [self viewControllerAtIndex:index];
//}
//
//
//- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
//{
//    NSUInteger index = ((IntroPageContentVC*) viewController).pageIndex;
//    
//    if (index == NSNotFound) {
//        return nil;
//    }
//
//    index++;
//    if (index == [pageImages count]) {
//        _btnLetsGo.hidden=NO;
//        return nil;
//    }
//    
//    return [self viewControllerAtIndex:index];
//}
//
//- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
//{
//    return [pageImages count];
//}
//
//- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
//{
//    return 0;
//}
//

-(void) onTimer {
   
    if (_scrollViewIntroPage.contentOffset.x < (pageImages.count-1)*self.view.frame.size.width)
    {
        
        [UIView animateWithDuration:1.0 animations:^{
            _scrollViewIntroPage.contentOffset = CGPointMake(_scrollViewIntroPage.contentOffset.x+self.view.frame.size.width, 0);
            

        }];
        pageControlIsBeingUsed = YES;
        
        CGFloat pageWidth = _scrollViewIntroPage.frame.size.width;
        int page = floor((_scrollViewIntroPage.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        self.pageControl.currentPage = page;
        
//        NSInteger pageCount = pageImages.count;
//        self.pageControl.currentPage = 0;
//        self.pageControl.numberOfPages = pageCount;
//        
        
    }
    else
    {
        _btnLetsGo.hidden = NO;
    }
    
    
}




- (IBAction)onLetsGoBtnClicked:(id)sender {
    [[NSUserDefaults standardUserDefaults] setObject:@"Not First Time" forKey:@"FirstTime"];
    // Vijay Commented
//    LandingPageViewController * landingVC = [[LandingPageViewController alloc]initWithNibName:@"BaseViewController" bundle:nil];
    // VJ_Added
    LandingScreenVC * landingVC = [[LandingScreenVC alloc]initWithNibName:@"LandingScreenVC" bundle:nil];
    SlideMenuViewController *slideMenu = [[SlideMenuViewController alloc]initWithNibName:@"BaseViewController" bundle:nil];
    
    SlideNavigationController *slideNavigationVC = [[SlideNavigationController alloc]initWithRootViewController:landingVC];
    
    [SlideNavigationController sharedInstance].leftMenu = slideMenu;
    [SlideNavigationController sharedInstance].portraitSlideOffset = 80;
    [SlideNavigationController sharedInstance].avoidSwitchingToSameClassViewController = YES;
    
    // [self presentViewController:slideNavigationVC animated:YES completion:nil];
    
    AppDelegate *appdel = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    [appdel.window setRootViewController:slideNavigationVC];
}
@end
