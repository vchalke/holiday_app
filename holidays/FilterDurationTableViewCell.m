//
//  FilterDurationTableViewCell.m
//  holidays
//
//  Created by Kush_Team on 24/06/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "FilterDurationTableViewCell.h"
#import "WebUrlConstants.h"
@implementation FilterDurationTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setupTableViewCell{
    filterDurationDay = ([self.filterDurationDict objectForKey:filterDuration]) ? [NSString stringWithFormat:@"%@",[self.filterDurationDict objectForKey:filterDuration]] : @"100";
    [self setButtonTagTitle];
    [self clickViewDetail:([filterDurationDay isEqualToString:@"7"]) ? self.btn_first : ([filterDurationDay isEqualToString:@"8"]) ? self.btn_Second : ([filterDurationDay isEqualToString:@"12"]) ? self.btn_Third : self.btn_Fourth];
}
-(void)setButtonTagTitle{
    [self.btn_first addTarget:self action:@selector(clickViewDetail:) forControlEvents:UIControlEventTouchUpInside];
    [self.btn_Second addTarget:self action:@selector(clickViewDetail:) forControlEvents:UIControlEventTouchUpInside];
    [self.btn_Third addTarget:self action:@selector(clickViewDetail:) forControlEvents:UIControlEventTouchUpInside];
}
-(void)clickViewDetail:(UIButton*)sender{
    [self.btn_first setImage:(sender.tag == 0) ? [UIImage imageNamed:@"rightCheckSelect"] : [UIImage imageNamed:@"rightCheckNoSelect"] forState:UIControlStateNormal];
    [self.btn_Second setImage:(sender.tag == 1) ? [UIImage imageNamed:@"rightCheckSelect"] : [UIImage imageNamed:@"rightCheckNoSelect"] forState:UIControlStateNormal];
    [self.btn_Third setImage:(sender.tag == 2) ? [UIImage imageNamed:@"rightCheckSelect"] : [UIImage imageNamed:@"rightCheckNoSelect"] forState:UIControlStateNormal];
    if (sender.tag != 4){
        [self.filterDurationDict setObject:(sender.tag == 0) ? @"7" : (sender.tag == 1) ? @"8" : (sender.tag == 2) ? @"12" : @"" forKey:filterDuration];
    }
    
}
@end
