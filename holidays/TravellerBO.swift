//
//  TravellerBO.swift
//  holidays
//
//  Created by Komal Katkade on 11/27/17.
//  Copyright © 2017 Mobicule. All rights reserved.
//

import UIKit

class TravellerBO
{
    var travellerTitle : String = String()
    
    var travellerFirstName : String = String()
    
    var travellerLastName : String = String()

    var travellerDateOfTravel : NSDate? = NSDate()

    var travellerDateOfBirth : NSDate? = nil
    
    var travellerAadharNumber : String = String()
    
    var travellerGSTINNumber : String = String()

    
    
    var productArray : [ProductBO] = []
      init() {
       productArray = []
    }
    func addProduct(product:ProductBO) -> Void
    {
        self.productArray.append(product)
    }
    
    func getProduct(atIndex:NSInteger) -> ProductBO
    {
        let product : ProductBO = productArray[atIndex];
        return product;
    }
    
    func replaceProduct(atIndex:NSInteger,newProduct:ProductBO) -> Void
    {
        productArray[atIndex] = newProduct
      
    }
    
    func deleteProduct(atIndex:NSInteger) -> Void
    {
        productArray.remove(at: atIndex)
    }
    
    func getProductCount() -> NSInteger
    {
        return self.productArray.count
    }
    
    func changeProductStatus() -> Void
    {
        for  i in (0..<productArray.count)
        {
            productArray[i].markAsOldProduct()
        }
    }
    
    func changeStatusOfUpdateButtonForAll() -> Void
    {
        for  i in (0..<productArray.count)
        {
            productArray[i].isUpdateButtonVisible = false
        }
    }
    
    func changeStatusOfUpdateButton(atIndex:NSInteger) -> Void
    {
        for  i in (0..<productArray.count)
        {
            if i == atIndex
            {
                productArray[i].isUpdateButtonVisible = true
            }
            else
            {
                productArray[i].isUpdateButtonVisible = false
            }
            
        }
    }
    
    func changeProductStatusForProduct(atIndex:NSInteger) -> Void
    {

                productArray[atIndex].markAsOpen()
       
            
    
    }
    func getTravellerAmount() -> NSInteger
    {
        var travellerAmount : NSInteger = 0
        for  i in (0..<productArray.count)
        {
            let inrAmt : NSInteger = productArray[i].inrAmount
            travellerAmount = travellerAmount + inrAmt
        }
        return travellerAmount
    }
    func getTravellerAmountInDollars() -> NSInteger
    {
        var travellerAmount : Float = 0.0
        for  i in (0..<productArray.count)
        {
            let amountInDollars : Float = productArray[i].amountInDollars
            travellerAmount = travellerAmount + amountInDollars
        }
        return  NSInteger(round(travellerAmount))
    }




}
