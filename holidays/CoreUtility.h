//
//  CoreUtility.h
//  mobicule-cms-ios-core
//
//  Created by Ujwala Patil on 4/29/14.
//  Copyright (c) 2014 Girish Patil. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CoreUtility : NSObject

+ (void) createDirectoryIfNotExists:(NSString *) directoryPath;
+ (NSString *) getContentsRootPath;
+ (NSString *) getContentDirectoryPath:(NSString *)contentId;
//+ (NSString *) getContentItem:(NSString *)contentItemName;
+ (NSString *) getDocumentsDirectoryPath;
+ (BOOL) fileExistsAtPath:(NSString *) contentItemPath;

+ (NSString *) getCategoryRootPath;
+ (NSString *) getCategoryDirectoryPath:(NSString *)categoryId;
+ (NSString *) getEmergencyMediaRootPath;
+ (NSString *) getEmergencyMediaRootPathForImage;
+ (NSString *) getEmergencyMediaRootPathForVideo;
+ (NSString *) getEmergencyMediaRootPathForAudio;
+ (NSString *) getMediaDirectoryPath:(NSString *)messageId WithMediaType:(NSString * )mediaType;

+ (NSString *) convertSystemMillisToDateString:(NSString *) systemMillis WithTimeFormat:(NSString *)timeFormat;
+ (NSDateFormatter*) createDateFormatter12hr:(NSString * )timeFormat;


+ (NSString *) convertSystemMillisToDateString:(NSString *) systemMillis;
+ (NSString *) convertDateToString:(NSDate *) atDate;
+ (NSDate *) createDateFromString:(NSString *) dateString;
+ (NSDate *) createDateFromTimeStampString:(NSString *) dateString;
+ (NSDateFormatter*) createDateFormatter12hr;
+ (BOOL) isFutureDate:(NSString *)date;
+ (NSDateFormatter*) createDateFormatter24hr;
+ (BOOL) isExpiredDate:(NSString *)date;
+ (NSString *) getStreamingIconDirectoryPath:(NSString *)contentID;

+(NSString *)getTimeDiffInStringFromDate:(NSDate*)fromDate WithToDate:(NSDate*)toDate;
+ (id) sharedclassname;
+ (BOOL)connected;
@property id object;
@property (nonatomic, assign) int pageNumber;
@property (nonatomic,assign)id obj;
@property (nonatomic,assign)int statusForResetPackageDetails;
@property (nonatomic,assign)int statusForNetPopUp;
@property(nonatomic,assign)int statusWhichPopUpCalled;
@property(nonatomic,strong)NSMutableArray *strPhoneNumber;
@property(nonatomic)BOOL leadSubmitted;
@property(nonatomic,strong)NSMutableArray *arrForResetPackageData;
@property(nonatomic,strong)NSMutableData *dataForSearch;

+(NSDictionary *)getHeaderDict;
+(void)reloadRequestID;
//+(NSDictionary *)getHeaderDictWithUDID;

@end
