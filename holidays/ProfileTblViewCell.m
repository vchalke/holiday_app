//
//  ProfileTblViewCell.m
//  holidays
//
//  Created by Kush_Team on 26/05/20.
//  Copyright © 2020 Mobicule. All rights reserved.
//

#import "ProfileTblViewCell.h"

@implementation ProfileTblViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setConstraints:(CGFloat)widht{
    self.const_imgWidth.constant = widht;
    self.const_imgHeight.constant = widht;
}
-(void)setProfileImageFromStore:(NSString*)imgStr{
    self.img_Icons.layer.cornerRadius = 20.0;
    NSURL* urlImage=[NSURL URLWithString:imgStr];
        if(urlImage)
        {
            UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            indicator.center = self.img_Icons.center;// it will display in center of image view
            [self.img_Icons addSubview:indicator];
            [indicator startAnimating];
            dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0ul);
            dispatch_async(queue, ^{
    //        dispatch_async(dispatch_get_main_queue(), ^{
               [self.img_Icons sd_setImageWithURL:urlImage placeholderImage:[UIImage imageNamed:@"defaultBanner.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
                {
                   [indicator stopAnimating];
               }];
            });
            
        }
}
@end
