//
//  Notification+CoreDataProperties.h
//  holidays
//
//  Created by Nishant on 5/11/17.
//  Copyright © 2017 Mobicule. All rights reserved.
//

#import "Notification+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Notification (CoreDataProperties)

+ (NSFetchRequest<Notification *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *createdOn;
@property (nullable, nonatomic, copy) NSString *data;
@property (nullable, nonatomic, copy) NSString *id;
@property (nullable, nonatomic, copy) NSString *message;
@property (nullable, nonatomic, copy) NSString *notificationType;
@property (nullable, nonatomic, copy) NSString *redirect;
@property (nullable, nonatomic, copy) NSString *status;
@property (nullable, nonatomic, copy) NSString *title;

@end

NS_ASSUME_NONNULL_END
