//
//  Notification+CoreDataProperties.m
//  holidays
//
//  Created by Nishant on 5/11/17.
//  Copyright © 2017 Mobicule. All rights reserved.
//

#import "Notification+CoreDataProperties.h"

@implementation Notification (CoreDataProperties)

+ (NSFetchRequest<Notification *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Notification"];
}

@dynamic createdOn;
@dynamic data;
@dynamic id;
@dynamic message;
@dynamic notificationType;
@dynamic redirect;
@dynamic status;
@dynamic title;

@end
