//
//  NotificationService.h
//  ServiceExtension
//
//  Created by darshan on 24/11/18.
//  Copyright © 2018 Mobicule. All rights reserved.
//

#import <UserNotifications/UserNotifications.h>
#import <NetCorePush/NetCorePush.h>

@interface NotificationService : UNNotificationServiceExtension

@end
