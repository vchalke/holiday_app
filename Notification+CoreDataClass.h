//
//  Notification+CoreDataClass.h
//  holidays
//
//  Created by Nishant on 5/11/17.
//  Copyright © 2017 Mobicule. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Notification : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Notification+CoreDataProperties.h"
